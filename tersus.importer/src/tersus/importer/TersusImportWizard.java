/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.importer;

import java.io.File;
import java.util.HashSet;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;

import tersus.editor.ConcreteModelIdentifier;
import tersus.editor.RepositoryManager;
import tersus.editor.TersusEditor;
import tersus.editor.projectimport.ImportUtils;
import tersus.model.ModelId;
import tersus.model.Repository;
import tersus.util.Misc;
import tersus.workbench.TersusWorkbench;

/**
 * @author Liat Shiff
 */
public class TersusImportWizard extends Wizard implements IImportWizard
{
	private String filePath;

	private String projectName;

	private boolean needToReplace;
	
	private TersusImportWizardPage mainPage;
	
	private String errorMessage;
	
	private File tersusFile;
	
	private HashSet<String> existProjectList;

	public TersusImportWizard()
	{
		super();
		setExistProjectList();
	}
	
	private void setExistProjectList()
	{
		existProjectList = new HashSet<String>();
		for(IProject project: TersusWorkbench.getWorkspace().getRoot().getProjects())
		{
			existProjectList.add(project.getName());
		}
	}

	@Override
	public boolean performFinish()
	{
		String newProjectName = getProjectName();
		if (newProjectName == null || newProjectName.length() == 0
				|| !Misc.isValidFileName(newProjectName))
			return false;
		
		if (isNeedToReplace())
			ImportUtils.deleteProject(newProjectName);
			
		ImportUtils.importAsProject(newProjectName, tersusFile);
		
		IWorkspace workspace = TersusWorkbench.getWorkspace();
		IProject project = workspace.getRoot().getProject(newProjectName);
		
		RepositoryManager m = RepositoryManager.getRepositoryManager(project);
		Repository repository = m.getRepository();
		ModelId rootModelId = repository.getModelId(newProjectName + "/" + newProjectName);
		
		if (repository.getModel(rootModelId, true) != null)
		{
			IWorkbenchPage page = TersusWorkbench.getActiveWorkbenchWindow().getActivePage();
			try
			{
				page.openEditor(new ConcreteModelIdentifier(project, rootModelId), TersusEditor.ID);
				return true;
			}
			catch (PartInitException e)
			{
				TersusWorkbench.log(e);
				return false;
			}
		}
		
		return true;
	}

	public void init(IWorkbench workbench, IStructuredSelection selection)
	{
		setWindowTitle("Import Tersus File");
	}

	public void addPages()
	{
		super.addPages();
		mainPage = new TersusImportWizardPage(this);
		addPage(mainPage);
	}

	public void setFilePath(String filePath)
	{
		this.filePath = filePath;
		tersusFile = new File(filePath);
	}

	public File getTersusFile()
	{
		return tersusFile;
	}
	
	public String getFilePath()
	{
		return this.filePath;
	}

	public void setProjectName(String name)
	{
		this.projectName = name;
	}

	public String getProjectName()
	{
		return this.projectName;
	}
	
	public String getErrorMessage()
	{
		return errorMessage;
	}
	
	public void validate()
	{
		errorMessage = null;
		
		if (getFilePath() == null || getFilePath().length() == 0)
		{
			errorMessage = "You must choose a Tersus file";
			return;
		}
		else if (!tersusFile.exists())
		{
			errorMessage = "Tersus file does not exit in the current path";
			return;
		}
		else if (getProjectName() == null || getProjectName().length() == 0)
		{
			errorMessage = "A project name can not be empty";
			return;
		}
		else if (!Misc.isValidFileName(getProjectName()))
		{
			errorMessage = "A project name must not contain any of the following charactes: "
                + Misc.INVALID_FILENAME_CHARACTERS;
			
			return;
		}
		else if (isProjectNameAlreadyExist() && !isNeedToReplace())
		{
			errorMessage = "A project with the same name is already exits in your workspace";
			return;
		}
	}
	
	public boolean isProjectNameAlreadyExist()
	{
		return existProjectList.contains(getProjectName());
	}
	
	public boolean isNeedToReplace()
	{
		return needToReplace && mainPage.isRplaceCheckBoxEnable();
	}
	
	public void setNeedToReplace(boolean needToReplace)
	{
		this.needToReplace = needToReplace;
	}
}

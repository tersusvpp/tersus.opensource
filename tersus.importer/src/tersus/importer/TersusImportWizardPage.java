package tersus.importer;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import tersus.editor.projectimport.ImportUtils;

public class TersusImportWizardPage extends WizardPage
{
	private static final String[] FILE_IMPORT_MASK =
	{ "*.tersus", "*.*" };

	private TersusImportWizard wizard;

	private Composite workArea;
	
	private Text filePathText;

	private Text projectNameText;

	private Button browseButton;	

	private Button replaceCheckBox;

	protected TersusImportWizardPage(TersusImportWizard wizard)
	{
		super("Import");

		this.wizard = wizard;

		setTitle("Import Tersus File");
		setDescription("Import Tersus file as new project or library.");
	}

	public void createControl(Composite parent)
	{
		initializeDialogUnits(parent);

		workArea = new Composite(parent, SWT.NONE);
		setControl(workArea);

		workArea.setLayout(new GridLayout());
		workArea.setLayoutData(new GridData(GridData.FILL_BOTH | GridData.GRAB_HORIZONTAL
				| GridData.GRAB_VERTICAL));

		initFilePathText();
		initProjectNameText();
		initReplaceCheckBox();
	}

	private void initFilePathText()
	{
		Composite filePathGroup = new Composite(workArea, SWT.NONE);

		GridLayout layout = new GridLayout(3, false);
		layout.marginWidth = 0;
		filePathGroup.setLayout(layout);
		filePathGroup.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		Label label = new Label(filePathGroup, SWT.NULL);
		label.setText("Choose Tersus file:");
		GridData gd1 = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_CENTER);
		gd1.horizontalSpan = 1;
		label.setLayoutData(gd1);

		filePathText = new Text(filePathGroup, SWT.BORDER | SWT.SINGLE);
		GridData gd2 = new GridData(GridData.FILL_HORIZONTAL);
		filePathText.setLayoutData(gd2);

		browseButton = new Button(filePathGroup, SWT.PUSH);
		browseButton.setText("Browse...");

		browseButton.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				handleFilePathButtonPressed();
			}
		});

		filePathText.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				wizard.setFilePath(filePathText.getText());
				projectNameText.setText(ImportUtils.getProjectName(wizard.getTersusFile()));
				dialogChanged();
			}
		});

		setPageComplete(false);
	}
	
	private void initProjectNameText()
	{
		Composite projectNameGroup = new Composite(workArea, SWT.NONE);

		GridLayout layout = new GridLayout(2, false);
		layout.marginWidth = 0;
		projectNameGroup.setLayout(layout);
		projectNameGroup.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		Label label = new Label(projectNameGroup, SWT.NULL);
		label.setText("Project Name:");
		GridData gd1 = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_CENTER);
		gd1.horizontalSpan = 1;
		label.setLayoutData(gd1);

		projectNameText = new Text(projectNameGroup, SWT.BORDER | SWT.SINGLE);
		GridData gd2 = new GridData(GridData.FILL_HORIZONTAL);
		projectNameText.setLayoutData(gd2);

		projectNameText.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				wizard.setProjectName(projectNameText.getText());
				
				if (replaceCheckBox != null)
					replaceCheckBox.setEnabled(wizard.isProjectNameAlreadyExist());
				
				dialogChanged();
			}
		});

		setPageComplete(false);
	}

	private void initReplaceCheckBox()
	{
		replaceCheckBox = new Button(workArea, SWT.CHECK);
		GridData gd2 = new GridData(GridData.FILL_HORIZONTAL);
		replaceCheckBox.setLayoutData(gd2);

		replaceCheckBox.setText("Replace existing project");
		
		
		replaceCheckBox.addSelectionListener(new SelectionListener()
		{
			public void widgetSelected(SelectionEvent e)
			{
				wizard.setNeedToReplace(replaceCheckBox.getSelection());
				dialogChanged();
			}

			public void widgetDefaultSelected(SelectionEvent e)
			{
				wizard.setNeedToReplace(replaceCheckBox.getSelection());
				dialogChanged();
			}
		});
		
		replaceCheckBox.setEnabled(false);
		setPageComplete(false);
	}

	private void handleFilePathButtonPressed()
	{
		FileDialog dialog = new FileDialog(workArea.getShell());
		dialog.setFilterExtensions(FILE_IMPORT_MASK);
		dialog.setText("Select Tersus file");

		String path = filePathText.getText().trim();
		String filePath = null;

		if (path != null)
		{
			int index = path.lastIndexOf('/');
			if (index > 0)
				filePath = path.substring(0, index + 1);
		}
		dialog.setFilterPath(filePath);
		String selectedArchive = dialog.open();
		if (selectedArchive != null)
			filePathText.setText(selectedArchive);
	}

	private void dialogChanged()
	{
		wizard.validate();
		String errorMessage = wizard.getErrorMessage();
		setErrorMessage(errorMessage);
		setPageComplete(errorMessage == null);
	}
	
	public boolean isRplaceCheckBoxEnable()
	{
		return replaceCheckBox != null && replaceCheckBox.isEnabled();
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.importer;

import java.util.ArrayList;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

/**
 * @author Liat Shiff
 */
public class ImportFileDialog extends TitleAreaDialog
{
	protected Composite composite;

	private String newProjectName;

	private ArrayList<IProject> projectList;

	private boolean importAsProject;

	private boolean needToReplace;

	protected ImportFileDialog(Shell parentShell, String newProjectName,
			ArrayList<IProject> projectList)
	{
		super(parentShell);

		this.newProjectName = newProjectName;
		this.projectList = projectList;
	}

	protected Control createDialogArea(Composite parent)
	{
		initDialogArea(parent);

		return composite;
	}

	private void initDialogArea(Composite parent)
	{
		// create the top level composite for the dialog area
		composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.marginHeight = 0;
		layout.numColumns = 2;
		layout.verticalSpacing = 9;
		composite.setLayout(layout);
		composite.setLayoutData(new GridData(GridData.FILL_BOTH));
		composite.setFont(parent.getFont());

		// Build the separator line
		Label titleBarSeparator = new Label(composite, SWT.HORIZONTAL | SWT.SEPARATOR);
		GridData titleBarSeparatorGridData = new GridData(GridData.FILL_HORIZONTAL);
		titleBarSeparatorGridData.horizontalSpan = 2;
		titleBarSeparator.setLayoutData(titleBarSeparatorGridData);

		initializeDialogUnits(composite);
	}

	protected void configureShell(Shell shell)
	{
		super.configureShell(shell);
		shell.setText("Import Tersus File");

		Composite comp = shell.getParent();
		int width = comp.getBounds().width;
		int height = comp.getBounds().height;

		shell.setBounds(width / 2 - 250, height / 2 - 225, 550, 300);
	}

	public boolean isImportAsProject()
	{
		return importAsProject;
	}

	public void setImportAsProject(boolean importAsProject)
	{
		this.importAsProject = importAsProject;
	}

	protected void setting(boolean enableOkButton, String messageText, String errorText)
	{
		getButton(OK).setEnabled(enableOkButton);
		setMessage(messageText);
		setErrorMessage(errorText);
	}

	protected boolean isProjectNameAlreadyExist(String projectName)
	{
		for (IProject project : projectList)
		{
			if (projectName.equals(project.getName()))
				return true;
		}
		return false;
	}

	protected void setProjectName(String projectName)
	{
		this.newProjectName = projectName;
	}

	public String getProjectName()
	{
		return newProjectName;
	}

	protected void setNeedToReplace(Boolean needToReplace)
	{
		this.needToReplace = needToReplace;
	}

	public boolean isNeedToReplace()
	{
		return needToReplace;
	}
	
	public ArrayList<IProject> getProjectList()
	{
		return projectList;
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.importer.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;

import tersus.util.FileUtils;

/**
 * @author Liat Shiff
 */
public class Importer
{
	private final static String TERSUS_DIR = ".tersus";
	private final static String LOCK_FILE = "tersus.lock";

	public static void main(String[] args)
	{
		try
		{
			File tersusDir = getTersusDir();

			copyFileToTersusDropinDirectory(args[0]);

			if (!isTersusPlatformUp(tersusDir))
				openTersusPlatform(tersusDir);
		}
		catch (IOException e)
		{
			System.out.println(e.getMessage());
		}

	}

	private static File getTersusDir() throws IOException
	{
		File tersusDir = new File(System.getProperty("user.home") + "/" + TERSUS_DIR);

		if (!tersusDir.exists())
			throw new IOException("Tersus drop-in file was not found in '"
					+ System.getProperty("user.home") + "'");
		return tersusDir;
	}

	public static void copyFileToTersusDropinDirectory(String tersusFilePath)
	{
		try
		{
			File tersusFile = new File(tersusFilePath);
			if (!tersusFile.exists())
				throw new IOException("Tersus file '" + tersusFilePath + "' was not found.");

			File dropinDir = new File(getTersusDir(),"dropins");
			if (!dropinDir.exists())
				dropinDir.mkdir();

			File targetFile = new File(dropinDir,tersusFile.getName());
			File tempFile = new File(dropinDir, targetFile.getName()+".temp");

			InputStream in = new FileInputStream(tersusFile);
			OutputStream out = new FileOutputStream(tempFile);

			FileUtils.copy(in, out, true);
			tempFile.renameTo(targetFile);
		}
		catch (FileNotFoundException ex)
		{
			System.out.println(ex.getMessage() + " in the specified directory.");
			System.exit(0);
		}
		catch (IOException e)
		{
			System.out.println(e.getMessage());
		}
	}

	public static void openTersusPlatform(File tersusDir)
	{
		File tersusStudioCommand = new File(tersusDir.getPath().toString()
				+ "/tersus-studio-command");

		if (tersusStudioCommand.exists())
		{
			try
			{
				String[] lines = readFileIntoString(tersusStudioCommand);
				String[] cmdArray = new String[lines.length - 1];
				for (int index = 1; index < lines.length; index++)
				{
					cmdArray[index - 1] = lines[index];
				}
				File workingDir = new File(lines[0]);
				Runtime.getRuntime().exec(cmdArray, null, workingDir);
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		return;
	}

	public static boolean isTersusPlatformUp(File tersusDir)
	{
		try
		{
			File file = new File(tersusDir.getPath().toString() + "/" + LOCK_FILE);

			if (file.exists())
			{
				FileChannel channel = new RandomAccessFile(file, "rw").getChannel();
				FileLock fLock = channel.tryLock();

				if (fLock == null)
					return true;
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		return false;
	}

	public static String[] readFileIntoString(File fileToRead)
	{
		ArrayList<String> lines = new ArrayList<String>();
		BufferedReader reader;
		try
		{
			reader = new BufferedReader(new FileReader(fileToRead.getPath().toString()));
			String thisLine = null;
			while ((thisLine = reader.readLine()) != null)
			{
				lines.add(thisLine);
			}
			reader.close();
		}
		catch (FileNotFoundException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String[] cmds = new String[lines.size()];

		for (int i = 0; i < lines.size(); i++)
		{
			cmds[i] = lines.get(i);
		}
		return cmds;
	}
}
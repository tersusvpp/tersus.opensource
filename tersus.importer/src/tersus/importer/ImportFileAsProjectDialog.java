/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.importer;

import java.util.ArrayList;

import org.eclipse.core.resources.IProject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * @author Liat Shiff
 */
public class ImportFileAsProjectDialog extends ImportFileDialog
{
	// Widgets
	private Text importFileAsProjectText;

	private Button replaceButton;

	protected ImportFileAsProjectDialog(Shell parentShell, String newProjectName,
			String tersusFileName, ArrayList<IProject> projectList)
	{
		super(parentShell, newProjectName, projectList);

		setTitle("Importer");
		setImportAsProject(true);
	}

	protected Control createDialogArea(Composite parent)
	{
		super.createDialogArea(parent);
		addProjectTextField();
		addOverwriteButton();

		validate();

		return composite;
	}

	private void addProjectTextField()
	{
		Label importFileAsProjectLabel = new Label(composite, SWT.NONE);
		importFileAsProjectLabel.setText("Import as &project named:");

		importFileAsProjectText = new Text(composite, SWT.BORDER | SWT.SINGLE);
		importFileAsProjectText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_CENTER));

		importFileAsProjectText.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				setProjectName(importFileAsProjectText.getText());
				validate();
			}
		});

		importFileAsProjectText.setText(getProjectName());
	}

	private void addOverwriteButton()
	{
		replaceButton = new Button(composite, SWT.CHECK);
		replaceButton.setText("Replace existing project");
		replaceButton.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true,
				GridData.HORIZONTAL_ALIGN_END, GridData.VERTICAL_ALIGN_CENTER));

		replaceButton.addSelectionListener(new SelectionListener()
		{
			public void widgetSelected(SelectionEvent e)
			{
				setNeedToReplace(replaceButton.getSelection());
				validate();
			}

			public void widgetDefaultSelected(SelectionEvent e)
			{
				setNeedToReplace(replaceButton.getSelection());
				validate();
			}
		});
	}

	private void validate()
	{
		if (getButton(OK) != null && importFileAsProjectText != null && replaceButton != null)
		{
			if (isProjectNameAlreadyExist(importFileAsProjectText.getText()))
			{
				replaceButton.setEnabled(true);
				if (isNeedToReplace())
					setting(
							true,
							"Project with the same name is already exists. By select the 'Replace existing project' option, "
									+ "the importer will replace the old project with the new one.",
							null);
				else
					setting(
							false,
							null,
							"Project with the same name is already exists."
									+ " You can overwrite the existing project by checking the 'Replace existing project' option.");
			}
			else
			{
				setting(true, "Import .tersus file as project.", null);
				replaceButton.setEnabled(false);
			}
		}
	}

	protected void createButtonsForButtonBar(Composite parent)
	{
		super.createButtonsForButtonBar(parent);
		validate();
	}
}

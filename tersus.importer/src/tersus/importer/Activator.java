/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.importer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import tersus.util.FileUtils;
import tersus.util.ProcessRunner;

/**
 * @author Liat Shiff
 */
public class Activator extends AbstractUIPlugin
{
	private ImportFile listener;

	public void start(BundleContext context) throws Exception
	{
		super.start(context);
		listener = new ImportFile();
		listener.start();
		copyImporterFilesToEclipseWorkDirectory();

		setFile("tersus-studio-command", System.getProperty("user.dir") + "\n"
				+ System.getProperty("eclipse.launcher") + "\n"
				+ System.getProperty("eclipse.commands"));
		setFile("java-home", System.getProperty("java.home"));
	}

	private void copyImporterFilesToEclipseWorkDirectory() throws Exception
	{
		copyImporterFileToEclipseWorkDirectory("tersus-importer.jar");
		copyImporterFileToEclipseWorkDirectory("tersus-importer.sh");
		copyImporterFileToEclipseWorkDirectory("tersus-importer.bat");
	}

	private void copyImporterFileToEclipseWorkDirectory(String importerFile) throws Exception
	{
		URL fileUrl = getBundle().getResource(importerFile);
		Long lastModefied = new Long(-1);
		Long bundleLastModified = getBundle().getLastModified();

		String tersusDropinFolder = listener.getTersusFolder().getPath().toString();

		String filePath = tersusDropinFolder + "/" + importerFile;
		File file = new File(filePath);

		if (file.exists())
			lastModefied = file.lastModified();

		if (lastModefied < bundleLastModified)
			FileUtils.copy(fileUrl.openStream(), new FileOutputStream(filePath), true);

		setExecutePremissiomOnFile(filePath);
	}

	private void setExecutePremissiomOnFile(String filePath)
	{
		if (!System.getProperty("os.name").toLowerCase().contains("win"))
		{
			try
			{
				ProcessRunner p = new ProcessRunner();
				String[] cmd =
				{ "chmod", "u+x", filePath };
				p.run(cmd, 2000);
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}

	private void setFile(String fileToSetName, String content)
	{
		String tersusDropinFolder = listener.getTersusFolder().getPath().toString();

		String newFilePath = tersusDropinFolder + "/" + fileToSetName;
		File newTersusStudioCommandFile = new File(newFilePath);

		try
		{
			if (!newTersusStudioCommandFile.exists())
				newTersusStudioCommandFile.createNewFile();

			FileWriter fileWriter = new FileWriter(newTersusStudioCommandFile);
			fileWriter.write(content);
			fileWriter.close();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void stop(BundleContext context) throws Exception
	{
		if (listener != null)
			listener.releaseSharedFolder();

		File file = new File(listener.getTersusFolder().getPath() + "/tersus.lock");
		if (file.exists())
			file.delete();
	}
}

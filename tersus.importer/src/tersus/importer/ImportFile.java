/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. -  Initial API and implementation
 *************************************************************************************************/
package tersus.importer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import tersus.ProjectStructure;
import tersus.editor.RepositoryManager;
import tersus.editor.projectimport.ImportUtils;
import tersus.editor.projectimport.ZipFileUtils;
import tersus.util.Misc;
import tersus.util.ReadXMLUtils;
import tersus.workbench.ConfigurationConstants;
import tersus.workbench.TersusWorkbench;

/**
 * @author Liat Shiff
 */
public class ImportFile
{
	private final String TERSUS_FILE_EXTENSION = "tersus";

	public final File USER_HOME_FOLDER = new File(System.getProperty("user.home"));

	private ArrayList<IProject> projectList;

	private File tersusFolder;

	private File dropinFolder;

	private ArrayList<File> dropinFileList;
	
	private Timer timer;

	private FileLock fileLock;

	private FileChannel channel;

	public void start()
	{
		lockSharedFolder();
		dropinFileList = new ArrayList<File>();
		startTimer();
	}

	private void startTimer()
	{
		timer = new Timer(true);
		timer.schedule(new FileMonitorNotifier(), 0, 5000);
	}

	private void lockSharedFolder()
	{
		try
		{
			if (!getTersusFolder().exists())
				getTersusFolder().mkdir();

			File file = new File(getTersusFolder().getPath().toString() + "/tersus.lock");
			file.createNewFile(); // only creates a new file if there is not existing file

			channel = new RandomAccessFile(file, "rw").getChannel();
			fileLock = channel.lock();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// File is already locked in this thread or virtual machine
		}
	}

	public File getTersusFolder()
	{
		if (tersusFolder == null)
			tersusFolder = new File(USER_HOME_FOLDER.getPath().toString() + "/.tersus");

		return tersusFolder;
	}

	public File getDropinFolder()
	{
		if (dropinFolder == null)
			dropinFolder = new File(getTersusFolder().getPath().toString() + "/dropins");

		return dropinFolder;
	}

	public void releaseSharedFolder()
	{
		try
		{
			if (fileLock != null)
				fileLock.release();

			channel.close();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// File is already locked in this thread or virtual machine
		}
	}

	private class FileMonitorNotifier extends TimerTask
	{
		public void run()
		{
			for (File file : dropinFileList)
			{
				if (file.delete())
					dropinFileList.remove(file);
			}
			File[] zipFileList = getDropinFolder().listFiles();
			if (zipFileList != null && zipFileList.length > 0)
			{
				for (File file : zipFileList)
				{
					if (!dropinFileList.contains(file) && isValidFile(file, TERSUS_FILE_EXTENSION))
					{
						final File zipFile = file;

						Display.getDefault().syncExec(new Runnable()
						{
							public void run()
							{
								String newProjectName = ImportUtils.getProjectName(zipFile);

								Shell shell = TersusWorkbench.getActiveWorkbenchWindow().getShell();

								boolean canBeImportAsLibrary = importAsLibrary(zipFile);

								ImportFileDialog dialog = null;
								if (canBeImportAsLibrary)
									dialog = new ImportFileAsProjectOrLibraryDialog(shell,
											newProjectName, zipFile.getName(), getProjectList());
								else
									dialog = new ImportFileAsProjectDialog(shell, newProjectName,
											zipFile.getName(), getProjectList());

								dialog.open();

								if (dialog.getReturnCode() == Window.OK)
								{
									if (dialog.isImportAsProject())
									{
										if (dialog.isNeedToReplace())
											ImportUtils.deleteProject(dialog.getProjectName());

										ImportUtils.importAsProject(dialog.getProjectName(), zipFile);
									}
									else
										importAsLibrary(dialog.getProjectName(), zipFile);
								}
//								zipFile.delete();
							}

						});
					}
//					else
//						file.delete();
					dropinFileList.add(file);
				}
			}
		}

		private IProject getProject(String projectName)
		{
			for (IProject project : getProjectList())
			{
				if (projectName.equals(project.getName()))
					return project;
			}

			return null;
		}

		private ArrayList<IProject> getProjectList()
		{
			IWorkspace workspace = TersusWorkbench.getWorkspace();
			IProject[] projects = workspace.getRoot().getProjects();

			projectList = new ArrayList<IProject>();
			for (IProject project : projects)
			{
				projectList.add(project);
			}

			return projectList;
		}

		private void importAsLibrary(String newProjectName, File zipFile)
		{
			IProject projectToAddLibraryInto = getProject(newProjectName);
			IFolder librariesFolder = projectToAddLibraryInto.getFolder("libraries");

			RepositoryManager repositoryManger = RepositoryManager
					.getRepositoryManager(projectToAddLibraryInto);

			try
			{
				if (!librariesFolder.exists())
					librariesFolder.create(true, true, null);

				IFile tersusFile = librariesFolder.getFile(zipFile.getName());

				Misc.assertion(!tersusFile.exists());
				tersusFile.create(new FileInputStream(zipFile), false, null);
			}
			catch (CoreException e)
			{
				e.printStackTrace();
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}

			repositoryManger.refresh();
		}

		private boolean isValidFile(File file, String extension)
		{
			String zipFileName = file.getName();
			String zipFileExtension = zipFileName.substring(zipFileName.lastIndexOf('.') + 1);

			return extension.equals(zipFileExtension);
		}

		private boolean importAsLibrary(File file)
		{
			ZipFile zipFile;
			try
			{
				zipFile = new ZipFile(file);
				InputStream in = ZipFileUtils.getFileFromZip(zipFile, ProjectStructure.CONFIGURATION_XML);
				if (in == null)
					return false;
				else
				{
					Document doc = getDocument(in);
					Element e = ReadXMLUtils.getElement(doc, ConfigurationConstants.APPLICATION_TAG,
							false);
					return e!= null && "yes".equalsIgnoreCase(e.getAttribute(ConfigurationConstants.LIBRARY_ATTR));
				}

			}
			catch (ZipException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}

			return false;
		}

		private Document getDocument(InputStream in)
		{
			try
			{
				DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				Document document = builder.parse(in);
				document.normalize();

				return document;
			}
			catch (ParserConfigurationException e)
			{
				e.printStackTrace();
			}
			catch (SAXException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}

			return null;
		}
	}
}

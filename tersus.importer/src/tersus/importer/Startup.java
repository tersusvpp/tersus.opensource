/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.importer;

import org.eclipse.ui.IStartup;

/**
 * @author Liat Shiff
 */
public class Startup implements IStartup
{
	public void earlyStartup()
	{
	}
}

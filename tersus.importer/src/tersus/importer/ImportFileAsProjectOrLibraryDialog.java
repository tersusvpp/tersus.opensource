/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.importer;

import java.util.ArrayList;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import tersus.editor.RepositoryManager;
import tersus.workbench.TersusWorkbench;

/**
 * @author Liat Shiff
 */
public class ImportFileAsProjectOrLibraryDialog extends ImportFileDialog
{
	private final String COMBO_INITIAL_TEXT = "Choose a project";

	private String tersusFileName;

	// Widgets
	private Button importAsProjectButton;

	private Button importAsLibraryButton;

	private Text projectTextField;

	private Button replaceButton;

	protected Combo projectCombo;

	protected ImportFileAsProjectOrLibraryDialog(Shell parentShell, String newProjectName,
			String tersusFileName, ArrayList<IProject> projectList)
	{
		super(parentShell, newProjectName, projectList);
		this.tersusFileName = tersusFileName;
	}

	protected Control createDialogArea(Composite parent)
	{
		super.createDialogArea(parent);
		addButtons();
		addOverwriteButton();

		return composite;
	}

	private void addButtons()
	{
		importAsProjectButton = new Button(composite, SWT.RADIO);
		importAsProjectButton.setText("Import as &project named:");
		importAsProjectButton.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				importAsProjectButton.setSelection(true);
				projectTextField.setEditable(true);

				if (importAsLibraryButton != null && projectCombo != null)
				{
					setImportAsProject(true);
					setWidgetsValues();
					validate();
				}

				if (projectTextField != null)
					setProjectName(projectTextField.getText());
			}
		});

		projectTextField = new Text(composite, SWT.BORDER | SWT.SINGLE);
		projectTextField.setLayoutData(new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_CENTER));

		projectTextField.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				setProjectName(projectTextField.getText());
				validate();
			}
		});

		setProjectName(getProjectName());
		projectTextField.setText(getProjectName());

		importAsLibraryButton = new Button(composite, SWT.RADIO);
		importAsLibraryButton.setText("Import as &library into:");
		importAsLibraryButton.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				setImportAsProject(false);
				setWidgetsValues();
				validate();
			}
		});
		initProjectComboBox();
		setImportAsProject(false);
		setWidgetsValues();
	}

	protected void initProjectComboBox()
	{
		projectCombo = new Combo(composite, SWT.NULL);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		projectCombo.setLayoutData(gd);

		projectTextField.setText(getProjectName());

		for (IProject project : getProjectList())
		{
			if (project.isOpen() && TersusWorkbench.isTersusProject(project))
			{
				String projectName = project.getName();
				projectCombo.add(projectName);
				if (getProjectName().equals(projectName))
					projectCombo.setText(projectName);
			}
		}

		projectCombo.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				setProjectName(projectCombo.getText());
				validate();
			}
		});

		projectCombo.setText(COMBO_INITIAL_TEXT);
	}

	private void setWidgetsValues()
	{
		importAsProjectButton.setSelection(isImportAsProject());
		projectTextField.setEnabled(isImportAsProject());

		if (importAsLibraryButton != null && projectCombo != null)
		{
			importAsLibraryButton.setSelection(!isImportAsProject());
			projectCombo.setEnabled(!isImportAsProject());
		}
	}

	private void addOverwriteButton()
	{
		replaceButton = new Button(composite, SWT.CHECK);
		replaceButton.setText("Replace existing project");
		replaceButton.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true,
				GridData.HORIZONTAL_ALIGN_END, GridData.VERTICAL_ALIGN_CENTER));

		replaceButton.addSelectionListener(new SelectionListener()
		{
			public void widgetSelected(SelectionEvent e)
			{
				setNeedToReplace(replaceButton.getSelection());
				validate();
			}

			public void widgetDefaultSelected(SelectionEvent e)
			{
				setNeedToReplace(replaceButton.getSelection());
				validate();
			}
		});

		if (isImportAsProject())
			replaceButton.setVisible(true);
	}

	protected void configureShell(Shell shell)
	{
		super.configureShell(shell);
		shell.setText("Import Tersus File");

		Composite comp = shell.getParent();
		int width = comp.getBounds().width;
		int height = comp.getBounds().height;

		shell.setBounds(width / 2 - 250, height / 2 - 225, 550, 300);
	}
	
	private void validate()
	{
		setTitle("Importer");
		if (getButton(OK) != null)
		{
			if (isImportAsProject())
			{
				replaceButton.setEnabled(true);
				replaceButton.setVisible(true);

				if (projectTextField != null && projectTextField.isEnabled()
						&& isProjectNameAlreadyExist(projectTextField.getText()))
				{
					replaceButton.setEnabled(true);
					if (isNeedToReplace())
						setting(
								true,
								"Project with the same name is already exists. By select the 'Replace existing project' option, "
										+ "the importer will replace the old project with the new one.",
								null);
					else
						setting(
								false,
								null,
								"Project with the same name is already exists."
										+ " You can overwrite the existing project by checking the 'Replace existing project' option.");
				}
				else
				{
					setting(true, "Import .tersus file as project.", null);
					replaceButton.setEnabled(false);
				}
			}
			else
			// If import as library
			{
				replaceButton.setVisible(false);

				if (projectCombo != null
						&& (projectCombo.getText() == null || projectCombo.getText().length() == 0)
						|| COMBO_INITIAL_TEXT.equals(projectCombo.getText()))
				{
					replaceButton.setEnabled(false);
					setting(false, null, "Select a destenation project for the imported library.");
				}
				else
				{
					IProject project = getProject(projectCombo.getText());
					RepositoryManager repositoryManager = RepositoryManager
							.getRepositoryManager(project);
					IFolder librariesFolder = repositoryManager.getLibrariesFolder();

					if (librariesFolder.exists()
							&& librariesFolder.getFile(tersusFileName).exists())
					{
						if (!replaceButton.getSelection())
							setting(true, null, "You cannot import '" + tersusFileName
									+ "' as library into '" + projectCombo.getText()
									+ "' because a file with the same name is already exists. "
									+ "You should check 'Replace' button in order to continue.");
						if (replaceButton != null)
							replaceButton.setVisible(true);
					}
					else
						setting(true, "Import .tersus file as library.", null);
				}
			}
		}
	}

	private IProject getProject(String projectName)
	{
		for (IProject project : getProjectList())
		{
			if (project.getName().equals(projectName))
				return project;
		}

		return null;
	}

	protected void createButtonsForButtonBar(Composite parent)
	{
		super.createButtonsForButtonBar(parent);
		validate();
	}

	protected void buttonPressed(int buttonId)
	{
		if (IDialogConstants.OK_ID == buttonId)
		{
			if (!replaceButton.isEnabled())
				setNeedToReplace(false);

			okPressed();
		}
		else if (IDialogConstants.CANCEL_ID == buttonId)
			cancelPressed();
	}
}

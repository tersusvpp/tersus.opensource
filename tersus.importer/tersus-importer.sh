#!/bin/bash
f=$1 #full path if path is absolute
ff=$PWD/$1 #full path it path is relative

if [ "${f:0:1}" = "/" ] ; then
        FILE="$f"
else
         FILE="$ff"
fi
cd `dirname $0`
JAVA_HOME=`cat java-home`
export PATH=$JAVA_HOME:$PATH
java -jar tersus-importer.jar "$FILE"
@echo off

rem Get fully qualified (short) path name of the .tersus file 
set TERSUSFILE="%~f1"

rem Change to the batch file (%USERPROFILE%\.tersus\) directory
cd "%~dp0"

rem Add JRE to path
set /p JAVA_HOME=<java-home
set PATH=%PATH%;%JAVA_HOME%\bin;

rem Launch importer
javaw -jar tersus-importer.jar %TERSUSFILE%



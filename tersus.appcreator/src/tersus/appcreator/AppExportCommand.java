/************************************************************************************************
 * Copyright (c) 2003-2019 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
package tersus.appcreator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import tersus.workbench.TersusWorkbench;

public class AppExportCommand extends AbstractHandler
{
	public AppExportCommand() // Constructor
	{
	}

	public Object execute(ExecutionEvent event) throws ExecutionException
	{
		// The command has been executed, so extract the needed information from the application context
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		IEditorPart targetEditor = (IEditorPart) window.getActivePage().getActiveEditor();
		IProject project;

		if (targetEditor instanceof IAdaptable)
		{
			project = (IProject)((IAdaptable)targetEditor).getAdapter(IProject.class);

			AppCreatorWizard wizard = new AppCreatorWizard();
			wizard.setProject(project);
			WizardDialog dialog = new WizardDialog(TersusWorkbench.getActiveWorkbenchShell(), wizard);
			dialog.open();
		}

		return null;
	}

}

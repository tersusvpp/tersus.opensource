/************************************************************************************************
 * Copyright (c) 2003-2021 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
package tersus.appcreator;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.ui.IExportWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.part.FileEditorInput;
import org.osgi.framework.Bundle;

import tersus.appserver.AppserverPlugin;
import tersus.model.Model;
import tersus.util.FileUtils;
import tersus.util.Misc;
import tersus.webapp.appexport.AndroidAppCreator;
import tersus.webapp.appexport.CordovaAppCreator;
import tersus.webapp.appexport.ExportException;
import tersus.webapp.appexport.ExportUtils;
import tersus.webapp.appexport.IAndroidCommandHelper;
import tersus.webapp.appexport.IExportAdapter;
import tersus.webapp.appexport.IPhoneAppCreator;
import tersus.webapp.appexport.IProcessDialog;
import tersus.webapp.appexport.MobileAppCreator;
import tersus.webapp.appexport.Win8AppCreator;
import tersus.workbench.Configurator;
import tersus.workbench.TersusWorkbench;

public class AppCreatorWizard extends Wizard implements IExportWizard, IExportAdapter
{
    public static final String WIZARD_NAME="Export a Mobile Application";
    
    public static final String IPHONE = "iPhone";
	public static final String ANDROID = "Android";
	public static final String WIN8 = "Windows 8";
	public static final String CORDOVA = "Cordova";

	private IProject project;

	private Properties properties;
	private String applicationIdentifier = "";

	public void addPages()
	{
		addPage(new AppCreatorWizardPage(this));
	}

	public String getDeviceName()
	{
		return properties.getProperty(getDeviceNamePropertyKey());
	}

	public void setDeviceName(String deviceName)
	{
		properties.setProperty(getDeviceNamePropertyKey(), deviceName);
	}

	public String getClassName()
	{
		return properties.getProperty(getClassNamePropertyKey());
	}

	public void setClassName(String className)
	{
		properties.setProperty(getClassNamePropertyKey(), className);
	}

	public String getPackageName()
	{
		return properties.getProperty(getPackageNamePropertyKey());
	}

	public void setPackageName(String packageName)
	{
		properties.setProperty(getPackageNamePropertyKey(), packageName);
	}

	public String getApplicationIdentifier()
	{
		return applicationIdentifier;
	}

	public void setApplicationIdentifier(String bundleIdentifierPrefix)
	{
		this.applicationIdentifier = bundleIdentifierPrefix;
	}

	public String getExportLocation()
	{
		String exportLocation = properties.getProperty(getExportLocationPropertyKey());
		if (exportLocation == null)
		{
			exportLocation = getDefaultExportFolder(project).getPath();
			setExportLocation(exportLocation);
		}
		return exportLocation;
	}

	public void setExportLocation(String exportLocation)
	{
		properties.setProperty(getExportLocationPropertyKey(), exportLocation);
		File exportFolder = new File(exportLocation);
		File info = new File(exportFolder, "Info.plist");
		try
		{
			if (info.isFile())
			{
				String content = FileUtils.readString(new FileInputStream(info), null, true);
				Pattern p = Pattern
						.compile("<key>CFBundleIdentifier</key>[^<]*<string>([^<]*)</string>");
				Matcher m = p.matcher(content);
				if (m.find())
				{
					setApplicationIdentifier(m.group(1));
				}
			}
		}
		catch (IOException e)
		{
			// Ignore
		}
	}

	public IProject getProject()
	{
		return project;
	}

	public void setProject(IProject project)
	{
		this.project = project;
		setApplicationIdentifier(Misc.sqlize(project.getName()));
		loadExportProperties();
	}

	private void loadExportProperties() {
		properties = getProperties();
	}

	public AppCreatorWizard()
	{
		super();
		setWindowTitle(WIZARD_NAME);
	}

	@Override
	public boolean performFinish()
	{
		try
		{
			if (getContainer().getCurrentPage() instanceof WizardPage)
				((WizardPage) getContainer().getCurrentPage()).setErrorMessage(null);
			export();
		}
		catch (Exception e)
		{
			TersusWorkbench.log(e);
			String errorMessage;
			if (e instanceof ExportException)
				errorMessage = e.getMessage();
			else
				errorMessage = Misc.getShortName(e.getClass()) + " : " + e.getMessage();
			if (getContainer().getCurrentPage() instanceof WizardPage)
				((WizardPage) getContainer().getCurrentPage()).setErrorMessage(errorMessage);
			return false;
		}
		return true;
	}

	private void saveExportProperties()
	{
		ExportUtils.saveProperties(this, properties);
	}
	
	private void export()
	{

		MobileAppCreator creator = null;
		saveExportProperties();
		if (ANDROID.equals(getTargetPlatform()) && getDeviceName() != null)
			creator = new AndroidAppCreator(this);
		else if (IPHONE.equals(getTargetPlatform()))
			creator = new IPhoneAppCreator(this);
		else if (WIN8.equals(getTargetPlatform()))
			creator = new Win8AppCreator(this);
		else if (CORDOVA.equals(getTargetPlatform()))
			creator = new CordovaAppCreator(this);

		if (creator != null)
			creator.export();
	}

	public void init(IWorkbench workbench, IStructuredSelection selection)
	{
		if (selection.size() == 1 && selection.getFirstElement() instanceof IProject)
			project = (IProject) selection.getFirstElement();
	}

	private File getProjectFolder(IProject project)
	{
		return project.getLocation().toFile();
	}
	
	private File getDefaultExportFolder(IProject project)
	{
		return new File(getProjectFolder(project), "export/" + ExportUtils.getPlatformKey(this));
	}

	private String getPropertyKey(String subKey)
	{
		String key = ExportUtils.getPlatformKey(this)+'.'+subKey;
		return key;
	}

	private String getExportLocationPropertyKey()
	{
		return getPropertyKey(ExportUtils.EXPORT_LOCATION_PROPERTY_SUBKEY);
	}

	private String getClassNamePropertyKey()
	{
		return getPropertyKey(ExportUtils.CLASS_NAME_PROPERTY_SUBKEY);
	}

	private String getPackageNamePropertyKey()
	{
		return getPropertyKey(ExportUtils.PACKAGE_NAME_PROPERTY_SUBKEY);
	}

	private String getDeviceNamePropertyKey()
	{
		return getPropertyKey(ExportUtils.DEVICE_NAME_PROPERTY_SUBKEY);
	}

	protected Properties getProperties()
	{
		Properties p = ExportUtils.getExportProperties(this);
		return p;
	}

	public String validate()
	{
		return validateLocation(getExportLocation());
	}

	public String validateLocation(String location)
	{
		if (location == null)
			return "Please select export location";
		if (ANDROID.equals(getTargetPlatform()) && location.indexOf(' ') >= 0)
			return "Can't export to locations where path contains blank (on Android)";
		return null;
	}

	public String getTargetPlatform()
	{
		return properties.getProperty(ExportUtils.LAST_TARGET_PLATFORM_PROPERTY_KEY, ANDROID);
	}

	public void setTargetPlatform(String targetPlatform)
	{
		properties.setProperty(ExportUtils.LAST_TARGET_PLATFORM_PROPERTY_KEY, targetPlatform);
	}

	public Model getRootModel()
	{
		return Configurator.getRootModel(getProject());
	}

	public File getProjectFolder()
	{
		return getProject().getLocation().toFile();
	}

	public IProcessDialog getProcessDialog()
	{
		return new ProcessDialog(getShell());
	}

	public void reportError(int rc, String message, String output, String error)
	{

		try
		{
			if (rc != 0)
			{
				MessageDialog
						.openError(TersusWorkbench.getActiveWorkbenchShell(), message, error);
				TersusWorkbench
						.getActiveWorkbenchWindow()
						.getActivePage()
						.openEditor(new FileEditorInput(getExportLogFile()),
								EditorsUI.DEFAULT_TEXT_EDITOR_ID);
			}
		}
		catch (Exception e)
		{
			TersusWorkbench.error("Internal Error", e);
		}
	}

	public void writeExportLog(String message)
	{
		try
		{
			IFile tmpFile = getExportLogFile();
			if (tmpFile.exists())
			{
				tmpFile.setContents(new ByteArrayInputStream(message.getBytes()), true, false, null);
			}
			else
				tmpFile.create(new ByteArrayInputStream(message.getBytes()), true, null);
		}
		catch (Exception e)
		{
			TersusWorkbench.error("Internal Error", e);
		}
	}

	private IFile getExportLogFile()
	{
		IFile tmpFile = getProject().getFile("export-log.txt");
		return tmpFile;
	}

	public String[] getAntCommand(String target)
	{
		String commandName = Misc.isWindows() ? "ant.bat" : "ant";
		File file;
		try
		{
			boolean useEclipseAnt = false;
			if (useEclipseAnt)
			{
				Bundle b = Platform.getBundle("org.apache.ant");
				if (b == null)
					throw new ExportException("Ant not found");
				file = new File(FileLocator.getBundleFile(b), "bin/" + commandName);
			}
			else
			{
				Bundle b = Activator.getDefault().getBundle();
				file = new File(FileLocator.getBundleFile(b), "ant/bin/" + commandName);
			}
		}
		catch (Exception e)
		{
			throw new ExportException("Failed to locate ant command", e);
		}
		String antScriptPath = file.getAbsolutePath();
		String[] command = Misc.isWindows() ? new String[]
		{ antScriptPath, target } : new String[]
		{ "sh", "-f", antScriptPath, target };
		return command;
	}

	public void message(String title, String details)
	{
		MessageDialog.openInformation(TersusWorkbench.getActiveWorkbenchShell(), title, details);
	}

	public void error(String title, String details)
	{
		MessageDialog.openError(TersusWorkbench.getActiveWorkbenchShell(), title, details);
	}

	public IAndroidCommandHelper getAndroidCommandHelper()
	{
		return new AndroidCommandHelper();
	}

	public File getTemplateFolder()
	{
		return AppserverPlugin.getAppTempalteFolder();
	}


	public File getServerWebDirectory()
	{
		return AppserverPlugin.getDefault().getWebAppFolder();
	}

	public String getKeyStorePath()
	{
		return Preferences.getString(Preferences.ANDROID_RELEASE_KEY_STORE);

	}

	public String getKeyAlias()
	{
		return Preferences.getString(Preferences.ANDROID_RELEASE_KEY_ALIAS);
	}

	public void saveKeyStorePath(String keyStorePath)
	{
		Preferences.setValue(Preferences.ANDROID_RELEASE_KEY_STORE, keyStorePath);
	}

	public void savePackagePrefix(String prefix)
	{
		Preferences.setValue(Preferences.DEFAULT_PACKAGE_PREFIX, prefix);
		
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2021 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
package tersus.appcreator;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.ArrayUtils;
import org.eclipse.swt.widgets.DirectoryDialog;

import tersus.util.Misc;
import tersus.util.ProcessRunner;
import tersus.webapp.appexport.AndroidSDKHelper;
import tersus.webapp.appexport.ExportException;
import tersus.webapp.appexport.IAndroidCommandHelper;
import tersus.workbench.TersusWorkbench;

public class AndroidCommandHelper implements IAndroidCommandHelper
{
	public String getAndroidCommand(String commandName)
	{
		String sdkHomePath = getAndroidSDKHome();
		File sdkHome = new File(sdkHomePath);
		if (Misc.isWindows())
		{
			if (commandName == AndroidSDKHelper.COMMAND_ANDROID)
				commandName=commandName+".bat";
			else
				commandName = commandName+".exe";
		}

		File file = new File(sdkHome, "tools/" + commandName);
		if (!file.exists())
			file = new File(sdkHome, "platform-tools/" + commandName);
			
		String androidCommand = file.getAbsolutePath();
		return androidCommand;
	}
	
	private static String getAndroidSDKHome()
	{
		String sdkHomePath = Preferences.getString(Preferences.ANDROID_SDK_HOME);
		if (!validateAndroidSDKPath(sdkHomePath))
		{
			sdkHomePath = promptForAndroidSDKHomePath();
			if (!validateAndroidSDKPath(sdkHomePath))
				throw new ExportException("Android SDK Missing");
			Preferences.setValue(Preferences.ANDROID_SDK_HOME, sdkHomePath);
		}
		return sdkHomePath;
	}
	
	private static boolean validateAndroidSDKPath(String sdkHomePath)
	{
		if (sdkHomePath == null)
			return false;
		File sdkHome = new File(sdkHomePath);
		if (!sdkHome.isDirectory())
			return false;
		File android = new File(sdkHome, "tools/"+(Misc.isWindows()? "android.bat":"android"));
		return (android.isFile());
	}
	
	private static String promptForAndroidSDKHomePath()
	{
		DirectoryDialog d = new DirectoryDialog(TersusWorkbench.getActiveWorkbenchShell());
		d.setText("Select Android SDK Folder");
		String path = d.open();
		return path;
	}
}

package tersus.appcreator;

import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;



public class PreferencePage extends FieldEditorPreferencePage  implements IWorkbenchPreferencePage
{
	public PreferencePage()
	{
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription("Settings Tersus Application Export Wizard");
	}

	

	@Override
	protected void createFieldEditors()
	{
		addField( new DirectoryFieldEditor(Preferences.ANDROID_SDK_HOME, "Android SDK Home folder", getFieldEditorParent()));
		addField( new StringFieldEditor(Preferences.DEFAULT_PACKAGE_PREFIX, "Default pacakge prefix for generated Java classes", getFieldEditorParent()));
		addField( new FileFieldEditor(Preferences.ANDROID_RELEASE_KEY_STORE, "Keystore file for signing release packages", getFieldEditorParent()));
		addField( new StringFieldEditor(Preferences.ANDROID_RELEASE_KEY_ALIAS, "Alias of key for signing release pacakges", getFieldEditorParent()));
	}



	public void init(IWorkbench workbench)
	{
	}

}

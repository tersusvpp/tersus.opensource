/************************************************************************************************
 * Copyright (c) 2003-2021 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
package tersus.appcreator;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Text;

import tersus.webapp.appexport.AndroidSDKHelper;
import tersus.webapp.appexport.ExportException;
import tersus.webapp.appexport.ExportUtils;
import tersus.workbench.TersusExportWizardPageBase;
import tersus.workbench.TersusWorkbench;

public class AppCreatorWizardPage extends TersusExportWizardPageBase implements IWizardPage
{
	private AppCreatorWizard wizard;
	
	protected Combo targetPlatformCombo;
	
	private Text classNameText;
	
	private Text packageNameText;
	
	private Combo deviceCombo;

	protected AppCreatorWizardPage(AppCreatorWizard creationWizard)
	{
		super(AppCreatorWizard.WIZARD_NAME);
		setTitle(AppCreatorWizard.WIZARD_NAME);
		setDescription("Export Tersus Project as a Mobile Application");
		
		this.wizard = creationWizard;
	}

	@Override
	protected void addComponents(final Composite container)
	{
		initLayout(container);
		initProjectCombo(container);
		targetPlatformCombo = addCombo(container, "&Target Platform");
		addLocation(container);
		classNameText = addText(container, "Class Name");
		packageNameText = addText(container, "Package Name");
		deviceCombo = addCombo(container, "Install on device:");
	}

	public void createControl(Composite parent)
	{
		super.createControl(parent);
		initProjectComboBox();
		initTargetPlatformComboBox();
		initControlAfterTargetPlatform();
	}

	private void initControlAfterTargetPlatform() {
		initLocationText();
		initClassNameText();
		initPackageNameText();
		initDeviceCombo();
		setPageComplete(isPageComplete());
	}

	protected void initProjectComboBox()
	{
		for (IProject project : TersusWorkbench.getWorkspace().getRoot().getProjects())
		{
			if (project.isOpen() && TersusWorkbench.isTersusProject(project))
			{
				projectCombo.add(project.getName());
				if (project == wizard.getProject())
					projectCombo.setText(project.getName());
			}
		}
	
		if (projectCombo.getItems().length == 1)
		{
			String projectName = projectCombo.getItem(0);
			projectCombo.setText(projectName);
			setSelectedProject(projectName);
		}
		projectCombo.addModifyListener(new ModifyListener()
		{
	
			public void modifyText(ModifyEvent e)
			{
				String projectName = projectCombo.getText();
				setSelectedProject(projectName);
			}
	
		});
	}

	@Override
	protected void promptForLocation() {
		DirectoryDialog dialog = new DirectoryDialog(getShell());
		dialog.setMessage("Select export location");
		dialog.setText(locationText.getText());
		locationText.setText(dialog.open());
	}

	private void initDeviceCombo()
	{
		deviceCombo.removeAll();
		deviceCombo.setEnabled(false);
		deviceCombo.setText("");

		if (AppCreatorWizard.ANDROID.equals(wizard.getTargetPlatform()))
			initAndroidDeviceCombo();
	}

	private void initAndroidDeviceCombo()
	{
		try {
			String[] devices = AndroidSDKHelper.getDeviceSelectionList(wizard.getAndroidCommandHelper());
			if (devices.length > 0)
			{
				deviceCombo.setEnabled(true);
				String deviceName = wizard.getDeviceName();
				for (String device : devices)
				{
					deviceCombo.add(device);
					if (device.equals(deviceName))
						deviceCombo.setText(deviceName);
				}
				if (deviceCombo.getText().isEmpty())
					deviceCombo.setText(devices[0]);
				wizard.setDeviceName(deviceCombo.getText());
				setPageComplete(isPageComplete());
			}
		} catch (ExportException e) {
			// TODO Auto-generated catch block
			MessageDialog
			.openError(TersusWorkbench.getActiveWorkbenchShell(), "Error", e.getMessage());
			throw(e);
		}

		deviceCombo.addModifyListener(new ModifyListener(){

			public void modifyText(ModifyEvent e)
			{
				wizard.setDeviceName(deviceCombo.getText());
				setPageComplete(isPageComplete());			
			}
		});

	}

	protected void initClassNameText()
	{
		IProject project = wizard.getProject();
		if (project != null)
		{
			String className = wizard.getClassName();
			if (className == null)
				className = ExportUtils.convertName(project.getName());
			classNameText.setText(className);
			wizard.setClassName(className);
			setPageComplete(isPageComplete());
		}
		classNameText.addModifyListener(new ModifyListener()
		{

			public void modifyText(ModifyEvent e)
			{
				wizard.setClassName(classNameText.getText());
				setPageComplete(isPageComplete());
			}

		});
	}

	protected void initPackageNameText()
	{
		if (wizard.getClassName() != null)
		{
			String packagePrefix = Preferences.getString(Preferences.DEFAULT_PACKAGE_PREFIX);
			if (packagePrefix == null || packagePrefix.length() == 0)
				packagePrefix = "com.mycompany";
			String packageName = wizard.getPackageName();
			if (packageName == null)
				packageName = packagePrefix + "." + wizard.getClassName().toLowerCase();
			packageNameText.setText(packageName);
			wizard.setPackageName(packageName);
			setPageComplete(isPageComplete());
		}
		packageNameText.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				if (packageNameText.isEnabled()) // When disabled the shown text is not relevant
				{
					wizard.setPackageName(packageNameText.getText());
					setPageComplete(isPageComplete());
				}
			}
		});
		updatePackageNameEnabled();

	}

	protected void initLocationText()
	{
		String exportLocation;
		
		if (wizard.getProject() != null)
		{
			exportLocation = wizard.getExportLocation();
			if (wizard.validateLocation(exportLocation) != null)
				exportLocation = "";
		}
		else
			exportLocation = "";

		locationText.setText(exportLocation);
		wizard.setExportLocation(exportLocation);
		setPageComplete(isPageComplete());

		locationText.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				wizard.setExportLocation(locationText.getText());
				setPageComplete(isPageComplete());
			}
		});
	}

	protected void initTargetPlatformComboBox()
	{
		targetPlatformCombo.add(AppCreatorWizard.ANDROID);
		targetPlatformCombo.add(AppCreatorWizard.IPHONE);
//		targetPlatformCombo.add(AppCreatorWizard.WIN8);
//		targetPlatformCombo.add(AppCreatorWizard.CORDOVA);
		
		targetPlatformCombo.setText(wizard.getTargetPlatform());

		targetPlatformCombo.addModifyListener(new ModifyListener()
		{

			public void modifyText(ModifyEvent e)
			{
				String platform = targetPlatformCombo.getText();
				wizard.setTargetPlatform(platform);
				initControlAfterTargetPlatform();
			}

		});
	}

	private void updatePackageNameEnabled()
	{
		boolean packageRequired = AppCreatorWizard.ANDROID.equals(wizard.getTargetPlatform());
		
		packageNameText.setEnabled(packageRequired);
		if (!packageRequired)
			packageNameText.setText("(not required)");
		else
			packageNameText.setText(wizard.getPackageName());
		// getLabel(packageNameText).setEnabled(packageNameVisible);
	}

	private void setSelectedProject(String projectName)
	{
		wizard.setProject(TersusWorkbench.getWorkspace().getRoot().getProject(projectName));
		initControlAfterTargetPlatform();
		setPageComplete(isPageComplete());
	}



	public boolean isPageComplete()
	{
		setErrorMessage(wizard.validate());
		return getErrorMessage() == null;
	}
}

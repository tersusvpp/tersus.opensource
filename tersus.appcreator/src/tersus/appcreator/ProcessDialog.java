package tersus.appcreator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.custom.VerifyKeyListener;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import tersus.webapp.appexport.IProcessDialog;

public class ProcessDialog extends Dialog implements IProcessDialog
{

	StyledText textArea;
	Process process;
	Thread outputRepeater;
	private Thread waitThread;
	protected boolean closeOnSuccess = true;

	private String title;

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public boolean getCloseOnSuccess()
	{
		return closeOnSuccess;
	}

	public void setCloseOnSuccess(boolean closeOnSuccess)
	{
		this.closeOnSuccess = closeOnSuccess;
	}

	@Override
	public void create()
	{
		super.create();
		isOpen = true;
		getButton(IDialogConstants.OK_ID).setEnabled(false);
		hookStreams();
		waitForCompletion();
	}

	private void waitForCompletion()
	{
		waitThread = new Thread("Wait for Process")
		{
			public void run()
			{
				try
				{
					final int status = process.waitFor();
					getShell().getDisplay().asyncExec(new Runnable()
					{
						public void run()
						{
							if (status == 0)
							{
								if (closeOnSuccess)
									okPressed();
								else
								{
									getButton(IDialogConstants.OK_ID).setEnabled(true);
									getButton(IDialogConstants.CANCEL_ID).setEnabled(false);
								}
							}
							else
							{
								appendText("\nError: status=" + status + "\n");
							}
						}
					});
				}
				catch (InterruptedException e)
				{
				}
				waitThread = null;

			}

		};
		waitThread.start();

	}

	public boolean isOpen = false;
	private OutputStream stdin;

	private void hookStreams()
	{
		final BufferedReader stdout = new BufferedReader(new InputStreamReader(
				process.getInputStream()));
		final BufferedReader stderr = new BufferedReader(new InputStreamReader(
				process.getErrorStream()));

		stdin = process.getOutputStream();
		outputRepeater = new Thread()
		{
			public void run()
			{
				StringBuffer buffer = new StringBuffer();
				while (isOpen)
				{

					buffer.setLength(0);
					try
					{
						while (stdout.ready())
						{
							int c = stdout.read();
							if (c != -1)
								buffer.append((char) c);
						}
						while (stderr.ready())
						{
							int c = stderr.read();
							if (c != -1)
								buffer.append((char) c);
						}
					}
					catch (IOException e)
					{
						e.printStackTrace();
					}
					final String text = buffer.toString();
					appendText(text);
					try
					{
						sleep(100);
					}
					catch (InterruptedException e)
					{

					}
				}
			}

		};
		outputRepeater.setName("Process output reader");
		outputRepeater.start();

	}

	public Process getProcess()
	{
		return process;
	}

	public void setProcess(Process process)
	{
		this.process = process;
	}

	protected void configureShell(Shell shell)
	{
		super.configureShell(shell);
		shell.setText(getTitle());
	}

	protected ProcessDialog(Shell parentShell)
	{
		super(parentShell);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean isResizable()
	{
		return true;
	}

	@Override
	protected IDialogSettings getDialogBoundsSettings()
	{
		return Activator.getDefault().getDialogSettings();
	}

	@Override
	protected Control createDialogArea(Composite parent)
	{
		// TODO Auto-generated method stub
		Composite composite = (Composite) super.createDialogArea(parent);
		textArea = new StyledText(getParentShell(), SWT.BORDER | SWT.MULTI | SWT.V_SCROLL
				| SWT.H_SCROLL);
		GridData gd = new GridData(GridData.FILL_BOTH | GridData.GRAB_HORIZONTAL
				| GridData.GRAB_VERTICAL);
		gd.widthHint = 500;
		gd.heightHint = 300;
		textArea.setLayoutData(gd);
		textArea.addKeyListener(new KeyAdapter()
		{

			@Override
			public void keyPressed(KeyEvent e)
			{
				if (e.character != 0)
				{
					try
					{
						char c = e.character;
						if (c == '\r')
							c = '\n';
						stdin.write(c);
						if (c == '\n')
							stdin.flush();
						if (c == '\b')
						{
							textArea.replaceTextRange(textArea.getText().length()-1,1,"");
						}
						else
							textArea.append(String.valueOf(c));
						moveToEnd();
					}
					catch (IOException ex)
					{
						ex.printStackTrace();
					}
				}
				System.out.println(0 + e.character);
			}

		});
		VerifyKeyListener listener = new VerifyKeyListener()
		{

			public void verifyKey(VerifyEvent event)
			{
				event.doit = false;

			}
		};
		textArea.addVerifyKeyListener(listener);

		textArea.setParent(composite);

		return composite;
	}

	@Override
	protected void okPressed()
	{
		dispose();
		super.okPressed();
	}

	private void dispose()
	{
		isOpen = false; // Stops repeater thread
		if (waitThread != null)
			waitThread.interrupt();
		try
		{
			process.destroy();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	protected void cancelPressed()
	{
		dispose();
		super.cancelPressed();
	}

	private void moveToEnd()
	{
		textArea.setCaretOffset(textArea.getText().length());
		textArea.setTopIndex(textArea.getLineCount());
	}

	private void appendText(final String text)
	{
		getShell().getDisplay().asyncExec(new Runnable()
		{
			public void run()
			{
				textArea.append(text);
				moveToEnd();
			}
		});
	}

}

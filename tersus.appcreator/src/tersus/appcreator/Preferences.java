package tersus.appcreator;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;


public class Preferences extends AbstractPreferenceInitializer
{

	public static final String ANDROID_SDK_HOME = "ANDROID_SDK_HOME";
	public static final String DEFAULT_PACKAGE_PREFIX = "DEFAULT_PACKAGE_PREFIX";
	public static final String ANDROID_RELEASE_KEY_STORE = "ANDOROID_RELEASE_KEY_STORE";
	public static final String ANDROID_RELEASE_KEY_ALIAS = "ANDROID_RELEASE_KEY_ALIAS";

	
	public static final String ANDROID_RELEASE_KEY_STORE_DEFAULT = ".tersus/android.keystore";
	public static final String ANDROID_RELEASE_KEY_ALIAS_DEFAULT = "default";
	
	@Override
	public void initializeDefaultPreferences()
	{
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		store.setDefault(ANDROID_RELEASE_KEY_STORE, "");
		store.setDefault(ANDROID_RELEASE_KEY_ALIAS, ANDROID_RELEASE_KEY_ALIAS_DEFAULT);
	}

	public static String getString(String key)
	{
		String value = Activator.getDefault().getPreferenceStore().getString(key);
		return value;
	}
	
	public static void setValue(String key, String value)
	{
		 Activator.getDefault().getPreferenceStore().setValue(key, value);
	}
}

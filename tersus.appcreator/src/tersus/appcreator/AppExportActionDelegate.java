/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
package tersus.appcreator;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;

import tersus.workbench.TersusWorkbench;

public class AppExportActionDelegate implements IEditorActionDelegate {

	private IProject project;

	public void setActiveEditor(IAction action, IEditorPart targetEditor) {
		if (targetEditor instanceof IAdaptable)
			project = (IProject)((IAdaptable)targetEditor).getAdapter(IProject.class);
		else
			project = null;

	}

	public void run(IAction action) {
		AppCreatorWizard wizard = new AppCreatorWizard();
		wizard.setProject(project);
		WizardDialog dialog = new WizardDialog(TersusWorkbench.getActiveWorkbenchShell(), wizard);
		dialog.open();
	}

	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub

	}
}

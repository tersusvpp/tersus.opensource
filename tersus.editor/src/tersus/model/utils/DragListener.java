package tersus.model.utils;


import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;

import tersus.editor.outline.ModelObjectTransfer;

public abstract class DragListener implements DragSourceListener
{
	public void dragFinished(DragSourceEvent event) 
	{
		ModelObjectTransfer.getInstance().setObject(null);
	}

	public void dragSetData(DragSourceEvent event) 
	{
		event.data = ModelObjectTransfer.getInstance().getObject();
	}

	public abstract void dragStart(DragSourceEvent event);
}

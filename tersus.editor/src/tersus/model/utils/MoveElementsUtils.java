/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import tersus.model.Link;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.model.ModelUtils;

public class MoveElementsUtils
{
    public static HashSet<ModelObject> getAllLinkedTargets(List<ModelObject> sourceList)
    {
        HashSet<ModelObject> referncedBy = new HashSet<ModelObject>();
        ArrayList<Link> links = new ArrayList<Link>();

        ModelObject source = null;
        ModelObject target = null;

        ModelElement first = (ModelElement) sourceList.get(0);

        for (ModelElement element : first.getParentModel().getElements())
        {
            if (element instanceof Link)
            {
                source = (ModelObject) ((Link) element).getSourceElement();
                target = (ModelObject) ((Link) element).getTargetElement();

                if (sourceList.contains(source) && !sourceList.contains(target))
                {
                    referncedBy.add(target);
                }
                else
                {
                    links.add((Link) element);
                }
            }
        }

        boolean isNewElementAdded = !referncedBy.isEmpty();

        while (isNewElementAdded)
        {
            isNewElementAdded = false;

            for (int index = 0; index < links.size(); index++)
            {
                source = (ModelObject) (links.get(index).getSourceElement());
                target = (ModelObject) (links.get(index).getTargetElement());

                if (referncedBy.contains(source) && !sourceList.contains(target))
                {
                    if (!isNewElementAdded)
                    {
                        isNewElementAdded = true;
                    }
                    links.remove(links.get(index));
                    referncedBy.add(target);
                }
            }
        }
        return referncedBy;
    }

    public static HashSet<ModelObject> getAllLinkedSources(List<ModelObject> targetList)
    {
        HashSet<ModelObject> referncedTo = new HashSet<ModelObject>();
        ArrayList<Link> links = new ArrayList<Link>();

        ModelObject source = null;
        ModelObject target = null;

        ModelElement first = (ModelElement) targetList.get(0);

        for (ModelElement element : first.getParentModel().getElements())
        {
            if (element instanceof Link)
            {
                source = (ModelObject) ((Link) element).getSourceElement();
                target = (ModelObject) ((Link) element).getTargetElement();

                if (!targetList.contains(source) && targetList.contains(target))
                {
                    referncedTo.add(source);
                }
                else
                {
                    links.add((Link) element);
                }
            }
        }

        boolean isNewElementAdded = !referncedTo.isEmpty();

        while (isNewElementAdded)
        {
            isNewElementAdded = false;
            for (int index = 0; index < links.size(); index++)
            {
                source = (ModelObject) (links.get(index).getSourceElement());
                target = (ModelObject) (links.get(index).getTargetElement());

                if (!targetList.contains(source) && referncedTo.contains(target))
                {
                    if (!isNewElementAdded)
                    {
                        isNewElementAdded = true;
                    }

                    links.remove(links.get(index));
                    referncedTo.add(source);
                }
            }
        }
        return referncedTo;
    }

    public static boolean validateDataElementEncapsulation(List<ModelObject> sel)
    {
        ModelElement element = null;

        for (ModelObject e : sel)
        {
            if (!ModelUtils.isStrictDataElement(e) || ModelUtils.isConstant(e))
            {
                return false;
            }

            if ((element == null) && (e instanceof ModelElement))
            {
                element = (ModelElement) e;
            }
        }

        boolean isInnerlinkedTo = false;

        for (ModelObject e : element.getParentModel().getElements())
        {
            if (e instanceof Link)
            {
                isInnerlinkedTo = checkLinkEnds(sel, sel, (Link) e);

                if (isInnerlinkedTo)
                {
                    return false;
                }
            }
        }

        return isCompleteSelectedPath(sel);
    }

    public static boolean validateDisplayElementEncapsulation(List<ModelObject> sel)
    {
        boolean displayElementExists = false;
        ModelElement firstDispay = null;

        for (ModelObject e : sel)
        {
            if (!ModelUtils.isAncestorReferanceModel(e))
            {
                if (!ModelUtils.isStrictDataElement(e) && !ModelUtils.isDisplayElement(e) 
                        && !ModelUtils.isDisplayData(e))
                {
                    return false;
                }

                if (ModelUtils.isDisplayElement(e) && !displayElementExists && ! (e instanceof ModelElement && ModelUtils.isDialog(((ModelElement)e).getReferredModel())))
                {
                    displayElementExists = true;
                    firstDispay = (ModelElement) e;
                }
            }
        }

        if (firstDispay == null || !displayElementExists)
        {
            return false;
        }

        List<ModelElement> elements = new ArrayList<ModelElement>();
        List<ModelObject> unSelectedObjects = new ArrayList<ModelObject>();
        elements.addAll(firstDispay.getParentModel().getElements());

        for (int i = 0; i < elements.size(); i++)
        {
            if (!sel.contains(elements.get(i)) && !(elements.get(i) instanceof Link))
            {
                unSelectedObjects.add((ModelObject) elements.get(i));
            }
        }

        for (int j = 0; j < elements.size(); j++)
        {
            if (elements.get(j) instanceof Link)
            {
                Link l = (Link) (elements.get(j));
                if (checkLinkEnds(unSelectedObjects, sel, l) || checkLinkEnds(sel, unSelectedObjects, l))
                {
                    return false;
                }
            }
        }

        return displayElementExists;
    }

    public static boolean validateProcessEncapsulation(List<ModelObject> sel)
    {
        for (ModelObject modelObject : sel)
        {
            if (!ModelUtils.isSubProcess(modelObject) && !ModelUtils.isStrictDataElement(modelObject)
                    && !ModelUtils.isDisplayElement(modelObject) && !ModelUtils.isDialog(modelObject)
                    && !ModelUtils.isAncestorReferanceModel(modelObject) && !ModelUtils.isDisplayData(modelObject))
            {
                return false;
            }
        }

        return isCompleteSelectedPath(sel);
    }

    public static boolean validateAncestorRefernceEncapsulation(List<ModelObject> sel)
    {
        ModelElement firstDispay = null;

        for (ModelObject e : sel)
        {
            if (!ModelUtils.isAncestorReferanceModel(e))
            {
                return false;
            }
            else if (firstDispay == null && ModelUtils.isDisplayElement(e))
            {
                firstDispay = (ModelElement) e;
            }
        }

        if (firstDispay == null)
        {
            return false;
        }

        List<ModelElement> elements = new ArrayList<ModelElement>();
        List<ModelObject> unSelectedObjects = new ArrayList<ModelObject>();
        elements.addAll(firstDispay.getParentModel().getElements());

        for (int i = 0; i < elements.size(); i++)
        {
            if (!sel.contains(elements.get(i)) && !(elements.get(i) instanceof Link))
            {
                unSelectedObjects.add((ModelObject) elements.get(i));
            }
        }

        for (int j = 0; j < elements.size(); j++)
        {
            if (elements.get(j) instanceof Link)
            {
                Link l = (Link) (elements.get(j));
                if (checkLinkEnds(unSelectedObjects, sel, l) || checkLinkEnds(sel, unSelectedObjects, l))
                {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Checks if the input link starts at sourceSet and ends in targetSet
     * 
     * @param t - set of source elements
     * @param targetSet - set of source elements
     * @param link - input link
     * @return true if the input link starts at sourceSet and ends in targetSet.
     */
    public static boolean checkLinkEnds(List<ModelObject> sourceSet, List<ModelObject> targetSet, Link link)
    {
        ModelObject source = (ModelObject) (link).getSourceElement();
        ModelObject target = (ModelObject) (link).getTargetElement();

        return sourceSet.contains(source) && targetSet.contains(target);
    }

    /**
     * 
     * @param sel
     * @return
     */
    private static boolean isCompleteSelectedPath(List<ModelObject> sel)
    {

        HashSet<ModelObject> referncedTo = getAllLinkedSources(sel);
        HashSet<ModelObject> referncedBy = getAllLinkedTargets(sel);

        return Collections.disjoint(referncedBy, referncedTo);
    }

    public static boolean validateProcessDencapsulation(List<ModelObject> sel)
    {
        boolean completePath = isCompleteSelectedPath(sel);
        boolean validParent = false;

        if (completePath)
            validParent = true;
        else
        {
            ModelObject first = sel.get(0);
            Model parentModel = null;

            // Check parent (assumption: all elements have the same parent)
            if (first instanceof ModelElement)
            {
                ModelElement element = (ModelElement) first;
                parentModel = element.getParentModel();
            }
            else
                return false;

            if (parentModel != null && ModelUtils.isSubProcess(parentModel))
                validParent = true;
        }

        for (ModelObject modelObject : sel)
        {
            if (!ModelUtils.isSubProcess(modelObject) && !ModelUtils.isStrictDataElement(modelObject)
                    && !ModelUtils.isDisplayElement(modelObject) && !ModelUtils.isDialog(modelObject)
                    && !ModelUtils.isAncestorReferanceModel(modelObject) && !ModelUtils.isDisplayData(modelObject))
            {
                return false;
            }
        }
        
        return true;
    }
}

/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model.commands;

import tersus.model.BuiltinProperties;
import tersus.model.ModelId;
import tersus.model.ModelObject;
import tersus.model.Slot;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public class SetSlotTypeCommand extends MultiStepCommand
{
	private ElementRef slotRef = null;

	private ModelId newType;

	private boolean dataTypeSetting;

	public SetSlotTypeCommand(ModelObject modelObject, ModelId propertyValue,
			boolean dataTypeSetting)
	{
		super((WorkspaceRepository) modelObject.getRepository());
		if (modelObject instanceof Slot)
		{
			slotRef = new ElementRef((Slot) modelObject);
			this.newType = propertyValue;
			this.dataTypeSetting = dataTypeSetting;

		}
	}

	private void initCommands()
	{
		Slot slot = (Slot) slotRef.getModelObject();

		setProperty(slot, BuiltinProperties.DATA_TYPE_ID, this.newType);

		if (this.dataTypeSetting)
			setProperty(slot, BuiltinProperties.DATA_TYPE_SETTING, "explicit");
		else
			setProperty(slot, BuiltinProperties.DATA_TYPE_SETTING, "inferred");

		propagateSlotType(slot);
	}

	@Override
	protected void run()
	{
		initCommands();
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model.commands;

import java.util.ArrayList;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.gef.commands.Command;

import tersus.editor.Errors;
import tersus.editor.actions.ValidatingCommand;
import tersus.editor.layout.LayoutConstraint;
import tersus.model.BuiltinPlugins;
import tersus.model.BuiltinProperties;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.ModelUtils;
import tersus.model.PackageId;
import tersus.model.SubFlow;
import tersus.util.Misc;
import tersus.workbench.WorkspaceRepository;

/**
 * An action that creates an element from a template.
 * 
 * This action creates a new model by duplicating the template, and then creates a new element (in a
 * given 'parent' models) that refers to the newly created model.
 * 
 * @author Youval Bronicki
 * 
 */
public class CreateElementFromTemplateCommand extends MultiStepCommand implements
		ValidatingCommand, IElementNameCommand
{
	private int index = -1;

	private ModelRef templateRef;

	private ModelRef parentRef;

	private LayoutConstraint constraint;

	private ArrayList<Command> propagateSlotCommands;

	private boolean doPropagateSlotType = true;
	
	private boolean createSubPackage = false;

	private ModelElement createdElement;

	private String suggestedName;
	
	private String constantValue;

	public CreateElementFromTemplateCommand(WorkspaceRepository workspaceRepository)
	{
		super(workspaceRepository);
	}

	public void setTemplateModel(Model templatedModel)
	{
		this.templateRef = new ModelRef(templatedModel);
	}

	public void setLayoutConstraint(LayoutConstraint constraint)
	{
		this.constraint = constraint;
	}

	public void setParent(Model parentModel)
	{
		parentRef = new ModelRef(parentModel);
	}

	public void run()
	{
		Model parentModel = (Model) parentRef.getModelObject();
		Model templateModel = (Model) templateRef.getModelObject();

		PackageId packageId = parentModel.getId().getPackageId();

		setCreateSubPackage("true".equals(templateModel.getProperty(BuiltinProperties.CREATE_SUB_PACKAGE)));
		if (getCreateSubPackage())
		{
			packageId = repository.getNewSubPackageId(packageId, getSuggestedElementName());
			addNewPackage(packageId);
		}

		ModelId newModelId = repository.getNewModelId(packageId, getSuggestedElementName());
		createModelFromTemplateOrPrototype(templateModel.getId(), newModelId, false);

		createdElement = AddElementHelper.getNewElement(parentModel, newModelId,
				getSuggestedElementName(), constraint, null);

		addElement(parentModel, createdElement, null, null, index, true);

		Model newModel = repository.getModel(newModelId, true);
		setProperty(newModel, BuiltinProperties.CREATE_SUB_PACKAGE, null);

		if (ModelUtils.isConstant(createdElement))
		{
			if (getConstantValue() != null && getConstantValue().length() == 0)
				setProperty(newModelId, BuiltinProperties.VALUE, "\"\"");
			else
				setProperty(newModelId, BuiltinProperties.VALUE, getConstantValue());
		}

		if (createdElement instanceof SubFlow && doPropagateSlotType)
		{
			propagateSlotCommands = new ArrayList<Command>();

			for (ModelElement element : createdElement.getReferredModel().getElements())
			{
				// TODO-Propagate - check if this is required (probably not because
				// AddElementCommand already handles propagation)
				propagateSlotType(element);
			}
		}
	}

	private boolean isBlankConstant()
	{
		return ((Model) templateRef.getModelObject()).getPlugin().equals(
				BuiltinPlugins.TEXT_CONSTANT)
				&& getSuggestedElementName() != null
				&& getSuggestedElementName().trim().length() == 0;
	}

	public void setIndex(int index)
	{
		this.index = index;

	}

	public String validate()
	{
		int invalidCharIndex;
		
		if (getSuggestedElementName() == null || getSuggestedElementName().length() == 0)
		{
			return Errors.USE_NON_EMPTY_NAME;
		}
		else if (getSuggestedElementName().indexOf('/') >= 0)
			return Errors.invalidCharacterInElementName('/');
		else if (getCreateSubPackage() && ((invalidCharIndex=validatePackageName())> -1))
			return Errors.invalidCharacterInPackageName(Misc.INVALID_FILENAME_CHARACTERS.charAt(invalidCharIndex));
		else if (!getSuggestedElementName().equals(getSuggestedElementName().trim())
				&& !isBlankConstant())
		{
			return Errors.LEADING_OR_TRAINLING_BLANKS;
		}
		else
			return null;
	}

	private int validatePackageName()
	{
		return (StringUtils.indexOfAny(getSuggestedElementName(), Misc.INVALID_FILENAME_CHARACTERS));
	}

	public void setElementName(String name)
	{
		setSuggestedName(name);
	}

	public String getActualElementName()
	{
		return createdElement != null ? createdElement.getElementName() : null;
	}

	public void setDoPropagateSlotType(boolean value)
	{
		doPropagateSlotType = value;
	}

	public ModelElement getCreatedElement()
	{
		return createdElement;
	}

	public void setSuggestedName(String name)
	{
		if (((Model) templateRef.getModelObject()).getPlugin().equals(BuiltinPlugins.TEXT_CONSTANT))
		{
			if (name != null && name.trim().length() == 0)
			{
				setConstantValue(name);
				switch (name.length())
				{
					case 0:
						suggestedName = "Empty";
						return;
					case 1:
						suggestedName = "Blank";
						return;
					default:
						suggestedName = "Blanks";
						return;
				}
			}
		}
		suggestedName = name;
	}

	private void setConstantValue(String val)
	{
		constantValue = val;
	}
	
	private String getConstantValue()
	{
		return constantValue;
	}
	
	public String getSuggestedElementName()
	{
		return suggestedName;
	}

	public boolean getCreateSubPackage()
	{
		return createSubPackage;
	}

	public void setCreateSubPackage(boolean createSubPackage)
	{
		this.createSubPackage = createSubPackage;
	}
}
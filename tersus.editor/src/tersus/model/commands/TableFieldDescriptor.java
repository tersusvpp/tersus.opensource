/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/

package tersus.model.commands;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import tersus.model.BuiltinModels;
import tersus.model.Filtering;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.util.IObservableItem;
import tersus.workbench.WorkspaceRepository;

/**
 * description of a field in a database record and in a corresponding table.
 * 
 * @author Ofer Brandes
 * 
 */
public class TableFieldDescriptor implements IObservableItem
{
	private static ModelId[] genericDataTypes = new ModelId[]
	{ BuiltinModels.TEXT_ID, BuiltinModels.NUMBER_ID, BuiltinModels.DATE_ID,
			BuiltinModels.DATE_AND_TIME_ID, BuiltinModels.BOOLEAN_ID };

	private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
	public static final int NOT_PK = 0;
	public static final int PK = 1; // Primary key whose value is explicitly provided (by the
	// application or by the user)
	public static final int MODEL_GENERATED_PK = 2; // For 'Number' or 'Date and Time' only, and
	// only if the sole primary key field
	public static final int DATABASE_GENERATED_PK = 3; // For 'Number' only, and only if the sole primary
	// key field

	private String initialName; // Null indicates a new field
	private String name; // Null indicates deletion of the field
	private String columnName;
	private ModelId dataType;
	private ModelId initialDataType;
	private int primaryKey;
	private Boolean mandatory;
	private String validValues; // Examples:
	// Foreign Key: Table=Categories, Column=Name
	// Value List: "Bakery", "Dairy" [Text]
	// Value List: -1, 0, 1 [Number]
	// Range: Range=[1-5] [Number, Date, Date and Time]
	// Regular Expression: Pattern=\d{1,8} [Text]
	private String initialValidValues;
	private int filtering; // Exact, Starts with, Contains, Range (or empty) - see
							// tersus.model.Filtering
	private boolean showRow;
	private boolean initiallyShown;
	private boolean isElementPK;

	public TableFieldDescriptor(String initialName, String name, String columnName, ModelId dataType,
			int primaryKey, boolean mandatory, String validValues, int filtering,
			boolean showRow, boolean initiallyShown, boolean isElementPK) // Existing field (with initial name)
	{
		this.initialName = initialName;
		this.name = name;
		this.columnName = columnName;
		this.dataType = dataType;
		this.primaryKey = primaryKey; // Initially, if a primary key, it implies read only in update
		this.mandatory = mandatory;
		this.validValues = validValues;
		this.filtering = filtering;
		this.showRow = showRow;
		this.initiallyShown = initiallyShown;
		this.isElementPK = isElementPK;
	}

	public void saveInitialState()
	{
		this.initialDataType = this.dataType;
		this.initialValidValues = this.validValues;
	}

	public TableFieldDescriptor(String name, ModelId dataType, int primaryKey, boolean mandatory,
			String validValues, int filtering, boolean showRow, boolean initiallyShown, boolean isElementPK) // New field
																						// (no
																						// initial
																						// name)
	{
		this(null, name, null, dataType, primaryKey, !(NOT_PK == primaryKey), validValues,
				filtering, showRow, initiallyShown, isElementPK);
	}

	public TableFieldDescriptor(String name, ModelId dataType, int primaryKey, boolean showRow,
			boolean initiallyShown)
	{
		this(name, dataType, primaryKey, !(NOT_PK == primaryKey), null, 0, showRow, initiallyShown, false);
	}

	public TableFieldDescriptor(String name, ModelId dataType)
	{
		this(name, dataType, NOT_PK, true, false);
	}

	public void markForDeletion()
	{
		setName(null);
	}

	public String getInitialName()
	{
		return initialName;
	}

	public void setName(String newValue)
	{
		String oldValue = getName();
		this.name = newValue;
		propertyChangeSupport.firePropertyChange("Name", oldValue, newValue);
	}

	public void setPrimaryKey(int newValue)
	{
		int oldValue = primaryKey;
		if (primaryKey != newValue)
		{
			this.primaryKey = newValue;
			propertyChangeSupport.firePropertyChange("Primary Key", oldValue, newValue);
		}
	}

	public int getPrimaryKey()
	{
		return primaryKey;
	}

	public void addListener(PropertyChangeListener listener)
	{
		propertyChangeSupport.addPropertyChangeListener(listener);
	}

	public void removeListener(PropertyChangeListener listener)
	{
		propertyChangeSupport.removePropertyChangeListener(listener);
	}

	public String getName()
	{
		return name;
	}

	public String getColumnName()
	{
		// TODO Auto-generated method stub
		return columnName;
	}

	public void setDataType(ModelId dataType)
	{
		ModelId oldValue = this.dataType;
		this.dataType = dataType;
		if (oldValue != dataType)
		{
			propertyChangeSupport.firePropertyChange("Data Type", oldValue, dataType);
		}
	}

	public ModelId getGenericDataType(WorkspaceRepository repository)
	{
		// If the data type is derived from a generic data type,
		// get the corresponding generic data type
		// (else returning the data type itself)

		if (dataType != null)
		{
			for (int i = 0; i < genericDataTypes.length; i++)
				if (hasSamePluginAs(repository, genericDataTypes[i]))
					return genericDataTypes[i];
		}
		return dataType;
	}

	protected ModelId getGenericConstantType(WorkspaceRepository repository)
	{
		if (hasSamePluginAs(repository, BuiltinModels.TEXT_ID))
			return (BuiltinModels.TEXT_CONSTANT_ID);
		else if (hasSamePluginAs(repository, BuiltinModels.NUMBER_ID))
			return (BuiltinModels.NUMBER_CONSTANT_ID);
		else if (hasSamePluginAs(repository, BuiltinModels.DATE_ID))
			return (BuiltinModels.DATE_CONSTANT_ID);
		else if (hasSamePluginAs(repository, BuiltinModels.DATE_AND_TIME_ID))
			return (BuiltinModels.DATE_AND_TIME_CONSTANT_ID);
		else
			return null;
	}

	protected boolean hasSamePluginAs(WorkspaceRepository repository, ModelId otherModelId)
	{
		Model model = (dataType == null ? null : repository.getModel(dataType, true));
		String plugin = (model == null ? null : model.getPlugin());

		Model otherModel = (otherModelId == null ? null : repository.getModel(otherModelId, true));
		String otherPlugin = (otherModel == null ? null : otherModel.getPlugin());

		return (plugin != null && plugin.equals(otherPlugin));
	}

	public ModelId getDataType()
	{
		return dataType;
	}

	protected ModelId getInitialDataType()
	{
		return initialDataType;
	}

	protected boolean hasDataTypeChanged()
	{
		return initialDataType != null && !initialDataType.equals(dataType);
	}

	public boolean isPrimaryKey()
	{
		return NOT_PK != primaryKey;
	}

	public boolean isAutoPrimaryKey()
	{
		return isAutoIncrementPrimaryKey() || isModelGeneratedPrimaryKey();
	}

	public boolean isAutoIncrementPrimaryKey()
	{
		return DATABASE_GENERATED_PK == primaryKey;
	}

	public boolean isModelGeneratedPrimaryKey()
	{
		return MODEL_GENERATED_PK == primaryKey;
	}

	public boolean isMandatoty()
	{
		return mandatory;
	}

	public void setValidValues(String newValue)
	{
		String oldValue = getValidValues();
		this.validValues = newValue;
		propertyChangeSupport.firePropertyChange("Valid Values", oldValue, newValue);
	}

	public String getInitialValidValues()
	{
		return initialValidValues;
	}

	public String getValidValues()
	{
		return validValues;
	}

	public void setFiltering(int newValue)
	{
		String oldValue = Filtering.getFilteringName(getFiltering());
		this.filtering = newValue;
		propertyChangeSupport.firePropertyChange("Filtering", oldValue, newValue);
	}

	public int getFiltering()
	{
		return filtering;
	}

	public static ModelId[] getGenericDataTypes()
	{
		return genericDataTypes;
	}

	public void setShowRow(boolean showRow)
	{
		Boolean oldValue = Boolean.valueOf(this.showRow);
		this.showRow = showRow;
		propertyChangeSupport.firePropertyChange("Show Row", oldValue, Boolean.valueOf(showRow));
	}

	public boolean getShowRow()
	{
		return showRow;
	}

	public boolean isInitiallyShown()
	{
		return initiallyShown;
	}
	
	public boolean isElementFieldPK()
	{
		return isElementPK;
	}
}

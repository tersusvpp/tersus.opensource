/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model.commands;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.RelativePosition;
import tersus.model.Role;
import tersus.model.Slot;
import tersus.model.SlotType;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 * @author Liat Shiff
 * 
 */
public class AddSlotCommand extends AddElementAndPropagateSlotTypeCommand
{
	public AddSlotCommand(Model parentModel)
	{
		super((WorkspaceRepository) parentModel.getRepository());
		setLabel("Add Slot");
		setParentModel(parentModel);

		setNewElement(new Slot());
	}

	public void run()
	{
		if (getSlotRole() == null)
			setSlotRole(getParent().defaultRole(getSlot().getType().toString()));

		super.run();
	}

	public SlotType getSlotType()
	{
		return getSlot().getType();
	}

	public void setSlotType(SlotType type)
	{
		getSlot().setType(type);

		if (type == SlotType.TRIGGER || type == SlotType.IO)
			getSlot().setMandatory(true);
		else
			getSlot().setMandatory(false);

		if (SlotType.ERROR == type)
		{
			// YB - 2008-11-29: This may not be the most elegant place to handle type of error
			// exits, but it was very easy to add it here.
			getSlot().setDataTypeId(BuiltinModels.ERROR_ID);
			getSlot().setDataTypeSetting(Slot.DATA_TYPE_SETTING_EXPLICIT);
		}
		else
			getSlot().setDataTypeSetting(Slot.DATA_TYPE_SETTING_INFERRED);
	}

	public void setSlotRole(Role role)
	{
		getSlot().setRole(role);
	}

	public Role getSlotRole()
	{
		return getSlot().getRole();
	}

	public RelativePosition getPosition()
	{
		return getSlot().getPosition();
	}

	public void setPosition(RelativePosition position)
	{
		getSlot().setPosition(position);;
	}

	private Slot getSlot()
	{
		return (Slot) getNewElement();
	}

	public String toString()
	{
		return ("AddSlotCommand[parent=" + getParent() + ";type=" + getSlot().getType()
				+ ";position" + getSlot().getPosition() + "]");
	}
}

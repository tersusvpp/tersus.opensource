/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
package tersus.model.commands;

import java.util.ArrayList;
import java.util.List;

import tersus.editor.actions.TersusActionConstants;
import tersus.model.BuiltinProperties;
import tersus.model.Link;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.RelativePosition;
import tersus.model.RelativeSize;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Ofer Brandes
 * 
 */

public class AlignElementsCommand extends MultiStepCommand
{
	private List<ModelElement> alignedElements;
	private String actionName;

	public AlignElementsCommand(Model parentModel, List<ModelElement> selectedElements, String actionName)
	{
		super((WorkspaceRepository) parentModel.getRepository(), actionName);
		this.actionName = actionName;

		this.alignedElements = new ArrayList<ModelElement>();
		for (ModelElement e : parentModel.getElements())
		{
			if (selectedElements.contains(e))
				this.alignedElements.add(e);
		}
	}

	public void run()
	{
		if (!this.alignedElements.isEmpty())
		{
			boolean alignX = TersusActionConstants.VERTICAL_ALIGN_ACTION_TEXT.equals(actionName);
			boolean alignY = TersusActionConstants.HORIZONTAL_ALIGN_ACTION_TEXT.equals(actionName);		
			boolean alignSize = TersusActionConstants.SIZE_ALIGN_ACTION_TEXT.equals(actionName);		

			int n = 0;
			double x = 0;
			double y = 0;
			double width = 0;
			double height = 0;
			for (ModelElement e : alignedElements)
			{
				if (!(e instanceof Link))
				{
					if (alignX || alignY)
					{
						RelativePosition pos = (RelativePosition) e.getProperty(BuiltinProperties.POSITION);
						x += pos.getX();
						y += pos.getY();
					}
					if (alignSize)
					{
						RelativeSize size = (RelativeSize) e.getProperty(BuiltinProperties.SIZE);
						width += size.getWidth();
						height+= size.getHeight();
					}
					n++;
				}
			}

			x /= n;
			y /= n;
			width /= n;
			height /= n;

			for (ModelElement e : alignedElements)
			{
				if (!(e instanceof Link))
				{
					if (alignX || alignY)
					{
						RelativePosition pos = (RelativePosition) e.getProperty(BuiltinProperties.POSITION);
						RelativePosition newPos = new RelativePosition(alignX ? x : pos.getX(),alignY ? y : pos.getY());
						setProperty (e, BuiltinProperties.POSITION, newPos);
					}
					if (alignSize)
					{
						RelativeSize size = (RelativeSize) e.getProperty(BuiltinProperties.SIZE);
						RelativeSize newSize = new RelativeSize(width,height);
						setProperty (e, BuiltinProperties.SIZE, newSize);
					}
				}
			}
		}
	}

}

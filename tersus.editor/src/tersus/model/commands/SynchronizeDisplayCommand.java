/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model.commands;

import java.util.List;

import org.eclipse.ui.texteditor.DeleteLineAction;

import tersus.model.BuiltinModels;
import tersus.model.Composition;
import tersus.model.DataElement;
import tersus.model.DisplayModelWrapper;
import tersus.model.FlowModel;
import tersus.model.Link;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.Path;
import tersus.model.Role;
import tersus.model.SlotType;
import tersus.model.SubFlow;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public abstract class SynchronizeDisplayCommand extends MultiStepCommand
{
	// The following are only required when creating transformation processes
	public static final String EXTRACT = "Extract";
	public static final String EXTRACT_PREFIX = (EXTRACT + " ");
	public static final String POPULATE = "Populate";
	public static final String POPULATE_PREFIX = (POPULATE + " ");
	public static final ModelId EXTRACT_TEMPLATE_ID = new ModelId("Common/Templates/Basic/Action/Action"); // FUNC1 Use a special plug-in
	public static final ModelId POPULATE_TEMPLATE_ID = new ModelId("Common/Templates/Basic/Action/Action"); // FUNC1 Use a special plug-in
	public static final Role DISPLAY_ROLE = Role.get("Display");
	public static final Role DATA_ROLE = Role.get("Data");
	protected static final Role INPUT_STRUCTURE_ROLE = Role.get("Input");
	protected static final Role OUTPUT_STRUCTURE_ROLE = Role.get("Output");

	public SynchronizeDisplayCommand(WorkspaceRepository repository)
	{
		super(repository);
	}

	// TODO Get rid of 'displayModel' and 'dataModel' (they can be found given 'displayElementRole' and 'dataElementRole'
	protected void syncCompositeExtract(Model displayModel, Role triggerRole,
			Role displayElementRole, String interimLevelSuffix, Model dataModel, List<Role> ignoreInDataModel, Role exitRole,
			Role dataElementRole, FlowModel extractModel, boolean deleteBrokenLinks, boolean deleteRedundantLinks,
			boolean wizardCreatedDisplayModel)
	{
		// Create transformation skeleton (unless already exists)
		syncSlots(extractModel, displayModel, triggerRole, dataModel, exitRole);

		// Delete Broken links (leftovers from deleted fields)
		if (deleteBrokenLinks)
			deleteBrokenLinks(extractModel);

		ModelElement in = extractModel.getElement(displayElementRole);
		DataElement out = (DataElement) extractModel.getElement(dataElementRole);

		// List initial links between the input display model and the output data model (as candidates for deletion)
		List<Link> linksToDelete = null;
		if (deleteRedundantLinks)
			linksToDelete = extractModel.getLinksBetween(new Path(displayElementRole),new Path(dataElementRole));

		// Ensure there is an extract flow to each data element
		List<?> dataElements = dataModel.getElements(ignoreInDataModel);
		for (int i = 0; i < dataElements.size(); i++)
		{
			// Assuming same role for corresponding display and data elements
			DataElement dataElement = (DataElement) dataElements.get(i);

			Path displayElementRelativePath = new Path();
			if (interimLevelSuffix != null)
			{
				Role interimLevelRole = Role.get(dataElement.getElementName() + interimLevelSuffix);
				displayElementRelativePath.addSegment(interimLevelRole);
			}
			displayElementRelativePath.addSegment(dataElement.getRole());
			ModelElement displayElement = (ModelElement) displayModel.getElement(displayElementRelativePath);
			if (wizardCreatedDisplayModel &&
					(displayElement == null || !CreateTableCommand.isTemplateIdForField(displayElement.getReferredModel())))  // Field with this role not found or is not the field created by the wizard
				{
					List<Path> descendants = displayModel.findSubFlow(dataElement.getRole(),4);
					for (int j = 0; j < descendants.size(); j++)
					{
						ModelElement e = displayModel.getElement((Path) descendants.get(j));
						if (e instanceof SubFlow)
						{
							displayElement = (SubFlow) e;
							if (CreateTableCommand.isTemplateIdForField(displayElement.getReferredModel()))
							{
								displayElementRelativePath = new Path();
								if (interimLevelSuffix != null)
								{
									Role interimLevelRole = Role.get(dataElement.getElementName() + interimLevelSuffix);
									displayElementRelativePath.addSegment(interimLevelRole);
								}
								displayElementRelativePath.append((Path) descendants.get(j));
								break;
							}
						}
					}
				}

			boolean isLeaf = (dataElement.getModel().getComposition() == Composition.ATOMIC);

			Path extractSourcePath = new Path(in.getRole());
			extractSourcePath.append(displayElementRelativePath);
			if (isLeaf) // Assuming the data element is a leaf iff the corresponding display element is a leaf
				extractSourcePath.addSegment(DisplayModelWrapper.correspondingLeafRole((FlowModel) displayElement.getReferredModel()));

			Path extractTargetPath = new Path(out.getRole());
			extractTargetPath.addSegment(dataElement.getRole());

			Link link = extractModel.getLinkTo(extractTargetPath);
			if (link != null)
				if (!link.getSource().isEquivalent(extractSourcePath))
					; // Shouldn't happen - notify a bug

			// Leaf - direct flow from the source to the target
			if (isLeaf)
			{
				if (link == null)
					addLink(extractModel, extractSourcePath, extractTargetPath);
				else
				{
					if (linksToDelete != null)
						linksToDelete.remove(link);
				}
			}

			// Composite - a sub-extract process is required
			// (will probably work only if 'interimLevelSuffix' is null)
			else
			{
				FlowModel subExtractModel = null;
				if (link != null)
					subExtractModel = (FlowModel) link.getSourceElement().getReferredModel();
				else
				{
					// Create sub-extract model
					ModelId subExtractId = new ModelId(extractModel.getPackageId(), EXTRACT_PREFIX
							+ displayElement.getRole());
					subExtractModel = (FlowModel) createModelFromTemplateOrPrototype(EXTRACT_TEMPLATE_ID, subExtractId, false);
					double y = (dataElements.size() == 1 ? 0.5 : 0.2 + i
							/ (dataElements.size() - 1.) * 0.6);
					ModelElement subExtractElement = addSubModel(extractModel, subExtractModel,
							subExtractModel.getId().getName(), 0.5, y);

					Path subExtractTriggerPath = new Path(subExtractElement.getRole());
					subExtractTriggerPath.addSegment(DISPLAY_ROLE);

					Path subExtractExitPath = new Path(subExtractElement.getRole());
					subExtractExitPath.addSegment(DATA_ROLE);

					// Add flow from the source to the trigger of the sub-extract element
					addLink(extractModel, extractSourcePath, subExtractTriggerPath);

					// // Add flow from exit of the sub-extract element to the target
					addLink(extractModel, subExtractExitPath, extractTargetPath);
				}

				syncCompositeExtract(displayElement.getReferredModel(), DISPLAY_ROLE,
						INPUT_STRUCTURE_ROLE, interimLevelSuffix, dataElement.getModel(), null,
						DATA_ROLE, OUTPUT_STRUCTURE_ROLE, subExtractModel, deleteBrokenLinks, deleteRedundantLinks,
						wizardCreatedDisplayModel);
			}
		}

		// Delete redundant links
		if (linksToDelete != null)
			for (Link link: linksToDelete)
				deleteElement(extractModel, link.getElementName(), false, false, true);
	}

	// TODO Get rid of 'dataModel' and 'displayModel' (they can be found given 'dataElementRole' and 'displayElementPath' 
	protected void syncCompositePopulate(Model dataModel, List<Role> ignoreInDataModel, Role triggerRole, Role dataElementRole,
			Model displayModel, Role exitRole, Path displayElementPath, String interimLevelSuffix,
			FlowModel populateModel, boolean deleteBrokenLinks, boolean primaryKeyIsReadOnly,
			boolean wizardCreatedDisplayModel)
	{
		// Create transformation skeleton (unless already exists)
		syncSlots(populateModel, dataModel, triggerRole, displayModel, exitRole);

		// Delete Broken links (leftovers from deleted fields)
		if (deleteBrokenLinks)
			deleteBrokenLinks(populateModel);

		DataElement in = (DataElement) populateModel.getElement(dataElementRole);

		// Ensure there is a populate flow from each data element
		String yesConstantName = BuiltinModels.YES_CONSTANT_ID.getName(); // Compare with BooleanHandler.YES_LITTERAL
		Role yesConstantRole = Role.get(yesConstantName);
		ModelElement yesConstant = populateModel.getElement(yesConstantRole);
		Path yesConstantPath = new Path(yesConstantRole);

		List<ModelElement> dataElements = dataModel.getElements(ignoreInDataModel);
		for (int i = 0; i < dataElements.size(); i++)
		{
			// Assuming same role for corresponding display and data elements
			DataElement dataElement = (DataElement) dataElements.get(i);

			Path displayElementRelativePath = new Path();
			if (interimLevelSuffix != null)
			{
				Role interimLevelRole = Role.get(dataElement.getElementName() + interimLevelSuffix);
				displayElementRelativePath.addSegment(interimLevelRole);
			}
			displayElementRelativePath.addSegment(dataElement.getRole());
			ModelElement displayElement = (ModelElement) displayModel.getElement(displayElementRelativePath);
			if (wizardCreatedDisplayModel &&
				(displayElement == null || !CreateTableCommand.isTemplateIdForField(displayElement.getReferredModel())))  // Field with this role not found or is not the field created by the wizard
			{
				List<Path> descendants = displayModel.findSubFlow(dataElement.getRole(),4);
				for (int j = 0; j < descendants.size(); j++)
				{
					ModelElement e = displayModel.getElement((Path) descendants.get(j));
					if (e instanceof SubFlow)
					{
						displayElement = (SubFlow) e;
						if (CreateTableCommand.isTemplateIdForField(displayElement.getReferredModel()))
						{
							displayElementRelativePath = new Path();
							if (interimLevelSuffix != null)
							{
								Role interimLevelRole = Role.get(dataElement.getElementName() + interimLevelSuffix);
								displayElementRelativePath.addSegment(interimLevelRole);
							}
							displayElementRelativePath.append((Path) descendants.get(j));
							break;
						}
					}
				}
			}

			boolean isLeaf = (dataElement.getModel().getComposition() == Composition.ATOMIC);

			Path populateSourcePath = new Path(in.getRole());
			populateSourcePath.addSegment(dataElement.getRole());

			Path populateTargetPath = displayElementPath.getCopy();
			populateTargetPath.append(displayElementRelativePath);
			if (isLeaf) // Assuming the data element is a leaf iff the corresponding display element is a leaf
				populateTargetPath.addSegment(DisplayModelWrapper.correspondingLeafRole((FlowModel) displayElement.getReferredModel()));

			Link link = populateModel.getLinkFrom(populateSourcePath,displayElementPath,populateTargetPath.getLastSegment());
			if (link != null)
				if (!link.getTarget().isEquivalent(populateTargetPath))
					; // Shouldn't happen - notify a bug

			// Leaf - direct flow from the source to the target
			if (isLeaf)
			{
				if (link == null)
					addLink(populateModel, populateSourcePath, populateTargetPath);

				// And prevent updating primary key fields
				Path readOnlyPath = populateTargetPath.getCopy();
				readOnlyPath.removeLastSegment();
				readOnlyPath.addSegment(DisplayModelWrapper.READ_ONLY_RESERVED_ROLE);

				Link markLink = (yesConstant == null ? null : populateModel.getLinkBetween(yesConstantPath,readOnlyPath));
				
				if (primaryKeyIsReadOnly && dataElement.isPrimaryKey()) // Mark as read only
				{
					if (markLink == null)
					{
						if (yesConstant == null)
						{
							Model yesConstantModel = repository.getModel(BuiltinModels.YES_CONSTANT_ID, true);
							addSubModel(populateModel, yesConstantModel, yesConstantName, 0.5, 0.4, 0.14, 0.08);
						}
						addLink(populateModel, yesConstantPath, readOnlyPath);
					}
				}
				else // Remove previous marking as read only
				{
					if (markLink != null)
						deleteElement(populateModel, markLink.getElementName(), false, false, true);
				}
			}

			// Composite - a sub-populate process is required
			// (will probably work only if 'interimLevelSuffix' is null)
			else
			{
				FlowModel subPopulateModel = null;
				if (link != null)
					subPopulateModel = (FlowModel) link.getTargetElement().getReferredModel();
				else
				{
					// Create sub-populate model
					ModelId subPopulateId = new ModelId(populateModel.getPackageId(),
							POPULATE_PREFIX + displayElement.getRole());
					subPopulateModel = (FlowModel) createModelFromTemplateOrPrototype(POPULATE_TEMPLATE_ID, subPopulateId, false);
					double y = (dataElements.size() == 1 ? 0.5 : 0.2 + i
							/ (dataElements.size() - 1.) * 0.6);
					ModelElement subPopulatElement = addSubModel(populateModel, subPopulateModel,
							subPopulateModel.getId().getName(), 0.5, y);

					Path subPopulateTriggerPath = new Path(subPopulatElement.getRole());
					subPopulateTriggerPath.addSegment(DATA_ROLE);

					Path subPopulateExitPath = new Path(subPopulatElement.getRole());
					subPopulateExitPath.addSegment(DISPLAY_ROLE);

					// Add flow from the source to the trigger of the sub-populate element
					addLink(populateModel, populateSourcePath, subPopulateTriggerPath);

					// Add flow from exit of the sub-populate element to the target
					addLink(populateModel, subPopulateExitPath, populateTargetPath);
				}

				syncCompositePopulate(dataElement.getModel(), null, DATA_ROLE, INPUT_STRUCTURE_ROLE,
						displayElement.getReferredModel(), DISPLAY_ROLE, new Path(OUTPUT_STRUCTURE_ROLE),
						interimLevelSuffix, subPopulateModel, deleteBrokenLinks, primaryKeyIsReadOnly,
						wizardCreatedDisplayModel);
			}
		}
	}

	protected void syncComposositeTransformation (FlowModel transformationModel,
			                                      Role input1Role, Role input2Role, List<Role> ignoreInInput1,
			                                      Role outputRole)
	{
		// No need to add slots (we assume the transformation skeleton already exists)

		// No need to create new fields (we assume all data models are already in their final state)

		// Delete existing links between the input and output data structures
		for (Link link: transformationModel.getLinksBetween(new Path(input1Role),new Path(outputRole)))
			deleteElement(transformationModel, link.getElementName(), false, false, true);

		if (input2Role != null)
			for (Link link: transformationModel.getLinksBetween(new Path(input2Role),new Path(outputRole)))
				deleteElement(transformationModel, link.getElementName(), false, false, true);

		// Ensure there is a populate flow from each pair of corresponding data elements in the input and output data structures
		// (source is in 'in1', or, if 'input2Role', possibly in 'in2')
		DataElement in1 = (DataElement) transformationModel.getElement(input1Role);
		Model inputDataModel1 = in1.getReferredModel();

		Model inputDataModel2 = null;
		if (input2Role != null)
		{
			DataElement in2 = (DataElement) transformationModel.getElement(input2Role);
			inputDataModel2 = in2.getReferredModel();
		}

		DataElement out = (DataElement) transformationModel.getElement(outputRole);
		Model outputDataModel = out.getReferredModel();

		for (ModelElement outputDataElement : outputDataModel.getElements())
			if (outputDataElement instanceof DataElement)
			{
				Role role = outputDataElement.getRole(); // Assuming same role for all corresponding data elements
				Path target = new Path(outputRole+"/"+role);
				Path source = null;

				DataElement inputDataElement = null;
				if (ignoreInInput1 == null || !ignoreInInput1.contains(role))
				{
					// Input is usually in 'in1'...
					inputDataElement = (DataElement) inputDataModel1.getElement(outputDataElement.getRole());
					source = new Path(input1Role+"/"+role);
				}
				else if (input2Role != null)
				{
					// ... unless listed in 'ignoreInInput1' and 'in2' exists
					inputDataElement = (DataElement) inputDataModel2.getElement(outputDataElement.getRole());
					source = new Path(input2Role+"/"+role);
				}

				if (inputDataElement != null)
					addLink (transformationModel, source, target);
			}
	}

	private void syncSlots(FlowModel transformationModel, Model triggerModel, Role triggerRole,
			Model exitModel, Role exitRole)
	{
		if (transformationModel.getElement(triggerRole) == null)
		{
			// Create trigger
			addSlot(transformationModel, SlotType.TRIGGER, triggerRole, triggerModel.getId(),
					false, true, null, true);

			// Create input data element
			addSubModel(transformationModel, triggerModel, INPUT_STRUCTURE_ROLE.toString(), 0.2,
					0.5);

			// Create a flow from trigger to input data element
			addLink(transformationModel, new Path(triggerRole), new Path(INPUT_STRUCTURE_ROLE));
		}

		if (transformationModel.getElement(exitRole) == null)
		{
			// Create output data element
			addSubModel(transformationModel, exitModel, OUTPUT_STRUCTURE_ROLE.toString(), 0.8, 0.5);

			// Create exit
			addSlot(transformationModel, SlotType.EXIT, exitRole, exitModel.getId(), false, true,
					null, true);

			// Create a flow from output data element to exit
			addLink(transformationModel, new Path(OUTPUT_STRUCTURE_ROLE), new Path(exitRole));
		}
	}

	private void deleteBrokenLinks(Model transformationModel)
	{
		// Deleting from a process model links with no source and no target (but not in sub-models)
		List<ModelElement> elements = transformationModel.getElements();
		for (int e = elements.size() - 1; e >= 0; e--)
			if (elements.get(e) instanceof Link)
			{
				Link link = (Link) elements.get(e);
				ModelElement source = transformationModel.getElement(link.getSource());
				ModelElement target = transformationModel.getElement(link.getTarget());
				if ((source == null) && (target == null))
					deleteElement(transformationModel, link.getElementName(), false, false, true);
			}
	}
}

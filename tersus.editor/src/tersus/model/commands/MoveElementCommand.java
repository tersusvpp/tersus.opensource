/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model.commands;

import java.util.ArrayList;

import tersus.model.BuiltinProperties;
import tersus.model.Link;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.Path;
import tersus.model.Repository;
import tersus.model.Role;
import tersus.model.indexer.RepositoryIndex;
import tersus.workbench.WorkspaceRepository;

public class MoveElementCommand extends MultiStepCommand
{
	private ElementRef elementRef, newParentElementRef;

	private RepositoryIndex index;

	ArrayList<Link> incomingLinks;

	ArrayList<Link> outgoingLinks;

	public MoveElementCommand(ModelElement element, ModelElement newParentElement)
	{
		super((WorkspaceRepository) element.getRepository(), "Move Element");

		this.setElement(element);
		this.setNewParentElement(newParentElement);
	}

	@Override
	protected void run()
	{
		index = repository.getUpdatedRepositoryIndex();
		ModelElement elementToMove = getElement();

		incomingLinks = index.getIncomingLinks(elementToMove, false);
		outgoingLinks = index.getOutgoingLinks(elementToMove, false);

		ModelElement newElement = Repository.copyElement(getElement());
		addElement(getNewParentElement().getReferredModel(), newElement, null, null, null, false);
		rerouteLinks();
		removeElement(getElement(), false);
	}

	private void rerouteLinks()
	{
		for (Link link : incomingLinks)
		{
			Path targetPath = link.getTarget();
			Path newTarget = reroute(targetPath, link.getParentModel());
			
			setProperty(link, BuiltinProperties.TARGET, newTarget);
		}
		
		for (Link link : outgoingLinks)
		{
			Path sourcePath = link.getSource();
			Path newSource = reroute(sourcePath, link.getParentModel());
			
			setProperty(link, BuiltinProperties.SOURCE, newSource);
		}
	}

	private Path reroute(Path pathToChange, Model linkParent)
	{
		Path newPath = null;
		Model currentModel = linkParent;
		
		for (int i = 0; i < pathToChange.getNumberOfSegments(); i++)
		{
			Role s = pathToChange.getSegment(i);
			ModelElement e = currentModel.getElement(s);
			if (e == getElement() && newPath == null)
			{
				newPath = pathToChange.getPrefix(i);
				newPath.addSegment(getNewParentElement().getElementName());
				newPath.addSegment(s);
			}
			else if (newPath != null)
				newPath.addSegment(s);

			if (e == null)
				return null;
			currentModel = e.getReferredModel();
			if (currentModel == null)
				return null;
		}
		
		return newPath;
	}

	private void setElement(ModelElement element)
	{
		this.elementRef = new ElementRef(element);
	}

	private ModelElement getElement()
	{
		return (ModelElement) elementRef.getModelObject();
	}

	private void setNewParentElement(ModelElement newParentElement)
	{
		this.newParentElementRef = new ElementRef(newParentElement);
	}

	private ModelElement getNewParentElement()
	{
		return (ModelElement) newParentElementRef.getModelObject();
	}
}

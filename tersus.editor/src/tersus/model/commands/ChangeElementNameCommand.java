/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model.commands;

import java.util.ArrayList;

import org.eclipse.gef.commands.Command;

import tersus.model.BuiltinProperties;
import tersus.model.FlowModel;
import tersus.model.Link;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.Path;
import tersus.model.Role;
import tersus.model.Slot;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Ofer Brandes
 * @author Youval Bronicki
 * @author Liat Shiff
 */
public class ChangeElementNameCommand extends MultiStepCommand
{
	private ModelElement modifiedElement;

	private Role newName;

	private ArrayList<Command> changes;

	public ChangeElementNameCommand(ModelElement element, Role newName)
	{
		super((WorkspaceRepository) element.getRepository());
		this.modifiedElement = element;
		this.newName = newName;
	}

	private void createChangeCommand()
	{
		changes = new ArrayList<Command>();

		changes.add(new SetPropertyValueCommand(modifiedElement, BuiltinProperties.ROLE,
				this.newName));
		update(changes);
	}

	private void update(ArrayList<Command> changes)
	{
		WorkspaceRepository repository = (WorkspaceRepository) modifiedElement.getRepository();
		if (modifiedElement instanceof Slot)
		{
			scanElementLinks(changes, (FlowModel) modifiedElement.getParentModel());
			for (ModelElement reference : repository.getReferences(modifiedElement.getParentModel()
					.getId()))
			{
				if (reference.getParentModel() instanceof FlowModel)
					scanElementLinks(changes, (FlowModel) reference.getParentModel());
			}
		}
		else if (!(modifiedElement instanceof Link))
		{
			for (Link link : repository.getUpdatedRepositoryIndex().getReferringLinks(
					modifiedElement))
			{
				addUpdateLinkCommands(changes, link, (FlowModel) link.getParentModel());
			}
		}
	}

	private void scanElementLinks(ArrayList<Command> changes, FlowModel model)
	{
		for (ModelElement element : model.getElements())
		{
			// Change references from current model's links
			if (element instanceof Link)
				addUpdateLinkCommands(changes, (Link) element, model);
		}
	}

	private void addUpdateLinkCommands(ArrayList<Command> changes, Link link, Model model)
	{
		Path sourcePath = link.getSource();
		Path newPath = changeRoleInPath(model, sourcePath);
		if (newPath != null)
			changes.add(new SetPropertyValueCommand(link, BuiltinProperties.SOURCE, newPath));

		Path targetPath = link.getTarget();
		newPath = changeRoleInPath(model, targetPath);
		if (newPath != null)
			changes.add(new SetPropertyValueCommand(link, BuiltinProperties.TARGET, newPath));
	}

	/**
	 * Calculate how a path should be changed to reflect a change of one of the roles composing it
	 * (the changed role may occur more than once along the path).
	 * 
	 * @param model
	 *            A model under which a path should be changed.
	 * @param path
	 *            The path to be changed.
	 * @param modifiedElement
	 *            The element whose role is changed.
	 * @param newRole
	 *            The new role of the element.
	 * 
	 * @return A new path that contains 'path' with the required change(s) (or null if identical to
	 *         'path', which happens if the original role of modified element does not appear in
	 *         'path').
	 */
	private Path changeRoleInPath(Model model, Path path)
	{
		Path newPath = null; // Will remain null as long as no change is made
		int numberOfSegments = path.getNumberOfSegments();
		
		if (path != null)
			for (int i = 0; i < path.getNumberOfSegments(); i++)
			{
				Role role = path.getSegment(i);
				Role pathRole = role;

				if ((model == modifiedElement.getParentModel())
						&& (role == modifiedElement.getRole()))
				{
					if (newPath == null)
						newPath = path.getPrefix(i); // create 'newPath' only
					// when the first change is
					// made

					pathRole = newName;
				}

				if (newPath != null)
					newPath.addSegment(pathRole);

				ModelElement element = model.getElement(role);
				// Using 'role', not 'pathRole', as no change has been made yet
				// (just added to the compound command for future execution)

				if (element == null) // Might happen when the initial path was
					// invalid (e.g. unconnected link)
					return (null); // FUNC3 Any correction possible?

				if (i+1 != numberOfSegments)
				{
					model = element.getReferredModel();
					
					if (model == null)
						return null;
				}
			}

		return (newPath); // null if no change has been made
	}

	public void run()
	{
		createChangeCommand();
		for (Command cmd : changes)
		{
			runCommand(cmd);
		}
		
		propagateSlotType(modifiedElement);
	}
}
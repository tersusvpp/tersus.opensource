/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
package tersus.model.commands;

import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.ModelUtils;
import tersus.model.RelativePosition;
import tersus.model.Repository;
import tersus.model.Role;
import tersus.model.Slot;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 * 
 */
public class AddPrototypeElementCommand extends InstantiatePrototypeCommand
{
	ModelId targetModelId;
	ModelId prototypeModelId;
	Role elementName;

	public AddPrototypeElementCommand(Model targetModel, ModelElement prototypeElement)
	{
		super((WorkspaceRepository) targetModel.getRepository(), "Add Element ("
				+ prototypeElement.getRole() + ")");
		this.targetModelId = targetModel.getId();
		this.elementName = prototypeElement.getRole();
		this.prototypeModelId = prototypeElement.getParentModel().getId();
	}

	public void run()
	{
		init();
		Model targetModel = repository.getModel(targetModelId, true);
		Model prototype = repository.getModel(prototypeModelId, true);
		ModelElement prototypeElement = prototype.getElement(elementName);
		
		ModelElement newElement = null;
		try
		{
			newElement = addElementFromPrototype(targetModel, prototypeElement);
		}
		catch (PrototypeInstantiationException e)
		{
			undo();
			throw e;
		}
		finally
		{
			cleanup();
		}
		
		if (newElement instanceof Slot)
			propagateSlotType((Slot)newElement);
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model.commands;

import tersus.editor.Debug;
import tersus.editor.EditorMessages;
import tersus.model.BuiltinProperties;
import tersus.model.Link;
import tersus.model.ModelElement;
import tersus.model.Path;
import tersus.model.Slot;
import tersus.util.Trace;
import tersus.workbench.WorkspaceRepository;

// NICE2 This class is redundant - 'source' & 'target' should be set using SetPropertyValueCommand()

public class ReconnectLinkCommand extends MultiStepCommand
{
	protected ElementRef elementRef;
	protected Path source;
	protected Path target;
	
	private Slot oldSlotToPropagate;

	public ReconnectLinkCommand(WorkspaceRepository repository)
	{
		super(repository, EditorMessages.ConnectionCommand_Label);
	}

	public void run()
	{

		if (Debug.LINKS)
			Trace.push("Executing " + this);
			
		setOldSlotToPropagate();
		
		setProperty(getLink(), BuiltinProperties.SOURCE, source);
		setProperty(getLink(), BuiltinProperties.TARGET, target);

		ModelElement sourceElement = getLink().getParentModel().getElement(getLink().getSource());
		ModelElement targetElement = getLink().getParentModel().getElement(getLink().getTarget());

		if (canPropagateSlotType(sourceElement))
			propagateSlotType((Slot) sourceElement);
		else if (canPropagateSlotType(targetElement))
			propagateSlotType((Slot) targetElement);
		
		if (oldSlotToPropagate != null)
			propagateSlotType(oldSlotToPropagate);

		if (Debug.LINKS)
			Trace.pop();
	}

	private void setOldSlotToPropagate()
	{
		Path currentSourcePath =  getLink().getSource();
		Path currentTargetPath =  getLink().getTarget();
		
		if (!source.equals(currentSourcePath))
		{
			ModelElement currentSource = getLink().getParentModel().getElement(currentSourcePath);
			if (canPropagateSlotType(currentSource))
				oldSlotToPropagate = (Slot) currentSource;			
		}
		else if (!target.equals(currentTargetPath))
		{
			ModelElement currentTarget = getLink().getParentModel().getElement(currentTargetPath);
			if (canPropagateSlotType(currentTarget))
				oldSlotToPropagate = (Slot) currentTarget;
		}
	}
	
	private boolean canPropagateSlotType(ModelElement element)
	{
		return element instanceof Slot && Slot.DATA_TYPE_SETTING_INFERRED.equals(((Slot) element).getDataTypeSetting());
	}
	
	public Path getSource()
	{
		return source;
	}

	public Path getTarget()
	{
		return target;
	}

	public void setLink(Link link)
	{
		elementRef = new ElementRef(link);
	}

	public Link getLink()
	{
		return (Link) elementRef.getModelObject();
	}

	public void setSource(Path path)
	{
		source = path;
	}

	public void setTarget(Path path)
	{
		target = path;
	}

	public String toString()
	{
		return "LinkCommand parent=" + (Link) elementRef.getModelObject() + " source=" + source
				+ " target=" + target;
	}

}

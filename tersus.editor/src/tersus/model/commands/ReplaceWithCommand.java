/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model.commands;

import java.util.List;

import tersus.model.BuiltinProperties;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.indexer.ElementEntry;
import tersus.model.indexer.RepositoryIndex;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public class ReplaceWithCommand extends MultiStepCommand
{
	private ModelElement selectedElement;

	private ModelId replaceWithModel;

	private boolean replaceAll;

	private boolean deleteOldModel;
	
	private boolean syncNames;
	
	private Model oldModel;

	public ReplaceWithCommand(WorkspaceRepository repository, ModelElement selectedElement,
			ModelId replaceWithModel, boolean replaceAll, boolean deleteOldModel, boolean syncNames)
	{
		super(repository);
		this.selectedElement = selectedElement;
		this.replaceWithModel = replaceWithModel;
		this.replaceAll = replaceAll;
		this.deleteOldModel = deleteOldModel;
		this.oldModel = selectedElement.getReferredModel();
		this.syncNames = syncNames;
	}

	@Override
	protected void run()
	{
		if (replaceAll)
		{
			RepositoryIndex index = repository.getUpdatedRepositoryIndex();
			List<ElementEntry> elements = index.getReferenceEntries(selectedElement.getRefId());

			for (ElementEntry entry : elements)
			{
				ModelElement e = index.getModelElement(entry);
				
				if (e != null)
					setProperty(e, BuiltinProperties.REF_ID, replaceWithModel);
				
				if (syncNames)
					renameElement(e, replaceWithModel.getName());
			}
		}
		else
		{
			setProperty(selectedElement, BuiltinProperties.REF_ID, replaceWithModel);
			if (syncNames)
				renameElement(selectedElement, replaceWithModel.getName());
		}

		
		if (deleteOldModel && oldModel != null)
			deleteModel(oldModel);
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model.commands;

import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.gef.commands.Command;

import tersus.model.Package;
import tersus.model.PackageId;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 */
public class DeletePackageCommand extends Command
{
	private WorkspaceRepository repository;

	private PackageId deletedPackageId;

	private List<Package> deletedPackageStates;

	private String message;

	public DeletePackageCommand(WorkspaceRepository repository, PackageId packageId)
	{
		this.repository = repository;
		this.deletedPackageId = packageId;
		setLabel("Delete Package");
	}

	public boolean canExecute()
	{
		Package pkg = repository.getPackage(deletedPackageId);

		if (pkg == null || pkg.isReadOnly())
			return false;
		if (!ContainsReadOnlyPackage(pkg))
			return false;

		return repository.getExternalReferences(deletedPackageId).isEmpty();
	}

	public void execute()
	{
		Package pkg = repository.getPackage(deletedPackageId);

		if (pkg == null)
			throw new CommandFailedException("Missing package "+deletedPackageId);
		if (pkg.isReadOnly())
			throw new CommandFailedException("Cannot delete read only package '" + deletedPackageId
					+ "'.");

		if (!ContainsReadOnlyPackage(pkg))
			throw new CommandFailedException(message);

		if (!pkg.isEmpty() &&  !repository.getExternalReferences(deletedPackageId).isEmpty())
			throw new CommandFailedException("Pakckage "+deletedPackageId+ " has external references");

		deletedPackageStates = repository.removePackage(deletedPackageId);
	}

	public boolean ContainsReadOnlyPackage(Package pkg)
	{
		for (PackageId packageId : repository.getSubPackageIds(pkg.getPackageId()))
		{
			Package subPackage = repository.getPackage(packageId);
			if (subPackage.isReadOnly())
			{
				message = "Cannot delete read only subpackage " + packageId + ".";
				return false;
			}
			else
				ContainsReadOnlyPackage(subPackage);
		}

		for (IFile file : repository.getFiles(pkg.getPackageId()))
		{
			if (file.isReadOnly())
			{
				message = "Cannot delete read only file " + file.getFullPath() + ".";
				return false;
			}
		}

		return true;
	}

	public void undo()
	{
		repository.restorePackage(deletedPackageStates);
	}
}

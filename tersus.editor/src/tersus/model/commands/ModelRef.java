/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model.commands;

import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.ModelObject;

/**
 * @author Liat Shiff
 */
public class ModelRef extends ModelObjectRef
{
	private ModelId modelId;

	public ModelRef(Model model)
	{
		super(model.getRepository());
		this.modelId = model.getId();
	}

	@Override
	public ModelObject getModelObject()
	{
		return repository.getModel(modelId, true);
	}
}

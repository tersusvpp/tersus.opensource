/************************************************************************************************
 * Copyright (c) 2003-2022 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/

package tersus.model.commands;

import java.util.ArrayList;
import java.util.List;

import tersus.model.indexer.ElementEntry;
import tersus.model.indexer.RepositoryIndex;
import tersus.model.BuiltinElements;
import tersus.model.BuiltinModels;
import tersus.model.BuiltinPlugins;
import tersus.model.BuiltinProperties;
import tersus.model.Composition;
import tersus.model.DataElement;
import tersus.model.DataType;
import tersus.model.DisplayModelWrapper;
import tersus.model.Filtering;
import tersus.model.FlowDataElementType;
import tersus.model.FlowModel;
import tersus.model.Link;
import tersus.model.LinkOperation;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelException;
import tersus.model.ModelId;
import tersus.model.ModelObject;
import tersus.model.Package;
import tersus.model.PackageId;
import tersus.model.Path;
import tersus.model.RelativePosition;
import tersus.model.RelativeSize;
import tersus.model.Role;
import tersus.model.Slot;
import tersus.model.SlotType;
import tersus.model.SubFlow;
import tersus.model.ValidValues;
import tersus.util.Misc;
import tersus.workbench.WorkspaceRepository;

/**
 * 
 * Modifying a generic table management pane template according to a specific database record.
 * 
 * @author Ofer Brandes
 */

public class CreateTableCommand extends SynchronizeDisplayCommand
{
	private final Role RecordRole = Role.get("Database Record");
	private Role PkRole;

	private boolean isNewUI;
	private SubFlow topElement;
	private Model model;
	private String initialTableName;
	private String tableName;
	private String databaseTableName;
	private ModelElement recordElement;
	private List<TableFieldDescriptor> fields;
	private TableConfiguration tableConfiguration;

	public CreateTableCommand(WorkspaceRepository repository, ModelObject initialModel) // Investigating initial model to get fields descriptors
	{
		super(repository);
		if (initialModel instanceof Model)
			this.model = (Model) initialModel;
		else if (initialModel instanceof SubFlow)
		{
			this.topElement = (SubFlow) initialModel;
			this.model = topElement.getReferredModel();
		}
		PackageId topPackageId = model.getPackageId();
		boolean isCompactModel = model.checkPluginVersion(4); // Version 4 is Dudu's compact implementation of 24/8/10 
		isNewUI = model.checkPluginVersion(5); // Version 5 is Ofer's adaption to the new UI, 2/8/20)
		PkRole = Role.get(isNewUI ? "[Primary Key]" : "<Primary Key>");
		
		// Get display table name
		ModelElement tabelElement = model.getElementByPlugin(isNewUI ? "Tersus/UI/Table" : "Tersus/Display/Table");
		initialTableName = tabelElement.getElementName();

		// Get database table name
		ModelId convertModelId = repository.getModelId(topPackageId.toString()+"/Wizard/Transformations/Convert Record to Row");
		FlowModel convertModel = (FlowModel) repository.getModel(convertModelId, true);
		if (convertModel == null)
			throw new ModelException("Missing model: " + convertModelId.getPath());
		recordElement = convertModel.getElement(RecordRole);
		DataType recordModel = (DataType) recordElement.getReferredModel();
		databaseTableName = (String) recordModel.getProperty(BuiltinProperties.TABLE_NAME);
		Path tableRowPath = new Path(isNewUI ? "Body Row" : "Table Row");

		// Get fields
		fields = new ArrayList<TableFieldDescriptor>();
		List<ModelElement> recordElements = recordModel.getElements(); // In the future - generalize to handle hierarchical data structures
		for (int i = 0; i < recordElements.size(); i++)
		{
			DataElement dataElement = (DataElement) recordElements.get(i);
			boolean isLeaf = (dataElement.getModel().getComposition() == Composition.ATOMIC);
			String name = dataElement.getElementName();
			ModelId dataType = dataElement.getModelId();
			DataType dataModel = (DataType) repository.getModel(dataType, true);
			boolean mandatory = Boolean.TRUE.equals(dataElement.getProperty(BuiltinProperties.MANDATORY));
			String validValues = (String) dataModel.getProperty(BuiltinProperties.VALID_VALUES);
			String columnName = (String) dataElement.getProperty(BuiltinProperties.COLUMN_NAME);
			int primaryKey = TableFieldDescriptor.NOT_PK;
			if (dataElement.isPrimaryKey())
			{
				// Deciding which type of primary key - compare with autoManageKeyField()
				primaryKey = TableFieldDescriptor.PK;
				ModelId creationModelId = repository.getModelId(topPackageId.toString()+"/Table/Action Header Group/New/Save/Save New/Perform Action");
				FlowModel creationModel = (FlowModel) repository.getModel(creationModelId, true);
				if (creationModel == null)
					throw new ModelException("Missing model: " + creationModelId.getPath());
				Link link = creationModel.getLinkTo(new Path(RecordRole + "/" + name));
				if (link != null)
					primaryKey = (LinkOperation.REMOVE.equals(link.getOperation()) ? TableFieldDescriptor.DATABASE_GENERATED_PK : TableFieldDescriptor.MODEL_GENERATED_PK);
			}
			Link linkFromSource = convertModel.getLinkFrom(new Path(recordElement.getRole() + "/" + name));
			Link linkToTarget = (linkFromSource != null && linkFromSource.getTarget().beginsWith(tableRowPath) ? linkFromSource : convertModel.getLinkTo(new Path(tableRowPath + "/" + name),null)); // Can differ from the link from the source (if some process is used in the middle), but if there is a link from the source element to the row, we tolerate a modified target role
			boolean showRow = (linkFromSource != null) && (linkToTarget != null);
			int filtering = 0;
			if (showRow)
			{
				Path target = linkToTarget.getTarget();
				Path displayElementPath = (isLeaf ? target.getPrefix(target.getNumberOfSegments() - 1) : target);
				SubFlow displayElement = (SubFlow) convertModel.getElement(displayElementPath);
				filtering = Filtering.getFilteringIntValue((String) displayElement.getProperty(BuiltinProperties.FILTERING));
			}

			Object isElementPrimaryKey = dataElement.getProperty(BuiltinProperties.PRIMARY_KEY);

			TableFieldDescriptor field = new TableFieldDescriptor(name, name, columnName, dataType,
					primaryKey, mandatory, validValues, filtering, showRow,
					showRow, isElementPrimaryKey != null ? ((Boolean) isElementPrimaryKey).booleanValue()
							: false);
			
			field.saveInitialState();
			field.setDataType(field.getGenericDataType(repository)); // Non-generic data types are displayed to the user as the corresponding generic data types
			fields.add(field);
		}

		// Get table configuration
		FlowModel initModel = (FlowModel) model.getElement(Role.get("Init")).getReferredModel();
		FlowModel initConfigurationModel = (FlowModel) initModel.getElement(Role.get("Init Configuration")).getReferredModel();

		DataType defaultOptimization = (DataType) initConfigurationModel.getElement(Role.get("Default Optimization")).getReferredModel();
		boolean defaultOptimizationValue = "Yes".equals(defaultOptimization.getValue()); // "Yes" = tersus.runtime.BooleanHandler.YES_LITTERAL

		DataType numberOfPagesToDisplay = (DataType) initConfigurationModel.getElement(Role.get("Number of Pages to Display")).getReferredModel();
		int numberOfPagesToDisplayValue = Integer.parseInt(numberOfPagesToDisplay.getValue()); // Compare with tersus.runtime.NumberHandler.newInstance

		DataType numberOfRecordsToDisplay = (DataType) initConfigurationModel.getElement(Role.get(isCompactModel ? "Number of Records to Display" : "Number of Records per Page")).getReferredModel();
		int numberOfRecordsToDisplayValue = Integer.parseInt(numberOfRecordsToDisplay.getValue()); // Compare with tersus.runtime.NumberHandler.newInstance

		DataType showSearchGroup = (DataType) initConfigurationModel.getElement(Role.get("Show Paging Header Group")).getReferredModel();
		boolean showSearchGroupValue = "Yes".equals(showSearchGroup.getValue()); // "Yes" = tersus.runtime.BooleanHandler.YES_LITTERAL

		DataType showActionGroup = (DataType) initConfigurationModel.getElement(Role.get("Show Action Header Group")).getReferredModel();
		boolean showActionGroupValue = "Yes".equals(showActionGroup.getValue()); // "Yes" = tersus.runtime.BooleanHandler.YES_LITTERAL

		DataType showCountFooterRow = (DataType) initConfigurationModel.getElement(Role.get("Show Count Footer Row")).getReferredModel();
		boolean showCountFooterRowValue = "Yes".equals(showCountFooterRow.getValue()); // "Yes" = tersus.runtime.BooleanHandler.YES_LITTERAL

		tableConfiguration = new TableConfiguration(defaultOptimizationValue,
				numberOfPagesToDisplayValue, numberOfRecordsToDisplayValue, showSearchGroupValue,
				showActionGroupValue, showCountFooterRowValue);
	}

	public String getInitialTableName()
	{
		return initialTableName;
	}

	public String getDatabaseTableName()
	{
		return databaseTableName;
	}

	public ModelElement getRecordElement()
	{
		return recordElement;
	}

	public List<TableFieldDescriptor> getInitialFields()
	{
		return fields;
	}

	public TableConfiguration getInitialTableConfiguration()
	{
		return tableConfiguration;
	}

	public void setNewDetails(String tableName, String databaseTableName,
			List<TableFieldDescriptor> fields, TableConfiguration configuration)
	{
		this.tableName = tableName;
		this.fields = fields;
		this.tableConfiguration = configuration;
		this.databaseTableName = databaseTableName;
	}

	public boolean canExecute()
	{
		return (fields != null);
	}

	/**
	 * 
	 * Modifying a generic table management pane template according to a specific database record
	 * 
	 * Existing models in the target project can be referenced.
	 * 
	 * can we avoid inconsistencies and allow for proper 'undo' if there are unsaved modified models
	 * prior to invoking this method?
	 * 
	 */
	public void run()
	{
		boolean isCompactModel = model.checkPluginVersion(4); // Version 4 is Dudu's compact implementation of 24/8/10 

		DataType recordModel = (DataType) recordElement.getReferredModel();

		// [1] Set top pane name and display table name (or rename them)
		if (topElement != null)
			if (!topElement.getElementName().equals(tableName))
			{
				changeModelId(model, tableName);
//				renameElement(topElement, tableName); // Does it crash if done after unsaved rename?
				RepositoryIndex index = repository.getUpdatedRepositoryIndex();
				List<ElementEntry> elements = index.getReferenceEntries(model.getModelId());
				for (ElementEntry entry : elements)
				{
					ModelElement e = index.getModelElement(entry);
					renameElement(e, tableName);
				}
			}

		if (!model.getPackageId().getName().equals(tableName))
			renamePackage(model.getPackageId(), tableName);

		PackageId topPackageId = model.getPackageId();

		ModelElement tabelElement = model.getElementByPlugin(isNewUI ? "Tersus/UI/Table" : "Tersus/Display/Table");
		if (!tabelElement.getElementName().equals(tableName))
			renameElement(tabelElement, tableName); // Does it still crash if done after unsaved rename?

		// [2] Set database table name (unless already defined)
		Model tableModel = tabelElement.getReferredModel();

		// if (!recordModel.getName().equals(model.getName())) Rename disabled (see mail to Dudu on the 19/8/09) changeModelId(recordModel, model.getName());

		if ((databaseTableName == null) || databaseTableName.equals("-"))
		{
			databaseTableName = Misc.sqlize(model.getName());
			setProperty(recordModel, BuiltinProperties.TABLE_NAME, databaseTableName);
		}

		// [3.1] Check which fields participate in filtering and in which type of filtering
		boolean shouldShowFromFilterRow = false;
		boolean shouldShowToFilterRow = false;
		List<Role> notRangeFilteringFields = new ArrayList<Role>(); // Negative list - all fields except 'Range' filtering fields
		List<Role> notOtherFilteringFields = new ArrayList<Role>(); // Negative list - all fields except filtering fields that are not 'Range'
		for (int i = 0; i < fields.size(); i++)
		{
			Role role = Role.get(fields.get(i).getName());

			if (fields.get(i).getShowRow())
			{
				int filteringIntValue = fields.get(i).getFiltering();

				if (filteringIntValue == Filtering.RANGE)
				{
					shouldShowFromFilterRow = true;
					shouldShowToFilterRow = true;
				}
				else
					notRangeFilteringFields.add(role);

				if (filteringIntValue == Filtering.EXACT
						|| filteringIntValue == Filtering.STARTS_WITH
						|| filteringIntValue == Filtering.CONTAINS)
					shouldShowFromFilterRow = true;
				else
					notOtherFilteringFields.add(role);
			}
			else
			// 'getShowRow() != true' means ignoring the field
			{
				notRangeFilteringFields.add(role);
				notOtherFilteringFields.add(role);
			}
		}

		// [3.2] Set configuration details
		FlowModel initModel = (FlowModel) model.getElement(Role.get("Init")).getReferredModel();
		FlowModel initConfigurationModel = (FlowModel) initModel.getElement(Role.get("Init Configuration")).getReferredModel();

		ModelElement defaultOptimization = initConfigurationModel.getElement(Role.get("Default Optimization"));
		setProperty(defaultOptimization, BuiltinProperties.REF_ID, tableConfiguration.defaultOptimization() ? BuiltinModels.YES_CONSTANT_ID : BuiltinModels.NO_CONSTANT_ID);

		DataType numberOfPagesToDisplay = (DataType) initConfigurationModel.getElement(Role.get("Number of Pages to Display")).getReferredModel();
		setProperty(numberOfPagesToDisplay, BuiltinProperties.VALUE, (new Integer(tableConfiguration.numberOfPagesToDisplay())).toString());

		DataType numberOfRecordsToDisplay = (DataType) initConfigurationModel.getElement(Role.get(isCompactModel ? "Number of Records to Display" : "Number of Records per Page")).getReferredModel();
		setProperty(numberOfRecordsToDisplay, BuiltinProperties.VALUE, (new Integer(tableConfiguration.numberOfRecordsPerPage())).toString());

		ModelElement showPagingHeaderGroup = initConfigurationModel.getElement(Role.get("Show Paging Header Group"));
		setProperty(showPagingHeaderGroup, BuiltinProperties.REF_ID, tableConfiguration.showPagingHeaderGroup() ? BuiltinModels.YES_CONSTANT_ID	: BuiltinModels.NO_CONSTANT_ID);

		ModelElement showActionHeaderGroup = initConfigurationModel.getElement(Role.get("Show Action Header Group"));
		setProperty(showActionHeaderGroup, BuiltinProperties.REF_ID, tableConfiguration.showActionHeaderGroup() ? BuiltinModels.YES_CONSTANT_ID	: BuiltinModels.NO_CONSTANT_ID);

		ModelElement showFromFilterRow = initConfigurationModel.getElement(Role.get("Show From Filter Row"));
		setProperty(showFromFilterRow, BuiltinProperties.REF_ID, shouldShowFromFilterRow ? BuiltinModels.YES_CONSTANT_ID : BuiltinModels.NO_CONSTANT_ID);

		ModelElement showToFilterRow = initConfigurationModel.getElement(Role.get("Show To Filter Row"));
		setProperty(showToFilterRow, BuiltinProperties.REF_ID, shouldShowToFilterRow ? BuiltinModels.YES_CONSTANT_ID : BuiltinModels.NO_CONSTANT_ID);

		ModelElement showCountFooterRow = initConfigurationModel.getElement(Role.get("Show Count Footer Row"));
		setProperty(showCountFooterRow, BuiltinProperties.REF_ID, tableConfiguration.showCountFooterRow() ? BuiltinModels.YES_CONSTANT_ID : BuiltinModels.NO_CONSTANT_ID);

		// [4.1] Create or update field data types according to their valid values
		Model validValuesStructure = repository.getModel(getValidValuesStructureId(model.getModelId()), true);

		for (int i = 0; i < fields.size(); i++)
		{
			TableFieldDescriptor fieldDescriptor = fields.get(i);
			// if (!fieldDescriptor.getShowRow())
			// continue; // 'getShowRow() != true' means ignoring the field

			if ("".equals(fieldDescriptor.getValidValues()))
				fieldDescriptor.setValidValues(null);

			addOrUpdateValidValuesModeling(fieldDescriptor, recordModel.getPackageId(),	validValuesStructure); // May update the data type of 'fieldDescriptor'
		}

		// [4.2] Add, update or delete fields in the database record
		for (int i = 0; i < fields.size(); i++)
		{
			TableFieldDescriptor fieldDescriptor = fields.get(i);

			String initialName = fieldDescriptor.getInitialName();
			DataType dataModel = (DataType) repository.getModel(fieldDescriptor.getDataType(), true);
			String name = fieldDescriptor.getName();

			if (name == null) // Field marked for deletion
			{
				// Delete the field's element from the record
				deleteElement(recordModel, initialName, false, false, true);
			}
			else
			{
				DataElement element;

				if (initialName == null) // A new field
				{
					// Add a new element to the record
					element = (DataElement) addSubModel(recordModel, dataModel, name, 0, 0);
				}
				else
				// A previously existing field
				{
					element = (DataElement) recordModel.getElement(Role.get(initialName));

					// Rename the field's element
					if (!name.equals(initialName))
						renameElement(element, name);

					// Change the type of the field's element
					if (!dataModel.getId().equals(element.getModelId()))
						setProperty(element, BuiltinProperties.REF_ID, dataModel.getId());
				}

				setProperty(element, BuiltinProperties.MANDATORY, fieldDescriptor.isMandatoty());
				setProperty(element, BuiltinProperties.PRIMARY_KEY, fieldDescriptor.isPrimaryKey());
				setProperty(element, BuiltinProperties.DATABASE_GENERATED, fieldDescriptor.isAutoIncrementPrimaryKey());
			}
		}

		// [4.3] Add, update or delete fields in the primary key data structure
		ModelId createPkModelId = repository.getModelId(topPackageId.toString()+"/Wizard/Transformations/Convert Record to Primary Key");
		FlowModel createPkModel = (FlowModel) repository.getModel(createPkModelId, true);
		if (createPkModel == null)
			throw new ModelException("Missing model: " + createPkModelId.getPath());
		Model PkModel = (DataType) createPkModel.getElement(PkRole).getReferredModel();

		for (ModelElement element : PkModel.getElements()) // We mark existing fields as non-mandatory (unless changed below to mandatory - they will be deleted)
		{
			if (element instanceof DataElement)
				setProperty(element, BuiltinProperties.MANDATORY, false);
		}

		for (int i = 0; i < fields.size(); i++) // Partially repeating [4.2] above (not combined for simplicity)
		{
			TableFieldDescriptor fieldDescriptor = fields.get(i);

			String initialName = fieldDescriptor.getInitialName();
			DataType dataModel = (DataType) repository.getModel(fieldDescriptor.getDataType(), true);
			String name = fieldDescriptor.getName();

			DataElement element = (initialName != null ? (DataElement) PkModel.getElement(Role.get(initialName)) : null);

			boolean newPkField = (element == null) && fieldDescriptor.isPrimaryKey();
			boolean existingPkField = (element != null) && fieldDescriptor.isPrimaryKey();
			boolean deletedPkField = (element != null) && !fieldDescriptor.isPrimaryKey();

			if (deletedPkField) // Field should be deleted from the primary key data structure
			{
				// Delete the field's element from the record (with all links from it or to it anywhere in the whole repository)
				deleteElement(PkModel, initialName, true, false, true);
			}
			else if (newPkField) // Field should be added to the primary key data structure
			{
				// Add a new element to the record
				element = (DataElement) addSubModel(PkModel, dataModel, name, 0, 0);
			}
			else if (existingPkField) // A previously existing field in the primary key data structure
			{
				// Rename the field's element
				if (!name.equals(initialName))
					renameElement(element, name);

				// Change the type of the field's element
				if (!dataModel.getId().equals(element.getModelId()))
					setProperty(element, BuiltinProperties.REF_ID, dataModel.getId());
			}

			if (newPkField || existingPkField)
				setProperty(element, BuiltinProperties.MANDATORY, true);
		}

		List<ModelElement> pkElements = PkModel.getElements(); // Also deleting redundant fields (will only become relevant when we support deletion of fields from the database record)
		if (pkElements != null)
		{
			for (int i = pkElements.size() - 1; i >= 0; i--)
			{
				ModelElement element = pkElements.get(i);
				if (element instanceof DataElement)
					if (!Boolean.TRUE.equals(element.getProperty(BuiltinProperties.MANDATORY)))
						deleteElement(PkModel, element.getElementName(), true, false, true);
			}
		}

		// TODO Implement flow deletion in 'syncCompositePopulate' and 'syncCompositeExtract'

		List<Role> ignoreInRecordModel = new ArrayList<Role>();
		for (int i = 0; i < fields.size(); i++)
			if (!fields.get(i).getShowRow()) // 'getShowRow() != true' means ignoring the field
				ignoreInRecordModel.add(Role.get(fields.get(i).getName()));

		// [4.4] Enrich the 'Convert Record to Primary Key' process
		syncComposositeTransformation(createPkModel, RecordRole, null, null, PkRole);

		// [4.5] Enrich the 'Convert Primary Key to Record' process
		ModelId createFromPkModelId = repository.getModelId(topPackageId.toString()+"/Wizard/Transformations/Convert Primary Key to Record");
		FlowModel createFromPkModel = (FlowModel) repository.getModel(createFromPkModelId, true);
		if (createFromPkModel == null)
			throw new ModelException("Missing model: " + createFromPkModelId.getPath());

		syncComposositeTransformation(createFromPkModel, PkRole, null, null, RecordRole);

		// [4.6] Enrich the 'Merge Record' process
		ModelId mergeModelId = repository.getModelId(topPackageId.toString()+"/Wizard/Transformations/Merge Records");
		FlowModel mergeModel = (FlowModel) repository.getModel(mergeModelId, true);
		if (mergeModel == null)
			throw new ModelException("Missing model: " + mergeModelId.getPath());

		syncComposositeTransformation(mergeModel, Role.get("Client-side"), Role.get("Server-side"), ignoreInRecordModel, RecordRole);

		// [5.1] Add, update or delete corresponding display elements in the table row
		Model tableRowModel = tableModel.getElement(Role.get(isNewUI ? "[Selected Row]" : "<Selected Row>")).getReferredModel();
		PackageId currentPackageId = tableRowModel.getPackageId();

		int newNumberOfFields = 0;
		for (int i = 0; i < fields.size(); i++)
			if (fields.get(i).getShowRow()) // 'getShowRow() != true' means ignoring the field
				if (fields.get(i).getName() != null)
					newNumberOfFields++;

		boolean fieldsDeleted = (newNumberOfFields < fields.size());

		double sizeFactor = 1 + 1.5 / newNumberOfFields;
		double width = 0.1 * sizeFactor;
		double height = 0.4 * sizeFactor;

		int newIndex = 0;
		for (int i = 0; i < fields.size(); i++)
		{
			TableFieldDescriptor fieldDescriptor = fields.get(i);
			if (!fieldDescriptor.getShowRow())
				continue; // 'getShowRow() != true' means ignoring the field

			String initialName = (fieldDescriptor.isInitiallyShown() ? fieldDescriptor.getInitialName() : null);
			String name = fieldDescriptor.getName();
			ModelId genericDataType = fieldDescriptor.getGenericDataType(repository);
			DataElement typeValues = (DataElement) validValuesStructure.getElementByReferredModel(fieldDescriptor.getDataType());
			boolean useChooser = !fieldDescriptor.getDataType().equals(genericDataType)	&& typeValues != null;
			ModelId templateId = (useChooser ? CHOOSER_TEMPLATE_ID : getTemplateIdForField(genericDataType)); // Change to 'false' once 'Display' and 'InputField' templates use the same plug-in
			ModelId newDisplayModelId = new ModelId(currentPackageId, name);

			Model parentModel = tableRowModel;
			double x = (newIndex + 1) * 0.9 / (newNumberOfFields + 1);
			double y = 0.56;

			SubFlow element = null;
			SubFlow fieldElement = null; // When 'element' is the wrapping td, this is the child element
			boolean directChild = true;
			if (initialName != null)
			{
				element = (SubFlow) tableRowModel.getElement(Role.get(isNewUI ? initialName.concat(" Td") : initialName));
				fieldElement = (isNewUI ? (SubFlow) element.getReferredModel().getElement(Role.get(initialName)) : element);
				if (element == null || !isTemplateIdForField(fieldElement.getReferredModel())) // Field with this role not found or is not the field created by the wizard
				{
					directChild = false;

					List<Path> descendants = tableRowModel.findSubFlow(Role.get(initialName), 4);
					for (int j = 0; j < descendants.size(); j++)
					{
						ModelElement e = tableRowModel.getElement((Path) descendants.get(j));
						if (e instanceof SubFlow)
						{
							element = (SubFlow) e;
							fieldElement = (isNewUI ? (SubFlow) element.getReferredModel().getElement(Role.get(initialName)) : element);
							if (fieldElement != null && isTemplateIdForField(fieldElement.getReferredModel()))
							{
								parentModel = element.getParentModel();
								x = element.getPosition().getX(); // If not a direct child, preserve position
								y = element.getPosition().getY(); // If not a direct child, preserve position
								break;
							}
						}
					}
				}
			}

			// Rename the field's element (if required)
			String existingElementName = (isNewUI && initialName != null ? initialName.concat(" Td") : initialName);
			String existingFieldElementName = initialName;
			if ((name != null) && (initialName != null) && !name.equals(initialName))
			{
				renameElement(element, existingElementName = (isNewUI ? name.concat(" Td") : name));
				if (isNewUI)
				{
					fieldElement = (SubFlow) element.getReferredModel().getElement(Role.get(initialName));
					renameElement(fieldElement, existingFieldElementName = name);
				}
			}

			// Delete the field's element from the row (with its init process)
			// (if field is marked for deletion or data type is changed)
			fieldElement = (isNewUI && element != null ? (SubFlow) element.getReferredModel().getElement(Role.get(existingFieldElementName)) : element);
			Model existingModel = (fieldElement == null ? null : fieldElement.getModel());
			boolean templateChanged = (fieldElement != null)	&& !existingModel.getPlugin().equals(repository.getModel(templateId, true).getPlugin());
			if ((name == null) || fieldDescriptor.hasDataTypeChanged() || templateChanged)
			{
				deleteEarlyInitOfChooser (existingModel);
				if (isNewUI)
				{
					Model tdModel = element.getReferredModel();
					deleteElement(tdModel, existingFieldElementName, false, true, true);
				}
				deleteElement(parentModel, existingElementName, false, true, true);
			}

			// Add a new element (if a new field or data type is changed)
			if ((initialName == null) || fieldDescriptor.hasDataTypeChanged() || templateChanged)
			{
				Model displayModel = createModelFromTemplateOrPrototype(templateId,	newDisplayModelId, isNewUI);
				element = (SubFlow) addSubModel(parentModel, displayModel, displayModel.getName(), x, y, width, height);
				fieldElement = isNewUI ? (SubFlow) element.getReferredModel().getElement(Role.get(name)) : element;
				ensureReadOnlyOption(fieldElement.getModel());
			}
			else if (name != null && directChild) // Existing element
			{
				// Just ensure its positioning is correct (might move if previous elements have been deleted or number of elements changed)
				RelativePosition position = new RelativePosition(x, y);
				setProperty(element, BuiltinProperties.POSITION, position);

				RelativeSize size = new RelativeSize(width, height);
				setProperty(element, BuiltinProperties.SIZE, size);
			}

			// Set field's data type (if a user defined data type)
			updateFieldTypeIfUserDefined(fieldElement, fieldDescriptor, validValuesStructure);

			// Set display field html.sortColumnName to match record's columnName 
			setProperty(fieldElement, BuiltinProperties.SORT_COLUMN_NAME, fieldDescriptor.getColumnName());
				
			// Set field's filtering type
			int filteringIntValue = fieldDescriptor.getFiltering();
//			String filtering = (filteringIntValue == Filtering.NO_FILTERING ? null : Filtering.getFilteringName(filteringIntValue));
			setProperty(fieldElement, BuiltinProperties.FILTERING, Filtering.getFilteringName(filteringIntValue));

			if (name != null)
				newIndex++;
		}

		// [5.2] Enrich the extract processes
		ModelId extractFromModelId = repository.getModelId(topPackageId.toString()+"/Wizard/Transformations/Extract From Constraints");
		FlowModel extractFromModel = (FlowModel) repository.getModel(extractFromModelId, true);
		if (extractFromModel == null)
			throw new ModelException("Missing model: " + extractFromModelId.getPath());

		syncCompositeExtract(tableRowModel, Role.get("Trigger"), Role.get(isNewUI ? "Body Row" : "Table Row"), isNewUI ? " Td" : null,
				recordModel, notRangeFilteringFields, Role.get("Exit"), RecordRole,
				extractFromModel, fieldsDeleted, true, true);

		ModelId extractToModelId = repository.getModelId(topPackageId.toString()+"/Wizard/Transformations/Extract To Constraints");
		FlowModel extractToModel = (FlowModel) repository.getModel(extractToModelId, true);
		if (extractToModel == null)
			throw new ModelException("Missing model: " + extractToModelId.getPath());

		syncCompositeExtract(tableRowModel, Role.get("Trigger"), Role.get(isNewUI ? "Body Row" : "Table Row"), isNewUI ? " Td" : null,
				recordModel, notRangeFilteringFields, Role.get("Exit"), RecordRole, extractToModel,
				fieldsDeleted, true, true);

		ModelId extractMatchModelId = repository.getModelId(topPackageId.toString()+"/Wizard/Transformations/Extract Match Constraints");
		FlowModel extractMatchModel = (FlowModel) repository.getModel(extractMatchModelId, true);
		if (extractMatchModel == null)
			throw new ModelException("Missing model: " + extractMatchModelId.getPath());

		replaceFilteringActionsWithLinks(extractMatchModel, Role.get(isNewUI ? "Body Row" : "Table Row"), RecordRole); // In case of STARTS_WITH or CONTAINS filtering
		syncCompositeExtract(tableRowModel, Role.get("Trigger"), Role.get(isNewUI ? "Body Row" : "Table Row"), isNewUI ? " Td" : null,
				recordModel, notOtherFilteringFields, Role.get("Exit"), RecordRole,
				extractMatchModel, fieldsDeleted, true, true);
		replaceLinksWithFilteringActions(extractMatchModel, Role.get(isNewUI ? "Body Row" : "Table Row"), RecordRole); // In case of STARTS_WITH or CONTAINS filtering

		// [6.1] Enrich the 'Convert Record to Row' process
		ModelId convertModelId = repository.getModelId(topPackageId.toString()+"/Wizard/Transformations/Convert Record to Row");
		FlowModel convertModel = (FlowModel) repository.getModel(convertModelId, true);
		if (convertModel == null)
			throw new ModelException("Missing model: " + convertModelId.getPath());

		Path targetDisplayElementPath = new Path(isNewUI ? "Body Row" : "Table Row");
		syncCompositePopulate(recordModel, ignoreInRecordModel, Role.get("Record"), RecordRole,
				tableRowModel, Role.get("Row"), targetDisplayElementPath, isNewUI ? " Td" : null, convertModel,
				fieldsDeleted, true, true);

		List<Path> fieldRelativePaths = getDisplayFields(convertModel, RecordRole, targetDisplayElementPath);

		// [6.2] Set visibility of fields in the 'From' and 'To' rows
		ModelId initHeaderByConfigurationModelId = repository.getModelId(topPackageId.toString()+(isCompactModel ? "/Init/Init Table Header by Configuration" : "/Reusable Models/Init Display/Init Table by Configuration"));
		FlowModel initHeaderByConfigurationModel = (FlowModel) repository.getModel(initHeaderByConfigurationModelId, true);

		FlowModel initFromVisibilityModel = (FlowModel) initHeaderByConfigurationModel.getElement(Role.get("Init From Filter Row Visibility")).getReferredModel();
		updateFieldVisibilities(initFromVisibilityModel, Role.get(isNewUI ? "Body Row" : "Table Row"), fieldRelativePaths,	false);

		FlowModel initToVisibilityModel = (FlowModel) initHeaderByConfigurationModel.getElement(Role.get("Init To Filter Row Visibility")).getReferredModel();
		updateFieldVisibilities(initToVisibilityModel, Role.get(isNewUI ? "Body Row" : "Table Row"), fieldRelativePaths, true);

		// [7] Enrich the 'Convert Row to Record' process (used by both 'Update' and 'Delete')
		ModelId backConvertModelId = repository.getModelId(topPackageId.toString()+"/Wizard/Transformations/Convert Row to Record");
		FlowModel backConvertModel = (FlowModel) repository.getModel(backConvertModelId, true);
		if (backConvertModel == null)
			throw new ModelException("Missing model: " + backConvertModelId.getPath());

		syncCompositeExtract(tableRowModel, Role.get("Row"),
				Role.get(tableRowModel.getModelName()), isNewUI ? " Td" : null, recordModel, ignoreInRecordModel,
				Role.get("Record"), RecordRole, backConvertModel, fieldsDeleted, false, true);

		// [8] Create row per field in the details grid (used by both 'New' and 'Update')
		ModelId gridModelId = repository.getModelId(topPackageId.toString()+"/Wizard/Details Grid/Details Grid");
		FlowModel gridModel = (FlowModel) repository.getModel(gridModelId, true);
		if (gridModel == null)
			throw new ModelException("Missing model: " + gridModelId.getPath());

		currentPackageId = gridModel.getPackageId();
		String rowNameSuffix = (isNewUI ? " " : " Row"); // In the new UI the caption names comes from the "row" (the labeled field), but need to get rid of the extra " "

		newIndex = 0;
		TableFieldDescriptor autoPrimaryKey = null;
		String autoPrimaryKeyRowName = null;
		for (int i = 0; i < fields.size(); i++)
		{
			TableFieldDescriptor fieldDescriptor = fields.get(i);

			if (fieldDescriptor.isAutoPrimaryKey())
				autoPrimaryKey = fieldDescriptor;

			if (!fieldDescriptor.getShowRow())
				continue; // 'getShowRow() != true' means ignoring the field

			String initialName = (fieldDescriptor.isInitiallyShown() ? fieldDescriptor.getInitialName() : null);
			String name = fieldDescriptor.getName();
			ModelId genericDataType = fieldDescriptor.getGenericDataType(repository);
			DataElement typeValues = (DataElement) validValuesStructure.getElementByReferredModel(fieldDescriptor.getDataType());
			boolean useChooser = !fieldDescriptor.getDataType().equals(genericDataType) && typeValues != null;
			ModelId templateId = (useChooser ? CHOOSER_TEMPLATE_ID : getTemplateIdForField(genericDataType));

			String initailRowName = (initialName != null ? initialName + rowNameSuffix : null);
			String rowName = (name != null ? name + rowNameSuffix : null);
			FlowModel rowModel = null;
			SubFlow rowElement = null;
			if (initialName != null)
			{
				rowElement = (SubFlow) gridModel.getElement(Role.get(initailRowName));
				rowModel = (FlowModel) rowElement.getReferredModel();
			}

			// Delete the field's row with all its children (if field is marked for deletion)
			if (name == null)
			{
				List<ModelElement> elements = rowModel.getElements();
				for (int e = elements.size() - 1; e >= 0; e--)
					deleteElement(rowModel, elements.get(e).getRole().toString(), false, true, true);
				deleteElement(gridModel, initailRowName, false, true, true);
			}
			else
			{
				double x = 0.5 + i * 0.0001;
				double y = 0.05 + (newIndex + 1) * 0.9 / (newNumberOfFields + 1);

				// Rename the field's row and its child elements (if required)
				String existingElementName = initialName;
				if ((name != null) && (initialName != null) && !name.equals(initialName))
				{
					renameElement(rowElement, rowName);

					if (!isNewUI) // In the new UI the label element is always named "Label" (its caption is dynamic)
					{
						SubFlow labelElement = (SubFlow) rowModel.getElement(Role.get(initialName + ":"));
						renameElement(labelElement, name + ":");
					}

					SubFlow FieldElement = (SubFlow) rowModel.getElement(Role.get(initialName));
					renameElement(FieldElement, name); // Required even if we delete it in the next step (to preserve flows)
					existingElementName = name;
				}

				// Delete the field's existing element from the field's row (if data type is changed)
				boolean addField = false;
				SubFlow fieldElement = (rowModel != null ? (SubFlow) rowModel.getElement(Role.get(name)) : null);
				boolean templateChanged = (fieldElement != null)
						&& !fieldElement.getModel().getPlugin().equals(repository.getModel(templateId,true).getPlugin());
				if (fieldDescriptor.hasDataTypeChanged() || templateChanged)
				{
					Model fieldModel = fieldElement.getModel();
					if (fieldModel.getPlugin().equals(repository.getModel(CHOOSER_TEMPLATE_ID, true).getPlugin()))
						deleteEarlyInitOfChooser (fieldModel);
					deleteElement(rowModel, existingElementName, false, true, true);
					addField = true; // The new field will replace the one just deleted
				}

				// Add a new row (if a new field)
				if (initialName == null)
				{
					// Row Itself
					ModelId rowModelId = new ModelId(currentPackageId, rowName);
					rowModel = (FlowModel) createModelFromTemplateOrPrototype(isNewUI ? LABELED_FIELD_TEMPLATE_ID : ROW_TEMPLATE_ID,rowModelId, false);
					addSubModel(gridModel, rowModel, rowName, x, y, 0.5, 0.1);

					// Label element
					if (!isNewUI) // In the new UI the label was created as part of the parent model
					{
						ModelId labelModelId = new ModelId(currentPackageId, name + ":");
						FlowModel labelModel = (FlowModel) createModelFromTemplateOrPrototype(LABEL_TEMPLATE_ID, labelModelId, false);
						addSubModel(rowModel, labelModel, name + ":", 0.3, 0.5);
					}

					addField = true;
				}
				else
				// Or just ensure its positioning is correct (might move if previous rows have been deleted or number of rows changed)
				{
					RelativePosition position = new RelativePosition(x, y);
					setProperty(rowElement, BuiltinProperties.POSITION, position);
				}

				// Row of an automatically managed primary key may become invisible sometimes
				if (fieldDescriptor.isAutoPrimaryKey())
				{
					DataElement visibilityElement = (DataElement) rowModel.getElement(DisplayModelWrapper.VISIBLE_RESERVED_ROLE);
					if (visibilityElement == null)
					{
						Model booleanModel = repository.getModel(BuiltinModels.BOOLEAN_ID, true);
						addSubModel(rowModel, booleanModel,
								DisplayModelWrapper.VISIBLE_RESERVED_ROLE.toString(), 0.92, 0.16, 0.125, 0.2);
					}
					autoPrimaryKeyRowName = rowName; /* 'autoPrimaryKey' already set above */
				}

				// Add the field's element (if a new field or data type is changed)
				if (addField)
				{
					ModelId inputFieldModelId = new ModelId(currentPackageId, name);
					Model inputFieldModel = createModelFromTemplateOrPrototype(templateId,inputFieldModelId, false);
					ensureReadOnlyOption(inputFieldModel);
					addSubModel(rowModel, inputFieldModel, name, 0.6, 0.5);
					// TODO In new UI, delete 'caption' if checkbox
				}

				// Set field's data type (if a user defined data type)
				updateFieldTypeIfUserDefined(rowModel.getElement(Role.get(name)), fieldDescriptor, validValuesStructure);

				newIndex++;
			}
		}

		// [9] Enrich the initialization processes for the details grid (used by 'New' and 'Update' respectively)
		ModelId newInitModelId = repository.getModelId(topPackageId.toString()+"/Table/Action Header Group/New/Init");
		FlowModel newInitModel = (FlowModel) repository.getModel(newInitModelId, true);
		if (newInitModel == null)
			throw new ModelException("Missing model: " + newInitModelId.getPath());
		hideAutoManagedKeyField(newInitModel, new Path("Enter Details/Details Grid"), autoPrimaryKeyRowName);

		ModelId populateGridModelId = repository.getModelId(topPackageId.toString()+"/Wizard/Transformations/Convert Record to Grid");
		FlowModel populateGridModel = (FlowModel) repository.getModel(populateGridModelId, true);
		if (populateGridModel == null)
			throw new ModelException("Missing model: " + populateGridModelId.getPath());

		syncCompositePopulate(recordModel, ignoreInRecordModel, Role.get("Record"), RecordRole,
				gridModel, Role.get("Grid"), new Path("Details Grid"), rowNameSuffix,
				populateGridModel, fieldsDeleted, true, true);

		// [10] Enrich the record extraction process (used by 'New/Save' and 'Update/Save')
		ModelId extractModelId = repository.getModelId(topPackageId.toString()+"/Wizard/Transformations/Convert Grid to Record");
		FlowModel extractModel = (FlowModel) repository.getModel(extractModelId, true);
		if (extractModel == null)
			throw new ModelException("Missing model: " + extractModelId.getPath());

		syncCompositeExtract(gridModel, Role.get("Grid"), Role.get(gridModel.getModelName()),
				rowNameSuffix, recordModel, ignoreInRecordModel, Role.get("Record"), RecordRole,
				extractModel, fieldsDeleted, false, true);

		// [11] Enrich the process of saving a new record (used by 'New/Save')
		ModelId creationModelId = repository.getModelId(topPackageId.toString()+"/Table/Action Header Group/New/Save/Save New/Perform Action");
		FlowModel creationModel = (FlowModel) repository.getModel(creationModelId, true);
		if (creationModel == null)
			throw new ModelException("Missing model: " + creationModelId.getPath());
		autoManageKeyField(creationModel, RecordRole, autoPrimaryKey);

		// [12] In each view containing a data type, use its initialization process
		// to set the corresponding values in the global valid values structure
		for (int i = 0; i < fields.size(); i++)
		{
			TableFieldDescriptor fieldDescriptor = fields.get(i);
//			if (fieldDescriptor.getShowRow()) // 'getShowRow() != true' means ignoring the field
				if (fieldDescriptor.getValidValues() != null)
					addOrUpdateValidValuesInit(fieldDescriptor);
		}
	}

	static ModelId ROW_TEMPLATE_ID = new ModelId("Common/Templates/Display/Row/Row");
	static ModelId LABELED_FIELD_TEMPLATE_ID = new ModelId("Common/Templates/UI/Labeled Field/Template/Labeled Field");
	static ModelId TEXT_INPUT_FIELD_TEMPLATE_ID = new ModelId("Common/Templates/Display/Text Input Field/Text Input Field");
	static ModelId TEXT_FIELD_TEMPLATE_ID = new ModelId("Common/Templates/UI/Text Field/Text Field");
	static ModelId NUMBER_INPUT_FIELD_TEMPLATE_ID = new ModelId("Common/Templates/Display/Number Input Field/Number Input Field");
   	static ModelId NUMBER_FIELD_TEMPLATE_ID = new ModelId("Common/Templates/UI/Number Field/Number Field");
   	static ModelId DATE_INPUT_FIELD_TEMPLATE_ID = new ModelId("Common/Templates/Display/Date Input Field/Date Input Field");
   	static ModelId DATE_FIELD_TEMPLATE_ID = new ModelId("Common/Templates/UI/Date Field/Date Field");
   	static ModelId DATE_AND_TIME_INPUT_FIELD_TEMPLATE_ID = new ModelId("Common/Templates/Display/Date and Time Input Field/Template/Date and Time Input Field");
   	static ModelId CHECKBOX_TEMPLATE_ID = new ModelId("Common/Templates/Display/Check Box/Check Box");
   	static ModelId UI_CHECKBOX_TEMPLATE_ID = new ModelId("Common/Templates/UI/Check Box/Check Box");
   	static ModelId CHOOSER_TEMPLATE_ID = new ModelId("Common/Templates/Display/Chooser/Chooser");
	static ModelId LABEL_TEMPLATE_ID = new ModelId("Common/Templates/Display/Label/Label");
	static ModelId SEQUENCE_NUMBER_GENERATOR_ID = new ModelId("Common/Templates/Database/Sequence Number/Sequence Number");
	static ModelId TIMESTAMP_GENERATOR_ID = new ModelId("Common/Templates/Dates/Now/Now");

	private ModelId getTemplateIdForField(ModelId dataTypeId) // Any update should be reflected in isTemplateIdForField() as well
	{
		if (BuiltinModels.TEXT_ID.equals(dataTypeId))
			return (isNewUI ? TEXT_FIELD_TEMPLATE_ID : TEXT_INPUT_FIELD_TEMPLATE_ID);
		else if (BuiltinModels.NUMBER_ID.equals(dataTypeId))
			return (isNewUI ? NUMBER_FIELD_TEMPLATE_ID : NUMBER_INPUT_FIELD_TEMPLATE_ID);
		else if (BuiltinModels.DATE_ID.equals(dataTypeId))
			return (isNewUI ? DATE_FIELD_TEMPLATE_ID : DATE_INPUT_FIELD_TEMPLATE_ID);
		else if (BuiltinModels.DATE_AND_TIME_ID.equals(dataTypeId))
			return (DATE_AND_TIME_INPUT_FIELD_TEMPLATE_ID);
		else if (BuiltinModels.BOOLEAN_ID.equals(dataTypeId))
			return (isNewUI ? UI_CHECKBOX_TEMPLATE_ID: CHECKBOX_TEMPLATE_ID);
		else
			return null;
	}

	protected static boolean isTemplateIdForField(Model model)
	{
		String prototype = model.getPrototype();
		if (prototype == null)
			return false;
		return     prototype.equals(TEXT_INPUT_FIELD_TEMPLATE_ID.toString())
				|| prototype.equals(TEXT_FIELD_TEMPLATE_ID.toString())
				|| prototype.equals(NUMBER_INPUT_FIELD_TEMPLATE_ID.toString())
				|| prototype.equals(NUMBER_FIELD_TEMPLATE_ID.toString())
				|| prototype.equals(DATE_INPUT_FIELD_TEMPLATE_ID.toString())
				|| prototype.equals(DATE_FIELD_TEMPLATE_ID.toString())
				|| prototype.equals(DATE_AND_TIME_INPUT_FIELD_TEMPLATE_ID.toString())
				|| prototype.equals(CHECKBOX_TEMPLATE_ID.toString())
				|| prototype.equals(UI_CHECKBOX_TEMPLATE_ID.toString())
				|| prototype.equals(CHOOSER_TEMPLATE_ID.toString());
	}

	private void ensureReadOnlyOption (Model model)
	{
		if (model.getElement(DisplayModelWrapper.READ_ONLY_RESERVED_ROLE) == null)
		{
			Model booleanModel = repository.getModel(BuiltinModels.BOOLEAN_ID, true);
			addSubModel(model, booleanModel, DisplayModelWrapper.READ_ONLY_RESERVED_ROLE.toString(), 0.3, 0.5, 0.3, 0.12);
		}
	}

	private void hideAutoManagedKeyField(FlowModel initModel, Path displayElementPath,
			String hiddenElementName)
	{
		// Unhide previously hidden element (if any)
		String noConstantName = BuiltinModels.NO_CONSTANT_ID.getName();
		Role noConstantRole = Role.get(noConstantName);
		ModelElement noConstant = initModel.getElement(noConstantRole);
		Path hideLinkSource = new Path(noConstantRole);
		if (noConstant != null)
		{
			Link link = initModel.getLinkFrom(hideLinkSource, displayElementPath,
					DisplayModelWrapper.VISIBLE_RESERVED_ROLE);
			if (link != null)
			{
				deleteElement(initModel, link.getElementName(), false, false, true);
				if (hiddenElementName == null)
				{
					deleteElement(initModel, noConstantName, false, false, true);
					noConstant = null;
				}
			}
		}

		// Hide the required element (if any)
		if (hiddenElementName != null)
		{
			if (noConstant == null)
			{
				Model noConstantModel = repository.getModel(BuiltinModels.NO_CONSTANT_ID, true);
				addSubModel(initModel, noConstantModel, noConstantName, 0.23, 0.31, 0.18, 0.10);
			}

			Path hideLinkTarget = displayElementPath.getCopy();
			hideLinkTarget.addSegment(hiddenElementName);
			hideLinkTarget.addSegment(DisplayModelWrapper.VISIBLE_RESERVED_ROLE);

			addLink(initModel, hideLinkSource, hideLinkTarget);
		}
	}

	private void autoManageKeyField(FlowModel creationModel, Role dataElementRole,
			TableFieldDescriptor autoPrimaryKey)
	{
		Path keyPath = null;
		if (autoPrimaryKey != null)
		{
			keyPath = new Path(dataElementRole);
			keyPath.addSegment(autoPrimaryKey.getName());
		}
		boolean alreadyOK = false;

		// Delete the previous creation of the primary key (if redundant)
		List<ModelElement> elements = creationModel.getElements();
		for (int e = 0; e < elements.size(); e++)
			if (elements.get(e) instanceof Link)
			{
				Link link = (Link) elements.get(e);
				if (link.getTarget().beginsWith(new Path(dataElementRole))
						&& (link.getTarget().getNumberOfSegments() > 1)) // Target is a field of the record
				{
					ModelElement source = creationModel.getElement(link.getSource());
					if (alreadyOK = link.getTarget().equals(keyPath)) // Specifically, if no primary key is defined now, 'keyPath' is null, so 'alreadyOK' will be 'false'
					{
						if (autoPrimaryKey.isAutoIncrementPrimaryKey())
							alreadyOK = (source instanceof DataElement); // Constant (see below)
						else if (autoPrimaryKey.isModelGeneratedPrimaryKey())
							alreadyOK = (source instanceof Slot); // Exit of a generator process (see below)
					}
					if (!alreadyOK)
					{
						deleteElement(creationModel, link.getElementName(), false, false, true);
						if (source instanceof DataElement)
							deleteElement(creationModel, source.getElementName(), false, false, true); // Deleting the constant
						else
							deleteElement(creationModel, link.getSource().getPrefix(1).toString(),
									false, true, true); // Deleting the generator process
					}
					break;
				}
			}

		// Add an automatic creation of the primary key (if missing)
		if ((autoPrimaryKey != null) && autoPrimaryKey.isAutoPrimaryKey() && !alreadyOK)
		{
			if (autoPrimaryKey.isAutoIncrementPrimaryKey())
			{
				// Clear the field (will be automatically created by the RDBMS)
				Model yesConstantModel = repository.getModel(BuiltinModels.YES_CONSTANT_ID, true);
				String yesConstantName = BuiltinModels.YES_CONSTANT_ID.getName(); // Compare with BooleanHandler.YES_LITTERAL
				addSubModel(creationModel, yesConstantModel, yesConstantName, 0.1, 0.9, 0.14,	0.08);
				Link link = addLink(creationModel, new Path(yesConstantName), keyPath);
				setProperty(link, BuiltinProperties.OPERATION, LinkOperation.REMOVE);
			}
			else if (autoPrimaryKey.isModelGeneratedPrimaryKey())
			{
				// Set the field to the output of a sequence number or timestamp generator
				boolean isDateAndTimeField = BuiltinModels.DATE_AND_TIME_ID.equals(autoPrimaryKey
						.getDataType());
				ModelId generatorTemplateId = (isDateAndTimeField ? TIMESTAMP_GENERATOR_ID
						: SEQUENCE_NUMBER_GENERATOR_ID);
				String generatorName = generatorTemplateId.getName();
				ModelId generatorId = new ModelId(creationModel.getPackageId(), generatorName);
				FlowModel generator = (FlowModel) createModelFromTemplateOrPrototype(
						generatorTemplateId, generatorId, false);
				if (!isDateAndTimeField)
				{
					String databaseTableName = (String) creationModel.getElement(dataElementRole)
							.getReferredModel().getProperty(BuiltinProperties.TABLE_NAME);
					Path relativeKeyPath = keyPath.getTail(keyPath.getNumberOfSegments() - 1);
					setProperty(generator, "Key" /* SequenceNumber.KEY_PROPERTY */,
							databaseTableName + "_" + Misc.sqlize(relativeKeyPath.toString()));
				}
				addSubModel(creationModel, generator, generatorName, 0.17, 0.75);
				String GeneratorExitName = (isDateAndTimeField ? "<Now>" /* Now.OUTPUT_ROLE */
															   : "<Next>" /* SequenceNumber.OUTPUT_ROLE */);
				addLink(creationModel, new Path(generatorName + "/" + GeneratorExitName), keyPath);
			}
		}
	}

	private void replaceLinksWithFilteringActions(FlowModel extractModel, Role displayElementRole,
			Role dataElementRole)
	{
		// List links between the input display model and the output data model (as candidates for
		// replacement)
		List<Link> extractLinks = extractModel.getLinksBetween(new Path(displayElementRole),
				new Path(dataElementRole));

		// Replace each link from a display element marked STARTS_WITH or CONTAINS
		int n = extractLinks.size();
		int i = 0;
		if (extractLinks != null)
			for (Link link : extractLinks)
			{
				// Get the display element that is the link's source
				boolean isLeaf = true; // TODO Properly set for non-leaf elements of the data
										// structure (compare with the constructor of the class)
				Path displayElementPath = (isLeaf ? link.getSource().getPrefix(
						link.getSource().getNumberOfSegments() - 1) : link.getSource());
				SubFlow displayElement = (SubFlow) extractModel.getElement(displayElementPath);
				int filtering = Filtering.getFilteringIntValue((String) displayElement
						.getProperty(BuiltinProperties.FILTERING));

				// Replace the direct link with an action if the field has STARTS_WITH or CONTAINS
				// filtering
				if (filtering == Filtering.STARTS_WITH || filtering == Filtering.CONTAINS)
				{
					String extractPrototype = (String) extractModel
							.getProperty(BuiltinProperties.PROTOTYPE);
					Model extractTemplate = repository
							.getModel(new ModelId(extractPrototype), true);
					String actionModelTemplate = (filtering == Filtering.STARTS_WITH ? "Create Starts With Pattern"
							: "Create Contains Pattern");
					ModelElement templateElement = extractTemplate.getElement(Role
							.get(actionModelTemplate));
					FlowModel ActionModel = (FlowModel) templateElement.getReferredModel();

					// Create a sub-model
					double size = 1. / (n + 2);
					double y = (0.33 + size / 2) + i * (0.6 / n);
					ModelElement action = addSubModel(extractModel, ActionModel,
							actionModelTemplate + " for " + link.getTarget().getLastSegment(), 0.5,
							y, size, size);

					// Replace the direct link with two links (one to the trigger of the sub-model,
					// one from its exit)
					Slot exit = ActionModel.getAllNonDistinguishedExits().iterator().next();
					addLink(extractModel, new Path(action.getRole() + "/" + exit.getRole()), link
							.getTarget());

					Slot trigger = ActionModel.getAllNonDistinguishedTriggers().iterator().next();
					setProperty(link, BuiltinProperties.TARGET, action.getRole() + "/"
							+ trigger.getRole());
				}

				i++;
			}
	}

	private void replaceFilteringActionsWithLinks(FlowModel extractModel, Role displayElementRole,
			Role dataElementRole)
	{
		// List all chains of:
		// (1) A Link from the input display model; and
		// (2) A "Create Starts With Pattern" or "Create Contains Pattern" action (with a single
		// trigger and a single exit); and
		// (3) A link to the output data model
		List<SubFlow> flteringActions = new ArrayList<SubFlow>();
		for (ModelElement element : extractModel.getElements())
		{
			ModelId actionModelId = element.getRefId();
			if (element instanceof SubFlow
					&& ("Create Starts With Pattern".equals(actionModelId.getName()) || "Create Contains Pattern"
							.equals(actionModelId.getName())))
			{
				flteringActions.add((SubFlow) element);
			}
		}

		// Replace each such chain with a direct link between the source of the first link and the
		// target of the second
		for (SubFlow action : flteringActions)
		{
			// (1) There is a single link from the input display model to the action
			List<Link> linksToAction = extractModel.getLinksBetween(new Path(displayElementRole),
					new Path(action.getRole()));
			if (linksToAction.size() != 1)
				continue;

			// (2) Action is with a single trigger and a single exit
			FlowModel ActionModel = (FlowModel) action.getReferredModel();
			if (ActionModel.getAllNonDistinguishedTriggers().size() != 1
					|| ActionModel.getAllNonDistinguishedExits().size() != 1)
				continue;

			// (3) There is a single link from the action to the output data model
			List<Link> linksFromAction = extractModel.getLinksBetween(new Path(action.getRole()),
					new Path(dataElementRole));
			if (linksFromAction.size() != 1)
				continue;

			// Delete the sub-model
			deleteElement(extractModel, action.getRole().toString(), false, false, true);

			// Merge the links
			Link link1 = linksToAction.iterator().next();
			Link link2 = linksFromAction.iterator().next();
			setProperty(link1, BuiltinProperties.TARGET, link2.getTarget());
			deleteElement(extractModel, link2.getRole().toString(), false, false, true);
		}
	}

	private static final String OPTIONS = "<Options>"; // Compare with '100-GenericField.js'
	private static final String EMPTY_OPTION_TEXT = "<Empty Option Text>"; // Compare with tersus.GenericField.EMPTY_OPTION_TEXT in '100-GenericField.js'

	private void updateFieldTypeIfUserDefined(ModelElement displayElement, TableFieldDescriptor f, Model validValuesStructure)
	{
		// Set type of <Value>
		Model displayModel = displayElement.getReferredModel();
		Role valueRole = DisplayModelWrapper.correspondingLeafRole((FlowModel) displayModel);
		ModelElement valueElement = displayModel.getElement(valueRole);
		ModelId valueModelId = valueElement.getModelId();
		if (!valueModelId.equals(f.getDataType()))
			setProperty(valueElement, BuiltinProperties.REF_ID, f.getDataType());

		// For choosers
		if (displayModel.getPlugin().equals(repository.getModel(CHOOSER_TEMPLATE_ID, true).getPlugin()))
		{
			// Set type of <Options>
			Role optionsRole = Role.get(OPTIONS);
			ModelElement optionsElement = displayModel.getElement(optionsRole);
			ModelId optionsModelId = optionsElement.getModelId();
			if (!optionsModelId.equals(f.getDataType()))
				setProperty(optionsElement, BuiltinProperties.REF_ID, f.getDataType());

			// Ensure we have the element for specifying whether an empty option is allowed
			Role emptyOptionTextRole = Role.get(EMPTY_OPTION_TEXT);
			ModelElement emptyOptionTextElement = displayModel.getElement(emptyOptionTextRole);
			if (emptyOptionTextElement == null)
				emptyOptionTextElement = addSubModel(displayModel, repository.getModel(
						BuiltinModels.TEXT_ID, true), EMPTY_OPTION_TEXT, 0.5, 0.85, 0.42, 0.175);

			// Create an early initialization process that retrieves values from the global valid values structure
			FlowModel initModel = null;
			ModelElement initElement = displayModel.getElement(BuiltinElements.EARLY_INIT);
			if (initElement == null)
			{
				ModelId initModelId = new ModelId(displayModel.getPackageId(), "Init " + f.getName() + " Chooser");
				addNewModel(initModelId, BuiltinModels.ACTION_TEMPLATE_ID, false);
				initModel = (FlowModel) repository.getModel(initModelId, true);
				initElement = addSubModel(displayModel, initModel, BuiltinElements.EARLY_INIT.toString(), 0.86, 0.86, 0.19, 0.29);
			}
			else
				initModel = (FlowModel) initElement.getReferredModel();

			// Add a parent reference to the global valid values structure
			String structureName = validValuesStructure.getName();
			Role structureRole = Role.get(structureName);
			ModelElement validValuesStructureReference = initModel.getElement(structureRole);
			if (validValuesStructureReference == null)
				validValuesStructureReference = addSubModel(initModel, validValuesStructure, structureName, 0.28, 0.5, 0.2, 0.9);
			setProperty(validValuesStructureReference, BuiltinProperties.TYPE, FlowDataElementType.PARENT);

			// Add a parent reference to the chooser
			Role chooserRole = Role.get(displayModel.getName());
			ModelElement chooserReference = initModel.getElement(chooserRole);
			if (chooserReference == null)
				chooserReference = addSubModel(initModel, displayModel, displayModel.getName(),
						0.8, 0.5, 0.25, 0.5);
			setProperty(chooserReference, BuiltinProperties.TYPE, FlowDataElementType.PARENT);

			// Create a flow from the global valid values structure to the chooser
			DataElement typeValues = (DataElement) validValuesStructure.getElementByReferredModel(f.getDataType());
			Path source = new Path(validValuesStructureReference.getRole() + "/" + typeValues.getRole());
			Path target = new Path(chooserReference.getRole() + "/" + optionsRole);
			if (initModel.getLinkBetween(source, target) == null)
				addLink(initModel, source, target);

			// Specify whether an empty option is allowed
			boolean allowEmptyOption = true; // In the future this may depend on whether the field is mandatory and/or other considerations
			String emptyOptionText = "\"\""; // In the future this may change
			String emptyOptionTextConstantName = "Empty " + f.getName() + " Text";
			ModelElement emptyOptionTextConstantElement = initModel.getElement(Role.get(emptyOptionTextConstantName));
			if (emptyOptionTextConstantElement == null)
			{
				ModelId emptyOptionTextConstantModelId = new ModelId(initModel.getPackageId(),emptyOptionTextConstantName);
				addConstantModel(emptyOptionTextConstantModelId, BuiltinModels.TEXT_CONSTANT_ID,emptyOptionText);
				DataType emptyOptionTextConstantModel = (DataType) repository.getModel(emptyOptionTextConstantModelId, true);
				emptyOptionTextConstantElement = addSubModel(initModel, emptyOptionTextConstantModel, emptyOptionTextConstantName, 0.5, 0.75, 0.15, 0.1);
			}
			source = new Path(emptyOptionTextConstantName);
			target = new Path(chooserReference.getRole() + "/" + EMPTY_OPTION_TEXT);
			Link emptyOptionTextSetting = initModel.getLinkBetween(source, target);
			if (emptyOptionTextSetting == null)
				emptyOptionTextSetting = addLink(initModel, source, target);
			setProperty(emptyOptionTextSetting, BuiltinProperties.OPERATION,
					allowEmptyOption ? LinkOperation.REPLACE : LinkOperation.REMOVE);
		}
	}

	protected List<Path> getDisplayFields(FlowModel populateModel, Role dataElementRole,
			Path displayElementPath)
	{
		// List the display fields managed by the wizard (relative paths in respect to a top display
		// element containing them)
		// (given on a transformation model that populates all of them)

		List<Path> fieldRelativePaths = new ArrayList<Path>();

		Path sourcePrefix = new Path(dataElementRole);
		int targetPrefixLength = displayElementPath.getNumberOfSegments();
		for (Link link : populateModel.getLinksBetween(sourcePrefix, displayElementPath))
		{
			Path relativePath = link.getTarget().getTail(
					link.getTarget().getNumberOfSegments() - targetPrefixLength);
			relativePath.removeLastSegment(); // We want the display element, not is
												// VALUE_RESERVED_ROLE or
												// VALUE_RESERVED_ROLE_DATE_AND_TIME child
			fieldRelativePaths.add(relativePath);
		}

		return fieldRelativePaths;
	}

	protected void updateFieldVisibilities(FlowModel visibilityInitModel, Role displayElementRole,
			List<Path> fieldRelativePaths, boolean rangeFieldsOnly)
	{
		// Ensure we have 'Yes' and 'No' constants
		String yesConstantName = BuiltinModels.YES_CONSTANT_ID.getName(); // Compare with BooleanHandler.YES_LITTERAL
		Role yesConstantRole = Role.get(yesConstantName);
		ModelElement yesConstant = visibilityInitModel.getElement(yesConstantRole);
		Path yesConstantPath = new Path(yesConstantRole);
		if (yesConstant == null)
		{
			Model yesConstantModel = repository.getModel(BuiltinModels.YES_CONSTANT_ID, true);
			addSubModel(visibilityInitModel, yesConstantModel, yesConstantName, 0.2, 0.5, 0.25,
					0.05);
		}

		String noConstantName = BuiltinModels.NO_CONSTANT_ID.getName(); // Compare with BooleanHandler.NO_LITTERAL
		Role noConstantRole = Role.get(noConstantName);
		ModelElement noConstant = visibilityInitModel.getElement(noConstantRole);
		Path noConstantPath = new Path(noConstantRole);
		if (noConstant == null)
		{
			Model noConstantModel = repository.getModel(BuiltinModels.NO_CONSTANT_ID, true);
			addSubModel(visibilityInitModel, noConstantModel, noConstantName, 0.2, 0.75, 0.25, 0.05);
		}

		// Set the visibility of each field to either 'True' or 'False'
		FlowModel displyModel = (FlowModel) visibilityInitModel.getElement(displayElementRole)
				.getReferredModel();
		for (Path relativePath : fieldRelativePaths)
		{
			SubFlow field = (SubFlow) displyModel.getElement(relativePath);
			int filtering = Filtering.getFilteringIntValue((String) field
					.getProperty(BuiltinProperties.FILTERING));
			boolean visible = (rangeFieldsOnly ? filtering == Filtering.RANGE
					: filtering != Filtering.NO_FILTERING);

			FlowModel fieldModel = (FlowModel) field.getReferredModel();
			if (fieldModel.getElement(DisplayModelWrapper.VISIBLE_RESERVED_ROLE) == null)
			{
				Model booleanModel = repository.getModel(BuiltinModels.BOOLEAN_ID, true);
				addSubModel(fieldModel, booleanModel, DisplayModelWrapper.VISIBLE_RESERVED_ROLE
						.toString(), 0.5, 0.08, 0.25, 0.08);
			}

			Path targetPath = new Path(displayElementRole);
			targetPath.append(relativePath);
			targetPath.addSegment(DisplayModelWrapper.VISIBLE_RESERVED_ROLE);
			Link yesLink = visibilityInitModel.getLinkBetween(yesConstantPath, targetPath);
			Link noLink = visibilityInitModel.getLinkBetween(noConstantPath, targetPath);

			if (visible)
			{
				if (noLink != null)
					deleteElement(visibilityInitModel, noLink.getRole().toString(), false, false, true);
				if (yesLink == null)
					addLink(visibilityInitModel, yesConstantPath, targetPath);
			}
			else
			{
				if (yesLink != null)
					deleteElement(visibilityInitModel, yesLink.getRole().toString(), false, false, true);
				if (noLink == null)
					addLink(visibilityInitModel, noConstantPath, targetPath);
			}
		}
	}

	private static final String TYPE_INITIATOR_MODEL_NAME = "Get Valid Values";
	private static final String TYPE_INITIATOR_EXIT_NAME = "Valid Values";

	private void addOrUpdateValidValuesModeling(TableFieldDescriptor f, PackageId ParentPackageId, Model validValuesStructure)
	{
		// Case 1 - No valid values
		if (f.getValidValues() == null)
		{
			// Remain with the generic data type (which has no valid values)
		}

		// Case 2 - Valid values for a data type with the same plugin as the initial user defined data type
		else if (f.getInitialValidValues() != null
				&& f.hasSamePluginAs(repository, f.getInitialDataType()))
		{
			// Revert to the initial user defined data type
			f.setDataType(f.getInitialDataType());

			// If the valid values have changed, update the data type model accordingly
			if (!f.getValidValues().equals(f.getInitialValidValues()))
			{
				setProperty(f.getDataType(), BuiltinProperties.VALID_VALUES, f.getValidValues());
				// Later will also update constants in the proper package
			}
		}

		// Case 3 - Valid values for a data type with a new plugin (or if initially was a generic data type)
		else
		{
			// Create a new user defined data type model (in a package of its own)
			String newTypeName = f.getName();
			PackageId newTypePackageId = new PackageId(ParentPackageId, newTypeName);
			if (repository.getPackage(newTypePackageId) == null)
				createNewPackage(newTypePackageId);

			ModelId newTypeModelId = new ModelId(newTypePackageId, newTypeName);
			if (repository.getModel(newTypeModelId, true) != null)
				deleteModel(repository.getModel(newTypeModelId, true));
			createModelFromTemplateOrPrototype(f.getGenericDataType(repository), newTypeModelId, false);
			setProperty(newTypeModelId, BuiltinProperties.VALID_VALUES, f.getValidValues());
			// Later will also create constants in the proper package

			f.setDataType(newTypeModelId);
		}

		// If there are valid values - enrich the model with relevant constants, data structures and initialization processes
		if (f.getValidValues() != null)
		{
			ValidValues validvalues = new ValidValues(f.getValidValues());
			DataType dataModel = (DataType) repository.getModel(f.getDataType(), true);
			PackageId packageId = dataModel.getPackageId();

			// Delete modeling of previous initialization process */
			ModelId initiatorModelId = new ModelId(packageId, TYPE_INITIATOR_MODEL_NAME);
			FlowModel initiator = (FlowModel) repository.getModel(initiatorModelId, true);
			if (initiator != null)
				deleteFlowsAndSubModels(initiator); // Also deleting non-constant referred models
			// We don't delete the initialization process itself and we don't get here for data
			// types no longer used (will it cause problems if redundant?)

			// Delete previous constants (if used and won't be recreated - we'll get broken
			// references)
			Package existingPackage = repository.getPackage(packageId);
			if (existingPackage != null)
				deleteModels(existingPackage, true);

			ModelId constantType = f.getGenericConstantType(repository);

			// For a list of values - create a constant for each value
			if (validvalues.isValueList() && constantType != null)
			{
				for (int i = 0; i < validvalues.getValueList().length; i++)
				{
					String value = validvalues.getValueList()[i];
					ModelId valueModelId = new ModelId(packageId, value);
					addConstantModel(valueModelId, constantType, value);
				}
			}

			// For a range - create constants for the minimal and maximal values
			if (validvalues.isRange() && constantType != null)
			{
				ModelId valueModelId = new ModelId(packageId, "Min Value");
				addConstantModel(valueModelId, constantType, validvalues.getMinValue());
				valueModelId = new ModelId(packageId, "Max Value");
				addConstantModel(valueModelId, constantType, validvalues.getMaxValue());
			}

			// For a foreign key or a list of values - create a repetitive data element in the global valid values structure
			DataElement values = null;
			if (validvalues.isForeignKey() || validvalues.isValueList())
			{
				values = (DataElement) validValuesStructure.getElementByReferredModel(f.getDataType());
				if (values == null)
				{
					values = (DataElement) addSubModel(validValuesStructure, dataModel,	f.getName(), 0, 0);
					setProperty(values, BuiltinProperties.REPETITIVE, true);
				}
			}

			// Create a process to initialize the valid values at runtime */
			if (validvalues.isForeignKey() || ((validvalues.isValueList() || validvalues.isRange()) && constantType != null))
			{
				if (initiator == null)
					initiator = (FlowModel) createModelFromTemplateOrPrototype(
							BuiltinModels.ACTION_TEMPLATE_ID, initiatorModelId, false);

				Role exitRole = Role.get(TYPE_INITIATOR_EXIT_NAME);
				Slot exit = (Slot) initiator.getElement(exitRole);
				if (exit == null)
					exit = addSlot(initiator, SlotType.EXIT, exitRole, f.getDataType(), true, true,
							new RelativePosition(1, 0.5), true);
				else if (!f.getDataType().equals(exit.getDataTypeId()))
					setProperty(exit, BuiltinProperties.DATA_TYPE_ID, f.getDataType());
				setProperty(exit, BuiltinProperties.DATA_TYPE_SETTING,
						Slot.DATA_TYPE_SETTING_EXPLICIT);

				// TODO If the field isn't mandatory, we should allow for an empty value

				// For a foreign key - retrieve the values from the database
				if (validvalues.isForeignKey())
				{
					String tableTriggerName = "Table";
					String columnTriggerName = "Column";

					ModelId queryTempalteModelId = new ModelId(packageId, "Valid Values Retrieval Query");
					addConstantModel(queryTempalteModelId, BuiltinModels.TEXT_CONSTANT_ID, "select distinct ${" + columnTriggerName + "} from ${" + tableTriggerName + "} order by ${" + columnTriggerName + "}");
					DataElement queryTemplate = (DataElement) addSubModel(initiator, repository.getModel(queryTempalteModelId, true), queryTempalteModelId.getName(), 0.35, 0.15, 0.6, 0.06);

					ModelId tableNameModelId = new ModelId(packageId, "Table Name");
					addConstantModel(tableNameModelId, BuiltinModels.TEXT_CONSTANT_ID, validvalues.getTableName());
					DataElement tableName = (DataElement) addSubModel(initiator, repository.getModel(tableNameModelId, true), tableNameModelId.getName(), 0.15, 0.45, 0.25, 0.06);

					ModelId columnNameModelId = new ModelId(packageId, "Column Name");
					addConstantModel(columnNameModelId, BuiltinModels.TEXT_CONSTANT_ID, validvalues.getColumnName());
					DataElement columnName = (DataElement) addSubModel(initiator, repository.getModel(columnNameModelId, true), columnNameModelId.getName(), 0.15, 0.6, 0.25, 0.06);

					ModelId createQueryModelId = new ModelId(packageId, "Create Query from Template");
					FlowModel createQueryModel = (FlowModel) createModelFromTemplateOrPrototype(BuiltinModels.CREATE_FROM_TEMPLATE_ID, createQueryModelId, false);
					SubFlow createQuery = (SubFlow) addSubModel(initiator, createQueryModel, createQueryModel.getName(), 0.5, 0.5, 0.22, 0.22);

					Role TEMPLATE = Role.get("<Template>");	// Same as tersus.plugins.misc.CreateFromTemplate.TEMPLATE
					Role TEXT = Role.get("<Text>");			// Same as tersus.plugins.misc.CreateFromTemplate.TEXT
					addLink(initiator, new Path(queryTemplate.getRole()), new Path(createQuery.getRole() + "/" + TEMPLATE));

					addSlot(createQueryModel, SlotType.TRIGGER, Role.get(tableTriggerName), BuiltinModels.TEXT_CONSTANT_ID, false, true, new RelativePosition(0,0.3333), true);
					addLink(initiator, new Path(tableName.getRole()), new Path(createQuery.getRole() + "/" + tableTriggerName));

					addSlot(createQueryModel, SlotType.TRIGGER, Role.get(columnTriggerName), BuiltinModels.TEXT_CONSTANT_ID, false, true, new RelativePosition(0,0.6667), true);
					addLink(initiator, new Path(columnName.getRole()), new Path(createQuery.getRole() + "/" + columnTriggerName));

					ModelId performQueryModelId = new ModelId(packageId, "Perform Query");
					FlowModel performQueryModel = (FlowModel) createModelFromTemplateOrPrototype(BuiltinModels.DATABASE_QUERY_ID, performQueryModelId, false);
					deleteNonDistinguishedSlots(performQueryModel);
					SubFlow performQuery = (SubFlow) addSubModel(initiator, performQueryModel, performQueryModel.getName(), 0.8, 0.65, 0.22, 0.22);

					Role SQL_TRIGGER_ROLE = Role.get("<SQL Statement>");	// Same as tersus.plugins.database.DatabaseQuery.SQL_TRIGGER_ROLE
					Role RESULTS_EXIT_ROLE = Role.get("<Results>");			// Same as tersus.plugins.database.DatabaseQuery.RESULTS_EXIT_ROLE
					addLink(initiator, new Path(createQuery.getRole() + "/" + TEXT), new Path(performQuery.getRole() + "/" + SQL_TRIGGER_ROLE));
					addLink(initiator, new Path(performQuery.getRole() + "/" + RESULTS_EXIT_ROLE), new Path(exitRole));
				}

				// For a list of values - return the constants in the order they have been listed by the user
				else if (validvalues.isValueList())
				{
					for (int i = 0; i < validvalues.getValueList().length; i++)
					{
						String value = validvalues.getValueList()[i];
						ModelId valueModelId = new ModelId(packageId, value);
						addSubModel(initiator, repository.getModel(valueModelId,true), value, 0.5,	(i + 1.) / (validvalues.getValueList().length + 1.), 0.3, 0.07);
						addLink (initiator, new Path(Role.get(value)), new Path(exitRole));
					}
				}
				
				// For a range - return the values in ascending order
				else if (validvalues.isRange() && constantType != null)
				{
					// Case 1: Range of integers with increment 1
					if (BuiltinPlugins.NUMBER_CONSTANT.equals(repository.getModel(constantType,true).getPlugin()))
					{
						String from = "From";
						ModelId valueModelId = new ModelId(packageId, "Min Value");
						addSubModel(initiator, repository.getModel(valueModelId,true), from, 0.2, 0.4, 0.3, 0.07);

						String to = "To";
						valueModelId = new ModelId(packageId, "Max Value");
						addSubModel(initiator, repository.getModel(valueModelId,true), to, 0.2, 0.6, 0.3, 0.07);

						ModelId rangeModelId = new ModelId(packageId, "Range");
						FlowModel rangeModel = (FlowModel) createModelFromTemplateOrPrototype(BuiltinModels.RANGE_ID, rangeModelId, false);
						SubFlow range = (SubFlow) addSubModel(initiator, rangeModel, rangeModelId.getName(), 0.65, 0.5, 0.22, 0.22);

						addLink (initiator, new Path(from), new Path(range.getRole() + "/" + "<Limit 1>"));		// Same as tersus.plugins.collections.Range.LIMIT1_ROLE
						addLink (initiator, new Path(to), new Path(range.getRole() + "/" + "<Limit 2>"));		// Same as tersus.plugins.collections.Range.LIMIT2_ROLE
						addLink (initiator, new Path(range.getRole() + "/" + "<Numbers>"), new Path(exitRole));	// Same as tersus.plugins.collections.Range.OUTPUT_ROLE
					}

					// TODO Case 2: Range of non-integers or increment different from 1
					
					// TODO case 3: Range of dates
				}
			}
		}
	}

	private void addOrUpdateValidValuesInit(TableFieldDescriptor f)
	{
		ValidValues validvalues = new ValidValues(f.getValidValues());
		ModelId constantType = f.getGenericConstantType(repository);
		if (!validvalues.isForeignKey() && !(validvalues.isValueList() && constantType != null))
			return; // Compare with the conditions for creating a initialization process in 'addOrUpdateValidValuesModeling'

		Model validValuesStructure = repository.getModel(getValidValuesStructureId(f.getDataType()), true);

		// In each view containing the data type, use its initialization process
		// to set the corresponding values in the global valid values structure
		RepositoryIndex index = repository.getUpdatedRepositoryIndex();
		List<ModelId> usages = index.getAncestorModels(f.getDataType(), null);
		for (int i = 0; i < usages.size(); i++)
		{
			ModelId ancestorModelId = usages.get(i);
			Model ancestorModel = repository.getModel(ancestorModelId, true);
			if (BuiltinPlugins.VIEW.equals(ancestorModel.getPlugin()) ||
				BuiltinPlugins.MOBILE_VIEW.equals(ancestorModel.getPlugin()) ||
				BuiltinPlugins.NEW_DESKTOP_VIEW.equals(ancestorModel.getPlugin()) ||
				BuiltinPlugins.NEW_TABLET_VIEW.equals(ancestorModel.getPlugin()) ||
				BuiltinPlugins.NEW_MOBILE_VIEW.equals(ancestorModel.getPlugin()))
			{
				// Have the global valid values structure as an element of the view ...
				ModelElement validValuesStructureElement = ancestorModel.getElementByReferredModel(validValuesStructure.getId());
				if (validValuesStructureElement == null)
					validValuesStructureElement = addSubModel(ancestorModel, validValuesStructure, validValuesStructure.getName(), 0.9, 0.15, 0.14, 0.28);

				// ... and create an early initialization process (one per view)
				boolean isPseudoInit = false;
				ModelElement initElement = null;
				FlowModel initModel = null;
				initElement = ancestorModel.getElement(BuiltinElements.DONT_RUN);
				if (!(isPseudoInit = (initElement != null)))
					initElement = ancestorModel.getElement(BuiltinElements.EARLY_INIT);
				if (initElement == null)
				{
					ModelId initModelId = new ModelId(ancestorModel.getPackageId(),BuiltinElements.EARLY_INIT.toString());
					addNewModel(initModelId, BuiltinModels.ACTION_TEMPLATE_ID, false);
					initModel = (FlowModel) repository.getModel(initModelId, true);
					initElement = addSubModel(ancestorModel, initModel, BuiltinElements.EARLY_INIT.toString(), 0.75, 0.08, 0.14, 0.14);
				}
				else
					initModel = (FlowModel) initElement.getReferredModel();

				// In the initialization process - create an initialization service ...
				String initServiceName = "Get Valid Values";
				FlowModel initServiceModel = null;
				ModelElement initServiceElement = initModel.getElement(Role.get(initServiceName));
				if (initServiceElement == null)
				{
					ModelId initServiceModelId = new ModelId(ancestorModel.getPackageId(), initServiceName);
					addNewModel(initServiceModelId, BuiltinModels.SERVICE_TEMPLATE_ID, false);
					initServiceModel = (FlowModel) repository.getModel(initServiceModelId, true);
					initServiceElement = addSubModel(initModel, initServiceModel, initServiceName, 0.3, 0.45, 0.35, 0.35);
				}
				else
					initServiceModel = (FlowModel) initServiceElement.getReferredModel();

				String structureName = validValuesStructure.getName();
				Role structureRole = Role.get(structureName);
				ModelElement structureElement = initServiceModel.getElement(structureRole);
				if (structureElement == null)
					structureElement = addSubModel(initServiceModel, validValuesStructure,
							structureName, 0.85, 0.5, 0.25, 0.95);

				String initExitName = "Valid Values";
				Role initExitRole = Role.get(initExitName);
				Slot initServiceExit = (Slot) initServiceModel.getElement(initExitRole);
				if (initServiceExit == null)
					initServiceExit = addSlot(initServiceModel, SlotType.EXIT, initExitRole,
							validValuesStructure.getModelId(), false, true, new RelativePosition(1,
									0.25), true);

				Path source = new Path(structureName);
				Path target = new Path(initExitName);
				if (initServiceModel.getLinkBetween(source, target) == null)
					addLink(initServiceModel, source, target);

				if (!isPseudoInit)
				{
					// ... An exit to return the initialized global valid values structure ...
					Slot initExit = (Slot) initModel.getElement(initExitRole);
					if (initExit == null)
						initExit = addSlot(initModel, SlotType.EXIT, initExitRole, validValuesStructure.getModelId(), false, true, new RelativePosition(1, 0.5), true);
	
					// ... and a link between the two
					source = new Path(initServiceName + "/" + initExitName);
					target = new Path(initExitName);
					if (initModel.getLinkBetween(source, target) == null)
						addLink(initModel, source, target);
	
					// Also link the exit of the early initialization process to the global valid values structure
					source = new Path(BuiltinElements.EARLY_INIT + "/" + initExitName);
					target = new Path(structureName);
					if (ancestorModel.getLinkBetween(source, target) == null)
						addLink((FlowModel) ancestorModel, source, target);
				}

				// Ensure the initialization service has the initialization sub-process for the data type
				DataElement typeValues = (DataElement) validValuesStructure
						.getElementByReferredModel(f.getDataType());
				target = new Path(structureName + "/" + typeValues.getRole());
				if (initServiceModel.getLinkTo(target) == null)
				{
					int typeIndex = validValuesStructure.indexOf(typeValues);
					int numberOfTypesInStructure = validValuesStructure.getElements().size();

					double x = 0.4;
					double y = 0.05 + (typeIndex + 1) * 0.9 / (numberOfTypesInStructure + 1); // TODO As types are added, re-arranging will be required

					double sizeFactor = 1 + 1.5 / numberOfTypesInStructure; // TODO As types are added, re-sizing will be required
					double width = 0.1 * sizeFactor;
					double height = 0.4 * sizeFactor;

					PackageId packageId = f.getDataType().getPackageId();
					ModelId initiatorModelId = new ModelId(packageId, TYPE_INITIATOR_MODEL_NAME);
					FlowModel initiator = (FlowModel) repository.getModel(initiatorModelId, true);
					String typeDependentName = Misc.sqlize(f.getDataType().getPath());
					ModelElement typeInit = addSubModel(initServiceModel, initiator,
							typeDependentName, x, y, width, height);

					Role exitRole = Role.get(TYPE_INITIATOR_EXIT_NAME);
					source = new Path(typeInit.getRole() + "/" + exitRole);
					addLink(initServiceModel, source, target);
				}
			}
		}
	}

	private void deleteModels(Package existingPackage, boolean deleteOnlyConatants)
	{
		List<Model> models = existingPackage.getModels();
		if (models != null)
		{
			for (int j = models.size() - 1; j >= 0; j--)
			{
				Model model = models.get(j);
				if (!deleteOnlyConatants
						|| (model instanceof DataType && ((DataType) model).isConstant()))
					deleteModel(model);
			}
		}
	}

	private void deleteFlowsAndSubModels(Model model) // Also deleting non-constant referred models
	{
		// First deleting flows (without deleting referred models)
		List<ModelElement> elements = model.getElements();
		if (elements != null)
		{
			for (int j = elements.size() - 1; j >= 0; j--)
			{
				ModelElement element = elements.get(j);
				if (element instanceof Link)
					deleteElement(model, element.getElementName(), false, false, true);
			}
		}

		// Then sub-models (including non-constant referred models)
		elements = model.getElements();
		if (elements != null)
		{
			for (int j = elements.size() - 1; j >= 0; j--)
			{
				ModelElement element = elements.get(j);
				if (element instanceof SubFlow || element instanceof DataElement)
				{
					Model referredModel = element.getReferredModel();
					boolean isConstant = (referredModel != null
							&& referredModel instanceof DataType && ((DataType) referredModel)
							.isConstant());
					deleteElement(model, element.getElementName(), false, referredModel != null
							&& !isConstant, true);
				}
			}
		}
	}

	private void deleteNonDistinguishedSlots(Model model)
	{
		List<ModelElement> elements = model.getElements();
		if (elements != null)
		{
			for (int j = elements.size() - 1; j >= 0; j--)
			{
				ModelElement element = elements.get(j);
				if (element instanceof Slot && !element.getRole().isDistinguished())
					deleteElement(model, element.getElementName(), false, false, true);
			}
		}
	}

	private void deleteEarlyInitOfChooser(Model chooserModel)
	{
		SubFlow initElement = (SubFlow) chooserModel.getElement(BuiltinElements.EARLY_INIT);
		if (initElement != null)
		{
			Model localInitModel = initElement.getModel();
			List<ModelElement> elements = localInitModel.getElements();
			for (int e = elements.size() - 1; e >= 0; e--)
			{
				ModelElement child = elements.get(e);
				Model childModel = child.getReferredModel();
				if (childModel != null && childModel instanceof DataType && ((DataType) childModel).isConstant() && localInitModel.getPackageId().equals(childModel.getModelId().getPackageId()))
					deleteElement(localInitModel, child.getRole().toString(), false, true, true);
			}
			deleteElement(chooserModel, initElement.getRole().toString(), false, true, true);
		}
	}
	
	public class TableConfiguration
	{
		private boolean defaultOptimization;
		private int numberOfPagesToDisplay;
		private int numberOfRecordsPerPage;
		private boolean showPagingHeaderGroup;
		private boolean showActionHeaderGroup;
		private boolean showCountFooterRow;

		public TableConfiguration(boolean defaultOptimization, int numberOfPagesToDisplay,
				int numberOfRecordsPerPage, boolean showPagingHeaderGroup,
				boolean showActionHeaderGroup, boolean showCountFooterRow)
		{
			this.defaultOptimization = defaultOptimization;
			this.numberOfPagesToDisplay = numberOfPagesToDisplay;
			this.numberOfRecordsPerPage = numberOfRecordsPerPage;
			this.showPagingHeaderGroup = showPagingHeaderGroup;
			this.showActionHeaderGroup = showActionHeaderGroup;
			this.showCountFooterRow = showCountFooterRow;
		}

		public boolean defaultOptimization()
		{
			return defaultOptimization;
		}

		public void setDefaultOptimization(boolean defaultOptimization)
		{
			this.defaultOptimization = defaultOptimization;
		}

		public int numberOfPagesToDisplay()
		{
			return numberOfPagesToDisplay;
		}

		public void setNumberOfPagesToDisplay(int numberOfPagesToDisplay)
		{
			this.numberOfPagesToDisplay = numberOfPagesToDisplay;
		}

		public int numberOfRecordsPerPage()
		{
			return numberOfRecordsPerPage;
		}

		public void setNumberOfRecordsPerPage(int numberOfRecordsPerPage)
		{
			this.numberOfRecordsPerPage = numberOfRecordsPerPage;
		}

		public boolean showPagingHeaderGroup()
		{
			return showPagingHeaderGroup;
		}

		public void setShowPagingHeaderGroup(boolean showPagingHeaderGroup)
		{
			this.showPagingHeaderGroup = showPagingHeaderGroup;
		}

		public boolean showActionHeaderGroup()
		{
			return showActionHeaderGroup;
		}

		public void setShowActionHeaderGroup(boolean showActionHeaderGroup)
		{
			this.showActionHeaderGroup = showActionHeaderGroup;
		}

		public boolean showCountFooterRow()
		{
			return showCountFooterRow;
		}

		public void setShowCountFooterRow(boolean showCountFooterRow)
		{
			this.showCountFooterRow = showCountFooterRow;
		}
	}
}
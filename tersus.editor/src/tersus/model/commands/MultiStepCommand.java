/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model.commands;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;

import tersus.editor.actions.RenamePackageCommand;
import tersus.editor.layout.LayoutConstraint;
import tersus.model.BuiltinModels;
import tersus.model.BuiltinProperties;
import tersus.model.DataType;
import tersus.model.FlowModel;
import tersus.model.InvalidLinkException;
import tersus.model.Link;
import tersus.model.LinkOperation;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.ModelObject;
import tersus.model.PackageId;
import tersus.model.Path;
import tersus.model.RelativePosition;
import tersus.model.Role;
import tersus.model.Slot;
import tersus.model.SlotType;
import tersus.model.SubFlow;
import tersus.model.TemplateAndPrototypeSupport;
import tersus.model.indexer.RepositoryIndex;
import tersus.util.Misc;
import tersus.workbench.WorkspaceRepository;

/**
 * Skeleton for commands composed of multiple model modification sub-commands (a simpler alternative
 * for org.eclipse.gef.commands.CompoundCommand).
 * 
 * @author Ofer Brandes
 * @author Liat Shiff
 */

public abstract class MultiStepCommand extends Command
{
	protected WorkspaceRepository repository;

	protected ArrayList<Command> history;

	public MultiStepCommand(WorkspaceRepository repository)
	{
		super();
		// super(label);
		this.repository = repository;
	}

	public MultiStepCommand(WorkspaceRepository repository, String lable)
	{
		super();
		setLabel(lable);
		this.repository = repository;
	}

	public boolean modelChanged()
	{
		return history.size() > 0;
	}

	private boolean alreadyRun;

	public boolean canExecute()
	{
		return true;
	}

	public final void execute()
	{
		if (!alreadyRun)
		{
			alreadyRun = true;

			if (history == null)
				history = new ArrayList<Command>();
			else
				history.clear();

			try
			{
				run();
			}
			catch (CommandFailedException e)
			{
				undo();
				throw e;
			}
			catch (InvalidLinkException e)
			{
				undo();
				throw e;
			}
		}
	}

	protected abstract void run();

	protected void runCommand(Command command)
	{
		command.execute();
		history.add(command);
	}

	public void undo()
	{
		for (int i = history.size() - 1; i >= 0; i--)
		{
			Command command = (Command) history.get(i);
			command.undo();
		}
	}

	public void redo()
	{
		for (int i = 0; i < history.size(); i++)
		{
			Command command = (Command) history.get(i);
			command.redo();
		}
	}

	protected void partialUndo(int leftSize)
	{
		for (int last = history.size() - 1; last >= leftSize; last--)
		{
			Command command = (Command) history.get(last);
			command.undo();
			history.remove(last);
		}
	}

	public void reset()
	{
		alreadyRun = false;

		if (history != null)
			history.clear();
	}

	// /////////////////////////////////////////////////////////////

	protected void changeModelId(Model model, String newName)
	{
		ChangeModelIdCommand c = new ChangeModelIdCommand(model, newName);
		runCommand(c);
	}

	protected void changeModelId(Model model, ModelId newModelId)
	{
		ChangeModelIdCommand c = new ChangeModelIdCommand(model, newModelId);
		runCommand(c);
	}

	protected void renameElement(ModelElement element, String newName)
	{
		ChangeElementNameCommand c = new ChangeElementNameCommand(element, Role.get(newName));
		runCommand(c);
	}

	protected ModelElement addSubModel(Model model, Model child, String elementName, double x,
			double y)
	{
		return addSubModel(model, child, elementName, x, y, 0, 0);
	}

	protected ModelElement addSubModel(Model model, Model child, String elementName, double x,
			double y, double width, double height)
	{
		LayoutConstraint constraint = x == 0 ? null : new LayoutConstraint(x, y, (width == 0 ? 0.2
				: width), (height == 0 ? 0.8 : height));

		AddElementAndPropagateSlotTypeCommand c = AddElementHelper.getAddElementCommand(model,
				child.getId(), elementName, constraint, null);
		runCommand(c);

		return c.getNewElement();
	}

	protected ModelElement addElement(Model parentModel, ModelElement newElement,
			LayoutConstraint constraint, String elementName, Integer index,
			boolean propagateSlotTypeOn)
	{
		AddElementAndPropagateSlotTypeCommand c = new AddElementAndPropagateSlotTypeCommand(
				parentModel, newElement, propagateSlotTypeOn);

		if (constraint != null)
			c.setConstraint(constraint);

		if (elementName != null)
			c.setSuggestedName(elementName);

		if (index != null)
			c.setIndex(index.intValue());

		c.execute();
		history.add(c);

		return c.getNewElement();
	}

	protected Slot addSlot(FlowModel model, SlotType slotType, Role role, ModelId slotDataType,
			boolean isRepetitive, boolean isMandatory, RelativePosition position,
			boolean propagateSlotTypeOn)
	{
		AddElementAndPropagateSlotTypeCommand c = createAddSlotCommand(model, slotType, role,
				slotDataType, isRepetitive, isMandatory, position);
		c.setPropagateSlotTypeOn(propagateSlotTypeOn);

		runCommand(c);

		return (Slot) c.getNewElement();
	}

	public static AddElementAndPropagateSlotTypeCommand createAddSlotCommand(FlowModel parentModel,
			SlotType slotType, Role role, ModelId slotDataType, boolean isRepetitive,
			boolean isMandatory, RelativePosition position)
	{
		Slot slot = new Slot();
		slot.setType(slotType);

		if (slotDataType != null)
			slot.setDataTypeId(slotDataType);

		if (SlotType.ERROR == slotType)
		{
			slot.setDataTypeId(BuiltinModels.ERROR_ID);
			slot.setDataTypeSetting(Slot.DATA_TYPE_SETTING_EXPLICIT);
		}
		else
			slot.setDataTypeSetting(Slot.DATA_TYPE_SETTING_INFERRED);

		if (role == null)
			slot.setRole(parentModel.defaultRole(slotType.toString()));
		else
			slot.setRole(role);

		slot.setPosition(position);

		if (isRepetitive)
			slot.setRepetitive(isRepetitive);

		if (isMandatory)
			slot.setMandatory(isMandatory);

		AddElementAndPropagateSlotTypeCommand command = new AddElementAndPropagateSlotTypeCommand(
				parentModel, slot, true);
		return command;
	}

	protected void createNewPackage(PackageId packageId)
	{
		NewPackageCommand c = new NewPackageCommand((WorkspaceRepository) repository, packageId);
		runCommand(c);
	}

	protected void removeElement(ModelElement element, boolean usePropagateSlotType)
	{
		RemoveElementCommand c = new RemoveElementCommand(element, usePropagateSlotType);
		runCommand(c);
	}

	protected void deleteElement(Model model, String elementName, boolean deleteLinks,
			boolean deleteModel, boolean usePropagateSlotType)
	{
		// Waring: If 'deleteModel' is specified, and the model is reused somewhere else,
		// we'll create a broken reference problem (this is not validated in purpose).
		// The only valid case is if 'deleteLinks' is also specified and there are no other
		// references to the model.

		ModelElement element = model.getElement(Role.get(elementName));
		Model referredModel = element.getReferredModel();

		// Delete all links from the element or to it (anywhere in the whole repository)
		if (deleteLinks)
		{
			RepositoryIndex index = repository.getUpdatedRepositoryIndex();
			for (Link link : index.getOutgoingLinks(element, false))
			{
				RemoveElementCommand c1 = new RemoveElementCommand(link, usePropagateSlotType);
				runCommand(c1);
			}
			for (Link link : index.getIncomingLinks(element, false))
			{
				RemoveElementCommand c2 = new RemoveElementCommand(link, usePropagateSlotType);
				runCommand(c2);
			}
		}

		// Delete the element itself
		RemoveElementCommand c3 = new RemoveElementCommand(element, usePropagateSlotType);
		runCommand(c3);

		// Also delete the model to which the element refers
		if (deleteModel)
		{
			DeleteModelAndPackageCommand c4 = new DeleteModelAndPackageCommand(referredModel);
			runCommand(c4);
		}
	}

	protected void deleteModel(Model model)
	{
		DeleteModelCommand c = new DeleteModelCommand(model);
		runCommand(c);
	}

	protected ModelObject setProperty(ModelObject model, String property, Object newValue)
	{
		CompoundCommand commands;
		SetPropertyValueCommand c1 = null;

		if (!(model.getProperty(property) != null && model.getProperty(property).equals(newValue)))
		{
			c1 = new SetPropertyValueCommand(model, property, newValue);
			commands = new CompoundCommand();
			commands.add(c1);

			CompoundCommand changeIgnoredProblemsPropertyCommands = ClearIgnoredProblemHelper
					.getNewIgnoredProblemValue(property, model, repository);
			
			if (changeIgnoredProblemsPropertyCommands != null)
				commands.add(changeIgnoredProblemsPropertyCommands);

			runCommand(commands);
		}

		if (c1 != null)
			return c1.getChangedModelObject();
		else
			return null;
	}

	protected Object setProperty(ModelId modelId, String property, Object newValue)
	{
		SetPropertyValueCommand c = new SetPropertyValueCommand(repository, modelId, property,
				newValue);
		runCommand(c);

		return newValue;
	}

	protected Boolean setBooleanProperty(ModelElement element, String property, Boolean value)
	{
		if (!Misc.equal(element.getProperty(property), value))
		{
			SetPropertyValueCommand c = new SetPropertyValueCommand(element, property, value);
			runCommand(c);
		}
		return value;
	}

	
 	protected Link addLink(FlowModel model, Path source, Path target)
 	{
 		return addLink(model, source, target, null);
 	}
 	protected Link addLink(FlowModel model, Path source, Path target, LinkOperation operation)
	{
		// TODO - eliminate compound command (also check other methods in this class)
		CompoundCommand cmd = new CompoundCommand();

		Link link = new Link();
		AddLinkCommand addLinkCommand = new AddLinkCommand(link, repository);

		addLinkCommand.setSource(source);
		addLinkCommand.setTarget(target);
		addLinkCommand.setParentModel(model);
		if (operation != null)
			addLinkCommand.setOperation(operation);
		cmd.add(addLinkCommand);
		ModelElement sourceElement = model.getElement(source);
		ModelElement targetElement = model.getElement(target);
		if (sourceElement instanceof Slot)
			cmd.add(new PropagateSlotTypeCommand((Slot) sourceElement));
		else if (targetElement instanceof Slot)
			cmd.add(new PropagateSlotTypeCommand((Slot) targetElement));
		runCommand(cmd);

		return (Link) (addLinkCommand.getNewElement());
	}

	protected PackageId addNewPackage(PackageId newPackageId)
	{
		NewPackageCommand c = new NewPackageCommand((WorkspaceRepository) repository, newPackageId);
		runCommand(c);

		return newPackageId;
	}

	protected void renamePackage(PackageId packageId, String newName)
	{
		RenamePackageCommand command = new RenamePackageCommand(repository, packageId, newName);
		runCommand(command);
	}

	protected void deletePackage(PackageId packageId)
	{
		DeletePackageCommand command = new DeletePackageCommand(repository, packageId);
		runCommand(command);
	}

	/**
	 * @deprecated Use either createModelFromTemplate or copyModel (think which one is appropriate)
	 */
	protected ModelId addNewModel(ModelId newModelId, ModelId templateId, Boolean createElements)
	{
		NewModelCommand newModelCommand = new NewModelCommand(repository, newModelId, templateId,
				createElements);
		runCommand(newModelCommand);

		return newModelId;
	}

	protected ModelId addNewModel(ModelId newModelId, Class<? extends Model> modelClass,
			Map properties)
	{
		NewModelCommand newModelCommand = new NewModelCommand(repository, newModelId, modelClass,
				properties);
		runCommand(newModelCommand);
		return newModelId;
	}

	protected ModelId createModelFromTemplate(ModelId newModelId, ModelId templateId,
			Boolean createElements)
	{
		addNewModel(newModelId, templateId, false);

		return newModelId;
	}

	protected ModelId copyModel(ModelId newModelId, ModelId sourceId, Boolean createElements)
	{
		addNewModel(newModelId, sourceId, false);

		Model template = repository.getModel(sourceId, true);
		Model newModel = repository.getModel(newModelId, true);

		newModel.setProperty(BuiltinProperties.TEMPLATE,
				template.getProperty(BuiltinProperties.TEMPLATE));

		return newModelId;
	}

	protected ModelId addConstantModel(ModelId newConstantId, ModelId templateConstantId,
			String constantValue)
	{
		addNewModel(newConstantId, templateConstantId, false);
		DataType constant = (DataType) repository.getModel(newConstantId, true);
		setProperty(constant, BuiltinProperties.CONSTANT, true);
		setProperty(constant, BuiltinProperties.VALUE, constantValue);
		return newConstantId;
	}

	protected void addMetaProperty(ModelObject obj, String propertyName)
	{
		AddMetaPropertyCommand c = new AddMetaPropertyCommand(obj, propertyName, true, true);
		runCommand(c);
	}

	// MultiStep commands
	protected void propagateSlotType(ModelElement element)
	{
		if (!(element instanceof Slot || element instanceof Link))
		{
			HashSet<ModelElement> elementsToPropagate = new HashSet<ModelElement>();
			RepositoryIndex index = repository.getUpdatedRepositoryIndex();

			for (Link link : index.getIncomingLinks(element, false))
			{
				ModelElement source = link.getParentModel().getElement(link.getSource());

				if (source != null && !elementsToPropagate.contains(source))
					elementsToPropagate.add(source);
			}

			for (Link link : index.getOutgoingLinks(element, false))
			{
				ModelElement target = link.getParentModel().getElement(link.getTarget());

				if (target != null && !elementsToPropagate.contains(target))
					elementsToPropagate.add(target);
			}

			for (ModelElement e : elementsToPropagate)
			{
				PropagateSlotTypeCommand c = new PropagateSlotTypeCommand(e);
				runCommand(c);
			}
		}
		else
		{
			PropagateSlotTypeCommand c = new PropagateSlotTypeCommand(element);
			runCommand(c);
		}

	}

	protected void moveElement(ModelElement e, ModelElement newParent)
	{
		Command c = new MoveElementCommand(e, newParent);
		runCommand(c);
	}

	protected ModelElement createElementFromTemplate(Model templateModel, Model parentModel,
			LayoutConstraint constraint, String elementName, boolean doPropagateSlotType)
	{
		CreateElementFromTemplateCommand createCommand = AddElementHelper
				.getCreateElementFromTemplateCommand(parentModel, templateModel, elementName,
						constraint, null);
		createCommand.setDoPropagateSlotType(doPropagateSlotType);

		runCommand(createCommand);

		return createCommand.getCreatedElement();
	}

	protected Model createModelFromTemplate(ModelId templateId, ModelId modelId)
	{
		CopyModelCommand c = new CopyModelCommand(repository.getModel(templateId, true), modelId);
		runCommand(c);

		return c.getNewModel();
	}

	protected void createModelFromPrototype(Model templateModel, ModelId newModelId)
	{
		CreateModelFromPrototypeCommand c = new CreateModelFromPrototypeCommand(templateModel,
				newModelId);
		runCommand(c);
	}

	protected Model createModelFromTemplateOrPrototype(ModelId templateId, ModelId newModelId, boolean addTdWrapper)
	{
		Model templateModel = repository.getModel(templateId, true);
		Model newModel = null;

		if (TemplateAndPrototypeSupport.isPrototype(templateModel))
		{
			createModelFromPrototype(templateModel, newModelId);
			newModel = repository.getModel(newModelId, true);
		}
		else
			newModel = createModelFromTemplate(templateId, newModelId);

		if (addTdWrapper) // Ofer, 21/12/2021 (for the new table wizard)
		{
			ModelId HTML_TAG_ID = new ModelId("Common/Templates/UI/HTML Tag/HTML Tag"); // Should go to BuiltinModels?
			PackageId targetPackageId = newModelId.getPackageId();
			Model tdModel = null;
			ModelId tagModelId = repository.getNewModelId(targetPackageId, newModelId.getName().concat(" Td"));
			templateModel = repository.getModel(HTML_TAG_ID, true);
			createModelFromPrototype(templateModel, tagModelId);
			tdModel = repository.getModel(tagModelId, true);
			setProperty(tdModel, BuiltinProperties.HTML_TAG, "td");
			addSubModel(tdModel, newModel, newModelId.getName(), 0.5, 0.5, 0.8, 0.8);
			return tdModel;
		}
		else
			return newModel;
	}

	protected ModelId getValidValuesStructureId(ModelId modelId) // Get the global data structure
																	// for valid values of user
																	// defined data types
	{
		PackageId rootPackageId = modelId.getPackageId();
		while (rootPackageId.getParent() != null)
			rootPackageId = rootPackageId.getParent();

		ModelId validValuesStructureId = new ModelId(rootPackageId, "<Valid Values>");
		if (repository.getModel(validValuesStructureId, true) == null)
			addNewModel(validValuesStructureId, BuiltinModels.DATA_STRUCTURE_TEMPLATE_ID, false);

		return validValuesStructureId;
	}
}
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/

package tersus.model.commands;

import tersus.editor.layout.LayoutConstraint;
import tersus.model.DataElement;
import tersus.model.FlowDataElementType;
import tersus.model.FlowModel;
import tersus.model.ModelElement;
import tersus.model.ModelUtils;
import tersus.model.RelativePosition;
import tersus.model.RelativeSize;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 * @author Liat Shiff
 */
public class AddAncestorReferenceCommand extends AddElementAndPropagateSlotTypeCommand
{

	public AddAncestorReferenceCommand(FlowModel parentModel, FlowModel childModel)
	{
		super((WorkspaceRepository) parentModel.getRepository());
		setParentModel(parentModel);

		LayoutConstraint constraint = new LayoutConstraint();
		constraint.setPosition(new RelativePosition(0.15, 0.3));
		constraint.setSize(new RelativeSize(0.25, 0.5));

		ModelElement newElement =  AddElementHelper.createDataElement(parentModel,
				childModel.getId(), childModel.getName(), constraint);
		newElement.setPosition(ModelUtils.findNonOverlappingPosition(parentModel, newElement));
		setNewElement(newElement);

		((DataElement) getNewElement()).setType(FlowDataElementType.PARENT);
	}
}

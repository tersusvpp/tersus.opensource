/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model.commands;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import tersus.editor.explorer.ExplorerEntry;
import tersus.editor.explorer.PackageEntry;
import tersus.editor.explorer.ProjectEntry;
import tersus.model.BuiltinProperties;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.PackageId;
import tersus.model.Repository;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public class PastePackageCommand extends MultiStepCommand
{
	private ExplorerEntry targetEntry;

	private PackageEntry packageToPaste;

	Map<ModelId, ModelId> copies;

	public PastePackageCommand(WorkspaceRepository repository, ExplorerEntry targetEntry,
			PackageEntry packageToPaste)
	{
		super(repository);

		this.targetEntry = targetEntry;
		this.packageToPaste = packageToPaste;
		copies = new HashMap<ModelId, ModelId>();
	}

	public void run()
	{
		if (targetEntry instanceof ProjectEntry)
			addPackageToNewParent(null, packageToPaste.getPackageId());
		else if (targetEntry instanceof PackageEntry)
			addPackageToNewParent(((PackageEntry) targetEntry).getPackageId(), packageToPaste
					.getPackageId());

		for (ModelId originalModelId : copies.keySet())
		{
			createCopy(originalModelId, copies.get(originalModelId));
		}
	}

	private void addPackageToNewParent(PackageId parent, PackageId packageToAdd)
	{
		PackageId newPackageId = addPackage(parent, packageToAdd);

		tersus.model.Package pkg = repository.getPackage(packageToAdd);
		for (Model model : pkg.getModels())
		{
			addModel(newPackageId, model);
		}

		for (PackageId subPackageId : repository.getSubPackageIds(packageToAdd))
		{
			addPackageToNewParent(newPackageId,subPackageId);
		}
	}


	private PackageId addPackage(PackageId parent, PackageId packageToAdd)
	{
		String newPackageName = null;
		HashSet<String> subPackageNames = getAllSubPackageNames(parent);

		newPackageName = packageToAdd.getName();
		while (true)
		{

			if (subPackageNames.contains(newPackageName))
			{
				String tempName = newPackageName;
				newPackageName = "Copy of " + tempName;
			}
			else
				break;
		}
		PackageId newPackageId = new PackageId(parent, newPackageName);

		return addNewPackage(newPackageId);
	}

	private HashSet<String> getAllSubPackageNames(PackageId parent)
	{
		HashSet<String> names = new HashSet<String>();
		for (PackageId subPackageId : repository.getSubPackageIds(parent))
		{
			names.add(subPackageId.getName());
		}

		return names;
	}

	private ModelId addModel(PackageId parent, Model model)
	{
		ModelId newModelId = new ModelId(parent, model.getName());
		copies.put(model.getModelId(), newModelId);

		copyModel(newModelId, model.getId(), false);

		return newModelId;
	}

	private void createCopy(ModelId originalModelId, ModelId copyModelId)
	{
		Model templateModel = repository.getModel(originalModelId, true);
		Model newParentModel = repository.getModel(copyModelId, true);

		for (ModelElement e : templateModel.getElements())
		{
			ModelId refId = getCloneModelId(e.getRefId());
			ModelElement newElement = Repository.copyElement(e);

			addElement(newParentModel, newElement, null, null, null, false);

			if (refId != null)
				setProperty(newElement, BuiltinProperties.REF_ID, refId);
		}
	}

	private ModelId getCloneModelId(ModelId refId)
	{
		for (ModelId originalModelId : copies.keySet())
		{
			if (originalModelId.equals(refId))
				return copies.get(originalModelId);
		}

		return null;
	}
}

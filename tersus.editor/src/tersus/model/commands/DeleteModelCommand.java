/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model.commands;

import org.eclipse.gef.commands.Command;

import tersus.model.Model;
import tersus.model.Package;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 */
public class DeleteModelCommand extends Command
{
	private int indexInPackage;

	private Model modelToDelete;

	public DeleteModelCommand(Model model)
	{
		super("Delete " + model.getId().getPath());
		modelToDelete = model;
	}

	public void execute()
	{
		Package pkg = modelToDelete.getPackage();

		if (modelToDelete.isReadOnly())
			throw new CommandFailedException("Cannot delete read only Model: "
					+ modelToDelete.getModelId());

		indexInPackage = pkg.getModels().indexOf(modelToDelete);
		WorkspaceRepository repository = (WorkspaceRepository) modelToDelete.getRepository();
		repository.removeModel(modelToDelete);
	}

	public void undo()
	{
		((WorkspaceRepository) modelToDelete.getRepository()).addModel(modelToDelete,
				indexInPackage);
	}
}

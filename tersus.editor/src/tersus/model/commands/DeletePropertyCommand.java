/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model.commands;

import org.eclipse.gef.commands.Command;

import tersus.editor.properties.ModelObjectWrapper;
import tersus.model.Model;
import tersus.model.ModelElement;

/**
 * Deletes the MetaProperty and property value (undo restores both)
 * 
 * @author Liat Shiff
 */
public class DeletePropertyCommand extends Command
{
	private ModelRef modelRef;

	private ElementRef elementRef;

	private String commandFaildMessage;

	protected String propertyName;

	protected String undoProperyName;

	protected Object undoModelPropertyValue;

	protected Object undoElementProperyValue;

	public DeletePropertyCommand(ModelObjectWrapper modelObjectWrapper, String propertyName)
	{
		Model model = modelObjectWrapper.getModel();
		ModelElement element = modelObjectWrapper.getElement();

		if (model != null)
			modelRef = new ModelRef(model);

		if (element != null)
			elementRef = new ElementRef(element);

		this.propertyName = propertyName;
	}

	public DeletePropertyCommand(Model model, String propertyName)
	{
		if (model != null)
			modelRef = new ModelRef(model);

		this.propertyName = propertyName;
	}

	public DeletePropertyCommand(ModelElement element, String propertyName)
	{
		if (element != null)
			elementRef = new ElementRef(element);

		this.propertyName = propertyName;
	}

	private boolean isPropertyReadOnly()
	{
		if (modelRef != null && ((Model) modelRef.getModelObject()).isReadOnly())
		{
			commandFaildMessage = "The property '" + propertyName + "' can not be deleted. \n\n"
					+ "The reference: '" + ((Model) modelRef.getModelObject()).getModelId()
					+ "' is read only.";
			return true;
		}

		if (elementRef != null
				&& ((ModelElement) elementRef.getModelObject()).getParentModel().isReadOnly())
		{
			commandFaildMessage = "The property '" + propertyName + "' can not be deleted.\n\n '"
					+ "The reference: '"
					+ ((ModelElement) elementRef.getModelObject()).getParentModel().getModelId()
					+ "' is read only.";
			return true;
		}

		return false;
	}

	public boolean canExecute()
	{
		return true;
	}

	public void execute()
	{
		if (isPropertyReadOnly())
			throw new CommandFailedException(commandFaildMessage);

		undoProperyName = propertyName;

		if (modelRef != null)
		{
			Model model = (Model) modelRef.getModelObject();

			undoModelPropertyValue = model.getProperty(propertyName);

			model.removeProperty(propertyName);
			model.removeMetaProperty(propertyName);
		}
		else
			undoModelPropertyValue = "";

		if (undoModelPropertyValue == null)
			undoModelPropertyValue = "";

		if (elementRef != null)
		{
			ModelElement element = (ModelElement) elementRef.getModelObject();

			undoElementProperyValue = element.getProperty(propertyName);

			element.removeProperty(propertyName);
			element.removeMetaProperty(propertyName);
		}
		else
			undoElementProperyValue = "";

		if (undoElementProperyValue == null)
			undoElementProperyValue = "";
	}

	public void undo()
	{
		Model model = null;
		if (modelRef != null)
		{
			model = (Model) modelRef.getModelObject();
			model.setProperty(propertyName, undoModelPropertyValue);
		}

		ModelElement element = null;
		if (elementRef != null)
		{
			element = (ModelElement) elementRef.getModelObject();
			element.setProperty(propertyName, undoElementProperyValue);
		}
	}

}

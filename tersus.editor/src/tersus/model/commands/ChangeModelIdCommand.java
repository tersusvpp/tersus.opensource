/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model.commands;

import java.util.List;

import org.eclipse.gef.commands.Command;

import tersus.editor.EditorMessages;
import tersus.editor.Errors;
import tersus.editor.actions.ValidatingCommand;
import tersus.model.BuiltinProperties;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.Path;
import tersus.model.Repository;
import tersus.workbench.WorkspaceRepository;

/**
 * Renaming a model in the repository.
 * 
 * @author Ofer Brandes
 */
public class ChangeModelIdCommand extends Command implements ValidatingCommand
{
	private String errorMessage;
	Model model = null;
	ModelId oldModelId = null;
	ModelId newModelId = null;
	Repository repository = null;

	/**
	 * @param model -The renamed model.
	 * @param newModelId -The new id of the model (in the same repository).
	 */
	public ChangeModelIdCommand(Model model, String newName)
	{
		super();

		if (newName == null || newName.length() == 0)
			errorMessage = EditorMessages.Error_Model_Name_Not_Specified;
		if (newName.indexOf(Path.SEPARATOR) >= 0)
			errorMessage = EditorMessages.Error_Invalid_Model_Name;
		else if (newName.equals(model.getName()))
			errorMessage = EditorMessages.Error_Same_Name;
		else
		{
			this.repository = ((WorkspaceRepository) model.getRepository());
			this.model = model;
			this.oldModelId = model.getId();
			this.newModelId = new ModelId(model.getId().getPackageId(), newName);

		}
	}

	public ChangeModelIdCommand(Model model, ModelId newModelId)
	{
		this.repository = ((WorkspaceRepository) model.getRepository());
		this.model = model;
		this.oldModelId = model.getId();
		this.newModelId = newModelId;
	}

	public boolean canExecute()
	{
		return validate() == null && !(newModelId.equals(oldModelId))
				&& !(model.getRepository().getModel(newModelId, true) != null);
	}

	public void execute()
	{
		// Ensure renaming makes sense and is legal
		if (newModelId.equals(oldModelId))
			throw new CommandFailedException("New modelId equals to old modelId.");
		if (model.getRepository().getModel(newModelId, true) != null)
			throw new CommandFailedException("There is an existing model with this id.");

		renameRelevantModels(oldModelId, newModelId);
	}

	private void renameRelevantModels(ModelId oldModelId, ModelId newModelId)
	{
		List<ModelElement> references = ((WorkspaceRepository) repository)
				.getReferences(oldModelId);

		repository.renameModel(repository.getModel(oldModelId, true), newModelId, -1);

		for (ModelElement ref : references)
		{
			ref.setProperty(BuiltinProperties.REF_ID, newModelId);
		}
	}
	
	public boolean canUndo()
	{
		return (oldModelId != null && newModelId != null);
	}

	public void undo()
	{
		renameRelevantModels(newModelId, oldModelId);
	}

	public void redo()
	{
		renameRelevantModels(oldModelId, newModelId);
	}

	public String validate()
	{
		if (errorMessage != null)
			return errorMessage;
		if (model == null)
			return "No model specified";
		if (newModelId == null)
			return "New model name not specified";
		if (model.getRepository().getModel(newModelId, true) != null)
			return EditorMessages.format(EditorMessages.Error_Model_Exists_Key, newModelId);
		if (!newModelId.getName().trim().equals(newModelId.getName()))
			return Errors.LEADING_OR_TRAINLING_BLANKS;
		return null;
	}
}
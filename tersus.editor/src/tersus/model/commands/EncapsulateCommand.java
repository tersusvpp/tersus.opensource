/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
package tersus.model.commands;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import org.eclipse.core.runtime.AssertionFailedException;

import tersus.editor.layout.LayoutConstraint;
import tersus.model.BuiltinModels;
import tersus.model.BuiltinProperties;
import tersus.model.DataElement;
import tersus.model.DataType;
import tersus.model.FlowModel;
import tersus.model.Link;
import tersus.model.LinkOperation;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.ModelObject;
import tersus.model.ModelUtils;
import tersus.model.Path;
import tersus.model.RelativePosition;
import tersus.model.Repository;
import tersus.model.Role;
import tersus.model.Slot;
import tersus.model.SlotType;
import tersus.model.utils.MoveElementsUtils;
import tersus.util.Misc;
import tersus.util.MultiMap;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 * @author Youval Bronicki
 * 
 * 
 * 
 * 
 * <h2>Logic for handling links (flows) when encapsulating into a process:</h2>
 * 
 * <p>Links between non-encapsulated elements are left as is</p>
 * <p>Links between encapsulated elements are moved to the new process ("newParent")</p>
 * <p>Links between encapsulated and non-encapsulated are classified as either incoming or outgoing</p>
 * 
 * <p>Incoming and outgoing links are replaced by triggers and exits (respectively) of the new process, with new links both inside and outside the new process.</p>
 * <p>Naively (original implementation), each incoming/outgoing link is replaced by an "outer" link, a slot (trigger/exit), and an "inner" link.
 * However, there are many cases where we want to unify slots and links:</p>
 * <ul>
 *    <li>If there are multiple sources going to the same target we want to have a single slot, with multiple links going from the sources to the slot and
 *     a single link to the target</li>
 *    <li>If there's a single slot going to multiple targets, we want to have a single slot, with one link from the source to the slot and multiple links
 *     from the slot the targets</li>
 *    <li>A natural generalization: if there are multiple (N) sources going to the same set of (M) targets (NxM original links), we want to create a new
 *     slot, with N links from the sources to the slot and M links from the slot to the targets</li>
 * </ul>   
 * 
 * <p>To complicate things further, "remove" flows should be handled differently than regular flows:  When grouping links by target,
 *  we consider targets of "remove" flows as different than targets of regular flows.
 * (we need to keep a distinction in order to decide whether to remove or to replace/update). </p>
 * 
 * <p>The algorithm is therefore the following:</p>
 * <ul>
 *  <li>Group links (incoming/outgoing) by source</li>
 *  <li>Group sources by targets</li>
 *  <li>For each group of sources, create a slot and the corresponding flows</li>
 *  <li>A slot is created as repetitive if any of the targets repetitive</li>
 *  <li>Slots are created with no type - The type is inferred at the end through regular type propagation</li>
 * </ul>
 * <h2>Mandatoriness</h2>
 * <p>It's difficult to tell exactly when new slots can be mandatory.  The process currently sets slots as mandatory in the 
 * special obvious case where all encapsulated elements are either actions with all mandatory triggers, or constants with incoming links.</p>
 */
public class EncapsulateCommand extends MultiStepCommand
{
	private Model parentModel;

	private List<ModelElement> encapsulatedElements;

	public EncapsulateCommand(Model parentModel, List<ModelElement> selectedElements)
	{
		super((WorkspaceRepository) parentModel.getRepository(), "Encapsulate");

		this.parentModel = parentModel;
		this.encapsulatedElements = new ArrayList<ModelElement>();
		for (ModelElement e : parentModel.getElements())
		{
			if (selectedElements.contains(e))
				this.encapsulatedElements.add(e);
		}
	}

	public void run()
	{
		Model templateModel = null;

		if (!this.encapsulatedElements.isEmpty())
		{
			List<ModelObject> sel = new ArrayList<ModelObject>();
			sel.addAll(this.encapsulatedElements);

			if (MoveElementsUtils.validateDataElementEncapsulation(sel))
			{
				templateModel = repository.getModel(BuiltinModels.DATA_STRUCTURE_TEMPLATE_ID, true);
				moveIntoDisplayOrDataElement(templateModel);
			}
			else if (MoveElementsUtils.validateDisplayElementEncapsulation(sel))
			{
				ModelId templateId = this.parentModel.getPlugin().startsWith("Tersus/Display/") ? BuiltinModels.LEGACY_PANE_TEMPLATE_ID : BuiltinModels.PANE_TEMPLATE_ID;
				templateModel = repository.getModel(templateId, true);
				moveIntoDisplayOrDataElement(templateModel);
			}
			else if (MoveElementsUtils.validateProcessEncapsulation(sel)
					|| MoveElementsUtils.validateAncestorRefernceEncapsulation(sel))
			{
				templateModel = repository.getModel(BuiltinModels.ACTION_TEMPLATE_ID, true);
				moveToProcessElement(templateModel);
			}
		}
	}

	private void moveToProcessElement(Model templateModel)
	{
		ModelElement newParent = createElementFromTemplate(templateModel, parentModel,
				calculateNewParentPosition(), null, false);

		ArrayList<ModelElement> elementsToRemove = new ArrayList<ModelElement>();
		boolean allMandatory = true;
		for (ModelElement e : encapsulatedElements)
		{
			Model model = e.getReferredModel();

			if (e instanceof DataElement )
			{
				// Non-self ready constants are like processes with mandatory triggers 
				if (!ModelUtils.isConstant(e) || e.isSelfReady())
					allMandatory = false;
			}
			else if (!ModelUtils.isSubProcess(e))
			{
				allMandatory = false;
			}
			else
			{
				FlowModel fm = ((FlowModel) model);
				if (!fm.hasTriggers())
					allMandatory = false;
				if (fm.hasNonMandatoryTriggers())
					allMandatory = false;
			}
			ModelElement newElement = Repository.copyElement(e);
			addElement(newParent.getReferredModel(), newElement, null, null, null, false);
			elementsToRemove.add(e);
		}

		ArrayList<Link> incomingLinks = new ArrayList<Link>();
		ArrayList<Link> outgoingLinks = new ArrayList<Link>();
		
		for (ModelElement e1 : parentModel.getElements())
		{
			if (!(e1 instanceof Link))
				continue;
			Link link = (Link) e1;
			boolean isTargetElementSelected = this.encapsulatedElements.contains((link).getTargetElement());
			boolean isSourceElementSelected = this.encapsulatedElements.contains((link).getSourceElement());
		
			if (!isTargetElementSelected && !isSourceElementSelected)
				continue;
			if (isTargetElementSelected && isSourceElementSelected)
			{
				// Link contained in target model
				addElement(newParent.getReferredModel(), Repository.copyElement(link), null, null,
						null, false);
			}
			else if (isTargetElementSelected)
			{
				incomingLinks.add(link);
			}
			else
			{
				outgoingLinks.add(link);
			}
			elementsToRemove.add(link);
		}
		handleIncomingLinks(incomingLinks, newParent, allMandatory);
		handleOutgoingLinks(outgoingLinks, newParent);
		for (ModelElement e: elementsToRemove)
			removeElement(e, false);

		propagateSlotType(newParent);
	}

	private void handleIncomingLinks(List<Link> incomingLinks, ModelElement newParent,
			boolean mandatory)
	{
		MultiMap<Path, Link> incomingLinksBySource = groupLinksBySource(incomingLinks);
		Collection<List<Path>> sourceGroups = groupByTargets(incomingLinksBySource);
		ArrayList<Collection<Path>> sourceGroupList = sort(sourceGroups);
		int numberOfTriggers = sourceGroupList.size();
		int triggerNum = 1;
		for (Collection<Path> sources : sourceGroupList)
		{
			boolean repetitive = checkRepetitive(sources, incomingLinksBySource);

			Slot trigger = addSlot((FlowModel) (newParent.getReferredModel()), SlotType.TRIGGER,
					Role.get("Trigger " + triggerNum), null/* data type */, repetitive, mandatory,
					new RelativePosition(0, triggerNum / (numberOfTriggers + 1.0)), false);
			Path triggerTargetPath = getOuterSlotPath(newParent, trigger);
			Path triggerSourcePath = getInnerSlotPath(trigger);
			List<Path> regularTargets = new ArrayList<Path>();
			List<Path> removeTargets = new ArrayList<Path>();
			for (Path source : sources)
			{
				addLink((FlowModel) parentModel, source, triggerTargetPath);
				for (Link l : incomingLinksBySource.get(source))
				{
					Path target = l.getTarget();
					List<Path> targetCollection = (l.getOperation() == LinkOperation.REMOVE) ? removeTargets
							: regularTargets;
					if (!targetCollection.contains(target))
						targetCollection.add(target);
				}
			}
			for (Path target : regularTargets)
				addLink((FlowModel) (newParent.getReferredModel()), triggerSourcePath, target);
			for (Path target : removeTargets)
				addLink((FlowModel) (newParent.getReferredModel()), triggerSourcePath, target,
						LinkOperation.REMOVE);
			++triggerNum;

		}
	}



	private void handleOutgoingLinks(List<Link> outgoingLinks, ModelElement newParent)
	{

		MultiMap<Path, Link> outgoingLinksBySource = groupLinksBySource(outgoingLinks);
		ArrayList<Collection<Path>> sourceGroupsList = sort(groupByTargets(outgoingLinksBySource));
		int numberOfExits = sourceGroupsList.size();
		int exitNum = 1;
		for (Collection<Path> sources : sourceGroupsList)
		{
			boolean repetitive = checkRepetitive(sources, outgoingLinksBySource);

			Slot exit = addSlot((FlowModel) (newParent.getReferredModel()), SlotType.EXIT,
					Role.get("Exit " + exitNum), null/* data type */, repetitive, false,
					new RelativePosition(1, exitNum / (numberOfExits + 1.0)), false);
			Path exitSourcePath = getOuterSlotPath(newParent, exit);
			Path exitTargetPath = getInnerSlotPath(exit);
			List<Path> regularTargets = new ArrayList<Path>();
			List<Path> removeTargets = new ArrayList<Path>();
			for (Path source : sources)
			{
				addLink((FlowModel) (newParent.getReferredModel()),
						source, exitTargetPath);
				for (Link l : outgoingLinksBySource.get(source))
				{
					Path target = l.getTarget();
					List<Path> targetCollection = (l.getOperation() == LinkOperation.REMOVE) ? removeTargets
							: regularTargets;
					if (!targetCollection.contains(target))
						targetCollection.add(target);
				}
			}
			for (Path target : regularTargets)
				addLink((FlowModel) parentModel, exitSourcePath, target);
			for (Path target : removeTargets)
				addLink((FlowModel) parentModel, exitSourcePath, target, LinkOperation.REMOVE);
			++exitNum;
		}
	}
	private Path getInnerSlotPath(Slot slot)
	{
		Path path = new Path();
		path.addSegment(slot.getRole());
		return path;
	}

	private Path getOuterSlotPath(ModelElement newParent, Slot slot)
	{
		Path path = new Path();
		path.addSegment(newParent.getRole());
		path.addSegment(slot.getRole());
		return path;
	}
	private boolean checkRepetitive(Collection<Path> sources, MultiMap<Path, Link> linksBySource)
	{
		ArrayList<Link> links = new ArrayList<Link>();
		for (Path source : sources)
			links.addAll(linksBySource.get(source));
		boolean repetitive = false;
		for (Link l : links)
		{
			if (l.isTargetRepetitive())
				repetitive = true;
		}
		return repetitive;
	}

	private Collection<List<Path>> groupByTargets(MultiMap<Path, Link> linksBySource)
	{
		MultiMap<String, Path> sourcesByTargets = new MultiMap<String, Path>();
		for (Path source : linksBySource.keySet())
		{
			HashSet<String> unorderedDescriptions = new HashSet<String>();
			for (Link l : linksBySource.get(source))
			{
				String prefix = l.getOperation() == LinkOperation.REMOVE ? "-\t" : "+\t";
				unorderedDescriptions.add(prefix + l.getTarget());
			}
			ArrayList<String> orderedDescriptions = new ArrayList<String>();
			orderedDescriptions.addAll(unorderedDescriptions);
			Collections.sort(orderedDescriptions);
			String targetsAsString = Misc.concatenateList(orderedDescriptions, "\n");
			sourcesByTargets.put(targetsAsString, source);
		}
		return sourcesByTargets.values();
	}

	private MultiMap<Path, Link> groupLinksBySource(List<Link> links)
	{
		MultiMap<Path, Link> linksBySource = new MultiMap<Path, Link>();
		for (Link link : links)
			linksBySource.put(link.getSource(), link);
		return linksBySource;
	}

	private ArrayList<Collection<Path>> sort(Collection<List<Path>> values)
	{
		ArrayList<Collection<Path>> sourceGroupList = new ArrayList<Collection<Path>>();
		sourceGroupList.addAll(values);
		Comparator<Collection<Path>> pathComparator = new Comparator<Collection<Path>>()
		{

			public int compare(Collection<Path> o1, Collection<Path> o2)
			{
				Path p1 = o1.iterator().next();
				Path p2 = o2.iterator().next();
				if (p1.equals(p2))
					return 0;
				Path commonPrefix = Path.getCommonPrefix(p1, p2);
				int segmentNum = commonPrefix.getNumberOfSegments();
				if (p1.getNumberOfSegments() == segmentNum)
					return 1;
				if (p2.getNumberOfSegments() == segmentNum)
					return -1;
				ModelElement e1 = parentModel.getElement(p1.getPrefix(segmentNum + 1));
				ModelElement e2 = parentModel.getElement(p2.getPrefix(segmentNum + 1));
				if (segmentNum == 0 || (e1 instanceof Slot && e2 instanceof Slot))
				{
					// Difference is in first element - use y position to compare
					return ((int) (1000 * e1.getPosition().getY() - 1000 * e2.getPosition().getY()));
				}
				else
				{
					return getElementIndex(e1) - getElementIndex(e2);
				}
			}
		};
		Collections.sort(sourceGroupList, pathComparator);
		return sourceGroupList;
	}

	protected int getElementIndex(ModelElement e)
	{
		int i = 0;
		for (ModelElement e0 : e.getParentModel().getElements())
		{
			if (e0 == e)
				return i;
			i = i + 1;
		}
		throw new AssertionFailedException("Element " + e
				+ " not an element of its own parent model " + e.getParentModel());
	}


	private void moveIntoDisplayOrDataElement(Model templateModel)
	{
		ModelElement newParent = null;

		if (parentModel instanceof DataType)
		{
			newParent = createElementFromTemplate(templateModel, parentModel, null, null, false);
			newParent.setProperty(BuiltinProperties.EXCLUDE_FROM_FIELD_NAME, Boolean.TRUE);
		}
		else
			newParent = createElementFromTemplate(templateModel, parentModel,
					calculateNewParentPosition(), null, false);

		for (ModelElement e : encapsulatedElements)
		{
			moveElement(e, newParent);
		}

		propagateSlotType(newParent);
	}

	

	private LayoutConstraint calculateNewParentPosition()
	{
		int elementsNumber = 0;
		double xPosition = 0;
		double yPosition = 0;

		for (ModelElement e : encapsulatedElements)
		{
			elementsNumber++;
			xPosition = xPosition + e.getPosition().getX();
			yPosition = yPosition + e.getPosition().getY();
		}

		LayoutConstraint constraint = new LayoutConstraint();
		constraint.setPosition(new RelativePosition(xPosition / elementsNumber, yPosition
				/ elementsNumber));
		constraint.setSize(0.25, 0.25);

		return constraint;
	}
}

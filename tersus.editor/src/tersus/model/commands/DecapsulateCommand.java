/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model.commands;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.eclipse.gef.commands.Command;

import tersus.model.BuiltinProperties;
import tersus.model.FlowModel;
import tersus.model.Link;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelUtils;
import tersus.model.Path;
import tersus.model.RelativePosition;
import tersus.model.RelativeSize;
import tersus.model.Repository;
import tersus.model.Role;
import tersus.model.Slot;
import tersus.model.indexer.RepositoryIndex;
import tersus.util.Misc;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public class DecapsulateCommand extends MultiStepCommand
{
	private Model newParentModel;
	
	// The element which is decapsulate
	private ModelElement selectedElement;

	// The newParent's original valid links
	private ArrayList<Link> originalNewParentValidLinks;
	
	private HashSet<Command> removeElementComamds;
	
	private HashSet<SetPropertyValueCommand> rerouteLinkCommands;
	
	private double newElementSizeHeight;
	
	private int newElementIndex;
	
	public DecapsulateCommand(Model newParentModel, ModelElement selectedElement)
	{
		super((WorkspaceRepository) newParentModel.getRepository(), "Decapsulate");
		
		this.newParentModel = newParentModel;
		this.selectedElement = selectedElement;
		this.newElementIndex = 1;
		
		initLinks();
	}
	
	protected void run()
	{
		removeElementComamds = new HashSet<Command>();
		rerouteLinkCommands = new HashSet<SetPropertyValueCommand>();
		
		List<ModelElement> elementsToMove = selectedElement.getReferredModel().getElements();
		
		if (selectedElement.getParentModel() instanceof FlowModel)
			newElementSizeHeight = selectedElement.getSize().getHeight() / elementsToMove.size();
		else
			newElementSizeHeight = 0;
		
		moveElements(elementsToMove);
		
		if (!ModelUtils.isSubProcess(selectedElement))
			rerouteLinks();
		
		for (SetPropertyValueCommand cmd : rerouteLinkCommands)
		{
			runCommand(cmd);
		}
		
		deleteElement(selectedElement);
		cleanInvalidLinks();
		
		for (Command cmd : removeElementComamds)
		{
			runCommand(cmd);
		}
	}
	
	private void moveElements(List<ModelElement> elementsToMove)
	{
		for (ModelElement element: elementsToMove)
		{
			if (!(element instanceof Slot))
			{
				if (element instanceof Link)
					{
						Link oldLink = (Link) element;
	
						ModelElement source = oldLink.getParentModel().getElement(oldLink.getSource());
						ModelElement target = oldLink.getParentModel().getElement(oldLink.getTarget());
	
						if (elementsToMove.contains(source) && source instanceof Slot)
						{
							for (Link link : originalNewParentValidLinks)
							{
								if (link.getParentModel().getElement(link.getTarget()) == source)
								{
									ModelElement newElement = Repository.copyElement(element);
									Link newLink = (Link) newElement;
									newLink.setSource(link.getSource());
									addElement(newParentModel, newElement, null, null, null, false);
								}
							}
						}
						else if (elementsToMove.contains(target) && target instanceof Slot)
						{
							for (Link link : originalNewParentValidLinks)
							{
								if (link.getParentModel().getElement(link.getSource()) == target)
								{
									ModelElement newElement = Repository.copyElement(element);
									Link newLink = (Link) newElement;
									newLink.setTarget(link.getTarget());
									addElement(newParentModel, newElement, null, null, null, false);
								}
							}
						}
						else
							addNewElement(element);
	
					}
					else
						addNewElement(element);
			}
		}
	}
	
	private void addNewElement(ModelElement element)
	{
		ModelElement newElement = Repository.copyElement(element);
		
		if (newElement.getPosition() == null)
			setNewElementPosition(newElement);
		
		if (newElement.getSize() == null)
			setNewElementSize(newElement);

		addElement(newParentModel, newElement, null, null, null, false);
	}

	private void rerouteLinks()
	{
		repository = (WorkspaceRepository) selectedElement.getRepository();
		RepositoryIndex index = repository.getUpdatedRepositoryIndex();
		
		for (Link link: index.getIncomingLinks(selectedElement, false))
		{
			Path targetPath = link.getTarget();
			Path newTarget = reroute(targetPath, link.getParentModel());
			
			if (newTarget != null)

			{
				SetPropertyValueCommand c = new SetPropertyValueCommand(link,
						BuiltinProperties.TARGET, newTarget);
				rerouteLinkCommands.add(c);
			}
		}
		
		for (Link link: index.getOutgoingLinks(selectedElement, false))
		{
			Path sourcePath = link.getSource();
			Path newSourcePath = reroute(sourcePath, link.getParentModel());
			
			if (newSourcePath != null)
			{
				SetPropertyValueCommand c = new SetPropertyValueCommand(link,
						BuiltinProperties.SOURCE, newSourcePath);
				rerouteLinkCommands.add(c);
			}
		}
	}

	private Path reroute(Path pathToChange, Model linkParent)
	{
		Path newPath = null;
		Model currentModel = linkParent;
		
		for (int i = 0; i < pathToChange.getNumberOfSegments(); i++)
		{
			Role s = pathToChange.getSegment(i);
			ModelElement e = currentModel.getElement(s);
			if (e == selectedElement && newPath == null)
				newPath = pathToChange.getPrefix(i);
			else if (newPath != null)
				newPath.addSegment(s);

			if (e == null)
				return null;
			currentModel = e.getReferredModel();
			if (currentModel == null)
				return null;
		}
		
		return newPath;
	}
			
	private ModelElement setNewElementPosition(ModelElement element)
	{
		RelativePosition selectedParentPosition = selectedElement.getPosition();

		if (selectedParentPosition != null)
		{
			element.setPosition(selectedElement.getPosition().getX(), selectedElement.getPosition()
					.getY()
					+ (newElementSizeHeight * newElementIndex));
			newElementIndex = newElementIndex + 2;
		}
		return element;
	}

	private ModelElement setNewElementSize(ModelElement element)
	{
		RelativeSize selectedParentSize = selectedElement.getSize();

		if (selectedParentSize != null)
			element.setSize(selectedParentSize.getWidth(), newElementSizeHeight);

		return element;
	}
	
	private void initLinks()
	{
		originalNewParentValidLinks = new ArrayList<Link>();
		for (ModelElement element : newParentModel.getElements())
		{
			if (element instanceof Link && ((Link) element).isValid())
				originalNewParentValidLinks.add((Link) element);
		}
	}
	
	private void deleteElement(ModelElement element)
	{
		Model currentModel = element.getParentModel();

		Boolean isRemove = false;
		for (ModelElement e : currentModel.getElements())
		{
			if (e instanceof Link)
			{
				Link link = (Link) e;
				Path sourcePath = link.getSource();
				Path targetPath = link.getTarget();

				for (int i = 0; i < sourcePath.getNumberOfSegments(); i++)
				{
					Role s = sourcePath.getSegment(i);
					ModelElement el = currentModel.getElement(s);

					if (element == el)
					{
						removeElementComamds.add(new RemoveElementCommand(e, false));
						isRemove = true;
						break;
					}
				}

				if (!isRemove)
					for (int i = 0; i < targetPath.getNumberOfSegments(); i++)
					{
						Role s = targetPath.getSegment(i);
						ModelElement el = currentModel.getElement(s);

						if (element == el)
						{
							removeElementComamds.add(new RemoveElementCommand(e, false));
							isRemove = true;
							break;
						}
					}
			}

			isRemove = false;
		}
		RemoveElementCommand removeElementCommand = new RemoveElementCommand(element, false);
		removeElementComamds.add(removeElementCommand);
	}
	
	private void cleanInvalidLinks()
	{
		for (Link link : originalNewParentValidLinks)
		{
			if (!link.isValid())
			{
				RemoveElementCommand removeElementCommand = new RemoveElementCommand(link, false);
				removeElementComamds.add(removeElementCommand);
			}
		}
	}
	
	public String validate()
	{
		Model childModel = this.selectedElement.getReferredModel();
		Model parentModel = this.newParentModel;
		ArrayList<String> errors = new ArrayList<String>();
		for (ModelElement e : childModel.getElements())
		{
			if (!(e instanceof Link) && !(e instanceof Slot))
			{
				if (parentModel.getElement(e.getRole()) != null)
					errors.add("Both models contain elements called '" + e.getRole() + "'");
			}
		}
		if (errors.size() > 0)
			return Misc.concatenateList(errors, "\n");
		else
			return null;
	}
}

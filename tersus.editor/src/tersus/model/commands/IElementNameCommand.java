package tersus.model.commands;

public interface IElementNameCommand
{

	String getSuggestedElementName();
	void setSuggestedName(String name);
	String getActualElementName();
}

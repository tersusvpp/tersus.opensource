/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/

package tersus.model.commands;

import org.eclipse.gef.commands.Command;

import tersus.editor.Errors;
import tersus.editor.actions.ValidatingCommand;
import tersus.editor.layout.LayoutConstraint;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.Role;
import tersus.model.indexer.RepositoryIndex;
import tersus.workbench.WorkspaceRepository;

/**
 * Adds an element to a model
 * 
 * This command uses default logic to determine what type of element should be added
 * 
 * @author Youval Bronicki
 */
public class AddElementAndPropagateSlotTypeCommand extends MultiStepCommand implements
		ValidatingCommand, IElementNameCommand
{
	protected ModelRef parentModelRef;

	protected LayoutConstraint constraint;

	private int index = -1;

	protected ModelElement newElement;

	private RepositoryIndex repositoryIndex;

	private boolean propagateSlotTypeOn;

	public AddElementAndPropagateSlotTypeCommand(WorkspaceRepository repository)
	{
		super(repository);
		setLabel("Add Element");
		this.propagateSlotTypeOn = true;
	}

	public AddElementAndPropagateSlotTypeCommand(WorkspaceRepository repository,
			LayoutConstraint constraint)
	{
		super(repository);
		setLabel("Add Element");
		this.constraint = constraint;
		this.propagateSlotTypeOn = true;
	}

	public AddElementAndPropagateSlotTypeCommand(Model parentModel, ModelElement newElement,
			boolean propagateSlotTypeOn)
	{
		super((WorkspaceRepository) parentModel.getRepository());
		setParentModel(parentModel);
		this.newElement = newElement;
		this.propagateSlotTypeOn = propagateSlotTypeOn;
	}

	public void setParentModel(Model parentModel)
	{
		parentModelRef = new ModelRef(parentModel);
	}

	public Model getParent()
	{
		return (Model) parentModelRef.getModelObject();
	}

	public void setNewElement(ModelElement newElement)
	{
		this.newElement = newElement;
	}

	public ModelElement getNewElement()
	{
		return newElement;
	}

	public void run()
	{
		newElement.setRole(getParent().defaultRole(newElement.getElementName()));
		runCommand(new AddElementCommand());


		if (propagateSlotTypeOn)
		{
			repositoryIndex = repository.getUpdatedRepositoryIndex();
			propagateSlotType(newElement);
		}
	}

	public String getSuggestedElementName()
	{
		return newElement.getElementName();
	}

	public String getActualElementName()
	{
		return newElement != null ? newElement.getRole().toString() : null;
	}

	public String toString()
	{

		return ("AddElementCommand[parent=" + getParent() + ";new element Id="
				+ getNewElement().getReferredModel().getModelId() + ";position"
				+ constraint.getPosition() + "]");
	}

	public void setIndex(int index)
	{
		this.index = index;

	}

	public void setSuggestedName(String name)
	{
		this.newElement.setRole(Role.get(name));
	}

	public String validate()
	{
		if (newElement == null)
		{
			if (getSuggestedElementName() == null || getSuggestedElementName().length() == 0)
				return Errors.USE_NON_EMPTY_NAME;
			else if (getSuggestedElementName().indexOf('/') > 0)
				return Errors.invalidCharacterInElementName('/');

		}
		return null;
	}

	public LayoutConstraint getConstraint()
	{
		return constraint;
	}

	public void setConstraint(LayoutConstraint constraint)
	{
		this.constraint = constraint;
	}

	public void setPropagateSlotTypeOn(boolean value)
	{
		propagateSlotTypeOn = value;
	}

	private class AddElementCommand extends Command
	{
		public void execute()
		{
			Model parentModel = getParent();

			if (parentModel.isReadOnly())
				throw new CommandFailedException("Cannot create add element to read only parent.\n"
						+ "Parent Id: " + parentModel.getModelId());

			if (newElement != null)
			{
				parentModel.addElement(index, newElement);
			}
			else
				throw new CommandFailedException("Faild to add element. New element is missing.");
		}

		public void undo()
		{
			getParent().removeElement(newElement);
			newElement.resetReferredModel(); // May hold a reference to an obsolete model
		}
	}
}

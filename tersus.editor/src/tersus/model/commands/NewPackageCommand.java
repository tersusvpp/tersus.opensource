/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model.commands;

import org.eclipse.gef.commands.Command;

import tersus.model.Package;
import tersus.model.PackageId;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 */
public class NewPackageCommand extends Command
{

	private WorkspaceRepository repository;
	private PackageId newPackageId;

	public NewPackageCommand(WorkspaceRepository repository, PackageId newPackageId)
	{
		this.repository = repository;
		this.newPackageId = newPackageId;
	}

	public void execute()
	{
		if (isParentReadOnly(newPackageId.getParent()))
			throw new CommandFailedException("Faild to add new package to read only package parent");

		repository.newPackage(newPackageId);
	}

	private boolean isParentReadOnly(PackageId parentId)
	{
		if (parentId == null)
			return false;
		
		Package parentPackage = repository.getPackage(parentId);

		if (parentPackage != null)
			return parentPackage.isReadOnly();
		else
			return isParentReadOnly(parentId.getParent());
	}

	public void undo()
	{
		repository.removePackage(newPackageId);
	}
}

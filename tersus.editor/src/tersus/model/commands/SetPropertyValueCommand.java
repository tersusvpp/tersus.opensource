/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model.commands;

import org.eclipse.gef.commands.Command;

import tersus.model.BuiltinProperties;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelException;
import tersus.model.ModelId;
import tersus.model.ModelObject;
import tersus.model.Repository;
import tersus.model.Role;

/**
 * A command for setting properties of ModelObjects. Note: It is assumed that it is safe to 'hold'
 * the value returned by ModelObject.getProperty() i.e. that whenever model properties are modified,
 * new value objects are created.
 * 
 * @author Youval Bronicki
 */
public class SetPropertyValueCommand extends Command
{
	protected Object propertyValue;

	protected String propertyName;

	protected Object undoValue;

	protected ModelObjectRef modelObjectRef;

	public SetPropertyValueCommand()
	{
		super("Set Property");
	}

	public SetPropertyValueCommand(Repository repository, ModelId modelId, String property,
			Object newValue)
	{
		setModelObject(repository.getModel(modelId, true));
		this.propertyName = property;
		this.propertyValue = newValue;
	}

	public SetPropertyValueCommand(ModelObject target, String propertyName, Object propertyValue)
	{
		setModelObject(target);
		// TODO: store id instead of reference - objects may be removed from the cache between
		// execute() and undo()
		this.propertyName = propertyName;
		this.propertyValue = propertyValue;
	}

	public void setModelObject(ModelObject modelObject)
	{
		if (modelObject instanceof Model)
			modelObjectRef = new ModelRef((Model) modelObject);
		else
		{
			assert modelObject instanceof ModelElement;
			modelObjectRef = new ElementRef((ModelElement) modelObject);
		}
	}

	public ModelObject getChangedModelObject()
	{
		return modelObjectRef.getModelObject();
	}

	public void execute()
	{
		try
		{
			if (isModelObjectReadOnly())
				throw new CommandFailedException("");

			ModelObject modelObject = getChangedModelObject();
			undoValue = modelObject.getProperty(propertyName);
			modelObject.setProperty(propertyName, propertyValue);

			if (BuiltinProperties.ROLE.equals(propertyName) && modelObjectRef instanceof ElementRef)
			{
				ElementRef elementRef = (ElementRef) modelObjectRef;
				if (propertyValue instanceof String)
					elementRef.setElementRole(Role.get((String) undoValue));
				else if (propertyValue instanceof Role)
					elementRef.setElementRole((Role) propertyValue);
			}
		}
		catch (Exception e)
		{
			ModelObject obj = getChangedModelObject();

			if (e instanceof CommandFailedException)
			{
				if (obj instanceof ModelElement)
				{
					throw new CommandFailedException("Failed to set local property " + propertyName
							+ " because parent model is read only. \n" + "Parent modelId: "
							+ ((ModelElement) obj).getParentModel().getModelId(), e);
				}
				else
				{
					throw new CommandFailedException("Failed to set shared property "
							+ propertyName + " on read only model.\n" + "ModelId = "
							+ ((Model) obj).getModelId(), e);
				}
			}

			if (obj instanceof ModelElement)
			{
				ModelElement element = (ModelElement) obj;
				throw new ModelException("Failed to set property " + propertyName + " modelId = "
						+ element.getModelId() + " role=" + element.getRole(), e);
			}
			else
			{
				Model model = (Model) obj;
				throw new ModelException("Failed to set property " + propertyName + " modelId = "
						+ model.getModelId(), e);
			}
		}
	}

	public void setPropertyName(String pName)
	{
		propertyName = pName;
	}

	public void setPropertyValue(Object val)
	{
		propertyValue = val;
	}

	public void undo()
	{
		ModelObject modelObject = getChangedModelObject();
		modelObject.setProperty(propertyName, undoValue);

		if (BuiltinProperties.ROLE.equals(propertyName) && modelObjectRef instanceof ElementRef)
		{
			ElementRef elementRef = (ElementRef) modelObjectRef;
			if (propertyValue instanceof String)
				elementRef.setElementRole(Role.get((String) undoValue));
			else if (propertyValue instanceof Role)
				elementRef.setElementRole((Role) undoValue);
		}
	}

	private boolean isModelObjectReadOnly()
	{
		ModelObject obj = getChangedModelObject();

		if (obj instanceof Model)
			return ((Model) obj).isReadOnly();
		else
			return ((ModelElement) obj).getParentModel().isReadOnly();
	}
}

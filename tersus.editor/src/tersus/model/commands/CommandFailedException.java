package tersus.model.commands;

// TODO need to extend Exception class
public class CommandFailedException extends RuntimeException
{
	public CommandFailedException(String message)
	{
		super(message);
	}

	public CommandFailedException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public CommandFailedException(Throwable cause)
	{
		super(cause);
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model.commands;

import java.util.ArrayList;

import org.eclipse.gef.commands.Command;

import tersus.editor.EditorMessages;
import tersus.model.Link;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.Path;
import tersus.model.Role;
import tersus.model.Slot;
import tersus.model.indexer.RepositoryIndex;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 * @author Liat Shiff
 */
public class RemoveElementCommand extends MultiStepCommand
{
	protected RepositoryIndex repositoryIndex;

	protected ModelRef parentRef;

	protected ModelElement child;

	protected Role childRole;

	protected ArrayList<PropagateSlotTypeCommand> propagateCommands;

	private boolean usePropagateSlotType;

	public RemoveElementCommand(ModelElement child, boolean usePropagateSlotType)
	{
		super((WorkspaceRepository) child.getRepository());

		this.child = child;
		this.parentRef = new ModelRef(child.getParentModel());
		this.usePropagateSlotType = usePropagateSlotType;

		childRole = child.getRole();
	}

	public Model getParent()
	{
		return (Model) parentRef.getModelObject();
	}

	private void propagateSlotTypeChanges(Model parentModel)
	{
		// ArrayList<Model> scannedParents = new ArrayList<Model>();
		repositoryIndex = repository.getUpdatedRepositoryIndex();

		if (child instanceof Link)
		{
			Link link = (Link) child;
			ModelElement source = parentModel.getElement(link.getSource());
			ModelElement target = parentModel.getElement(link.getTarget());

			if (source instanceof Slot
					&& Slot.DATA_TYPE_SETTING_INFERRED.equals(((Slot) source).getDataTypeSetting()))
				propagateCommands.add(new PropagateSlotTypeCommand((Slot) source));
			if (target instanceof Slot
					&& Slot.DATA_TYPE_SETTING_INFERRED.equals(((Slot) target).getDataTypeSetting()))
				propagateCommands.add(new PropagateSlotTypeCommand((Slot) target));
		}
		else
		{
			for (Link link : repositoryIndex.getIncomingLinks(child, false))
			{
				ModelElement source = parentModel.getElement(link.getSource());
				if (source != null)
					propagateCommands.add(new PropagateSlotTypeCommand(source));
			}

			for (Link link : repositoryIndex.getOutgoingLinks(child, false))
			{
				ModelElement target = parentModel.getElement(link.getTarget());
				if (target != null)
					propagateCommands.add(new PropagateSlotTypeCommand(target));
			}
		}
	}

	@Override
	protected void run()
	{
		if (usePropagateSlotType)
		{
			propagateCommands = new ArrayList<PropagateSlotTypeCommand>();

			// Before removing the element, we need to know if type propagate is necessary and if
			// that
			// being the case,
			// we create the command (and not execute it).
			// If we create the command after removing the element, we won't have the data we need.
			propagateSlotTypeChanges(getParent());

			RemoveElementBasicCommand removeElementBasicCommand = new RemoveElementBasicCommand();
			runCommand(removeElementBasicCommand);

			for (PropagateSlotTypeCommand command : propagateCommands)
			{
				runCommand(command);
			}
		}
		else
		{
			RemoveElementBasicCommand removeElementBasicCommand = new RemoveElementBasicCommand();
			runCommand(removeElementBasicCommand);
		}
	}

	public boolean isDeletedChildInPath(Path elementPath)
	{
		if (elementPath == null)
			return false;

		return elementPath.toString().contains(child.getRole().toString());
	}

	private class RemoveElementBasicCommand extends Command
	{

		private int index;

		public RemoveElementBasicCommand()
		{
			super(EditorMessages.Remove_Element_Command_Label);
		}

		public void execute()
		{
			Model parent = getParent();

			if (parent.isReadOnly())
				throw new CommandFailedException("Cannot remove element from read only parent.\n"
						+ "Parent id: " + parent.getModelId() + ".");

			index = parent.indexOf(child);
			parent.removeElement(parent.getElement(childRole));
		}

		public String toString()
		{
			return (getLabel() + " parent=" + getParent() + ";child=" + child);
		}

		public void undo()
		{
			Model parent = getParent();
			parent.addElement(index, child);
		}
	}

}

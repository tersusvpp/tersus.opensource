/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import tersus.model.BuiltinProperties;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.PackageId;
import tersus.workbench.WorkspaceRepository;

/**
 * Creating a new model in the repository (according to an existing template).
 * 
 * @author Ofer Brandes
 */
public class CopyModelCommand extends MultiStepCommand
{
	private Model templateModel;

	private ModelId newModelId;

	private ArrayList<Model> newModels;

	private ArrayList<PackageId> newPackages;

	private HashMap<ModelId, ModelId> idConversionMap = new HashMap<ModelId, ModelId>();

	/**
	 * @param newModelId
	 *            The - id of the new model.
	 * @param templateModel
	 *            - The template to create the model from.
	 */
	public CopyModelCommand(Model templateModel, ModelId newModelId)
	{
		super((WorkspaceRepository) templateModel.getRepository(), "Copy Model");

		this.templateModel = templateModel;
		this.newModelId = newModelId;
	}

	@Override
	protected void run()
	{
		newModels = new ArrayList<Model>();
		newPackages = new ArrayList<PackageId>();

		createFromTemplate(newModelId, templateModel);
	}

	public boolean canExecute()
	{
		return (newModelId != null && !newModelId.equals(templateModel.getId()));
	}

	private void createFromTemplate(ModelId newId, Model template)
	{
		addNewModel(newId, template.getId(), true);
		PackageId packageId = newId.getPackageId();

		if (repository.getPackage(packageId) == null)
			createPackage(packageId);

		Model newModel = repository.getModel(newId, true);
		newModels.add(newModel);
		idConversionMap.put(template.getId(), newId);
		copyChildren(newModel, template);
	}

	private void createPackage(PackageId packageId)
	{
		PackageId parent = packageId.getParent();

		if (parent != null && repository.getPackage(parent) == null)
			createPackage(packageId);

		createPackage(packageId);
		newPackages.add(packageId);
	}

	private void copyChildren(Model newModel, Model template)
	{
		List<ModelElement> elements = newModel.getElements();

		for (ModelElement element : elements)
		{
			ModelId oldId = element.getRefId();
			if (oldId != null && needsConversion(oldId))
			{
				if (alreadyConverted(oldId)) // this element has already been
					setProperty(element, BuiltinProperties.REF_ID, (ModelId) idConversionMap
							.get(oldId));
				else
				{
					
					ModelId newId = convertModelId(oldId);

					addNewModel(newId, oldId, true);
					Model newChildModel = repository.getModel(newId, true);
					newModels.add(newChildModel);
					idConversionMap.put(oldId, newId);
					copyChildren(newChildModel, element.getReferredModel());
					setProperty(element, BuiltinProperties.REF_ID, newId);
				}
			}
		}

	}

	private ModelId convertModelId(ModelId templateId)
	{
		String templateRootPackage = templateModel.getId().getPackageId().toString();
		String oldIdSuffix = templateId.toString().substring(templateRootPackage.length());
		String newPackageRoot = newModelId.getPackageId().toString();
		String newModelIdStr = newPackageRoot + oldIdSuffix;
		ModelId newId = new ModelId(newModelIdStr);
		int count = 1;
		while (repository.getModel(newId, true) != null)
		{
			++count;
			newId = new ModelId(newModelIdStr + " " + count);
		}
		return newId;
	}

	public Model getNewModel()
	{
		return (newModels.size() > 0 ? (Model) newModels.get(0) : null);
	}

	/**
	 * Checks whether a model needs to be duplicated. A model needs to be duplicated if it is
	 * contained in the package of the template model
	 * 
	 * @param oldId
	 *            the id of the model that needs to be checked
	 * @return
	 */
	private boolean needsConversion(ModelId oldId)
	{
		return templateModel.getId().getPackageId().isAncestorOf(oldId);
	}

	private boolean alreadyConverted(ModelId oldId)
	{
		return idConversionMap.containsKey(oldId);
	}
}
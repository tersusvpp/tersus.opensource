/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model.commands;

import org.eclipse.gef.commands.Command;

import tersus.model.PackageId;
import tersus.workbench.WorkspaceRepository;

public class SetPackageVersionCommand extends Command
{
	private PackageId packageId;
	private double newVersion, oldVersion;
	private WorkspaceRepository repository;

	public SetPackageVersionCommand(WorkspaceRepository repository, PackageId id, double version)
	{
		this.repository = repository;
		this.packageId = id;
		this.newVersion = version;
	}

	public void execute()
	{
		tersus.model.Package pkg = getPackage();

		if (pkg.isReadOnly())
			throw new CommandFailedException(
					"Failed to change read only package version.\nPackageId: "
							+ packageId.getPath() + "/" + packageId.getName());

		oldVersion = pkg.getRepositoryVersion();
		pkg.setRepositoryVersion(newVersion);
		repository.packageModified(pkg);
	}

	private tersus.model.Package getPackage()
	{
		tersus.model.Package pkg = repository.getPackage(packageId);
		return pkg;
	}

	public void undo()
	{
		getPackage().setRepositoryVersion(oldVersion);
		repository.packageModified(getPackage());
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model.commands;

import java.util.ArrayList;

import tersus.model.ModelId;
import tersus.model.PackageId;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public class MoveModelCommand extends MultiStepCommand
{
	private ArrayList<ModelId> modelIdsToMove;
	
	private PackageId parentPackageId;
	
	private String newPackageName;

	public MoveModelCommand(WorkspaceRepository repository, ArrayList<ModelId> modelIds, PackageId parentPackageId, String newPackageName) 
	{
		super(repository, "Move Models");
		
		modelIdsToMove = modelIds;
		this.parentPackageId = parentPackageId;
		this.newPackageName = newPackageName;
	}

	public void run()
	{
		if (newPackageName == null || newPackageName.length() == 0)
			moveModels(parentPackageId);
		else
		{
			PackageId newPackageId = new PackageId(parentPackageId, newPackageName);
			addNewPackage(newPackageId);
			moveModels(newPackageId);
		}
	}
	
	private void moveModels (PackageId parentPackageId)
	{
		if (repository instanceof WorkspaceRepository)
		{
			WorkspaceRepository workspaceRepository = (WorkspaceRepository)repository;
			for (ModelId modelId: modelIdsToMove)
			{	
				ModelId newModelId = workspaceRepository.getNewModelId(parentPackageId, modelId.getName());
				changeModelId(repository.getModel(modelId, true), newModelId);
			}
		}
	}
}

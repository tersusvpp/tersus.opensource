package tersus.model.commands;

import java.util.Map;

import org.eclipse.gef.commands.Command;

import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Package;
import tersus.model.PackageId;
import tersus.workbench.WorkspaceRepository;

public class NewModelCommand extends Command
{

	private WorkspaceRepository repository;

	private ModelId newModelId;

	private ModelId templateId;

	private Boolean createElements;

	private Class<? extends Model> modelClass;

	private Map<String, Object> properties;

	public NewModelCommand(WorkspaceRepository repository, ModelId newModelId, ModelId templateId,
			Boolean createElements)
	{
		this.repository = repository;
		this.newModelId = newModelId;
		this.templateId = templateId;
		this.createElements = createElements;
	}
	
	public NewModelCommand(WorkspaceRepository repository, ModelId newModelId, Class<? extends Model> modelClass, Map<String, Object> properties)
	{
		this.repository = repository;
		this.newModelId = newModelId;
		this.modelClass = modelClass;
		this.properties = properties;
	}

	@Override
	public void execute()
	{
		Package parentPackage = getParentPackage(newModelId.getPackageId());
		if (parentPackage != null && parentPackage.isReadOnly())
			throw new CommandFailedException("Cannot create new model inside read only package.\n"
					+ "PackageId" + newModelId.getPackageId());

		if (templateId != null)
			repository.newModel(newModelId, templateId, createElements);
		else
			repository.newModel(newModelId, modelClass, properties);
	}

	private Package getParentPackage(PackageId packageId)
	{
		if (packageId == null)
			return null;

		Package parentPackage = repository.getPackage(packageId);

		if (parentPackage == null)
			return getParentPackage(packageId.getParent());
		else
			return parentPackage;
	}

	@Override
	public void undo()
	{
		// TODO Auto-generated method stub
		repository.removeModel(repository.getModel(newModelId, true));
	}
}

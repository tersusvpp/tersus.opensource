package tersus.model.commands;

/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

import java.util.ArrayList;

import org.eclipse.gef.commands.Command;

import tersus.model.IRepository;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.Repository;
import tersus.model.Role;

/**
 * @author Liat Shiff
 */
public class ChangeElementOrderInDataModelCommand extends Command
{
	ModelId parentId;

	IRepository repository;

	Role childToMoveRole;

	ModelElement afterChild;

	int oldIndex;

	ArrayList<ModelElement> undoElementList;

	public ChangeElementOrderInDataModelCommand(Model parent, ModelElement childToMove,
			ModelElement afterChild)
	{
		this.childToMoveRole = childToMove.getRole();
		this.afterChild = afterChild;
		parentId = parent.getModelId();
		repository = parent.getRepository();
	}

	private Model getParentModel()
	{
		Model parentModel = repository.getModel(parentId, true);
		return parentModel;
	}

	private ModelElement getChildToMove(Model parentModel)
	{
		if (childToMoveRole != null && parentModel != null)
			return parentModel.getElement(childToMoveRole);

		return null;
	}

	public void execute()
	{
		Model parentModel = getParentModel();

		if (parentModel.isReadOnly())
			throw new CommandFailedException(
					"Faild to change elements order in read only data model.\n" + "Data model id: "
							+ parentModel.getModelId());

		ModelElement childToMoveElement = getChildToMove(parentModel);
		oldIndex = parentModel.getElements().indexOf(childToMoveElement);
		parentModel.removeElement(childToMoveElement);
		int newIndex = parentModel.getElements().indexOf(afterChild);
		parentModel.addElement(newIndex + 1, childToMoveElement);
	}

	public void undo()
	{
		Model parentModel = getParentModel();
		ModelElement childToMoveElement = getChildToMove(parentModel);
		parentModel.removeElement(childToMoveElement);
		parentModel.addElement(oldIndex, childToMoveElement);
	}

	public void redo()
	{
		execute();
	}
}

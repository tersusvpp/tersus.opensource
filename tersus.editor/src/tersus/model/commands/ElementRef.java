/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model.commands;

import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.ModelObject;
import tersus.model.Role;

/**
 * @author Youval Bronicki
 */
public class ElementRef extends ModelObjectRef
{
	private ModelId parentModelId;

	private Role elementRole;

	public ElementRef(ModelElement element)
	{
		super(element.getRepository());
		this.parentModelId = element.getParentModel().getId();
		this.elementRole = element.getRole();
	}

	public ElementRef(ModelElement element, Model parentModel)
	{
		super(parentModel.getRepository());
		this.parentModelId = parentModel.getId();
		this.elementRole = element.getRole();
	}

	public void setElementRole(Role newElementRole)
	{
		this.elementRole = newElementRole;
	}

	@Override
	public ModelObject getModelObject()
	{
		return repository.getModel(parentModelId, true).getElement(elementRole);
	}
}

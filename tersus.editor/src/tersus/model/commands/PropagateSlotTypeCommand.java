package tersus.model.commands;

import java.util.ArrayList;
import java.util.HashSet;

import org.eclipse.core.runtime.Status;

import tersus.editor.EditorMessages;
import tersus.editor.EditorPlugin;
import tersus.editor.Errors;
import tersus.model.BuiltinModels;
import tersus.model.BuiltinProperties;
import tersus.model.DataElement;
import tersus.model.Link;
import tersus.model.LinkOperation;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.ModelUtils;
import tersus.model.PluginDescriptor;
import tersus.model.Role;
import tersus.model.Slot;
import tersus.model.SlotType;
import tersus.model.indexer.RepositoryIndex;

import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public class PropagateSlotTypeCommand extends MultiStepCommand
{
	private static final String TEXT_HANDLER = "tersus.runtime.TextHandler";

	private static final String NUMBER_HANDLER = "tersus.runtime.NumberHandler";

	private static final String DATE_HANDLER = "tersus.runtime.DateHandler";

	private static final String DATE_AND_TIME_HANDLER = "tersus.runtime.DateAndTimeHandler";

	private static final String BOOLEAN_HANDLER = "tersus.runtime.BooleanHandler";

	private ElementRef rootElementRef;

	private ModelId newSlotType;

	private HashSet<Slot> slotsToPropagate = new HashSet<Slot>();

	private HashSet<ModelElement> scannedElements = new HashSet<ModelElement>();

	private RepositoryIndex index;

	private boolean canPropagate;

	public PropagateSlotTypeCommand(ModelElement rootElement)
	{
		super((WorkspaceRepository) rootElement.getRepository());

		setRootElement(rootElement);
		newSlotType = null;
		slotsToPropagate = new HashSet<Slot>();
		scannedElements = new HashSet<ModelElement>();
		canPropagate = true;
	}

	protected void run()
	{
		index = repository.getUpdatedRepositoryIndex();

		if (getRootElement() instanceof Link)
			scanLinkRootElement();
		else
		{
			newSlotType = getExplicitType(getRootElement());

			if (getRootElement() instanceof Slot)
			{
				Slot rootSlot = (Slot) getRootElement();
				if (Slot.DATA_TYPE_SETTING_INFERRED.equals(rootSlot.getDataTypeSetting()))
					slotsToPropagate.add(rootSlot);
			}
			scan(getRootElement());
		}

		if (canPropagate && (newSlotType != null) && !slotsToPropagate.isEmpty())
		{
			for (Slot slot : slotsToPropagate)
			{
				if (!newSlotType.equals(slot.getDataTypeId())
						&& !slot.getParentModel().isReadOnly())
					setProperty(slot, BuiltinProperties.DATA_TYPE_ID, newSlotType);
			}
		}
	}

	private void scanLinkRootElement()
	{
		Link link = (Link) getRootElement();

		if (!LinkOperation.REMOVE.equals(link.getProperty(BuiltinProperties.OPERATION)))
		{
			ModelElement target = link.getParentModel().getElement(link.getTarget());
			ModelElement source = link.getParentModel().getElement(link.getSource());

			ModelId explicitTargetType = getExplicitType(target);
			if (!BuiltinModels.NOTHING_ID.equals(explicitTargetType))
				newSlotType = getExplicitType(target);

			if (target instanceof Slot && !isDataTypeExpicit((Slot) target))
			{
				slotsToPropagate.add((Slot) target);
				scan(target);
			}
			else if (source instanceof Slot && !isDataTypeExpicit((Slot) source))
			{
				slotsToPropagate.add((Slot) source);
				scan(source);
			}
		}
	}

	private void scan(ModelElement element)
	{
		if (element == null)
			return;
		if (scannedElements.contains(element))
			return;

		scannedElements.add(element);

		scanIncomingLinks(element);
		scanOutgoingLinks(element);

		if (element instanceof Slot)
			scanTypePropagationGroup((Slot) element);
	}

	private void scanOutgoingLinks(ModelElement element)
	{
		ArrayList<Link> outgoingLinks = index.getOutgoingLinks(element, true);

		if (!outgoingLinks.isEmpty())
		{
			for (Link link : outgoingLinks)
			{
				if (!LinkOperation.REMOVE.equals(link.getProperty(BuiltinProperties.OPERATION)))
				{
					ModelElement target = link.getParentModel().getElement(link.getTarget());
					handleLink(element, target, target);
				}
			}
		}
	}

	private void scanIncomingLinks(ModelElement element)
	{
		ArrayList<Link> incomingLinks = index.getIncomingLinks(element, true);

		int i = incomingLinks.size();
		if (!incomingLinks.isEmpty())
		{
			for (Link link : incomingLinks)
			{
				if (!LinkOperation.REMOVE.equals(link.getProperty(BuiltinProperties.OPERATION)))
				{
					ModelElement source = link.getParentModel().getElement(link.getSource());
					handleLink(source, element, source);
				}
			}
		}
	}

	private ModelId getExplicitType(ModelElement element)
	{
		if (element instanceof DataElement)
		{
			if (ModelUtils.isConstant(element))
				return getBuiltinTypeForConstant(element.getRefId());
			else
				return element.getModelId();
		}
		else if (isDataTypeExpicit(element))
			return ((Slot) element).getDataTypeId();
		else if (ModelUtils.isDisplayElement(element))
			return element.getModelId();
		else
			return null;
	}

	private boolean isDataTypeExpicit(ModelElement element)
	{
		return element instanceof Slot
				&& Slot.DATA_TYPE_SETTING_EXPLICIT.equals(((Slot) element).getDataTypeSetting());
	}

	private void handleLink(ModelElement source, ModelElement target, ModelElement nextElement)
	{
		if (nextElement == null)
			return;
		if (scannedElements.contains(nextElement))
			return;

		if (ModelUtils.isConstant(target))
			return; // Constants are not propagated from target to source

		ModelId sourceExplicitType = getExplicitType(source);
		ModelId targetExplicitType = getExplicitType(target);

		if (sourceExplicitType != null && targetExplicitType != null
				&& sourceExplicitType != targetExplicitType)
			return; // No need to propagate a link which have both explicit ends

		ModelId explicitTargetType = getExplicitType(target);
		if (BuiltinModels.NOTHING_ID.equals(explicitTargetType))
			return; // "Nothing" is not propagated from target to source
		if (BuiltinModels.NOTHING_ID.equals(newSlotType) && !nextElement.equals(target))
			return; // "Nothing" is not propagated from target to source

		// Check next type
		ModelId nextElementType = getExplicitType(nextElement);
		if (nextElementType != null)
		{
			if (newSlotType == null || BuiltinModels.ANYTHING_ID.equals(newSlotType))
				newSlotType = nextElementType;
			else if (!nextElementType.equals(newSlotType)
					&& !nextElementType.equals(BuiltinModels.ANYTHING_ID))
				canPropagate = false;
		}
		else
		{
			if (nextElement instanceof Slot)
				slotsToPropagate.add((Slot) nextElement);

			scan(nextElement);
		}
	}

	private void scanTypePropagationGroup(Slot sourceSlot)
	{
		Model parentModel = sourceSlot.getParentModel();

		PluginDescriptor pluginDescriptor = parentModel.getPluginDescriptor();
		
		if (pluginDescriptor != null)
		{
			HashSet<String> typePropagationGroup = pluginDescriptor.getTypePropagationGroup();

			if (!typePropagationGroupContainsElement(sourceSlot, typePropagationGroup, parentModel))
				return;

			ArrayList<Slot> slotsToScan = new ArrayList<Slot>();

			for (String elementName : typePropagationGroup)
			{
				if (elementName.equals(PluginDescriptor.ALL_TRIGERS))
					slotsToScan.addAll(parentModel.getAllNonDistinguishedTriggers());

				else if (elementName.equals(PluginDescriptor.ALL_EXITS))
					slotsToScan.addAll(parentModel.getAllNonDistinguishedExits());

				else if (elementName.contains("|"))
				{
					ModelElement element = getPreferableElement(elementName, parentModel);

					if (element != null && element instanceof Slot)
						slotsToScan.add((Slot) element);
				}
				else
				{
					ModelElement element = parentModel.getElement(Role.get(elementName));

					if (element != null && !slotsToScan.contains(element) && element instanceof Slot)
						slotsToScan.add((Slot) element);
				}
			}

			for (Slot slot : slotsToScan)
			{
				ModelId nextElementType = getExplicitType(slot);
				if (nextElementType != null)
				{
					if (newSlotType == null || BuiltinModels.ANYTHING_ID.equals(newSlotType))
						newSlotType = nextElementType;
					else if (!nextElementType.equals(newSlotType)
							&& !nextElementType.equals(BuiltinModels.ANYTHING_ID))
						canPropagate = false;
				}
				else
				{
					slotsToPropagate.add(slot);
					scan(slot);
				}
			}
		}
		
	}

	private boolean typePropagationGroupContainsElement(Slot slot,
			HashSet<String> typePropagationGroupRoleList, Model parentModel)
	{
		for (String name : typePropagationGroupRoleList)
		{
			if (name.equals(slot.getElementName()))
				return true;
			if (PluginDescriptor.ALL_TRIGERS.equals(name)
					&& slot.getType().equals(SlotType.TRIGGER))
				return true;
			if (PluginDescriptor.ALL_EXITS.equals(name) && slot.getType().equals(SlotType.EXIT))
				return true;
			if (name.contains("|"))
			{
				ModelElement e = getPreferableElement(name, parentModel);
				if (e != null && e.equals(slot))
					return true;
			}
		}
		return false;
	}

	private ModelElement getPreferableElement(String elementName, Model parentModel)
	{
		ModelElement element = null;
		String[] s = elementName.split("\\|");

		for (String name : s)
		{
			element = parentModel.getElement(Role.get("<" + name + ">"));
			if (element != null)
				return element;
		}
		return element; // element == null
	}

	private ModelId getBuiltinTypeForConstant(ModelId constantId)
	{
		Model model = repository.getModel(constantId, true);
		String pluginClass = model.getPluginJavaClass();
		if (TEXT_HANDLER.equals(pluginClass))
			return BuiltinModels.TEXT_ID;
		else if (NUMBER_HANDLER.equals(pluginClass))
			return BuiltinModels.NUMBER_ID;
		else if (DATE_HANDLER.equals(pluginClass))
			return BuiltinModels.DATE_ID;
		else if (BOOLEAN_HANDLER.equals(pluginClass))
			return BuiltinModels.BOOLEAN_ID;
		else if (DATE_AND_TIME_HANDLER.equals(pluginClass))
			return BuiltinModels.DATE_AND_TIME_ID;
		else
		{
			TersusWorkbench.log(new Status(Status.WARNING, EditorPlugin.getPluginId(),
					Errors.UNKNOWN_CONSTANT_TYPE_CODE, EditorMessages.format(
							Errors.UNKNOWN_CONSTANT_TYPE_KEY, new Object[]
							{ constantId, pluginClass }), null));
			return null;
		}
	}

	private void setRootElement(ModelElement rootElement)
	{
		rootElementRef = new ElementRef(rootElement);
	}

	private ModelElement getRootElement()
	{
		return (ModelElement) rootElementRef.getModelObject();
	}
}

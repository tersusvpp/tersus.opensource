package tersus.model.commands;

import org.eclipse.gef.commands.Command;

import tersus.model.MetaProperty;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelException;
import tersus.model.ModelObject;

public class AddMetaPropertyCommand extends Command
{
	private ModelObjectRef modelObjectRef;

	private boolean canAddSharedProperty;
	private boolean canAddLocalProperty;

	protected String newPropertyName;

	public AddMetaPropertyCommand(ModelObject selectedModelObject, String propertyName,
			boolean canAddSharedProperty, boolean canAddLocalProperty)
	{
		setModelObject(selectedModelObject);
		this.newPropertyName = propertyName;

		this.canAddLocalProperty = canAddLocalProperty;
		this.canAddSharedProperty = canAddSharedProperty;
	}

	private void setModelObject(ModelObject modelObject)
	{
		if (modelObject instanceof Model)
			modelObjectRef = new ModelRef((Model) modelObject);
		else if (modelObject instanceof ModelElement)
			modelObjectRef = new ElementRef((ModelElement) modelObject);
	}

	public void execute()
	{
		try
		{
			String temp = "";
			ModelObject modelObject = modelObjectRef.getModelObject();

			if (modelObject instanceof Model)
			{
				Model model = (Model) modelObject;
				
				if (model.isReadOnly())
					throw new CommandFailedException("Failed to add shared new property "
							+ newPropertyName + " on read only model.\n" + "ModelId = "
							+ model.getModelId());
				
				model.addMetaProperty(new MetaProperty(newPropertyName, temp.getClass(), true,
						true, canAddLocalProperty && canAddSharedProperty));
				model.addProperty(newPropertyName, null);
			}
			else if (modelObject instanceof ModelElement)
			{
				ModelElement element = (ModelElement) modelObject;

				if (element.getParentModel().isReadOnly())
					throw new CommandFailedException(
							("Failed to set local property " + newPropertyName
									+ " because parent model is read only. \n" + "Parent modelId: " + 
									element.getParentModel().getModelId()));
				
				element.addMetaProperty(new MetaProperty(newPropertyName, temp.getClass(), true,
						true, canAddLocalProperty && canAddSharedProperty));
				element.addProperty(newPropertyName, null);
			}

		}
		catch (Exception e)
		{
			if (e instanceof tersus.model.commands.CommandFailedException)
				throw (tersus.model.commands.CommandFailedException) e;
			else
				throw new ModelException("Failed to add new property ", e);
		}
	}

	public void undo()
	{
		ModelObject modelObject = modelObjectRef.getModelObject();
		if (modelObject instanceof Model)
		{
			Model model = (Model) modelObject;
			model.removeMetaProperty(newPropertyName);
			model.removeProperty(newPropertyName);
		}
		else if (modelObject instanceof ModelElement)
		{
			ModelElement element = (ModelElement) modelObject;
			element.removeMetaProperty(newPropertyName);
			element.removeProperty(newPropertyName);
		}
	}
}
/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. -  Initial API and implementation
 *************************************************************************************************/

package tersus.model.commands;

import tersus.editor.layout.LayoutConstraint;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.ModelObject;
import tersus.model.ModelUtils;
import tersus.model.PackageId;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 */
public class PasteCloneCommand extends MultiStepCommand
{
	private ModelObjectRef templateRef;

	private ModelRef parentRef;

	private ModelId newModelId;

	public PasteCloneCommand(Model parentModel, ModelObject template)
	{
		super((WorkspaceRepository) parentModel.getRepository(), "Paste");
		parentRef = new ModelRef(parentModel);

		if (!PasteElementsCommand.checkValidChild(template, parentModel, false))
			throw new IllegalArgumentException("Invalid child " + template + " for " + parentModel);

		if (template instanceof Model)
			templateRef = new ModelRef((Model) template);
		else if (template instanceof ModelElement)
			templateRef = new ElementRef((ModelElement) template);
	}

	@Override
	protected void run()
	{
		Model templateModel;
		if (templateRef instanceof ModelRef)
			templateModel = (Model) ((ModelRef) templateRef).getModelObject();
		else
			templateModel = ((ModelElement) ((ElementRef) templateRef).getModelObject())
					.getReferredModel();

		Model parentModel = (Model) parentRef.getModelObject();

		newModelId = ((WorkspaceRepository) repository).getNewModelId(parentModel.getPackage()
				.getPackageId(), templateModel.getName());
		createCopy(newModelId, templateModel);

		ModelElement newElement = null;
		if (templateRef instanceof ModelRef)
			newElement = AddElementHelper.getNewElement(parentModel, newModelId, newModelId
					.getName(), new LayoutConstraint(0.15, 0.3, 0.25, 0.5), null);

		else
		{
			ModelElement templateElement = ((ModelElement) templateRef.getModelObject());

			newElement = AddElementHelper.getNewElement(parentModel, newModelId, templateElement
					.getElementName(), null, templateElement);
		}
		
		newElement.setPosition(ModelUtils.findNonOverlappingPosition(parentModel, newElement));
		addElement(parentModel, newElement, null, null, null, true);

		templateRef = null; // We no longer need a reference to the template
	}

	private void createCopy(ModelId newModelId, Model templateModel)
	{
		PackageId packageId = newModelId.getPackageId();
		createPackageIfMissing(packageId);

		addNewModel(newModelId, templateModel.getId(), true);
	}

	private void createPackageIfMissing(PackageId packageId)
	{
		if (repository.getPackage(packageId) == null)
		{
			if (packageId.getParent() != null)
				createPackageIfMissing(packageId.getParent());

			createNewPackage(packageId);
		}
	}
}

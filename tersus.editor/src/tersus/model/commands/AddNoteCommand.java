/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model.commands;

import tersus.editor.layout.LayoutConstraint;
import tersus.model.BuiltinProperties;
import tersus.model.Model;
import tersus.model.Note;
import tersus.model.Role;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 * @author Liat Shiff
 */
public class AddNoteCommand extends AddElementAndPropagateSlotTypeCommand
{
	public AddNoteCommand(Model parentModel, LayoutConstraint constraint)
	{
		super((WorkspaceRepository) parentModel.getRepository(), constraint);
		setLabel("Add Note");
		setParentModel(parentModel);

		Role role = Role.get("Note 1");
		if (parentModel.getElement(role) != null)
			role = parentModel.defaultRole("Note");
		setNewElement(new Note());
		getNewElement().setRole(role);
		getNewElement().setPosition(constraint.getPosition());
		getNewElement().setSize(constraint.getSize());
		getNewElement().setProperty(BuiltinProperties.COMMENT, role.toString());
	}
}
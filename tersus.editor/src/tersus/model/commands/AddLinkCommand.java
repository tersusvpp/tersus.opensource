/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model.commands;

import tersus.editor.Debug;
import tersus.editor.EditorMessages;
import tersus.model.Link;
import tersus.model.LinkOperation;
import tersus.model.Path;
import tersus.model.Role;
import tersus.util.Trace;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 * @author Liat Shiff
 */
public class AddLinkCommand extends AddElementAndPropagateSlotTypeCommand
{
	protected Link newLink;

	protected Path source;
 
	protected Path target;

	public AddLinkCommand(Link newLink, WorkspaceRepository repository)
	{
		super(repository);

		setLabel(EditorMessages.ConnectionCommand_Label);
		setNewElement(newLink);
	}

	public void run()
	{
		if (Debug.LINKS)
			Trace.push("Executing " + this);

		Role role = getParent().defaultRole("Flow");
		getLink().setRole(role);

		super.run();

		if (Debug.LINKS)
			Trace.pop();
	}

	public Path getSource()
	{
		return getLink().getSource();
	}

	public Path getTarget()
	{
		return getLink().getTarget();
	}

	public void setSource(Path path)
	{
		getLink().setSource(path);
	}

	public void setOperation(LinkOperation operation)
	{
		getLink().setOperation(operation);
	}
	public void setTarget(Path path)
	{
		getLink().setTarget(path);
	}

	private Link getLink()
	{
		return (Link) getNewElement();
	}

	public String toString()
	{
		return "LinkCommand parent=" + getParent() + " source=" + source + " target=" + target;
	}

}
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model.commands;

import tersus.model.Model;
import tersus.model.ModelId;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 */
public class CreateModelFromPrototypeCommand extends InstantiatePrototypeCommand
{

	private ModelId prototypeId;

	private ModelId newModelId;

	public CreateModelFromPrototypeCommand(Model prototype, ModelId newModelId)
	{
		super((WorkspaceRepository) prototype.getRepository(), "Create " + prototype.getName());
		this.prototypeId = prototype.getId();
		this.newModelId = newModelId;
	}

	public void run()
	{
		init();
		Model prototypeModel = repository.getModel(prototypeId, true);
		try
		{
			createModelFromPrototype(prototypeModel, newModelId);
		}
		catch (PrototypeInstantiationException e)
		{
			undo();
			throw e;
		}
		finally
		{
			cleanup();
		}
	}
}

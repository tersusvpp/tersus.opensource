/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
package tersus.model.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import tersus.editor.layout.LayoutConstraint;
import tersus.model.BuiltinModels;
import tersus.model.Composition;
import tersus.model.DataElement;
import tersus.model.DataType;
import tersus.model.FlowModel;
import tersus.model.Link;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.ModelObject;
import tersus.model.ModelUtils;
import tersus.model.PackageId;
import tersus.model.Path;
import tersus.model.Repository;
import tersus.model.Role;
import tersus.model.Slot;
import tersus.workbench.WorkspaceRepository;

public class PasteElementsCommand extends MultiStepCommand
{
	private Model parentModel;

	private List<ModelObject> objects;

	public PasteElementsCommand(Model parentModel, List<ModelObject> objects)
	{
		super((WorkspaceRepository) parentModel.getRepository(), "Paste");
		this.parentModel = parentModel;
		this.objects = objects;
	}

	@Override
	protected void run()
	{
		HashMap<Role, Role> roleMap = new HashMap<Role, Role>();
		ArrayList<Link> links = new ArrayList<Link>();

		for (ModelObject obj : objects)
		{
			importModelIfNeeded(obj);

			if (checkValidChild(obj, parentModel, true))
			{
				ModelElement newElement = null;

				if (obj instanceof ModelElement)
				{
					ModelElement element = (ModelElement) obj;
					if (obj instanceof Link)
						links.add((Link) obj);
					else
					{
						newElement = AddElementHelper.getNewElement(parentModel, element.getModelId(),
								element.getElementName(), null, element);
						
						if (parentModel instanceof DataType)
						{
							// Clearing fields that are not relevant for element of data types
							((DataElement) newElement).clearPosition();
							((DataElement) newElement).clearSize();
							((DataElement) newElement).setType(null);
						}
						Role originalRole = newElement.getRole();
						Role newRole = parentModel.defaultRole(originalRole.toString());
						if (newRole != originalRole)
						{
							roleMap.put(originalRole, newRole);
							newElement.setRole(newRole);
						}
					}
				}
				else
				{
					ModelId newModelId = ((Model) obj).getId();
					if (parentModel instanceof FlowModel)
						newElement = AddElementHelper.getNewElement(parentModel, newModelId,
								newModelId.getName(), new LayoutConstraint(0.15, 0.3, 0.25, 0.5), null);
					else
						newElement = AddElementHelper.getNewElement(parentModel, newModelId,
								newModelId.getName(), null, null);
				}

				if (newElement != null)
				{
					newElement.setPosition(ModelUtils.findNonOverlappingPosition(parentModel, newElement));
					addElement(parentModel, newElement, null, null, null, true);
				}
			}
		}

		for (Link originalLink : links)
		{
			Link newLink = (Link) Repository.copyElement(originalLink);
			updateSourcePath(newLink, roleMap);
			updateTargetPath(newLink, roleMap);
			newLink.setRole(parentModel.defaultRole("Flow"));

			addElement(parentModel, newLink, null, null, null, true);
		}
	}

	private void importModelIfNeeded(ModelObject obj)
	{
		if (obj.getRepository() != repository)
		{
			Model targetModel = obj instanceof Model ? (Model) obj : ((ModelElement) obj)
					.getReferredModel();
			if (targetModel !=null && repository.getModel(targetModel.getId(), true) == null)
			{
				importModel(targetModel);
			}
		}

	}

	private void importModel(Model targetModel)
	{
		PackageId packageId = targetModel.getId().getPackageId();
		createPackageIfMissing(packageId);
		Model newModel = repository.getModel(
				addNewModel(targetModel.getId(), targetModel.getClass(),
						targetModel.getProperties(true)), true);
		for (ModelElement e : targetModel.getElements())
		{
			ModelId refId = e.getRefId();

			if (refId != null && repository.getModel(refId, true) == null)
				importModel(e.getReferredModel());

			ModelElement newElement = Repository.copyElement(e);
			addElement(newModel, newElement, null, null, null, false);
		}
	}

	private void createPackageIfMissing(PackageId packageId)
	{
		if (repository.getPackage(packageId) == null)
		{
			if (packageId.getParent() != null)
				createPackageIfMissing(packageId.getParent());

			createNewPackage(packageId);
		}
	}

	private void updateSourcePath(Link newLink, HashMap<Role, Role> roleMap)
	{
		Path newSourcePath = updatePath(newLink.getSource(), roleMap);
		if (newSourcePath != null)
			newLink.setSource(newSourcePath);
	}

	private void updateTargetPath(Link newLink, HashMap<Role, Role> roleMap)
	{
		Path newTargetPath = updatePath(newLink.getTarget(), roleMap);
		if (newTargetPath != null)
			newLink.setTarget(newTargetPath);
	}

	private Path updatePath(Path path, HashMap<Role, Role> roleMap)
	{
		if (path == null || path.getNumberOfSegments() == 0)
			return null;
		Role segment0 = path.getSegment(0);
		if (roleMap.containsKey(segment0))
		{
			Path newPath = new Path();
			newPath.copy(path);
			newPath.setSegment(0, roleMap.get(segment0));
			return newPath;
		}
		else
			return null;
	}

	public static boolean checkValidChild(Object childObject, Model parentModel, boolean isReuse)
	{
		if (parentModel instanceof FlowModel)
			return true;

		if (parentModel instanceof DataType
				&& ((DataType) parentModel).getComposition() != Composition.ATOMIC)
		{
			if (isReuse && childObject instanceof Slot)
			{
				Model refModel = ((Slot) childObject).getReferredModel();

				if (refModel == null)
					return true;
				
				if (refModel instanceof DataType
						&& !BuiltinModels.NOTHING_ID.equals(refModel.getModelId()))
					return true;
			}
			if ((childObject instanceof DataType))
				return true;
			
			if (childObject instanceof DataElement
					&& (((DataElement) childObject).getReferredModel() instanceof DataType))
				return true;
		}
		
		return false;
	}
}

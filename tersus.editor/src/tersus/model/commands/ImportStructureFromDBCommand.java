/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model.commands;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.gef.commands.Command;

import tersus.model.BuiltinProperties;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.ModelUtils;
import tersus.model.TersusTableFieldDescriptor;
import tersus.util.Misc;
import tersus.util.SQLUtils;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public class ImportStructureFromDBCommand extends MultiStepCommand
{
	private ElementRef elementRef;

	private ArrayList<TersusTableFieldDescriptor> fields;

	private ArrayList<TersusTableFieldDescriptor> initialFields;

	private String dataSource;

	private String schema;

	private String tableName;

	private String catalog;

	List<Command> removeElementComamds = new ArrayList<Command>();

	public ImportStructureFromDBCommand(WorkspaceRepository repository, ModelElement element)
	{
		super(repository, "Import Structure from DB");

		elementRef = new ElementRef(element);
		setInitialFields();
	}

	public void run()
	{
		ModelElement element = (ModelElement) elementRef.getModelObject();
		Model model = element.getReferredModel();

		setProperty(model, BuiltinProperties.DATA_SOURCE, dataSource);
		setProperty(model, BuiltinProperties.TABLE_NAME, tableName);
		setProperty(model, BuiltinProperties.SCHEMA, schema);

		if (catalog != null)
			setProperty(model, BuiltinProperties.CATALOG, catalog);
		else
			setProperty(model, BuiltinProperties.CATALOG, "");

		addNewFields(model);
		removeFields(model);
	}

	private void addNewFields(Model model)
	{
		for (TersusTableFieldDescriptor descriptor : fields)
		{
			ModelElement elementField = getElementFromList(descriptor, initialFields);

			if (elementField != null)
			{
				if (elementField.getRole().toString().equals(descriptor.getClass()))
					renameElement(elementField, descriptor.getElementName());
			}
			else
			// Need to add new field.
			{
				ModelId newModelId = descriptor.getDataType();
				elementField = AddElementHelper.getNewElement(model, newModelId, descriptor
						.getElementName(), null, null);
				addElement(model, elementField, null, null, null, true);
			}

			if (elementField != null)
			{
				setProperty(elementField, BuiltinProperties.REF_ID, descriptor.getDataType());

				setProperty(elementField, BuiltinProperties.PRIMARY_KEY,
						descriptor.getPrimaryKey() == null ? false : descriptor.getPrimaryKey());

				setBooleanProperty(elementField, BuiltinProperties.NULLABLE, descriptor
						.getNullable());

				setProperty(elementField, BuiltinProperties.DEFAULT_VALUE, descriptor
						.getDefaultValue());

				setProperty(elementField, BuiltinProperties.COLUMN_NAME, descriptor.getColumnName());

				setProperty(elementField, BuiltinProperties.COLUMN_SIZE,
						descriptor.getColumnSize() == null ? null : descriptor.getColumnSize()
								.toString());
				setProperty(elementField, BuiltinProperties.COLUMN_TYPE, descriptor.getColumnType());
			}
		}
	}

	private void removeFields(Model model)
	{
		for (TersusTableFieldDescriptor initField : initialFields)
		{
			if (getElementFromList(initField, fields) == null)
				deleteElement(model, initField.getElement().getElementName(), false, false, true);
		}
	}

	public ModelElement getElementFromList(TersusTableFieldDescriptor descriptor,
			ArrayList<TersusTableFieldDescriptor> fields)
	{
		if (descriptor.getElement() != null)
		{
			for (TersusTableFieldDescriptor field : fields)
			{
				ModelElement element = field.getElement();
				if (element != null && element.equals(descriptor.getElement()))
					return element;
			}
		}
		return null;
	}

	public ArrayList<TersusTableFieldDescriptor> getInitialFields()
	{
		return initialFields;
	}

	private void setInitialFields()
	{
		initialFields = new ArrayList<TersusTableFieldDescriptor>();
		ModelElement element = (ModelElement) elementRef.getModelObject();

		int position = 1;
		for (ModelElement field : ModelUtils.getFieldList(element))
		{
			String elementName = field.getElementName();

			String columnName = null;
			Object columnNameVal = field.getProperty(BuiltinProperties.COLUMN_NAME);

			if (columnNameVal instanceof String)
			{
				String columnNameStringVal = (String) columnNameVal;
				if (columnNameStringVal.length() == 0)
					columnName = Misc.sqlize(elementName);
				else
					columnName = Misc.sqlize(columnNameStringVal);
			}
			else
				// columnNameVal == null
				columnName = Misc.sqlize(elementName);

			ModelId dataType = field.getRefId();

			String columnType = null;
			Object columnTypeVal = field.getProperty(BuiltinProperties.COLUMN_TYPE);

			if (columnTypeVal instanceof String)
				columnType = (String) columnTypeVal;
			else if (columnTypeVal == null)
			{
				int columnTypeNumber = SQLUtils.convertFromTersusTypeToSQLType(dataType);
				columnType = SQLUtils.getSQLTypeName(columnTypeNumber);
			}

			Boolean primaryKey = null;
			Object primaryKeyVal = field.getProperty(BuiltinProperties.PRIMARY_KEY);

			if (primaryKeyVal instanceof Boolean)
				primaryKey = (Boolean) primaryKeyVal;

			Boolean nullable = null;
			Object nullableVal = field.getProperty(BuiltinProperties.NULLABLE);

			if (nullableVal instanceof Boolean)
				nullable = (Boolean) nullableVal;

			String columnSize = null;
			Object columnSizeVal = field.getProperty(BuiltinProperties.COLUMN_SIZE);

			if (columnSizeVal instanceof String)
				columnSize = (String) columnSizeVal;
			
			String defaultValue = null;
			Object defaultValueVal = field.getProperty(BuiltinProperties.DEFAULT_VALUE);

			if (defaultValueVal instanceof String)
				defaultValue = (String) defaultValueVal;


			String status = null;

			initialFields.add(new TersusTableFieldDescriptor(field, elementName, columnName,
					dataType, columnType, primaryKey, nullable, columnSize, defaultValue, null, status, position));
			position ++ ;
		}
	}

	public void setFields(List<TersusTableFieldDescriptor> fieldList)
	{
		fields = new ArrayList<TersusTableFieldDescriptor>();
		fields.addAll(fieldList);
	}

	public void setNewDetails(String newDataSource, String schema, String tableName,
			String catalogName, ArrayList<TersusTableFieldDescriptor> fields)
	{
		this.dataSource = newDataSource;
		this.schema = schema;
		this.tableName = tableName;

		if (catalogName != null && catalogName.length() > 0)
			this.catalog = catalogName;

		this.fields = fields;
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model.commands;

import tersus.editor.layout.LayoutConstraint;
import tersus.model.BuiltinModels;
import tersus.model.BuiltinPlugins;
import tersus.model.BuiltinProperties;
import tersus.model.DataElement;
import tersus.model.DataType;
import tersus.model.FlowDataElementType;
import tersus.model.FlowModel;
import tersus.model.FlowType;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.ModelUtils;
import tersus.model.Repository;
import tersus.model.Slot;
import tersus.util.Misc;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public class AddElementHelper
{
	public static ModelElement getNewElement(Model parentModel, ModelId childModelId,
			String newElementName, LayoutConstraint constraint, ModelElement element)
	{
		ModelElement newElement = null;
		WorkspaceRepository repository = (WorkspaceRepository) parentModel.getRepository();

		if (parentModel instanceof FlowModel)
		{
			if (parentIsProcess(parentModel)
					&& (element != null ? ModelUtils.isDisplayElement(element)
							: modelIsDisplay(repository.getModel(childModelId, true))
									&& !(element instanceof Slot)))
			{
				if (element != null)
				{
					newElement = new DataElement();

					newElement.setRefId(childModelId);
					newElement.setPosition(element.getPosition());
					newElement.setSize(element.getSize());
					newElement.setRole(element.getRole());
				}
				else
				{
					Misc.assertion(constraint != null); // Or element or constraint must be with a value
					newElement = createDataElement(parentModel, childModelId, newElementName,
							constraint);
				}
				
			}
			else if (element != null)
				newElement = copyElement(element, childModelId);
			else if (constraint != null)
			{
				newElement = ((FlowModel) parentModel).createChild(childModelId, newElementName);
				newElement.setPosition(constraint.getPosition());
				newElement.setSize(constraint.getSize());
				newElement.setRole(parentModel.defaultRole(newElementName));
			}
		}
		else if (element instanceof Slot && parentModel instanceof DataType)
		{
			Slot slot = (Slot)element;
			newElement = new DataElement();
			
			if (slot.getRefId() != null)
				newElement.setRefId(slot.getRefId());
			else
				newElement.setRefId(BuiltinModels.ANYTHING_ID);
			
			newElement.setRole(parentModel.defaultRole(newElementName));
		}
		else
		// If parent isn't flow model
		{
			if (element != null)
				newElement = copyElement(element, childModelId);
			else
			{
				newElement = new DataElement();
				newElement.setRefId(childModelId);
				newElement.setProperty(BuiltinProperties.MANDATORY, Boolean.TRUE);
				newElement.setRole(parentModel.defaultRole(newElementName));
			}
		}
		return newElement;
	}

	private static ModelElement copyElement(ModelElement element, ModelId childModelId)
	{
		ModelElement newElement = Repository.copyElement(element);
		if (newElement.getRefId() != null && !newElement.getRefId().equals(childModelId))
			newElement.setProperty(BuiltinProperties.REF_ID, childModelId);
		
		return newElement;
	}
	
	public static DataElement createDataElement(Model parentModel, ModelId childModelId,
			String newElementName, LayoutConstraint constraint)
	{
		DataElement newDataElement = new DataElement();
		newDataElement.setType(FlowDataElementType.INTERMEDIATE);
		newDataElement.setRefId(childModelId);
		newDataElement.setRole(parentModel.defaultRole(newElementName));
		newDataElement.setPosition(constraint.getPosition());
		newDataElement.setSize(constraint.getSize());
		return newDataElement;
	}

	public static AddElementAndPropagateSlotTypeCommand getAddElementCommand(Model parentModel,
			ModelId newModelId, String newElementName, LayoutConstraint constraint, Integer index)
	{
		ModelElement element = getNewElement(parentModel, newModelId, newElementName, constraint,
				null);
		AddElementAndPropagateSlotTypeCommand cmd = new AddElementAndPropagateSlotTypeCommand(
				parentModel, element, true);

		if (index != null)
			cmd.setIndex(index.intValue());

		return cmd;
	}

	public static CreateElementFromTemplateCommand getCreateElementFromTemplateCommand(
			Model parentModel, Model addedModel, String elementName, LayoutConstraint constraint,
			Integer index)
	{
		CreateElementFromTemplateCommand cmd = new CreateElementFromTemplateCommand(
				(WorkspaceRepository) parentModel.getRepository());

		cmd.setParent(parentModel);
		cmd.setTemplateModel(addedModel);

		if (elementName == null)
			cmd.setSuggestedName(parentModel.defaultRole(addedModel.getName()).toString());
		else
			cmd.setSuggestedName(parentModel.defaultRole(elementName).toString());

		if (constraint != null)
			cmd.setLayoutConstraint(constraint);

		if (index != null)
			cmd.setIndex(index);

		return cmd;
	}

	public static boolean parentIsProcess(Model parentModel)
	{
		Object parentModelType = parentModel.getProperty(BuiltinProperties.TYPE);

		boolean parentIsProcess = FlowType.ACTION == parentModelType
				|| FlowType.SERVICE == parentModelType
				|| BuiltinPlugins.BUTTON.equals(parentModel.getPlugin());

		return parentIsProcess;
	}

	public static boolean modelIsDisplay(Model childModel)
	{
		boolean childIsDisplay = childModel instanceof FlowModel
				&& FlowType.DISPLAY == childModel.getProperty(BuiltinProperties.TYPE)
				&& !(childModel.getPluginDescriptor() != null && childModel.getPluginDescriptor()
						.isDisplayAction());

		return childIsDisplay;
	}
}

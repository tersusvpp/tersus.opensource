package tersus.model.commands;

import tersus.model.IRepository;
import tersus.model.ModelObject;

public abstract class ModelObjectRef
{
	protected IRepository repository;

	public ModelObjectRef(IRepository repository)
	{
		this.repository = repository;
	}

	public abstract ModelObject getModelObject();
}

/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model.commands;

import java.util.ArrayList;

import org.eclipse.gef.commands.CompoundCommand;

import tersus.model.BuiltinProperties;
import tersus.model.Link;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.model.Slot;
import tersus.model.indexer.RepositoryIndex;
import tersus.model.validation.Problems;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public class ClearIgnoredProblemHelper
{
	public static CompoundCommand getNewIgnoredProblemValue(String propertyName,
			ModelObject modelObject, WorkspaceRepository repository)
	{
		CompoundCommand cmd = new CompoundCommand();
		if (BuiltinProperties.SOURCE.equals(propertyName)
				|| BuiltinProperties.TARGET.equals(propertyName))
		{
			SetPropertyValueCommand c = clearIgnoredProblem(modelObject, Problems.INCONSISTENT_FLOW);

			if (c != null)
				cmd.add(c);
		}

		if (BuiltinProperties.REPETITIVE.equals(propertyName))
		{
			RepositoryIndex index = repository.getUpdatedRepositoryIndex(null);
			ArrayList<Link> links = new ArrayList<Link>();
			if (modelObject instanceof Slot)
			{
				links.addAll(index.getIncomingLinks((Slot) modelObject, true));
				links.addAll(index.getOutgoingLinks((Slot) modelObject, true));
			}
			else if (modelObject instanceof ModelElement)
			{
				links.addAll(index.getIncomingLinks((ModelElement) modelObject, false));
				links.addAll(index.getOutgoingLinks((ModelElement) modelObject, false));
			}

			for (Link link : links)
			{
				SetPropertyValueCommand c = clearIgnoredProblem(link, Problems.INCONSISTENT_FLOW);
				if (c != null)
					cmd.add(c);
			}
		}

		if (BuiltinProperties.MANDATORY.equals(propertyName))
		{
			SetPropertyValueCommand c = clearIgnoredProblem(modelObject, Problems.MISSING_FLOW);

			if (c != null)
				cmd.add(c);
		}

		if (!cmd.getCommands().isEmpty())
			return cmd;
		else
			return null;
	}

	private static SetPropertyValueCommand clearIgnoredProblem(ModelObject modelObject,
			String problemToRemove)
	{
		Object ignoredProblemsObjectVal = modelObject
				.getProperty(BuiltinProperties.IGNORED_PROBLEMS);

		if (ignoredProblemsObjectVal != null && ignoredProblemsObjectVal instanceof String
				&& ((String) ignoredProblemsObjectVal).length() > 0)
		{
			String ignoredProblems = (String) ignoredProblemsObjectVal;
			int problemIndex = ignoredProblems.indexOf(problemToRemove);

			if (problemIndex != -1)
			{
				String s1 = ignoredProblems.substring(0, problemIndex);
				String s2 = "";

				int index = problemIndex + problemToRemove.length() + 1;
				if (index == ignoredProblems.length())
					s2 = ignoredProblems.substring(0, index);

				return new SetPropertyValueCommand(modelObject, BuiltinProperties.IGNORED_PROBLEMS,
						s1.concat(s2));
			}
		}

		return null;
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model.commands;

import tersus.model.Model;
import tersus.model.Package;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 * @author Liat Shiff
 */
public class DeleteModelAndPackageCommand extends MultiStepCommand
{
	private ModelRef modelRef;

	public DeleteModelAndPackageCommand(Model model)
	{
		super((WorkspaceRepository) model.getRepository(), "Delete " + model.getId().getPath());
		modelRef = new ModelRef(model);
	}

	public void run()
	{
		Model model = (Model) modelRef.getModelObject();
		deleteModel(model);
		Package pkg = model.getPackage();

		if (pkg.getModels().size() == 0
				&& repository.getSubPackageIds(pkg.getPackageId()).size() == 0)
			deletePackage(pkg.getPackageId());
	}
}

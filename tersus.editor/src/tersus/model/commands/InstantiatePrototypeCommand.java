/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
package tersus.model.commands;

import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import tersus.model.BuiltinProperties;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.ModelUtils;
import tersus.model.Package;
import tersus.model.PackageId;
import tersus.model.Repository;
import tersus.model.Role;
import tersus.model.TemplateAndPrototypeSupport;
import tersus.model.indexer.RepositoryIndex;
import tersus.util.Misc;
import tersus.util.Multiplicity;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 */
public abstract class InstantiatePrototypeCommand extends MultiStepCommand
{
	private static final int REUSE_FULL = 1;

	private static final int REUSE_INTERNAL = 2;

	private static final int REUSE_NONE = 3;

	private Stack<ModelElement> elementStack;

	private RepositoryIndex repositoryIndex;

	private HashMap<PackageId, Boolean> prototypePackages;

	protected InstantiatePrototypeCommand(WorkspaceRepository repository, String label)
	{
		super(repository, label);
	}

	public abstract void run();

	protected void init()
	{
		elementStack = new Stack<ModelElement>();
		prototypePackages = new HashMap<PackageId, Boolean>();
		loadReferences();
	}

	private void loadReferences()
	{
		repositoryIndex = repository.getUpdatedRepositoryIndex();
	}

	/**
	 * Get rid of uneeded references
	 */
	protected void cleanup()
	{
		repositoryIndex = null;
		prototypePackages = null;
	}

	/**
	 * @param prototypeModel
	 * @param newModelId
	 */
	protected void createModelFromPrototype(Model prototypeModel, ModelId newModelId)
	{
		createPackageIfMissing(newModelId.getPackageId());

		addNewModel(newModelId, prototypeModel.getId(), false);
		Model newModel = repository.getModel(newModelId, true);
		setPrototype(newModel, prototypeModel);

		List<ModelElement> elements = prototypeModel.getElements();
		for (int i = 0; i < elements.size(); i++)
		{
			ModelElement element = (ModelElement) elements.get(i);
			int nInstances = (getMultiplicity(element)).def;
			if (elementStack.contains(element))
				throw new PrototypeInstantiationException(
						"Infinite recursion encountered on element " + element.getRole() + " of "
								+ prototypeModel.getId());
			elementStack.push(element);
			for (int j = 1; j <= nInstances; j++)
			{
				addElementFromPrototype(newModel, element);
			}
			elementStack.pop();
		}
	}

	private void createPackageIfMissing(PackageId packageId)
	{
		if (!packageExists(packageId))
		{
			if (packageId.getParent() != null)
				createPackageIfMissing(packageId.getParent());
			createPackage(packageId);
		}
	}

	/**
	 * @param newModel
	 * @param prototypeElement
	 */
	protected ModelElement addElementFromPrototype(Model newModel, ModelElement prototypeElement)
	{
		int reuse = classifyReuse(prototypeElement);
		switch (reuse)
		{
			case REUSE_FULL:
				return addElementWithFullReuse(newModel, prototypeElement);
			case REUSE_INTERNAL:
				return addElementWithInternalReuse(newModel, prototypeElement);
			default:
				Misc.assertion(reuse == REUSE_NONE);
				return addElementWithNoReuse(newModel, prototypeElement);
		}
	}

	private ModelElement addElementWithNoReuse(Model parentModel, ModelElement prototypeElement)
	{
		PackageId prototypePackageId = prototypeElement.getParentModel().getId().getPackageId();
		PackageId prototypeElementPackageId = prototypeElement.getRefId().getPackageId();
		boolean createNewPackage = !prototypeElementPackageId.equals(prototypePackageId);
		ModelId newModelId;
		if (createNewPackage)
		{
			PackageId targetPackageId = createNewPackageId(parentModel, prototypeElement);
			newModelId = new ModelId(targetPackageId, targetPackageId.getName());
		}
		else
		{
			PackageId targetPackageId = parentModel.getId().getPackageId();
			newModelId = findFreeModelId(parentModel, prototypeElement, targetPackageId);
		}
		System.out.println("Instantiating " + prototypeElement.getRefId() + " as " + newModelId);
		return addElementWithNewModel(parentModel, prototypeElement, newModelId);

	}

	private ModelElement addElementWithNewModel(Model parentModel, ModelElement prototypeElement,
			ModelId newModelId)
	{
		Model referredModel = prototypeElement.getReferredModel();
		if (TemplateAndPrototypeSupport.isStrictTemplate(referredModel))
			createModelFromTemplate(referredModel.getId(), newModelId);
		else
			createModelFromPrototype(referredModel, newModelId);
		
		return createAndAddNewElement(parentModel, prototypeElement, newModelId);
	}

	private ModelId findFreeModelId(Model newModel, ModelElement prototypeElement,
			PackageId targetPackageId)
	{
		String baseName = prototypeElement.getRefId().getName();
		ModelId modelId = null;
		int i = 0;
		while (modelId == null)
		{
			++i;
			String name = baseName;
			if (i > 1)
			{
				name += " " + i;
			}
			modelId = new ModelId(targetPackageId, name);
			if (repository.getModel(modelId, false) != null)
				modelId = null;
		}
		return modelId;

	}

	private PackageId createNewPackageId(Model newModel, ModelElement prototypeElement)
	{
		String baseName = prototypeElement.getRole().toString();
		PackageId packageId;
		boolean startFrom1 = getMultiplicity(prototypeElement).max > 1;
		int i = 0;
		while (true)
		{
			++i;
			String name = baseName;

			if (i > 1 || startFrom1)
				name += " " + i;

			packageId = new PackageId(newModel.getId().getPackageId(), name);
			if (newModel.getElement(Role.get(name)) == null
					&& repository.getPackage(packageId) == null)
				return packageId;
		}

	}

	private ModelElement addElementWithInternalReuse(Model newModel, ModelElement prototypeElement)
	{
		// Missing: searching for the element in the parent model (before
		// searching by package)
		PackageId targetPackageId = calculateTargetPackageId(newModel, prototypeElement);
		Model existingModel = findPrototypeInstanceInPackage(prototypeElement, targetPackageId);
		if (existingModel != null)
			return createAndAddNewElement(newModel, prototypeElement, existingModel.getId());
		else
		{
			ModelId newModelId = findFreeModelId(newModel, prototypeElement, targetPackageId);
			return addElementWithNewModel(newModel, prototypeElement, newModelId);
		}
	}

	private Model findPrototypeInstanceInPackage(ModelElement prototypeElement,
			PackageId targetPackageId)
	{
		Package pkg = repository.getPackage(targetPackageId);
		Model instance = null;
		if (pkg == null)
			return null;
		List<Model> models = pkg.getModels();
		for (int i = 0; i < models.size(); i++)
		{
			Model model = (Model) models.get(i);
			if (prototypeElement.getRefId().toString().equals(
					model.getProperty(BuiltinProperties.PROTOTYPE)))
			{
				if (instance != null)
				{
					throw new PrototypeInstantiationException("2 instances of protoype '"
							+ prototypeElement.getRefId() + "' were found in package '"
							+ targetPackageId + "'");
				}
				instance = model;
			}
		}
		return instance;
	}

	private PackageId calculateTargetPackageId(Model newModel, ModelElement prototypeElement)
	{
		Model prototypeModel = prototypeElement.getParentModel();
		PackageId prototypePackageId = prototypeModel.getId().getPackageId();
		PackageId newModelPackageId = newModel.getId().getPackageId();

		PackageId prototypeContainingPackageId = prototypePackageId;
		PackageId targetContainingPackageId = newModelPackageId;
		while (!prototypeContainingPackageId.isAncestorOf(prototypeElement.getRefId()))
		{
			prototypeContainingPackageId = prototypeContainingPackageId.getParent();
			targetContainingPackageId = targetContainingPackageId.getParent();
			if (targetContainingPackageId == null)
				throw new PrototypeInstantiationException("The target model " + newModel.getId()
						+ " has an incompatible package structure with the prototype "
						+ prototypeModel.getId());
			if (prototypeContainingPackageId == null)
				throw new PrototypeInstantiationException("The prototype model "
						+ prototypeModel.getId() + " has an invalid element '"
						+ prototypeElement.getRole() + "' (no common package)");
		}
		PackageId refPackageId = prototypeElement.getRefId().getPackageId();
		String refSuffix = refPackageId.toString().substring(
				prototypeContainingPackageId.toString().length());
		PackageId targetPackageId = new PackageId(targetContainingPackageId.toString() + refSuffix);
		return targetPackageId;
	}

	private ModelElement addElementWithFullReuse(Model newModel, ModelElement prototypeElement)
	{
		ModelId refId = prototypeElement.getRefId();
		return createAndAddNewElement(newModel, prototypeElement, refId);
	}

	private ModelElement createAndAddNewElement(Model newModel, ModelElement prototypeElement,
			ModelId refId)
	{
		ModelElement newElement = Repository.copyElement(prototypeElement);
		Role role = prototypeElement.getRole();
		if (role.isDistinguished() && newModel.getElement(role) != null)
			throw new PrototypeInstantiationException("Can't add element " + role
					+ " (an element with the same name aleady exists in " + newModel.getId() + ")");
		/*
		 * if (prototypeElement.getRefId() != null && role.toString()
		 * .equals(prototypeElement.getRefId().getName())) { / Since the element name and model name
		 * are the same in the prototype, we attempt to acheive the same in the instantiated model
		 * 
		 * Role modelNameRole = Role.get(refId.getName()); if (newModel.getElement(modelNameRole) ==
		 * null) role = modelNameRole; }
		 */
		String baseName;
		if (prototypeElement.getRefId() != null && prototypeElement.getRole().toString().equals(prototypeElement.getRefId().getName()))
		{
			//If prototype had role = name, we try to maintain in new model (even if new model name is different)
			baseName = refId.getName();
		}
		else
			baseName = role.toString();
		newElement.setRole(newModel.defaultRole(baseName));
		if (!newElement.getRole().isDistinguished())
			if (!getMultiplicity(prototypeElement).isExample)
				newElement.setProperty(BuiltinProperties.PROTOTYPE_ELEMENT, prototypeElement
						.getRole().toString());
		newElement.setRefId(refId);
		newElement.setPosition(ModelUtils.findNonOverlappingPosition(newModel, newElement));
		addElement(newModel, newElement, null, null, null, true);
		return newElement;
	}

	private int classifyReuse(ModelElement prototypeElement)
	{
		if (prototypeElement.getRefId() == null)
			return REUSE_FULL; // No model is the same as a reused model
		if (childIsInSamePackage(prototypeElement)
				&& (getMultiplicity(prototypeElement).max > 1 || !isReused(prototypeElement)))
			return REUSE_NONE;
		else if (!isPrototypePackage(prototypeElement.getRefId().getPackageId()))
			return REUSE_FULL;
		else
			return REUSE_INTERNAL;
	}

	private boolean isReused(ModelElement prototypeElement)
	{
		return (getReferences(prototypeElement.getRefId()).size() > 1);
	}

	private List<ModelElement> getReferences(ModelId refId)
	{
		return repositoryIndex.getReferences(refId);
	}

	private boolean isPrototypePackage(PackageId packageId)
	{
		Boolean result = (Boolean) prototypePackages.get(packageId);
		if (result == null)
		{
			result = Boolean.FALSE;
			for (PackageId current = packageId; current != null; current = current.getParent())
			{
				if (packageContainsPrototypes(current))
				{
					result = Boolean.TRUE;
					break;
				}
			}
			prototypePackages.put(packageId, result);
		}
		return result.booleanValue();
	}

	private boolean packageContainsPrototypes(PackageId packageId)
	{
		Package pkg = repository.getPackage(packageId);
		List<Model> models = pkg.getModels();
		for (int i = 0; i < models.size(); i++)
		{
			Model model = (Model) models.get(i);
			if (TemplateAndPrototypeSupport.PROTOTYPE.equals(model
					.getProperty(BuiltinProperties.TEMPLATE)))
				return true;
		}
		return false;
	}

	private boolean childIsInSamePackage(ModelElement prototypeElement)
	{
		return prototypeElement.getParentModel().getId().getPackageId().isAncestorOf(
				prototypeElement.getRefId());
	}

	private Multiplicity getMultiplicity(ModelElement prototypeElement)
	{
		return new Multiplicity((String) prototypeElement
				.getProperty(BuiltinProperties.MULTIPLICITY));
	}

	private void setPrototype(Model newModel, Model prototype)
	{

		// Won't work correctly if the "root" is a template and not a prototype
		if (!TemplateAndPrototypeSupport.TRUE.equals(prototype
				.getProperty(BuiltinProperties.TEMPLATE)))
			newModel.setProperty(BuiltinProperties.PROTOTYPE, prototype.getId().toString());
	}

	private void createPackage(PackageId packageId)
	{
		PackageId parent = packageId.getParent();
		if (parent != null && repository.getPackage(parent) == null)
			createPackage(packageId);

		addNewPackage(packageId);
	}

	private boolean packageExists(PackageId packageId)
	{
		return repository.getPackage(packageId) != null;
	}
}
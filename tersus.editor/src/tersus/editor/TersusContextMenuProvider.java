/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor;

import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.gef.ui.actions.GEFActionConstants;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.actions.ActionFactory;

import tersus.editor.actions.TersusActionConstants;


public class TersusContextMenuProvider extends org.eclipse.gef.ContextMenuProvider
{
	private ActionRegistry actionRegistry;

	public TersusContextMenuProvider(EditPartViewer viewer, ActionRegistry registry) 
	{
		super(viewer);
		setActionRegistry(registry);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.gef.ContextMenuProvider#menuAboutToShow(org.eclipse.jface.action.IMenuManager)
	 */
	public void buildContextMenu(IMenuManager manager) {
		addActionGroups(manager);
	
		IAction action;
	
	//	action = getActionRegistry().getAction(GEFActionConstants.UNDO);
	//	manager.appendToGroup(GEFActionConstants.GROUP_UNDO, action);
	//
	//	action = getActionRegistry().getAction(GEFActionConstants.REDO);
	//	manager.appendToGroup(GEFActionConstants.GROUP_UNDO, action);
	
		
		
		action = getActionRegistry().getAction(TersusActionConstants.OPEN_IN_A_NEW_TAB_ACTION_ID);
		if (action.isEnabled())
			manager.appendToGroup(GEFActionConstants.GROUP_VIEW, action);
	
		action = getActionRegistry().getAction(TersusActionConstants.SHOW_IN_REPOSITORY_ACTION_ID);
		if (action.isEnabled())
			manager.appendToGroup(GEFActionConstants.GROUP_VIEW, action);
		
		action = getActionRegistry().getAction(TersusActionConstants.OPEN_ACTION_ID);
		if (action.isEnabled())
			manager.appendToGroup(GEFActionConstants.GROUP_VIEW, action);
	
	    action = getActionRegistry().getAction(TersusActionConstants.SHOW_USAGES_ACTION_ID);
	    if (action.isEnabled())
	        manager.appendToGroup(GEFActionConstants.GROUP_VIEW, action);
		
		action = getActionRegistry().getAction(TersusActionConstants.SHOW_SHOW_LINKS_ID);
		if (action.isEnabled())
			manager.appendToGroup(GEFActionConstants.GROUP_VIEW, action);

		action = getActionRegistry().getAction(ActionFactory.COPY.getId());
	    if (action.isEnabled())
	        manager.appendToGroup(GEFActionConstants.GROUP_COPY, action);
	    
	    action = getActionRegistry().getAction(TersusActionConstants.COPY_ELEMENT_NAME_ACTION_ID);
	    if (action.isEnabled())
	        manager.appendToGroup(GEFActionConstants.GROUP_COPY, action);
	
		action = getActionRegistry().getAction(TersusActionConstants.PASTE_ELEMENTS_ACTION_ID);
	    if (action.isEnabled())
	        manager.appendToGroup(GEFActionConstants.GROUP_COPY, action);
	
	    action = getActionRegistry().getAction(TersusActionConstants.PASTE_CLONE_ACTION_ID);
	    if (action.isEnabled())
	        manager.appendToGroup(GEFActionConstants.GROUP_COPY, action);
	    
	    action = getActionRegistry().getAction(TersusActionConstants.ENCAPSULATE_ACTION_ID);
	    if (action.isEnabled())
	        manager.appendToGroup(GEFActionConstants.GROUP_COPY, action);
	
	    action = getActionRegistry().getAction(TersusActionConstants.DECAPSULATE_ACTION_ID);
	    if (action.isEnabled())
	        manager.appendToGroup(GEFActionConstants.GROUP_COPY, action);
	    
	    action = getActionRegistry().getAction(TersusActionConstants.MOVE_ELEMENT_ACTION_ID);
	    if (action.isEnabled())
	        manager.appendToGroup(GEFActionConstants.GROUP_COPY, action);
	    
	    action = getActionRegistry().getAction(TersusActionConstants.REMOVE_ELEMENT_ACTION_ID);
	    if (action.isEnabled())
	        manager.appendToGroup(GEFActionConstants.GROUP_COPY, action);
	    
	    action = getActionRegistry().getAction(TersusActionConstants.MOVE_MODELS_ACTION_ID);
	    if (action.isEnabled())
	        manager.appendToGroup(GEFActionConstants.GROUP_COPY, action);
	    
	    action = getActionRegistry().getAction(TersusActionConstants.REMOVE_INVALID_FLOWS_ID);
	    if (action.isEnabled())
	        manager.appendToGroup(GEFActionConstants.GROUP_COPY, action);
	
	    action = getActionRegistry().getAction(TersusActionConstants.RENAME_ACTION_ID);
		if (action.isEnabled())
			manager.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
		
		action = getActionRegistry().getAction(TersusActionConstants.REPLACE_WITH_ACTION_ID);
		if (action.isEnabled())
			manager.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
		
		action = getActionRegistry().getAction(TersusActionConstants.CHANGE_PLUGIN_ACTION_ID);
		if (action.isEnabled())
			manager.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
		
		action = getActionRegistry().getAction(TersusActionConstants.TEMP_APPLY_TABLE_WIZARD_ID);
		if (action.isEnabled())
			manager.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
	
		action = getActionRegistry().getAction(TersusActionConstants.IMPORT_STRUCTURE_FROM_DB_ID);
	    if (action.isEnabled())
	        manager.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
	
	    action = getActionRegistry().getAction(TersusActionConstants.EDIT_NOTE_ID);
		if (action.isEnabled())
			manager.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
	
		action = getActionRegistry().getAction(TersusActionConstants.TOGGLE_MANDATORY);
		if (action.isEnabled())
			manager.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
	
		action = getActionRegistry().getAction(TersusActionConstants.TOGGLE_REPETITIVE);
		if (action.isEnabled())
			manager.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
			
		action = getActionRegistry().getAction(TersusActionConstants.TOGGLE_ALWAYS_CREATE);
		if (action.isEnabled())
			manager.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
		
		action = getActionRegistry().getAction(TersusActionConstants.SET_FLOW_DATA_ELEMENT_TYPE);
		if (action.isEnabled())
			manager.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
	
	    action = getActionRegistry().getAction(TersusActionConstants.TOGGLE_ELEMENT_TYPE);
	    if (action.isEnabled())
	        manager.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
	    
	    action = getActionRegistry().getAction(TersusActionConstants.ADD_PROPERTY_ACTION_ID);
	    if (action.isEnabled())
	        manager.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
	
		action = getActionRegistry().getAction(TersusActionConstants.ADD_ANCESTOR_REFERENCE_ACTION_ID);
		if (action.isEnabled())
			manager.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
	
	    action = getActionRegistry().getAction(TersusActionConstants.ADD_ELEMENT_ACTION_ID);
		if (action.isEnabled())
			manager.appendToGroup(GEFActionConstants.GROUP_EDIT, action); 
	    
		action = getActionRegistry().getAction(TersusActionConstants.ANALYZE_DEPENDENCIES_ACTION_ID);
			manager.appendToGroup(GEFActionConstants.GROUP_REST, action); 
			
		action = getActionRegistry().getAction(TersusActionConstants.HELP_ACTION_ID);
			manager.appendToGroup(GEFActionConstants.GROUP_HELP, action);

		action = getActionRegistry().getAction(TersusActionConstants.HORIZONTAL_ALIGN_ACTION_ID);
		    if (action.isEnabled())
		        manager.appendToGroup(GEFActionConstants.GROUP_COPY, action);

		action = getActionRegistry().getAction(TersusActionConstants.VERTICAL_ALIGN_ACTION_ID);
		    if (action.isEnabled())
		        manager.appendToGroup(GEFActionConstants.GROUP_COPY, action);

		action = getActionRegistry().getAction(TersusActionConstants.SIZE_ALIGN_ACTION_ID);
		    if (action.isEnabled())
		        manager.appendToGroup(GEFActionConstants.GROUP_COPY, action);
	
	//	action = getActionRegistry().getAction(IWorkbenchActionConstants.PASTE);
	//	if (action.isEnabled())
	//		manager.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
	//
	//	action = getActionRegistry().getAction(IWorkbenchActionConstants.DELETE);
	//	if (action.isEnabled())
	//		manager.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
	//
	//	action = getActionRegistry().getAction(GEFActionConstants.DIRECT_EDIT);
	//	if (action.isEnabled())
	//		manager.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
	//
		
	//	action = getActionRegistry().getAction(IWorkbenchActionConstants.SAVE);
	//	manager.appendToGroup(GEFActionConstants.GROUP_SAVE, action);
	
	}
	
	private ActionRegistry getActionRegistry() 
	{
		return actionRegistry;
	}
	
	private void setActionRegistry(ActionRegistry registry) 
	{
		actionRegistry = registry;
	}
	
	private void addActionGroups(IMenuManager menu)
	{
		menu.add(new Separator(GEFActionConstants.GROUP_VIEW));
		menu.add(new Separator(GEFActionConstants.GROUP_UNDO));
		menu.add(new Separator(GEFActionConstants.GROUP_COPY));
		menu.add(new Separator(GEFActionConstants.GROUP_EDIT));
		menu.add(new Separator(GEFActionConstants.GROUP_REST));
		menu.add(new Separator(GEFActionConstants.GROUP_HELP));

//		Unused groups
//		menu.add(new Separator(GEFActionConstants.GROUP_FIND));
//		menu.add(new Separator(GEFActionConstants.GROUP_ADD));
//		menu.add(new Separator(GEFActionConstants.MB_ADDITIONS));
//		menu.add(new Separator(GEFActionConstants.GROUP_SAVE));
	}

}
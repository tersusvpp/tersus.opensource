/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.usages;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;

/**
 * @author Liat Shiff
 */
public class UsagesViewSort extends ViewerSorter
{
	public static final String BASE_MODEL_SORT = "Base Model";
	public static final String TYPE_SORT = "Type";
	public static final String PATH_SORT = "Path";
	
	private String column = null;
	private int dir = SWT.DOWN;

	public UsagesViewSort(String column, int dir)
	{
		super();
		this.column = column;
		this.dir = dir;
	}

	public int compare(Viewer viewer, Object e1, Object e2)
	{
		int returnValue = 0;

		if (BASE_MODEL_SORT.equals(column))
			returnValue = ((Entry) e1).getBaseModel().getName().compareTo(((Entry) e2).getBaseModel().getName());
		if (TYPE_SORT.equals(column))
			returnValue = ((Entry) e1).getType().compareTo(((Entry) e2).getType());
		if (PATH_SORT.equals(column))
			returnValue = ((Entry) e1).getPath().toString().compareTo(((Entry) e2).getPath().toString());

		if (this.dir == SWT.DOWN)
			returnValue = returnValue * -1;

		return returnValue;
	}
}

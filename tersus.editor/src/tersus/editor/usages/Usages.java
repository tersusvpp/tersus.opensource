/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.usages;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Stack;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;

import tersus.model.DataElement;
import tersus.model.FlowDataElementType;
import tersus.model.FlowModel;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.Path;
import tersus.model.Role;
import tersus.model.Slot;
import tersus.model.SubFlow;
import tersus.model.indexer.RepositoryIndex;
import tersus.workbench.WorkspaceRepository;

public class Usages
{
    private Model model;

    private Entry[] usages;

    private RepositoryIndex index;

    public Usages(Model model, IProgressMonitor monitor)
    {
        this.model = model;
        calculate(monitor);
    }

    private void calculate(IProgressMonitor monitor)
    {
        
        monitor.beginTask("Calculating usages", 100);
        SubProgressMonitor subMonitor  = new SubProgressMonitor(monitor, 50);
        index = ((WorkspaceRepository) model.getRepository()).getUpdatedRepositoryIndex(subMonitor);
        List<ModelElement> references = index.getReferences(model.getId());
        ArrayList<Entry> usageList = new ArrayList<Entry>();
        for (ModelElement e: references)
        {
        	usageList.add(createEntry(e));
            int work = 50/references.size();
            monitor.worked(work);
        }
        usages = (Entry[]) usageList.toArray(new Entry[usageList.size()]);
        monitor.done();
    }


    private Entry createEntry(ModelElement e)
    {
        Model baseModel = e.getParentModel();
        Stack<Role> pathSegments = new Stack<Role>();
        boolean isReadOnly = e.getParentModel().isReadOnly();
        pathSegments.push(e.getRole());
        HashSet<ModelId> checkedModelIds = new HashSet<ModelId>();
        
        while (true)
        {
            List<ModelElement> references = index.getReferences(baseModel.getId());
            
            if (references.size() == 1 && ! checkedModelIds.contains(baseModel.getId()))
            {
               checkedModelIds.add(baseModel.getId());  
               ModelElement ref = (ModelElement)references.get(0);
               pathSegments.push(ref.getRole());
               baseModel = ref.getParentModel();
            }
            else
            {
                Path path = new Path();
                while (!pathSegments.isEmpty())
                {
                    path.addSegment((Role)pathSegments.pop());
                }
                Entry entry = new Entry(baseModel, references.size(), getType(e), path, isReadOnly);
                return entry;
            }
        }

    }

    private String getType(ModelElement element)
    {
        if (element instanceof DataElement)
        {
        	DataElement dataElemement = (DataElement)element;
        	if (FlowDataElementType.PARENT.equals(dataElemement.getType()))
        		return "Data Element (Parent Reference)";
        	
            return "Data Element";
        }
        if (element instanceof Slot)
            return ((Slot)element).getType().toString();
        if (element instanceof SubFlow)
        {
            return (((FlowModel)element.getReferredModel()).getType().toString());
        }
        return null;
    }

    public Entry[] getUsages()
    {
        return usages;
    }
}

/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.usages;

import tersus.model.Model;
import tersus.model.Path;

public class Entry
{
    private Model baseModel;
    
    private int nOccurences;
    
    private String type;
    
    private Path path;
    
    private boolean isReadOnly;
    
    public Entry(Model baseModel, int occurences, String type, Path path, boolean isReadOnly)
    {
        super();
        this.baseModel = baseModel;
        nOccurences = occurences;
        this.type = type;
        this.path = path;
        this.isReadOnly = isReadOnly;
    }
    
    public Model getBaseModel()
    {
        return baseModel;
    }
    
    public void setBaseModel(Model baseModel)
    {
        this.baseModel = baseModel;
    }
    
    public int getNOccurences()
    {
        return nOccurences;
    }
    
    public void setNOccurences(int occurences)
    {
        nOccurences = occurences;
    }
    
    public Path getPath()
    {
        return path;
    }
    
    public void setPath(Path path)
    {
        this.path = path;
    }
    
    public String getType()
    {
        return type;
    }
    
    public void setType(String type)
    {
        this.type = type;
    }
    
    public boolean isReadOnly()
    {
        return isReadOnly;
    }
    
    public void setIsReadOnly(boolean isReadOnly)
    {
        this.isReadOnly = isReadOnly;
    }
    
    public String getPathStr()
    {
        return path == null? null:path.toString();
    }
    
    public boolean equals(Object o)
    {
        return (this == o); // We don't worry about real equality - it's OK to have a false negative here
    }
}

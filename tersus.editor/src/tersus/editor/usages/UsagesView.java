/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.usages;

import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import tersus.editor.SyncableView;
import tersus.workbench.SelectionHelper;

public class UsagesView extends SyncableView
{
	public static final String VIEW_ID = "tersus.editor.usages.UsagesView";

	private TableViewer viewer;
	private static final Object[] NO_CHILDREN = new Object[0];

	public UsagesView()
	{
	}

	public void createPartControl(Composite parent)
	{
		viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		configureTable();
		viewer.setContentProvider(new ViewContentProvider());
		viewer.setLabelProvider(new ViewLabelProvider());
		hookDoubleClickAction();
	}

	private void hookDoubleClickAction()
	{
		viewer.addDoubleClickListener(new IDoubleClickListener()
		{
			public void doubleClick(DoubleClickEvent event)
			{
				Entry e = getSelectedEntry();
				if (e != null)
				{
					showAndHighlight(e.getBaseModel(), e.getPath());
				}

			}
		});
	}

	public Entry getSelectedEntry()
	{
		return (Entry) SelectionHelper.getSelectedItem(viewer.getSelection(), Entry.class);
	}

	public void setFocus()
	{
	}

	public void configureTable()
	{
		Table table = viewer.getTable();
		TableLayout layout = new TableLayout();
		table.setLayout(layout);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		ColumnWeightData[] columnLayouts =
		{ new ColumnWeightData(100), new ColumnWeightData(25), new ColumnWeightData(50),
				new ColumnWeightData(25), new ColumnWeightData(300) };
		String[] labels = new String[]
		{ "Base Model", "Usages", "Type", "Read Only", "Path" };
		String[] tooltips = new String[]
		{ "Base Model", "Number of usages of the base model", "Type",
				"Is element's parent is read only model", "Path" };

		final Table t = table;
		Listener sortListener = new Listener()
		{
			public void handleEvent(Event event)
			{
				// determine new sort column and direction
				TableColumn sortColumn = t.getSortColumn();
				TableColumn selectedColumn = (TableColumn) event.widget;

				int dir = t.getSortDirection();
				if (sortColumn == selectedColumn)
					dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
				else
				{
					t.setSortColumn(selectedColumn);
					dir = SWT.UP;
				}

				// sort the data based on column and direction
				String sortIdentifier = null;

				if (UsagesViewSort.BASE_MODEL_SORT.equals(selectedColumn.getText()))
					sortIdentifier = UsagesViewSort.BASE_MODEL_SORT;
				if (UsagesViewSort.TYPE_SORT.equals(selectedColumn.getText()))
					sortIdentifier = UsagesViewSort.TYPE_SORT;
				if (UsagesViewSort.PATH_SORT.equals(selectedColumn.getText()))
					sortIdentifier = UsagesViewSort.PATH_SORT;

				t.setSortDirection(dir);
				viewer.setSorter(new UsagesViewSort(sortIdentifier, dir));
			}
		};

		for (int i = 0; i < labels.length; i++)
		{

			TableColumn tc = new TableColumn(table, SWT.NONE, i);
			tc.setText(labels[i]);
			tc.setToolTipText(tooltips[i]);
			tc.pack();
			columnLayouts[i].minimumWidth = tc.getWidth();
			layout.addColumnData(columnLayouts[i]);

			tc.addListener(SWT.Selection, sortListener);
		}

	}

	public class ViewLabelProvider implements ITableLabelProvider
	{

		public Image getColumnImage(Object element, int columnIndex)
		{
			return null;
		}

		public String getColumnText(Object element, int columnIndex)
		{
			if (!(element instanceof Entry))
				return null;
			Entry entry = (Entry) element;
			switch (columnIndex)
			{
				case 0:
					return entry.getBaseModel().getName();
				case 1:
					return String.valueOf(entry.getNOccurences());
				case 2:
					return entry.getType();
				case 3:
					return String.valueOf(entry.isReadOnly());
				case 4:
					return entry.getPathStr();
				default:
					return null;

			}
		}

		public void addListener(ILabelProviderListener listener)
		{
		}

		public void dispose()
		{
		}

		public boolean isLabelProperty(Object element, String property)
		{
			return false;
		}

		public void removeListener(ILabelProviderListener listener)
		{
		}

	}

	public class ViewContentProvider implements IStructuredContentProvider
	{

		public Object[] getElements(Object inputElement)
		{
			if (inputElement instanceof Usages)
			{
				return ((Usages) inputElement).getUsages();
			}
			else
				return NO_CHILDREN;
		}

		public void dispose()
		{
		}

		public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
		{
		}

	}

	public TableViewer getViewer()
	{
		return viewer;
	}

}

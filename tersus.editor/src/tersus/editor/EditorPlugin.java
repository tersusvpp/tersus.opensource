/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor;

import java.util.HashMap;
import java.util.Iterator;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPluginDescriptor;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import tersus.editor.preferences.Preferences;
import tersus.util.Misc;
import tersus.util.Trace;

/**
 * 
 */
public class EditorPlugin extends AbstractUIPlugin
{

	public static int INVALID_MODEL = 15;

	private static EditorPlugin plugin;

	/**
	 * The constructor.
	 */
	public EditorPlugin(IPluginDescriptor descriptor)
	{
		super(descriptor);

		plugin = this;
	}

	/**
	 * Returns the shared instance.
	 */
	public static EditorPlugin getDefault()
	{
		return plugin;
	}

	/**
	 * @param descriptor
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#initializeDefaultPreferences(org.eclipse.jface.preference
	 * .IPreferenceStore)
	 */
	protected void initializeDefaultPreferences(IPreferenceStore store)
	{
		Preferences.initializeDefaultPreferences(store);
	}

	private HashMap allocatedColors = new HashMap();

	/**
	 * Returns a Color for a given RGB value
	 * 
	 * This method must be called from within the UI thread.
	 * 
	 * @param rgb
	 * @return
	 */
	public Color getColor(RGB rgb)
	{
		Color color;
		if (allocatedColors.containsKey(rgb))
		{
			color = (Color) allocatedColors.get(rgb);
		}
		else
		{
			color = new Color(Display.getCurrent(), rgb);
			if (Debug.HANDLES)
				Trace.add("Created color handle for " + rgb);
			allocatedColors.put(rgb, color);
		}
		return color;
	}

	private void disposeColors()
	{
		for (Iterator iter = allocatedColors.values().iterator(); iter.hasNext();)
		{
			Color color = (Color) iter.next();
			color.dispose();
		}
		allocatedColors.clear();
	}

	private HashMap allocatedFonts = new HashMap();

	/**
	 * @param fd
	 * @return
	 */
	public Font getFont(FontData fd[])
	{
		Font Font;
		Misc.assertion(fd.length == 1);
		Object key = fd[0];
		if (allocatedFonts.containsKey(key))
		{
			Font = (Font) allocatedFonts.get(key);
		}
		else
		{
			Font = new Font(null, fd);
			if (Debug.HANDLES)
				Trace.add("Created font handle for " + key);
			allocatedFonts.put(key, Font);
		}
		return Font;
	}

	private void disposeFonts()
	{
		for (Iterator iter = allocatedFonts.values().iterator(); iter.hasNext();)
		{
			Font Font = (Font) iter.next();
			Font.dispose();

		}
		allocatedFonts.clear();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.Plugin#shutdown()
	 */
	public void shutdown() throws CoreException
	{
		super.shutdown();
		disposeColors();
		disposeFonts();
	}

	/**
	 * 
	 */
	public static String getPluginId()
	{
		return getDefault().getDescriptor().getUniqueIdentifier();

	}

}

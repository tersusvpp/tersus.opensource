/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor;

import org.eclipse.gef.requests.CreationFactory;

/**
 * A simple CreationFactory that returns a constant object.
 */

//TODO find a more elegant way to create model objects (do we need a CreationFactory at all?)
public class SingletonFactory implements CreationFactory
{
	private Object obj;
	public SingletonFactory(Object obj)
	{
		this.obj = obj;
	}
	public Object getNewObject()
	{
		return obj;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.requests.CreationFactory#getObjectType()
	 */
	public Object getObjectType()
	{
		// TODO Review this auto-generated method stub
		return obj.getClass();
	}

}

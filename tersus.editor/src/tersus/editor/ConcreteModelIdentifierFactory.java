/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.ui.IElementFactory;
import org.eclipse.ui.IMemento;

import tersus.model.ModelId;
import tersus.workbench.TersusWorkbench;

/**
 * @author Youval Bronicki
 *
 */
public class ConcreteModelIdentifierFactory implements IElementFactory
{
    private static final String MODEL_ID = "ModelId";
    private static final String PROJECT_NAME = "ProjectName";
    public static final String FACTORY_ID = ConcreteModelIdentifierFactory.class.getName();

    /* (non-Javadoc)
     * @see org.eclipse.ui.IElementFactory#createElement(org.eclipse.ui.IMemento)
     */
    public IAdaptable createElement(IMemento memento)
    {
        String projectName = memento.getString(PROJECT_NAME);
        IProject project = TersusWorkbench.getWorkspace().getRoot().getProject(projectName);
        String modelIdPath = memento.getString(MODEL_ID);
        ModelId modelId = new ModelId(modelIdPath);
        return new ConcreteModelIdentifier(project, modelId);
    }

    /**
     * @param identifier
     * @param memento
     */
    public static void save(ConcreteModelIdentifier identifier, IMemento memento)
    {
        memento.putString(PROJECT_NAME, identifier.getProject().getName());
        memento.putString(MODEL_ID, identifier.getModelId().getPath());
    }

}

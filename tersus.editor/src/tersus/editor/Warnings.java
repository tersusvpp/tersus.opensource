/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor;

/**
 * @author Youval Bronicki
 *
 */
public interface Warnings
{
    int XPROJECT_DND_UNSUPPORTED_CODE = 1001;
    String XPROJECT_DND_UNSUPPORTED_MESSAGE = EditorMessages.getString("Warning.Cross_Project_DND_Unsupported");

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor;

import org.eclipse.core.resources.IFile;

/**
 * @author Youval Bronicki
 *
 */
public class Errors
{
    public static final int FAILED_TO_SAVE_PALETTE_CONFIGURATION_CODE = 101;
    public static final  String FAILED_TO_SAVE_PALETTE_CONFIGURATION_MESSAGE = EditorMessages.getString("Error.Failed_To_Save_Palette_Configuration");
    public static final  int FAILED_TO_LOAD_PALETTE_CONFIGURATION_CODE = 102;
    public static final  String FAILED_TO_LOAD_PALETTE_CONFIGURATION_MESSAGE = EditorMessages.getString("Error.Failed_To_Load_Palette_Configuration");
    public static final  int DROP_FAILED_CODE = 103;
    public static final  String DROP_FAILED_MESSAGE = EditorMessages.getString("Errors.DND_Drop_Failed");
    public static final  int FAILED_TO_READ_PACKAGE_CODE = 104;
    private static final String FAILED_TO_READ_PACKAGE_KEY = "Errors.Failed_To_Read_Package";
    public static final  int FAILED_TO_INIT_EDITOR_CODE = 105;
    public static final String FAILED_TO_INIT_EDITOR_MESSAGE = EditorMessages.getString("Error.Failed_To_Init_Editor");
    public static final int UNKNOWN_CONSTANT_TYPE_CODE = 106;
    public static final String UNKNOWN_CONSTANT_TYPE_KEY = "Error.Unknown_Constant_Type";
	public static final String USE_NON_EMPTY_NAME = "Please use a non-empty element name";
	public static final String LEADING_OR_TRAINLING_BLANKS = "Please don't use leading or trailing blanks in model names";
	public static final String failedToReadPackage(IFile file) 
    {
        return EditorMessages.format(FAILED_TO_READ_PACKAGE_KEY, new Object[]{file});
    }
	public static String invalidCharacterInElementName(char c)
	{
		
		return "Invalid character '"+c+"' in element name";
	}
	public static String invalidCharacterInPackageName(char c)
	{
		
		return "Invalid character '"+c+"' in package name";
	}
}

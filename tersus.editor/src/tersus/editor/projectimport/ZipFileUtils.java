/************************************************************************************************
 * Copyright (c) 2003-2021 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.projectimport;

import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;

import tersus.workbench.TersusWorkbench;


/**
 * 
 * @author Liat Shiff
 */
public class ZipFileUtils
{
	public static InputStream getFileFromZip(ZipFile zipFile, String fileName)
	{
		Enumeration<?> entries = zipFile.entries();

		while (entries.hasMoreElements())
		{
			ZipEntry entry = (ZipEntry) entries.nextElement();
			String fullPathName = entry.getName();
			int index = fullPathName.lastIndexOf('/');

			if (index >= -1)
			{
				String entryName = fullPathName.substring(index + 1);
				if (fileName.equals(entryName))
				{
					try
					{
						return zipFile.getInputStream(entry);
					}
					catch (FileNotFoundException e)
					{
						e.printStackTrace();
					}
					catch (IOException e)
					{
						e.printStackTrace();
					}

				}
			}
		}
		return null;
	}

	public static void unzip(ZipFile zipFile, IProject project, FilenameFilter filter) throws IOException
	{
		String modelsLibraryName = project.getName() + "/models/";
		IFolder projectRootFolder = TersusWorkbench.getRepositoryRoot(project);

		for (Enumeration<?> e = zipFile.entries(); e.hasMoreElements();)
		{
			ZipEntry entry = (ZipEntry) e.nextElement();
			String entryName = entry.getName();

			if (entryName.endsWith("/")) // ignore folder entries created by some zip libraries (e.g. Ant)
				continue;
			if (entryName.startsWith("/"))
				entryName = entryName.substring(1);
			IFile file = null;
			if (entryName.contains(modelsLibraryName))
				file = projectRootFolder.getFile(entryName.substring(modelsLibraryName.length()));
			else
				file = project.getFile(entryName.substring(entryName.indexOf('/')));
			if (filter!=null && !filter.accept(file.getParent().getLocation().toFile(), file.getName()))
				continue;

			if (file != null)
				TersusWorkbench.write(file, zipFile.getInputStream(entry));
		
		}
		zipFile.close();
	}
}

package tersus.editor.projectimport;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Status;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import tersus.editor.EditorPlugin;
import tersus.editor.RepositoryManager;
import tersus.util.ReadXMLUtils;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

public class ImportUtils
{
	private static final String PROJECT_NAME_TAG = "name";
	
	public static void importAsProject(String newProjectName, File zipFile)
	{
		try
		{
			IProject project = createProject(newProjectName);
			ZipFileUtils.unzip(new ZipFile(zipFile), project, new FilenameFilter(){

				public boolean accept(File dir, String name)
				{
					return (!".project".equals(name));
				}});
			project.refreshLocal(IResource.DEPTH_INFINITE, null);
			WorkspaceRepository repository = RepositoryManager.getRepositoryManager(project)
					.getRepository();
			repository.clearCache();
			repository.notifyRepositoryRefreshed();
		}
		catch (Exception e)
		{
			TersusWorkbench.log(e);
		}
	}

	public static IProject createProject(String name)
	{
		IWorkspace workspace = TersusWorkbench.getWorkspace();
		IProject project = workspace.getRoot().getProject(name);

		try
		{
			project.create(null);
			project.open(null);
			TersusWorkbench.getRepositoryRoot(project).create(false, true, null);
			TersusWorkbench.getDatabaseRoot(project).create(false, true, null);

			return project;
		}
		catch (CoreException e)
		{
			e.printStackTrace();
			TersusWorkbench.log(new Status(Status.ERROR, EditorPlugin.getPluginId(), 1,
					"Failed to create project " + name, e));
			if (project.exists())
			{
				try
				{
					project.delete(true, true, null);
				}
				catch (CoreException e1)
				{
					// Secondary Exception ignored
				}
			}
		}
		return null;
	}
	
	public static String getProjectName(File file)
	{
		ZipFile zipFile = null;
		try
		{
			try
			{
				zipFile = new ZipFile(file);
			}
			catch (ZipException e)
			{
				(new ZipException("Cannot crete new zip file '" + file.getName() + "'. "
						+ e.getMessage())).printStackTrace();
			}

			InputStream in = ZipFileUtils.getFileFromZip(zipFile, ".project");

			DocumentBuilder builder;
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

			Document document;
			document = builder.parse(in);

			document.normalize();

			return ReadXMLUtils.getElementContent(document, PROJECT_NAME_TAG);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (ParserConfigurationException e)
		{
			e.printStackTrace();
		}
		catch (SAXException e)
		{
			e.printStackTrace();
		}

		return null;
	}
	
	public static void deleteProject(String projectName)
	{
		try
		{
			IWorkspace workspace = TersusWorkbench.getWorkspace();
			IProject projectToDelete = workspace.getRoot().getProject(projectName);

			RepositoryManager repositoryManger = RepositoryManager
					.getRepositoryManager(projectToDelete);

			repositoryManger.closeOpenEditors();
			projectToDelete.delete(true, null);
			repositoryManger.dispose();
		}
		catch (CoreException e)
		{
			e.printStackTrace();
		}
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor;

import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.MouseWheelHandler;
import org.eclipse.swt.widgets.Event;

import tersus.editor.TersusZoomManager;
import tersus.editor.actions.ZoomAction;

/**
 * 
 * @author Liat Shiff
 */
public class TersusMouseWheelZoomHandle implements MouseWheelHandler
{

	/**
	 * The Singleton
	 */
	public static final MouseWheelHandler SINGLETON = new TersusMouseWheelZoomHandle();

	private TersusMouseWheelZoomHandle() { }
	
	public void handleMouseWheel(Event event, EditPartViewer viewer)
	{
		TersusZoomManager zoomMgr = ((TersusEditor)((TersusGraphicalViewer)viewer).getEditor()).getZoomManager();
		if (zoomMgr != null) 
		{
			if (event.count > 0)
				zoomMgr.zoom(ZoomAction.ZOOM_IN);
			else
				zoomMgr.zoom(ZoomAction.ZOOM_OUT);
			event.doit = false;
		}
	}
}

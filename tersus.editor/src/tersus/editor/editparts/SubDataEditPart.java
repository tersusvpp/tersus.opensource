/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.editparts;

import org.eclipse.gef.EditPolicy;

/**
 * An EditPart that represents a sub-node in a data tree hierarchy.
 */
public class SubDataEditPart extends DataEditPart
{
	/* (non-Javadoc)
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	protected void createEditPolicies()
	{
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, new TersusDirectEditPolicy());
		super.createEditPolicies();

	}
	
	/* (non-Javadoc)
	 * @see tersus.editor.editparts.ModelEditPart#refreshLabel()
	 */

	
    protected String getLabelSeparator()
    {
        return " ";
    }
}


/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.editparts;

import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.DirectEditPolicy;
import org.eclipse.gef.requests.DirectEditRequest;
import org.eclipse.swt.widgets.Display;

import tersus.editor.actions.ValidatingCommand;
import tersus.editor.requests.EditInitialNameRequest;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelException;
import tersus.model.Role;
import tersus.model.commands.ChangeElementNameCommand;
import tersus.model.commands.IElementNameCommand;
import tersus.model.commands.MultiStepCommand;
import tersus.util.Misc;

/**
 * Enables direct editing of roles by editing the lable.
 * 
 * @author Youval Bronicki
 * 
 */
public class TersusDirectEditPolicy extends DirectEditPolicy
{

	/**
	 * Returns a command that sets the host's role to the new value. If the new value is invalid,
	 * sounds a beep and returns null.
	 * 
	 */
	// NICE2 Update class description (now that it uses a request)
	protected Command getDirectEditCommand(DirectEditRequest edit)
	{
		TersusDirectEditFeature feature = (TersusDirectEditFeature) edit.getDirectEditFeature();
		TersusEditPart part = (TersusEditPart) getHost();
		Object newValue = edit.getCellEditor().getValue();
		if (feature == TersusDirectEditFeature.INITIAL_NAME)
		{
			String originalName = part.getModelElement().getRole().toString();
			Command initialCommand = ((EditInitialNameRequest) edit).getInitialCreationCommand();
			initialCommand.undo();
			String name = newValue.toString();
			((IElementNameCommand) initialCommand).setSuggestedName(name);
			if (((ValidatingCommand) initialCommand).validate() != null)
				((IElementNameCommand) initialCommand).setSuggestedName(originalName);

			if (initialCommand instanceof MultiStepCommand)
				((MultiStepCommand) initialCommand).reset();

			return initialCommand;
		}
		else
		{
			Misc.assertion(feature == TersusDirectEditFeature.ROLE);
			return getChangeElementNameCommand((String) newValue);
		}
	}

	protected void showCurrentEditValue(DirectEditRequest request)
	{
		// Nothing to do: We don't need intermediate feedback
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.EditPolicy#understandsRequest(org.eclipse.gef.Request)
	 */
	public boolean understandsRequest(Request request)
	{
		if (request instanceof DirectEditRequest)
		{
			if (((DirectEditRequest) request).getDirectEditFeature() instanceof TersusDirectEditFeature)
				return true;
		}
		return super.understandsRequest(request);
	}

	/**
	 * Creates a command that performs a global change of the element's name ("global" means within
	 * the whole repository).
	 * 
	 * Perfroms no change to any model object (just creates the compound command for future
	 * execution).
	 * 
	 * @param suggestedRole
	 *            The new role of the element (a string).
	 * @return The command to change the element's role.
	 */

	// CORE2 Currently this method only handles references under the given context - take care of
	// external refernces!
	protected Command getChangeElementNameCommand(String suggestedRole)
	{
		ModelEditPart topEditPart = (ModelEditPart) ((TersusEditPart) getHost()).getRoot()
				.getContents();
		Model context = topEditPart.getTersusModel();
		Misc.assertion(context != null);

		ModelElement element = ((TersusEditPart) getHost()).getModelElement();
		Misc.assertion(element != null);

		Role newRole = null;
		try
		{
			newRole = Role.get(suggestedRole);
			if (newRole == element.getRole())
			{
				// If the role is the same as the current role, do nothing
				return null;
			}
		}
		catch (ModelException e)
		{
			// New role is invalid
			Display.getCurrent().beep();
			// TEST test this case
			// FUNC2 display error message
			return null;
		}

		if (element.getParentModel().getElement(newRole) != null)
		{
			// There is an existing element with this role
			Display.getCurrent().beep();
			// TEST Test this case
			// FUNC2 display error message
			return null;
		}
		return new ChangeElementNameCommand(element, newRole);
	}

}
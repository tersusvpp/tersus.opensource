/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.editparts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.RootEditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.gef.requests.DirectEditRequest;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;

import tersus.editor.ConcreteModelIdentifier;
import tersus.editor.Debug;
import tersus.editor.TersusEditor;
import tersus.editor.TersusRootEditPart;
import tersus.editor.TersusZoomManager;
import tersus.editor.ZoomLimitReachedException;
import tersus.editor.figures.FlowRectangle;
import tersus.editor.figures.Repeatable;
import tersus.editor.preferences.Preferences;
import tersus.editor.properties.ModelObjectWrapper;
import tersus.editor.requests.EditInitialNameRequest;
import tersus.model.BuiltinProperties;
import tersus.model.ElementPath;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.ModelObject;
import tersus.model.Path;
import tersus.model.Role;
import tersus.util.Misc;
import tersus.util.NotImplementedException;
import tersus.util.PropertyChangeListener;
import tersus.util.Trace;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

public abstract class TersusEditPart extends AbstractGraphicalEditPart implements
		PropertyChangeListener
{
	public final Role getRole()
	{
		if (getModelElement() == null)
			return null;
		else
			return getModelElement().getRole();
	}

	public boolean modelIsModifiable()
	{
		return true;
	}

	public boolean elementIsModifiable()
	{
		return true;
	}

	public boolean isModelReadOnly()
	{
		return getTersusModel().isReadOnly();
	}

	/**
	 * @return A new path representing the the path of this element relative to the top model
	 *         opened. For the top model, the path is an empty path.
	 */
	public Path getPath()
	{
		ModelElement element = getModelElement();
		if (element == null) // Top-level edit part - path should be empty
			return new Path();
		else
		{
			Role role = element.getRole();
			Misc.assertion(role != null);
			Path path = (((TersusEditPart) getParent()).getPath());
			path.addSegment(role);
			return path;
		}
	}

	public TersusEditPart getChild(ElementPath path, boolean open, int index)
	{
		Misc.assertion(index >= 0);
		Misc.assertion(index <= path.getNumberOfSegments());
		if (path.getNumberOfSegments() <= index)
			return this;
		else
		{
			TersusEditPart child = getChild(path.getSegment(index));
			if (child == null && open)
			{
				if (isComposite())
					setOpen(true);
				child = getChild(path.getSegment(index));
			}
			if (child == null)
				return null;
			else
				return child.getChild(path, open, index + 1);
		}
	}

	public boolean isComposite()
	{
		return false;
	}

	public void setOpen(boolean b)
	{
	}

	public TersusEditPart getChild(ElementPath path, boolean open)
	{
		return getChild(path, open, 0);
	}

	public abstract TersusEditPart getChild(Role role);

	protected void refreshLayout()
	{
		System.out.println("");
	}

	protected void refreshVisuals()
	{
		refreshLayout();
		refreshColor();
		refreshLabel();
		if (getModelElement() != null)
			refreshRepetitive();
	}

	protected void refreshColor()
	{
	}

	protected void refreshLine()
	{
	}

	/**
	 * Calls refreshVisuals (recursively) on this EditPart and its offsprings.
	 * 
	 */
	public void refreshVisualsDeep()
	{
		boolean tracePushed = false;
		if (Debug.REFRESH)
		{
			Trace.push("refreshVisualsDeep() part=" + this);
			tracePushed = true;
		}
		try
		{
			refreshVisuals();
			for (Object obj : getChildren())
			{
				TersusEditPart child = (TersusEditPart) obj;
				child.refreshVisualsDeep();
			}
		}
		finally
		{
			if (tracePushed)
				Trace.pop();
		}
	}

	/**
	 * @see org.eclipse.gef.EditPart#activate()
	 */
	public void activate()
	{
		if (isActive() == false)
		{
			super.activate();
			startListening();
		}
	}

	protected void startListening()
	{
		ModelObjectWrapper wrapper = (ModelObjectWrapper) getModel();
		wrapper.addPropertyChangeListener(this);
	}

	/**
	 * @see org.eclipse.gef.EditPart#deactivate()
	 */
	public void deactivate()
	{
		if (isActive())
		{
			super.deactivate();
			stopListening();
		}
	}

	protected void stopListening()
	{
		ModelObjectWrapper wrapper = (ModelObjectWrapper) getModel();
		wrapper.removePropertyChangeListener(this);
	}

	public Model getTersusModel()
	{
		return ((ModelObjectWrapper) getModel()).getModel();
	}

	public ModelElement getModelElement()
	{
		return ((ModelObjectWrapper) getModel()).getElement();
	}

	protected void modelChanged(Object sourceObject, String property, Object oldValue,
			Object newValue)
	{
		if (BuiltinProperties.PROTOTYPE_ELEMENT.equals(property)
				|| BuiltinProperties.ID.equals(property))
			refreshLabel();
		else if (BuiltinProperties.PLUGIN.equals(property))
			refreshVisuals();
		else if ("ReadOnlyStatus".equals(property))
		{
			Display.getDefault().asyncExec(new Runnable()
			{
				public void run()
				{
					refreshPolicies();
					refreshVisuals();
					FlowRectangle rect = (FlowRectangle) getFigure();
					rect.repaint();
				}
			});
		}
	}

	protected void refreshPolicies()
	{
		EditPolicy layoutPolicy = getEditPolicy(EditPolicy.LAYOUT_ROLE);
		if (layoutPolicy != null)
			layoutPolicy.deactivate();
		installEditPolicy(EditPolicy.LAYOUT_ROLE, new TersusXYLayoutEditPolicy());

		EditPolicy containerPolicy = getEditPolicy(EditPolicy.CONTAINER_ROLE);
		if (containerPolicy != null)
			containerPolicy.deactivate();
		installEditPolicy(EditPolicy.CONTAINER_ROLE, new FlowContainerEditPolicy());
	}

	protected void elementChanged(Object sourceObject, String property, Object oldValue,
			Object newValue)
	{
		if (BuiltinProperties.POSITION.equals(property) || BuiltinProperties.SIZE.equals(property))
		{
			getFigure().invalidate();
			refreshLayout();
		}
		else if (BuiltinProperties.REPETITIVE.equals(property))
			refreshRepetitive();
		else if (BuiltinProperties.ROLE.equals(property))
		{
			refreshLabel();
			refreshTraversingLinkEnds();
		}
		else if (BuiltinProperties.PROTOTYPE_ELEMENT.equals(property))
		{
			refreshLabel();
		}
		else if ("ReadOnlyStatus".equals(property))
		{
			System.out.println("Read only change");
		}

	}

	/**
	 * Refresh the source and target of any links that may traverse this edit part (and may be
	 * affected by a a change in the role of this edit part)
	 */
	protected abstract void refreshTraversingLinkEnds();

	/**
	 * Refreshes the element's label. This method is overriden by sub-classes that have labels
	 */
	protected void refreshLabel()
	{
	}

	public void propertyChange(Object sourceObject, String property, Object oldValue,
			Object newValue)
	{
		if (!isActive())
			return;
		/*
		 * We need to ignore property change notifications received after deactivating. This can
		 * happen beacuse PropertyChangeSupport clones the list of listeners before starting to
		 * iterate over them.
		 */
		if (sourceObject == getTersusModel())
			modelChanged(sourceObject, property, oldValue, newValue);
		else if (sourceObject == getModelElement())
			elementChanged(sourceObject, property, oldValue, newValue);
		else if (Debug.WARNING)
			Trace.add("FlowEditPart#propertyChange() - Unknown source part=" + this + " source="
					+ sourceObject);
	}

	/*
	 * (non-Javadoc) This method overidden for sole purpose of add trace entries
	 * 
	 * @see org.eclipse.gef.EditPart#setSelected(int)
	 */
	public void setSelected(int value)
	{
		if (Debug.SELECT)
			Trace.push("setSelected() part=" + this + " value=" + value);
		super.setSelected(value);
		getViewer().flush();
		if (Debug.SELECT)
			Trace.pop();
	}

	// NICE2? add a generic method to refresh visual attributes
	// or go back to using refreshVisuals for this
	private void refreshRepetitive()
	{
		if (getFigure() instanceof Repeatable)
		{
			((Repeatable) getFigure()).setRepetitive(getModelElement().isRepetitive());
			refreshTraversingLinkEnds();
		}

	}

	/**
	 * A cached DirectEditManager for role editing
	 */
	TersusDirectEditManager directEditManager;

	/**
	 * Performs edit role requests by triggering direct-edit of the role FUNC3 Open a dialog if
	 * 'heavy' change-role may be needed
	 */
	private void performDirectEdit(TersusDirectEditFeature feature, Command initialCreationCommand)
	{
		if (directEditManager == null)
			directEditManager = new TersusDirectEditManager(this);
		directEditManager.setFeature(feature);
		directEditManager.setInitialCommand(initialCreationCommand);
		getViewer().getControl().getDisplay().asyncExec(new Runnable()
		{
			public void run()
			{
				// We showing the direct edit manager asynchronously
				// Because on Linux/GTK (Ubuntu 7.4,7.10) the direct edit was committed immediately
				directEditManager.show();

			}
		});
	}

	/*
	 * (non-Javadoc) Overriden to capture and perform EDIT_ROLE
	 * 
	 * @see org.eclipse.gef.EditPart#performRequest(org.eclipse.gef.Request)
	 */
	public void performRequest(Request request)
	{
		if (request instanceof DirectEditRequest)
		{
			DirectEditRequest directEditRequest = (DirectEditRequest) request;
			Object feature = directEditRequest.getDirectEditFeature();
			if (feature instanceof TersusDirectEditFeature)
			{
				Command initialCreationCommand = null;
				if (request instanceof EditInitialNameRequest)
				{
					initialCreationCommand = ((EditInitialNameRequest) request)
							.getInitialCreationCommand();
				}
				performDirectEdit((TersusDirectEditFeature) feature, initialCreationCommand);
				return;
			}
		}
		super.performRequest(request);
	}

	/**
	 * @return a CellEditorLocator for the element of this edit part
	 */
	public abstract CellEditorLocator getRoleEditorLocator();

	/**
	 * Remove highlighting from this part's visual representation.
	 * 
	 * The default implementation restores the orignal color of the figure.
	 * 
	 */
	public void clearHighlight()
	{
		refreshColor();
	}

	/**
	 * Visually highlight the part's element. The default behavior is to change the figure's
	 * background color.
	 */
	public void highlight()
	{
		getFigure().setBackgroundColor(Preferences.getColor(Preferences.HIGHLIGHT_COLOR));
	}

	public void zoomToPart()
	{
	}

	public ModelObject getModelObject()
	{
		return ((ModelObjectWrapper) getModel()).getModelObject();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	public Object getAdapter(Class key)
	{
		if (ModelObject.class.equals(key))
			return getModelObject();
		else if (ConcreteModelIdentifier.class.equals(key))
		{
			throw new NotImplementedException("Obsolete Mechanism");
		}
		return super.getAdapter(key);
	}

	protected String calculateStereotypeLabel()
	{
		String stereotype = null;
/*		if (getModelElement() == null)
		{
			String prototypeStr = (String) getTersusModel()
					.getProperty(BuiltinProperties.PROTOTYPE);
			if (prototypeStr != null)
			{
				try
				{
					stereotype = (new ModelId(prototypeStr)).getName();
				}
				catch (IllegalArgumentException e)
				{
					stereotype = null;
				}
			}
		}
		else
			stereotype = (String) getModelElement()
					.getProperty(BuiltinProperties.PROTOTYPE_ELEMENT);*/
		return stereotype;
	}

	protected IFigure createFigure()
	{
		return null;
	}

	protected void createEditPolicies()
	{

	}

	public RootEditPart getRoot()
	{
		if (getParent() == null)
			return null;
		else
			return super.getRoot();
	}

	protected void performZoomToPart()
	{
		if (Debug.OTHER)
			Trace.push("TersusEditPart.performZoomToPart() editPart=" + this);
		try
		{

			TersusRootEditPart root = (TersusRootEditPart) getRoot();
			IFigure figure = getFigure();
			TersusZoomManager zoomManager = root.getZoomManager();
			zoomManager.zoomToFigure(figure);
			if (isComposite())
				setOpen(true);
		}
		catch (ZoomLimitReachedException e)
		{
			boolean newTab = MessageDialog.openQuestion(getViewer().getControl().getShell(),
					"Zoom limit reached",
					"Zoom limit reached.\nWould you like to open the parent model in a new tab?");
			if (newTab)
			{
				EditPart parent = getParent();
				while (parent != null && !(parent instanceof FlowEditPart))
					parent = parent.getParent();
				if (parent != null)
				{
					Model parentModel = ((TersusEditPart) parent).getTersusModel();
					ConcreteModelIdentifier modelIdentifier = new ConcreteModelIdentifier(
							((WorkspaceRepository) parentModel.getRepository()).getRepositoryRoot()
									.getProject(), parentModel.getId());
					IWorkbenchPage page = TersusWorkbench.getActiveWorkbenchWindow()
							.getActivePage();
					try
					{
						page.openEditor(modelIdentifier, TersusEditor.ID);
					}
					catch (PartInitException e1)
					{
						TersusWorkbench.log(e1);
						return;
					}
				}

			}
		}
		finally
		{
			if (Debug.OTHER)
				Trace.pop();
		}
	}
}

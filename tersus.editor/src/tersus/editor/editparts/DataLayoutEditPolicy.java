/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.editparts;

import java.util.List;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Polyline;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.OrderedLayoutEditPolicy;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gef.requests.LocationRequest;
import org.eclipse.jface.action.IStatusLineManager;

import tersus.editor.Debug;
import tersus.editor.TersusEditDomain;
import tersus.editor.figures.DataRectangle;
import tersus.editor.properties.ModelObjectWrapper;
import tersus.editor.requests.DropModelObjectRequest;
import tersus.model.DataType;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.ModelObject;
import tersus.model.TemplateAndPrototypeSupport;
import tersus.model.commands.AddElementHelper;
import tersus.model.commands.ChangeElementOrderInDataModelCommand;
import tersus.model.commands.CreateElementFromTemplateCommand;
import tersus.util.Trace;

/**
 * An OrderedLayoutEditPolicy that supports data editing:
 * 
 * Supports selecting data elements.
 * 
 * @author Youval Bronicki
 * 
 */
public class DataLayoutEditPolicy extends OrderedLayoutEditPolicy
{

	private Polyline insertionLine;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.OrderedLayoutEditPolicy#createAddCommand(org.eclipse.gef.EditPart
	 * , org.eclipse.gef.EditPart)
	 */
	protected Command createAddCommand(EditPart child, EditPart after)
	{
		// TODO Review this auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.OrderedLayoutEditPolicy#createMoveChildCommand(org.eclipse.gef
	 * .EditPart, org.eclipse.gef.EditPart)
	 */
	protected Command createMoveChildCommand(EditPart child, EditPart after)
	{
		DataEditPart childData = null;

		if (child instanceof DataEditPart)
			childData = (DataEditPart) child;
		else
			return null;

		ModelElement childElement = childData.getModelElement();

		if (childElement == null)
			return null;

		DataType parentModel = (DataType) ((ModelObjectWrapper) ((FlowDataEditPart) child
				.getParent()).getModel()).getModel();

		if (parentModel == null)
			return null;

		ModelElement afterElement = null;

		if (after instanceof DataEditPart)
			afterElement = ((DataEditPart) after).getModelElement();

		Command moveCommand = new ChangeElementOrderInDataModelCommand(parentModel, childElement,
				afterElement);

		return moveCommand;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.OrderedLayoutEditPolicy#getInsertionReference(org.eclipse.gef
	 * .Request)
	 */
	protected EditPart getInsertionReference(Request request)
	{
		Point location = null;
		if (request instanceof LocationRequest)
			location = ((LocationRequest) request).getLocation();
		else if (request instanceof ChangeBoundsRequest)
			location = ((ChangeBoundsRequest) request).getLocation();
		else
			return null;

		DataEditPart parent = (DataEditPart) getHost();
		DataEditPart reference = null;

		for (Object obj : parent.getChildren())
		{
			if (obj instanceof DataEditPart)
			{
				DataEditPart child = (DataEditPart) obj;
				Point p = location.getCopy();
				child.getFigure().translateToRelative(p);
				if (child.getFigure().getBounds().bottom() < p.y)
					reference = child;
			}
		}
		return reference;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.gef.editpolicies.LayoutEditPolicy#getCreateCommand(org.eclipse.gef.requests.
	 * CreateRequest)
	 */
	protected Command getCreateCommand(CreateRequest request)
	{
		// TODO Review this auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.LayoutEditPolicy#getDeleteDependantCommand(org.eclipse.gef.Request
	 * )
	 */
	protected Command getDeleteDependantCommand(Request request)
	{
		// TODO Review this auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.EditPolicy#getCommand(org.eclipse.gef.Request)
	 */
	public Command getCommand(Request request)
	{
		if (request instanceof DropModelObjectRequest)
			return getAddElementCommand((DropModelObjectRequest) request);
		else
			return super.getCommand(request);
	}

	private Command getAddElementCommand(DropModelObjectRequest request)
	{

		if (Debug.CREATE)
		{
			// Trace.push("getAddElementCommand() host=" + getHost());
		}
		try
		{
			if (!(getHost() instanceof DataEditPart))
			{
				if (Debug.CREATE)
					Trace.add("request ignored - host is not a FlowEditPart");
				return null;
			}
			DataEditPart parent = (DataEditPart) getHost();

			IStatusLineManager statusLineManager = ((TersusEditDomain) parent.getRoot().getViewer()
					.getEditDomain()).getEditorPart().getEditorSite().getActionBars()
					.getStatusLineManager();

			if (!(parent.isComposite() && parent.isOpen()))
				return null;
			ModelObject sourceObject = request.getSourceObject();
			DataType parentModel = (DataType) parent.getTersusModel();
			DataEditPart referenceEditPart = (DataEditPart) getInsertionReference(request);
			int index = referenceEditPart == null ? 0 : parent.getChildren().indexOf(
					referenceEditPart) + 1;

			if (sourceObject instanceof DataType && !((DataType) sourceObject).isConstant())
			{
				Model addedModel = (Model) sourceObject;
				ModelId addedModelId = addedModel.getId();

				if (TemplateAndPrototypeSupport.isTemplate(addedModel))
				{
					CreateElementFromTemplateCommand command = 
						AddElementHelper.getCreateElementFromTemplateCommand(
								parentModel, addedModel, null, null, index);

					statusLineManager.setErrorMessage(null);
					return command;
				}
				else
				{
					return AddElementHelper.getAddElementCommand(parentModel, addedModelId,
							addedModelId.getName(), null, new Integer(index));
				}
			}
			else
			{
				if (sourceObject instanceof DataType && ((DataType) sourceObject).isConstant())
					statusLineManager.setErrorMessage("Cannot add constant to data structure.");

				return null;
			}
		}
		finally
		{
			// if (Debug.CREATE)
			// Trace.pop();
		}
	}

	public EditPart getTargetEditPart(Request request)
	{
		if (request instanceof DropModelObjectRequest)
			return getHost();
		else
			return super.getTargetEditPart(request);
	}

	/**
	 * Lazily creates and returns a <code>Polyline</code> Figure for use as feedback.
	 * 
	 * @return a Polyline figure
	 */
	private Polyline getFeedbackLine()
	{
		if (insertionLine == null)
		{
			insertionLine = new Polyline();
			insertionLine.setLineWidth(1);
			addFeedback(insertionLine);
			insertionLine.setForegroundColor(ColorConstants.darkBlue);
		}
		return insertionLine;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.LayoutEditPolicy#showLayoutTargetFeedback(org.eclipse.gef.Request
	 * )
	 */
	protected void showLayoutTargetFeedback(Request request)
	{
		DataEditPart host = (DataEditPart) getHost();

		if (!(host.isComposite() && host.isOpen()))
			return;
		Point p1 = new Point();
		DataEditPart previousEditPart = (DataEditPart) getInsertionReference(request);

		int length;
		if (previousEditPart == null)
		{
			DataRectangle f = (DataRectangle) ((DataEditPart) getHost()).getFigure();
			p1.y = f.getTextY() + f.getTextHeight() + 1;
			p1.x = f.getTextX();
			length = f.getFrame().width - 2 * (p1.x - f.getFrame().x);

			f.translateToAbsolute(p1);
		}
		else
		{
			DataRectangle f = (DataRectangle) previousEditPart.getFigure();
			p1.y = f.getBounds().bottom() + 1;
			p1.x = f.getFrame().x;
			length = f.getFrame().width;
			f.translateToAbsolute(p1);

		}
		Point p2 = p1.getTranslated(length - 1, 0);
		Polyline feedbackLine = getFeedbackLine();
		feedbackLine.translateToRelative(p1);
		feedbackLine.translateToRelative(p2);
		feedbackLine.setStart(p1);
		feedbackLine.setEnd(p2);

	}

	/**
	 * @see LayoutEditPolicy#eraseLayoutTargetFeedback(Request)
	 */
	protected void eraseLayoutTargetFeedback(Request request)
	{
		if (insertionLine != null)
		{
			removeFeedback(insertionLine);
			insertionLine = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.EditPolicy#eraseTargetFeedback(org.eclipse.gef.Request)
	 */
	public void eraseTargetFeedback(Request request)
	{
		if (request instanceof DropModelObjectRequest
				&& ((DropModelObjectRequest) request).getSourceObject() instanceof DataType)
			eraseLayoutTargetFeedback(request);
		else
			super.eraseTargetFeedback(request);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.EditPolicy#showTargetFeedback(org.eclipse.gef.Request)
	 */
	public void showTargetFeedback(Request request)
	{
		if (request instanceof DropModelObjectRequest
				&& ((DropModelObjectRequest) request).getSourceObject() instanceof DataType)
			showLayoutTargetFeedback(request);
		else
			super.showTargetFeedback(request);
	}

	@SuppressWarnings("unchecked")
	protected Command getMoveChildrenCommand(Request request)
	{
		CompoundCommand command = new CompoundCommand();
		List<EditPart> editParts = ((ChangeBoundsRequest) request).getEditParts();
		EditPart insertionReference = getInsertionReference(request);

		for (int i = 0; i < editParts.size(); i++)
		{
			EditPart child = (EditPart) editParts.get(i);
			command.add(createMoveChildCommand(child, insertionReference));
		}
		return command.unwrap();
	}

}

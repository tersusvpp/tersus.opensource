/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.editparts;

import java.util.List;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.DragTracker;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.swt.graphics.Color;

import tersus.editor.Debug;
import tersus.editor.TersusDragTracker;
import tersus.editor.figures.FlowRectangle;
import tersus.editor.layout.LayoutConstraint;
import tersus.editor.preferences.Preferences;
import tersus.editor.properties.ModelObjectWrapper;
import tersus.editor.useractions.Comment;
import tersus.model.BuiltinProperties;
import tersus.model.FlowModel;
import tersus.model.FlowType;
import tersus.model.Link;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.Slot;
import tersus.util.Trace;

/**
 * The EditPart that handles process elements
 */
public abstract class FlowEditPart extends ModelEditPart
{
	/**
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 **/
	protected void createEditPolicies()
	{
		installEditPolicy(EditPolicy.CONTAINER_ROLE, new FlowContainerEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, new TersusXYLayoutEditPolicy());
	}

	public Model getParentModel()
	{
		Model parent = null;

		if (this.getParent() instanceof SubFlowEditPart)
		{
			parent = ((SubFlowEditPart) this.getParent()).getSubFlow().getReferredModel();
		}
		if (this.getParent() instanceof TopFlowEditPart)
		{
			parent = ((TopFlowEditPart) this.getParent()).getFlowModel();
		}

		return parent;
	}

	public boolean isComposite()
	{
		// TODO Update this logic to use model attributes (a composite can have 0 elements)
		// TODO Ignore slots when counting elements
		FlowModel model = getFlowModel();
		List<ModelElement> elements = model.getElements();
		for (ModelElement element : elements)
		{
			if (!(element instanceof Slot))
				return true;
		}
		return false;
	}

	protected void modelChanged(Object sourceObject, String property, Object oldValue,
			Object newValue)
	{
		String prop = property;
		if (Debug.OTHER)
			Trace.push("FlowModelEditPart#modelPropertyChange() editpart=" + this + " prop=" + prop);
		try
		{
			if (BuiltinProperties.ELEMENTS.equals(prop))
			{
				boolean wasComposite = (toggle != null);
				refreshComposite();
				refreshChildren();
				boolean isComposite = (toggle != null);
				// Handle with open/close rectangle here is effect on all the appearances of this
				// editPart.
				// For example - if we have more then one appearance of the same model (which use
				// the same edit part) and they are all collapsed (close), when the user will add a
				// new element to
				// one of the models , It will open (as we wish) but also all the models with the
				// same editPart will be opened.
				if ((isComposite && !wasComposite) || (wasComposite && isComposite && !isOpen()))
					setOpen(true);
			}
			else if (BuiltinProperties.TYPE.equals(prop))
				refreshColor();
			else if (BuiltinProperties.WIDTH.equals(prop))
				refreshWidth();
			else
				super.modelChanged(sourceObject, property, oldValue, newValue);

		}
		finally
		{
			if (Debug.OTHER)
				Trace.pop();
		}
	}

	public FlowRectangle getFlowRectangle()
	{
		return (FlowRectangle) getFigure();
	}

	protected abstract boolean getParity();

	protected void refreshColor()
	{
		String preferenceName = null;
		FlowType type = getFlowModel().getType();
		getFlowRectangle().setForegroundColor(ColorConstants.black);
		if (!open && getFlowRectangle().hasImage()
				&& Preferences.getBoolean(Preferences.TRANSPARENT_IMAGE_BACKGROUND))
		{
			getFlowRectangle().setBackgroundColor(null);
			getFlowRectangle().setLineColor(ColorConstants.lightGray);
			return;
		}
		getFlowRectangle().setLineColor(ColorConstants.black);
		if (getParity())
		{
			if (type == FlowType.SYSTEM)
				preferenceName = Preferences.SYSTEM_COLOR_1;
			else
				preferenceName = Preferences.PROCESS_COLOR_1;
		}
		else
		{
			if (type == FlowType.SYSTEM)
				preferenceName = Preferences.SYSTEM_COLOR_2;
			else
				preferenceName = Preferences.PROCESS_COLOR_2;
		}

		Color backgroundColor = Preferences.getColor(preferenceName);

		getFlowRectangle().setBackgroundColor(backgroundColor);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.EditPart#performRequest(org.eclipse.gef.Request) Handles REQ_OPEN
	 * requests (double click) by zooming on the image
	 */
	public void performRequest(Request req)
	{
		if (req.getType() == RequestConstants.REQ_OPEN)
		{
			if (Debug.USER_ACTIONS)
				Trace.push(new Comment("zoomToElement path=\"" + getPath() + "\""));
			try
			{
				zoomToPart();
			}

			finally
			{
				if (Debug.USER_ACTIONS)
					Trace.pop();
			}
		}
		else
		{
			if (Debug.OTHER)
				Trace.add("FlowEditPart.performRequest() [Ignored] editpart=" + this + " req="
						+ req + " type=" + req.getType());
			super.performRequest(req);
		}
	}

	/**
	 * Zooms so that the content FlowModel captures a big proportion of the visible area, openning
	 * it if it's a composite
	 */
	public void zoomToPart()
	{
		performZoomToPart();
	}

	public DragTracker getDragTracker(Request req)
	{
		if (Debug.OTHER)
			Trace.add("Called ModelEditPart.getDragTracker() with editPart=" + this + " req=" + req);

		return new TersusDragTracker(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	protected IFigure createFigure()
	{
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.editor.editparts.ModelEditPart#refreshLayout()
	 */
	protected void refreshLayout()
	{
		refreshWidth();
		super.refreshLayout();
	}

	protected void refreshWidth()
	{
		((FlowRectangle) getFigure()).setWidthProportion(getFlowModel().getWidth());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#refreshChildren()
	 */
	protected void refreshChildren()
	{
		if (Debug.REFRESH)
		{
			Trace.push("Refreshing children of " + this);
		}
		try
		{
			super.refreshChildren();
			/*
			 * Link ends are refreshed at the end, allowing a link to to find its source and target
			 * even if the link appears before the source/target in the children list
			 */
			refreshLinkEnds();
		}
		finally
		{
			if (Debug.REFRESH)
				Trace.pop();
		}
	}

	protected void refreshLinkEnds()
	{
		if (Debug.REFRESH)
		{
			Trace.push("Refreshing link ends of " + this);
		}
		try
		{
			for (Object obj : getChildren())
			{
				TersusEditPart child = (TersusEditPart) obj;
				if (child instanceof LinkEditPart)
					((LinkEditPart) child).refreshEnds();
			}
		}
		finally
		{
			if (Debug.REFRESH)
				Trace.pop();
		}
	}

	public static LayoutConstraint adjustConstraint(LayoutConstraint constraint)
	{
		double width = constraint.getWidth();
		double height = constraint.getHeight();
		double x = constraint.getX();
		double y = constraint.getY();

		if (width < 0)
			return null;
		if (height < 0)
			return null;
		if (x - width / 2 < 0)
			return null;
		if (y - height / 2 < 0)
			return null;

		if (x + width / 2 > 1)
			return null;
		if (y + height / 2 > 1)
			return null;

		return constraint;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#getModelChildren() Overrident to make sure
	 * links appear after both their source and target elements
	 */
	@SuppressWarnings("unchecked")
	protected List getModelChildren()
	{
		List children = super.getModelChildren();
		if (open) // nothing to do if closed
		{
			// Make sure that links appear after (and on top of) their source and target nodes
			
			for (int i=0;i<children.size();i++)
			{
				ModelObjectWrapper linkWrapper = (ModelObjectWrapper)children.get(i);
				if (! (linkWrapper.getElement() instanceof Link))
					continue;
				Link link = (Link)linkWrapper.getElement();
				for (int j=i+1; j<children.size(); j++)
				{
					ModelObjectWrapper otherWrapper = (ModelObjectWrapper)children.get(j);
					ModelElement other = otherWrapper.getElement();
					if (other == link.getSourceElement() || other == link.getTargetElement())
					{
						children.set(i, otherWrapper);
						children.set(j, linkWrapper);
						break;
					}
				}
				
			}
			
		}
		return children;
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.editparts;

import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.editpolicies.ResizableEditPolicy;
import org.eclipse.gef.editpolicies.XYLayoutEditPolicy;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gef.requests.CreateRequest;

import tersus.editor.Debug;
import tersus.editor.figures.FlowRectangle;
import tersus.editor.figures.NoteEditPart;
import tersus.editor.figures.PrecisionRectangle;
import tersus.editor.layout.LayoutConstraint;
import tersus.editor.properties.ModelObjectWrapper;
import tersus.editor.requests.DropModelObjectRequest;
import tersus.model.BuiltinProperties;
import tersus.model.FlowModel;
import tersus.model.Link;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.RelativePosition;
import tersus.model.RelativeSize;
import tersus.model.Slot;
import tersus.model.SlotType;
import tersus.model.SubFlow;
import tersus.model.commands.MultiStepCommand;
import tersus.model.commands.SetPropertyValueCommand;
import tersus.util.Misc;
import tersus.util.Trace;

public class TersusXYLayoutEditPolicy extends XYLayoutEditPolicy
{

	/**
	 * @see org.eclipse.gef.editpolicies.ConstrainedLayoutEditPolicy#createAddCommand(EditPart,
	 *      Object)
	 **/
	protected Command createAddCommand(EditPart child, Object constraint)
	{
		if (Debug.OTHER)
			Trace.add("Called HelloXYLayoutEditPolicy.createAddCommand() with child=" + child
					+ ",constraint=" + constraint);
		return null;
	}

	/**
	 * @see org.eclipse.gef.editpolicies.ConstrainedLayoutEditPolicy#createChangeConstraintCommand(EditPart,
	 *      Object)
	 **/
	protected Command createChangeConstraintCommand(GraphicalEditPart child,
			ChangeBoundsRequest request)
	{
		Point moveDelta = request.getMoveDelta();
		Dimension sizeDelta = request.getSizeDelta();

		if (Debug.LAYOUT)
			Trace.push("TersusxYayoutEditPolicy.createChangeConstraintCommand() with child="
					+ child + " moveDelta=" + moveDelta + " sizeDelta=" + sizeDelta);
		try
		{
			PrecisionRectangle newBounds = getNewBounds(request, child);
			TersusEditPart childEditPart = (TersusEditPart) child;
			Rectangle currentBounds = childEditPart.getFigure().getBounds();
			RelativeSize currentSize = ((TersusEditPart) child).getModelElement().getSize();
			RelativePosition currentPosition = ((TersusEditPart) child).getModelElement()
					.getPosition();
			FlowEditPart parentFlowEditPart = (FlowEditPart) getHost();
			FlowRectangle parentFigure = parentFlowEditPart.getFlowRectangle();
			PrecisionRectangle parentFrame = parentFigure.getFrame();
			if (Debug.LAYOUT)
				Trace.add("parentFrame=" + parentFrame);
			double width = newBounds.preciseWidth / parentFrame.preciseWidth;
			double height = newBounds.preciseHeight / parentFrame.preciseHeight;
			double minX = (newBounds.preciseX - parentFrame.preciseX)
					/ (double) parentFrame.preciseWidth;
			double minY = (newBounds.preciseY - parentFrame.preciseY)
					/ (double) parentFrame.preciseHeight;
			double centerX = minX + width / 2;
			double centerY = minY + height / 2;
			if (Debug.LAYOUT)
			{
				Trace.add("current position:" + currentPosition + " current size:" + currentSize);
				Trace.add("New position:" + centerX + "," + centerY);
				Trace.add("New size:" + width + "," + height);
			}
			if (sizeDelta.width == 0)
			{
				if (currentSize != null)
					width = currentSize.getWidth();
			}
			if (sizeDelta.height == 0)
			{
				if (currentSize != null)
					height = currentSize.getHeight();
			}

			if (moveDelta.x == 0 && sizeDelta.width == 0)
			{
				if (currentPosition != null)
					centerX = currentPosition.getX();
			}
			if (moveDelta.y == 0 && sizeDelta.height == 0)
			{
				if (currentPosition != null)					
					centerY = currentPosition.getY();
			}
			if (Debug.LAYOUT)
				Trace.add(" current bounds=" + currentBounds + " new bounds=" + newBounds
						+ " current size=" + currentSize + " current position=" + currentPosition);

			LayoutConstraint layoutConstraint = new LayoutConstraint(centerX, centerY, width,
					height);
			if (Debug.LAYOUT)
				Trace.add(" new constraint (raw)=" + layoutConstraint);
			if (childEditPart instanceof SlotEditPart)
				layoutConstraint = SlotEditPart.adjustConstraint(layoutConstraint);
			else if (childEditPart instanceof SubFlowEditPart
					|| childEditPart instanceof FlowDataEditPart
					|| childEditPart instanceof NoteEditPart)
				layoutConstraint = FlowEditPart.adjustConstraint(layoutConstraint);
			else
				Misc.assertion(false); // unexpected type of EditPart
			if (Debug.LAYOUT)
				Trace.add(" adjusted constraint=" + layoutConstraint);
			if (layoutConstraint == null)
				return null;
			if (Debug.LAYOUT)
				Trace.add(" adjusted constraint=" + layoutConstraint);

			ModelElement element = childEditPart.getModelElement();
			Command moveCommand = new SetPropertyValueCommand(element, BuiltinProperties.POSITION,
					layoutConstraint.getPosition().getCopy());
			boolean moveOnly = sizeDelta.width == 0 && sizeDelta.height == 0;
			if (moveOnly)
			{
				return moveCommand;
			}
			CompoundCommand command = new CompoundCommand("Resize " + element);
			command.add(moveCommand);
			command.add(new SetPropertyValueCommand(element, BuiltinProperties.SIZE,
					layoutConstraint.getSize().getCopy()));
			if (element instanceof SubFlow && !isProportionalResize(request)
					&& !(childEditPart instanceof MissingModelEditPart))
			{
				// Update internal layout to reflect squeezing or stretching
				double widthProportion = newBounds.preciseWidth / newBounds.preciseHeight;
				command.add(new SetPropertyValueCommand(element.getReferredModel(),
						BuiltinProperties.WIDTH, new Double(widthProportion)));
				FlowModel childModel = (FlowModel) element.getReferredModel();
				PrecisionRectangle preciseBounds = (PrecisionRectangle) currentBounds;
				double horizontalScale = newBounds.preciseWidth / preciseBounds.preciseWidth;
				double verticalScale = newBounds.preciseHeight / preciseBounds.preciseHeight;
				command.add(getRearrangeChildrenCommand(childModel, horizontalScale, verticalScale,
						request.getResizeDirection()));
			}

			return command;
		}
		finally
		{
			if (Debug.LAYOUT)
				Trace.pop();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.ContainerEditPolicy#getCreateCommand(org.eclipse.gef.requests
	 * .CreateRequest)
	 */

	/**
	 * Rearranges the children of a FlowModel to fit a changed scale
	 * 
	 * The logic is the following:
	 * 
	 * If the frame is "enlarged", elements maintain their screen positions (i.e. positions are
	 * scaled reversely, with slots remaining on the appropriate side)
	 * 
	 * If the frame is "shrunk", elements maintain their screen positions "as long as possible" and
	 * are then shrunk proportionally
	 * 
	 * @param childModel
	 * @param horizontalScale
	 * @param verticalScale
	 * @return
	 */
	private Command getRearrangeChildrenCommand(FlowModel model, double horizontalScale,
			double verticalScale, int resizeDirection)
	{
		if (Debug.LAYOUT)
			Trace.add("Scaling elements h=" + horizontalScale + " v=" + verticalScale);
		CompoundCommand command = new CompoundCommand("Scale contents of " + model);
		Misc.assertion(verticalScale == 1 || horizontalScale == 1); // simultaneous scaling not
		// taken into account
		double maxX = 0, maxY = 0;
		double minX = 1, minY = 1;

		List<ModelElement> elements = model.getElements();
		for (ModelElement element : elements)
		{
			if (element instanceof Link)
				continue; // No position data to scale (currently)
			RelativePosition position = element.getPosition();
			double x = position.getX();
			double y = position.getY();
			double right, left, top, bottom;
			if (element instanceof Slot)
			{
				right = left = x;
				top = bottom = y;
			}
			else
			{
				RelativeSize size = element.getSize();
				if (size == null)
					size = new RelativeSize(0.3,0.3);
				right = x + size.getWidth() / 2;
				left = x - size.getWidth() / 2;
				top = y - size.getHeight() / 2;
				bottom = y + size.getHeight() / 2;
			}
			if (x > 0 && x < 1)
			{
				maxX = Math.max(maxX, right);
				minX = Math.min(minX, left);
			}
			if (y > 0 && y < 1)
			{
				maxY = Math.max(maxY, bottom);
				minY = Math.min(minY, top);
			}
		}
		boolean east = (0 != (resizeDirection & PositionConstants.EAST));
		boolean west = (0 != (resizeDirection & PositionConstants.WEST));
		boolean north = (0 != (resizeDirection & PositionConstants.NORTH));
		boolean south = (0 != (resizeDirection & PositionConstants.SOUTH));

		if (south & maxY > verticalScale)
			verticalScale = maxY;
		if (north && 1 - minY > verticalScale)
			verticalScale = 1 - minY;
		if (east && maxX > horizontalScale)
			horizontalScale = maxX;
		if (west && 1 - minX > horizontalScale)
			horizontalScale = 1 - minX;
		if (Debug.LAYOUT)
			Trace.add("Horizontal scale=" + horizontalScale + " verticalScale=" + verticalScale);

		for (ModelElement element : elements)
		{
			if (element instanceof Link)
				continue; // No position data to scale (currently)
			RelativePosition position = element.getPosition();
			double x = position.getX();
			double y = position.getY();
			if (x > 0 && x < 1)
			{
				if (east)
					x = x / horizontalScale;
				if (west)
					x = 1 - (1 - x) / horizontalScale;
			}
			if (y > 0 && y < 1)
			{
				if (south)
					y = y / verticalScale;
				if (north)
					y = 1 - (1 - y) / verticalScale;
			}
			command.add(new SetPropertyValueCommand(element, BuiltinProperties.POSITION,
					new RelativePosition(x, y)));
			RelativeSize size = element.getSize();
			if (size != null)
			{
				command.add(new SetPropertyValueCommand(element, BuiltinProperties.SIZE,
						new RelativeSize(size.getWidth() / horizontalScale, size.getHeight()
								/ verticalScale)));
			}
		}
		if (command.getChildren().length == 0)
			return null;
		return command;

	}

	/**
	 * Returns true if this request represents a proportional resize
	 * 
	 * @param request
	 * @return
	 */
	private boolean isProportionalResize(ChangeBoundsRequest request)
	{
		/**
		 * The following logic depends on the behavior of TersusResizeTracker. If the tracker allows
		 * non-proportional resize which is not vertical or horizontal, this method will return a
		 * wrong result
		 */
		Dimension sizeDelta = request.getSizeDelta();
		return (sizeDelta.height != 0 && sizeDelta.width != 0);
	}

	/**
	 * @see org.eclipse.gef.editpolicies.LayoutEditPolicy#createChildEditPolicy(EditPart)
	 **/
	protected EditPolicy createChildEditPolicy(EditPart child)
	{
		if (Debug.OTHER)
			Trace
					.add("Called TersusXYLayoutEditPolicy.createChildEditPolicy() with child="
							+ child);
		if (child instanceof SlotEditPart)
			return new NonResizableEditPolicy();
		else if (child instanceof LinkEditPart)
			return new LinkEndpointEditPolicy();
		else
		{
			Model childParent = null;
			if (child.getParent() instanceof SubFlowEditPart)
				childParent = ((SubFlowEditPart) child.getParent()).getSubFlow().getReferredModel();
			else if (child.getParent() instanceof TopFlowEditPart)
			{
				TopFlowEditPart topFlowEditPart = (TopFlowEditPart) child.getParent();
				childParent = ((ModelObjectWrapper) topFlowEditPart.getModel()).getModel();
			}

			if (!(childParent != null && childParent.getPackage().isReadOnly()))
			{
				if (child instanceof FlowDataEditPart || child instanceof NoteEditPart)
					return new ResizableEditPolicy();
				else
					return new ResizeSubFlowEditPolicy();
			}

			return null;
		}
	}

	/**
	 * @see org.eclipse.gef.editpolicies.LayoutEditPolicy#getDeleteDependantCommand(Request)
	 **/
	protected Command getDeleteDependantCommand(Request request)
	{
		if (Debug.OTHER)
			Trace.add("Called HelloXYLayoutEditPolicy.getDeleteDependantCommand() with req="
					+ request);
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.ConstrainedLayoutEditPolicy#getConstraintFor(org.eclipse.gef
	 * .requests.ChangeBoundsRequest, org.eclipse.gef.GraphicalEditPart)
	 */
	protected PrecisionRectangle getNewBounds(ChangeBoundsRequest request, GraphicalEditPart child)
	{
		PrecisionRectangle rect = new PrecisionRectangle(child.getFigure().getBounds());
		Point moveDelta = request.getMoveDelta();
		Dimension sizeDelta = request.getSizeDelta();
		rect.preciseX += moveDelta.x;
		rect.preciseY += moveDelta.y;
		if (sizeDelta.width != 0 || sizeDelta.height != 0)
		{

			rect.preciseWidth = rect.preciseWidth + sizeDelta.width;
			rect.preciseHeight = rect.preciseHeight + sizeDelta.height;
			Dimension minSize = getMinimumSizeFor(child);
			if (rect.preciseWidth < minSize.width)
				rect.preciseWidth = minSize.width;
			if (rect.preciseHeight < minSize.height)
				rect.preciseHeight = minSize.height;
			// adjust proportions if proportional resize of a SubFlow
			if (child instanceof SubFlowEditPart && isProportionalResize(request))
			{
				double currentWidthProportion = ((SubFlowEditPart) child).getFlowModel().getWidth();
				if (rect.preciseWidth > rect.preciseHeight * currentWidthProportion)
					rect.preciseWidth = rect.preciseHeight * currentWidthProportion;
				else
					rect.preciseHeight = rect.preciseWidth / currentWidthProportion;
			}

		}

		rect.calculate();
		return rect;
	}

	/*
	 * Overriden to allow access to the request when creating the command
	 */
	protected Command getResizeChildrenCommand(ChangeBoundsRequest request)
	{
		CompoundCommand resize = new CompoundCommand();
		Command c;
		GraphicalEditPart child;
		List<?> children = request.getEditParts();

		for (int i = 0; i < children.size(); i++)
		{
			child = (GraphicalEditPart) children.get(i);

			c = createChangeConstraintCommand(child, request);
			resize.add(c);
		}
		return resize.unwrap();
	} /*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.gef.editpolicies.LayoutEditPolicy#getCreateCommand(org.eclipse.gef.requests.
	 * CreateRequest)
	 */

	protected Command getCreateCommand(CreateRequest request)
	{
		if (Debug.CREATE)
		{
			Trace.push("getCreateCommand() host=" + getHost());
		}
		try
		{
			// NICE2 merge with DropModelElementRequest
			if (!(getHost() instanceof FlowEditPart))
			{
				if (Debug.CREATE)
					Trace.add("request ignored - host is not a FlowEditPart");
				return null;
			}
			Object obj = request.getNewObject();
			FlowEditPart parent = (FlowEditPart) getHost();
			FlowRectangle parentFigure = parent.getFlowRectangle();
			PrecisionRectangle parentFrame = parentFigure.getFrame();
			Point location = request.getLocation().getCopy();
			parentFigure.translateToRelative(location);
			double centerX = (location.x - parentFrame.preciseX) / parentFrame.preciseWidth;
			double centerY = (location.y - parentFrame.preciseY) / parentFrame.preciseHeight;
			if (Debug.CREATE)
			{
				Trace.add("obj=" + obj);
				Trace.add("absolute location=" + request.getLocation());
				Trace.add("parentFrame=" + parentFrame);
				Trace.add("relative location=" + location + " relativePosition=" + centerX + ","
						+ centerY);
			}
			if (obj instanceof SlotType)
			{
				FlowModel parentModel = ((FlowEditPart) getHost()).getFlowModel();
				if (!parentModel.getPackage().isReadOnly())
				{
					SlotType slotType = (SlotType) obj;

					double size = 2 * parentFigure.getMargin() / parentFrame.preciseWidth;
					LayoutConstraint constraint = new LayoutConstraint(centerX, centerY, size, size);
					if (Debug.CREATE)
						Trace.add("raw constraint=" + constraint);
					constraint = SlotEditPart.adjustConstraint(constraint);
					if (Debug.CREATE)
						Trace.add("adjusted constraint=" + constraint);
					if (constraint == null)
						return null; // not a valid location for a slot

					return MultiStepCommand.createAddSlotCommand(parentModel, slotType, null, null,
							false, true, constraint.getPosition());
				}
				return null;
			}
			else
				return null;
		}
		finally
		{
			if (Debug.CREATE)
				Trace.pop();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.LayoutEditPolicy#getAddCommand(org.eclipse.gef.Request)
	 */
	protected Command getAddCommand(Request generic)
	{
		return null;
	}

	/*
	 * Null implementation is irrevelvant because getResizeChildrenCommand was overriden
	 */
	protected Command createChangeConstraintCommand(EditPart child, Object constraint)
	{
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.EditPolicy#eraseTargetFeedback(org.eclipse.gef.Request)
	 */
	public void eraseTargetFeedback(Request request)
	{
		if (request instanceof DropModelObjectRequest)
			eraseSizeOnDropFeedback(request);
		else
			super.eraseTargetFeedback(request);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.EditPolicy#showTargetFeedback(org.eclipse.gef.Request)
	 */
	public void showTargetFeedback(Request request)
	{
		Model parentModel = null;

		if (getHost() instanceof FlowEditPart)
		{
			parentModel = ((FlowEditPart) getHost()).getFlowModel();
		}

		if (parentModel != null && !parentModel.getPackage().isReadOnly())
		{
			if (request instanceof DropModelObjectRequest)
				showSizeOnDropFeedback((DropModelObjectRequest) request);
			else
				super.showTargetFeedback(request);
		}
	}

	/**
	 * @param request
	 */
	private void showSizeOnDropFeedback(DropModelObjectRequest request)
	{
		Point p = new Point(request.getLocation());
		IFigure feedback = getSizeOnDropFeedback();
		feedback.translateToRelative(p);
		Dimension size = request.getSize();
		if (size != null)
		{
			Rectangle b = new Rectangle(p, size);
			feedback.setBounds(b);
		}
		else
			feedback.setBounds(new Rectangle(p, p));

	}

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.editparts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.ChangeEvent;
import org.eclipse.draw2d.ChangeListener;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.widgets.Text;

import tersus.editor.Debug;
import tersus.editor.TersusEditor;
import tersus.editor.figures.FlowRectangle;
import tersus.editor.figures.OpenToggle;
import tersus.editor.properties.ModelObjectWrapper;
import tersus.model.BuiltinProperties;
import tersus.model.Composition;
import tersus.model.DataElement;
import tersus.model.DataType;
import tersus.model.FlowModel;
import tersus.model.Link;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.Note;
import tersus.model.Role;
import tersus.model.Slot;
import tersus.model.SubFlow;
import tersus.util.Misc;
import tersus.util.Trace;
import tersus.workbench.ImageLocator;

/**
 * An edit part that represents models or sub-models (Flow Models, Sub Flows, and Data Elements)
 */
public abstract class ModelEditPart extends TersusEditPart
{
	protected void refreshTraversingLinkEnds()
	{
	}

	protected OpenToggle toggle;

	protected boolean open = false;

	public FlowModel getFlowModel()
	{
		ModelObjectWrapper wrapper = (ModelObjectWrapper) getModel();
		return (FlowModel) wrapper.getModel();
	}

	public boolean isOpen()
	{
		return open;
	}

	public void setOpen(boolean b)
	{
		if (open != b)
		{
			open = b;
			if (toggle != null)
			{
				toggle.setOpen(open);
			}
			((FlowRectangle) getFigure()).setReadOnly(isReadOnly());

			((FlowRectangle) getFigure()).setOpen(open);

			refreshLabel();
			refreshColor();
			refreshLine();
			refreshChildren();
			getFigure().revalidate();
		}
	}

	protected void addToggle(IFigure parent)
	{
		((FlowRectangle) getFigure()).setReadOnly(isReadOnly());
		toggle = new OpenToggle((FlowRectangle) getFigure());
		toggle.setOpen(false);
		ChangeListener toggleListener = new ChangeListener()
		{

			public void handleStateChanged(ChangeEvent event)
			{
				if (Debug.OTHER)
					Trace.add("OpenToggle of " + this + " changed (" + toggle.isOpen());
				setOpen(toggle.isOpen());
			}
		};
		toggle.addChangeListener(toggleListener);
		toggle.setOpaque(true);
		parent.add(toggle, 0); // Toggle must always be the first child

	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractEditPart#getModelChildren() You must implement this
	 *      method if you want you root model to have children!
	 */
	protected List<ModelObjectWrapper> getModelChildren()
	{

		if (Debug.OTHER)
			Trace.push("ModelEditPart.getModelChildren() model=" + getModel());
		try
		{
			Model model = getTersusModel();
			Misc.assertion(model != null);

			List<ModelObjectWrapper> children = new ArrayList<ModelObjectWrapper>();

			for (ModelElement element : model.getElements())
			{
				if (element instanceof Slot)
					children.add(new ModelObjectWrapper(element, false));
				else if (open && (element instanceof SubFlow || element instanceof DataElement))
				{
					
					Model referredModel = element.getReferredModel();
					if (referredModel == null)
						children.add(new ModelObjectWrapper(element, true));
					else
						children.add(new ModelObjectWrapper(referredModel, element));
					
				}
				else if (open && (element instanceof Link || element instanceof Note))
					children.add(new ModelObjectWrapper(element, false));
			}
			if (Debug.OTHER)
				Trace.add("returning list of " + children.size() + " children");
			return children;
		}
		finally
		{
			if (Debug.OTHER)
				Trace.pop();
		}
	}

	void refreshComposite()
	{
		if (isComposite() && toggle == null)
		{
			addToggle(getFigure());
			setOpen(getDefaultOpenState());
		}
		else if (!isComposite() && toggle != null)
		{
			getFigure().remove(toggle);
			setOpen(false);
			toggle = null;
		}
	}

	protected boolean getDefaultOpenState()
	{
		return false;
	}

	public TersusEditPart getChild(Role role)
	{
		for (Object obj : getChildren())
		{
			TersusEditPart part = (TersusEditPart) obj;
			if (part.getRole() == role)
				return part;
		}
		return null;
	}

	/**
	 * Adds the child's Figure to the {@link #getContentPane() contentPane}.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#addChildVisual(EditPart, int) Make sure that
	 *      the toggle remains at index 0 of the figure
	 */
	protected void addChildVisual(EditPart childEditPart, int index)
	{
		IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		// The toggle, if it exists, must be the content pane's first child
		if (toggle != null)
			++index;
		getContentPane().add(child, index);
	}

	protected ImageLocator getImageLocator()
	{
		Model model = getTersusModel();
		return TersusEditor.getImageLocator(model);
	}

	protected void createEditPolicies()
	{
		// TODO Review this auto-generated method stub

	}

	protected IFigure createFigure()
	{
		// TODO Review this auto-generated method stub
		return null;
	}

	protected void refreshVisuals()
	{
		((FlowRectangle) getFigure()).setImageLocator(getImageLocator());
		((FlowRectangle) getFigure()).setReadOnly(isReadOnly());

		super.refreshVisuals();
		refreshComposite();
	}

	public CellEditorLocator getRoleEditorLocator()
	{
		CellEditorLocator locator = new CellEditorLocator()
		{
			public void relocate(CellEditor celleditor)
			{
				Text text = (Text) celleditor.getControl();
				org.eclipse.swt.graphics.Point preferredSize = text.computeSize(-1, -1);
				FlowRectangle rect = (FlowRectangle) getFigure();
				Point textOrigin;
				if (rect.paintText)
					textOrigin = new Point(rect.getTextX(), rect.getTextY());
				else
					textOrigin = rect.getBounds().getLocation();
				rect.translateToAbsolute(textOrigin);
				text.setBounds(textOrigin.x - 4, textOrigin.y - 1, preferredSize.x + 1,
						preferredSize.y + 1);
			}
		};

		return locator;
	}

	protected void refreshLabel() // Overridden for FlowDataEditPart &
	// SubDataEditPart
	{
		Role role = getRole();
		String name = getTersusModel().getName();
		String label;
		String stereotype = calculateStereotypeLabel();
		String stereotypePart = null;
		if (stereotype != null)
			stereotypePart = "<<" + stereotype + ">>";
		String roleStr = role == null ? null : role.toString();
		String separator = getLabelSeparator();
		String namePart = "[" + name + "]";
		if (role == null)
		{

			label = namePart;
			if (stereotype != null)
			{
				label += separator + stereotypePart;
			}
		}
		else
		{

			if (Misc.equal(roleStr, stereotype) && Misc.equal(roleStr, name))
				label = roleStr;
			else
			{
				if (Misc.equal(roleStr, name))
				{
					label = roleStr;
					if (stereotype != null)
						label += separator + stereotypePart;
				}
				else if (Misc.equal(roleStr, stereotype))
				{
					label = roleStr + separator + namePart;
				}
				else
				// 3 different parts
				{
					label = roleStr;
					if (stereotype != null)
						label += (separator + stereotypePart);
					label += separator + namePart;
				}

			}
		}

		((FlowRectangle) getFigure()).setLabel(label);
		((FlowRectangle) getFigure()).setReadOnly(isReadOnly());
		getFigure().revalidate();
	}

	protected String getLabelSeparator()
	{
		String separator = isOpen() || !isComposite() ? " " : "\n";
		return separator;
	}

	protected void elementChanged(Object sourceObject, String property, Object oldValue,
			Object newValue)
	{
		super.elementChanged(sourceObject, property, oldValue, newValue);
	}

	/**
	 * @return true if tersus model is read only ( We ignore atomic dataType Models )
	 */
	protected boolean isReadOnly()
	{
		Model tersusModel = getTersusModel();

		if (tersusModel == null)
			return false;

		if (tersusModel instanceof DataType
				&& ((DataType) tersusModel).getProperty(BuiltinProperties.COMPOSITION).equals(
						Composition.ATOMIC))
			return false;

		return tersusModel.isReadOnly();
	}
}
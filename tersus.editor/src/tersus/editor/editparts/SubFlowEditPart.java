/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.editparts;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseMotionListener;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;

import tersus.editor.Debug;
import tersus.editor.figures.DataRectangle;
import tersus.editor.figures.FlowRectangle;
import tersus.editor.layout.LayoutConstraint;
import tersus.editor.layout.FlowLayout;
import tersus.model.BuiltinProperties;
import tersus.model.FlowModel;
import tersus.model.FlowType;
import tersus.model.ModelElement;
import tersus.model.SubFlow;
import tersus.util.Misc;
import tersus.util.Trace;

/**
 * 
 */
public class SubFlowEditPart extends FlowEditPart
{
	Boolean parity;

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.editor.FlowModelEditPart#getFlowModel()
	 */
	protected IFigure createFigure()
	{
		if (Debug.OTHER)
			Trace.add("Called TopProcessEditPart.createFigure()");

		FlowRectangle f = new FlowRectangle();
		f.setLayoutManager(new FlowLayout());
		// f.setLabel(getSubFlow().getRole().toString());
		f.setOpaque(true);

		final FlowRectangle finalFigure = f;

		f.addMouseMotionListener(new MouseMotionListener()
		{
			public void mouseDragged(MouseEvent me)
			{
			}

			public void mouseEntered(MouseEvent me)
			{
			}

			public void mouseExited(MouseEvent me)
			{
			}

			public void mouseHover(MouseEvent me)
			{
				ModelElement element = getModelElement();
				if (element != null)
				{
					String toolTipText;
					if (element.getElementName().equals(element.getReferredModel().getName()))
						toolTipText = element.getElementName();
					else
						toolTipText = element.getElementName() + " ["
								+ element.getReferredModel().getName() + "]";

					finalFigure.setToolTip(new Label(toolTipText));
				}
			}

			public void mouseMoved(MouseEvent me)
			{
			}
		});

		return f;
	}

	protected boolean getParity()
	{
		if (parity == null)
		{
			FlowEditPart parent = ((FlowEditPart) getParent());
			FlowType type = getFlowModel().getType();
			FlowType parentType = parent.getFlowModel().getType();
			if (parentType == FlowType.SYSTEM && type != FlowType.SYSTEM)
				parity = Boolean.TRUE;
			else
				parity = Misc.getBoolean(!parent.getParity());
		}
		return parity.booleanValue();
	}

	public SubFlow getSubFlow()
	{
		return (SubFlow) getModelElement();
	}

	protected void refreshLayout()
	{
		super.refreshLayout();
		FlowModel parentModel = ((FlowEditPart) getParent()).getFlowModel();
		LayoutConstraint constraint = new LayoutConstraint(parentModel, getSubFlow());
		((GraphicalEditPart) getParent()).setLayoutConstraint(this, getFigure(), constraint);

	}

	protected void elementChanged(Object sourceObject, String property, Object oldValue,
			Object newValue)
	{
		if (BuiltinProperties.ALWAYS_CREATE.equals(property))
		{
			refreshLine();
		}
		else
			super.elementChanged(sourceObject, property, oldValue, newValue);
	}

	protected void refreshLine()
	{
		ModelElement e = getModelElement();
		FlowRectangle f = (FlowRectangle) getFigure();

		Object alwaysCreateVal = e.getProperty(BuiltinProperties.ALWAYS_CREATE);
		if (alwaysCreateVal == null
				|| Boolean.TRUE.equals(alwaysCreateVal)
				|| (alwaysCreateVal instanceof String && "true".equals(((String) alwaysCreateVal)
						.toLowerCase())))
			f.setLineColor(ColorConstants.black);
		else
			f.setLineColor(ColorConstants.lightGray);

	}

	protected void createEditPolicies()
	{
		installEditPolicy(EditPolicy.COMPONENT_ROLE, new ElementEditPolicy());
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, new TersusDirectEditPolicy());
		super.createEditPolicies();
	}

	protected void refreshChildren()
	{
		super.refreshChildren();
		/*
		 * Link ends of the parent flow need to be refreshed, as links of the parent may reference
		 * children of the sub-flow
		 */
		refreshTraversingLinkEnds();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.editor.editparts.TersusEditPart#refreshTraversingLinkEnds()
	 */
	protected void refreshTraversingLinkEnds()
	{
		// The links of the parent flow may be affected
		FlowEditPart containingFlowEditPart = (FlowEditPart) getParent();
		if (containingFlowEditPart != null)
			containingFlowEditPart.refreshLinkEnds();
		/*
		 * The containing flow can be null if the edit part is 'orphaned'. This happens temporarily
		 * when removing model elements.
		 */
	}

	protected void refreshVisuals()
	{
		super.refreshVisuals();
		refreshLine();
	}
}

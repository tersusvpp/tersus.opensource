/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.editparts;

import java.beans.PropertyChangeEvent;

import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.LayerConstants;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Text;

import tersus.InternalErrorException;
import tersus.editor.Debug;
import tersus.editor.figures.LinkArrow;
import tersus.editor.figures.SlotFigure;
import tersus.editor.figures.SlotLabel;
import tersus.editor.layout.LayoutConstraint;
import tersus.editor.preferences.Preferences;
import tersus.editor.properties.ModelObjectWrapper;
import tersus.model.BuiltinProperties;
import tersus.model.RelativePosition;
import tersus.model.Role;
import tersus.model.Slot;
import tersus.model.SlotType;
import tersus.util.Misc;
import tersus.util.Trace;

/**
 *  
 */
public class SlotEditPart extends TersusEditPart implements NodeEditPart
{

    /**
     * We use the connection layer for our labels.
     */
    private static final String LABEL_LAYER = LayerConstants.CONNECTION_LAYER;

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
     */
    protected IFigure createFigure()
    {
        SlotFigure figure = new SlotFigure();
        figure.setName(getPath().toString());
        figure.setOpaque(true);
        return figure;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
     */

    protected void createEditPolicies()
    {
        installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE,
                new TersusDirectEditPolicy());
        installEditPolicy(EditPolicy.COMPONENT_ROLE, new SlotEditPolicy());
        installEditPolicy(EditPolicy.LAYOUT_ROLE,
                new TersusXYLayoutEditPolicy());
        installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE,
                new LinkableEditPolicy());
    }

    protected void refreshLayout()
    {
        FlowEditPart parentFlowEditPart = (FlowEditPart) getParent();
        if (Debug.OTHER)
            Trace.push("SlotEditPart.refreshVisuals(). parent="
                    + parentFlowEditPart + " this=" + this);
        try
        {
            refreshShape();
            refreshOrientation();
            parentFlowEditPart.setLayoutConstraint(this, getFigure(),
                    new LayoutConstraint(getModelElement()));

        }
        finally
        {
            if (Debug.OTHER)
                Trace.pop();

        }
    }

    /**
     *  
     */
    private void refreshOrientation()
    {
        int side = getSide();
        ((SlotFigure) getFigure()).setSide(side);
        ((SlotFigure) getFigure()).setOrientation(getOrientation(side));
    }

    /**
     *  
     */
    private int getSide()
    {
        RelativePosition p = getModelElement().getPosition();
        double epsilon = 0.000001;
        if (p.getX() < epsilon)
            return PositionConstants.LEFT;
        if (p.getX() > 1 - epsilon)
            return PositionConstants.RIGHT;
        if (p.getY() < epsilon)
            return PositionConstants.TOP;
        Misc.assertion(p.getY() > 1 - epsilon);
        return PositionConstants.BOTTOM;
    }

    private int getOrientation(int side)
    {
        SlotType type = getSlot().getType();
        if (type.isOutput())
            return side;
        else
        // reverse the side
        {
            switch (side)
            {
                case PositionConstants.RIGHT:
                    return PositionConstants.LEFT;
                case PositionConstants.LEFT:
                    return PositionConstants.RIGHT;
                case PositionConstants.TOP:
                    return PositionConstants.BOTTOM;
                default:
                    Misc.assertion(side == PositionConstants.BOTTOM);
                    return PositionConstants.TOP;
            }
        }

    }

    public Slot getSlot()
    {
        ModelObjectWrapper wrapper = (ModelObjectWrapper) getModel();
        return (Slot) wrapper.getElement();

    }

    public ConnectionAnchor getSourceConnectionAnchor(
            ConnectionEditPart connection)
    {
        return ((SlotFigure) getFigure()).getSourceAnchor();
    }

    public ConnectionAnchor getTargetConnectionAnchor(
            ConnectionEditPart connection)
    {
        return ((SlotFigure) getFigure()).getTargetAnchor();
    }

    public ConnectionAnchor getSourceConnectionAnchor(Request request)
    {
        return ((SlotFigure) getFigure()).getSourceAnchor();
    }

    public ConnectionAnchor getTargetConnectionAnchor(Request request)
    {
        return ((SlotFigure) getFigure()).getTargetAnchor();
    }

    protected void refreshColor()
    {
        String preferenceName;
        SlotType type = getSlot().getType();
        boolean mandatory = getSlot().isMandatory();
        if (type == SlotType.TRIGGER)
            preferenceName = mandatory ? Preferences.MANDATORY_TRIGGER_COLOR : Preferences.NON_MANDATORY_TRIGGER_COLOR;
        else if (type == SlotType.INPUT)
            preferenceName = Preferences.INPUT_COLOR;
        else if (type == SlotType.EXIT)
            preferenceName = Preferences.EXIT_COLOR;
        else if (type == SlotType.ERROR)
            preferenceName = Preferences.ERROR_COLOR;
        else if (type == SlotType.IO)
            preferenceName = Preferences.MANDATORY_TRIGGER_COLOR;
        else
            throw new InternalErrorException("Unexpected SlotType (" + type
                    + ") path=" + getPath());
        Color backgroundColor = Preferences.getColor(preferenceName);

        getFigure().setBackgroundColor(backgroundColor);
    }

	protected void elementChanged(Object sourceObject, String property, Object oldValue, Object newValue)
    {
        if (BuiltinProperties.TYPE.equals(property))
        {
            refreshColor();
            refreshShape();
            refreshOrientation();
            refreshTraversingLinkEnds();

        }
        if (BuiltinProperties.MANDATORY.equals(property))
        {
            refreshColor();
        }
        if (BuiltinProperties.DATA_TYPE_ID.equals(property))
            refreshTraversingLinkEnds();
        else
			super.elementChanged(sourceObject,property, oldValue, newValue);
    }

    private void refreshShape()
    {
        if (getSlot().getType() == SlotType.IO)
        {
            getSlotFigure().setShape(SlotFigure.DIAMOND);
        }
        else
        {
            getSlotFigure().setShape(SlotFigure.TRIANGLE);
        }

    }

    public static LayoutConstraint adjustConstraint(LayoutConstraint constraint)
    {
        double threshold = 0.1;
        double x = constraint.getX();
        double y = constraint.getY();
        double deltaX = Math.min(Math.abs(x), Math.abs(x - 1));
        double deltaY = Math.min(Math.abs(y), Math.abs(y - 1));
        if (deltaX > threshold && deltaY > threshold)
            return null;
        if (deltaX < deltaY)
        {
            x = Math.round(x);
        }
        else
        {
            y = Math.round(y);
        }
        constraint.setPosition(x, y);
        return constraint;
    }

    public CellEditorLocator getRoleEditorLocator()
    {
        CellEditorLocator locator = new CellEditorLocator() {

            public void relocate(CellEditor celleditor)
            {
                Text text = (Text) celleditor.getControl();
                org.eclipse.swt.graphics.Point preferredSize = text
                        .computeSize(-1, -1);
                Point textOrigin;
                SlotLabel label = getSlotFigure().getLabel();
                if (label == null)
                {
                    textOrigin = getFigure().getBounds().getLeft();
                    getFigure().translateToAbsolute(textOrigin);
                }
                else
                {
                    textOrigin = label.getLocation();
                    label.translateToAbsolute(textOrigin);
                }
                text.setBounds(textOrigin.x - 4, textOrigin.y - 1,
                        preferredSize.x + 1, preferredSize.y + 1);
            }
        };

        return locator;
    }

    public TersusEditPart getChild(Role role)
    {
        // Slots have no children
        return null;
    }

    protected void refreshTraversingLinkEnds()
    {
        // The links of the parent edit part, may traverse this slot, as do
        // those that traverse the parent
        FlowEditPart parent = (FlowEditPart) getParent();
        parent.refreshLinkEnds();
        parent.refreshTraversingLinkEnds();
    }

    public void removeNotify()
    {
        removeLabel();
        super.removeNotify();
    }

    /**
     * Remove the SlotFigure's label from the label layer (if the SlotFigure has
     * a label)
     *  
     */
    private void removeLabel()
    {
        SlotLabel label = getSlotFigure().getLabel();
        if (label != null)
            getLayer(LABEL_LAYER).remove(label);
        getSlotFigure().removeLabel();
    }

    /**
     * @return The slot's figure. The same as getFigure(), but cast to the
     *         proper type
     */
    private SlotFigure getSlotFigure()
    {
        return (SlotFigure) getFigure();
    }

    /**
     * Adds a LinkEditPart's figure to this slot figure's list of connections
     * 
     * @param part
     *            A LinkEditPart of a link connected from this part's slot.
     */
    public void removeExternalLink(LinkEditPart linkEditPart)
    {
        ((SlotFigure) getFigure()).removeExternalLink((LinkArrow) linkEditPart
                .getFigure());

    }

    /**
     * Removes a LinkEditPart's figure from this slot figure's list of
     * connections
     * 
     * @param part
     *            A LinkEditPart of a link disconnected from this part's slot.
     */
    public void addExternalLink(LinkEditPart linkEditPart)
    {
        ((SlotFigure) getFigure()).addExternalLink((LinkArrow) linkEditPart
                .getFigure());
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.editor.editparts.TersusEditPart#refreshLabel()
     * 
     * Updates the label of this slot, possibly creating or removing the label.
     *  
     */
    protected void refreshLabel()
    {
        String text = getLabelText();
        if (text == null)
            removeLabel();
        else
        {
            SlotLabel label = getSlotFigure().getLabel();
            if (label == null)
            {
                label = getSlotFigure().createLabel(text);
                getLayer(LABEL_LAYER).add(label);
            }
            else
                label.setText(text);
        }
    }

    /**
     * @return The text that should be used for this slot's label, or null if
     *         the slot should not have a label.
     *  
     */
    private String getLabelText()
    {
        String roleStr = getRole().toString();
        String stereotype = calculateStereotypeLabel();
        String stereotypeStr = stereotype == null ? null : "<<" + stereotype
                + ">>";
        if (roleStr.startsWith(getSlot().getType().toString()))
            //Ignored Slot Name
            return null;
        if (getRole().isDistinguished() || stereotype == null)
            return roleStr;
        else if (Misc.equal(stereotype, roleStr))
            return roleStr;
        else
            return (roleStr + " " + stereotypeStr);
    }
}
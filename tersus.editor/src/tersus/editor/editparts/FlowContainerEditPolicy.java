/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.editparts;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.ContainerEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.jface.action.IStatusLineManager;

import tersus.editor.Debug;
import tersus.editor.TersusEditDomain;
import tersus.editor.figures.FlowRectangle;
import tersus.editor.figures.PrecisionRectangle;
import tersus.editor.layout.LayoutConstraint;
import tersus.editor.requests.DropModelObjectRequest;
import tersus.model.FlowModel;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.ModelObject;
import tersus.model.Note;
import tersus.model.TemplateAndPrototypeSupport;
import tersus.model.commands.AddElementHelper;
import tersus.model.commands.AddNoteCommand;
import tersus.model.commands.CreateElementFromTemplateCommand;
import tersus.util.Trace;

public class FlowContainerEditPolicy extends ContainerEditPolicy
{
	public Command getCommand(Request request)
	{
		Command command;
		if (request instanceof DropModelObjectRequest)
		{
			command = getAddElementCommand((DropModelObjectRequest) request);
		}
		else
			command = super.getCommand(request);
		if (Debug.OTHER)
		{
			Trace.add("FlowContainerEditPolicy.getCommand() request=" + request + " type="
					+ request.getType());
			Trace.add("command=" + command);
		}
		return command;
	}

	private Command getAddElementCommand(DropModelObjectRequest request)
	{

		if (Debug.CREATE)
		{
			// Trace.push("getAddElementCommand() host=" + getHost());
		}
		try
		{
			// NICE2 merge with DropModelElementRequest
			if (!(getHost() instanceof FlowEditPart))
			{
				if (Debug.CREATE)
					Trace.add("request ignored - host is not a FlowEditPart");
				return null;
			}
			ModelObject sourceObject = request.getSourceObject();
			FlowEditPart parent = (FlowEditPart) getHost();

			// We didn't find any better place to reset the editor status line.
			IStatusLineManager statusLineManager = ((TersusEditDomain) parent.getRoot().getViewer()
					.getEditDomain()).getEditorPart().getEditorSite().getActionBars()
					.getStatusLineManager();
			statusLineManager.setMessage(null);
			statusLineManager.setErrorMessage(null);

			FlowRectangle parentFigure = parent.getFlowRectangle();
			PrecisionRectangle parentFrame = parentFigure.getFrame();
			Point location = request.getLocation().getCopy();
			parentFigure.translateToRelative(location);
			double centerX = (location.x - parentFrame.preciseX) / parentFrame.preciseWidth;
			double centerY = (location.y - parentFrame.preciseY) / parentFrame.preciseHeight;
			double width = 0.25;
			double height = 0.25;
			Dimension size = request.getSize();
			if (size != null)
			{
				centerX = (location.x - parentFrame.preciseX + size.width / 2)
						/ parentFrame.preciseWidth;
				centerY = (location.y - parentFrame.preciseY + size.height / 2)
						/ parentFrame.preciseHeight;
				height = size.height / parentFrame.preciseHeight;
				width = size.width / parentFrame.preciseWidth;
				if (Debug.CREATE)
				{
					Trace.push("getAddElementCommand(): calculating constraint");
					Trace.add("source object=" + sourceObject);
					Trace.add("absolute location=" + request.getLocation());
					Trace.add("absolute size = " + size);
					Trace.add("relative size = " + width + "," + height);
					Trace.add("parentFrame=" + parentFrame);
					Trace.add("relative location=" + location + " relativePosition=" + centerX
							+ "," + centerY);
					Trace.pop();
				}
			}
			LayoutConstraint constraint = new LayoutConstraint(centerX, centerY, width, height);

			FlowModel parentModel = parent.getFlowModel();
			if (!parentModel.getPackage().isReadOnly())
			{
				if (sourceObject instanceof Model)
				{
					Model addedModel = (Model) sourceObject;
					ModelId addedModelId = addedModel.getId();
					// WorkspaceRepository repository = (WorkspaceRepository)
					// parentModel.getRepository();

					constraint = FlowEditPart.adjustConstraint(constraint);
					if (constraint == null)
						return null;
					if (TemplateAndPrototypeSupport.isTemplate(addedModel))
					{	
						CreateElementFromTemplateCommand command =
							AddElementHelper.getCreateElementFromTemplateCommand(
									parentModel, addedModel, null, constraint, null);
						return command;
					}
					else
					{
						return AddElementHelper.getAddElementCommand(parentModel, addedModelId,
								addedModelId.getName(), constraint, null);
					}
				}
				else if (sourceObject instanceof Note)
				{
					AddNoteCommand command = new AddNoteCommand(parentModel, constraint);
					return command;
				}
				else
					return null;
			}

			else
				return null;
		}
		finally
		{
			// if (Debug.CREATE)
			// Trace.pop();
		}
	}

	public EditPart getTargetEditPart(Request request)
	{
		if (request instanceof DropModelObjectRequest)
			return getHost();
		else
			return null;
	}

	protected Command getCreateCommand(CreateRequest request)
	{
		return null;
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.editparts;

import java.util.Collections;
import java.util.List;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.DragTracker;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;

import tersus.editor.Debug;
import tersus.editor.TersusDragTracker;
import tersus.editor.figures.DataRectangle;
import tersus.editor.figures.FlowRectangle;
import tersus.editor.layout.DataLayout;
import tersus.editor.layout.LayoutConstraint;
import tersus.editor.properties.ModelObjectWrapper;
import tersus.model.ModelElement;
import tersus.model.Role;
import tersus.util.Misc;
import tersus.util.Trace;
import tersus.workbench.ImageLocator;
import tersus.workbench.WorkspaceRepository;

/**
 * An edit part that represents missing models
 * @author Liat Shiff
 */
public class MissingModelEditPart extends ModelEditPart implements NodeEditPart
{
	
	protected IFigure createFigure()
	{
		if (Debug.OTHER)
			Trace.add("Called TopProcessEditPart.createFigure()");
		DataRectangle f = new DataRectangle();
		f.setLayoutManager(new DataLayout());
		f.setOpaque(true);
		return f;
	}
	
	protected void refreshLabel() 
	{
		String label;
		
		Role role = getRole();
		String name = ((ModelObjectWrapper) getModel()).getElement().getRefId().getName();
		
		String stereotype = calculateStereotypeLabel();
		String stereotypePart = null;
		if (stereotype != null)
			stereotypePart = "<<" + stereotype + ">>";
		String roleStr = role == null ? null : role.toString();
		String separator = getLabelSeparator();
		String namePart = "[" + name + "]";
		if (role == null)
		{
			label = namePart;
			if (stereotype != null)
				label += separator + stereotypePart;
		}
		else
		{
			if (Misc.equal(roleStr, stereotype) && Misc.equal(roleStr, name))
				label = roleStr;
			else
			{
				if (Misc.equal(roleStr, name))
				{
					label = roleStr;
					if (stereotype != null)
						label += separator + stereotypePart;
				}
				else if (Misc.equal(roleStr, stereotype))
				{
					label = roleStr + separator + namePart;
				}
				else
				// 3 different parts
				{
					label = roleStr;
					if (stereotype != null)
						label += (separator + stereotypePart);
					label += separator + namePart;
				}
			}
		}
		((FlowRectangle) getFigure()).setLabel(label + " - Missing Model");
		getFigure().revalidate();
	}
	
	public WorkspaceRepository getRepository()
	{
		return (WorkspaceRepository)((ModelObjectWrapper) getModel()).getElement().getRepository();
	}
	
	protected ImageLocator getImageLocator()
	{
		ModelElement element = getModelElement();
		return MissingModelEditPart.getImageLocator(element);
	}
	
	public static ImageLocator getImageLocator(ModelElement element)
	{
		WorkspaceRepository repository = (WorkspaceRepository) element.getRepository();
	
		ImageLocator locator = repository.getImageLocator("/home/liat/Desktop/questionMark.gig");
		return locator;
	}
	
	protected List<ModelObjectWrapper> getModelChildren()
	{
		return Collections.emptyList();
	}
	
	public void zoomToPart()
	{
		performZoomToPart();
	}

	public DragTracker getDragTracker(Request req)
	{
		if (Debug.OTHER)
			Trace.add("Called ModelEditPart.getDragTracker() with editPart=" + this +" req=" + req);

		return new TersusDragTracker(this);
	}

	protected void refreshColor()
	{
		getFlowRectangle().setForegroundColor(ColorConstants.white);
		getFlowRectangle().setLineColor(ColorConstants.white);
		getFlowRectangle().setBackgroundColor(ColorConstants.red);
	}
	
	protected void refreshLayout()
	{
		super.refreshLayout();
		LayoutConstraint constraint = new LayoutConstraint(getModelElement());
		((GraphicalEditPart) getParent()).setLayoutConstraint(this, getFigure(), constraint);
	}
	
	public FlowRectangle getFlowRectangle()
	{
		return (FlowRectangle) getFigure();
	}

	public ConnectionAnchor getSourceConnectionAnchor(ConnectionEditPart connection)
	{
		return null;
	}

	public ConnectionAnchor getSourceConnectionAnchor(Request request)
	{
		return null;
	}

	public ConnectionAnchor getTargetConnectionAnchor(ConnectionEditPart connection)
	{
		return null;
	}

	public ConnectionAnchor getTargetConnectionAnchor(Request request)
	{
		return null;
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.editparts;

import org.eclipse.draw2d.AbstractConnectionAnchor;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseMotionListener;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.DragTracker;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gef.tools.SelectEditPartTracker;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.widgets.Text;
import tersus.editor.Debug;
import tersus.editor.figures.FlowRectangle;
import tersus.editor.figures.LinkArrow;
import tersus.editor.preferences.Preferences;
import tersus.model.BuiltinProperties;
import tersus.model.Link;
import tersus.model.LinkOperation;
import tersus.model.Path;
import tersus.model.Role;
import tersus.util.Misc;
import tersus.util.Trace;

class RelativeAnchor extends AbstractConnectionAnchor
{
	double x, y;
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.ConnectionAnchorBase#getLocation(org.eclipse.draw2d.geometry.Point)
	 */
	Point location = new Point();

	RelativeAnchor(double x, double y)
	{
		this.x = x;
		this.y = y;
	}

	public Point getLocation(Point reference)
	{
		Rectangle bounds = getOwner().getBounds();
		location.x = (int) Math.round(bounds.x + x * bounds.width);
		location.y = (int) Math.round(bounds.y + y * bounds.height);
		getOwner().translateToAbsolute(location);
		if (Debug.LAYOUT)
		{
			Rectangle absoluteBounds = bounds.getCopy();
			Trace.add("InnerAnchor of " + getOwner() + " getLocation() ref=" + reference
					+ " location=" + location + " ");
			getOwner().translateToAbsolute(absoluteBounds);
			Trace.add("Bounds=" + bounds + " absoluteBounds=" + absoluteBounds);
		}
		return location;
	}
}

public class LinkEditPart extends TersusEditPart implements EditPart, ConnectionEditPart
{
	public CellEditorLocator getRoleEditorLocator()
	{
		CellEditorLocator locator = new CellEditorLocator()
		{
			public void relocate(CellEditor celleditor)
			{
				Text text = (Text) celleditor.getControl();
				org.eclipse.swt.graphics.Point preferredSize = text.computeSize(-1, -1);
				Point start = getLinkArrow().getStart();
				Point end = getLinkArrow().getEnd();
				Point textOrigin = new Point((start.x + end.x) / 2, (start.y + end.y) / 2);
				getFigure().translateToAbsolute(textOrigin);
				text.setBounds(textOrigin.x - 4, textOrigin.y - 1, preferredSize.x + 1,
						preferredSize.y + 1);
			}
		};
		return locator;
	}

	private boolean targetVisible;
	private RelativeAnchor defaultSourceAnchor = new RelativeAnchor(0.1, 0.1);
	private RelativeAnchor defaultTargetAnchor = new RelativeAnchor(0.9, 0.9);
	private boolean sourceVisible;
	private TersusEditPart _source, _target;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	protected void createEditPolicies()
	{
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, new TersusDirectEditPolicy());
		installEditPolicy(EditPolicy.COMPONENT_ROLE, new ElementEditPolicy());
	}

	protected LinkArrow getLinkArrow()
	{
		return (LinkArrow) getFigure();
	}

	protected IFigure createFigure()
	{
		LinkArrow arrow = new LinkArrow(getPath().toString());
		final LinkArrow finalArrow = arrow;

		arrow.addMouseMotionListener(new MouseMotionListener()
		{

			public void mouseDragged(MouseEvent me)
			{
			}

			public void mouseEntered(MouseEvent me)
			{
			}

			public void mouseExited(MouseEvent me)
			{
			}
		
			public void mouseHover(MouseEvent me)
			{
				String arrowToolTipText = "Source: " + getLink().getSource() + "\nTarget: " + getLink().getTarget();
				finalArrow.setToolTip(new Label(arrowToolTipText));
			}

			public void mouseMoved(MouseEvent me)
			{
			}

		});
		return arrow;
	}

	public EditPart getSource()
	{
		return _source;
	}

	public EditPart getTarget()
	{
		return _target;
	}

	public void setSource(EditPart source)
	{
		if (_source instanceof SlotEditPart)
		{
			((SlotEditPart) _source).removeExternalLink(this);
		}
		this._source = (TersusEditPart) source;
		if (source instanceof SlotEditPart)
		{
			if (getLink().getSource().getNumberOfSegments() == 2)
				((SlotEditPart) _source).addExternalLink(this);
		}
	}

	public void setTarget(EditPart target)
	{
		if (_target instanceof SlotEditPart)
		{
			/*
			 * NICE2 - check if the link is really 'external' with respect to its target . This
			 * requires maintaing An additional flag, because we don't remember the original path
			 */
			((SlotEditPart) _target).removeExternalLink(this);
		}
		this._target = (TersusEditPart) target;
		if (target instanceof SlotEditPart)
		{
			if (getLink().getTarget().getNumberOfSegments() == 2)
				((SlotEditPart) _target).addExternalLink(this);
		}
	}

	public void refresh()
	{
		refreshEnds();
		super.refresh();
	}

	private void refreshTarget()
	{
		TersusEditPart targetEditPart = ((FlowEditPart) getParent()).getChild(
				getLink().getTarget(), false);
		ConnectionAnchor anchor;
		TersusEditPart innermostParent = getInnermostParent((FlowEditPart) getParent(), getLink()
				.getTarget());
		if (Misc.equal(getLink().getSource(), getLink().getTarget()))
		{
			FlowRectangle rectangle = ((FlowEditPart) getParent()).getFlowRectangle();
			anchor = rectangle.getInnerAnchor();
		}
		else if (targetEditPart != null)
		{
			setTarget(targetEditPart);
			targetVisible = true;
			Misc.assertion(targetEditPart instanceof NodeEditPart);
			NodeEditPart editPart = (NodeEditPart) targetEditPart;
			anchor = editPart.getTargetConnectionAnchor(this);
		}
		else
		// targetEditPart == null
		{
			if (_target == innermostParent)
				return;
			setTarget(innermostParent);
			targetVisible = false;
			Misc.assertion(innermostParent instanceof ModelEditPart);
			if (innermostParent == getParent())
			{
				defaultTargetAnchor.setOwner(innermostParent.getFigure());
				anchor = defaultTargetAnchor;
			}
			else
			{
				FlowRectangle rectangle = (FlowRectangle) innermostParent.getFigure();
				anchor = rectangle.getInnerAnchor();
			}
		}
		getLinkArrow().setTargetAnchor(anchor);
		if (Debug.LINKS)
			Trace.add("refreshTarget() this=" + this + " target=" + getTarget() + " anchor="
					+ anchor + " visible=" + targetVisible);
	}

	private void refreshSource()
	{
		ConnectionAnchor anchor;
		TersusEditPart sourceEditPart = ((FlowEditPart) getParent()).getChild(
				getLink().getSource(), false);
		if (getSource() != null && getSource() == sourceEditPart)
			return;
		if (sourceEditPart != null)
		{
			setSource(sourceEditPart);
			sourceVisible = true;
			Misc.assertion(sourceEditPart instanceof NodeEditPart);
			NodeEditPart editPart = (NodeEditPart) sourceEditPart;
			anchor = editPart.getSourceConnectionAnchor(this);
		}
		else
		// sourceEditPart == null
		{
			TersusEditPart innermostParent = getInnermostParent((FlowEditPart) getParent(),
					getLink().getSource());
			if (getSource() == innermostParent)
				return;
			setSource(innermostParent);
			sourceVisible = false;
			Misc.assertion(innermostParent instanceof ModelEditPart);
			if (innermostParent == getParent())
			{
				defaultSourceAnchor.setOwner(innermostParent.getFigure());
				anchor = defaultSourceAnchor;
			}
			else
			{
				FlowRectangle rectangle = (FlowRectangle) innermostParent.getFigure();
				anchor = rectangle.getInnerAnchor();
			}
		}
		getLinkArrow().setSourceAnchor(anchor);
		if (Debug.LINKS)
			Trace.add("refreshSource() this=" + this + " source=" + getSource() + " anchor="
					+ anchor + " visible=" + sourceVisible);
	}

	/**
	 * Returns the innermost EditPart that contains an element (identified by a child)
	 */
	private TersusEditPart getInnermostParent(TersusEditPart parent, Path path)
	{
		TersusEditPart part = parent;
		for (int i = 0; i < path.getNumberOfSegments(); i++)
		{
			TersusEditPart child = part.getChild(path.getSegment(i));
			if (child == null)
				return part;
			else
				part = child;
		}
		return part;
	}

	private Link getLink()
	{
		return ((Link) getModelElement());
	}

	public DragTracker getDragTracker(Request req)
	{
		return new SelectEditPartTracker(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.editor.editparts.ModelEditPart#elementChanged(java.beans.PropertyChangeEvent)
	 */
	protected void elementChanged(Object sourceObject, String property, Object oldValue,
			Object newValue)
	{
		if (BuiltinProperties.SOURCE.equals(property))
		{
			refreshSource();
			refreshColor();
		}
		else if (BuiltinProperties.TARGET.equals(property))
		{
			if (Debug.LINKS)
				Trace.add("Link target changed for " + this);
			refreshTarget();
			refreshColor();
		}
		else if (BuiltinProperties.OPERATION.equals(property))
		{
			refreshLineStyle();
		}
		else
			super.elementChanged(sourceObject, property, oldValue, newValue);
	}

	public void refreshEnds()
	{
		refreshSource();
		refreshTarget();
		refreshColor();
		refreshBidirectional();
	}

	/**
	 * Refreshes the 'bidirectional' attributes of the link's arrow
	 */
	private void refreshBidirectional()
	{
		getLinkArrow().setBidirectional(getLink().isBidirectional());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.editor.editparts.TersusEditPart#refreshColor()
	 */
	protected void refreshColor()
	{
		if (!getLink().isValid())
			getLinkArrow().setForegroundColor(ColorConstants.red);
		else if (sourceVisible && targetVisible)
			getLinkArrow().setForegroundColor(ColorConstants.black);
		else
			getLinkArrow().setForegroundColor(ColorConstants.lightGray);
	}

	protected void refreshLineStyle()
	{
		if (getLink().getOperation() == LinkOperation.REMOVE)
		{
			getLinkArrow().setDashed(true);
		}
		else
		{
			getLinkArrow().setDashed(false);
		}
	}

	public TersusEditPart getChild(Role role)
	{
		// Links have no children
		return null;
	}

	protected void refreshTraversingLinkEnds()
	{
		// Nothinng to do: Links may not refer to links
	}

	public void highlight()
	{
		getLinkArrow().setForegroundColor(Preferences.getColor(Preferences.HIGHLIGHT_COLOR));
		getLinkArrow().setLineWidth(2);
	}

	public void clearHighlight()
	{
		refreshColor();
		getLinkArrow().setLineWidth(1);
	}

	public void removeNotify()
	{
		setSource(null);
		setTarget(null);
		super.removeNotify();
	}

	protected void refreshVisuals()
	{
		super.refreshVisuals();
		refreshLineStyle();
	}
}

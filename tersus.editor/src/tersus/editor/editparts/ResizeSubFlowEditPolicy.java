/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.editparts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Handle;
import org.eclipse.gef.editpolicies.ResizableEditPolicy;
import org.eclipse.gef.handles.MoveHandle;
import org.eclipse.gef.handles.ResizeHandle;

import tersus.editor.figures.FlowRectangle;
import tersus.editor.properties.ModelObjectWrapper;
import tersus.model.Model;

public class ResizeSubFlowEditPolicy extends ResizableEditPolicy
{

	/* (non-Javadoc)
	 * @see org.eclipse.gef.editpolicies.SelectionHandlesEditPolicy#createSelectionHandles()
	 */
    protected List createSelectionHandles()
    {
        List handles = new ArrayList();
        GraphicalEditPart part = (GraphicalEditPart) getHost();
        addMoveHandle(part, handles);
        
        Model partModel = null;
        Model partParent = null;
        
        if ((part.getModel() instanceof ModelObjectWrapper) || 
            (part.getModel() instanceof SubFlowEditPart) ||
            (part.getModel() instanceof TopFlowEditPart) ||
            (part.getModel() instanceof DataEditPart))
        {
            partModel = ((ModelObjectWrapper)part.getModel()).getModel();
        }
        
        if (part.getParent() instanceof SubFlowEditPart)
        {
            partParent = ((SubFlowEditPart)part.getParent()).getSubFlow().getReferredModel();
        }
        else if (part.getParent() instanceof TopFlowEditPart)
        {
            partParent = ((TopFlowEditPart)part.getParent()).getFlowModel();
        }
        
        handles.add(createHandle(part, PositionConstants.SOUTH_EAST));
        handles.add(createHandle(part, PositionConstants.SOUTH_WEST));
        handles.add(createHandle(part, PositionConstants.NORTH_WEST));
        handles.add(createHandle(part, PositionConstants.NORTH_EAST));
    
        if ((part instanceof MissingModelEditPart) || (!partParent.getPackage().isReadOnly() && !partModel.getPackage().isReadOnly()))
        {
            handles.add(createHandle(part, PositionConstants.NORTH));
            handles.add(createHandle(part, PositionConstants.WEST));
            handles.add(createHandle(part, PositionConstants.SOUTH));
            handles.add(createHandle(part, PositionConstants.EAST));
        }
        
        return handles;
    }
	
	static public void addMoveHandle(GraphicalEditPart f, List handles)
	{
		handles.add(moveHandle(f));
	}

	private Handle createHandle(GraphicalEditPart owner, int direction)
	{
		FlowRectangle figure = (FlowRectangle)getHostFigure();
		Rectangle bounds =figure.getBounds();
		double width = figure.getWidthProportion();
		ResizeHandle handle = new ResizeHandle(owner, direction);
		handle.setDragTracker(new TersusResizeTracker((TersusEditPart)owner, direction, width));
		return handle;
	}

	static public Handle moveHandle(GraphicalEditPart owner)
	{
		return new MoveHandle(owner);
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.editparts;

import tersus.util.Enum;

/**
 * Describes possible features editable using direct edit
 * 
 * @author Youval Bronicki
 *
 */
public class TersusDirectEditFeature extends Enum
{

	public static final TersusDirectEditFeature ROLE = new TersusDirectEditFeature("Role");
	public static final TersusDirectEditFeature INITIAL_NAME = new TersusDirectEditFeature("Role and Name");

	public static final TersusDirectEditFeature[] values = new TersusDirectEditFeature[]{ROLE, INITIAL_NAME};	
	/**
	 * @param value
	 */
	public TersusDirectEditFeature(String value)
	{
		super(value);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see tersus.util.Enum#getValues()
	 */
	protected Enum[] getValues()
	{
		return values;
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.editparts;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseMotionListener;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.swt.graphics.Color;

import tersus.editor.Debug;
import tersus.editor.figures.DataRectangle;
import tersus.editor.preferences.Preferences;
import tersus.editor.properties.ModelObjectWrapper;
import tersus.editor.useractions.Comment;
import tersus.model.BuiltinPlugins;
import tersus.model.BuiltinProperties;
import tersus.model.Composition;
import tersus.model.DataElement;
import tersus.model.DataType;
import tersus.model.FlowType;
import tersus.model.Link;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.Slot;
import tersus.model.SubFlow;
import tersus.util.Misc;
import tersus.util.Trace;

public abstract class DataEditPart extends ModelEditPart implements NodeEditPart
{
	Boolean parity = null;
	protected boolean getParity()
	{
		if (parity == null)
		{
			EditPart parent = getParent();
			if (parent instanceof DataEditPart)
				parity = Misc.getBoolean(!((DataEditPart) parent).getParity());
			else
				parity = Misc.getBoolean(true);
		}
		return (parity.booleanValue());
	}
	protected void refreshColor()
	{
		String preferenceName;
		if (getParity())
		{
			preferenceName = Preferences.DATA_COLOR_1;
		}
		else
		{
			preferenceName = Preferences.DATA_COLOR_2;
		}
		Color backgroundColor = Preferences.getColor(preferenceName);
		((DataRectangle) getFigure()).setBackgroundColor(backgroundColor);
	}
	protected IFigure createFigure()
	{
		DataRectangle f = new DataRectangle();
		f.setOpaque(true);
		final DataRectangle finalFigure = f;
		
		f.addMouseMotionListener(new MouseMotionListener()
		{
			public void mouseDragged(MouseEvent me)
			{	
			}
			public void mouseEntered(MouseEvent me)
			{
			}
			public void mouseExited(MouseEvent me)
			{
			}
			public void mouseHover(MouseEvent me)
			{
				ModelElement element = getModelElement();
				if (element != null)
				{
					String toolTipText;
					if (element.getElementName().equals(element.getReferredModel().getName()))
						toolTipText = element.getElementName();
					else
						toolTipText = element.getElementName() + " [" + element.getReferredModel().getName() + "]";
					
					finalFigure.setToolTip(new Label(toolTipText));
				}
			}
			public void mouseMoved(MouseEvent me)
			{
			}
		});
		return f;
	}
	/* (non-Javadoc)
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	protected void createEditPolicies()
	{
		if (elementIsModifiable())
			installEditPolicy(EditPolicy.COMPONENT_ROLE, new ElementEditPolicy());
		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new LinkableEditPolicy());
		if (modelIsModifiable())
			installEditPolicy(EditPolicy.LAYOUT_ROLE, new DataLayoutEditPolicy());
		super.createEditPolicies();
	}
	/* (non-Javadoc)
	 * @see tersus.editor.editparts.TersusEditPart#modelChanged(java.beans.PropertyChangeEvent)
	 */
	protected void modelChanged(Object sourceObject, String property, Object oldValue, Object newValue)
	{
		if (BuiltinProperties.COMPOSITION.equals(property))
			refreshComposite();
		else if (BuiltinProperties.ELEMENTS.equals(property))
		{
			refreshChildren();
			refreshComposite();
		}
		else
			super.modelChanged(sourceObject,property, oldValue, newValue);

	}
	/**
	 * 
	 */
	/* (non-Javadoc)
	 * @see tersus.editor.editparts.ModelEditPart#isComposite()
	 */
	public boolean isComposite()
	{
		Object composition = getTersusModel().getProperty(BuiltinProperties.COMPOSITION);
		if (Composition.COLLECTION == composition)
			return true;
		else if (Composition.ATOMIC == composition)
			return false;
		else
			return getChildDataElementWrappers().size() > 0;
	}
	/* (non-Javadoc)
	 * @see org.eclipse.gef.NodeEditPart#getSourceConnectionAnchor(org.eclipse.gef.ConnectionEditPart)
	 */
	public ConnectionAnchor getSourceConnectionAnchor(ConnectionEditPart connection)
	{
		return ((DataRectangle) getFigure()).getEdgeAnchor();
	}
	/* (non-Javadoc)
	 * @see org.eclipse.gef.NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef.ConnectionEditPart)
	 */
	public ConnectionAnchor getTargetConnectionAnchor(ConnectionEditPart connection)
	{
		return ((DataRectangle) getFigure()).getEdgeAnchor();
	}
	/* (non-Javadoc)
	 * @see org.eclipse.gef.NodeEditPart#getSourceConnectionAnchor(org.eclipse.gef.Request)
	 */
	public ConnectionAnchor getSourceConnectionAnchor(Request request)
	{
		return ((DataRectangle) getFigure()).getEdgeAnchor();
	}
	/* (non-Javadoc)
	 * @see org.eclipse.gef.NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef.Request)
	 */
	public ConnectionAnchor getTargetConnectionAnchor(Request request)
	{
		return ((DataRectangle) getFigure()).getEdgeAnchor();
	}
	protected void refreshChildren()
	{
		super.refreshChildren();
		/* 
		 * Link ends of the containing parent flow are refreshed at the end, allowing a link to to find its source and target even
		 * if the link appears before the source/target in the children list, and allowing links to update their state if any of their ends are missing
		 */
		refreshTraversingLinkEnds();
	}
	/* (non-Javadoc)
	 * @see tersus.editor.editparts.TersusEditPart#refreshTraversingLinkEnds()
	 */
	protected void refreshTraversingLinkEnds()
	{
		// The links of the containing flow may traverse this element
		FlowEditPart containingFlowEditPart = getContainingFlowEditPart();
		// There may not be a containing flow edit part, e.g. if this edit part is detached.
		if (containingFlowEditPart != null)
			containingFlowEditPart.refreshLinkEnds();
	}
	/**
	 * @return
	 */
	private FlowEditPart getContainingFlowEditPart()
	{
		EditPart parent = getParent();
		if (parent == null)
			return null;
		else if (parent instanceof FlowEditPart)
			return (FlowEditPart) parent;
		else
		{
			Misc.assertion(parent instanceof DataEditPart);
			return ((DataEditPart) parent).getContainingFlowEditPart();
		}
	}
	/* (non-Javadoc)
	 *
	 * Overriden to support flow-data elements:  We return wrappers only for sub-flows, slots and data elements (not links),
	 * and set the display mode of sub-flows to 'Data'.
	 * @see org.eclipse.gef.editparts.AbstractEditPart#getModelChildren()
	 */
	protected List getModelChildren()
	{
		if (Debug.OTHER)
			Trace.push("ModelEditPart.getModelChildren() model=" + getModel());
		try
		{
			List children = Collections.EMPTY_LIST;
			if (open)
				children = getChildDataElementWrappers();
			if (Debug.OTHER)
				Trace.add("returning list of " + children.size() + " children");
			return children;
		}
		finally
		{
			if (Debug.OTHER)
				Trace.pop();
		}
	}
	/**
	 * Returns a list of wrappers for "relevant" child data elements: 
	 *  For data types, this is the list of all DataElements
	 *  For flow models (display models), this is the list of data elements and of display elements.
	 * 
	 * Note the special handling of 'Popup', which in this context is not considered a display element (to prevent confusion).
	 * @return
	 */
	private List getChildDataElementWrappers()
	{
		Model model = getTersusModel();
		Misc.assertion(model != null);
		List children = new ArrayList();
		for (ModelElement element: model.getElements())
		{
			if (element instanceof Slot || element instanceof Link)
			    continue;
			
			Model referredModel = element.getReferredModel();
			
			boolean isRealDataElement = element instanceof DataElement && referredModel instanceof DataType;
			boolean parentIsDisplayModel = model.getProperty(BuiltinProperties.TYPE) == FlowType.DISPLAY && ! BuiltinPlugins.BUTTON.equals(model.getProperty(BuiltinProperties.PLUGIN));
			boolean childIsDisplayModel = referredModel!= null && referredModel.getProperty(BuiltinProperties.TYPE) == FlowType.DISPLAY;
            boolean isDisplayElement = element instanceof SubFlow
			&& childIsDisplayModel;
			if (isRealDataElement || parentIsDisplayModel && childIsDisplayModel)
			{
				ModelObjectWrapper child = new ModelObjectWrapper(referredModel, element);
				child.setMode(ModelObjectWrapper.MODE_DATA_ELEMENT);
				children.add(child);
			}
			if (referredModel == null && element.getRefId() != null)
			{
				ModelObjectWrapper child = new ModelObjectWrapper(element, true);
				children.add(child);
			}
				
		}
		return children;
	}
	/* 
	 * The element of a data edit part is modifiable if the parent model is modifiable.
	 */
	public boolean elementIsModifiable()
	{
		EditPart parent = getParent();
		if ( parent instanceof TersusEditPart)
		{
			if (((TersusEditPart)parent).modelIsModifiable())
				return true;
		}
		return false;
	}
	/* 
	 * The model of a data edit part is modifiable if
	 * 1) The parent model is modifiable
	 * 2) The type of the element is a real DataType (and not a FlowModel). A Display Model 
	 *    is not modifiable in the context of a Display Data Element
	 */
	public boolean modelIsModifiable()
	{
		EditPart parent = getParent();
		
		boolean ModelReadOnly = false;
		if (((ModelObjectWrapper)this.getModel()).getModel() != null)
			ModelReadOnly = ((ModelObjectWrapper)this.getModel()).getModel().isReadOnly();
	
		if ( parent instanceof TersusEditPart && !ModelReadOnly)
		{
			if (((TersusEditPart)parent).modelIsModifiable())
				return (getTersusModel() instanceof DataType);
		}
		return false;
	}
	
	public void performRequest(Request req)
	{
		if (req.getType() == RequestConstants.REQ_OPEN)
		{
			System.out.println();
			if (Debug.USER_ACTIONS)
				Trace.push(new Comment("zoomToElement path=\"" + getPath() + "\""));
			try
			{	
				for (Object childObject :getParent().getChildren())
				{
					if (childObject instanceof SubDataEditPart)
					{
						SubDataEditPart child = (SubDataEditPart)childObject;

						if (child.equals(this))
							child.setOpen(true);
						else 
							child.setOpen(false);
					}
				}
			}

			finally
			{
				if (Debug.USER_ACTIONS)
					Trace.pop();
			}
		}
		else
		{
			if (Debug.OTHER)
				Trace.add(
					"FlowDataEditPart.performRequest() [Ignored] editpart="
						+ this
						+ " req="
						+ req
						+ " type="
						+ req.getType());
			super.performRequest(req);
		}
	}

}

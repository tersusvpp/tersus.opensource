/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.editparts;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseMotionListener;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.editpolicies.RootComponentEditPolicy;

import tersus.editor.Debug;
import tersus.editor.figures.FlowRectangle;
import tersus.editor.layout.FlowLayout;
import tersus.model.ModelElement;
import tersus.model.Path;
import tersus.util.Trace;

/**
 * An edit part that handles the 'top' process (the root of the diagram)
 */
public class TopFlowEditPart extends FlowEditPart
{

	/* (non-Javadoc)
		 * @see org.eclipse.gef.EditPart#getTargetEditPart(org.eclipse.gef.Request)
		 */
	public EditPart getTargetEditPart(Request request)
	{
		// TODO This method is for debug purpose only
		return super.getTargetEditPart(request);
	}
	private int OFFSET = 5; // root figure's offset from origin
	private Path path = new Path();
	/* (non-Javadoc)
	* @see org.eclipse.gef.editparts.AbstractEditPart#refreshVisuals()
	*/

	private FlowRectangle contentFigure;

	/* (non-Javadoc)
	 * @see tersus.editor.FlowEditPart#getParity()
	 */
	protected boolean getParity()
	{
		return true;
	}

	protected IFigure createFigure()
	{
		if (Debug.OTHER)
			Trace.add("Called TopFlowEditPart.createFigure()");
		contentFigure = new FlowRectangle();
//		contentFigure.setLabel(getFlowModel().getName());
		contentFigure.setLayoutManager(new FlowLayout());
		contentFigure.setOpaque(true);

		contentFigure.setBackgroundColor(ColorConstants.orange);
		contentFigure.setBounds(new Rectangle(5,5,400, 300));
		
		final FlowRectangle finalFigure = contentFigure;

		contentFigure.addMouseMotionListener(new MouseMotionListener()
		{
			public void mouseDragged(MouseEvent me)
			{
			}

			public void mouseEntered(MouseEvent me)
			{
			}

			public void mouseExited(MouseEvent me)
			{
			}

			public void mouseHover(MouseEvent me)
			{
				ModelElement element = getModelElement();
				if (element != null)
				{
					String toolTipText;
					if (element.getElementName().equals(element.getReferredModel().getName()))
						toolTipText = element.getElementName();
					else
						toolTipText = element.getElementName() + " ["
								+ element.getReferredModel().getName() + "]";

					finalFigure.setToolTip(new Label(toolTipText));
				}
			}

			public void mouseMoved(MouseEvent me)
			{
			}
		});
		
		return contentFigure;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.GraphicalEditPart#getContentPane()
	 */
	public IFigure getContentPane()
	{
		return contentFigure;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	protected void createEditPolicies()
	{
		installEditPolicy(EditPolicy.COMPONENT_ROLE, new RootComponentEditPolicy());
		super.createEditPolicies();
	}

	/* (non-Javadoc)
	 * @see tersus.editor.editparts.ModelEditPart#refreshLayout()
	 */
	protected void refreshLayout()
	{
		super.refreshLayout();
	}

	/* (non-Javadoc)
	 * @see tersus.editor.editparts.FlowEditPart#setWidth()
	 */
	protected void refreshWidth()
	{
		super.refreshWidth();
		int height = contentFigure.getBounds().height;
		double width = (int)Math.round(getFlowModel().getWidth()* height);
		contentFigure.setBounds(OFFSET, OFFSET, width, height);
	}

	/* (non-Javadoc)
	 * @see tersus.editor.editparts.ModelEditPart#getDefaultOpenState()
	 */
	protected boolean getDefaultOpenState()
	{
		return true;
	}

	/* (non-Javadoc)
	 * @see tersus.editor.editparts.TersusEditPart#refreshTraversingLinkEnds()
	 */
	protected void refreshTraversingLinkEnds()
	{
		// No links traverse the top edit part (it doesn't have a role)

	}

}

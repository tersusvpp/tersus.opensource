/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.editparts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;

import tersus.editor.requests.DropModelObjectRequest;
import tersus.model.BuiltinProperties;
import tersus.model.DataType;
import tersus.model.FlowType;
import tersus.model.Model;
import tersus.model.ModelObject;
import tersus.model.Slot;
import tersus.model.commands.SetPropertyValueCommand;
import tersus.model.commands.SetSlotTypeCommand;


/**
 * Extends ElementEditPolicy with the ability to set a Slot's data type
 * by dragging the data type's resource onto the slot.
 * 
 * @author Youval Bronicki
 *
 */
public class SlotEditPolicy extends  ElementEditPolicy
{
	public Command getCommand(Request request)
	{
		Command command;
		if (request instanceof DropModelObjectRequest)
			command = getDropResourceCommand((DropModelObjectRequest) request);
		else
			command = super.getCommand(request);
		return command;
	}

	/**
	 * @param request
	 * @return
	 */
	private Command getDropResourceCommand(DropModelObjectRequest request)
	{
		Slot slot = (Slot)((SlotEditPart)getHost()).getModelElement();
		ModelObject sourceObject = request.getSourceObject();
        if ( ! (sourceObject != null && (sourceObject instanceof DataType || sourceObject.getProperty(BuiltinProperties.TYPE) == FlowType.DISPLAY) ))
			return null;
		Model droppedModel = (Model) sourceObject;
		SetSlotTypeCommand command = new SetSlotTypeCommand(slot, droppedModel.getId(), true);
		
		return command;
		
	}
	/* (non-Javadoc)
	 * @see org.eclipse.gef.EditPolicy#getTargetEditPart(org.eclipse.gef.Request)
	 */
	/* (non-Javadoc)
	 * @see org.eclipse.gef.EditPolicy#getTargetEditPart(org.eclipse.gef.Request)
	 */
	public EditPart getTargetEditPart(Request request)
	{
		if (request instanceof DropModelObjectRequest)
			return getHost();
		else
			return null;
	}

}

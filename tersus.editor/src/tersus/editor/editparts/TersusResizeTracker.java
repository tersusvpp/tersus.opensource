/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.editparts;

import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gef.tools.ResizeTracker;

import tersus.util.Trace;

/**
 * 
 */
public class TersusResizeTracker extends ResizeTracker
{

	private double width;
	/**
	 * @param direction
	 */
	public TersusResizeTracker(TersusEditPart owner, int direction, double width)
	{
		super(owner, direction);
		this.width = width;
	}
	protected void updateSourceRequest()
	{
		ChangeBoundsRequest request = (ChangeBoundsRequest) getSourceRequest();

		Dimension d = getDragMoveDelta().getCopy();

		Point location = new Point(getLocation());
		Point corner = new Point(0, 0);
		Dimension resize = new Dimension(0, 0);
		int delta;
		int direction = getResizeDirection();
		// adjust mouse delta to the figure's proportions
		switch (direction)
		{
			case PositionConstants.NORTH_EAST :
			case PositionConstants.SOUTH_WEST :
				adjustDelta(d, -1 / width);
				break;
			case PositionConstants.NORTH_WEST :
			case PositionConstants.SOUTH_EAST :
			Trace.add("width="+width+ " slope="+1/width);
				adjustDelta(d, 1 / width);
				break;

		}
		if ((direction & PositionConstants.NORTH) != 0)
		{
			corner.y += d.height;
			resize.height -= d.height;
		}

		if ((direction & PositionConstants.SOUTH) != 0)
		{
			resize.height += d.height;
		}
		if ((direction & PositionConstants.WEST) != 0)
		{
			corner.x += d.width;
			resize.width -= d.width;
		}

		if ((direction & PositionConstants.EAST) != 0)
		{
			resize.width += d.width;
		}

		request.setMoveDelta(corner);
		request.setSizeDelta(resize);
		request.setLocation(location);
		request.setEditParts(getOperationSet());

	}
	
	/** adjust the drag delta so that proportion is maintained
	 * 
	 * @param d drag delta
	 * @param slope the slope of the relevant diagonal
	 * @param north
	 */
	
	private void adjustDelta(Dimension d, double slope)
	{
		if ((d.height < d.width * slope) ^ ((getResizeDirection() & PositionConstants.SOUTH) != 0))
		{
			// primary drag is vertical - adjust horizontal value 
			d.width = (int) Math.round(d.height / slope);
		}
		else
		{
			// primary drag is horizontal - adjust vartical value 
			d.height = (int) Math.round(d.width * slope);
		}
		Trace.add("Size Delta="+d + " slope="+slope);
	}
}
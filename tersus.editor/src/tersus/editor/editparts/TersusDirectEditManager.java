/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.editparts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.requests.DirectEditRequest;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gef.tools.DirectEditManager;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import tersus.editor.actions.RenameAction;
import tersus.editor.requests.EditInitialNameRequest;
import tersus.model.ModelObject;
import tersus.model.Role;
import tersus.model.commands.IElementNameCommand;

/**
 * A <code>DirectEditManager</code> for editing roles in-place.
 * 
 * 
 * The only specific logic of this class is setting the font and initial text of the cell editor.
 * 
 * @author Youval Bronicki
 * 
 */
public class TersusDirectEditManager extends DirectEditManager
{

	private TersusDirectEditFeature feature;

	private boolean comitting = false;

	private Command initialCommand;

	/**
	 * @return Returns the initialCommand.
	 */
	public Command getInitialCommand()
	{
		return initialCommand;
	}

	/**
	 * @param initialCommand
	 *            The initialCommand to set.
	 */
	public void setInitialCommand(Command initialCommand)
	{
		this.initialCommand = initialCommand;
	}

	/**
	 * 
	 * Creates a new DirectEditRoleManager for a given EditPart
	 * 
	 * @param source
	 *            the source edit part
	 */
	public TersusDirectEditManager(final TersusEditPart source)
	{
		super(source, TextCellEditor.class, null);
		CellEditorLocator locator = source.getRoleEditorLocator();
		setLocator(locator);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.tools.DirectEditManager#initCellEditor()
	 */
	protected void initCellEditor()
	{
		Role currentRole = ((TersusEditPart) getEditPart()).getRole();
		getCellEditor().setValue(currentRole.toString());
		Text text = (Text) getCellEditor().getControl();
		IFigure figure = ((TersusEditPart) getEditPart()).getFigure();
		text.setFont(figure.getFont());
		text.selectAll();
		comitting = false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.tools.DirectEditManager#createDirectEditRequest()
	 */
	protected DirectEditRequest createDirectEditRequest()
	{
		DirectEditRequest req;
		if (getFeature() == TersusDirectEditFeature.INITIAL_NAME)
		{
			req = new EditInitialNameRequest();
			((EditInitialNameRequest) req).setInitialCreationCommand(getInitialCommand());
		}
		else
		{
			req = new DirectEditRequest();
		}
		req.setCellEditor(getCellEditor());
		req.setDirectEditFeature(getFeature());
		return req;

	}

	/**
	 * @return
	 */
	public TersusDirectEditFeature getFeature()
	{
		return feature;
	}

	/**
	 * @param features
	 */
	public void setFeature(TersusDirectEditFeature features)
	{
		feature = features;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.tools.DirectEditManager#commit()
	 * 
	 * Overriden to allow maintaining the 'open' state of the edit part
	 */
	protected void commit()
	{
		if (comitting)
			return;
		comitting = true;
		System.out.println("Direct Edit Comitted");
		try
		{
			eraseFeedback();
			TersusEditPart part = (TersusEditPart) getEditPart();
			TersusEditPart parent = (TersusEditPart) part.getParent();
			boolean open = false;
			if (part instanceof ModelEditPart)
				open = ((ModelEditPart) part).isOpen();
			if (isDirty())
			{
				String editValueNotFinal = (String) getCellEditor().getValue();
				final String editValue = editValueNotFinal.length() == 0 ? "Empty"
						: editValueNotFinal.trim().length() != 0 ? editValueNotFinal : " "
								.equals(editValueNotFinal) ? "Blank" : "Blanks";

				final Shell shell = getEditPart().getViewer().getControl().getShell();

				CommandStack stack = getEditPart().getViewer().getEditDomain().getCommandStack();
				Command command = getEditPart().getCommand(getDirectEditRequest());
				System.out.println("Command");
				stack.execute(command);

				Role role = Role.get(((IElementNameCommand) command).getActualElementName());

				part = parent.getChild(role);
				if (part != null)
				{
					part.setOpen(open);
					part.getViewer().select(part);
				}
				if (!(((IElementNameCommand) command).getActualElementName()).equals(editValue))
				{
					final ModelObject mo = part.getModelObject();
					shell.getDisplay().asyncExec(new Runnable()
					{
						public void run()
						{
							RenameAction.openRenameWizard(mo, shell, editValue);
						}
					});
				}
			}
		}
		finally
		{
			bringDown();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.tools.DirectEditManager#bringDown()
	 */
	protected void bringDown()
	{
		super.bringDown();
		if (!comitting)
			cancel();
	}

	protected void cancel()
	{
		if (feature == TersusDirectEditFeature.INITIAL_NAME)
		{
			if (getInitialCommand() != null)
				getInitialCommand().undo();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.tools.DirectEditManager#isDirty()
	 */
	protected boolean isDirty()
	{
		if (getInitialCommand() != null)
		{
			/**
			 * We are in the initial creation of an element. Even though the default name has been
			 * accepted, we still want the initial command to be undone and a new command to be
			 * executed through the command stack (so that undo will be possible).
			 */
			return true;
		}
		else
			return super.isDirty();
	}
}
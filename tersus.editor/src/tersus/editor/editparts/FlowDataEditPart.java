/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.editparts;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;

import tersus.editor.Debug;
import tersus.editor.figures.FlowRectangle;
import tersus.editor.layout.DataLayout;
import tersus.editor.layout.LayoutConstraint;
import tersus.editor.useractions.Comment;
import tersus.model.BuiltinProperties;
import tersus.model.DataElement;
import tersus.model.FlowDataElementType;
import tersus.model.Model;
import tersus.util.EscapeUtil;
import tersus.util.Trace;

public class FlowDataEditPart extends DataEditPart
{

	protected boolean getDefaultOpenState()
	{
		return true;
	}

	public void performRequest(Request req)
	{
		if (req.getType() == RequestConstants.REQ_OPEN)
		{
			if (Debug.USER_ACTIONS)
				Trace.push(new Comment("zoomToElement path=\"" + getPath() + "\""));
			try
			{
				zoomToPart();
			}

			finally
			{
				if (Debug.USER_ACTIONS)
					Trace.pop();
			}
		}
		else
		{
			if (Debug.OTHER)
				Trace.add(
					"FlowDataEditPart.performRequest() [Ignored] editpart="
						+ this
						+ " req="
						+ req
						+ " type="
						+ req.getType());
			super.performRequest(req);
		}
	}

	/**
	 * Zooms so that the content Data Type captures a big proportion of the visible area, openning it if it's a composite
	 */
	public void zoomToPart()
	{
		performZoomToPart();
	}

	/* (non-Javadoc)
	 * @see tersus.editor.editparts.ModelEditPart#refreshLabel()
	 */
	protected void refreshLabel()
	{
		Model model = getTersusModel();
		if (Boolean.TRUE.equals(model.getProperty(BuiltinProperties.CONSTANT)))
		{
			String value = (String) model.getProperty(BuiltinProperties.VALUE);
			String label = "\"" + EscapeUtil.unquote(value) + "\"";
			((FlowRectangle) getFigure()).setLabel(label);
		}
		else 
		    super.refreshLabel();

	}

	protected void refreshLayout()
	{
		super.refreshLayout();
		LayoutConstraint constraint = new LayoutConstraint(getModelElement());
		((GraphicalEditPart) getParent()).setLayoutConstraint(this, getFigure(), constraint);		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	protected IFigure createFigure()
	{
		IFigure f = super.createFigure();
		f.setLayoutManager(new DataLayout());
		return f;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	protected void createEditPolicies()
	{
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, new TersusDirectEditPolicy());
		super.createEditPolicies();

	}
	/* (non-Javadoc)
	 * @see tersus.editor.editparts.TersusEditPart#refreshColor()
	 */
	protected void refreshLine()
	{
		DataElement e = (DataElement) getModelElement();
		FlowRectangle f = (FlowRectangle) getFigure();
		if (e.getType() == FlowDataElementType.PARENT)
		{
			f.setLineColor(ColorConstants.blue);
		}
		else
		{
			f.setLineColor(ColorConstants.black);
		}

	}

	/* (non-Javadoc)
	 * @see tersus.editor.editparts.TersusEditPart#elementChanged(java.beans.PropertyChangeEvent)
	 */
	protected void elementChanged(Object sourceObject, String property, Object oldValue, Object newValue)
	{
		if (BuiltinProperties.TYPE.equals(property))
		{
			refreshLine(); //TEST test that references are shown correctly
			refreshTraversingLinkEnds();
		}
		super.elementChanged(sourceObject,property, oldValue, newValue);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.gef.editparts.AbstractEditPart#refreshVisuals()
	 */
	protected void refreshVisuals()
	{
		super.refreshVisuals();
		refreshLine();
	}

	/* (non-Javadoc)
	 * @see tersus.editor.editparts.TersusEditPart#modelChanged(java.beans.PropertyChangeEvent)
	 */
	protected void modelChanged(Object sourceObject, String property, Object oldValue, Object newValue)
	{
		if (BuiltinProperties.VALUE.equals(property))
			refreshLabel();
		else
			super.modelChanged(sourceObject,property, oldValue, newValue);
	}

}

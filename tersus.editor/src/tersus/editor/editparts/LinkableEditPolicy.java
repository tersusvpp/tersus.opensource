/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.editparts;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy;
import org.eclipse.gef.requests.CreateConnectionRequest;
import org.eclipse.gef.requests.ReconnectRequest;

import tersus.editor.Debug;
import tersus.editor.properties.ModelObjectWrapper;
import tersus.model.ElementPath;
import tersus.model.FlowModel;
import tersus.model.Link;
import tersus.model.Path;
import tersus.model.commands.AddLinkCommand;
import tersus.model.commands.ReconnectLinkCommand;
import tersus.util.Trace;
import tersus.workbench.WorkspaceRepository;

public class LinkableEditPolicy extends GraphicalNodeEditPolicy
{
	protected Command getConnectionCompleteCommand(CreateConnectionRequest request)
	{
		if (Debug.LINKS)
			Trace.push("getConnectionCompleteCommand() request=" + request);
		try
		{
			// TODO consider reusing MultiStepCommand.addLink
			CompoundCommand cmd = new CompoundCommand();

			AddLinkCommand addLinkCommand = (AddLinkCommand) request.getStartCommand();
			Path startPath = ((TersusEditPart) request.getSourceEditPart()).getPath();
			Path endPath = ((TersusEditPart) request.getTargetEditPart()).getPath();
			TersusEditPart host = (TersusEditPart) getHost();
			FlowEditPart top = (FlowEditPart) host.getRoot().getContents();

			ElementPath parentPath = Path.getCommonPrefix(startPath, endPath);
			TersusEditPart parentEditPart = top.getChild(parentPath, false);
			Path sourcePath = startPath.getTail(startPath.getNumberOfSegments()
					- parentPath.getNumberOfSegments());
			Path targetPath = endPath.getTail(endPath.getNumberOfSegments()
					- parentPath.getNumberOfSegments());
			if (Debug.LINKS)
			{
				Trace.add("startPath=" + startPath);
				Trace.add("endPath=" + endPath);
				Trace.add("sourcePath=" + sourcePath);
				Trace.add("parentPath=" + parentPath);
				Trace.add("host=" + host);
				Trace.add("targetPath=" + targetPath);
				Trace.add("parentEditPart=" + parentEditPart);
			}
			if (!(parentEditPart instanceof FlowEditPart))
				return null;
			FlowModel parentModel = (FlowModel) parentEditPart.getTersusModel();
			if (!parentModel.isValidLinkPath(sourcePath))
				return null;
			if (!parentModel.isValidLinkPath(targetPath))
				return null;

			addLinkCommand.setSource(sourcePath);
			addLinkCommand.setTarget(targetPath);
			addLinkCommand.setParentModel(parentModel);
			cmd.add(addLinkCommand);

			return cmd;
		}
		finally
		{
			if (Debug.LINKS)
				Trace.pop();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy#getConnectionCreateCommand(org.eclipse
	 * .gef.requests.CreateConnectionRequest)
	 */
	protected Command getConnectionCreateCommand(CreateConnectionRequest request)
	{
		if (Debug.LINKS)
			Trace.push("getConnectionCreateCommand() request=" + request);
		try
		{
			TersusEditPart editPart = (TersusEditPart) getHost();
			AddLinkCommand command = new AddLinkCommand((Link) request.getNewObject(),
					(WorkspaceRepository) editPart.getModelObject().getRepository());
			request.setStartCommand(command);
			return command;
		}
		finally
		{
			if (Debug.LINKS)
				Trace.pop();
		}
	}

	private Path getPath()
	{
		return ((TersusEditPart) getHost()).getPath();
	}

	protected Command getReconnectTargetCommand(ReconnectRequest request)
	{
		if (Debug.LINKS)
			Trace.push("getReconnectTargetCommand() request=" + request);
		try
		{
			return getReconnectCommand(request, false);

		}
		finally
		{
			if (Debug.LINKS)
				Trace.pop();
		}
	}

	protected Command getReconnectSourceCommand(ReconnectRequest request)
	{
		if (Debug.LINKS)
			Trace.push("getReconnectSourceCommand() request=" + request);
		try
		{
			return getReconnectCommand(request, true);

		}
		finally
		{
			if (Debug.LINKS)
				Trace.pop();
		}
	}

	protected Command getReconnectCommand(ReconnectRequest request, boolean source)
	{
		CompoundCommand command = new CompoundCommand();
		ModelObjectWrapper wrapper = (ModelObjectWrapper) request.getConnectionEditPart()
				.getModel();
		ReconnectLinkCommand cmd = new ReconnectLinkCommand((WorkspaceRepository) wrapper
				.getModelObject().getRepository());
		Path parentPath = ((TersusEditPart) request.getConnectionEditPart().getParent()).getPath();
		Path linkablePath = getPath();
		TersusEditPart topEditPart = (TersusEditPart) getHost().getRoot().getContents();
		TersusEditPart parentEditPart = topEditPart.getChild(parentPath, false);
		FlowModel parentModel = (FlowModel) parentEditPart.getTersusModel();
		if (!linkablePath.beginsWith(parentPath))
			return null;
		linkablePath = linkablePath.getTail(linkablePath.getNumberOfSegments()
				- parentPath.getNumberOfSegments());
		if (!parentModel.isValidLinkPath(linkablePath))
			return null;

		Link link = (Link) wrapper.getElement();
		cmd.setLink(link);

		if (source)
		{
			cmd.setSource(linkablePath);
			cmd.setTarget(link.getTarget());
			command.add(cmd);
		}
		else
		{
			cmd.setSource(link.getSource());
			cmd.setTarget(linkablePath);
			command.add(cmd);
		}
		if (cmd.getSource().equals(cmd.getTarget()))
			return null;

		/*
		 * If one of the new ends of the link is a slot, we start propagation on it TODO: only
		 * propagate types of implicitly typed elements
		 */

		return command;
	}

}

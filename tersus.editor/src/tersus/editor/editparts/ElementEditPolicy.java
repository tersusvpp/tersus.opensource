/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.editparts;

import org.eclipse.gef.editpolicies.ComponentEditPolicy;

/**
 * 
 * A model-based edit policy for a model element (applicable to any displayed component except for
 * the editor's top model).
 * 
 * @author Youval Bronicki
 * @author Ofer Brandes
 * 
 */
public class ElementEditPolicy extends ComponentEditPolicy
{

	/**
	 * Creates an ElementEditPolicy instance.
	 */
	public ElementEditPolicy()
	{
		super();
	}

}

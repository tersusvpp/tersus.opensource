/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor;

import tersus.model.Path;

public class ConcretePackageIdentifier
{
    private Path path;
    private int hash;
    private String pathStr;
    public ConcretePackageIdentifier(String projectName, String packageId)
    {
        super();
        this.pathStr = projectName+ "/" + packageId;
        this.path = new Path(pathStr);
        this.hash = pathStr.hashCode();
    }
    public String toString()
    {
        return pathStr;
    }
    public boolean equals(Object other)
    {
        if (other instanceof ConcretePackageIdentifier)
            return path.equals(other.toString()); 
        else
            return false;
    }
    public int hashCode()
    {
        return hash;
    }
    public Path getPath()
    {
        return path;
    }
    

}

/************************************************************************************************
 * Copyright (c) 2003-2019 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.tools.MarqueeSelectionTool;
import org.eclipse.gef.util.EditPartUtilities;

/**
 * @author Alberto Romo Valverde
 */
public class TersusMarqueeSelectionTool extends MarqueeSelectionTool
{
	static final int DEFAULT_MODE = 0;
	static final int TOGGLE_MODE = 1;
	static final int APPEND_MODE = 2;
	
	/**
	 * Tersus implementation of MarqueeSelectionTool
	 * 
	 * Added support for nodes contained and connections touch combined on new eclipse versions
	 * 
	 * for more details see {@link #MarqueeSelectionTool}
	 */
	public TersusMarqueeSelectionTool() {
		super();
	}
	
	/**
	 * {@link MarqueeSelectionTool#calculateMarqueeSelectedEditParts()}
	 */
	@Override
	protected Collection calculateMarqueeSelectedEditParts() {
		Collection marqueeSelectedEditParts = new HashSet();
		marqueeSelectedEditParts
				.addAll(calculatePrimaryMarqueeSelectedEditParts());
		marqueeSelectedEditParts
				.addAll(calculateSecondaryMarqueeSelectedEditParts(marqueeSelectedEditParts));
		return marqueeSelectedEditParts;
	}
	
	/**
	 * Force NODES and CONNECTIONS
	 * 
	 * {@link MarqueeSelectionTool#calculatePrimaryMarqueeSelectedEditParts()}
	 */
	private Collection calculatePrimaryMarqueeSelectedEditParts() {
		Collection editPartsToProcess = new HashSet();
		
		editPartsToProcess.addAll(EditPartUtilities
				.getAllChildren((GraphicalEditPart) getCurrentViewer()
						.getRootEditPart()));
		
		editPartsToProcess
		.addAll(EditPartUtilities
				.getAllNestedConnectionEditParts((GraphicalEditPart) getCurrentViewer()
						.getRootEditPart()));

		// process all edit parts and determine which are affected by the
		// current marquee selection
		Collection marqueeSelectedEditParts = new ArrayList();
		for (Iterator iterator = editPartsToProcess.iterator(); iterator
				.hasNext();) {
			GraphicalEditPart editPart = (GraphicalEditPart) iterator.next();
			if (isMarqueeSelectable(editPart)
					&& isPrimaryMarqueeSelectedEditPart(editPart)) {
				marqueeSelectedEditParts.add(editPart);
			}
		}
		return marqueeSelectedEditParts;
	}
	
	/**
	 * 
	 * {@link MarqueeSelectionTool#calculateSecondaryMarqueeSelectedEditParts()}
	 */
	private Collection calculateSecondaryMarqueeSelectedEditParts(
			Collection directlyMarqueeSelectedEditParts) {

		Collection editPartsToProcess = new HashSet();
		for (Iterator iterator = directlyMarqueeSelectedEditParts.iterator(); iterator
				.hasNext();) {
			GraphicalEditPart marqueeSelectedEditPart = (GraphicalEditPart) iterator
					.next();
			editPartsToProcess.addAll(marqueeSelectedEditPart
					.getSourceConnections());
			editPartsToProcess.addAll(marqueeSelectedEditPart
					.getTargetConnections());
		}

		// process all edit parts and decide, whether they are indirectly
		// affected by marquee selection
		Collection secondaryMarqueeSelectedEditParts = new HashSet();
		for (Iterator iterator = editPartsToProcess.iterator(); iterator
				.hasNext();) {
			GraphicalEditPart editPart = (GraphicalEditPart) iterator.next();
			if (isSecondaryMarqueeSelectedEditPart(
					directlyMarqueeSelectedEditParts, editPart)) {
				secondaryMarqueeSelectedEditParts.add(editPart);
			}
		}
		return secondaryMarqueeSelectedEditParts;
	}
	
	/**
	 * Force NODES and CONNECTIONS with touch behaviour on connections and contained behaviour on nodes
	 * 
	 * {@link MarqueeSelectionTool#isPrimaryMarqueeSelectedEditPart(GraphicalEditPart)}
	 */
	private boolean isPrimaryMarqueeSelectedEditPart(GraphicalEditPart editPart) {
		// figure bounds are used to determine if edit part is included in
		// selection
		IFigure figure = editPart.getFigure();
		Rectangle r = figure.getBounds().getCopy();
		figure.translateToAbsolute(r);

		boolean included = false;
		Rectangle marqueeSelectionRectangle = getCurrentMarqueeSelectionRectangle();
		if (editPart instanceof ConnectionEditPart) {
			if (marqueeSelectionRectangle.intersects(r)) {
				// children will contain FlowEditParts only in case
				// behavior is BEHAVIOR_CONNECTIONS_TOUCHED or
				// BEHAVIOR_CONNECTIONS_CONTAINED
				Rectangle relMarqueeRect = Rectangle.SINGLETON;
				figure.translateToRelative(relMarqueeRect
						.setBounds(marqueeSelectionRectangle));
				included = ((Connection) figure).getPoints()
						.intersects(relMarqueeRect);
			}
		} else {
			// otherwise children will only be 'node' edit parts
			included = marqueeSelectionRectangle.contains(r);
		}
		return included;
	}
	
	/**
	 * Force NODES and CONNECTIONS
	 * 
	 * {@link MarqueeSelectionTool#isSecondaryMarqueeSelectedEditPart(Collection,EditPart)}
	 */
	private boolean isSecondaryMarqueeSelectedEditPart(
			Collection directlyMarqueeSelectedEditParts, EditPart editPart) {
		int mode = getCurrentSelectionMode();
		
		boolean included = false;
		if (editPart instanceof ConnectionEditPart) {
			// connections are included, if related nodes are included
			ConnectionEditPart connection = (ConnectionEditPart) editPart;
			GraphicalEditPart source = (GraphicalEditPart) connection
					.getSource();
			GraphicalEditPart target = (GraphicalEditPart) connection
					.getTarget();
			boolean sourceIncludedInMarqueeSelection = directlyMarqueeSelectedEditParts
					.contains(source);
			boolean targetIncludedInMarqueeSelection = directlyMarqueeSelectedEditParts
					.contains(target);

			if (mode == DEFAULT_MODE) {
				// in default mode, select connection if source and
				// target are included in marqee selection
				included = sourceIncludedInMarqueeSelection
						&& targetIncludedInMarqueeSelection;
			} else if (mode == APPEND_MODE) {
				// in append mode, the current viewer selection is of interest
				// as well, so select connection if not already selected and
				// source and target are already selected or will get selected
				included = connection.getSelected() == EditPart.SELECTED_NONE
						&& (getCurrentViewer().getSelectedEditParts().contains(
								source) || sourceIncludedInMarqueeSelection)
						&& (getCurrentViewer().getSelectedEditParts().contains(
								target) || targetIncludedInMarqueeSelection);
			} else if (mode == TOGGLE_MODE) {
				if (connection.getSelected() == EditPart.SELECTED_NONE) {
					// connection is currently deselected, include it in the
					// marquee selection, i.e. select it, if one of
					// source or target will become selected in the new viewer
					// selection
					included = ((source.getSelected() == EditPart.SELECTED_NONE && sourceIncludedInMarqueeSelection) || (source
							.getSelected() != EditPart.SELECTED_NONE && !sourceIncludedInMarqueeSelection))
							&& ((target.getSelected() == EditPart.SELECTED_NONE && targetIncludedInMarqueeSelection) || (target
									.getSelected() != EditPart.SELECTED_NONE && !targetIncludedInMarqueeSelection));
				} else {
					// connection is currently selected, include it in marquee
					// selection, i.e. deselect it, if one of source or target
					// will become deselected in the new viewer selection
					included = (source.getSelected() != EditPart.SELECTED_NONE && sourceIncludedInMarqueeSelection)
							|| (target.getSelected() != EditPart.SELECTED_NONE && targetIncludedInMarqueeSelection);
				}
			}
		}
		return included;
	}
	
	/**
	 * Force icon show on all views including tree ones
	 * 
     * @param EditPartViewer - The viewer part that will receive the event focus
     *  
     */
	@Override
	protected boolean isViewerImportant(EditPartViewer viewer) {
		return true;
	}
}

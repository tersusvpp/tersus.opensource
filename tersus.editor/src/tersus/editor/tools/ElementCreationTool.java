/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.tools;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.Request;
import org.eclipse.gef.SharedCursors;
import org.eclipse.gef.EditPartViewer.Conditional;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.tools.TargetingTool;

import tersus.editor.editparts.TersusDirectEditFeature;
import tersus.editor.editparts.TersusEditPart;
import tersus.editor.requests.DropModelObjectRequest;
import tersus.editor.requests.EditInitialNameRequest;
import tersus.model.DataType;
import tersus.model.FlowModel;
import tersus.model.ModelObject;
import tersus.model.Note;
import tersus.model.Role;
import tersus.model.commands.AddElementAndPropagateSlotTypeCommand;
import tersus.model.commands.CreateElementFromTemplateCommand;
import tersus.util.Misc;

/**
 * A <code>TargetingTool</code> that triggers the creation of new elements from model objects.
 * 
 * The tool is activated when the user selects the corresponding palette entry.
 * 
 * @author Youval Bronicki
 * 
 */
public class ElementCreationTool extends TargetingTool
{

	private ModelObject sourceObject;
	private EditPart targetPart;

	public ElementCreationTool()
	{
		setDefaultCursor(SharedCursors.CURSOR_TREE_ADD);
		setDisabledCursor(SharedCursors.NO);
	}

	public ElementCreationTool(ModelObject sourceObject)
	{
		this();
		this.sourceObject = sourceObject;
	}

	protected Request createTargetRequest()
	{
		DropModelObjectRequest request = new DropModelObjectRequest();
		request.setSourceObject(sourceObject);
		return request;
	}

	protected String getCommandName()
	{
		return REQ_CREATE;
	}

	protected DropModelObjectRequest getDropModelObjectRequest()
	{
		return (DropModelObjectRequest) getTargetRequest();
	}

	protected String getDebugName()
	{
		return "Element Creation Tool"; //$NON-NLS-1$
	}

	protected boolean handleButtonDown(int button)
	{
		if (button != 1)
		{
			setState(STATE_INVALID);
			handleInvalidInput();
			return true;
		}
		this.targetPart = getTargetEditPart();
		if (stateTransition(STATE_INITIAL, STATE_DRAG))
		{
			getDropModelObjectRequest().setLocation(getLocation());
			lockTargetEditPart(getTargetEditPart());
		}
		return true;
	}

	protected boolean handleButtonUp(int button)
	{
		if (targetPart == null)
			return false;
		if (stateTransition(STATE_DRAG | STATE_DRAG_IN_PROGRESS, STATE_TERMINAL))
		{
			eraseTargetFeedback();
			unlockTargetEditPart();
			performCreation(button);
		}

		setState(STATE_TERMINAL);
		handleFinished();

		return true;
	}

	protected boolean handleDragInProgress()
	{
		if (isInState(STATE_DRAG_IN_PROGRESS))
		{
			updateTargetRequest();
			setCurrentCommand(getCommand());
			showTargetFeedback();
		}
		return true;
	}

	protected boolean handleDragStarted()
	{
		return stateTransition(STATE_DRAG, STATE_DRAG_IN_PROGRESS);

	}

	protected boolean handleFocusLost()
	{
		if (isInState(STATE_DRAG | STATE_DRAG_IN_PROGRESS))
		{
			eraseTargetFeedback();
			setState(STATE_INVALID);
			handleFinished();
			return true;
		}
		return false;
	}

	protected boolean handleMove()
	{
		updateTargetRequest();
		updateTargetUnderMouse();
		setCurrentCommand(getCommand());
		showTargetFeedback();
		return true;
	}

	protected void performCreation(int button)
	{
		Command command = getCurrentCommand();

		if (command != null)
		{
			if (AddElementAndPropagateSlotTypeCommand.class.equals(command.getClass())
					|| command instanceof CreateElementFromTemplateCommand)
			{
				EditInitialNameRequest request = new EditInitialNameRequest();
				request.setDirectEditFeature(TersusDirectEditFeature.INITIAL_NAME);
				request.setInitialCreationCommand(command);

				if (command instanceof CreateElementFromTemplateCommand)
					((CreateElementFromTemplateCommand) command).setDoPropagateSlotType(false);

				if (command instanceof AddElementAndPropagateSlotTypeCommand)
					((AddElementAndPropagateSlotTypeCommand) command).setPropagateSlotTypeOn(false);
				
				command.execute();

				if (command instanceof CreateElementFromTemplateCommand)
					((CreateElementFromTemplateCommand) command).setDoPropagateSlotType(true);
				
				if (command instanceof AddElementAndPropagateSlotTypeCommand)
					((AddElementAndPropagateSlotTypeCommand) command).setPropagateSlotTypeOn(true);

				Role newElementRole;
				if (command instanceof CreateElementFromTemplateCommand)
				{
					newElementRole = ((CreateElementFromTemplateCommand) command).getCreatedElement()
							.getRole();
				}
				else
				{
					Misc.assertion(command instanceof AddElementAndPropagateSlotTypeCommand);
					newElementRole = ((AddElementAndPropagateSlotTypeCommand) command).getNewElement()
							.getRole();
				}
				TersusEditPart newElementEditPart = ((TersusEditPart) targetPart)
						.getChild(newElementRole);
				if (newElementEditPart == null)
					return;
//				if (newElementEditPart.isComposite())
//					newElementEditPart.setOpen(true);
				getCurrentViewer().select(newElementEditPart);
				newElementEditPart.performRequest(request);
			}
			else
			{
				targetPart.getViewer().getEditDomain().getCommandStack().execute(command);
			}
		}

	}

	protected void updateTargetRequest()
	{
		if (isInState(STATE_DRAG_IN_PROGRESS))
		{
			Point loq = getStartLocation();
			Rectangle bounds = new Rectangle(loq, loq);
			bounds.union(loq.getTranslated(getDragMoveDelta()));
			getDropModelObjectRequest().setLocation(bounds.getLocation());
			if (sourceObject instanceof FlowModel)
			{
				double widthProportion = ((FlowModel) sourceObject).getWidth();
				int width = bounds.width;
				int height = bounds.height;
				if (width < height * widthProportion)
					width = (int) (height * widthProportion);
				else
					height = (int) (width / widthProportion);
				getDropModelObjectRequest().setSize(new Dimension(width, height));

			}
			else if (sourceObject instanceof DataType || sourceObject instanceof Note)
			{
				getDropModelObjectRequest().setSize(bounds.getSize());
			}

		}
		else
		{
			getDropModelObjectRequest().setSize(null);
			getDropModelObjectRequest().setLocation(getLocation());
			if (getTargetEditPart() != null)
			{
				getDropModelObjectRequest().setDefaultSize((TersusEditPart) getTargetEditPart());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.tools.TargetingTool#getTargetingConditional() overriden to accept all
	 * edit parts (if an edit part doesn't handle the request, we don't want the parent edit part to
	 * intercept it
	 */
	protected Conditional getTargetingConditional()
	{
		return new EditPartViewer.Conditional()
		{
			public boolean evaluate(EditPart editpart)
			{
				return true;
			}
		};
	}

}
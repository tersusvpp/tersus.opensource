/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.figures;

import java.beans.PropertyChangeEvent;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.tools.CellEditorLocator;

import tersus.editor.editparts.ElementEditPolicy;
import tersus.editor.editparts.TersusDirectEditPolicy;
import tersus.editor.editparts.TersusEditPart;
import tersus.editor.editparts.TersusXYLayoutEditPolicy;
import tersus.editor.layout.LayoutConstraint;
import tersus.model.BuiltinProperties;
import tersus.model.Role;

/**
 * @author Youval Bronicki
 *
 */
public class NoteEditPart extends TersusEditPart
{

	protected void elementChanged(Object sourceObject, String property, Object oldValue, Object newValue)
    {
        if (BuiltinProperties.COMMENT.equals(property))
            refreshLabel();
        else
			super.elementChanged(sourceObject,property, oldValue, newValue);
    }
    /* (non-Javadoc)
     * @see tersus.editor.editparts.TersusEditPart#getChild(tersus.model.Role)
     */
    public TersusEditPart getChild(Role role)
    {
        return null;
    }

    /* (non-Javadoc)
     * @see tersus.editor.editparts.TersusEditPart#refreshTraversingLinkEnds()
     */
    protected void refreshTraversingLinkEnds()
    {
    }

    /* (non-Javadoc)
     * @see tersus.editor.editparts.TersusEditPart#getRoleEditorLocator()
     */
    public CellEditorLocator getRoleEditorLocator()
    {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
     */
    protected IFigure createFigure()
    {
        NoteFigure f = new NoteFigure();
		f.setOpaque(true);
		return f;
    }

    /* (non-Javadoc)
     * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
     */
    protected void createEditPolicies()
    {
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, new TersusDirectEditPolicy());
		installEditPolicy(EditPolicy.COMPONENT_ROLE, new ElementEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, new TersusXYLayoutEditPolicy());
    }

    protected void refreshLabel()
    {
        NoteFigure f = (NoteFigure)getFigure();
        f.setText((String)getModelElement().getProperty(BuiltinProperties.COMMENT));
    }
    protected void refreshLayout()
    {
		LayoutConstraint constraint = new LayoutConstraint(getModelElement());
		((GraphicalEditPart) getParent()).setLayoutConstraint(this, getFigure(), constraint);
    }
	public void performRequest(Request req)
	{
		if (req.getType() == RequestConstants.REQ_OPEN)
			zoomToPart();
		else
			super.performRequest(req);
	}
	public void zoomToPart()
	{
		performZoomToPart();
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.figures;

import java.util.Iterator;

import org.eclipse.draw2d.ChangeEvent;
import org.eclipse.draw2d.ChangeListener;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseListener;
import org.eclipse.draw2d.geometry.Rectangle;

import tersus.editor.Debug;
import tersus.util.Misc;
import tersus.util.Trace;

/**
 * 
 */
public class OpenToggle extends Figure
{
	public static final int SIZE = 13; // Toggles are always drawn 9x9
	private static final int LENGTH = 7; // Length of '+'/'-' lines
	private static final int SMALL_MARGIN = 2; // Margin between '+'/'-' and square
	private static final int LARGE_MARGIN = 5;
	/**
	 * 
	 */
	private FlowRectangle owner;
	ChangeEvent changeEvent;
	public OpenToggle(FlowRectangle owner)
	{
		super();
		this.owner = owner;
		setOpaque(true);
		changeEvent = new ChangeEvent(this);
		addMouseListener(new MouseListener()
		{

			public void mousePressed(MouseEvent me)
			{
				toggle();
			}

			public void mouseReleased(MouseEvent me)
			{
			}

			public void mouseDoubleClicked(MouseEvent me)
			{
			}
		});
	}

	protected void toggle()
	{
		setOpen(!open);
	}

	boolean open = true;

	public void setOpen(boolean b)
	{
		if (open != b)
		{
			open = b;
			erase();
			repaint();
		}
		fireStateChanged();

	}

	public boolean isOpen()
	{
		return open;
	}

	protected void paintFigure(Graphics graphics)
	{

		graphics.setForegroundColor(owner.getLineColor());

		int lineWidth = 1;
		Rectangle r = getBounds();
		Misc.assertion(SIZE <= r.width); // bounds must accomodate SIZE pixels
		Misc.assertion(SIZE + 1 > r.width); // bounds must not accomodate more than SIZE pixels   
		int right = r.x + r.width;
		int left = right - SIZE;
		int x = left;
		int top = r.y;
		int y = top;
		int w = SIZE - lineWidth;
		int h = SIZE - lineWidth;
		if (Debug.PAINT)
			Trace.add("Drawing openToggle x=" + x + "y=" + y + " w=" + w + " h=" + h);
		//		graphics.setLineWidth(1);
		graphics.drawRectangle(x, y, w, h);
		graphics.setLineWidth(1);
		// draw the hozizontal line of common to  '+' and '-'
		int hX = left + lineWidth + SMALL_MARGIN;
		int hY = top + lineWidth + LARGE_MARGIN;
		graphics.drawRectangle(hX, hY, LENGTH - lineWidth, 0);
		// drawing a thick line doesn't work: the ends are pointed
		if (!isOpen())
		{
			// draw the vertical line the distinguished '+' from '-'
			int vX = left + lineWidth + LARGE_MARGIN;
			int vY = top + lineWidth + SMALL_MARGIN;
			graphics.drawRectangle(vX, vY, 0, LENGTH - lineWidth);
		}
		graphics.restoreState();
	}

	protected void fireStateChanged()
	{
		Iterator listeners = getListeners(ChangeListener.class);
		while (listeners.hasNext())
			 ((ChangeListener) listeners.next()) //Leave newline for debug stepping
			.handleStateChanged(changeEvent);
	}

	public void addChangeListener(ChangeListener toggleListener)
	{
		addListener(ChangeListener.class, toggleListener);
	}
}

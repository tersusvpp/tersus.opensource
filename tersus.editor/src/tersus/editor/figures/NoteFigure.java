/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.figures;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.text.FlowPage;
import org.eclipse.draw2d.text.ParagraphTextLayout;
import org.eclipse.draw2d.text.TextFlow;
import org.eclipse.swt.graphics.Font;

import tersus.editor.preferences.Preferences;

/**
 * @author Youval Bronicki
 *  
 */
public class NoteFigure extends Figure
{

    private TextFlow textFlow;
    private static final int CORNER_SIZE = 5;

    /**
     *  
     */
    public NoteFigure()
    {
        setFont(Preferences.getFont(Preferences.FLOW_FONT_SMALL));
        setBackgroundColor(Preferences.getColor(Preferences.NOTE_COLOR));
    	setBorder(new MarginBorder(5));
        FlowPage flowPage = new FlowPage();
        textFlow = new TextFlow();

        ParagraphTextLayout layout = new ParagraphTextLayout(textFlow,
                ParagraphTextLayout.WORD_WRAP_TRUNCATE);
        textFlow.setLayoutManager(layout);
        setLayoutManager(new StackLayout());
        flowPage.add(textFlow);
        setLayoutManager(new StackLayout());
        add(flowPage);
    }
    
    public void setText(String text)
    {
        textFlow.setText(text);
    }
    protected void paintFigure(Graphics graphics) {
    	Rectangle bounds = getBounds().getCopy();

    	graphics.translate(getLocation());

    	// fill the note
    	PointList outline = new PointList();
    	
    	outline.addPoint(0, 0);
    	outline.addPoint(bounds.width - CORNER_SIZE, 0);
    	outline.addPoint(bounds.width - 1, CORNER_SIZE);
    	outline.addPoint(bounds.width - 1, bounds.height - 1);
    	outline.addPoint(0, bounds.height - 1);
    	
    	graphics.fillPolygon(outline); 
    	
    	// draw the inner outline
    	PointList innerLine = new PointList();
    	
    	innerLine.addPoint(bounds.width - CORNER_SIZE - 1, 0);
    	innerLine.addPoint(bounds.width - CORNER_SIZE - 1, CORNER_SIZE);
    	innerLine.addPoint(bounds.width - 1, CORNER_SIZE);
    	innerLine.addPoint(bounds.width - CORNER_SIZE - 1, 0);
    	innerLine.addPoint(0, 0);
    	innerLine.addPoint(0, bounds.height - 1);
    	innerLine.addPoint(bounds.width - 1, bounds.height - 1);
    	innerLine.addPoint(bounds.width - 1, CORNER_SIZE);
    	
    	graphics.drawPolygon(innerLine);
    	
    	graphics.translate(getLocation().getNegated());
    }

}
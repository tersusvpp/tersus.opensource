/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.figures;

import org.eclipse.draw2d.AbstractConnectionAnchor;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.TreeSearch;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;

import tersus.editor.Debug;
import tersus.editor.TersusEditor;
import tersus.util.Trace;
import tersus.workbench.ImageLocator;
import tersus.workbench.TersusWorkbench;

/**
 * A figure that represents Flow models with as a rectangular area
 * 
 * The figure paints a rectangular "frame", surrounded by a margin intended for use by "protruding"
 * slots.
 * 
 * The figure should be selected only when the user click the "frame"
 */
public class FlowRectangle extends Figure implements Repeatable, PrintableFigure
{
	public String substituteLabel;

	protected boolean drawImage;

	private boolean readOnly;

	private InnerAnchor innerAnchor = new InnerAnchor(this);

	protected static final int OFFSET = 2; // offset for repetitive stack

	private String label;

	protected PrecisionRectangle frame = new PrecisionRectangle();

	private Rectangle stack1 = new Rectangle();

	private Rectangle stack2 = new Rectangle();

	private PrecisionRectangle preciseBounds;

	protected boolean open = false;

	protected boolean repetitive;

	private int lineWidth = 1;

	private double widthProportion;

	private ImageLocator imageLocator;

	private Color lineColor;

	protected Font font;

	public Rectangle imageFrame = new Rectangle(0, 0, 0, 0);

	public Rectangle targetFrame = new Rectangle();

	public Rectangle lockedImageFrame = new Rectangle(0, 0, 0, 0);

	public Rectangle lockedTargetFrame = new Rectangle();

	private Image lockedImage;

	public FlowRectangle()
	{
		super();
		bounds = preciseBounds = new PrecisionRectangle(0, 0, 64, 36);
		readOnly = false;

		ImageDescriptor lockedImageDescriptor = ImageDescriptor.createFromFile(TersusEditor.class,
				"icons/readOnlyState.big.gif");
		lockedImage = TersusWorkbench.getImage(lockedImageDescriptor);;
		lockedImageFrame.setSize(lockedImage.getBounds().width, lockedImage.getBounds().height);
	}

	public String getLabel()
	{
		return label;
	}

	public String toString()
	{
		return "RectangularFlowFigure[" + getLabel() + "]";
	}

	PreciseDimension tmpDim = new PreciseDimension();
	private int textX, textY;
	public boolean paintText;

	public void paintText(Graphics graphics)
	{
		if (paintText)
		{
			graphics.setFont(font);
			if (substituteLabel == null)
				graphics.drawText(getLabel(), textX, getTextY());
			else
				graphics.drawText(substituteLabel, textX, getTextY());

		}
	}

	public void paint(Graphics graphics)
	{
		// TODO Remove this debug-only method
		if (Debug.PAINT)
		{
			Trace.push("Painting " + this);
			Trace.add("Bounds=" + bounds + " frame=" + frame);
		}
		try
		{
			super.paint(graphics);
		}
		finally
		{
			if (Debug.PAINT)
				Trace.pop();
		}
	}

	private void paintRectangle(Graphics graphics, Rectangle rect)
	{
		graphics.fillRectangle(rect);
		int x = rect.x + lineWidth / 2;
		int y = rect.y + lineWidth / 2;
		int w = rect.width - lineWidth;
		int h = rect.height - lineWidth;

		if (w > 100000 && h > 100000)
			return;

		if (Debug.PAINT)
			Trace.add("Drawing frame: x=" + x + "y=" + y + " w=" + w + " h=" + h);
		graphics.drawRectangle(x, y, w, h);

		if (Debug.PAINT)
			Trace.add("lineWidth=" + lineWidth);
	}

	public void paintFigure(Graphics graphics)
	{
		graphics.setLineWidth(lineWidth);

		graphics.setForegroundColor(getLineColor());
		if (repetitive)
		{
			paintRectangle(graphics, stack1);
			paintRectangle(graphics, stack2);
		}
		paintRectangle(graphics, frame);
		graphics.restoreState();
		paintImage(graphics);
		paintText(graphics);

		paintLockImage(graphics);
	}

	private void paintImage(Graphics graphics)
	{
		if (drawImage && imageLocator != null)
		{
			Image image = imageLocator.getImage(imageFrame.width, imageFrame.height);
			if (Debug.PAINT)
				Trace.add("Painting image " + imageLocator + " imageFrame =" + imageFrame
						+ " targetFrame=" + targetFrame);
			graphics.drawImage(image, imageFrame, targetFrame);
		}
	}

	private void paintLockImage(Graphics graphics)
	{
		if (readOnly)
		{
			if (Debug.PAINT)
				Trace.add("Painting image " + imageLocator + " imageFrame =" + lockedImageFrame
						+ " targetFrame=" + lockedTargetFrame);

			graphics.drawImage(lockedImage, lockedImageFrame, lockedTargetFrame);
		}
	}

	public PrecisionRectangle getFrame()
	{
		return frame;
	}

	public void setFrame(PrecisionRectangle rectangle)
	{
		setFrame(rectangle.preciseX, rectangle.preciseY, rectangle.preciseWidth,
				rectangle.preciseHeight);
	}

	public void setFrame(double x, double y, double width, double height)
	{
		if (frame.equals(x, y, width, height))
			return;
		frame.setBounds(x, y, width, height);
		erase();
		if (repetitive)
			setStack();
		repaint();
	}

	private void setStack()
	{
		stack1 = frame.getCopy().translate(2 * OFFSET, 2 * OFFSET);
		stack2 = frame.getCopy().translate(OFFSET, OFFSET);
	}

	public void setLabel(String string)
	{
		label = string;
		repaint();
	}

	public boolean containsPoint(int x, int y)
	{
		if (!bounds.contains(x, y))
		{
			if (Debug.SELECT)
				Trace.add(this + " does not contain point " + x + ',' + y + " (out of bounds)");
			return false;
		}
		if (frame.contains(x, y))
		{
			if (Debug.SELECT)
				Trace.add(this + " contains point " + x + ',' + y + " (inside frame)");
			return true;
		}
		if (repetitive)
			if (stack1.contains(x, y) || stack2.contains(x, y))
			{

				if (Debug.SELECT)
					Trace.add(this + " contains point " + x + ',' + y + " (inside stack)");
				return true;
			}
		for (Object obj : getChildren())
		{
			IFigure child = (IFigure) obj;
			if (child.containsPoint(x, y))
			{
				if (Debug.SELECT)
					Trace.add(this + " contains point " + x + ',' + y + " (inside child " + child
							+ ")");
				return true;
			}
		}
		return false;
	}

	public int getMargin()
	{
		return frame.x - bounds.x;
	}

	public IFigure findFigureAt(int x, int y, TreeSearch search)
	{
		boolean tracePushed = false;
		if (Debug.SELECT)
		{
			Trace.push("findFigureAt() figure=" + this + " x=" + x + " y=" + y + " search="
					+ search);
			tracePushed = true;
		}
		// TODO Review this auto-generated method stub
		try
		{
			IFigure target = super.findFigureAt(x, y, search);
			if (Debug.SELECT)
				Trace.add("target=" + target);
			return target;
		}
		finally
		{
			if (tracePushed)
				Trace.pop();
		}
	}

	public void setOpen(boolean b)
	{
		if (open != b)
		{
			open = b;
			erase();
			repaint();
		}
	}

	public boolean isOpen()
	{
		return open;
	}

	public void setRepetitive(boolean b)
	{
		if (repetitive != b)
		{
			repetitive = b;
			erase();
			if (repetitive)
				setStack();
			repaint();
			fireMoved();
		}
	}

	public boolean isRepetitive()
	{
		return repetitive;
	}

	protected void primTranslate(int dx, int dy)
	{
		setBounds(preciseBounds.preciseX + dx, preciseBounds.preciseY + dy,
				preciseBounds.preciseWidth, preciseBounds.preciseHeight);
		invalidate();
	}

	public PrecisionRectangle getPreciseBounds()
	{
		return preciseBounds;
	}

	public void setBounds(Rectangle rect)
	{
		if (rect instanceof PrecisionRectangle)
			setBounds((PrecisionRectangle) rect);
		else
			setBounds(rect.x, rect.y, rect.width, rect.height);
	}

	public void setBounds(PrecisionRectangle rect)
	{
		setBounds(rect.preciseX, rect.preciseY, rect.preciseWidth, rect.preciseHeight);
	}

	public void setBounds(double x, double y, double width, double height)
	{
		if (Debug.LAYOUT)
		{
			Trace.add("setBounds() this=" + this + "x=" + x + " y=" + y + " width=" + width
					+ " height=" + height);
		}
		if (widthProportion > 0)
		{
			// TODO if rectangles for data and flow figures are separated, this block belongs to
			// Flow figures only
			if (width > height * widthProportion)
			{
				double centerX = x + width / 2;
				width = height * widthProportion;
				x = centerX - width / 2;
				if (Debug.LAYOUT)
					Trace.add("width adjusted to " + width + ", x adjusted to " + x
							+ " (widthProportion=" + widthProportion + ")");
			}
			else if (width < height * widthProportion)
			{
				double centerY = y + height / 2;
				height = width / widthProportion;
				y = centerY - height / 2;
				if (Debug.LAYOUT)
					Trace.add("height adjusted to " + height + ", y adjusted to " + y
							+ " (widthProportion=" + widthProportion + ")");
			}
		}
		if ((width != preciseBounds.preciseWidth) || (height != preciseBounds.preciseHeight)
				|| (x != preciseBounds.preciseX) || (y != preciseBounds.preciseY))
		{
			if (isVisible())
				erase();
			preciseBounds.setBounds(x, y, width, height);
			invalidate();
			fireMoved();
			repaint();
		}
	}

	public Rectangle getBounds()
	{
		// bounds.setBounds(preciseBounds);
		// preciseBounds.round();
		return preciseBounds;
	}

	public double getWidthProportion()
	{
		return widthProportion;
	}

	public void setWidthProportion(double d)
	{
		widthProportion = d;
		revalidate();
	}

	public ConnectionAnchor getInnerAnchor()
	{
		return innerAnchor;
	}

	public Font getFont()
	{
		return font;
	}

	public void setFont(Font font)
	{
		this.font = font;
	}

	public ImageLocator getImageLocator()
	{
		return imageLocator;
	}

	public void setImageLocator(ImageLocator locator)
	{
		imageLocator = locator;
	}

	public boolean hasImage()
	{
		return (imageLocator != null && imageLocator.imageExists());
	}

	public Color getLineColor()
	{
		if (lineColor == null)
			return getForegroundColor();
		else
			return lineColor;
	}

	public void setLineColor(Color color)
	{
		if (lineColor != null && lineColor.equals(color))
			return;
		lineColor = color;
		repaint();
	}

	public void setTextPosition(int textX, int textY)
	{
		this.textX = textX;
		this.textY = textY;
	}

	public int getTextX()
	{
		return textX;
	}

	public int getTextY()
	{
		return textY;
	}

	public boolean getDrawImage()
	{
		return drawImage;
	}

	public void setDrawImage(boolean drawImage)
	{
		this.drawImage = drawImage;
	}

	public boolean isReadOnly()
	{
		return readOnly;
	}

	public void setReadOnly(boolean readOnly)
	{
		this.readOnly = readOnly;
	}

	public Image getLockedImage()
	{
		return lockedImage;
	}

	private class InnerAnchor extends AbstractConnectionAnchor
	{
		Point location = new Point();

		private InnerAnchor(FlowRectangle owner)
		{
			super(owner);
		}

		public Point getLocation(Point ref)
		{
			Point reference = ref.getCopy();
			translateToRelative(reference);
			int offset = Math.min(10, Math.min(frame.width, frame.height) / 2);
			if (open)
			{
				location.y = frame.y + offset;
			}
			else
			{
				location.y = frame.y + frame.height / 2;
			}
			float centerX = frame.x + 0.5f * frame.width;
			if (reference.x < centerX)
			{
				location.x = frame.x + offset;
			}
			else
			{
				location.x = frame.right() - offset;
			}
			translateToAbsolute(location);
			if (Debug.LAYOUT)
			{
				Rectangle absoluteFrame = frame.getCopy();
				Trace.add("InnerAnchor of " + FlowRectangle.this + " getLocation() ref=" + ref
						+ " location=" + location + " ");
				translateToAbsolute(absoluteFrame);
				Trace.add("Frame=" + frame + " absoluteFrame=" + absoluteFrame);
			}
			return location;
		}

		/*
		 * Overriden to add visibility
		 */
		protected void fireAnchorMoved()
		{
			super.fireAnchorMoved();
		}

	}
}

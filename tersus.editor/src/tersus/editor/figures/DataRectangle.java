/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.figures;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.AbstractConnectionAnchor;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

import tersus.editor.Debug;
import tersus.util.Trace;

public class DataRectangle extends FlowRectangle
{

	private class EdgeAnchor extends AbstractConnectionAnchor
	{
		/* (non-Javadoc)
		 * @see org.eclipse.draw2d.ConnectionAnchorBase#getLocation(org.eclipse.draw2d.geometry.Point)
		 */
		Point location = new Point();
		/**
		 * @param owner
		 */
		private EdgeAnchor(DataRectangle owner)
		{
			super(owner);
		}

		public Point getLocation(Point ref)
		{
			Point reference = ref.getCopy();
			translateToRelative(reference);
			float minY, maxY;
			if (drawImage)
			{
				minY = targetFrame.y;
				maxY = targetFrame.bottom();
			}
			else if (paintText)
			{
				minY = getTextY();
				maxY = getTextY() + textHeight;
			}
			else
			{
				minY = frame.y;
				maxY = frame.height/2;
			}
			float centerX = bounds.x + 0.5f * bounds.width;
			float focX, edgeX;
			float focY = (minY + maxY) / 2;
			if (reference.x < centerX)
			{
				edgeX = bounds.x;
				focX = bounds.x + textHeight * .3f;
			}
			else
			{
				focX = bounds.right() - textHeight * .3f;
				edgeX = bounds.right() - 1;
			}
			float dx = reference.x - focX;
			float dy = reference.y - focY;
			float y;
			if (dx == 0)
				y = reference.y > focY ? maxY : minY;
			else
			{
				y = focY + dy * (edgeX - focX) / dx;
				if (y > maxY)
					y = maxY;
				if (y < minY)
					y = minY;
			}
			location.x = Math.round(edgeX);
			location.y = Math.round(y);

			translateToAbsolute(location);
			if (Debug.LAYOUT)
			{
				Rectangle absoluteFrame = frame.getCopy();
				Trace.add("EdgeAnchor of "+ DataRectangle.this + " getLocation() ref="+ref + " location="+location + " ");
				translateToAbsolute(absoluteFrame);
				Trace.add("Frame="+frame + " absoluteFrame="+ absoluteFrame);
			}
			return location;
		}
		/* 
		 * Overriden to add visibility
		 */
		protected void fireAnchorMoved()
		{
			super.fireAnchorMoved();
		}

	}
	public int status;
	private int sizeLevel;
	private int textHeight;
	private EdgeAnchor edgeAnchor = new EdgeAnchor(this);
	public static int STACK_OFFSET = OFFSET;

	/* (non-Javadoc)
	 * @see tersus.editor.figures.Repeatable#setRepetitive(boolean)
	 */
	public void setRepetitive(boolean b)
	{
		if (repetitive != b)
		{
			repetitive = b;
			revalidate();
			repaint();
		}
	}
	PreciseDimension tmpDim = new PreciseDimension();

	/**
	 * @return the size level for the current layout, indicating the combination
	 * of font size and offset size 
	 */
	public int getSizeLevel()
	{
		return sizeLevel;
	}

	/**
	 * @param i
	 */
	public void setSizeLevel(int i)
	{
		sizeLevel = i;
	}

	/**
	 * @return
	 */
	public int getTextHeight()
	{
		return textHeight;
	}

	/**
	 * @param i
	 */
	public void setTextHeight(int i)
	{
		textHeight = i;
	}

	/**
	 * @return
	 */
	public ConnectionAnchor getEdgeAnchor()
	{
		return edgeAnchor;
	}

	/* (non-Javadoc)
	 * @see tersus.editor.figures.FlowRectangle#setOpen(boolean)
	 */
	public void setOpen(boolean b)
	{
		// TODO Review this auto-generated method stub
		super.setOpen(b);
		edgeAnchor.fireAnchorMoved();
	}

	/**
	 * @return
	 */
	public boolean hasSubNodes()
	{
		List children = getChildren();
		int nChildren = children.size();
		for (int i = 0; i < children.size(); i++)
		{
			if (children.get(i) instanceof DataRectangle)
				return true;
		}
		return false;
	}

	/**
	 * @return
	 */
	List subNodes = new ArrayList();
	public List getSubNodes()
	{
		List children = getChildren();
		int nChildren = children.size();
		subNodes.clear();
		for (int i = 0; i < children.size(); i++)
		{
			if (children.get(i) instanceof DataRectangle)
				subNodes.add(children.get(i));
		}
		return subNodes;
	}
	/* (non-Javadoc)
	 * @see org.eclipse.draw2d.Figure#layout()
	 */
	protected void layout()
	{
		super.layout();
		//After the layout, the anchor position potetially changes
		edgeAnchor.fireAnchorMoved();
	}

}

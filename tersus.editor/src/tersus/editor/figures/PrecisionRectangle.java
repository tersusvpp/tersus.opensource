/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.figures;

import org.eclipse.draw2d.geometry.Point;

import org.eclipse.draw2d.geometry.PrecisionPoint;
import org.eclipse.draw2d.geometry.Rectangle;

import tersus.util.Misc;

/**
 * 
 */
public class PrecisionRectangle extends Rectangle
{
	/** Double value for X **/
	public double preciseX;

	/** Double value for Y **/
	public double preciseY;

	public double preciseWidth;
	public double preciseHeight;
	/**
	 * Constructor for PrecisionRectangle.
	 */
	public PrecisionRectangle()
	{
		super();
		Misc.assertion(valid());
	}

	/**
	 * Constructor for PrecisionRectangle.
	 * @param copy Point from which the initial values are taken
	 */
	public PrecisionRectangle(Rectangle copy)
	{
		super();
		setBounds(copy);
		if (Misc.ASSERTIONS) Misc.assertion(valid());
	}

	public PrecisionRectangle(PrecisionRectangle copy)
	{
		super();
		setBounds(copy);
		if (Misc.ASSERTIONS) Misc.assertion(valid());
	}
	/**
	 * Constructor for PrecisionRectangle.
	 * @param x X value
	 * @param y Y value
	 */
	public PrecisionRectangle(int x, int y, int width, int height)
	{
		super(x, y, width, height);
		preciseX = (double) x;
		preciseY = (double) y;
		preciseWidth = (double) width;
		preciseHeight = (double) height;
		if (Misc.ASSERTIONS) Misc.assertion(valid());
	}

	/**
	 * Constructor for PrecisionRectangle.
	 * @param x X value
	 * @param y Y value
	 */
	public PrecisionRectangle(double x, double y, double width, double height)
	{
		if (Misc.ASSERTIONS) Misc.assertion(valid());
		setBounds(x, y, width, height);
	}

	/**
	 * 
	 */
	private boolean valid()
	{
		Misc.assertion(x == round(preciseX));
		Misc.assertion(y == round(preciseY));
		Misc.assertion(width == round(preciseWidth + preciseX) - x);
		Misc.assertion(height == round(preciseHeight + preciseY) - y);
		//		assert width == round(preciseWidth);
		//		assert height == round(preciseHeight);
		return true;

	}

	/**
	 * @see org.eclipse.draw2d.geometry.Point#performScale(double)
	 */
	public void performScale(double factor)
	{
		if (Misc.ASSERTIONS) Misc.assertion(valid());
		preciseX = preciseX * factor;
		preciseY = preciseY * factor;
		preciseWidth = preciseWidth * factor;
		preciseHeight = preciseHeight * factor;
		calculate();
	}

	/**
	 * @see org.eclipse.draw2d.geometry.Point#performTranslate(int, int)
	 */
	public void performTranslate(int dx, int dy)
	{
		if (Misc.ASSERTIONS) Misc.assertion(valid());
		performTranslate((double) dx, (double) dy);
	}
	public void performTranslate(double dx, double dy)
	{
		preciseX += dx;
		preciseY += dy;
		calculate();
	}

	/**
	 * @see org.eclipse.draw2d.geometry.Point#setLocation(Point)
	 */
	public Rectangle setLocation(Point pt)
	{
		if (Misc.ASSERTIONS) Misc.assertion(valid());
		if (pt instanceof PrecisionPoint)
		{
			preciseX = ((PrecisionPoint) pt).preciseX;
			preciseY = ((PrecisionPoint) pt).preciseY;
		}
		else
		{
			preciseX = pt.x;
			preciseY = pt.y;
		}
		calculate();
		return this;
	}
	/* (non-Javadoc)
	 * @see org.eclipse.draw2d.geometry.Rectangle#setBounds(org.eclipse.draw2d.geometry.Rectangle)
	 */
	public Rectangle setBounds(Rectangle rect)
	{
		if (Misc.ASSERTIONS) Misc.assertion(valid());
		if (rect instanceof PrecisionRectangle)
			setBounds((PrecisionRectangle) rect);
		else
		{
			super.setBounds(rect);
			preciseX = (double) x;
			preciseY = (double) y;
			preciseWidth = (double) width;
			preciseHeight = (double) height;
		}
		if (Misc.ASSERTIONS) Misc.assertion(valid());
		return this;
	}
	public PrecisionRectangle setBounds(PrecisionRectangle rect)
	{
		if (Misc.ASSERTIONS) Misc.assertion(valid());
		setBounds(rect.preciseX, rect.preciseY, rect.preciseWidth, rect.preciseHeight);

		return this;
	}

	private int round(double value)
	{
		return (int) Math.round(value);
	}
	public void calculate()
	{
		x = round(preciseX);
		y = round(preciseY);
		width = round(preciseX + preciseWidth) - x;
		height = round(preciseY + preciseHeight) - y;
		//		width = round(preciseWidth);
		//		height = round(preciseHeight);
	}
	public PrecisionRectangle setBounds(double x, double y, double width, double height)
	{
		if (Misc.ASSERTIONS) Misc.assertion(valid());
		preciseX = x;
		preciseY = y;
		preciseWidth = width;
		preciseHeight = height;
		calculate();
		return this;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o)
	{
		if (Misc.ASSERTIONS) Misc.assertion(valid());
		if (this == o)
			return true;
		if (o instanceof PrecisionRectangle)
			return equals((PrecisionRectangle) o);
		return false;
	}
	public boolean equals(PrecisionRectangle o)
	{
		if (Misc.ASSERTIONS) Misc.assertion(valid());
		if (this == o)
			return true;
		return equals(o.preciseX, o.preciseY, o.preciseWidth, o.preciseHeight);
	}

	/**
	 * @param width
	 * @param height
	 */
	public void setSize(double width, double height)
	{
		if (Misc.ASSERTIONS) Misc.assertion(valid());
		preciseWidth = width;
		preciseHeight = height;
		calculate();

	}

	/* (non-Javadoc)
	 * @see org.eclipse.draw2d.geometry.Rectangle#getCopy()
	 */
	public Rectangle getCopy()
	{
		return new Rectangle(this);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		// TODO Review this auto-generated method stub
		return "PrecisionRectangle["
			+ preciseX
			+ "("
			+ x
			+ "), "
			+ preciseY
			+ "("
			+ y
			+ "), "
			+ preciseWidth
			+ "("
			+ width
			+ "), "
			+ preciseHeight
			+ "("
			+ height
			+ ")]";
	}

	/**
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @return
	 */
	public boolean equals(double x, double y, double width, double height)
	{
		return preciseX == x && preciseY == y && preciseWidth == width && preciseHeight == height;
	}

}
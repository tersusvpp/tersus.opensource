/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.figures;

import org.eclipse.draw2d.geometry.Translatable;

/**
 * 
 */
public class PreciseDimension implements Translatable
{
	public double width,height;
	/* (non-Javadoc)
	 * @see org.eclipse.draw2d.geometry.Translatable#performTranslate(int, int)
	 */
	public void performTranslate(int dx, int dy)
	{
	}

	/* (non-Javadoc)
	 * @see org.eclipse.draw2d.geometry.Translatable#performScale(double)
	 */
	public void performScale(double factor)
	{
		width*= factor;
		height*= factor;
	}

}

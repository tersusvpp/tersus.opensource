/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.figures;

import java.util.List;

import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;

import tersus.editor.Debug;
import tersus.editor.preferences.Preferences;
import tersus.util.Trace;

/**
 * 
 */
public class LinkArrow extends PolylineConnection
{

    private boolean dashed = false;

    private boolean bidirectional;

    private String id;

    /**
     * @param string
     */
    public LinkArrow(String id)
    {
        this.id = id;
        head = new PolygonDecoration();
        setTargetDecoration(head);
    }

    private PolygonDecoration head;

    private PolygonDecoration tail;

    double headLength;

    // temporary storage for points
    private static Point p1 = new Point();

    private static Point p2 = new Point();

    public void layout()
    {
        if (Debug.LAYOUT)
            Trace.push("Calculating layout for " + this);
        try
        {
        	if (getBounds() == null)
        		setBounds(new Rectangle(0,0,1,1));
            super.layout();

            int nPoints = getPoints().size();
            if (Debug.LAYOUT)
            {
                Trace.add("Source:" + getPoints().getPoint(p1, 0));
                Trace.add("Target:" + getPoints().getPoint(p1, nPoints - 1));
            }
            Point lastPoint = getPoints().getPoint(p1, nPoints - 1);
            Point beforeLast = getPoints().getPoint(p2, nPoints - 2);
            double dx = lastPoint.x - (double) beforeLast.x;
            double dy = lastPoint.y - (double) beforeLast.y;
            double lastSegmentLength = Math.sqrt(dx * dx + dy * dy);
            double maxHeadLength = Preferences.getInt(Preferences.MAX_ARROW_HEAD_SIZE);
            headLength = Math.min(maxHeadLength, lastSegmentLength / 2);
            head.setScale(headLength, headLength / 2);
            if (tail != null)
                tail.setScale(headLength, headLength / 2);
            bounds = null; // forces a recalucaltion of bounds the next time
        }
        finally
        {
            if (Debug.LAYOUT)
                Trace.pop();
        }
    }

    public void paint(Graphics graphics)
    {
        if (Debug.PAINT)
        {
            Trace.push("Painting " + this);
            Trace.add("bounds= " + getBounds());
            Trace.add("clientArea= " + getClientArea());
            Trace.add(" headLength=" + headLength);
            Trace.add(" head bounds=" + head.getBounds());
        }
        try
        {
            /*
             * if (isDashed()) { graphics.pushState();
             * graphics.setLineStyle(SWT.LINE_CUSTOM); graphics.setLineDash(new
             * int[] {2, 3 }); super.paint(graphics);
             * graphics.setLineDash(null);
             * graphics.setLineStyle(SWT.LINE_SOLID);
             * 
             * graphics.popState(); graphics.setLineStyle(SWT.LINE_SOLID);
             * graphics.setLineDash(null); } else
             */

            super.paint(graphics);
        }
        finally
        {
            if (Debug.PAINT)
                Trace.pop();
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    public String toString()
    {
        return "LinkArrow[" + id + "]";
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.draw2d.IFigure#containsPoint(int, int)
     */
    private static final int TOLERANCE = 2;

    private static final Rectangle LINEBOUNDS = Rectangle.SINGLETON;

    /*
     * Overriden as the super implementation does not work properly with our
     * 'heavy' zooming. Fixed by using longs for the calculation and by
     * adjusting the scale based on the zoom.
     */
    public boolean containsPoint(int x, int y)
    {

        int tolerance = Math.max(lineWidth / 2, TOLERANCE);
        LINEBOUNDS.setBounds(getBounds());
        LINEBOUNDS.expand(tolerance, tolerance);
        if (!LINEBOUNDS.contains(x, y))
            return false;
        int ints[] = getPoints().toIntArray();
        for (int index = 0; index < ints.length - 3; index += 2)
        {
            if (lineContainsPoint(ints[index], ints[index + 1], ints[index + 2], ints[index + 3], x, y, tolerance))
                return true;
        }

        List children = getChildren();
        for (int i = 0; i < children.size(); i++)
        {
            if (((IFigure) children.get(i)).containsPoint(x, y))
                return true;
        }
        return false;
    }

    private boolean lineContainsPoint(int x1, int y1, int x2, int y2, int px, int py, int tolerance)
    {
        LINEBOUNDS.setSize(0, 0);
        LINEBOUNDS.setLocation(x1, y1);
        LINEBOUNDS.union(x2, y2);
        LINEBOUNDS.expand(tolerance, tolerance);
        if (!LINEBOUNDS.contains(px, py))
            return false;

        long v1x, v1y, v2x, v2y;
        long numerator, denominator;
        long result = 0;

        if (x1 != x2 && y1 != y2)
        {
            v1x = x2 - (long) x1;
            v1y = y2 - (long) y1;
            v2x = px - (long) x1;
            v2y = py - (long) y1;

            numerator = (v2x * v1y - v1x * v2y);
            denominator = (v1x * v1x + v1y * v1y);

            result = numerator * numerator / denominator;
            /** In very deep zoom the calculation above can still cause an overflow.  Currently (Sep 2008) in these levels we have other problems,
             * so we can't really test any correction.  The following is an attempt to correct the problem by changing the order of operations,
             * but this may have its precision problems:
             * 
             */

        }

        /*
         * If the line is horizontal or vertical, and the point passes the
         * bounding box test, the result is always true.
         */

        boolean contained = result <= tolerance * (long) tolerance;
        if (false)
            Trace
                    .add(this + " distance=" + Math.sqrt(result) + " tolerance = " + tolerance + " contained="
                            + contained);
        
        return contained;

    }

    /*
     * Tracing wrapper for super implementation
     */
    public void setSourceAnchor(ConnectionAnchor anchor)
    {
        if (Debug.LINKS)
            Trace.add(this + " sourceAnchor " + getSourceAnchor() + "->" + anchor);
        super.setSourceAnchor(anchor);
    }

    /*
     * Tracing wrapper for super implementation
     */
    public void setTargetAnchor(ConnectionAnchor anchor)
    {
        if (Debug.LINKS)
            Trace.add(this + " targetAnchor " + getTargetAnchor() + "->" + anchor);
        super.setTargetAnchor(anchor);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.draw2d.IFigure#intersects(org.eclipse.draw2d.geometry.Rectangle)
     *      Overriden: check whether either the head or any of the segments
     *      intersect the rectangle.
     */
    public boolean intersects(Rectangle rect)
    {
        PointList pointList = getPoints();
        int nPoints = pointList.size();
        for (int i = 0; i < nPoints - 1; i++)
        {
            pointList.getPoint(p1, i);
            pointList.getPoint(p2, i + 1);
            if (rectangleIntersectsSegment(rect, p1, p2))
                return true;
        }

        return head.intersects(rect);
    }

    /**
     * Check wether a rectangle intersects a line segment
     * 
     * @param rect The rectangle
     * @param p1 The first end-point of the line segment
     * @param p2 The second end-point of the line segment
     * @return true if the line-segment from p1 to p2 intersects the rectangle
     *         (or is contained in the rectangle)
     */
    private boolean rectangleIntersectsSegment(Rectangle rect, Point p1, Point p2)
    {
        // A rectangle intersects a segment if and only if it contains one of
        // the end points or if any of its edges intersects the segment
        if (rect.contains(p1) || rect.contains(p2))
            return true;
        if (segmentsIntersect(p1.x, p1.y, p2.x, p2.y, rect.x, rect.y, rect.right(), rect.y))
            return true;
        if (segmentsIntersect(p1.x, p1.y, p2.x, p2.y, rect.x, rect.y, rect.x, rect.bottom()))
            return true;
        if (segmentsIntersect(p1.x, p1.y, p2.x, p2.y, rect.right(), rect.y, rect.right(), rect.bottom()))
            return true;
        if (segmentsIntersect(p1.x, p1.y, p2.x, p2.y, rect.x, rect.bottom(), rect.right(), rect.bottom()))
            return true;

        return false;

    }

    /**
     * Tests whether two line segments intersect
     * 
     * @param x1 The x coodinate of point 1
     * @param y1 The y coodinate of point 1
     * @param x2 The x coodinate of point 2
     * @param y2 The y coodinate of point 2
     * @param x3 The x coodinate of point 3
     * @param y3 The y coodinate of point 3
     * @param x4 The x coodinate of point 4
     * @param y4 The y coodinate of point 4
     * @return true if the line segment from point 1 to point 2 intersects the
     *         line segment from point 3 to point 4
     */
    private boolean segmentsIntersect(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4)
    {
        /*
         * The two line segments intersect iff: Points 3 and 4 are on differenct
         * sides of the line through points 1 and 2 AND Points 1 and 2 are on
         * different sides of the line through points 3 and 4
         */
        long crossProduct1 = (x2 - x1) * (long) (y3 - y1) - (y2 - y1) * (long) (x3 - x1);
        long crossProduct2 = (x2 - x1) * (long) (y4 - y1) - (y2 - y1) * (long) (x4 - x1);
        if (crossProduct1 * crossProduct2 > 0)
            return false; // Points 3 & 4 are on the same side of the
        // (infinite) line that goes through point 1 and 2

        long crossProduct3 = (x4 - x3) * (long) (y1 - y3) - (y4 - y3) * (long) (x1 - x3);
        long crossProduct4 = (x4 - x3) * (long) (y2 - y3) - (y4 - y3) * (long) (x2 - x3);

        if (crossProduct3 * crossProduct4 > 0)
            return false; // Points 1 & 2 are on the same side of the
        // (infinite) line that goes through points 3 and 4

        return true;
    }

    /**
     * @param b
     */
    public void setBidirectional(boolean b)
    {
        if (bidirectional != b)
        {
            bidirectional = b;
            if (bidirectional)
            {
                tail = new PolygonDecoration();
                setSourceDecoration(tail);
            }
            else
            {
                tail = null;
                setSourceDecoration(null);
            }
        }

    }

    public boolean isDashed()
    {
        return dashed;
    }

    public void setDashed(boolean dashed)
    {
        this.dashed = dashed;
        setLineStyle(dashed ? SWT.LINE_DASH : SWT.LINE_SOLID);
    }
}
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.figures;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.AbstractConnectionAnchor;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FigureListener;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;

import tersus.editor.Debug;
import tersus.util.Misc;
import tersus.util.Trace;

/**
 * 
 */
public class SlotFigure extends Figure implements Repeatable, PrintableFigure
{

	public static final int TRIANGLE = 1;
	public static final int DIAMOND = 2;
	private int lineWidth = 1;

	private ConnectionAnchor sourceAnchor;
	private ConnectionAnchor targetAnchor;

	/**
	 * Holds a list of the LinkArrows that are connected to this slot, and
	 * that are "external" in the sense that they belong to the 
	 * parent of this slot's flow rectangle.  This list is maintained
	 * in order to allow the slot's label to adjust its position according
	 * to the links.
	 */
	private ArrayList externalLinks = new ArrayList();

	/**
	 * 
	 */
	private String name;
	private Point tip = new Point();
	private Point side1 = new Point();
	private Point side2 = new Point();
	private Point tail = new Point();
	private PointList points = new PointList();
	private PointList stacked = new PointList();
	private int orientation, side;
	private int shape = -1;
	private boolean repetitive = false;
	private SlotLabel label;

	public void setName(String name)
	{
		this.name = name;
	}
	public String toString()
	{
		return "Slot[" + name + "]";
	}
	public SlotFigure()
	{
		super();
		bounds = new Rectangle(0, 0, 100, 50);
		orientation = PositionConstants.RIGHT;
		points.setSize(3);
		stacked.setSize(3);
		sourceAnchor = new AbstractConnectionAnchor(this)
		{
			private Point source = new Point();
			private Point ref = new Point();
			public Point getLocation(Point arg0)
			{
				if (shape == TRIANGLE)
				{
					source.x = tip.x;
					source.y = tip.y;
				}
				else
				{
					Misc.assertion(shape == DIAMOND);
					ref.setLocation(arg0);
					translateToRelative(ref);
					long dTip = getDistanceSquared(ref, tip);
					long dTail = getDistanceSquared(ref, tail);
					if (dTip < dTail)
					{
						source.setLocation(tip);
					}
					else
					{
						source.setLocation(tail);
					}
				}
				translateToAbsolute(source);
				return source;
			}
		};
		targetAnchor = new AbstractConnectionAnchor(this)
		{

			private Point target = new Point();
			public Point getLocation(Point arg0)
			{
				target.x = tail.x;
				target.y = tail.y;

				translateToAbsolute(target);
				return target;
			}
		};

	}

	/* (non-Javadoc)
	 * @see org.eclipse.draw2d.IFigure#setBounds(org.eclipse.draw2d.geometry.Rectangle)
	 */
	public void setBounds(Rectangle rect)
	{
		if (Debug.LAYOUT)
			Trace.add(
				"setBounds() this="
					+ this
					+ " rect="
					+ rect
					+ " bounds="
					+ bounds
					+ " rect==bounds:"
					+ (rect == bounds));
		boolean resize = false;
		boolean translate = false;
		if (bounds != null)
		{
			resize = (rect.width != bounds.width) || (rect.height != bounds.height);
			translate = (rect.x != bounds.x) || (rect.y != bounds.y);
		}
		if (resize || translate)
		{
			bounds.x = rect.x;
			bounds.y = rect.y;
			bounds.width = rect.width;
			bounds.height = rect.height;
			setPoints();
		}
	}

	public void setOrientation(int orientation)
	{
		this.orientation = orientation;
		if (bounds != null)
			setPoints();
	}
	private static final Point tmpPoint = new Point();
	public void setPoints()
	{
		if (shape == TRIANGLE)
		{
			points.setSize(3);
			stacked.setSize(3);
		}
		else
		{
			Misc.assertion(shape == DIAMOND);

			points.setSize(4);
			stacked.setSize(4);
		}

		erase();
		calculatePoints();
		offsetLength = Math.min(bounds.width / 2, 3);
		offset.width = 0;
		offset.height = 0;
		switch (orientation)
		{
			case PositionConstants.TOP :
				offset.height = -offsetLength;
				break;
			case PositionConstants.BOTTOM :
				offset.height = offsetLength;
				break;
			case PositionConstants.RIGHT :
				offset.width = offsetLength;
				break;
			case PositionConstants.LEFT :
				offset.width = -offsetLength;
		}
		if (!repetitive)
		{
			points.setPoint(side1, 0);
			points.setPoint(tip, 1);
			points.setPoint(side2, 2);
			if (shape == DIAMOND)
				points.setPoint(tail, 3);
		}
		else
		{
			tmpPoint.setLocation(tip);
			tmpPoint.x -= offset.width;
			tmpPoint.y -= offset.height;
			points.setPoint(side1, 0);
			points.setPoint(tmpPoint, 1);
			points.setPoint(side2, 2);

			if (shape == DIAMOND)
				points.setPoint(tail, 3);

			tmpPoint.setLocation(side1);
			tmpPoint.translate(offset);
			stacked.setPoint(tmpPoint, 0);
			stacked.setPoint(tip, 1);
			tmpPoint.setLocation(side2);
			tmpPoint.translate(offset);
			stacked.setPoint(tmpPoint, 2);

			if (shape == DIAMOND)
			{
				tmpPoint.setLocation(tail);
				tmpPoint.translate(offset);
				stacked.setPoint(tmpPoint, 3);

			}
		}

		if (Debug.LAYOUT)
		{
			Trace.add(this +": tip=" + tip + " back1=" + side1 + " back2=" + side2);
		}
		repaint();
		fireFigureMoved();
		revalidateLabel();
	}

	int offsetLength;
	static Dimension offset = new Dimension();
	private void calculatePoints()
	{
		switch (orientation)
		{
			case PositionConstants.TOP :
				calculatePointsUp();
				break;
			case PositionConstants.BOTTOM :
				calculatePointsDown();
				break;
			case PositionConstants.LEFT :
				calculatePointsLeft();
				break;
			default :
				Misc.assertion(orientation == PositionConstants.RIGHT);
				calculatePointsRight();
				break;
		}
	}
	private void calculatePointsRight()
	{
		if (shape == TRIANGLE)
		{

			side1.x = bounds.x;
			side1.y = bounds.y;
			tip.x = bounds.x + bounds.width - 1;
			tip.y = bounds.y + bounds.height / 2;
			side2.x = bounds.x;
			side2.y = bounds.y + bounds.height - 1;
			tail.x = bounds.x;
			tail.y = bounds.y + bounds.height / 2;
		}
		else
		{
			Misc.assertion(shape == DIAMOND);
			int x = bounds.x + bounds.width / 2;
			int y = bounds.y + bounds.height / 2;
			int d = (bounds.width - 1) / 2;
			side1.x = x;
			side1.y = y-d;
			tip.x = x+d;
			tip.y = y;
			side2.x = x;
			side2.y = y+d;
			tail.x = x-d;
			tail.y = y;
		}
	}
	private void calculatePointsLeft()
	{
		if (shape == TRIANGLE)
		{

			side1.x = bounds.x + bounds.width - 1;
			side1.y = bounds.y;
			tip.x = bounds.x;
			tip.y = bounds.y + bounds.height / 2;
			side2.x = bounds.x + bounds.width - 1;
			side2.y = bounds.y + bounds.height - 1;
			tail.x = bounds.x + bounds.width - 1;
			tail.y = bounds.y + bounds.height / 2;
		}
		else
		{
			Misc.assertion(shape == DIAMOND);
			int x = bounds.x + bounds.width / 2 - 1;
			int y = bounds.y + bounds.height / 2;
			int d = (bounds.width - 1) / 2;
			side1.x = x;
			side1.y = y - d;
			tip.x = x - d;
			tip.y = y;
			side2.x = x;
			side2.y = y + d;
			tail.x = x + d;
			tail.y = y;
		}
	}
	private void calculatePointsUp()
	{
		if (shape == TRIANGLE)
		{
			side1.x = bounds.x;
			side1.y = bounds.y + bounds.height - 1;
			tip.x = bounds.x + bounds.width / 2;
			tip.y = bounds.y;
			side2.x = bounds.x + bounds.width - 1;
			side2.y = bounds.y + bounds.height - 1;
			tail.x = bounds.x + bounds.width / 2;
			tail.y = bounds.y + bounds.height - 1;
		}
		else
		{
			//FUNC3 correct and test this case
			Misc.assertion(shape == DIAMOND);
			int x = bounds.x + bounds.width / 2;
			int y = bounds.y + bounds.height / 2-1;
			int d = (bounds.width - 1) / 2;
			side1.x = x-d;
			side1.y = y;
			tip.x = x;
			tip.y = y-d;
			side2.x = x+d;
			side2.y = y;
			tail.x = x;
			tail.y = y+d;

		}
	}
	private void calculatePointsDown()
	{
		if (shape == TRIANGLE)
		{
			side1.x = bounds.x;
			side1.y = bounds.y;
			tip.x = bounds.x + bounds.width / 2;
			tip.y = bounds.y + bounds.height - 1;
			side2.x = bounds.x + bounds.width - 1;
			side2.y = bounds.y;
			tail.x = bounds.x + bounds.width / 2;
			tail.y = bounds.y;
		}
		else
		{
			Misc.assertion(shape == DIAMOND);
			int x = bounds.x + bounds.width / 2;
			int y = bounds.y + bounds.height / 2;
			int d = (bounds.width - 1) / 2;
			side1.x = x-d;
			side1.y = y;
			tip.x = x;
			tip.y = y+d;
			side2.x = x+d;
			side2.y = y;
			tail.x = x;
			tail.y = y-d;
		}
	}
	public void paint(Graphics graphics)
	{
		if (Debug.PAINT)
		{
			Trace.push("Painting " + this);
			Trace.add("Bounds=" + bounds);
			Trace.add("tip=" + tip);
			Trace.add("tail=" + tail);
		}
		try
		{
			super.paint(graphics);
		}
		finally
		{
			if (Debug.PAINT)
				Trace.pop();
		}

	}

	/* (non-Javadoc)
	 * @see org.eclipse.draw2d.IFigure#getBounds()
	 */
	public Rectangle getBounds()
	{
		return bounds;
	}

	/**
	 * @return
	 */
	public ConnectionAnchor getSourceAnchor()
	{
		return sourceAnchor;
	}

	/**
	 * @return
	 */
	public ConnectionAnchor getTargetAnchor()
	{
		return targetAnchor;
	}
	
	public boolean containsPoint(int x, int y)
	{
		if (!getBounds().contains(x, y))
			return false;
		if (containsPoint(points, x, y))
			return true;
		if (repetitive && containsPoint(stacked, x, y))
			return true;
		return false;
	}
	private boolean containsPoint(PointList pointList, int x, int y)
	{
		boolean isOdd = false;
		int[] pointsxy = pointList.toIntArray();
		int n = pointsxy.length;
		if (n > 3)
		{ //If there are at least 2 Points (4 ints)
			int x1, y1;
			int x0 = pointsxy[n - 2];
			int y0 = pointsxy[n - 1];

			for (int i = 0; i < n; x0 = x1, y0 = y1)
			{
				x1 = pointsxy[i++];
				y1 = pointsxy[i++];

				if (y0 <= y && y < y1 && crossProduct(x1, y1, x0, y0, x, y) > 0)
					isOdd = !isOdd;
				if (y1 <= y && y < y0 && crossProduct(x0, y0, x1, y1, x, y) > 0)
					isOdd = !isOdd;
			}
			if (isOdd)
				return true;
		}

		List children = getChildren();
		for (int i = 0; i < children.size(); i++)
			if (((IFigure) children.get(i)).containsPoint(x, y))
				return true;

		return false;
	}

	private long crossProduct(long ax, long ay, long bx, long by, long cx, long cy)
	{
		return (ax - cx) * (by - cy) - (ay - cy) * (bx - cx);
	}
	public void paintFigure(Graphics graphics)
	{
		graphics.setLineWidth(lineWidth);
		if (repetitive)
		{
			paintPolygon(graphics, stacked);
		}
		paintPolygon(graphics, points);
	}
	/**
	 * @param graphics
	 * @param points
	 */
	private void paintPolygon(Graphics graphics, PointList points)
	{
		graphics.fillPolygon(points);
		graphics.drawPolygon(points);

	}

	/* (non-Javadoc)
	 * @see org.eclipse.draw2d.Figure#primTranslate(int, int)
	 */
	protected void primTranslate(int dx, int dy)
	{
		points.translate(dx, dy);
		tip.translate(dx, dy);
		side1.translate(dx, dy);
		side2.translate(dx, dy);
		tail.translate(dx, dy);
		super.primTranslate(dx, dy);
	}

	/**
	 * @return
	 */
	public boolean isRepetitive()
	{
		return repetitive;
	}

	/**
	 * @param b
	 */
	public void setRepetitive(boolean b)
	{
		if (b != repetitive)
		{
			repetitive = b;
			setPoints();
		}
	}
	/**
	 * @param arrow
	 */

	private void revalidateLabel()
	{
		if (label != null)
			label.revalidate();
	}
	/**
	 * Adds a LinkArrow to the list of external LinkArrows connected to this 
	 * slot. 
	 * Note: The list of extenal links is maintained to allow the slot's 
	 * label to adjust its to position to the layout of links. 
	 * @param arrow A LinkArrow which has been disconnected from this SlotFigure
	 */
	public void removeExternalLink(LinkArrow arrow)
	{
		arrow.removeFigureListener(listener);
		externalLinks.remove(arrow);
		revalidateLabel();
	}
	/**
	 * Adds a LinkArrow to the list of external LinkArrows connected to this 
	 * slot. 
	 * Note: The list of extenal links is maintained to allow the slot's 
	 * label to adjust its to position to the layout of links. 
	 * @param arrow A LinkArrow which has been connected to the slot.
	 */
	public void addExternalLink(LinkArrow arrow)
	{
		Misc.assertion(!externalLinks.contains(arrow));
		externalLinks.add(arrow);
		arrow.addFigureListener(listener);
		revalidateLabel();
	}

	/**
	 * A FigureListener that reacts to  motion events by revalidating the slot's label.
	 * This mechanism was added to allow slot labels to adjust themselves to the current layout of the slot's links and to
	 * the slot's parent rectangle.
	 */
	private FigureListener listener = new FigureListener()
	{

		public void figureMoved(IFigure source)
		{
			revalidateLabel();
		}
	};
	/**
	 * @return The orientation of this SlotFigure - the direction of the figure's tip (the value is 
	 * one of (PositionConstants.LEFT, PositionConstants.RIGHT, PositionConstants.TOP, PositionConstants.BOTTOM)
	 */
	public int getOrientation()
	{
		return orientation;
	}

	/**
	 * @return The side of the FlowRectangle on which this SlotFigure is positioned - 
	 * one of (PositionConstants.LEFT, PositionConstants.RIGHT, PositionConstants.TOP, PositionConstants.BOTTOM)
	 */
	public int getSide()
	{
		return side;
	}

	/**
	 * Sets the side of this SlotFigure relative to the parent 
	 * flow.  
	 * 
	 * Note: This parameter is maintained for conveniece.  The
	 * side is derived from the SlotFigure's position, and is set by
	 * the slot's EditPart.  Changing the side does not affect the 
	 * layout of the figure, but may affect the layout of its label.
	 * @param side The side,  one of PositionConstants.LEFT, PositionConstants.RIGHT, PositionConstants.TOP, PositionConstants.BOTTOM
	 */
	public void setSide(int side)
	{
		this.side = side;
	}

	/**
	 * @return True if any of the links connected to this slot intersects
	 * the given rectangle.
	 * @param The given Rectangle (in absolute coordinates)
	 */
	private static Rectangle tmpRect = new Rectangle();
	public boolean hasIntersectingLinks(Rectangle rect)
	{
		for (int i = 0; i < externalLinks.size(); i++)
		{

			LinkArrow link = (LinkArrow) externalLinks.get(i);
			tmpRect.setBounds(rect);
			link.translateToRelative(tmpRect);
			if (link.intersects(tmpRect))
				return true;
		}
		return false;
	}
	/**
	 * Sets this slot's SlotLabel
	 * @param label A 
	 */
	public void removeLabel()
	{
		if (label != null)
			getParent().removeFigureListener(listener);
		label = null;

	}

	/**
	 * @return The SlotLabel attached to this SlotFigure, or null if there is no label
	 */
	public SlotLabel getLabel()
	{
		return label;
	}
	/**
	 * @return
	 */
	public SlotLabel createLabel(String text)
	{
		Misc.assertion(label == null);
		label = new SlotLabel(this, text);
		getParent().addFigureListener(listener);
		return label;
	}
	/* (non-Javadoc)
	 * @see org.eclipse.draw2d.IFigure#setBackgroundColor(org.eclipse.swt.graphics.Color)
	 */
	public void setBackgroundColor(Color bg)
	{
		super.setBackgroundColor(bg);
		if (label != null)
			label.repaint();
	}

    public void setForegroundColor(Color fg)
    {
        super.setForegroundColor(fg);
    }
	/**
	 * @return
	 */
	public int getShape()
	{
		return shape;
	}

	/**
	 * @param i
	 */
	public void setShape(int i)
	{
		shape = i;
	}

	static long getDistanceSquared(Point p1, Point p2)
	{
		long dx = p1.x - p2.x;
		long dy = p1.y - p2.y;

		return dx * dx + dy * dy;
	}
    public int getLineWidth()
    {
        return lineWidth;
    }
    public void setLineWidth(int lineWidth)
    {
        this.lineWidth = lineWidth;
    }
}

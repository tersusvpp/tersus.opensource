/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.figures;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import tersus.editor.Debug;
import tersus.editor.EditorPlugin;
import tersus.editor.preferences.Preferences;
import tersus.util.Misc;
import tersus.util.Trace;
/**
 * A Label for a SlotFigure.
 * <br>
 * This class extends Label in the following ways:
 * <ul>
 * <li>It is transparent (does not respond to mouse clicks)</li>
 * <li>The label know how to position itself relative to the 'owning' slot figure</li>
 * </ul>
 * @author Youval Bronicki
 *
 */
public class SlotLabel extends Label
{
	SlotFigure owner;
	/**
	 * 
	 */
	public SlotLabel(SlotFigure owner, String label)
	{
		super(label);
		Misc.assertion(owner != null);
		Misc.assertion(label != null);
		this.owner = owner;
		setFont(Preferences.getFont(Preferences.SLOT_LABEL_FONT));
	}
	/* (non-Javadoc)
	 * @see org.eclipse.draw2d.Figure#layout()
	 * 
	 * Sets the position of this label relative to the owning SlotFigure
	 */
	private static Rectangle r = new Rectangle();
	protected void layout()
	{
		//FUNC2 hide the figure if the slot is too small
		Rectangle slotBounds = owner.getBounds().getCopy();
		if (slotBounds.width < 2 * Preferences.getInt(Preferences.MIN_SLOT_SIZE))
		{
			setVisible(false);
			setLocation(slotBounds.getLocation()); // Need to update the location even if label is invisible, or scroll problems may occur
			return;
		}
		setVisible(true);
		FlowRectangle parentFlowRectangle = (FlowRectangle) owner.getParent();
		Rectangle flowFrame = parentFlowRectangle.getFrame().getCopy();
		parentFlowRectangle.translateToAbsolute(flowFrame);
		owner.translateToAbsolute(slotBounds);
		Point location = new Point();
		Dimension size = getPreferredSize();
		setSize(size);
		r.setSize(size);
		int side = owner.getSide();
		switch (side)
		{
			case PositionConstants.RIGHT :
				if (parentFlowRectangle.isRepetitive())
					location.x = r.x = flowFrame.right() + FlowRectangle.OFFSET * 2;
				else
					location.x = r.x = flowFrame.right() + 1;
				location.y = r.y = slotBounds.y - size.height;
//				if (owner.hasIntersectingLinks(r))
//				{
//					r.y = slotBounds.bottom();
//					if (!owner.hasIntersectingLinks(r))
//						location.y = r.y;
//				}
				break;
			case PositionConstants.LEFT :
				location.x = r.x = flowFrame.x - size.width;
				location.y = r.y = slotBounds.y - size.height;
//				if (owner.hasIntersectingLinks(r))
//				{
//					r.y = slotBounds.bottom();
//					if (!owner.hasIntersectingLinks(r))
//						location.y = r.y;
//				}
				break;
			case PositionConstants.TOP :
				location.y = r.y = flowFrame.y - size.height - 1;
				location.x = r.x = slotBounds.right();
//				if (owner.hasIntersectingLinks(r))
//				{
//					r.x = slotBounds.x - size.width;
//					if (!owner.hasIntersectingLinks(r))
//						location.x = r.x;
//				}
				break;
			default :
				Misc.assertion(side == PositionConstants.BOTTOM);
				if (parentFlowRectangle.isRepetitive())
					location.y = r.y = flowFrame.bottom() + FlowRectangle.OFFSET * 2;
				else
					location.y = r.y = flowFrame.bottom();
				location.x = r.x = slotBounds.right();
//				if (owner.hasIntersectingLinks(r))
//				{
//					r.x = slotBounds.x - size.width;
//					if (!owner.hasIntersectingLinks(r))
//						location.x = r.x;
//				}
		}
		translateToRelative(location);
		setLocation(location);
		Rectangle labelBounds = getBounds().getCopy();
		translateToAbsolute(labelBounds);
		if (Debug.LAYOUT)
			Trace.add(
				"SlotLabel.layout(): slot="
					+ owner
					+ " slotBounds="
					+ slotBounds
					+ " flowBounds="
					+ flowFrame
					+ " labelBounds="
					+ labelBounds);
	}
	/* (non-Javadoc)
	 * @see org.eclipse.draw2d.IFigure#containsPoint(int, int)
	 */
	public boolean containsPoint(int x, int y)
	{
		return false;
	}
	/* (non-Javadoc)
	 * @see org.eclipse.draw2d.Figure#paintFigure(org.eclipse.draw2d.Graphics)
	 */
	protected void paintFigure(Graphics graphics)
	{
		graphics.setForegroundColor(getDarkerColor(owner.getBackgroundColor()));
		super.paintFigure(graphics);
		graphics.restoreState();
	}
	/**
	 * @param original The original color
	 * @return A Color that is 35% darker than a given color.
	 */
	private Color getDarkerColor(Color original)
	{
		double factor = 0.65; //FUNC3 use preferences
		int red = (int) (original.getRed() * factor);
		int green = (int) (original.getGreen() * factor);
		int blue = (int) (original.getBlue() * factor);
		RGB rgb = new RGB(red, green, blue);
		return EditorPlugin.getDefault().getColor(rgb);
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2019 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor;

import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PerspectiveAdapter;
import org.eclipse.ui.PlatformUI;

/**
 * @author Liat Shiff
 */
public class Startup implements IStartup
{

	final String SEARCH_PERSPECTIVE_LABEL = "Search";
	final String KEYBOARD_SHORTCUTS_LABEL = "Keyboard Shortcuts";
	final String CHEAT_SHEETS = "Cheat Sheets";

	public void earlyStartup()
	{
		/*
		 * The registration of the listener should have been done in the UI thread since
		 * PlatformUI.getWorkbench().getActiveWorkbenchWindow() returns null if it is called outside
		 * of the UI thread.
		 */
		Display.getDefault().asyncExec(new Runnable()
		{
			/*
			 * (non-Javadoc)
			 * 
			 * @see java.lang.Runnable#run()
			 */
			public void run()
			{
				if (PlatformUI.getWorkbench().isStarting()) {
		            Display.getDefault().timerExec(1000, this);
		        } else {
		        	final IWorkbenchWindow workbenchWindow = PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow();
					if (workbenchWindow != null)
					{
						TersusPerspectiveAdapter tersusPerspectiveListener = new TersusPerspectiveAdapter();
			            workbenchWindow.addPerspectiveListener(tersusPerspectiveListener);
			            
			            tersusPerspectiveListener.perspectiveActivated(PlatformUI
			                    .getWorkbench().getActiveWorkbenchWindow()
			                    .getActivePage(), PlatformUI.getWorkbench()
			                    .getActiveWorkbenchWindow().getActivePage()
			                    .getPerspective());
					}
		        }
			}
		});
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.useractions;

import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPage;

import tersus.editor.TersusEditor;

/**
 * 
 */
public interface UserActionContext
{
	TersusEditor getEditor();

	/**
	 * @return
	 */
	IWorkbenchPage getPage();

	/**
	 * @return
	 */
	Display getDisplay();
}

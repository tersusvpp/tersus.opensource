/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.useractions;

import org.eclipse.gef.ui.actions.GEFActionConstants;

/**
 * 
 */
public class ZoomOutUserAction extends UserAction
{

	/**
	 * 
	 */
	public ZoomOutUserAction()
	{
		setText("zoomOut();");
	}

	/* (non-Javadoc)
	 * @see tersus.editor.useractions.UserAction#run(tersus.editor.useractions.UserActionContext)
	 */
	public void run(UserActionContext context)
	{
		context.getEditor().getActionRegistry().getAction(GEFActionConstants.ZOOM_OUT).run();
	}

}

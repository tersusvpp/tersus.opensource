/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.useractions;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;

import tersus.InternalErrorException;

/**
 * 
 */
public class ShowPropertySheetUserAction extends UserAction
{
	public ShowPropertySheetUserAction()
	{
		super();
		String text="showPropertySheet();";
		setText(text);
	}
	

	/* (non-Javadoc)
	 * @see tersus.editor.useractions.UserAction#run()
	 */
	public void run(UserActionContext context)
	{
		IWorkbenchPage page = context.getPage();
		try
		{
			page.showView(IPageLayout.ID_PROP_SHEET);
		}
		catch (PartInitException e)
		{
			e.printStackTrace();
			throw new InternalErrorException("Can't show Property Sheet",e);
		}
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.useractions;

import tersus.util.TraceItem;

/**
 * 
 */
public class Comment extends TraceItem
{

	/**
	 * @param e
	 */
	public Comment(Throwable e)
	{
		super(e);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	public Comment()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param type
	 * @param text
	 */
	public Comment(String type, String text)
	{
		super(type, text);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param text
	 */
	public Comment(String text)
	{
		super(text);
		// TODO Auto-generated constructor stub
	}

}

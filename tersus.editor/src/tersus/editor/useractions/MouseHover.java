/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.useractions;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;

/**
 * 
 */
public class MouseHover extends MouseAction
{
	protected int type()
	{
		return SWT.MouseHover;
	}
	protected String method()
	{
		return "mouseHover";
	}

	public MouseHover(MouseEvent me)
	{
		init(me);
	}

	public MouseHover(int x, int y, int button)
	{
		init(x, y, button);
	}

}

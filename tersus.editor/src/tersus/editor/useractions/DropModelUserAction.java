/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.useractions;

import tersus.editor.requests.DropModelObjectRequest;
import tersus.model.Model;
import tersus.model.ModelId;

/**
 * 
 */
public class DropModelUserAction extends UserAction
{

	/**
	 * 
	 */
	public DropModelUserAction(DropModelObjectRequest request)
	{
		super();
		if (request.getSourceObject() instanceof Model)
		{
			ModelId modelId = ((Model)request.getSourceObject()).getId();
			setText("dropModel(\""+modelId+"\","+request.getLocation().x+","+request.getLocation().y+");");
		}
		else
		{
			setText("dropResource(\""+request.getSourceObject()+"\","+request.getLocation().x+","+request.getLocation().y+");");
		}
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see tersus.editor.useractions.UserAction#run()
	 */
	public void run(UserActionContext context)
	{
		// TODO Review this auto-generated method stub

	}

}

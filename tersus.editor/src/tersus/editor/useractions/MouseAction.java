/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.useractions;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.widgets.Event;

/**
 * 
 */
public abstract class MouseAction extends UserAction
{

	static int state;
	protected int x;
	protected int y;
	protected int button;
	protected abstract int type();
	protected abstract String method();
	/**
	 * 
	 */
	protected void init(MouseEvent me)
	{
		init(me.x, me.y, me.button);
	}

	protected void init(int x, int y, int button)
	{
		this.x = x;
		this.y = y;
		this.button = button;

		setText(method() + "(" + x + "," + y + "," + button + ");");
	}

	protected Event createEvent(UserActionContext context)
	{
		Event e = new Event();
		e.type = type();
		if (e.type == SWT.MouseDown)
			setMouseState(button);
		else if (e.type == SWT.MouseUp)
			resetMouseState();
		e.button = button;
		e.display = context.getDisplay();
		e.widget = context.getEditor().getViewer().getControl();
		e.x = x;
		e.y = y;
		e.stateMask = state;
		return e;
	}
	/* (non-Javadoc)
	 * @see tersus.editor.useractions.UserAction#run(tersus.editor.useractions.UserActionContext)
	 */
	public void run(UserActionContext context)
	{
		Event e = createEvent(context);
		context.getEditor().getViewer().sendMouseEvent(e);
	}
	
	protected void setMouseState(int button)
	{
		switch (button)
		{
			case 1 :
				state= SWT.BUTTON1;
			case 2 :
				state= SWT.BUTTON2;
			case 3 :
				state= SWT.BUTTON3;
		}
	}
	
	protected void resetMouseState()
	{
		state = 0;
	}
}
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.useractions;

import org.eclipse.core.resources.IFile;
import org.eclipse.gef.commands.Command;

import tersus.model.ModelId;
import tersus.util.TraceItem;
import tersus.workbench.TersusWorkbench;

/**
 * 
 */
public abstract class UserAction extends TraceItem
{
	/**
	 * @param type
	 * @param text
	 */
	public UserAction()
	{
		setType(TraceItem.USER_ACTION);
		// TODO Auto-generated constructor stub
	}

	public abstract void run(UserActionContext context);

	/**
	 * @return
	 */
	public String getGeneratedCode()
	{
		return getText();
	}

	protected void executeCommand(Command command, UserActionContext context) {
		if (command != null && command.canExecute()) {
			context.getEditor().getViewer().getEditDomain().getCommandStack().execute(command);
		}
	}

}

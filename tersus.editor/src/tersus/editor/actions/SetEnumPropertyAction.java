/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuCreator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;

import tersus.editor.TersusEditor;
import tersus.model.ModelObject;
import tersus.model.commands.SetPropertyValueCommand;
import tersus.util.Enum;
import tersus.util.EnumFactory;
import tersus.util.Misc;
import tersus.workbench.TersusWorkbench;

/**
 * An action that allows setting the value of an enumerated property
 * 
 * @author Youval Bronicki
 */
public abstract class SetEnumPropertyAction extends SetPropertyAction implements IMenuCreator
{
	private EnumFactory factory;
	private List<SetValueAction> actions;
	private Menu menu;

	/**
	 * 
	 */
	public SetEnumPropertyAction(TersusEditor editor, String propertyName, String category,
			EnumFactory factory)
	{
		super(editor);
		setMenuCreator(this);
		this.factory = factory;
		this.category = category;
		this.propertyName = propertyName;
		setText(propertyName);
		actions = createActions();
	}

	/**
	 * @return
	 */
	private List<SetValueAction> createActions()
	{
		List<SetValueAction> actions = new ArrayList<SetValueAction>();
		Enum[] values = factory.getValues();
		for (int i = 0; i < values.length; i++)
		{
			Enum value = values[i];
			SetValueAction action = new SetValueAction(value);
			actions.add(action);

		}
		return actions;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.IMenuCreator#dispose()
	 */
	public void dispose()
	{
		// TODO Review this auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.IMenuCreator#getMenu(org.eclipse.swt.widgets.Control)
	 */
	public Menu getMenu(Control parent)
	{
		// TODO Review this auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.IMenuCreator#getMenu(org.eclipse.swt.widgets.Menu)
	 */
	public Menu getMenu(Menu parent)
	{
		if (menu != null)
			menu.dispose();
		menu = new Menu(parent);

		for (SetValueAction action : actions)
		{
			addActionToMenu(menu, action);
		}

		return menu;
	}

	/**
	 * Helper method that wraps the given action in an ActionContributionItem and then adds it to
	 * the given menu.
	 * 
	 * @param parent
	 *            The menu to which the given action is to be added
	 * @param action
	 *            The action that is to be added to the given menu
	 */
	protected void addActionToMenu(Menu parent, IAction action)
	{
		ActionContributionItem item = new ActionContributionItem(action);
		item.fill(parent, -1);
	}

	private class SetValueAction extends Action
	{
		Enum value;

		/**
		 * @param value
		 */
		public SetValueAction(Enum value)
		{
			super(String.valueOf(value), AS_RADIO_BUTTON);
			this.value = value;
		}

		public void run()
		{
			CompoundCommand command = new CompoundCommand("Set " + propertyName);
			for (Object selectedObject : getSelectedObjects())
			{
				ModelObject target = getTarget(selectedObject);
				if (target == null)
					continue;

				// BUG for some reason, when the user checks a new value, the current value's action
				// is also
				// executed. The following code prevents this bug from interfering with the
				// CommandStack mechanism
				if (target.getProperty(propertyName) == value)
					continue;
				command.add(new SetPropertyValueCommand(target, propertyName, value));
			}
			if (!command.isEmpty())
			{
				try
				{
					((TersusEditor) getWorkbenchPart()).getEditDomain().getCommandStack().execute(
							command);
				}
				catch (Exception e)
				{
					StringBuffer message = new StringBuffer();
					message.append(e.getMessage());

					MessageDialog.openInformation(TersusWorkbench.getActiveWorkbenchShell(),
							"Read Only Package/File", message.toString());
				}
			}

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.actions.EditorPartAction#calculateEnabled()
	 */
	protected boolean calculateEnabled()
	{
		if (super.calculateEnabled())
		{
			ModelObject target = getFirstSelectedObject();
			Misc.assertion(target != null);
			for (SetValueAction action : actions)
			{
				action.setChecked(action.value == target.getProperty(propertyName));
			}
			return true;
		}
		else
			return false;
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuCreator;
import org.eclipse.search.ui.NewSearchUI;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;

import tersus.editor.TersusEditor;
import tersus.editor.editparts.MissingModelEditPart;
import tersus.editor.editparts.TersusEditPart;
import tersus.editor.showLinks.ShowLinksQuery;
import tersus.model.Link;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.model.Slot;

/**
 * 
 * @author Liat Shiff
 */
public class ShowLinksAction extends EditorSelectionAction implements IMenuCreator
{
	private Menu menu;

	private ModelElement selectedElement;

	public ShowLinksAction(TersusEditor editor)
	{
		super(editor);
		setMenuCreator(this);
		setId(TersusActionConstants.SHOW_SHOW_LINKS_ID);
		setText(TersusActionConstants.SHOW_SHOW_LINKS_TEXT);
	}

	@Override
	protected boolean calculateEnabled()
	{
		TersusEditPart selectedEditPart = getSelectedEditPart();
		
		if (selectedEditPart instanceof MissingModelEditPart)
			return false;
		
		List<ModelObject> modelObjects = getSelectedModelObjects();

		if (modelObjects.size() != 1)
			return false;

		if (modelObjects.get(0) instanceof Link || modelObjects.get(0) instanceof Slot)
			return false;

		if (!(modelObjects.get(0) instanceof ModelElement))
			return false;
		else
			selectedElement = (ModelElement) modelObjects.get(0);

		return true;
	}

	private void populateMenu()
	{
		addActionToMenu(menu, new ShowSpecificLinks(ShowLinksQuery.SHOW_INCOMING_LINKS));
		addActionToMenu(menu, new ShowSpecificLinks(ShowLinksQuery.SHOW_OUTGOING_LINKS));
	}

	protected void addActionToMenu(Menu parent, IAction action)
	{
		ActionContributionItem item = new ActionContributionItem(action);
		item.fill(parent, -1);
	}


	public Menu getMenu(Menu parent)
	{
		if (menu == null || menu.isDisposed())
			menu = new Menu(parent);
		populateMenu();
		return menu;
	}

	public Menu getMenu(Control parent)
	{
		return null;
	}

	private class ShowSpecificLinks extends Action
	{
		private String searchType;

		public ShowSpecificLinks(String searchType)
		{
			super(searchType);
			this.searchType = searchType;
		}

		public void run()
		{
			NewSearchUI.runQueryInForeground(null, new ShowLinksQuery(searchType, selectedElement));
		}
	}
}

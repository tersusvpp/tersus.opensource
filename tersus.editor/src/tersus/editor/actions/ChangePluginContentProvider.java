/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import java.util.ArrayList;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import tersus.model.BuiltinPlugins;

public class ChangePluginContentProvider implements IStructuredContentProvider
{
	public void dispose()
	{
	}

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
	{
	}

	public Object[] getElements(Object inputElement)
	{
		ArrayList<String> elements = new ArrayList<String>();
		
		if (BuiltinPlugins.ACTION.equals(inputElement))
			elements.add(BuiltinPlugins.SERVICE);
		else if (BuiltinPlugins.SERVICE.equals(inputElement))
			elements.add(BuiltinPlugins.ACTION);
		
		return elements.toArray();
	}
}

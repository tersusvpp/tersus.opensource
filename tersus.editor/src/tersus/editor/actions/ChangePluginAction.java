/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import java.util.List;

import org.eclipse.ui.dialogs.ListDialog;

import tersus.editor.TersusEditor;
import tersus.model.BuiltinProperties;
import tersus.model.FlowModel;
import tersus.model.FlowType;
import tersus.model.Model;
import tersus.model.commands.MultiStepCommand;

/**
 * @author Liat Shiff 
 */
public class ChangePluginAction extends EditorSelectionAction
{
	public ChangePluginAction(TersusEditor editor)
	{
		super(editor);

		setId(TersusActionConstants.CHANGE_PLUGIN_ACTION_ID);
		setText(TersusActionConstants.CHANGE_PLUGIN_ACTION_TEXT);
	}

	protected boolean calculateEnabled()
	{
		 List selectedList = getSelectedObjects();
		 
		 if(selectedList.size() == 1)
		 {
			 Model selectedModel = getSelectedModel();
			 
			 if (selectedModel instanceof FlowModel
						&& (FlowType.ACTION.equals(((FlowModel) selectedModel).getType()) || FlowType.SERVICE
								.equals(((FlowModel) selectedModel).getType())))
					return true; 
		 }
		return false;
	}

	public void run()
	{
		ListDialog dialog = createChangePluginDialog();
		dialog.open();

		final Object[] result = dialog.getResult();
		if (result != null)
		{
			MultiStepCommand command = new MultiStepCommand(null)
			{
				protected void run()
				{
					String newPlugin = (String) result[0];
					Model selectedModel = getSelectedModel();
					setProperty(selectedModel, BuiltinProperties.PROTOTYPE, null);
					setProperty(selectedModel, BuiltinProperties.PLUGIN, newPlugin);
				}
			};

			((TersusEditor) getWorkbenchPart()).getEditDomain().getCommandStack().execute(command);
		}
	}

	public ListDialog createChangePluginDialog()
	{
		ListDialog dialog = new ListDialog(getShell());

		dialog.setTitle("Change Plugin");
		dialog.setMessage("select the plugin which you want to replace with the current");

		dialog.setContentProvider(new ChangePluginContentProvider());
		dialog.setLabelProvider(new ChangePluginLabelProvider());

		dialog.setInput(getSelectedModel().getPlugin());

		return dialog;
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.actions;

import org.eclipse.core.resources.IProject;
import org.eclipse.gef.commands.Command;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.actions.RenameResourceAction;

import tersus.editor.Debug;
import tersus.editor.EditorMessages;
import tersus.editor.RepositoryManager;
import tersus.editor.editparts.MissingModelEditPart;
import tersus.editor.wizards.RenameWizard;
import tersus.model.ModelObject;
import tersus.model.Note;
import tersus.util.Trace;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * Change a model's id.
 * 
 * @author Ofer Brandes
 */

public class RenameAction extends RepositoryAction
{
	private RenameResourceAction renameResourceAction;

	/**
	 * @param editor
	 */
	public RenameAction(IWorkbenchPart part)
	{
		super(part);
		setId(TersusActionConstants.RENAME_ACTION_ID);
		setText(TersusActionConstants.RENAME_ACTION_TEXT);
		renameResourceAction = new RenameResourceAction(getShell());
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.actions.EditorPartAction#calculateEnabled()
	 */
	protected boolean calculateEnabled()
	{
		if (Debug.OTHER)
			Trace.push("RenameAction.calculateEnabled()");
		try
		{
			if (getSelectedObject() instanceof MissingModelEditPart)
				return false;
			if (getModelObject() instanceof Note)
				return false;
			if (getModelObject() != null)
				return true;
			else if (getPackageEntry() != null)
				return true;
			else if (getProject() != null)
				return true;
			else
				return false;
		}

		finally
		{
			if (Debug.OTHER)
				Trace.pop();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.IAction#run()
	 */
	public void run()
	{
		if (Debug.OTHER)
			Trace.push("RenameAction.run()");
		try
		{
			if (getModelObject() == null && getPackageEntry() == null)
			{
				if (Debug.OTHER)
					Trace.add("No model selected (or multiple models selected)");
			}
			if (getModelObject() != null)
			{
				renameModelObject();

			}
			else if (getProject() != null)
			{
				renameProject();
			}
			else
				renamePackage();

		}
		finally
		{
			if (Debug.OTHER)
				Trace.pop();
		}
	}

	private boolean checkIfEditorsAreClosed()
	{
		boolean editorsClosed = getRepositoryManager().getActiveEditors().isEmpty();
		if (!editorsClosed)
		{
			MessageDialog.openInformation(getShell(), "Rename",
					"Please close all open editors before renaming a project");
		}
		return editorsClosed;
	}

	protected void renameProject()
	{
		if (!checkIfEditorsAreClosed())
			return;

		IProject originalProject = getProject();
		RepositoryManager repositoryManager = getRepositoryManager();

		if (repositoryManager.getCommandStack().isDirty())
		{
			MessageDialog.openInformation(getShell(), "Rename",
					"Please save or discard all changes before renaming a project");
			return;
		}
		renameResourceAction.selectionChanged((IStructuredSelection) getSelection());
		renameResourceAction.run();

		if (!originalProject.exists())
			repositoryManager.dispose();

	}

	private void renamePackage()
	{
		RepositoryManager repositoryManager = getRepositoryManager();
		final String oldName = getOldPackageName();
		IInputValidator validator = new IInputValidator()
		{
			public String isValid(String newName)
			{
				Command command = getRenamePackageCommand(newName);
				return ((ValidatingCommand) command).validate();
			}

		};
		// FUNC2 - Use a better dialog
		InputDialog dialog = new InputDialog(TersusWorkbench.getActiveWorkbenchShell(),
				EditorMessages.Rename_Dialog_Title, EditorMessages.Rename_Dialog_Message, oldName,
				validator);
		dialog.setBlockOnOpen(true);
		if (dialog.open() != InputDialog.OK)
		{
			if (Debug.OTHER)
				Trace.add("Rename cancelled (in rename dialog)");
			return;
		}
		String newName = dialog.getValue();

		Command command = getRenamePackageCommand(newName);
		if (command.canExecute())
			repositoryManager.getCommandStack().execute(command);

	}

	/**
     * 
     */
	private void renameModelObject()
	{
		ModelObject modelObject = getModelObject();
		Shell shell = getShell();
		openRenameWizard(modelObject, shell, null);
	}

	public static void openRenameWizard(ModelObject modelObject, Shell shell,
			String initialElementName)
	{
		RenameWizard wizard = new RenameWizard(modelObject);
		wizard.setInitialElementName(initialElementName);
		WizardDialog dialog = new WizardDialog(shell, wizard);
		dialog.setMinimumPageSize(0, 0);
		dialog.open();
	}

	Command getRenamePackageCommand(String newName)
	{
		WorkspaceRepository repository = getRepositoryManager().getRepository();

		if (getPackageEntry() != null)
			return new RenamePackageCommand(repository, getPackageEntry().getPackageId(), newName);
		else
			return null;

	}

	/**
	 * @return
	 */
	private String getOldPackageName()
	{
		if (getPackageEntry() != null)
			return getPackageEntry().getPackageId().getName();
		else
			return null;
	}

}
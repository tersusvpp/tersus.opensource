/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;

import tersus.editor.RepositoryManager;
import tersus.model.ModelId;
import tersus.model.PackageId;
import tersus.model.indexer.RepositoryIndex;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public class ReplaceWithContentProvider implements ITreeContentProvider
{
	private TreeViewer viewer;

	private RepositoryIndex repositoryIndex;

	private IProject project;

	public ReplaceWithContentProvider(TreeViewer viewer)
	{
		this.viewer = viewer;
	}

	public Object[] getChildren(Object parentElement)
	{
		ArrayList<SearchDialogEntry> elements = new ArrayList<SearchDialogEntry>();

		// I think this condition will never occur
		if (parentElement instanceof IProject)
		{
			elements = getPackageList(null);
			return elements.toArray();
		}
		else if (parentElement instanceof SearchDialogPackageEntry)
		{
			SearchDialogPackageEntry parentPackageEntry = (SearchDialogPackageEntry) parentElement;

			elements.addAll(getModeList(parentPackageEntry.getPackageId(), parentPackageEntry));
			elements.addAll(getPackageList(parentPackageEntry.getPackageId()));

			return elements.toArray();
		}
		return null;
	}

	private ArrayList<SearchDialogEntry> getPackageList(PackageId parent)
	{
		ArrayList<SearchDialogEntry> packages = new ArrayList<SearchDialogEntry>();
		for (PackageId packageId : getRepository().getSubPackageIds(parent))
		{
			packages.add(new SearchDialogPackageEntry(packageId, null, project, getRepository()));

			Collections.sort(packages, new Comparator<Object>()
			{
				public int compare(Object o1, Object o2)
				{
					return comparePackageEntries(o1, o2);
				}
			});
		}

		return packages;
	}

	private ArrayList<SearchDialogEntry> getModeList(PackageId parent,
			SearchDialogPackageEntry parentPackageEntry)
	{
		ArrayList<SearchDialogEntry> models = new ArrayList<SearchDialogEntry>();
		for (ModelId modelId : getRepositoryIndex().getAllModelIds())
		{
			if (modelId.getPackageId().equals(parentPackageEntry.getPackageId()))
				models.add(new SearchDialogModelEntry(modelId, parentPackageEntry, project,
						getRepository()));

			Collections.sort(models, new Comparator<Object>()
			{
				public int compare(Object o1, Object o2)
				{
					return compareModelEntries(o1, o2);
				}
			});
		}

		return models;
	}

	public Object getParent(Object element)
	{
		if (element instanceof SearchDialogPackageEntry)
			return ((SearchDialogPackageEntry) element).getParentPackageEntry();

		return null;
	}

	public boolean hasChildren(Object element)
	{
		if (element instanceof IProject)
		{
			if (!((getRepository().getSubPackageIds(null)).isEmpty()))
				return true;
		}
		else if (element instanceof SearchDialogPackageEntry)
		{
			SearchDialogPackageEntry parentPackageEntry = (SearchDialogPackageEntry) element;

			if (!(getRepository().getSubPackageIds(parentPackageEntry.getPackageId()).isEmpty()))
				return true;

			for (ModelId model : getRepositoryIndex().getAllModelIds())
			{
				if (model.getPackageId().equals(parentPackageEntry.getPackageId()))
					return true;
			}
		}
		return false;
	}

	public Object[] getElements(Object inputElement)
	{
		ArrayList<SearchDialogPackageEntry> elements = new ArrayList<SearchDialogPackageEntry>();

		if (inputElement instanceof IProject)
		{
			if (project == null)
				project = ((IProject) inputElement);

			for (PackageId packageId : getRepository().getSubPackageIds(null))
			{
				elements
						.add(new SearchDialogPackageEntry(packageId, null, project, getRepository()));
			}

			Collections.sort(elements, new Comparator<Object>()
			{
				public int compare(Object o1, Object o2)
				{
					return comparePackageEntries(o1, o2);
				}
			});

			return elements.toArray();
		}

		return null;
	}

	private int comparePackageEntries(Object o1, Object o2)
	{
		SearchDialogPackageEntry m1 = (SearchDialogPackageEntry) o1;
		SearchDialogPackageEntry m2 = (SearchDialogPackageEntry) o2;
		return m1.getPackageId().getName().compareTo(m2.getPackageId().getName());
	}

	private int compareModelEntries(Object o1, Object o2)
	{
		SearchDialogModelEntry m1 = (SearchDialogModelEntry) o1;
		SearchDialogModelEntry m2 = (SearchDialogModelEntry) o2;
		return m1.getModelId().getName().compareTo(m2.getModelId().getName());
	}

	public void dispose()
	{
		// Do Nothing
	}

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
	{
		if (newInput instanceof IProject)
			project = (IProject) newInput;
	}

	private WorkspaceRepository getRepository()
	{
		if (project != null)
		{
			RepositoryManager editDomain = RepositoryManager.getRepositoryManager(project);
			return editDomain.getRepository();
		}

		return null;
	}

	public void elementsChanged(Object[] updatedElements)
	{
		viewer.refresh();
	}

	private RepositoryIndex getRepositoryIndex()
	{
		if (repositoryIndex == null)
			repositoryIndex = getRepository().getUpdatedRepositoryIndex();

		return repositoryIndex;
	}
}

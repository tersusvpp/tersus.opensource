/************************************************************************************************
 * Copyright (c) 2003-2013 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import java.util.List;

import org.eclipse.gef.commands.Command;

import tersus.editor.TersusEditor;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.model.commands.AlignElementsCommand;

/**
 * @author Ofer Brandes
 */
public class VerticalAlignAction extends EditorSelectionAction
{

	public VerticalAlignAction(TersusEditor editor)
	{
		super(editor);
		setId(TersusActionConstants.VERTICAL_ALIGN_ACTION_ID);
		setText(TersusActionConstants.VERTICAL_ALIGN_ACTION_TEXT);
		setToolTipText(TersusActionConstants.VERTICAL_ALIGN_ACTION_TOOLTIP);
	}

	@SuppressWarnings("unchecked")
	public void run()
	{
		if (!calculateEnabled())
			return;
		List<?> selection = getSelectedModelObjects();
		ModelElement first = (ModelElement) selection.get(0);
		List<ModelElement> selectedElements = (List<ModelElement>) selection;
		Command command = new AlignElementsCommand(first.getParentModel(), selectedElements,getText());
		((TersusEditor) getWorkbenchPart()).getEditDomain().getCommandStack().execute(command);
	}

	protected boolean calculateEnabled()
	{
		List<ModelObject> sel = getSelectedModelObjects();
		return isEnabledForMultiElementModification(sel,2,true,true,false);
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.actions;

import org.eclipse.core.resources.IProject;
import org.eclipse.gef.commands.CompoundCommand;

import tersus.model.Model;
import tersus.model.PackageId;
import tersus.model.commands.DeleteModelAndPackageCommand;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 *  
 */
public class RemoveUnusedCommand extends CompoundCommand
{
	private static PackageId packageId = null;
    /**
     * @param repository
     * @param selectedProject
     */
    public RemoveUnusedCommand(WorkspaceRepository repository, IProject selectedProject)
    {
    	
    	Model[] unused = repository.getUnusedModels(null);

    	if (unused != null && unused.length > 0)
    	{
	    	for (int i = 0; i < unused.length; i++)
	    	{
	    		Model model = unused[i];
	    		add(new DeleteModelAndPackageCommand(model));
	    	}
    	}
    }
    
    public PackageId getUnusedPackageId()
    {
    	return packageId;
    }
}
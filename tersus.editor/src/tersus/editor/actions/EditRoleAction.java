/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.actions;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.requests.DirectEditRequest;
import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;

import tersus.editor.EditorMessages;
import tersus.editor.editparts.TersusDirectEditFeature;
import tersus.editor.editparts.TersusEditPart;
import tersus.editor.properties.ModelObjectWrapper;
import tersus.editor.wizards.RenameWizard;

/**
 * An action that initiates editing of the selected element's role.
 * 
 * Editing is initiate by "sending" a request with type <code>TersusRequestIds.EDIT_ROLE</code>
 * 
 * @author Youval Bronicki
 *
 */
public class EditRoleAction extends SelectionAction
{

	private DirectEditRequest request = new DirectEditRequest();
	

	public EditRoleAction(IEditorPart editor){
		super(editor);
		request.setDirectEditFeature(TersusDirectEditFeature.ROLE);
	}
	/* (non-Javadoc)
	 * @see org.eclipse.gef.ui.actions.EditorPartAction#calculateEnabled()
	 */
	protected boolean calculateEnabled()
	{
		if (getSelectedObjects().size() == 1
			&& (getSelectedObjects().get(0) instanceof TersusEditPart))
		{
			TersusEditPart part = (TersusEditPart)getSelectedObjects().get(0);
			return part.understandsRequest(request);
		}
		return false;
	}
	/* (non-Javadoc)
	 * Run this action (send an DirectEdit request to the selected edit part with feature = Role)
	 * @see org.eclipse.jface.action.IAction#run()
	 */
	public void run() {
		try {
			EditPart part = (EditPart)getSelectedObjects().get(0);
		    if (part instanceof TersusEditPart)
		    {
		        ModelObjectWrapper model = (ModelObjectWrapper) ((TersusEditPart)part).getModel();
		        
                RenameWizard wizard = new RenameWizard (model.getModelObject());
                WizardDialog dialog = new WizardDialog(getWorkbenchPart().getSite().getShell(), wizard);
                dialog.setMinimumPageSize(0,0);
                dialog.open();
		    }
//			part.performRequest(request);
		} catch (ClassCastException e) {
			Display.getCurrent().beep();
		} catch (IndexOutOfBoundsException e){
			Display.getCurrent().beep();
		}
	}


	protected void init() {
		super.init();
		setText(EditorMessages.ChangeRoleAction_Label);
		setToolTipText(EditorMessages.ChangeRoleAction_Tooltip);
		setId(TersusActionConstants.EDIT_ROLE);
	}



}

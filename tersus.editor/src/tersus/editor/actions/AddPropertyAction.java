/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.actions;

import java.util.List;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.views.properties.PropertySheet;

import tersus.editor.TersusEditor;
import tersus.editor.editparts.MissingModelEditPart;
import tersus.editor.editparts.TersusEditPart;
import tersus.editor.properties.TersusPropertySheetPage;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.commands.AddMetaPropertyCommand;
import tersus.workbench.TersusWorkbench;

/**
 * @author liat Shiff
 */
public class AddPropertyAction extends SelectionAction
{
	boolean canAddSharedProperty;
	boolean canAddLocalProperty;

	public AddPropertyAction(IEditorPart editor)
	{
		super(editor);

		setId(TersusActionConstants.ADD_PROPERTY_ACTION_ID);
		setText(TersusActionConstants.ADD_PROPERTY_ACTION_TEXT);
		setImageDescriptor(ImageDescriptor.createFromFile(TersusEditor.class,
				"icons/AddProperty.gif"));

		canAddSharedProperty = false;
		canAddLocalProperty = false;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected boolean calculateEnabled()
	{
		List<Object> selectedObjects = getSelectedObjects();

		if (selectedObjects.isEmpty() || selectedObjects.size() > 1)
			return false;

		Object selectedObject = selectedObjects.get(0);

		if (!(selectedObject instanceof TersusEditPart)
				|| (selectedObject instanceof MissingModelEditPart))
			return false;

		TersusEditPart selectedPart = (TersusEditPart) selectedObject;

		canAddSharedProperty = enableToAddSharedProperty(selectedPart);
		canAddLocalProperty = enableToAddLocalProperty(selectedPart);

		return canAddSharedProperty || canAddLocalProperty;
	}

	private boolean enableToAddSharedProperty(TersusEditPart selectedPart)
	{
		Model selectedModel = selectedPart.getTersusModel();

		if (selectedModel == null)
			return false;

		if (selectedModel.getPackage().isReadOnly())
			return false;

		return true;
	}

	private boolean enableToAddLocalProperty(TersusEditPart selectedPart)
	{
		ModelElement selectedModelElement = selectedPart.getModelElement();

		if (selectedModelElement == null)
			return false;

		Model parentModel = null;
		parentModel = selectedModelElement.getParentModel();

		if ((parentModel != null) && parentModel.isReadOnly())
			return false;

		return true;
	}

	@SuppressWarnings("unchecked")
	public void run()
	{
		InputDialog dialog = new InputDialog(TersusWorkbench.getActiveWorkbenchShell(),
				"Add Property", "Choose property Name", null, null);
		dialog.setBlockOnOpen(true);
		if (InputDialog.OK != dialog.open())
			return;

		String propertyName = dialog.getValue();
		List<Object> objects = getSelectedObjects();
		Object obj = objects.get(0);

		if (obj instanceof TersusEditPart)
		{
			TersusEditPart part = (TersusEditPart) obj;

			Command c = null;

			if (canAddSharedProperty)
			{
				Model model = part.getTersusModel();

				if (!model.isMetaPropertyExists(propertyName))
					c = new AddMetaPropertyCommand(model, propertyName, canAddSharedProperty,
							canAddLocalProperty);
				else
					MessageDialog.openError(null, "Add New Property", "The property name '"
							+ propertyName + "' already exists.");
			}
			else if (canAddLocalProperty)
				c = new AddMetaPropertyCommand(part.getModelElement(), propertyName,
						canAddSharedProperty, canAddLocalProperty);

			if (c != null && c.canExecute())
				getCommandStack().execute(c);

			IWorkbenchPage page = getWorkbenchPart().getSite().getPage();
			try
			{
				PropertySheet propertySheet = (PropertySheet) page
						.showView("org.eclipse.ui.views.PropertySheet");

				TersusPropertySheetPage propertySheetPage = (TersusPropertySheetPage) propertySheet
						.getCurrentPage();
				propertySheetPage.showProperty(propertyName);
			}
			catch (PartInitException e)
			{
				TersusWorkbench.log(e);
				return;
			}
			return;
		}
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gef.ui.palette.PaletteViewer;
import org.eclipse.jface.action.Action;

import tersus.editor.TersusEditor;

public class SetActiveToolAction extends Action
{
    private TersusEditor editor;

    private String toolKey;

    public SetActiveToolAction(TersusEditor editor, String toolKey)
    {
        this.editor = editor;
        this.toolKey = toolKey;
    }

    public void run()
    {

        PaletteViewer paletteViewer = editor.getCurrentPaletteViewer();
        ToolEntry tool = (ToolEntry) editor.getToolRegistry().get(toolKey);
        if (tool != null)
        {
            paletteViewer.setActiveTool(tool);
        }
    }

}

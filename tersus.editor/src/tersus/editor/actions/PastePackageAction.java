/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.actions;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.ui.IWorkbenchPart;

import tersus.editor.RepositoryManager;
import tersus.editor.explorer.PackageEntry;
import tersus.editor.explorer.ProjectEntry;
import tersus.model.Package;
import tersus.model.commands.PastePackageCommand;

/**
 * @author Liat Shiff
 */
public class PastePackageAction extends RepositoryAction
{

	public PastePackageAction(IWorkbenchPart part) 
	{
		super(part);
		setId(TersusActionConstants.PASTE_PACKAGE_ACTION_ID);
        setText(TersusActionConstants.PASTE_PACKAGE_ACTION_TEXT);
	}

	protected boolean calculateEnabled() 
	{
		Object selectedObject = getSelectedObject();
		
		if (selectedObject == null)
			return false;
		
		Object cc = getClipboardContents();
		
		if (cc == null)
			return false;
		
		if (!(cc instanceof ArrayList))
			return false;
		
		if (((ArrayList<?>)cc).size() == 0 || cc == null)
			return false;
		
		for (Object obj: (ArrayList<?>)cc)
		{
			if (!(obj instanceof PackageEntry))
				return false;
		}
		
		// We support in copy and paste of one Package.
		PackageEntry packageToPaste = (PackageEntry)((ArrayList<?>)cc).get(0);
			
		if (selectedObject instanceof PackageEntry)
		{
			PackageEntry packageEntry = (PackageEntry)selectedObject;
			Package pack = packageEntry.getRepository().getPackage(packageEntry.getPackageId());
			
			if (pack.isReadOnly())
				return false;
			
			if (packageEntry.getProject().equals(packageToPaste.getProject()))
				return true;
			else
				return false;
		}
		else if (selectedObject instanceof ProjectEntry)
		{
			ProjectEntry projectEntry = (ProjectEntry)selectedObject;
			if (projectEntry.getProject().equals(packageToPaste.getProject()))
				return true;
			else
				return false;
		}

		return false;
	}
	
	public void run()
	{
		PackageEntry entry = getClipboardObject();
		Object selectedObject = getSelectedObject();
		PastePackageCommand command = null;
		
		if (selectedObject instanceof PackageEntry)
		{
			PackageEntry packageEntry = (PackageEntry)selectedObject;
			command = new PastePackageCommand(entry.getRepository(), packageEntry, entry);
		}
		else if (selectedObject instanceof ProjectEntry)
		{
			ProjectEntry projectEntry = (ProjectEntry)selectedObject;
			command = new PastePackageCommand(entry.getRepository(), projectEntry, entry); 
		}
		
		if (command != null)
		{
			RepositoryManager editDomain = entry.getEditDomain();
			editDomain.getCommandStack().execute(command);
		}
	}
	
	protected PackageEntry getClipboardObject()
    {
        Object cc = getClipboardContents();
        PackageEntry entry = null;
        if (cc instanceof List<?>)
        {
        	if (((List<?>)cc).size() != 1)
        		return null;
        	entry = (PackageEntry)((List<?>)cc).get(0); 
        }
        else
        	entry = (PackageEntry)cc;
        return entry;
    }
}

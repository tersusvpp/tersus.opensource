/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;

import tersus.editor.ConcreteModelIdentifier;
import tersus.editor.TersusEditor;
import tersus.model.Model;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 *  
 */
public class OpenInANewTabAction extends EditorSelectionAction
{

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#calculateEnabled()
     */
    protected boolean calculateEnabled()
    {
        return getSelectedFlowModel() != null;
    }

    public OpenInANewTabAction(TersusEditor editor)
    {
        super(editor);
        setId(TersusActionConstants.OPEN_IN_A_NEW_TAB_ACTION_ID);
        setText(TersusActionConstants.OPEN_IN_A_NEW_TAB_ACTION_TEXT);
    }

    public void run()
    {

        Model model = getSelectedFlowModel();
        ConcreteModelIdentifier modelIdentifier = new ConcreteModelIdentifier(
                ((WorkspaceRepository) model.getRepository())
                        .getRepositoryRoot().getProject(), model.getId());
        IWorkbenchPage page = getWorkbenchPart().getSite().getPage();
        try
        {
            page.openEditor(modelIdentifier, TersusEditor.ID);
        }
        catch (PartInitException e)
        {
            TersusWorkbench.log(e);
            return;
        }
    }
}
/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import org.eclipse.jface.viewers.IColorProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import tersus.model.Package;

/**
 * @author Liat Shiff
 */
public class MoveModelLabelProvider extends LabelProvider implements IColorProvider
{

	public String getText(Object element)
	{
		if (element instanceof SearchDialogPackageEntry)
			return ((SearchDialogPackageEntry) element).getPackageId().getName();

		return super.getText(element);
	}

	@Override
	public Image getImage(Object element)
	{
		if (element instanceof SearchDialogPackageEntry)
			return ((SearchDialogPackageEntry) element).getImage();

		return null;
	}

	public Color getBackground(Object element)
	{
		return null;
	}

	public Color getForeground(Object element)
	{
		if (element instanceof SearchDialogPackageEntry)
		{
			SearchDialogPackageEntry packageEntry = (SearchDialogPackageEntry) element;
			Package pkg = packageEntry.getRepository().getPackage(packageEntry.getPackageId());

			if (pkg.isReadOnly())
				return Display.getCurrent().getSystemColor(SWT.COLOR_GRAY);
			else
				return Display.getCurrent().getSystemColor(SWT.COLOR_BLACK);
		}

		return null;
	}

}

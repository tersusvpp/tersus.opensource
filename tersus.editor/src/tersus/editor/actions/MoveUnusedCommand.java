/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.actions;

import java.util.HashSet;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;

import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.PackageId;
import tersus.model.commands.ChangeModelIdCommand;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Ofer Brandes
 * @author Youval Bronicki
 * 
 */
public class MoveUnusedCommand extends CompoundCommand
{
	private PackageId targetPackageId;

	public static final String UNUSED_MODELS_PACKAGE = "Unused";

	/**
	 * @param repository
	 * @param selectedProject
	 */
	public MoveUnusedCommand(WorkspaceRepository repository, IProject selectedProject,
			PackageId scopePackageId)
	{
		targetPackageId = repository.getNewSubPackageId(null, UNUSED_MODELS_PACKAGE);
		Model[] unused = repository.getUnusedModels(targetPackageId);
		HashSet<ModelId> externallyReferredModeIds = new HashSet<ModelId>();
		if (scopePackageId != null)
		{
			for (ModelElement reference: repository.getExternalReferences(scopePackageId))
			{
				if (!targetPackageId.isAncestorOf(reference.getParentModel().getId()))
					externallyReferredModeIds.add(reference.getRefId());
			}
		}

		if (unused != null && unused.length > 0)
		{
			for (int i = 0; i < unused.length; i++)
			{
				Model model = unused[i];
				if (scopePackageId != null)
				{
					if (!scopePackageId.isAncestorOf(model.getId()))
						continue; // Model is not in our scope;
					if (externallyReferredModeIds.contains(model.getId()))
						/*
						 * If we move a model that has external references, we won't be able to
						 * remove the "Unused Models" package
						 */
						continue;
				}
				ModelId newId = new ModelId(targetPackageId.getName() + "/" + model.getId());
				add(new ChangeModelIdCommand(model, newId));
				// System.out.println ("Renaming: "+model.getId()+" -->
				// "+newId);
			}
		}
	}

	public PackageId getUnusedPackageId()
	{
		return targetPackageId;
	}

	public void execute()
	{
		ProgressMonitorDialog dialog = null;
		try
		{
			dialog = new ProgressMonitorDialog(TersusWorkbench.getActiveWorkbenchShell());
			dialog.open();
			IProgressMonitor monitor = dialog.getProgressMonitor();
			List<?> moveCommands = getCommands();
			monitor.beginTask("Moving " + moveCommands.size() + " models", moveCommands.size());
			for (int i = 0; i < moveCommands.size(); i++)
			{
				ChangeModelIdCommand cmd = (ChangeModelIdCommand) moveCommands.get(i);
				cmd.execute();
				monitor.worked(1);
			}
		}
		finally
		{
			if (dialog != null)
				dialog.close();
		}

	}
}
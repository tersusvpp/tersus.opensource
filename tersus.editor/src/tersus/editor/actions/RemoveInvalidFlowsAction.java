/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.actions;

import java.util.HashSet;
import java.util.List;

import org.eclipse.gef.commands.CompoundCommand;

import tersus.editor.RepositoryManager;
import tersus.editor.TersusEditor;
import tersus.model.FlowModel;
import tersus.model.IRepository;
import tersus.model.Link;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.model.ModelUtils;
import tersus.model.commands.MultiStepCommand;
import tersus.workbench.WorkspaceRepository;

/**
 * @author liat Shiff
 */
public class RemoveInvalidFlowsAction extends EditorSelectionAction
{

	Model selectedModel;

	CompoundCommand removeElementCommands;

	public RemoveInvalidFlowsAction(TersusEditor editor)
	{
		super(editor);

		setId(TersusActionConstants.REMOVE_INVALID_FLOWS_ID);
		setText(TersusActionConstants.REMOVE_INVALID_FLOWS_TEXT);
	}

	@Override
	protected boolean calculateEnabled()
	{
		removeElementCommands = new CompoundCommand();
		selectedModel = null;

		List<ModelObject> sel = getSelectedModelObjects();

		if (sel.size() != 1)
			return false;

		if (sel.get(0) instanceof ModelElement)
		{
			try
			{
				selectedModel = ((ModelElement) sel.get(0)).getReferredModel();
				if (!(ModelUtils.isSubProcess(sel.get(0)) || ModelUtils.isDisplayElement(sel.get(0))))
					return false;
			}
			catch (Exception e)
			{
				return false;
			}
			
		}
		else if (sel.get(0) instanceof FlowModel)
			selectedModel = (Model) sel.get(0);
		else
			return false;

		if (selectedModel.getPackage().isReadOnly())
			return false;

		for (ModelElement element : selectedModel.getElements())
		{
			if (element instanceof Link && !((Link) element).isValid())
				return true;
		}

		return false;
	}

	public void run()
	{
		if (!calculateEnabled())
			return;

		MultiStepCommand cmd = new MultiStepCommand((WorkspaceRepository) selectedModel
				.getRepository())
		{
			@Override
			protected void run()
			{
				clean(selectedModel);
			}

			private void clean(Model selectedModel)
			{
				HashSet<Link> invalidLinks = new HashSet<Link>();
				for (ModelElement element : selectedModel.getElements())
				{
					if (element instanceof Link)
					{
						Link link = (Link) element;
						if (!link.isValid())
							invalidLinks.add(link);
					}
				}

				for (Link l : invalidLinks)
					removeElement(l, true);
			}
		};

		IRepository repository = selectedModel.getRepository();
		if (repository instanceof WorkspaceRepository)
		{
			WorkspaceRepository workspaceRepository = (WorkspaceRepository) repository;
			RepositoryManager.getRepositoryManager(workspaceRepository.getProject())
					.getCommandStack().execute(cmd);
		}
	}
}

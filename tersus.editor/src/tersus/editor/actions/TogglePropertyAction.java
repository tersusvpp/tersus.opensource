/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.actions;

import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;

import tersus.editor.TersusEditor;
import tersus.editor.editparts.TersusEditPart;
import tersus.model.MetaProperty;
import tersus.model.ModelObject;
import tersus.model.commands.MultiStepCommand;
import tersus.util.Misc;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * An abstract base-class for actions that toggle a boolean property of a model object
 * 
 * @author Youval Bronicki
 * 
 */
public abstract class TogglePropertyAction extends SetPropertyAction
{
	/**
	 * 
	 * Creates a new TogglePropertyAction
	 * 
	 * @param editor
	 *            The editor with which this action is associated.
	 * @param propertyName
	 *            The name of the property to be toggled.
	 * @param id
	 *            A unique identifier for the action.
	 * @param category
	 *            The category of the property: either <code>MODEL</code> or <code>ELEMENT</code>
	 */
	public TogglePropertyAction(TersusEditor editor, String id, String propertyName, String category)
	{
		super(editor);
		this.propertyName = propertyName;
		this.category = category;
		setId(id);
		setText(Misc.ucFirst(propertyName));
		setToolTipText("Toggle the value of property " + propertyName);
	}

	public void setChecked()
	{
		ModelObject target = getFirstSelectedObject();
		setChecked(false);
		if (target == null)
			return;
		boolean value = Boolean.TRUE.equals(getPropertyValue(target));
		setChecked(value);
	}
	
	public abstract Object getPropertyValue(ModelObject target);

	public void run()
	{
		final List<?> selectedObjects = getSelectedObjects();

		if (!selectedObjects.isEmpty() && selectedObjects.get(0) instanceof TersusEditPart)
		{
			ModelObject first = getTarget(selectedObjects.get(0));
			MultiStepCommand command = new MultiStepCommand((WorkspaceRepository) first
					.getRepository(), "Set " + propertyName)
			{
				@Override
				protected void run()
				{
					Boolean newValue = null;

					for (Object selectedObject : selectedObjects)
					{
						ModelObject target = getTarget(selectedObject);
						if (target == null)
							continue;
						if (newValue == null)
						{
							boolean currentValue = Boolean.TRUE.equals(getPropertyValue(target));
							newValue = Misc.getBoolean(!currentValue);
						}
						
						MetaProperty metaProperty = target.getMetaProperty(propertyName);
						if (metaProperty == null)
							addMetaProperty(target, propertyName);
						boolean useBoolean = metaProperty != null && Boolean.class.equals(metaProperty.valueClass);
						
						setProperty(target, propertyName, useBoolean ? newValue : String.valueOf(newValue));
					}
				}
			};

			try
			{
				((TersusEditor) getWorkbenchPart()).getEditDomain().getCommandStack().execute(
						command);
			}
			catch (Exception e)
			{
				StringBuffer message = new StringBuffer();
				message.append(e.getMessage());

				MessageDialog.openInformation(TersusWorkbench.getActiveWorkbenchShell(),
						"Read Only Package/File", message.toString());
			}

			firePropertyChange(CHECKED, null, null);
		}
	}

	protected void refresh()
	{
		super.refresh();
	}

	protected boolean calculateEnabled()
	{
		if (super.calculateEnabled())
		{
			setChecked();
			return true;
		}
		else
			return false;
	}

}

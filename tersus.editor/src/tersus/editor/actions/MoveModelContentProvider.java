/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

/**
 * @author Liat Shiff
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;

import tersus.editor.RepositoryManager;
import tersus.model.PackageId;
import tersus.workbench.WorkspaceRepository;

public class MoveModelContentProvider implements ITreeContentProvider
{
	private TreeViewer viewer;

	private IProject project;

	public MoveModelContentProvider(TreeViewer viewer)
	{
		this.viewer = viewer;
	}

	public Object[] getChildren(Object parentElement)
	{
		ArrayList<SearchDialogPackageEntry> elements = new ArrayList<SearchDialogPackageEntry>();

		// I think this condition will never occur
		if (parentElement instanceof IProject)
		{
			for (PackageId packageId : getRepository().getSubPackageIds(null))
			{
				elements
						.add(new SearchDialogPackageEntry(packageId, null, project, getRepository()));
			}

			return elements.toArray();
		}
		else if (parentElement instanceof SearchDialogPackageEntry)
		{
			SearchDialogPackageEntry packageEntry = (SearchDialogPackageEntry) parentElement;

			for (PackageId packageId : getRepository()
					.getSubPackageIds(packageEntry.getPackageId()))
			{
				elements.add(new SearchDialogPackageEntry(packageId, packageEntry, project,
						getRepository()));
			}

			Collections.sort(elements, new Comparator<Object>()
			{
				public int compare(Object o1, Object o2)
				{
					SearchDialogPackageEntry m1 = (SearchDialogPackageEntry) o1;
					SearchDialogPackageEntry m2 = (SearchDialogPackageEntry) o2;
					return m1.getPackageId().getName().compareTo(m2.getPackageId().getName());
				}
			});

			return elements.toArray();
		}
		return null;
	}

	public Object getParent(Object element)
	{
		if (element instanceof SearchDialogPackageEntry)
			return ((SearchDialogPackageEntry) element).getParentPackageEntry();

		return null;
	}

	public boolean hasChildren(Object element)
	{
		if (element instanceof IProject)
		{
			if (!((getRepository().getSubPackageIds(null)).isEmpty()))
				return true;
		}
		else if (element instanceof SearchDialogPackageEntry)
		{
			SearchDialogPackageEntry packageEntry = (SearchDialogPackageEntry) element;

			if (!(getRepository().getSubPackageIds(packageEntry.getPackageId()).isEmpty()))
				return true;
		}
		return false;
	}

	public Object[] getElements(Object inputElement)
	{
		ArrayList<SearchDialogPackageEntry> elements = new ArrayList<SearchDialogPackageEntry>();

		if (inputElement instanceof IProject)
		{
			if (project == null)
				project = ((IProject) inputElement);

			for (PackageId packageId : getRepository().getSubPackageIds(null))
			{
				elements
						.add(new SearchDialogPackageEntry(packageId, null, project, getRepository()));
			}

			Collections.sort(elements, new Comparator<Object>()
			{
				public int compare(Object o1, Object o2)
				{
					SearchDialogPackageEntry m1 = (SearchDialogPackageEntry) o1;
					SearchDialogPackageEntry m2 = (SearchDialogPackageEntry) o2;
					return m1.getPackageId().getName().compareTo(m2.getPackageId().getName());
				}
			});

			return elements.toArray();
		}
		return null;
	}

	public void dispose()
	{
		// Do Nothing
	}

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
	{
		if (newInput instanceof IProject)
			project = (IProject) newInput;
	}

	public void elementsChanged(Object[] updatedElements)
	{
		viewer.refresh();
	}

	private WorkspaceRepository getRepository()
	{
		if (project != null)
		{
			RepositoryManager editDomain = RepositoryManager.getRepositoryManager(project);
			return editDomain.getRepository();
		}

		return null;
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;

import tersus.editor.*;

/**
 * 
 */
public abstract class ZoomAction extends Action
{
	public static final double ZOOM_IN = 1.25;
	public static final double ZOOM_OUT = 0.8;
	
	private TersusZoomManager zoomManager;
	
	public ZoomAction(String text, ImageDescriptor image, TersusZoomManager zoomManager) 
	{
		super(text, image);
		this.zoomManager = zoomManager;
	}
	protected void zoomIn()
	{
		zoomManager.zoom(ZOOM_IN);
	}

	protected void zoomOut()
	{
		zoomManager.zoom(ZOOM_OUT);
	}
}

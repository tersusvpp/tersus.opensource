/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.MessageDialog;

import tersus.editor.TersusEditor;
import tersus.model.DataElement;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.ModelObject;
import tersus.model.Package;
import tersus.model.PackageId;
import tersus.model.SubFlow;
import tersus.model.commands.MoveModelCommand;

/**
 * @author Liat Shiff
 */
public class MoveModelsEditorAction extends EditorSelectionAction
{
	final private String MOVE_MODELS_ACTION_TEXT = "Move Models";
	final private String MOVE_MODEL_ACTION_TEXT = "Move Model";

	public MoveModelsEditorAction(TersusEditor editor)
	{
		super(editor);
		setId(TersusActionConstants.MOVE_MODELS_ACTION_ID);
		setText(TersusActionConstants.MOVE_MODELS_ACTION_TEXT);
		setToolTipText(TersusActionConstants.MOVE_MODELS_ACTION_TOOLTIP);
	}

	@Override
	protected boolean calculateEnabled()
	{
		List<ModelObject> selectedModelObjects = getSelectedModelObjects();

		if (selectedModelObjects.equals(Collections.EMPTY_LIST) || selectedModelObjects == null
				|| selectedModelObjects.isEmpty())
			return false;

		for (ModelObject obj : selectedModelObjects)
		{
			if (!(obj instanceof DataElement || obj instanceof SubFlow))
				return false;

			if (((ModelElement) obj).getReferredModel() == null)
				return false;
			else if (((ModelElement) obj).getReferredModel().isReadOnly())
				return false;
		}

		if (selectedModelObjects.size() > 1)
			setText(MOVE_MODELS_ACTION_TEXT);
		else
			setText(MOVE_MODEL_ACTION_TEXT);

		return true;
	}

	public void run()
	{
		List<ModelObject> selectedModelObjects = getSelectedModelObjects();
		ArrayList<ModelId> modelIds = new ArrayList<ModelId>();

		for (Object obj : selectedModelObjects)
		{
			modelIds.add(((ModelElement) obj).getModelId());
		}

		TersusEditor editor = (TersusEditor) getWorkbenchPart();
		IProject project = editor.getRepository().getProject();

		MoveModelDialog dialog = new MoveModelDialog(getShell(), project, editor.getRepository());
		dialog.open();

		String newPackageName = dialog.getNewPackageName();
		Object[] selectedColumns = dialog.getResult();

		if (selectedColumns == null || selectedColumns.length == 0)
		{
			// Do Nothing
		}
		else if (selectedColumns[0] instanceof PackageId)
		{
			PackageId packageId = (PackageId) selectedColumns[0];
			Package pkg = editor.getRepository().getPackage(packageId);

			if (pkg.isReadOnly())
				MessageDialog.openQuestion(getShell(), "Read Only Package",
						"Failed to move model to read only package.\n" + "packageId: " + packageId);

			MoveModelCommand command = new MoveModelCommand(editor.getRepository(), modelIds,
					(PackageId) selectedColumns[0], newPackageName);
			editor.getEditDomain().getCommandStack().execute(command);
		}
	}

}

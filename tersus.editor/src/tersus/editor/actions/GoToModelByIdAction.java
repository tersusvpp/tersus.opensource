/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;

import tersus.editor.ConcreteModelIdentifier;
import tersus.editor.TersusEditor;
import tersus.editor.explorer.ExplorerEntry;
import tersus.editor.explorer.ModelEntry;
import tersus.editor.explorer.PackageEntry;
import tersus.editor.explorer.ProjectEntry;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public class GoToModelByIdAction extends RepositoryAction
{
	public GoToModelByIdAction(IWorkbenchPart part)
	{
		super(part);
		setId(TersusActionConstants.GOTO_MODEL_BY_ID_ACTION_ID);
		setText(TersusActionConstants.GOTO_MODEL_BY_ID_ACTION_TEXT);
	}

	protected boolean calculateEnabled()
	{
		Object selectedObject = getSelectedObject();

		if (selectedObject instanceof PackageEntry || selectedObject instanceof ModelEntry
				|| selectedObject instanceof ProjectEntry)
			return true;

		return false;
	}

	public void run()
	{
		InputDialog dialog = new InputDialog(getShell(), "Goto Model by Id",
				"Enter full model id:", null, null);
		dialog.open();

		if (dialog.getReturnCode() == Window.OK)
		{
			String path = dialog.getValue();
			WorkspaceRepository repository = ((ExplorerEntry) getSelectedObject()).getRepository();

			Model model = null;
			try
			{
				model = repository.getModel(new ModelId(path), true);
			}
			catch (IllegalArgumentException e)
			{
				openErrorMessage("Invalid Model Id", "Model id: '" + path + "' is illegle.");
				return;
			}

			if (model != null)
			{

				ConcreteModelIdentifier modelIdentifier = new ConcreteModelIdentifier(
						((ExplorerEntry) getSelectedObject()).getProject(), model.getId());

				IWorkbenchPage page = getWorkbenchPart().getSite().getPage();
				try
				{
					page.openEditor(modelIdentifier, TersusEditor.ID);
				}
				catch (PartInitException e)
				{
					TersusWorkbench.log(e);
				}
			}
			else
			{
				openErrorMessage("Invalid Model Id", "Model id: '" + path + "' does not exist.");
			}
		}
	}

	private void openErrorMessage(String title, String message)
	{
		MessageDialog messageDialog = new MessageDialog(getShell(), title, null, message,
				MessageDialog.ERROR, new String[]
				{ IDialogConstants.OK_LABEL }, 0);
		messageDialog.open();
	}
}

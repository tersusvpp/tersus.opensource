/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.actions;

import tersus.editor.TersusEditor;
import tersus.editor.editparts.LinkEditPart;
import tersus.editor.editparts.MissingModelEditPart;
import tersus.editor.editparts.TersusEditPart;
import tersus.model.BuiltinProperties;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.model.Note;

/**
 * An action that toggles the 'repetitive' property.
 * 
 * @author Youval Bronicki
 *
 */
public class ToggleRepetitiveAction extends TogglePropertyAction
{

	/**
	 * @param editor The editor with which this action is associated
	 */
	public ToggleRepetitiveAction(TersusEditor editor)
	{
		super(editor, TersusActionConstants.TOGGLE_REPETITIVE, BuiltinProperties.REPETITIVE ,ELEMENT);
	}

	/* (non-Javadoc)
	 * @see tersus.editor.actions.TogglePropertyAction#calculateEnabled(tersus.editor.editparts.TersusEditPart)
	 */
	protected boolean calculateEnabled(TersusEditPart selectedPart)
	{
		if (selectedPart instanceof MissingModelEditPart)
			return false;
		
	    ModelElement element = selectedPart.getModelElement();
	    
		if (element == null)
			return false;
		
		if (element instanceof Note)
            return false;
		
		if (element.getParentModel().getPackage().isReadOnly())
            return false;
        
		if (selectedPart instanceof LinkEditPart)
		    return false;
		
		if (!selectedPart.elementIsModifiable())
		    return false;
		
		return true;
	}

	public Object getPropertyValue(ModelObject target)
	{
		return target.getProperty(propertyName);
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;

import tersus.editor.TersusEditor;
import tersus.editor.usages.Usages;
import tersus.editor.usages.UsagesView;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.workbench.TersusWorkbench;

/**
 * @author Youval Bronicki
 *  
 */
public class ShowUsagesAction extends RepositoryAction
{
    public ShowUsagesAction(IWorkbenchPart part)
    {
        super(part);
        setId(TersusActionConstants.SHOW_USAGES_ACTION_ID);
        setText(TersusActionConstants.SHOW_USAGES_ACTION_TEXT);
        setImageDescriptor(ImageDescriptor.createFromFile(TersusEditor.class, "icons/usages.gif")); 
    }
    
    protected boolean calculateEnabled()
    {
        return getRelevantModel() != null;
    }

    public void run()
    {
        final Model model = getRelevantModel();
        final IWorkbenchPage page = getWorkbenchPart().getSite().getPage();
        Job job = new Job("Show usages")
        {
            protected IStatus run(final IProgressMonitor monitor)
            {
                Display.getDefault().asyncExec(new Runnable(){

                    final Usages usages = new Usages(model, monitor);
                    public void run()
                    {
                        try
                        {
                            UsagesView view = (UsagesView)page.showView(UsagesView.VIEW_ID);
                            TableViewer viewer = view.getViewer();
                            viewer.setInput(usages);
                            viewer.refresh();
                        }
                        catch (PartInitException e)
                        {
                            TersusWorkbench.log(e);
                            return;
                        }
                        
                    }});
                return Status.OK_STATUS;
                
            }};
            job.setPriority(Job.LONG);
            job.setUser(true);
            job.schedule();
    }

    private Model getRelevantModel()
    {
    	ModelObject modelObject = getModelObject();
    	
		if (modelObject instanceof Model)
    		return (Model)modelObject;
    	else if(modelObject instanceof ModelElement)
			return ((ModelElement)modelObject).getReferredModel();

		return null;
    }
}
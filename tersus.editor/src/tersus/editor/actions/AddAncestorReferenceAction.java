/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.actions;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.eclipse.gef.EditPart;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuCreator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import tersus.editor.TersusEditor;
import tersus.editor.editparts.FlowEditPart;
import tersus.editor.editparts.TersusEditPart;
import tersus.model.BuiltinProperties;
import tersus.model.FlowModel;
import tersus.model.FlowType;
import tersus.model.Model;
import tersus.model.commands.AddAncestorReferenceCommand;
import tersus.workbench.ImageLocator;
/**
 * @author Youval Bronicki
 *
 */
public class AddAncestorReferenceAction extends EditorSelectionAction implements IMenuCreator
{
	/**
	 * @param part
	 */
	private Menu menu;
	public AddAncestorReferenceAction(TersusEditor editor)
	{
		super(editor);
		setMenuCreator(this);
		setId(TersusActionConstants.ADD_ANCESTOR_REFERENCE_ACTION_ID);
		setText(TersusActionConstants.ADD_ANCESTOR_REFERENCE_ACTION_TEXT);
	}
	/* (non-Javadoc)
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#calculateEnabled()
	 */
	protected boolean calculateEnabled()
	{
	    Model selectedModel = getSelectedModel();
        
        if (selectedModel != null && selectedModel.getPackage().isReadOnly())
        {
            return false;
        }
        
		return (getRelevantAncestors().size() > 0);
	}
	/**
	 * 
	 */
	private void getFlowModel()
	{
	}
	public Menu getMenu(Menu parent)
	{
		if (menu == null || menu.isDisposed())
			menu = new Menu(parent);
		populateMenu();
		return menu;
	}
	private void populateMenu()
	{
		for (Iterator i = getRelevantAncestors().iterator(); i.hasNext();)
		{
			FlowModel ancestor = (FlowModel) i.next();
			AddSpecificAncestorAction action = new AddSpecificAncestorAction(ancestor);
			addActionToMenu(menu, action);
		}
	}
	private List getRelevantAncestors()
	{
		FlowEditPart part = getSelectedFlowEditPart();
		if (part == null)
			return Collections.EMPTY_LIST;
		List ancestors = new ArrayList();
		EditPart parent = part.getParent();
		while (parent instanceof TersusEditPart)
		{
			Model model = ((TersusEditPart) parent).getTersusModel();
			if (model instanceof FlowModel && model.getProperty(BuiltinProperties.TYPE) == FlowType.DISPLAY)
			{
				ancestors.add(model);
			}
			parent = parent.getParent();
		}
		return ancestors;
	}
	/**
	 * Helper method that wraps the given action in an ActionContributionItem and then adds it
	 * to the given menu.
	 * 
	 * @param	parent	The menu to which the given action is to be added
	 * @param	action	The action that is to be added to the given menu
	 */
	protected void addActionToMenu(Menu parent, IAction action)
	{
		ActionContributionItem item = new ActionContributionItem(action);
		item.fill(parent, -1);
	}
	private class AddSpecificAncestorAction extends Action
	{
		private FlowModel referredModel;
		/**
		 * @param text
		 */
		public AddSpecificAncestorAction(FlowModel model)
		{
			super(model.getName());
			setReferredModel(model);
			TersusEditor editor = (TersusEditor) getWorkbenchPart();
			ImageLocator imageLocator = editor.getImageLocator(model);
			if (imageLocator != null)
			{
				ImageDescriptor image = imageLocator.getImageDescriptor(0, 0);
				setImageDescriptor(image);
			}
		}
		public void run()
		{
			FlowEditPart targetEditPart = getSelectedFlowEditPart();
			FlowModel targetModel = targetEditPart.getFlowModel();
			AddAncestorReferenceCommand command = new AddAncestorReferenceCommand(targetModel, getReferredModel());
			((TersusEditor) getWorkbenchPart()).getEditDomain().getCommandStack().execute(command);

		}
		/**
		 * @return
		 */
		public FlowModel getReferredModel()
		{
			return referredModel;
		}
		/**
		 * @param model
		 */
		public void setReferredModel(FlowModel model)
		{
			this.referredModel = model;
		}
	}
	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.IMenuCreator#getMenu(org.eclipse.swt.widgets.Control)
	 */
	public Menu getMenu(Control parent)
	{
		return null;
	}
	/* (non-Javadoc)
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#update()
	 */
	public void update()
	{
		super.update();
		if (menu != null)
		{
			if (menu.isDisposed())
				menu = null;
			else
			{
				MenuItem[] items = menu.getItems();
				for (int i = 0; i < items.length; i++)
				{
					MenuItem item = items[i];
					item.dispose();
				}
				populateMenu();
			}
		}
	}
	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.IAction#run()
	 */
}

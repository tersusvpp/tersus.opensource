/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.ui.IWorkbenchPart;

import tersus.editor.explorer.ExplorerPart;
import tersus.editor.explorer.ModelEntry;
import tersus.editor.explorer.PackageEntry;
import tersus.editor.outline.ModelObjectTransfer;
import tersus.model.Model;
import tersus.model.ModelObject;

/**
 * 
 * @author Liat Shiff
 */
public class CopyEntryFromExplorerAction extends RepositoryAction
{
	public CopyEntryFromExplorerAction(IWorkbenchPart part) 
	{
		super(part);
		setId(TersusActionConstants.COPY_ENTRY_FROM_EXPLORER_ACTION_ID);
        setText(TersusActionConstants.COPY_ENTRY_FROM_EXPLORER_ACTION_TEXT);
	}

	@Override
	protected boolean calculateEnabled() 
	{
		return isAllModelObjectsCopy() || isAllPackagesCopy();
	}

	private boolean isAllModelObjectsCopy()
	{
		IWorkbenchPart part = getWorkbenchPart();
		
		if (part instanceof ExplorerPart)
		{
			for (Object obj: ((ExplorerPart)part).getSelectedEntries())
			{
				if (!(obj instanceof ModelEntry))
					return false;
			}
			return true;
		}
		
		return false;
	}
	
	private boolean isAllPackagesCopy()
	{
		IWorkbenchPart part = getWorkbenchPart();
		if (part instanceof ExplorerPart)
		{
			for (Object obj: ((ExplorerPart)part).getSelectedEntries())
			{
				if (!(obj instanceof PackageEntry))
					return false;
			}
			return true;
		}
		return false;
	}
	
	public void run()
	{
		IWorkbenchPart part = getWorkbenchPart();
		
		
		if (isAllModelObjectsCopy() && part instanceof ExplorerPart) 
		{
			List<ModelObject> models = new ArrayList<ModelObject>();
			
			for (Object obj: ((ExplorerPart)part).getSelectedEntries())
			{
				if (obj instanceof ModelEntry)
				{
					ModelEntry entry = (ModelEntry) obj;
					Model model = entry.getRepository().getModel(entry.getModelId(), true);
					models.add(model);
				}
			}
			Clipboard clipboard = new Clipboard( ((ExplorerPart)part).getSite().getShell().getDisplay());
			ModelObjectTransfer transfer = ModelObjectTransfer.getInstance();
			transfer.setObject(models);
			clipboard.setContents(new Object[] { models },new Transfer[] { transfer });
			clipboard.dispose();
		}
		else if (isAllPackagesCopy() && part instanceof ExplorerPart)
		{
			List<PackageEntry> packages = new ArrayList<PackageEntry>();
			
			for (Object obj: ((ExplorerPart)part).getSelectedEntries())
			{
				if (obj instanceof PackageEntry)
				{
					PackageEntry entry = (PackageEntry) obj;
					packages.add(entry);
				}
			}
			Clipboard clipboard = new Clipboard( ((ExplorerPart)part).getSite().getShell().getDisplay());
			ModelObjectTransfer transfer = ModelObjectTransfer.getInstance();
			transfer.setObject(packages);
			clipboard.setContents(new Object[] { packages },new Transfer[] { transfer });
			clipboard.dispose();
		}
		
	}
}

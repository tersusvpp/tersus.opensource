/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import java.util.ArrayList;

import org.eclipse.core.resources.IProject;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.FilteredTree;
import org.eclipse.ui.dialogs.PatternFilter;
import org.eclipse.ui.dialogs.SelectionDialog;

import tersus.model.Package;
import tersus.model.PackageId;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public class MoveModelDialog extends SelectionDialog
{
	private Composite composite;

	private IProject project;

	private PackageId selectedPackageId;

	WorkspaceRepository repository;

	private TreeViewer viewer;

	private Button encapsulate;

	private Text newPackageNameTextInput;

	private String newPackageName;

	private Label errorLabel;

	public MoveModelDialog(Shell parentShell, IProject project, WorkspaceRepository repository)
	{
		super(parentShell);

		this.project = project;
		this.newPackageName = null;
		this.repository = repository;

		this.setTitle("Move Models");
	}

	protected Control createDialogArea(Composite parent)
	{
		composite = (Composite) super.createDialogArea(parent);
		initializeDialogUnits(composite);

		PatternFilter filter = new PatternFilter();
		int styleBits = SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER;
		FilteredTree filteredTree = new FilteredTree(composite, styleBits, filter);
		viewer = filteredTree.getViewer();

		viewer.setContentProvider(new MoveModelContentProvider(viewer));
		viewer.setLabelProvider(new MoveModelLabelProvider());
		viewer.setInput(project);
		
		viewer.addSelectionChangedListener(new ISelectionChangedListener()
		{
			public void selectionChanged(SelectionChangedEvent event)
			{
				Object obj = event.getSelection();

				if (obj instanceof TreeSelection)
				{
					TreeSelection treeSelection = (TreeSelection) obj;
					Object selectedObject = treeSelection.getFirstElement();

					if (treeSelection.size() == 1
							&& selectedObject instanceof SearchDialogPackageEntry)
					{
						PackageId packageId = ((SearchDialogPackageEntry) selectedObject)
								.getPackageId();
						WorkspaceRepository repository = ((SearchDialogPackageEntry) selectedObject)
								.getRepository();
						Package pkg = repository.getPackage(packageId);

						if (pkg.isReadOnly())
						{
							errorLabel.setText("'" + packageId.getName()
									+ "' is read only package.");
							errorLabel.setVisible(true);
							errorLabel.setForeground(ColorConstants.red);
							getOkButton().setEnabled(false);
						}
						else
						{
							selectedPackageId = packageId;
							errorLabel.setVisible(false);
							getOkButton().setEnabled(true);
						}
					}
					else
					{
						errorLabel.setText("You need to select parent package.");
						errorLabel.setVisible(true);
						errorLabel.setForeground(ColorConstants.blue);
						getOkButton().setEnabled(false);
					}
				}
			}
		});

		viewer.addDoubleClickListener(new IDoubleClickListener()
		{
			public void doubleClick(DoubleClickEvent event)
			{
				Object obj = event.getSelection();
				if (obj instanceof TreeSelection)
				{
					TreeSelection treeSelection = (TreeSelection) obj;
					Object selectedObject = treeSelection.getFirstElement();

					if (treeSelection.size() == 1
							&& selectedObject instanceof SearchDialogPackageEntry)
					{
						PackageId packageId = ((SearchDialogPackageEntry) selectedObject)
								.getPackageId();
						WorkspaceRepository repository = ((SearchDialogPackageEntry) selectedObject)
								.getRepository();
						Package pkg = repository.getPackage(packageId);

						if (pkg.isReadOnly())
							getOkButton().setEnabled(false);
						else
							okPressed();
					}
					else
						getOkButton().setEnabled(false);
				}
			}
		});
		
		addEncapsulate();
		addErorrLabel();
		
		return composite;
	}

	private void addEncapsulate()
	{
		encapsulate = new Button(composite, SWT.CHECK);
		encapsulate.setText("Move models into new package:");
		encapsulate.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true,
				GridData.HORIZONTAL_ALIGN_END, GridData.VERTICAL_ALIGN_CENTER));

		encapsulate.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				if (encapsulate.getSelection())
					newPackageNameTextInput.setEnabled(true);
				else
					newPackageNameTextInput.setEnabled(false);

			}
		});
		encapsulate.setSelection(false);

		newPackageNameTextInput = new Text(composite, SWT.BORDER | SWT.SINGLE);
		newPackageNameTextInput.setLayoutData(new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_CENTER));
		newPackageNameTextInput.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				if (selectedPackageId != null && selectedPackageId instanceof PackageId)
				{
					String newPackageNameTemp = newPackageNameTextInput.getText().toLowerCase();

					for (PackageId packageId : repository.getSubPackageIds(selectedPackageId))
					{
						if (packageId.getName().toLowerCase().equals(newPackageNameTemp)
								&& getOkButton() != null)
						{
							getOkButton().setEnabled(false);
							return;
						}
					}
					getOkButton().setEnabled(true);
				}
				else
					getOkButton().setEnabled(false);
			}
		});
		newPackageNameTextInput.setEnabled(false);
	}

	private void addErorrLabel()
	{
		errorLabel = new Label(composite, SWT.WRAP);
		GridData gd = new GridData(GridData.VERTICAL_ALIGN_BEGINNING);
		gd.horizontalSpan = 3;
		errorLabel.setLayoutData(gd);
		errorLabel.setText("You need to select parent package.");
		errorLabel.setForeground(ColorConstants.blue);
	}

	protected void createButtonsForButtonBar(Composite parent)
	{
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
		getOkButton().setEnabled(false);
	}

	protected void okPressed()
	{
		ArrayList<PackageId> resultSet = new ArrayList<PackageId>();
		resultSet.add(selectedPackageId);
		setResult(resultSet);

		String packageName = newPackageNameTextInput.getText();
		if (encapsulate.getSelection() && packageName != null && packageName.length() != 0)
			newPackageName = packageName;

		super.okPressed();
	}

	public String getNewPackageName()
	{
		return newPackageName;
	}

	protected void configureShell(Shell shell) 
	{
		super.configureShell(shell);
		
		Composite comp = shell.getParent();
		int width = comp.getBounds().width;
		int height = comp.getBounds().height;
		
		shell.setBounds(width/2 - 250, height/2 - 225, 500, 450);
	}
}

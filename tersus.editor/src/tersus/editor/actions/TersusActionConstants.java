/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.actions;

public abstract class TersusActionConstants
{

	public static final String SHOW_IN_REPOSITORY_ACTION_ID = "tersus.actions.show_in_repository";
	public static final String SHOW_IN_REPOSITORY_ACTION_TEXT = "Show in Repository Explorer";

	public static final String SHOW_USAGES_ACTION_ID = "tersus.actions.show_usages";
	public static final String SHOW_USAGES_ACTION_TEXT = "Show Usages";

	public static final String SHOW_SHOW_LINKS_ID = "tersus.actions.show_links";
	public static final String SHOW_SHOW_LINKS_TEXT = "Show Links";

	public static final String OPEN_ACTION_TEXT = "Open";

	public static final String OPEN_IN_A_NEW_TAB_ACTION_ID = "tersus.actions.open_in_a_new_tab";
	public static final String OPEN_IN_A_NEW_TAB_ACTION_TEXT = "Open in a New Tab";

	public static final String SET_SLOT_DATA_TYPES = "tersus.actions.set_slot_data_types";

	public static final String SET_FLOW_DATA_ELEMENT_TYPE = "tersus.actions.set_flow_data_element_type";

	public static final String SET_MAIN = "tersus.actions.set_main";

	public static final String SET_PARENT = "tersus.actions.set_parent";

	public static final String SET_INTERMEDIATE = "tersus.actions.set_intermediate";

	public static final String TOGGLE_MANDATORY = "tersus.actions.toggle_mandatory";

	public static final String SAVE_ACTION_ID = "tersus.actions.save";

	public static final String SAVE_ACTION_TEXT = "Save Project";

	public static final String TOGGLE_REPETITIVE = "tersus.actions.toggle_repetitive";

	public static final String TOGGLE_ALWAYS_CREATE = "tersus.actions.toggle_always_create";

	public static final String OPEN_ACTION_ID = "tersus.actions.open";

	public static final String RENAME_ACTION_ID = "tersus.actions.rename";
	public static final String RENAME_ACTION_TEXT = "Rename";

	public static final String DELETE_ACTION_ID = "tersus.actions.delete";
	public static final String DELETE_ACTION_TEXT = "Delete";

	public static final String REMOVE_ELEMENT_ACTION_ID = "tersus.actions.remove_element";
	public static final String REMOVE_ELEMENT_ACTION_TEXT = "Remove Element";

	public static final String TEMP_APPLY_TABLE_WIZARD_ID = "tersus.actions.applyTableWizard";
	public static final String TEMP_APPLY_TABLE_WIZARD_TEXT = "Apply Table Wizard";

	public static final String IMPORT_STRUCTURE_FROM_DB_ID = "tersus.actions.importStructureFromDB";
	public static final String IMPORT_STRUCTURE_FROM_DB_TEXT = "Import Structure from Database";

	public static String ADD_SHARED_PROPERTY_ACTION_ID = "tersus.actions.add_shared_property";
	public static String ADD_SHARED_PROPERTY_ACTION_TEXT = "Add Shared Property";

	public static String ADD_LOCAL_PROPERTY_ACTION_ID = "tersus.actions.add_local_property";
	public static String ADD_LOCAL_PROPERTY_ACTION_TEXT = "Add Local Property";

	public static final String ADD_PROPERTY_ACTION_ID = "tersus.actions.addPtoperty";
	public static final String ADD_PROPERTY_ACTION_TEXT = "Add Property";
	public static final String ADD_PROPERTY_ACTION_TOOLTIP = "Add Property";

	public static String ADD_ANCESTOR_REFERENCE_ACTION_ID = "tersus.actions.add_ancestor_reference";
	public static String ADD_ANCESTOR_REFERENCE_ACTION_TEXT = "Add Ancestor Reference";

	public static final String EDIT_NOTE_ID = "tersus.actions.edit_note";

	public static final String EDIT_NOTE_TEXT = "Edit";

	public static final String EDIT_ROLE = "tersus.actions.edit_role";

	public static final String TOGGLE_ELEMENT_TYPE = "tersus.actions.toggle_element_type";
	public static final String TOGGLE_ELEMENT_TEXT = "Type";

	public static final String PASTE_CLONE_ACTION_ID = "tersus.actions.paste_copy";
	public static final String PASTE_CLONE_ACTION_TEXT = "Paste (Clone)";

	public static final String COPY_ACTION_ID = "tersus.actions.copy";
	public static final String COPY_ACTION_TEXT = "Copy";

	public static final String COPY_ELEMENT_NAME_ACTION_ID = "tersus.actions.copy_element_name";
	public static final String COPY_ELEMENT_NAME_ACTION_TEXT = "Copy Element Name";
	
	public static final String COPY_ENTRY_FROM_EXPLORER_ACTION_ID = "tersus.actions.copy_entry_from_explorer";
	public static final String COPY_ENTRY_FROM_EXPLORER_ACTION_TEXT = "Copy";

	public static final String PASTE_ELEMENTS_ACTION_ID = "tersus.actions.paste_references";
	public static final String PASTE_ELEMENTS_ACTION_TEXT = "Paste (Reuse)";
	public static final String PASTE_ELEMENTS_ACTION_TOOLTIP = "Paste elements (reuse existing models)";

	public static final String HORIZONTAL_ALIGN_ACTION_ID = "tersus.actions.horizontal_align";
	public static final String HORIZONTAL_ALIGN_ACTION_TEXT = "Horizontal Alignment";
	public static final String HORIZONTAL_ALIGN_ACTION_TOOLTIP = "Horizontally align selected elements";

	public static final String VERTICAL_ALIGN_ACTION_ID = "tersus.actions.vartical_align";
	public static final String VERTICAL_ALIGN_ACTION_TEXT = "Vertical Alignment";
	public static final String VERTICAL_ALIGN_ACTION_TOOLTIP = "Vertically align selected elements";

	public static final String SIZE_ALIGN_ACTION_ID = "tersus.actions.size_align";
	public static final String SIZE_ALIGN_ACTION_TEXT = "Size Alignment";
	public static final String SIZE_ALIGN_ACTION_TOOLTIP = "Align sizes of selected elements";

	public static final String ENCAPSULATE_ACTION_ID = "tersus.actions.encapsulate";
	public static final String ENCAPSULATE_ACTION_TEXT = "Encapsulate";
	public static final String ENCAPSULATE_ACTION_TOOLTIP = "Moves selected elements into an intermediate level";

	public static final String DECAPSULATE_ACTION_ID = "tersus.actions.decapsulate";
	public static final String DECAPSULATE_ACTION_TEXT = "Decapsulate";
	public static final String DECAPSULATE_ACTION_TOOLTIP = "Moves the selected element's children into upper level";

	public static final String REMOVE_INVALID_FLOWS_ID = "tersus.actions.remove_invalid_flows";
	public static final String REMOVE_INVALID_FLOWS_TEXT = "Remove Invalid Flows";

	public static final String ANALYZE_DEPENDENCIES_ACTION_ID = "tersus.actions.analyze_dependencies";
	public static final String ANALYZE_DEPENDENCIES_ACTION_TEXT = "Analyze Dependencies";

	public static final String MOVE_ELEMENT_ACTION_ID = "tersus.actions.move";
	public static final String MOVE_ELEMENT_ACTION_TEXT = "Move Element";

	public static String ADD_ELEMENT_ACTION_ID = "tersus.actions.add_element";
	public static String ADD_ELEMENT_ACTION_TEXT = "Add Element";

	public static final String RESET_PROPERTIES_VIEW_ACTION_ID = "tersus.actions.reset_properties_view";
	public static final String RESET_PROPERTIES_VIEW_ACTION_TEXT = "Reset View";

	public static final String DELETE_PROPERTY_ACTION_ID = "tersus.actions.delete_property";
	public static final String DELETE_PROPERTY_VIEW_ACTION_TEXT = "Delete Property";

	public static final String PASTE_PACKAGE_ACTION_ID = "tersus.actions.paste_package";
	public static final String PASTE_PACKAGE_ACTION_TEXT = "Paste Package";

	public static final String MOVE_MODELS_ACTION_ID = "tersus.actions.move_model";
	public static final String MOVE_MODELS_ACTION_TEXT = "Move Model";
	public static final String MOVE_MODELS_ACTION_TOOLTIP = "Moves selected models into new package";

	public static final String REPLACE_WITH_ACTION_ID = "tersus.actions.replace_with";
	public static final String REPLACE_WITH_ACTION_TEXT = "Replace with...";
	public static final String REPLACE_WITH_ACTION_TOOLTIP = "Replace refId with other one from the repository explorer";
	
	public static final String CHANGE_PLUGIN_ACTION_ID = "tersus.actions.change_plugin";
	public static final String CHANGE_PLUGIN_ACTION_TEXT = "Change Plugin";

	public static final String HELP_ACTION_ID = "tersus.actions.help";
	public static final String HELP_ACTION_TEXT = "Help";

	public static final String SHOW_IN_NAVIGATOR_ACTION_ID = "tersus.actions.show_in_navigator";
	public static final String SHOW_IN_NAVIGATOR_ACTION_TEXT = "Show in Navigator View";

	public static final String GOTO_MODEL_BY_ID_ACTION_ID = "tersus.actions.goto_model_by_id";
	public static final String GOTO_MODEL_BY_ID_ACTION_TEXT = "Goto Model";

	public static final String SHOW_PREVIOUS_MODEL_MATCH_ACTION_ID = "tersus.actions.show_previous_model_match";
	public static final String SHOW_PREVIOUS_MODEL_MATCH_ACTION_TEXT = "Show the previous row which match to the selected model";

	public static final String SHOW_NEXT_MODEL_MATCH_ACTION_ID = "tersus.actions.show_next_model_match";
	public static final String SHOW_NEXT_MODEL_MATCH_ACTION_TEXT = "Show the next row which match to the selected model";
}

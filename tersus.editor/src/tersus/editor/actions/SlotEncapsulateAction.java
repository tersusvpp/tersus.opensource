/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import java.util.List;

import tersus.editor.TersusEditor;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.model.Slot;

/**
 * 
 * @author Liat Shiff
 */
public class SlotEncapsulateAction extends EditorSelectionAction
{
	public SlotEncapsulateAction(TersusEditor editor)
	{
		super(editor);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean calculateEnabled()
	{
		Model parentModel = null;

		List<ModelObject> sel = getSelectedModelObjects();
		if (sel.isEmpty())
			return false;

		for (ModelObject e : sel)
		{
			if (e instanceof ModelElement)
			{
				if (parentModel == null)
					parentModel = ((ModelElement) e).getParentModel();
				else if (parentModel != ((ModelElement) e).getParentModel())
					return false;

				if (!(e instanceof Slot))
					return false;
			}
		}

		return true;
	}

}

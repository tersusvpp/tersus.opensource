/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import org.eclipse.jface.viewers.IColorProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import tersus.model.DataElement;
import tersus.model.DataType;
import tersus.model.FlowModel;
import tersus.model.FlowType;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.ModelUtils;
import tersus.model.Slot;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public class ReplaceWithLabelProvider extends LabelProvider implements IColorProvider
{
	private ModelElement selectedElement;

	public ReplaceWithLabelProvider(ModelElement selectedElement)
	{
		super();
		this.selectedElement = selectedElement;
	}

	public String getText(Object element)
	{
		if (element instanceof SearchDialogPackageEntry)
			return ((SearchDialogPackageEntry) element).getPackageId().getName();
		else if (element instanceof SearchDialogModelEntry)
			return ((SearchDialogModelEntry) element).getModelId().getName();

		return super.getText(element);
	}

	@Override
	public Image getImage(Object element)
	{
		if (element instanceof SearchDialogPackageEntry)
			return ((SearchDialogPackageEntry) element).getImage();
		if (element instanceof SearchDialogModelEntry)
			return ((SearchDialogModelEntry) element).getImage();

		return null;
	}

	public Color getBackground(Object element)
	{
		return null;
	}

	public Color getForeground(Object element)
	{
		if (element instanceof SearchDialogModelEntry)
		{
			ModelId modelId = ((SearchDialogModelEntry) element).getModelId();
			Model model = ((WorkspaceRepository) selectedElement.getRepository()).getModel(modelId,
					true);

			if (selectedElement instanceof Slot || selectedElement instanceof DataElement)
			{
				if (model instanceof DataType)
					return Display.getCurrent().getSystemColor(SWT.COLOR_BLACK);
				else
					return Display.getCurrent().getSystemColor(SWT.COLOR_GRAY);
			}
			if (ModelUtils.isSubProcess(selectedElement))
			{
				if (model instanceof FlowModel
						&& (FlowType.ACTION.equals(((FlowModel) model).getType())
								|| FlowType.SERVICE.equals(((FlowModel) model).getType()) || FlowType.OPERATION
								.equals(((FlowModel) model).getType())))
					return Display.getCurrent().getSystemColor(SWT.COLOR_BLACK);
				else
					return Display.getCurrent().getSystemColor(SWT.COLOR_GRAY);
			}
			if (ModelUtils.isDisplayElement(selectedElement))
			{
				if (ModelUtils.isDisplayModel(model))
					return Display.getCurrent().getSystemColor(SWT.COLOR_BLACK);
				else
					return Display.getCurrent().getSystemColor(SWT.COLOR_GRAY);
			}
		}
		return null;
	}
}

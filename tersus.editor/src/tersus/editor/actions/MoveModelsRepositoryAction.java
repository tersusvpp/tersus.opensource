/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.IWorkbenchPart;

import tersus.editor.RepositoryManager;
import tersus.editor.explorer.ModelEntry;
import tersus.editor.explorer.PackageEntry;
import tersus.editor.explorer.ProjectEntry;
import tersus.editor.explorer.RootEntry;
import tersus.model.ModelId;
import tersus.model.Package;
import tersus.model.PackageId;
import tersus.model.commands.MoveModelCommand;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public class MoveModelsRepositoryAction extends RepositoryAction
{
	final private String MOVE_MODELS_ACTION_TEXT = "Move Models";
	final private String MOVE_MODEL_ACTION_TEXT = "Move Model";

	public MoveModelsRepositoryAction(IWorkbenchPart part)
	{
		super(part);
		setId(TersusActionConstants.MOVE_MODELS_ACTION_ID);
		setText(MOVE_MODEL_ACTION_TEXT);
	}

	@Override
	protected boolean calculateEnabled()
	{
		List<?> selectedObjects = getSelectedObjects();

		if (selectedObjects.equals(Collections.EMPTY_LIST) || selectedObjects == null
				|| selectedObjects.isEmpty())
			return false;

		IProject projectParent = null;

		for (Object obj : selectedObjects)
		{
			if (!(obj instanceof ModelEntry))
				return false;

			ModelEntry entry = (ModelEntry) obj;

			if (projectParent == null)
				projectParent = (entry).getProject();

			else if (!projectParent.equals(entry.getProject()))
				return false;

			if (entry.getParent() instanceof PackageEntry)
			{
				WorkspaceRepository repository = entry.getRepository();
				Package parentPackage = repository.getPackage(((PackageEntry) entry.getParent())
						.getPackageId());
				if (parentPackage != null && parentPackage.isReadOnly())
					return false;
			}

		}

		if (selectedObjects.size() > 1)
			setText(MOVE_MODELS_ACTION_TEXT);
		else
			setText(MOVE_MODEL_ACTION_TEXT);

		return true;
	}

	public void run()
	{
		List<?> selectedObjects = getSelectedObjects();
		ArrayList<ModelId> modelIds = new ArrayList<ModelId>();

		ModelEntry firstEntry = null;
		for (Object obj : selectedObjects)
		{
			if (firstEntry == null)
				firstEntry = (ModelEntry) obj;
			modelIds.add(((ModelEntry) obj).getModelId());
		}

		Object obj = firstEntry.getViewer().getInput();
		ProjectEntry projectEntry = null;

		if (obj instanceof RootEntry)
		{
			RootEntry rootEntry = (RootEntry) obj;
			IProject parentProject = firstEntry.getProject();
			for (Object ObjChild : rootEntry.getChildren())
			{
				if (ObjChild instanceof ProjectEntry
						&& ((ProjectEntry) ObjChild).getProject().equals(parentProject))
				{
					projectEntry = (ProjectEntry) ObjChild;
					break;
				}
			}
		}

		if (projectEntry != null)
		{
			MoveModelDialog dialog = new MoveModelDialog(projectEntry.getViewer().getControl()
					.getShell(), projectEntry.getProject(), (WorkspaceRepository) projectEntry
					.getRepository());
			dialog.open();
			
			if (dialog.getReturnCode() == Window.OK)
			{
				String newPackageName = dialog.getNewPackageName();
				Object[] selectedColumns = dialog.getResult();

				if (selectedColumns == null || selectedColumns.length == 0)
				{
					// Do Nothing
				}
				else if (selectedColumns[0] instanceof PackageId)
				{
					MoveModelCommand command = new MoveModelCommand(firstEntry.getRepository(),
							modelIds, (PackageId) selectedColumns[0], newPackageName);
					RepositoryManager editDomain = firstEntry.getEditDomain();
					editDomain.getCommandStack().execute(command);
				}
			}
		}
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.actions;

import tersus.editor.TersusEditor;
import tersus.editor.editparts.FlowDataEditPart;
import tersus.editor.editparts.TersusEditPart;
import tersus.model.BuiltinPlugins;
import tersus.model.BuiltinProperties;
import tersus.model.DataElement;
import tersus.model.FlowType;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelUtils;
import tersus.model.SubFlow;
import tersus.model.commands.MultiStepCommand;
import tersus.workbench.WorkspaceRepository;

/**
 * Changes an element's between Data Element to Sub-Flow
 * 
 * @author Youval Bronicki
 */

public class ToggleElementTypeAction extends EditorSelectionAction
{
	public ToggleElementTypeAction(TersusEditor editor)
	{
		super(editor);
		setId(TersusActionConstants.TOGGLE_ELEMENT_TYPE);
		setText(TersusActionConstants.TOGGLE_ELEMENT_TEXT);
		setLazyEnablementCalculation(false);
	}

	public void update()
	{
		super.update();
		TersusEditPart part = getSelectedEditPart();

		if (calculateEnabled())
		{
			setEnabled(true);
			if (part instanceof FlowDataEditPart)
			{
				setText("Change Data Element to Display Element");
			}
			else
			{
				setText("Change Display Element to Data Element");
			}
		}
		else
			setEnabled(false);
	}

	public void run()
	{
		final ModelElement element = getSelectedEditPart().getModelElement();
		MultiStepCommand command = new MultiStepCommand((WorkspaceRepository) element
				.getRepository(), this.getText())
		{
			protected void run()
			{

				ModelElement newElement;
				if (element instanceof DataElement)
					newElement = new SubFlow();
				else
					newElement = new DataElement();

				newElement.setRefId(element.getRefId());
				newElement.setPosition(element.getPosition());
				newElement.setSize(element.getSize());
				newElement.setRole(element.getRole());

				Model parentModel = element.getParentModel();

				removeElement(element, true);
				addElement(parentModel, newElement, null, null, null, true);
			}

		};

		getCommandStack().execute(command);
	}

	@Override
	protected boolean calculateEnabled()
	{
		TersusEditPart part = getSelectedEditPart();

		if (part != null && (ModelUtils.isDisplayModel(part.getTersusModel())))
		{
			ModelElement element = part.getModelElement();

			if (element == null)
				return false;
			
			Model parentModel = element.getParentModel();
			if (ModelUtils.isDisplayData(element) && !ModelUtils.isDialog(element.getReferredModel()))
			{
				Object parentModelType = parentModel.getProperty(BuiltinProperties.TYPE);

				boolean parentIsProcess = FlowType.ACTION == parentModelType
						|| FlowType.SERVICE == parentModelType
						|| BuiltinPlugins.BUTTON.equals(parentModel.getPlugin());
				
				if (parentIsProcess)
					return false;
			}
			if (parentModel.getPackage().isReadOnly())
				return false;
			else
				return true;
		}

		return false;
	}
}

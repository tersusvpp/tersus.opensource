/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;

import tersus.editor.ConcreteModelIdentifier;
import tersus.editor.TersusEditor;
import tersus.editor.editparts.MissingModelEditPart;
import tersus.editor.editparts.TersusEditPart;
import tersus.editor.explorer.ExplorerContentProvider;
import tersus.editor.explorer.ExplorerPart;
import tersus.editor.explorer.ModelEntry;
import tersus.editor.explorer.ProjectEntry;
import tersus.editor.explorer.RootEntry;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 * 
 */
public class ShowInRepositoryAction extends EditorSelectionAction
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#calculateEnabled()
	 */
	protected boolean calculateEnabled()
	{
		TersusEditPart selected = getSelectedEditPart();
		return (selected != null)
				&& !(selected instanceof MissingModelEditPart)
				&& (selected.getTersusModel() != null || selected.getModelElement() != null
						&& selected.getModelElement().getRefId() != null);

	}

	public ShowInRepositoryAction(TersusEditor editor)
	{
		super(editor);
		setId(TersusActionConstants.SHOW_IN_REPOSITORY_ACTION_ID);
		setText(TersusActionConstants.SHOW_IN_REPOSITORY_ACTION_TEXT);
	}

	public void run()
	{

		TersusEditPart selectedEditPart = getSelectedEditPart();
		Model model = selectedEditPart.getTersusModel();
		if (model == null)
			model = selectedEditPart.getModelElement().getReferredModel();
		ConcreteModelIdentifier modelIdentifier = new ConcreteModelIdentifier(
				((WorkspaceRepository) model.getRepository()).getRepositoryRoot().getProject(),
				model.getId());
		IWorkbenchPage page = getWorkbenchPart().getSite().getPage();
		try
		{
			ExplorerPart explorer = (ExplorerPart) page.showView(ExplorerPart.VIEW_ID);
			TreeViewer viewer = explorer.getViewer();
			RootEntry root = (RootEntry) viewer.getInput();
			ProjectEntry project = (ProjectEntry) ExplorerContentProvider.findEntryForResource(
					root, modelIdentifier.getProject());
			ModelId modelId = modelIdentifier.getModelId();
			ModelEntry modelEntry = project.findModelEntry(modelId, true);
			if (modelEntry != null)
			{
				IStructuredSelection selection = new StructuredSelection(modelEntry);
				viewer.setSelection(selection, true);
			}
			return;

		}
		catch (PartInitException e)
		{
			TersusWorkbench.log(e);
			return;
		}
	}
}
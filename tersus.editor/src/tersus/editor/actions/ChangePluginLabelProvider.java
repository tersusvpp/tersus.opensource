/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import org.eclipse.jface.viewers.LabelProvider;

/**
 * @author Liat Shiff
 */
public class ChangePluginLabelProvider extends LabelProvider
{
	public String getText(Object element)
	{
		if (element instanceof String)
		return (String)element;
		
		return null;
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.actions;

import java.util.List;

import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.ui.actions.ActionFactory;

import tersus.editor.TersusEditor;
import tersus.editor.editparts.MissingModelEditPart;
import tersus.editor.outline.ModelObjectTransfer;
import tersus.model.ModelObject;

/**
 * @author Youval Bronicki
 * 
 */
public class CopyAction extends EditorSelectionAction
{
	/**
	 * @param part
	 */
	public CopyAction(TersusEditor editor)
	{
		super(editor);
		setId(ActionFactory.COPY.getId());
		setText(TersusActionConstants.COPY_ACTION_TEXT);
	}

	protected boolean calculateEnabled()
	{
		if ((getSelectedObjects().size() > 0))
		{
			for (Object selectedObject : getSelectedObjects())
			{
				if (selectedObject instanceof MissingModelEditPart)
					return false;
			}

			return true;
		}
		return false;
	}

	public void run()
	{
		List<ModelObject> selectedObjects = getSelectedModelObjects();
		if (selectedObjects.size() > 0)
		{
			Clipboard clipboard = new Clipboard(getWorkbenchPart().getSite().getShell()
					.getDisplay());
			ModelObjectTransfer transfer = ModelObjectTransfer.getInstance();
			transfer.setObject(selectedObjects);
			clipboard.setContents(new Object[]
			{ selectedObjects }, new Transfer[]
			{ transfer });
			clipboard.dispose();
		}

	}
}

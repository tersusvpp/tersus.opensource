/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.actions;

import org.eclipse.ui.IWorkbenchPart;

import tersus.editor.Debug;
import tersus.editor.tools.ElementCreationWizard;
import tersus.model.BuiltinPlugins;
import tersus.model.FlowModel;
import tersus.model.Model;
import tersus.model.ModelObject;
import tersus.model.SubFlow;
import tersus.util.Trace;

/**
 * Change the skeleton of a table management model to fit a user specified structure of database
 * record.
 * 
 * @author Ofer Brandes
 */

public class ApplyTableWizardAction extends RepositoryAction
{
	/**
	 * @param editor
	 */
	public ApplyTableWizardAction(IWorkbenchPart part)
	{
		super(part);
		setId(TersusActionConstants.TEMP_APPLY_TABLE_WIZARD_ID);
		setText(TersusActionConstants.TEMP_APPLY_TABLE_WIZARD_TEXT);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.actions.EditorPartAction#calculateEnabled()
	 */
	protected boolean calculateEnabled()
	{
		if (Debug.OTHER)
			Trace.push("ApplyTableWizardAction.calculateEnabled()");
		try
		{
			ModelObject modelObject = getModelObject();
			if (modelObject instanceof SubFlow)
			{
				Model model = ((SubFlow) modelObject).getReferredModel();
				return  model != null && BuiltinPlugins.TABLE_WIZARD.equals(model.getPlugin())
						&& model.checkPluginVersion(3);
			}
			else if (modelObject instanceof FlowModel)
			{
				FlowModel model = (FlowModel) modelObject;
				return BuiltinPlugins.TABLE_WIZARD.equals(model.getPlugin())
						&& model.checkPluginVersion(3) && !model.isReadOnly();
			}
			return false;
		}		
		finally
		{
			if (Debug.OTHER)
				Trace.pop();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.IAction#run()
	 */
	public void run()
	{
		if (Debug.OTHER)
			Trace.push("ApplyTableWizardAction.run()");
		try
		{
			if (getModelObject() == null && getPackageEntry() == null)
			{
				if (Debug.OTHER)
					Trace.add("No model selected (or multiple models selected)");
			}
			if (calculateEnabled())
				applyTableWizard();
		}
		finally
		{
			if (Debug.OTHER)
				Trace.pop();
		}
	}

	private void applyTableWizard()
	{
		ElementCreationWizard.openWizard(getModelObject(), getShell());
	}
}
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.actions;

import tersus.editor.TersusEditor;
import tersus.editor.editparts.DataEditPart;
import tersus.editor.editparts.FlowDataEditPart;
import tersus.editor.editparts.TersusEditPart;
import tersus.model.BuiltinProperties;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.model.Slot;
import tersus.model.SlotType;

/**
 * An action that toggles the 'mandatory' property of model elements.
 * 
 * @author Youval Bronicki
 *
 */
public class ToggleMandatoryAction extends TogglePropertyAction
{

	/**
	 * @param editor The editor with which this action is associated
	 */
	public ToggleMandatoryAction(TersusEditor editor)
	{
		super(editor, TersusActionConstants.TOGGLE_MANDATORY, BuiltinProperties.MANDATORY, ELEMENT);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see tersus.editor.actions.TogglePropertyAction#calculateEnabled(tersus.editor.editparts.TersusEditPart)
	 */
	protected boolean calculateEnabled(TersusEditPart selectedPart)
	{
		ModelElement element = selectedPart.getModelElement();
		
		if (selectedPart instanceof DataEditPart && ! (selectedPart instanceof FlowDataEditPart))
		    return true;	
		else if (element instanceof Slot)
		{
		    if (element.getParentModel().getPackage().isReadOnly())
                return false;
		    else
		        return ((Slot) element).getType() == SlotType.TRIGGER;
		}    
		
		return false;
	}

	public Object getPropertyValue(ModelObject target)
	{
		return target.getProperty(propertyName);
	}
}

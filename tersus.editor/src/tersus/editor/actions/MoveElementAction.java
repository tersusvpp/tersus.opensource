/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.actions;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuCreator;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

import tersus.editor.TersusEditor;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.model.ModelUtils;
import tersus.model.commands.MoveElementCommand;
/**
 * @author Youval Bronicki
 * @author Liat Shiff
 */
public class MoveElementAction extends EditorSelectionAction implements IMenuCreator
{
	/**
	 * @param part
	 */
	private Menu menu;
	
	public MoveElementAction(TersusEditor editor)
	{
		super(editor);
		setMenuCreator(this);
        setId(TersusActionConstants.MOVE_ELEMENT_ACTION_ID);
        setText(TersusActionConstants.MOVE_ELEMENT_ACTION_TEXT);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#calculateEnabled()
	 */
	protected boolean calculateEnabled()
	{
		List<ModelObject>  sel = getSelectedModelObjects();
		Model parentModel = null;
		
		if (sel.isEmpty())
		    return false;
		
		for (ModelObject e : sel)
		{
		    
            if (e instanceof ModelElement)
            {
                if (parentModel == null)
                    parentModel = ((ModelElement) e).getParentModel();       
                
                if (parentModel != ((ModelElement) e).getParentModel())
                    return false;
                
    			if (!(ModelUtils.isDisplayElement(e)) && !(ModelUtils.isStrictDataElement(e)))
    				return false;
            }
		}
		 
		if (parentModel == null)
		    return false;
		
		if (parentModel.getPackage().isReadOnly())
		    return false;

		return true;
	}
	
	/**
	 * 
	 */
	public Menu getMenu(Menu parent)
	{
		if (menu == null || menu.isDisposed())
			menu = new Menu(parent);
		populateMenu();
		return menu;
	}
	
	private void populateMenu()
	{
		for (ModelElement target: getPotentialTargetElements())
		{
			MoveSpecificAction action = new MoveSpecificAction(target);
			addActionToMenu(menu, action);
		}
		
	}
	
	private List<ModelElement> getPotentialTargetElements()
	{
		ArrayList<ModelElement> targets = new ArrayList<ModelElement>();
		List<ModelObject>  sel = getSelectedModelObjects();
		
		if (sel.isEmpty())
		{
		    return targets;
		}
		
		// Only ModelElemnts are valid
		for (int i = 0; i < sel.size(); i++)
		{
		    if (!(sel.get(i) instanceof ModelElement))
		    {
		        return targets;
		    }
		}
		ModelElement e = (ModelElement) sel.get(0);
		
		boolean listIncludesDisplayElement = listIncludesDisplayElement(sel);
		
		for (ModelElement sibling: e.getParentModel().getElements())
        {
            if (!sel.contains((ModelObject)sibling))
            {
                if (sibling != e && ModelUtils.isStrictDataElement(sibling) && !ModelUtils.isConstant(sibling) &&
                    !listIncludesDisplayElement && !sibling.getReferredModel().getPackage().isReadOnly())
                    targets.add(sibling);
                if (sibling != e && ModelUtils.isDisplayElement(sibling))
                    targets.add(sibling);
            } 
        }
		
		return targets;
	}
	
	private boolean listIncludesDisplayElement(List<ModelObject>  sel)
	{
		for (ModelObject e : sel)
		{
			if (ModelUtils.isDisplayElement(e))
			{
				return true;
			}
		}
		
		return false;
	}
    
	/**
	 * Helper method that wraps the given action in an ActionContributionItem and then adds it
	 * to the given menu.
	 * 
	 * @param	parent	The menu to which the given action is to be added
	 * @param	action	The action that is to be added to the given menu
	 */
	protected void addActionToMenu(Menu parent, IAction action)
	{
		ActionContributionItem item = new ActionContributionItem(action);
		item.fill(parent, -1);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.IMenuCreator#getMenu(org.eclipse.swt.widgets.Control)
	 */
	public Menu getMenu(Control parent)
	{
		return null;
	}
	/* (non-Javadoc)
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#update()
	 */
	public void update()
	{
		super.update();
		if (menu != null)
		{
			if (menu.isDisposed())
				menu = null;
			else
			{
				MenuItem[] items = menu.getItems();
				for (int i = 0; i < items.length; i++)
				{
					MenuItem item = items[i];
					item.dispose();
				}
				populateMenu();
			}
		}
	}
	
	private class MoveSpecificAction extends Action
    {
        private ModelElement target;
        /**
         * @param text
         */
        public MoveSpecificAction(ModelElement target)
        {
            super(target.getElementName());
            this.target = target; 
        }
        
        public void run()
        {
            CompoundCommand compoundCommand = new CompoundCommand(); 
            
            for (ModelObject sel: getSelectedModelObjects())
            {
                MoveElementCommand command = new MoveElementCommand(((ModelElement)sel), target);
                compoundCommand.add(command);
            }
            
            ((TersusEditor) getWorkbenchPart()).getEditDomain().getCommandStack().execute(compoundCommand);
        }
    }
}

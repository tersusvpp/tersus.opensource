/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.commands.CompoundCommand;
import tersus.editor.TersusEditor;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.Package;
import tersus.model.TemplateAndPrototypeSupport;
import tersus.model.commands.DeleteModelAndPackageCommand;
import tersus.model.commands.RemoveElementCommand;
import tersus.workbench.WorkspaceRepository;

public class RemoveElementAction extends EditorSelectionAction
{
	private WorkspaceRepository repository;
	
	public RemoveElementAction(TersusEditor editor)
	{
		super(editor);
		setId(TersusActionConstants.REMOVE_ELEMENT_ACTION_ID);
		setText(TersusActionConstants.REMOVE_ELEMENT_ACTION_TEXT);
	}

	protected boolean calculateEnabled()
	{
		List<?> selection = getSelectedModelObjects();
		Model parent = null;
        
        for (Object obj: selection)
        {
        	if (!(obj instanceof ModelElement))
        		return false;
        	
        	ModelElement element = (ModelElement)obj;
        	
        	if (parent == null)
        		parent = element.getParentModel();
        	else
        	{
        		Model elementParent = element.getParentModel();
            	if (elementParent == null || !parent.equals(elementParent) || elementParent.isReadOnly())
            		return false;
        	}
        }
        
        return true;
	}

	public void run()
	{
		repository = (WorkspaceRepository)((ModelElement)getSelectedModelObjects().get(0)).getRepository();
		removeElements();
	}
	
	private void removeElements()
	{
		List<?> selection = getSelectedModelObjects();
		ArrayList<Model> elementsWithOneOccurence = new ArrayList<Model>();
		
		CompoundCommand command = new CompoundCommand("Remove");
		
		for (Object obj: selection)
        {
        	ModelElement element = (ModelElement)obj;
    		command.add(new RemoveElementCommand(element, true));
    		
    		if (element.getRefId() != null)
    		{
    			if (repository.getUpdatedRepositoryIndex().getReferenceEntries(element.getRefId())
    					.size() == 1 && element.getReferredModel() != null)
    			{
    				Model refModel = element.getReferredModel();
    				Package pkg = refModel.getPackage();
    				
    				if (!pkg.isReadOnly() && !TemplateAndPrototypeSupport.isTemplate(refModel))
    					elementsWithOneOccurence.add(refModel);
    			}
    		}
        }
		
		RemoveElementDialog dialog = null;
		
		if (elementsWithOneOccurence.size() > 0)
		{
			dialog =  new RemoveElementDialog(getShell(), elementsWithOneOccurence);
			dialog.open();
			
			System.out.println(dialog.getReturnCode());
			if (dialog.getReturnCode() == 0)
			{
				for (Model refModel: elementsWithOneOccurence)
				{
					command.add(new DeleteModelAndPackageCommand(refModel));
				}
			}
		}
		
		if ((dialog != null && dialog.getReturnCode() != 1) || dialog == null)
			((TersusEditor) getWorkbenchPart()).getEditDomain().getCommandStack().execute(command);
	}
}

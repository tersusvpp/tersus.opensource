/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import java.util.List;
import org.eclipse.jface.wizard.WizardDialog;

import tersus.editor.Debug;
import tersus.editor.TersusEditor;
import tersus.editor.wizards.ImportStructureFromDB.ImportStructureFromDBWizard;
import tersus.model.BuiltinPlugins;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.model.ModelUtils;
import tersus.util.Trace;

/**
 * @author Liat Shiff
 */
public class ImportStructureFormDBAction extends EditorSelectionAction
{
	private ModelElement selectedElement;

	private TersusEditor editor;

	public ImportStructureFormDBAction(TersusEditor editor)
	{
		super(editor);
		this.editor = editor;
		setId(TersusActionConstants.IMPORT_STRUCTURE_FROM_DB_ID);
		setText(TersusActionConstants.IMPORT_STRUCTURE_FROM_DB_TEXT);
	}

	@Override
	protected boolean calculateEnabled()
	{
		if (Debug.OTHER)
			Trace.push("ImportTableStructureWizardAction.calculateEnabled()");
		try
		{
			List<ModelObject> sel = getSelectedModelObjects();
			if (sel.size() != 1)
				return false;

			ModelObject selectedModel = sel.get(0);
			if (!ModelUtils.isStrictDataElement(selectedModel))
				return false;

			selectedElement = (ModelElement) selectedModel;

			return (BuiltinPlugins.DATABASE_RECORD.equals(selectedElement.getReferredModel()
					.getPlugin()));
		}

		finally
		{
			if (Debug.OTHER)
				Trace.pop();
		}
	}

	public void run()
	{
		if (Debug.OTHER)
			Trace.push("importTableStructureWizardAction.run()");
		try
		{
			ImportStructureFromDBWizard wizard = new ImportStructureFromDBWizard(selectedElement,
					editor, getShell(), true, false);
			WizardDialog dialog = new WizardDialog(getShell(), wizard);
			dialog.setPageSize(1000, 500);
			dialog.open();
		}
		finally
		{
			if (Debug.OTHER)
				Trace.pop();
		}
	}
}

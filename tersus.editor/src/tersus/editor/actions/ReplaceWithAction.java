/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import org.eclipse.core.resources.IProject;
import org.eclipse.gef.commands.Command;

import tersus.editor.TersusEditor;
import tersus.editor.editparts.SlotEditPart;
import tersus.model.ModelElement;
import tersus.model.Slot;
import tersus.model.commands.ReplaceWithCommand;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public class ReplaceWithAction extends EditorSelectionAction
{
	public ReplaceWithAction(TersusEditor editor)
	{
		super(editor);
		setId(TersusActionConstants.REPLACE_WITH_ACTION_ID);
		setText(TersusActionConstants.REPLACE_WITH_ACTION_TEXT);
		setToolTipText(TersusActionConstants.REPLACE_WITH_ACTION_TOOLTIP);
	}

	@Override
	protected boolean calculateEnabled()
	{
		if (getSelectedObjects().size() != 1)
			return false;

		if (getSelectedEditPart() instanceof SlotEditPart)
			return false;

		return true;
	}

	public void run()
	{
		TersusEditor editor = (TersusEditor) getWorkbenchPart();
		IProject project = editor.getRepository().getProject();

		ModelElement element = getSelectedModelElement();

		ReplaceWithDialog dialog = new ReplaceWithDialog(getShell(), project);
		dialog.setSelectedElement(element);
		dialog.open();

		if (dialog.getReturnCode() == 0)
		{
			Command cmd = new ReplaceWithCommand((WorkspaceRepository) element.getRepository(),
					element, dialog.getSelectedModelId(), dialog.replaceAll(), dialog
							.deleteOldModel(), dialog.isSyncNames());

			editor.getEditDomain().getCommandStack().execute(cmd);
		}
	}
}

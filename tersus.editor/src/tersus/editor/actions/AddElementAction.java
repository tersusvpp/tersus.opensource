/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuCreator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

import tersus.editor.Images;
import tersus.editor.TersusEditor;
import tersus.editor.editparts.ModelEditPart;
import tersus.editor.editparts.TersusEditPart;
import tersus.model.Link;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.Slot;
import tersus.model.SlotType;
import tersus.model.TemplateAndPrototypeSupport;
import tersus.model.commands.AddPrototypeElementCommand;
import tersus.util.Multiplicity;
import tersus.workbench.ImageLocator;

/**
 * @author Youval Bronicki
 * 
 */
public class AddElementAction extends EditorSelectionAction implements IMenuCreator
{
	private Menu menu;

	public AddElementAction(TersusEditor editor)
	{
		super(editor);
		setMenuCreator(this);
		setId(TersusActionConstants.ADD_ELEMENT_ACTION_ID);
		setText(TersusActionConstants.ADD_ELEMENT_ACTION_TEXT);
	}

	protected boolean calculateEnabled()
	{
		Model model = getSelectedModel();

		return (model != null && !model.getPackage().isReadOnly() && getRelevantElements().size() > 0);
	}

	public Menu getMenu(Menu parent)
	{
		if (menu == null || menu.isDisposed())
			menu = new Menu(parent);
		populateMenu();
		return menu;
	}

	private void populateMenu()
	{
		for (ModelElement element : getRelevantElements())
		{
			AddSpecificElementAction action = new AddSpecificElementAction(element);
			addActionToMenu(menu, action);
		}
	}

	private List<ModelElement> getRelevantElements()
	{
		TersusEditPart part = getSelectedEditPart();
		if (!(part instanceof ModelEditPart))
			return Collections.emptyList();
		Model model = part.getTersusModel();
		Model prototype = TemplateAndPrototypeSupport.getPrototypeModel(model);
		if (prototype == null)
			return Collections.emptyList();
		List<ModelElement> pElements = prototype.getElements();
		List<ModelElement> additionalElements = null;
		for (int i = 0; i < pElements.size(); i++)
		{
			ModelElement element = (ModelElement) pElements.get(i);
			if (element instanceof Link)
				continue;

			Multiplicity multiplicity = TemplateAndPrototypeSupport.getMultiplicity(element);
			if (multiplicity.isExample)
				continue;
			if (multiplicity.max > TemplateAndPrototypeSupport.getInstanceElements(model, element)
					.size())
			{
				if (additionalElements == null)
					additionalElements = new ArrayList<ModelElement>();
				additionalElements.add(element);
			}
		}
		if (additionalElements == null)
			return Collections.emptyList();
		else
			return additionalElements;
	}

	protected void addActionToMenu(Menu parent, IAction action)
	{
		ActionContributionItem item = new ActionContributionItem(action);
		item.fill(parent, -1);
	}

	private class AddSpecificElementAction extends Action
	{
		private ModelElement sourceElement;

		public AddSpecificElementAction(ModelElement element)
		{
			super(element.getRole().toString());
			setSourceElement(element);
			if (element instanceof Slot)
			{
				if (((Slot) element).getType() == SlotType.TRIGGER)
					setImageDescriptor(Images.TRIGGER16);
				else if (((Slot) element).getType() == SlotType.EXIT)
					setImageDescriptor(Images.EXIT16);
				else if (((Slot) element).getType() == SlotType.ERROR)
					setImageDescriptor(Images.ERROR16);
			}
			else if (element.getReferredModel() != null)
			{
				ImageLocator imageLocator = TersusEditor
						.getImageLocator(element.getReferredModel());
				if (imageLocator != null)
				{
					ImageDescriptor image = imageLocator.getImageDescriptor(0, 0);
					setImageDescriptor(image);
				}
			}
		}

		public void run()
		{
			ModelEditPart targetEditPart = (ModelEditPart) getSelectedEditPart();
			Model targetModel = targetEditPart.getTersusModel();
			AddPrototypeElementCommand command = new AddPrototypeElementCommand(targetModel,
					getSourceElement());
			((TersusEditor) getWorkbenchPart()).getEditDomain().getCommandStack().execute(command);
			update();

		}

		public ModelElement getSourceElement()
		{
			return sourceElement;
		}

		public void setSourceElement(ModelElement sourceElement)
		{
			this.sourceElement = sourceElement;
		}
	}

	public Menu getMenu(Control parent)
	{
		return null;
	}

	public void update()
	{
		super.update();
		if (menu != null)
		{
			if (menu.isDisposed())
				menu = null;
			else
			{
				MenuItem[] items = menu.getItems();
				for (int i = 0; i < items.length; i++)
				{
					MenuItem item = items[i];
					item.dispose();
				}
				populateMenu();
			}
		}
	}
}

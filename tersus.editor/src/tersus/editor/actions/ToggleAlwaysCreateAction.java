/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import tersus.editor.TersusEditor;
import tersus.editor.editparts.MissingModelEditPart;
import tersus.editor.editparts.TersusEditPart;
import tersus.model.BuiltinProperties;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.model.ModelUtils;
import tersus.util.Misc;

/**
 * @author Liat Shiff
 */
public class ToggleAlwaysCreateAction extends TogglePropertyAction
{ 
	public ToggleAlwaysCreateAction(TersusEditor editor)
	{
		super(editor, TersusActionConstants.TOGGLE_ALWAYS_CREATE, BuiltinProperties.ALWAYS_CREATE,
				ELEMENT);
		setText(Misc.ucFirst("Always Create"));
	}

	protected boolean calculateEnabled(TersusEditPart selectedPart)
	{
		if (selectedPart instanceof MissingModelEditPart)
			return false;

		ModelElement element = selectedPart.getModelElement();

		if (element == null)
			return false;

		Model parentModel = element.getParentModel();

		if (ModelUtils.isDisplayElement(element) && !parentModel.getPackage().isReadOnly()
				&& ModelUtils.isDisplayModel(parentModel))
			return true;

		return false;
	}

	public Object getPropertyValue(ModelObject target)
	{
		Object value = target.getProperty(propertyName);

		if (value == null)
			return Boolean.TRUE;

		if (value instanceof String && "true".equals(((String) value).toLowerCase()))
			return Boolean.TRUE;
			
		return value;
	}
}

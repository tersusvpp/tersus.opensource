/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.views.navigator.NavigatorFrameSource;
import org.eclipse.ui.views.navigator.ResourceNavigator;

import tersus.editor.explorer.ExplorerEntry;
import tersus.editor.explorer.ModelEntry;
import tersus.editor.explorer.PackageEntry;
import tersus.editor.search.TersusSearchModelEntry;
import tersus.editor.search.TersusSearchPackageEntry;

/**
 * @author Liat Shiff
 */
public class ShowInNavigatorAction extends RepositoryAction
{

	public ShowInNavigatorAction(IWorkbenchPart part)
	{
		super(part);
		setId(TersusActionConstants.SHOW_IN_NAVIGATOR_ACTION_ID);
		setText(TersusActionConstants.SHOW_IN_NAVIGATOR_ACTION_TEXT);
	}

	protected boolean calculateEnabled()
	{
		Object selectedObject = getSelectedObject();

		if (selectedObject instanceof PackageEntry
				|| selectedObject instanceof TersusSearchPackageEntry 
				|| selectedObject instanceof ModelEntry
				||  selectedObject instanceof TersusSearchModelEntry)
			return true;

		return false;
	}

	public void run()
	{
		Object selectedObject = getSelectedObject();
		
		IFile file = null;
		
		if (selectedObject instanceof PackageEntry)
		{
			PackageEntry entry = (PackageEntry) getSelectedObject();
			file = entry.getFile();
		}
		else if (selectedObject instanceof TersusSearchPackageEntry)
		{
			TersusSearchPackageEntry entry = (TersusSearchPackageEntry) getSelectedObject();
			file = entry.getFile();
		}
		else if (selectedObject instanceof ModelEntry)
		{
			ModelEntry entry = (ModelEntry) getSelectedObject();
			
			ExplorerEntry parentEntry = entry.getParent();
			if (parentEntry instanceof PackageEntry)
				file = ((PackageEntry)parentEntry).getFile();
		}
		else if (selectedObject instanceof TersusSearchModelEntry)
		{
			TersusSearchModelEntry entry = (TersusSearchModelEntry) getSelectedObject();
			
			TersusSearchPackageEntry parentEntry = entry.getParent();
			file = parentEntry.getFile();	
		}

		if (file != null)
		{
			IWorkbenchPage page = getWorkbenchPart().getSite().getPage();
			try
			{
				ResourceNavigator nav = (ResourceNavigator) page.showView(IPageLayout.ID_RES_NAV);
				TreeViewer viewer = nav.getViewer();
				IStructuredSelection selection = new StructuredSelection(file);
				viewer.setSelection(selection, true);
			}
			catch (PartInitException e)
			{
				e.printStackTrace();
			}
		}
	}
}

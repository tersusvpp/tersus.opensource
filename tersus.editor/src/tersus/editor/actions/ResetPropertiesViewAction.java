package tersus.editor.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;

import tersus.editor.TersusEditor;
import tersus.editor.properties.TersusPropertySheetPage;

public class ResetPropertiesViewAction extends Action
{
	private TersusPropertySheetPage page;
	
	public ResetPropertiesViewAction(TersusPropertySheetPage page)
	{
		this.page = page;
		
		setId(TersusActionConstants.RESET_PROPERTIES_VIEW_ACTION_ID);
		setText(TersusActionConstants.RESET_PROPERTIES_VIEW_ACTION_TEXT); 
		setImageDescriptor(ImageDescriptor.createFromFile(TersusEditor.class, "icons/Reset.gif"));
	}
	
	public void update()
	{
		// Do Nothing
	}
	
	public void run() 
	{
		page.resetPropertyView();
	}
}

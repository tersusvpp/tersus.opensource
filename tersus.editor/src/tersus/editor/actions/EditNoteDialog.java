/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * @author Youval Bronicki
 *
 */
public class EditNoteDialog extends Dialog
{

    protected String comment="";
    private Text text;
    public void setComment(String comment)
    {
        if (comment != null)
            this.comment = comment;
        else
            this.comment = "";
    }
    protected Control createDialogArea(Composite parent)
    {
        getShell().setText("Edit Note");
        Composite composite = (Composite)super.createDialogArea(parent);
        GridLayout layout = new GridLayout();
        composite.setLayout(layout);
        layout.numColumns = 1;

        GridData gd = null;
        text = new Text(composite,SWT.BORDER|SWT.MULTI|SWT.WRAP);
        text.setText(comment);
        text.addModifyListener(new ModifyListener(){

            public void modifyText(ModifyEvent e)
            {
                comment = text.getText();
            }});
        gd = new GridData(GridData.FILL_BOTH);
        gd.heightHint = 200;
        gd.widthHint = 400;
        text.setLayoutData(gd);
        return composite;
    }
    /**
     * @param parentShell
     */
    protected EditNoteDialog(Shell parentShell)
    {
        super(parentShell);
        setShellStyle(SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL | SWT.RESIZE);
    }
    /**
     * @return
     */
    public String getComment()
    {
        return comment;
    }

}

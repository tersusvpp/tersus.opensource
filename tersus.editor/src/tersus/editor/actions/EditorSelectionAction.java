/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.swt.widgets.Shell;

import tersus.editor.TersusEditor;
import tersus.editor.editparts.FlowEditPart;
import tersus.editor.editparts.ModelEditPart;
import tersus.editor.editparts.TersusEditPart;
import tersus.editor.outline.ModelObjectTransfer;
import tersus.model.BuiltinProperties;
import tersus.model.FlowModel;
import tersus.model.Link;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.model.RelativePosition;
import tersus.model.RelativeSize;
import tersus.model.Slot;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 *
 */
public abstract class EditorSelectionAction extends SelectionAction
{

    /* (non-Javadoc)
     * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#calculateEnabled()
     */

	public List<ModelObject> getSelectedModelObjects()
	{
		List<?> selectedParts = getSelectedObjects();
		ArrayList<ModelObject> selectedModelObjects = new ArrayList<ModelObject>(selectedParts.size());
		for (Object part: selectedParts)
		{
			if (part instanceof TersusEditPart)
			{
				selectedModelObjects.add(((TersusEditPart)part).getModelObject());
			}
				
		}
		return selectedModelObjects;
	}
    public EditorSelectionAction(TersusEditor editor)
    {
        super(editor);
    }
    protected TersusEditPart getSelectedEditPart()
    {
        Object selectedObject = getSelectedObject();
    	if (selectedObject instanceof TersusEditPart)
    	    return (TersusEditPart)selectedObject;
    	else
    	    return null;
    }

    protected FlowEditPart getSelectedFlowEditPart()
    {
        Object selectedObject = getSelectedObject();
    	if (selectedObject instanceof FlowEditPart)
    		return (FlowEditPart) selectedObject;
    	else
    		return null;
    }
    protected FlowModel getSelectedFlowModel()
    {
    	Object selectedObject = getSelectedObject();
    	if (selectedObject == null)
    	    return null;
    	if (selectedObject instanceof ModelEditPart && ((ModelEditPart)selectedObject).getTersusModel() instanceof FlowModel)
    		return (FlowModel) ((ModelEditPart)selectedObject).getTersusModel();
    	else
    		return null;
    }
    private Object getSelectedObject()
    {
        int selectedObjects = getSelectedObjects().size();
    	Object selectedObject = null;
    	if (selectedObjects == 1)
            selectedObject = getSelectedObjects().get(0);
        return selectedObject;
    }
    protected ModelObject getSelectedModelObject()
    {
        Object selectedObject = getSelectedObject();
        if (selectedObject instanceof ModelEditPart)
            return ((ModelEditPart)selectedObject).getModelObject();
        else 
            return null;
    }
    protected ModelElement getSelectedModelElement()
    {
        ModelObject selectedObject = getSelectedModelObject();
        if (selectedObject instanceof ModelElement)
            return ((ModelElement)selectedObject);
        else 
            return null;
    }
    protected Model getSelectedModel()
    {
        Object selectedObject = getSelectedObject();
        if (selectedObject instanceof ModelEditPart)
            return ((ModelEditPart)selectedObject).getTersusModel();
        else 
            return null;
    }
    protected Object getClipboardContents()
    {
        Clipboard clipboard = new Clipboard(getWorkbenchPart().getSite().getShell().getDisplay());
        try
        {
    
            ModelObjectTransfer transfer = ModelObjectTransfer.getInstance();
            for (TransferData type : clipboard.getAvailableTypes())
            {
                if (transfer.isSupportedType(type))
                {
                    Object contents = clipboard.getContents(transfer);
                    if (contents != null)
                        return contents;
                }
            }
            return null;
        }
        finally
        {
            clipboard.dispose();
        }
    
    }
    protected WorkspaceRepository getRepository()
    {
    	return ((TersusEditor)getWorkbenchPart()).getRepository();
    }
    protected Shell getShell()
    {
        return getWorkbenchPart().getSite().getShell();
    }
	protected boolean isEnabledForMultiElementModification(List<ModelObject> selection, int min, boolean skipLinks, boolean allowTopBottomSlots, boolean allowLeftRightSlots)
	{
		// Verify all selected elements have the same non-read-only parent
		Model parentModel = null;

		if (selection.isEmpty())
			return false;

		// Checks that all selected elements have the same parent.
		int n = 0;
		for (ModelObject e : selection)
		{
			if (e instanceof ModelElement)
			{
				if (parentModel == null)
					parentModel = ((ModelElement) e).getParentModel();
				else if (parentModel != ((ModelElement) e).getParentModel())
					return false;
				if (e instanceof Link)
				{
					if (!skipLinks)
						n++;
				}
				else if (e instanceof Slot)
				{
					RelativePosition pos = (RelativePosition) e.getProperty(BuiltinProperties.POSITION);
					double x = pos.getX();
					double y = pos.getY();
					if ( (!allowTopBottomSlots && (y==0 || y==1)) || (!allowLeftRightSlots && (x==0 || x==1)) )
						return false;
					else
						n++;
				}
				else
					n++;
			}
		}

		if (parentModel == null)
			return false;

		if (parentModel.getPackage().isReadOnly())
			return false;

		return (n>=min);		
	}
}

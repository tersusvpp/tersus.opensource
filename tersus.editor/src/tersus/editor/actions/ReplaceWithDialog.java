/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import org.eclipse.core.resources.IProject;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.FilteredTree;
import org.eclipse.ui.dialogs.PatternFilter;

import tersus.model.DataElement;
import tersus.model.DataType;
import tersus.model.FlowModel;
import tersus.model.FlowType;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.ModelUtils;
import tersus.model.Role;
import tersus.model.Slot;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public class ReplaceWithDialog extends TitleAreaDialog
{
	private Composite composite;

	private IProject project;

	private TreeViewer viewer;

	private Button syncNamesButton;

	private Button replaceAllButton;

	private Button replaceOnlySelected;

	private Button deleteOldModelButton;

	private ModelElement element;

	private Label referenceCountLabel;

	private int refCount;

	private ModelId selectedModelId;

	private boolean replaceAll;

	private boolean deleteOldModel;

	private boolean syncNames;

	private WorkspaceRepository repository;

	protected ReplaceWithDialog(Shell parentShell, IProject project)
	{
		super(parentShell);
		this.project = project;
		this.replaceAll = false;
		this.deleteOldModel = false;
	}

	protected Control createDialogArea(Composite parent)
	{
		composite = (Composite) super.createDialogArea(parent);
		initializeDialogUnits(composite);

		GridLayout layout = new GridLayout();
		layout.marginHeight = 5;
		layout.marginWidth = 5;
		layout.verticalSpacing = 0;
		layout.horizontalSpacing = 0;
		composite.setLayout(layout);
		composite.setLayoutData(new GridData(GridData.FILL_BOTH));
		composite.setFont(parent.getFont());

		addFilteredTree();
		addSyncNamesCheckbox();

		refCount = repository.getUpdatedRepositoryIndex().getReferenceEntries(element.getRefId())
				.size();

		if (element != null && element.getReferredModel() != null)
			addDeleteOldModel();

		if (refCount > 1)
			addReplaceAll();

		setting(false, "Please select one model to replace with..", null);

		return composite;
	}

	private void addSyncNamesCheckbox()
	{
		syncNamesButton = new Button(composite, SWT.CHECK);
		syncNamesButton.setText("Keep the model name and element name equal");
		syncNamesButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true,
				GridData.HORIZONTAL_ALIGN_END, GridData.VERTICAL_ALIGN_CENTER));

		syncNamesButton.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				syncNames = syncNamesButton.getSelection();
				runValidate();
			}
		});
		syncNamesButton.setSelection(false);
		syncNames = false;
	}

	private void addFilteredTree()
	{
		Composite container = new Composite(composite, SWT.NULL);
		container.setLayout(new GridLayout());
		
		GridData gridData = new GridData(GridData.FILL_BOTH);
		gridData.heightHint = 160;
		container.setLayoutData(gridData);
		
		PatternFilter filter = new PatternFilter();
		int styleBits = SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER;
		FilteredTree filteredTree = new FilteredTree(container, styleBits, filter);

		viewer = filteredTree.getViewer();
		viewer.setContentProvider(new ReplaceWithContentProvider(viewer));
		viewer.setLabelProvider(new ReplaceWithLabelProvider(element));
		viewer.setInput(project);

		viewer.addSelectionChangedListener(new ISelectionChangedListener()
		{
			public void selectionChanged(SelectionChangedEvent event)
			{
				Object obj = event.getSelection();

				if (obj instanceof TreeSelection)
				{
					TreeSelection treeSelection = (TreeSelection) obj;
					Object selectedObject = treeSelection.getFirstElement();

					if (treeSelection.size() == 1)
					{
						if (selectedObject instanceof SearchDialogPackageEntry)
							setting(false, null, "Package can not be selected");
						else
						// selectedObject instanceof SearchDialogModelEntry
						{
							Model tempModel = repository.getModel(
									((SearchDialogModelEntry) selectedObject).getModelId(), true);

							String validationError = null;
							if ((validationError = validate(tempModel)) == null)
							{
								selectedModelId = ((SearchDialogModelEntry) selectedObject)
										.getModelId();
								setting(true, null, null);
							}
							else
								setting(false, null, validationError);
						}
					}
					else
						setting(false, "Select one model to replace with..", null);
				}
			}
		});
	}

	private String validate(Model selectedModel)
	{
		if (element instanceof DataElement)
			if (!(selectedModel instanceof DataType))
				return "You need to select data type model.";

		if (ModelUtils.isDisplayElement(element))
			if (!(ModelUtils.isDisplayModel(selectedModel)))
				return "You need to select a display model.";

		if (element instanceof Slot)
			if (!(selectedModel instanceof DataType || ModelUtils.isDisplayModel(selectedModel)))
				return "You need to select data type or display model.";

		if (ModelUtils.isSubProcess(element))
		{
			if (!(selectedModel instanceof FlowModel && (FlowType.ACTION
					.equals(((FlowModel) selectedModel).getType())
					|| FlowType.SERVICE.equals(((FlowModel) selectedModel).getType()) || FlowType.OPERATION
					.equals(((FlowModel) selectedModel).getType()))))
				return "You need to select a process model (Action/Service).";
		}

		if (element.getModelId().equals(selectedModel.getModelId()))
			return "The selected modelId '" + selectedModel.getModelId().getName()
					+ "' equals to the selected element's modelId.";
		if (syncNamesButton.getSelection())
		{
			for (ModelElement reference : repository.getReferences(getElement().getModelId()))
			{
				if (reference.getParentModel().getElement(Role.get(selectedModel.getName())) != null)
					return "There is an existing element called '" + selectedModel.getName()
							+ "' in: " + reference.getParentModel().getModelId();
			}
		}

		return null;
	}

	public void runValidate()
	{
		if (getSelectedModelId() != null)
		{
			Model tempModel = repository.getModel(getSelectedModelId(), true);

			String validationError = null;
			if (tempModel != null && (validationError = validate(tempModel)) != null)
				setting(false, null, validationError);
			else
				setting(true, null, null);
		}
	}

	private void addReplaceAll()
	{
		Group replaceAllRGrup = new Group(composite, SWT.NONE | composite.getStyle());
		replaceAllRGrup.setText("Replace All?");
		GridLayout layout = new GridLayout(1, true);
		layout.horizontalSpacing = 1;
		layout.verticalSpacing = 8;
		replaceAllRGrup.setLayout(layout);

		referenceCountLabel = new Label(replaceAllRGrup, SWT.WRAP);
		GridData gd = new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);
		gd.horizontalSpan = 3;
		referenceCountLabel.setLayoutData(gd);

		if (refCount > 1)
		{
			referenceCountLabel.setText("The model '" + element.getModelId().getName() + "' has "
					+ (refCount - 1) + " additional references.");
			referenceCountLabel.setVisible(true);
			referenceCountLabel.setForeground(ColorConstants.blue);
		}
		else
			referenceCountLabel.setVisible(false);

		replaceAllButton = new Button(replaceAllRGrup, SWT.RADIO);
		replaceAllButton.setText("Replace all elements");
		replaceAllButton.addSelectionListener(new SelectionListener()
		{
			public void widgetDefaultSelected(SelectionEvent e)
			{
				if (deleteOldModelButton != null && replaceAllButton.getSelection())
				{
					deleteOldModelButton.setEnabled(true);
					replaceAll = true;
					runValidate();
				}
			}

			public void widgetSelected(SelectionEvent e)
			{
				if (deleteOldModelButton != null && replaceAllButton.getSelection())
				{
					deleteOldModelButton.setEnabled(true);
					replaceAll = true;
					runValidate();
				}
			}
		});

		replaceOnlySelected = new Button(replaceAllRGrup, SWT.RADIO);
		replaceOnlySelected.setText("Replace only the selected element");

		replaceOnlySelected.addSelectionListener(new SelectionListener()
		{
			public void widgetDefaultSelected(SelectionEvent e)
			{
				if (deleteOldModelButton != null && replaceOnlySelected.getSelection())
				{
					deleteOldModelButton.setEnabled(false);
					replaceAll = false;
					runValidate();
				}
			}

			public void widgetSelected(SelectionEvent e)
			{
				if (deleteOldModelButton != null && replaceOnlySelected.getSelection())
				{
					deleteOldModelButton.setEnabled(false);
					replaceAll = false;
					runValidate();
				}
			}
		});

		replaceOnlySelected.setSelection(true);
		if (deleteOldModelButton != null)
			deleteOldModelButton.setEnabled(false);

	}

	private void addDeleteOldModel()
	{
		deleteOldModelButton = new Button(composite, SWT.CHECK);
		deleteOldModelButton.setText("Delete old model");
		deleteOldModelButton.setLayoutData(new GridData(SWT.FILL, SWT.LEFT_TO_RIGHT, true, true,
				GridData.HORIZONTAL_ALIGN_END, GridData.VERTICAL_ALIGN_CENTER));

		if (replaceOnlySelected != null && replaceOnlySelected.getSelection())
			deleteOldModelButton.setEnabled(false);

		deleteOldModelButton.addSelectionListener(new SelectionListener()
		{
			public void widgetDefaultSelected(SelectionEvent e)
			{
				deleteOldModel = deleteOldModelButton.getSelection();
			}

			public void widgetSelected(SelectionEvent e)
			{
				deleteOldModel = deleteOldModelButton.getSelection();
			}
		});
	}

	protected void createButtonsForButtonBar(Composite parent)
	{
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
		getButton(IDialogConstants.OK_ID).setEnabled(false);
	}

	@Override
	protected void configureShell(Shell shell)
	{
		super.configureShell(shell);
		
		shell.setText("Replace With..");
		Composite comp = shell.getParent();
		int width = comp.getBounds().width;
		int height = comp.getBounds().height;

		refCount = repository.getUpdatedRepositoryIndex().getReferenceEntries(element.getRefId())
				.size();

		if (refCount > 1)
			shell.setBounds(width / 2 - 250, height / 2 - 225, 500, 475);
		else
			shell.setBounds(width / 2 - 250, height / 2 - 225, 500, 375);
	}

	private void setting(boolean enableOkButton, String messageText, String errorText)
	{
		if (getButton(IDialogConstants.OK_ID) != null)
			getButton(IDialogConstants.OK_ID).setEnabled(enableOkButton);

		setMessage(messageText);
		setErrorMessage(errorText);
	}

	public boolean replaceAll()
	{
		return replaceAll;
	}

	public boolean deleteOldModel()
	{
		return deleteOldModel;
	}

	public boolean isSyncNames()
	{
		return syncNames;
	}

	public void setSelectedElement(ModelElement element)
	{
		this.element = element;

		repository = (WorkspaceRepository) element.getRepository();
	}

	public ModelElement getElement()
	{
		return element;
	}

	public ModelId getSelectedModelId()
	{
		return selectedModelId;
	}
}

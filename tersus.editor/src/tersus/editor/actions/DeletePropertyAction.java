package tersus.editor.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;

import tersus.editor.TersusEditor;
import tersus.editor.properties.TersusPropertySheetPage;

public class DeletePropertyAction extends Action
{
	private TersusPropertySheetPage page;
	
	public DeletePropertyAction(TersusPropertySheetPage page)
	{
		this.page = page;
		
		setId(TersusActionConstants.DELETE_PROPERTY_ACTION_ID);
		setText(TersusActionConstants.DELETE_PROPERTY_VIEW_ACTION_TEXT); 
		setImageDescriptor(ImageDescriptor.createFromFile(TersusEditor.class, "icons/DeleteProperty.gif"));
	}
	
	
	public void run() 
	{
		page.deleteProperty();
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import org.eclipse.core.resources.IProject;
import org.eclipse.swt.graphics.Image;

import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public abstract class SearchDialogEntry
{
	private WorkspaceRepository repository;

	private SearchDialogPackageEntry parentPackageEntry;

	private IProject project;

	public SearchDialogEntry(WorkspaceRepository repository,
			SearchDialogPackageEntry parentPackageEntry, IProject project)
	{
		this.repository = repository;
		this.parentPackageEntry = parentPackageEntry;
		this.project = project;
	}

	public WorkspaceRepository getRepository()
	{
		return repository;
	}

	public IProject getProject()
	{
		return project;
	}

	public SearchDialogPackageEntry getParentPackageEntry()
	{
		return parentPackageEntry;
	}

	abstract public Image getImage();
}

/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import java.util.ArrayList;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import tersus.model.Model;

/**
 * @author Liat Shiff
 */
public class RemoveElementDialog extends Dialog
{
	private Composite composite;

	private ArrayList<Model> elementsWithOneOccurence;
	
	public static final int NO = 2;

	protected RemoveElementDialog(Shell shell, ArrayList<Model> elementsWithOneOccurence)
	{
		super(shell);
		this.elementsWithOneOccurence = elementsWithOneOccurence;
	}

	protected Control createDialogArea(Composite parent)
	{
		composite = (Composite) super.createDialogArea(parent);
		initializeDialogUnits(composite);

		setDialogMessage();

		return composite;
	}

	private Label setDialogMessage()
	{
		Label dialogMessage = new Label(composite, SWT.LEFT);

		if (elementsWithOneOccurence.size() == 1)
			dialogMessage.setText("You are removing the only occurence of \""
					+ elementsWithOneOccurence.get(0).getName()
					+ "\".\nWould you like to remove this model from the repository?");
		else
		{
			StringBuffer buffer = new StringBuffer("");
			for (Model refModel : elementsWithOneOccurence)
			{
				buffer.append(refModel.getName() + "\n");
			}
			dialogMessage
					.setText("You are removing the only occurence of the following models:\n\n"
							+ buffer.toString()
							+ "\nWould you like to remove these models from the repository?");

		}

		return dialogMessage;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent)
	{
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.YES_LABEL, true);
		createButton(parent, IDialogConstants.NO_ID, IDialogConstants.NO_LABEL, false);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}
	
	protected void noPressed() 
	{
		setReturnCode(NO);
		close();
	}
	
	protected void buttonPressed(int buttonId) 
	{
		if (IDialogConstants.OK_ID == buttonId) 
			okPressed();
		else if (IDialogConstants.CANCEL_ID == buttonId)
			cancelPressed();
		else if (IDialogConstants.NO_ID == buttonId)
			noPressed();
	}
}

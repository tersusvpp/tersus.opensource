/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.actions;

import java.util.List;

import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.jface.dialogs.MessageDialog;

import tersus.editor.TersusEditor;

import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.model.ModelUtils;
import tersus.model.Package;
import tersus.model.Slot;
import tersus.model.commands.DecapsulateCommand;
import tersus.model.commands.DeleteModelAndPackageCommand;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public class DecapsulateAction extends EditorSelectionAction
{
	public DecapsulateAction(TersusEditor editor)
	{
		super(editor);
		setId(TersusActionConstants.DECAPSULATE_ACTION_ID);
		setText(TersusActionConstants.DECAPSULATE_ACTION_TEXT);
		setToolTipText(TersusActionConstants.DECAPSULATE_ACTION_TOOLTIP);
	}

	public void run()
	{
		if (!calculateEnabled())
			return;

		List<ModelObject> sel = getSelectedModelObjects();

		ModelElement element = null;
		if (sel.get(0) instanceof ModelElement)
			element = (ModelElement) sel.get(0);

		CompoundCommand commands = new CompoundCommand();
		DecapsulateCommand decapsulateCommand = new DecapsulateCommand(element.getParentModel(),
				element);
		String errorMessag = decapsulateCommand.validate();
		if (errorMessag != null)
		{
			MessageDialog.openInformation(TersusWorkbench.getActiveWorkbenchShell(),
					"Can't decapsulate", errorMessag);
			return;
		}
		commands.add(decapsulateCommand);

		if (element.getRefId() != null)
		{
			if (((WorkspaceRepository) element.getRepository()).getReferences(element.getRefId())
					.size() == 1)
			{
				Model refModel = element.getReferredModel();
				Package pkg = refModel.getPackage();
				if (!pkg.isReadOnly())
					if (MessageDialog
							.openQuestion(
									getWorkbenchPart().getSite().getShell(),
									"Delete Model?",
									"You are removing the only occurence of \""
											+ element.getReferredModel().getName()
											+ "\". Would you like to remove this model from the repository?"))
						commands.add(new DeleteModelAndPackageCommand(refModel));
			}
		}

		((TersusEditor) getWorkbenchPart()).getEditDomain().getCommandStack().execute(commands);
	}

	@Override
	protected boolean calculateEnabled()
	{
		Model parentModel = null;

		List<ModelObject> sel = getSelectedModelObjects();
		if (sel.isEmpty())
			return false;

		if (sel.size() != 1)
			return false;

		ModelElement selectedElement = null;

		if (sel.get(0) instanceof ModelElement)
		{
			selectedElement = (ModelElement) sel.get(0);
			
			if (selectedElement.getReferredModel() == null)
				return false;
			parentModel = selectedElement.getParentModel();
		}
		else
			return false;

		if (selectedElement instanceof Slot)
			return false;
		
		if (ModelUtils.isConstant(selectedElement))
			return false;

		if (parentModel == null)
			return false;

		if (parentModel.getPackage().isReadOnly())
			return false;

		return true;
	}

}

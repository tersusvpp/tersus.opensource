/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.actions;


import org.eclipse.gef.internal.GEFMessages;
import org.eclipse.gef.internal.InternalImages;
import org.eclipse.gef.ui.actions.GEFActionConstants;

import tersus.editor.Debug;
import tersus.editor.TersusZoomManager;
import tersus.editor.useractions.ZoomInUserAction;
import tersus.util.Trace;

/**
 * 
 */
public class ZoomInAction extends ZoomAction
{

	/**
	 * @param zoomManager
	 */
	public ZoomInAction(TersusZoomManager zoomManager)
	{
		super(GEFMessages.ZoomIn_Label, InternalImages.DESC_ZOOM_IN, zoomManager);
		setToolTipText(GEFMessages.ZoomIn_Tooltip);
		setId(GEFActionConstants.ZOOM_IN);
		setActionDefinitionId(GEFActionConstants.ZOOM_IN);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.IAction#run()
	 */
	public void run()
	{
		if (Debug.USER_ACTIONS)
			Trace.push(new ZoomInUserAction());
		try
		{	
			zoomIn();
		}
		finally
		{
			if (Debug.USER_ACTIONS)
				Trace.pop();
		}
	}

}

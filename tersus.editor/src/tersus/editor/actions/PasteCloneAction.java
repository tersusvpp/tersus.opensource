/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import java.util.List;

import tersus.editor.TersusEditor;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.model.commands.PasteCloneCommand;
import tersus.model.commands.PasteElementsCommand;

public class PasteCloneAction extends EditorSelectionAction
{
	public PasteCloneAction(TersusEditor editor)
	{
		super(editor);
		setId(TersusActionConstants.PASTE_CLONE_ACTION_ID);
		setText(TersusActionConstants.PASTE_CLONE_ACTION_TEXT);
	}

	public void run()
	{
		ModelObject mo = getClipboardObject();
		if (getModel(mo) != null)
		{
			Model targetModel = getSelectedModel();
			PasteCloneCommand command = new PasteCloneCommand(targetModel, mo);
			((TersusEditor) getWorkbenchPart()).getEditDomain().getCommandStack().execute(command);
		}

	}

	protected boolean calculateEnabled()
	{
		Model selectedModel = getSelectedModel();

		return selectedModel != null && getModel(getClipboardObject()) != null
				&& PasteElementsCommand.checkValidChild(getClipboardObject(), getSelectedModel(), false)
				&& getClipboardObject().getRepository() == getSelectedModel().getRepository()
				&& !selectedModel.getPackage().isReadOnly();
	}

	protected ModelObject getClipboardObject()
	{
		Object cc = getClipboardContents();
		ModelObject mo = null;
		if (cc instanceof List<?>)
		{
			if (((List<?>) cc).size() != 1)
				return null;
			if (((List<?>) cc).get(0) instanceof ModelObject)
				mo = (ModelObject) ((List<?>) cc).get(0);
			else
				return null;
		}
		else
			mo = (ModelObject) cc;
		return mo;
	}

	protected Model getModel(ModelObject mo)
	{
		if (mo == null)
			return null;
		if (mo instanceof Model)
			return (Model) mo;
		if (((ModelElement) mo).getRefId() != null)
			return getRepository().getModel(((ModelElement) mo).getRefId(), true);
		
		return null;
	}
}

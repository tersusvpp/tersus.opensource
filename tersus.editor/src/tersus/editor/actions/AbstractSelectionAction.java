/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.actions;

import java.util.Collections;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;

import tersus.editor.Debug;
import tersus.editor.RepositoryManager;
import tersus.editor.explorer.ExplorerEntry;
import tersus.editor.explorer.ISelectionAction;
import tersus.editor.explorer.PackageEntry;
import tersus.model.Model;
import tersus.model.ModelObject;
import tersus.util.Trace;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 *  
 */
public abstract class AbstractSelectionAction extends Action implements
        ISelectionAction
{

    protected abstract boolean calculateEnabled();
    public abstract void run();
    private ISelectionProvider selectionProvider;

    /**
     * @param part
     */
    public AbstractSelectionAction()
    {
        super();
        setEnabled(false);
    }

    public void setSelectionProvider(ISelectionProvider provider)
    {
        this.selectionProvider = provider;
    }
    
    protected PackageEntry getPackageEntry()
    {
        if (getSelectedObjects().size() == 1
                && getSelectedObjects().get(0) instanceof PackageEntry)
            return (PackageEntry) getSelectedObjects().get(0);
        else
            return null;
    }

    protected List<Object> getSelectedObjects()
    {
        if (!(getSelection() instanceof IStructuredSelection))
            return Collections.emptyList();
        return ((IStructuredSelection) getSelection()).toList();
    }

    protected IProject getProject()
    {
        return (IProject) getSelectedObject(IProject.class);
    }

    protected ModelObject getModelObject()
    {
        return (ModelObject) getSelectedObject(ModelObject.class);
    }

    protected Object getSelectedObject(Class adapter)
    {
        Object selectedObject = getSelectedObject();
        if (selectedObject != null)
        {
            if (!adapter.isAssignableFrom(selectedObject.getClass()))
            {
                if (Debug.OTHER)
                    Trace.add("Selected Object:" + selectedObject);
                if (selectedObject instanceof IAdaptable)
                {
                    selectedObject = ((IAdaptable) selectedObject)
                            .getAdapter(adapter);
                }
                else
                    selectedObject = null;
            }
        }
        return selectedObject;
    }
    
    protected ExplorerEntry getSelectedEntry()
    {
        return (ExplorerEntry) getSelectedObject(ExplorerEntry.class);
    }

    protected Object getSelectedObject()
    {
        Object selectedObject = null;
        if (getSelectedObjects().size() == 1)
        {
            selectedObject = getSelectedObjects().get(0);
        }
        return selectedObject;
    }

    protected Model getModel()
    {
        if (getModelObject() instanceof Model)
            return (Model) getModelObject();
        else
            return null;
    }

    protected RepositoryManager getRepositoryManager()
    {
        ExplorerEntry entry = (ExplorerEntry) getSelectedObject(ExplorerEntry.class);
        IProject project = null;
        if (entry != null)
            project = entry.getProject();
        else if (getModelObject() != null)
            project = ((WorkspaceRepository) getModelObject().getRepository())
                    .getProject();
        if (project == null)
            return null;
        return RepositoryManager.getRepositoryManager(project);
    }

    public ISelectionProvider getSelectionProvider()
    {
        return selectionProvider;
    }

    public ISelection getSelection()
    {
        return selectionProvider.getSelection();
    }
    
    public void update()
    {
        setEnabled(calculateEnabled());
    }
 
    public Shell getShell()
    {
      
        return TersusWorkbench.getActiveWorkbenchShell();
    }
}
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.actions;

import java.util.Iterator;

import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.ui.IEditorPart;

import tersus.editor.editparts.TersusEditPart;
import tersus.model.ModelObject;
import tersus.util.Misc;

/**
 * NICE2 [Description]
 * 
 * @author Youval Bronicki
 *
 */
public abstract class SetPropertyAction extends SelectionAction
{

	/**
	 * @param editor
	 */
	public SetPropertyAction(IEditorPart editor)
	{
		super(editor);
		// TODO Auto-generated constructor stub
	}

	/**
		 * The name of the property
		 */
	protected String propertyName;

	/**
		 * The category of the propery - either MODEL or ELEMENT
		 */
	protected String category;

	protected static final String MODEL = "Model";

	protected static final String ELEMENT = "Element";

	protected abstract boolean calculateEnabled(TersusEditPart selectedPart);

	protected boolean calculateEnabled()
	{
		Iterator i = getSelectedObjects().iterator();
	
		if (!i.hasNext())
			return false;
		Object selectedObject = i.next();
		if (!(selectedObject instanceof TersusEditPart))
			return false;
		
		return calculateEnabled((TersusEditPart) selectedObject);
	}

	/**
		* @param selectedObject
		* @return
		*/
	protected ModelObject getTarget(Object selectedObject)
	{
		if (!(selectedObject instanceof TersusEditPart))
			return null;
		TersusEditPart part = (TersusEditPart) selectedObject;
		if (ELEMENT.equals(category))
			return part.getModelElement();
		else
		{
			Misc.assertion(MODEL.equals(category));
			return part.getTersusModel();
		}
	}

	protected ModelObject getFirstSelectedObject()
	{
		if (getSelectedObjects().size() == 0)
			return null;
		ModelObject target = getTarget(getSelectedObjects().get(0));
		return target;
	}

}

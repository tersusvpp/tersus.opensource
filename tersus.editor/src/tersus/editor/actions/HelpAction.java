/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.help.WorkbenchHelpSystem;

import tersus.editor.TersusEditor;
import tersus.editor.preferences.Preferences;
import tersus.model.Model;
import tersus.workbench.TersusWorkbench;

/**
 * @author Liat Shiff
 */
public class HelpAction extends EditorSelectionAction
{
	public HelpAction(TersusEditor editor)
	{
		super(editor);
		setId(TersusActionConstants.HELP_ACTION_ID);
		setText(TersusActionConstants.HELP_ACTION_TEXT);
	}

	public boolean calculateEnabled()
	{
		if (getSelectedObjects().size() != 1)
			return false;

		return true;
	}

	public void run()
	{
		if (TersusWorkbench.getActiveWorkbenchWindow().getActivePage().getActiveEditor()
				== ((TersusEditor)this.getWorkbenchPart()).getViewer().getEditor())
		{
			String pluginString = getDocumentation();
			try
			{
				// Checks the Internet connection
				InetAddress.getByName("www.tersus.com");
	
				final String pluginHelpURL = Preferences.HELP_URL + pluginString;
				getShell().getDisplay().syncExec(new Runnable()
				{
					public void run()
					{
						TersusEditor.openBrowser(pluginHelpURL);
					}
				});
			}
			catch (UnknownHostException ex)
			{
				WorkbenchHelpSystem helpSystem = (WorkbenchHelpSystem) PlatformUI.getWorkbench()
						.getHelpSystem();
				
				helpSystem
						.displayHelpResource("topic=/tersus.help/html/" + pluginString);
				
			}
		}
	}
	
	private String getDocumentation()
	{
		Model model = getSelectedModel();
		return model.getDocumentation();
	}
}

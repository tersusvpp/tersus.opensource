/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;

import tersus.editor.editparts.SubFlowEditPart;
import tersus.editor.editparts.TopFlowEditPart;
import tersus.editor.figures.NoteEditPart;
import tersus.editor.properties.ModelObjectWrapper;
import tersus.model.BuiltinProperties;
import tersus.model.Note;
import tersus.model.commands.SetPropertyValueCommand;
import tersus.util.Misc;
import tersus.workbench.TersusWorkbench;

/**
 * @author Youval Bronicki
 * 
 */
public class EditNoteAction extends SelectionAction
{
	/**
	 * @param part
	 */
	public EditNoteAction(IWorkbenchPart part)
	{
		super(part);
		setText(TersusActionConstants.EDIT_NOTE_TEXT);
		setId(TersusActionConstants.EDIT_NOTE_ID);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#calculateEnabled()
	 */
	protected boolean calculateEnabled()
	{
		Object selectedObject = getSelectedObject();

		if (selectedObject instanceof NoteEditPart)
		{
			if (((NoteEditPart) selectedObject).getParent() instanceof SubFlowEditPart)
			{
				SubFlowEditPart subFlowEditPart = (SubFlowEditPart) ((NoteEditPart) selectedObject)
						.getParent();
				return !((ModelObjectWrapper) (subFlowEditPart).getModel()).getModel().getPackage()
						.isReadOnly();
			}
			if (((NoteEditPart) selectedObject).getParent() instanceof TopFlowEditPart)
			{
				TopFlowEditPart topFlowEditPart = (TopFlowEditPart) ((NoteEditPart) selectedObject)
						.getParent();
				return !((ModelObjectWrapper) (topFlowEditPart).getModel()).getModel().getPackage()
						.isReadOnly();
			}
		}
		return false;
	}

	protected Object getSelectedObject()
	{
		Object selectedObject = null;
		if (getSelectedObjects().size() == 1)
		{
			selectedObject = getSelectedObjects().get(0);
		}
		return selectedObject;
	}

	public void run()
	{
		Shell shell = getWorkbenchPart().getSite().getShell();
		EditNoteDialog dialog = new EditNoteDialog(shell);
		NoteEditPart noteEditPart = (NoteEditPart) getSelectedObject();
		Note note = (Note) noteEditPart.getModelElement();
		String originalComment = (String) note.getProperty(BuiltinProperties.COMMENT);
		dialog.setComment(originalComment);
		if (dialog.open() == Dialog.OK)
		{
			String comment = dialog.getComment();
			if (!Misc.equal(comment, originalComment))
			{
				comment = Misc.replaceAll(comment, "\r\n", "\n");
				comment = Misc.replaceAll(comment, "\r", "\n");

				SetPropertyValueCommand command = new SetPropertyValueCommand(note,
						BuiltinProperties.COMMENT, comment);

				try
				{
					getCommandStack().execute(command);
				}
				catch (Exception e)
				{
					StringBuffer message = new StringBuffer();
					message.append(e.getMessage());

					MessageDialog.openInformation(TersusWorkbench.getActiveWorkbenchShell(),
							"Read Only Package/File", message.toString());
				}
			}
		}
	}
}

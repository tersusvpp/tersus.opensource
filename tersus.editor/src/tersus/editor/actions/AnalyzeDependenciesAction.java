/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.ui.actions.UpdateAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.internal.editors.text.NonExistingFileEditorInput;
import org.eclipse.ui.part.FileEditorInput;

import tersus.editor.EditorPlugin;
import tersus.editor.TersusEditor;
import tersus.editor.outline.ModelObjectTransfer;
import tersus.model.FlowModel;
import tersus.model.Model;
import tersus.model.ModelObject;
import tersus.model.Role;
import tersus.model.commands.PasteElementsCommand;

public class AnalyzeDependenciesAction extends EditorSelectionAction implements
		UpdateAction
{

	private ArrayList<Command> commands;

	public AnalyzeDependenciesAction(TersusEditor editor)
	{
		super(editor);
		setId(TersusActionConstants.ANALYZE_DEPENDENCIES_ACTION_ID);
		setText(TersusActionConstants.ANALYZE_DEPENDENCIES_ACTION_TEXT);
	}

	public void run()
	{
		StringBuffer report = new StringBuffer();
		getSelectedFlowModel().updateRanks(report);
        IFile file = ((TersusEditor)getWorkbenchPart()).getRepository().getProject().getFile("dependencies.txt");
        final ByteArrayInputStream contentBytes = new ByteArrayInputStream(
                report.toString().getBytes());
        try
        {
            if (file.exists())
                file.setContents(contentBytes, IResource.FORCE, null);
            else
                file.create(contentBytes, IResource.FORCE, null);
            PlatformUI.getWorkbench().getActiveWorkbenchWindow()
                    .getActivePage().openEditor(new FileEditorInput(file),
                            EditorsUI.DEFAULT_TEXT_EDITOR_ID);
        }
        catch (CoreException e)
        {
            MessageDialog.openError(Display.getCurrent().getActiveShell(),
                    "Error", "Can't save file" + file.getFullPath());
        }

	}


	protected boolean calculateEnabled()
	{
		return getSelectedModel() instanceof FlowModel;
	}



}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.actions;

import org.eclipse.gef.commands.Command;

import tersus.editor.EditorMessages;
import tersus.model.PackageId;
import tersus.util.Misc;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 *  
 */
public class RenamePackageCommand extends Command implements ValidatingCommand
{

    String errorMessage;

    private WorkspaceRepository repository;

    private PackageId originalPackageId;

    private PackageId newPackageId;

    /**
     * @param repository
     * @param packageId
     * @param newName
     */
    public RenamePackageCommand(WorkspaceRepository repository,
            PackageId packageId, String newName)
    {
        if (newName == null || newName.length() == 0)
            errorMessage = EditorMessages.Error_Model_Name_Not_Specified;
        else if (!Misc.isValidFileName(newName))
            errorMessage = EditorMessages.Error_Invalid_Package_Name;
        else if (packageId.getName().equals(newName))
            errorMessage = EditorMessages.Error_Same_Name;
        else
        {
            this.repository = repository;
            this.originalPackageId = packageId;
            this.newPackageId = new PackageId(packageId.getParent(), newName);
        }

    }
    
    public RenamePackageCommand(WorkspaceRepository repository, PackageId originalPackageId, PackageId newPackageId)
    {
       this.repository = repository;
       this.originalPackageId = originalPackageId;
       this.newPackageId = newPackageId;
    	
    }

    public String validate()
    {
        if (errorMessage != null)
            return errorMessage;
        PackageId existingPackageId = repository
                .getSubPackageIdCaseInsensitive(newPackageId.getParent(),
                        newPackageId.getName());
        if (existingPackageId != null)
            return EditorMessages.format(
                    EditorMessages.Error_Package_Exists_Key, existingPackageId
                            .getPath());
        else
            return null;

    }

    public boolean canExecute()
    {
        return validate() == null;
    }

    public void execute()
    {

        repository.renamePackage(originalPackageId, newPackageId);

    }

 

    public void undo()
    {
        repository.renamePackage(newPackageId, originalPackageId);
    }
}
/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;

import tersus.editor.TersusEditor;
import tersus.editor.editparts.SlotEditPart;
import tersus.model.ModelElement;

/**
 * @author Liat Shiff
 */
public class CopyElementNameAction extends EditorSelectionAction
{

	public CopyElementNameAction(TersusEditor editor)
	{
		super(editor);
		
		setId(TersusActionConstants.COPY_ELEMENT_NAME_ACTION_ID);
		setText(TersusActionConstants.COPY_ELEMENT_NAME_ACTION_TEXT);
	}

	protected boolean calculateEnabled()
	{
		if (getSelectedEditPart() != null)
			return true;

		return false;
	}
	
	public void run() 
	{
		String value;
		
		
		ModelElement element = getSelectedModelElement();
		if (element != null)
			value = element.getElementName();
		else if (getSelectedEditPart() instanceof SlotEditPart)
		{
			SlotEditPart slotEditPart = (SlotEditPart)getSelectedEditPart();
			value = slotEditPart.getSlot().getElementName();
		}
		else
			value = getSelectedModel().getName();
			
		Clipboard clipboard = new Clipboard(getShell().getDisplay());
		TextTransfer textTransfer = TextTransfer.getInstance();

		clipboard.setContents(new String[]
		{ value }, new Transfer[]
		{ textTransfer });
		clipboard.dispose();
	}
}

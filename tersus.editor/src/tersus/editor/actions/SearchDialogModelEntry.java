/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import org.eclipse.core.resources.IProject;
import org.eclipse.swt.graphics.Image;

import tersus.model.BuiltinProperties;
import tersus.model.ModelId;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public class SearchDialogModelEntry extends SearchDialogEntry
{
	private ModelId modelId;

	private String iconPath;

	public SearchDialogModelEntry(ModelId modelId, SearchDialogPackageEntry parentPackageEntry,
			IProject project, WorkspaceRepository repository)
	{
		super(repository, parentPackageEntry, project);
		this.modelId = modelId;

		iconPath = (String) (repository.getModel(modelId, true))
				.getProperty(BuiltinProperties.ICON_FOLDER);
	}

	public ModelId getModelId()
	{
		return modelId;
	}

	public Image getImage()
	{
		if (iconPath != null)
			return getRepository().getImageLocator(iconPath).get16x16Image();
		else
			return null;
	}
}

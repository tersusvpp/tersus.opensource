/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.actions;

import tersus.editor.TersusEditor;
import tersus.editor.editparts.FlowDataEditPart;
import tersus.editor.editparts.TersusEditPart;
import tersus.model.BuiltinProperties;
import tersus.model.FlowDataElementType;
import tersus.util.EnumFactory;

/**
 * An action for setting the 'type' property of data elements
 * 
 * @author Youval Bronicki
 *
 */
public class SetFlowDataElementType extends SetEnumPropertyAction
{

	/**
	 * @param editor
	 * @param propertyName
	 * @param category
	 * @param factory
	 */
	public SetFlowDataElementType(TersusEditor editor)
	{
		super(editor, BuiltinProperties.TYPE, ELEMENT, new EnumFactory(FlowDataElementType.class));
		setId(TersusActionConstants.SET_FLOW_DATA_ELEMENT_TYPE);
	}

	/* (non-Javadoc)
	 * @see tersus.editor.actions.SetPropertyAction#calculateEnabled(tersus.editor.editparts.TersusEditPart)
	 */
	protected boolean calculateEnabled(TersusEditPart selectedPart)
	{
		return (selectedPart instanceof FlowDataEditPart);
	}

}

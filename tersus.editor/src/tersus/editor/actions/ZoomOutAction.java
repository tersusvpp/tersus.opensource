/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.actions;

import org.eclipse.gef.internal.GEFMessages;
import org.eclipse.gef.internal.InternalImages;
import org.eclipse.gef.ui.actions.GEFActionConstants;

import tersus.editor.Debug;
import tersus.editor.TersusZoomManager;
import tersus.editor.useractions.ZoomOutUserAction;
import tersus.util.Trace;

/**
 * 
 */
public class ZoomOutAction extends ZoomAction
{

	/**
	 * @param zoomManager
	 */
	public ZoomOutAction(TersusZoomManager zoomManager)
	{
		super(GEFMessages.ZoomOut_Label, InternalImages.DESC_ZOOM_OUT, zoomManager);
		setToolTipText(GEFMessages.ZoomOut_Tooltip);
		setId(GEFActionConstants.ZOOM_OUT);
		setActionDefinitionId(GEFActionConstants.ZOOM_OUT);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.IAction#run()
	 */
	public void run()
	{
		if (Debug.USER_ACTIONS)
			Trace.push(new ZoomOutUserAction());
		try
		{
			zoomOut();
		}
		finally
		{
			if (Debug.USER_ACTIONS)
				Trace.pop();
		}
	}

}

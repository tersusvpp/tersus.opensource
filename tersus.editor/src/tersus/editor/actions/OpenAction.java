/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.actions;

import java.util.Iterator;

import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.ui.IEditorPart;

import tersus.editor.Debug;
import tersus.editor.editparts.FlowEditPart;
import tersus.editor.editparts.ModelEditPart;
import tersus.util.Trace;

/**
 * 
 */
public class OpenAction extends SelectionAction
{

	/**
	 * @param editor
	 */
	public OpenAction(IEditorPart editor)
	{
		super(editor);
		setId(TersusActionConstants.OPEN_ACTION_ID);
		setText(TersusActionConstants.OPEN_ACTION_TEXT);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.ui.actions.EditorPartAction#calculateEnabled()
	 */
	protected boolean calculateEnabled()
	{
		if (Debug.OTHER)
			Trace.push("OpenAction.calculateEnabled()");
		try
		{

			Iterator i = getSelectedObjects().iterator();

			while (i.hasNext())
			{
				Object selectedObject = i.next();
				if (Debug.OTHER)
					Trace.add("Selected Object:" + selectedObject);
				if (selectedObject instanceof ModelEditPart)
				{
				    ModelEditPart selectedPart = (ModelEditPart) selectedObject;
					if (selectedPart.isComposite())
					{
						if (Debug.OTHER)
							Trace.add("returning true");
						return true;
					}
				}
			}
			if (Debug.OTHER)
				Trace.add("returning false");
			return false;
		}

		finally
		{
			if (Debug.OTHER)
				Trace.pop();
		}
	}
	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.IAction#run()
	 */
	public void run()
	{
		// TODO Auto-generated method stub
		if (Debug.OTHER)
			Trace.push("OpenAction.run()");
		try
		{
			Iterator i = getSelectedObjects().iterator();
			if (!i.hasNext())
				if (Debug.OTHER)
					Trace.add("No Object Selected");

			while (i.hasNext())
			{
				Object obj = i.next();
				if (obj instanceof ModelEditPart)
					 ((ModelEditPart) obj).setOpen(true);

			}
		}
		finally
		{
			if (Debug.OTHER)
				Trace.pop();
		}
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.ui.actions.UpdateAction;

import tersus.editor.TersusEditor;
import tersus.model.Model;
import tersus.model.ModelObject;
import tersus.model.commands.PasteElementsCommand;

public class PasteElementsAction extends EditorSelectionAction implements UpdateAction
{

	public PasteElementsAction(TersusEditor editor)
	{
		super(editor);
		setId(TersusActionConstants.PASTE_ELEMENTS_ACTION_ID);
		setText(TersusActionConstants.PASTE_ELEMENTS_ACTION_TEXT);
		setToolTipText(TersusActionConstants.PASTE_ELEMENTS_ACTION_TOOLTIP);
	}

	@SuppressWarnings("unchecked")
	public void run()
	{
		Object contents = getClipboardContents();
		Model targetModel = getSelectedModel();
		List<ModelObject> objects;
		if (contents instanceof List)
			objects = (List<ModelObject>) contents;
		else
		{
			objects = new ArrayList<ModelObject>(1);
			objects.add((ModelObject) contents);
		}
		Command command = new PasteElementsCommand(targetModel, objects);
		((TersusEditor) getWorkbenchPart()).getEditDomain().getCommandStack().execute(command);

	}

	protected boolean calculateEnabled()
	{
		return getSelectedModel() != null && !getSelectedModel().getPackage().isReadOnly()
				&& getRelevantElements().size() > 0;
	}

	@SuppressWarnings("unchecked")
	private List<ModelObject> getRelevantElements()
	{
		Model parentModel = getSelectedModel();
		if (parentModel == null)
			return Collections.emptyList();
		ArrayList<ModelObject> list = new ArrayList<ModelObject>();
		Object cc = getClipboardContents();
		if (cc instanceof ModelObject)
		{
			if (PasteElementsCommand.checkValidChild(cc, parentModel, true))
				list.add((ModelObject) cc);
		}
		else if (cc instanceof List)
		{
			for (Object obj : (List<Object>) cc)
			{
				if ((PasteElementsCommand.checkValidChild(obj, parentModel, true) && (obj instanceof ModelObject)))
					list.add((ModelObject) obj);
			}

		}
		return list;
	}

	private boolean isInSameProject(ModelObject obj)
	{
		return obj != null
				&& obj.getRepository() == ((TersusEditor) getWorkbenchPart()).getRepository();
	}

	@Override
	public void update()
	{
		// TODO Auto-generated method stub
		super.update();
		updateLabel();
	}

	private void updateLabel()
	{
		boolean reuseModels = false;
		boolean importModels = false;
		for (ModelObject obj : getRelevantElements())
		{
			if (isInSameProject(obj))
				reuseModels = true;
			else
				importModels = true;
		}
		if (reuseModels && importModels)
			setText("Paste (Reuse,Import)");
		else if (importModels)
			setText("Paste (Import)");
		else
			setText("Paste (Reuse)");

	}

}

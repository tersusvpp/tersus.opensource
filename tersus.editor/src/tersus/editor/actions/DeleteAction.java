/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.actions;

import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.actions.DeleteResourceAction;

import tersus.editor.Debug;
import tersus.editor.EditorMessages;
import tersus.editor.RepositoryManager;
import tersus.editor.explorer.PackageEntry;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.PackageId;
import tersus.model.commands.CommandFailedException;
import tersus.model.commands.DeleteModelAndPackageCommand;
import tersus.model.commands.DeletePackageCommand;
import tersus.util.Trace;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * Deletes a model (in the future - a set of models)
 * 
 * @author Youval Bronicki
 */

public class DeleteAction extends RepositoryAction
{

    DeleteResourceAction deleteResourceAction;
    /**
     * @param editor
     */
    public DeleteAction(IWorkbenchPart part)
    {
        super(part);
        setId(TersusActionConstants.DELETE_ACTION_ID);
        setText(TersusActionConstants.DELETE_ACTION_TEXT);
        deleteResourceAction = new DeleteResourceAction(getShell());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.gef.ui.actions.EditorPartAction#calculateEnabled()
     */
    protected boolean calculateEnabled()
    {
        if (Debug.OTHER)
            Trace.push("RenameAction.calculateEnabled()");
        try
        {
            if (getModel() != null)
                return true;
            else if (getPackageEntry() != null)
                return true;
            else if (getProject() != null)
                return true;
            else
                return false;
        }

        finally
        {
            if (Debug.OTHER)
                Trace.pop();
        }
    }

    public void run()
    {
        if (getModel() != null)
            deleteModel();
        else if (getPackageEntry() != null)
            deletePackage();
        else if (getProject() != null)
            deleteProject();
    }

    private void deleteProject()
    {
        RepositoryManager repositoryManger = getRepositoryManager();
        repositoryManger.closeOpenEditors();
        deleteResourceAction.selectionChanged((IStructuredSelection)getSelection());
        deleteResourceAction.run();
        
        if (getProject() == null)
        	repositoryManger.dispose();
    }

    private void deletePackage()
    {
        PackageEntry packageEntry = getPackageEntry();
        PackageId packageId = packageEntry.getPackageId();
        if (Debug.OTHER)
            Trace.push("DeleteAction.run( Deleting package '"+packageId+"')");
        try
        {
            RepositoryManager editDomain = packageEntry.getEditDomain();
            WorkspaceRepository repository = editDomain.getRepository();
            List<ModelElement> references = repository.getExternalReferences(packageId);
            if (references.size() > 0)
            {
                StringBuffer message = new StringBuffer();
                message.append(EditorMessages.format(
                        EditorMessages.PackageInUseDialog_Message_Key, packageId
                                ));
                for (ModelElement element: references)
                {
                    message.append('\n');
                    message.append(element.getParentModel().getId() + " ["
                            + element.getRole() + "] -> " + element.getRefId());
                }
                MessageDialog.openInformation(TersusWorkbench
                        .getActiveWorkbenchShell(),
                        EditorMessages.ModelInUseDialog_Title, message
                                .toString());
                System.out.println("\nUsages of package "+packageId+"\n------------------");
                System.out.println(message);
                System.out.println("\n\n");
                return; // No action
            }
            
            DeletePackageCommand command = new DeletePackageCommand(repository, packageId);
            
            try 
            {
            	editDomain.getCommandStack().execute(command);
			} 
            catch (CommandFailedException e) 
            {
            	StringBuffer message = new StringBuffer();
            	message.append(e.getMessage());
            
            	MessageDialog.openInformation(TersusWorkbench.getActiveWorkbenchShell(),
                        "Read Only Package/File", message
                                .toString());
			}
        }
        finally
        {
            if (Debug.OTHER)
                Trace.pop();
        }
    }
    private void deleteModel()
    {	
        WorkspaceRepository repository = null;
        Model model = getModel();
        
        if (Debug.OTHER)
            Trace.push("DeleteAction.run( Deleting model '"+model.getId()+"')");
        try
        {
            if (model == null)
            {
                if (Debug.OTHER)
                    Trace
                            .add("No model selected (or multiple models selected)");
            }
            RepositoryManager repositoryManager = getRepositoryManager();
            repositoryManager.closeOpenEditors(model);
            repository = repositoryManager.getRepository();
            List<ModelElement> references = repository.getReferences(model.getId());
            if (references.size() > 0)
            {
                StringBuffer message = new StringBuffer();
                message.append(EditorMessages.format(
                        EditorMessages.ModelInUseDialog_Message_Key, model
                                .getId()));
                for (ModelElement element: references)
                {
                    message.append('\n');
                    message.append(element.getParentModel().getId() + " ["
                            + element.getRole() + "]");
                }
                MessageDialog.openInformation(TersusWorkbench
                        .getActiveWorkbenchShell(),
                        EditorMessages.ModelInUseDialog_Title, message.toString());
                return; // No action
            }
            if (model != null)
            {
                DeleteModelAndPackageCommand command = new DeleteModelAndPackageCommand(model);
                
                try 
                {
                	repositoryManager.getCommandStack().execute(command);
    			} 
                catch (CommandFailedException e) 
                {
                	StringBuffer message = new StringBuffer();
                	message.append(e.getMessage());
                
                	MessageDialog.openInformation(TersusWorkbench.getActiveWorkbenchShell(),
                            "Read Only Package/File", message.toString());
    			}
            }
        }
        finally
        {
            if (Debug.OTHER)
                Trace.pop();
        }
    }

}
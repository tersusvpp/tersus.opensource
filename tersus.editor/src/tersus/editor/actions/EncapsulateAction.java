/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.actions;

import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.gef.commands.Command;
import org.eclipse.jface.dialogs.ErrorDialog;

import tersus.editor.TersusEditor;
import tersus.model.InvalidLinkException;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.model.commands.EncapsulateCommand;
import tersus.model.utils.MoveElementsUtils;
import tersus.workbench.TersusWorkbench;

/**
 * @author Liat Shiff
 */
public class EncapsulateAction extends EditorSelectionAction
{

	public EncapsulateAction(TersusEditor editor)
	{
		super(editor);
		setId(TersusActionConstants.ENCAPSULATE_ACTION_ID);
		setText(TersusActionConstants.ENCAPSULATE_ACTION_TEXT);
		setToolTipText(TersusActionConstants.ENCAPSULATE_ACTION_TOOLTIP);
	}

	@SuppressWarnings("unchecked")
	public void run()
	{
		if (!calculateEnabled())
			return;
		List<?> selection = getSelectedModelObjects();
		ModelElement first = (ModelElement) selection.get(0);
		List<ModelElement> selectedElements = (List<ModelElement>) selection;
		Command command = new EncapsulateCommand(first.getParentModel(), selectedElements);

		try
		{
			((TersusEditor) getWorkbenchPart()).getEditDomain().getCommandStack().execute(command);
		}
		catch (InvalidLinkException e)
		{
			Status status = new Status(IStatus.ERROR, TersusWorkbench.getPluginId(), 1, e
					.getMessage(), e);

			ErrorDialog.openError(getShell(), "Encapsulate Failed",
					"Encapsulate cannot be performed due to an invalid link. " + e, status);
		}
	}

	protected boolean calculateEnabled()
	{
		List<ModelObject> sel = getSelectedModelObjects();
		if (!isEnabledForMultiElementModification(sel,1,false,true,true)) // Preserving logic prior to isEnabledForMultiElementModification()
			return false;

		return MoveElementsUtils.validateDataElementEncapsulation(sel)
				|| MoveElementsUtils.validateDisplayElementEncapsulation(sel)
				|| MoveElementsUtils.validateProcessEncapsulation(sel)
				|| MoveElementsUtils.validateAncestorRefernceEncapsulation(sel);
	}

}

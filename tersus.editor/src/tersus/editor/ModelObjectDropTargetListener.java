/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.Request;
import org.eclipse.gef.dnd.AbstractTransferDropTargetListener;
import org.eclipse.jface.action.SubStatusLineManager;
import org.eclipse.swt.dnd.DND;

import tersus.editor.editparts.FlowEditPart;
import tersus.editor.editparts.TersusEditPart;
import tersus.editor.explorer.ModelEntry;
import tersus.editor.outline.ModelObjectTransfer;
import tersus.editor.requests.DropModelObjectRequest;
import tersus.editor.search.TersusSearchModelEntry;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.model.Note;
import tersus.workbench.WorkspaceRepository;


//TODO: understand the scope of this listener, and id it properly
//      (will there need to be other DropTargerListeners?)
public class ModelObjectDropTargetListener extends AbstractTransferDropTargetListener
{

    protected void handleDrop()
    {   
    	super.handleDrop();
        if (getCurrentEvent().detail != DND.DROP_NONE)
        {
            // Put the focus on the editor after a successful drop
            getViewer().getControl().getDisplay().asyncExec(new Runnable() {

                public void run()
                {
                    editor.getEditorSite().getPage().activate(editor);
                }
            });

        }
    }

    private TersusEditor editor;

    public ModelObjectDropTargetListener(TersusEditor editor,
            EditPartViewer viewer)
    {
        super(viewer, ModelObjectTransfer.getInstance());
        this.editor = editor;
        
    }

    protected Request createTargetRequest()
    {
        DropModelObjectRequest request = new DropModelObjectRequest();
        request.setLocation(getDropLocation());
        return request;
    }

    protected DropModelObjectRequest getDropModelObjectRequest()
    {
        return (DropModelObjectRequest) getTargetRequest();
    }

    protected void updateTargetRequest()
    {
        updateTargetEditPart();
        Point location = new Point(getDropLocation());
        getDropModelObjectRequest().setLocation(location);

        ModelObject sourceObject;
        if (getSourceObject() instanceof Note)
        {
            sourceObject = getSourceObject();
            
        }
        else
        {
            sourceObject = getSourceModel();
        }
        getDropModelObjectRequest().setSourceObject(sourceObject);
        TersusEditPart targetEditPart = ((TersusEditPart) getTargetEditPart());
        if (sourceObject != null && targetEditPart instanceof FlowEditPart)
        {
            getDropModelObjectRequest().setDefaultSize(targetEditPart);
            getDropModelObjectRequest().updateConstraint(targetEditPart);

        }
    }

    @SuppressWarnings("unchecked")
	protected ModelObject getSourceObject()
    {
        List<Object> objs = (List<Object>) ModelObjectTransfer.getInstance().getObject();
        if (objs.size() == 1)
        {
            Object obj = objs.get(0);
            if (obj instanceof ModelObject)
                return (ModelObject) obj;
            
            else if (obj instanceof IAdaptable && 
            		(obj instanceof ModelEntry || obj instanceof TersusSearchModelEntry))
            {
               	WorkspaceRepository repository = editor.getRepository();
            	IAdaptable objAdapter = (IAdaptable)obj;           	
            	ModelObject modelObject = (ModelObject) objAdapter.getAdapter(ModelObject.class);
            	IProject project = (IProject)objAdapter.getAdapter(IProject.class);
            	
            	if (project.equals(repository.getProject()))
            	{
            		if (modelObject instanceof Model)
            			return (Model)modelObject;
                	else
                	{
                		System.out.println("No such model");
                        editor.getEditorSite().getActionBars()
                                .getStatusLineManager().setMessage(
                                        "No such model" + modelObject);
                	}
            	}
            	else
                {
                	
            		SubStatusLineManager statusLineManager = 
                		(SubStatusLineManager)editor.getEditorSite().getActionBars().getStatusLineManager();
            		statusLineManager.setVisible(true);
                	statusLineManager.setErrorMessage("Drag and Drop between separate projects is not supported yet.");
                }
            }
        }
        else if (objs.size() > 1)
        {
        	System.out.println("Dropping multiple objects is not supported yet.");
            editor.getEditorSite().getActionBars().getStatusLineManager()
                    .setMessage(
                            "Dropping multiple objects is not supported yet.");
        }
        return null;
    }

    private Model getSourceModel()
    {
        ModelObject source = getSourceObject();
        getCurrentEvent().detail = DND.DROP_COPY;
        Model model = null;
        if (source instanceof ModelElement)
            model = ((ModelElement) source).getReferredModel();
        else
            model = (Model) source;
        return model;
    }

}
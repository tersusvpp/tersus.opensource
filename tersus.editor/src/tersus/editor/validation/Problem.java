/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.validation;

import java.beans.PropertyChangeSupport;

import tersus.model.BuiltinProperties;
import tersus.model.ElementPath;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.model.Path;
import tersus.util.IObservableItem;
import tersus.util.PropertyChangeListener;

/**
 * @author Youval Bronicki
 * 
 */
public class Problem implements IObservableItem
{
	private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

	private PropertyChangeListener propertyChangeListener = new PropertyChangeListener()
	{
		public void propertyChange(Object sourceObject, String property, Object oldValue,
				Object newValue)
		{
			boolean currentIgnoreProblems = isProblemIgnored();
			if (currentIgnoreProblems != ignore)
			{
				propertyChangeSupport.firePropertyChange(BuiltinProperties.IGNORED_PROBLEMS,
						ignore, currentIgnoreProblems);
				
				ignore = currentIgnoreProblems;
			}
		}
	};

	private Path path;

	private String problem;

	private String details;

	private Model root;

	private boolean ignore;

	public Problem(Model root, ElementPath path, String message, String details)
	{
		if (message == null)
			throw new IllegalArgumentException("Message must not be null");
		this.root = root;
		this.path = new Path();
		this.path.copy(path);
		this.problem = message;
		this.details = details;
		this.ignore = isProblemIgnored();

		getModelObject().addPropertyChangeListener(propertyChangeListener);
	}

	public String getProblem()
	{
		return problem;
	}

	public Path getPath()
	{
		if (path == null)
			return Path.EMPTY;
		else
			return path;
	}

	public String getDetails()
	{
		if (details == null)
			return "";
		else
			return details;
	}

	public Model getRoot()
	{
		return root;
	}

	public ModelElement getElement()
	{
		if (!Path.EMPTY.equals(getPath()))
			return getRoot().getElement(getPath());

		return null;
	}

	public boolean isProblemIgnored()
	{
		Object ignoredProblemObject = getModelObject().getProperty(
				BuiltinProperties.IGNORED_PROBLEMS);

		if (ignoredProblemObject != null && ignoredProblemObject instanceof String)
		{
			String ignoredProblemList = (String) ignoredProblemObject;
			for (String ignoredProblem : ignoredProblemList.split(","))
			{
				if (ignoredProblem.equals(getProblem()))
					return true;
			}
		}

		return false;
	}

	public ModelObject getModelObject()
	{
		if (getElement() != null)
			return getElement();
		else
			return getRoot();
	}

	public void addListener(java.beans.PropertyChangeListener listener)
	{
		propertyChangeSupport.addPropertyChangeListener(listener);

	}

	public void removeListener(java.beans.PropertyChangeListener listener)
	{
		propertyChangeSupport.removePropertyChangeListener(listener);

	}

	public void dispose()
	{
		getModelObject().removePropertyChangeListener(propertyChangeListener);
	}
}

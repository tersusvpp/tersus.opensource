package tersus.editor.validation.dtd;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DeclHandler;

import com.sun.org.apache.xerces.internal.parsers.SAXParser;

public class DtdReader implements DeclHandler
{
	HashMap<String, HashSet<String>> htmlTagHierarchy;

	public void parseDTD()
	{
		try
		{
			htmlTagHierarchy = new HashMap<String, HashSet<String>>();
			SAXParser parser = new SAXParser();
			parser.setProperty("http://xml.org/sax/properties/declaration-handler", this);

			URL htmlDtdUrl = getClass().getResource("html.dtd");
			InputStream is = createStarterFile(htmlDtdUrl, "html");
			InputSource source = new InputSource(is);
			parser.parse(source);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private InputStream createStarterFile(URL url, String rootName)
	{
		// This Method assume the name of the dtd file is the same as the name of the root element,
		// and it plant it in the xml_starter stream.
		// The xml_starter stream contain the name of the root element and a path of the dtd file
		InputStream is = null;
		try
		{
			String header = "<?xml version=\"1.0\" standalone=\"no\"?>" + "\r\n" + "<!DOCTYPE "
					+ rootName + " SYSTEM \"" + url.toString() + "\">" + "\r\n" + "<" + rootName
					+ "/>" + "\r\n";

			byte[] bytes = header.getBytes();
			is = new ByteArrayInputStream(bytes);
		}

		catch (Exception e)
		{
			e.printStackTrace();
		}

		return is;
	}

	public void attributeDecl(String eName, String aName, String type, String mode, String value)
			throws SAXException
	{
	}

	public void elementDecl(String name, String model) throws SAXException
	{
		if (!htmlTagHierarchy.containsKey(name))
			htmlTagHierarchy.put(name, new HashSet<String>());

		HashSet<String> tagList = htmlTagHierarchy.get(name);

		model = model.replace("(", "");
		model = model.replace(")", "");
		model = model.replace("#PCDATA", "");
		model = model.replace("|", " ");
		model = model.replace(",", " ");
		model = model.replace("+", "");
		model = model.replace("*", "");
		model = model.replace("?", "");
		model = model.trim();

		for (String tag : model.split(" "))
		{
			tagList.add(tag.trim().toLowerCase());
		}
	}

	public void externalEntityDecl(String name, String publicId, String systemId)
			throws SAXException
	{
	}

	public void internalEntityDecl(String name, String value) throws SAXException
	{
	}

	public HashMap<String, HashSet<String>> getHtmlTagHierarchy()
	{
		return htmlTagHierarchy;
	}
}

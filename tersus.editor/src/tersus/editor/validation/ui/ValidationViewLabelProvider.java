/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.validation.ui;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import tersus.editor.TersusEditor;
import tersus.editor.validation.Problem;
import tersus.model.validation.Problems;

/**
 * @author Liat Shiff
 */
public class ValidationViewLabelProvider extends LabelProvider implements ITableLabelProvider
{
	private static final Image EROOR = ImageDescriptor.createFromFile(TersusEditor.class,
			"icons/error.gif").createImage();

	private static final Image WARNING = ImageDescriptor.createFromFile(TersusEditor.class,
			"icons/warning.gif").createImage();

	private static final Image CHECKED = ImageDescriptor.createFromFile(TersusEditor.class,
			"icons/checked.gif").createImage();

	private static final Image UNCHECKED = ImageDescriptor.createFromFile(TersusEditor.class,
			"icons/unchecked.gif").createImage();

	public Image getColumnImage(Object element, int columnIndex)
	{
		Problem problem = (Problem) element;

		if (columnIndex == 0)
		{
			return CHECKED;
		}
		if (columnIndex == 2)
				return Problems.isWarning(problem.getProblem()) ? WARNING : EROOR;

		return null;
	}

	public String getColumnText(Object element, int columnIndex)
	{
		Problem problem = (Problem) element;
		if (columnIndex == 1)
			return problem.getRoot().getName();
		if (columnIndex == 2)
			return problem.getProblem();
		if (columnIndex == 3)
			return problem.getDetails();
		if (columnIndex == 4)
			return problem.getPath().toString();
		
		
			return null;
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.validation.ui;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;

import tersus.editor.validation.Problem;
import tersus.model.Model;

/**
 * @author Liat Shiff
 */
public class ValidationViewSorter extends ViewerSorter
{
	public static final String PROBLEM_SORT = "Problem";
	public static final String LOCATION_SORT = "Location";
	public static final String ROOT_MODEL_SORT = "Root Model";
	public static final String DETAILS_SORT = "Details";
	public static final String PACKAGE_ID_SORT = "Package Id";

	private String column = null;
	private int dir = SWT.DOWN;

	public ValidationViewSorter(String column, int dir)
	{
		super();
		this.column = column;
		this.dir = dir;
	}

	public int compare(Viewer viewer, Object e1, Object e2)
	{
		int returnValue = 0;

		if (PROBLEM_SORT.equals(column))
			returnValue = ((Problem) e1).getProblem().compareTo(((Problem) e2).getProblem());
		if (LOCATION_SORT.equals(column))
			returnValue = ((Problem) e1).getPath().toString().compareTo(
					((Problem) e2).getPath().toString());
		if (ROOT_MODEL_SORT.equals(column))
			returnValue = ((Problem) e1).getRoot().getName().compareTo(
					((Problem) e2).getRoot().getName());
		if (DETAILS_SORT.equals(column))
			returnValue = ((Problem) e1).getDetails().compareTo(((Problem) e2).getDetails());
		if (PACKAGE_ID_SORT.equals(column))
		{
			Model m1 = ((Problem) e1).getRoot();
			Model m2 = ((Problem) e2).getRoot();
			
			returnValue = m1.getPackageId().getPath().compareTo(m2.getPackageId().getPath());
		}

		if (this.dir == SWT.DOWN)
			returnValue = returnValue * -1;

		return returnValue;
	}
}

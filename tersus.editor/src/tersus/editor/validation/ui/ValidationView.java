/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.validation.ui;

import org.eclipse.core.resources.IProject;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;

import tersus.editor.EditorPlugin;
import tersus.editor.RepositoryManager;
import tersus.editor.SyncableView;
import tersus.editor.TersusEditDomain;
import tersus.editor.TersusEditor;
import tersus.editor.validation.Problem;
import tersus.editor.validation.ValidationEngine;
import tersus.editor.validation.ValidationListener;
import tersus.model.BuiltinProperties;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.model.Path;
import tersus.model.RelativeSize;
import tersus.model.commands.AddMetaPropertyCommand;
import tersus.model.commands.SetPropertyValueCommand;
import tersus.model.commands.TableFieldDescriptor;
import tersus.model.validation.Problems;
import tersus.util.Misc;
import tersus.workbench.SelectionHelper;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 * @author Liat Shiff
 */
public class ValidationView extends SyncableView
{
	private static final Image IGNORED_PROBLEM = ImageDescriptor.createFromFile(TersusEditor.class,
			"icons/ignoredProblem.gif").createImage();

	private static final Image EROOR = ImageDescriptor.createFromFile(TersusEditor.class,
			"icons/error.gif").createImage();

	private static final Image WARNING = ImageDescriptor.createFromFile(TersusEditor.class,
			"icons/warning.gif").createImage();

	public static final String ID = "tersus.editor.validation.ui.ValidationView";

	public static final String ERRORS_AND_UNIGNORED_WARNINGS = "Errors+Unignored Warnings";

	public static final String ERRORS = "Errors";

	public static final String UNIGNORED_WARNINGS = "Unignored Warnings";

	public static final String IGNORED_WARNINGS = "Ignored Warnings";

	public static final String ALL_WARNINGS = "All Warnings";

	public static final String ALL = "All";

	private Combo problemStateCombo;

	private TableViewer viewer;

	private ValidateAction validateAction;

	private Action openContainingModelAction;

	private Action ignoreWarningsAction;

	private Action cancelIgnoreWarningsAction;

	private IPartListener partListener;

	private Action showLocationAction;

	private IDialogSettings settings;

	// Editors:
	private CheckboxCellEditor ignoredProblemEditor;

	private TextCellEditor rootModelEditor;
	
	private TextCellEditor packageIdEditor;

	private TextCellEditor problemEditor;

	private TextCellEditor detailsEditor;

	private TextCellEditor locationEditor;

	public ValidationView()
	{
		super();
		settings = EditorPlugin.getDefault().getDialogSettings();
	}

	public void createPartControl(Composite parent)
	{
		Composite container = new Composite(parent, SWT.NULL);

		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		layout.horizontalSpacing = 1;
		layout.verticalSpacing = 0;
		layout.makeColumnsEqualWidth = false;
		container.setLayout(layout);

		createTopRow(container);
		addFields(container);

		validateAction = new ValidateAction();
		openContainingModelAction = new OpenContainingContextAction();
		showLocationAction = new ShowPathAction();
		ignoreWarningsAction = new IgnoreWarningsAction();
		ignoreWarningsAction.setEnabled(false);
		cancelIgnoreWarningsAction = new CancelIgnoreWarningsAction();
		cancelIgnoreWarningsAction.setEnabled(false);

		getViewSite().getActionBars().getToolBarManager().add(validateAction);
		getViewSite().getActionBars().getToolBarManager().add(openContainingModelAction);
		getViewSite().getActionBars().getToolBarManager().add(showLocationAction);
		getViewSite().getActionBars().getToolBarManager().add(ignoreWarningsAction);
		getViewSite().getActionBars().getToolBarManager().add(cancelIgnoreWarningsAction);

		hookMouseListener();
		hookSelectionListener();

		getViewSite().getPage().addPartListener(getPartListener());
		syncWithPage();
	}

	public void createTopRow(Composite parent)
	{
		Composite topRow = new Composite(parent, SWT.NULL);
		GridLayout topRowLayout = new GridLayout();
		topRowLayout.numColumns = 3;
		topRowLayout.verticalSpacing = 0;
		topRow.setLayout(topRowLayout);

		initProblemStateCombo(topRow);
	}

	private void addFields(Composite container)
	{
		configureTable(container);
		viewer.setContentProvider(new ValidationViewContentProvider(problemStateCombo.getText(),
				viewer));
		syncWithPage();
	}

	private void configureTable(Composite container)
	{
		Table table = new Table(container, SWT.BORDER | SWT.MULTI | SWT.FULL_SELECTION);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		table.setLayoutData(new GridData(GridData.FILL_BOTH));

		viewer = new TableViewer(table);

		Listener sorterListener = getSortListener(table);

		TableViewerColumn ignoredProblemColumn = new TableViewerColumn(viewer, SWT.HORIZONTAL);
		ignoredProblemColumn.getColumn().setWidth(67);
		ignoredProblemColumn.getColumn().setText("Ignored");
		ignoredProblemColumn.setLabelProvider(new CellLabelProvider()
		{
			public void update(ViewerCell cell)
			{
				Problem problem = (Problem) cell.getElement();

				if (problem.isProblemIgnored())
					cell.setImage(IGNORED_PROBLEM);
				else
					cell.setImage(null);
			}
		});

		ignoredProblemEditor = new CheckboxCellEditor(null, SWT.CHECK | SWT.HORIZONTAL);
		ignoredProblemColumn.setEditingSupport(new ValidationViewEditingSupoort(
				ignoredProblemEditor)
		{

			protected boolean canEdit(Object element)
			{
				return true;
			}

			protected void setValue(Object element, Object value)
			{
				System.out.println();
			}

			protected Object getValue(Problem problem)
			{
				return new Boolean(false);
			}
		});

		TableViewerColumn rootModelColumn = new TableViewerColumn(viewer, SWT.NONE);
		rootModelColumn.getColumn().setWidth(150);
		rootModelColumn.getColumn().setText("Root Model");
		rootModelColumn.getColumn().addListener(SWT.Selection, sorterListener);
		rootModelColumn.setLabelProvider(new CellLabelProvider()
		{
			public void update(ViewerCell cell)
			{
				Problem problem = (Problem) cell.getElement();
				cell.setText(problem.getRoot().getName());
			}

		});

		rootModelEditor = new TextCellEditor(viewer.getTable());
		rootModelColumn.setEditingSupport(new ValidationViewEditingSupoort(rootModelEditor)
		{
			protected void setValue(Object element, Object value)
			{
			}

			protected Object getValue(Problem problem)
			{
				return problem.getRoot().getName();
			}
		});
		
		TableViewerColumn packageIdColumn = new TableViewerColumn(viewer, SWT.NONE);
		packageIdColumn.getColumn().setWidth(150);
		packageIdColumn.getColumn().setText("Package Id");
		packageIdColumn.getColumn().addListener(SWT.Selection, sorterListener);
		packageIdColumn.setLabelProvider(new CellLabelProvider()
		{
			public void update(ViewerCell cell)
			{
				Problem problem = (Problem) cell.getElement();
				cell.setText(problem.getRoot().getPackageId().getPath());
			}

		});
		
		packageIdEditor = new TextCellEditor(viewer.getTable());
		packageIdColumn.setEditingSupport(new ValidationViewEditingSupoort(packageIdEditor)
		{
			protected void setValue(Object element, Object value)
			{
			}

			protected Object getValue(Problem problem)
			{
				return problem.getRoot().getPackageId().getPath();
			}
		});

		TableViewerColumn problemColumn = new TableViewerColumn(viewer, SWT.NONE);
		problemColumn.getColumn().setWidth(175);
		problemColumn.getColumn().setText("Problem");
		problemColumn.getColumn().addListener(SWT.Selection, sorterListener);
		problemColumn.setLabelProvider(new CellLabelProvider()
		{
			public void update(ViewerCell cell)
			{
				Problem problem = (Problem) cell.getElement();
				cell.setText(problem.getProblem());
				cell.setImage(Problems.isWarning(problem.getProblem()) ? WARNING : EROOR);
			}

		});
		
		problemEditor = new TextCellEditor(viewer.getTable());
		problemColumn.setEditingSupport(new ValidationViewEditingSupoort(problemEditor)
		{
			protected void setValue(Object element, Object value)
			{
				// TODO Auto-generated method stub
			}

			protected Object getValue(Problem problem)
			{
				return problem.getProblem();
			}
		});

		TableViewerColumn detailsColumn = new TableViewerColumn(viewer, SWT.NONE);
		detailsColumn.getColumn().setWidth(350);
		detailsColumn.getColumn().setText("Details");
		detailsColumn.getColumn().addListener(SWT.Selection, sorterListener);
		detailsColumn.setLabelProvider(new CellLabelProvider()
		{
			public void update(ViewerCell cell)
			{
				Problem problem = (Problem) cell.getElement();
				cell.setText(problem.getDetails());
			}

		});

		detailsEditor = new TextCellEditor(viewer.getTable());
		detailsColumn.setEditingSupport(new ValidationViewEditingSupoort(detailsEditor)
		{
			protected void setValue(Object element, Object value)
			{
				// TODO Auto-generated method stub
			}

			protected Object getValue(Problem problem)
			{
				return problem.getDetails();
			}
		});

		TableViewerColumn locationColumn = new TableViewerColumn(viewer, SWT.NONE);
		locationColumn.getColumn().setWidth(150);
		locationColumn.getColumn().setText("Location");
		locationColumn.getColumn().addListener(SWT.Selection, sorterListener);
		locationColumn.setLabelProvider(new CellLabelProvider()
		{
			public void update(ViewerCell cell)
			{
				Problem problem = (Problem) cell.getElement();
				cell.setText(problem.getPath().toString());
			}

		});

		locationEditor = new TextCellEditor(viewer.getTable());
		locationColumn.setEditingSupport(new ValidationViewEditingSupoort(locationEditor)
		{
			protected void setValue(Object element, Object value)
			{
				// TODO Auto-generated method stub
			}

			protected Object getValue(Problem problem)
			{
				return problem.getPath().toString();
			}

			protected boolean canEdit(Object element)
			{
				return false;
			}
		});

	}

	private void initProblemStateCombo(Composite parent)
	{
		problemStateCombo = new Combo(parent, SWT.DROP_DOWN | SWT.READ_ONLY);

		problemStateCombo.add(ERRORS_AND_UNIGNORED_WARNINGS);
		problemStateCombo.add(ERRORS);
		problemStateCombo.add(UNIGNORED_WARNINGS);
		problemStateCombo.add(IGNORED_WARNINGS);
		problemStateCombo.add(ALL_WARNINGS);
		problemStateCombo.add(ALL);
		
		problemStateCombo.setVisibleItemCount(6);
		
		String isAdvancedOpenString = settings.get("ValidationFilter");
		if (isAdvancedOpenString == null)
		{
			settings.put("ValidationFilter", ERRORS_AND_UNIGNORED_WARNINGS);
			problemStateCombo.setText(settings.get("ValidationFilter"));
		}
		else
			problemStateCombo.setText(isAdvancedOpenString);

		problemStateCombo.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				settings.put("ValidationFilter", problemStateCombo.getText());
				((ValidationViewContentProvider) viewer.getContentProvider())
						.changeFilter(problemStateCombo.getText());

				ignoreWarningsAction.setEnabled(!IGNORED_WARNINGS.equals(problemStateCombo
						.getText()) && !ERRORS.equals(problemStateCombo.getText()));
			}
		});
	}

	public void dispose()
	{
		getViewSite().getPage().removePartListener(getPartListener());
		super.dispose();
	}

	private IPartListener getPartListener()
	{
		if (partListener == null)
			partListener = new IPartListener()
			{

				public void partActivated(IWorkbenchPart part)
				{
					syncWithPage();
				}

				public void partBroughtToTop(IWorkbenchPart part)
				{
				}

				public void partClosed(IWorkbenchPart part)
				{
				}

				public void partDeactivated(IWorkbenchPart part)
				{
				}

				public void partOpened(IWorkbenchPart part)
				{
				}
			};
		return partListener;
	}

	protected void syncWithPage()
	{
		IWorkbenchPage activePage = TersusWorkbench.getActiveWorkbenchWindow().getActivePage();
		if (activePage == null)
			return;
		IEditorPart editor = activePage.getActiveEditor();
		if (editor instanceof TersusEditor)
		{
			ValidationEngine engine = ((TersusEditDomain) ((TersusEditor) editor).getEditDomain())
					.getRepositoryManager().getValidationEngine();
			setInput(engine);

		}
	}

	private void hookMouseListener()
	{
		viewer.getTable().addListener(SWT.MouseUp, new Listener()
		{
			public void handleEvent(Event event)
			{
				Table table = viewer.getTable();
				Point pt = new Point(event.x, event.y);
				int index = table.getTopIndex();

				while (index < table.getItemCount())
				{
					TableItem item = table.getItem(index);
					for (int i = 0; i < table.getColumnCount(); i++)
					{
						Rectangle rect = item.getBounds(i);
						if (rect.contains(pt))
							showAndHighlightSelected(i);
					}
					index++;
				}
			}

			private void showAndHighlightSelected(int columnNumber)
			{
				int selectionCount = viewer.getTable().getSelectionCount();
				IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();

				// It is possible to show highlight only when there is only one selection in the
				// table and the selected column is not the Ignore column.
				if ((columnNumber != 0) && selectionCount == 1
						&& selection.getFirstElement() instanceof Problem)
				{
					Problem problem = (Problem) selection.getFirstElement();
					Path path = problem.getPath();

					if (path.getNumberOfSegments() < 8 && checkFocus(path, problem.getRoot()))
					{
						Model root = problem.getRoot();
						showAndHighlight(root, path);
					}
					else
						openContainingModelAction.run();
				}
			}
		});
	}

	private void hookSelectionListener()
	{
		viewer.getTable().addSelectionListener(new SelectionListener()
		{
			public void widgetSelected(SelectionEvent e)
			{
				ignoreWarningsAction.setEnabled(isEnableIgnoreWarningsAction());
				cancelIgnoreWarningsAction.setEnabled(isEnableToCancelIgnoreWarningsAction());
			}

			public void widgetDefaultSelected(SelectionEvent e)
			{

			}
		});
	}

	private boolean isEnableIgnoreWarningsAction()
	{
		IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
		for (Object selectedObject : selection.toArray())
		{
			if (!(selectedObject instanceof Problem))
				return false;

			Problem selectedProblem = (Problem) selectedObject;
			if (!Problems.isWarning(selectedProblem.getProblem())
					|| selectedProblem.isProblemIgnored())
				return false;
		}
		return true;
	}
	
	private boolean isEnableToCancelIgnoreWarningsAction()
	{
		IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
		for (Object selectedObject : selection.toArray())
		{
			if (!(selectedObject instanceof Problem))
				return false;

			Problem selectedProblem = (Problem) selectedObject;
			if (!Problems.isWarning(selectedProblem.getProblem())
					|| !selectedProblem.isProblemIgnored())
				return false;
		}
		
		return true;
	}

	private static double MIN_SIZE = 1000000.0 / Integer.MAX_VALUE;

	private boolean checkFocus(Path path, Model baseModel)
	{
		double vSize = 1;
		double hSize = 1;
		for (int i = path.getNumberOfSegments() - 2; i > path.getNumberOfSegments(); i--)
		{
			ModelElement e = baseModel.getElement(path.getPrefix(i));
			if (e != null)
			{
				RelativeSize size = e.getSize();
				if (size != null)
				{
					vSize *= size.getHeight();
					hSize *= size.getWidth();
					if (Math.min(vSize, hSize) < MIN_SIZE)
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	private Listener getSortListener(final Table t)
	{
		Listener sortListener = new Listener()
		{
			public void handleEvent(Event event)
			{
				// determine new sort column and direction
				TableColumn sortColumn = t.getSortColumn();
				TableColumn selectedColumn = (TableColumn) event.widget;

				int dir = t.getSortDirection();
				if (sortColumn == selectedColumn)
					dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
				else
				{
					t.setSortColumn(selectedColumn);
					dir = SWT.UP;
				}

				// sort the data based on column and direction
				String sortIdentifier = null;

				if (ValidationViewSorter.PROBLEM_SORT.equals(selectedColumn.getText()))
					sortIdentifier = ValidationViewSorter.PROBLEM_SORT;
				if (ValidationViewSorter.LOCATION_SORT.equals(selectedColumn.getText()))
					sortIdentifier = ValidationViewSorter.LOCATION_SORT;
				if (ValidationViewSorter.ROOT_MODEL_SORT.equals(selectedColumn.getText()))
					sortIdentifier = ValidationViewSorter.ROOT_MODEL_SORT;
				if (ValidationViewSorter.DETAILS_SORT.equals(selectedColumn.getText()))
					sortIdentifier = ValidationViewSorter.DETAILS_SORT;
				if (ValidationViewSorter.PACKAGE_ID_SORT.equals(selectedColumn.getText()))
					sortIdentifier = ValidationViewSorter.PACKAGE_ID_SORT;

				t.setSortDirection(dir);
				viewer.setSorter(new ValidationViewSorter(sortIdentifier, dir));
			}
		};

		return sortListener;
	}

	public void setFocus()
	{
		viewer.getControl().setFocus();
	}

	private ValidationListener validationListener;

	private class ValidateAction extends Action
	{
		ValidateAction()
		{
			setText("Validate");
			setImageDescriptor(ImageDescriptor.createFromFile(TersusEditor.class,
					"icons/validate.gif"));
		}

		public void run()
		{
			ValidationViewContentProvider contentProvider = (ValidationViewContentProvider) viewer
					.getContentProvider();
			if (contentProvider.getEngine() instanceof ValidationEngine)
			{
				// Do not run in the background.
				((ValidationEngine) contentProvider.getEngine()).validate(false);
			}
		}

	};

	private class OpenContainingContextAction extends Action
	{
		OpenContainingContextAction()
		{
			setText("Open Containing Context");
			setImageDescriptor(ImageDescriptor.createFromFile(TersusEditor.class,
					"icons/openContainingContext.gif"));
		}

		public void run()
		{
			Problem problem = (Problem) SelectionHelper.getSelectedItem(viewer.getSelection(),
					Problem.class);
			if (problem == null)
			{
				MessageDialog.openInformation(getSite().getShell(), "Message",
						"Please select a problem");
				return;
			}
			Path path = problem.getPath();
			int levelsUp = 4;
			if (path.getNumberOfSegments() <= levelsUp)
				showAndHighlight(problem.getRoot(), path);
			else
			{
				int prefixLength = Math.max(0, path.getNumberOfSegments() - levelsUp);

				ModelElement element = problem.getRoot().getElement(path.getPrefix(prefixLength));

				if (element != null)
				{
					Model modelToOpen = element.getReferredModel();
					Path pathSuffix = path.getTail(path.getNumberOfSegments() - prefixLength);
					showAndHighlight(modelToOpen, pathSuffix);
				}
			}
		}
	};

	private class ShowPathAction extends Action
	{
		ShowPathAction()
		{
			setText("Show Path");
			setImageDescriptor(ImageDescriptor.createFromFile(TersusEditor.class,
					"icons/showPath.gif"));
		}

		public void run()
		{
			Problem problem = (Problem) SelectionHelper.getSelectedItem(viewer.getSelection(),
					Problem.class);
			if (problem == null)
			{
				MessageDialog.openInformation(getSite().getShell(), "Message",
						"Please select a problem");
				return;
			}
			Path path = problem.getPath();
			MessageDialog.openInformation(getSite().getShell(), "Error Location",
					Misc.replaceAll(path.toString(), "/", "/\n"));
		}
	};

	private class IgnoreWarningsAction extends Action
	{
		CompoundCommand command;

		IgnoreWarningsAction()
		{
			setText("Ignore selected warnings");
			setImageDescriptor(ImageDescriptor.createFromFile(TersusEditor.class,
					"icons/ignore.gif"));
		}

		public void run()
		{
			IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
			command = new CompoundCommand();

			for (Object selectedObject : selection.toArray())
			{
				Problem selectedProblem = (Problem) selectedObject;
				ModelObject modelObject = selectedProblem.getModelObject();

				ignore(modelObject, selectedProblem);

				IProject project = ((WorkspaceRepository) modelObject.getRepository()).getProject();
				RepositoryManager manager = RepositoryManager.getRepositoryManager(project);
				manager.getCommandStack().execute(command);
			}

			ignoreWarningsAction.setEnabled(isEnableIgnoreWarningsAction());
			cancelIgnoreWarningsAction.setEnabled(isEnableToCancelIgnoreWarningsAction());
		}

		private void ignore(ModelObject modelObject, Problem problem)
		{
			Object ignoredProblemObject = modelObject
					.getProperty(BuiltinProperties.IGNORED_PROBLEMS);

			if (ignoredProblemObject == null)
			{
				command.add(new AddMetaPropertyCommand(modelObject,
						BuiltinProperties.IGNORED_PROBLEMS, true, true));
				command.add(new SetPropertyValueCommand(modelObject,
						BuiltinProperties.IGNORED_PROBLEMS, problem.getProblem()));
			}
			else if (ignoredProblemObject instanceof String)
			{
				String ignoredProblemVal = (String) ignoredProblemObject;

				if (ignoredProblemVal.isEmpty())
					command.add(new SetPropertyValueCommand(modelObject,
							BuiltinProperties.IGNORED_PROBLEMS, problem.getProblem()));
				else

				{
					String newValue = ignoredProblemVal + "," + problem.getProblem();
					command.add(new SetPropertyValueCommand(modelObject,
							BuiltinProperties.IGNORED_PROBLEMS, newValue));
				}
			}
		}
	};

	private class CancelIgnoreWarningsAction extends Action
	{
		CompoundCommand command;

		CancelIgnoreWarningsAction()
		{
			setText("Cancel ignore on selected warnings");
			setImageDescriptor(ImageDescriptor.createFromFile(TersusEditor.class,
					"icons/cancel ignore.gif"));
		}

		public void run()
		{
			IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
			command = new CompoundCommand();

			for (Object selectedObject : selection.toArray())
			{
				Problem selectedProblem = (Problem) selectedObject;
				ModelObject modelObject = selectedProblem.getModelObject();

				cancelIgnore(modelObject, selectedProblem);

				IProject project = ((WorkspaceRepository) modelObject.getRepository()).getProject();
				RepositoryManager manager = RepositoryManager.getRepositoryManager(project);
				manager.getCommandStack().execute(command);
			}
			
			cancelIgnoreWarningsAction.setEnabled(isEnableToCancelIgnoreWarningsAction());
			ignoreWarningsAction.setEnabled(isEnableIgnoreWarningsAction());
		}

		private void cancelIgnore(ModelObject modelObject, Problem selectedProblem)
		{
			Object ignoredProblemObject = modelObject
					.getProperty(BuiltinProperties.IGNORED_PROBLEMS);

			if (ignoredProblemObject instanceof String)
			{
				String ignoredProblemVal = (String) ignoredProblemObject;
				if (!ignoredProblemVal.isEmpty())
				{
					StringBuffer newIgnoredProblemValue = new StringBuffer("");
					for (String ignoredProblem : ignoredProblemVal.split(","))
					{
						if (!ignoredProblem.equals(selectedProblem.getProblem()))
							newIgnoredProblemValue.append(ignoredProblem);
					}

					command.add(new SetPropertyValueCommand(modelObject,
							BuiltinProperties.IGNORED_PROBLEMS, newIgnoredProblemValue.toString()));
				}
			}
		}
	};

	private void setInput(ValidationEngine engine)
	{
		ValidationViewContentProvider provider = (ValidationViewContentProvider) viewer
				.getContentProvider();

		if (engine == provider.getEngine())
			return;
		if (provider.getEngine() instanceof ValidationEngine)
		{
			((ValidationEngine) provider.getEngine()).removeListener(getValidationListener());
		}

		viewer.setInput(engine.getProblems());
		provider.setEngine(engine);
		engine.addListener(getValidationListener());
	}

	private ValidationListener getValidationListener()
	{
		if (validationListener == null)
		{
			validationListener = new ValidationListener()
			{

				public void problemAdded(String problem)
				{
					getSite().getWorkbenchWindow().getShell().getDisplay()
							.syncExec(refreshRunnable);
				}

				public void problemsCleared()
				{
					getSite().getWorkbenchWindow().getShell().getDisplay()
							.syncExec(refreshRunnable);
				}

				public void refreshView()
				{
					getSite().getWorkbenchWindow().getShell().getDisplay()
							.syncExec(refreshRunnable);
				}
			};
		}
		return validationListener;
	}

	protected Runnable refreshRunnable = new Runnable()
	{
		public void run()
		{
			if (!viewer.getControl().isDisposed())
				viewer.refresh();
		}
	};

	abstract class ValidationViewEditingSupoort extends EditingSupport
	{
		private CellEditor editor;

		public ValidationViewEditingSupoort(CellEditor editor)
		{
			super(viewer);
			this.editor = editor;
		}

		protected boolean canEdit(Object element)
		{
			return false;
		}

		protected CellEditor getCellEditor(Object element)
		{
			return editor;
		}

		protected Object getValue(Object element)
		{
			return getValue((TableFieldDescriptor) element);
		}

		protected abstract Object getValue(Problem problem);

		protected abstract void setValue(Object element, Object value);

	}
}
/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.validation.ui;

import java.util.ArrayList;

import org.eclipse.jface.viewers.TableViewer;

import tersus.eclipse.utils.ListContentProvider;
import tersus.editor.validation.Problem;
import tersus.editor.validation.ValidationEngine;
import tersus.model.validation.Problems;
import tersus.util.IObservableList;
import tersus.util.ObservableList;

/**
 * @author Liat Shiff
 */
public class ValidationViewContentProvider extends ListContentProvider<Problem>
{
	private static final Object[] EMPTY_ARRAY = new Object[0];

	private String filter;

	private ValidationEngine engine;

	public ValidationViewContentProvider(String filter, TableViewer viewer)
	{
		super(viewer);
		this.filter = filter;
	}

	public Object[] getElements(Object inputElement)
	{
		if (inputElement instanceof ObservableList)
		{
			ObservableList<Problem> problemList = (ObservableList<Problem>) inputElement;
			
			if (ValidationView.ERRORS_AND_UNIGNORED_WARNINGS.equals(filter))
			{
				ArrayList<Problem> outputList = new ArrayList<Problem>();
				for (Problem problem : problemList.getContent())
				{
					if (!problem.isProblemIgnored())
						outputList.add(problem);
				}
				return outputList.toArray();
			}
			if (ValidationView.ERRORS.equals(filter))
			{
				ArrayList<Problem> outputList = new ArrayList<Problem>();
				for (Problem problem : problemList.getContent())
				{
					if (!Problems.isWarning(problem.getProblem()))
						outputList.add(problem);
				}
				return outputList.toArray();
			}
			if (ValidationView.UNIGNORED_WARNINGS.equals(filter))
			{
				ArrayList<Problem> outputList = new ArrayList<Problem>();
				for (Problem problem : problemList.getContent())
				{
					if (Problems.isWarning(problem.getProblem()) && !problem.isProblemIgnored())
						outputList.add(problem);
				}
				return outputList.toArray();
			}
			if (ValidationView.IGNORED_WARNINGS.equals(filter))
			{
				ArrayList<Problem> outputList = new ArrayList<Problem>();
				for (Problem problem : problemList.getContent())
				{
					if (Problems.isWarning(problem.getProblem()) && problem.isProblemIgnored())
						outputList.add(problem);
				}
				return outputList.toArray();
			}
			if (ValidationView.ALL_WARNINGS.equals(filter))
			{
				ArrayList<Problem> outputList = new ArrayList<Problem>();
				for (Problem problem : problemList.getContent())
				{
					if (Problems.isWarning(problem.getProblem()))
						outputList.add(problem);
				}
				return outputList.toArray();
			}
			else // Show all problems (ValidationView.ALL)
				return problemList.getContent().toArray();
		}
		else
		{
			return EMPTY_ARRAY;
		}
	}
	
	public void changeFilter(String filter)
	{
		this.filter = filter;
		viewer.refresh();
	}

	public ValidationEngine getEngine()
	{
		return engine;
	}

	public void setEngine(ValidationEngine engine)
	{
		this.engine = engine;
	}
	
	public void contentChanged(IObservableList<Problem> list)
	{
		//Do Nothing.
	}
	
	public void itemAdded(final Problem item)
	{
		//Do Nothing.
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2007 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/

package tersus.editor.validation;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PartInitException;

import tersus.ProjectStructure;
import tersus.eclipse.ExtensionClassLoader;
import tersus.editor.EditorPlugin;
import tersus.editor.preferences.Preferences;
import tersus.editor.validation.dtd.DtdReader;
import tersus.editor.validation.ui.ValidationView;
import tersus.model.ElementPath;
import tersus.model.FlowModel;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.ModelUtils;
import tersus.model.PluginDescriptor;
import tersus.model.indexer.ElementEntry;
import tersus.model.indexer.RepositoryIndex;
import tersus.model.validation.IValidationContext;
import tersus.model.validation.IValidator;
import tersus.model.validation.ValidatorBase;
import tersus.util.ObservableList;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 * @author Liat Shiff
 * 
 */
public class ValidationEngine implements IValidationContext
{
	private static final String DEFAULT_VALIDATOR = "tersus.runtime.DefaultValidator";

	private ObservableList<Problem> problems = new ObservableList<Problem>();

	private HashMap<String, HashSet<String>> htmlTagHierarchy;

	private WorkspaceRepository repository;

	private IProgressMonitor monitor;

	private Model currentModel;

	private Map<String, IValidator> validators = new HashMap<String, IValidator>();

	private ClassLoader validatorLoader = new ExtensionClassLoader(getClass().getClassLoader());

	private boolean validationInProgress = false;

	private HashMap<ModelId, Integer> contextMap = new HashMap<ModelId, Integer>();

	private List<ValidationListener> listeners = new ArrayList<ValidationListener>();

	private RepositoryIndex index;

	private boolean newProblemAdded;

	private boolean validateUnused;

	private HashMap<ModelId, Model> modelsToValidate;

	private HashMap<ModelId, Model> cachedModels;

	public ValidationEngine(WorkspaceRepository repository)
	{
		this.repository = repository;
		DtdReader dtdReader = new DtdReader();
		dtdReader.parseDTD();
		htmlTagHierarchy = dtdReader.getHtmlTagHierarchy();
	}

	public ObservableList<Problem> getProblems()
	{
		return problems;
	}

	private class ValidateJob extends Job
	{
		private Display display;

		ValidateJob()
		{
			super("Validating " + repository.getProject().getName());
			display = Display.getCurrent();

		}

		protected IStatus run(IProgressMonitor monitor)
		{
			int initialNumberOfProblems = problems.getContent().size();
			setMonitor(monitor);
			doValidate();
			if ((problems.getContent().size() > 0)
					&& (isUnIgnoredProblemExist())
					&& (Preferences.getBoolean(Preferences.ALWAYS_NOTIFY_ON_VALIDATION_PROBLEMS) || problems
							.getContent().size() > initialNumberOfProblems))
			{

				display.asyncExec(new Runnable()
				{
					public void run()
					{
						try
						{
							TersusWorkbench.getActiveWorkbenchWindow().getActivePage()
									.showView(ValidationView.ID);
						}
						catch (PartInitException e)
						{
							TersusWorkbench.log(e);
						}
						MessageDialog.openWarning(TersusWorkbench.getActiveWorkbenchShell(),
								"Validation Problems", "Problems were detected in project '"
										+ repository.getProject().getName()
										+ "'.\n\nSee the 'Validation' view for details.");
					}
				});
			}
			if (monitor != null && monitor.isCanceled())
				return Status.CANCEL_STATUS;
			else
				return Status.OK_STATUS;
		}

	}

	private boolean isUnIgnoredProblemExist()
	{
		for (Problem problem : problems.getContent())
		{
			if (!problem.isProblemIgnored())
				return true;
		}
		return false;
	}

	public void validate(boolean runInBackground)
	{
		if (isValidationInProgress())
			return;
		setValidationInProgress(true);
		Job job = new ValidateJob();
		job.setPriority(Job.LONG);
		if (!runInBackground)
			job.setUser(true);
		job.schedule();
	}

	public void doValidate()
	{
		setValidationInProgress(true);
		validateUnused = Preferences.getBoolean(Preferences.INCLUDE_UNUSED_MODELS);
		try
		{
			IProgressMonitor indexMonitor = null;
			if (monitor != null)
				indexMonitor = new SubProgressMonitor(monitor, 10);

			index = repository.getUpdatedRepositoryIndex(indexMonitor);
			Collection<ModelId> models = index.getAllModelIds();

			if (!validateUnused)
				getAllModels(getUsedModels(models));
			else
				getAllModels(models);

			initCachedModels();
			clearProblems();
			long lastTimeViewRefreshed = System.currentTimeMillis();

			refreshView();
			for (Model current : modelsToValidate.values())
			{
				if (isCancelled())
					return;

				long now = System.currentTimeMillis();
				if ((now - lastTimeViewRefreshed) > 5000 && newProblemAdded)
				{
					newProblemAdded = false;
					refreshView();
					lastTimeViewRefreshed = now;
				}

				currentModel = current;
				validate(currentModel, monitor);
			}
			writeProblemReport();
		}
		finally
		{
			refreshView();
			setMonitor(null);
			setValidationInProgress(false);
			validators.clear();
			contextMap.clear();
			currentModel = null;
		}
	}

	protected void writeProblemReport()
	{
		try
		{

			IFile f = repository.getProject().getFile(ProjectStructure.PROBLEMS);
			StringWriter s = new StringWriter();
			List<Problem> problemList = this.problems.getContent();
			{
				for (Problem p : problemList)
				{
					if (!p.isProblemIgnored())
					{
						s.write(p.getProblem());
						s.write("\t");
						s.write(p.getRoot().getModelId().toString());
						s.write("\t");
						s.write(p.getPath().toString());
						s.write("\t");
						s.write(p.getDetails());
						s.write("\n");
					}
				}
			}
			StringBuffer contentBuffer = s.getBuffer();
			ByteArrayInputStream contentStream = new ByteArrayInputStream(contentBuffer
					.toString().getBytes());
			if (contentBuffer.length() > 0)
				if (f.exists())
					f.setContents(contentStream, true, true, null);
				else 
					f.create(contentStream, true, null);
			else
				f.delete(true, null);

		}
		catch (Exception e)
		{
		}
	}

	private void initCachedModels()
	{
		cachedModels = new HashMap<ModelId, Model>();

		for (Model m : modelsToValidate.values())
		{
			cachedModels.put(m.getModelId(), m);
		}
	}

	private void getAllModels(Collection<ModelId> models)
	{
		modelsToValidate = new HashMap<ModelId, Model>();
		for (ModelId modelId : models)
		{
			Model model = repository.getModel(modelId, true);
			if (model != null && !model.getPackage().isLibrary())
				modelsToValidate.put(model.getModelId(), model);
		}
	}

	public void addProblem(String problem, String details, ElementPath relativePath)
	{
		Problem problemRecord = new Problem(currentModel, relativePath, problem, details);
		problems.add(problemRecord);

		newProblemAdded = true;
	}

	public void clearProblems()
	{
		for (Problem problem : problems.getContent())
		{
			problem.dispose();
		}

		problems.clear();
		refreshView();
	}

	public void refreshView()
	{
		for (ValidationListener listener : listeners)
		{
			listener.refreshView();
		}

		newProblemAdded = false;
	}

	private HashSet<ModelId> getUsedModels(Collection<ModelId> modelIdList)
	{
		ArrayList<ModelId> unusedModelIds = new ArrayList<ModelId>();
		for (Model unusedModel : repository.getUnusedModels(null))
		{
			unusedModelIds.add(unusedModel.getModelId());
		}

		HashSet<ModelId> newList = new HashSet<ModelId>();
		for (ModelId id : modelIdList)
		{
			if (!unusedModelIds.contains(id))
				newList.add(id);
		}

		return newList;
	}

	private void setMonitor(IProgressMonitor monitor)
	{
		this.monitor = monitor;
	}

	public boolean isCancelled()
	{
		return monitor != null && monitor.isCanceled();
	}

	protected void validate(Model model, IProgressMonitor monitor)
	{
		IValidator validator = getValidator(model);
		validator.validate(this, model);
	}

	public IValidator getValidator(Model model)
	{
		IValidator validator = null;
		PluginDescriptor pd = model.getPluginDescriptor();
		String className = ValidatorBase.class.getName();
		if (pd != null)
		{
			if (pd.getValidatorClass() != null)
				className = pd.getValidatorClass();
			else if (model instanceof FlowModel && pd.getJavaClass() != null)
				className = DEFAULT_VALIDATOR; // For runs plugin initialization to detect errors
		}
		validator = validators.get(className);
		if (validator == null)
		{
			try
			{
				validator = (IValidator) validatorLoader.loadClass(className).newInstance();
				validator.setProjectRoot(repository.getProject().getLocation().toFile());
				validators.put(className, validator);
			}
			catch (Exception e)
			{
				TersusWorkbench.log(new Status(Status.ERROR, EditorPlugin.getPluginId(), 0,
						"Failed to load validator " + className, e));
				validator = null;
			}

		}
		if (validator != null)
			return validator;
		else
			return ValidatorBase.INSTANCE;
	}

	synchronized public boolean isValidationInProgress()
	{
		return validationInProgress;
	}

	synchronized void setValidationInProgress(boolean running)
	{
		this.validationInProgress = running;
	}

	public HashMap<String, HashSet<String>> getHtmlTagHierarchy()
	{
		return htmlTagHierarchy;
	}

	public int getModelInnerContext(Model model)
	{
		Integer context = contextMap.get(model.getModelId());
		if (context == null)
		{
			context = new Integer(_getModelInnerContext(model, new HashSet<Model>()));
			contextMap.put(model.getId(), context);
		}

		return context.intValue();
	}

	private int _getModelInnerContext(Model model, HashSet<Model> checkedModels)
	{
		checkedModels.add(model);

		ModelId modelId = model.getModelId();
		Integer innerContext = contextMap.get(modelId);
		if (innerContext != null)
			return innerContext.intValue();

		if (!validateUnused && !isModelsContain(model))
			return UNUSED_CONTEXT;

		if (ModelUtils.isSystemModel(model))
			return SYSTEM_CONTEXT;

		if (ModelUtils.isDisplayModel(model))
			return CLIENT_CONTEXT;

		if (model.getPluginDescriptor() != null && model.getPluginDescriptor().isServiceClient())
			return SERVER_CONTEXT;

		int contextIntVal = NO_CONTEXT;
		for (ElementEntry ref : index.getReferenceEntries(model.getModelId()))
		{
			// The following was rewritten by Ofer, 5/7/12, to use previous values from contextMap
			// when existing
			ModelId parentModelId = ref.getParentId();
			Model refModel = repository.getModel(parentModelId, true);
			Integer parentContext;
			if (checkedModels.contains(refModel))
				parentContext = contextMap.get(parentModelId);
			else
			{
				parentContext = _getModelInnerContext(refModel, checkedModels);
				contextMap.put(parentModelId, parentContext);
			}
			if (parentContext != null)
			{
				int newContext = parentContext;
				if (ModelUtils.isProcessModel(model) && (newContext & SYSTEM_CONTEXT) != 0)
				{
					// A process inside a system has server context;
					newContext = newContext ^ SYSTEM_CONTEXT; // Turn off the SYSTEM_CONTEXT Bit
					newContext = newContext | SERVER_CONTEXT;
				}
				contextIntVal |= newContext;
			}
		}

		contextMap.put(modelId, new Integer(contextIntVal));

		return contextIntVal;
	}

	private boolean isModelsContain(Model model)
	{
		return modelsToValidate.get(model.getModelId()) != null;
	}

	public void addListener(ValidationListener listener)
	{
		if (!listeners.contains(listener))
			listeners.add(listener);
	}

	public void removeListener(ValidationListener listener)
	{
		listeners.remove(listener);
	}

	public RepositoryIndex getRepositoryIndex()
	{
		return index;
	}

	public Model getRefModel(ModelElement e)
	{
		Model model = cachedModels.get(e.getModelId());

		if (model == null)
			model = e.getReferredModel();

		return model;
	}
}
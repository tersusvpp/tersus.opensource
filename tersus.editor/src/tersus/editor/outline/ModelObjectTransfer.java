/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.outline;

import java.util.Collections;
import java.util.List;

import org.eclipse.gef.dnd.SimpleObjectTransfer;

/**
 * @author Youval Bronicki
 *
 */
public class ModelObjectTransfer extends SimpleObjectTransfer
{
    private static final String TYPE_NAME = "TERSUS_MODEL_OBJECT";
    public static final int TYPE_ID = registerType(TYPE_NAME);

    private static final ModelObjectTransfer instance = new ModelObjectTransfer();
    /* (non-Javadoc)
     * @see org.eclipse.swt.dnd.Transfer#getTypeIds()
     */
    protected int[] getTypeIds()
    {
        return new int[] {TYPE_ID};
    }

    /* (non-Javadoc)
     * @see org.eclipse.swt.dnd.Transfer#getTypeNames()
     */
    protected String[] getTypeNames()
    {
        return new String[] {TYPE_NAME};
    }

    /**
     * @return
     */
    public static ModelObjectTransfer getInstance()
    {
        return instance;
    }
    
    public List getEntries()
    {
        if (getInstance().getObject() instanceof List)
            return (List)getInstance().getObject();
        else
            return Collections.EMPTY_LIST;
    }

}

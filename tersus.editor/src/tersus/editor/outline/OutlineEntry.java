/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.outline;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.Status;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.TreeItem;

import tersus.editor.TersusEditor;
import tersus.editor.properties.ModelObjectWrapper;

import tersus.model.BuiltinProperties;
import tersus.model.DataElement;
import tersus.model.DataType;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.model.Path;
import tersus.model.Role;
import tersus.model.SubFlow;
import tersus.util.PropertyChangeListener;
import tersus.workbench.ImageLocator;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 *  
 */
public class OutlineEntry extends ModelObjectWrapper implements PropertyChangeListener
{

    private AbstractTreeViewer viewer;

    private OutlineEntry[] children = null;

    private OutlineEntry parent;

    private boolean active;

    public OutlineEntry(AbstractTreeViewer viewer, ModelObject modelObject)
    {
        super(modelObject);
        this.viewer = viewer;
    }

    /**
     * @return
     */
    public Object[] getChildren()
    {
        if (children == null)
            refreshChildren();
        return children;
    }

    private ArrayList getModelElements()
    {
        if (modelElements == null)
            refreshModelElements();
        return modelElements;
    }

    private void refreshModelElements()
    {
        if (modelElements == null)
            modelElements = new ArrayList();
        modelElements.clear();
        if (getModel() != null)
        {
            List elements = getModel().getElements();
            for (int i = 0; i < elements.size(); i++)
            {
                ModelElement element = (ModelElement) elements.get(i);
                if (element instanceof SubFlow
                        || element instanceof DataElement)
                    modelElements.add(element);
            }
        }
    }

    /**
     * @return
     */
    public String getModelName()
    {
        if (getModel() != null)
            return getModel().getName();
        else
            return "";
    }

    /**
     * @return
     */
    public String getTypeText()
    {
        if (getModel() != null)
            return text(getModel().getProperty(BuiltinProperties.TYPE));
        else
            return text(getElement().getProperty(BuiltinProperties.TYPE));
    }

    /**
     * @return
     */
    public String getText()
    {
        return text(getElementName());
    }

    /**
     * @return
     */
    public OutlineEntry getParent()
    {
        return parent;
    }

    public void setParent(OutlineEntry parent)
    {
        this.parent = parent;
    }

    /**
     * @return
     */
    public boolean hasChildren()
    {
        return getModelElements().size() > 0;
    }

    /**
     * @return
     */
    public Image getImage()
    {
        if (getModel() != null)
        {
            ImageLocator imageLocator = TersusEditor
                    .getImageLocator(getModel());
            if (imageLocator != null)
                return imageLocator.get16x16Image();
        }
        return null;
    }

    /**
     * @return
     */
    public String getElementName()
    {
        if (getElement() != null)
            return getElement().getRole().toString();
        else
            return "[" + getModelName() + "]";
    }

    private String text(Object obj)
    {
        if (obj == null)
            return "";
        else
            return obj.toString();
    }

    public void activate()
    {
        if (!active)
        {
            System.out.println("Activating " + this + " .");
            active = true;
            if (getModel() != null)
                getModel().addPropertyChangeListener(this);
            if (getElement() != null)
                getElement().addPropertyChangeListener(this);
            System.out.println(this + " activated.");
        }
    }

    public void dispose()
    {
        if (active)
        {
            System.out.println("Disposing " + this + " .");
            if (getModel() != null)
                getModel().removePropertyChangeListener(this);
            if (getElement() != null)
                getElement().removePropertyChangeListener(this);
            if (children != null)
            {
                for (int i = 0; i < children.length; i++)
                {
                    children[i].dispose();
                }
            }
            active = false;
            System.out.println(this + " disposed.");
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
     */
    
    public void propertyChange(Object sourceObject, String property, Object oldValue, Object newValue)
    {
        if (viewer.getControl().isDisposed())
        {
            String message = "OutlineEntry.modelChanged(): Outline viewer disposed";
            if (viewer.getInput() instanceof Model)
            {
                Model root = (Model)viewer.getInput();
                WorkspaceRepository repository = (WorkspaceRepository) root.getRepository();
                String projectName = repository.getRepositoryRoot().getProject().getName();
                message += "  (project= "+projectName + " root model="+root.getId()+" changed model="+getModel().getIconFolder()+")";;
            }
            TersusWorkbench.log(new Status(0, TersusEditor.ID, 0, message,new Exception(message)));
            return;
        }
        //super.propertyChange(evt);
        if (sourceObject == getElement())
            elementChanged(property, oldValue, newValue);
        else if (sourceObject == getModel())
            modelChanged(property, oldValue, newValue);
    }

    public void modelChanged(String property, Object oldValue, Object newValue)
    {
        if (property.equals(BuiltinProperties.ELEMENTS))
            if (refreshChildren())
                viewer.refresh(this);
    }

    public void elementChanged(String property, Object oldValue, Object newValue)
    {
        viewer.refresh(this);
    }

    private void refresh()
    {
        refreshChildren();
    }

    /**
     * Refreshes the 'children' array.
     * 
     * @return true if the array was modified, false otherwise
     *  
     */
    private boolean refreshChildren()
    {
        boolean changed = false;
        refreshModelElements();
        OutlineEntry[] oldChildren = children;
        children = new OutlineEntry[modelElements.size()];
        // Rebuild the 'children' array according to list of new children.
        // Fill the list with existing entries (if possible)
        for (int i = 0; i < children.length; i++)
        {
            ModelElement element = (ModelElement) modelElements.get(i);
            if (oldChildren != null)
                for (int j = 0; j < oldChildren.length; j++)
                {
                    if (oldChildren[j] != null
                            && element == oldChildren[j].getElement())
                    {
                        children[i] = oldChildren[j];
                        oldChildren[j] = null;
                        if (i != j)
                            changed = true;
                    }
                }
            if (children[i] == null)
            {
                //Existing entry not found. Activate newly created entry
                OutlineEntry newChild = new OutlineEntry(viewer, element);
                newChild.setParent(this);
                newChild.activate();
                children[i] = newChild;
                changed = true;
            }
        }
        // dispose any existing children that were removed
        if (oldChildren != null)
            for (int i = 0; i < oldChildren.length; i++)
            {
                if (oldChildren[i] != null)
                {
                    oldChildren[i].dispose();
                    changed = true;
                }
            }
        return changed;

    }

    /**
     * Checks whether this OutlineEntry refers to the same ModelElement as
     * another OutlineEntry.
     * 
     * @param other
     * @return
     */
    private boolean refersToSameModelObjectAs(OutlineEntry other)
    {
        if (other == null)
            return false;
        else
            return (this.getModel() == other.getModel() && this.getElement() == other
                    .getElement());
    }

    private String path = null;

    private ArrayList modelElements;

    public String toString()
    {
        if (path == null)
        {
            if (parent == null)
                path = getModelName();
            else
                path = parent.toString() + "/" + getElement().getRole();
        }
        return path;
    }

    /**
     * Returns the sub-entry identified by the given path suffix
     * 
     * @return the OutlineEntry specified by the given path, or null if no such
     *         entry was found.
     * @param path
     *            a path that identifies the sub-entry to select
     * @param index -
     *            indicates the number of segments at the head of the path that
     *            should be ignored.
     */
    public OutlineEntry get(Path path, int index)
    {
        if (index == path.getNumberOfSegments())
        {
            return this;
        }
        else
        {
            Role role = path.getSegment(index);
            OutlineEntry[] children = (OutlineEntry[]) getChildren();
            for (int i = 0; i < children.length; i++)
            {
                if (children[i].getElement().getRole() == role)
                    return children[i].get(path, index + 1);
            }
            return null; // Not found

        }
    }

    /**
     * @return
     */
    public Path getPath()
    {
        if (parent == null)
            return new Path();
        else
        {
            Path path = parent.getPath();
            path.addSegment(getElement().getRole());
            return path;
        }
    }

    /**
     * @return the bounds of this entry's tree item, in control coordinates
     */
    public Rectangle getBounds()
    {
        TreeItem item = (TreeItem) viewer.testFindItem(this);
        org.eclipse.swt.graphics.Rectangle itemBounds = item.getBounds();
//        Control control = viewer.getControl();
//        org.eclipse.swt.graphics.Point displayLocation = control.toDisplay(
//                itemBounds.x, itemBounds.y);
//        Display display = control.getDisplay();
//        org.eclipse.swt.graphics.Rectangle shellBounds = display
//                .getActiveShell().getBounds();
//        Rectangle output = new Rectangle(shellBounds.x + displayLocation.x,
//                shellBounds.y + displayLocation.y, itemBounds.width,
//                itemBounds.height);
        Rectangle output = new Rectangle(itemBounds);
        return output;
    }
}
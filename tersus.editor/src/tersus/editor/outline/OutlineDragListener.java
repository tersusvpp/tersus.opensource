/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.outline;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;

/**
 * @author Youval Bronicki
 *
 */
public class OutlineDragListener implements DragSourceListener
{

    TersusOutlinePage outlinePage;
    /**
     * @param viewer
     */
    public OutlineDragListener(TersusOutlinePage outlinePage)
    {
        this.outlinePage = outlinePage;
    }

    /* (non-Javadoc)
     * @see org.eclipse.swt.dnd.DragSourceListener#dragStart(org.eclipse.swt.dnd.DragSourceEvent)
     */
    public void dragStart(DragSourceEvent event)
    {
        List entries = outlinePage.getSelectedEntries();
        List modelObjects = new ArrayList(entries.size());
        for (int i=0; i< entries.size(); i++)
        {
            OutlineEntry entry = (OutlineEntry)entries.get(i);
            if (entry.getElement()!= null)
            modelObjects.add(entry.getElement());
            else
                modelObjects.add(entry.getModel());
        }
        ModelObjectTransfer.getInstance().setObject(modelObjects);
		event.doit= modelObjects.size() > 0;
        
    }

    /* (non-Javadoc)
     * @see org.eclipse.swt.dnd.DragSourceListener#dragSetData(org.eclipse.swt.dnd.DragSourceEvent)
     */
    public void dragSetData(DragSourceEvent event)
    {
        
		event.data= ModelObjectTransfer.getInstance().getObject();

    }

    /* (non-Javadoc)
     * @see org.eclipse.swt.dnd.DragSourceListener#dragFinished(org.eclipse.swt.dnd.DragSourceEvent)
     */
    public void dragFinished(DragSourceEvent event)
    {
        ModelObjectTransfer.getInstance().setObject(null);
    }

}

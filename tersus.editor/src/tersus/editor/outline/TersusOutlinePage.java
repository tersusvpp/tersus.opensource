/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.outline;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.ListenerList;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.util.SafeRunnable;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableTreeViewer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableTree;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.part.Page;
import org.eclipse.ui.part.PageBook;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;

import tersus.editor.TersusEditor;
import tersus.editor.editparts.FlowEditPart;
import tersus.editor.editparts.TersusEditPart;
import tersus.model.Model;
import tersus.model.Path;
import tersus.util.Misc;

/**
 * @author Youval Bronicki
 * 
 */
public class TersusOutlinePage extends Page implements IContentOutlinePage
{
	private TersusEditor editor;

	private AbstractTreeViewer viewer;

	private ListenerList selectionChangedListeners = new ListenerList();

	private OutlineEntry rootEntry;

	private Model rootModel;

	private static final int ELEMENT_NAME_INDEX = 0;

	private static final int TYPE_INDEX = 1;

	private static final int MODEL_NAME_INDEX = 2;

	private static final String ELEMENT_NAME_COLUMN = "Element Name";

	private static final String TYPE_COLUMN = "Type";

	private static final String MODEL_NAME_COLUMN = "Model Name";

	private PageBook pageBook;

	/**
	 * A flag that's set while this outline page is synchronizing the editor. It is used to prevent
	 * circular synchronization.
	 */
	protected boolean isSynchronizing = false;

	/**
	 * @param viewer
	 */
	public TersusOutlinePage(TersusEditor editor)
	{
		super();
		this.editor = editor;
		editor.getViewer().addSelectionChangedListener(new ISelectionChangedListener()
		{

			public void selectionChanged(SelectionChangedEvent event)
			{
				if (!isSynchronizing)
					synchronizeWithEditor();
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.Page#createControl(org.eclipse.swt.widgets.Composite)
	 */
	public void createControl(Composite parent)
	{
		pageBook = new PageBook(parent, SWT.NONE);

		// viewer = createTableTree();
		viewer = new TreeViewer(pageBook);
		viewer.setContentProvider(new OutlineContentProvider());
		viewer.setLabelProvider(new OutlineLabelProvider());
		final TersusOutlinePage page = this;
		viewer.addSelectionChangedListener(new ISelectionChangedListener()
		{

			public void selectionChanged(SelectionChangedEvent viewerEvent)
			{
				if (!isSynchronizing)
				{
					synchronizeEditorSelection();
				}
				final SelectionChangedEvent pageEvent = new SelectionChangedEvent(page, viewerEvent
						.getSelection());
				Object[] listeners = selectionChangedListeners.getListeners();
				for (int i = 0; i < listeners.length; ++i)
				{
					final ISelectionChangedListener listener = (ISelectionChangedListener) listeners[i];
					Platform.run(new SafeRunnable()
					{
						public void run()
						{
							listener.selectionChanged(pageEvent);
						}
					});
				}
			}
		});
		addActions();
		int ops = DND.DROP_COPY | DND.DROP_MOVE;
		Transfer[] transfers = new Transfer[]
		{ ModelObjectTransfer.getInstance() };
		viewer.addDragSupport(ops, transfers, new OutlineDragListener(this));

		attachToEditor();
		pageBook.showPage(viewer.getControl());
	}

	public void attachToEditor()
	{
		if (viewer == null)
			return; // Avoid NullPointerException
		OutlineEntry oldRoot = getRootEntry();
		rootModel = editor.getRootModel();
		OutlineEntry rootEntry = new OutlineEntry(viewer, rootModel);
		setRootEntry(rootEntry);
		viewer.setInput(rootModel); // The rootModel is arbitrarily set as the
		if (oldRoot != null)
			oldRoot.dispose();
		// 'invisible' root of the tree
		rootEntry.activate();
	}

	/**
	 * Creates actions and hooks them to the viewer
	 */
	private void addActions()
	{
		final Action zoomAction = new Action()
		{
			public void run()
			{
				OutlineEntry selectedEntry = getSelectedEntry();
				if (selectedEntry != null)
				{
					TersusEditPart part = editor.getTopFlowEditPart().getChild(
							selectedEntry.getPath(), true);
					if (part instanceof FlowEditPart)
					{
						((FlowEditPart) part).zoomToPart();
					}
				}

			}
		};
		viewer.addDoubleClickListener(new IDoubleClickListener()
		{

			public void doubleClick(DoubleClickEvent event)
			{
				zoomAction.run();
			}
		});
	}

	/**
     *  
     */
	protected void synchronizeEditorSelection()
	{
		isSynchronizing = true;
		List selectedEntries = getSelectedEntries();
		List selectedEditParts = new ArrayList();
		for (Iterator i = selectedEntries.iterator(); i.hasNext();)
		{
			OutlineEntry entry = (OutlineEntry) i.next();
			Path path = entry.getPath();
			TersusEditPart editPart = editor.getTopFlowEditPart().getChild(path, false);
			if (editPart != null)
				selectedEditParts.add(editPart);

		}
		editor.getViewer().setSelection(new StructuredSelection(selectedEditParts));
		isSynchronizing = false;
	}

	List getSelectedEntries()
	{
		return ((StructuredSelection) viewer.getSelection()).toList();
	}

	/**
	 * Returns the selected outline entry
	 * 
	 * @return The selected entry in the outline (null if no entry is selected or if more than one
	 *         entry is selected).
	 */
	private OutlineEntry getSelectedEntry()
	{
		List selectedEntries = getSelectedEntries();
		if (selectedEntries.size() != 1)
			return null;
		else
			return (OutlineEntry) selectedEntries.get(0);
	}

	private TableTreeViewer createTableTree()
	{
		TableTree tableTree = new TableTree(pageBook, SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI
				| SWT.FULL_SELECTION);
		Table table = tableTree.getTable();
		TableLayout layout = new TableLayout();
		table.setLayout(layout);
		table.setLinesVisible(false);
		table.setHeaderVisible(true);
		ColumnWeightData[] columnLayouts =
		{ new ColumnWeightData(100), new ColumnWeightData(50), new ColumnWeightData(50) };
		String[] labels = new String[]
		{ ELEMENT_NAME_COLUMN, TYPE_COLUMN, MODEL_NAME_COLUMN };
		for (int i = 0; i < labels.length; i++)
		{

			TableColumn tc = new TableColumn(table, SWT.NONE, i);
			tc.setText(labels[i]);
			tc.pack();
			columnLayouts[i].minimumWidth = tc.getWidth();
			layout.addColumnData(columnLayouts[i]);
		}

		/*
		 * Create and configure the viewer
		 */
		TableTreeViewer v = new TableTreeViewer(tableTree);
		return v;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.Page#getControl()
	 */
	public Control getControl()
	{
		return pageBook;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.Page#setFocus()
	 */
	public void setFocus()
	{
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ISelectionProvider#addSelectionChangedListener(org.eclipse.jface
	 * .viewers.ISelectionChangedListener)
	 */
	public void addSelectionChangedListener(ISelectionChangedListener listener)
	{
		selectionChangedListeners.add(listener);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ISelectionProvider#getSelection()
	 */
	public ISelection getSelection()
	{
		if (viewer == null)
			return StructuredSelection.EMPTY;
		return viewer.getSelection();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ISelectionProvider#removeSelectionChangedListener(org.eclipse.jface
	 * .viewers.ISelectionChangedListener)
	 */
	public void removeSelectionChangedListener(ISelectionChangedListener listener)
	{
		selectionChangedListeners.remove(listener);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ISelectionProvider#setSelection(org.eclipse.jface.viewers.ISelection
	 * )
	 */
	public void setSelection(ISelection selection)
	{
	}

	class OutlineLabelProvider extends LabelProvider implements ITableLabelProvider
	{

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
		 */
		public Image getColumnImage(Object element, int columnIndex)
		{
			if (columnIndex == 1)
				return ((OutlineEntry) element).getImage();
			else
				return null;
		}

		public Image getImage(Object element)
		{
			return ((OutlineEntry) element).getImage();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object, int)
		 */
		public String getColumnText(Object element, int columnIndex)
		{
			OutlineEntry entry = (OutlineEntry) element;
			switch (columnIndex)
			{
				case ELEMENT_NAME_INDEX:
					return entry.getElementName();
				case MODEL_NAME_INDEX:
					return entry.getModelName();
				case TYPE_INDEX:
					return entry.getTypeText();
				default:
					return null;

			}
		}

		public String getText(Object element)
		{
			return ((OutlineEntry) element).getText();
		}
	}

	class OutlineContentProvider implements IStructuredContentProvider, ITreeContentProvider
	{

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
		 */
		public Object[] getElements(Object inputElement)
		{
			/*
			 * This method is called to create the 'top' elements in the tree
			 */
			Misc.assertion(inputElement == rootModel);
			return new Object[]
			{ getRootEntry() };
			// return ((OutlineEntry)inputElement).getChildren();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
		 */
		public void dispose()
		{
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer,
		 * java.lang.Object, java.lang.Object)
		 */
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
		{
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.Object)
		 */
		public Object[] getChildren(Object parentElement)
		{
			return ((OutlineEntry) parentElement).getChildren();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object)
		 */
		public Object getParent(Object element)
		{
			return ((OutlineEntry) element).getParent();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.Object)
		 */
		public boolean hasChildren(Object element)
		{
			return ((OutlineEntry) element).hasChildren();
		}
	}

	protected void synchronizeWithEditor()
	{
		isSynchronizing = true;
		List editorSelection = editor.getViewer().getSelectedEditParts();
		List newSelection = new ArrayList(editorSelection.size());
		for (Iterator i = editorSelection.iterator(); i.hasNext();)
		{
			TersusEditPart part = (TersusEditPart) i.next();
			Path path = part.getPath();
			if (getRootEntry() != null)
			{
				OutlineEntry entry = getRootEntry().get(path, 0);
				// Not all edit parts appear in the outline (e.g. slots)
				if (entry != null)
				{
					viewer.expandToLevel(entry, 0);
					newSelection.add(entry);
				}
			}
		}
		viewer.setSelection(new StructuredSelection(newSelection));
		isSynchronizing = false;
	}

	public void dispose()
	{
		rootEntry.dispose();
		editor.outlinePage = null;
		super.dispose();
	}

	public OutlineEntry getRootEntry()
	{
		return rootEntry;
	}

	public void setRootEntry(OutlineEntry rootEntry)
	{
		this.rootEntry = rootEntry;
	}
}
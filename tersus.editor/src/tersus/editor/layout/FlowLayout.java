/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.layout;

import java.util.HashMap;
import java.util.List;

import org.eclipse.draw2d.FigureUtilities;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LayoutManager;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;

import tersus.editor.Debug;
import tersus.editor.figures.FlowRectangle;
import tersus.editor.figures.NoteFigure;
import tersus.editor.figures.OpenToggle;
import tersus.editor.figures.PrecisionRectangle;
import tersus.editor.figures.SlotFigure;
import tersus.editor.preferences.Preferences;
import tersus.util.Misc;
import tersus.util.Trace;

public class FlowLayout implements LayoutManager
{

	private static double MIN_TOGGLE_FRACTION = 0.5;
	private HashMap<IFigure, Object> constraints = new HashMap<IFigure, Object>();
	// Temporary rectangle (for shortcalculations)
	private static final PrecisionRectangle tmpPrecisionRectangle = new PrecisionRectangle();

	public Object getConstraint(IFigure child)
	{
		return constraints.get(child);
	}

	public Dimension getMinimumSize(IFigure container, int wHint, int hHint)
	{
		return null;
	}
	
	public Dimension getPreferredSize(IFigure container, int wHint, int hHint)
	{
		return null;
	}
	
	public void invalidate()
	{

	}
	
	public void layout(IFigure container)
	{
		Misc.assertion(container instanceof FlowRectangle);
		FlowRectangle rectangle = (FlowRectangle) container;

		boolean tracePushed = false;
		if (Debug.LAYOUT)
		{
			Trace.push("Calculating layout for " + container);
			tracePushed = true;
		}
		try
		{
			int margin = layoutFrame(rectangle);
			layoutTextAndImage(rectangle);
			PrecisionRectangle parentFrame = rectangle.getFrame();
			List<IFigure> children = rectangle.getChildren();
			for (IFigure child : children)
			{
				if (child instanceof OpenToggle)
				{
					layoutToggle((OpenToggle) child, parentFrame);
					continue;
				}
				LayoutConstraint constraint = (LayoutConstraint) getConstraint(child);
				if (Debug.LAYOUT)
					Trace.add("Child: " + child + " constraint:" + constraint + " current bounds="
							+ child.getBounds());
				if (constraint == null)
					continue;
				double dx = parentFrame.preciseWidth * constraint.getX();
				double dy = parentFrame.preciseHeight * constraint.getY();
				double centerX = parentFrame.preciseX + dx;
				double centerY = parentFrame.preciseY + dy;
				if (Debug.LAYOUT)
					Trace.add("dx=" + dx + " dy=" + dy + " centerX=" + centerX + " centerY="
							+ centerY);
				double width, height;
				if (child instanceof FlowRectangle || child instanceof NoteFigure)
				{
					if (Debug.LAYOUT)
					{
						for (Object obj: child.getChildren())
						{
							IFigure grandChild = (IFigure) obj;
							Trace.add("grandChild:" + grandChild + " bounds="
									+ grandChild.getBounds());

						}
					}
					width = parentFrame.preciseWidth * constraint.getWidth();
					height = parentFrame.preciseHeight * constraint.getHeight();
				}
				else
				{
					// child is a slot circle
					Misc.assertion(child instanceof SlotFigure);
					width = margin * 2;
					height = margin * 2;
				}
				PrecisionRectangle childBounds = tmpPrecisionRectangle;
				childBounds.setBounds(centerX - width / 2, centerY - height / 2, width, height);
				if (Debug.LAYOUT)
					Trace.add("child bounds:" + childBounds);
				child.setBounds(childBounds);
			}
		}

		finally
		{
			if (tracePushed)
				Trace.pop();
		}
	}

	static Dimension labelSize = new Dimension(0, 0);

	protected static void layoutTextAndImage(FlowRectangle rectangle)
	{
		PrecisionRectangle frame = rectangle.getFrame();
		Font font = null;

		Image lockedImage = rectangle.getLockedImage();

		// FUNC2 improve the selection of fonts (requires better specification)
		int maxWidth = frame.width - 2;
		int maxHeight = frame.height - 2;

		int textHeight = 0;
		rectangle.paintText = false;

		rectangle.substituteLabel = null;
		if (maxWidth >= 300)
		{
			font = Preferences.getFont(Preferences.FLOW_FONT_LARGE);
			FigureUtilities.getTextExtents(rectangle.getLabel(), font, labelSize);
			textHeight = labelSize.height;
			rectangle.paintText = maxWidth >= labelSize.width && maxHeight >= textHeight;
		}
		if (!rectangle.paintText && maxWidth >= 100)
		{
			font = Preferences.getFont(Preferences.FLOW_FONT_MEDIUM);
			FigureUtilities.getTextExtents(rectangle.getLabel(), font, labelSize);
			textHeight = labelSize.height;
			rectangle.paintText = maxWidth >= labelSize.width && maxHeight >= textHeight;
		}

		if (!rectangle.paintText)
		{
			font = Preferences.getFont(Preferences.FLOW_FONT_SMALL);
			FigureUtilities.getTextExtents(rectangle.getLabel(), font, labelSize);
			textHeight = labelSize.height;
			rectangle.paintText = maxWidth >= labelSize.width && maxHeight >= textHeight;

			if (!rectangle.paintText && maxWidth > 20)
			{
				// OPT Optimize and improve the elipsis logic
				String fullText = rectangle.getLabel();
				int length = fullText.length();
				while (length >= 4 && !rectangle.paintText)
				{
					rectangle.substituteLabel = fullText.substring(0, length) + "..";
					FigureUtilities.getTextExtents(rectangle.substituteLabel, font, labelSize);
					textHeight = labelSize.height;
					rectangle.paintText = maxWidth >= labelSize.width && maxHeight >= textHeight;
					--length;
				}
			}
		}
		rectangle.setFont(font);

		if (rectangle.isOpen())
		{
			int headingHeight = 0;
			if (rectangle.paintText)
				headingHeight = textHeight;
			// image and label at top left
			int x = frame.x + 1;
			if (rectangle.hasImage())
			{
				Image image = rectangle.getImageLocator().getImage(0, 0); // get smallest image
				int imageHeight = image.getBounds().height;
				int imageWidth = image.getBounds().width;
				if (imageHeight <= Math.min(textHeight, maxHeight) && imageWidth <= maxWidth)
				{
					rectangle.setDrawImage(true);
					rectangle.imageFrame.setSize(imageWidth, imageHeight);
					if (imageHeight > headingHeight)
						headingHeight = imageHeight;
					int imageY = (headingHeight - imageHeight) / 2 + frame.y + 1;
					rectangle.targetFrame.setLocation(x, imageY);
					rectangle.targetFrame.setSize(imageWidth, imageHeight);
					x += imageWidth + 1;
				}
				else
					rectangle.setDrawImage(false);
			}
			if (rectangle.paintText)
				rectangle.setTextPosition(x + 1, frame.y + 1 + (headingHeight - textHeight) / 2);

			int lockedImageHeight = lockedImage.getBounds().height;
			int lockedImageWidth = lockedImage.getBounds().width;
			
			rectangle.lockedTargetFrame.setLocation(frame.x + 2, frame.y + frame.height - lockedImageHeight - 4);
			rectangle.lockedTargetFrame.setSize(lockedImageWidth + 1, lockedImageHeight + 1);
		}
		else
		{
			// label below image , or label at center
			rectangle.setDrawImage(false);
			if (rectangle.hasImage())
			{
				maxWidth = frame.width - 2;
				maxHeight = frame.height - 2 - 2 * textHeight;
				Image image = rectangle.getImageLocator().getImage(maxWidth, maxHeight);
				int imageHeight = image.getBounds().height;
				int imageWidth = image.getBounds().width;
				rectangle.imageFrame.setSize(imageWidth, imageHeight);
				float scaleX = maxWidth / (float) imageWidth;
				float scaleY = maxHeight / (float) imageHeight;
				float scale = Math.min(scaleX, scaleY);
				boolean autoResize = Preferences
						.getBoolean(Preferences.AUTOMATICALLY_RESIZE_IMAGES);

				// /* "Hide" the label if the vertical scale is < 1 and
				// * - either autoResize is on
				// * - or removing the text will make the image fit
				// *
				// */
				// if (scaleY < 1 && (autoResize || (scaleX >= 1 && imageHeight <= frame.height)))
				// {
				// rectangle.paintText = false;
				// maxHeight = frame.height - 2;
				// scaleY = maxHeight / (float) imageHeight;
				// scale = Math.min(scaleX, scaleY);
				// }
				if (!autoResize)
				{
					if (scale < 1)
						scale = 0;
					if (scale > 1)
						scale = 1;
				}
				if (scale > 0)
				{
					rectangle.targetFrame.setSize(Math.round(scale * imageWidth), Math.round(scale
							* imageHeight));
					rectangle.setDrawImage(true);
					int dx = Math.round((maxWidth - scale * imageWidth) / 2);
					int dy = Math.round((maxHeight - scale * imageHeight) / 2);
					if (rectangle.paintText)
						rectangle.targetFrame.setLocation(frame.x + dx, frame.y + 1 + textHeight
								+ dy);
					else
						rectangle.targetFrame.setLocation(frame.x + dx, frame.y + 1 + dy);
				}
			}
			if (rectangle.paintText)
			{
				int textX, textY;
				if (rectangle.getDrawImage())
				{
					textX = frame.x + (frame.width - labelSize.width) / 2;
					float vCenter = (frame.bottom() + rectangle.targetFrame.bottom()) / 2;
					textY = Math.round(vCenter - textHeight / 2f);
				}
				else
				{
					textX = (int) (frame.x + (frame.width - labelSize.width) / 2);
					textY = (int) (frame.y + (frame.height - textHeight) / 2);
				}
				rectangle.setTextPosition(textX, textY);
			}
			
			int lockedImageHeight = lockedImage.getBounds().height;
			int lockedImageWidth = lockedImage.getBounds().width;
			
			rectangle.lockedTargetFrame.setLocation(frame.x + 2, frame.y + frame.height - lockedImageHeight - 4);
			rectangle.lockedTargetFrame.setSize(lockedImageWidth + 1, lockedImageHeight + 1);
		}
	}

	private int layoutFrame(FlowRectangle rectangle)
	{
		PrecisionRectangle bounds = rectangle.getPreciseBounds();
		int minSlotSize = Preferences.getInt(Preferences.MIN_SLOT_SIZE);
		int maxSlotSize = Preferences.getInt(Preferences.MAX_SLOT_SIZE);

		int length = Math.min(bounds.height, bounds.width);
		int margin = Math.min(maxSlotSize, length / 12);
		if (margin < minSlotSize)
			margin = Math.min(minSlotSize, length / 8);

		if (Debug.LAYOUT)
		{
			Trace.add("bounds=" + bounds);
			Trace.add("length=" + length + " margin=" + margin);

		}
		PrecisionRectangle frame = tmpPrecisionRectangle;
		frame.setBounds(bounds.preciseX + margin, bounds.preciseY + margin, bounds.preciseWidth - 2
				* margin, bounds.preciseHeight - 2 * margin);
		if (Debug.LAYOUT)
			Trace.add("Frame=" + frame);
		rectangle.setFrame(frame);

		return margin;
	}

	/**
	 * @param rectangle
	 */
	private static Rectangle tmpRect = new Rectangle();

	private void layoutToggle(OpenToggle toggle, Rectangle frame)
	{
		if (Math.min(frame.width, frame.height) * MIN_TOGGLE_FRACTION < OpenToggle.SIZE)
		{
			toggle.setVisible(false);
			return;
		}
		toggle.setVisible(true);

		tmpRect.width = OpenToggle.SIZE;
		tmpRect.height = OpenToggle.SIZE;
		tmpRect.x = frame.x + frame.width - tmpRect.width;
		tmpRect.y = frame.y;
		toggle.setBounds(tmpRect);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.LayoutManager#remove(org.eclipse.draw2d.IFigure)
	 */
	public void remove(IFigure child)
	{
		constraints.remove(child);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.LayoutManager#setConstraint(org.eclipse.draw2d.IFigure,
	 * java.lang.Object)
	 */
	public void setConstraint(IFigure child, Object constraint)
	{
		Misc.assertion(constraint == null || constraint instanceof LayoutConstraint);
		constraints.put(child, constraint);
		invalidate();

	}

}

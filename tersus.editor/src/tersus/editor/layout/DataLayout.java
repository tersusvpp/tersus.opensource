/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.layout;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.draw2d.FigureUtilities;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LayoutManager;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import tersus.editor.Debug;
import tersus.editor.figures.DataRectangle;
import tersus.editor.figures.OpenToggle;
import tersus.editor.figures.PrecisionRectangle;
import tersus.editor.preferences.Preferences;
import tersus.util.Misc;
import tersus.util.Trace;
/**
 * Holds parameters for laying out a tree.
 * Parameters include size of spaces around various graphical elements (e.g. before label, after label), as well
 * as the Font uses for labels and the (maximum) width for icons.
 */
class DataLayoutParemeterSet
{
	private int vBeforeLabel; // Vertical number of pixels between the frame and the label 
	private int vAfterLabel; // Vertical number of pixels after label (preceding the frame or the children)
	private int vBeforeFirstChild;
	// Vertical number of pixels before first child, if exists (after space after label)
	private int vBetweenChildren; // Vertical number of pixels between children
	private int vAfterLastChild; // Vertical number of pixels after last child, if exists
	private int hBeforeIcon; // Number of horizontal pixels before (left of) icon
	private int iconWidth = 13; // Width of the icon
	private int hAfterIcon; // Number of horizontal pixels following the icon (before label)
	private int hAfterChild;
	// Number of horizontal pixels between right of child frame and right of parent frame
	private int minimizedNodeHeight; // Height in pixels of minimized nodes
	private Font font; // Font for label
	private int textHeight = -1; // Height of label text
	private static final int LINE_WIDTH = 1; // Line width of DataRectangle frame (assumed to be constant)
	/**
	 * Creates a LayoutParameters structure with the given font and basic offset. 
	 * The different space sizes are calculated relative to this unit (.e.g. the vertical space before
	 * the label is equal to the basic offset, while the space after childnodes is equal to twice the basic offset 
	 *
	 * @param font The font used for labels of the tree
	 * @param offset The number of pixels used as the basic o unit in the tree.
	 */
	public DataLayoutParemeterSet(Font font, int basicOffset)
	{
		// FUNC3 The actual parameters were determined visually. Should they be parameterized?
		this.font = font;
		vBeforeLabel = basicOffset;
		vAfterLabel = basicOffset;
		vBeforeFirstChild = 0;
		vBetweenChildren = basicOffset;
		vAfterLastChild = basicOffset * 2;
		hBeforeIcon = basicOffset;
		hAfterIcon = basicOffset;
		hAfterChild = basicOffset * 2;
		minimizedNodeHeight = basicOffset;
	}
	/**
	 * @return The height in pixels of the current font.
	 */
	public int getTextHeight()
	{
		//FUNC3 refresh when preference changes
		if (textHeight == -1)
		{
			textHeight = FigureUtilities.getTextExtents("Ugly", font).height;
		}
		return textHeight;
	}
	/**
	 * 
	 * @returns The width difference (in pixels) between parent and child nodes.
	 */
	public int getHorizontalChildrenDelta()
	{
		return 2 * LINE_WIDTH + hBeforeIcon + iconWidth + hAfterIcon + hAfterChild;
	}
	/**
	 * @return The height in pizels of a "regular" a node (excluding the height of any children
	 * or extra height of repetitive nodes.
	 */
	public int getSimpleNodeHeight()
	{
		return 2 * LINE_WIDTH + vBeforeLabel + getTextHeight() + vAfterLabel;
	}
	/**
	 * @return The minimum extra height if any child nodes are displayed (equals to getVerticalSpaceAroundChildren() +  getMinimizedChildHeight()) 
	 */
	public int getMinimumChildrenHeight()
	{
		return vBeforeFirstChild + vAfterLastChild + minimizedNodeHeight;
	}
	/**
	 * @return The horizontal offset in pixels from the figure's boundary to the text (includes line width, space before icon, icon width and space after icon) 
	 */
	public int getTextHorizontalOffset()
	{
		return LINE_WIDTH + hBeforeIcon + iconWidth + hAfterIcon;
	}
	/**
	 * @return The horizontal offset to a child (currently equals to getTextHorizontalOffset())
	 */
	public int getChildHorizontalOffset()
	{
		return getTextHorizontalOffset();
	}
	/**
	 * @return The vertical offset in pixels form the figure's boundary to to the text (includes line width and vertical space before label )
	 */
	public int getTextVerticalOffset()
	{
		return LINE_WIDTH + vBeforeLabel;
	}
	/**
	 * @return The horizontal offset to the icon (includes line width and horizontal space before icon)
	 */
	public int getIconHorizontalOffset()
	{
		return LINE_WIDTH + hBeforeIcon;
	}
	/**
	 * @return The vertical offset in pixels form the figure's boundary to to the children area
	 *  (includes line width, vertical space before label, text height and vertical space after label, but excludes 
	 * vertical space before first child, which is logically part of the "children area" )
	 */
	public int getChildrenVerticalOffset()
	{
		return LINE_WIDTH + vBeforeLabel + getTextHeight() + vAfterLabel;
	}
	/**
	 * @return The height in pixels of a minimized node ( a node that appears as a thin rectangle with no label, icon or children)
	 */
	public int getMinimizedNodeHeight()
	{
		return minimizedNodeHeight;
	}
	/**
	 * @return The size in pixels of the vertical space between children
	 */
	public int getVerticalOffsetBetweenChildren()
	{
		return vBetweenChildren;
	}
	/**
	 * @return The font used for label text
	 */
	public Font getFont()
	{
		return font;
	}
	/**
	 * @return The maximum width (in pixels) allocated for icons.
	 */
	public int getIconWidth()
	{
		return iconWidth;
	}
	/**
	 * @return The size in pixels of the vertical space that separates the last child and the bottom part of the parent frame
	 */
	public int getVerticalOffsetAfterLastChild()
	{
		return vAfterLastChild;
	}
	/**
	 * @return Vertical number of pixels before first child, if exists (after space after label)
	 */
	public int getVerticalOffsetBeforeFirstChild()
	{
		return vBeforeFirstChild;
	}
	/**
	 * @return Size in pixels of the vertical space that surronds child nodes (equals to getVerticalOffsetAfterLastChild() + getVerticalSpaceAroundChildren() )
	 */
	public int getVerticalSpaceAroundChildren()
	{
		return vBeforeFirstChild + vAfterLastChild;
	}
	/**
	 * @return The vertical offset in pixels form the figure's boundary to to the position of super-minized children
	 */
	public int getSuperMinimizedVerticalOffset()
	{
		return getSimpleNodeHeight() - 3; // 1 pixel offset from the bottom
	}
	/**
	 * @return
	 */
	public int getSuperMinimizedNodeHeight()
	{
		return 1;
	}
}
/**
 * 
 * A Layout Manager that lays out DataElement "trees" inside flow rectangles.
 * 
 * A DataLayout lays out a whole DataElement hierarchy, so only a flow data element (top element) should have a DataLayout
 * 
 */
public class DataLayout implements LayoutManager
{
	/**
	 * A list of ParamerSets used to layout data trees (The sets are stored in decreasing preferrence order, the first one that "fits" will be used)
	 */
	static private DataLayoutParemeterSet[] parameterSets =
		new DataLayoutParemeterSet[] {
			new DataLayoutParemeterSet(
				Preferences.getFont(Preferences.FLOW_FONT_MEDIUM),
				Preferences.getInt(Preferences.SUB_DATA_PREFERRED_OFFSET)),
			new DataLayoutParemeterSet(
				Preferences.getFont(Preferences.FLOW_FONT_MEDIUM),
				Preferences.getInt(Preferences.SUB_DATA_MIN_OFFSET)),
			new DataLayoutParemeterSet(
				Preferences.getFont(Preferences.FLOW_FONT_SMALL),
				Preferences.getInt(Preferences.SUB_DATA_MIN_OFFSET))};
	// Constants indicating visual node status
	/* NICE3 move status constants to DataRectangle?
	 * The motivation for store status on DataRectangle (or event FlowRectangle) is to allow the links to check 
	 * the status of a node and update their behavior accordingly. The node itself might also use the status (e.g.
	 * in order not to paint sub-nodes
	 */
	/**	A NORMAL node is drawn fully (either open or closed) */
	private static final int NORMAL = 1;
	/** A MINIMIZED node appears minimized (a thin rectangle inside parent node) */
	private static final int MINIMIZED = 2;
	/** An OVERLAP_PREVIOUS node appears minimized, on top on previous minimized child of same parent */
	private static final int OVERLAP_PREVIOUS = 3;
	/** A SUPER_MINIMIZED node appears as a line near the bottom of parent node */
	private static final int SUPER_MINIMIZED = 4;
	/** An OVERLAP_PARENT node appears minimized or super-minimized, on top of the parent node (which is also minimized/super-minimized) */
	private static final int OVERLAP_PARENT = 5;
	/** A MINIMIZED_ROOT node appears as an empty box, whose size is the root's bounds */
	private static final int MINIMIZED_ROOT = 6;
	/* 
	 * We have to implement this method, but we are not using constraints
	 * @see org.eclipse.draw2d.LayoutManager#getConstraint(org.eclipse.draw2d.IFigure)
	 */
	public Object getConstraint(IFigure child)
	{
		return null;
	}
	/* We have to implement this method, but we are not using minimum sizes
	 * @see org.eclipse.draw2d.LayoutManager#getMinimumSize(org.eclipse.draw2d.IFigure, int, int)
	 */
	public Dimension getMinimumSize(IFigure container, int wHint, int hHint)
	{
		return null;
	}
	// Temporary lists used by the calculateState(), allocated once to avoid frequent object creation 
	List currentLevelNodes = new ArrayList();
	List nextLevelNodes = new ArrayList();
	/**
	 * Determine the status of nodes in the data tree, based on the total height available.
	 * 
	 * If there is enough space, and 
	 * If there is not enough space, the method returns false (if minimize=false) or sets the status to REGULAR to nodes 
	 * in as many layers as possible.  The nodes in the next layer will either be MINIMIZED (drawn as a thin rectangle) 
	 * ,OVERLAP_PREVIOUS (drawn as a thin rectangle that overlaps the previous child of the same parent), or SUPER_MINIMIZED  
	 * (drawn as a thin line near the bottom of the parent node)
	 *  
	 * @param root the root of the data tree (representing a flow data element) 
	 * @param minimize indicates whether nodes should be minimized if there is not enough space
	 * @return
	 */
	private boolean calculateStatus(DataRectangle root, boolean minimize)
	{
		int level = 0;
		remainingHeight = root.getBounds().height - parameters.getSimpleNodeHeight();
		if (root.isRepetitive())
			remainingHeight -= DataRectangle.STACK_OFFSET;
		if (remainingHeight < 0)
		{
			if (!minimize)
				return false;
			else
			{
				setNodeStatus(root, MINIMIZED_ROOT);
			}
			remainingHeight = 0;
			return true;
		}
		/*
		 * Algorithm:
		 * 
		 * Go down the layers of the tree , assuming at each step that there is enough
		 * space to draw the current level, and checking if enough space remains to draw the next level
		 * .  If there is not enough space for the next level, we either return false, or change 
		 * set the status of next level nodes to either MINIMIZED, OVERLAP_PREVIOUS, or SUPER-MINIMIZED, based on
		 * the amount of vertical space remaining,
		 * The status of  nodes in subsequent layers are set to OVERLAP_PARENT (drawn on top of the parent node) 
		 */
		currentLevelNodes.clear();
		currentLevelNodes.add(root);
		while (currentLevelNodes.size() > 0)
		{
			if (Debug.LAYOUT)
				Trace.push(
					"Level "
						+ level
						+ " nodes="
						+ currentLevelNodes.size()
						+ " remainingHeight="
						+ remainingHeight);
			try
			{
				int requiredHeightForNextLevel = 0;
				nextLevelNodes.clear();
				int nCompositeNodes = 0;
				/* Go over current level nodes, setting status to NORMAL, consuming the appropriate amount of vertical space,
				 * and calculating the vertical space required for next level nodes 
				 */
				for (int i = 0; i < currentLevelNodes.size(); i++)
				{
					DataRectangle node = (DataRectangle) currentLevelNodes.get(i);
					node.status = NORMAL;
					List subNodes = node.getSubNodes();
					int nSubNodes = subNodes.size();
					if (nSubNodes > 0)
					{
						nCompositeNodes++;
						//Caclculate the vertical space required for children
						requiredHeightForNextLevel += parameters.getVerticalSpaceAroundChildren();
						for (int childIndex = 0; childIndex < nSubNodes; childIndex++)
						{
							DataRectangle child = (DataRectangle) subNodes.get(childIndex);
							nextLevelNodes.add(child);
							int minimumChildHeight = parameters.getSimpleNodeHeight();
							if (child.isRepetitive())
							{
								minimumChildHeight += DataRectangle.STACK_OFFSET;
							}
							requiredHeightForNextLevel += minimumChildHeight;
							if (childIndex > 0)
								requiredHeightForNextLevel += parameters.getVerticalOffsetBetweenChildren();
							if (Debug.LAYOUT)
								Trace.add(
									"Next-level node " + child + " minimum height=" + minimumChildHeight);
						}
					}
				}
				if (Debug.LAYOUT)
				{
					Trace.add(
						"availableHeight="
							+ remainingHeight
							+ " requiredHeightForNextLevel="
							+ requiredHeightForNextLevel);
					Trace.add(
						"nCompositeNodes=" + nCompositeNodes + " nNextLevelNodes=" + nextLevelNodes.size());
				}
				if (requiredHeightForNextLevel <= remainingHeight)
				{
					List tmp = currentLevelNodes;
					currentLevelNodes = nextLevelNodes;
					remainingHeight -= requiredHeightForNextLevel;
					nextLevelNodes = tmp;
				}
				else
				{
					if (!minimize)
						return false;
					/*
					 * Calculate the size available for each next level node (excluding the first in each composite child, which will always be shown) 
					 */
					int minimizedNodeHeight = parameters.getMinimizedNodeHeight();
					if (remainingHeight
						< nCompositeNodes
							* (minimizedNodeHeight + parameters.getVerticalSpaceAroundChildren()))
					{
						// not enough space to show even minimized nodes
						for (int i = 0; i < currentLevelNodes.size(); i++)
						{
							DataRectangle node = (DataRectangle) currentLevelNodes.get(i);
							setChildrenStatus(node, SUPER_MINIMIZED);
						}
					}
					else
					{
						int nonInitialNextLevelNodes = nextLevelNodes.size() - nCompositeNodes;
						double sizePerNextLevelNode = 0;
						//						sizePerNextLevelNode will remain 0 if there are no 'nonInitialNextLevel Nodes'.  We don't care about the height in this case, but we need to prevent dividion by zero
						if (nonInitialNextLevelNodes > 0)
							sizePerNextLevelNode =
								(remainingHeight
									- nCompositeNodes
										* (minimizedNodeHeight + parameters.getVerticalSpaceAroundChildren()))
									/ (double) nonInitialNextLevelNodes;
						if (Debug.LAYOUT)
							Trace.add("sizePerNextLevelNode=" + sizePerNextLevelNode);
						for (int i = 0; i < currentLevelNodes.size(); i++)
						{
							DataRectangle node = (DataRectangle) currentLevelNodes.get(i);
							List subNodes = node.getSubNodes();
							int nSubNodes = subNodes.size();
							if (nSubNodes > 0)
							{
								int heightForChildren =
									(int) Math.floor(
										(nSubNodes - 1) * sizePerNextLevelNode + minimizedNodeHeight);
								if (Debug.LAYOUT)
									Trace.add(
										" node="
											+ node
											+ " nSubNodes="
											+ nSubNodes
											+ " heightForChildren="
											+ heightForChildren);
								remainingHeight -= parameters.getVerticalSpaceAroundChildren();
								for (int childIndex = 0; childIndex < nSubNodes; childIndex++)
								{
									DataRectangle childNode = (DataRectangle) subNodes.get(childIndex);
									/* Check that there is enough space for this child. 
									 */
									if (heightForChildren >= minimizedNodeHeight)
									{
										setNodeStatus(childNode, MINIMIZED);
										remainingHeight -= minimizedNodeHeight;
										heightForChildren -= minimizedNodeHeight;
										heightForChildren -= parameters.getVerticalOffsetBetweenChildren();
										if (childIndex > 0)
										{
											remainingHeight -= parameters.getVerticalOffsetBetweenChildren();
										}
									}
									else
									{
										/* The first child will always have enough space (previous validations). */
										Misc.assertion(childIndex > 0);
										setNodeStatus(childNode, OVERLAP_PREVIOUS);
									}
									if (Debug.LAYOUT)
										Trace.add(
											"Minimized child "
												+ childNode
												+ " overlap="
												+ (childNode.status == OVERLAP_PREVIOUS));
								}
							}
						}
					}
					currentLevelNodes.clear();
				}
				++level;
			}
			finally
			{
				if (Debug.LAYOUT)
					Trace.pop();
			}
		}
		return true;
	}
	/**
	 * Sets the status of a DataRectangle node, updating offsprings as needed:
	 * 
	 * If the status is NORMAL, offsprings are not updated 
	 * For any other status, offsprings are updated with status OVERLAP_PARENT
	 * @param node The DataRectangle whose status shold be changed
	 * @param status The new status for the node (one of NORMAL, MINIMIZED, SUPER_MINIMIZED, OVERLAP_PARENT and OVERLAP_PREVIOUS)
	 */
	private void setNodeStatus(DataRectangle node, int status)
	{
		node.status = status;
		if (status != NORMAL)
		{
			setChildrenStatus(node, OVERLAP_PARENT);
		}
	}
	/**
	 * 
	 * Sets the status of all the children of a DataRectangleNode to a given status, updating offsprings of the children as needed. 
	 * 
	 * This method is a convenience method that runs <code>setNodeStatus</code> on all the sub-nodes of the given node.
	 * 
	 * @param node The parent <code>DataRectangle</code> 
	 * @param status The status for child nodes.
	 */
	private void setChildrenStatus(DataRectangle parent, int status)
	{
		List subNodes = parent.getSubNodes();
		int nSubNodes = subNodes.size();
		for (int i = 0; i < nSubNodes; i++)
			setNodeStatus((DataRectangle) subNodes.get(i), status);
	}
	/**
	 * Lays out a DataRectangle hierarchy, using the current layout parameters.  If the <code>root</code> ( a flow 
	 * data element)  isn't tall enough to draw all the nodes, this method will either return false (if <code>minimize</code> is false,
	 * or minimize the lower layers in the hierarchy.
	 * 
	 * The size level signif
	 * 
	 * @param root - The root of the hierarchy, a DataRectangle  
	 * @param minimize
	 * @return
	 */
	private boolean layout(DataRectangle root, boolean minimize)
	{
		if (Debug.LAYOUT)
		{
			Trace.push("Trying layout textHeight=" + parameters.getTextHeight());
		}
		try
		{
			if (!calculateStatus(root, minimize))
				return false;
			Misc.assertion(remainingHeight >= 0);
			Rectangle bounds = root.getBounds();
			layoutNode(root, bounds.x, bounds.y, bounds.width, remainingHeight, false);
			return true;
		}
		finally
		{
			if (Debug.LAYOUT)
			{
				Trace.pop();
			}
		}
	}
	/**
	 * @param root
	 */
	private static Rectangle tmpRect = new Rectangle();
	private void layoutToggle(DataRectangle parent)
	{
		OpenToggle toggle;
		List children = parent.getChildren();
		Rectangle frame = parent.getFrame();
		if (children.size() > 0)
		{
			toggle = (OpenToggle) children.get(0);
			if (Math.min(frame.width, frame.height) < OpenToggle.SIZE)
			{
				toggle.setVisible(false);
				return;
			}
			toggle.setVisible(true);
			tmpRect.width = OpenToggle.SIZE;
			tmpRect.height = OpenToggle.SIZE;
			tmpRect.x = frame.x + frame.width - tmpRect.width;
			tmpRect.y = frame.y;
			if (Debug.LAYOUT)
				Trace.add("Toggle bounds:" + tmpRect);
			toggle.setBounds(tmpRect);
		}
	} /* (non-Javadoc)
																																																
																																																	/**
																																																	 * @param root
																																																	 */
	private int layoutNode(
		DataRectangle node,
		int x,
		int y,
		int width,
		int extraHeightAroundChildren,
		boolean setBounds)
	{
		List subNodes = node.getSubNodes();
		int nSubNodes = subNodes.size();
		if (Debug.LAYOUT)
		{
			Trace.push(
				"Layout node:"
					+ node
					+ " status="
					+ node.status
					+ " x="
					+ x
					+ " y="
					+ y
					+ " width="
					+ width
					+ " extraHeight="
					+ extraHeightAroundChildren);
		}
		try
		{
			if (node.status == NORMAL)
			{
				int childrenHeight = 0;
				int currentY = y + parameters.getChildrenVerticalOffset() + extraHeightAroundChildren / 2;
				int previousY = currentY;
				int childWidth = width - parameters.getHorizontalChildrenDelta();
				if (node.isRepetitive())
					childWidth -= DataRectangle.STACK_OFFSET;
				int childX = x + parameters.getChildHorizontalOffset();
				for (int i = 0; i < nSubNodes; i++)
				{
					DataRectangle child = (DataRectangle) subNodes.get(i);
					if (Debug.LAYOUT)
					{
						Trace.add("child:" + child);
					}
					if (child.status == OVERLAP_PREVIOUS)
					{
						Misc.assertion(i > 0); // first child can't overlap previous
						layoutNode(child, childX, previousY, childWidth, 0, true);
					}
					else if (child.status == SUPER_MINIMIZED)
					{
						layoutNode(
							child,
							childX,
							y + parameters.getSuperMinimizedVerticalOffset(),
							childWidth,
							0,
							true);
					}
					else
					{
						Misc.assertion(child.status == MINIMIZED || child.status == NORMAL);
						if (i > 0)
						{
							currentY += parameters.getVerticalOffsetBetweenChildren();
							childrenHeight += parameters.getVerticalOffsetBetweenChildren();
						}
						int height = layoutNode(child, childX, currentY, childWidth, 0, true);
						previousY = currentY;
						// If the next node is overlapping, it will have the same y as this one
						currentY += height;
						childrenHeight += height;
					}
				}
				int height = parameters.getSimpleNodeHeight();
				if (node.isRepetitive())
					height += DataRectangle.STACK_OFFSET;
				if (childrenHeight > 0)
				{
					height += childrenHeight;
					height += parameters.getVerticalSpaceAroundChildren();
					height += extraHeightAroundChildren;
				}
				if (setBounds)
					node.setBounds(x, y, width, height);
				
				setFrame(node);
			}
			else if (
				node.status == SUPER_MINIMIZED
					|| node.status == MINIMIZED
					|| node.status == OVERLAP_PREVIOUS)
			{
				int height =
					node.status == SUPER_MINIMIZED
						? parameters.getSuperMinimizedNodeHeight()
						: parameters.getMinimizedNodeHeight();
				node.setBounds(x, y, width, height);
				node.setFrame(x, y, width, height);
				for (int i = 0; i < nSubNodes; i++)
				{
					DataRectangle child = (DataRectangle) subNodes.get(i);
					Misc.assertion(child.status == OVERLAP_PARENT);
					layoutNode(child, x, y, width, 0, true);
				}
			}
			else if (node.status == OVERLAP_PARENT)
			{
				int height = node.getParent().getBounds().height;
				node.setBounds(x, y, width, height);
				node.setFrame(x, y, width, height);
				for (int i = 0; i < nSubNodes; i++)
				{
					DataRectangle child = (DataRectangle) subNodes.get(i);
					Misc.assertion(child.status == OVERLAP_PARENT);
					layoutNode(child, x, y, width, 0, true);
				}
			}
			else
			{
				Misc.assertion(node.status == MINIMIZED_ROOT);
				setFrame(node);
				for (int i = 0; i < nSubNodes; i++)
				{
					DataRectangle child = (DataRectangle) subNodes.get(i);
					Misc.assertion(child.status == OVERLAP_PARENT);
					layoutNode(child, x, y, width, 0, true);
				}
			}
			layoutToggle(node);
			layoutTextAndImage(node, x, y);
			//layoutLockedImage(node, x, y);
			node.setValid(true);
			if (Debug.LAYOUT)
			{
				Trace.add("Bounds:" + node.getBounds() + " Frame:" + node.getFrame());
			}
			return node.getBounds().height;
		}
		finally
		{
			if (Debug.LAYOUT)
			{
				Trace.pop();
			}
		}
	}
	private void layoutTextAndImage(DataRectangle node, int x, int y)
	{
		if (node.status != NORMAL)
		{
			node.setDrawImage(false);
			node.paintText = false;
			return;
		}
		node.setDrawImage(false);
		node.setFont(parameters.getFont());
		node.paintText = true;
		int textX = x + parameters.getTextHorizontalOffset();
		int textY = y + parameters.getTextVerticalOffset();
		node.setTextPosition(textX, textY);
		node.setTextHeight(parameters.getTextHeight());
		if (node.hasImage())
		{
			int textHeight = parameters.getTextHeight();
			Image image = node.getImageLocator().getImage(textHeight, parameters.getIconWidth());
			int imageWidth = image.getBounds().width;
			int imageHeight = image.getBounds().height;
			if (imageHeight <= textHeight && imageWidth <= parameters.getIconWidth())
			{
				node.setDrawImage(true);
				node.imageFrame.setSize(imageWidth, imageHeight);
				node.targetFrame.setSize(imageWidth, imageHeight);
				node.targetFrame.setLocation(
					x + parameters.getIconHorizontalOffset(),
					y + parameters.getTextVerticalOffset() + (textHeight - imageHeight) / 2);
			}
		}
		
		Image lockedImage = node.getLockedImage();
		PrecisionRectangle frame = node.getFrame();
		
		int lockedImageHeight = lockedImage.getBounds().height;
		int lockedImageWidth = lockedImage.getBounds().width;
		
		node.lockedTargetFrame.setLocation(frame.x + 2, frame.y + frame.height - lockedImageHeight);
		node.lockedTargetFrame.setSize(lockedImageWidth - 2, lockedImageHeight - 2);
	}
	
	public Dimension getPreferredSize(IFigure container, int availableWidth, int availableHeight)
	{
		return null;
	}
	/* (non-Javadoc)
	 * @see org.eclipse.draw2d.LayoutManager#invalidate()
	 */
	public void invalidate()
	{
	}
	/* 
	 * Calucates the frame of a DataRectangle, and the bounds of its children
	 */
	/**
	 * Lays out the children of this DataRectangle
	 * @param font Font for labels
	 * @param offset number of "space" pixels (between nodes and between text and lines)
	 * @param force indicates whether layout should be forced, or whether the method should return if there 
	 * isn't enough height to show the labels of all nodes. 
	 */
	private DataLayoutParemeterSet parameters;
	private int remainingHeight;
	public void layout(IFigure container)
	{
		Misc.assertion(container instanceof DataRectangle);
		DataRectangle rectangle = (DataRectangle) container;
		/* Things are going to change so the figure will need to be repainted on the next update.
		 * If we don't call repaint, not all of the figure will be repainted (test case in Data2#openNested, Data2#closeNested). 
		 * Probably, calling repaint would be unnecessary if the label was a separate figure 
		 */
		rectangle.repaint();
		if (Debug.LAYOUT)
		{
			Trace.push("Calculating layout for " + container + " bounds=" + container.getBounds());
		}
		try
		{
			if (rectangle.isOpen() || rectangle.getChildren().size() == 0)
			{
				// Try to lay out using 'preferred' layout parameters
				for (int sizeLevel = 0; sizeLevel < parameterSets.length - 1; sizeLevel++)
				{
					parameters = parameterSets[sizeLevel];
					if (layout(rectangle, false))
						return;
				}
				// layout using the most compact parameters, minimizing nodes as necessary
				parameters = parameterSets[parameterSets.length - 1];
				layout(rectangle, true);
			}
			else
			{
				setFrame(rectangle);
				FlowLayout.layoutTextAndImage(rectangle);
				layoutToggle(rectangle);
			}
		}
		finally
		{
			if (Debug.LAYOUT)
				Trace.pop();
		}
	}
	/**
	 * Sets the frame of a <code>DataRectangle</code> node, assuming its bounds are set.
	 * 
	 * If the node is non-repetitive, the frame is to to be equal to the bounds.
	 * If the node is repetitive, the frame's width and height are set to be <code>DataRectangle.STACK_OFFSET</code>
	 * pixels shorter (allowing space for 1 "stacked frame")
	 * 
	 * @param node The <code>DataRectangle</code> whose frame is to be set. 
	 */
	private void setFrame(DataRectangle node)
	{
		PrecisionRectangle bounds = node.getPreciseBounds();
		if (node.isRepetitive())
		{
			node.setFrame(
				bounds.x,
				bounds.y,
				bounds.width - DataRectangle.STACK_OFFSET,
				bounds.height - DataRectangle.STACK_OFFSET);
		}
		else
			node.setFrame(bounds);
	}
	public void remove(IFigure child)
	{
	}
	public void setConstraint(IFigure child, Object constraint)
	{
	}
	/**
	 * @return
	 */
}

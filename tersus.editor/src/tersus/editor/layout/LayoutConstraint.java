/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.layout;

import tersus.model.FlowModel;
import tersus.model.ModelElement;
import tersus.model.RelativePosition;
import tersus.model.RelativeSize;

/**
 * 
 */
public class LayoutConstraint
{
	/**
	 * @param x
	 * @param y
	 */
	public LayoutConstraint(double x, double y)
	{

		position = new RelativePosition(x, y);
	}
	/**
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public LayoutConstraint(double x, double y, double width, double height)
	{
		position = new RelativePosition(x, y);
		size = new RelativeSize(width, height);
	}
	/**
	 * @param element
	 */
	private RelativePosition position = new RelativePosition(0, 0);
	private RelativeSize size = new RelativeSize(0, 0);
	public LayoutConstraint()
	{
	}
	public LayoutConstraint(ModelElement element)
	{
		setPosition(element.getPosition());
		if (element.getSize() != null)
			setSize(element.getSize());
		else
		    setSize(0.25,0.25);
	}
	// TODO remove this constructor (it does the same as previous one) 
	public LayoutConstraint(FlowModel parentModel, ModelElement element)
	{
		setPosition(element.getPosition());
		RelativeSize relativeSize = element.getSize();
		double maxWidth = relativeSize.getWidth();
		double maxHeight = relativeSize.getHeight();
		setSize(maxWidth, maxHeight);
	}

	/**
	 * @return
	 */
	public RelativePosition getPosition()
	{
		return position;
	}

	/**
	 * @return
	 */
	public RelativeSize getSize()
	{
		return size;
	}
	/**
	 * @return
	 */
	public double getX()
	{

		return position.getX();
	}

	public double getY()
	{

		return position.getY();
	}
	/**
	 * @return
	 */
	public double getHeight()
	{
		return size.getHeight();
	}
	/**
	 * @return
	 */
	public double getWidth()
	{
		return size.getWidth();
	}
	/**
	 * @param size
	 */
	public void setSize(RelativeSize size)
	{
		this.size.copy(size);
	}
	
	public void setSize(double width, double height)
	{
		size.setHeight(height);
		size.setWidth(width);
	}

	public void setPosition(RelativePosition position)
	{
	    if (position != null)
	        setPosition(position.getX(), position.getY());
	    else
	        setPosition(0.5,0.5);
	}
	/**
	 * @param x
	 * @param y
	 */
	public void setPosition(double x, double y)
	{
		position.setX(x);
		position.setY(y);

	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		return "LayoutConstraint[position=" + position + " size=" + size + "]";
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gef.tools.DragEditPartsTracker;

import tersus.editor.editparts.*;
import tersus.editor.useractions.Comment;
import tersus.util.Trace;

/**
 * 
 */
public class TersusDragTracker extends DragEditPartsTracker
{

	/**
	 * @param sourceEditPart
	 */
	public TersusDragTracker(EditPart sourceEditPart)
	{
		super(sourceEditPart);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.tools.DragEditPartsTracker#performDrag()
	 */
	protected void performDrag()
	{
		// TODO Review this auto-generated method stub
		boolean tracePushed = false;
		
		if (Debug.USER_ACTIONS)
		{
			ChangeBoundsRequest req = (ChangeBoundsRequest) getTargetRequest();
			if (TersusXYLayoutEditPolicy.REQ_MOVE.equals(req.getType()))
			{
				if (getCurrentCommand() != null
					&& getCurrentCommand().canExecute()
					&& req.getEditParts().size() == 1)
				{
					Trace.push(
						new Comment(
							"Move Element path=\""
								+ ((TersusEditPart) getTargetEditPart()).getPath()
								+ "\" delta="
								+ req.getMoveDelta()));
					tracePushed = true;
				}
				//TODO: support moving multiple elements
			}
			if (TersusXYLayoutEditPolicy.REQ_RESIZE.equals(req.getType()))
			{
				if (getCurrentCommand() != null
					&& getCurrentCommand().canExecute()
					&& req.getEditParts().size() == 1)
				{
					Trace.push(
						new Comment(
							"Resize Element path=\""
								+ ((TersusEditPart) getTargetEditPart()).getPath()
								+ "\" delta="
								+ req.getSizeDelta()));
					tracePushed = true;
				}
				//TODO: support moving multiple elements
			}

		}
		try
		{
			super.performDrag();
		}
		finally
		{
			if (tracePushed)
				Trace.pop();
		}
	}
}

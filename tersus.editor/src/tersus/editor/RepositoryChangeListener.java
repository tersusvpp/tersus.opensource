package tersus.editor;

import java.util.HashMap;
import java.util.HashSet;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

import tersus.model.PackageFormat;
import tersus.model.Repository;
import tersus.workbench.TersusWorkbench;

/**
 * Listens to workspace for changes in 'models' folders, and notifies users for such changes.
 * 
 * This works with some 'tricks':
 * 
 * In order to ignore changes caused by saving models, we use the lastSave timestamp. Any changes
 * detected up to SAVE_DELAY_THRESHOLD milliseconds after a save are ignored.
 * 
 * In order to prevent raising multiple dialogs for the same project, we use the set
 * 'projectBeingModified'. When we discover that a project has been changed, we add it to the set,
 * and remove it from the set after the dialog is closed.
 * 
 * @author youval
 * 
 */
public class RepositoryChangeListener implements IResourceChangeListener
{
	private long lastSave = 0;
	private boolean disabled = false;
	private static final long SAVE_DELAY_THRESHOLD = 5000;
	private static final String SVN_FOLDER_NAME = ".svn";
	private static final String CVS_FOLDER_NAME = ".cvs";
	private static RepositoryChangeListener instance = new RepositoryChangeListener();
	private HashSet<IProject> projectsBeingNotified = new HashSet();

	public static RepositoryChangeListener getInstance()
	{
		return instance;
	}

	private RepositoryChangeListener()
	{

	}

	synchronized public void resourceChanged(IResourceChangeEvent event)
	{
		if (isDisabled())
			return;
		long now = System.currentTimeMillis();
		if (getLastSave() + SAVE_DELAY_THRESHOLD < now)
			handleResourceDelta(event.getDelta());
		else
			setLastSave(now); // Update "lastSave" so that a long operation with many events won't
								// eventually begin to display notifications
	}

	private void handleResourceDelta(IResourceDelta delta)
	{
		if (delta == null)
			return;
		IResource res = delta.getResource();
		final IProject project = res.getProject();
		if (res instanceof IFolder
				&& res.equals(TersusWorkbench.getRepositoryRoot(project))
				&& (delta.getKind() == IResourceDelta.ADDED || delta.getKind() == IResourceDelta.REMOVED))
		{
			// Ignore newly created, closed, opened or deleted projects
			setLastSave(System.currentTimeMillis()); 
			return;
		}
		if (res instanceof IFolder
				&& (CVS_FOLDER_NAME.equals(res.getName()) || SVN_FOLDER_NAME.equals(res.getName())))
			return;
		if (res instanceof IFolder && !TersusWorkbench.getRepositoryRoot(project).contains(res))
			return;
		if (res instanceof IFile && TersusWorkbench.getRepositoryRoot(project).contains(res)
				&& Repository.PACKAGE_EXTENSION.equals(res.getFileExtension()))
		{
			if (!projectsBeingNotified.contains(project) && Display.getDefault() != null)
			{
				projectsBeingNotified.add(project);
				Display.getDefault().asyncExec(new Runnable()
				{
					public void run()
					{
						modelsChanged(project);
					}
				});
			}
			return;
		}
		IResourceDelta[] deltas = delta.getAffectedChildren();
		for (int i = 0; i < deltas.length; i++)
		{
			handleResourceDelta(deltas[i]);
		}

	}

	private void modelsChanged(final IProject project)
	{
		MessageDialog
				.openInformation(
						TersusWorkbench.getActiveWorkbenchShell(),
						"Models changed externally",
						"Models in project "
								+ project.getName()
								+ " have been modified externally. You may want to refresh or revert the project to avoid overwriting these changes.");
		synchronized (this)
		{
			projectsBeingNotified.remove(project);
		}
	}

	public long getLastSave()
	{
		return lastSave;
	}

	public void setLastSave(long lastSave)
	{
		this.lastSave = lastSave;
	}

	public boolean isDisabled()
	{
		return disabled;
	}

	public void setDisabled(boolean disabled)
	{
		this.disabled = disabled;
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor;

import org.eclipse.draw2d.Viewport;
import org.eclipse.gef.editparts.FreeformGraphicalRootEditPart;

/**
 * 
 */
public class TersusRootEditPart	extends FreeformGraphicalRootEditPart
{


	private TersusZoomManager zoomManager;

	/**
	 * 
	 */
	public TersusRootEditPart()
	{
		zoomManager = new TersusZoomManager(this,
										((Viewport)getFigure()));
	}

	/**
	 * @return
	 */
	public TersusZoomManager getZoomManager()
	{
		return zoomManager;
	}

}
/*******************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others. All rights reserved.
 * 
 * This program is made available under the terms of the GNU General Public
 * License v2, which is part of this distribution and is available at
 * http://www.gnu.org/licenses/gpl.txt.
 * 
 * Contributors: Tersus Software Ltd.� Initial API and implementation
 ******************************************************************************/
package tersus.editor.palette;

import java.util.ArrayList;

import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.FilteredTree;
import org.eclipse.ui.dialogs.PatternFilter;
import org.eclipse.ui.dialogs.SelectionDialog;

/**
 * @author Liat Shiff
 */
public class PaletteTreeSelectionDialog extends SelectionDialog
{
	private PaletteRoot root;
	
	private ElementCreationToolEntry selectedEntry;
	
	private Composite composite;
	
	public PaletteTreeSelectionDialog(Shell parentShell, PaletteRoot root) 
	{
		super(parentShell);
		
		this.root = root;
		this.setTitle("Palette Search");
	}

	protected Control createDialogArea(Composite parent) 
	{
		composite = (Composite) super.createDialogArea(parent);
        initializeDialogUnits(composite);

        PatternFilter filter = new PatternFilter();
		int styleBits = SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER;
        FilteredTree filteredTree = new FilteredTree(composite, styleBits, filter);
        TreeViewer viewer = filteredTree.getViewer();
        
        viewer.setLabelProvider(new PaletteSearchLabelProvider());
        viewer.setContentProvider(new PaletteSearchContentProvider());
        viewer.setInput(root);
        
        viewer.addSelectionChangedListener(new ISelectionChangedListener()
        {
			public void selectionChanged(SelectionChangedEvent event) 
			{
				Object obj = event.getSelection();
				
				if (obj instanceof TreeSelection)
				{
					TreeSelection treeSelection = (TreeSelection)obj;
					
					if (treeSelection.size() == 1 && 
							treeSelection.getFirstElement() instanceof ElementCreationToolEntry)
					{
						selectedEntry = (ElementCreationToolEntry)treeSelection.getFirstElement();
						getOkButton().setEnabled(true);
					}
					else 
						getOkButton().setEnabled(false);
					
				}
			}
        });
        
        viewer.addDoubleClickListener(new IDoubleClickListener()
        {
			public void doubleClick(DoubleClickEvent event) 
			{
				Object obj = event.getSelection();
				if (obj instanceof TreeSelection)
				{
					TreeSelection treeSelection = (TreeSelection)obj;
					
					if (treeSelection.size() == 1 && 
							treeSelection.getFirstElement() instanceof ElementCreationToolEntry)
					{
						selectedEntry = (ElementCreationToolEntry)treeSelection.getFirstElement();
						okPressed();
					}
					else 
						getOkButton().setEnabled(false);
				}
			}
        });
        
        return composite;
    }
	
	protected void createButtonsForButtonBar(Composite parent) 
	{
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
		getOkButton().setEnabled(false);
	}
	
	protected void okPressed() 
    {
        ArrayList<ElementCreationToolEntry> resultSet = new ArrayList<ElementCreationToolEntry>();
        resultSet.add(selectedEntry);
        setResult(resultSet);
        
        super.okPressed();
    }
}

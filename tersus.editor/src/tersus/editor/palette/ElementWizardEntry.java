/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.palette;

import java.io.IOException;

import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.PaletteEntry;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.jface.resource.ImageDescriptor;
import org.xml.sax.Attributes;

import tersus.editor.TersusEditor;
import tersus.editor.tools.ElementCreationWizard;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.ModelObject;
import tersus.model.Repository;
import tersus.util.XMLWriter;
import tersus.workbench.ImageLocator;

/**
 * @author Ofer Brrandes
 *  
 */
public class ElementWizardEntry extends ToolEntry implements
        SelfConfiguringEntry
{

    private static final String MODEL_ID_ATTRIBUTE_NAME = "modelId";

    public static final String ENTRY_TAG = "ModelWizard";

    private ModelObject source;

    /**
     * @param label
     * @param shortDesc
     * @param iconSmall
     * @param iconLarge
     * @param source -
     *            The model object to be used to create the element
     *  
     */
    public ElementWizardEntry(String label, String shortDesc,
            ImageDescriptor iconSmall, ImageDescriptor iconLarge,
            ModelObject source)
    {
        super(label, shortDesc, iconSmall, iconLarge);
        setSource(source);

    }

    public Tool createTool()
    {
        return new ElementCreationWizard(getSource());
    }

    /**
     * @return Returns the source.
     */
    public ModelObject getSource()
    {
        return source;
    }

    /**
     * @param source
     *            The source to set.
     */
    public void setSource(ModelObject source)
    {
        this.source = source;
    }

    /**
     * Writes non-standard properties to the palette description file (using an
     * XMLWriter)
     * 
     * @param writer
     */
    public void writeAttributes(XMLWriter writer) throws IOException
    {
        if (getSource() instanceof Model)
            writer.writeAttribute(MODEL_ID_ATTRIBUTE_NAME,
                    ((Model) getSource()).getId());
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.editor.palette.SelfConfuringEntry#getTag()
     */
    public String getTag()
    {
        return ENTRY_TAG;
    }

    /**
     * @param attributes
     * @param repository
     * @return
     */
    public static PaletteEntry create(Attributes attributes,
            Repository repository)
    {
        String modelIDStr = attributes.getValue(MODEL_ID_ATTRIBUTE_NAME);
        ModelId modelId = null;
        Model model = null;
        String name = "?";
        ImageDescriptor smallIcon = ImageDescriptor.createFromFile(TersusEditor.class,
        "icons/unknown20.gif"); //$NON-NLS-1$
        
        ImageDescriptor largeIcon =ImageDescriptor.createFromFile(TersusEditor.class,
        "icons/unknown20.gif");
        if (modelIDStr != null)
        {
        modelId = new ModelId(modelIDStr);
        model = repository.getModel(modelId, true);
        }
        if (model != null)
        {
            ImageLocator locator = TersusEditor.getImageLocator(model);
            if (locator != null)
            {
                smallIcon = locator.getImageDescriptor(20, 20);
                largeIcon = locator.getImageDescriptor(32, 32);
            }
            name = model.getName();
        }
        String description = null; //FUNC3 add description elements
        ToolEntry entry = new ElementWizardEntry(name, description,
                smallIcon, largeIcon, model);
        PaletteConfigurator
                .copyDefaultPaletteEntryAttributes(attributes, entry);
        if (model == null && modelId != null)
        {
            entry.setLabel("Missing Model '"+modelId.getName()+"'");
            entry.setDescription(modelIDStr);
        }
        return entry;
    }

}
/*******************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others. All rights reserved.
 * 
 * This program is made available under the terms of the GNU General Public
 * License v2, which is part of this distribution and is available at
 * http://www.gnu.org/licenses/gpl.txt.
 * 
 * Contributors: Tersus Software Ltd.� Initial API and implementation
 ******************************************************************************/
package tersus.editor.palette;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;

import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

/**
 * @author Liat Shiff
 */
public class PaletteSearchContentProvider implements ITreeContentProvider
{
	PaletteRoot paletteRoot;

	public Object[] getChildren(Object parentElement)
	{
		ArrayList<Object> elements = new ArrayList<Object>();

		if (parentElement instanceof PaletteRoot)
		{
			elements.addAll(((PaletteRoot) parentElement).getChildren());

			for (Object obj : elements)
			{
				if (!(obj instanceof PaletteDrawer))
					elements.remove(obj);
			}
		}
		if (parentElement instanceof PaletteDrawer)
		{
			elements.addAll(((PaletteDrawer) parentElement).getChildren());
		}

		if (elements.isEmpty())
			return null;
		else
			return sortObjectList(elements.toArray());
	}

	public Object getParent(Object element)
	{
		if (element instanceof PaletteContainer)
			return ((PaletteDrawer) element).getParent();

		return null;
	}

	public boolean hasChildren(Object element)
	{
		if (element instanceof PaletteDrawer)
			return true;

		return false;
	}

	public Object[] getElements(Object inputElement)
	{
		HashSet<PaletteDrawer> elements = null;

		if (inputElement instanceof PaletteRoot)
		{
			elements = new HashSet<PaletteDrawer>();
			ArrayList<Object> objects = new ArrayList<Object>();
			objects.addAll(((PaletteRoot) inputElement).getChildren());

			for (Object obj : objects)
			{
				if (obj instanceof PaletteDrawer)
					elements.add((PaletteDrawer) obj);
			}
		}
		if (elements != null)
			return sortObjectList(elements.toArray());
		else
			return null;
	}

	public void dispose()
	{
		System.out.println("dispose");

	}

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
	{
		System.out.println("input changed");

	}

	private Object[] sortObjectList(Object[] objects)
	{
		ArrayList<Object> sortObjects = new ArrayList<Object>();

		for (Object obj : objects)
		{
			sortObjects.add(obj);
		}

		Collections.sort(sortObjects, new Comparator<Object>()
		{
			public int compare(Object o1, Object o2)
			{
				String o1Label = null;
				String o2Label = null;

				if (o1 instanceof PaletteDrawer)
					o1Label = ((PaletteDrawer) o1).getLabel();
				else if (o1 instanceof ElementCreationToolEntry)
					o1Label = ((ElementCreationToolEntry) o1).getLabel();
				else
					o1Label = ((ElementWizardEntry) o1).getLabel();

				if (o2 instanceof PaletteDrawer)
					o2Label = ((PaletteDrawer) o2).getLabel();
				else if (o2 instanceof ElementCreationToolEntry)
					o2Label = ((ElementCreationToolEntry) o2).getLabel();
				else
					o2Label = ((ElementWizardEntry) o2).getLabel();

				return o1Label.compareTo(o2Label);
			}
		});

		return sortObjects.toArray();
	}

}

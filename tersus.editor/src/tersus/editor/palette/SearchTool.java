/*******************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others. All rights reserved.
 * 
 * This program is made available under the terms of the GNU General Public
 * License v2, which is part of this distribution and is available at
 * http://www.gnu.org/licenses/gpl.txt.
 * 
 * Contributors: Tersus Software Ltd.� Initial API and implementation
 ******************************************************************************/
package tersus.editor.palette;

import java.util.Map;

import org.eclipse.gef.EditDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.Tool;
import org.eclipse.gef.internal.ui.palette.editparts.DrawerEditPart;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteGroup;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.SelectionToolEntry;
import org.eclipse.gef.ui.palette.PaletteViewer;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.widgets.Event;

import tersus.editor.TersusEditor;

/**
 * @author Liat Shiff
 */
public class SearchTool implements Tool
{
	private PaletteRoot root;
	
	private TersusEditor editor;
	
	public SearchTool(PaletteRoot root, TersusEditor editor)
	{
		this.root = root;
		this.editor = editor;
	}
	
	private SelectionToolEntry getSelectionEntry()
	{
		for (Object obj: root.getChildren())
		{
			if(obj instanceof PaletteGroup)
			{
				for (Object groupChild: ((PaletteGroup)obj).getChildren())
				{
					if(groupChild instanceof SelectionToolEntry)
						return (SelectionToolEntry)groupChild;	
				}
				return null;		
			}
		}
		return null;
	}
	
	public PaletteTreeSelectionDialog openDialog()
    {
		PaletteTreeSelectionDialog dialog = new PaletteTreeSelectionDialog(editor.getSite().getShell(), root);
		dialog.setBlockOnOpen(true);
		dialog.create();
		 
		dialog.open();
		
		return dialog;
    }

	public void activate() 
	{
		PaletteTreeSelectionDialog dialog = openDialog();
		Object[] selectedColumns = dialog.getResult();
		
		if (selectedColumns == null || selectedColumns.length == 0)
		{
			SelectionToolEntry entry = getSelectionEntry();
			if (entry != null && editor != null)
			{
				editor.getCurrentPaletteViewer().deselectAll();
				editor.getCurrentPaletteViewer().setActiveTool(entry);
			}
		}
		else
		{
			if (selectedColumns[0] instanceof ElementCreationToolEntry)
			{
				if (editor != null)
				{
					PaletteViewer viewer = editor.getCurrentPaletteViewer();
					viewer.deselectAll();
					
					ElementCreationToolEntry elementCreationToolEntry = (ElementCreationToolEntry)selectedColumns[0];
					viewer.setActiveTool(elementCreationToolEntry);
					
					PaletteContainer container = elementCreationToolEntry.getParent();
					EditPart ep = (EditPart)viewer.getEditPartRegistry().get(container);
					if (ep instanceof DrawerEditPart)
						((DrawerEditPart)ep).setExpanded(true);
				}
			}
		}
		
	}

	public void deactivate() 
	{
		
	}

	public void focusGained(FocusEvent event, EditPartViewer viewer) {
		// TODO Auto-generated method stub
		
	}

	public void focusLost(FocusEvent event, EditPartViewer viewer) {
		// TODO Auto-generated method stub
		
	}

	public void keyDown(KeyEvent keyEvent, EditPartViewer viewer) {
		// TODO Auto-generated method stub
		
	}

	public void keyTraversed(TraverseEvent event, EditPartViewer viewer) {
		// TODO Auto-generated method stub
		
	}

	public void keyUp(KeyEvent keyEvent, EditPartViewer viewer) {
		// TODO Auto-generated method stub
		
	}

	public void mouseDoubleClick(MouseEvent mouseEvent, EditPartViewer viewer) {
		// TODO Auto-generated method stub
		
	}

	public void mouseDown(MouseEvent mouseEvent, EditPartViewer viewer) {
		// TODO Auto-generated method stub
		
	}

	public void mouseDrag(MouseEvent mouseEvent, EditPartViewer viewer) {
		// TODO Auto-generated method stub
		
	}

	public void mouseHover(MouseEvent mouseEvent, EditPartViewer viewer) {
		// TODO Auto-generated method stub
		
	}

	public void mouseMove(MouseEvent mouseEvent, EditPartViewer viewer) {
		// TODO Auto-generated method stub
		
	}

	public void mouseUp(MouseEvent mouseEvent, EditPartViewer viewer) {
		// TODO Auto-generated method stub
		
	}

	public void mouseWheelScrolled(Event event, EditPartViewer viewer) {
		// TODO Auto-generated method stub
		
	}

	public void nativeDragFinished(DragSourceEvent event, EditPartViewer viewer) {
		// TODO Auto-generated method stub
		
	}

	public void nativeDragStarted(DragSourceEvent event, EditPartViewer viewer) {
		// TODO Auto-generated method stub
		
	}

	public void setEditDomain(EditDomain domain) {
		// TODO Auto-generated method stub
		
	}

	public void setProperties(Map properties) {
		// TODO Auto-generated method stub
		
	}

	public void setViewer(EditPartViewer viewer) {
		// TODO Auto-generated method stub
		
	}

	public void viewerEntered(MouseEvent mouseEvent, EditPartViewer viewer) {
		// TODO Auto-generated method stub
		
	}

	public void viewerExited(MouseEvent mouseEvent, EditPartViewer viewer) {
		// TODO Auto-generated method stub
		
	}

}

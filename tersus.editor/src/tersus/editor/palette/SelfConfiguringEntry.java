/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.palette;

import java.io.IOException;

import tersus.util.XMLWriter;

/**
 * @author Youval Bronicki
 *
 */
public interface SelfConfiguringEntry
{

    /**
     * @param writer
     * @throws IOException
     */
    void writeAttributes(XMLWriter writer) throws IOException;
    String getTag();
}

/*******************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others. All rights reserved.
 * 
 * This program is made available under the terms of the GNU General Public
 * License v2, which is part of this distribution and is available at
 * http://www.gnu.org/licenses/gpl.txt.
 * 
 * Contributors: Tersus Software Ltd.� Initial API and implementation
 ******************************************************************************/
package tersus.editor.palette;

import org.eclipse.draw2d.FocusEvent;
import org.eclipse.draw2d.FocusListener;
import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.internal.ui.palette.editparts.DrawerEditPart;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.ui.palette.editparts.PaletteAnimator;

/**
 * @author Liat Shiff
 */
public class TersusDrawerEditPart extends DrawerEditPart
{

	public TersusDrawerEditPart(PaletteDrawer drawer) 
	{
		super(drawer);
	}

	public IFigure createFigure() 
	{
		TersusDrawerFigure fig = new TersusDrawerFigure(getViewer().getControl()) 
		{
			IFigure buildTooltip() 
			{
				return createToolTip();
			}
		};
		
		fig.setExpanded(getDrawer().isInitiallyOpen());
		fig.setPinned(getDrawer().isInitiallyPinned());

		fig.getCollapseToggle().addFocusListener(new FocusListener.Stub() 
		{
			public void focusGained(FocusEvent fe) 
			{	
				getViewer().select(TersusDrawerEditPart.this);
			}
		});
		
		fig.getScrollpane().getContents().addLayoutListener(getPaletteAnimator());
		
		return fig;
	}
	
	private PaletteAnimator getPaletteAnimator() 
	{
		return (PaletteAnimator)getViewer()
				.getEditPartRegistry()
				.get(PaletteAnimator.class);
	}
}

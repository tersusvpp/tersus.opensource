/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.palette;

import java.io.IOException;

import org.eclipse.gef.palette.CombinedTemplateCreationEntry;
import org.eclipse.gef.palette.PaletteEntry;
import org.eclipse.gef.requests.CreationFactory;
import org.eclipse.jface.resource.ImageDescriptor;
import org.xml.sax.Attributes;

import tersus.InternalErrorException;
import tersus.editor.EditorMessages;
import tersus.editor.SingletonFactory;
import tersus.editor.TersusEditor;
import tersus.model.SlotType;
import tersus.util.XMLWriter;

/**
 * @author Youval Bronicki
 *  
 */
public class SlotCreationToolEntry extends CombinedTemplateCreationEntry implements SelfConfiguringEntry
{

    private static final String SLOT_TYPE_ATTRIBUTE_NAME = "Type";
    public static final String ENTRY_TAG = "SlotEntry";
    private SlotType slotType;

    /**
     * @param label
     * @param shortDesc
     * @param template
     * @param factory
     * @param iconSmall
     * @param iconLarge
     */
    private SlotCreationToolEntry(SlotType type, String label,
            String shortDesc, Object template, CreationFactory factory,
            ImageDescriptor iconSmall, ImageDescriptor iconLarge)
    {
        super(label, shortDesc, template, factory, iconSmall, iconLarge);
        setSlotType(type);
    }

    public static SlotCreationToolEntry create(SlotType type)
    {
        if (type == SlotType.TRIGGER)
            return new SlotCreationToolEntry(type,
                    EditorMessages.Palette_Trigger_Label,
                    EditorMessages.Palette_Trigger_Description,
                    SlotType.TRIGGER, new SingletonFactory(SlotType.TRIGGER),
                    ImageDescriptor.createFromFile(TersusEditor.class,
                            "icons/Trigger 20x20.gif"), //$NON-NLS-1$
                    ImageDescriptor.createFromFile(TersusEditor.class,
                            "icons/Trigger 20x20.gif")); //$NON-NLS-1$
        else if (type == SlotType.INPUT)
            return new SlotCreationToolEntry(type,
                    EditorMessages.Palette_Input_Slot_Label,
                    EditorMessages.Palette_Input_Slot_Description,
                    SlotType.INPUT, new SingletonFactory(SlotType.INPUT),
                    ImageDescriptor.createFromFile(TersusEditor.class,
                            "icons/Input 20x20.gif"), //$NON-NLS-1$
                    ImageDescriptor.createFromFile(TersusEditor.class,
                            "icons/Input 20x20.gif")); //$NON-NLS-1$
        else if (type == SlotType.EXIT)

            return new SlotCreationToolEntry(type,
                    EditorMessages.Palette_Exit_Label,
                    EditorMessages.Palette_Exit_Description, SlotType.EXIT,
                    new SingletonFactory(SlotType.EXIT), ImageDescriptor
                            .createFromFile(TersusEditor.class,
                                    "icons/Exit 20x20.gif"), //$NON-NLS-1$
                    ImageDescriptor.createFromFile(TersusEditor.class,
                            "icons/Exit 20x20.gif")); //$NON-NLS-1$
        else if (type == SlotType.ERROR)

            return new SlotCreationToolEntry(type,
                    EditorMessages.Palette_Error_Label,
                    EditorMessages.Palette_Error_Description, SlotType.ERROR,
                    new SingletonFactory(SlotType.ERROR), ImageDescriptor
                            .createFromFile(TersusEditor.class,
                                    "icons/Error 20x20.gif"), //$NON-NLS-1$
                    ImageDescriptor.createFromFile(TersusEditor.class,
                            "icons/Error 20x20.gif"));//$NON-NLS-1$
        else if (type == SlotType.IO)
            return new SlotCreationToolEntry(type,
                    EditorMessages.Palette_IO_Trigger_Label,
                    EditorMessages.Palette_IO_Trigger_Description, SlotType.IO,
                    new SingletonFactory(SlotType.IO), ImageDescriptor
                            .createFromFile(TersusEditor.class,
                                    "icons/IOTrigger 20x20.gif"), //$NON-NLS-1$
                    ImageDescriptor.createFromFile(TersusEditor.class,
                            "icons/IOTrigger 20x20.gif")); //$NON-NLS-1$
        else
            throw new InternalErrorException("Unexpected slot type " + type);

    }

    /**
     * @return Returns the type.
     */
    public SlotType getSlotType()
    {
        return slotType;
    }

    /**
     * @param type
     *            The type to set.
     */
    public void setSlotType(SlotType type)
    {
        this.slotType = type;
    }

    /* (non-Javadoc)
     * @see tersus.editor.palette.SelfConfuringEntry#writeAttributes(tersus.util.XMLWriter)
     */
    public void writeAttributes(XMLWriter writer) throws IOException
    {
        writer.writeAttribute(SLOT_TYPE_ATTRIBUTE_NAME, getSlotType());
    }

    /* (non-Javadoc)
     * @see tersus.editor.palette.SelfConfuringEntry#getTag()
     */
    public String getTag()
    {
        return ENTRY_TAG;
    }

    /**
     * @param attributes
     * @return
     */
    public static PaletteEntry create(Attributes attributes)
    {
        String typeStr = attributes.getValue(SLOT_TYPE_ATTRIBUTE_NAME);
        SlotType type = (SlotType)SlotType.get(typeStr, SlotType.values);
        PaletteEntry entry = create(type);
        PaletteConfigurator.copyDefaultPaletteEntryAttributes(attributes, entry);
        return entry;
    }
}
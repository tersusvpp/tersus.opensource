package tersus.editor.palette;

/*******************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others. All rights reserved.
 * 
 * This program is made available under the terms of the GNU General Public
 * License v2, which is part of this distribution and is available at
 * http://www.gnu.org/licenses/gpl.txt.
 * 
 * Contributors: Tersus Software Ltd.� Initial API and implementation
 ******************************************************************************/

import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.gef.internal.ui.palette.editparts.DrawerFigure;
import org.eclipse.swt.widgets.Control;

/**
 * @author Liat Shiff
 */
public class TersusDrawerFigure extends DrawerFigure
{

	public TersusDrawerFigure(Control control) 
	{	
		super(control);
	}

	public Dimension getMinimumSize(int wHint, int hHint) 
	{
		/**
		 * Instead of returns the minimum size, 
		 * the method returns the greater size such that the internal scroll bar is no longer exist
		 * and all icons inside the drawer are presented on screen.
		 */
		if (isExpanded()) 
		{
			List children = getContentPane().getChildren();
			if (!children.isEmpty()) 
			{
				Dimension result = getCollapseToggle().getPreferredSize(wHint, hHint).getCopy();
				result.height += getContentPane().getInsets().getHeight();
				IFigure child = (IFigure)children.get(0);
				result.height += Math.min(80, child.getPreferredSize(wHint, -1).height + 9);
				return result.union(getPreferredSize(wHint, hHint));
			}
		}
		return super.getMinimumSize(wHint, hHint);
	}
}

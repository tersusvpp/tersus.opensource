/*******************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others. All rights reserved.
 * 
 * This program is made available under the terms of the GNU General Public
 * License v2, which is part of this distribution and is available at
 * http://www.gnu.org/licenses/gpl.txt.
 * 
 * Contributors: Tersus Software Ltd.� Initial API and implementation
 ******************************************************************************/
package tersus.editor.palette;

import org.eclipse.gef.internal.InternalImages;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteEntry;
import org.eclipse.gef.palette.PaletteSeparator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

/**
 * @author Liat Shiff
 */
public class PaletteSearchLabelProvider extends LabelProvider
{
	public String getText(Object element) 
    {
    	if (element instanceof PaletteEntry)
    		return ((PaletteEntry)element).getLabel();
		   	
        return super.getText(element);
    }
    
    @Override
    public Image getImage(Object element)
    {
    	if (element instanceof PaletteEntry)
    	{
    		PaletteEntry entry = (PaletteEntry)element;
    		ImageDescriptor descriptor = entry.getSmallIcon();
    		
    		if (descriptor == null) 
    		{
    			if (entry instanceof PaletteContainer) 
    				descriptor = InternalImages.DESC_FOLDER_OPEN;
    			
    			else if (entry instanceof PaletteSeparator)
    				descriptor = InternalImages.DESC_SEPARATOR;
    			else
    				return null;	
    		}
    		return descriptor.createImage();
    	}
			
        return null;
    }
    
}

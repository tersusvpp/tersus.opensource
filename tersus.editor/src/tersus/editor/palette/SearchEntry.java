/*******************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others. All rights reserved.
 * 
 * This program is made available under the terms of the GNU General Public
 * License v2, which is part of this distribution and is available at
 * http://www.gnu.org/licenses/gpl.txt.
 * 
 * Contributors: Tersus Software Ltd.� Initial API and implementation
 ******************************************************************************/
package tersus.editor.palette;

import java.io.IOException;

import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.PaletteEntry;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.xml.sax.Attributes;

import tersus.editor.TersusEditor;
import tersus.util.XMLWriter;
import tersus.workbench.TersusWorkbench;

/**
 * @author Liat Shiff
 */
public class SearchEntry extends ToolEntry implements SelfConfiguringEntry
{
	private static final String MODEL_ID_ATTRIBUTE_NAME = "modelId";
	
	public static final String ENTRY_TAG = "Search";
	
	private PaletteRoot paletteRoot;
	 
	public SearchEntry(String label, String description,
			ImageDescriptor iconSmall, ImageDescriptor iconLarge) 
	{
		super(label, description, iconSmall, iconLarge);
	}

	public String getTag() 
	{
		return ENTRY_TAG;
	}

	public void writeAttributes(XMLWriter writer) throws IOException 
	{
		writer.writeAttribute(MODEL_ID_ATTRIBUTE_NAME, getTag());
	}
	
	public Tool createTool()
    {
        return new SearchTool(getPaletteRoot(), getActiveEditor());
    }
	
	private PaletteRoot getPaletteRoot()
	{
		if (paletteRoot == null)
		{
			TersusEditor editor = getActiveEditor();
			if (editor != null)
				this.paletteRoot = editor._getPaletteRoot();
		}
		
		return paletteRoot;
	}
	
	private TersusEditor getActiveEditor()
	{
		IWorkbenchPage page = TersusWorkbench.getActiveWorkbenchWindow().getActivePage();
		IEditorPart activeEditor = page.getActiveEditor();
	
		if (activeEditor instanceof TersusEditor)
			return (TersusEditor)activeEditor;
		
		return null;
	}
	
	public static PaletteEntry create(Attributes attributes)
	{
		String labelAttributeName = attributes.getValue(PaletteConfigurator.ID_ATTRIBUTE_NAME);
		String descriptionAttributeName = attributes.getValue(PaletteConfigurator.DESCRIPTION_ATTRIBUTE_NAME);
		ImageDescriptor smallIcon = ImageDescriptor.createFromFile(TersusEditor.class,"icons/Search.gif");
        ImageDescriptor largeIcon = ImageDescriptor.createFromFile(TersusEditor.class,"icons/Search.gif");
        
        return new SearchEntry(labelAttributeName, descriptionAttributeName, smallIcon, largeIcon);
	}
}

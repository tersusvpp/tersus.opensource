/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.palette;

import java.io.IOException;

import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.CreationToolEntry;
import org.eclipse.gef.palette.PaletteEntry;
import org.eclipse.gef.requests.CreationFactory;
import org.eclipse.gef.tools.ConnectionCreationTool;
import org.eclipse.jface.resource.ImageDescriptor;
import org.xml.sax.Attributes;

import tersus.editor.EditorMessages;
import tersus.editor.ModelObjectFactory;
import tersus.editor.TersusEditor;
import tersus.model.BuiltinProperties;
import tersus.model.Link;
import tersus.model.LinkOperation;
import tersus.util.XMLWriter;

/**
 * A Palette entry that activates the link creation tool
 * 
 * @author Youval Bronicki
 *  
 */
public class LinkCreationToolEntry extends CreationToolEntry implements
        SelfConfiguringEntry
{
    static final String ENTRY_TAG = "Link";

    /**
     * Constructor for LinkCreationToolEntry.
     * 
     * @param label
     *            the label
     * @param shortDesc
     *            the description
     * @param factory
     *            the CreationFactory
     * @param iconSmall
     *            the small icon
     * @param iconLarge
     *            the large icon
     */
    public LinkCreationToolEntry(String label, String shortDesc,
            CreationFactory factory, ImageDescriptor iconSmall,
            ImageDescriptor iconLarge)
    {
        super(label, shortDesc, factory, iconSmall, iconLarge);
        setUserModificationPermission(PERMISSION_NO_MODIFICATION);
    }

    /**
     * @see org.eclipse.gef.palette.CreationToolEntry#createTool()
     */
    public Tool createTool()
    {
        ConnectionCreationTool tool = new ConnectionCreationTool(factory);
        tool.setUnloadWhenFinished(true);
        return tool;
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.editor.palette.SelfConfiguringEntry#writeAttributes(tersus.util.XMLWriter)
     */
    public void writeAttributes(XMLWriter writer) throws IOException
    {
        Link template = (Link)((ModelObjectFactory)factory).getTemplate();
        LinkOperation operation = template.getOperation();
        if (operation != null)
            writer.writeAttribute("operation", operation.toString());
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.editor.palette.SelfConfiguringEntry#getTag()
     */
    public String getTag()
    {
        return ENTRY_TAG;
    }

    static PaletteEntry create(Attributes attributes)
    {
        Link template = new Link();
        String operationName = attributes.getValue(BuiltinProperties.OPERATION);
        String icon="icons/link";
        if (operationName != null)
        {
            LinkOperation operation = (LinkOperation) LinkOperation.get(
                    operationName, LinkOperation.values);
            template.setOperation(operation);
            icon+="_"+operationName;
        }
        if (LinkOperation.REMOVE.toString().equals(operationName))
            icon = "icons/link_r";
        ModelObjectFactory factory = new ModelObjectFactory(template);
        PaletteEntry entry = new LinkCreationToolEntry(
                EditorMessages.Palette_ConnectionCreationTool_Label,
                EditorMessages.Palette_ConnectionCreationTool_Description,
                factory, ImageDescriptor.createFromFile(TersusEditor.class,
                        icon+".16.gif"), //$NON-NLS-1$
                ImageDescriptor.createFromFile(TersusEditor.class,
                        icon+".24.gif")); //$NON-NLS-1$
        PaletteConfigurator
                .copyDefaultPaletteEntryAttributes(attributes, entry);
        return entry;

    }
}
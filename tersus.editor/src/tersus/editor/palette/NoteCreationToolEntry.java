/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.palette;

import java.io.IOException;

import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.CreationToolEntry;
import org.eclipse.gef.palette.PaletteEntry;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gef.requests.CreationFactory;
import org.eclipse.gef.tools.ConnectionCreationTool;
import org.eclipse.jface.resource.ImageDescriptor;
import org.xml.sax.Attributes;

import tersus.editor.EditorMessages;
import tersus.editor.ModelObjectFactory;
import tersus.editor.TersusEditor;
import tersus.editor.tools.ElementCreationTool;
import tersus.model.Note;
import tersus.util.XMLWriter;

/**
 * A Palette entry that activates the link creation tool
 * 
 * @author Youval Bronicki
 *  
 */
public class NoteCreationToolEntry extends ToolEntry implements
        SelfConfiguringEntry
{
    static final String ENTRY_TAG = "Note";

    /**
     * Constructor for LinkCreationToolEntry.
     * 
     * @param label
     *            the label
     * @param shortDesc
     *            the description
     * @param factory
     *            the CreationFactory
     * @param iconSmall
     *            the small icon
     * @param iconLarge
     *            the large icon
     */
    public NoteCreationToolEntry(String label, String shortDesc,
            ImageDescriptor iconSmall, ImageDescriptor iconLarge)
    {
        super(label, shortDesc, iconSmall, iconLarge);
        setUserModificationPermission(PERMISSION_NO_MODIFICATION);
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.editor.palette.SelfConfiguringEntry#writeAttributes(tersus.util.XMLWriter)
     */
    public void writeAttributes(XMLWriter writer) throws IOException
    {
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.editor.palette.SelfConfiguringEntry#getTag()
     */
    public String getTag()
    {
        return ENTRY_TAG;
    }

    public Tool createTool()
    {
        return new ElementCreationTool(new Note());
    }

    static PaletteEntry create(Attributes attributes)
    {
        String icon="icons/note";
        PaletteEntry entry = new NoteCreationToolEntry(
                EditorMessages.Palette_ConnectionCreationTool_Label,
                EditorMessages.Palette_ConnectionCreationTool_Description,
                ImageDescriptor.createFromFile(TersusEditor.class,
                        icon+".16.gif"), //$NON-NLS-1$
                ImageDescriptor.createFromFile(TersusEditor.class,
                        icon+".24.gif")); //$NON-NLS-1$
        PaletteConfigurator
                .copyDefaultPaletteEntryAttributes(attributes, entry);
        return entry;

    }
}
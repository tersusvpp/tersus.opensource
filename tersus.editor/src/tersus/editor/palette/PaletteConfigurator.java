/************************************************************************************************
 * Copyright (c) 2003-2019 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/

package tersus.editor.palette;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.gef.palette.MarqueeToolEntry;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteEntry;
import org.eclipse.gef.palette.PaletteGroup;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.PaletteToolbar;
import org.eclipse.gef.palette.SelectionToolEntry;
import org.eclipse.gef.tools.MarqueeSelectionTool;
import org.eclipse.jface.resource.ImageDescriptor;
import org.osgi.framework.Bundle;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import tersus.InternalErrorException;
import tersus.ProjectStructure;
// import tersus.Localize;
import tersus.editor.EditorPlugin;
import tersus.editor.Errors;
import tersus.editor.TersusEditor;
import tersus.model.Library;
import tersus.model.Repository;
import tersus.util.FileUtils;
import tersus.util.XMLWriter;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * Loads and saves palette configuration files
 * 
 * @author Youval Bronicki
 * @author David Davidson
 * 
 */
public class PaletteConfigurator
{

	public static final String VISIBLE_ATTRIBUTE_NAME = "visible";

	public static final String ID_ATTRIBUTE_NAME = "id";

	public static final String LABEL_ATTRIBUTE_NAME = "label";

	public static final String DESCRIPTION_ATTRIBUTE_NAME = "description";

	public static final String SHORTCUT_ATTRIBUTE_NAME = "shortcut";

	private static final String PALETTE_DRAWER_TAG = "Drawer";

	private static final String MARQUEE_TOOL_TAG = "Marquee";

	private static final String SELECTION_TOOL_TAG = "Select";

	private static final String PALETTE_GROUP_TAG = "Group";

	private static final String PALETTE_TOOLBAR_TAG = "Toolbar";

	private static final String INITIAL_STATE_ATTRIBUTE_NAME = "state";

	private static final String INITIAL_STATE_OPEN = "Open";

	private static final String INITIAL_STATE_PINNED_OPEN = "Pinned Open";	
	
	@SuppressWarnings("unchecked")
	public static void load(final PaletteRoot paletteRoot, IFile inputFile, IFile propertiesFile,
			final WorkspaceRepository repository, HashMap<String,String> paletteKeys)
	{
		Properties p = new Properties();
		if (propertiesFile != null && propertiesFile.exists())
		{
			InputStream in = null;
			try
			{
				in = propertiesFile.getContents(true);
				p.load(in);
				in.close();
				in = null;
			}
			catch (Exception e)
			{
				TersusWorkbench.error("Failed to read " + propertiesFile.getName(), e);
			}
			finally
			{
				FileUtils.forceClose(in);
			}
		}
		loadInputFile(paletteRoot, inputFile, repository, paletteKeys);
		loadDefaultFiles(paletteRoot, repository, paletteKeys);
		HashMap<String, PaletteEntry> entries = new HashMap<String, PaletteEntry>();
		for (PaletteEntry entry: (List<PaletteEntry>)paletteRoot.getChildren())
		{
			entries.put(entry.getId(), entry);
		}
				
		for (String excludedDrawer : p.getProperty("excluded.drawers", "UI").split(","))
		{
			PaletteEntry entry = entries.get(excludedDrawer.trim());
			if (entry!=null)
				paletteRoot.remove(entry);
		}

	}

	/**
	 * @param paletteRoot
	 * @param repository
	 * @param paletteKeys 
	 */
	private static void loadDefaultFiles(PaletteRoot paletteRoot, WorkspaceRepository repository, HashMap<String, String> paletteKeys)
	{
		PaletteExtension[] extensions = getPaletteExtensions();
		for (int i = 0; i < extensions.length; i++)
			loadURL(paletteRoot, repository, extensions[i].url, paletteKeys);
		for (Library library: repository.getLibraries())
		{
			byte[] paletteFileContent = library.readProjectFile(ProjectStructure.PALETTE_XML);
//	Failed attempt to handle localized palettes (palette.<lang>.xml)
//	Null exception When palette.xml is missing 
//			byte[] paletteFileContent = library.readProjectFile(Localize.getLocalizedFilename(ProjectStructure.PALETTE_XML));
//			if (paletteFileContent == null)
//				paletteFileContent = library.readProjectFile(Localize.getLocalizedFilename(ProjectStructure.PALETTE_XML,ProjectStructure.DEFAULT_LOCALE));
//			if (paletteFileContent == null)
//				 paletteFileContent = library.readProjectFile(ProjectStructure.PALETTE_XML);
			if (paletteFileContent != null)
			{
				try
				{
					loadPaletteConfigurationFile(paletteRoot, repository, new ByteArrayInputStream(paletteFileContent), paletteKeys);
				}
				catch (Exception e)
				{
					TersusWorkbench.log(new Status(Status.ERROR, TersusEditor.ID,
							Errors.FAILED_TO_LOAD_PALETTE_CONFIGURATION_CODE,
							Errors.FAILED_TO_LOAD_PALETTE_CONFIGURATION_MESSAGE, e));
				}

			}	
		}
	}

	/**
	 * @param paletteRoot
	 * @param repository
	 * @param url
	 * @param paletteKeys 
	 */
	private static void loadURL(PaletteRoot paletteRoot, WorkspaceRepository repository, URL url, HashMap<String, String> paletteKeys)
	{
		InputStream input = null;
		try
		{
			input = url.openStream();
			loadPaletteConfigurationFile(paletteRoot, repository, input, paletteKeys);
			input.close();
			input = null;
		}
		catch (Exception e)
		{
			TersusWorkbench.log(new Status(Status.ERROR, TersusEditor.ID,
					Errors.FAILED_TO_LOAD_PALETTE_CONFIGURATION_CODE,
					Errors.FAILED_TO_LOAD_PALETTE_CONFIGURATION_MESSAGE, e));
		}
		finally
		{
			if (input != null)
				try
				{
					input.close();
				}
				catch (IOException e1)
				{
					// Secondary exception ignored
				}
		}
	}

	/**
	 * @param paletteRoot
	 * @param inputFile
	 * @param repository
	 * @param paletteKeys 
	 */
	private static void loadInputFile(final PaletteRoot paletteRoot, IFile inputFile,
			final WorkspaceRepository repository, HashMap<String, String> paletteKeys)
	{
		InputStream input = null;
		try
		{
			if (inputFile.exists())
			{
				input = inputFile.getContents();
				loadPaletteConfigurationFile(paletteRoot, repository, input, paletteKeys);
				input.close();
				input = null;
			}
		}
		catch (Exception e)
		{
			TersusWorkbench.log(new Status(Status.ERROR, TersusEditor.ID,
					Errors.FAILED_TO_LOAD_PALETTE_CONFIGURATION_CODE,
					Errors.FAILED_TO_LOAD_PALETTE_CONFIGURATION_MESSAGE, e));
		}
		finally
		{
			if (input != null)
				try
				{
					input.close();
				}
				catch (IOException e1)
				{
					// Secondary exception ignored
				}
		}
	}

	/**
	 * @param paletteRoot
	 * @param repository
	 * @param input
	 * @param paletteKeys 
	 * @throws SAXException
	 * @throws IOException
	 */
	private static void loadPaletteConfigurationFile(PaletteRoot paletteRoot,
			WorkspaceRepository repository, InputStream input, HashMap<String, String> paletteKeys) throws SAXException, IOException
	{
		XMLReader reader = XMLReaderFactory.createXMLReader();
		DefaultHandler handler = createHandler(paletteRoot, repository, paletteKeys);
		reader.setContentHandler(handler);
		reader.setErrorHandler(handler);
		reader.parse(new InputSource(input));
	}

	/**
	 * @param paletteRoot
	 * @param repository
	 * @param paletteKeys 
	 * @return
	 */
	private static DefaultHandler createHandler(final PaletteRoot paletteRoot,
			final WorkspaceRepository repository, HashMap<String, String> paletteKeys)
	{
		DefaultHandler handler = new DefaultHandler()
		{
			PaletteEntry parent = null;

			public void startElement(String uri, String name, String qName, Attributes atts)
					throws SAXException
			{
				if (parent == null) // Document element
					parent = paletteRoot;
				else
				{
					PaletteEntry existingEntry = findEntry(parent, atts.getValue(ID_ATTRIBUTE_NAME));
					if (existingEntry == null)
					{
						PaletteEntry entry = createPaletteEntry(name, atts, repository);
						if (parent instanceof PaletteContainer)
						{
							((PaletteContainer) parent).add(entry);
							String shortcut = atts.getValue(SHORTCUT_ATTRIBUTE_NAME);
							if (shortcut != null)
							{
								String label = entry.getLabel();
								if (label == null || label.isEmpty())
									label = entry.getId();
								paletteKeys.put(shortcut, parent.getLabel() + "." + label);
							}

						}
						else
							throw new SAXException("Element " + name
									+ " appears inside an element that is not a container");
						parent = entry;
					}
					else
						parent = existingEntry;
				}
			}

			public void endElement(String uri, String name, String qName)
			{
				parent = parent.getParent();
			}

		};
		return handler;
	}

	private static PaletteEntry findEntry(PaletteEntry parent, String id)
	{
		if (parent instanceof PaletteContainer)
		{
			List<PaletteEntry> children = ((PaletteContainer) parent).getChildren();
			for (Iterator<PaletteEntry> i = children.iterator(); i.hasNext();)
			{
				PaletteEntry entry = i.next();
				if (entry.getId().equals(id))
					return entry;
			}
		}
		return null;
	}

	public static PaletteEntry createPaletteEntry(String tagName, Attributes attributes,
			Repository repository) throws SAXException
	{
		PaletteEntry entry = null;
		if (PALETTE_TOOLBAR_TAG.equals(tagName))
			entry = createPaletteToolbar(attributes);
		else if (PALETTE_GROUP_TAG.equals(tagName))
			entry = createPaletteGroup(attributes);
		else if (PALETTE_DRAWER_TAG.equals(tagName))
			entry = createPaletteDrawer(attributes);
		else if (SELECTION_TOOL_TAG.equals(tagName))
			entry = createSelectionToolEntry(attributes);
		else if (LinkCreationToolEntry.ENTRY_TAG.equals(tagName))
			entry = LinkCreationToolEntry.create(attributes);
		else if (MARQUEE_TOOL_TAG.equals(tagName))
			entry = createMarqueeToolEntry(attributes);
		else if (SlotCreationToolEntry.ENTRY_TAG.equals(tagName))
			entry = SlotCreationToolEntry.create(attributes);
		else if (NoteCreationToolEntry.ENTRY_TAG.equals(tagName))
			entry = NoteCreationToolEntry.create(attributes);
		else if (ElementCreationToolEntry.ENTRY_TAG.equals(tagName))
			entry = ElementCreationToolEntry.create(attributes, repository);
		else if (ElementWizardEntry.ENTRY_TAG.equals(tagName))
			entry = ElementWizardEntry.create(attributes, repository);
		else if (SearchEntry.ENTRY_TAG.equals(tagName))
			entry = SearchEntry.create(attributes);
		else
			throw new SAXException("Unrecognized palette entry type '" + tagName + "'");
		return entry;

	}

	/**
	 * @param attributes
	 * @return
	 */
	private static PaletteEntry createMarqueeToolEntry(Attributes attributes)
	{
		PaletteEntry entry = new TersusMarqueeToolEntry();
		copyDefaultPaletteEntryAttributes(attributes, entry);
		entry.setSmallIcon(ImageDescriptor.createFromFile(TersusEditor.class,
				"icons/Marquee 20x20.gif"));
		((TersusMarqueeToolEntry) entry).setToolProperty(MarqueeSelectionTool.PROPERTY_MARQUEE_BEHAVIOR,
			    new Integer(MarqueeSelectionTool.BEHAVIOR_NODES_CONTAINED_AND_RELATED_CONNECTIONS));
		return entry;
	}

	/**
	 * @param attributes
	 * @return
	 */
	private static PaletteEntry createSelectionToolEntry(Attributes attributes)
	{
		PaletteEntry entry = new SelectionToolEntry();
		copyDefaultPaletteEntryAttributes(attributes, entry);
		entry.setSmallIcon(ImageDescriptor.createFromFile(TersusEditor.class,
				"icons/Select 20x20.gif"));
		return entry;
	}

	/**
	 * @param attributes
	 * @return
	 */
	private static PaletteEntry createPaletteDrawer(Attributes attributes)
	{
		PaletteDrawer entry = new PaletteDrawer(null);
		entry.setSmallIcon(null);
		entry.setLargeIcon(null);
		copyDefaultPaletteEntryAttributes(attributes, entry);
		copyPaletteDrawerAttributes(attributes, entry);
		return entry;
	}

	/**
	 * @param attributes
	 * @param entry
	 */
	private static void copyPaletteDrawerAttributes(Attributes attributes, PaletteDrawer entry)
	{
		String initialState = attributes.getValue(INITIAL_STATE_ATTRIBUTE_NAME);
		if (INITIAL_STATE_OPEN.equals(initialState))
			entry.setInitialState(PaletteDrawer.INITIAL_STATE_OPEN);
		else if (INITIAL_STATE_PINNED_OPEN.equals(initialState))
			entry.setInitialState(PaletteDrawer.INITIAL_STATE_PINNED_OPEN);
		else
			entry.setInitialState(PaletteDrawer.INITIAL_STATE_CLOSED);
	}

	/**
	 * @param attributes
	 * @return
	 */
	private static PaletteEntry createPaletteToolbar(Attributes attributes)
	{
		PaletteEntry entry = new PaletteToolbar(null);
		copyDefaultPaletteEntryAttributes(attributes, entry);
		return entry;
	}

	/**
	 * @param attributes
	 * @return
	 */
	private static PaletteEntry createPaletteGroup(Attributes attributes)
	{
		PaletteEntry entry = new PaletteGroup(null);
		copyDefaultPaletteEntryAttributes(attributes, entry);
		return entry;
	}

	/**
	 * @param attributes
	 * @param entry
	 */
	static void copyDefaultPaletteEntryAttributes(Attributes attributes, PaletteEntry entry)
	{
		String id = attributes.getValue(ID_ATTRIBUTE_NAME);
		String label = attributes.getValue(LABEL_ATTRIBUTE_NAME);
		if (label == null)
			label = id;
		String description = attributes.getValue(DESCRIPTION_ATTRIBUTE_NAME);
		String visible = attributes.getValue(VISIBLE_ATTRIBUTE_NAME);
		entry.setId(id);
		entry.setLabel(label);
		if (description == null)
			description = "";
		entry.setDescription(description);
		String shortcut = attributes.getValue(SHORTCUT_ATTRIBUTE_NAME);
		if (shortcut != null)
			description += " [" + shortcut + "]";
		if (visible != null)
			entry.setVisible(Boolean.valueOf(visible).booleanValue());
	}

	/**
	 * Saves palette configuration in file "palette.xml"
	 * 
	 * @param paletteRoot
	 */
	public static void save(PaletteRoot paletteRoot, IFile outputFile)
	{
		try
		{
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			try
			{
				XMLWriter writer = new XMLWriter(out);
				write(paletteRoot, writer);
			}
			catch (IOException e)
			{
				throw new InternalErrorException("Failed to create palette configuration");
			}
			if (outputFile.exists())
				outputFile.setContents(new ByteArrayInputStream(out.toByteArray()), true, true,
						null);
			else
				outputFile.create(new ByteArrayInputStream(out.toByteArray()), true, null);

		}
		catch (Exception e)
		{
			TersusWorkbench.log(new Status(Status.ERROR, TersusEditor.ID,
					Errors.FAILED_TO_SAVE_PALETTE_CONFIGURATION_CODE,
					Errors.FAILED_TO_SAVE_PALETTE_CONFIGURATION_MESSAGE, e));
		}

	}

	public static void write(PaletteRoot paletteRoot, XMLWriter writer) throws IOException
	{
		writer.openTag("Palette");
		writer.closeTag();
		writeChildren(paletteRoot, writer);
		writer.endElement();
		writer.close();
	}

	/**
	 * @param parent
	 * @param writer
	 * @throws IOException
	 */
	private static void writeChildren(PaletteContainer parent, XMLWriter writer) throws IOException
	{
		for (Object obj : parent.getChildren())
		{
			if (obj instanceof PaletteEntry)
			{
				PaletteEntry entry = (PaletteEntry) obj;
				writeEntry(entry, writer);
			}
		}
	}

	/**
	 * @param entry
	 * @param writer
	 * @throws IOException
	 */
	private static void writeEntry(PaletteEntry entry, XMLWriter writer) throws IOException
	{
		String tag = null;
		if (entry instanceof SelfConfiguringEntry)
			tag = ((SelfConfiguringEntry) entry).getTag();
		else if (entry instanceof PaletteGroup)
			tag = PALETTE_GROUP_TAG;
		else if (entry instanceof SelectionToolEntry)
			tag = SELECTION_TOOL_TAG;
		else if (entry instanceof MarqueeToolEntry)
			tag = MARQUEE_TOOL_TAG;
		else if (entry instanceof PaletteDrawer)
			tag = PALETTE_DRAWER_TAG;
		else
			throw new InternalErrorException("Unexpected Palette Entry Type "
					+ entry.getClass().getName());
		if (tag != null)
		{
			writer.openTag(tag);
			writeAttributes(writer, entry);
			if (entry instanceof PaletteContainer)
			{
				writer.closeTag();
				writeChildren((PaletteContainer) entry, writer);
			}
			writer.endElement();
		}
	}

	/**
	 * @param writer
	 * @param entry
	 */
	private static void writeAttributes(XMLWriter writer, PaletteEntry entry) throws IOException
	{
		String id = entry.getId();
		String label = entry.getLabel();
		if (id == null || id.length() == 0)
			id = label;
		writer.writeAttribute(ID_ATTRIBUTE_NAME, id);
		if (!id.equals(label))
			writer.writeAttribute(LABEL_ATTRIBUTE_NAME, label);
		String description = entry.getDescription();
		if (description != null && description.length() > 0 && !description.equals(label))
			writer.writeAttribute(DESCRIPTION_ATTRIBUTE_NAME, description);
		if (!entry.isVisible())
			writer.writeAttribute(VISIBLE_ATTRIBUTE_NAME, String.valueOf(entry.isVisible()));
		if (entry instanceof SelfConfiguringEntry)
			((SelfConfiguringEntry) entry).writeAttributes(writer);
	}

	/**
	 * Sort the extensions using the dependency attribute
	 * 
	 * @param extensions
	 * @return
	 */
	private static PaletteExtension[] getPaletteExtensions()
	{
		ArrayList<PaletteExtension> output = new ArrayList<PaletteExtension>();
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint point = registry.getExtensionPoint("tersus.palette");
		if (point != null)
		{
			IExtension[] extensions = point.getExtensions();
			ArrayList<PaletteExtension> toSort = new ArrayList<PaletteExtension>(extensions.length);
			for (int i = 0; i < extensions.length; i++)
				toSort.add(new PaletteExtension(extensions[i]));
			while (toSort.size() > 0)
			{
				boolean found = false;
				for (int j = 0; j < toSort.size() && !found; j++)
				{
					PaletteExtension current = toSort.get(j);
					if (null == findDependantExtension(current, toSort))
					{
						output.add(current);
						toSort.remove(current);
						found = true;
					}
				}
				if (!found) // Cycle
				{
					TersusWorkbench.log(new Status(Status.WARNING, EditorPlugin.getPluginId(), 0,
							"A cycle was found among palette configuration declarations", null));
					output.addAll(toSort);
					toSort.clear();
				}
			}
		}
		return (output.toArray(new PaletteExtension[output.size()]));

	}

	/**
	 * @param current
	 * @param toSort
	 * @return
	 */
	private static PaletteExtension findDependantExtension(PaletteExtension current, ArrayList<PaletteExtension> list)
	{
		if (current.depends == null)
			return null;
		for (int i = 0; i < list.size(); i++)
		{
			PaletteExtension other = list.get(i);
			if (current.depends.equals(other.id))
				return other;
		}
		return null;

	}

}

class PaletteExtension
{
	String depends;

	URL url;

	String id;

	PaletteExtension(IExtension extension)
	{
		id = extension.getUniqueIdentifier();
		IConfigurationElement[] elements = extension.getConfigurationElements();
		for (int j = 0; j < elements.length; j++)
		{
			IConfigurationElement element = elements[j];
			if ("depends".equals(element.getName()))
				this.depends = element.getValueAsIs();
			if ("file".equals(element.getName()))
			{
				Bundle bundle = Platform.getBundle(extension.getNamespace());
				URL installURL = bundle.getEntry(element.getValue());
				URL resolvedURL;
				try
				{
					resolvedURL = Platform.resolve(installURL);
				}
				catch (IOException e)
				{
					throw new RuntimeException(e);
				}
				url = resolvedURL;
			}
		}

	}

}
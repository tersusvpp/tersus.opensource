/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor;

import org.eclipse.gef.ui.views.palette.PaletteView;
import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;
import org.eclipse.ui.IPlaceholderFolderLayout;

import tersus.editor.explorer.ExplorerPart;
import tersus.editor.validation.ui.ValidationView;
import tersus.editor.wizards.NewActionWizard;
import tersus.editor.wizards.NewPackageWizard;
import tersus.editor.wizards.NewProjectWizard;
import tersus.editor.wizards.NewSystemWizard;
import tersus.editor.wizards.NewViewWizard;

/**
 * 
 * @author Joel Barel
 * @author Youval Bronicki
 *
 */
public class TersusModelingPerspective implements IPerspectiveFactory {
	
	private static final String APPSERVER_VIEW = "tersus.appserver.ui.views.ApplicationServerView";
    public static final String ID = "tersus.editor.TersusModelingPerspective";
    public static final String TRACE_VIEW = "tersus.debug.TraceView";
    public static final String DEBUGGING_PERSPECTIVE = "tersus.debug.TersusDebuggingPerspective";
    public static final String RESOURCE_PERSPECTIVE = "org.eclipse.ui.resourcePerspective";
    public static final String CVS_PERSPECTIVE = "org.eclipse.team.cvs.ui.cvsPerspective";
    /**
	 * Constructs a new Default layout engine.
	 */
	public TersusModelingPerspective() {
		super();
	}

	public void createInitialLayout(IPageLayout layout) {

 		createLeftFolder(layout);
 		createBottomFolder(layout);
 		createRightFolder(layout);
		addNewWizards(layout);
		addShowViewShortcuts(layout);
//		addHiddenMenuItemIds(layout); 
		layout.addPerspectiveShortcut(DEBUGGING_PERSPECTIVE);
		layout.addPerspectiveShortcut(ID);
		layout.addPerspectiveShortcut(RESOURCE_PERSPECTIVE);
		layout.addPerspectiveShortcut(CVS_PERSPECTIVE);
	}

	// It works only on Eclipse 3.5  
//	public static void addHiddenMenuItemIds(IPageLayout layout)
//    {
//		if (layout instanceof PageLayout)
//		{
//			((PageLayout)layout).addHiddenMenuItemId("org.eclipse.ui.project.buildProject");
//			((PageLayout)layout).addHiddenMenuItemId("org.eclipse.ui.internal.ide.actions.BuildSetMenu");
//			((PageLayout)layout).addHiddenMenuItemId("org.eclipse.ui.project.buildAutomatically");
//			((PageLayout)layout).addHiddenMenuItemId("org.eclipse.ui.project.closeProject");
//			((PageLayout)layout).addHiddenMenuItemId("org.eclipse.ui.project.openProject");
//			((PageLayout)layout).addHiddenMenuItemId("org.eclipse.ui.project.cleanAction");
//			((PageLayout)layout).addHiddenMenuItemId("org.eclipse.ui.project.properties");
//			((PageLayout)layout).addHiddenMenuItemId("org.eclipse.ui.project.buildAll");
//		}
//    }
	
    public static void addShowViewShortcuts(IPageLayout layout)
    {
        layout.addShowViewShortcut(ExplorerPart.VIEW_ID);
		layout.addShowViewShortcut(IPageLayout.ID_OUTLINE);
		layout.addShowViewShortcut(APPSERVER_VIEW);
		layout.addShowViewShortcut(TRACE_VIEW);
		layout.addShowViewShortcut(ValidationView.ID);
		layout.addShowViewShortcut(IPageLayout.ID_RES_NAV);
		layout.addShowViewShortcut(IPageLayout.ID_PROP_SHEET);
		layout.addShowViewShortcut(PaletteView.ID);
    }

    
    protected void createBottomFolder(IPageLayout layout)
    {
        String editorArea1 = layout.getEditorArea();
        IFolderLayout bottomFolder = layout.createFolder("bottom", IPageLayout.BOTTOM, (float)0.7, editorArea1);
		bottomFolder.addView(ValidationView.ID);
		bottomFolder.addPlaceholder(TRACE_VIEW);
		bottomFolder.addView(IPageLayout.ID_PROP_SHEET);
    }

    private void addNewWizards(IPageLayout layout)
    {
        layout.addNewWizardShortcut(NewProjectWizard.ID);
		layout.addNewWizardShortcut(NewPackageWizard.ID);
		layout.addNewWizardShortcut(NewSystemWizard.ID);
		layout.addNewWizardShortcut(NewViewWizard.ID);
		layout.addNewWizardShortcut(NewActionWizard.ID);
    }

    private void createRightFolder(IPageLayout layout)
    {
        String editorArea = layout.getEditorArea();
        IPlaceholderFolderLayout rightFolder= layout.createPlaceholderFolder("right", IPageLayout.RIGHT, (float)0.8, editorArea); //$NON-NLS-1$
		rightFolder.addPlaceholder(PaletteView.ID);
    }
    
    private void createLeftFolder(IPageLayout layout)
    {
        String editorArea = layout.getEditorArea();
		IFolderLayout leftFolder= layout.createFolder("left", IPageLayout.LEFT, (float)0.2, editorArea); //$NON-NLS-1$
		leftFolder.addView(ExplorerPart.VIEW_ID);
		leftFolder.addPlaceholder(APPSERVER_VIEW);
		leftFolder.addView(IPageLayout.ID_OUTLINE);
		leftFolder.addPlaceholder(IPageLayout.ID_RES_NAV);
    }
}

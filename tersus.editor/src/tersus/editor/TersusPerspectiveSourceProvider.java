/************************************************************************************************
 * Copyright (c) 2003-2019 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.ui.AbstractSourceProvider;
import org.eclipse.ui.ISources;

/**
 * 
 * @author Alberto Romo Valverde
 *
 */
public class TersusPerspectiveSourceProvider extends AbstractSourceProvider {
 
    public final static String tersusPerspectiveKey = "tersus.editor.TersusModelingPerspective.showActions";
    private final static String tersusPerspective = "showActions";
    private final static String otherPerspective = "hideActions";
 
    /**
     * true if the current perspective is the Resource Manager Perspective
     */
    private boolean isTersusPerspective = true;
 
    @Override
    public void dispose() {
        // TODO Auto-generated method stub
 
    }
 
    @Override
    public Map getCurrentState() {
        Map<String, String> currentState = new HashMap<String, String>(1);
        String currentState1 = isTersusPerspective ? tersusPerspective
                : otherPerspective;
        currentState.put(tersusPerspectiveKey, currentState1);
        return currentState;
    }
 
    @Override
    public String[] getProvidedSourceNames() {
        return new String[] { tersusPerspectiveKey };
    }
 
    public void perspectiveChanged(boolean _isTersusPerspective) {
        if (this.isTersusPerspective == _isTersusPerspective)
            return; // no change
 
        this.isTersusPerspective = _isTersusPerspective;
        String currentState = isTersusPerspective ? tersusPerspective
                : otherPerspective;
        fireSourceChanged(ISources.WORKBENCH, tersusPerspectiveKey, currentState);
    }
 
}
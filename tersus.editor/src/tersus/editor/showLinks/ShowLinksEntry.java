/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.showLinks;

import java.util.Stack;

import tersus.model.FlowModel;
import tersus.model.Link;
import tersus.model.Path;
import tersus.model.Role;

/**
 * 
 * @author Liat Shiff
 */
public class ShowLinksEntry
{
	private FlowModel parentModel;

	private Link link;

	private Path path;

	public ShowLinksEntry(FlowModel parentModel, Link link)
	{
		this.parentModel = parentModel;
		this.link = link;
	}

	public Path getPath()
	{
		if (path == null)
		{
			Stack<Role> pathSegments = new Stack<Role>();
			pathSegments.push(link.getRole());

			Path path = new Path();
			while (!pathSegments.isEmpty())
			{
				path.addSegment((Role) pathSegments.pop());
			}
		}
		return path;
	}

	public FlowModel parentModel()
	{
		return parentModel;
	}
}

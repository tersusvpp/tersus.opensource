/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.showLinks;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.search.ui.ISearchQuery;
import org.eclipse.search.ui.ISearchResult;
import org.eclipse.search.ui.NewSearchUI;
import org.eclipse.search.ui.text.Match;

import tersus.editor.search.TersusSearchResult;
import tersus.editor.search.TersusSearchResultDescriptor;
import tersus.model.Link;
import tersus.model.ModelElement;
import tersus.model.Slot;
import tersus.model.indexer.RepositoryIndex;
import tersus.workbench.WorkspaceRepository;

/**
 * 
 * @author Liat Shiff
 */
public class ShowLinksQuery implements ISearchQuery 
{
	public final static String SHOW_INCOMING_LINKS = "Incoming Links";
	public final static String SHOW_OUTGOING_LINKS = "Outgoing Links";
	
	private TersusSearchResult tSearchResult;
	
	private String searchType;
	
	private ModelElement element;
	
	private int totalMatches;
	
	public ShowLinksQuery(String searchType, ModelElement element) 
	{
		this.searchType = searchType;
		this.element = element;
		
		totalMatches = 0;
	}
	
	public boolean canRerun() 
	{
		return false;
	}


	public boolean canRunInBackground() 
	{
		return true;
	}


	public String getLabel() 
	{
		return "Show links search.";
	}

	public ISearchResult getSearchResult() 
	{
        if (tSearchResult == null) 
        {
        	tSearchResult = new TersusSearchResult(this);
        }
        return tSearchResult;
	}


	public IStatus run(IProgressMonitor monitor)
			throws OperationCanceledException 
	{
		monitor.beginTask("Calculating Links", 100);
        SubProgressMonitor subMonitor  = new SubProgressMonitor(monitor, 50);

        WorkspaceRepository repository = (WorkspaceRepository) element.getRepository();
        RepositoryIndex index = repository.getUpdatedRepositoryIndex(subMonitor);
        
       if (!(element instanceof Link && element instanceof Slot))
        {
        	if (SHOW_INCOMING_LINKS.equals(searchType))
        	{
        		for (Link link: index.getIncomingLinks(element, false))
    			{
        			createDescriptorAndAddToResult(link, repository);
    			}
        	}
        	
        	if (SHOW_OUTGOING_LINKS.equals(searchType))
        	{
        		for (Link link: index.getOutgoingLinks(element, false))
    			{
        			createDescriptorAndAddToResult(link, repository);	
    			}
        	}
        }
        monitor.done();
        MultiStatus status = new MultiStatus(NewSearchUI.PLUGIN_ID, IStatus.OK, "Alright", null);
        return status;
	}
	
	private void createDescriptorAndAddToResult(Link link, WorkspaceRepository repository)
	{
		TersusSearchResultDescriptor descriptor = new TersusSearchResultDescriptor(
				link.getParentModel().getId(), repository.getProject(), repository, link.getRole());
		tSearchResult.addMatch(new Match(descriptor, 0, 0));
		totalMatches++;
	}
	
	public String toString() 
	{
		return totalMatches > 1 ? totalMatches + " " + searchType: 
			totalMatches == 1? totalMatches + " " + searchType.substring(0, searchType.length() - 2) : "No links";
	}

}

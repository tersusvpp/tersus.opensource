/*******************************************************************************
 * Copyright (c) 2003-2020 Tersus Software Ltd. and others. All rights reserved.
 * 
 * This program is made available under the terms of the GNU General Public
 * License v2, which is part of this distribution and is available at
 * http://www.gnu.org/licenses/gpl.txt.
 * 
 * Contributors: Tersus Software Ltd. - Initial API and implementation
 ******************************************************************************/

package tersus.editor;

import java.net.URL;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.draw2d.Viewport;
import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.KeyHandler;
import org.eclipse.gef.KeyStroke;
import org.eclipse.gef.MouseWheelHandler;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteEntry;
import org.eclipse.gef.palette.PaletteListener;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.gef.ui.actions.PrintAction;
import org.eclipse.gef.ui.actions.RedoAction;
import org.eclipse.gef.ui.actions.SaveAction;
import org.eclipse.gef.ui.actions.SelectAllAction;
import org.eclipse.gef.ui.actions.UndoAction;
import org.eclipse.gef.ui.palette.PaletteCustomizer;
import org.eclipse.gef.ui.palette.PaletteEditPartFactory;
import org.eclipse.gef.ui.palette.PaletteViewer;
import org.eclipse.gef.ui.palette.PaletteViewerPreferences;
import org.eclipse.gef.ui.palette.PaletteViewerProvider;
import org.eclipse.gef.ui.palette.FlyoutPaletteComposite.FlyoutPreferences;
import org.eclipse.gef.ui.parts.GraphicalEditorWithFlyoutPalette;
import org.eclipse.gef.ui.parts.GraphicalViewerKeyHandler;
import org.eclipse.gef.ui.parts.SelectionSynchronizer;
import org.eclipse.help.IContextProvider;
import org.eclipse.help.ui.internal.views.HelpView;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.commands.ActionHandler;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.browser.IWorkbenchBrowserSupport;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;
import org.eclipse.ui.views.properties.IPropertySheetPage;

import tersus.ProjectStructure;
// import tersus.Localize;
import tersus.editor.actions.AddAncestorReferenceAction;
import tersus.editor.actions.AddElementAction;
import tersus.editor.actions.AddPropertyAction;
import tersus.editor.actions.AnalyzeDependenciesAction;
import tersus.editor.actions.ApplyTableWizardAction;
import tersus.editor.actions.ChangePluginAction;
import tersus.editor.actions.CopyElementNameAction;
import tersus.editor.actions.RemoveInvalidFlowsAction;
import tersus.editor.actions.CopyAction;
import tersus.editor.actions.DecapsulateAction;
import tersus.editor.actions.EditNoteAction;
import tersus.editor.actions.EditRoleAction;
import tersus.editor.actions.EncapsulateAction;
import tersus.editor.actions.HelpAction;
import tersus.editor.actions.HorizontalAlignAction;
import tersus.editor.actions.ImportStructureFormDBAction;
import tersus.editor.actions.MoveElementAction;
import tersus.editor.actions.MoveModelsEditorAction;
import tersus.editor.actions.OpenAction;
import tersus.editor.actions.OpenInANewTabAction;
import tersus.editor.actions.PasteCloneAction;
import tersus.editor.actions.PasteElementsAction;
import tersus.editor.actions.RemoveElementAction;
import tersus.editor.actions.RenameAction;
import tersus.editor.actions.ReplaceWithAction;
import tersus.editor.actions.SetActiveToolAction;
import tersus.editor.actions.SetFlowDataElementType;
import tersus.editor.actions.SizeAlignAction;
import tersus.editor.actions.ShowInRepositoryAction;
import tersus.editor.actions.ShowLinksAction;
import tersus.editor.actions.ShowUsagesAction;
import tersus.editor.actions.TersusActionConstants;
import tersus.editor.actions.ToggleAlwaysCreateAction;
import tersus.editor.actions.ToggleElementTypeAction;
import tersus.editor.actions.ToggleMandatoryAction;
import tersus.editor.actions.ToggleRepetitiveAction;
import tersus.editor.actions.VerticalAlignAction;
import tersus.editor.actions.ZoomInAction;
import tersus.editor.actions.ZoomOutAction;
import tersus.editor.editparts.TersusEditPart;
import tersus.editor.editparts.TersusToolEntryEditPart;
import tersus.editor.editparts.TopFlowEditPart;
import tersus.editor.outline.TersusOutlinePage;
import tersus.editor.palette.PaletteConfigurator;
import tersus.editor.palette.TersusDrawerEditPart;
import tersus.editor.preferences.Preferences;
import tersus.editor.properties.ModelObjectWrapper;
import tersus.editor.properties.TersusPropertySheetPage;
import tersus.editor.useractions.SetActiveToolUserAction;
import tersus.model.BuiltinProperties;
import tersus.model.FlowModel;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Path;
import tersus.util.PropertyChangeListener;
import tersus.util.Trace;
import tersus.workbench.ImageLocator;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

public class TersusEditor extends GraphicalEditorWithFlyoutPalette
{
	private static PaletteViewerPreferences PALETTE_PREFERENCES = new TersusPaletteViewerPreferences();

	private PaletteRoot paletteRoot;

	public static String ID = "tersus.editor";

	public static final String COMPOSITE_DATA_TYPE_TEMPLATE_PACKAGE = "Common/Templates/Composite Data Types";

	private boolean savePreviouslyNeeded;

	private TracingMouseListener mouseListener;

	private IPropertyChangeListener preferenceListener;

	private KeyHandler sharedKeyHandler;

	private HashMap<String, String> paletteKeys = new HashMap<String, String>();

	protected ModelObjectWrapper modelWrapper;

	private TersusZoomManager zoomManager;

	HashMap<String, PaletteEntry> toolRegistry = new HashMap<String, PaletteEntry>();

	public TersusOutlinePage outlinePage;

	private IContextProvider contextprovider;
	
	private PartListener partListener;

	private PropertyChangeListener modelListener = new PropertyChangeListener()
	{

		public void propertyChange(Object sourceObject, String property, Object oldValue,
				Object newValue)
		{
			if (BuiltinProperties.NAME.equals(property))
				setPartName(getRootModel().getName());
		}

	};;

	private ConcreteModelIdentifier input;

	public TersusEditor()
	{
		if (Debug.OTHER)
			Trace.push("TersusEditor() constructor");
		try
		{
		}
		finally
		{
			if (Debug.OTHER)
				Trace.pop();

		}
	}

	public WorkspaceRepository getRepository()
	{
		return getTersusEditDomain().getRepository();
	}

	protected void setInput(IEditorInput input)
	{

		super.setInput(input);
		if (input instanceof ConcreteModelIdentifier)
		{
			this.input = (ConcreteModelIdentifier) input;
			IProject project = ((ConcreteModelIdentifier) input).getProject();
			TersusEditDomain editDomain = new TersusEditDomain(project, this);
			try
			{
				setEditDomain(editDomain);
				reset();
			}
			catch (RuntimeException e)
			{
				TersusWorkbench.log(new Status(Status.ERROR, ID, Errors.FAILED_TO_INIT_EDITOR_CODE,
						Errors.FAILED_TO_INIT_EDITOR_MESSAGE, e));
				throw e;
			}
		}
	}

	private void reset()
	{
		if (getRootModel() != null)
			getRootModel().removePropertyChangeListener(modelListener);

		ModelId id = this.input.getModelId();
		FlowModel model = (FlowModel) getRepository().getModel(id, true);
		if (model == null)
		{
			getSite().getPage().closeEditor(this, false);
			return;
		}
		setModel(new ModelObjectWrapper(model));
		setPartName(model.getName());
		getRootModel().addPropertyChangeListener(modelListener);
		configurePalette();
	}

	@SuppressWarnings("unchecked")
	private void configurePalette()
	{
		PaletteRoot root = getPaletteRoot();

		// Clear the palette root first
		List paletteRootEntries = new ArrayList();
		paletteRootEntries.addAll(root.getChildren());

		for (int i = 0; i < paletteRootEntries.size(); i++)
			root.remove((PaletteEntry) paletteRootEntries.get(i));
		// Then load it
		PaletteConfigurator.load(root, getPaletteConfigurationFile(),getPalettePropertiesFile(), getRepository(), paletteKeys);
		updateToolRegistry(root);

	}

	/**
	 * @see org.eclipse.gef.ui.parts.GraphicalEditor#initializeGraphicalViewer()
	 */
	protected void initializeGraphicalViewer()
	{
		if (Debug.OTHER)
			Trace.push("TersusEditor.initializeGraphicalViewer()");
		try
		{
			getGraphicalViewer().setContents(modelWrapper);
			getGraphicalViewer().addDropTargetListener(
					new ModelObjectDropTargetListener(this, getGraphicalViewer()));
			getGraphicalViewer().addDropTargetListener(new FileDropTargetListener(this,getGraphicalViewer()));
		}
		finally
		{
			if (Debug.OTHER)
				Trace.pop();
		}
	}

	public boolean isSaveAsAllowed()
	{
		return false;
	}

	public void doSave(IProgressMonitor iMonitor)
	{
		if (Debug.PERSISTENCE)
			Trace.push("TersusEditor.doSave() root=" + modelWrapper);
		try
		{
			// getActionRegistry().getAction(
			// TersusActionConstants.SET_SLOT_DATA_TYPES).run();
			getTersusEditDomain().save();
		}
		finally
		{

			if (Debug.PERSISTENCE)
				Trace.pop();
		}
	}

	public boolean isDirty()
	{
		if (Debug.OTHER)
			Trace.push("TersusEditor.isDirty() model=" + modelWrapper);
		try
		{
			boolean result = getCommandStack().isDirty();
			if (Debug.OTHER)
				Trace.add("result=" + result);
			return result;
		}
		finally
		{
			if (Debug.OTHER)
				Trace.pop();

		}
	}

	protected void createGraphicalViewer(Composite parent)
	{
		TersusGraphicalViewer viewer = new TersusGraphicalViewer();
		viewer.createControl(parent);
		setGraphicalViewer(viewer);
		configureGraphicalViewer();
		hookGraphicalViewer();
		initializeGraphicalViewer();
		hookPreferenceListener();
		hookMouseListener();
		viewer.setEditor(this);
	}

	private void hookMouseListener()
	{
		if (Debug.USER_ACTIONS)
		{

			mouseListener = new TracingMouseListener();
			getViewer().getControl().addMouseListener(mouseListener);
			getViewer().getControl().addMouseMoveListener(mouseListener);
			getViewer().getControl().addMouseTrackListener(mouseListener);
		}
	}

	private void hookPreferenceListener()
	{
		preferenceListener = new IPropertyChangeListener()
		{

			public void propertyChange(PropertyChangeEvent event)
			{
				getTopFlowEditPart().refreshVisualsDeep();
				getViewport().invalidateTree();
			}
		};
		EditorPlugin.getDefault().getPreferenceStore()
				.addPropertyChangeListener(preferenceListener);
	}

	public void disposePreferenceListener()
	{
		if (preferenceListener != null)
			EditorPlugin.getDefault().getPreferenceStore().removePropertyChangeListener(
					preferenceListener);
	}

	protected void configureGraphicalViewer()
	{
		if (Debug.OTHER)
			Trace.push("TersusEditor.configureGraphicalViewer() model=" + modelWrapper);

		try
		{
			super.configureGraphicalViewer();
			TersusGraphicalViewer viewer = (TersusGraphicalViewer) getGraphicalViewer();

			TersusRootEditPart root = new TersusRootEditPart();
			viewer.setRootEditPart(root);

			zoomManager = root.getZoomManager();
			IAction zoomIn = new ZoomInAction(zoomManager);
			IAction zoomOut = new ZoomOutAction(zoomManager);
			getActionRegistry().registerAction(zoomIn);
			getActionRegistry().registerAction(zoomOut);

			getGraphicalViewer().setProperty(MouseWheelHandler.KeyGenerator.getKey(SWT.MOD1),
					TersusMouseWheelZoomHandle.SINGLETON);

			IHandlerService service = (IHandlerService) getEditorSite().getService(
					IHandlerService.class);
			service.activateHandler(zoomIn.getActionDefinitionId(), new ActionHandler(zoomIn));
			service.activateHandler(zoomOut.getActionDefinitionId(), new ActionHandler(zoomOut));
			viewer.setEditPartFactory(new GraphicalPartFactory());
			viewer.setKeyHandler(new GraphicalViewerKeyHandler(viewer)
					.setParent(getCommonKeyHandler()));
			ContextMenuProvider provider = new TersusContextMenuProvider(viewer,
					getActionRegistry());
			viewer.setContextMenu(provider);

			// If you don't put this line, then moving figures by drag & drop
			// above the left or top limit of the editor window will lead to
			// an infinite loop!
			((FigureCanvas) viewer.getControl()).setScrollBarVisibility(FigureCanvas.ALWAYS);
			getSite().getWorkbenchWindow().getWorkbench().getHelpSystem().setHelp(
					viewer.getControl(), "tersus.help.main_help");
			
			partListener = new PartListener();
			getSite().getPage().addPartListener(partListener);
		}
		finally
		{
			if (Debug.OTHER)
				Trace.pop();
		}
	}

	public TersusZoomManager getZoomManager()
	{
		return zoomManager;
	}

	/**
	 * Returns the KeyHandler with common bindings for both the Outline and Graphical Views. For
	 * example, delete is a common action.
	 * 
	 * Support for Non-Ascii keys, such as F2, require the char equivalent.
	 * I used a breakpoint on org.eclipse.gef.ui.parts.GraphicalViewerKeyHandler.keyPressed,
	 * and the expression "(int) event.character" to figure our what it is
	 * 
	 */
	protected KeyHandler getCommonKeyHandler()
	{
		if (sharedKeyHandler == null)
		{
			sharedKeyHandler = new KeyHandler();
			sharedKeyHandler.put(KeyStroke.getPressed(SWT.DEL, 127, 0), getActionRegistry()
					.getAction(TersusActionConstants.REMOVE_ELEMENT_ACTION_ID));
			sharedKeyHandler.put(KeyStroke.getPressed(SWT.BS, 8, 0), getActionRegistry().getAction(
					TersusActionConstants.REMOVE_ELEMENT_ACTION_ID));
			sharedKeyHandler.put(KeyStroke.getPressed((char)11, SWT.F2, 0), getActionRegistry().getAction(
					TersusActionConstants.EDIT_ROLE));
			sharedKeyHandler.put(KeyStroke.getPressed('r', 'r', 0), getActionRegistry().getAction(
					TersusActionConstants.TOGGLE_REPETITIVE));
			sharedKeyHandler.put(KeyStroke.getPressed('m', 'm', 0), getActionRegistry().getAction(
					TersusActionConstants.TOGGLE_MANDATORY));

			for (Map.Entry<String, String> paletteKey : paletteKeys.entrySet()) {
				addPaletteKey(paletteKey);
			}
		}
		return sharedKeyHandler;
	}

	/**
	 * @param paletteKey
	 */
	private void addPaletteKey(Map.Entry<String, String> paletteKey)
	{
		String keys = paletteKey.getKey();
		for (int i = 0; i< keys.length(); i++) {
			SetActiveToolAction toolAction = new SetActiveToolAction(this, paletteKey.getValue());
			char key = keys.charAt(i);
			sharedKeyHandler.put(KeyStroke.getPressed(key, key, 0), toolAction);
		}
	}

	public FlowModel getRootModel()
	{
		if (modelWrapper == null)
			return null;
		return (FlowModel) modelWrapper.getModel();
	}
	
	public void setModel(ModelObjectWrapper model)
	{
		this.modelWrapper = model;
	}

	public ModelObjectWrapper getModelWrapper()
	{
		return modelWrapper;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.ISaveablePart#isSaveOnCloseNeeded()
	 */
	public boolean isSaveOnCloseNeeded()
	{
		return isDirty()
				&& getTersusEditDomain().getRepositoryManager().getOpenEditors().size() == 1;
	}

	public TersusEditDomain getTersusEditDomain()
	{
		return (TersusEditDomain) getEditDomain();
	}

	public Object getAdapter(Class type)
	{
		if (type == IContentOutlinePage.class)
		{
			if (outlinePage == null)
				outlinePage = new TersusOutlinePage(this);
			return outlinePage;
		}
		if (type == IPropertySheetPage.class)
		{
			return new TersusPropertySheetPage(this);
		}
		if (type == IProject.class)
		{
			return getRepository().getProject();
		}
		// Support eclipse dynamic help - not in use
		// if (type == IContextProvider.class)
		// {
		// if (contextprovider == null)
		// contextprovider = new TersusHelpContentProvider(this);
		// return contextprovider;
		// }
		return super.getAdapter(type);
	}

	protected void createActions()
	{

		super.createActions();
		ActionRegistry registry = getActionRegistry();

		registerAction(registry, new UndoAction(this));

		registerAction(registry, new RedoAction(this));

		registerAction(registry, new SelectAllAction(this));

		registerAction(registry, new SaveAction(this));

		registerAction(registry, new PrintAction(this));

		registerAction(registry, new OpenAction(this));

		registerAction(registry, new RemoveElementAction(this));

		registerAction(registry, new OpenInANewTabAction(this));

		registerAction(registry, new ShowInRepositoryAction(this));

		registerAction(registry, new ShowUsagesAction(this));

		registerAction(registry, new ShowLinksAction(this));

		registerAction(registry, new ApplyTableWizardAction(this));

		registerAction(registry, new RenameAction(this));

		registerAction(registry, new ReplaceWithAction(this));
		
		registerAction(registry, new ChangePluginAction(this));

		registerAction(registry, new AddPropertyAction(this));

		registerAction(registry, new EditRoleAction(this));

		registerAction(registry, new ToggleRepetitiveAction(this));

		registerAction(registry, new ToggleMandatoryAction(this));
		
		registerAction(registry, new ToggleAlwaysCreateAction(this));

		registerAction(registry, new SetFlowDataElementType(this));

		registerAction(registry, new AddAncestorReferenceAction(this));

		registerAction(registry, new CopyAction(this));

		registerAction(registry, new PasteElementsAction(this));

		registerAction(registry, new HorizontalAlignAction(this));

		registerAction(registry, new VerticalAlignAction(this));

		registerAction(registry, new SizeAlignAction(this));

		registerAction(registry, new EncapsulateAction(this));

		registerAction(registry, new DecapsulateAction(this));

		registerAction(registry, new RemoveInvalidFlowsAction(this));

		registerAction(registry, new MoveElementAction(this));

		registerAction(registry, new MoveModelsEditorAction(this));

		registerAction(registry, new AnalyzeDependenciesAction(this));

		registerAction(registry, new PasteCloneAction(this));

		registerAction(registry, new CopyElementNameAction(this));
		
		registerAction(registry, new PasteElementsAction(this));

		registerAction(registry, new AddElementAction(this));

		registerAction(registry, new EditNoteAction(this));

		registerAction(registry, new ToggleElementTypeAction(this));

		registerAction(registry, new ImportStructureFormDBAction(this));

		registerAction(registry, new HelpAction(this));
	}

	private void registerAction(ActionRegistry registry,IAction action)
	{
		registry.registerAction(action);
		getSelectionActions().add(action.getId());
	}
/*
	private void registerAction(IAction action)
	{
		getActionRegistry().registerAction(action);
		getSelectionActions().add(action.getId());
	}
*/
	public Viewport getViewport()
	{
		TersusRootEditPart root = (TersusRootEditPart) getGraphicalViewer().getRootEditPart();

		return (Viewport) root.getFigure();
	}

	public TopFlowEditPart getTopFlowEditPart()
	{
		return (TopFlowEditPart) getGraphicalViewer().getContents();
	}

	public TersusGraphicalViewer getViewer()
	{
		return (TersusGraphicalViewer) getGraphicalViewer();
	}

	public void dispose()
	{
		try
		{
			if (modelListener != null && getRootModel() != null)
				getRootModel().removePropertyChangeListener(modelListener);
			disposePreferenceListener();
		}
		catch (Exception e)
		{
			TersusWorkbench.log(e);
		}
		TersusEditDomain tersusEditDomain = getTersusEditDomain();
		if (tersusEditDomain != null)
		{
			super.dispose();
			tersusEditDomain.dispose();
		}
		
		getSite().getPage().removePartListener(partListener);
	}

	public DefaultEditDomain getEditDomain()
	{
		return super.getEditDomain();

	}
	
	public ActionRegistry getActionRegistry()
	{
		return super.getActionRegistry();
	}

	protected PaletteRoot getPaletteRoot()
	{
		if (paletteRoot == null)
		{
			paletteRoot = new PaletteRoot();
		}
		return paletteRoot;
	}

	public PaletteRoot _getPaletteRoot()
	{
		return getPaletteRoot();
	}

	private void updateToolRegistry(PaletteRoot root)
	{
		toolRegistry.clear();
		addPaletteEntryChildrenToToolRegistry(root, null);
	}

	private void addPaletteEntryToToolRegistry(PaletteEntry entry, String pathPrefix)
	{
		String path;
		if (pathPrefix != null)
			path = pathPrefix + "." + entry.getLabel();
		else
			path = entry.getLabel();

		if (entry instanceof ToolEntry)
			toolRegistry.put(path, entry);
		else if (entry instanceof PaletteContainer)
			addPaletteEntryChildrenToToolRegistry(entry, path);
	}

	private void addPaletteEntryChildrenToToolRegistry(PaletteEntry entry, String path)
	{
		PaletteContainer container = (PaletteContainer) entry;
		List children = container.getChildren();
		for (Iterator iter = children.iterator(); iter.hasNext();)
		{
			PaletteEntry child = (PaletteEntry) iter.next();
			addPaletteEntryToToolRegistry(child, path);
		}
	}

	/**
	 * @param tool
	 * @return
	 */
	protected Object getToolRegistryKey(ToolEntry tool)
	{
		Set entries = toolRegistry.entrySet();
		for (Iterator iter = entries.iterator(); iter.hasNext();)
		{
			Map.Entry entry = (Map.Entry) iter.next();
			if (entry.getValue() == tool)
				return entry.getKey();
		}
		return null;
	}

	public Map getToolRegistry()
	{
		return toolRegistry;
	}

	public void commandStackChanged(EventObject event)
	{
		if (isDirty())
		{
			if (!savePreviouslyNeeded())
			{
				setSavePreviouslyNeeded(true);
				firePropertyChange(IEditorPart.PROP_DIRTY);
			}
		}
		else
		{
			setSavePreviouslyNeeded(false);
			firePropertyChange(IEditorPart.PROP_DIRTY);
		}
		super.commandStackChanged(event);
	}

	private boolean savePreviouslyNeeded()
	{
		return savePreviouslyNeeded;
	}

	/**
	 * @param b
	 */
	public void setSavePreviouslyNeeded(boolean b)
	{
		savePreviouslyNeeded = b;
	}

	/**
	 * Holds the last highlighted part
	 */
	private TersusEditPart highlighted = null;

	protected String TERSUS_PALETTE_DOCK_LOCATION;

	protected static final String TERSUS_PALETTE_STATE = null;

	protected static final String TERSUS_PALETTE_SIZE = null;

	private PaletteViewer currentPaletteViewer;

	private SelectionSynchronizer synchronizer;

	/**
	 * Highlight an element in the diagram (by temporarily modifying it's visual characteristics)
	 * 
	 * If no element is found with the given path, no part is highlighted (existing highlighting is
	 * cleared anyway)
	 * 
	 * @param path
	 *            A <code>Path></code> that identifies the element relative to the 'top' element
	 */
	public void highlight(Path path)
	{
		if (highlighted != null)
			highlighted.clearHighlight();
		TersusEditPart part = getTopFlowEditPart().getChild(path, true);
		if (part == null)
		{
			System.out.println("Element not found in path: " + path);
			return;
		}
		part.highlight();
		highlighted = part;
	}

	/**
	 * @param model
	 * @return
	 */
	public static ImageLocator getImageLocator(Model model)
	{
		if (model == null)
			return null;
		String iconFolderName = (String) model.getProperty(BuiltinProperties.ICON_FOLDER);
		WorkspaceRepository repository = (WorkspaceRepository) model.getRepository();
		if (iconFolderName == null)
			return null;
		ImageLocator locator = repository.getImageLocator(iconFolderName);
		return locator;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.parts.GraphicalEditorWithFlyoutPalette#getPalettePreferences()
	 */
	protected FlyoutPreferences getPalettePreferences()
	{
		return new FlyoutPreferences()
		{
			public int getDockLocation()
			{
				return Preferences.getInt(Preferences.PALETTE_DOCK_LOCATION);
			}

			public int getPaletteState()
			{
				return Preferences.getInt(Preferences.PALETTE_STATE);
			}

			public int getPaletteWidth()
			{
				return Preferences.getInt(Preferences.PALETTE_WIDTH);
			}

			public void setDockLocation(int location)
			{
				Preferences.setValue(Preferences.PALETTE_DOCK_LOCATION, location);
			}

			public void setPaletteState(int state)
			{
				Preferences.setValue(Preferences.PALETTE_STATE, state);
			}

			public void setPaletteWidth(int width)
			{
				Preferences.setValue(Preferences.PALETTE_WIDTH, width);
			}
		};
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.parts.GraphicalEditorWithFlyoutPalette#createPaletteViewerProvider()
	 */
	protected PaletteViewerProvider createPaletteViewerProvider()
	{
		return new PaletteViewerProvider(getEditDomain())
		{
			protected void configurePaletteViewer(final PaletteViewer viewer)
			{
				super.configurePaletteViewer(viewer);
				viewer.setPaletteViewerPreferences(PALETTE_PREFERENCES);
				setCurrentPaletteViewer(viewer);
				/*
				 * (non-Javadoc)
				 * 
				 * @see org.eclipse.gef.ui.parts.GraphicalEditorWithPalette#configurePaletteViewer()
				 */
				if (Debug.USER_ACTIONS)
				{
					PaletteListener listener = new PaletteListener()
					{
						public void activeToolChanged(PaletteViewer palette, ToolEntry tool)
						{
							Object key = getToolRegistryKey(tool);
							Trace.add(new SetActiveToolUserAction(key));
						}
					};
					viewer.addPaletteListener(listener);
				}
				PaletteCustomizer customizer = new PaletteCustomizer()
				{
					public void revertToSaved()
					{
					}

					public void save()
					{
						IFile paletteConfigurationFile = getPaletteConfigurationFile();

						PaletteConfigurator.save(getPaletteRoot(), paletteConfigurationFile);
					}
				};
				viewer.setCustomizer(customizer);
				// ((GraphicalEditPart)
				// TODO remove if possible
				// paletteViewer.getRootEditPart()).getFigure().validate();
				PaletteEditPartFactory factory = new PaletteEditPartFactory()
				{
					protected EditPart createEntryEditPart(EditPart parentEditPart, Object model)
					{
						EditPart part = new TersusToolEntryEditPart((PaletteEntry) model);
						return part;
					}

					protected EditPart createDrawerEditPart(EditPart parentEditPart, Object model)
					{
						return new TersusDrawerEditPart((PaletteDrawer) model);
					}
				};
				viewer.setEditPartFactory(factory);
			}
		};
	}

	/**
	 * Returns the current palette viewer. This method was added to facilitate testing.
	 * 
	 * @return the current palette viewer or null if a palette viewer was never initialized
	 */
	public PaletteViewer getCurrentPaletteViewer()
	{
		return currentPaletteViewer;
	}

	/**
	 * @param currentPaletteViewer
	 *            The currentPaletteViewer to set.
	 */
	private void setCurrentPaletteViewer(PaletteViewer currentPaletteViewer)
	{
		this.currentPaletteViewer = currentPaletteViewer;
	}

	private IFile getPaletteConfigurationFile()
	{
		IProject project = getRepository().getProject();
		IFile paletteConfigurationFile = project.getFile(ProjectStructure.PALETTE_XML);
//		Failed attempt to handle localized palettes (palette.<lang>.xml)
//		Null exception When palette.xml is missing 
//		IFile  paletteConfigurationFile = project.getFile(Localize.getLocalizedFilename(ProjectStructure.PALETTE_XML));
//		if (paletteConfigurationFile == null)
//			paletteConfigurationFile = project.getFile(Localize.getLocalizedFilename(ProjectStructure.PALETTE_XML,ProjectStructure.DEFAULT_LOCALE));
//		if (paletteConfigurationFile == null)
//			paletteConfigurationFile = project.getFile(ProjectStructure.PALETTE_XML);
		return paletteConfigurationFile;
	}
	private IFile getPalettePropertiesFile()
	{
		IProject project = getRepository().getProject();
		IFile palettePropertiesFile = project.getFile("palette.properties");
		return palettePropertiesFile;
	}

	/**
	 * Returns s SelectionSynchronizer (used to synchronize the outline view with the editor)
	 * 
	 * @return the syncrhonizer
	 */
	public SelectionSynchronizer getSelectionSynchronizer()
	{
		if (synchronizer == null)
			synchronizer = new SelectionSynchronizer();
		return synchronizer;
	}

	public void refresh()
	{
		Path selectedPath = null;
		if (getViewer() == null)
			return;
		List<?> selection = getViewer().getSelectedEditParts();
		if (selection != null)
		{
			// Find the top-most selected part
			for (Object obj : selection)
			{
				TersusEditPart part = (TersusEditPart) obj;
				Path path = part.getPath();
				if (selectedPath == null
						|| path.getNumberOfSegments() < selectedPath.getNumberOfSegments())
					selectedPath = path.getCopy();
			}
		}
		reset();
		if (outlinePage != null)
			outlinePage.attachToEditor();
		getGraphicalViewer().setContents(getModelWrapper());
		// configureGraphicalViewer();
		while (selectedPath != null && selectedPath.getNumberOfSegments() > 0)
		{
			TersusEditPart selectedEditPart = getTopFlowEditPart().getChild(selectedPath, true);
			if (selectedEditPart != null)
			{
				getViewer().select(selectedEditPart);
				if (selectedEditPart.getParent() instanceof TersusEditPart)
					((TersusEditPart) selectedEditPart.getParent()).zoomToPart();
				else
					selectedEditPart.zoomToPart();
				break;
			}
			else
				selectedPath.removeLastSegment();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.EditorPart#doSaveAs()
	 */
	public void doSaveAs()
	{
	}

	public static void openBrowser(String url)
	{
		try
		{
			/*
			 * Removed Tersus-Specific browser settings on 2009-06-07 (planning to remove commented
			 * code in a few weeks if we don't see problems)
			 * 
			 * String browserPath = tersus.editor.preferences.Preferences
			 * .getString(tersus.editor.preferences.Preferences.BROWSER_PATH); File file = new
			 * File(browserPath); if (!file.exists()) { if (MessageDialog
			 * .openQuestion(TersusWorkbench.getActiveWorkbenchShell(), "Browser not found",
			 * "A browser was not found in the default location. Would you like to choose the browser?"
			 * )) { FileDialog dialog = new FileDialog(TersusWorkbench.getActiveWorkbenchShell());
			 * dialog.setText("Please choose the browser program or script"); browserPath =
			 * dialog.open(); if (browserPath != null)
			 * Preferences.setValue(tersus.editor.preferences.Preferences.BROWSER_PATH,
			 * browserPath); }
			 * 
			 * } Runtime.getRuntime().exec(new String[] { browserPath, url });
			 */
			IWorkbenchBrowserSupport support = PlatformUI.getWorkbench().getBrowserSupport();
			support.getExternalBrowser().openURL(new URL(url));
		}
		catch (Exception e)
		{
			TersusWorkbench.log(e);
			String msg = "Failed to open URL '" + url;
			TersusWorkbench.error(msg, e);
		}
	}

	public Display getDisplay()
	{
		return getViewer().getControl().getDisplay();
	}

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException
	{
		if (!(input instanceof ConcreteModelIdentifier))
			throw new PartInitException("Invalid Input: Must be ConcreteModelIdentidier");
		if (!((ConcreteModelIdentifier) input).exists())
			throw new PartInitException("Missing model "
					+ ((ConcreteModelIdentifier) input).getModelId() + " in project "
					+ ((ConcreteModelIdentifier) input).getProject().getName());

		super.init(site, input);
	}

	public void activate()
	{
		IWorkbenchPage page = getEditorSite().getPage();
		if (page.getActivePart() != this)
			page.activate(this);
	}
	
	protected class PartListener implements IPartListener
	{
		public void partOpened(IWorkbenchPart part)
		{
			if ("org.eclipse.help.ui.internal.views.HelpView".equals(part.getClass().getName()))
			{
				final HelpView helpPart = (HelpView)part;
				new UIJob("Fix for Help View")
				{
					public IStatus runInUIThread(IProgressMonitor monitor)
					{
						getSite().getPage().hideView(helpPart);
						return Status.OK_STATUS;
					}
				}.schedule();
				
				ActionRegistry actionRegistry = getActionRegistry();
				HelpAction helpAction = (HelpAction)actionRegistry.getAction(TersusActionConstants.HELP_ACTION_ID);
				
				if (helpAction.calculateEnabled())
					helpAction.run();
			}
		}

		public void partDeactivated(IWorkbenchPart part)
		{
		}

		public void partClosed(IWorkbenchPart part)
		{
		}

		public void partBroughtToTop(IWorkbenchPart part)
		{
		}

		public void partActivated(IWorkbenchPart part)
		{
		}
	}
}

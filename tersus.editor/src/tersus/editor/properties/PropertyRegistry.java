package tersus.editor.properties;

import java.util.ArrayList;

import tersus.model.BuiltinProperties;
import tersus.model.Slot;

public class PropertyRegistry
{
    private static ArrayList<String> RegisteredProperties;

    static
    {
        RegisteredProperties = new ArrayList<String>();
        RegisteredProperties.add(BuiltinProperties.DATA_TYPE_SETTING);
    }
    
    public static boolean isPropertyRegistred(String propertyName)
    {
        return RegisteredProperties.contains(propertyName);
    }
    
    public static String[] getPropertyOptionalValues(String propertyName)
    {
        if(BuiltinProperties.DATA_TYPE_SETTING.equals(propertyName))
        {
            String[] values = {Slot.DATA_TYPE_SETTING_EXPLICIT, Slot.DATA_TYPE_SETTING_INFERRED};
            return values;
        }
        
        return null;  
    }

    public static String getDefaultPropertyVal(String propertyName)
    {
        if(BuiltinProperties.DATA_TYPE_SETTING.equals(propertyName))
            return "inferred";
        
        return null;  
    }
}

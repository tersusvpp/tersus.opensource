/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.properties;

import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.util.Misc;
import tersus.util.PropertyChangeListener;

/**
 * @author liat Shiff
 */
public class ModelObjectWrapper
{
	public static final int MODE_DEFAULT = 0;

	private Model model;

	private ModelElement element;

	private boolean missingModel;
	/**
	 * MODE_DATA_ELEMENT is used to indicate that an element appears inside a data structure
	 * (regardless of whether the type of the element is sub-flow, data element or slot.
	 */
	public static final int MODE_DATA_ELEMENT = 1;

	private int mode = MODE_DEFAULT;

	/**
	 * Creates a new Wrapper that represnets a top-level model
	 */
	public ModelObjectWrapper(Model model)
	{
		Misc.assertion(model != null);
		this.model = model;
		this.missingModel = false;
	}

	public ModelObjectWrapper(Model model, ModelElement element)
	{
		Misc.assertion(model != null);
		Misc.assertion(element != null);

		this.model = model;
		this.element = element;
		
		this.missingModel = false;
			
	}

	public ModelObjectWrapper(ModelElement element, boolean missingModel)
	{
		Misc.assertion(element != null);

		this.element = element;
		this.missingModel = missingModel;
	}

	public ModelObjectWrapper(ModelObject modelObject)
	{
		if (modelObject instanceof Model)
			model = (Model) modelObject;
		else
		{
			element = (ModelElement) modelObject;
			model = element.getReferredModel();
		}
		
		this.missingModel = false;
	}

	public ModelElement getElement()
	{
		return element;
	}

	public Model getModel()
	{
		return model;
	}

	public String toString()
	{
		if (element != null)
			return String.valueOf(element);
		else
			return String.valueOf(model);
	}

	public int hashCode()
	{
		if (element != null)
			return element.hashCode();
		else
			return model.hashCode();
	}

	public void addPropertyChangeListener(PropertyChangeListener listener)
	{
		if (model != null)
			model.addPropertyChangeListener(listener);
		if (element != null)
			element.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener)
	{
		if (model != null)
			model.removePropertyChangeListener(listener);
		if (element != null)
			element.removePropertyChangeListener(listener);
	}

	public boolean equals(Object obj)
	{
		if (obj instanceof ModelObjectWrapper)
		{
			ModelObjectWrapper wrapper = (ModelObjectWrapper) obj;
			return wrapper.element == element && wrapper.model == model;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Gets the mode of this wrapper. The mode is a machanism for distinguishing between sub-flows
	 * and flow-as-data elements.
	 */
	public int getMode()
	{
		return mode;
	}

	public void setMode(int mode)
	{
		this.mode = mode;
	}

	public ModelObject getModelObject()
	{
		if (element != null)
			return element;
		else
			return model;
	}
	
	public boolean isMissingModel()
	{
		return missingModel;
	}
}

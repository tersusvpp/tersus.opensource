/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.properties;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.swt.widgets.Composite;

import tersus.model.MetaProperty;
import tersus.util.Enum;
import tersus.util.EnumFactory;

public class EnumPropertyDescriptor extends TersusPropertyDescriptor
{

    private EnumFactory factory;

    /**
     * @param category
     * @param property
     */
    public EnumPropertyDescriptor(String name, boolean prototype)
    {
        super(name, prototype);

    }

    public void setSharedMetaProperty(MetaProperty metaProperty)
    {
        super.setSharedMetaProperty(metaProperty);

        if (metaProperty != null)
        {
            factory = new EnumFactory(metaProperty.valueClass);
            
        }

    }

    public void setLocalMetaProperty(MetaProperty metaProperty)
    {
        if (metaProperty != null)
        {
            super.setLocalMetaProperty(metaProperty);
            factory = new EnumFactory(metaProperty.valueClass);

        }
        
    }

    public CellEditor createPropertyEditor(Composite parent)
    {
        ComboBoxCellEditor editor = new ComboBoxCellEditor(parent, factory.getStringValues());
        return editor;
    }

    public Object translateToModel(Object value)
    {
        if (value instanceof String)
        {
            return translateToModelFromString((String) value);
        }
        return factory.getValue(((Integer) value).intValue());
    }

    private Object translateToModelFromString(String value)
    {
        Enum valEnum = factory.getValue(value);

        return factory.getValue(valEnum.getIndexObject());
    }

    public Object translateFromModel(Object value)
    {
        if (value == null)
            return new Integer(0);
        return ((Enum) value).getIndexObject();
    }
}
/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.properties;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.widgets.Composite;

import tersus.model.MetaProperty;

//TODO this class should be abstract 
public class TersusPropertyDescriptor
{
    private MetaProperty sharedMetaProperty = null;
    private MetaProperty localMetaProperty = null;

    private String displayName;
    private boolean readOnly = true;
    private boolean prototype;

    public TersusPropertyDescriptor(String name, boolean prototype)
    {
        setDisplayName(name);
        this.prototype = prototype;
    }

    public void setLocalMetaProperty(MetaProperty metaProperty)
    {
        localMetaProperty = metaProperty;
    }

    public MetaProperty getLocalMetaProperty()
    {
        if (localMetaProperty != null)
            return localMetaProperty;

        return null;
    }

    public void setSharedMetaProperty(MetaProperty metaProperty)
    {
        sharedMetaProperty = metaProperty;
    }

    public MetaProperty getSharedMetaProperty()
    {
        if (sharedMetaProperty != null)
            return sharedMetaProperty;

        return null;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String name)
    {
        displayName = name;
    }

    public Object translateToModel(Object value)
    {
        return value;
    }

    public Object translateFromModel(Object value)
    {
        return value;
    }

    protected void setPrototype(boolean prototype)
    {
        this.prototype = prototype;
    }
    
    public String getPropertyName()
    {
        if(sharedMetaProperty != null)
            return sharedMetaProperty.getName();
        if(localMetaProperty != null)
            return localMetaProperty.getName();
        
        return null;
    }
    public CellEditor createPropertyEditor(Composite parent)
    {
        return null;
    }

    /**
     * Indicates whether both shared and local values can be modified (unless read only) 
     * @return
     */
    public boolean isDualDescriptor()
    {
        return ((sharedMetaProperty != null && sharedMetaProperty.isdualProperty())
                || (localMetaProperty != null && localMetaProperty.isdualProperty()));
    }

    public boolean canDelete()
    {
        return !prototype && (localMetaProperty == null || localMetaProperty.canDelete())
                && (sharedMetaProperty == null || sharedMetaProperty.canDelete());
    }
}

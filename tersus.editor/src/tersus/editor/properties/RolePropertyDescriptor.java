/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.properties;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.widgets.Composite;

import tersus.model.Role;

/**
 * An <code>IPropertyDescriptor</code> for role property of ModelObjects
 */
public class RolePropertyDescriptor extends TersusPropertyDescriptor
{

	/**
	 * @param category
	 * @param property
	 */
	public RolePropertyDescriptor(String name, boolean prototype)
	{
		super(name, prototype);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.views.properties.IPropertyDescriptor#createPropertyEditor(org.eclipse.swt.widgets.Composite)
	 */
	public CellEditor createPropertyEditor(Composite parent)
	{
	    CellEditor editor = new TextCellEditor(parent);
        return editor;
	}

	public Object translateToModel(Object value)
	{
	    if ("".equals(value.toString()) || value == null)
	    {
	        return null;
	    }
		return Role.get((String)value);
	}
	
	public Object translateFromModel(Object value)
	{
	    if (value == null)
	        return "";
		return String.valueOf(value);
	}

}
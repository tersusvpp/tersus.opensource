/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.properties;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.widgets.Composite;

/**
 * An <code>IPropertyDescriptor</code> for string properties of ModelObjects
 */
public class StringPropertyDescriptor extends TersusPropertyDescriptor
{

    /**
     * @param category
     * @param property
     */
    public StringPropertyDescriptor(String name, boolean prototype)
    {
        super(name, prototype);
    }

    public CellEditor createPropertyEditor(Composite parent)
    {
        CellEditor editor = new TextCellEditor(parent);
        return editor;
    }

    public Object translateToModel(Object value)
    {
        if (value.toString().length() == 0)
            return null;
        
        return value;
    }

    public Object translateFromModel(Object value)
    {
        if (value == null)
            return "";
        return value;
    }
}
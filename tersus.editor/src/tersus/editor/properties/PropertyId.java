/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.properties;

/**
 * 
 */
public class PropertyId
{
	String name;
	String category;
	
	public PropertyId (String name, String category)
	{
		this.name = name;
		this.category = category;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj)
	{
		if (obj instanceof PropertyId)
		{
			return equals((PropertyId)obj);
		}
		return false;
	}

	public boolean equals (PropertyId other)
	{
		if (other == null)
			return false;
		return isEqual(category, other.category) && isEqual (name, other.name);
	}
	/**
	 * @param category
	 * @param string
	 */
	private boolean isEqual(String s1, String s2)
	{
		if (s1 == null)
			return (s2 == null);
		return s1.equals(s2);
		
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		return category+"." + name;
	}

}

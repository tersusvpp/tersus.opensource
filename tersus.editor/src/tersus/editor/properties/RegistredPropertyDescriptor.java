package tersus.editor.properties;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.swt.widgets.Composite;

public class RegistredPropertyDescriptor extends TersusPropertyDescriptor
{

    public RegistredPropertyDescriptor(String name, boolean prototype)
    {
        super(name, prototype);
    }

    public CellEditor createPropertyEditor(Composite parent)
    {
        ComboBoxCellEditor editor = new ComboBoxCellEditor(parent, PropertyRegistry
                .getPropertyOptionalValues(getPropertyName()));
        return editor;
    }
    
    public Object translateToModel(Object value)
    {
        if ((value instanceof String && ((String) value).length() == 0)
                || ((value instanceof Boolean) && !((Boolean)value)))
            return PropertyRegistry.getDefaultPropertyVal(getPropertyName());
        
        return value;
    }

    public Object translateFromModel(Object value)
    {
        if (value == null)
            return PropertyRegistry.getDefaultPropertyVal(getPropertyName());
        
        return value.toString();
    }
}

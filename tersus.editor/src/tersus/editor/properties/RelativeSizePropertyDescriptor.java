/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.properties;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.widgets.Composite;

import tersus.model.RelativeSize;

/**
 * An <code>IPropertyDescriptor</code> for RelativeSize properties of
 * ModelObjects
 * 
 * @author Liat Shiff
 */
public class RelativeSizePropertyDescriptor extends TersusPropertyDescriptor
{

    public RelativeSizePropertyDescriptor(String name, boolean prototype)
    {
        super(name, prototype);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.views.properties.IPropertyDescriptor#createPropertyEditor(org.eclipse.swt.widgets.Composite)
     */
    public CellEditor createPropertyEditor(Composite parent)
    {
        CellEditor editor = new TextCellEditor(parent);
        return editor;
    }

    public Object translateToModel(Object value)
    {
        RelativeSize relativeSizeVal = null;

        if (value instanceof String)
            relativeSizeVal = RelativeSize.valueOf((String) value);
        else
            relativeSizeVal = (RelativeSize) value;

        if (relativeSizeVal == null || relativeSizeVal.getHeight() == 0 && relativeSizeVal.getWidth() == 0)
            return null;

        return relativeSizeVal;
    }

    // From Model to Double
    public Object translateFromModel(Object value)
    {
        if (value == null)
            // Default size
            return null;
        return RelativeSize.valueOf(value.toString());
    }

}

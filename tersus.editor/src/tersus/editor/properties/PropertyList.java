/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.properties;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.swing.event.EventListenerList;

import tersus.model.BuiltinProperties;
import tersus.model.MetaProperty;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.model.RelativePosition;
import tersus.model.RelativeSize;
import tersus.model.Role;
import tersus.model.TemplateAndPrototypeSupport;
import tersus.util.Enum;
import tersus.util.Misc;
import tersus.util.ObservableListBase;
import tersus.util.PropertyChangeListener;


/**
 * @author Liat Shiff
 */
public class PropertyList extends ObservableListBase<TersusPropertyDescriptor>
{
    protected ArrayList<TersusPropertyDescriptor> content;
    protected HashSet<TersusPropertyDescriptor> list = new HashSet<TersusPropertyDescriptor>();
    ModelObjectWrapper modelObjectWrapper;
    
    private PropertyChangeListener modelWrapperListener = new PropertyChangeListener()
    {
        public void propertyChange(Object sourceObject, String property, Object oldValue, Object newValue)
        {
            if (BuiltinProperties.PROPERTIES.equals(property))
            {
                ArrayList<TersusPropertyDescriptor> newContent = createDescriptors();

                // Checks if property has been deleted
                for (TersusPropertyDescriptor descriptor : getContent())
                {
                    if (findPropertyDescriptor(descriptor.getDisplayName(), newContent) == null)
                    {
                        remove(descriptor);
                        return;
                    }
                }

                // Checks if property has been added
                for (TersusPropertyDescriptor descriptor : newContent)
                {
                    if (findPropertyDescriptor(descriptor.getDisplayName(), getContent()) == null)
                    {
                        add(descriptor);
                        return;
                    }
                }
            }
            
            for (TersusPropertyDescriptor descriptorToChange: getContent())
            {
                if (Misc.equal(descriptorToChange.getPropertyName(),property))
                {
                    descriptorToChange = refreshDescriptor(descriptorToChange);
                    fireItemChanged(descriptorToChange, property, oldValue, newValue);
                    break;
                }
            }
            
            
        }
    };
    
    public PropertyList(ModelObjectWrapper modelObjectWrapper)
    {
        this.modelObjectWrapper = modelObjectWrapper;
        
        init();
    }
    
    private ArrayList<TersusPropertyDescriptor> createDescriptors()
    {
        ArrayList<TersusPropertyDescriptor> list = new ArrayList<TersusPropertyDescriptor>();
        
        Model model = null;
        ModelElement element = null;
        
        if (modelObjectWrapper != null)
        {
            model = modelObjectWrapper.getModel();
            element = modelObjectWrapper.getElement();
        }
        
        if (model != null)
            addPropertyDescriptors(list, model, false);
        if (element != null)
            addPropertyDescriptors(list, element, false);

        if (model != null)
        {
            Model prototypeModel = TemplateAndPrototypeSupport.getPrototypeModel(model);
            Model oldPrototypeModel = null;

            // Prototype descriptors
            while (prototypeModel != null && prototypeModel != oldPrototypeModel)
            {
                addPropertyDescriptors(list, prototypeModel, true);

                oldPrototypeModel = prototypeModel;
                prototypeModel = TemplateAndPrototypeSupport.getPrototypeModel(prototypeModel);
            }
        }
        
        return list;
    }
    
    private TersusPropertyDescriptor refreshDescriptor(TersusPropertyDescriptor descriptor)
    {
        Model model = modelObjectWrapper.getModel();
        ModelElement element = modelObjectWrapper.getElement();
        
        if (model != null)
        {
            List<MetaProperty> metaProperties = model.getMetaProperties();
            MetaProperty metaProperty = null;
            
            for (int i = 0; i < metaProperties.size(); i++)
            {
                metaProperty = metaProperties.get(i);
                
                if (metaProperty.getDisplayName().equals(descriptor.getDisplayName()) &&
                        metaProperty.canDisplay())
                {
                    descriptor.setSharedMetaProperty(metaProperty);
                    break;
                }
                
                metaProperty = null;
            }
            
            if (metaProperty == null)
                descriptor.setSharedMetaProperty(null);
        }
        
        if (element != null)
        {
            List<MetaProperty> metaProperties = element.getMetaProperties();
            MetaProperty metaProperty = null;
            
            for (int i = 0; i < metaProperties.size(); i++)
            {
                metaProperty = metaProperties.get(i);
                
                if (metaProperty.getDisplayName().equals(descriptor.getDisplayName()) &&
                        metaProperty.canDisplay())
                {
                    descriptor.setLocalMetaProperty(metaProperty);
                    break;
                }
                
                metaProperty = null;
            }
            
            if (metaProperty == null)
                descriptor.setLocalMetaProperty(null);
        }
        
        return descriptor;
    }
    
    private void addPropertyDescriptors(ArrayList<TersusPropertyDescriptor> descriptors, ModelObject modelObject, boolean prototype)
    {
        List<MetaProperty> metaProperties = modelObject.getMetaProperties();
        for (int i = 0; i < metaProperties.size(); i++)
        {
            MetaProperty property = (MetaProperty) metaProperties.get(i);
            if (property.canDisplay())
            {
                TersusPropertyDescriptor descriptor = findPropertyDescriptor(property.getName(), descriptors);

                if (descriptor == null)
                {
                    descriptor = createDescriptor(property, prototype);
                    descriptors.add(descriptor);
                }
                else if (descriptor != null && prototype)
                {
                    descriptor.setPrototype(prototype);
                }

                if (modelObject instanceof Model)
                    descriptor.setSharedMetaProperty(property);
                else
                    // modelObject instanceof ModelElement
                    descriptor.setLocalMetaProperty(property);
            }
        }

    }
    
    private TersusPropertyDescriptor createDescriptor(MetaProperty property, boolean prototype)
    {
        if (PropertyRegistry.isPropertyRegistred(property.getName()))
            return new RegistredPropertyDescriptor(property.getDisplayName(), prototype);
        if (Enum.class.isAssignableFrom(property.valueClass))
            return new EnumPropertyDescriptor(property.getDisplayName(), prototype);
        if (Boolean.class.equals(property.valueClass))
            return new BooleanPropertyDescriptor(property.getDisplayName(), prototype);
        if (Role.class.equals(property.valueClass))
            return new RolePropertyDescriptor(property.getDisplayName(), prototype);
        if (String.class.equals(property.valueClass))
            return new StringPropertyDescriptor(property.getDisplayName(), prototype);
        if (Double.class.equals(property.valueClass))
            return new DoublePropertyDescriptor(property.getDisplayName(), prototype);
        if (RelativeSize.class.equals(property.valueClass))
            return new RelativeSizePropertyDescriptor(property.getDisplayName(), prototype);
        if (RelativePosition.class.equals(property.valueClass))
            return new RelativePositionPropertyDescriptor(property.getDisplayName(), prototype);
        
            
        return new TersusPropertyDescriptor(property.getDisplayName(), prototype); //TODO ensure that aren't unknown property types
    }
    
    public TersusPropertyDescriptor findPropertyDescriptor(String propertyName, ArrayList<TersusPropertyDescriptor> list)
    {
        if (list == null)
            return null;
        for (TersusPropertyDescriptor descriptor : list)
        {
            if (propertyName.equals(descriptor.getDisplayName()))
            {
                return descriptor;
            }
        }
        // Descriptor deosn't exist
        return null;
    }
    
    public Object getPropertyValue(String displayName, ModelObject modelObject)
    {
        Object value;
        TersusPropertyDescriptor descriptor = (TersusPropertyDescriptor) findPropertyDescriptor(displayName, content);

        if (modelObject instanceof Model)
        {
            value = (modelObjectWrapper.getModel()).getProperty(descriptor.getPropertyName());
        }
        else
        {
            Misc.assertion(modelObject instanceof ModelElement);
            value = (modelObjectWrapper.getElement()).getProperty(descriptor.getPropertyName());
        }
        return descriptor.translateFromModel(value);
    }

    private void init()
    {
        listeners = new EventListenerList();
        modelObjectWrapper.addPropertyChangeListener(modelWrapperListener);
    }
    
    public ArrayList<TersusPropertyDescriptor> getContent()
    {
        // The array list of descriptors is recreated each time the list is requested (new properties may have been adedd since the last time the list was requested
        if (content == null)
            content = createDescriptors();
        return content;
    }
    
    private void addAll(List<TersusPropertyDescriptor> inputContent)
    {
        if (content == null)
            content = createDescriptors();
        
        content.addAll(inputContent);
        fireContentChanged();
    }
    
    public void add(TersusPropertyDescriptor descriptor)
    {
        if (content == null)
            content = createDescriptors();
        
        content.add(descriptor);
        fireItemAdded(descriptor);
    }
    
    public void add(TersusPropertyDescriptor descriptor, int index)
    {
        if (content == null)
            content = createDescriptors();
        
        content.add(index, descriptor);
        fireItemAdded(descriptor);
    }
    
    public void remove(TersusPropertyDescriptor descriptor)
    {
        if (content == null)
            content = createDescriptors();
        
        content.remove(descriptor);
        fireContentChanged();
    }
    public void remove(int index)
    {   
        if (content == null)
            content = createDescriptors();
    
        content.remove(index);
        fireContentChanged();
    }
    
    public ModelObjectWrapper getModelObject()
    {
        return modelObjectWrapper;
    }
    
    public void dispose()
    {
        modelObjectWrapper.removePropertyChangeListener(modelWrapperListener);
        content.clear();
    }
}

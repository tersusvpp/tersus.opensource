/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.properties;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.swt.widgets.Composite;

import tersus.model.MetaProperty;
import tersus.util.Misc;

public class BooleanPropertyDescriptor extends TersusPropertyDescriptor
{

    /**
     * @param category
     * @param property
     */
    public BooleanPropertyDescriptor(String nameProperty, boolean prototype)
    {
        super(nameProperty, prototype);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.views.properties.IPropertyDescriptor#createPropertyEditor(org.eclipse.swt.widgets.Composite)
     */
    private static final String[] values = { "false", "true" };
    private static final Integer FALSE = new Integer(0);
    private static final Integer TRUE = new Integer(1);

    public CellEditor createPropertyEditor(Composite parent)
    {
        ComboBoxCellEditor editor = new ComboBoxCellEditor(parent, values);
        return editor;
    }

    public Object translateToModel(Object value)
    {
        return Misc.getBoolean(TRUE.equals(value)) ;
    }

    public Object translateFromModel(Object value)
    {
        if (value != null && ((Boolean) value).booleanValue())
            return TRUE;
        return FALSE;
    }

}
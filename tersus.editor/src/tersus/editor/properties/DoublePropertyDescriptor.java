/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.properties;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ICellEditorValidator;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.widgets.Composite;

/**
 * An <code>IPropertyDescriptor</code> for Double properties of
 * ModelObjects
 * 
 * @author Liat Shiff
 */
public class DoublePropertyDescriptor extends TersusPropertyDescriptor
{
    public DoublePropertyDescriptor(String name, boolean prototype)
    {
        super(name, prototype);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.views.properties.IPropertyDescriptor#createPropertyEditor(org.eclipse.swt.widgets.Composite)
     */
    public CellEditor createPropertyEditor(Composite parent)
    {
        CellEditor editor = new TextCellEditor(parent);
        return editor;
    }

    public Object translateToModel(Object value)
    {
        if (value instanceof String)
        {
            String stringVal = (String) value;
            if (null == stringVal || "".equals(stringVal))
                return null;
            else
                return Double.valueOf((String) value);
        }

        if (((Double) value).intValue() == 0)
            return null;
        return value;
    }

    // From Model to Double
    public Object translateFromModel(Object value)
    {
        if (value == null)
            return new Double(0);
        return Double.valueOf(value.toString());
    }
}

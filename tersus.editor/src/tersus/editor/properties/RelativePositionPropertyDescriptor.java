/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.properties;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.widgets.Composite;

import tersus.model.RelativePosition;

/**
 * An <code>IPropertyDescriptor</code> for RelativePosition properties of
 * ModelObjects
 * @author Liat Shiff
 */
public class RelativePositionPropertyDescriptor extends TersusPropertyDescriptor
{

    public RelativePositionPropertyDescriptor(String name, boolean prototype)
    {
        super(name, prototype);
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.ui.views.properties.IPropertyDescriptor#createPropertyEditor(org.eclipse.swt.widgets.Composite)
     */
    public CellEditor createPropertyEditor(Composite parent)
    {
        CellEditor editor = new TextCellEditor(parent);
        return editor;
    }
    
    public Object translateToModel(Object value)
    {
        RelativePosition relativePositionVal = null;
        
        if (value instanceof String)
            relativePositionVal = relativePositionVal.valueOf((String)value);
        else
            relativePositionVal = (RelativePosition)value;
        
        if (relativePositionVal == null ||
                relativePositionVal.getX() == 0 && relativePositionVal.getY() == 0)
            return null;
        
        return relativePositionVal;
    }
    
    // From Model to Double
    public Object translateFromModel(Object value)
    {
        if (value == null)
            // Default size
            return null;
        return RelativePosition.valueOf(value.toString());
    }

}

/************************************************************************************************
 * Copyright (c) 2003-2020 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. Initial API and implementation
 *************************************************************************************************/

package tersus.editor.properties;

import org.eclipse.core.resources.IProject;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.Page;
import org.eclipse.ui.views.properties.IPropertySheetPage;

import tersus.editor.RepositoryManager;
import tersus.editor.TersusEditor;
import tersus.editor.actions.AddPropertyAction;
import tersus.editor.actions.DeletePropertyAction;
import tersus.editor.actions.ResetPropertiesViewAction;
import tersus.editor.actions.TersusActionConstants;
import tersus.editor.editparts.TersusEditPart;
import tersus.model.MetaProperty;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.model.Repository;
import tersus.model.commands.CommandFailedException;
import tersus.model.commands.DeletePropertyCommand;
import tersus.model.commands.MultiStepCommand;
import tersus.util.Misc;
import tersus.workbench.SelectionHelper;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 * @author David Davidson
 */
public class TersusPropertySheetPage extends Page implements IPropertySheetPage
{
	private static final String[] TABLE_COLUMN_TITLES =
	{ "Name", "Shared", "Local" };

	private ModelObjectWrapper selectedModel;

	private TersusEditor sourcePart;

	private Repository repository;

	// SWT widgets
	private Table table;

	private Text searchInput;

	private Button showAllButton;

	private Composite container;

	private Composite topRow;

	private TableViewer viewer;

	private Action copyLocalPropertyAction;

	private Action copySharedPropertyAction;

	private Action copyPropertyNameAction;

	private AddPropertyAction addPropertyAction;

	private ResetPropertiesViewAction resetPropertyViewAction;

	private DeletePropertyAction deletePropertyAction;

	private PropertyListContentProvider propertiesListContentProvider;

	private PropertyList descriptors;

	public TersusPropertySheetPage(TersusEditor editor)
	{
		super();
		sourcePart = editor;
		repository = sourcePart.getRepository();

		resetPropertyViewAction = new ResetPropertiesViewAction(this);
		addPropertyAction = new AddPropertyAction(editor);

		deletePropertyAction = new DeletePropertyAction(this);
		deletePropertyAction.setEnabled(false);
	}

	@Override
	public void createControl(Composite parent)
	{
		container = new Composite(parent, SWT.NULL);

		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		layout.horizontalSpacing = 1;
		layout.verticalSpacing = 0;
		layout.makeColumnsEqualWidth = false;
		container.setLayout(layout);

		createTopRow(container);
		createSearchInTableButton();
		createCheckBoxShowAll();
		addFields();

		viewer.addSelectionChangedListener(new ISelectionChangedListener()
		{
			public void selectionChanged(SelectionChangedEvent event)
			{
				TersusPropertyDescriptor selectedPropertyDescriptor = getSelectedDescriptor();

				if (selectedPropertyDescriptor != null)
					deletePropertyAction.setEnabled(selectedPropertyDescriptor.canDelete()
							&& !isSelectedModelReadOnly());
				else
					deletePropertyAction.setEnabled(false);
			}
		});

		makeActions();
		hookContextMenu();

		IActionBars actionBars = getSite().getActionBars();
		addActionsToToolBarManager(actionBars.getToolBarManager());
	}

	public void createTopRow(Composite parent)
	{
		topRow = new Composite(parent, SWT.NULL);
		GridLayout topRowLayout = new GridLayout();
		topRowLayout.numColumns = 3;
		topRowLayout.verticalSpacing = 0;
		topRow.setLayout(topRowLayout);
	}

	private void addActionsToToolBarManager(IToolBarManager toolBarManager)
	{
		toolBarManager.add(resetPropertyViewAction);
		toolBarManager.add(addPropertyAction);
		toolBarManager.add(deletePropertyAction);
	}

	private void makeActions()
	{
		copyLocalPropertyAction = new Action()
		{
			public void run()
			{
				TersusPropertyDescriptor descriptor = getSelectedDescriptor();

				ModelElement modelElement = null;

				if (selectedModel != null)
					modelElement = selectedModel.getElement();

				copyValueToClipboard(getPropertyStringValue(modelElement, descriptor));
			}
		};

		copyLocalPropertyAction.setText("Copy local property value");
		copyLocalPropertyAction.setToolTipText("Copy local property value.");

		copySharedPropertyAction = new Action()
		{
			public void run()
			{
				TersusPropertyDescriptor descriptor = getSelectedDescriptor();

				Model model = null;

				if (selectedModel != null)
					model = selectedModel.getModel();

				copyValueToClipboard(getPropertyStringValue(model, descriptor));
			}
		};

		copySharedPropertyAction.setText("Copy shared property value");
		copySharedPropertyAction.setToolTipText("Copy sared property value.");

		copyPropertyNameAction = new Action()
		{
			public void run()
			{
				TersusPropertyDescriptor descriptor = getSelectedDescriptor();

				String propertyName = descriptor.getDisplayName();

				copyValueToClipboard(propertyName);
			}
		};

		copyPropertyNameAction.setText("Copy property name");
		copyPropertyNameAction.setToolTipText("Copy the selected property name.");
	}

	private String getPropertyStringValue(ModelObject modelObject,
			TersusPropertyDescriptor descriptor)
	{
		Object value = null;

		if (modelObject != null)
			value = modelObject.getProperty(descriptor.getPropertyName());

		String stringValue = null;

		if (descriptor.createPropertyEditor(table) instanceof TextCellEditor)
		{
			if (value == null)
				stringValue = "";
			else
				stringValue = value.toString();

		}
		else
			stringValue = descriptor.translateFromModel(value).toString();

		return stringValue;
	}

	private void copyValueToClipboard(String value)
	{
		if (value != null && !(value.length() == 0))
		{
			Clipboard clipboard = new Clipboard(getSite().getShell().getDisplay());
			TextTransfer textTransfer = TextTransfer.getInstance();
			clipboard.setContents(new String[]
			{ value }, new Transfer[]
			{ textTransfer });
			clipboard.dispose();
		}
	}

	protected void hookContextMenu()
	{
		MenuManager menuMgr = new MenuManager("Available Actions");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener()
		{
			public void menuAboutToShow(IMenuManager manager)
			{
				manager.add(copySharedPropertyAction);
				manager.add(copyPropertyNameAction);
				manager.add(copyLocalPropertyAction);
			}

		});

		Menu menu = menuMgr.createContextMenu(getControl());
		viewer.getControl().setMenu(menu);
	}

	private void addFields()
	{
		configureTable();
		propertiesListContentProvider = new PropertyListContentProvider(viewer);
		viewer.setContentProvider(propertiesListContentProvider);
		setViewerInput();
	}

	private void setViewerInput()
	{
		if (selectedModel != null)
		{
			viewer.setInput(descriptors);
		}
		else
			viewer.setInput(null);
	}

	private void configureTable()
	{
		table = new Table(container, SWT.BORDER | SWT.MULTI | SWT.FULL_SELECTION);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		table.setLayoutData(new GridData(GridData.FILL_BOTH));

		viewer = new TableViewer(table);

		TableViewerColumn propertyNameColumn = new TableViewerColumn(viewer, SWT.NONE);

		propertyNameColumn.getColumn().setWidth(150);
		propertyNameColumn.getColumn().setText(TABLE_COLUMN_TITLES[0]);
		propertyNameColumn.setLabelProvider(new CellLabelProvider()
		{
			@Override
			public void update(ViewerCell cell)
			{
				cell.setText(((TersusPropertyDescriptor) cell.getElement()).getDisplayName());
				cell.setImage(null);
			}

		});

		TableViewerColumn sharedPropertyColumn = new TableViewerColumn(viewer, SWT.NONE);

		sharedPropertyColumn.getColumn().setWidth(150);
		sharedPropertyColumn.getColumn().setText(TABLE_COLUMN_TITLES[1]);
		sharedPropertyColumn.setLabelProvider(new CellLabelProvider()
		{
			@Override
			public void update(ViewerCell cell)
			{
				TersusPropertyDescriptor descriptor = (TersusPropertyDescriptor) cell.getElement();

				String propertyName = descriptor.getDisplayName(); // TODO - use the property name,
				// not the display name

				if (descriptor.getSharedMetaProperty() != null)
				{
					Object objectValue = descriptors.getPropertyValue(propertyName, descriptors
							.getModelObject().getModel());
					String stringValue = getPropertyValueString(descriptor
							.translateToModel(objectValue));

					if (objectValue == null)
						stringValue = "";

					cell.setText(stringValue);
					cell.setImage(null);
				}
			}
		});

		sharedPropertyColumn.setEditingSupport(new TableEditingSupoort()
		{
			@Override
			protected void setValue(TersusPropertyDescriptor descriptor, Object value)
			{
				Command setPropertyCommand = null;

				final Object modelValue = descriptor.translateToModel(value);
				Object oldValue = selectedModel.getModel().getProperty(descriptor.getDisplayName());

				if ((oldValue != null && !oldValue.equals(modelValue))
						|| (modelValue != null && !modelValue.equals(oldValue)))
				{
					final Model model = selectedModel.getModel();
					final String propertyName = descriptor.getPropertyName();
					setPropertyCommand = new MultiStepCommand((WorkspaceRepository) model
							.getRepository())
					{
						@Override
						protected void run()
						{
							if (model.getMetaProperty(propertyName) == null)
								addMetaProperty(model, propertyName);

							setProperty(model, propertyName, modelValue);
						}

					};

					getCommandStack().execute(setPropertyCommand);
				}
			}

			@Override
			protected void setModelObject()
			{
				if (selectedModel != null)
					modelObject = selectedModel.getModel();
			}
		});

		TableViewerColumn localPropertyColumn = new TableViewerColumn(viewer, SWT.NONE);

		localPropertyColumn.getColumn().setWidth(150);
		localPropertyColumn.getColumn().setText(TABLE_COLUMN_TITLES[2]);
		localPropertyColumn.setLabelProvider(new CellLabelProvider()
		{
			@Override
			public void update(ViewerCell cell)
			{
				TersusPropertyDescriptor descriptor = (TersusPropertyDescriptor) cell.getElement();
				ModelObjectWrapper modelObjectWrapper = descriptors.getModelObject();

				String propertyName = descriptor.getDisplayName();

				if (descriptor.getLocalMetaProperty() != null)
				{
					Object objectValue = null;
					ModelElement element = modelObjectWrapper.getElement();

					if (element != null)
						objectValue = descriptors.getPropertyValue(propertyName, element);

					String stringValue;
					if (objectValue == null)
						stringValue = "";
					else
						stringValue = getPropertyValueString(descriptor
								.translateToModel(objectValue));

					cell.setText(stringValue);
					cell.setImage(null);
				}
			}
		});

		localPropertyColumn.setEditingSupport(new TableEditingSupoort()
		{
			@Override
			protected void setValue(TersusPropertyDescriptor descriptor, Object value)
			{
				Command setPropertyCommand = null;

				final Object modelValue = descriptor.translateToModel(value);
				Object oldValue = selectedModel.getElement().getProperty(
						descriptor.getDisplayName());

				final ModelElement element = selectedModel.getElement();

				if (((oldValue != null && !oldValue.equals(modelValue)) || (modelValue != null && !modelValue
						.equals(oldValue))))
				{

					final String propertyName = descriptor.getPropertyName();
					setPropertyCommand = new MultiStepCommand((WorkspaceRepository) element
							.getRepository())
					{
						@Override
						protected void run()
						{
							if (element.getMetaProperty(propertyName) == null)
								addMetaProperty(element, propertyName);

							setProperty(element, propertyName, modelValue);
						}

					};

					getCommandStack().execute(setPropertyCommand);
				}
			}

			@Override
			protected void setModelObject()
			{
				if (selectedModel != null)
					modelObject = selectedModel.getElement();
			}
		});
	}

	private String getPropertyValueString(Object propertyValue)
	{
		if (propertyValue == null || propertyValue.equals(""))
		{
			return "";
		}
		else
			return propertyValue.toString();
	}

	private void createSearchInTableButton()
	{
		Label title = new Label(topRow, SWT.SINGLE);
		title.setText("Property name: ");

		searchInput = new Text(topRow, SWT.SINGLE | SWT.BORDER);
		searchInput.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				String inputText = searchInput.getText();
				String previousFilter = propertiesListContentProvider.getFilterPattern();
				if (!"".equals(inputText) && previousFilter == null)
					showAllButton.setSelection(true);

				updateFiltering();
			}
		});
	}

	private void createCheckBoxShowAll()
	{
		showAllButton = new Button(topRow, SWT.CHECK);

		showAllButton.setText("Show All");
//		showAllButton.setSelection(true);
		showAllButton.addSelectionListener(new SelectionListener()
		{
			public void widgetDefaultSelected(SelectionEvent e)
			{
				updateFiltering();
			}

			public void widgetSelected(SelectionEvent e)
			{
				updateFiltering();
			}
		});
	}

	public void resetPropertyView()
	{
		showAllButton.setSelection(false);
		propertiesListContentProvider.setShowAllProperties(false);

		searchInput.setText("");

		deletePropertyAction.setEnabled(false);
	}

	public void deleteProperty()
	{
		Command c = new DeletePropertyCommand(selectedModel, getSelectedDescriptor()
				.getPropertyName());

		try
		{
			getCommandStack().execute(c);
		}
		catch (CommandFailedException e)
		{
			StringBuffer message = new StringBuffer();
			message.append(e.getMessage());

			MessageDialog.openInformation(TersusWorkbench.getActiveWorkbenchShell(),
					"Read Only Package/File", message.toString());
		}
	}

	private CommandStack getCommandStack()
	{
		IProject project = ((WorkspaceRepository) repository).getProject();
		RepositoryManager manager = RepositoryManager.getRepositoryManager(project);
		return manager.getCommandStack();
	}

	@Override
	public Control getControl()
	{
		return container;
	}

	@Override
	public void setFocus()
	{
		// Not clear when this is called and why
	}

	public void selectionChanged(IWorkbenchPart part, ISelection selection)
	{
		// There is currently one TersusPropertySheetPage per editor- we want to change it in the
		// future
		if (Misc.ASSERTIONS)
			Misc.assertion(part == sourcePart);

		TersusEditPart selectedEditPart = (TersusEditPart) SelectionHelper.getSelectedItem(
				selection, TersusEditPart.class);
		if (selectedEditPart != null)
		{
			selectedModel = (ModelObjectWrapper) selectedEditPart.getModel();
			descriptors = new PropertyList(selectedModel);
		}
		else
		{
			selectedModel = null;
			descriptors = null;
		}

		if (selectedModel != null)
		{
			addPropertyAction.setEnabled(isModelObjectReadOnly(selectedModel.getModel())
					|| isModelObjectReadOnly(selectedModel.getElement()));
		}

		updateActions();
		setViewerInput();
	}

	public void updateActions()
	{
		addPropertyAction.update();
		resetPropertyViewAction.update();
	}

	public void dispose()
	{
		super.dispose();
		if (addPropertyAction != null)
		{
			addPropertyAction.dispose();
			addPropertyAction = null;
		}

	}

	private TersusPropertyDescriptor getSelectedDescriptor()
	{
		TersusPropertyDescriptor selectedProperty = (TersusPropertyDescriptor) SelectionHelper
				.getSelectedItem(viewer.getSelection(), TersusPropertyDescriptor.class);
		return selectedProperty;
	}

	private void updateFiltering()
	{
		String filterText = searchInput.getText();
		if ("".equals(filterText))
			filterText = null;
		propertiesListContentProvider.changeFilter(showAllButton.getSelection(), true, filterText);
	}

	protected boolean isModelObjectReadOnly(ModelObject obj)
	{
		if (obj instanceof Model)
			return ((Model) obj).isReadOnly();
		if (obj instanceof ModelElement)
			return ((ModelElement) obj).getParentModel().isReadOnly();

		return true;
	}

	protected boolean isSelectedModelReadOnly()
	{
		ModelElement element = selectedModel.getElement();
		Model model = selectedModel.getModel();

		return (element != null && element.getParentModel().isReadOnly())
				|| (model != null && model.isReadOnly());
	}

	abstract class TableEditingSupoort extends EditingSupport
	{
		ModelObject modelObject;

		public TableEditingSupoort()
		{
			super(viewer);
		}

		@Override
		protected boolean canEdit(Object element)
		{
			MetaProperty property = null;
			TersusPropertyDescriptor descriptor = (TersusPropertyDescriptor) element;

			setModelObject();

			if (modelObject instanceof Model)
				property = descriptor.getSharedMetaProperty();
			else
				// selectedModelObject instanceof ModelElement
				property = descriptor.getLocalMetaProperty();

			return !isModelObjectReadOnly(modelObject)
					&& ((property != null && property.canEdit()) || (property == null && descriptor
							.isDualDescriptor()));
		}

		abstract protected void setModelObject();

		@Override
		protected CellEditor getCellEditor(Object element)
		{
			TersusPropertyDescriptor propertyDescriptor = (TersusPropertyDescriptor) element;
			CellEditor cellEditor = propertyDescriptor.createPropertyEditor(table);

			if (cellEditor == null)
				cellEditor = new TextCellEditor(table);

			return cellEditor;
		}

		protected Object getValue(Object element)
		{
			TersusPropertyDescriptor descriptor = (TersusPropertyDescriptor) element;
			setModelObject();

			Object value = null;

			if (modelObject != null)
				value = modelObject.getProperty(descriptor.getPropertyName());

			// TODO eliminate special case after all properties are explicit
			if (getCellEditor(element) instanceof TextCellEditor)
			{
				if (value == null)
					return "";
				else
					return value.toString();
			}
			else
				return descriptor.translateFromModel(value);
		}

		@Override
		protected void setValue(Object element, Object value)
		{
			setValue((TersusPropertyDescriptor) element, value);
		}

		protected abstract void setValue(TersusPropertyDescriptor descriptor, Object value);
	}

	public void showProperty(String propertyName)
	{
		showAllButton.setSelection(true);

		updateFiltering();

		TableItem[] items = viewer.getTable().getItems();
		for (TableItem item : items)
		{
			if (item.getText(0).equals(propertyName))
			{
				Event e = new Event();
				e.item = item; //$NON-NLS-1$
				viewer.getTable().setSelection(new TableItem[]
				{ (TableItem) e.item });
				viewer.getTable().notifyListeners(SWT.Selection, e);
				break;
			}
		}
	}
}
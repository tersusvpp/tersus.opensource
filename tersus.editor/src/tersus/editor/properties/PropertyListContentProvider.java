/************************************************************************************************
 * Copyright (c) 2003-2020 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.properties;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import org.eclipse.jface.viewers.TableViewer;

import tersus.eclipse.utils.ListContentProvider;
import tersus.model.BuiltinProperties;
import tersus.model.MetaProperty;
import tersus.model.Model;

/**
 * @author Liat Shiff
 * @author David Davidson
 */
public class PropertyListContentProvider extends ListContentProvider<TersusPropertyDescriptor>
{
    boolean showAllProperties;
    boolean filterOn;
    String filterPattern;

    public PropertyListContentProvider(TableViewer viewer)
    {
        super(viewer);
        showAllProperties = false;
    }

    public void setShowAllProperties(boolean showAll)
    {
        showAllProperties = showAll;
    }

    public boolean getShowAllProperties()
    {
        return showAllProperties;
    }

    public void setFilterPattern(String pattern)
    {
        filterPattern = pattern;
    }

    public String getFilterPattern()
    {
        return filterPattern;
    }

    public void setFilterOn(boolean filterOn)
    {
        this.filterOn = filterOn;
    }

    public boolean getFilterOn()
    {
        return filterOn;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object[] getElements(Object inputElement)
    {
        Object[] objects = ((PropertyList) inputElement).getContent().toArray();

        if (inputElement instanceof PropertyList)
        {
            if (showAllProperties)
                return sortObjectList(getElementsByPattern(objects));
            else            
                return sortObjectList(getKeyElementsByPattern(objects, (PropertyList)inputElement));
        }
        else
            return NONE;
    }

    private Object[] getValidProperties(Object[] elements)
    {
        ArrayList<Object> validPropertyItems = new ArrayList<Object>();

        for (Object obj : elements)
        {
            validPropertyItems.add(obj);     
        }

        return validPropertyItems.toArray();
    }
    
    private Object[] getElementsByPattern(Object[] elements)
    {
        ArrayList<Object> filterResult = new ArrayList<Object>();

        for (Object obj : elements)
        {
            TersusPropertyDescriptor descriptor = (TersusPropertyDescriptor) obj;
            String propertyDisplayName = descriptor.getDisplayName();

            
            if (filterPattern == null || (propertyDisplayName.toLowerCase()).contains(filterPattern.toLowerCase()))
                filterResult.add(obj);
        }

        return filterResult.toArray();
    }

    private Object[] getKeyElementsByPattern(Object[] elements, PropertyList propertyList)
    {
        ArrayList<Object> keyElements = new ArrayList<Object>();

        for (Object obj : elements)
        {
            if (obj instanceof TersusPropertyDescriptor)
            {
                TersusPropertyDescriptor descriptor = (TersusPropertyDescriptor)obj;
                if (filterPattern != null && ! descriptor.getDisplayName().contains(filterPattern))
                    continue;
                if (Arrays.asList(BuiltinProperties.KEY_PROPERTIES).contains(descriptor.getDisplayName()))
                	keyElements.add(obj);
            }
        }

        return keyElements.toArray();
    }

/*	Show All checkbox functionality changed from empty to non-key properties
    private Object[] getNonEmptyElementsByPattern(Object[] elements, PropertyList propertyList)
    {
        ArrayList<Object> nonEmptyElements = new ArrayList<Object>();

        for (Object obj : elements)
        {
            if (obj instanceof TersusPropertyDescriptor)
            {
                TersusPropertyDescriptor descriptor = (TersusPropertyDescriptor)obj;
                if (filterPattern != null && ! descriptor.getDisplayName().contains(filterPattern))
                    continue;
                ModelObjectWrapper modelObjectWrapper = propertyList.getModelObject();

                MetaProperty sharedProperty = descriptor.getSharedMetaProperty();
                MetaProperty localProperty = descriptor.getLocalMetaProperty();

                Object sharedVal = null;
                if (sharedProperty != null)
                {
                    Model model = modelObjectWrapper.getModel();
                    sharedVal = propertyList.getPropertyValue(sharedProperty.getName(), model);

                    if (sharedVal != null && !(sharedVal instanceof String && ((((String) sharedVal).length() == 0))))
                        nonEmptyElements.add(obj);
                }

                Object localVal = null;
                if (localProperty != null && !nonEmptyElements.contains(obj))
                {
                    localVal = propertyList.getPropertyValue(localProperty.getDisplayName(), modelObjectWrapper
                            .getElement());

                    if (localVal != null && !(localVal instanceof String && ((((String) localVal).length() == 0))))
                        nonEmptyElements.add(obj);
                }
            }
        }

        return nonEmptyElements.toArray();
    }
*/
    public void changeFilter(boolean showAllProperties, boolean filterOn, String filterPattern)
    {
        setShowAllProperties(showAllProperties);
        setFilterOn(filterOn);
        setFilterPattern(filterPattern);

        viewer.refresh();
    }

    private Object[] sortObjectList(Object[] objects)
    {
        ArrayList<Object> sortObjects = new ArrayList<Object>();

        for (Object obj : objects)
        {
            sortObjects.add(obj);
        }

        Collections.sort(sortObjects, new Comparator<Object>()
        {
            public int compare(Object o1, Object o2)
            {
                return ((TersusPropertyDescriptor) o1).getDisplayName().compareTo(
                        ((TersusPropertyDescriptor) o2).getDisplayName());
            }
        });
        
        return sortObjects.toArray();
    }

    @Override
    public void itemChanged(TersusPropertyDescriptor item, String propertyName, Object oldValue, Object newValue)
    {
        super.itemChanged(item, propertyName, oldValue, newValue);
        if (!showAllProperties && (newValue == null || oldValue == null))
                contentChanged(input);
    }
    
}

/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.search;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.model.IWorkbenchAdapter;

import tersus.workbench.TersusWorkbench;

/**
 * @author Liat Shiff
 */
public class TersusSearchProjectEntry 
{
	private IProject project;
	
	private TersusSearchResult searchResult;
	
	public TersusSearchProjectEntry(IProject project, TersusSearchResult searchResult)
	{
		this.project = project;
		this.searchResult = searchResult;
	}
	
	public IProject getProject()
	{
		return project;
	}
	
	public TersusSearchResult getSearchResult()
	{
		return searchResult;
	}
	
	public Image getImage()
    {
        IWorkbenchAdapter adapter = (IWorkbenchAdapter) project.getAdapter(IWorkbenchAdapter.class);
        
        if (adapter == null)
            return null;
        ImageDescriptor descriptor = adapter.getImageDescriptor(project);
        Image image = TersusWorkbench.getImage(descriptor);
        
        return image;
    }
}

/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.viewers.DecoratingLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.search.ui.IContextMenuConstants;
import org.eclipse.search.ui.ISearchResultViewPart;
import org.eclipse.search.ui.text.AbstractTextSearchViewPage;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;

import tersus.editor.ConcreteModelIdentifier;
import tersus.editor.TersusEditor;
import tersus.editor.actions.ShowInNavigatorAction;
import tersus.editor.actions.ShowUsagesAction;
import tersus.editor.outline.ModelObjectTransfer;
import tersus.model.Model;
import tersus.model.ModelObject;
import tersus.model.Path;
import tersus.workbench.Configurator;
import tersus.workbench.TersusWorkbench;

/**
 * @author Liat Shiff
 */
public class TersusSearchResultPage extends AbstractTextSearchViewPage
{
	private TersusSearchTreeContentProvider contentProvider;

	private ShowUsagesAction fShowUsages;
	
	private ShowInNavigatorAction fShowInNavigator;

	private Action copyAction;

	private ISelectionChangedListener selectionChangedListener = new ISelectionChangedListener()
	{
		public void selectionChanged(SelectionChangedEvent event)
		{
			TersusSearchResultPage.this.selectionChanged(event);
		}
	};

	public void createControl(Composite parent)
	{
		super.createControl(parent);
		getViewer().addSelectionChangedListener(selectionChangedListener);

		int ops = DND.DROP_COPY | DND.DROP_MOVE;
		Transfer[] transfers = new Transfer[]
		{ ModelObjectTransfer.getInstance() };
		getViewer().addDragSupport(ops, transfers, new TersusSearchDragListener(this));

		getSite().getActionBars().setGlobalActionHandler(ActionFactory.COPY.getId(), copyAction);
	}

	@Override
	protected void configureTableViewer(TableViewer viewer)
	{
		// Table viewer is not using for present search results.
	}

	@Override
	protected void configureTreeViewer(TreeViewer viewer)
	{
		TersusSearchLabelProvider innerLabelProvider = new TersusSearchLabelProvider();

		viewer.setLabelProvider(new DecoratingLabelProvider(innerLabelProvider, PlatformUI
				.getWorkbench().getDecoratorManager().getLabelDecorator()));
		contentProvider = new TersusSearchTreeContentProvider(viewer);
		viewer.setContentProvider(contentProvider);

		viewer.addDoubleClickListener(new IDoubleClickListener()
		{
			public void doubleClick(DoubleClickEvent event)
			{
				ISelection selection = event.getSelection();
				if (selection instanceof IStructuredSelection)
				{
					IStructuredSelection structuredSelection = (IStructuredSelection) selection;
					for (Object selectedObj : structuredSelection.toList())
					{
						if (selectedObj instanceof TersusSearchModelEntry)
							openEditor((TersusSearchModelEntry) selectedObj);
						else if (selectedObj instanceof TersusSearchProjectEntry)
						{
							IProject project = ((TersusSearchProjectEntry) selectedObj)
									.getProject();
							Model rootModel = Configurator.getRootModel(project);

							if (rootModel != null)
								openEditor(new ConcreteModelIdentifier(project, rootModel.getId()));

						}
						else if (selectedObj instanceof TersusSearchElementEntry)
						{
							TersusSearchElementEntry entry = (TersusSearchElementEntry) selectedObj;
							TersusEditor editor = openEditor(entry.getParentModelEntry());
							editor.highlight(new Path(entry.getElement().getRole()));
						}
					}
				}
			}
		});
	}

	protected TersusEditor openEditor(TersusSearchModelEntry entry)
	{
		if (entry.isFlowModel())
		{
			ConcreteModelIdentifier concreteModelIdentifier = (ConcreteModelIdentifier) entry
					.getAdapter(ConcreteModelIdentifier.class);
			return openEditor(concreteModelIdentifier);
		}

		return null;
	}

	private TersusEditor openEditor(ConcreteModelIdentifier concreteModelIdentifier)
	{
		System.out.println("Opening " + concreteModelIdentifier.getModelId());
		IWorkbenchPage page = getSite().getPage();
		try
		{
			return (TersusEditor) page.openEditor(concreteModelIdentifier, TersusEditor.ID);
		}
		catch (PartInitException e)
		{
			TersusWorkbench.log(e);
		}

		return null;
	}

	protected void elementsChanged(Object[] objects)
	{
		if (contentProvider != null)
			contentProvider.elementsChanged(objects);
	}

	protected void fillContextMenu(IMenuManager mgr)
	{
		mgr.appendToGroup(IContextMenuConstants.GROUP_SHOW, fShowUsages);
		mgr.appendToGroup(IContextMenuConstants.GROUP_SHOW, fShowInNavigator);
		mgr.appendToGroup(IContextMenuConstants.GROUP_EDIT, copyAction);
	}

	protected void fillToolbar(IToolBarManager tbm)
	{
		super.fillToolbar(tbm);
		tbm.appendToGroup(IContextMenuConstants.GROUP_SHOW, fShowUsages);
	}

	protected void selectionChanged(SelectionChangedEvent event)
	{
		updateActions();
	}

	public void setViewPart(ISearchResultViewPart part)
	{
		super.setViewPart(part);

		fShowUsages = new ShowUsagesAction(getViewPart());
		fShowInNavigator = new ShowInNavigatorAction(getViewPart());
		
		copyAction = new Action()
		{
			public void run()
			{
				List<?> selectedEntries = getSelectedEntries();
				List<ModelObject> models = new ArrayList<ModelObject>();
				for (int i = 0; i < selectedEntries.size(); i++)
				{
					if (selectedEntries.get(i) instanceof TersusSearchModelEntry)
					{
						TersusSearchModelEntry entry = (TersusSearchModelEntry) selectedEntries
								.get(i);
						Model model = entry.getRepository().getModel(entry.getModelId(), true);
						models.add(model);
					}
				}
				Clipboard clipboard = new Clipboard(getSite().getShell().getDisplay());
				ModelObjectTransfer transfer = ModelObjectTransfer.getInstance();
				transfer.setObject(models);
				clipboard.setContents(new Object[]
				{ models }, new Transfer[]
				{ transfer });
				clipboard.dispose();
			}
		};
		copyAction.setText("Copy");
		copyAction.setToolTipText("Copy the selected model");
	}

	public List<?> getSelectedEntries()
	{
		ISelection selection = getViewer().getSelection();
		if (selection instanceof IStructuredSelection)
		{
			return ((IStructuredSelection) selection).toList();
		}
		else
			return Collections.EMPTY_LIST;
	}

	public void updateActions()
	{
		fShowUsages.update();
		fShowInNavigator.update();
	}

	public void dispose()
	{
		getViewer().removeSelectionChangedListener(selectionChangedListener);
		if (fShowUsages != null)
		{
			fShowUsages.dispose();
			fShowUsages = null;
		}
		if (fShowInNavigator != null)
		{
			fShowInNavigator.dispose();
			fShowInNavigator = null;
		}
		super.dispose();
	}

	public StructuredViewer getViewer()
	{
		return super.getViewer();
	}

	@Override
	protected void clear()
	{
		// TODO Auto-generated method stub
	}
}

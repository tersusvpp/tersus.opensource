/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.search;

import org.eclipse.core.resources.IProject;

import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.Repository;
import tersus.model.Role;

/**
 * @author Liat Shiff
 */
public class TersusSearchResultDescriptor
{
	private ModelId modelId;
	private Repository repository;
	private IProject project;
	private Role elementRole;

	public TersusSearchResultDescriptor(ModelId modelId, IProject project, Repository repository,
			Role elementRole)
	{
		this.modelId = modelId;
		this.project = project;
		this.repository = repository;
		this.elementRole = elementRole;
	}

	public Model getModel()
	{
		return repository.getModel(modelId, true);
	}

	public String getIconFolder()
	{
		return getModel().getIconFolder();
	}

	public Repository getRepository()
	{
		return repository;
	}

	public IProject getProject()
	{
		return project;
	}

	public ModelElement getElement()
	{
		if (getModel() != null)
			return getModel().getElement(elementRole);
		return null;
	}

	public ModelId getModelId()
	{
		return modelId;
	}
}

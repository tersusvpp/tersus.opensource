/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.search;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import tersus.editor.RepositoryManager;
import tersus.editor.TersusEditor;
import tersus.model.PackageId;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public class TersusSearchPackageEntry
{
	private static final ImageDescriptor PACKAGE_ICON_DESCRIPTOR = ImageDescriptor.createFromFile(
			TersusEditor.class, "icons/package 16x16.gif");

	private static final ImageDescriptor LIBRARY_PACKAGE_ICON_DESCRIPTOR = ImageDescriptor
			.createFromFile(TersusEditor.class, "icons/library package 16x16.gif");

	private PackageId packageId;

	private TersusSearchProjectEntry parentProject;

	private TersusSearchResult searchResult;

	public TersusSearchPackageEntry(PackageId packageId, TersusSearchProjectEntry projectEntry,
			TersusSearchResult searchResult)
	{
		this.packageId = packageId;
		this.parentProject = projectEntry;
		this.searchResult = searchResult;
	}

	public PackageId getPackageId()
	{
		return packageId;
	}

	public TersusSearchProjectEntry getParentProject()
	{
		return parentProject;
	}

	public TersusSearchResult getSearchResult()
	{
		return searchResult;
	}

	public Image getImage()
	{
		RepositoryManager manager = RepositoryManager.getRepositoryManager(parentProject
				.getProject());
		WorkspaceRepository workspaceRepository = manager.getRepository();

		if (workspaceRepository.isLibraryPackage(getPackageId()))
			return TersusWorkbench.getImage(LIBRARY_PACKAGE_ICON_DESCRIPTOR);
		else
			return TersusWorkbench.getImage(PACKAGE_ICON_DESCRIPTOR);
	}

	public IFile getFile()
	{
		RepositoryManager manager = RepositoryManager.getRepositoryManager(parentProject
				.getProject());
		WorkspaceRepository workspaceRepository = manager.getRepository();
		
		return workspaceRepository.getPackageFile(getPackageId());
	}
}

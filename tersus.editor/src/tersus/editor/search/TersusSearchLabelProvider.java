/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.search;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

/**
 * @author Liat Shiff
 */
public class TersusSearchLabelProvider extends LabelProvider
{
	public String getText(Object element)
	{
		if (element instanceof TersusSearchModelEntry)
			return ((TersusSearchModelEntry) element).getModelId().getName();

		else if (element instanceof TersusSearchPackageEntry)
			return ((TersusSearchPackageEntry) element).getPackageId().getName();

		else if (element instanceof TersusSearchProjectEntry)
			return ((TersusSearchProjectEntry) element).getProject().getName();

		if (element instanceof TersusSearchElementEntry)
		{
			return ((TersusSearchElementEntry) element).getElement().getElementName();
		}

		return super.getText(element);
	}

	@Override
	public Image getImage(Object element)
	{
		if (element instanceof TersusSearchModelEntry)
			return ((TersusSearchModelEntry) element).getImage();

		if (element instanceof TersusSearchPackageEntry)
			return ((TersusSearchPackageEntry) element).getImage();

		if (element instanceof TersusSearchProjectEntry)
			return ((TersusSearchProjectEntry) element).getImage();

		if (element instanceof TersusSearchElementEntry)
			return ((TersusSearchElementEntry) element).getImage();

		return null;
	}
}

package tersus.editor.search;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import tersus.editor.TersusEditor;
import tersus.model.Link;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.Repository;
import tersus.model.Role;
import tersus.workbench.WorkspaceRepository;

public class TersusSearchElementEntry implements IAdaptable
{
	private Role elementRole;

	private Repository repository;

	private TersusSearchModelEntry parentModelEntry;

	public TersusSearchElementEntry(TersusSearchModelEntry parentModelEntry, Role elementRole,
			Repository repository)
	{
		this.parentModelEntry = parentModelEntry;
		this.elementRole = elementRole;

		if (repository instanceof WorkspaceRepository)
			this.repository = (WorkspaceRepository) repository;
	}

	public Model getParentModel()
	{
		return repository.getModel(parentModelEntry.getModelId(), true);
	}

	public TersusSearchModelEntry getParentModelEntry()
	{
		return parentModelEntry;
	}

	public ModelElement getElement()
	{
		return getParentModel().getElement(elementRole);
	}

	public IProject getProject()
	{
		return parentModelEntry.getParentProject().getProject();
	}

	public TersusSearchResult getResultSet()
	{
		return parentModelEntry.getSearchResult();
	}

	public Image getImage()
	{
		if (repository != null && getElement() != null && getElement() instanceof Link)
		{
			ImageDescriptor descriptor = ImageDescriptor.createFromFile(TersusEditor.class,
					"icons/link.16.gif");
			return descriptor.createImage();
		}
		else
			return null;
	}


	public Object getAdapter(Class adapter)
	{
		return null;
	}
}

package tersus.editor.search;

import java.util.List;

import org.eclipse.swt.dnd.DragSourceEvent;

import tersus.editor.outline.ModelObjectTransfer;
import tersus.model.utils.DragListener;

public class TersusSearchDragListener extends DragListener
{
	private TersusSearchResultPage page;
	
	public TersusSearchDragListener(TersusSearchResultPage page)
	{
		this.page = page;
	}

	@Override
	public void dragStart(DragSourceEvent event) 
	{
		List entries = page.getSelectedEntries();
		
		ModelObjectTransfer.getInstance().setObject(entries);
	    event.doit = entries.size() > 0;
	}
}

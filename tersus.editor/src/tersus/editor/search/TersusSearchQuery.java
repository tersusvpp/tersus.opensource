/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.search;

import java.util.Collection;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.search.ui.ISearchQuery;
import org.eclipse.search.ui.ISearchResult;
import org.eclipse.search.ui.NewSearchUI;
import org.eclipse.search.ui.text.Match;

import tersus.editor.RepositoryManager;
import tersus.model.BuiltinPlugins;
import tersus.model.BuiltinProperties;
import tersus.model.DataType;
import tersus.model.FlowType;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.indexer.RepositoryIndex;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public class TersusSearchQuery implements ISearchQuery
{
	private String searchString;
	private boolean isCaseSensitive;
	private boolean isWholeWordSearch;
	private String projectToSearchIn;
	private int searchFor;
	private int totalMatches;

	private TersusSearchResult tSearchResult;

	public TersusSearchQuery(String searchString, boolean isCaseSensitive,
			boolean isWholeWordSearch, String projectToSearchInName, int searchFor)
	{
		this.searchString = searchString;
		this.isCaseSensitive = isCaseSensitive;
		this.isWholeWordSearch = isWholeWordSearch;
		this.projectToSearchIn = projectToSearchInName;
		this.searchFor = searchFor;

		totalMatches = 0;
	}

	public boolean canRerun()
	{
		return false;
	}

	public boolean canRunInBackground()
	{
		return true;
	}

	public String getLabel()
	{
		return "model " + searchString;
	}

	public ISearchResult getSearchResult()
	{
		if (tSearchResult == null)
		{
			tSearchResult = new TersusSearchResult(this);
		}
		return tSearchResult;
	}

	public IStatus run(IProgressMonitor monitor) throws OperationCanceledException
	{
		IWorkspace workspace = TersusWorkbench.getWorkspace();
		IProject[] projects = workspace.getRoot().getProjects();

		if (projects != null && projects.length != 0)
		{
			if (projectToSearchIn == null || projectToSearchIn.length() == 0)
				searchInWorkspace(projects, monitor);
			else
				searchInSpecificProject(projects, monitor);

		}
		MultiStatus status = new MultiStatus(NewSearchUI.PLUGIN_ID, IStatus.OK, "Alright", null);
		return status;
	}

	private void searchInSpecificProject(IProject[] projects, IProgressMonitor monitor)
	{
		monitor.beginTask("Searching in project...", projects.length);
		for (IProject project : projects)
		{
			if (project.getName().equals(projectToSearchIn))
			{
				RepositoryManager manager = RepositoryManager.getRepositoryManager(project);
				WorkspaceRepository workspaceRepository = manager.getRepository();
				RepositoryIndex index = workspaceRepository
						.getUpdatedRepositoryIndex(new NullProgressMonitor());
				Collection<ModelId> allModelIdCollection = index.getAllModelIds();

				if (isCaseSensitive && isWholeWordSearch)
					caseSensitiveAndWholeWordSearch(project, allModelIdCollection,
							workspaceRepository);

				else if (!isCaseSensitive && isWholeWordSearch)
					wholeWordMatchSearch(project, allModelIdCollection, workspaceRepository);

				else if (isCaseSensitive && !isWholeWordSearch)
					caseSensitiveSearch(project, allModelIdCollection, workspaceRepository);

				else
					simpleMatchSearch(project, allModelIdCollection, workspaceRepository);
			}
			monitor.worked(1);
		}
		monitor.done();
	}

	private void searchInWorkspace(IProject[] projects, IProgressMonitor monitor)
	{
		monitor.beginTask("Searching in workspace", projects.length);
		for (IProject project : projects)
		{
			if (project.isOpen())
			{
				monitor.subTask("Search in " + project.getName());
				RepositoryManager manager = RepositoryManager.getRepositoryManager(project);
				WorkspaceRepository workspaceRepository = manager.getRepository();
				RepositoryIndex index = workspaceRepository.getUpdatedRepositoryIndex();
				Collection<ModelId> allModelIdCollection = index.getAllModelIds();

				if (isCaseSensitive && isWholeWordSearch)
					caseSensitiveAndWholeWordSearch(project, allModelIdCollection, workspaceRepository);

				else if (!isCaseSensitive && isWholeWordSearch)
					wholeWordMatchSearch(project, allModelIdCollection, workspaceRepository);

				else if (isCaseSensitive && !isWholeWordSearch)
					caseSensitiveSearch(project, allModelIdCollection, workspaceRepository);

				else
					simpleMatchSearch(project, allModelIdCollection, workspaceRepository);

			}			
			monitor.worked(1);
		}
		monitor.done();
	}

	private void simpleMatchSearch(IProject project, Collection<ModelId> allModelIdCollection,
			WorkspaceRepository workspaceRepository)
	{
		String searchStringLowerCase = searchString.toLowerCase();
		for (ModelId modelId : allModelIdCollection)
		{
			String modelName = modelId.getName().toLowerCase();
			if (modelName.indexOf(searchStringLowerCase) > -1
					&& isSearchForType(modelId, workspaceRepository))
			{
				TersusSearchResultDescriptor descriptor = new TersusSearchResultDescriptor(modelId,
						project, workspaceRepository, null);
				tSearchResult.addMatch(new Match(descriptor, 0, 0));
				totalMatches++;
			}
		}
	}

	private void caseSensitiveAndWholeWordSearch(IProject project,
			Collection<ModelId> allModelIdCollection, WorkspaceRepository workspaceRepository)
	{
		for (ModelId modelId : allModelIdCollection)
		{
			if (modelId.getName().equals(searchString)
					&& isSearchForType(modelId, workspaceRepository))
			{
				TersusSearchResultDescriptor descriptor = new TersusSearchResultDescriptor(modelId,
						project, workspaceRepository, null);
				tSearchResult.addMatch(new Match(descriptor, 0, 0));
				totalMatches++;
			}
		}
	}

	private void caseSensitiveSearch(IProject project, Collection<ModelId> allModelIdCollection,
			WorkspaceRepository workspaceRepository)
	{
		for (ModelId modelId : allModelIdCollection)
		{
			if (modelId.getName().indexOf(searchString) > -1
					&& isSearchForType(modelId, workspaceRepository))
			{
				TersusSearchResultDescriptor descriptor = new TersusSearchResultDescriptor(modelId,
						project, workspaceRepository, null);
				tSearchResult.addMatch(new Match(descriptor, 0, 0));
				totalMatches++;
			}
		}
	}

	private void wholeWordMatchSearch(IProject project, Collection<ModelId> allModelIdCollection,
			WorkspaceRepository workspaceRepository)
	{
		String searchStringLowerCase = searchString.toLowerCase();

		for (ModelId modelId : allModelIdCollection)
		{
			String modelName = modelId.getName().toLowerCase();

			if (modelName.equals(searchStringLowerCase)
					&& isSearchForType(modelId, workspaceRepository))
			{
				TersusSearchResultDescriptor descriptor = new TersusSearchResultDescriptor(modelId,
						project, workspaceRepository, null);
				tSearchResult.addMatch(new Match(descriptor, 0, 0));
			}
		}
	}

	private boolean isSearchForType(ModelId modelId, WorkspaceRepository workspaceRepository)
	{
		Model model = workspaceRepository.getModel(modelId, true);

		Object typeObject = model.getProperty(BuiltinProperties.TYPE);
		if (typeObject instanceof FlowType)
		{
			FlowType type = (FlowType) typeObject;
			switch (searchFor)
			{
				case TersusSearchPage.SYSTEM_TYPE:
					return FlowType.SYSTEM.equals(type);

				case TersusSearchPage.ACTION_TYPE:
					return FlowType.ACTION.equals(type)
							&& BuiltinPlugins.ACTION.equals(model.getPlugin());

				case TersusSearchPage.SERVICE_TYPE:
					return FlowType.ACTION.equals(type)
							&& BuiltinPlugins.SERVICE.equals(model.getPlugin());

				case TersusSearchPage.DISPLAY_TYPE:
					return FlowType.DISPLAY.equals(type);

				case TersusSearchPage.ALL_TYPES:
					return true;

				default:
					return false;
			}
		}
		else if (model instanceof DataType
				&& (searchFor == TersusSearchPage.DATA_TYPE || searchFor == TersusSearchPage.ALL_TYPES))
			return true;

		return false;
	}

	public int getTotalMatches()
	{
		return totalMatches;
	}

	public String toString()
	{
		if (projectToSearchIn == null || projectToSearchIn.length() == 0)
			return "\"" + searchString + "\" - " + totalMatches + " matches in workspace.";
		else
			return "\"" + searchString + "\" - " + totalMatches + " matches in \""
					+ projectToSearchIn + "\" project.";
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.search;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.swt.graphics.Image;

import tersus.editor.ConcreteModelIdentifier;
import tersus.model.ModelId;
import tersus.model.ModelObject;
import tersus.model.Repository;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public class TersusSearchModelEntry implements IAdaptable
{
	private ModelId moedelId;

	private WorkspaceRepository repository;

	private String iconPath;

	private boolean isFlowModel;

	private TersusSearchPackageEntry parent;

	public TersusSearchModelEntry(ModelId moedelId,
			Repository repository, String iconPath, boolean isFlowModel,
			TersusSearchPackageEntry parent)
	{
		this.parent = parent;
		this.moedelId = moedelId;

		if (repository instanceof WorkspaceRepository)
			this.repository = (WorkspaceRepository) repository;

		this.iconPath = iconPath;
		this.isFlowModel = isFlowModel;
	}

	public ModelId getModelId()
	{
		return moedelId;
	}

	public TersusSearchPackageEntry getParent()
	{
		return parent;
	}

	public TersusSearchProjectEntry getParentProject()
	{
		return parent.getParentProject();
	}

	public TersusSearchResult getSearchResult()
	{
		return parent.getSearchResult();
	}

	public Image getImage()
	{
		if (iconPath != null && repository != null)
			return repository.getImageLocator(iconPath).get16x16Image();
		else
			return null;
	}

	public Repository getRepository()
	{
		return repository;
	}

	public Object getAdapter(Class adapter)
	{
		if (ModelObject.class.equals(adapter))
			return repository.getModel(getModelId(), true);

		else if (ConcreteModelIdentifier.class.equals(adapter))
			return new ConcreteModelIdentifier(getParentProject().getProject(), getModelId());

		else if (IProject.class.equals(adapter))
			return getParentProject().getProject();

		else
			return null;
	}

	public boolean isFlowModel()
	{
		return isFlowModel;
	}
}

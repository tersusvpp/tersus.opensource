/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;

import tersus.editor.RepositoryManager;
import tersus.model.BuiltinProperties;
import tersus.model.FlowModel;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Package;
import tersus.model.PackageId;
import tersus.model.Repository;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public class TersusSearchTreeContentProvider implements ITreeContentProvider
{
	private TreeViewer viewer;

	@SuppressWarnings("unused")
	private TersusSearchResult searchResult;

	public TersusSearchTreeContentProvider(TreeViewer viewer)
	{
		this.viewer = viewer;
	}

	public Object[] getChildren(Object parentElement)
	{
		ArrayList<Object> children = null;
		ArrayList<PackageId> relventSubPackages = new ArrayList<PackageId>();
		ArrayList<Model> relventSubModels = new ArrayList<Model>();

		if (parentElement instanceof TersusSearchPackageEntry)
		{
			children = new ArrayList<Object>();
			TersusSearchPackageEntry packageEntry = (TersusSearchPackageEntry) parentElement;
			IProject parentProject = packageEntry.getParentProject().getProject();
			PackageId packageId = packageEntry.getPackageId();

			for (Object obj : packageEntry.getSearchResult().getElements())
			{
				if (obj instanceof TersusSearchResultDescriptor)
				{
					TersusSearchResultDescriptor descriptor = (TersusSearchResultDescriptor) obj;
					ModelId modelIdToSearch = descriptor.getModelId();

					if (descriptor.getProject().equals(parentProject)
							&& packageId.isAncestorOf(modelIdToSearch))
					{
						RepositoryManager manager = RepositoryManager
								.getRepositoryManager(parentProject);
						WorkspaceRepository workspaceRepository = manager.getRepository();

						ArrayList<PackageId> subPackages = new ArrayList<PackageId>();
						subPackages.addAll(workspaceRepository.getSubPackageIds(packageId));

						for (PackageId subPackage : subPackages)
						{
							if (subPackage.isAncestorOf(modelIdToSearch)
									&& !relventSubPackages.contains(subPackage))
							{
								relventSubPackages.add(subPackage);
								children.add(new TersusSearchPackageEntry(subPackage, packageEntry
										.getParentProject(), packageEntry.getSearchResult()));
							}
						}

						ArrayList<Model> subModels = new ArrayList<Model>();
						Package pgk = workspaceRepository.getPackage(packageId);
						subModels.addAll(pgk.getModels());

						for (Model subModel : subModels)
						{
							if (subModel.getModelId().equals(modelIdToSearch)
									&& !relventSubModels.contains(subModel))
							{
								relventSubModels.add(subModel);
								children.add(new TersusSearchModelEntry(subModel.getModelId(),
												 (Repository) subModel.getRepository(), (String) subModel
												.getProperty(BuiltinProperties.ICON_FOLDER),
										subModel instanceof FlowModel, packageEntry));
							}
						}
					}
				}
			}
		}
		else if (parentElement instanceof TersusSearchModelEntry)
		{
			children = new ArrayList<Object>();

			TersusSearchModelEntry modelEntry = (TersusSearchModelEntry) parentElement;
			ModelId parentModelId = modelEntry.getModelId();

			for (Object obj : modelEntry.getSearchResult().getElements())
			{
				if (obj instanceof TersusSearchResultDescriptor)
				{
					TersusSearchResultDescriptor descriptor = (TersusSearchResultDescriptor) obj;
					if (descriptor.getElement() != null)
					{
						if (descriptor.getModel().getId().equals(parentModelId))
						{
							children.add(new TersusSearchElementEntry(modelEntry, descriptor
									.getElement().getRole(), descriptor.getRepository()));
						}
					}
				}
			}

		}
		else if (parentElement instanceof TersusSearchProjectEntry)
		{
			children = new ArrayList<Object>();

			TersusSearchProjectEntry projectEntry = (TersusSearchProjectEntry) parentElement;
			IProject parentProject = projectEntry.getProject();

			for (Object obj : projectEntry.getSearchResult().getElements())
			{
				if (obj instanceof TersusSearchResultDescriptor)
				{
					TersusSearchResultDescriptor descriptor = (TersusSearchResultDescriptor) obj;
					ModelId modelIdToSearch = descriptor.getModelId();

					if (parentProject.equals(descriptor.getProject()))
					{
						RepositoryManager manager = RepositoryManager
								.getRepositoryManager(parentProject);
						WorkspaceRepository workspaceRepository = manager.getRepository();

						ArrayList<PackageId> subPackages = new ArrayList<PackageId>();
						subPackages.addAll(workspaceRepository.getSubPackageIds(null));

						for (PackageId subPackage : subPackages)
						{
							if (subPackage.isAncestorOf(modelIdToSearch)
									&& !relventSubPackages.contains(subPackage))
							{
								relventSubPackages.add(subPackage);
								children.add(new TersusSearchPackageEntry(subPackage, projectEntry,
										projectEntry.getSearchResult()));

							}
						}
					}
				}
			}
		}

		if (children != null)
		{
			sort(children);
			return children.toArray();
		}
		return null;
	}

	private void sort(ArrayList<Object> objects)
	{
		Collections.sort(objects, new Comparator<Object>()
		{
			public int compare(Object o1, Object o2)
			{
				return getEntryName(o1).toLowerCase().compareTo(getEntryName(o2).toLowerCase());
			}

			private String getEntryName(Object obj)
			{
				if (obj instanceof TersusSearchProjectEntry)
					return ((TersusSearchProjectEntry) obj).getProject().getName();
				if (obj instanceof TersusSearchPackageEntry)
					return ((TersusSearchPackageEntry) obj).getPackageId().getName();
				if (obj instanceof TersusSearchModelEntry)
					return ((TersusSearchModelEntry) obj).getModelId().getName();

				return "";
			}
		});
	}

	public Object getParent(Object element)
	{
		if (element instanceof TersusSearchPackageEntry)
		{
			TersusSearchPackageEntry packageEntry = (TersusSearchPackageEntry) element;
			PackageId parentPackageId = packageEntry.getPackageId().getParent();

			if (parentPackageId == null || parentPackageId.equals(packageEntry.getPackageId()))
				return packageEntry.getParentProject();
			else
				return new TersusSearchPackageEntry(parentPackageId,
						packageEntry.getParentProject(), packageEntry.getSearchResult());
		}
		if (element instanceof TersusSearchModelEntry)
		{
			TersusSearchModelEntry modelEntry = (TersusSearchModelEntry) element;

			RepositoryManager manager = RepositoryManager.getRepositoryManager(modelEntry
					.getParentProject().getProject());
			WorkspaceRepository workspaceRepository = manager.getRepository();
			Model model = workspaceRepository.getModel(modelEntry.getModelId(), true);
			if (model != null)
				return new TersusSearchPackageEntry(model.getPackage().getPackageId(),
						modelEntry.getParentProject(), modelEntry.getSearchResult());
		}

		return null;
	}

	public boolean hasChildren(Object element)
	{
		if (element instanceof TersusSearchProjectEntry)
			return true;
		else if (element instanceof TersusSearchPackageEntry)
			return true;
		else if (element instanceof TersusSearchModelEntry)
		{
			TersusSearchModelEntry entry = (TersusSearchModelEntry) element;
			for (Object obj : entry.getSearchResult().getElements())
			{
				if (obj instanceof TersusSearchResultDescriptor)
				{
					TersusSearchResultDescriptor descriptor = (TersusSearchResultDescriptor) obj;
					if (descriptor.getElement() != null
							&& descriptor.getModel().getId().equals(entry.getModelId()))
						return true;
				}
			}
		}

		// element instance of TersusSearchModelEntry
		return false;
	}

	/**
	 * @param inputElement
	 *            - TersusSearchResult.
	 * @return The root elements (in our case, the projects names).
	 */
	public Object[] getElements(Object inputElement)
	{
		ArrayList<Object> elements = null;
		HashSet<IProject> projects = new HashSet<IProject>();

		if (inputElement instanceof TersusSearchResult)
		{
			elements = new ArrayList<Object>();
			projects = new HashSet<IProject>();

			TersusSearchResult searchResult = (TersusSearchResult) inputElement;
			Object[] objs = searchResult.getElements();

			for (Object obj : objs)
			{
				if (obj instanceof TersusSearchResultDescriptor)
				{
					TersusSearchResultDescriptor descriptor = (TersusSearchResultDescriptor) obj;
					IProject project = descriptor.getProject();

					if (!projects.contains(project))
					{
						elements.add(new TersusSearchProjectEntry(project, searchResult));
						projects.add(project);
					}
				}
			}
		}

		if (elements != null)
		{
			sort(elements);
			return elements.toArray();
		}
		else
			return null;
	}

	public void dispose()
	{
		// Do nothing
	}

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
	{
		if (newInput instanceof TersusSearchResult)
			searchResult = (TersusSearchResult) newInput;
	}

	public void elementsChanged(Object[] updatedElements)
	{
		viewer.refresh();
	}
}

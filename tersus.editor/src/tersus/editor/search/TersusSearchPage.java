/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.jface.dialogs.DialogPage;
import org.eclipse.search.internal.ui.SearchMessages;
import org.eclipse.search.ui.ISearchPage;
import org.eclipse.search.ui.ISearchPageContainer;
import org.eclipse.search.ui.NewSearchUI;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;

import tersus.editor.EditorPlugin;
import tersus.editor.TersusEditor;
import tersus.workbench.TersusWorkbench;

/**
 * @author Liat Shiff
 */
public class TersusSearchPage extends DialogPage implements ISearchPage
{
	private static final String PREFS_SEARCH_PAGE_SEARCH_HISTORY = "Search History";

	// Mode types
	public static final int ALL_TYPES = 0;

	public static final int SYSTEM_TYPE = 1;

	public static final int ACTION_TYPE = 2;

	public static final int SERVICE_TYPE = 3;

	public static final int DISPLAY_TYPE = 4;

	public static final int DATA_TYPE = 5;

	private boolean isCaseSensitive;

	private boolean isWholeWordSearch;

	private int searchFor;

	// Widgets
	private ISearchPageContainer container;
	
	private Combo searchPattern;

	private Button isCaseSensitiveCheckbox;

	private CLabel patternSearchInstructions;

	private Button isWholeWordSearchCheckbox;

	private Button workspaceScopeRadioButton;

	private Button projectScopeRadioButton;

	private Combo projectNameComboBox;

	private Button allRadioButton;

	private Button systemRadioButton;

	private Button actionRadioButton;

	private Button serviceRadioButton;

	private Button displayRadioButton;

	private Button dataRadioButton;

	public void createControl(Composite parent)
	{
		initializeDialogUnits(parent);

		Composite result = new Composite(parent, SWT.NONE);
		result.setFont(parent.getFont());
		GridLayout layout = new GridLayout(2, false);
		result.setLayout(layout);
		result.setLayoutData(new GridData(GridData.FILL_BOTH));

		addTextPatternControls(result);
		createSpaceLabel(result, 50);
		createSpaceLabel(result, 50);
		addSearchFor(result);
		addScope(result);

		setControl(result);
		updateOKStatus();
	}

	private void addTextPatternControls(Composite group)
	{
		// Info text
		Label label = new Label(group, SWT.LEAD);
		label.setText("Search model name:");
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		label.setFont(group.getFont());

		searchPattern = new Combo( group, SWT.DROP_DOWN | SWT.BORDER );
		searchPattern.setFont(group.getFont());
		searchPattern.setLayoutData( new GridData( SWT.FILL, SWT.NONE, true, false ) );

		isCaseSensitiveCheckbox = new Button(group, SWT.CHECK);
		isCaseSensitiveCheckbox.setText(SearchMessages.SearchPage_caseSensitive);
		isCaseSensitiveCheckbox.setSelection(false);
		isCaseSensitiveCheckbox.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				isCaseSensitive = isCaseSensitiveCheckbox.getSelection();
			}
		});
		isCaseSensitiveCheckbox
				.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		isCaseSensitiveCheckbox.setFont(group.getFont());

		patternSearchInstructions = new CLabel(group, SWT.LEAD);
		patternSearchInstructions.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1,
				1));
		patternSearchInstructions.setFont(group.getFont());
		patternSearchInstructions.setAlignment(SWT.LEFT);

		isWholeWordSearchCheckbox = new Button(group, SWT.CHECK);
		isWholeWordSearchCheckbox.setText("&Whole model name");
		isWholeWordSearchCheckbox.setSelection(false);
		isWholeWordSearchCheckbox.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				isWholeWordSearch = isWholeWordSearchCheckbox.getSelection();
			}
		});
		isWholeWordSearchCheckbox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1,
				1));
		isWholeWordSearchCheckbox.setFont(group.getFont());

		initSearchStringHistory();
		searchPattern.setFocus();
	}
	
	private void initSearchStringHistory()
    {
		searchPattern.setItems( loadSearchStringHistory() );
    }

	private void addScope(Composite parent)
	{
		Group result = new Group(parent, SWT.NONE | parent.getStyle());

		result.setText("Scope");
		GridLayout layout = new GridLayout(2, true);
		layout.horizontalSpacing = 25;
		layout.verticalSpacing = 8;
		result.setLayout(layout);

		GridData data = new GridData(SWT.LEFT, SWT.BEGINNING, false, false, 1, 1);
		result.setData(data);

		GridData gridData = new GridData(GridData.FILL_BOTH);
		gridData.minimumHeight = 100;
		result.setLayoutData(gridData);

		workspaceScopeRadioButton = new Button(result, SWT.RADIO);
		createSpaceLabel(result, -1);
		projectScopeRadioButton = new Button(result, SWT.RADIO);
		projectNameComboBox = new Combo(result, SWT.DROP_DOWN | SWT.READ_ONLY);

		workspaceScopeRadioButton.setText("Work&space");
		workspaceScopeRadioButton.setSelection(false);
		workspaceScopeRadioButton.setLayoutData(new GridData());

		workspaceScopeRadioButton.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				updateOKStatus();
				projectNameComboBox.setEnabled(false);
			}
		});

		createSpaceLabel(result, -1);

		projectScopeRadioButton.setText("&Project name: ");
		projectScopeRadioButton.setSelection(true);
		projectScopeRadioButton.setLayoutData(new GridData());

		projectScopeRadioButton.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				updateOKStatus();
				projectNameComboBox.setEnabled(true);
			}
		});

		projectNameComboBox.setFont(result.getFont());
		projectNameComboBox.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				updateOKStatus();
			}
		});

		initializeScopeGroup();
	}

	private void createSpaceLabel(Composite composite, int minimumHeight)
	{
		CLabel space = new CLabel(composite, SWT.LEAD);
		space.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		space.setFont(composite.getFont());
		space.setAlignment(SWT.LEFT);

		if (minimumHeight > -1)
		{
			GridData gridData = new GridData(GridData.FILL_BOTH);
			gridData.minimumHeight = minimumHeight;
			space.setLayoutData(gridData);
		}

	}

	private void initializeScopeGroup()
	{
		IWorkbenchPage page = TersusWorkbench.getActiveWorkbenchWindow().getActivePage();
		IEditorPart activeEditor = page.getActiveEditor();

		IWorkspace workspace = TersusWorkbench.getWorkspace();
		IProject[] projects = workspace.getRoot().getProjects();

		ArrayList<String> projectNameItems = new ArrayList<String>();
		for (IProject project : projects)
		{
			if (project.isOpen())
				projectNameItems.add(project.getName());
		}

		projectNameItems.add("");
		projectNameComboBox.setItems(projectNameItems.toArray(new String[projectNameItems.size()]));

		if (activeEditor instanceof TersusEditor)
		{
			TersusEditor tersusEditor = (TersusEditor) activeEditor;
			projectScopeRadioButton.setSelection(true);
			projectNameComboBox.setText(tersusEditor.getRepository().getProject().getName());
			workspaceScopeRadioButton.setSelection(false);
		}
		else
		{
			workspaceScopeRadioButton.setSelection(false);
			projectScopeRadioButton.setSelection(true);
			projectNameComboBox.setEnabled(true);
		}

	}

	private void addSearchFor(Composite parent)
	{
		Group result = new Group(parent, SWT.NONE | parent.getStyle());
		result.setText("Search for");

		GridLayout layout = new GridLayout(3, false);
		layout.horizontalSpacing = 25;
		layout.verticalSpacing = 8;
		result.setLayout(layout);

		GridData data = new GridData(SWT.LEFT, SWT.BEGINNING, false, false, 1, 1);
		result.setData(data);

		GridData gridData = new GridData(GridData.FILL_BOTH);
		gridData.minimumHeight = 100;
		result.setLayoutData(gridData);

		allRadioButton = createRadioButton(result, "A&ll");
		systemRadioButton = createRadioButton(result, "&System");
		actionRadioButton = createRadioButton(result, "A&ction");
		serviceRadioButton = createRadioButton(result, "Ser&vice");
		displayRadioButton = createRadioButton(result, "&Display");
		dataRadioButton = createRadioButton(result, "D&ata");

		initializeSearchForGroup();
	}

	private void initializeSearchForGroup()
	{
		allRadioButton.setSelection(true);

		allRadioButton.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				searchFor = ALL_TYPES;
			}
		});

		systemRadioButton.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				searchFor = SYSTEM_TYPE;
			}
		});

		actionRadioButton.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				searchFor = ACTION_TYPE;
			}
		});

		serviceRadioButton.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				searchFor = SERVICE_TYPE;
			}
		});

		displayRadioButton.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				searchFor = DISPLAY_TYPE;
			}
		});

		dataRadioButton.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				searchFor = DATA_TYPE;
			}
		});

		searchFor = ALL_TYPES;
	}

	private Button createRadioButton(Group group, String text)
	{
		Button button = new Button(group, SWT.RADIO | SWT.MULTI);
		button.setText(text);
		button.setSelection(false);
		button.setLayoutData(new GridData(SWT.LEFT, SWT.BEGINNING, true, true, 1, 1));

		return button;
	}

	final void updateOKStatus()
	{

		getContainer().setPerformActionEnabled(checkEnabled());
	}

	protected boolean checkEnabled()
	{
		return workspaceScopeRadioButton.getSelection()
				|| (projectNameComboBox.getText() != null && projectNameComboBox.getText().length() >0);
	}

	/**
	 * Sets the search page's container.
	 * 
	 * @param container
	 *            the container to set
	 */
	public void setContainer(ISearchPageContainer container)
	{
		this.container = container;
	}

	private ISearchPageContainer getContainer()
	{
		return container;
	}

	public boolean performAction()
	{
		if (!checkEnabled())
			return false;
		NewSearchUI.runQueryInForeground(null, new TersusSearchQuery(searchPattern.getText(),
				isCaseSensitive, isWholeWordSearch,
				projectScopeRadioButton.getSelection() ? projectNameComboBox.getText() : "",
				searchFor));
		
		addSearchStringHistory(searchPattern.getText());
		return true;
	}

	public static String[] loadSearchStringHistory()
	{
		String[] history = EditorPlugin.getDefault().getDialogSettings().getArray(
				PREFS_SEARCH_PAGE_SEARCH_HISTORY);

		if (history == null)
			history = new String[0];

		return history;
	}

	public static void addSearchStringHistory(String value)
	{
		// get current history
		String[] history = loadSearchStringHistory();
		List<String> list = new ArrayList<String>(Arrays.asList(history));

		// add new value or move to first position
		if (list.contains(value))
			list.remove(value);

		list.add(0, value);

		// check history size
		while (list.size() > 10)
		{
			list.remove(list.size() - 1);
		}

		// save
		history = (String[]) list.toArray(new String[list.size()]);
		EditorPlugin.getDefault().getDialogSettings()
				.put(PREFS_SEARCH_PAGE_SEARCH_HISTORY, history);
	}

	public static void removeSearchStringHistory(String value)
	{
		// get current history
		String[] history = loadSearchStringHistory();
		List<String> list = new ArrayList<String>(Arrays.asList(history));

		// add new value or move to first position
		if (list.contains(value))
		{
			list.remove(value);
		}

		// save
		history = (String[]) list.toArray(new String[list.size()]);
		EditorPlugin.getDefault().getDialogSettings()
				.put(PREFS_SEARCH_PAGE_SEARCH_HISTORY, history);
	}

	public static void clearSearchHistory()
	{
		EditorPlugin.getDefault().getDialogSettings().put(PREFS_SEARCH_PAGE_SEARCH_HISTORY,
				new String[0]);
	}
}

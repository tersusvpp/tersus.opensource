/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.search;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.search.ui.ISearchQuery;
import org.eclipse.search.ui.text.AbstractTextSearchResult;
import org.eclipse.search.ui.text.IEditorMatchAdapter;
import org.eclipse.search.ui.text.IFileMatchAdapter;

/**
 * @author Liat Shiff
 */
public class TersusSearchResult extends AbstractTextSearchResult
{
	private ISearchQuery query;

	public TersusSearchResult(ISearchQuery query)
	{
		this.query = query;
	}

	@Override
	public IEditorMatchAdapter getEditorMatchAdapter()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IFileMatchAdapter getFileMatchAdapter()
	{
		// TODO Auto-generated method stub
		return null;
	}

	public ImageDescriptor getImageDescriptor()
	{
		// TODO Auto-generated method stub
		return null;
	}

	public String getLabel()
	{
		return query.toString();
	}

	public ISearchQuery getQuery()
	{
		return query;
	}

	public String getTooltip()
	{
		// TODO Auto-generated method stub
		return null;
	}

	public int getMatchCount(Object element)
	{
		int matchCounter = 0;
		Object[] matches = getElements();

		if (element instanceof TersusSearchModelEntry)
		{
			TersusSearchModelEntry modelEntry = (TersusSearchModelEntry) element;
			for (Object obj : matches)
			{
				if (obj instanceof TersusSearchResultDescriptor)
				{
					TersusSearchResultDescriptor descriptor = (TersusSearchResultDescriptor) obj;

					if (element instanceof TersusSearchModelEntry && descriptor.getModel() != null
							&& modelEntry.getModelId().equals(descriptor.getModel().getModelId()))
						matchCounter++;
				}
			}
		}
		return matchCounter;
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
package tersus.editor;

import org.eclipse.core.resources.IProject;
import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.ViewPart;

import tersus.editor.editparts.TersusEditPart;
import tersus.editor.editparts.TopFlowEditPart;
import tersus.model.DataElement;
import tersus.model.FlowModel;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.Path;
import tersus.model.Role;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

public abstract class SyncableView extends ViewPart
{

	private TersusEditor editor;
	public TersusEditPart zoomed;

	protected TersusEditor getEditor()
	{
		return editor;
	}

	protected void setEditor(TersusEditor editor)
	{
		this.editor = editor;
	}

	/**
     *  
     */
	protected void switchEditor(ConcreteModelIdentifier editorInput)
	{
		try
		{
			IEditorPart newEditor = getViewSite().getPage().openEditor(
					editorInput, TersusEditor.ID);
			setEditor((TersusEditor) newEditor);
		}
		catch (PartInitException e)
		{
			TersusWorkbench.log(e);
		}
		getViewSite().getPage().activate(this);
	}

	protected ConcreteModelIdentifier getEditorInput(Model model)
	{
		IProject project = ((WorkspaceRepository) model.getRepository()).getProject();
		ConcreteModelIdentifier editorInput = new ConcreteModelIdentifier(project, model.getId());
		return editorInput;
	}
	protected void showAndHighlight(final Model root, final Path path)
	{
		// If there are display data elements in the path, we limit the context to them because we
		// can't zoom into display models when displayed as data
		Model m = root;
		for (int i = 0; i < path.getNumberOfSegments() - 1; i++)
		{
			Role role = path.getSegment(i);
			ModelElement e = m.getElement(role);
			if (e instanceof DataElement && e.getReferredModel() instanceof FlowModel)
			{
				showAndHighlight(e.getReferredModel(), path.getTail(path.getNumberOfSegments() - 1
						- i));
				return;
			}
			m = e.getReferredModel();
		}
		ConcreteModelIdentifier editorInput = getEditorInput(root);

		Control editorControl = null;
		if (getEditor()!= null)
				editorControl = getEditor().getViewer().getControl();
		if (getEditor() == null || !editorInput.equals(getEditor().getEditorInput())
				|| getEditor().getViewer() == null || editorControl == null
				|| !editorControl.isEnabled())
		{
			switchEditor(editorInput);
			Display.getCurrent().asyncExec(new Runnable()
			{
				public void run()
				{
					showAndHighlight(root, path);
				}
			});
			return;
		}
		getSite().getPage().activate(getEditor());
		TersusEditPart element = getEditor().getTopFlowEditPart().getChild(path, true);

		TersusEditPart parent = (element == null || element instanceof TopFlowEditPart) ? null
				: (TersusEditPart) element.getParent();
		if (parent != zoomed && parent != null)
		{
			parent.zoomToPart();
			zoomed = parent;
		}
		getEditor().highlight(path);
	}
}

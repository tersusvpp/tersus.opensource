/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.requests;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.DirectEditRequest;

/**
 * @author Youval Bronicki
 *
 */
public class EditInitialNameRequest extends DirectEditRequest
{
    private Command initialCreationCommand;

    /**
     * @return Returns the dummyModelCommand.
     */
    public Command getInitialCreationCommand()
    {
        return initialCreationCommand;
    }
    /**
     * @param dummyModelCommand The dummyModelCommand to set.
     */
    public void setInitialCreationCommand(Command dummyModelCommand)
    {
        this.initialCreationCommand = dummyModelCommand;
    }
}

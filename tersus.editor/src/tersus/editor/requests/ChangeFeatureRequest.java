/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.requests;

import org.eclipse.gef.Request;

import tersus.editor.editparts.TersusDirectEditFeature;

/**
 * A request to change the role of a model element.
 * 
 * This request is handeled by ElementEditPolicy.getChangeRoleCommand().
 * 
 * @author Ofer Brandes
 *
 */
public class ChangeFeatureRequest extends Request
{
	private TersusDirectEditFeature feature;
	private Object value;

	/**
	 * 
	 * Create a ChangeRoleRequest instance, given a suggested new role.
	 * 
	 * @param newRole The new role suggested for the model element (a string).
	 */	 
	public ChangeFeatureRequest (TersusDirectEditFeature feature, Object value)
	{
		this.feature = feature;
		this.value = value;
	}
	
	/**
	 * @return
	 */
	public TersusDirectEditFeature getFeature()
	{
		return feature;
	}

	/**
	 * @return
	 */
	public Object getValue()
	{
		return value;
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.requests;

import org.eclipse.gef.Request;

/**
 * A request to rename a model (never used?).
 * 
 * This request is handeled by ElementEditPolicy.getLocalRenameCommand().
 * 
 * @author Ofer Brandes
 *
 */
public class RenameRequest extends Request
{
	private String newName = null;

	/**
	 * 
	 * Create a RenameRequest instance, given a suggested new name.
	 * 
	 * @param newName The new name suggested for the model (a string).
	 */	 
	public RenameRequest (String newName)
	{
		this.newName = newName;
	}
	
	/**
	 * 
	 * Get the suggested new name in the request.
	 * 
	 */
	public String getNewName()
	{
		return newName;
	}
}

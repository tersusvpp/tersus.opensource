/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.requests;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.requests.LocationRequest;

import tersus.editor.Debug;
import tersus.editor.editparts.FlowEditPart;
import tersus.editor.editparts.TersusEditPart;
import tersus.editor.figures.FlowRectangle;
import tersus.editor.figures.PrecisionRectangle;
import tersus.editor.layout.LayoutConstraint;
import tersus.model.Composition;
import tersus.model.DataType;
import tersus.model.FlowModel;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.ModelObject;
import tersus.util.Trace;
import tersus.workbench.WorkspaceRepository;

/**
 * 
 */
public class DropModelObjectRequest extends LocationRequest
{

	private ModelObject sourceObject;
	private Dimension size;
    private LayoutConstraint constraint;
	public DropModelObjectRequest()
	{
	}
	public DropModelObjectRequest(ModelObject sourceObject, Point location)
	{
		setLocation(location);
		setSourceObject(sourceObject);
	}
	
	/**
	 * @return
	 */

	/**
	 * @return
	 */
	public ModelObject getSourceObject()
	{
		return sourceObject;
	}

	/**
	 * @param object
	 */
	public void setSourceObject(ModelObject object)
	{
		sourceObject = object;
	}

	/**
	 * @return
	 */
	public Dimension getSize()
	{
		return size;
	}

	/**
	 * @param dimension
	 */
	public void setSize(Dimension dimension)
	{
		size = dimension;
	}

	public void setDefaultSize(TersusEditPart targetEditPart)
	{
		IFigure targetFigure = targetEditPart.getFigure();
		if (targetFigure instanceof FlowRectangle)
		{
			Dimension size = new Dimension();
			Rectangle frame = ((FlowRectangle) targetFigure).getFrame().getCopy();
			Point location = getLocation().getCopy();
			targetFigure.translateToRelative(location);
			double widthProportion = 0.5;
			if (sourceObject instanceof DataType
				&& ((DataType) sourceObject).getComposition() == Composition.ATOMIC)
				widthProportion = 6;
			if (sourceObject instanceof FlowModel)
				widthProportion = ((FlowModel) sourceObject).getWidth();
			int maxHeight = frame.height / 2;
			int maxWidth = frame.width / 4;
			if (location.x + maxWidth > frame.right())
			    maxWidth = frame.right()- location.x;
			if (location.y + maxHeight > frame.bottom())
			    maxHeight = frame.bottom()- location.y;
			if (maxWidth >= maxHeight * widthProportion)
			{
				size.height = maxHeight;
				size.width = (int) (maxHeight * widthProportion);
			}
			else
			{
				size.height = (int) (maxWidth / widthProportion);
				size.width = maxWidth;
			}
			if (size.width == 0)
			    size.width =1;
			if (size.height == 0)
			    size.height = 1;
			setSize(size);

		}
		
	}
	public void updateConstraint(TersusEditPart targetEditPart)
	{
		ModelObject sourceObject = getSourceObject();
		FlowEditPart parent = (FlowEditPart) targetEditPart;
		FlowRectangle parentFigure = parent.getFlowRectangle();
		PrecisionRectangle parentFrame = parentFigure.getFrame();
		Point location = getLocation().getCopy();
		parentFigure.translateToRelative(location);
		double centerX = (location.x - parentFrame.preciseX) / parentFrame.preciseWidth;
		double centerY = (location.y - parentFrame.preciseY) / parentFrame.preciseHeight;
		double width = 0.25;
		double height = 0.25;
		Dimension size = getSize();
		if (size != null)
		{
			centerX = (location.x - parentFrame.preciseX + size.width / 2) / parentFrame.preciseWidth;
			centerY = (location.y - parentFrame.preciseY + size.height / 2) / parentFrame.preciseHeight;
			height = size.height / parentFrame.preciseHeight;
			width = size.width / parentFrame.preciseWidth;
			if (Debug.CREATE)
			{
				Trace.push("getAddElementCommand(): calculating constraint");
				Trace.add("source object=" + sourceObject);
				Trace.add("absolute location=" + getLocation());
				Trace.add("absolute size = " + size);
				Trace.add("relative size = " + width + "," + height);
				Trace.add("parentFrame=" + parentFrame);
				Trace.add("relative location=" + location + " relativePosition=" + centerX + "," + centerY);
				Trace.pop();
			}
		}
		if (sourceObject instanceof Model)
		{
			Model addedModel = (Model) sourceObject;
			ModelId addedModelId = addedModel.getId();
			FlowModel parentModel = parent.getFlowModel();
			WorkspaceRepository repository = (WorkspaceRepository) parentModel.getRepository();

			constraint = new LayoutConstraint(centerX, centerY, width, height);
			constraint = FlowEditPart.adjustConstraint(constraint);
			if (constraint == null)
			{
			    	constraint = null;
			    	return;
			}
		}
	    
	}
    /**
     * @return
     */
    public boolean isValid()
    {
        return constraint != null;
    }
}

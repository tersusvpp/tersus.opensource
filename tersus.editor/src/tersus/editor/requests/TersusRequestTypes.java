/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.requests;

/**
 * IDs of TersusRequests
 * 
 * @author Youval Bronicki
 * @author Ofer Brandes
 *
 */
public interface TersusRequestTypes
{
	static final String EDIT_ROLE="terus.editor.edit_role";
	static final String CHANGE_ROLE="tersus.editor.change_role";
}

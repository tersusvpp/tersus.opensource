/************************************************************************************************
 * Copyright (c) 2003-2021 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.Path;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IEditorPart;

import tersus.editor.preferences.Preferences;
import tersus.editor.validation.ValidationEngine;
import tersus.model.Model;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 */
public class RepositoryManager
{
	private CommandStack commandStack = new CommandStack();

	private boolean checkedForUpgrade = false;

	public CommandStack getCommandStack()
	{
		return commandStack;
	}

	private ArrayList<TersusEditor> editors = new ArrayList<TersusEditor>();

	public static Collection<RepositoryManager> getActiveManagers()
	{
		return managerMap.values();
	}

	public static RepositoryManager getRepositoryManager(IProject project)
	{
		if (!managerMap.containsKey(project))
		{
			managerMap.put(project, new RepositoryManager(project));
			project.getWorkspace()
					.addResourceChangeListener(RepositoryChangeListener.getInstance());
		}
		return (RepositoryManager) managerMap.get(project);
	}

	private static HashMap<IProject, RepositoryManager> managerMap = new HashMap<IProject, RepositoryManager>();

	private WorkspaceRepository repository;

	private ValidationEngine validationEngine;

	private IProject project;

	/**
	 * @param editorPart
	 */
	private RepositoryManager(IProject project)
	{
		this.project = project;
		repository = WorkspaceRepository.getRepository(project);
		validationEngine = new ValidationEngine(repository);
	}

	public void addEditor(TersusEditor editor)
	{
		editors.add(editor);
	}

	public void removeEditor(TersusEditor editor)
	{
		editors.remove(editor);
		// Revert to saved
		if (editors.size() == 0)
		{
			repository.clearCache();
			getCommandStack().flush();
		}
	}

	public Collection<TersusEditor> getActiveEditors()
	{
		return editors;
	}

	@SuppressWarnings("unchecked")
	public Collection<TersusEditor> getActiveEditorsClone()
	{
		return (Collection<TersusEditor>) editors.clone();
	}

	public WorkspaceRepository getRepository()
	{
		return repository;
	}

	public List<IEditorPart> getOpenEditors()
	{
		return new ArrayList<IEditorPart>(editors);
	}

	public int getNumberOfEditors()
	{
		return editors.size();
	}

	public void save()
	{
		if (!checkFileSystemSync())
			return;
		if (!checkLongPackagePaths())
			return;
		if (!checkReadOnlyPackageExists())
			return;
		final RepositoryChangeListener changeListener = RepositoryChangeListener.getInstance();
		changeListener.setDisabled(true);
		if (getRepository().anyDatabaseRecordsModified())
			touchTimestamp(".timestamp.db", "Last database modification:");
		getRepository().saveAllModifiedPackages(new Runnable()
		{
			public void run()
			{
				synchronized (changeListener)
				{
					changeListener.setLastSave(System.currentTimeMillis());
					changeListener.setDisabled(false);
				}
				touchTimestamp(".timestamp", "Last save:");
				getCommandStack().markSaveLocation();
				if (Preferences.getBoolean(Preferences.VALIDATE_ON_SAVE))
					validationEngine.validate(true/* run in the background */);
			}
		});

	}

	private static final int NEW = 1;
	@SuppressWarnings("unused")
	private static final int MODIFIED = 2;
	private static final int DELETED = 3;

	private class ResourceInfo
	{

		IResource resource;
		int status;

		public ResourceInfo(IFile resource, int status)
		{
			this.resource = resource;
			this.status = status;
		}

		public ResourceInfo(IFolder folder)
		{
			super();
			this.resource = folder;
		}
	}

	private boolean checkFileSystemSync()
	{
		ArrayList<ResourceInfo> relevantResources = new ArrayList<ResourceInfo>();
		ArrayList<ResourceInfo> outOfSyncResources = new ArrayList<ResourceInfo>();
		for (tersus.model.Package pkg : getRepository().getModifiedPackages())
		{
			addRelevantResources(pkg, relevantResources);
		}
		for (tersus.model.Package pkg : getRepository().getDeletedPackages())
		{
			addRelevantResources(pkg, relevantResources);
		}
		StringBuffer outOfSyncMessage = new StringBuffer();
		for (ResourceInfo resInfo : relevantResources)
		{
			IResource res = resInfo.resource;

			if (resInfo.status == NEW && res.getLocation().toFile().exists()
					&& !isResourceDeletedFirst(relevantResources, resInfo))
			{
				addOutOfSyncResource(outOfSyncResources, outOfSyncMessage, resInfo, res,
						"Conflicting file added");
			}
			else if (!res.isSynchronized(IResource.DEPTH_ZERO))
			{
				addOutOfSyncResource(outOfSyncResources, outOfSyncMessage, resInfo, res,
						"Out of sync");
			}
		}
		if (!outOfSyncResources.isEmpty())
		{
			MessageDialog.openInformation(TersusWorkbench.getActiveWorkbenchShell(), "Out of sync",
					EditorMessages.FILES_OUT_OF_SYNC + "\n" + outOfSyncMessage);
			return false;
		}
		return true;
	}

	private boolean isResourceDeletedFirst(ArrayList<ResourceInfo> relevantResources,
			ResourceInfo resInfoToCheck)
	{
		for (ResourceInfo t : relevantResources)
		{
			if (t != resInfoToCheck && resInfoToCheck.resource.equals(t.resource)
					&& t.status == DELETED)
				return true;
		}

		return false;
	}

	private void addOutOfSyncResource(ArrayList<ResourceInfo> outOfSyncResources,
			StringBuffer outOfSyncMessage, ResourceInfo resInfo, IResource res, String code)
	{
		outOfSyncResources.add(resInfo);
		outOfSyncMessage.append('\n');
		outOfSyncMessage.append(outOfSyncResources.size());
		outOfSyncMessage.append("\t(" + code + "): ");
		outOfSyncMessage.append(res.getFullPath().toString());
		outOfSyncMessage.append('\n');
	}

	private void addRelevantResources(tersus.model.Package pkg,
			Collection<ResourceInfo> relevantResources)
	{
		if (!pkg.isDeleted())
			relevantResources.add(new ResourceInfo(getRepository().getPackageFolder(
					pkg.getPackageId())));
		if (pkg.isNew() || pkg.isMoved())
			relevantResources.add(new ResourceInfo(getRepository().getPackageFile(
					pkg.getPackageId()), NEW));
		if (pkg.isMoved() || pkg.isDeleted())
		{
			relevantResources.add(new ResourceInfo(getRepository().getPackageFile(
					pkg.getSavedPackageId()), DELETED));
			relevantResources.add(new ResourceInfo(getRepository().getPackageFolder(
					pkg.getSavedPackageId())));
			for (IFile originalFile : getRepository().getFiles(pkg.getPackageId()))
			{
				relevantResources.add(new ResourceInfo(originalFile, DELETED));
				if (pkg.isMoved())
				{
					IFile newFile = getRepository().getPackageFolder(pkg.getPackageId()).getFile(
							new Path(originalFile.getName()));
					relevantResources.add(new ResourceInfo(newFile, NEW));
				}
			}
		}
	}

	private boolean checkLongPackagePaths()
	{
		int MAX_PATH_LENGTH = 255;
		int RESERVED_LENGTH = 19; // For subversion (e.g
		// .svn/text-base/<Name>.svn )
		int basePathLength = getRepository().getRepositoryRoot().getLocation().toFile()
				.getAbsolutePath().length() + 1;
		int maxLength = MAX_PATH_LENGTH - RESERVED_LENGTH - basePathLength;
		StringBuffer longPackages = new StringBuffer();
		int deepPackagesCount = 0;
		for (tersus.model.Package pkg : getRepository().getModifiedPackages())
		{
			String path = pkg.getPackageId().getPath();
			if (path.length() > maxLength)
			{
				if (longPackages.length() > 0)
					longPackages.append("\n\n");
				longPackages.append(++deepPackagesCount);
				longPackages.append(". ");
				longPackages.append(path);
			}

		}
		if (longPackages.length() > 0)
		{
			MessageDialog
					.openError(
							TersusWorkbench.getActiveWorkbenchShell(),
							"Deep Packages",
							"The packages listed below have very long paths. To save your changes, you should rename or move these packages.\n\n"
									+ longPackages);
			return false;

		}
		else
			return true;
	}

	private boolean checkReadOnlyPackageExists()
	{
		StringBuffer readOnlyPackages = new StringBuffer();
		int readOnlyPackagesCount = 0;

		for (tersus.model.Package pkg : getRepository().getModifiedPackages())
		{
			if (pkg.isReadOnly())
			{
				if (readOnlyPackages.length() > 0)
					readOnlyPackages.append("\n\n");
				readOnlyPackages.append(++readOnlyPackagesCount);
				readOnlyPackages.append(". ");
				readOnlyPackages.append(pkg.getPackageId().getPath());
			}
		}
		if (readOnlyPackages.length() > 0)
		{
			MessageDialog.openError(TersusWorkbench.getActiveWorkbenchShell(), "Read Only",
					"Cannot save your changes because the modified packages listed below are read only.\n\n"
							+ readOnlyPackages);
			return false;

		}
		else
			return true;
	}

	private void touchTimestamp(String fileName, String label)
	{
		IFile file = getProject().getFile(fileName);
		ByteArrayInputStream content = new ByteArrayInputStream((label + new Date()).getBytes());
		try
		{
			if (file.exists())
				file.setContents(content, true, false, null);
			else
				file.create(content, true, null);
		}
		catch (Exception e)
		{
			TersusWorkbench.warn("Failed to update timestamp file " + file.getFullPath(), e);
		}
	}

	public IProject getProject()
	{
		return project;
	}

	public static void refreshAll()
	{
		for (RepositoryManager manager : managerMap.values())
		{
			manager.refresh();
		}
	}

	public void refresh()
	{
		if (!getCommandStack().isDirty())
		{
			revert();
		}
	}

	public void revert()
	{
		try
		{
			getRepository().refreshLibraries();
			getRepository().clearCache();
			getCommandStack().flush();
			checkedForUpgrade = false;
			for (TersusEditor editor : getActiveEditorsClone())
			{
				editor.refresh();
			}
		}
		catch (RuntimeException e)
		{
			TersusWorkbench.error("An error occured when refreshing project "
					+ getProject().getName() + "\n See the log file for details.", e);
		}
	}

	public ValidationEngine getValidationEngine()
	{
		return validationEngine;
	}

	public void dispose()
	{
		if (getActiveEditors().isEmpty() && getRepository().getListeners().isEmpty())
		{
			getRepository().removeProjectFromRepositories(project);
			getRepository().dispose();
			managerMap.remove(project);
		}
//		else
//		throw new IllegalStateException("Repository is in use");
	}

	public void closeOpenEditors()
	{
		ArrayList<IEditorPart> editors = new ArrayList<IEditorPart>();
		editors.addAll(getActiveEditors());
		for (IEditorPart editor : editors)
		{
			editor.getSite().getPage().closeEditor(editor, false);
		}
	}

	public void closeOpenEditors(Model model)
	{
		ArrayList<IEditorPart> editors = new ArrayList<IEditorPart>();
		editors.addAll(getActiveEditors());

		for (IEditorPart editor : editors)
		{
			if (editor instanceof TersusEditor
					&& ((TersusEditor) editor).getModelWrapper().getModel().equals(model))
				editor.getSite().getPage().closeEditor(editor, false);

		}
	}

	public IFolder getLibrariesFolder()
	{
		return project.getFolder("libraries");
	}
}
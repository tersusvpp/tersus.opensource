/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor;

import org.eclipse.draw2d.InputEvent;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.MouseTrackListener;

import tersus.editor.useractions.DoubleClick;
import tersus.editor.useractions.MouseDown;
import tersus.editor.useractions.MouseDrag;
import tersus.editor.useractions.MouseHover;
import tersus.editor.useractions.MouseUp;
import tersus.util.Trace;

/**
 * 
 */
public class TracingMouseListener implements MouseListener, MouseMoveListener, MouseTrackListener
{

	/**
	 * 
	 */
	public TracingMouseListener()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.events.MouseListener#mouseDoubleClick(org.eclipse.swt.events.MouseEvent)
	 */
	public void mouseDoubleClick(MouseEvent e)
	{
				Trace.add(new DoubleClick(e));
	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.events.MouseListener#mouseDown(org.eclipse.swt.events.MouseEvent)
	 */
	public void mouseDown(MouseEvent e)
	{
		Trace.add(new MouseDown(e));
	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.events.MouseListener#mouseUp(org.eclipse.swt.events.MouseEvent)
	 */
	public void mouseUp(MouseEvent e)
	{
		Trace.add(new MouseUp(e));

	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.events.MouseMoveListener#mouseMove(org.eclipse.swt.events.MouseEvent)
	 */
	public void mouseMove(MouseEvent e)
	{
		if ((e.stateMask & InputEvent.ANY_BUTTON) != 0)
			Trace.add(new MouseDrag(e));
//		else
//			 Trace.add(new MouseMove(e));

	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.events.MouseTrackListener#mouseEnter(org.eclipse.swt.events.MouseEvent)
	 */
	public void mouseEnter(MouseEvent e)
	{
		Trace.add(new MouseHover(e));
	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.events.MouseTrackListener#mouseExit(org.eclipse.swt.events.MouseEvent)
	 */
	public void mouseExit(MouseEvent e)
	{
	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.events.MouseTrackListener#mouseHover(org.eclipse.swt.events.MouseEvent)
	 */
	public void mouseHover(MouseEvent e)
	{
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2020 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor;

import org.eclipse.core.resources.IProject;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.swt.events.KeyEvent;

import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 * @author David Davidson
 *
 */
public class TersusEditDomain extends DefaultEditDomain
{

    private RepositoryManager repositoryManager;
    private boolean initialized = false;
    /**
     * @param editorPart
     */
    public TersusEditDomain(IProject project, TersusEditor editorPart)
    {
        
        super(editorPart);
        initialized = true;
        repositoryManager = RepositoryManager.getRepositoryManager(project);
        repositoryManager.addEditor(editorPart);
        loadDefaultTool();
    }
    

    public CommandStack getCommandStack()
    {
        return repositoryManager.getCommandStack();
    }
    
    public void dispose()
    {
        repositoryManager.removeEditor((TersusEditor)getEditorPart());
    }


    /**
     * @return
     */
    public WorkspaceRepository getRepository()
    {
        return repositoryManager.getRepository();
    }


    /**
     * 
     */
    public void save()
    {
        repositoryManager.save();
    }

    public RepositoryManager getRepositoryManager()
    {
        return repositoryManager;
    }
    public void loadDefaultTool()
    {
        if ( initialized)
            super.loadDefaultTool();
    }

//	Handle model editor palette shortcuts when keyboard is non-ascii or in caps lock mode
    public void keyDown(KeyEvent keyEvent, EditPartViewer viewer)
    {
    	char character = keyEvent.character;
    	int keyCode = keyEvent.keyCode;
    	
		if ((char) keyCode != character )
			if (keyCode == 0)
				keyEvent.keyCode = (int) character;
			else
				keyEvent.character = (char) keyCode;
		super.keyDown(keyEvent, viewer);
	}
}

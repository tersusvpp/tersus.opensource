/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.explorer;

import java.util.EventObject;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.gef.commands.CommandStackListener;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;

import tersus.model.ModelId;
import tersus.model.PackageId;
import tersus.model.Path;
import tersus.workbench.IRepositoryChangeListener;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 *  
 */
public class ProjectEntry extends ExplorerEntry implements
        IRepositoryChangeListener, CommandStackListener
{

    public String toString()
    {
        return getName();
    }

    public IResource getResource()
    {
        return getProject();
    }

    public boolean owns(IResource resource)
    {
        return resource != null
                && (resource.equals(getProject()) || resource
                        .equals(getRepository().getRepositoryRoot()));
    }

    public ProjectEntry(RootEntry parent, IProject project)
    {
        super(parent);
        setProject(project);
    }

    public ExplorerEntry[] loadChildren()
    {
        if (!getProject().isOpen())
            return NO_CHILDREN;
        List<PackageId> childIds = getRepository().getSubPackageIds(null);
        int numberOfChildren = childIds.size();
        if (numberOfChildren == 0)
            return NO_CHILDREN;
        PackageEntry[] children = new PackageEntry[numberOfChildren];
        for (int i = 0; i < numberOfChildren; i++)
        {
            PackageId pkgId = (PackageId) childIds.get(i);
            children[i] = new PackageEntry(this, pkgId);

        }
        return children;
    }

    public boolean hasChildren() throws CoreException
    {
        return getChildren().length > 0;
    }

    public boolean equals(Object other)
    {
        if (other instanceof ProjectEntry)
            return getProject().equals(((ProjectEntry) other).getProject());
        else
            return false;
    }

    public String getName()
    {
        return getProject().getName();
    }

    public void modelAdded(WorkspaceRepository repository, ModelId modelId)
    {
        refreshEntry(findLoadedEntry(modelId.getPackageId()));
    }

    public void modelRemoved(WorkspaceRepository repository, ModelId modelId)
    {
        ModelEntry oldEntry = findLoadedEntry(modelId);
        boolean wasSelected = oldEntry != null && oldEntry.isSelected();
        ExplorerEntry parentPackageEntry = findLoadedEntry(modelId
                .getPackageId());
        refreshEntry(parentPackageEntry);
        if (wasSelected)
            select(parentPackageEntry);

    }

    public void modelRenamed(WorkspaceRepository repository,
            ModelId oldModelId, ModelId newModelId)
    {

        PackageId oldPackageId = oldModelId.getPackageId();
        PackageId newPackageId = newModelId.getPackageId();
        ModelEntry oldEntry = findLoadedEntry(oldModelId);
        boolean wasSelected = oldEntry != null && oldEntry.isSelected();
        refreshEntry(findLoadedEntry(oldPackageId));
        if (!newPackageId.equals(oldPackageId))
            refreshEntry(findLoadedEntry(newPackageId));
        if (wasSelected)
        {
            ModelEntry newEntry = findLoadedEntry(newModelId);
            select(newEntry);
        }
    }

    private void select(ExplorerEntry entry)
    {
        if (entry != null)
        {

            StructuredSelection selection = new StructuredSelection(
                    new Object[] { entry });
            getViewer().setSelection(selection, true);
        }
    }

    public void packageAdded(WorkspaceRepository repository, PackageId packageId)
    {
        refreshEntry(findLoadedEntry(packageId.getParent()));

    }

    public void packageRemoved(WorkspaceRepository repository,
            PackageId packageId)
    {
        PackageEntry oldEntry = (PackageEntry) findLoadedEntry(packageId);
        boolean wasSelected = oldEntry != null && oldEntry.isSelected();
        ExplorerEntry parentPackageEntry = findLoadedEntry(packageId
                .getParent());
        refreshEntry(parentPackageEntry);
        if (wasSelected)
            select(parentPackageEntry);

    }

	public void packageReadOnlyStatusChanged(WorkspaceRepository repository, final PackageId packageId)
	{
        Display.getDefault().asyncExec(new Runnable()
        {
			public void run()
			{
				PackageEntry changedEntry = (PackageEntry) findLoadedEntry(packageId);
				refreshEntry(changedEntry);
			}
        });
	}
    
    public void packageRenamed(WorkspaceRepository repository,
            PackageId originalId, PackageId newId)
    {
        PackageEntry oldEntry = (PackageEntry) findLoadedEntry(originalId);
        boolean wasSelected = oldEntry != null && oldEntry.isSelected();
        ExplorerEntry originalParent = findLoadedEntry(originalId.getParent());
        ExplorerEntry newParent = findLoadedEntry(newId.getParent());
        refreshEntry(originalParent);
        if (newParent != originalParent)
            refreshEntry(newParent);
        if (wasSelected)
            select(findLoadedEntry(newId));

    }

    public void cacheCleared(WorkspaceRepository repository)
    {
        fullRefresh();
    }

    public void repositorySaved(WorkspaceRepository repository)
    {
        fullRefresh();
    }

    private void fullRefresh()
    {
        requestDeepRefresh();
        getViewer().refresh(this);
    }

    private ModelEntry findLoadedEntry(ModelId modelId)
    {
        return (ModelEntry) findLoadedEntry(new Path(modelId.getPath()), 0, ModelEntry.class);
    }

    public PackageEntry findPackageEntry(PackageId packageId, boolean expand)
    {
        Path path = new Path(packageId.getPath());
        ExplorerEntry scope = this;
        for (int i = 0; i < path.getNumberOfSegments(); i++)
        {
            if (expand)
                getViewer().expandToLevel(scope, 1);
            String name = path.getSegment(i).toString();
            Object[] children = scope.getLoadedChildren();
            scope = null;
            for (int j = 0; j < children.length; j++)
            {
                ExplorerEntry child = (ExplorerEntry) children[j];
                if (child instanceof PackageEntry
                        && child.getName().equals(name))
                {
                    scope = child;
                    break;
                }
            }
            if (scope == null)
                return null;
        }
        return (PackageEntry) scope;
    }

    public ModelEntry findModelEntry(ModelId modelId, boolean expand)
    {
        PackageEntry packageEntry = findPackageEntry(modelId.getPackageId(),
                expand);
        if (packageEntry == null)
            return null;
        if (expand)
            getViewer().expandToLevel(packageEntry, 1);
        ExplorerEntry[] children = packageEntry.getLoadedChildren();
        for (int j = 0; j < children.length; j++)
        {
            ExplorerEntry child = (ExplorerEntry) children[j];
            if (child instanceof ModelEntry
                    && child.getName().equals(modelId.getName()))
            {
                return (ModelEntry) child;
            }
        }
        return null;
    }

    private ExplorerEntry findLoadedEntry(PackageId packageId)
    {
        if (packageId == null)
            return this;
        ExplorerEntry entry = findLoadedEntry(new Path(packageId.getPath()), 0, PackageEntry.class);
        if (entry instanceof PackageEntry)
            return (PackageEntry) entry;
        else
            return null;
    }

    private void refreshEntry(ExplorerEntry entry)
    {
        if (entry != null)
        {
            entry.refreshChildren();
            getViewer().refresh(entry);
        }
    }

    public void activate()
    {
        super.activate();
        getRepository().addRepositoryChangeListener(this);
        getEditDomain().getCommandStack().addCommandStackListener(this);
    }

    public void dispose()
    {
        getRepository().removeRepositoryChangeListener(this);
        getEditDomain().getCommandStack().removeCommandStackListener(this);
        super.dispose();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.gef.commands.CommandStackListener#commandStackChanged(java.util.EventObject)
     */
    public void commandStackChanged(EventObject event)
    {
        part.updateActions();
        part.updateDirty();
        getViewer().update(this, null); //Updates the dirty indicator
    }

    public String getText()
    {
        if (getEditDomain().getCommandStack().isDirty())
            return '*' + getName();
        else
            return getName();
    }

    public Object getAdapter(Class adapter)
    {
        if (adapter.isAssignableFrom(IProject.class))
            return getProject();
        else
            return super.getAdapter(adapter);
    }

	public void repositoryRefreshed(WorkspaceRepository repository)
	{
		requestDeepRefresh();
		getViewer().refresh(this);
		
	}
}
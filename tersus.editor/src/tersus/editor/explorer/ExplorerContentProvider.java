/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.explorer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.eclipse.core.internal.events.ResourceChangeEvent;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;

import tersus.workbench.TersusWorkbench;

/**
 * @author Youval Bronicki
 * 
 */
public class ExplorerContentProvider implements ITreeContentProvider, IResourceChangeListener
{

	IWorkspace workspace;

	ExplorerEntry rootEntry;

	TreeViewer viewer;

	public ExplorerContentProvider(TreeViewer viewer)
	{
		this.viewer = viewer;
	}

	public void dispose()
	{
		if (workspace != null)
		{
			workspace.removeResourceChangeListener(this);
			workspace = null;
		}
		rootEntry = null;
	}

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
	{
		dispose();
		if (newInput != null)
		{
			rootEntry = (ExplorerEntry) newInput;
			IResource rootResource = rootEntry.getResource();
			if (rootResource != null)
			{
				workspace = rootResource.getWorkspace();
				workspace.addResourceChangeListener(this, ResourceChangeEvent.POST_CHANGE);
			}
		}
		System.out.println("ExplorerContentProvider New input:" + newInput);

	}

	public Object[] getChildren(Object parentElement)
	{
		ArrayList<ExplorerEntry> children = new ArrayList<ExplorerEntry>();

		for (Object childObject : ((ExplorerEntry) parentElement).getChildren())
		{
			children.add((ExplorerEntry) childObject);
		}

		Collections.sort(children, new Comparator<Object>()
		{
			public int compare(Object o1, Object o2)
			{
				if (o1 instanceof PackageEntry && !(o2 instanceof PackageEntry))
					return -1;
					
				if (o2 instanceof PackageEntry && !(o1 instanceof PackageEntry))
					return 1;
				
				return getEntryName(o1).toLowerCase()
						.compareTo(getEntryName(o2).toLowerCase());
			}
			
			private String getEntryName(Object obj)
			{
				if (obj instanceof ProjectEntry)
					return ((ProjectEntry)obj).getResource().getName();
				if (obj instanceof PackageEntry)
					return ((PackageEntry)obj).getName();
				if (obj instanceof ModelEntry)
					return ((ModelEntry)obj).getModelId().getName();
				else 
					return ((ExplorerEntry)obj).getResource().getName();
				
			}
		});

		return children.toArray();
	}
	
	public Object getParent(Object element)
	{
		return ((ExplorerEntry) element).getParent();
	}

	public boolean hasChildren(Object element)
	{
		if (element == null)
			return false;
		try
		{
			return ((ExplorerEntry) element).hasChildren();
		}
		catch (CoreException e)
		{
			TersusWorkbench.log(e);
			return false;
		}
	}

	public Object[] getElements(Object inputElement)
	{
		return getChildren(inputElement);
	}

	public void resourceChanged(IResourceChangeEvent event)
	{
		final IResourceDelta delta = event.getDelta();
		viewer.getControl().getDisplay().asyncExec(new Runnable()
		{
			public void run()
			{
				handleResourceDelta(delta);
			}
		});
	}

	public void handleResourceDelta(IResourceDelta delta)
	{
		if (viewer == null || viewer.getControl() == null || viewer.getControl().isDisposed())
			return;
		handleResourceDelta(rootEntry, delta);
	}

	private void handleResourceDelta(ExplorerEntry scope, IResourceDelta delta)
	{
		ExplorerEntry entry = findEntryForResource(scope, delta.getResource());
		if (entry != null && !isSaveInProgress(entry)) // If the entry's repository is saving, we
														// ignore the delta
		{
			boolean needRefresh = false;
			if ((delta.getKind() & (IResourceDelta.ADDED | IResourceDelta.REMOVED | IResourceDelta.OPEN)) != 0)
				needRefresh = true;
			else if ((delta.getFlags() & IResourceDelta.CONTENT) != 0)
				needRefresh = true;
			IResourceDelta[] deltas = delta.getAffectedChildren();
			for (int i = 0; i < deltas.length; i++)
			{
				int kind = deltas[i].getKind();
				if ((kind & (IResourceDelta.ADDED | IResourceDelta.REMOVED)) != 0)
				{
					needRefresh = true;
					break;
				}
			}
			if (needRefresh)
				refreshEntry(entry);
			for (int i = 0; i < deltas.length; i++)
			{
				handleResourceDelta(entry, deltas[i]);
			}
		}
	}

	/**
	 * Checks whether the repository of this entry is currently saving its state
	 * 
	 * @param entry
	 * @return
	 */
	private boolean isSaveInProgress(ExplorerEntry entry)
	{
		return entry.getRepository() != null && entry.getRepository().isSaveInProgress();
	}

	private void refreshEntry(ExplorerEntry entry)
	{
		if (entry != null)
		{
			entry.refreshChildren();
			viewer.refresh(entry);
		}
	}

	public static ExplorerEntry findEntryForResource(ExplorerEntry scope, IResource resource)
	{
		try
		{
			if (scope.owns(resource))
				return scope;
			if (scope.getResource() != null && scope.getResource().contains(resource))
			{
				Object[] children = scope.getLoadedChildren();
				for (int i = 0; i < children.length; i++)
				{
					ExplorerEntry child = (ExplorerEntry) children[i];
					ExplorerEntry entry = findEntryForResource(child, resource);
					if (entry != null)
						return entry;
				}
				return scope;
			}
		}
		catch (Exception e)
		{
			TersusWorkbench.log(e);
		}
		return null;
	}
}
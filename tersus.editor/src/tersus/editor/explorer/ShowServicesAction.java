/************************************************************************************************
 * Copyright (c) 2003-2023 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.explorer;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.gef.ui.actions.UpdateAction;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.part.FileEditorInput;

import tersus.editor.RepositoryManager;
import tersus.model.ModelId;
import tersus.model.Path;
import tersus.model.indexer.ElementEntry;
import tersus.model.indexer.RepositoryIndex;
import tersus.util.Misc;
import tersus.workbench.Configuration;
import tersus.workbench.Configurator;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 *  
 */
public class ShowServicesAction extends Action implements UpdateAction
{
    private TreeViewer viewer;
    IProject selectedProject = null;

    /**
     * @param viewer
     */
    public ShowServicesAction(TreeViewer viewer)
    {
        super();
        this.viewer = viewer;
        setId("tersus.explorer.show_services");
        setText("Show Services");
    }
    public TreeViewer getViewer()
    {
        return viewer;
    }
    
    public void run()
    {
        if (selectedProject != null)
        {
            RepositoryManager manager = RepositoryManager.getRepositoryManager(selectedProject);
            Configuration app = Configurator.getConfiguration(selectedProject);

            WorkspaceRepository repository = manager.getRepository();
			RepositoryIndex index = repository.getUpdatedRepositoryIndex();
			
			HashSet<ModelId> uiModels = index.calculateUIModelIds(repository.getModel(app.getRootSystemId(),true),null);
			ArrayList<String> list = new ArrayList<String>();
			ArrayList<String> list2 = new ArrayList<String>();
			for (ModelId id: uiModels)
			{
				if (index.getModelEntry(id).getDescriptor().isServiceClient())
				{
					String permission = index.getPermissionMap().get(id);
					if (permission == null)
						permission = "-";
					list.add(id.getName()+"\t["+ permission + "]\t"+id.toString());
					list2.add("Paths for \""+ id.toString()+"\"	");
					list2.addAll(getServicesPaths(index, id, app.getRootSystemId()));
					
				}
				
			}
			Collections.sort(list);
			
			list.add("\nService Paths");
			list.addAll(list2);
	        IFile file = selectedProject.getFile("services.txt");
	        final ByteArrayInputStream contentBytes = new ByteArrayInputStream(
	                Misc.concatenateList(list, "\n").getBytes());
	        try
	        {
	            if (file.exists())
	                file.setContents(contentBytes, IResource.FORCE, null);
	            else
	                file.create(contentBytes, IResource.FORCE, null);
	            PlatformUI.getWorkbench().getActiveWorkbenchWindow()
	                    .getActivePage().openEditor(new FileEditorInput(file),
	                            EditorsUI.DEFAULT_TEXT_EDITOR_ID);
	        }
	        catch (CoreException e)
	        {
	            MessageDialog.openError(Display.getCurrent().getActiveShell(),
	                    "Error", "Can't save file" + file.getFullPath());
	        }

        }
    }
    
    private Collection<? extends String> getServicesPaths(RepositoryIndex index, ModelId id, ModelId rootId)
	{
    	ArrayList<String> paths = new ArrayList<String>();
    	Path p = new Path();
    	ModelId currentRoot = id;
    	addReference(rootId, index, currentRoot, p, paths, new HashSet<Path>());
    	return paths;
	}
	protected void addReference(ModelId rootId, RepositoryIndex index, ModelId currentModel, Path p,
			ArrayList<String> paths, HashSet<Path> traversed)
	{
		if (traversed.contains(p))
			return;
//		traversed.add(p);
		for (ElementEntry ref: index.getReferenceEntries(currentModel))
    	{
    		if (!ref.isSubflow())
    			continue;
    		Path p1 = new Path();
    		p1.addSegment(ref.getRole());
    		p1.append(p);
    		if (rootId.equals(ref.getParentId()))
    		{
    			paths.add(p1.toString());
    		}
    		else
    			addReference(rootId, index, ref.getParentId(), p1, paths, traversed);
    	}
	}
	public void update()
    {
        ISelection selection = viewer.getSelection();
        ExplorerEntry selectedEntry = null;
        if (selection instanceof IStructuredSelection)
            selectedEntry = (ExplorerEntry)((IStructuredSelection)selection).getFirstElement();
        if (selectedEntry != null)
        {
            selectedProject = selectedEntry.getProject();
            setText("Show services in project '"+selectedProject.getName()+"'");
        }
        else
        {
            selectedProject = null;
        }
        
    }
    
    
    public boolean calculateEnabled()
    {
        return selectedProject != null;
    }

}
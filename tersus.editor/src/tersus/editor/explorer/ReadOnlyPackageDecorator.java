package tersus.editor.explorer;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.ILightweightLabelDecorator;
import org.eclipse.jface.viewers.LabelProvider;

import tersus.editor.TersusEditor;
import tersus.model.Model;
import tersus.model.Package;

public class ReadOnlyPackageDecorator extends LabelProvider implements ILightweightLabelDecorator
{
	static final String decoratorId = "tersus.editor.explorer.readOnlyDecorator";

	private ImageDescriptor lockImageDescriptor;

	public ReadOnlyPackageDecorator()
	{
		super();
		lockImageDescriptor = ImageDescriptor.createFromFile(TersusEditor.class,
				"icons/readOnlyState.big.gif");
	}

	public void decorate(Object element, IDecoration decoration)
	{
		if (element instanceof PackageEntry)
		{
			PackageEntry entry = (PackageEntry)element;
			Package pkg = entry.getRepository().getPackage(entry.getPackageId());
			
			if(pkg != null && pkg.isReadOnly())
				decoration.addOverlay(lockImageDescriptor);
		}
		else if (element instanceof ModelEntry)
		{
			ModelEntry entry = (ModelEntry)element;
			Model model = entry.getRepository().getModel(entry.getModelId(), true);
			if(model != null && model.isReadOnly())
				decoration.addOverlay(lockImageDescriptor);
		}
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.explorer;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.gef.ui.actions.UpdateAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.internal.WorkbenchPlugin;
import org.eclipse.ui.part.FileEditorInput;

import tersus.editor.RepositoryManager;
import tersus.editor.actions.AbstractSelectionAction;
import tersus.editor.actions.MoveUnusedCommand;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.PackageId;
import tersus.util.Misc;
import tersus.workbench.WorkspaceRepository;

public class ListUnusedAction extends AbstractSelectionAction implements
        UpdateAction
{

    private IProject _project;

    private PackageId _packageId;

    public void update()
    {
        _packageId = getPackageEntry() == null ? null : getPackageEntry()
                .getPackageId();
        if (getPackageEntry() != null)
            _project = getPackageEntry().getProject();
        else
            _project = getProject();

        if (_packageId != null)
            setText("List unused models in package " + _packageId.getName());
        else if (_project != null)
            setText("List unused models in project " + _project.getName());
        else
            setText("List unused models");
        super.update();

    }

    public boolean calculateEnabled()
    {
        boolean _enabled = _packageId != null || _project != null;
        return _enabled;
    }

    public void run()
    {
        if (_project != null)
        {
            WorkspaceRepository repository = RepositoryManager
                    .getRepositoryManager(_project).getRepository();
            PackageId unusedPackageId = repository.getNewSubPackageId(null,
                    MoveUnusedCommand.UNUSED_MODELS_PACKAGE);
            Model[] unused = repository.getUnusedModels(
                    unusedPackageId);
            HashSet externallyReferredModeIds = new HashSet();
            if (_packageId != null)
            {
                for (Iterator i = repository.getExternalReferences(_packageId)
                        .iterator(); i.hasNext();)
                {
                    ModelElement reference = (ModelElement) i.next();
                    if (!unusedPackageId.isAncestorOf(reference
                            .getParentModel().getId()))
                        externallyReferredModeIds.add(reference.getRefId());
                }
            }

            ArrayList outputList = new ArrayList();
            if (unused != null && unused.length > 0)
            {
                for (int i = 0; i < unused.length; i++)
                {
                    Model model = unused[i];
                    if (_packageId != null)
                    {
                        if (!_packageId.isAncestorOf(model.getId()))
                            continue; // Model is not in our scope;
                        if (externallyReferredModeIds.contains(model.getId()))
                            /*
                             * If we move a model that has external references,
                             * we won't be able to remove the "Unused Models"
                             * package
                             */
                            continue;
                    }
                    outputList.add(model.getId().getPath());
                }
            }
            StringBuffer message = new StringBuffer();
            message.append("Total number of models: "+repository.getUpdatedRepositoryIndex().countLocalModels()+"\n");
            message.append("Unused models:"+outputList.size()+"\n\n");
            message.append(Misc.concatenateList(outputList, "\n"));
            IFile file = _project.getFile("unused models.txt");
            final ByteArrayInputStream contentBytes = new ByteArrayInputStream(
                    message.toString().getBytes());
            try
            {
                if (file.exists())
                    file.setContents(contentBytes, IResource.FORCE, null);
                else
                    file.create(contentBytes, IResource.FORCE, null);
                PlatformUI.getWorkbench().getActiveWorkbenchWindow()
                        .getActivePage().openEditor(new FileEditorInput(file),
                                EditorsUI.DEFAULT_TEXT_EDITOR_ID);
            }
            catch (CoreException e)
            {
                MessageDialog.openError(Display.getCurrent().getActiveShell(),
                        "Error", "Can't save file" + file.getFullPath());
            }

        }

    }

    public ListUnusedAction(TreeViewer viewer)
    {
        setSelectionProvider(viewer);
    }

}

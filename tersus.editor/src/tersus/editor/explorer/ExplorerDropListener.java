/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.explorer;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.swt.dnd.DropTargetListener;
import org.eclipse.swt.dnd.TransferData;

import tersus.editor.actions.RenamePackageCommand;
import tersus.editor.outline.ModelObjectTransfer;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.PackageId;
import tersus.model.commands.ChangeModelIdCommand;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 * @author Liat Shiff
 *  
 */
public class ExplorerDropListener extends ViewerDropAdapter implements
        DropTargetListener
{
    @SuppressWarnings("unused")
	private ExplorerPart explorer;
    

    /**
     * @param explorer
     */
    public ExplorerDropListener(ExplorerPart explorer)
    {
        super(explorer.getViewer());
        setFeedbackEnabled(false);
        this.explorer = explorer;
    }

    public boolean performDrop(Object data)
    {	
    	List<?> entries = (List<?>) data;
        CompoundCommand command = new CompoundCommand();
        PackageEntry target = (PackageEntry) getCurrentTarget();
        WorkspaceRepository repository = target.getRepository();
        for (Object element:  entries)
        {
            if (element instanceof ModelEntry || element instanceof PackageEntry)
            {
                ExplorerEntry entry = (ExplorerEntry) element;
                if (entry.getProject().equals(target.getProject()))
                {
                    if (entry instanceof ModelEntry)
                    {
                        Model model = repository.getModel(((ModelEntry)entry).getModelId(), true);
                        if (model != null)
                        {
                            ModelId newModelId = new ModelId(target.getPackageId(),
                                    model.getName());
                            command.add(new ChangeModelIdCommand(model, newModelId));
                        }
                    }
                    else if (entry instanceof PackageEntry)
                    {
                        PackageId originalPackageId = ((PackageEntry)entry).getPackageId();
                        PackageId newPackageId = new PackageId(target.getPackageId(), originalPackageId.getName());
                        command.add(new RenamePackageCommand(repository, originalPackageId, newPackageId));
                    }
                }

            }
            
            
        }

		
        if (command.canExecute())
        {
            command.setLabel("Move");
            target.getEditDomain().getCommandStack().execute(command);

            if (target.getChildren().length == 1)
            	target.getViewer().setExpandedState(target, true);
            
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public boolean validateDrop(Object target, int operation,
            TransferData transferType)
    {	
    	if (!ModelObjectTransfer.getInstance().isSupportedType(transferType))
    		return false;
    	
        List<?> entries = ModelObjectTransfer.getInstance().getEntries();
        
        if (entries.size() == 0)
        	return false;
        
        return target instanceof PackageEntry;
    }
}
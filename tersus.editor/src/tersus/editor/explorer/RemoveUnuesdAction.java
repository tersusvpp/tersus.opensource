/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.explorer;

import org.eclipse.core.resources.IProject;
import org.eclipse.gef.ui.actions.UpdateAction;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;

import tersus.editor.RepositoryManager;
import tersus.editor.actions.RemoveUnusedCommand;

/**
 * @author Youval Bronicki
 *  
 */
public class RemoveUnuesdAction extends Action implements UpdateAction
{
    private TreeViewer viewer;
    IProject selectedProject = null;

    /**
     * @param viewer
     */
    public RemoveUnuesdAction(TreeViewer viewer)
    {
        super();
        this.viewer = viewer;
        setId("tersus.explorer.remove_unused");
        setText("Remove unused models in project");
    }
    public TreeViewer getViewer()
    {
        return viewer;
    }
    
    public void run()
    {
        if (selectedProject != null)
        {
            RepositoryManager manager = RepositoryManager.getRepositoryManager(selectedProject);
            if (manager.getCommandStack().isDirty())
            	MessageDialog.openWarning(viewer.getControl().getShell(),"Cannot Move","Please save first all unsaved changes in project '"+selectedProject.getName()+"'");
            else
            {
            	RemoveUnusedCommand command = new RemoveUnusedCommand(manager.getRepository(), selectedProject);
                if (command.canExecute()) {
                	manager.getCommandStack().execute(command);
//                	PackageId newPackageId = command.getUnusedPackageId();
//                	manager.getRepository().notifyNewPackage(newPackageId);
                }
            }
        }
    }
    
    public void update()
    {
        ISelection selection = viewer.getSelection();
        ExplorerEntry selectedEntry = null;
        if (selection instanceof IStructuredSelection)
            selectedEntry = (ExplorerEntry)((IStructuredSelection)selection).getFirstElement();
        if (selectedEntry != null)
        {
            selectedProject = selectedEntry.getProject();
            setText("Remove unused models in project '"+selectedProject.getName()+"'");
        }
        else
        {
            selectedProject = null;
        }
        
    }
    
    
    public boolean calculateEnabled()
    {
        return selectedProject != null;
    }

}
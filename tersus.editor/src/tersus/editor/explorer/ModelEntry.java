/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.explorer;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.swt.graphics.Image;

import tersus.editor.ConcreteModelIdentifier;
import tersus.model.BuiltinProperties;
import tersus.model.FlowModel;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.ModelObject;
import tersus.util.Misc;

/**
 * @author Youval Bronicki
 *
 */
public class ModelEntry extends ExplorerEntry implements IAdaptable
{
    String name;
    ModelId modelId;
    private boolean isFlowModel;
    private String iconPath;
    
    /* (non-Javadoc)
     * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
     */
    public Object getAdapter(Class adapter)
    {
        if (ModelObject.class.equals(adapter))
            return getRepository().getModel(getModelId(), true);
        
        else if (ConcreteModelIdentifier.class.equals(adapter))
            return new ConcreteModelIdentifier(getProject(), getModelId());
        
        else if (IProject.class.equals(adapter))
        	return getProject();
        
        else
            return null;
    }
    ModelEntry(PackageEntry parent, Model model)
    {
        super(parent);
        this.name = model.getName();
        this.isFlowModel = model instanceof FlowModel;
        this.modelId = model.getId();
        iconPath = (String)model.getProperty(BuiltinProperties.ICON_FOLDER);
    }
    
    public Image getImage()
    {
        if (iconPath != null)
            return getRepository().getImageLocator(iconPath).get16x16Image();
        else
            return null;
    }
    
    public String getText()
    {
        return name;
    }
    
    protected ExplorerEntry[] loadChildren() 
    {
        return NO_CHILDREN;
    }
    
    public boolean equals(Object other)
    {
        if (other instanceof ModelEntry)
            return Misc.equal((Object)modelId,(Object)((ModelEntry)other).modelId) && getProject().equals(((ModelEntry)other).getProject());
        else
            return false;
    }
    
    public ModelId getModelId()
    {
        return modelId;
    }
    
    public boolean isFlowModel()
    {
        return isFlowModel;
    }

    public String getName()
    {
        return name;
    }
    
    public String toString()
    {
        return String.valueOf(modelId);
    }
}

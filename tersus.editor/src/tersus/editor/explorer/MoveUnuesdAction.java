/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.explorer;

import org.eclipse.core.resources.IProject;
import org.eclipse.gef.ui.actions.UpdateAction;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;

import tersus.editor.RepositoryManager;
import tersus.editor.actions.MoveUnusedCommand;
import tersus.model.PackageId;

/**
 * @author Ofer Brandes
 *  
 */
public class MoveUnuesdAction extends Action implements UpdateAction
{
    private TreeViewer viewer;
    IProject selectedProject = null;
    private boolean onlyInSelectedPackage;
    private PackageId selectedPackageId;

    /**
     * @param viewer
     */
    public MoveUnuesdAction(TreeViewer viewer, boolean onlyInSelectedPackage)
    {
        super();
        this.viewer = viewer;
        setId("tersus.explorer.move_unused");
        setText("Move unused models in project");
        this.onlyInSelectedPackage = onlyInSelectedPackage;
    }
    public TreeViewer getViewer()
    {
        return viewer;
    }
    
    public void run()
    {
        if (selectedProject != null)
        {
            RepositoryManager manager = RepositoryManager.getRepositoryManager(selectedProject);
            if (manager.getCommandStack().isDirty())
            	MessageDialog.openWarning(viewer.getControl().getShell(),"Cannot Move","Please save first all unsaved changes in project '"+selectedProject.getName()+"'");
            else
            {
            	MoveUnusedCommand command = new MoveUnusedCommand(manager.getRepository(), selectedProject, selectedPackageId);
                if (command.canExecute()) {
                	manager.getCommandStack().execute(command);
//                	PackageId newPackageId = command.getUnusedPackageId();
//                	manager.getRepository().notifyNewPackage(newPackageId);
                }
            }
        }
    }
    
    public void update()
    {
        selectedProject = null;
        selectedPackageId = null;
        if (onlyInSelectedPackage)
            setText("Move unused models in package");
        else
            setText("Move unused models in project");
        ISelection selection = viewer.getSelection();
        ExplorerEntry selectedEntry = null;
        if (selection instanceof IStructuredSelection)
            selectedEntry = (ExplorerEntry)((IStructuredSelection)selection).getFirstElement();
        if (selectedEntry != null)
        {
            selectedProject = selectedEntry.getProject();
            if (onlyInSelectedPackage)
            {
                if (selectedEntry instanceof PackageEntry)
                {
                    selectedPackageId = ((PackageEntry)selectedEntry).getPackageId();
                    setText("Move unused models in package '"+selectedPackageId.getName()+"'");
                }
            }
            else
                setText("Move unused models in project '"+selectedProject.getName()+"'");
        }
        setEnabled(calculateEnabled());
        
    }
    
    
    public boolean calculateEnabled()
    {
        return onlyInSelectedPackage ? selectedPackageId != null : selectedProject != null;
    }

}
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.explorer;

import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.internal.GEFMessages;
import org.eclipse.gef.ui.actions.RedoAction;
import org.eclipse.gef.ui.actions.UndoAction;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DecoratingLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProviderChangedEvent;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IKeyBindingService;
import org.eclipse.ui.ISaveablePart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.CloseResourceAction;
import org.eclipse.ui.actions.ExportResourcesAction;
import org.eclipse.ui.actions.ImportResourcesAction;
import org.eclipse.ui.actions.NewWizardMenu;
import org.eclipse.ui.actions.OpenFileAction;
import org.eclipse.ui.actions.OpenResourceAction;
import org.eclipse.ui.actions.OpenWithMenu;
import org.eclipse.ui.part.DrillDownAdapter;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.texteditor.IWorkbenchActionDefinitionIds;
import org.eclipse.ui.views.navigator.OpenActionGroup;
import org.osgi.framework.Bundle;

import tersus.editor.ConcreteModelIdentifier;
import tersus.editor.ConcretePackageIdentifier;
import tersus.editor.EditorMessages;
import tersus.editor.TersusEditor;
import tersus.editor.actions.CopyEntryFromExplorerAction;
import tersus.editor.actions.DeleteAction;
import tersus.editor.actions.GoToModelByIdAction;
import tersus.editor.actions.ShowInNavigatorAction;
import tersus.editor.actions.MoveModelsRepositoryAction;
import tersus.editor.actions.PastePackageAction;
import tersus.editor.actions.RenameAction;
import tersus.editor.actions.ShowUsagesAction;
import tersus.editor.actions.TersusActionConstants;
import tersus.editor.outline.ModelObjectTransfer;
import tersus.model.Model;
import tersus.workbench.Configurator;
import tersus.workbench.TersusWorkbench;

/**
 * @author Youval Bronicki
 */
public class ExplorerPart extends ViewPart implements ISaveablePart, ILabelProviderListener
{

	public void doSave(IProgressMonitor monitor)
	{
		RootEntry root = (RootEntry) viewer.getInput();
		ExplorerEntry[] projects = root.getLoadedChildren();
		for (int i = 0; i < projects.length; i++)
		{
			ExplorerEntry entry = projects[i];
			if (entry.getEditDomain().getCommandStack().isDirty())
				entry.getEditDomain().save();

		}
	}

	public void doSaveAs()
	{
	}

	public boolean isDirty()
	{
		RootEntry root = (RootEntry) viewer.getInput();
		ExplorerEntry[] projects = root.getLoadedChildren();
		for (int i = 0; i < projects.length; i++)
		{
			ExplorerEntry entry = projects[i];
			if (entry.getEditDomain().getCommandStack().isDirty())
				return true;

		}
		return false;
	}

	public boolean isSaveAsAllowed()
	{
		return false;
	}

	public boolean isSaveOnCloseNeeded()
	{
		return isDirty();
	}

	private TreeViewer viewer;

	private DrillDownAdapter drillDownAdapter;

	public static final String VIEW_ID = "tersus.editor.explorer";

	private RenameAction renameAction;

	private PastePackageAction pastePackageAction;

	private MoveModelsRepositoryAction moveModelsAction;

	private ShowUsagesAction showUsagesAction;
	
	private ShowInNavigatorAction showInNavigatorAction;

	private DeleteAction deleteAction;

	private UndoAction undoAction;

	private RedoAction redoAction;
	
	private GoToModelByIdAction goToModelByIdAction;

	private ISelectionAction[] pluginActions;

	private ISelectionChangedListener selectionChangedListener = new ISelectionChangedListener()
	{

		public void selectionChanged(SelectionChangedEvent event)
		{
			ExplorerPart.this.selectionChanged(event);
		}

	};

	private OpenFileAction openFileAction;

	private CopyEntryFromExplorerAction copyAction;

	private Action refreshAction;

	private Action collapseAllAction;

	private RevertAction revertAction;

	private MoveUnuesdAction moveUnusedAction;

	private RemoveUnuesdAction removeUnusedAction;

	private ShowServicesAction showServicesAction;

	private ListUnusedAction listUnusedAction;

	private OpenResourceAction openResourceAction;

	private CloseResourceAction closeResourceAction;

	private MoveUnuesdAction moveUnusedModelsInPackageAction;

	private Action arrangeAction;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	public void createPartControl(Composite parent)
	{
		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		drillDownAdapter = new DrillDownAdapter(viewer);
		viewer.setContentProvider(new ExplorerContentProvider(viewer));
		viewer.setLabelProvider(new DecoratingLabelProvider(new ExplorerLabelProvider(), getSite()
				.getWorkbenchWindow().getWorkbench().getDecoratorManager().getLabelDecorator()));
		// viewer.setLabelProvider(new ExplorerLabelProvider());
		RootEntry rootEntry = new RootEntry(this);
		viewer.setInput(rootEntry);
		viewer.addDoubleClickListener(new IDoubleClickListener()
		{

			public void doubleClick(DoubleClickEvent event)
			{

				ISelection selection = event.getSelection();
				if (selection instanceof IStructuredSelection)
				{
					for (Object selectedObj : ((IStructuredSelection) selection).toList())
					{
						if (selectedObj instanceof ModelEntry)
							openEditor((ModelEntry) selectedObj);
						else if (selectedObj instanceof ProjectEntry)
						{
							IProject project = ((ProjectEntry) selectedObj).getProject();
							Model rootModel = Configurator.getRootModel(project);
							if (rootModel != null)
							{
								openEditor(new ConcreteModelIdentifier(project, rootModel.getId()));
							}

						}
					}
				}
			}
		});
		int ops = DND.DROP_COPY | DND.DROP_MOVE;
		Transfer[] transfers = new Transfer[]
		{ ModelObjectTransfer.getInstance() };
		viewer.addDragSupport(ops, transfers, new ExplorerDragListener(this));
		viewer.addDropSupport(ops, transfers, new ExplorerDropListener(this));
		makeActions();
		registerActions();
		initContextMenu();
		viewer.addSelectionChangedListener(selectionChangedListener);
		getSite().setSelectionProvider(viewer);
		viewer.getLabelProvider().addListener(this);
	}

	private void fillToolBar(IToolBarManager manager)
	{
		manager.add(collapseAllAction);
		manager.add(refreshAction);

	}

	protected void selectionChanged(SelectionChangedEvent event)
	{
		updateActions();
	}

	private void makeActions()
	{
		IKeyBindingService keyBindingService = getViewSite().getKeyBindingService();
		renameAction = new RenameAction(this);
		renameAction.setSelectionProvider(viewer);
		renameAction.setActionDefinitionId(IWorkbenchActionDefinitionIds.RENAME);
		keyBindingService.registerAction(renameAction);

		showUsagesAction = new ShowUsagesAction(this);
		showUsagesAction.setSelectionProvider(viewer);
		showUsagesAction.setActionDefinitionId(TersusActionConstants.SHOW_USAGES_ACTION_ID);
		keyBindingService.registerAction(showUsagesAction);
		
		showInNavigatorAction = new ShowInNavigatorAction(this);
		showInNavigatorAction.setSelectionProvider(viewer);
		keyBindingService.registerAction(showInNavigatorAction);
		
		goToModelByIdAction = new GoToModelByIdAction(this);
		goToModelByIdAction.setSelectionProvider(viewer);
		keyBindingService.registerAction(goToModelByIdAction);

		copyAction = new CopyEntryFromExplorerAction(this);
		copyAction.setSelectionProvider(viewer);
		keyBindingService.registerAction(copyAction);

		pastePackageAction = new PastePackageAction(this);
		pastePackageAction.setSelectionProvider(viewer);
		keyBindingService.registerAction(pastePackageAction);

		moveModelsAction = new MoveModelsRepositoryAction(this);
		moveModelsAction.setSelectionProvider(viewer);
		keyBindingService.registerAction(moveModelsAction);
	
		deleteAction = new DeleteAction(this);
		deleteAction.setSelectionProvider(viewer);
		deleteAction.setActionDefinitionId(IWorkbenchActionDefinitionIds.DELETE);

		undoAction = new UndoAction(this)
		{
			public boolean calculateEnabled()
			{
				boolean enabled = false;
				CommandStack commandStack = getCommandStack();
				if (commandStack != null)
					enabled = commandStack.canUndo();
				return enabled;
			}

			protected void refresh()
			{
				if (getCommandStack() != null)
					super.refresh();
				else
				{
					setToolTipText(MessageFormat.format(GEFMessages.UndoAction_Tooltip,
							new Object[]
							{ "" }).trim()); //$NON-NLS-1$
					setText(MessageFormat.format(GEFMessages.UndoAction_Label, new Object[]
					{ "" }).trim() //$NON-NLS-1$
					);
				}
			}

			public void run()
			{
				if (getCommandStack() != null)
					super.run();
			}

		};
		undoAction.setActionDefinitionId(IWorkbenchActionDefinitionIds.UNDO);
		redoAction = new RedoAction(this)
		{
			public void run()
			{
				if (getCommandStack() != null)
					super.run();
			}

			public boolean calculateEnabled()
			{
				boolean enabled = false;
				CommandStack commandStack = getCommandStack();
				if (commandStack != null)
					enabled = commandStack.canRedo();
				return enabled;
			}

			protected void refresh()
			{
				if (getCommandStack() != null)
					super.refresh();
				else
				{
					setToolTipText(MessageFormat.format(GEFMessages.UndoAction_Tooltip,
							new Object[]
							{ "" }).trim()); //$NON-NLS-1$
					setText(MessageFormat.format(GEFMessages.UndoAction_Label, new Object[]
					{ "" }).trim() //$NON-NLS-1$
					);
				}
			}

		};
		redoAction.setActionDefinitionId(IWorkbenchActionDefinitionIds.REDO);
		openFileAction = new OpenFileAction(getSite().getPage());

		refreshAction = new RefreshAction(getViewer());
		collapseAllAction = new Action()
		{
			public void run()
			{
				getViewer().collapseAll();
			}
		};

		collapseAllAction.setText("Collapse All");
		collapseAllAction.setToolTipText("Collapse All");
		collapseAllAction.setImageDescriptor(ImageDescriptor.createFromFile(TersusEditor.class,
				"icons/collapseall.gif"));

		revertAction = new RevertAction(getViewer());
		arrangeAction = new Action()
		{
			public void run()
			{
				if (getSelectedEntry() instanceof ModelEntry)
				{
					ModelEntry me = (ModelEntry) getSelectedEntry();
				}
			}
		};
		arrangeAction.setText("Arrange");
		arrangeAction.setToolTipText("Arrange package hierarchy for sub-models");
		// arrangeAction.setImageDescriptor(ImageDescriptor.createFromFile(
		// TersusEditor.class, "icons/collapseall.gif"));
		moveUnusedAction = new MoveUnuesdAction(getViewer(), false);
		moveUnusedModelsInPackageAction = new MoveUnuesdAction(getViewer(), true);
		removeUnusedAction = new RemoveUnuesdAction(getViewer());

		showServicesAction = new ShowServicesAction(getViewer());
		listUnusedAction = new ListUnusedAction(getViewer());

		openResourceAction = new OpenResourceAction(getSite().getShell());
		closeResourceAction = new CloseResourceAction(getSite().getShell());

		viewer.addSelectionChangedListener(openResourceAction);
		viewer.addSelectionChangedListener(closeResourceAction);
		IWorkspace workspace = TersusWorkbench.getWorkspace();
		workspace.addResourceChangeListener(openResourceAction);
		workspace.addResourceChangeListener(closeResourceAction);

		createPluginActions();

	}

	private void createPluginActions()
	{
		ArrayList actions = new ArrayList();
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint point = registry.getExtensionPoint("tersus.editor.repositoryActions");
		if (point != null)
		{
			IExtension[] extensions = point.getExtensions();
			for (int i = 0; i < extensions.length; i++)
			{
				IExtension extension = extensions[i];
				Bundle bundle = Platform.getBundle(extension.getNamespace());
				IConfigurationElement[] elements = extension.getConfigurationElements();
				for (int j = 0; j < elements.length; j++)
				{
					IConfigurationElement element = elements[j];
					if ("repositoryAction".equals(element.getName()))
					{
						String className = element.getAttribute("class");
						String label = element.getAttribute("label");
						String tooltip = element.getAttribute("tooltip");
						String iconPath = element.getAttribute("icon");
						try
						{
							Class c = bundle.loadClass(className);
							ISelectionAction action = (ISelectionAction) c.newInstance();
							action.setSelectionProvider(viewer);
							actions.add(action);
							action.setText(label);
							action.setToolTipText(tooltip);
							if (iconPath != null)
							{
								URL iconURL = bundle.getResource(iconPath);
								String extendingPluginId = element.getDeclaringExtension()
										.getNamespace();
								action.setImageDescriptor(AbstractUIPlugin
										.imageDescriptorFromPlugin(extendingPluginId, iconPath));
							}
						}
						catch (Exception e)
						{
							TersusWorkbench.log("Failed to create repository action " + className,
									e);
						}
					}
				}
			}
		}
		pluginActions = (ISelectionAction[]) actions.toArray(new ISelectionAction[actions.size()]);

	}

	private void registerActions()
	{
		IActionBars actionBars = getViewSite().getActionBars();

		actionBars.setGlobalActionHandler(ActionFactory.DELETE.getId(), deleteAction);
		actionBars.setGlobalActionHandler(ActionFactory.RENAME.getId(), renameAction);
		actionBars.setGlobalActionHandler(ActionFactory.UNDO.getId(), undoAction);
		actionBars.setGlobalActionHandler(ActionFactory.REDO.getId(), redoAction);
		actionBars.setGlobalActionHandler(TersusActionConstants.SHOW_USAGES_ACTION_TEXT,
				showUsagesAction);
		actionBars.setGlobalActionHandler(ActionFactory.COPY.getId(), copyAction);
		actionBars.setGlobalActionHandler(ActionFactory.PASTE.getId(), pastePackageAction);

		actionBars.setGlobalActionHandler(ActionFactory.PASTE.getId(), copyAction);
		actionBars.setGlobalActionHandler(ActionFactory.PASTE.getId(), pastePackageAction);
		actionBars.setGlobalActionHandler(ActionFactory.PASTE.getId(), moveModelsAction);

		for (int i = 0; i < pluginActions.length; i++)
		{
			ISelectionAction action = pluginActions[i];
			actionBars.setGlobalActionHandler(ActionFactory.ACTIVATE_EDITOR.getId(), action);
		}
		actionBars.updateActionBars();
		fillToolBar(actionBars.getToolBarManager());
		fillPullDownMenu(actionBars.getMenuManager());
	}

	private void fillPullDownMenu(IMenuManager menuManager)
	{
		menuManager.add(revertAction);
		menuManager.add(refreshAction);
		menuManager.add(moveUnusedAction);
		menuManager.add(moveUnusedModelsInPackageAction);
		menuManager.add(removeUnusedAction);
		menuManager.add(listUnusedAction);
		menuManager.add(showServicesAction);

		for (int i = 0; i < pluginActions.length; i++)
		{
			ISelectionAction action = pluginActions[i];
			menuManager.add(action);
		}
	}

	private void initContextMenu()
	{
		MenuManager menuMgr = new MenuManager("#PopupMenu"); //$NON-NLS-1$
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener()
		{
			public void menuAboutToShow(IMenuManager manager)
			{
				fillContextMenu(manager);
			}

		});
		Menu menu = menuMgr.createContextMenu(viewer.getTree());
		viewer.getTree().setMenu(menu);
	}

	protected void fillContextMenu(IMenuManager manager)
	{
		List<?> selection = getSelectedEntries();
		if (selection.size() != 1)
			return;
		Object element = selection.get(0);
		
		MenuManager newMenu = new MenuManager(EditorMessages.getString("RepositoryExplorer.new")); //$NON-NLS-1$
		manager.add(newMenu);
		NewWizardMenu wizardMenu = new NewWizardMenu(getSite().getWorkbenchWindow());
		newMenu.add(wizardMenu);
		manager.add(new Separator());
		
		if (!(element instanceof ProjectEntry))
			manager.add(copyAction);
		
		manager.add(deleteAction);
		manager.add(renameAction);
		manager.add(goToModelByIdAction);
		manager.add(showInNavigatorAction);
		
		if (element instanceof PackageEntry)
			manager.add(pastePackageAction);
		if (element instanceof ModelEntry)
		{
			manager.add(new Separator());
			manager.add(moveModelsAction);
			manager.add(showUsagesAction);
		}
		if ((element instanceof FileEntry))
		{
			openFileAction.selectionChanged(new StructuredSelection(((FileEntry) element)
					.getResource()));
			manager.add(openFileAction);

			MenuManager openWithMenu = new MenuManager(EditorMessages
					.getString("RepositoryExplorer.openWith"), OpenActionGroup.OPEN_WITH_ID); //$NON-NLS-1$
			openWithMenu.add(new OpenWithMenu(getSite().getPage(), ((FileEntry) element)
					.getResource()));
			manager.add(openWithMenu);
		}
		if (element instanceof ProjectEntry)
		{
			manager.add(new Separator());
			manager.add(new ImportResourcesAction(getSite().getWorkbenchWindow()));
			manager.add(new ExportResourcesAction(getSite().getWorkbenchWindow()));
			manager.add(new Separator());
			manager.add(openResourceAction);
			manager.add(closeResourceAction);
		}
		
		boolean pluginActionExists = false;
		for (int i = 0; i < pluginActions.length; i++)
		{
			ISelectionAction action = pluginActions[i];
			if (action.isEnabled())
			{
				if (!pluginActionExists)
				{
					manager.add(new Separator());
					pluginActionExists = true;
				}
				manager.add(action);
			}		
		}
	}

	public void setFocus()
	{
		viewer.getTree().setFocus();
	}

	public TreeViewer getViewer()
	{
		return viewer;
	}

	protected void openEditor(ModelEntry entry)
	{
		if (entry.isFlowModel())
		{
			ConcreteModelIdentifier concreteModelIdentifier = (ConcreteModelIdentifier) entry
					.getAdapter(ConcreteModelIdentifier.class);
			openEditor(concreteModelIdentifier);
		}

	}

	private void openEditor(ConcreteModelIdentifier concreteModelIdentifier)
	{
		System.out.println("Opening " + concreteModelIdentifier.getModelId());
		IWorkbenchPage page = getSite().getPage();
		try
		{
			page.openEditor(concreteModelIdentifier, TersusEditor.ID);
		}
		catch (PartInitException e)
		{
			TersusWorkbench.log(e);
		}
	}

	/**
	 * @return
	 */
	public List<?> getSelectedEntries()
	{
		ISelection selection = getViewer().getSelection();
		if (selection instanceof IStructuredSelection)
		{
			return ((IStructuredSelection) selection).toList();
		}
		else
			return Collections.EMPTY_LIST;
	}

	public Object getSelectedEntry()
	{
		ISelection selection = getViewer().getSelection();
		if (selection instanceof IStructuredSelection)
		{
			if (((IStructuredSelection) selection).size() == 1)
				return ((IStructuredSelection) selection).getFirstElement();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchPart#dispose()
	 */
	public void dispose()
	{
		viewer.getLabelProvider().removeListener(this);
		viewer.removeSelectionChangedListener(selectionChangedListener);
		viewer.removeSelectionChangedListener(openResourceAction);
		viewer.removeSelectionChangedListener(closeResourceAction);
		IWorkspace workspace = TersusWorkbench.getWorkspace();
		workspace.removeResourceChangeListener(openResourceAction);
		workspace.removeResourceChangeListener(closeResourceAction);

		if (viewer.getInput() != null)
			((RootEntry) viewer.getInput()).dispose();
		if (renameAction != null)
		{
			renameAction.dispose();
			renameAction = null;
		}
		if (deleteAction != null)
		{
			deleteAction.dispose();
			deleteAction = null;
		}
		if (undoAction != null)
		{
			undoAction.dispose();
			undoAction = null;
		}
		if (redoAction != null)
		{
			redoAction.dispose();
			redoAction = null;
		}
		if (showUsagesAction != null)
		{
			showUsagesAction.dispose();
			showUsagesAction = null;
		}
		if (showInNavigatorAction != null)
		{
			showInNavigatorAction.dispose();
			showInNavigatorAction = null;
		}
		if (goToModelByIdAction != null)
		{
			goToModelByIdAction.dispose();
			goToModelByIdAction = null;
		}
		
		for (int i = 0; i < pluginActions.length; i++)
		{
			ISelectionAction action = pluginActions[i];
			action = null;
		}
		super.dispose();
	}

	public Object getAdapter(Class adapter)
	{
		if (CommandStack.class.equals(adapter))
			return getCurrentCommandStack();
		return super.getAdapter(adapter);
	}

	/**
	 * @return the command stack of the project of the currently selected entry (or first selected
	 *         entry if there are several selected entries)
	 */
	public CommandStack getCurrentCommandStack()
	{
		if (getSelectedEntries().isEmpty())
			return null;
		else
			return ((ExplorerEntry) getSelectedEntries().get(0)).getEditDomain().getCommandStack();
	}

	public void updateActions()
	{
		if (getSite() == null || getSite().getShell() == null || getSite().getShell().isDisposed())
			return;
		renameAction.update();
		showUsagesAction.update();
		deleteAction.update();
		undoAction.update();
		redoAction.update();
		revertAction.update();
		copyAction.update();
		pastePackageAction.update();
		moveModelsAction.update();
		moveUnusedAction.update();
		showServicesAction.update();
		moveUnusedModelsInPackageAction.update();
		removeUnusedAction.update();
		listUnusedAction.update();
		showInNavigatorAction.update();
		goToModelByIdAction.update();
		
		for (int i = 0; i < pluginActions.length; i++)
		{
			pluginActions[i].update();
		}
	}

	public void updateDirty()
	{
		firePropertyChange(IEditorPart.PROP_DIRTY);
	}

	public void labelProviderChanged(LabelProviderChangedEvent event)
	{
		RootEntry root = (RootEntry) viewer.getInput();
		if (root == null)
			return; // Nothing to do if there's no input
		Object[] elements = event.getElements();
		if (elements != null)
		{
			for (int i = 0; i < elements.length; i++)
			{
				Object object = elements[i];
				if (object instanceof ConcretePackageIdentifier)
				{
					ExplorerEntry entry = root.findLoadedEntry(((ConcretePackageIdentifier) object)
							.getPath(), 0, PackageEntry.class);
					if (entry != null)
						entry.requestShallowRefresh();
				}

			}
		}
	}
}
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.explorer;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;

/**
 * @author Youval Bronicki
 *
 */
public class FileEntry extends ExplorerEntry
{
    public Object getAdapter(Class adapter)
    {
        if (adapter.isAssignableFrom(IFile.class))
            return file;
        else
            return super.getAdapter(adapter);
    }
    private IFile file;
    /**
     * @param parent
     */
    public FileEntry(ExplorerEntry parent, IFile file)
    {
        super(parent);
        this.file = file;
    }

    /* (non-Javadoc)
     * @see tersus.editor.explorer.ExplorerEntry#loadChildren()
     */
    protected ExplorerEntry[] loadChildren()
    {
        return NO_CHILDREN;
    }

    /* (non-Javadoc)
     * @see tersus.editor.explorer.ExplorerEntry#getName()
     */
    public String getName()
    {
        return file.getName();

    }

    public IResource getResource()
    {
        return file;
    }
    public String getText()
    {
        return getName();
    }
    public String toString()
    {
        return file.getFullPath().toString();
    }
}

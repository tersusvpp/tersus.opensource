/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.explorer;

import org.eclipse.core.resources.IProject;
import org.eclipse.gef.ui.actions.UpdateAction;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;

import tersus.editor.RepositoryManager;
import tersus.editor.TersusEditor;

/**
 * @author Youval Bronicki
 *  
 */
public class RevertAction extends Action implements UpdateAction
{
    private TreeViewer viewer;
    IProject selectedProject = null;

    /**
     * @param viewer
     */
    public RevertAction(TreeViewer viewer)
    {
        super();
        this.viewer = viewer;
        setId("tersus.explorer.revert");
        setText("Revert Project - discard unsaved changes");
    }
    public TreeViewer getViewer()
    {
        return viewer;
    }
    
    public void run()
    {
        if (selectedProject != null)
        {
            RepositoryManager manager = RepositoryManager.getRepositoryManager(selectedProject);
            if (manager.getCommandStack().isDirty())
            {
                if ( MessageDialog.openConfirm(viewer.getControl().getShell(), "Confirm revert", "Are you sure you want to discard all unsaved changes in project '"+selectedProject.getName()+"'"))
                    manager.revert();
            }
            else
                manager.refresh();
            
        }
    }
    
    public void update()
    {
        ISelection selection = viewer.getSelection();
        ExplorerEntry selectedEntry = null;
        if (selection instanceof IStructuredSelection)
            selectedEntry = (ExplorerEntry)((IStructuredSelection)selection).getFirstElement();
        if (selectedEntry != null)
        {
            selectedProject = selectedEntry.getProject();
            setText("Revert Project '"+selectedProject.getName()+"'");
        }
        else
        {
            selectedProject = null;
            setText("Revert Project");
        }
        
    }
    
    public boolean calculateEnabled()
    {
        return selectedProject != null;
    }

}
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.explorer;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.model.IWorkbenchAdapter;

import tersus.editor.RepositoryManager;
import tersus.model.Path;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 *  
 */
public abstract class ExplorerEntry implements IAdaptable
{
    private ExplorerEntry parent;

    protected ExplorerPart part;
    
    private boolean needsRefresh = true;

    public void activate()
    {
    };

    public void dispose()
    {
        ExplorerEntry[] loadedChildren = getLoadedChildren();
        for (int i = 0; i < loadedChildren.length; i++)
        {
            loadedChildren[i].dispose();
        }
    };

    protected static final ExplorerEntry[] NO_CHILDREN = new ExplorerEntry[0];

    private ExplorerEntry[] children = null;

    protected ExplorerEntry(ExplorerEntry parent)
    {
        this.parent = parent;
        if (parent != null)
        {
            setProject(parent.getProject());
            this.part = parent.part;
        }
    }

    public boolean hasChildren() throws CoreException
    {
        return false;
    }

    public Object[] getChildren()
    {
        if (needsRefresh)
        {
            refreshChildren();
        }
        needsRefresh = false;
        if (children == null)
            return NO_CHILDREN;
        else
            return children;
    }

    protected void clearChildren()
    {
        children = null;
    }

    public ExplorerEntry[] getLoadedChildren()
    {
        if (children != null)
            return children;
        else
            return NO_CHILDREN;
    }

    public void refreshChildren()
    {
        try
        {
            ExplorerEntry[] old = getLoadedChildren();
            children = loadChildren();
            for (int i = 0; i < children.length; i++)
            {
                boolean isNew = true;
                for (int j = 0; j < old.length; j++)
                {
                    if (children[i].equals(old[j]))
                    {
                        children[i] = old[j];
                        old[j] = null;
                        isNew = false;
                    }
                }
                if (isNew)
                    children[i].activate();
            }
            for (int j = 0; j < old.length; j++)
            {
                if (old[j] != null)
                    old[j].dispose();
            }
        }
        catch (Exception e)
        {
            TersusWorkbench.log(e);
            children = null;
        }
    }

    protected abstract ExplorerEntry[] loadChildren();

    public ExplorerEntry getParent()
    {
        return parent;
    }

    public Image getImage()
    {
        IResource resource = getResource();
        if (resource == null)
            return null;
        IWorkbenchAdapter adapter = (IWorkbenchAdapter) resource
                .getAdapter(IWorkbenchAdapter.class);
        if (adapter == null)
            return null;
        ImageDescriptor descriptor = adapter.getImageDescriptor(resource);
        Image image = TersusWorkbench.getImage(descriptor);
        return image;
    }

    public String getText()
    {
        return null;
    }

    public WorkspaceRepository getRepository()
    {
        if (getProject() == null)
            return null;
        RepositoryManager editDomain = getEditDomain();
        return editDomain.getRepository();
    }

    /**
     * @return
     */
    public RepositoryManager getEditDomain()
    {
        if (getProject() == null)
            return null;
        RepositoryManager editDomain = RepositoryManager
                .getRepositoryManager(getProject());
        return editDomain;
    }

    private IProject project;

    /**
     * @param project
     *            The project to set.
     */
    protected void setProject(IProject project)
    {
        this.project = project;
    }

    /**
     * @return Returns the project.
     */
    public IProject getProject()
    {
        return project;
    }

    /**
     * @return the IResource with which this entry is assoicated, or null if the
     *         entry is not directly associated with a workspace resource.
     */
    public IResource getResource()
    {
        return null;
    }

    /**
     * @param string
     * @return
     */
    public abstract String getName();

    /**
     * @param path -
     *            the path that identifies the entry
     * @return
     */
    public ExplorerEntry findLoadedEntry(Path path, int index, Class classValue)
    {
    	String childName = path.getSegment(index).toString();
        ExplorerEntry[] children = getLoadedChildren();
        for (int i = 0; i < children.length; i++)
        {
            ExplorerEntry entry = children[i];
            if (childName.equals(entry.getName()))
            {
           		if (entry.getClass().equals(classValue))
           		{
	            	if (index + 1 == path.getNumberOfSegments())
               			return entry;
	            	else
            			return entry.findLoadedEntry(path, index + 1, classValue);
            	}
            }		
        }
        
        return null;
    }

    /**
     * @return
     */
    public boolean isSelected()
    {
        ISelection selection = getViewer().getSelection();
        if (selection instanceof IStructuredSelection)
        {
            return ((IStructuredSelection) selection).toList().contains(this);
        }
        else
            return false;
    }

    public TreeViewer getViewer()
    {
        return part.getViewer();
    }

    /**
     * @param resource
     * @return
     */
    public boolean owns(IResource resource)
    {
        return resource != null && resource.equals(getResource());
    }
    
    public void requestDeepRefresh()
    {
        requestShallowRefresh();
        ExplorerEntry[] loadedChildren = getLoadedChildren();
        for( int i=0; i< loadedChildren.length; i++)
            loadedChildren[i].requestDeepRefresh();
    }

    public void requestShallowRefresh()
    {
        needsRefresh = true;
    }
    public Object getAdapter(Class adapter)
    {
        if (adapter != null && adapter.isAssignableFrom(this.getClass()))
            return this;
        else
            return null;
    }
}
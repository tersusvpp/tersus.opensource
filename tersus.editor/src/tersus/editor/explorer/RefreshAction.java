/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.explorer;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.TreeViewer;

import tersus.editor.RepositoryManager;
import tersus.editor.TersusEditor;

/**
 * @author Youval Bronicki
 *  
 */
public class RefreshAction extends Action
{
    private TreeViewer viewer;

    /**
     * @param viewer
     */
    public RefreshAction(TreeViewer viewer)
    {
        super();
        this.viewer = viewer;
        setId("tersus.explorer.refresh");
        setText("Refresh all unmodified projects");
        setImageDescriptor(ImageDescriptor.createFromFile(TersusEditor.class, "icons/refresh.gif"));
    }

    public void run()
    {
        ExplorerEntry root = (ExplorerEntry) getViewer().getInput();
        if (root instanceof RootEntry)
        {
            root.requestDeepRefresh();
            RepositoryManager.refreshAll();
            viewer.refresh();
        }
    };

    public TreeViewer getViewer()
    {
        return viewer;
    }
}
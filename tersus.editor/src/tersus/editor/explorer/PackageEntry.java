/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.explorer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import tersus.editor.TersusEditor;
import tersus.model.Model;
import tersus.model.Package;
import tersus.model.PackageId;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 * 
 */
public class PackageEntry extends ExplorerEntry implements IAdaptable
{

	private static final ImageDescriptor PACKAGE_ICON_DESCRIPTOR = ImageDescriptor.createFromFile(
			TersusEditor.class, "icons/package 16x16.gif");
	private static final ImageDescriptor LIBRARY_PACKAGE_ICON_DESCRIPTOR = ImageDescriptor
			.createFromFile(TersusEditor.class, "icons/library package 16x16.gif");

	public String toString()
	{
		return String.valueOf(packageId);
	}

	private PackageId packageId;

	/**
	 * @param folder
	 */
	public PackageEntry(ExplorerEntry parent, PackageId id)
	{
		super(parent);
		this.setPackageId(id);
	}

	@SuppressWarnings("unchecked")
	public ExplorerEntry[] loadChildren()
	{
		WorkspaceRepository repository = getRepository();
		List<PackageId> subPackageIds = repository.getSubPackageIds(getPackageId());
		List<Model> models = new ArrayList<Model>();
		Package pkg = repository.getPackage(getPackageId());
		if (pkg != null)
			models.addAll(pkg.getModels());

		Collections.sort(models, new Comparator()
		{
			public int compare(Object o1, Object o2)
			{
				Model m1 = (Model) o1;
				Model m2 = (Model) o2;
				return m1.getName().compareTo(m2.getName());
			}
		});

		List<IFile> files = repository.getFiles(getPackageId());
		int numberOfChildren = subPackageIds.size() + models.size() + files.size();
		if (numberOfChildren == 0)
			return NO_CHILDREN;
		ExplorerEntry[] children = new ExplorerEntry[numberOfChildren];
		int counter = 0;
		for (int i = 0; i < models.size(); i++)
		{
			children[counter++] = new ModelEntry(this, (Model) models.get(i));
		}
		for (int i = 0; i < subPackageIds.size(); i++)
		{
			children[counter++] = new PackageEntry(this, (PackageId) subPackageIds.get(i));
		}
		for (int i = 0; i < files.size(); i++)
		{
			children[counter++] = new FileEntry(this, (IFile) files.get(i));
		}
		return children;
	}

	public boolean hasChildren()
	{
		return (getChildren().length > 0);
	}

	public Image getImage()
	{
		if (getRepository().isLibraryPackage(getPackageId()))
			return TersusWorkbench.getImage(LIBRARY_PACKAGE_ICON_DESCRIPTOR);
		else
			return TersusWorkbench.getImage(PACKAGE_ICON_DESCRIPTOR);
	}

	public String getText()
	{
		return getPackageId().getName();
	}

	public boolean equals(Object other)
	{
		if (other instanceof PackageEntry)
			return ((PackageEntry) other).getProject().equals(getProject())
					&& ((PackageEntry) other).getPackageId().equals(getPackageId());
		else
			return false;
	}

	/**
	 * @param packageId
	 *            The packageId to set.
	 */
	private void setPackageId(PackageId packageId)
	{
		this.packageId = packageId;
	}

	/**
	 * @return Returns the packageId.
	 */
	public PackageId getPackageId()
	{
		return packageId;
	}

	public String getName()
	{
		return packageId.getName();
	}

	public IResource getResource()
	{
		return getFolder();
	}

	public IFolder getFolder()
	{
		return getRepository().getPackageFolder(getPackageId());
	}

	public IFile getFile()
	{
		return getRepository().getPackageFile(getPackageId());
	}
}
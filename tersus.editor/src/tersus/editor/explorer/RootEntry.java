/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.explorer;

import java.util.ArrayList;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;

import tersus.workbench.TersusWorkbench;

/**
 * @author Youval Bronicki
 *  
 */
public class RootEntry extends ExplorerEntry
{

    /**
     * @param parent
     */
	/*
	 * Note:  This implementation treats Explorer entries as part of the controller layer and not part of the model layer
	 *        , so they are specific to an ExplorerPart.
	 *        An alternative would be to treat them as belonging to an intermediate level of the model layer
	 *        and then they would not hold a reference to the part (or viewer), but would instead fire
	 *        events that cause the viewer to refresh itself.
	 *        
	 *        Currently (2009-02-19), there is no good reason to change this. 
	 */
    protected RootEntry(ExplorerPart part)
    {
        super(null);
        this.part = part;
    }

    IWorkspaceRoot root = TersusWorkbench.getWorkspace().getRoot();

    public ExplorerEntry[] loadChildren()
    {
        IProject[] projects = root.getProjects();
        ArrayList childList = new ArrayList();
        for (int i = 0; i < projects.length; i++)
        {
            IProject project = projects[i];
            if (TersusWorkbench.isTersusProject(project))
                childList.add(new ProjectEntry(this,project));
        }
        ExplorerEntry[] children = (ExplorerEntry[])childList.toArray(new ExplorerEntry[childList.size()]);
        return children;
    }

    public boolean hasChildren()
    {
        return (root.getProjects().length > 0);
    }
    
    public boolean equals(Object other)
    {
        if (other instanceof RootEntry)
            return root.equals(((RootEntry)other).root);
        else
            return false;
    }
    public String getName()
    {
        return null;
    }
    public IResource getResource()
    {
        return root;
    }
}
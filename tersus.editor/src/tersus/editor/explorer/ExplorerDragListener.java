/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.explorer;

import java.util.List;

import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.internal.ViewSite;

import tersus.editor.outline.ModelObjectTransfer;
import tersus.model.utils.DragListener;

/**
 * @author Youval Bronicki
 * @author Liat Shiff
 */
public class ExplorerDragListener extends DragListener
{
    private ExplorerPart explorer;

    public ExplorerDragListener(ExplorerPart explorer)
    {
        this.explorer = explorer;
    }

    public void dragStart(DragSourceEvent event)
    {
        List entries = explorer.getSelectedEntries();
/*        List modelEntries = new ArrayList(entries.size());
        for (int i = 0; i < entries.size(); i++)
        {
            if (entries.get(i) instanceof ModelEntry)
                modelEntries.add(entries.get(i));
        }*/
        ModelObjectTransfer.getInstance().setObject(entries);
        event.doit = entries.size() > 0;
    }
    
    @Override
	public void dragFinished(DragSourceEvent event) 
	{
    	IWorkbenchPartSite site = explorer.getSite();
    	
    	if (site instanceof ViewSite)
    		((ViewSite)site).getActionBars().getStatusLineManager().setErrorMessage(null);
    	
		ModelObjectTransfer.getInstance().setObject(null);
	}
}
/************************************************************************************************
 * Copyright (c) 2003-2019 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import org.eclipse.jface.action.ContributionManager;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IContributionManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.internal.provisional.action.ICoolBarManager2;
import org.eclipse.jface.internal.provisional.action.ToolBarContributionItem2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PerspectiveAdapter;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.CoolBarToTrimManager;
import org.eclipse.ui.internal.Perspective;
import org.eclipse.ui.internal.WorkbenchPage;
import org.eclipse.ui.internal.WorkbenchWindow;
import org.eclipse.ui.internal.registry.IActionSetDescriptor;
import org.eclipse.ui.services.ISourceProviderService;

/**
 * 
 * @author Alberto Romo Valverde
 *
 */
public class TersusPerspectiveAdapter extends PerspectiveAdapter
{
	@Override
    public void perspectiveActivated(IWorkbenchPage page,
            IPerspectiveDescriptor perspectiveDescriptor) {
        super.perspectiveActivated(page, perspectiveDescriptor);
        
        if (perspectiveDescriptor.getId().equals("tersus.editor.TersusModelingPerspective"))
		{
			page.hideActionSet("org.eclipse.ui.NavigateActionSet");
			page.hideActionSet("org.eclipse.ui.actionSet.openFiles");
		}
		// The purpose of the following commented-out code seems to be "Removing unused shortcuts from Tersus Modeling perspective"
		// See r1646
		// It has been commented out because it is not compatible with Eclipse 4.3.1
		
		/*							WorkbenchPage worbenchPage = (WorkbenchPage) workbenchWindow
				.getActivePage();
        page.hideActionSet("org.eclipse.search.searchActionSet");
		Perspective perspective = worbenchPage
				.findPerspective(perspectiveDescriptor);
		if (perspective != null
				&& "tersus.editor.TersusModelingPerspective".equals(perspective
						.getDesc().getId()))
		{
			IActionSetDescriptor[] set = perspective.getAlwaysOnActionSets();
			for (IActionSetDescriptor actionSetDescriptor : set)
			{
				if (!SEARCH_PERSPECTIVE_LABEL.equals(actionSetDescriptor.getLabel())
						&& !KEYBOARD_SHORTCUTS_LABEL.equals(actionSetDescriptor.getLabel())
						&& !CHEAT_SHEETS.equals(actionSetDescriptor.getLabel()))
					perspective.turnOffActionSet(actionSetDescriptor);
			}
		}
		*/
        
        boolean tersusPerspective = false;
        if (perspectiveDescriptor
                .getId()
                .equals("tersus.editor.TersusModelingPerspective") 
                || 
                perspectiveDescriptor
                .getId()
                .equals("tersus.debug.TersusDebuggingPerspective")) {
        	tersusPerspective = true;
        }
 
        IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow(); 
        WorkbenchWindow wWindow = ((WorkbenchWindow) window);
        
        ISourceProviderService service = (ISourceProviderService) window.getService(ISourceProviderService.class); 
        TersusPerspectiveSourceProvider sourceProvider = (TersusPerspectiveSourceProvider) service.getSourceProvider(TersusPerspectiveSourceProvider.tersusPerspectiveKey); 
        if (tersusPerspective)
            sourceProvider.perspectiveChanged(true);
        else
            sourceProvider.perspectiveChanged(false);
        
        try{
            for (IContributionItem element : wWindow.getActionBars().getCoolBarManager().getItems()) {
            	/*if(element.getId().equals("org.eclipse.ui.workbench.file")){
            		Class cls = element.getClass();
            		Method getManager = cls.getMethod("getToolBarManager", null);
					ToolBarManager tbm = (ToolBarManager) getManager.invoke(element, null);
					tbm.find("print.ext").setVisible(tersusPerspective ? false : true);
					tbm.find("build.group").setVisible(tersusPerspective ? false : true);
					tbm.find("print").setVisible(tersusPerspective ? false : true);
					tbm.update(true);
            	}*/
            	
            	if(tersusPerspective){
            		if(element.getId().equals("Tersus")){
                		Class cls = element.getClass();
                		Method getManager = cls.getMethod("getToolBarManager", null);
    					ToolBarManager tbm = (ToolBarManager) getManager.invoke(element, null);
    					//tbm.find("tersus.appserver.ui.quick_hosting").setVisible(false);
    					tbm.update(true);
    					if(!(tbm != null && tbm.getControl() != null && tbm.getControl().getItem(0) != null
    							&& tbm.getControl().getItem(0).getStyle() == SWT.SEPARATOR)){
    						Separator startSeparator = new Separator();
    						startSeparator.fill(tbm.getControl(), 0);
    						Separator endSeparator = new Separator();
    						endSeparator.fill(tbm.getControl(),-1);
    					}
                	}
            	}
            	
            	/*if(element.getId().equals("org.eclipse.search.searchActionSet")){
            		
            	}*/
            	if(element.getId().equals("org.eclipse.ui.workbench.navigate")){
            		Class cls = element.getClass();
            		Method getManager = cls.getMethod("getToolBarManager", null);
					ToolBarManager tbm = (ToolBarManager) getManager.invoke(element, null);
					tbm.find("history.group").setVisible(tersusPerspective ? false : true);
					tbm.find("org.eclipse.ui.edit.text.gotoNextAnnotation").setVisible(tersusPerspective ? false : true);
					tbm.find("org.eclipse.ui.edit.text.gotoPreviousAnnotation").setVisible(tersusPerspective ? false : true);
					tbm.find("org.eclipse.ui.edit.text.gotoLastEditPosition").setVisible(tersusPerspective ? false : true);
					tbm.update(true);
            	}
            	/*if(element.getId().equals("org.eclipse.ui.workbench.help")){
            		
            	}*/
            	
            	if(element.getId().equals("org.eclipse.debug.ui.launchActionSet")){
            		Class cls = element.getClass();
            		Method getManager = cls.getMethod("getToolBarManager", null);
					ToolBarManager tbm = (ToolBarManager) getManager.invoke(element, null);
					for(IContributionItem item : tbm.getItems()){
    					if(item != null){
    						item.setVisible(tersusPerspective ? false : true);
    					}
    				}
					tbm.update(true);
					element.setVisible(tersusPerspective ? false : true);
					element.update();
            	}
            }
    	}catch(Exception e){
    		// Cannot contribute separators
    		e.printStackTrace();
    	}
        
        try{
        	wWindow.getCoolBarManager2().update(true);
        }catch(Exception e){
        	e.printStackTrace();
        }
    }
     
    public void perspectiveDeactivated(IWorkbenchPage page,
            IPerspectiveDescriptor perspective) {
    }
}

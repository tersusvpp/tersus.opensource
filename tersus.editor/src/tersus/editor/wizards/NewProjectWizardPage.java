/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.wizards;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import tersus.eclipse.Extensions;
import tersus.editor.EditorMessages;
import tersus.editor.EditorPlugin;
import tersus.model.FileRepository;
import tersus.model.PackageId;
import tersus.workbench.TersusWorkbench;

/**
 * @author Youval Bronicki
 * @author Liat Shiff
 */
public class NewProjectWizardPage extends WizardPage implements IWizardPage
{
    private static final String LABEL_KEY = "Label";

    private static final String DEFAULT_TEMPLATE = "Blank View";
    
//    private static final String DEFAULT_THEME = "Default";
    
    private Combo projectTemplateChooser;

//    private Combo themeChooser;

    private Text objectName;

    private Composite container;

    private File defaultProjectTemplate;
    
    private List<File> projectTemplateList = new ArrayList<File>();
    
//    private ArrayList<String> themes;

    /**
     * @param selection
     */
    public NewProjectWizardPage()
    {
        super("NewProject");
        setTitle(EditorMessages.NewProjectWizard_PageTitle);
        setDescription(EditorMessages.NewProjectWizard_PageDescription);
        setImageDescriptor(ImageDescriptor.createFromURL(EditorPlugin.getDefault().getBundle().getEntry(
                "icons/wizards/New Project.gif")));
    }

    public void createControl(Composite parent)
    {
        container = new Composite(parent, SWT.NULL);
        GridLayout layout = new GridLayout();
        container.setLayout(layout);
        layout.numColumns = 2;
        layout.verticalSpacing = 9;

        createProjectNameTextField("&Project name:");
        projectTemplateChooser = createChooser("&Template");
        initializeProjectTemplateChooser();
//        themeChooser = createChooser("T&heme");
//        initializeThemeChooser();

        initialize();
        setPageComplete(false);
        setControl(container);
    }

    private void createProjectNameTextField(String labelText)
    {
        Label lable = new Label(container, SWT.NULL);
        lable.setText(labelText);
        GridData gd = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_CENTER);
        gd.horizontalSpan = 1;
        lable.setLayoutData(gd);

        objectName = new Text(container, SWT.BORDER | SWT.SINGLE);
        objectName.setLayoutData(new GridData(GridData.FILL_HORIZONTAL | GridData.VERTICAL_ALIGN_CENTER));
        objectName.setData(LABEL_KEY, objectName);

        ModifyListener listener = new ModifyListener()
        {
            public void modifyText(ModifyEvent e)
            {
                dialogChanged();
            }
        };
        objectName.addModifyListener(listener);
    }

    private Combo createChooser(String text)
    {
        Label label = new Label(container, SWT.WRAP);
        label.setText(text + ":");
        GridData gd = new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);
        gd.horizontalSpan = 1;
        label.setLayoutData(gd);

        Combo box = new Combo(container, SWT.DROP_DOWN | SWT.READ_ONLY);
        box.setVisibleItemCount(15);
        box.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.VERTICAL_ALIGN_CENTER));
        box.setBackgroundMode(SWT.NONE);

        SelectionListener listener = new SelectionListener()
        {
            public void widgetDefaultSelected(SelectionEvent e)
            {
                dialogChanged();  
            }

            public void widgetSelected(SelectionEvent e)
            {
                dialogChanged();
            }
        };

        box.addSelectionListener(listener);
        
        return box;
    }
    
    private void initializeProjectTemplateChooser()
    {
  
    	projectTemplateList = new ArrayList<File>();
    	for (File templateFolder: Extensions.getProjectTemplateFolders())
    	{
    		for (File file: templateFolder.listFiles())
    		{
    			if (file.isFile() && file.getName().endsWith(".tersus"))
    				projectTemplateList.add(file);
    		}
    	}
        String[] itemList = new String[projectTemplateList.size()];
        int defaultIndex = -1;
        for (int i = 0; i < projectTemplateList.size(); i++)
        {
            String templateName = projectTemplateList.get(i).getName().replaceAll("[.]tersus$", "");
            itemList[i] = templateName;
            if (DEFAULT_TEMPLATE.equals(templateName))
            {
                defaultProjectTemplate = projectTemplateList.get(i);
                defaultIndex = i;
                getNewProjectWizard().setProjectTemplate(defaultProjectTemplate);
            }
        }
        
        projectTemplateChooser.setItems(itemList);

        if (defaultIndex>=0)
        	projectTemplateChooser.select(defaultIndex);
    }
/*
    private void initializeThemeChooser()
    {
        try
        {
            URL installURL = Platform.getBundle("tersus.appserver").getEntry("/DefaultWebapp");
            URL resolvedURL = FileLocator.resolve(installURL);
            
            String DefaultWebappHomePath = FileLocator.toFileURL(resolvedURL).getFile();
            File themesFolder = new File(DefaultWebappHomePath, "themes");
            
            themes = new ArrayList<String>();
            
            for (File themeFolder:themesFolder.listFiles() )
            {
                if (themeFolder.isDirectory())
                    if (! themeFolder.getName().startsWith(".")) // Ignore .svn and other "Hidden" folders
                        themes.add(themeFolder.getName());
            }
            Collections.sort(themes);
            
            themeChooser.setItems(themes.toArray(new String[themes.size()]));
            getNewProjectWizard().setTheme(DEFAULT_THEME);
            themeChooser.setText(DEFAULT_THEME);
            themeChooser.addModifyListener( new ModifyListener(){

                public void modifyText(ModifyEvent e)
                {
                   dialogChanged();
                }});
        }
        catch (IOException e)
        {
            TersusWorkbench.log(e);
        }
    }
    */
    private void dialogChanged()
    {
        getNewProjectWizard().setObjectName(objectName.getText());
        getNewProjectWizard().setProjectTemplate(getSelectedProjectTemplate());
//        getNewProjectWizard().setTheme(getSelectedTheme());
        getNewProjectWizard().validate();
        setErrorMessage(getNewProjectWizard().getErrorMessage());
        setPageComplete(getNewProjectWizard().isReady());
    }
/*
    private String getSelectedTheme()
    {
        if(themeChooser != null)
            return themeChooser.getText();
        
        return null;
    }
    */
    private File getSelectedProjectTemplate()
    {
        if(projectTemplateChooser != null)
        {
            int selectedItemIndex = projectTemplateChooser.getSelectionIndex();
            
            if(selectedItemIndex == -1)
                return null;
            
            return projectTemplateList.get(selectedItemIndex);
        }
        return null;
    }
    
    private void initialize()
    {
        objectName.forceFocus();
    }

    protected NewProjectWizard getNewProjectWizard()
    {
        return (NewProjectWizard) getWizard();
    }
}
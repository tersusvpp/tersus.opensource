/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.wizards;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import tersus.editor.EditorMessages;

/**
 * @author Youval Bronicki
 *  
 */
public class NewObjectWizardPage extends WizardPage implements IWizardPage
{

    private Text locationText;

    private Text objectName;

    private String objectNameLabelText;

  
    /**
     * @param selection
     */
    public NewObjectWizardPage(String wizardName, String objectNameLabelText, IStructuredSelection selection)
    {
        super(wizardName);
        this.objectNameLabelText = objectNameLabelText;
        setTitle(EditorMessages.getString(wizardName+".pageTitle"));
        setDescription(EditorMessages.getString(wizardName+".pageDescription"));
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
     */
    public void createControl(Composite parent)
    {
        Composite container = new Composite(parent, SWT.NULL);
        addComponents(container);
        initialize();
        setPageComplete(false);
        setControl(container);
    }

    protected void addComponents(Composite container)
    {
        GridLayout layout = new GridLayout();
        container.setLayout(layout);
        layout.numColumns = 3;
        layout.verticalSpacing = 9;
        Label label = new Label(container, SWT.NULL);
        label.setText("&Location:");

        locationText = new Text(container, SWT.BORDER | SWT.SINGLE);
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        locationText.setLayoutData(gd);
        locationText.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e)
            {
                dialogChanged();
            }
        });
        locationText.setEnabled(false);

        Button browseButton = new Button(container, SWT.PUSH);
        browseButton.setText("Browse...");
        browseButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e)
            {
            }
        });
        browseButton.setEnabled(false);
        browseButton.setVisible(false);
        label = new Label(container, SWT.NULL);
        label.setText(objectNameLabelText);

        objectName = new Text(container, SWT.BORDER | SWT.SINGLE);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.grabExcessHorizontalSpace=true;
        objectName.setLayoutData(gd);
        objectName.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e)
            {
                dialogChanged();
            }
        });
        Canvas spacer = new Canvas(container, SWT.NONE);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.heightHint =0;
        gd.widthHint = 0;
        spacer.setLayoutData(gd);
    }

    private void dialogChanged()
    {

        getNewObjectWizard().setObjectName(objectName.getText());
        getNewObjectWizard().validate();
        setErrorMessage(getNewObjectWizard().getErrorMessage());
        setPageComplete(getNewObjectWizard().isReady());
    }

    /**
     *  
     */
    private void initialize()
    {
        locationText.setText(getNewObjectWizard().getLocation());
        objectName.forceFocus();
    }

    protected NewObjectWizard getNewObjectWizard()
    {
        return (NewObjectWizard) getWizard();
    }
}
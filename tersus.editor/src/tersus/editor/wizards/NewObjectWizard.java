/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.wizards;

import org.eclipse.core.resources.IProject;
import org.eclipse.gef.commands.Command;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

import tersus.editor.RepositoryManager;
import tersus.editor.explorer.ExplorerEntry;
import tersus.editor.explorer.PackageEntry;
import tersus.model.PackageId;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 *  
 */
public abstract class NewObjectWizard extends Wizard implements INewWizard
{

    String errorMessage = null;

    protected IStructuredSelection selection;

    private IProject project;

    private PackageId parentPackageId;

    private String objectName;

    private static final String INVALID_CHARACTERS = "/\\?:*<>|\"";

    /**
     *  
     */
    public NewObjectWizard()
    {
        super();
    }

    protected abstract String getObjectNameLabel();
    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.wizard.IWizard#performFinish()
     */
    public boolean performFinish()
    {
        if (! isReady())
            return false;
        Command command = getCommand();
        getEditDomain().getCommandStack().execute(command);
        return true;
    }

    /**
     * @return
     */
    protected abstract Command getCommand();

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench,
     *      org.eclipse.jface.viewers.IStructuredSelection)
     */
    public void init(IWorkbench workbench, IStructuredSelection selection)
    {
        this.selection = selection;
        if (selection.size() == 1)
        {
            Object selectedObject = selection.getFirstElement();
            if (selectedObject instanceof ExplorerEntry)
            {
                IProject selectedProject = ((ExplorerEntry) selectedObject).getProject();
                if (selectedProject.isOpen())
                {
                    setProject(selectedProject);
                    if (selectedObject instanceof PackageEntry)
                        setParentPackageId(((PackageEntry) selectedObject)
                                .getPackageId());
                }
            }
        }
    }

    public void addPages()
    {
        addPage(new NewObjectWizardPage(getWizardName(),getObjectNameLabel(), selection));
    }

    /**
     * @return
     */
    protected abstract String getWizardName();

    /**
     * @return
     */
    public IStructuredSelection getSelection()
    {
        return selection;
    }

    /**
     * @return
     */
    public String getLocation()
    {
        String location = "";
        if (project != null)
        {
            location += project.getName();
            if (parentPackageId != null)
            {
                location += "/" + parentPackageId.getPath();
            }
        }
        return location;
    }

    public String getObjectName()
    {
        return objectName;
    }

    public void setObjectName(String packageName)
    {
        this.objectName = packageName;
    }

    
    protected WorkspaceRepository getRepository()
    {
        return getEditDomain().getRepository();
    }
    private RepositoryManager getEditDomain()
    {
        return RepositoryManager.getRepositoryManager(getProject());
    }

 
    public PackageId getParentPackageId()
    {
        return parentPackageId;
    }

    public void setParentPackageId(PackageId parentPackageId)
    {
        this.parentPackageId = parentPackageId;
    }

    public IProject getProject()
    {
        return project;
    }

    public void setProject(IProject project)
    {
        this.project = project;
    }

    /**
     * @return
     */
    public String getErrorMessage()
    {
        return errorMessage;
    }

    /**
     * @return
     */
    public boolean isReady()
    {
        return errorMessage == null && objectName != null && project != null;
    }

    /**
     * 
     */
    public abstract void validate();

    protected static final String MISSING_LOCATION_ERROR_MESSAGE = "A location must be selected before starting this operation";
}
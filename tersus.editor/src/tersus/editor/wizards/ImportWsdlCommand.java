/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. � Initial API and implementation
 *************************************************************************************************/

package tersus.editor.wizards;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;
import java.util.Vector;

import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import tersus.editor.RepositoryManager;
import tersus.model.BuiltinModels;
import tersus.model.BuiltinPlugins;
import tersus.model.BuiltinProperties;
import tersus.model.Composition;
import tersus.model.DataElement;
import tersus.model.DataType;
import tersus.model.FlowModel;
import tersus.model.Link;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.Path;
import tersus.model.RelativePosition;
import tersus.model.Role;
import tersus.model.Slot;
import tersus.model.SlotType;
import tersus.model.SubFlow;
import tersus.model.commands.MultiStepCommand;
import tersus.util.SpecialRoles;
import tersus.workbench.WorkspaceRepository;

/**
 * 
 * Creating models (for invoking web services) from a WSDL file.
 * 
 * This class does not properly support all theoretical WSDL bindings (e.g. we assume
 * "literal"/"encoded" and "document"/"rpc" distinctions are made at the operation level, not per
 * slot).
 * 
 * For a discussion of WSDL styles, refer to
 * http://www-128.ibm.com/developerworks/webservices/library
 * /ws-whichwsdl/?ca=dgr-devx-WebServicesMVP03.
 * 
 * @author Ofer Brandes
 * 
 */

public class ImportWsdlCommand extends MultiStepCommand
{
	private final boolean CONVERT_COLONS_IN_NAMESPACE = true; // See http://lists.w3.org/Archives/Public/xml-names-editor/2002Nov/0009.html

	private final static int NONE = 0;
	private final static int SOME = 5;
	private final static int FULL = 10;

	private final int DEBUG_LEVEL = FULL;

	private String projectName;

	private String wsdlFilePath;

	public ImportWsdlCommand(String projectName, String wsdlFilePath, WorkspaceRepository repository)
	{
		super(repository, "ImportWsdl");
		this.projectName = projectName;
		this.wsdlFilePath = wsdlFilePath;
	}

	public void run()
	{
		try
		{
			runImport();
		}
		catch (SAXException e)
		{
			Exception cause = e.getException();
			cause.printStackTrace();
			throw new WsdlImportEception("Failed to import WSDL file", cause);
		}
		catch (Exception e)
		{
			throw new WsdlImportEception("Failed to import WSDL file", e);
		}
	}

	/**
	 * 
	 * Creates a data type hierarchy from a WSDL file.
	 * 
	 * Existing models in the target project can be referenced.
	 * 
	 * To avoid inconsistencies and allow for proper 'undo', it is assumed all modified models are
	 * saved prior to invoking this method (and once again once it finishes successfully), but
	 * sometimes several WSDL files may contain mutual references, in which case saving should be
	 * performed only after all them are imported (to avoid unresolved references).
	 * 
	 * Standard SAX parser is used to parse the input WSDL file.
	 * 
	 * @param fileName
	 *            The URI ir name of a file containing the WSDL definitions (relative or absolute
	 *            path).
	 * @param projectName
	 *            The name of the project to which created models are to be added.
	 * 
	 */
	public void runImport() throws IOException, SAXException
	{
		if (DEBUG_LEVEL > NONE)
			System.out.println("\nImporting " + wsdlFilePath);

		XMLReader xr = XMLReaderFactory.createXMLReader();
		WsdlHandler parser = new WsdlHandler();
		try
		{
			xr.setContentHandler(parser);
			xr.setErrorHandler(parser);
			xr.setFeature("http://xml.org/sax/features/namespaces", false);
			InputStream xmlDocument;
			if (wsdlFilePath.startsWith("http"))
			{
				URL url = new URL(wsdlFilePath);
				xmlDocument = url.openStream();
			}
			else
				xmlDocument = new FileInputStream(wsdlFilePath);
			xr.parse(new InputSource(xmlDocument));
		}
		catch (IOException e)
		{
			System.err.println("Failed to process '" + wsdlFilePath + "': " + e.getMessage());
		}
		finally
		{
			parser.dispose();
		}
	}

	/**
	 * 
	 * A SAX handler to parse an WSDL file while creating the hierarchy of its elements.
	 * 
	 */

	// NICE3 Algorithm needs to be better documented (e.g. "memory" implemented through the 'models' stack).
	private class WsdlHandler extends DefaultHandler
	{
		WorkspaceRepository repository = null;

		private Stack<ModelInfo> models = null;
		private int withinBinding = 0;
		private HashSet<ModelRef> missingModels = null;
		private HashMap<ModelRef, ModelRef> incompleteElements = null;
		private HashMap<ModelRef, ModelInfo> incompleteModels = null;
		private HashMap<ModelId, ModelInfo> bindings = null;

		private final String IGNORE = "Ignored";
		private final String COLLECTION_OF_SERVICES = "Collection of Services (Possibly with Various Bindings)";
		private final String EXPOSED_SERVICE_PACKAGE = "Exposed Service Package";
		private final String HTTP_LOCATION = "HTTP Location";
		private final String SOAP_LOCATION = "Soap Location";
		private final String WSDL_BINDING = "Binding";
		private final String SOAP_BINDING = "Soap Binding";
		private final String EXPOSED_HTTP_SERVICE = "Exposed HTTP Service";
		private final String EXPOSED_SOAP_SERVICE = "Exposed SOAP Service";
		private final String SERVICE_PACKAGE = "Service Package";
		private final String SERVICE = "Service";
		private final String SOAP_HEADER_SLOT = "Soap Header Slot";
		private final String TRIGGER = "Trigger";
		private final String EXIT = "Exit";
		private final String SLOT_TYPE = "Slot Type";
		private final String SLOT_TYPE_ELEMENT = "Slot Type Element";
		private final String ATOMIC = "Atomic Data Type";
		private final String COMPOSITE = "Composite Data Type";
		private final String ATTRIBUTES_DEFINITION = "Attribute Definition";
		private final String COMPOSITION_DEFINITION = "Composition Definition";
		private final String ANYTHING = "Anything"; // Need to implement!
		private final String ELEMENT = "Element";
		private final String ELEMENT_SET = ELEMENT + "(s)";
		private final String ATTRIBUTE = "Attribute";
		private final String ANY_ATTRIBUTE = "Any Attribute"; // Need to implement!
		private final String ATTRIBUTE_GROUP = "Attribute Group"; // Need to implement!
		private final String BASE = "Base";
		private final String PROPERTY = "Property";

		private WsdlHandler()
		{
			super();

			IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
			repository = RepositoryManager.getRepositoryManager(root.getProject(projectName))
					.getRepository();

			missingModels = new HashSet<ModelRef>();
			incompleteElements = new HashMap<ModelRef, ModelRef>();
			incompleteModels = new HashMap<ModelRef, ModelInfo>();
			bindings = new HashMap<ModelId, ModelInfo>();
		}

		public void dispose() // Done
		{
		}

		/**
		 * 
		 * A model (one of the models created while parsing the WSDL file) and all the elements that
		 * have been encountered under it.
		 * 
		 */
		private class ModelInfo
		{
			private HashMap<String, String> nameSpaces; //Not common through out the parsing
			private String targetNameSpace = null;
			private String elementType;	// Type of Schema/WSDL element (e.g. "message")
			private String name;		// Relative name (role), defined by the 'name' attribute of the element
			private String modelType;	// SERVICE_PACKAGE (portType), SERVICE (operation), TRIGGER (input), ...
			private ModelRef modelRef;	// 3 fields:
			// ModelId modelId;			// Model Id (Package Name & Model Name)
			// String xmlNameSpace;		// Name space used to create 'modelId'
			// Role elementRole;		// Only when referring to an element of the model
			private ModelId wrappedPackageId; // Only for WSDL_BINDING
			private String soapAction;	// Only within WSDL_BINDING
			private String style;		// Only within WSDL_BINDING
			private String use;			// Only for slots
			private Model model;
			private int minOccurs, maxOccurs;
			private Vector<ModelInfo> elements;

			private ModelInfo(HashMap<String, String> nameSpaces, String targetNameSpace, String wsdlType, String packageName,
					String name, String shortXmlType, boolean isReusableElement, String style,
					int minOccurs, int maxOccurs)
			{
				// String shortXmlType; // XML type (e.g. "http://www.w3.org/2001/XMLSchema:string"), defined by the 'type' (or 'name') attribute of the element
				this.nameSpaces = nameSpaces;
				this.targetNameSpace = targetNameSpace;
				this.elementType = wsdlType;
				this.name = name;
				this.modelType = getModelType(wsdlType);
				if (WSDL_BINDING.equals(modelType))
				{
					this.modelRef = getModelRef(nameSpaces, targetNameSpace, packageName, name, null, modelType, false);
					this.wrappedPackageId = getModelRef(nameSpaces, targetNameSpace, packageName, name,	shortXmlType, modelType, false).modelId; // Patchy, but seems to work fine
				}
				else
				{
					this.modelRef = getModelRef(nameSpaces, targetNameSpace, packageName, name, shortXmlType, modelType, isReusableElement);
					this.wrappedPackageId = null;
				}
				this.style = style;
				this.model = (modelRef.modelId == null ? null : repository.getModel(
						modelRef.modelId, true));
				this.minOccurs = minOccurs;
				this.maxOccurs = maxOccurs;
				this.elements = new Vector<ModelInfo>();
			}

			private ModelId tersusType()
			{
				ModelId tersusType = null;

				for (int i = 0; i < elements.size(); i++)
				{
					ModelInfo child = (ModelInfo) elements.elementAt(i);
					if (BASE.equals(child.modelType))
					{
						tersusType = child.modelRef.modelId; // Bug(?) - Model might not exist yet due to order of processing
						break;
					}
				}

				if (tersusType == null)
				{
					System.err.println("No base type for '" + name + "'"); // Full XML type instead of 'name'?
					tersusType = BuiltinModels.TEXT_ID;
				}

				return tersusType;
			}

			public String toString()
			{
				String children = (elements.size() == 0 ? " (no children)" : " (" + elements.size()
						+ " children)");
				return (modelType + " " + modelRef.modelId + children);
			}
		}

		/**
		 * Handles a notification of the start of an XML element.
		 * 
		 * The start of an XML element triggers the creation of a model with the same name.
		 * 
		 * @param uri
		 *            Namespace URI (used for the name of the created model).
		 * @param name
		 *            Element's local name (used for the name of the created model).
		 * @param qName
		 *            Element's raw XML 1.0 name.
		 * @param atts
		 *            The element's attributes.
		 */
		public void startElement(String uri, String name, String qName, Attributes atts)
		{
			if (models == null)
				models = new Stack<ModelInfo>(); // NICE2 Move to the constructor?

			HashMap<String, String> nameSpaces = new HashMap<String, String>();
			
			String packageName = null;

			// Current model is a child of the enclosing model
			ModelInfo parent = null;
			if (!models.empty())
			{
				parent = models.peek();
				if (SERVICE_PACKAGE.equals(parent.modelType))
					packageName = parent.modelRef.modelId.toString();
				else if (WSDL_BINDING.equals(parent.modelType))
					packageName = parent.wrappedPackageId.toString();

				if (parent.nameSpaces.size() > 0)
					nameSpaces.putAll(parent.nameSpaces);
			}

			if (nameSpaces.size() == 0)
				nameSpaces.put("xml", "http://www.w3.org/XML/1998/namespace"); // See http://www.w3.org/XML/1998/namespace.html

			String nameSpace = null;
			for (int i = 0; i < atts.getLength(); i++)
			{
				String attributeName = atts.getQName(i);
				String attributeValue = atts.getValue(i);
				if (attributeName.startsWith("xmlns:"))
					nameSpaces.put(attributeName.substring("xmlns:".length()), attributeValue);
				if ("xmlns".equals(attributeName))
				{
					nameSpace = attributeValue;
					nameSpaces.put("", attributeValue);
				}
			}

			String elementType = elementName(nameSpaces, uri, name, qName, nameSpace);
			if (DEBUG_LEVEL > NONE)
				System.out.println("Start element: " + elementType);

			String targetNameSpace = null;
			String wsdlElementName = null;
			String shortXmlType = null;
			String headXmlType = null; // The type referred by a 'substitutionGroup' attribute of a reusable element
			boolean isReusableElement = false;
			int minOccurs = 1;
			int maxOccurs = 1;

			// Some XML attributes contain important information (others are currently ignored)
			for (int i = 0; i < atts.getLength(); i++)
			{
				String attributeName = atts.getQName(i);
				String attributeValue = atts.getValue(i);
				if (DEBUG_LEVEL > SOME)
					System.out.println("\t" + attributeName + " = " + attributeValue);

				// Namespace, Name and Type
				if ("targetNamespace".equals(attributeName))
					targetNameSpace = attributeValue;
				if ("name".equals(attributeName))
					wsdlElementName = attributeValue;
				if ("ref".equals(attributeName)) // Implies both type & name
				{
					shortXmlType = attributeValue;
					if (wsdlElementName == null)
					{
						int p = attributeValue.lastIndexOf(':');
						wsdlElementName = (p >= 0 ? attributeValue.substring(p + 1)
								: attributeValue);
					}
					if ("http://www.w3.org/2001/XMLSchema:element".equals(elementType)
							|| "http://www.w3.org/2001/XMLSchema:attribute".equals(elementType))
						isReusableElement = true;
				}
				if ("element".equals(attributeName)
						&& "http://schemas.xmlsoap.org/wsdl/:part".equals(elementType))
				{
					shortXmlType = attributeValue;
					isReusableElement = true;
				}
				if (("type".equals(attributeName) && ("http://www.w3.org/2001/XMLSchema:element".equals(elementType) ||
					 "http://schemas.xmlsoap.org/wsdl/:part".equals(elementType) ||
					 "http://www.w3.org/2001/XMLSchema:attribute".equals(elementType))) ||
					 ("message".equals(attributeName) && ("http://schemas.xmlsoap.org/wsdl/:input".equals(elementType) ||
					 "http://schemas.xmlsoap.org/wsdl/:output".equals(elementType) ||
					 "http://schemas.xmlsoap.org/wsdl/:fault".equals(elementType) ||
					 "http://schemas.xmlsoap.org/wsdl/soap/:header".equals(elementType))) ||
					 ("base".equals(attributeName) && ("http://www.w3.org/2001/XMLSchema:restriction".equals(elementType) ||
					 "http://www.w3.org/2001/XMLSchema:extension".equals(elementType))) ||
					 ("binding".equals(attributeName) && "http://schemas.xmlsoap.org/wsdl/:port".equals(elementType)))
					shortXmlType = attributeValue;
				if ("type".equals(attributeName) && "http://schemas.xmlsoap.org/wsdl/:binding".equals(elementType))
					shortXmlType = attributeValue; // Will be treated specially later
				// Need to differently handle "type" for mime:content
				if ("substitutionGroup".equals(attributeName) && "http://www.w3.org/2001/XMLSchema:element".equals(elementType))
				{
					headXmlType = attributeValue; // Not used yet (should become the value of a property of the reusable element)
				}
				if ("location".equals(attributeName) && ("http://schemas.xmlsoap.org/wsdl/http/:address".equals(elementType) || "http://schemas.xmlsoap.org/wsdl/soap/:address".equals(elementType)))
					wsdlElementName = attributeValue;
				if ("soapAction".equals(attributeName) && "http://schemas.xmlsoap.org/wsdl/soap/:operation".equals(elementType))
				{
					parent.soapAction = attributeValue;

					if (attributeValue.endsWith("/" + parent.name))
						wsdlElementName = attributeValue;
					else if ("".equals(attributeValue))
						wsdlElementName = parent.name;
					else
						wsdlElementName = attributeValue + "/" + parent.name;
				}

				// Style and Use
				if ("style".equals(attributeName)
						&& ("http://schemas.xmlsoap.org/wsdl/soap/:operation".equals(elementType) || "http://schemas.xmlsoap.org/wsdl/soap/:binding".equals(elementType)))
				{
					parent.style = attributeValue; // "document" or "rpc"
				}
				if ("use".equals(attributeName) && (withinBinding > 0))
				{
					if (TRIGGER.equals(parent.modelType) || EXIT.equals(parent.modelType))
						parent.use = attributeValue; // "encoded" or "literal"
				}

				// Multiplicity
				if ("minOccurs".equals(attributeName))
					minOccurs = Integer.decode(attributeValue).intValue();
				if ("maxOccurs".equals(attributeName))
					maxOccurs = ("unbounded".equals(attributeValue) ? Integer.MAX_VALUE : Integer
							.decode(attributeValue).intValue());
			}

			if (targetNameSpace == null)
				if (!models.empty())
					targetNameSpace = (models.peek()).targetNameSpace;

			if ("http://schemas.xmlsoap.org/wsdl/soap/:operation".equals(elementType)
					&& (wsdlElementName == null)) // Missing "soapAction" attribute
			{
				System.err.println(elementType + ": Missing 'soapAction'");
				wsdlElementName = parent.name; // Is it correct?
			}

			// We now have enough information to create an object describing the current model
			String style = (parent == null ? null : parent.style); // Might be overridden later
			ModelInfo modelInfo = new ModelInfo(nameSpaces, targetNameSpace, elementType, packageName,
					wsdlElementName, shortXmlType, isReusableElement, style, minOccurs, maxOccurs);
			models.push(modelInfo);
			if (WSDL_BINDING.equals(modelInfo.modelType))
				withinBinding++;
		}

		/**
		 * Handles a notification of character data inside an XML element.
		 * 
		 * @param ch
		 *            A characters array containing the element's data.
		 * @param start
		 *            The start position in the character array.
		 * @param length
		 *            The number of characters to use from the characters array.
		 */
		public void characters(char ch[], int start, int length)
		{
			String content = (new String(ch, start, length)).trim();
			if (content.length() == 0)
				return;

			// Textual content is not expected
			if (DEBUG_LEVEL > SOME)
				System.err.println("Unexpected textual content: " + content);
		}

		/**
		 * Handles a notification of the end of an XML element.
		 * 
		 * The end of an XML element triggers the creation of a new model and making it an element
		 * of the containing model.
		 * 
		 * If it is the top model, parsing is done.
		 * 
		 * @param uri
		 *            Namespace URI (used for the name of the created model).
		 * @param name
		 *            Element's local name (used for the name of the created model).
		 * @param qName
		 *            Element's raw XML 1.0 name.
		 */
		public void endElement(String uri, String name, String qName)
		{
			ModelInfo modelInfo = (ModelInfo) models.pop();
			if (WSDL_BINDING.equals(modelInfo.modelType))
				withinBinding--;

			if (DEBUG_LEVEL > NONE)
			{
				System.out.println("End element: " + modelInfo.elementType + " " + modelInfo.name);

				if (IGNORE.equals(modelInfo.modelType))
					System.out.println(modelInfo.modelType);
				else if (modelInfo.modelRef.elementRole == null)
					System.out.println("Model: " + modelInfo.modelRef.modelId + " ("
							+ modelInfo.modelType + ")");
				else
					System.out.println("Element: " + modelInfo.modelRef + " ("
							+ modelInfo.modelType + ")");

				System.out.println(modelInfo.elementType + " '" + modelInfo.name + "' has "
						+ modelInfo.elements.size() + " children");
				if (modelInfo.elements.size() > 0)
					System.out.println(modelInfo.elements.toString());
			}

			// If does not exist yet - create model
			if ((withinBinding < 1) && (modelInfo.modelRef.modelId != null)
					&& (modelInfo.model == null))
			{
				boolean saveForLater = false;

				// Attribute
				if (ATTRIBUTE.equals(modelInfo.modelType))
				{
					// - There are 3 cases for attributes:
					// Case 1: <xs:attribute name="lang" type="xs:language">
					// Case 2: <xs:attribute name="space"> <xs:simpleType> ... </xs:simpleType> </xs:attribute>
					// Case 3: <xs:attribute ref="xml:base" />
					// - Cases 1 & 3 do not get here (modelInfo.model != null)
					// - Case 2 should get here only for local attribute (reusable attributes are
					// handled below similarly to reusable elements)
					if (modelInfo.modelRef.modelId.getPackageId().toString().endsWith(
							SpecialRoles.XML_ATTRIBUTE_COLLECTION_NAME))
					{
						// Reusable (expected only under 'attributeGroup', which is currently ignored)
						String id = modelInfo.modelRef.modelId.toString();
					}
					else
					{
						// Local (expected case 2)
						// Example: <xs:attribute name="style" type="soap:tStyleChoice" use="optional" /> (in http://schemas.xmlsoap.org/wsdl/soap/)
						String id = modelInfo.modelRef.modelId.toString();
					}
				}

				// Atomic Data Type
				if (ATOMIC.equals(modelInfo.modelType))
				{
					ModelId tersusType = modelInfo.tersusType();
					modelInfo.model = newDataType(modelInfo.modelRef, tersusType);
					if (modelInfo.modelRef.elementRole != null)
					{
						modelInfo.modelType = ELEMENT; // It was ELEMENT to begin with and changed to ATOMIC to force the creation of the data type
						modelInfo.modelRef.modelId = modelInfo.model.getId();
					}
					else if (modelInfo.modelRef.modelId.getPackageId().toString().endsWith(SpecialRoles.XML_ATTRIBUTE_COLLECTION_NAME))
					{
						modelInfo.modelType = ATTRIBUTE; // It was ATTRIBUTE to begin with and changed to ATOMIC to force the creation of the data type
						modelInfo.modelRef.modelId = modelInfo.model.getId();
					}
				}

				// Composite Data Type
				else if (COMPOSITE.equals(modelInfo.modelType) || SLOT_TYPE.equals(modelInfo.modelType))
				{
					if (getTypeInheritanceInfo(modelInfo) == null)
					{
						modelInfo.model = newDataType(modelInfo.modelRef, BuiltinModels.DATA_STRUCTURE_TEMPLATE_ID);
						addDataElements((DataType) modelInfo.model, modelInfo.name,	modelInfo.modelType, modelInfo.elements);
						if (COMPOSITE.equals(modelInfo.modelType))
							if (modelInfo.modelRef.elementRole != null)
							{
								modelInfo.modelType = ELEMENT; // It was ELEMENT to begin with and changed to COMPOSITE to force the creation of the data structure
								modelInfo.modelRef.modelId = modelInfo.model.getId();
							}
					}
					else
						// If inherited from another type, will be handled later
						saveForLater = true;
				}

				// Service
				else if (SERVICE.equals(modelInfo.modelType))
				{
					modelInfo.model = repository.newModel(modelInfo.modelRef.modelId, BuiltinModels.CALL_WEB_SERVICE_TEMPLATE_ID, true);
					if (DEBUG_LEVEL > NONE)
						System.out.println("-> Created model: " + modelInfo.modelRef.modelId);
					saveForLater = true; // Slots will be added later
				}

				// Collection of Services (Possibly with Various Bindings)
				else if (COLLECTION_OF_SERVICES.equals(modelInfo.modelType))
				{
					saveForLater = true;
				}
				else if (WSDL_BINDING.equals(modelInfo.modelType))
				{
					bindings.put(modelInfo.modelRef.modelId, modelInfo);
				}

				if (saveForLater && (incompleteModels.get(modelInfo.modelRef) == null))
				{
					incompleteModels.put(modelInfo.modelRef, modelInfo);
					if (DEBUG_LEVEL > NONE)
						System.out.println("Model for future completion/use: " + modelInfo.modelRef.modelId);
				}
			}

			// New name for an existing Data Type 1 (creating a model)
			if ((withinBinding < 1) && ATTRIBUTE.equals(modelInfo.modelType))
			{
				ModelInfo parent = (models.empty() ? null : (ModelInfo) models.peek());
				boolean isLocal = (parent != null) && (COMPOSITE.equals(parent.modelType) || BASE.equals(parent.modelType) || ATTRIBUTE_GROUP.equals(parent.modelType)); // ATTRIBUTE_GROUP added on the 25/2/09 to skip the problem of attributes used before defined in http://schemas.xmlsoap.org/wsdl/soap/
				if (!isLocal) // We assume a locally defined attribute cannot be used anywhere else
				{
					// Get the model to which the attribute refers
					ModelRef attributeModelRef = getModelRef(modelInfo.nameSpaces, modelInfo.targetNameSpace, null,
							modelInfo.name, null, modelInfo.modelType, true);
					// Create the model
					if (repository.getModel(attributeModelRef.modelId, true) == null)
						modelInfo.model = newDataType(attributeModelRef, modelInfo.modelRef.modelId);
				}
			}

			// New name for an existing Data Type 2 (creating an element, not a model)
			if ((withinBinding < 1) && ELEMENT.equals(modelInfo.modelType))
			{
				ModelInfo parent = (models.empty() ? null : (ModelInfo) models.peek());
				boolean isLocal = (parent != null) && ELEMENT_SET.equals(parent.modelType);
				if (!isLocal) // We assume a locally defined element cannot be used anywhere else
				{
					// Get the model to which the element refers
					ModelRef elementModelRef = getModelRef(modelInfo.nameSpaces, modelInfo.targetNameSpace, null,
							modelInfo.name, null, modelInfo.modelType, true);
					ModelId referredDataTypeId = null;
					if (modelInfo.modelRef.modelId == null) // Possible if element is just a generic element with no content (e.g. 'anyType' in "http://schemas.xmlsoap.org/soap/encoding/")
					{
						// Creating an ad-hoc data structure just for this element
						Model adhocModel = newDataType(elementModelRef,
								BuiltinModels.DATA_STRUCTURE_TEMPLATE_ID);
						referredDataTypeId = modelInfo.modelRef.modelId = adhocModel.getId();
					}
					else
					{
						referredDataTypeId = modelInfo.modelRef.modelId;
						if (repository.getModel(referredDataTypeId, true) == null)
						{
							System.err.println(elementModelRef + ": No model '"
									+ referredDataTypeId + "' (may be resolved later)");
							missingModels.add(modelInfo.modelRef);	// E.g. "XML/ebay/apis/eBLBaseComponents/(Elements)/InsuranceOption" (for <element name="InsuranceOption" type="ns:InsuranceOptionCodeType">)
							// missingModels.add (elementModelRef);	// E.g. "XML/ebay/apis/eBLBaseComponents/InsuranceOptionCodeType" (ditto)
						}
					}

					// Create the element
					modelInfo.model = createReusableElement(elementModelRef, referredDataTypeId, modelInfo.minOccurs, modelInfo.maxOccurs);
					if (DEBUG_LEVEL > NONE)
						System.out.println("-> Created element: "
								+ idToString(modelInfo.model.getId(), elementModelRef.elementRole));

					incompleteModels.put(elementModelRef, modelInfo); // Will be used later (hopefully)
					if (DEBUG_LEVEL > NONE)
						System.out.println("Element for future use: " + elementModelRef + " ("
								+ modelInfo.modelRef.modelId + ")");
				}
			}

			// Done with the current model
			if (!models.empty())
			{
				ModelInfo parent = (ModelInfo) models.peek();
				boolean anonymousComposite = ELEMENT_SET.equals(modelInfo.modelType);
				boolean anonymousTypeUnderElement = ELEMENT.equals(parent.modelType)
						&& (ATOMIC.equals(modelInfo.modelType) || COMPOSITE.equals(modelInfo.modelType))
						&& (modelInfo.modelRef.modelId == null);
				boolean anonymousTypeUnderAttribute = ATTRIBUTE.equals(parent.modelType)
						&& ATOMIC.equals(modelInfo.modelType)
						&& (modelInfo.modelRef.modelId == null);
				if (anonymousComposite || anonymousTypeUnderElement || anonymousTypeUnderAttribute)
				{
					// Children of current model become the children of the enclosing model
					ModelInfo baseChild = null;
					for (int i = 0; i < modelInfo.elements.size(); i++) // Used to be: parent.elements = modelInfo.elements;
					{
						ModelInfo child = (ModelInfo) modelInfo.elements.elementAt(i);
						child.minOccurs *= modelInfo.minOccurs;
						child.maxOccurs *= modelInfo.maxOccurs;
						if (BASE.equals(child.modelType))
								baseChild = child;
						parent.elements.add(child);
					}

					if (anonymousTypeUnderElement || anonymousTypeUnderAttribute)
					{
						ModelInfo grandParent = (models.size() == 1 ? null : (ModelInfo) models.get(models.size() - 2));
						boolean isLocalElement = anonymousTypeUnderElement
								&& (grandParent != null)
								&& ELEMENT_SET.equals(grandParent.modelType); // Global elements are elements that are immediate children of the "schema" element!
						boolean isLocalAttribute = anonymousTypeUnderAttribute
								&& (grandParent != null)
								&& (COMPOSITE.equals(grandParent.modelType) || BASE.equals(grandParent.modelType));

						if (isLocalElement) // Anonymous type under local element - to be treated as if it were the type attribute of the element (Ofer, 16/11/2013)
						{
							// Using lower level modelRef (no lost of information, since modelInfo.modelRef.modelId == null)
							if (ATOMIC.equals(modelInfo.modelType) && (baseChild != null))
							{
								parent.modelRef = baseChild.modelRef; // baseChild is always expected to be non-null in these cases
							}
							else
							{
								// COMPOSITE not handled yet (might require a different treatment than ATOMIC)
							}
						}
						else
						{
							if (!isLocalAttribute) // Not a local element nor a local attribute - reusable element/attribute
								parent.modelRef = getModelRef(modelInfo.nameSpaces, parent.targetNameSpace, null, parent.name, null, parent.modelType, true);
							parent.modelType = modelInfo.modelType;
						}
					}
				}
				else
				{
					// Current model is an element of the enclosing model
					parent.elements.add(modelInfo);
					if ((modelInfo.use != null) && (withinBinding > 0))
						parent.use = modelInfo.use; // Currently ignoring the possibility of mixed use between messages of the operation
				}
			}

			// Current model is the top model - done with parsing
			else
			{
				// Create complex types inheriting from other (not necessarily complex) types
				Iterator iterator = incompleteModels.values().iterator();
				while (iterator.hasNext())
				{
					modelInfo = (ModelInfo) iterator.next();
					if (COMPOSITE.equals(modelInfo.modelType))
						createInheritedType(modelInfo);
				}

				// Still trying to resolve unresolved references - Models
				if (missingModels.size() > 0)
				{
					if (DEBUG_LEVEL > NONE)
						System.err.println("\nMissing models:");
					int nMissing = 0;

					for (ModelRef missingModelRef : missingModels)
					{
						if (repository.getModel(missingModelRef.modelId, true) == null)
						{
							ModelInfo found = (ModelInfo) incompleteModels.get(missingModelRef);
							if (found != null)
							{
								Model newlyCreatedModel = repository.newModel(
										missingModelRef.modelId, found.modelRef.modelId, true); // E.g. creating "XML/schemas.xmlsoap.org/soap/envelope/(Elements)/Envelope" from "XML/schemas.xmlsoap.org/soap/envelope/Envelope"
								newlyCreatedModel.setProperty(BuiltinProperties.XML_NAME_SPACE,
										missingModelRef.xmlNameSpace);
								if (DEBUG_LEVEL > NONE)
									System.out.println("-> Created model: "
											+ missingModelRef.modelId);
							}
							else
							{
								nMissing++;
								if (DEBUG_LEVEL > NONE)
									System.err.println(nMissing + ". " + missingModelRef.modelId);
							}
						}
					}
					if (nMissing == 0)
					{
						if (DEBUG_LEVEL > NONE)
							System.err.println("None");
					}
					else
					{
						// Throw exception?
					}
					System.err.flush();
				}

				// Still trying to resolve unresolved references - Model references by elements
				if (incompleteElements.size() > 0)
				{
					if (DEBUG_LEVEL > NONE)
						System.err.println("\nElement references to missing models:");
					int nMissing = 0;

					for (ModelRef incompleteElementRef : incompleteElements.keySet())
					{
						Model incompleteModel = repository.getModel(incompleteElementRef.modelId,
								true);
						DataElement incompleteElement = (DataElement) incompleteModel
								.getElement(incompleteElementRef.elementRole);

						ModelRef reusableElementRef = (ModelRef) incompleteElements
								.get(incompleteElementRef);
						Model elementCollectionModel = repository.getModel(
								reusableElementRef.modelId, true);
						DataElement reusableElement = (DataElement) elementCollectionModel
								.getElement(reusableElementRef.elementRole);

						if (reusableElement.getRefId() != null)
						{
							incompleteElement.setRefId(reusableElement.getRefId());
							if (DEBUG_LEVEL > NONE)
								System.out.println("--> " + incompleteElementRef
										+ ": Now refers to '" + incompleteElement.getRefId());
						}
						else
						{
							nMissing++;
							if (DEBUG_LEVEL > NONE)
								System.err
										.println(nMissing + ". " + incompleteElementRef + ": "
												+ reusableElementRef
												+ " still does not refer to any model");
						}
					}
					if (nMissing == 0)
					{
						if (DEBUG_LEVEL > NONE)
							System.err.println("None");
					}
					else
					{
						// Throw exception?
					}
					System.err.flush();
				}

				// Add slots to services
				iterator = incompleteModels.values().iterator();
				while (iterator.hasNext())
				{
					modelInfo = (ModelInfo) iterator.next();
					if (SERVICE.equals(modelInfo.modelType))
						addSlotsToSrvices(modelInfo);
				}

				// Add higher-level services (wrapping the lower-level services)
				iterator = incompleteModels.values().iterator();
				while (iterator.hasNext())
				{
					modelInfo = (ModelInfo) iterator.next();
					if (COLLECTION_OF_SERVICES.equals(modelInfo.modelType))
						wrapSrvices(modelInfo);
				}

				if (DEBUG_LEVEL > NONE)
				{
					System.err.flush();
					System.out.println("\nDone\n");
					System.out.flush();
				}
			}
		}

		private void createInheritedType(ModelInfo newTypeInfo)
		{
			ModelInfo inheritanceInfo = getTypeInheritanceInfo(newTypeInfo);
			if (inheritanceInfo != null)
			{
				if (newTypeInfo.model != null)
				{
					// newTypeInfo.model is expected to be null, unless it has just been created through a recursive call to 'createInheritedType'
					return;
				}

            	if (inheritanceInfo.model == null && inheritanceInfo.modelRef.modelId != null) // Ofer, 26/2/09 (if created since the reference had been created)
            		inheritanceInfo.model = repository.getModel(inheritanceInfo.modelRef.modelId,true);
            		
				if (inheritanceInfo.model == null)
				{
					if (COMPOSITE.equals(inheritanceInfo.modelType)
							|| BASE.equals(inheritanceInfo.modelType)) // BASE case added on the 31/8/07 (for http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd)
					{
						ModelInfo incompleteModelInfo = (ModelInfo) incompleteModels
								.get(inheritanceInfo.modelRef);
						if (incompleteModelInfo != null)
						{
							createInheritedType(incompleteModelInfo); // Recursive call to 'createInheritedType'
							inheritanceInfo.model = incompleteModelInfo.model; // Required
							inheritanceInfo.modelRef = incompleteModelInfo.modelRef; // OK? Required? Sufficient?
						}
					}

					if (inheritanceInfo.model == null) // The recursive call has updated it
					{
						System.err
								.println("Cannot create '" + newTypeInfo.modelRef.modelId
										+ "' from missing model '"
										+ inheritanceInfo.modelRef.modelId + "'");
						return;
					}
				}

				DataType baseModel = (DataType) inheritanceInfo.model;
				boolean baseIsAtomic = Composition.ATOMIC.equals(baseModel.getComposition());
				if (DEBUG_LEVEL > NONE)
					System.out.println("Creating '" + newTypeInfo.modelRef.modelId + "' from '"
							+ inheritanceInfo.modelRef.modelId + "':");
				newTypeInfo.model = repository.newModel(newTypeInfo.modelRef.modelId,
						(baseIsAtomic ? BuiltinModels.DATA_STRUCTURE_TEMPLATE_ID
								: inheritanceInfo.modelRef.modelId), !baseIsAtomic);
				newTypeInfo.model.setProperty(BuiltinProperties.XML_NAME_SPACE,
						newTypeInfo.modelRef.xmlNameSpace);
				if (DEBUG_LEVEL > NONE)
					System.out.println("-> Created model: " + newTypeInfo.modelRef.modelId);
				if (baseIsAtomic)
				{
					DataElement contentlElement = new DataElement();
					contentlElement.setRole(Role.get(SpecialRoles.XML_CONTENT_LEAF_ROLE));
					contentlElement.setRefId(inheritanceInfo.modelRef.modelId); // No need to set the element's namespace
					contentlElement.setProperty(BuiltinProperties.MANDATORY, Boolean.FALSE);
					contentlElement.setRepetitive(false);
					newTypeInfo.model.addElement(contentlElement);
				}
				addDataElements((DataType) newTypeInfo.model, newTypeInfo.name,
						newTypeInfo.modelType, inheritanceInfo.elements);
			}
		}

		private void addSlotsToSrvices(ModelInfo serviceInfo)
		{
			if (DEBUG_LEVEL > SOME)
				System.out.println(serviceInfo.modelRef.modelId.getPath() + ": Adding Slots");
			FlowModel service = (FlowModel) serviceInfo.model;

			int nTriggers = 1;
			int nExits = 1; // That's what we expect to initially have in "Call Web Service" (it will be better to actually count them)
			for (int i = 0; i < serviceInfo.elements.size(); i++)
			{
				ModelInfo child = (ModelInfo) serviceInfo.elements.elementAt(i);
				if (!TRIGGER.equals(child.modelType) && !EXIT.equals(child.modelType))
					continue;
				String childId = child.modelRef.modelId.getPath();
				String modelType = child.modelType;
				SlotType slotType = (TRIGGER.equals(modelType) ? SlotType.TRIGGER : SlotType.EXIT);
				double x = (slotType == SlotType.TRIGGER ? 0. : 1.);

				if (child.modelRef.modelId != null)
				{
					if (DEBUG_LEVEL > SOME)
						System.out.println("\t" + modelType + ": " + child);
					double y = 0.5;
					RelativePosition position = new RelativePosition(x, y);
					String slotName = (child.name != null ? child.name : child.modelRef.modelId
							.getName());
					Role role = Role.get(slotName);
					Slot slot = service.createSlot(slotType, position, role);
					slot.setDataTypeId(child.modelRef.modelId);
					slot.setProperty(BuiltinProperties.MANDATORY, new Boolean(child.minOccurs > 0));
					slot.setRepetitive(child.maxOccurs > 1);
					if (slotType == SlotType.TRIGGER)
						nTriggers++;
					else
						nExits++;
				}
				else
					System.err.println(serviceInfo.name + ": No pseudo-model for " + modelType
							+ " '" + childId + "'");
			}

			// Repositioning slots with equal distances between them
			List<ModelElement> elements = service.getElements();
			int t = 0;
			int e = 0;
			for (int i = 0; i < elements.size(); i++)
			{
				if (elements.get(i) instanceof Slot)
				{
					Slot slot = (Slot) elements.get(i);
					RelativePosition position = slot.getPosition();
					if ((slot.getType() == SlotType.TRIGGER) && (position.getX() == 0.))
					{
						position.setY(++t / (nTriggers + 1.));
						slot.setPosition(position);
					}
					else if ((slot.getType() != SlotType.TRIGGER) && (position.getX() == 1.))
					{
						position.setY(++e / (nExits + 1.));
						slot.setPosition(position);
					}
				}
			}
		}

		private void wrapSrvices(ModelInfo logicalServiceInfo)
		{
			if (DEBUG_LEVEL > SOME)
				System.out.println("\n" + logicalServiceInfo.name + ": Adding "
						+ logicalServiceInfo.modelType);

			for (int i = 0; i < logicalServiceInfo.elements.size(); i++)
			{
				ModelInfo child = (ModelInfo) logicalServiceInfo.elements.elementAt(i);
				if (EXPOSED_SERVICE_PACKAGE.equals(child.modelType))
				{
					if (DEBUG_LEVEL > SOME)
						System.out.println("Creating: " + child);
					String childId = child.modelRef.modelId.getPath();
					ModelId urlConstantId = null;
					ModelId documentConstantId = null;
					ModelId rpcConstantId = null;
					ModelId encodedConstantId = null;
					ModelId literalConstantId = null;
					boolean isSoap = false;
					for (int j = 0; j < child.elements.size(); j++)
					{
						ModelInfo grandchild = (ModelInfo) child.elements.elementAt(j);
						if (isSoap = SOAP_LOCATION.equals(grandchild.modelType))
							urlConstantId = new ModelId(childId+".URL.soap");
						else if (HTTP_LOCATION.equals(grandchild.modelType))
							urlConstantId = new ModelId(childId+".URL.http");
						if (urlConstantId != null)
						{
							if (repository.getModel(urlConstantId,true) != null) // Possible if more than one WSDL file defines the same binding name
								urlConstantId = repository.getNewModelId(urlConstantId.getPackageId(), urlConstantId.getName());

							DataType urlConstant = (DataType) repository.newModel(urlConstantId,BuiltinModels.TEXT_CONSTANT_ID, false);
							if (DEBUG_LEVEL > NONE)
								System.out.println("-> Created model: " + urlConstantId);
							urlConstant.setValue(grandchild.name);
							break;
						}
					}
					child = (ModelInfo) bindings.get(child.modelRef.modelId);
					if (child != null)
					{
						if (DEBUG_LEVEL > SOME)
							System.out.println("Using " + child);
						for (int j = 0; j < child.elements.size(); j++)
						{
							ModelInfo wrappedService = (ModelInfo) child.elements.elementAt(j);
							if (!SERVICE.equals(wrappedService.modelType))
								continue;
							if (DEBUG_LEVEL > SOME)
								System.out.println(" Using: " + wrappedService);
							ModelInfo exposedService = null;
							for (int k = 0; k < wrappedService.elements.size(); k++)
							{
								ModelInfo service = (ModelInfo) wrappedService.elements
										.elementAt(k);
								if (EXPOSED_SOAP_SERVICE.equals(service.modelType))
									exposedService = service;
								else if (EXPOSED_HTTP_SERVICE.equals(service.modelType))
								{
									// HTTP GET and HTTP POST not supported yet
									Slot protocolTrigger = ((FlowModel) wrappedService.model)
											.createSlot(SlotType.TRIGGER, new RelativePosition(0.2,
													1.0), SpecialRoles.PROTOCOL_TRIGGER_ROLE);
									protocolTrigger.setDataTypeId(BuiltinModels.TEXT_ID);
									protocolTrigger.setProperty(BuiltinProperties.MANDATORY,
											new Boolean(true));
								}
							}
							if (exposedService != null)
							{
								if (DEBUG_LEVEL > SOME)
									System.out.println(" Creating: " + exposedService);

								ModelId soapActionConstantId = null;
								if (wrappedService.soapAction != null)
								{
									if (DEBUG_LEVEL > SOME)
										System.out.println(" Using soapAction=\"" + wrappedService.soapAction + "\"");
									soapActionConstantId = new ModelId(wrappedService.modelRef.modelId.getPath() + ".soap_action");
									DataType soapActionConstant = (DataType) repository.newModel(soapActionConstantId, BuiltinModels.TEXT_CONSTANT_ID, false);
									if (DEBUG_LEVEL > NONE)
										System.out.println("-> Created model: " + soapActionConstantId);
									soapActionConstant.setValue(wrappedService.soapAction);
								}

								ModelId styleId = null;
								if (wrappedService.style != null)
								{
									if (DEBUG_LEVEL > SOME)
										System.out.println(" Using style=\"" + wrappedService.style + "\"");
									styleId = (wrappedService.style.equals(SpecialRoles.WSDL_DOCUMENT) ? documentConstantId	: rpcConstantId);
									if (styleId == null)
									{
										styleId = new ModelId(child.modelRef.modelId.getPackageId(),wrappedService.style);
										DataType styleConstant = (DataType) repository.getModel(styleId, true);
										if (styleConstant == null)
										{
											styleConstant = (DataType) repository.newModel(styleId, BuiltinModels.TEXT_CONSTANT_ID, false);
											if (DEBUG_LEVEL > NONE)
												System.out.println("-> Created model: " + styleId);
										}
										styleConstant.setValue(wrappedService.style);
										if (wrappedService.style.equals(SpecialRoles.WSDL_DOCUMENT))
											documentConstantId = styleId;
										else
											rpcConstantId = styleId;
									}
								}

								ModelId useId = null;
								if (wrappedService.use != null)
								{
									if (DEBUG_LEVEL > SOME)
										System.out.println(" Using use=\"" + wrappedService.use
												+ "\"");
									useId = (wrappedService.use.equals(SpecialRoles.WSDL_ENCODED) ? encodedConstantId
											: literalConstantId);
									if (useId == null)
									{
										useId = new ModelId(child.modelRef.modelId.getPackageId(),
												wrappedService.use);
										DataType useConstant = (DataType) repository.getModel(
												useId, true);
										if (useConstant == null)
										{
											useConstant = (DataType) repository.newModel(useId,
													BuiltinModels.TEXT_CONSTANT_ID, false);
											if (DEBUG_LEVEL > NONE)
												System.out.println("-> Created model: " + useId);
										}
										useConstant.setValue(wrappedService.use);
										if (wrappedService.use.equals(SpecialRoles.WSDL_ENCODED))
											encodedConstantId = useId;
										else
											literalConstantId = useId;
									}
								}

								exposedService.model = wrappingService(exposedService.modelRef.modelId, wrappedService, urlConstantId, soapActionConstantId, styleId, useId, isSoap);
							}
							else
								System.err.println(" No service to wrap");
						}
					}
					else
						System.err.println(logicalServiceInfo.name + ": No binding '" + childId
								+ "'");
				}
			}
		}

		private String elementName(HashMap<String, String> nameSpaces, String uri, String name, String qName, String nameSpace)
		{
			if ("".equals(uri))
			{
				int p = qName.indexOf(':');
				if (p >= 0)
				{
					uri = qName.substring(0, p);
					name = qName.substring(p + 1);
				}
				else
					name = qName;
			}

			if (nameSpace == null)
			{
				nameSpace = nameSpaces.get(uri);
				if (nameSpace == null)
				{
					System.err.println("Unknown namespace '" + uri + "'");
					nameSpace = uri; // Patch
				}
			}
			return nameSpace + ":" + name;
		}

		private String getModelType(String wsdlType)
		{
			if ("http://schemas.xmlsoap.org/wsdl/:definitions".equals(wsdlType)
					|| "http://schemas.xmlsoap.org/wsdl/:types".equals(wsdlType)
					|| "http://www.w3.org/2001/XMLSchema:schema".equals(wsdlType)
					|| "http://www.w3.org/2001/XMLSchema:annotation".equals(wsdlType)
					|| "http://www.w3.org/2001/XMLSchema:documentation".equals(wsdlType)
					|| "http://www.w3.org/2001/XMLSchema:appinfo".equals(wsdlType)
					|| "http://schemas.xmlsoap.org/wsdl/:documentation".equals(wsdlType)
					|| "http://schemas.xmlsoap.org/wsdl/http/:binding".equals(wsdlType)
					|| "http://schemas.xmlsoap.org/wsdl/http/:urlEncoded".equals(wsdlType)
					|| "http://schemas.xmlsoap.org/wsdl/mime/:mimeXml".equals(wsdlType)
					|| "http://schemas.xmlsoap.org/wsdl/mime/:content".equals(wsdlType)
					|| "http://schemas.xmlsoap.org/wsdl/soap/:body".equals(wsdlType))
				return IGNORE;

			if ("http://schemas.xmlsoap.org/wsdl/:service".equals(wsdlType))
				return COLLECTION_OF_SERVICES;

			if ("http://schemas.xmlsoap.org/wsdl/:port".equals(wsdlType))
				return EXPOSED_SERVICE_PACKAGE;

			if ("http://schemas.xmlsoap.org/wsdl/http/:address".equals(wsdlType))
				return HTTP_LOCATION;

			if ("http://schemas.xmlsoap.org/wsdl/soap/:address".equals(wsdlType))
				return SOAP_LOCATION;

			if ("http://schemas.xmlsoap.org/wsdl/:binding".equals(wsdlType))
				return WSDL_BINDING;

			if ("http://schemas.xmlsoap.org/wsdl/soap/:binding".equals(wsdlType))
				return (SOAP_BINDING);

			if ("http://schemas.xmlsoap.org/wsdl/http/:operation".equals(wsdlType))
				return EXPOSED_HTTP_SERVICE;

			if ("http://schemas.xmlsoap.org/wsdl/soap/:operation".equals(wsdlType))
				return EXPOSED_SOAP_SERVICE;

			if ("http://schemas.xmlsoap.org/wsdl/:portType".equals(wsdlType))
				return SERVICE_PACKAGE;

			if ("http://schemas.xmlsoap.org/wsdl/:operation".equals(wsdlType))
				return SERVICE;

			if ("http://schemas.xmlsoap.org/wsdl/:input".equals(wsdlType))
				return TRIGGER;

			if ("http://schemas.xmlsoap.org/wsdl/:output".equals(wsdlType)
					|| "http://schemas.xmlsoap.org/wsdl/:fault".equals(wsdlType))
				return EXIT;

			if ("http://schemas.xmlsoap.org/wsdl/soap/:header".equals(wsdlType))
				return SOAP_HEADER_SLOT;

			if ("http://schemas.xmlsoap.org/wsdl/:message".equals(wsdlType))
				return SLOT_TYPE;

			if ("http://schemas.xmlsoap.org/wsdl/:part".equals(wsdlType))
				return SLOT_TYPE_ELEMENT;

			if ("http://www.w3.org/2001/XMLSchema:complexType".equals(wsdlType))
				return COMPOSITE; // Or atomic with attributes if contains SIMPLE_DEFINITION

			if ("http://www.w3.org/2001/XMLSchema:simpleContent".equals(wsdlType))
				return ATTRIBUTES_DEFINITION;

			if ("http://www.w3.org/2001/XMLSchema:complexContent".equals(wsdlType))
				return COMPOSITION_DEFINITION;

			if ("http://www.w3.org/2001/XMLSchema:sequence".equals(wsdlType)
					|| "http://www.w3.org/2001/XMLSchema:choice".equals(wsdlType)
					|| "http://www.w3.org/2001/XMLSchema:all".equals(wsdlType))
				return ELEMENT_SET; // We'll later distinguish between "sequence", "choice" and "all"

			if ("http://www.w3.org/2001/XMLSchema:element".equals(wsdlType))
				return ELEMENT;

			if ("http://www.w3.org/2001/XMLSchema:any".equals(wsdlType))
				return ANYTHING; // Need to implement!

			if ("http://www.w3.org/2001/XMLSchema:simpleType".equals(wsdlType))
				return ATOMIC;

			if ("http://www.w3.org/2001/XMLSchema:attribute".equals(wsdlType))
				return ATTRIBUTE;

			if ("http://www.w3.org/2001/XMLSchema:anyAttribute".equals(wsdlType))
				return ANY_ATTRIBUTE; // Need to implement!


			if ("http://www.w3.org/2001/XMLSchema:attributeGroup".equals(wsdlType))
				return ATTRIBUTE_GROUP; // Need to implement

			if ("http://www.w3.org/2001/XMLSchema:extension".equals(wsdlType)
					|| "http://www.w3.org/2001/XMLSchema:restriction".equals(wsdlType))
				return BASE;

			if ("http://www.w3.org/2001/XMLSchema:enumeration".equals(wsdlType))
				return PROPERTY;

			if (DEBUG_LEVEL > SOME)
				System.err.println("Unknown Schema or WSDL Type: " + wsdlType);
			return null;
		}

		private class ModelRef
		{
			private ModelId modelId;
			private String xmlNameSpace;
			private Role elementRole; // Only when referring to an element of the model

			private ModelRef(ModelId modelId)
			{
				this.modelId = modelId;
			}

			private ModelRef(ModelId modelId, String xmlNameSpace, Role elementRole)
			{
				this(modelId);
				this.xmlNameSpace = xmlNameSpace;
				this.elementRole = elementRole;
			}

			public String toString()
			{
				return idToString(modelId, elementRole);
			}

			public boolean equals(Object other)
			{
				if (this == other)
					return true;

				if ((other == null) || (other.getClass() != this.getClass()))
					return false;

				ModelRef o = (ModelRef) other;

				boolean sameModelId = (this.modelId == null ? (o.modelId == null) : this.modelId
						.equals(o.modelId));
				boolean sameXmlNameSpace = (this.xmlNameSpace == null ? (o.xmlNameSpace == null)
						: this.xmlNameSpace.equals(o.xmlNameSpace));
				boolean sameElementRole = (this.elementRole == null ? (o.elementRole == null)
						: this.elementRole.equals(o.elementRole));

				return (sameModelId && sameXmlNameSpace && sameElementRole);
			}

			public int hashCode()
			{
				return (modelId == null ? 0 : modelId.hashCode());
			}
		}

		private ModelRef getModelRef(HashMap<String, String> nameSpaces, String targetNameSpace, String packageName, String name,
				String shortXmlType, String modelType, boolean isReusableElement)
		{
			// Special treatment for a model to be created within a specific package
			if (packageName != null) // Expected to happen only for children of SERVICE_PACKAGE and WSDL_BINDING
			{
				if (name == null)
					return new ModelRef(null);
				else
				{
					if (!packageName.startsWith(SpecialRoles.XML_PACKAGE + "/"))
					{
						System.err.println("** BUG: Package '" + packageName
								+ "' does not start with '" + (SpecialRoles.XML_PACKAGE + "/")
								+ "'");
						packageName = SpecialRoles.XML_PACKAGE + "/" + packageName;
					}
					ModelId newModelId = new ModelId(packageName + "/" + name + suffix(modelType));
					return (new ModelRef(newModelId)); // Assuming type and namespace are not needed for children of SERVICE_PACKAGE and WSDL_BINDING
				}
			}

			// Get the name and namespace of the XML Type
			boolean isLocalElement = ELEMENT.equals(modelType) && !isReusableElement;
			boolean useNameAsType = (shortXmlType == null) && !isLocalElement;
			String typeName = (useNameAsType ? name : shortXmlType);
			if (typeName == null)
				return new ModelRef(null); // E.g. 'schema' or anonymous 'sequence' (no type nor name)

			SplitName xmlTypeName = new SplitName(nameSpaces, typeName, targetNameSpace, !useNameAsType);

			// Get the element's namespace
			boolean elementAndTypeNameSpacesAreIdentical = useNameAsType
					|| xmlTypeName.usedImplicitNameSpace;
			String xmlElementNameSpace;
			if (elementAndTypeNameSpacesAreIdentical)
				xmlElementNameSpace = xmlTypeName.nameSpace;
			else if (name != null)
			{
				SplitName elementName = new SplitName(nameSpaces, name, targetNameSpace, false);
				xmlElementNameSpace = elementName.nameSpace;
			}
			else
				xmlElementNameSpace = targetNameSpace;

			// Calculate corresponding package path
			int p = xmlTypeName.nameSpace.indexOf(':');
			while ((p + 1 < xmlTypeName.nameSpace.length())
					&& (xmlTypeName.nameSpace.charAt(p + 1) == '/'))
				p++; // Skipping prefixes like "http://" or "urn:"
			String relativePackageName = (p >= 0 ? xmlTypeName.nameSpace.substring(p + 1)
					: xmlTypeName.nameSpace);

			if ("".equals(relativePackageName))
			{
				; // is it OK?
			}
			else
			{
				if (relativePackageName.charAt(relativePackageName.length() - 1) != '/')
					relativePackageName = relativePackageName + "/";
			}

			if (CONVERT_COLONS_IN_NAMESPACE)
				relativePackageName = relativePackageName.replace(':', '/'); // E.g. "loc.gov:books" becomes "loc.gov/books" (see http://www.w3.org/TR/REC-xml-names/)

			if (ATTRIBUTE.equals(modelType) && isReusableElement)
				relativePackageName = relativePackageName
						+ SpecialRoles.XML_ATTRIBUTE_COLLECTION_NAME + "/";
			else if (SLOT_TYPE.equals(modelType) || TRIGGER.equals(modelType)
					|| EXIT.equals(modelType) || SOAP_HEADER_SLOT.equals(modelType))
				relativePackageName = relativePackageName + SpecialRoles.WSDL_MESSAGE_PACKAGE_NAME
						+ "/";

			// Create ModelId
			boolean elementRatherThanModel = isReusableElement
					&& (ELEMENT.equals(modelType) || SLOT_TYPE_ELEMENT.equals(modelType));
			String newModelIdPath = SpecialRoles.XML_PACKAGE
					+ "/"
					+ relativePackageName
					+ (elementRatherThanModel ? SpecialRoles.XML_ELEMENT_COLLECTION_NAME
							: xmlTypeName.name + suffix(modelType));
			ModelId newModelId = new ModelId(newModelIdPath);

			// That's it
			Role elementRole = (elementRatherThanModel ? Role.get(xmlTypeName.name) : null);
			ModelRef newModelRef = new ModelRef(newModelId, xmlElementNameSpace, elementRole);
			return (newModelRef);
		}

		private class SplitName
		{
			private String name;
			private String nameSpace;
			private boolean usedImplicitNameSpace;

        	private SplitName (HashMap<String, String> nameSpaces, String fullName, String targetNameSpace, boolean preferDefaultNameSpace)
        	{
            	int p = fullName.lastIndexOf(':');
            	usedImplicitNameSpace = (p < 0); // Namespace not explicitly specified
            	if (usedImplicitNameSpace)
            	{
                	name = fullName;
                   	if (preferDefaultNameSpace && nameSpaces.get("") != null) // Ofer, 25/2/09
                	    nameSpace = (String) nameSpaces.get("");
                	else
                		nameSpace = targetNameSpace;
            	}
            	else // p >= 0
            	{
            		String nameSpaceAlias = fullName.substring(0,p);
            		if (fullName.startsWith("http://") || fullName.startsWith("https://")) // E.g. soapAction="http://www.27seconds.com/Holidays/US/Dates/GetNewYear"
            		{
            			p = fullName.lastIndexOf('/');
            			nameSpace = fullName.substring(0,p);
            		}
            		else
            			nameSpace = (String) nameSpaces.get(nameSpaceAlias);
            		name = fullName.substring(p+1);
                	if (nameSpace == null)
                	{
                		System.err.println ("Unknown namespace '"+nameSpaceAlias+"'");
                		nameSpace = nameSpaceAlias; // Patch
                	}
            	}
        	}
        }

		private ModelInfo getTypeInheritanceInfo(ModelInfo modelInfo)
		{
			for (int i = 0; i < modelInfo.elements.size(); i++)
			{
				ModelInfo child = (ModelInfo) modelInfo.elements.elementAt(i);
				if (COMPOSITION_DEFINITION.equals(child.modelType)
						|| ATTRIBUTES_DEFINITION.equals(child.modelType))
				{
					for (int j = 0; j < child.elements.size(); j++)
					{
						ModelInfo grandchild = (ModelInfo) child.elements.elementAt(j);
						if (BASE.equals(grandchild.modelType))
							return grandchild;
					}
				}
			}
			return null;
		}

		private Model newDataType(ModelRef elementModelRef, ModelId templateType)
		{
			ModelId referredDataTypeId;
			if (elementModelRef.elementRole == null)
				referredDataTypeId = elementModelRef.modelId;
			else
				referredDataTypeId = new ModelId(elementModelRef.modelId + "/"
						+ elementModelRef.elementRole); // Same path the element referring to it

			Model createdDataType = repository.newModel(referredDataTypeId, templateType, false);
			createdDataType.setProperty(BuiltinProperties.XML_NAME_SPACE,
					elementModelRef.xmlNameSpace);
			if (DEBUG_LEVEL > NONE)
				System.out.println("-> Created model: " + referredDataTypeId);

			return createdDataType;
		}

		private Model createReusableElement(ModelRef elementModelRef, ModelId referredDataTypeId,
				int minOccurs, int maxOccurs)
		{
			// Get or create the element collection model (holding all reusable elements in its package)
			Model elementCollectionModel = repository.getModel(elementModelRef.modelId, true);
			if (elementCollectionModel == null)
				elementCollectionModel = repository.newModel(elementModelRef.modelId,
						BuiltinModels.DATA_STRUCTURE_TEMPLATE_ID, false);

			// Create (or update) the reusable element
			DataElement existingElement = (DataElement) elementCollectionModel.getElement(elementModelRef.elementRole);
			if (existingElement == null)
			{
				DataElement modelElement = new DataElement();
				modelElement.setRole(elementModelRef.elementRole);
				modelElement.setRefId(referredDataTypeId);
				modelElement.setProperty(BuiltinProperties.XML_NAME_SPACE,elementModelRef.xmlNameSpace);
				modelElement.setProperty(BuiltinProperties.MANDATORY, new Boolean(minOccurs > 0));
				modelElement.setRepetitive(maxOccurs > 1);
				elementCollectionModel.addElement(modelElement);
			}
			else
			{
				if (existingElement.getRefId() == null) // Possible if reusable element has been created ad-hoc with no data type, to be resolved later
					existingElement.setRefId(referredDataTypeId);
				else
					System.err.println("** BUG: Duplicate reusable element '" + elementModelRef	+ "'");
			}

			return elementCollectionModel;
		}

		private void addDataElements(DataType composite, String name, String modelType,
				Vector elements)
		{
			// Helper class used to collect the elements
			final class ElementToAdd
			{
				private ModelInfo elementInfo;
				boolean elementOrType;
				boolean slotElementUnderSlotType;
				boolean isAttribute;
				Role role;
				boolean fullRole;

				private ElementToAdd(String parentModelType, ModelInfo elementInfo)
				{
					this.elementInfo = elementInfo;
					this.elementOrType = ELEMENT.equals(elementInfo.modelType)
							|| COMPOSITE.equals(elementInfo.modelType)
							|| ATOMIC.equals(elementInfo.modelType);
					this.slotElementUnderSlotType = !COMPOSITE.equals(parentModelType)
							&& SLOT_TYPE_ELEMENT.equals(elementInfo.modelType);
					this.isAttribute = ATTRIBUTE.equals(elementInfo.modelType);
					this.role = null;
					this.fullRole = false;
				}
			}

			// Collecting the elements to add (and calculating the role of each)
			Vector<ElementToAdd> elementsToAdd = new Vector<ElementToAdd>();
			for (int i = 0; i < elements.size(); i++)
			{
				ModelInfo child = (ModelInfo) elements.elementAt(i);
				ElementToAdd candidate = new ElementToAdd(modelType, child);

				if (candidate.elementOrType || candidate.slotElementUnderSlotType
						|| candidate.isAttribute)
				{
					if (candidate.isAttribute)
						candidate.role = Role.get(SpecialRoles.DEPENDENT_MODEL_ROLE_PREFIX
								+ child.name);
					else if (candidate.slotElementUnderSlotType /* && isGenericName(child.name) */
							&& (child.modelRef.elementRole != null))
						candidate.role = /* Role.get(child.modelRef.modelId.getName()); */child.modelRef.elementRole;
					else
						candidate.role = Role.get(child.name);

					candidate.fullRole = (composite.getElement(candidate.role) != null);
					for (int j = 0; j < elementsToAdd.size(); j++)
					{
						ElementToAdd brotherElement = elementsToAdd.elementAt(j);
						if (brotherElement.role.equals(candidate.role))
						{
							brotherElement.fullRole = true;
							candidate.fullRole = true;
							break;
						}
					}

					elementsToAdd.add(candidate);
				}
				else
					System.err.println(name + ": Unexpected child " + child.modelType + " '"
							+ child.name + "'");
			}

			// Actually creating and adding the elements
			for (int i = 0; i < elementsToAdd.size(); i++)
			{
				ElementToAdd elementToAdd = (ElementToAdd) elementsToAdd.elementAt(i);
				ModelInfo child = elementToAdd.elementInfo;
				Role role = elementToAdd.role;

				if (child.modelRef.modelId != null)
				{
					DataElement modelElement;
					if (child.modelRef.elementRole != null) // Reusable element - copy
					{
						Model elementCollectionModel = repository.getModel(child.modelRef.modelId,
								true);
						ModelElement reusableElement = (elementCollectionModel != null ? elementCollectionModel
								.getElement(child.modelRef.elementRole)
								: null);
						if (reusableElement == null)
						{
							elementCollectionModel = createReusableElement(child.modelRef, null, child.minOccurs, child.maxOccurs);
							reusableElement = elementCollectionModel
									.getElement(child.modelRef.elementRole);
							System.err
									.println(name
											+ ": No reusable element '"
											+ child.modelRef
											+ "' (created ad-hoc with no data type, may be resolved later)");
						}
						modelElement = (DataElement) WorkspaceRepository
								.copyElement(reusableElement);
						// No need to set the element's namespace (apparently, there is no case
						// where the namespace should differ from the one of the reusable element)

						if (modelElement.getRefId() == null) // Possible if reusable element has been created ad-hoc with no data type, to be resolved later
						{
							System.err.println(name + ": Element '" + role
									+ "' created with no data type (may be resolved later)");
							String namespace = (String) modelElement
									.getProperty(BuiltinProperties.XML_NAME_SPACE);
							ModelRef incompleteElement = new ModelRef(composite.getId(), namespace,
									role);
							incompleteElements.put(incompleteElement, child.modelRef);
						}
					}
					else
					// Local element - create
					{
						modelElement = new DataElement();
						String elementNameSpace = child.modelRef.xmlNameSpace; // Default (may change shortly)

						if ((child.model == null)
								&& (repository.getModel(child.modelRef.modelId, true) == null))
						{
							ModelInfo childElement = (ModelInfo) incompleteModels
									.get(child.modelRef);
							if (childElement != null)
							{
								modelElement.setRefId(childElement.modelRef.modelId);
								elementNameSpace = childElement.modelRef.xmlNameSpace;
							}
							else
							{
								System.err.println(name + ": No model '" + child.modelRef.modelId
										+ "' (may be resolved later)");
								missingModels.add(child.modelRef); // E.g. "XML/schemas.xmlsoap.org/soap/envelope/(Elements)/Header" (for <xs:element ref="tns:Header" minOccurs="0")
								modelElement.setRefId(child.modelRef.modelId); // May still be resolved later
							}
						}
						else
							modelElement.setRefId(child.modelRef.modelId);

						if (!elementToAdd.isAttribute) // Are we sure element's namespace is not needed for attributes?
							modelElement.setProperty(BuiltinProperties.XML_NAME_SPACE,
									elementNameSpace);

						boolean mandatory = (elementToAdd.isAttribute ? false : child.minOccurs > 0); // For an attribute we should actually look at the 'use' attribute
						modelElement.setProperty(BuiltinProperties.MANDATORY,
								new Boolean(mandatory));
						modelElement.setRepetitive(child.maxOccurs > 1);
					}

					// In both cases - add element to parent model
					if (elementToAdd.fullRole)
					{
						// role = fullRole(elementNameSpace, role.toString()); // Doesn't work - we no longer have the distinguishing namespace
						role = uniqueRelativeRole(composite, role);
					}
					modelElement.setRole(role);
					composite.addElement(modelElement);
				}
				else
					System.err.println(name + ": No model for " + child.modelType + " '"
							+ child.name + "'");
			}
		}

		private Role uniqueRelativeRole(DataType composite, Role role) // See tersus.plugins.misc.CallWebService.netRole()
		{
			int prefix = 1;
			Role prefixedRole;
			do
			{
				prefixedRole = Role.get((prefix++) + ":" + role);
			}
			while (composite.getElement(prefixedRole) != null);

			return prefixedRole;
		}

		// private Role fullRole (String xmlNameSpace, Role role)
		// {
		// boolean useDefaultNameSpace = false;
		// for (String nameSpaceAlias: nameSpaces.keySet())
		// {
		// String nameSpace = (String) nameSpaces.get(nameSpaceAlias);
		// if (xmlNameSpace.equals(nameSpace))
		// {
		// if (nameSpaceAlias.equals(""))
		// useDefaultNameSpace = true;
		// else
		// return Role.get(nameSpaceAlias + ":" + role);
		// }
		// }
		// if (useDefaultNameSpace)
		// {
		// String nameSpaceAlias = "tns"; // Target Name Space
		// while (nameSpaces.get(nameSpaceAlias) != null)
		// nameSpaceAlias = "_" + nameSpaceAlias;
		// return Role.get(nameSpaceAlias + ":" + role);
		// }
		// else
		// return Role.get(xmlNameSpace + ":" + role); // Bug!
		// }

		// private ModelId singleCompositeChildOfDataType (ModelId compositeId)
		// {
		// DataType composite = (DataType) repository.getModel(compositeId,true);
		// if ((composite != null) && (composite.getComposition() != Composition.ATOMIC) &&
		// (composite.getElements().size()== 1))
		// {
		// DataElement child = (DataElement) composite.getElements().get(0);
		// if (child.getModel().getComposition() != Composition.ATOMIC)
		// return child.getRefId();
		// }
		// return null;
		// }

		// private boolean isGenericName (String partName)
		// {
		// return ("parameters".equalsIgnoreCase(partName) ||
		// "body".equalsIgnoreCase(partName));
		// }

		final String SERVICE_SUFFIX = ".call";
		final String EXPOSED_SOAP_SUFFIX = ".soap";
		final String EXPOSED_HTTP_SUFFIX = ".http";

		private String suffix(String modelType)
		{
			if (SERVICE.equals(modelType))
				return SERVICE_SUFFIX;
			if (EXPOSED_SOAP_SERVICE.equals(modelType))
				return EXPOSED_SOAP_SUFFIX;
			else if (EXPOSED_HTTP_SERVICE.equals(modelType))
				return EXPOSED_HTTP_SUFFIX; // Should we distinguish here berween GET and POST?
			else
				return "";
		}

		private Model wrappingService(ModelId wrappingModelId, ModelInfo wrappedService,
				ModelId urlConstantId, ModelId soapActionConstantId, ModelId styleId, ModelId useId, boolean isSoap)
		{
			// Check if there are SOAP headers defined
			ModelId inputHeaderType = null;
			ModelId outputHeaderType = null;
			if (isSoap)
			{
				for (int i = 0; i < wrappedService.elements.size(); i++)
				{
					ModelInfo slotInfo = (ModelInfo) wrappedService.elements.elementAt(i);
					if (TRIGGER.equals(slotInfo.modelType) || EXIT.equals(slotInfo.modelType))
					{
						for (int j = 0; j < slotInfo.elements.size(); j++)
						{
							ModelInfo soapSlotInfo = (ModelInfo) slotInfo.elements.elementAt(j);
							if (SOAP_HEADER_SLOT.equals(soapSlotInfo.modelType))
							{
								// ModelId headerElementTypeId = singleCompositeChildOfDataType(soapSlotInfo.modelRef.modelId);
								// if (headerElementTypeId != null)
								// {
								// if (TRIGGER.equals(slotInfo.modelType))
								//     inputHeaderType = headerElementTypeId;
								// else
								//     outputHeaderType = headerElementTypeId;
								// }
								// else // If there is a single child, but it is atomic, we can still make it a composite (?)
								//     System.err.println(wrappedService.name+": No single composite child of '"+soapSlotInfo.modelRef.modelId+"'");

								if (TRIGGER.equals(slotInfo.modelType))
									inputHeaderType = soapSlotInfo.modelRef.modelId;
								else
									outputHeaderType = soapSlotInfo.modelRef.modelId;

								if (DEBUG_LEVEL > NONE)
								{
									SlotType slotType = (TRIGGER.equals(slotInfo.modelType) ? SlotType.TRIGGER
											: SlotType.EXIT);
									System.out.println(" Header " + slotType + ": "
											+ soapSlotInfo.modelRef.modelId);
								}
							}
						}
					}
				}
			}

			// Modify the data types of the SOAP Headers - add optional attributes
			if (inputHeaderType != null)
			{
				DataType message = (DataType) repository.getModel(inputHeaderType, true);
				for (int i = 0; i < message.getElements().size(); i++)
				{
					DataElement part = (DataElement) message.getElements().get(i);
					if (part.getModel().getComposition() != Composition.ATOMIC)
						addOptionalSoapAttributes(part.getRefId());
				}
			}
			if (outputHeaderType != null)
			{
				DataType message = (DataType) repository.getModel(outputHeaderType, true);
				for (int i = 0; i < message.getElements().size(); i++)
				{
					DataElement part = (DataElement) message.getElements().get(i);
					if (part.getModel().getComposition() != Composition.ATOMIC)
						addOptionalSoapAttributes(part.getRefId());
				}
			}

			// Modify the wrapped service - set the types of the <Header> trigger and the <<Header>> exit (or remove redundant slots)
			Slot headerTrigger = (Slot) wrappedService.model
					.getElement(SpecialRoles.HEADER_TRIGGER_ROLE);
			if (inputHeaderType != null)
				headerTrigger.setDataTypeId(inputHeaderType);
			else
				wrappedService.model.removeElement(headerTrigger);

			Slot headerExit = (Slot) wrappedService.model.getElement(SpecialRoles.HEADER_EXIT_ROLE);
			if (outputHeaderType != null)
				headerExit.setDataTypeId(outputHeaderType);
			else
				wrappedService.model.removeElement(headerExit);

			// Start with a copy of the wrapped service
			FlowModel model = (FlowModel) repository.newModel(wrappingModelId,
					wrappedService.modelRef.modelId, true);
			if (DEBUG_LEVEL > NONE)
				System.out.println("-> Created model: " + wrappingModelId);

			// But set plugin to "regular service"
			model.setPlugin(BuiltinPlugins.SERVICE);

			// Add invocation of the wrapped service
			SubFlow subService = new SubFlow();
			subService.setRole(Role.get(wrappedService.modelRef.modelId.getName()));
			subService.setRefId(wrappedService.modelRef.modelId);
			subService.setPosition(0.5, 0.65);
			subService.setSize(0.38, 0.38);
			model.addElement(subService);

			// Replace the <URL> trigger by a constant
			Slot urlSlot = (Slot) model.getElement(SpecialRoles.URL_TRIGGER_ROLE);
			model.removeElement(urlSlot);
			DataElement urlConstantElement = new DataElement();
			urlConstantElement.setRole(SpecialRoles.URL_TRIGGER_ROLE);
			urlConstantElement.setRefId(urlConstantId);
			urlConstantElement.setPosition(0.5, 0.15);
			urlConstantElement.setSize(0.9, 0.07);
			model.addElement(urlConstantElement);

			// Replace the <SOAP Action> trigger by a constant
			Slot soapActionSlot = (Slot) model.getElement(SpecialRoles.SOAP_ACTION_TRIGGER_ROLE);
			model.removeElement(soapActionSlot);
			if (soapActionConstantId != null)
			{
				DataElement soapActionConstantElement = new DataElement();
				soapActionConstantElement.setRole(SpecialRoles.SOAP_ACTION_TRIGGER_ROLE);
				soapActionConstantElement.setRefId(soapActionConstantId);
				soapActionConstantElement.setPosition(0.6, 0.25);
				soapActionConstantElement.setSize(0.7, 0.07);
				model.addElement(soapActionConstantElement);
			}
			
			// Replace the <Method> trigger by a constant
			Slot methodSlot = (Slot) model.getElement(SpecialRoles.METHOD_TRIGGER_ROLE);
			model.removeElement(methodSlot);
			ModelId methodConstantId = new ModelId(wrappedService.modelRef.modelId.getPath() + ".method");
			DataType methodConstant = (DataType) repository.newModel(methodConstantId, BuiltinModels.TEXT_CONSTANT_ID, false);
			if (DEBUG_LEVEL > NONE)
				System.out.println("-> Created model: " + methodConstantId);
			String methodName = wrappedService.modelRef.modelId.getName(); // Is this indeed the place to take the method name from?
			if (methodName.endsWith(SERVICE_SUFFIX))
				methodName = methodName.substring(0, methodName.length() - SERVICE_SUFFIX.length());
			methodConstant.setValue(methodName);
			DataElement methodConstantElement = new DataElement();
			methodConstantElement.setRole(SpecialRoles.METHOD_TRIGGER_ROLE);
			methodConstantElement.setRefId(methodConstantId);
			methodConstantElement.setPosition(0.7, 0.35);
			methodConstantElement.setSize(0.5, 0.07);
			model.addElement(methodConstantElement);

			// Replace the <HTTP Protocol> trigger by a constant
			if (!isSoap)
			{
				// HTTP GET and HTTP POST not supported yet (trigger does not exist for SOAP)
			}

			// Replace the <Binding> trigger by constants
			Slot bindingSlot = (Slot) model.getElement(SpecialRoles.BINDING_TRIGGER_ROLE);
			model.removeElement(bindingSlot);
			if (styleId != null)
			{
				DataElement bindingConstantElement = new DataElement();
				bindingConstantElement.setRole(Role.get("style " + SpecialRoles.BINDING_TRIGGER_ROLE));
				bindingConstantElement.setRefId(styleId);
				bindingConstantElement.setPosition(0.47, 0.9);
				bindingConstantElement.setSize(0.18, 0.07);
				model.addElement(bindingConstantElement);
			}
			if (useId != null)
			{
				DataElement bindingConstantElement = new DataElement();
				bindingConstantElement.setRole(Role.get("use " + SpecialRoles.BINDING_TRIGGER_ROLE));
				bindingConstantElement.setRefId(useId);
				bindingConstantElement.setPosition(0.68, 0.9);
				bindingConstantElement.setSize(0.18, 0.07);
				model.addElement(bindingConstantElement);
			}

			// Create flows between elements of the wrapping service and the slots of wrapped service
			Model wrappedServiceModel = repository.getModel(subService.getRefId(), true);
			List<ModelElement> elements = wrappedServiceModel.getElements();
			boolean openTriggers = false, openExits = false;
			if (isSoap)
			{
				int nTriggers = 0;
				int nExits = 0;
				for (int i = 0; i < elements.size(); i++)
				{
					ModelElement element = (ModelElement) elements.get(i);
					if (element instanceof Slot)
					{
						if (((Slot) element).getType() == SlotType.TRIGGER)
						{
							if ((element.getRole() != SpecialRoles.HEADER_TRIGGER_ROLE)
									&& (element.getRole() != SpecialRoles.URL_TRIGGER_ROLE)
									&& (element.getRole() != SpecialRoles.SOAP_ACTION_TRIGGER_ROLE)
									&& (element.getRole() != SpecialRoles.METHOD_TRIGGER_ROLE)
									&& (element.getRole() != SpecialRoles.BINDING_TRIGGER_ROLE))
								nTriggers++;
						}
						else
						{
							if (element.getRole() != SpecialRoles.HEADER_EXIT_ROLE)
								nExits++;
						}
					}
				}
				openTriggers = (nTriggers == 1); // Actually it must be 1 ("input")
				openExits = (nExits == 1); // Can be 2 ("output" & "fault")
			}
			for (int i = 0; i < elements.size(); i++)
			{
				ModelElement element = (ModelElement) elements.get(i);
				if (element instanceof Slot)
				{
					Role role = element.getRole();
					Path innerPath = new Path(subService.getRole());
					innerPath.addSegment(role);
					Path outerPath = new Path(role);
					DataType dataType = (DataType) ((Slot) element).getReferredModel();

					if (((Slot) element).getType() == SlotType.TRIGGER)
					{
						if (openTriggers && (role != SpecialRoles.HEADER_TRIGGER_ROLE)
								&& (dataType.getComposition() != Composition.ATOMIC))
						{
							// Replace trigger by an intermediate data structure
							Role dataElementRole = (dataType.getElement(role) == null ? role : Role.get(role + ".in"));
							Slot outerSlot = (Slot) model.getElement(role);
							model.removeElement(outerSlot);
							DataElement dataElement = new DataElement();
							dataElement.setRole(dataElementRole);
							dataElement.setRefId(dataType.getId());
							dataElement.setPosition(0.12, 0.7);
							dataElement.setSize(0.18, 0.52);
							model.addElement(dataElement);
							outerPath = new Path(dataElementRole);

							// Create a trigger for each data element in the intermediate data structure and link it to the corresponding data element
							List<ModelElement> subElements = dataType.getElements();
							for (int j = 0; j < subElements.size(); j++)
							{
								ModelElement subElement = ((ModelElement) subElements.get(j));
								double y = (j + 1.) / (subElements.size() + 1.);
								RelativePosition position = new RelativePosition(0., y);
								Slot slot = model.createSlot(SlotType.TRIGGER, position, subElement.getRole());
								slot.setDataTypeId(subElement.getRefId());
								slot.setProperty(BuiltinProperties.MANDATORY, subElement.getProperty(BuiltinProperties.MANDATORY));
								slot.setRepetitive(subElement.isRepetitive());

								Path elementPath = new Path(dataElementRole);
								elementPath.addSegment(subElement.getRole());
								Link flow = new Link(new Path(slot.getRole()), elementPath);
								flow.setRole(Role.get("Flow to element " + subElement.getRole()));
								model.addElement(flow);
							}
						}

						// Link trigger (or the intermediate data structure) to the corresponding trigger of the wrapped service
						if (role != SpecialRoles.BINDING_TRIGGER_ROLE)
						{
							if ((role != SpecialRoles.SOAP_ACTION_TRIGGER_ROLE)
								|| (soapActionConstantId != null)) // <SOAP Action> is optional
							{
								Link triggerFlow = new Link(outerPath, innerPath);
								triggerFlow.setRole(Role.get("Flow to trigger " + role));
								model.addElement(triggerFlow);
							}
						}
						// Several constants may flow into <Binding>
						else
						{
							List<ModelElement> modelElements = model.getElements();
							for (int j = 0; j < modelElements.size(); j++)
							{
								ModelElement modelElement = (ModelElement) modelElements.get(j);
								if ((modelElement instanceof DataElement)
										&& modelElement.getRole().toString().endsWith(SpecialRoles.BINDING_TRIGGER_ROLE.toString()))
								{
									outerPath = new Path(modelElement.getRole());
									Link triggerFlow = new Link(outerPath, innerPath);
									triggerFlow.setRole(Role.get("Flow from " + modelElement.getRole() + " to trigger " + role));
									model.addElement(triggerFlow);
								}
							}
						}
					}
					else
					// Exit
					{
						if (openExits && (role != SpecialRoles.HEADER_EXIT_ROLE)
								&& (dataType.getComposition() != Composition.ATOMIC))
						{
							// Replace exit by an intermediate data structure
							Role dataElementRole = (dataType.getElement(role) == null ? role : Role
									.get(role + ".out"));
							Slot outerSlot = (Slot) model.getElement(role);
							model.removeElement(outerSlot);
							DataElement dataElement = new DataElement();
							dataElement.setRole(dataElementRole);
							dataElement.setRefId(dataType.getId());
							dataElement.setPosition(0.88, 0.7);
							dataElement.setSize(0.18, 0.52);
							model.addElement(dataElement);
							outerPath = new Path(dataElementRole);

							// Create an exit for each data element in the intermediate data structure and link the corresponding data element to it
							List<ModelElement> subElements = dataType.getElements();
							for (int j = 0; j < subElements.size(); j++)
							{
								ModelElement subElement = ((ModelElement) subElements.get(j));
								double y = (j + 1.) / (subElements.size() + 1.);
								RelativePosition position = new RelativePosition(1., y);
								Slot slot = model.createSlot(SlotType.EXIT, position, subElement
										.getRole());
								slot.setDataTypeId(subElement.getRefId());
								slot.setProperty(BuiltinProperties.MANDATORY, subElement
										.getProperty(BuiltinProperties.MANDATORY));
								slot.setRepetitive(subElement.isRepetitive());

								Path elementPath = new Path(dataElementRole);
								elementPath.addSegment(subElement.getRole());
								Link flow = new Link(elementPath, new Path(slot.getRole()));
								flow.setRole(Role.get("Flow from element " + subElement.getRole()));
								model.addElement(flow);
							}
						}

						// Link exit of the wrapped service to the corresponding exit of the wrapping service (or the intermediate data structure)
						Link Exitflow = new Link(innerPath, outerPath);
						Exitflow.setRole(Role.get("Flow from exit " + role));
						model.addElement(Exitflow);
					}
				}
			}

			return model;
		}

		private void addOptionalSoapAttributes(ModelId compositeDataTypeId)
		{
			DataType dataType = (DataType) repository.getModel(compositeDataTypeId, true);
			if (Composition.ATOMIC.equals(dataType.getComposition()))
			{
				System.err.println(compositeDataTypeId + ": Not a composite data type");
				return;
			}

			// Add #actor (http://www.w3.org/TR/2000/NOTE-SOAP-20000508/#_Toc478383499)
			if (dataType.getElement(SpecialRoles.ACTOR_ATTRIBUTE_ROLE) == null)
			{
				DataElement leafElement = new DataElement();
				leafElement.setRole(SpecialRoles.ACTOR_ATTRIBUTE_ROLE);
				leafElement.setRefId(new ModelId(SpecialRoles.XML_PACKAGE
						+ "/www.w3.org/2001/XMLSchema/anyURI")); // Another type?
				leafElement.setProperty(BuiltinProperties.MANDATORY, Boolean.FALSE);
				leafElement.setRepetitive(false);
				dataType.addElement(leafElement);
			}

			// Add #mustUnderstand (http://www.w3.org/TR/2000/NOTE-SOAP-20000508/#_Toc478383500)
			// Note that this attribute is defined in http://schemas.xmlsoap.org/soap/envelope/
			if (dataType.getElement(SpecialRoles.MUST_UNDERSTAND_ATTRIBUTE_ROLE) == null)
			{
				DataElement leafElement = new DataElement();
				leafElement.setRole(SpecialRoles.MUST_UNDERSTAND_ATTRIBUTE_ROLE);
				leafElement.setRefId(new ModelId(SpecialRoles.XML_PACKAGE
						+ "/www.w3.org/2001/XMLSchema/int")); // Boolean?
				leafElement.setProperty(BuiltinProperties.MANDATORY, Boolean.FALSE);
				leafElement.setRepetitive(false);
				dataType.addElement(leafElement);
			}

			// Add #encodingStyle (http://www.w3.org/TR/2000/NOTE-SOAP-20000508/#_Toc478383495)
			if (dataType.getElement(SpecialRoles.ENCODING_STYLE_ATTRIBUTE_ROLE) == null)
			{
				DataElement leafElement = new DataElement();
				leafElement.setRole(SpecialRoles.ENCODING_STYLE_ATTRIBUTE_ROLE);
				leafElement.setRefId(new ModelId(SpecialRoles.XML_PACKAGE
						+ "/www.w3.org/2001/XMLSchema/string")); // Another type?
				leafElement.setProperty(BuiltinProperties.MANDATORY, Boolean.FALSE);
				leafElement.setRepetitive(false);
				dataType.addElement(leafElement);
			}
		}
	}

	private String idToString(ModelId modelId, Role elementRole)
	{
		return (modelId + (elementRole == null ? "" : "|" + elementRole));
	}

	public boolean canUndo()
	{
		return false; // Check what is needed to allow undo
    }
}
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.wizards;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import tersus.model.BuiltinProperties;
import tersus.model.Link;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 */
public class RenameWizardPage extends WizardPage implements IWizardPage
{
	public static final String NAME = "Main";
	
	private static final String LABEL_KEY = "Label";
	
	private RenameWizard wizard;

	private Text elementNameText;

	private Text modelNameText;
	
	private Text constantValueText;

	private Button syncNames;
	
	private Button syncReferenceNames;

	private Button syncPackageName;
	
	private Button syncConstantValueAndModelName;

	private Label referenceCountLabel;

	public RenameWizardPage(RenameWizard wizard)
	{
		super(NAME);
		setTitle("Rename");
		setDescription("Type the new element name and/or model name.");
		this.wizard = wizard;
	}

	public void createControl(Composite parent)
	{
		Composite container = new Composite(parent, SWT.NULL);
		
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		layout.verticalSpacing = 9;
		container.setLayout(layout);

		if (wizard.getElement() != null)
			addElementNameTextField(container);
		if (wizard.getElement() != null && wizard.getModel() != null)
			addSyncNamesCheckbox(container);
		if (wizard.getModel() != null)
			addModelNameTextField(container);
		
		if (wizard.isConstant())
		{
			addSyncConstantCheckbox(container);
        	addConstantTextField(container);
        }
		if (wizard.renameOtherReferencesIsRelevant())
			addSyncReferenceNamesCheckbox(container);
		if (wizard.getModel() != null
				&& wizard.getModel().getName().equals(
						wizard.getModel().getPackage().getPackageId().getName()))
			addSyncPackageNameCheckbox(container);

		if (wizard.getModel() != null)
			addReferenceCountLabel(container);

		setPageComplete(false);
		setControl(container);
		init();
	}

	private void addSyncReferenceNamesCheckbox(Composite container)
	{
		syncReferenceNames = new Button(container, SWT.CHECK);
		syncReferenceNames.setText("Keep the other reference names equal to model name");
		syncReferenceNames.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true,
				GridData.HORIZONTAL_ALIGN_END, GridData.VERTICAL_ALIGN_CENTER));
		syncReferenceNames.setSelection(true);
		wizard.setSyncReferences(true);
		syncReferenceNames.addSelectionListener(new SelectionListener()
		{
			public void widgetSelected(SelectionEvent e)
			{
				wizard.setSyncReferences(syncReferenceNames.getSelection());
				validate();
			}

			public void widgetDefaultSelected(SelectionEvent e)
			{
				wizard.setSyncReferences(syncReferenceNames.getSelection());
				validate();
			}
		});
	}

	private void addReferenceCountLabel(Composite container)
	{
		referenceCountLabel = new Label(container, SWT.WRAP);
		GridData gd = new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);
		gd.horizontalSpan = 3;
		referenceCountLabel.setLayoutData(gd);
	}

	private void addElementNameTextField(Composite container)
	{
		String labelText = "&Element Name (local):";
		elementNameText = createLabeledTextField(container, labelText);
		elementNameText.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				setElementName(elementNameText.getText());
			}
		});
	}

	private void addModelNameTextField(Composite container)
	{
		String labelText = "&Model Name (shared):";
		modelNameText = createLabeledTextField(container, labelText);
		modelNameText.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				setModelName(modelNameText.getText());
			}
		});
	}

	private void addConstantTextField(Composite container)
	{
		String labelText = "&Constant Value (shared):";
		constantValueText = createLabeledTextField(container, labelText);
		constantValueText.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				setConstantValue(constantValueText.getText());
			}
		});
		
		if (syncConstantValueAndModelName != null)
        	constantValueText.setEnabled(!syncConstantValueAndModelName.getSelection());
	}

	private void addSyncConstantCheckbox(Composite container)
    {
        syncConstantValueAndModelName = new Button(container, SWT.CHECK);
        syncConstantValueAndModelName.setText("Keep the model name and constant value equal");
        syncConstantValueAndModelName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, GridData.HORIZONTAL_ALIGN_END, GridData.VERTICAL_ALIGN_CENTER));
        
        syncConstantValueAndModelName.addSelectionListener(new SelectionAdapter() 
	    {
			public void widgetSelected(SelectionEvent e) 
			{
				setSyncConstantNameAndValue(syncConstantValueAndModelName.getSelection());
			}
		});

    }
	
	private Text createLabeledTextField(Composite container, String labelText)
	{
		Label label = new Label(container, SWT.NULL);
		label.setText(labelText);
		GridData gd = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_CENTER);
		gd.horizontalSpan = 2;
		label.setLayoutData(gd);

		Text textField = new Text(container, SWT.BORDER | SWT.SINGLE);
		textField.setLayoutData(new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_CENTER));
		textField.setData(LABEL_KEY, label);
		return textField;
	}

	private void addSyncNamesCheckbox(Composite container)
	{
		syncNames = new Button(container, SWT.CHECK);
		syncNames.setText("Keep the model name and element name equal");
		syncNames.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true,
				GridData.HORIZONTAL_ALIGN_END, GridData.VERTICAL_ALIGN_CENTER));

		syncNames.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				setSyncNames(syncNames.getSelection());
			}
		});
		syncNames.setSelection(true);
	}

	private void addSyncPackageNameCheckbox(Composite container)
	{
		syncPackageName = new Button(container, SWT.CHECK);
		syncPackageName.setText("Keep package name equal to model name");
		syncPackageName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true,
				GridData.HORIZONTAL_ALIGN_END, GridData.VERTICAL_ALIGN_CENTER));
		syncPackageName.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				setSyncPackageName(syncPackageName.getSelection());
			}
		});

		setSyncPackageName(true);
	}

	private void init()
    {
        ModelObject obj = wizard.getModelObject();
        Model model;
        ModelElement element = null;
        if (obj instanceof Model)
        {
            model = (Model) obj;
            modelNameText.setText(model.getName());
            modelNameText.selectAll();
        }
        else
        {
            element = (ModelElement) obj;
            model = element.getReferredModel();
            elementNameText.setEnabled(true);
            elementNameText.setText(element.getRole().toString());
            elementNameText.selectAll();
            if (model != null)
            {
                modelNameText.setText(model.getName());
                boolean sameName = element.getRole().toString().equals(
                        model.getName());
                setSyncNames(sameName);
                if (element.getParentModel().isReadOnly())
                    elementNameText.setEnabled(false);
            }
        }

        if (model != null)
        {
            int refCount = ((WorkspaceRepository) model.getRepository())
                    .getUpdatedRepositoryIndex().getReferenceEntries(model.getId()).size();
            if (refCount > 1)
            {
                referenceCountLabel
                        .setText("The model '" + model.getId() + "' has "
                                + (refCount - 1) + " additional references.");
                referenceCountLabel.setVisible(true);
                referenceCountLabel.setForeground(ColorConstants.blue);
                if (syncNames!= null)
                    setSyncNames(false); //If there's more than one reference, we don't change the element name by default. 
            }
            else
            {
                referenceCountLabel.setVisible(false);
            }
            if (wizard.isConstant())
            {
            	constantValueText.setText((String)wizard.getModel().getProperty(BuiltinProperties.VALUE));
            	
            	if (constantValueText.getText().equals(wizard.getModelName()))
            	{
            		syncConstantValueAndModelName.setSelection(true);
            		constantValueText.setEnabled(false);
            	}
            	else
            	{
            		syncConstantValueAndModelName.setSelection(false);
            		constantValueText.setEnabled(true);
            	}
            }
            if (model.isReadOnly())
            {
                modelNameText.setEnabled(false);
                syncNames.setSelection(false);
                syncNames.setEnabled(false);
                if (constantValueText != null)
                {
                	syncConstantValueAndModelName.setSelection(true);
                	syncConstantValueAndModelName.setEnabled(false);
                	constantValueText.setEnabled(false);
                }
            }
            
            if (wizard.getInitialElementName()!=null && elementNameText != null)
            	elementNameText.setText(wizard.getInitialElementName());
            
            if (wizard.isConstant() && constantValueText.getText().contains("/"))
            {
            	syncConstantValueAndModelName.setSelection(false);
            	constantValueText.setEnabled(true);
            }
        }
    }
	
	protected void setSyncConstantNameAndValue(boolean sync)
    {
		constantValueText.setEnabled(!sync);
        
        if (sync)
        	constantValueText.setText(modelNameText.getText());
    }

	private void checkIfReadOnly(Model model, ModelElement element)
	{
		if (element != null && element.getParentModel().isReadOnly())
		{
			setReferenceCountLabelMessage("The Element is read only and cannot be changed.");
			elementNameText.setEnabled(false);
			setModelRenameEnable(false);
		}
		else if (model != null && model.isReadOnly())
		{
			setReferenceCountLabelMessage("The Model is read only and cannot be changed.");
			setModelRenameEnable(false);
		}
		else
		{
			if (model != null && model.getRepository() instanceof WorkspaceRepository)
			{
				WorkspaceRepository workspaceRepository = (WorkspaceRepository) model
						.getRepository();
				checkReadOnlyReferencesAndRefferedLinks(workspaceRepository, model);
			}
		}
	}

	private void checkReadOnlyReferencesAndRefferedLinks(WorkspaceRepository workspaceRepository,
			Model model)
	{
		for (ModelElement reference : workspaceRepository.getReferences(model.getId()))
		{
			if (reference.getParentModel().isReadOnly())
			{
				setReferenceCountLabelMessage("This model has reference which located in read only parent.");
				setModelRenameEnable(false);
				break;
			}

			for (Link link : workspaceRepository.getUpdatedRepositoryIndex().getReferringLinks(
					reference))
			{
				if (link.getParentModel().isReadOnly())
				{
					setReferenceCountLabelMessage("This model has reffered link which located in read only parent.");
					setModelRenameEnable(false);
					break;
				}
			}
		}
	}

	private void setModelRenameEnable(boolean value)
	{
		modelNameText.setEnabled(value);

		if (syncNames != null)
		{
			syncNames.setSelection(value);
			syncNames.setEnabled(value);
		}

		if (syncPackageName != null)
		{
			syncPackageName.setSelection(value);
			syncPackageName.setEnabled(value);
		}

		if (syncReferenceNames != null)
		{
			syncReferenceNames.setSelection(value);
			syncReferenceNames.setEnabled(value);
		}

		if (constantValueText != null)
			constantValueText.setEnabled(value);
	}

	private void setReferenceCountLabelMessage(String msg)
	{
		referenceCountLabel.setForeground(ColorConstants.blue);

		if (!referenceCountLabel.isVisible())
			referenceCountLabel.setVisible(true);

		String referenceCountLabelOldMessage = referenceCountLabel.getText();

		if (referenceCountLabel.getText() != null && referenceCountLabel.getText().length() != 0)
			referenceCountLabel.setText(referenceCountLabelOldMessage + "\n\n" + msg);
		else
			referenceCountLabel.setText(msg);
	}

	protected void setSyncNames(boolean sync)
	{
		syncNames.setSelection(sync);
		modelNameText.setEnabled(!sync);
		if (sync)
		{
			modelNameText.setText(elementNameText.getText());
			setModelName(elementNameText.getText());
		}
	}

	protected void setSyncReferenceNames(boolean sync)
	{
		syncReferenceNames.setSelection(sync);
	}

	protected void setSyncPackageName(boolean sync)
	{
		syncPackageName.setSelection(sync);
		wizard.setSyncPackageName(sync);
	}

	protected void setModelName(String text)
    {	
        wizard.setModelName(text);
        
        if ( constantValueText != null && syncConstantValueAndModelName != null && syncConstantValueAndModelName.getSelection())
        	constantValueText.setText(text);
        
        validate();
    }

	protected void setConstantValue(String text)
	{
		wizard.setConstantValue(text);
		validate();
	}

	public void setElementName(String text)
    {
        wizard.setElementName(text);
        
        if (syncNames != null && syncNames.getSelection())
        {
        	modelNameText.setText(text);
            setModelName(text);
        }
        
        validate();
    }

	public void validate()
	{
		wizard.validate();
		setErrorMessage(wizard.getErrorMessage());
		setPageComplete(wizard.isReady());
	}

}
/************************************************************************************************
 * Copyright (c) 2003-2021 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. -  Initial API and implementation
 *************************************************************************************************/

package tersus.editor.wizards;

/**
 * @author Liat Shiff
 */
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import tersus.workbench.TersusExportWizardPageBase;
import tersus.workbench.TersusWorkbench;

public class TersusExportProjectWizardPage extends TersusExportWizardPageBase implements IWizardPage
{
	protected TersusExportProjectWizard wizard;
	
	private Button exportAsLibrary;
	
	private Button exportAsApplication;
	
	private Button includeDBFolder;
	
	public TersusExportProjectWizardPage(TersusExportProjectWizard wizard)
	{
		super(TersusExportProjectWizard.WIZARD_NAME);
		this.wizard = wizard;
		
		setTitle(TersusExportProjectWizard.WIZARD_NAME);
		setDescription("Export a project as a Tersus file");

	}

	public void createControl(Composite parent)
	{
		super.createControl(parent);
		initProjectComboBox();
		initLocationText();
		createExportAsButtons();
		createCopyDBFolderButton();
		setPageComplete(isPageComplete());
	}

	private void createCopyDBFolderButton()
	{   
		includeDBFolder = new Button(container, SWT.CHECK);
		includeDBFolder.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true,
				GridData.HORIZONTAL_ALIGN_END, GridData.VERTICAL_ALIGN_CENTER));
		includeDBFolder.setText("&Include 'database' folder");
		
		includeDBFolder.addSelectionListener(new SelectionListener()
		{
			public void widgetSelected(SelectionEvent e)
			{
				wizard.setIncludeDBFolder(includeDBFolder.getSelection());
			}

			public void widgetDefaultSelected(SelectionEvent e)
			{
				wizard.setIncludeDBFolder(includeDBFolder.getSelection());
			}
		});
		
		includeDBFolder.setSelection(false);
		wizard.setIncludeDBFolder(false);
	}
	
	private void createExportAsButtons()
	{
		Label lable = new Label(container, SWT.NONE);
		lable.setText("Export As:");
		
		Composite con = new Composite(container, SWT.NONE);
		
		GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.verticalSpacing = 9;
        con.setLayout(layout);
		
        exportAsApplication = new Button(con, SWT.RADIO);
        exportAsApplication.setText("&Application");
        exportAsApplication.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				exportAsApplication.setSelection(true);
				exportAsLibrary.setSelection(false);
				wizard.setExportAsLibrary(false);
			}
		});
        
		exportAsLibrary = new Button(con, SWT.RADIO);
		exportAsLibrary.setText("&Library");
		exportAsLibrary.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				exportAsLibrary.setSelection(true);
				exportAsApplication.setSelection(false);
				wizard.setExportAsLibrary(true);
			}
		});
		
		initExportRadioButtons();
	}

	protected void initProjectComboBox()
	{
		for (IProject project : TersusWorkbench.getWorkspace().getRoot().getProjects())
		{
			if (project.isOpen() && TersusWorkbench.isTersusProject(project))
			{
				projectCombo.add(project.getName());
				if (project == wizard.getProject())
					projectCombo.setText(project.getName());
				setPageComplete(isPageComplete());
			}
		}

		projectCombo.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				wizard.setProject(TersusWorkbench.getWorkspace().getRoot().getProject(
						projectCombo.getText()));
				setDefaultLocationText();
				initExportRadioButtons();
				setPageComplete(isPageComplete());
			}
		});
		
	}

	private void initExportRadioButtons()
	{
		if (wizard.defaultLibrarySetting())
		{
			exportAsLibrary.setSelection(true);
        	exportAsApplication.setSelection(false);
		}
        else
        {
        	exportAsLibrary.setSelection(false);
        	exportAsApplication.setSelection(true);
        }
	}
	
	protected void initLocationText()
	{
		locationText.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				wizard.setExportLocation(locationText.getText());
				setPageComplete(isPageComplete());
			}
		});
		setDefaultLocationText();
	}

	protected void setDefaultLocationText()
	{
		locationText.setText(wizard.getDefaultExportLocation());
	}

	public boolean isPageComplete()
	{
		return wizard.getProject() != null
				&& locationText.getText() != null && locationText.getText() != "";
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.wizards;

/**
 * @author Ofer Brandes
 *
 */
public class WsdlImportEception extends RuntimeException
{

    /**
     * 
     */
    public WsdlImportEception()
    {
        super();
    }

    /**
     * @param message
     */
    public WsdlImportEception(String message)
    {
        super(message);
    }

    /**
     * @param message
     * @param cause
     */
    public WsdlImportEception(String message, Throwable cause)
    {
        super(message, cause);
    }

    /**
     * @param cause
     */
    public WsdlImportEception(Throwable cause)
    {
        super(cause);
    }

}

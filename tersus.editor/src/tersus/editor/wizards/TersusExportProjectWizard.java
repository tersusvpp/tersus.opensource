/************************************************************************************************
 * Copyright (c) 2003-2023 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
package tersus.editor.wizards;

import java.io.File;
import java.io.FileOutputStream;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IExportWizard;
import org.eclipse.ui.IWorkbench;

import tersus.ProjectStructure;
import tersus.workbench.Configuration;
import tersus.workbench.Configurator;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.TersusZipFileUtils;

/**
 * @author Liat Shiff
 */
public class TersusExportProjectWizard extends Wizard implements IExportWizard
{
    public static final String WIZARD_NAME="Export Tersus Project";

    private IProject project;

	private String exportLocation;

	private boolean exportAsLibrary;
	
	private boolean includeDBFolder;

	private boolean removeConfigFile;

	public TersusExportProjectWizard()
	{
		super();
		setWindowTitle(WIZARD_NAME);

		exportAsLibrary = defaultLibrarySetting();
		includeDBFolder = false;
		removeConfigFile = false;
	}

	public boolean defaultLibrarySetting()
	{
		if (getProject() != null)
		{
			Configuration config = Configurator.getConfiguration(getProject());
			return config != null && config.isLibrary();
		}
		else
			return false;
	}

	public void init(IWorkbench workbench, IStructuredSelection selection)
	{
		if (selection.size() == 1 && selection.getFirstElement() instanceof IProject)
			project = (IProject) selection.getFirstElement();
	}

	public void addPages()
	{
		addPage(new TersusExportProjectWizardPage(this));
	}

	public String getDefaultExportLocation()
	{
		if (project == null)
			return "";
		else
			return getDefaultLocation(project).getPath();
	}

	public void setExportLocation(String location)
	{
		this.exportLocation = location;
	}

	public IProject getProject()
	{
		return project;
	}

	public void setProject(IProject project)
	{
		this.project = project;
	}

	public boolean getExportAsLibrary()
	{
		return exportAsLibrary;
	}

	public void setExportAsLibrary(boolean exportAsLibrary)
	{
		this.exportAsLibrary = exportAsLibrary;
	}

	public void setIncludeDBFolder(boolean includeDBFolder)
	{
		this.includeDBFolder = includeDBFolder;
	}
	
	public File getDefaultLocation(IProject project)
	{
		File desktopFolder = new File(System.getProperty("user.home") + "/Desktop");
		if (desktopFolder.exists() && desktopFolder.isDirectory())
			return new File(desktopFolder, project.getName() + ".tersus");
		else
			return new File(new File(System.getProperty("user.home")), project.getName()
					+ ".tersus");
	}

	public boolean performFinish()
	{
		prepareConfigurationFile();

		byte[] zipBytes = TersusZipFileUtils.prepareZipFile(project, includeDBFolder);
		if (zipBytes == null)
			return false;

		if (!exportLocation.endsWith(".tersus"))
			exportLocation = exportLocation.concat(".tersus");

		FileOutputStream out = null;
		try
		{
			out = new FileOutputStream(exportLocation);

			out.write(zipBytes);
			out.close();
			out = null;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (out != null)
			{
				try
				{
					out.close();
				}
				catch (Exception e)
				{
					TersusWorkbench.log(e);
				}
			}
		}

		if (removeConfigFile)
		{
			IFile configFile = project.getFile(ProjectStructure.CONFIGURATION_XML);
			if (configFile.exists())
			{
				try
				{
					configFile.delete(true, null);
				}
				catch (CoreException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return true;
	}

	private void prepareConfigurationFile()
	{
		IFile configFile = project.getFile(ProjectStructure.CONFIGURATION_XML);
		if (!configFile.exists())
			removeConfigFile = true; 

		Configuration config = Configurator.getConfiguration(getProject());
		if (config != null && exportAsLibrary != config.isLibrary())
		{
			config.setLibrary(exportAsLibrary);
			config.save(configFile);
		}
		else if (config == null && exportAsLibrary)
		{
			config = new Configuration();
			config.setLibrary(true);
			config.save(configFile);
		}
	}
}

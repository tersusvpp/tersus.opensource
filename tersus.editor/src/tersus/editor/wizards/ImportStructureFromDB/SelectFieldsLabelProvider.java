/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.wizards.ImportStructureFromDB;

import java.util.ArrayList;

import org.eclipse.jface.viewers.ColumnLabelProvider;

import tersus.model.TersusTableFieldDescriptor;

/**
 * 
 * @author Liat Shiff
 */

public class SelectFieldsLabelProvider extends ColumnLabelProvider
{
	private ArrayList<TersusTableFieldDescriptor> input;

	public SelectFieldsLabelProvider(ArrayList<TersusTableFieldDescriptor> input)
	{
		this.input = input;
	}

	public String getText(Object element)
	{
		if (element instanceof String)
		{
			String columnName = (String) element;
			for (TersusTableFieldDescriptor descriptor : input)
			{
				if (descriptor.getColumnName().equals(columnName))
					return descriptor.getPrimaryKey().booleanValue() ? columnName
							+ " - (primary Key)" : columnName;
			}
		}
		return "";
	}
}

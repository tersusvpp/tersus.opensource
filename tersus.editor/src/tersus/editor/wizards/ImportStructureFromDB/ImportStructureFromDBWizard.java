/************************************************************************************************
 * Copyright (c) 2003-2023 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.wizards.ImportStructureFromDB;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Properties;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Shell;

import tersus.ProjectStructure;
import tersus.editor.TersusEditor;
import tersus.model.BuiltinProperties;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.TersusTableFieldDescriptor;
import tersus.model.commands.ImportStructureFromDBCommand;
import tersus.util.DataSourceProperties;
import tersus.util.Misc;
import tersus.util.ObservableList;
import tersus.util.SQLUtils;
import tersus.workbench.Configuration;
import tersus.workbench.Configurator;
import tersus.workbench.Parameter;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat Shiff
 */
public class ImportStructureFromDBWizard extends Wizard
{
	private static final String JDBC_MAIN = "jdbc/Main";

	private ModelElement databaseRecordElement;

	private TersusEditor editor;

	private Shell shell;

	private ImportStructureFromDBWizardPage page;

	private ImportStructureFromDBCommand command;

	private ObservableList<TersusTableFieldDescriptor> dataRecordFields;

	private ArrayList<TersusTableFieldDescriptor> dataBaseColumns;

	private String errorMessage;

	private boolean canEditTable;

	private boolean addPrimaryKeyFields;

	// Data Source:
	private HashSet<String> dataSourceNameList;

	private String currentDataSource;

	// Catalog:
	private HashSet<String> catalogNameList;

	private String currentCatalog;

	// Schema:
	private HashSet<String> schemaNameList;

	private String currentSchema;

	// Table:
	private HashSet<String> tableNameList;

	private String currentTable;

	private Properties dataSourceProperties;

	public ImportStructureFromDBWizard(ModelElement element, TersusEditor editor, Shell sell,
			boolean canEditTable, boolean addPrimaryKeyFields)
	{
		setWindowTitle("Import Structure From Database");

		this.databaseRecordElement = element;
		this.editor = editor;
		this.canEditTable = canEditTable;
		this.addPrimaryKeyFields = addPrimaryKeyFields;

		setInputFields();

		command = new ImportStructureFromDBCommand((WorkspaceRepository) element.getRepository(),
				element);

		ArrayList<TersusTableFieldDescriptor> initialFields = new ArrayList<TersusTableFieldDescriptor>();

		for (TersusTableFieldDescriptor descriptor : command.getInitialFields())
		{
			setStatusField(descriptor);
			initialFields.add(descriptor);
		}

		dataRecordFields = new ObservableList<TersusTableFieldDescriptor>(initialFields);
	}

	public void addPages()
	{
		page = new ImportStructureFromDBWizardPage(this);
		addPage(page);
	}

	private void setInputFields()
	{
		Model model = this.databaseRecordElement.getReferredModel();

		Object dataSourceObj = model.getProperty(BuiltinProperties.DATA_SOURCE);
		if (dataSourceObj != null && dataSourceObj instanceof String)
		{
			currentDataSource = (String) dataSourceObj;
			createDataSourceProperties();
		}

		Object schemaObject = model.getProperty(BuiltinProperties.SCHEMA);
		if (schemaObject != null && schemaObject instanceof String)
			currentSchema = (String) schemaObject;

		Object tableNameObj = model.getProperty(BuiltinProperties.TABLE_NAME);
		if (tableNameObj != null && tableNameObj instanceof String)
			currentTable = (String) tableNameObj;

		Object catalogObj = model.getProperty(BuiltinProperties.CATALOG);
		if (catalogObj != null && catalogObj instanceof String)
			currentCatalog = (String) catalogObj;
	}

	public HashSet<String> getDataSourceNameList()
	{
		if (dataSourceNameList == null)
			createDataSourceNameList();

		return dataSourceNameList;
	}

	/**
	 * Support data sources and resources.
	 */
	public void createDataSourceNameList()
	{
		dataSourceNameList = new HashSet<String>();
		Configuration configuration = Configurator.getConfiguration(editor.getRepository()
				.getProject());

		String dataSourceName = null;

		for (Properties datasourceProperties : configuration.getDataSources())
		{
			dataSourceName = datasourceProperties.getProperty(Configuration.DATA_SOURCE_NAME);

			if (!dataSourceNameList.contains(dataSourceName))
				dataSourceNameList.add(dataSourceName);
		}

		for (Properties resourceProperties : configuration.getResources())
		{
			String type = resourceProperties.getProperty(Configuration.TYPE_PROPERTY);
			if (type != null && type.equals("javax.sql.DataSource"))
			{
				dataSourceName = resourceProperties.getProperty(Configuration.DATA_SOURCE_NAME);
				if (!dataSourceNameList.contains(dataSourceName))
					dataSourceNameList.add(dataSourceName);
			}
		}
		if (!dataSourceNameList.contains(JDBC_MAIN))
		dataSourceNameList.add(JDBC_MAIN);
	}

	public String getCurrentSchema()
	{
		if (currentSchema == null || currentSchema.length() == 0)
			return null;
		return currentSchema;
	}

	public void setCurrentSchema(String schema)
	{
		currentSchema = schema;
	}

	public HashSet<String> getSchemaNameList() throws SQLException, RuntimeException
	{
		if (schemaNameList == null)
			createSchemaNameList();

		return schemaNameList;
	}

	public void setSchemaNameList(HashSet<String> schemaNameList)
	{
		this.schemaNameList = schemaNameList;
	}

	private void createSchemaNameList() throws SQLException, RuntimeException
	{
		if (getCurrentDataSource() != null && getDataSourceProperties() != null)
		{	
			Connection con = null;
			try
			{
				con = SQLUtils.connectToDataSource(getCurrentDataSource(),
						getDataSourceProperties());
				
				String databaseProductName = con.getMetaData().getDatabaseProductName();
				
				if (getCurrentCatalog() != null || SQLUtils.MYSQL.equals(databaseProductName))
				{
					if ("DB2 UDB for AS/400".equals(databaseProductName) || SQLUtils.MYSQL.equals(databaseProductName))
					{
						String url = dataSourceProperties.getProperty(SQLUtils.URL_PROPERTY);
						String[] urlParts = url.split("[;?]");
						int index = urlParts[0].lastIndexOf('/');
						String schema = urlParts[0].substring(index + 1);
						schemaNameList = new HashSet<String>();
						schemaNameList.add(schema);
					}
					else
						schemaNameList = SQLUtils.getSchemaNameList(con, getCurrentCatalog());
				}

				con.close();
				con = null;
			}
			finally
			{
				SQLUtils.forceClose(con);
			}
		}
	}

	private void getSchemaListFromConfigFile()
	{
		Configuration configuration = Configurator.getConfiguration(editor.getRepository()
				.getProject());
		
		ArrayList<Parameter> parameters = configuration.getParameters();
		if (parameters != null && !parameters.isEmpty())
		{
			for (Parameter parameter : parameters)
			{
				if ("schema_list".equals(parameter.getName()))
				{
					String schemaListValue = parameter.getValue();
					String[] schemas = schemaListValue.split(",");
					for (String schema: schemas)
					{
						if (schemaNameList == null)
							schemaNameList = new HashSet<String>();
						if (!schemaNameList.contains(schema))
							schemaNameList.add(schema);
					}
				}
			}
		}
	}
	
	public String getCurrentCatalog()
	{
		if (currentCatalog == null || "".equals(currentCatalog))
			return null;
		return currentCatalog;
	}

	public void setCurrentCatalog(String catalog)
	{
		currentCatalog = catalog;
	}

	public void setCatalogNameList(HashSet<String> catalogNameList)
	{
		this.catalogNameList = catalogNameList;
	}

	public HashSet<String> getCatalogNameList() throws SQLException, RuntimeException
	{
		if (catalogNameList == null)
			createCatalogNameList();

		return catalogNameList;
	}

	private void createCatalogNameList() throws SQLException, RuntimeException
	{
		if (getCurrentDataSource() != null && getDataSourceProperties() != null)
		{
			Connection con = null;
			try
			{
				con = SQLUtils.connectToDataSource(getCurrentDataSource(),
						getDataSourceProperties());
				catalogNameList = SQLUtils.getCatalogNameList(con);

				con.close();
				con = null;
			}
			finally
			{
				SQLUtils.forceClose(con);
			}
		}
	}

	public HashSet<String> getTableNameList() throws SQLException, RuntimeException
	{
		if (tableNameList == null)
			createTableNameList();

		return tableNameList;
	}

	public void setTableNameList(HashSet<String> tableNameList)
	{
		this.tableNameList = tableNameList;
	}

	private void createTableNameList() throws SQLException
	{
		if (getCurrentDataSource() != null && getDataSourceProperties() != null
				)
		{
			Connection con = null;
			try
			{
				con = SQLUtils.connectToDataSource(getCurrentDataSource(),
						getDataSourceProperties());
				if (! SQLUtils.HSQLDB.equals(con.getMetaData().getDatabaseProductName()) &&  getCurrentCatalog() == null && getCurrentSchema() == null)
					return; //Currently only HSQLDB has no schema and catalog
				tableNameList = SQLUtils.getTableNameList(con, getCurrentCatalog(),
						getCurrentSchema());

				con.close();
				con = null;
			}
			finally
			{
				SQLUtils.forceClose(con);
			}
		}
	}

	public Shell getShell()
	{
		return shell;
	}

	public TersusEditor getEditor()
	{
		return editor;
	}

	public boolean canEditTable()
	{
		return canEditTable;
	}

	public boolean isAddPrimaryKeyFields()
	{
		return addPrimaryKeyFields;
	}

	public ObservableList<TersusTableFieldDescriptor> getDataRecordFields()
	{
		return dataRecordFields;
	}

	// Data source functions:

	public String getCurrentDataSource()
	{
		if (currentDataSource == null || "".equals(currentDataSource))
			return null;
		return currentDataSource;
	}

	public void setCurrentDataSource(String dataSource)
	{
		currentDataSource = dataSource;
		if (dataSource != null)
			createDataSourceProperties();
	}

	private Properties getDataSourceProperties()
	{
		return dataSourceProperties;
	}

	private void createDataSourceProperties()
	{
		Configuration configuration = Configurator.getConfiguration(editor.getRepository()
				.getProject());

		dataSourceProperties = null;
		for (Properties properties : configuration.getDataSources())
		{
			String name = properties.getProperty(Configuration.DATA_SOURCE_NAME);

			if (getCurrentDataSource().equals(name))
				dataSourceProperties = properties;
		}

		for (Properties properties : configuration.getResources())
		{
			String type = properties.getProperty(Configuration.TYPE_PROPERTY);
			if (type != null && type.equals("javax.sql.DataSource"))
			{
				String name = properties.getProperty(Configuration.DATA_SOURCE_NAME);
				if (getCurrentDataSource().equals(name))
					dataSourceProperties = properties;
			}
		}
		if (dataSourceProperties == null && JDBC_MAIN.equals(getCurrentDataSource()))
		{
			IProject project = editor.getRepository().getProject();
			String dbPath = project.getFolder(ProjectStructure.DATABASE).getFile(project.getName()).getLocation().toFile().getAbsolutePath();
			Properties p  = new Properties();
			p.setProperty(Configuration.DATA_SOURCE_NAME, getCurrentDataSource());
			p.setProperty(DataSourceProperties.DRIVER_CLASS_NAME, "org.hsqldb.jdbcDriver");
	        p.setProperty(DataSourceProperties.USERNAME, "sa");
	        p.setProperty(DataSourceProperties.PASSWORD, "");
	        p.setProperty(DataSourceProperties.URL, "jdbc:hsqldb:file:" + dbPath);
	        p.setProperty(DataSourceProperties.VALIDATION_QUERY, "CALL Now()");
	        dataSourceProperties = p;
			
		}
	}

	public void addNewFields(ArrayList<String> selectedColumnNames)
	{
		ArrayList<TersusTableFieldDescriptor> newColumnsToAdd = new ArrayList<TersusTableFieldDescriptor>();

		for (String selectedColumnName : selectedColumnNames)
		{
			newColumnsToAdd.add(getColumnDescriptorByName(selectedColumnName));
		}
		dataRecordFields.addAll(newColumnsToAdd);
	}

	private TersusTableFieldDescriptor getColumnDescriptorByName(String name)
	{
		for (TersusTableFieldDescriptor descriptor : dataBaseColumns)
		{
			if (descriptor.getColumnName().equals(name))
				return descriptor;
		}
		// Descruptor was not found
		return null;
	}

	public boolean performFinish()
	{
		ArrayList<TersusTableFieldDescriptor> fields = new ArrayList<TersusTableFieldDescriptor>();
		fields.addAll(dataRecordFields.getContent());
		command.setNewDetails(currentDataSource, currentSchema, currentTable, currentCatalog,
				fields);

		((TersusEditor) editor).getEditDomain().getCommandStack().execute(command);
		return true;
	}

	public void setCurrentTable(String table)
	{
		currentTable = table;
	}

	public String getCurrentTableName()
	{
		return currentTable;
	}

	@SuppressWarnings("unchecked")
	public void setColumns(HashSet<TersusTableFieldDescriptor> list)
	{
		if (list != null)
		{
			if (dataBaseColumns != null)
				dataBaseColumns.clear();
			else
				dataBaseColumns = new ArrayList<TersusTableFieldDescriptor>();

			dataBaseColumns.addAll(list);
			Collections.sort(dataBaseColumns, new Comparator(){

				public int compare(Object o1, Object o2)
				{
					int p1= ((TersusTableFieldDescriptor)o1).getPosition();
					int p2= ((TersusTableFieldDescriptor)o2).getPosition();
					return p1-p2;
				}
});
		}
		else
			dataBaseColumns = null;
	}

	public void setColumns()
	{
		Connection con = null;
		try
		{
			con = SQLUtils.connectToDataSource(getCurrentDataSource(), getDataSourceProperties());
			HashSet<TersusTableFieldDescriptor> descriptors = SQLUtils.getColumnsParameters(
					con, currentCatalog, currentSchema, currentTable);
			setColumns(descriptors);

			con.close();
			con = null;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			throw new RuntimeException(
					"Failed to get columns parameters. You need to check your connection to DB.", e);
		}
		finally
		{
			SQLUtils.forceClose(con);
		}
	}

	public ArrayList<TersusTableFieldDescriptor> getDataBaseColumns()
	{
		return dataBaseColumns;
	}

	public TersusTableFieldDescriptor setStatusField(TersusTableFieldDescriptor descriptor)
	{
		int statusValidateNamber = statusValidate(descriptor);

		switch (statusValidateNamber)
		{
			case 0:
				descriptor.setStatus("X");
				break;
			case 1:
				descriptor.setStatus("V");
				break;
			case 2:
				descriptor.setStatus("?");
				break;
		}

		return descriptor;
	}

	/**
	 * 
	 * @param descriptor
	 * @return 0 = 'x' 1 = 'V' 2 = '?'
	 * @throws SQLException
	 */
	public int statusValidate(TersusTableFieldDescriptor descriptor)
	{
		if (getCurrentDataSource() != null)
		{
			TersusTableFieldDescriptor columnDescriptor = null;
			Connection con = null;

			try
			{
				con = SQLUtils.connectToDataSource(getCurrentDataSource(),
						getDataSourceProperties());
				columnDescriptor = SQLUtils.getColumnParameters(con, null, currentSchema,
						currentTable, descriptor.getColumnName());

				con.close();
				con = null;
			}
			catch (SQLException e)
			{
				// TODO Need to think what to do with it.
				e.printStackTrace();
			}
			finally
			{
				SQLUtils.forceClose(con);
			}

			if (columnDescriptor != null)
			{
				if (columnDescriptor.getColumnSize().equals(descriptor.getColumnSize())
						&& columnDescriptor.getColumnType().equals(descriptor.getColumnType()))
					return 1;
				else
					return 0;
			}
		}
		return 2;
	}

	public String getErrorMessage()
	{
		return errorMessage;
	}

	public void validate()
	{
		// TODO - Need to add more validation for the new columns.
		errorMessage = null;
		for (TersusTableFieldDescriptor descriptorToCheck : dataRecordFields.getContent())
		{
			for (TersusTableFieldDescriptor descriptor : dataRecordFields.getContent())
			{
				String descriptorToCheckElementName = Misc.sqlize(descriptorToCheck
						.getElementName());
				String descriptorElementName = Misc.sqlize(descriptor.getElementName());

				String descriptorToCheckColumnName = Misc.sqlize(descriptorToCheck.getColumnName());
				String descriptorColumnName = Misc.sqlize(descriptor.getColumnName());

				if (!descriptor.equals(descriptorToCheck))
				{
					if (descriptorElementName.equals(descriptorToCheckElementName))
						errorMessage = "There is an existing element called '"
								+ descriptorToCheckElementName + "'";
					else if (descriptorColumnName.equals(descriptorToCheckColumnName))
						errorMessage = "There is an existing column called '"
								+ descriptorToCheckColumnName + "'";
				}
			}

			if (errorMessage == null && descriptorToCheck.getColumnSize() != null)
				try
				{
					Integer.parseInt(descriptorToCheck.getColumnSize());
				}
				catch (NumberFormatException e)
				{
					errorMessage = "Illegal argument: the column size "
							+ descriptorToCheck.getColumnSize() + " is invalid.";
				}
		}
	}

	public boolean isReady()
	{
		return errorMessage == null;
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.wizards.ImportStructureFromDB;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ListSelectionDialog;

import tersus.model.TersusTableFieldDescriptor;

/**
 * @author Liat Shiff
 */
public class SelectFieldsDialog extends ListSelectionDialog
{
	private final String INITIAL_TEXT = "type filter text";

	private Text filterText;

	HashMap<String, Boolean> checkedElements;

	public SelectFieldsDialog(Shell parentShell,
			ArrayList<TersusTableFieldDescriptor> input, IStructuredContentProvider contentProvider,
			ILabelProvider labelProvider, String message)
	{
		super(parentShell, input, contentProvider, labelProvider, message);

		checkedElements = new HashMap<String, Boolean>();

		for (TersusTableFieldDescriptor descriptor : input)
		{
			checkedElements
					.put(descriptor.getColumnName(), new Boolean(descriptor.getPrimaryKey()));
		}
	}

	protected Control createDialogArea(Composite parent)
	{
		Composite topRow = new Composite(parent, SWT.NULL);
		GridLayout topRowLayout = new GridLayout();
		topRowLayout.numColumns = 3;
		topRowLayout.verticalSpacing = 0;
		topRow.setLayout(topRowLayout);

		createFilterText(parent);

		Control ctrl = super.createDialogArea(parent);
		final StringPatternFilter filter = new StringPatternFilter();

		filterText.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				if (getViewer() != null)
				{
					filter.setPattern(filterText.getText());
					getViewer().addFilter(filter);
					getViewer().setCheckedElements(getInitializeList().toArray());
				}
			}
		});

		getViewer().addCheckStateListener(new ICheckStateListener()
		{

			public void checkStateChanged(CheckStateChangedEvent event)
			{
				if (event.getElement() instanceof TersusTableFieldDescriptor
						&& !((TersusTableFieldDescriptor) event.getElement()).getPrimaryKey())
				{
					checkedElements.remove(event.getElement());
					checkedElements.put((String) event.getElement(), event.getChecked());
				}

				checkInitialSelections();
			}
		});

		return ctrl;
	}

	private void checkInitialSelections()
	{
		for (Object obj: getInitialElementSelections())
		{
			getViewer().setChecked(obj, true);
		}
	}

	private ArrayList<String> getInitializeList()
	{
		ArrayList<String> initializeList = new ArrayList<String>();

		for (String key : checkedElements.keySet())
		{
			if (checkedElements.get(key))
				initializeList.add(key);
		}
		return initializeList;
	}

	private void createFilterText(Composite composite)
	{
		filterText = new Text(composite, SWT.SINGLE | SWT.BORDER);

		filterText.addFocusListener(new FocusAdapter()
		{
			public void focusGained(FocusEvent e)
			{
				Display display = filterText.getDisplay();
				display.asyncExec(new Runnable()
				{
					public void run()
					{
						if (!filterText.isDisposed())
						{
							if (INITIAL_TEXT.equals(filterText.getText().trim()))
							{
								filterText.selectAll();
							}
						}
					}
				});
			}
		});

		GridData gridData = new GridData(SWT.FILL, SWT.BEGINNING, true, false);

		if ((filterText.getStyle() & SWT.CANCEL) != 0)
			gridData.horizontalSpan = 2;
		filterText.setLayoutData(gridData);

		filterText.setText(INITIAL_TEXT);
	}
}

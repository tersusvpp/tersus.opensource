/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.wizards.ImportStructureFromDB;

import java.util.ArrayList;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

/**
 * @author Liat Shiff
 */
public class ColumnContentProvider implements IStructuredContentProvider
{
	ArrayList<String> columns;
    
    public ColumnContentProvider (ArrayList<String> columns)
    {
        this.columns = columns;
    }
    
    public Object[] getElements(Object inputElement)
    {
    	return columns.toArray();
    }
    
    public void dispose()
    {
        // TODO Auto-generated method stub\
    }

    public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
    {
        // TODO Auto-generated method stub
        
    }
}

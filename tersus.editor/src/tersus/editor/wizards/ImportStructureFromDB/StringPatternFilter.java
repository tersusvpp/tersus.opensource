/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.wizards.ImportStructureFromDB;

import java.util.HashSet;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

import tersus.model.TersusTableFieldDescriptor;

/**
 * @author Liat Shiff
 */
public class StringPatternFilter extends ViewerFilter
{
	String pattern;
	
	public StringPatternFilter ()
	{
		super();
	}
	
	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) 
	{
		if (parentElement instanceof HashSet)
		{
			HashSet<TersusTableFieldDescriptor> columnNames = (HashSet<TersusTableFieldDescriptor>) parentElement;
			
			if (pattern == null || pattern.equals(""))
	    		return true;
			else
			{
				if (element instanceof String)
				{
					if (((String)element).toLowerCase().contains(pattern.toLowerCase()))
						for (TersusTableFieldDescriptor s: columnNames)
						{
							if (s.getColumnName().equals((String)element))
								return true;
						}
				}
			}	
		}
		return false;
	}
	
	public void setPattern(String pattern)
	{
		this.pattern = pattern;
	}

}

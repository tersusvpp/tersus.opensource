/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.wizards.ImportStructureFromDB;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnViewerEditor;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationStrategy;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TableViewerEditor;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;

import tersus.eclipse.utils.ListContentProvider;
import tersus.model.BuiltinModels;
import tersus.model.TersusTableFieldDescriptor;
import tersus.model.commands.TableFieldDescriptor;
import tersus.util.Misc;
import tersus.util.SQLUtils;
import tersus.workbench.SelectionHelper;
import tersus.workbench.TersusWorkbench;

/**
 * @author Liat Shiff
 */
public class ImportStructureFromDBWizardPage extends WizardPage implements IWizardPage
{
	private static final String LABEL_KEY = "Label";

	public final String[] BOOLEAN_VALUES =
	{ "false", "true" };

	protected static Object[] NONE = new Object[]
	{};

	private ImportStructureFromDBWizard wizard;

	private Composite parent;

	private Text dataSourceText;

	private Text catalogText;

	private Text schemaText;

	private Text tableText;

	private Button importFromDBButton;

	private Action removeFieldAction;

	protected TableViewer fieldViewer;

	private String[] dataTypeNames;

	private String[] columnTypeList;

	private TextCellEditor elementNameEditor;

	private TextCellEditor columnNameEditor;

	private TextCellEditor columnDescriptionEditor;

	private ComboBoxCellEditor dataTypeEditor;

	private ComboBoxCellEditor columnTypeEditor;

	private ComboBoxCellEditor primaryKeyEditor;

	private ComboBoxCellEditor nullableEditor;

	private TextCellEditor defaultValueEditor;

	private TextCellEditor columnSizeEditor;

	private TextCellEditor statusEditor;

	public ImportStructureFromDBWizardPage(ImportStructureFromDBWizard wizard)
	{
		super("Main Page");
		setTitle("Edit Database Record Structure");

		this.wizard = wizard;

		dataTypeNames = new String[BuiltinModels.ALL_DATA_TYPES.length];
		for (int i = 0; i < BuiltinModels.ALL_DATA_TYPES.length; i++)
		{
			dataTypeNames[i] = BuiltinModels.ALL_DATA_TYPES[i].getName();
		}

		columnTypeList = SQLUtils.getSQLTypeList();
	}

	public void createControl(Composite parent)
	{
		if (this.parent == null)
			this.parent = parent;

		Composite container = new Composite(parent, SWT.NULL);
		setLayout(container, 1, 1, 0, false);
		setControl(container);

		addInputDetails(container);
		addButtons(container);
		addTableFields(container);

		makeActions();
		hookContextMenu();
	}

	private void addInputDetails(Composite container)
	{
		Composite details = new Composite(container, SWT.NULL);
		setLayout(details, 12, 20, 8, false);
		setControl(details);

		setDataSourceText(details);
		setCatalogText(details);
		setSchemaText(details);
		setTableNameText(details);
	}

	private void setDataSourceText(Composite details)
	{
		dataSourceText = createInputField(details, "&Data Source");
		final AtomicBoolean dialogOpen = new AtomicBoolean(false);
		dataSourceText.addMouseListener(new MouseAdapter()
		{
			public void mouseDown(MouseEvent e)
			{
				if (dialogOpen.get())
					return;
				ElementListSelectionDialog dialog = setElementListSelectionDialog(wizard
						.getDataSourceNameList().toArray(), "Choose a data source.",
						"Data Source List");
				openDialog(dialog, dialogOpen);
				if (dialog.getResult() != null
						&& dialog.getResult()[0] != null
						&& dialog.getResult()[0] instanceof String
						&& !dataSourceText.getSelectionText()
								.equals((String) dialog.getResult()[0]))
				{
					resetCatalogTextField();
					resetSchemaTextField();
					resetTableTextField();

					String newDataSourceName = (String) dialog.getResult()[0];
					dataSourceText.setText(newDataSourceName);
					wizard.setCurrentDataSource(newDataSourceName);

					catalogText.setEnabled(true);
				};
			}
		});

		if (wizard.getCurrentDataSource() != null)
			dataSourceText.setText(wizard.getCurrentDataSource());
	}

	private void setCatalogText(Composite details)
	{
		catalogText = createInputField(details, "&Catalog Name");
		final AtomicBoolean dialogOpen = new AtomicBoolean(false); // Using AtomicBoolean as a
																	// mutable boolean
		catalogText.addMouseListener(new MouseAdapter()
		{
			public void mouseDown(MouseEvent e)
			{
				try
				{
					if (dialogOpen.get())
						return;
					ElementListSelectionDialog dialog = setElementListSelectionDialog(wizard
							.getCatalogNameList().toArray(), "Choose a catalog.", "Catalog List");
					openDialog(dialog, dialogOpen);

					if (dialog.getResult() != null && dialog.getResult()[0] != null
							&& dialog.getResult()[0] instanceof String)
					{
						String newSelectedCatalog = (String) dialog.getResult()[0];

						if (!catalogText.getSelectionText().equals(newSelectedCatalog))
						{
							resetSchemaTextField();
							resetTableTextField();

							importFromDBButton.setEnabled(false);

							catalogText.setText(newSelectedCatalog);
							wizard.setCurrentCatalog(newSelectedCatalog);
						}
					}
				}
				catch (Exception e1)
				{
					String message = "Faild to get catalog list from data source: '"
							+ wizard.getCurrentDataSource() + "'.\n";
					openErrorDialog(e1, message);
					e1.printStackTrace();
				}
			}
		});
		if (wizard.getCurrentCatalog() != null)
			catalogText.setText(wizard.getCurrentCatalog());
	}

	private void setSchemaText(Composite details)
	{
		schemaText = createInputField(details, "&Schema Name");
		final AtomicBoolean dialogOpen = new AtomicBoolean(false);

		schemaText.addMouseListener(new MouseAdapter()
		{
			public void mouseDown(MouseEvent e)
			{
				try
				{
					if (dialogOpen.get())
						return;
					ElementListSelectionDialog dialog = setElementListSelectionDialog(wizard
							.getSchemaNameList().toArray(), "Choose a schema.", "Schema List");
					openDialog(dialog, dialogOpen);

					if (dialog.getResult() != null && dialog.getResult()[0] != null
							&& dialog.getResult()[0] instanceof String)
					{
						String oldSelectedScema = schemaText.getSelectionText();
						String newSchemaName = (String) dialog.getResult()[0];

						if (!oldSelectedScema.equals(newSchemaName))
						{
							resetTableTextField();

							importFromDBButton.setEnabled(false);

							schemaText.setText(newSchemaName);
							wizard.setCurrentSchema(newSchemaName);
						}
					};
				}
				catch (Exception e1)
				{
					String message = "Faild to get schema list from data source: '"
							+ wizard.getCurrentDataSource() + "' where catalog name is: '"
							+ wizard.getCurrentCatalog() + "'.\n";
					openErrorDialog(e1, message);
					e1.printStackTrace();
				}
			}
		});

		if (wizard.getCurrentSchema() != null)
			schemaText.setText(wizard.getCurrentSchema());
	}

	private void openErrorDialog(Exception e, String message)
	{
		Status status = new Status(IStatus.ERROR, TersusWorkbench.getPluginId(), 1, e.getMessage(),
				e);
		ErrorDialog.openError(wizard.getShell(), "Error", message + e.getStackTrace(), status);
	}

	private void setTableNameText(Composite details)
	{
		tableText = createInputField(details, "&Table Name");
		final AtomicBoolean dialogOpen = new AtomicBoolean(false);

		tableText.addMouseListener(new MouseAdapter()
		{
			public void mouseDown(MouseEvent e)
			{
				try
				{
					if (dialogOpen.get())
						return;
					ElementListSelectionDialog dialog = setElementListSelectionDialog(wizard
							.getTableNameList().toArray(), "Choose a table/view.",
							"Table/View List");
					openDialog(dialog, dialogOpen);

					if (dialog.getResult() != null && dialog.getResult()[0] != null
							&& dialog.getResult()[0] instanceof String)
					{
						String newTableName = (String) dialog.getResult()[0];
						if (!tableText.getText().equals(newTableName))
						{
							tableText.setText(newTableName);
							wizard.setCurrentTable(newTableName);
							wizard.setColumns(null);
						}

						openImportFieldsDailog();
						importFromDBButton.setEnabled(true);
					}
				}
				catch (Exception e1)
				{
					String message = "Faild to get catalog list from data source: '"
							+ wizard.getCurrentDataSource() + "' where ctalog name is: '"
							+ wizard.getCurrentCatalog() + "' and schema name is: '"
							+ wizard.getCurrentSchema() + "'.\n";
					openErrorDialog(e1, message);
					e1.printStackTrace();
				}
			}
		});
		if (wizard.getCurrentTableName() != null)
			tableText.setText(wizard.getCurrentTableName());
	}

	private void resetCatalogTextField()
	{
		catalogText.setText("");
		wizard.setCatalogNameList(null);
		wizard.setCurrentCatalog(null);
	}

	private void resetSchemaTextField()
	{
		schemaText.setText("");
		wizard.setSchemaNameList(null);
		wizard.setCurrentSchema(null);
	}

	private void resetTableTextField()
	{
		tableText.setText("");
		wizard.setTableNameList(null);
		wizard.setCurrentTable(null);
	}

	private ElementListSelectionDialog setElementListSelectionDialog(Object[] elements,
			String message, String title)
	{
		ElementListSelectionDialog dialog = new ElementListSelectionDialog(wizard.getShell(),
				new ColumnLabelProvider());

		dialog.setElements(elements);
		dialog.setMessage(message);
		dialog.setTitle(title);

		return dialog;
	}

	private Text createInputField(Composite details, String text)
	{
		Composite container = new Composite(details, SWT.LEFT_TO_RIGHT);
		setLayout(container, 2, 1, 15, true);

		Label label = new Label(container, SWT.NULL);
		label.setText(text + ": ");
		GridData gd = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_CENTER);
		gd.horizontalSpan = 1;
		label.setLayoutData(gd);

		Text textInputField = new Text(container, SWT.BORDER | SWT.SINGLE);
		textInputField.setLayoutData(new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_CENTER));

		textInputField.setBounds(0, 0, 30, 20);
		textInputField.setData(LABEL_KEY, label);
		textInputField.setEditable(false);

		return textInputField;
	}

	private void addButtons(Composite container)
	{
		Composite buttonBar = new Composite(container, SWT.NULL);
		setLayout(buttonBar, 1, 1, 9, false);

		GridData gd = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_CENTER);
		gd.horizontalSpan = 1;
		buttonBar.setLayoutData(gd);

		setControl(buttonBar);
		addImportFromDBButton(buttonBar);
	}

	private void addImportFromDBButton(Composite container)
	{
		importFromDBButton = new Button(container, SWT.PUSH);
		importFromDBButton.setText("&Import Columns");

		importFromDBButton.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				openImportFieldsDailog();
			}
		});

		if (wizard.getCurrentDataSource() != null && wizard.getCurrentCatalog() != null
				&& wizard.getCurrentSchema() != null && wizard.getCurrentTableName() != null)
			importFromDBButton.setEnabled(true);
		else
			importFromDBButton.setEnabled(false);
	}

	private void makeActions()
	{
		removeFieldAction = new Action()
		{
			public void run()
			{
				TersusTableFieldDescriptor selectedField = getSelectedDescriptor();
				wizard.getDataRecordFields().remove(selectedField);
			}
		};

		removeFieldAction.setText("Remove Field");
		removeFieldAction.setToolTipText("Remove field from Database Record");
	}

	protected void hookContextMenu()
	{
		MenuManager menuMgr = new MenuManager("Available Actions");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener()
		{
			public void menuAboutToShow(IMenuManager manager)
			{
				manager.add(removeFieldAction);
			}
		});
		Menu menu = menuMgr.createContextMenu(getControl());
		fieldViewer.getControl().setMenu(menu);
	}

	private TersusTableFieldDescriptor getSelectedDescriptor()
	{
		TersusTableFieldDescriptor selectedField = (TersusTableFieldDescriptor) SelectionHelper
				.getSelectedItem(fieldViewer.getSelection(), TersusTableFieldDescriptor.class);
		return selectedField;
	}

	private void openImportFieldsDailog()
	{
		setWizardColumns();

		ArrayList<String> columnNames = new ArrayList<String>();

		for (TersusTableFieldDescriptor descriptor : getUnselectedColumnsDescriptors())
		{
			columnNames.add(descriptor.getColumnName());
		}

		SelectFieldsDialog dialog = new SelectFieldsDialog(wizard.getShell(),
				getUnselectedColumnsDescriptors(), new ColumnContentProvider(columnNames),
				new SelectFieldsLabelProvider(getUnselectedColumnsDescriptors()),
				"Choose one or more columns.");

		if (wizard.isAddPrimaryKeyFields())
		{
			ArrayList<String> selectedElements = new ArrayList<String>();
			for (TersusTableFieldDescriptor descriptor : getUnselectedColumnsDescriptors())
			{
				if (descriptor.getPrimaryKey())
					selectedElements.add(descriptor.getColumnName());
			}

			dialog.setInitialElementSelections(selectedElements);
		}

		dialog.open();

		if (dialog.getResult() != null && dialog.getResult().length != 0)
		{
			ArrayList<String> selectedColumnNames = new ArrayList<String>();

			for (Object obj : dialog.getResult())
			{
				if (obj instanceof String)
					selectedColumnNames.add((String) obj);
			}

			wizard.addNewFields(selectedColumnNames);
		}
	}

	private void setWizardColumns()
	{
		if (wizard.getCurrentTableName() != null)
			wizard.setColumns();
	}

	private ArrayList<TersusTableFieldDescriptor> getUnselectedColumnsDescriptors()
	{
		setWizardColumns();
		List<TersusTableFieldDescriptor> existingRecordsFields = wizard.getDataRecordFields()
				.getContent();

		ArrayList<TersusTableFieldDescriptor> unselectedColumns = new ArrayList<TersusTableFieldDescriptor>();

		for (TersusTableFieldDescriptor descriptor : wizard.getDataBaseColumns())
		{
			if (!isContains(existingRecordsFields, descriptor))
				unselectedColumns.add(descriptor);
		}

		return unselectedColumns;
	}

	private boolean isContains(List<TersusTableFieldDescriptor> descriptorList,
			TersusTableFieldDescriptor descriptorToCheck)
	{
		for (TersusTableFieldDescriptor descriptor : descriptorList)
		{
			if (descriptor.getColumnName().equals(descriptorToCheck.getColumnName()))
				return true;
		}

		return false;
	}

	private void addTableFields(Composite container)
	{
		configureFields(container);
		fieldViewer.setContentProvider(new ListContentProvider<TableFieldDescriptor>(fieldViewer));
		fieldViewer.setInput(wizard.getDataRecordFields());
	}

	private void configureFields(final Composite container)
	{
		Composite tableContainer = new Composite(container, SWT.NULL);

		GridData gridData = new GridData(GridData.FILL_BOTH);
		gridData.widthHint = 980;
		gridData.heightHint = 400;
		gridData.verticalSpan = 4;
		tableContainer.setLayoutData(gridData);

		fieldViewer = new TableViewer(new Table(tableContainer, SWT.BORDER | SWT.FULL_SELECTION
				| SWT.V_SCROLL | SWT.H_SCROLL));
		fieldViewer.getTable().setSize(980, 400);

		TableViewerColumn elementNameCol = new TableViewerColumn(fieldViewer, SWT.HORIZONTAL);
		elementNameCol.getColumn().setWidth(100);
		elementNameCol.getColumn().setText("Element Name");
		elementNameCol.setLabelProvider(new CellLabelProvider()
		{
			public void update(ViewerCell cell)
			{
				cell.setText(((TersusTableFieldDescriptor) cell.getElement()).getElementName());
				cell.setImage(null);
			}
		});

		elementNameEditor = new TextCellEditor(fieldViewer.getTable());
		elementNameCol.setEditingSupport(new TableEditingSupoort(elementNameEditor)
		{
			@Override
			protected void setValue(Object element, Object value)
			{
				TersusTableFieldDescriptor descriptor = (TersusTableFieldDescriptor) element;
				descriptor.setElementName((String) value);
				validate();
			}

			@Override
			protected Object getValue(TersusTableFieldDescriptor descriptor)
			{
				return descriptor.getElementName();
			}

			@Override
			protected boolean canEdit(Object element)
			{
				return wizard.canEditTable();
			}
		});

		TableViewerColumn columnNameCol = new TableViewerColumn(fieldViewer, SWT.NONE);
		columnNameCol.getColumn().setWidth(100);
		columnNameCol.getColumn().setText("Column Name");
		columnNameCol.setLabelProvider(new CellLabelProvider()
		{
			public void update(ViewerCell cell)
			{
				TersusTableFieldDescriptor descriptor = (TersusTableFieldDescriptor) cell
						.getElement();
				String columnName = descriptor.getColumnName();

				if (columnName == null)
					columnName = Misc.sqlize(descriptor.getElementName());

				cell.setText(columnName);
				cell.setImage(null);
			}
		});

		columnNameEditor = new TextCellEditor(fieldViewer.getTable());
		columnNameCol.setEditingSupport(new TableEditingSupoort(columnNameEditor)
		{
			@Override
			protected void setValue(Object element, Object value)
			{
				TersusTableFieldDescriptor descriptor = (TersusTableFieldDescriptor) element;
				descriptor.setColumnName((String) value);
				descriptor = wizard.setStatusField(descriptor);
				validate();
			}

			@Override
			protected Object getValue(TersusTableFieldDescriptor descriptor)
			{
				return descriptor.getColumnName();
			}

			@Override
			protected boolean canEdit(Object element)
			{
				return false;
			}

			@Override
			protected CellEditor getCellEditor(Object element)
			{
				return null;
			}
		});

		final TableViewerColumn columnNameColFinal = columnNameCol;
		fieldViewer.getTable().addMouseListener(new MouseAdapter()
		{
			public void mouseDown(MouseEvent e)
			{
				setWizardColumns();
				ViewerCell cell = columnNameColFinal.getViewer().getCell(new Point(e.x, e.y));
				if (cell != null && cell.getElement() instanceof TersusTableFieldDescriptor
						&& cell.getColumnIndex() == 1)
				{
					TersusTableFieldDescriptor cellDescriptor = (TersusTableFieldDescriptor) cell
							.getElement();
					SelectColumnNameDialog dialog = new SelectColumnNameDialog(wizard.getShell(),
							getUnselectedColumnsDescriptors(), cellDescriptor.getElementName(),
							cellDescriptor.getColumnName());
					dialog.setTitle("Select Column Name");
					dialog.setInitialPattern("?");
					dialog.open();

					if (dialog.getReturnCode() == Window.OK)
					{
						Object[] objects = dialog.getResult();
						TersusTableFieldDescriptor selectedDescriptor = (TersusTableFieldDescriptor) objects[0];
						setValue(cellDescriptor, selectedDescriptor.getColumnName());
					}
				}
			}

			private void setValue(Object element, Object value)
			{
				TersusTableFieldDescriptor descriptor = (TersusTableFieldDescriptor) element;
				descriptor.setColumnName((String) value);
				descriptor = wizard.setStatusField(descriptor);
				validate();
			}
		});

		TableViewerColumn columnDescriptionCol = new TableViewerColumn(fieldViewer, SWT.NONE);
		columnDescriptionCol.getColumn().setWidth(230);
		columnDescriptionCol.getColumn().setText("Column Description");
		columnDescriptionCol.setLabelProvider(new CellLabelProvider()
		{
			public void update(ViewerCell cell)
			{
				cell.setText(((TersusTableFieldDescriptor) cell.getElement())
						.getColumnDescription());
				cell.setImage(null);
			}
		});

		columnDescriptionEditor = new TextCellEditor(fieldViewer.getTable());
		columnDescriptionCol.setEditingSupport(new TableEditingSupoort(columnDescriptionEditor)
		{
			@Override
			protected void setValue(Object element, Object value)
			{
				TersusTableFieldDescriptor descriptor = (TersusTableFieldDescriptor) element;
				descriptor.setColumnDescription((String) value);
				validate();
			}

			@Override
			protected Object getValue(TersusTableFieldDescriptor descriptor)
			{
				return descriptor.getColumnDescription();
			}

			@Override
			protected boolean canEdit(Object element)
			{
				return false;
			}
		});

		TableViewerColumn dataTypeCol = new TableViewerColumn(fieldViewer, SWT.HORIZONTAL);
		dataTypeCol.getColumn().setWidth(70);
		dataTypeCol.getColumn().setText("Type");
		dataTypeCol.setLabelProvider(new CellLabelProvider()
		{
			public void update(ViewerCell cell)
			{
				String s = ((TersusTableFieldDescriptor) cell.getElement()).getDataType().getName();
				cell.setText(s);
				cell.setImage(null);
			}
		});

		dataTypeEditor = new ComboBoxCellEditor(getTable(), dataTypeNames);
		dataTypeCol.setEditingSupport(new TableEditingSupoort(dataTypeEditor)
		{

			@Override
			protected Object getValue(TersusTableFieldDescriptor descriptor)
			{
				for (int i = 0; i < BuiltinModels.ALL_DATA_TYPES.length; i++)
				{
					if (BuiltinModels.ALL_DATA_TYPES[i].equals(descriptor.getDataType()))
						return Integer.valueOf(i);
				}
				return 0;
			}

			@Override
			protected void setValue(Object element, Object value)
			{
				TersusTableFieldDescriptor descriptor = (TersusTableFieldDescriptor) element;
				int index = ((Integer) value).intValue();
				if (index >= 0 && index < BuiltinModels.ALL_DATA_TYPES.length)
					descriptor.setDataType(BuiltinModels.ALL_DATA_TYPES[index]);

				validate();
			}

			@Override
			protected boolean canEdit(Object element)
			{
				if (wizard.canEditTable())
					return true;
				else
					return false;
			}
		});

		TableViewerColumn columnTypeCol = new TableViewerColumn(fieldViewer, SWT.NONE);
		columnTypeCol.getColumn().setWidth(90);
		columnTypeCol.getColumn().setText("Column Type");
		columnTypeCol.setLabelProvider(new CellLabelProvider()
		{
			public void update(ViewerCell cell)
			{
				TersusTableFieldDescriptor descriptor = (TersusTableFieldDescriptor) cell
						.getElement();
				cell.setText(descriptor.getColumnType());
				cell.setImage(null);
			}
		});

		columnTypeEditor = new ComboBoxCellEditor(fieldViewer.getTable(), columnTypeList);
		columnTypeCol.setEditingSupport(new TableEditingSupoort(columnTypeEditor)
		{
			@Override
			protected void setValue(Object element, Object value)
			{
				TersusTableFieldDescriptor descriptor = (TersusTableFieldDescriptor) element;
				int index = ((Integer) value).intValue();
				if (index >= 0 && index < columnTypeList.length)
					descriptor.setColumnType(columnTypeList[index]);

				wizard.setStatusField(descriptor);
				validate();
			}

			@Override
			protected Object getValue(TersusTableFieldDescriptor descriptor)
			{
				for (int i = 0; i < columnTypeList.length; i++)
				{
					if (columnTypeList[i].equals(descriptor.getColumnType()))
						return Integer.valueOf(i);
				}
				return 0;
			}

			@Override
			protected boolean canEdit(Object element)
			{
				if (wizard.canEditTable())
					return true;
				else
					return false;
			}
		});

		TableViewerColumn primaryKeyCol = new TableViewerColumn(fieldViewer, SWT.NONE);
		primaryKeyCol.getColumn().setWidth(80);
		primaryKeyCol.getColumn().setText("Primary Key");
		primaryKeyCol.setLabelProvider(new CellLabelProvider()
		{
			public void update(ViewerCell cell)
			{
				Boolean primaryKey = ((TersusTableFieldDescriptor) cell.getElement())
						.getPrimaryKey();

				if (primaryKey == null)
					primaryKey = false;

				cell.setText(primaryKey.toString());
				cell.setImage(null);
			}
		});

		primaryKeyEditor = new ComboBoxCellEditor(getTable(), BOOLEAN_VALUES);
		primaryKeyCol.setEditingSupport(new TableEditingSupoort(primaryKeyEditor)
		{

			@Override
			protected Object getValue(TersusTableFieldDescriptor descriptor)
			{
				for (int i = 0; i < BOOLEAN_VALUES.length; i++)
				{
					if (BOOLEAN_VALUES[i].equals(descriptor.getPrimaryKey()))
						return Integer.valueOf(i);
				}
				return 0;
			}

			@Override
			protected void setValue(Object element, Object value)
			{
				TersusTableFieldDescriptor descriptor = (TersusTableFieldDescriptor) element;
				int index = ((Integer) value).intValue();
				if (index >= 0 && index < BOOLEAN_VALUES.length)
					descriptor.setPrimaryKey(new Boolean(BOOLEAN_VALUES[index]));
				wizard.setStatusField(descriptor);
				validate();
			}

			@Override
			protected boolean canEdit(Object element)
			{
				return wizard.canEditTable();
			}
		});

		TableViewerColumn nullableCol = new TableViewerColumn(fieldViewer, SWT.NONE);
		nullableCol.getColumn().setWidth(60);
		nullableCol.getColumn().setText("Nullable");
		nullableCol.setLabelProvider(new CellLabelProvider()
		{
			public void update(ViewerCell cell)
			{
				Boolean nullable = ((TersusTableFieldDescriptor) cell.getElement()).getNullable();

				if (nullable == null)
					nullable = false;

				cell.setText(nullable.toString());
				cell.setImage(null);
			}
		});

		nullableEditor = new ComboBoxCellEditor(getTable(), BOOLEAN_VALUES);
		nullableCol.setEditingSupport(new TableEditingSupoort(nullableEditor)
		{

			@Override
			protected Object getValue(TersusTableFieldDescriptor descriptor)
			{
				for (int i = 0; i < BOOLEAN_VALUES.length; i++)
				{
					if (BOOLEAN_VALUES[i].equals(descriptor.getNullable()))
						return Integer.valueOf(i);
				}
				return 0;
			}

			@Override
			protected void setValue(Object element, Object value)
			{
				TersusTableFieldDescriptor descriptor = (TersusTableFieldDescriptor) element;
				int index = ((Integer) value).intValue();

				if (index >= 0 && index < BOOLEAN_VALUES.length)
					descriptor.setNullable(new Boolean(BOOLEAN_VALUES[index]));

				wizard.setStatusField(descriptor);
				validate();
			}

			@Override
			protected boolean canEdit(Object element)
			{
				return wizard.canEditTable();
			}
		});

		TableViewerColumn defaultValueCol = new TableViewerColumn(fieldViewer, SWT.NONE);
		defaultValueCol.getColumn().setWidth(100);
		defaultValueCol.getColumn().setText("Default Value");
		defaultValueCol.setLabelProvider(new CellLabelProvider()
		{
			public void update(ViewerCell cell)
			{
				cell.setText(((TersusTableFieldDescriptor) cell.getElement()).getDefaultValue());
				cell.setImage(null);
			}
		});

		defaultValueEditor = new TextCellEditor(fieldViewer.getTable());
		defaultValueCol.setEditingSupport(new TableEditingSupoort(defaultValueEditor)
		{
			@Override
			protected void setValue(Object element, Object value)
			{
				TersusTableFieldDescriptor descriptor = (TersusTableFieldDescriptor) element;
				descriptor.setDefaultValue((String) value);
				validate();
			}

			@Override
			protected Object getValue(TersusTableFieldDescriptor descriptor)
			{
				return descriptor.getDefaultValue();
			}

			@Override
			protected boolean canEdit(Object element)
			{
				return wizard.canEditTable();
			}
		});

		TableViewerColumn columnSizeCol = new TableViewerColumn(fieldViewer, SWT.NONE);
		columnSizeCol.getColumn().setWidth(90);
		columnSizeCol.getColumn().setText("Column Size");
		columnSizeCol.setLabelProvider(new CellLabelProvider()
		{
			public void update(ViewerCell cell)
			{
				TersusTableFieldDescriptor descriptor = (TersusTableFieldDescriptor) cell
						.getElement();
				String columnSize = descriptor.getColumnSize();

				if (columnSize == null)
					cell.setText(null);
				else
					cell.setText(columnSize);

				cell.setImage(null);
			}
		});

		columnSizeEditor = new TextCellEditor(fieldViewer.getTable());
		columnSizeCol.setEditingSupport(new TableEditingSupoort(columnSizeEditor)
		{
			@Override
			protected void setValue(Object element, Object value)
			{
				TersusTableFieldDescriptor descriptor = (TersusTableFieldDescriptor) element;
				descriptor.setColumnSize((String) value);
				wizard.setStatusField(descriptor);
				validate();
			}

			@Override
			protected Object getValue(TersusTableFieldDescriptor descriptor)
			{
				return descriptor.getColumnSize();
			}

			@Override
			protected boolean canEdit(Object element)
			{
				return wizard.canEditTable();
			}
		});

		TableViewerColumn statusCol = new TableViewerColumn(fieldViewer, SWT.NONE);
		statusCol.getColumn().setWidth(50);
		statusCol.getColumn().setText("Status");
		statusCol.setLabelProvider(new CellLabelProvider()
		{
			public void update(ViewerCell cell)
			{
				TersusTableFieldDescriptor descriptor = (TersusTableFieldDescriptor) cell
						.getElement();
				String status = descriptor.getStatus();

				cell.setText(status);

				if ("X".equals(status))
					cell.setForeground(new Color(container.getDisplay(), 255, 0, 0));
				if ("V".equals(status))
					cell.setForeground(new Color(container.getDisplay(), 0, 255, 0));
				if ("?".equals(status))
					cell.setForeground(new Color(container.getDisplay(), 0, 0, 255));
			}
		});

		statusEditor = new TextCellEditor(getTable());
		statusCol.setEditingSupport(new TableEditingSupoort(statusEditor)
		{

			@Override
			protected Object getValue(TersusTableFieldDescriptor descriptor)
			{
				return descriptor.getStatus();
			}

			@Override
			protected void setValue(Object element, Object value)
			{
				TersusTableFieldDescriptor descriptor = (TersusTableFieldDescriptor) element;
				descriptor.setStatus((String) value);
				validate();
			}

			@Override
			protected boolean canEdit(Object element)
			{
				return false;
			}
		});

		TableViewerEditor.create(fieldViewer,
				new ColumnViewerEditorActivationStrategy(fieldViewer),
				ColumnViewerEditor.TABBING_HORIZONTAL
						| ColumnViewerEditor.TABBING_MOVE_TO_ROW_NEIGHBOR
						| ColumnViewerEditor.TABBING_VERTICAL);

		Table table = getTable();

		table.setLinesVisible(true);
		table.setHeaderVisible(true);

		TableLayout layout = new TableLayout();
		table.setLayout(layout);
		ColumnWeightData layoutData = new ColumnWeightData(50);
		layout.addColumnData(layoutData);
	}

	private Table getTable()
	{
		Table table = fieldViewer.getTable();
		return table;
	}

	private void setLayout(Composite container, int columns, int horizontalSpacing,
			int verticalSpacing, boolean makeColumnsEqualWidth)
	{
		GridLayout layout = new GridLayout();
		layout.numColumns = columns;
		layout.horizontalSpacing = horizontalSpacing;
		layout.verticalSpacing = verticalSpacing;
		layout.makeColumnsEqualWidth = makeColumnsEqualWidth;
		container.setLayout(layout);
	}

	private boolean validate()
	{
		wizard.validate();
		setErrorMessage(wizard.getErrorMessage());
		setPageComplete(wizard.isReady());

		return wizard.isReady();
	}

	private void openDialog(ElementListSelectionDialog dialog, final AtomicBoolean guard)
	{
		guard.set(true);
		try
		{
			dialog.open();
		}
		finally
		{
			guard.set(false);
		}
	}

	public abstract class TableEditingSupoort extends EditingSupport
	{
		private CellEditor editor;

		public TableEditingSupoort(CellEditor editor)
		{
			super(fieldViewer);
			this.editor = editor;
		}

		abstract protected boolean canEdit(Object element);

		protected CellEditor getCellEditor(Object element)
		{
			return editor;
		}

		protected Object getValue(Object element)
		{
			return getValue((TersusTableFieldDescriptor) element);
		}

		protected abstract Object getValue(TersusTableFieldDescriptor descriptor);

		protected abstract void setValue(Object element, Object value);
	}
}

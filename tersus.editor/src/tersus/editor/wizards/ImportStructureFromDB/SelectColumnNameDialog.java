/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.wizards.ImportStructureFromDB;

import java.util.ArrayList;
import java.util.Comparator;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.internal.WorkbenchPlugin;

import tersus.editor.EditorPlugin;
import tersus.model.TersusTableFieldDescriptor;

/**
 * @author Liat Shiff
 */
public class SelectColumnNameDialog extends FilteredItemsSelectionDialog
{
	private static final String SETTINGS = SelectColumnNameDialog.class.getCanonicalName();

	private ArrayList<TersusTableFieldDescriptor> columns;

	public SelectColumnNameDialog(Shell shell, ArrayList<TersusTableFieldDescriptor> columns,
			String elementName, String currentlyColumnName)
	{
		super(shell, false);

		this.columns = columns;

		setListLabelProvider(new ColumnNameLableProvider());
		setDetailsLabelProvider(new ColumnDetailsLabelProvider());
		setMessage("Select column name for element '" + elementName + ". (Currently '"
				+ currentlyColumnName + "')");
	}

	protected Control createExtendedContentArea(Composite parent)
	{
		return null;
	}

	protected ItemsFilter createFilter()
	{
		return new ItemsFilter()
		{
			public boolean isConsistentItem(Object item)
			{
				return true;
			}

			public boolean matchItem(Object item)
			{
				if (item instanceof TersusTableFieldDescriptor)
					return matches(((TersusTableFieldDescriptor) item).getColumnName());

				return false;
			}
		};
	}

	protected final IStatus run(IProgressMonitor parent)
	{

		return null;
	}

	protected void fillContentProvider(AbstractContentProvider contentProvider,
			ItemsFilter itemsFilter, IProgressMonitor progressMonitor) throws CoreException
	{
		progressMonitor.beginTask("Looking for column name...", columns.size());

		for (TersusTableFieldDescriptor descriptor : columns)
		{
			contentProvider.add(descriptor, itemsFilter);
			progressMonitor.worked(1);
		}

		progressMonitor.done();
	}

	protected IDialogSettings getDialogSettings()
	{
		IDialogSettings settings = EditorPlugin.getDefault().getDialogSettings().getSection(
				SETTINGS);
		if (settings == null)
			settings = EditorPlugin.getDefault().getDialogSettings().addNewSection(SETTINGS);

		return settings;
	}

	public String getElementName(Object item)
	{
		System.out.println();
		return null;
	}

	protected Comparator getItemsComparator()
	{
		return new Comparator()
		{
			public int compare(Object o1, Object o2)
			{
				if (o1 instanceof TersusTableFieldDescriptor
						&& o2 instanceof TersusTableFieldDescriptor)
				{
					TersusTableFieldDescriptor o1Descriptor = (TersusTableFieldDescriptor) o1;
					TersusTableFieldDescriptor o2Descriptor = (TersusTableFieldDescriptor) o2;

					if (o1Descriptor.getColumnName().equals(o2Descriptor.getColumnName()))
						return 1;
				}

				return 0;
			}
		};
	}

	protected IStatus validateItem(Object item)
	{
		return new Status(IStatus.OK, WorkbenchPlugin.PI_WORKBENCH, 0, "", null);
	}

	private class ColumnNameLableProvider extends LabelProvider
	{
		public String getText(Object element)
		{
			if (element instanceof TersusTableFieldDescriptor)
			{
				TersusTableFieldDescriptor descriptor = (TersusTableFieldDescriptor) element;
				return descriptor.getColumnName() + " - " + descriptor.getColumnDescription();
			}

			// Not suppose to get here
			return super.getText(element);
		}
	}

	private class ColumnDetailsLabelProvider extends LabelProvider
	{
		public String getText(Object element)
		{
			if (element instanceof TersusTableFieldDescriptor)
				return ((TersusTableFieldDescriptor) element).getColumnDescription();

			// Not suppose to get here
			return super.getText(element);
		}
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2023 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.wizards;

import java.io.File;

import org.eclipse.core.internal.resources.Workspace;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;

import tersus.ProjectStructure;
import tersus.editor.ConcreteModelIdentifier;
import tersus.editor.EditorMessages;
import tersus.editor.EditorPlugin;
import tersus.editor.RepositoryManager;
import tersus.editor.TersusEditor;
import tersus.editor.projectimport.ImportUtils;
import tersus.model.ModelId;
import tersus.model.commands.CommandFailedException;
import tersus.model.commands.MultiStepCommand;
import tersus.util.Misc;
import tersus.workbench.Configuration;
import tersus.workbench.TersusWorkbench;

/**
 * @author Youval Bronicki
 */
public class NewProjectWizard extends Wizard implements INewWizard
{

	private static final String ADD_SAMPLE_VIEW_KEY = NewProjectWizard.class.getName()
			+ ":addSampleView";
	public static final String ID = "tersus.editor.wizards.NewProject";

	String name;

	String errorMessage;

	boolean addSampleView = true;

	File projectTemplate;

//	String theme;

	public NewProjectWizard()
	{
		super();
		setWindowTitle("New Tersus Project");
	}
	
	public boolean performFinish()
	{
		IDialogSettings settings = EditorPlugin.getDefault().getDialogSettings();
		settings.put(ADD_SAMPLE_VIEW_KEY, addSampleView);
		IWorkspace workspace = TersusWorkbench.getWorkspace();
		IProject project = workspace.getRoot().getProject(name);
		try
		{
			ImportUtils.importAsProject(name, projectTemplate);
			RepositoryManager manager = RepositoryManager.getRepositoryManager(project);
			IFile f = project.getFile(ProjectStructure.CONFIGURATION_XML);
			String rootSystemName = name + "/" + name;
			ModelId rootId = null;
			Configuration c = null;
			if (f.exists())
			{
				c = Configuration.loadFromFile(f.getLocation().toFile());
				rootId = c.getRootSystemId();
				// f.delete(true, null);
			}
			else
			{
				c = new Configuration();
				rootId = new ModelId(rootSystemName);
			}
//			c.addDefaults(name);
			
			final ModelId templateId = rootId;
			final ModelId newModelId = new ModelId(rootSystemName);
//			if (c != null)
//			{
				c.setName(name);
				c.setRootSystem(rootSystemName);
//			}

			MultiStepCommand command = new MultiStepCommand(manager.getRepository())
			{
				protected void run()
				{
					changeModelId(repository.getModel(templateId,true),name);
					renamePackage(templateId.getPackageId(), newModelId.getPackageId().getName());
				}
			};

			try
			{
				command.execute();
			}
			catch (CommandFailedException e)
			{
				StringBuffer message = new StringBuffer();
				message.append(e.getMessage());

				MessageDialog.openInformation(TersusWorkbench.getActiveWorkbenchShell(),
						"Read Only Package/File", message.toString());
			}
			c.save(f);
			manager.save();
			IWorkbenchPage page = TersusWorkbench.getActiveWorkbenchWindow().getActivePage();
			try
			{
				page.openEditor(new ConcreteModelIdentifier(project, newModelId), TersusEditor.ID);
				return true;
			}
			catch (PartInitException e)
			{
				TersusWorkbench.log(e);
				return false;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			TersusWorkbench.log(new Status(Status.ERROR, EditorPlugin.getPluginId(), 1,
					"Failed to create project " + name, e));
			if (project.exists())
			{
				try
				{
					project.delete(true, true, null);
				}
				catch (CoreException e1)
				{
					// Secondary Exception ignored
				}
			}
			return false;
		}
	
	}

	public void init(IWorkbench workbench, IStructuredSelection selection)
	{
	}

	public void setObjectName(String name)
	{
		this.name = name;
	}

	public void validate()
	{
		errorMessage = null;
		if (name == null)
			return;
		if (name.length() == 0)
			errorMessage = "A project name must be specified.";
		else if (!Misc.isValidFileName(name))
			errorMessage = "A project name must not contain any of the following charactes: "
					+ Misc.INVALID_FILENAME_CHARACTERS;
		else
		{
			IProject[] projects = TersusWorkbench.getWorkspace().getRoot().getProjects();
			for (int i = 0; i < projects.length; i++)
			{
				IProject project = projects[i];
				if (name.compareToIgnoreCase(project.getName()) == 0)
				{
					errorMessage = EditorMessages.format(EditorMessages.Error_Project_Exists_Key,
							project.getName());
					break;
				}
			}

			File p = ((Workspace) TersusWorkbench.getWorkspace()).getRoot().getLocation().toFile();
			if (errorMessage == null && p.isDirectory())
			{
				for (File file : p.listFiles())
				{
					if (name.compareToIgnoreCase(file.getName()) == 0 && file.isDirectory())
					{
						String filePath = p.getPath() + "/"+file.getName();
						errorMessage = EditorMessages.format(
								EditorMessages.Error_Project_Exists_In_File_System, filePath);
						break;
					}
				}
			}
		}
	}

	public void setProjectTemplate(File file)
	{
		this.projectTemplate = file;
	}
/*
	public void setTheme(String theme)
	{
		this.theme = theme;
	}*/

	public String getErrorMessage()
	{
		return errorMessage;
	}

	public boolean isReady()
	{
		return errorMessage == null && name != null;
	}

	public void addPages()
	{
		addPage(new NewProjectWizardPage());
	}
}
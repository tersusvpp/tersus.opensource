/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.wizards;

import org.eclipse.gef.commands.Command;

import tersus.editor.EditorMessages;
import tersus.model.PackageId;
import tersus.model.commands.NewPackageCommand;
import tersus.util.Misc;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 *  
 */
public class NewPackageWizard extends NewObjectWizard
{
    public static final String ID = "tersus.editor.wizards.NewPackage";
    
    public static final String WIZARD_NAME="NewPackageWizard";

    public void validate()
    {
        errorMessage = null;
        String packageName = getObjectName();
        if (packageName == null)
            return;
        if (packageName.length() == 0)
            errorMessage = "A package name must be specified.";
        else if (!Misc.isValidFileName(packageName))
            errorMessage = "A package name must not contain any of the following charactes: "
                    + Misc.INVALID_FILENAME_CHARACTERS;
        else if (getProject() == null)
            errorMessage = MISSING_LOCATION_ERROR_MESSAGE;
        else 
        {
            WorkspaceRepository repository = getRepository();
            PackageId existingPakcageId = repository.getSubPackageIdCaseInsensitive(getParentPackageId(), packageName);
            if (existingPakcageId != null)
                errorMessage = EditorMessages.format(EditorMessages.Error_Package_Exists_Key, existingPakcageId);
        }
    }
    
    protected Command getCommand()
    {
        Command command = new NewPackageCommand(getRepository(), new PackageId(getParentPackageId(), getObjectName()));
        return command;
    }

    protected String getWizardName()
    {
        return WIZARD_NAME;
    }
    protected String getObjectNameLabel()
    {
        return "Package na&me:";
    }
}
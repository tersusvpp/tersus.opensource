/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.wizards;

import org.eclipse.gef.commands.Command;

import tersus.editor.EditorMessages;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Path;
import tersus.model.commands.CopyModelCommand;
import tersus.model.commands.CreateModelFromPrototypeCommand;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 *
 */
public abstract class NewModelWizard extends NewObjectWizard
{

    public void validate()
    {
        errorMessage = null;
        String modelName = getObjectName();
        if (getTemplateModel() == null)
        {
            errorMessage = "The template '"+getTemplateModelId()+"' is missing.";
        }
        else if (modelName == null)
            return;
        else if(modelName.length() == 0)
            errorMessage = "A model name must be specified.";
        else if (modelName.indexOf(Path.SEPARATOR)>0 || modelName.endsWith(" "))
            errorMessage = EditorMessages.Error_Invalid_Model_Name;
        else if (getProject() == null)
            errorMessage = MISSING_LOCATION_ERROR_MESSAGE;
        else 
        {
            WorkspaceRepository repository = getRepository();
            ModelId newModelId= getNewModelId();
            if (repository.getModel(newModelId, true) != null)
                    errorMessage = "There is an existing model called '"
                            + modelName + "' (in package '"+getParentPackageId()+"'";
        }
    }

    protected Command getCommand()
    {
    	Command command = new CreateModelFromPrototypeCommand(getTemplateModel(), getNewModelId());
    	return command;
    }

    protected ModelId getNewModelId()
    {
        return new ModelId(getParentPackageId(), getObjectName());
    }

    /**
     * @return
     */
    protected Model getTemplateModel()
    {
        return getRepository().getModel(getTemplateModelId(), true);
    }
    
    protected abstract ModelId getTemplateModelId();

    protected String getObjectNameLabel()
    {
        return "Model na&me:";
    }
    protected String getWizardName()
    {
        return null;
    }
}

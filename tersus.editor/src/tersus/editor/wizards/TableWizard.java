package tersus.editor.wizards;

import org.eclipse.core.resources.IProject;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;

import tersus.editor.RepositoryManager;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelObject;
import tersus.model.commands.CreateTableCommand;
import tersus.model.commands.CreateTableCommand.TableConfiguration;
import tersus.model.commands.TableFieldDescriptor;
import tersus.util.IObservableList;
import tersus.util.ListListener;
import tersus.util.ObservableList;
import tersus.workbench.WorkspaceRepository;

public class TableWizard extends Wizard
{
	private ModelObject modelObject;

	private ObservableList<TableFieldDescriptor> fields;

	private TableConfiguration tableConfiguration;

	private String tableName;

	private String displayName;

	private ModelElement recordElement;

	private String errorMessage;

	private CreateTableCommand command;

	private ListListener<TableFieldDescriptor> listListener;

	private TableWizardPage page;

	private int ImportFromDBCounter;

	public ObservableList<TableFieldDescriptor> getFields()
	{
		return fields;
	}

	public TableWizard(ModelObject obj)
	{
		setModelObject(obj);
		setWindowTitle("Table settings");
		command = new CreateTableCommand((WorkspaceRepository) getModel().getRepository(), obj);
		fields = new ObservableList<TableFieldDescriptor>(command.getInitialFields());
		tableConfiguration = command.getInitialTableConfiguration();

		String databaseRecordName = command.getDatabaseTableName();
		
		if ("-".equals(databaseRecordName))
			setDisplayName("");
		else if (getDisplayName() == null && obj instanceof ModelElement)
			setDisplayName(((ModelElement) obj).getRole().toString()); // Ofer, 18/12/10: No longer setting the display name to the table name
		
		setTableName(command.getDatabaseTableName());
		setRecordElement(command.getRecordElement());

		listListener = new ListListener<TableFieldDescriptor>()
		{

			@SuppressWarnings("unused")
			public void contentChanged(ObservableList<TableFieldDescriptor> list)
			{
				((WizardDialog) getContainer()).updateSize();
			}

			public void itemChanged(TableFieldDescriptor item, String propertyName,
					Object oldValue, Object newValue)
			{
				// TODO Auto-generated method stub

			}

			public void contentChanged(IObservableList<TableFieldDescriptor> list)
			{
				// TODO Auto-generated method stub

			}

			public void itemAdded(TableFieldDescriptor item)
			{
				// TODO Auto-generated method stub

			}
		};
		fields.addListener(listListener);
		ImportFromDBCounter = 0;
	}

	public void addField(TableFieldDescriptor f)
	{
		fields.add(f);
	}

	public void removeField(TableFieldDescriptor f)
	{
		fields.remove(f);
	}

	public Model getModel()
	{
		if (getModelObject() instanceof Model)
			return (Model) getModelObject();
		else
			return ((ModelElement) getModelObject()).getReferredModel();
	}

	@Override
	public boolean performFinish()
	{
		command.setNewDetails(getDisplayName(), getTableName(), fields.getContent(),
				tableConfiguration);
		getCommandStack().execute(command);
		return true;
	}

	public boolean performCancel()
	{
		for (int i = 0; i < ImportFromDBCounter; i++)
		{
			getCommandStack().undo();
		}

		return true;
	}

	public ModelObject getModelObject()
	{
		return modelObject;
	}

	public void setModelObject(ModelObject modelObject)
	{
		this.modelObject = modelObject;
	}

	public void addPages()
	{
		page = new TableWizardPage(this);
		addPage(page);
	}

	public TableConfiguration getTableConfiguration()
	{
		return tableConfiguration;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}

	public String getTableName()
	{
		return tableName;
	}

	public void setTableName(String tableName)
	{
		this.tableName = tableName;
	}

	public String getDisplayName()
	{
		return displayName;
	}

	public void setRecordElement(ModelElement recordElement)
	{
		this.recordElement = recordElement;
	}

	public ModelElement getRecordElement()
	{
		return recordElement;
	}

	private CommandStack getCommandStack()
	{
		IProject project = ((WorkspaceRepository) getModel().getRepository()).getProject();
		RepositoryManager manager = RepositoryManager.getRepositoryManager(project);
		return manager.getCommandStack();
	}

	@Override
	public void dispose()
	{
		fields.removeListener(listListener);
		super.dispose();
	}

	public void validate(Object value, TableFieldDescriptor descriptorToCheck, String pageNumber,
			String numberOfRecords)
	{
		errorMessage = null;

		validateFields(value, descriptorToCheck);

		if (errorMessage == null)
			validateDisplayName();
		if (errorMessage == null)
			validateDefinedPK(value, descriptorToCheck);
		if (errorMessage == null)
			validateNumberOfPages(pageNumber);
		if (errorMessage == null)
			validateNumberOfRecords(numberOfRecords);
		if (errorMessage == null)
			validateNumberFields();
	}

	private void validatePK(int primaryKey, TableFieldDescriptor descriptorToCheck)
	{
		WorkspaceRepository repository = (WorkspaceRepository) modelObject.getRepository();

		if (primaryKey == TableFieldDescriptor.MODEL_GENERATED_PK
				&& !"Number".equals(descriptorToCheck.getDataType().getName())
				&& !"Date and Time".equals(descriptorToCheck.getDataType().getName()))
			errorMessage = "You can choose 'Model Generated' primary key only if data type fiels is "
					+ "'Number' or 'Date and Time'.";

		if (errorMessage == null && primaryKey == TableFieldDescriptor.NOT_PK
				&& descriptorToCheck.isElementFieldPK())
			errorMessage = "Primary key must be set for: '" + descriptorToCheck.getName()
					+ "' field.";
	}

	private void validateDefinedPK(Object value, TableFieldDescriptor descriptorToCheck)
	{
		if (descriptorToCheck != null && value instanceof Integer)
		{
			if (descriptorToCheck.getPrimaryKey() != ((Integer) value).intValue())
			{
				int primaryKey = ((Integer) value).intValue();
				validatePK(primaryKey, descriptorToCheck);
			}
		}
	}

	private void validateNumberOfPages(String numberOfPages)
	{
		if (numberOfPages != null && numberOfPages.length() > 0)
		{
			int number;
			try
			{
				number = Integer.parseInt(numberOfPages);
				if (number < 0)
					errorMessage = "Invalid value - page number should be bigger or equals to 0.";
			}
			catch (NumberFormatException nfe)
			{
				errorMessage = "Invalid value - you must enter number value into 'number of pages' field.";
			}
		}
	}

	private void validateNumberOfRecords(String numberOfRecords)
	{
		if (numberOfRecords != null && numberOfRecords.length() > 0)
		{
			int number;
			try
			{
				number = Integer.parseInt(numberOfRecords);
				if (number < 0)
					errorMessage = "Invalid value - page number should be bigger then 0.";
			}
			catch (NumberFormatException nfe)
			{
				errorMessage = "Invalid value - you must enter number value into 'number of pages' field.";
			}
		}
		else
			errorMessage = "'Number of records per page' field can not be empty or equals to 0.";
	}

	private void validateFields(Object value, TableFieldDescriptor descriptorToCheck)
	{
		errorMessage = null;
		String stringValue = null;

		if (value instanceof String)
			stringValue = (String) value;

		if (stringValue != null && descriptorToCheck != null)
		{
			for (TableFieldDescriptor fieldDescriptor : fields.getContent())
			{
				if (descriptorToCheck != fieldDescriptor
						&& (stringValue.equals(fieldDescriptor.getInitialName()) || stringValue
								.equals(fieldDescriptor.getName())))
					errorMessage = "There is an existing field called '" + stringValue + "'";
			}

			if (stringValue.contains("/"))
				errorMessage = "A field name must not contain the slash character (/).";
		}

		if ((value == null || (stringValue != null && stringValue.length() == 0))
				&& descriptorToCheck != null)
			errorMessage = "Field name must be declared.";
	}

	private void validateNumberFields()
	{
		int size = fields.getContent().size();
		if (fields.getContent().size() == 0)
			errorMessage = "You must enter at least one field.";
	}

	public void validateAll()
	{
		errorMessage = null;

		validateAllFields();

		if (errorMessage == null)
			validateDisplayName();
		if (errorMessage == null)
			validateAllPK();
	}

	private void validateAllPK()
	{
		boolean atLeastOnePKExists = false;
		for (TableFieldDescriptor fieldDescriptor : fields.getContent())
		{
			int pk = fieldDescriptor.getPrimaryKey();
			validatePK(pk, fieldDescriptor);

			if (pk != 0)
				atLeastOnePKExists = true;
		}

		if (errorMessage == null && !atLeastOnePKExists)
			errorMessage = "At least one field must be a primary key";
	}

	private void validateAllFields()
	{
		errorMessage = null;

		validateNumberFields();

		if (errorMessage == null)
		{
			for (TableFieldDescriptor fieldDescriptor : fields.getContent())
			{
				String fieldName = fieldDescriptor.getName();

				if (fieldName == null || (fieldName != null && fieldName.length() == 0))
				{
					errorMessage = "Field name must be declared.";
					break;
				}

				for (TableFieldDescriptor descriptor : fields.getContent())
				{
					if (fieldDescriptor != descriptor
							&& (fieldName.equals(descriptor.getInitialName()) || fieldName
									.equals(descriptor.getName())))
						errorMessage = "There is an existing field called '" + fieldName + "'";
				}

				if (fieldName.contains("/"))
					errorMessage = "A field name must not contain the slash character (/).";
			}
		}
	}

	public void validateDisplayName()
	{
		String displayName = page.getDisplayName();

		if (displayName == null || (displayName != null && displayName.length() == 0))
			errorMessage = "Display name must be declared.";
		if (displayName != null && displayName.contains("/"))
			errorMessage = "Dislplay name must not contain the slash character (/).";
	}

	public String getErrorMessage()
	{
		return errorMessage;
	}

	public void setErrorMessage(String errorString)
	{
		errorMessage = errorString;
	}

	public boolean isReady()
	{
		return errorMessage == null;
	}

	public void addToImportFromDBCounter()
	{
		this.ImportFromDBCounter++;
	}

	public int getImportFromDBCounter()
	{
		return ImportFromDBCounter;
	}
}

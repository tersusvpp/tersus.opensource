/************************************************************************************************
 * Copyright (c) 2003-2021 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.wizards;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import tersus.editor.EditorMessages;
import tersus.editor.EditorPlugin;

/**
 * @author Youval Bronicki
 *
 */
public class ImportXMLDataStructuresByExampleWizardPage extends WizardPage
        implements IWizardPage
{

    private Text locationText;
    private Button createDatabaseForProject;
    private Text sampleFileText;

    /**
     * @param wizardName
     * @param selection
     */
    public ImportXMLDataStructuresByExampleWizardPage(String wizardName)
    {
        super(wizardName);
/*
 *         setTitle(EditorMessages.getString(wizardName+".pageTitle"));
 *         setDescription(EditorMessages.getString(wizardName+".pageDescription"));
*/
		setTitle(wizardName);
		setDescription("Create a data structure model representing the example XML file.");
    }
    
    public void createControl(Composite parent)
    {
        Composite container = new Composite(parent, SWT.NULL);
        addComponents(container);
        initialize();
        setPageComplete(false);
        setControl(container);
    }
    protected void addComponents(final Composite container)
    {
        GridLayout layout = new GridLayout();
        container.setLayout(layout);
        layout.numColumns = 3;
        layout.verticalSpacing = 9;
        Label label = new Label(container, SWT.NULL);
        label.setText("&Location:");
        
        locationText = new Text(container, SWT.BORDER | SWT.SINGLE);
        GridData gd1 = new GridData(GridData.FILL_HORIZONTAL);
        locationText.setLayoutData(gd1);
        locationText.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e)
            {
                dialogChanged();
            }
        });
        locationText.setEnabled(false);
        
        Button browseButton1 = new Button(container, SWT.PUSH);
        browseButton1.setText("Browse...");
        browseButton1.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e)
            {
            }
        });
        browseButton1.setEnabled(false);
        browseButton1.setVisible(false);
        Label sampleDataFileLabel =  new Label(container, SWT.WRAP);
        sampleDataFileLabel.setText("&Sample file:");
    	sampleFileText = new Text(container, SWT.BORDER|SWT.SINGLE);
    	GridData gd = new GridData(GridData.FILL_HORIZONTAL);
    	sampleFileText.setLayoutData(gd);
    	sampleFileText.addModifyListener(new ModifyListener() {

            public void modifyText(ModifyEvent e)
            {
                dialogChanged();
            }});
    	Button browseButton = new Button(container, SWT.PUSH);
        browseButton.setText("Browse...");
        browseButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e)
            {
                FileDialog dialog = new FileDialog(container.getShell());
                sampleFileText.setText(dialog.open());
            }
        });
	
    }
    private void dialogChanged()
    {
        getImportWizard().setSampleFilePath(sampleFileText.getText());
        getImportWizard().validate();
        setErrorMessage(getImportWizard().getErrorMessage());
        setPageComplete(getImportWizard().isReady());
    }

    /**
     *  
     */
    private void initialize()
    {
        locationText.setText(getImportWizard().getLocation());
        setImageDescriptor(ImageDescriptor.createFromURL(EditorPlugin
                .getDefault().getBundle().getEntry(
                        "icons/wizards/ImportXMLDataStructuresByExample 64x64.gif")));
    }

    protected ImportXMLDataStructuresByExampleWizard getImportWizard()
    {
        return (ImportXMLDataStructuresByExampleWizard) getWizard();
    }
}

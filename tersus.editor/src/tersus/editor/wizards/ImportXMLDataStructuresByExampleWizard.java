/************************************************************************************************
 * Copyright (c) 2003-2021 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.wizards;

import org.eclipse.core.resources.IProject;
import org.eclipse.gef.commands.Command;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;

import tersus.editor.RepositoryManager;
import tersus.editor.explorer.ExplorerEntry;
import tersus.editor.explorer.PackageEntry;
import tersus.model.PackageId;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 *
 */
public class ImportXMLDataStructuresByExampleWizard extends Wizard implements IImportWizard
{
    public static final String ID = "tersus.editor.wizards.ImportXMLDataStructuresByExampleWizard";
    public static final String WIZARD_NAME="Import XML Data Structures by Example";
    private String errorMessage;
    private String sampleFilePath;
    private IProject project;
    private PackageId parentPackageId;
    private IStructuredSelection selection;

    public void init(IWorkbench workbench, IStructuredSelection selection)
    {
		setWindowTitle(WIZARD_NAME);

		this.selection = selection;
        if (selection.size() == 1)
        {
            Object selectedObject = selection.getFirstElement();
            if (selectedObject instanceof PackageEntry)
            {
                setProject(((ExplorerEntry) selectedObject).getProject());
                if (selectedObject instanceof PackageEntry)
                    setParentPackageId(((PackageEntry) selectedObject)
                            .getPackageId());
            }
        }
    }
    protected Command getCommand()
    {
        return null;
    }

    /* (non-Javadoc)
     * @see tersus.editor.wizards.NewObjectWizard#getWizardName()
     */
    protected String getWizardName()
    {
        return WIZARD_NAME;
    }

    /* (non-Javadoc)
     * @see tersus.editor.wizards.NewObjectWizard#validate()
     */
    protected WorkspaceRepository getRepository()
    {
        return getRepositoryManager().getRepository();
    }
    private RepositoryManager getRepositoryManager()
    {
        return RepositoryManager.getRepositoryManager(getProject());
    }
    public void validate()
    {
        errorMessage = null;
        if (getProject() == null)
            errorMessage = "A location (target package) must be selected before starting this operation";
        else if (sampleFilePath == null || sampleFilePath.trim().length() == 0) 
        {
            errorMessage = "A sample XML file must be specified";
        }
    }


    public void addPages()
    {
        addPage(new ImportXMLDataStructuresByExampleWizardPage(WIZARD_NAME));
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.Wizard#performFinish()
     */
    public boolean performFinish()
    {
        try
        {
            ImportXMLDataStructureByExampleCommand command = new ImportXMLDataStructureByExampleCommand(getProject().getName(), getParentPackageId(), sampleFilePath);
            getRepositoryManager().getCommandStack().execute(command);
            return true;
        }
        catch (Exception e)
        {
            TersusWorkbench.log(e);
            MessageDialog.openError(getShell(), "Error", "Import Failed");
            return false;
        }
    }
    public String getErrorMessage()
    {
        return errorMessage;
    }

    /**
     * @return
     */
    public boolean isReady()
    {
        return errorMessage == null && sampleFilePath != null;
    }
    /**
     * @return
     */
    public String getLocation()
    {
        String location = "";
        if (project != null)
        {
            location += project.getName();
            if (parentPackageId != null)
            {
                location += "/" + parentPackageId.getPath();
            }
        }
        return location;
    }
    public PackageId getParentPackageId()
    {
        return parentPackageId;
    }
    public void setParentPackageId(PackageId parentPackageId)
    {
        this.parentPackageId = parentPackageId;
    }
    public IProject getProject()
    {
        return project;
    }
    public void setProject(IProject project)
    {
        this.project = project;
    }
    /**
     * @param text
     */
    public void setSampleFilePath(String sampleFilePath)
    {
        this.sampleFilePath = sampleFilePath;
    }
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.wizards;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.jface.wizard.Wizard;

import tersus.editor.RepositoryManager;
import tersus.editor.actions.RenamePackageCommand;
import tersus.model.BuiltinProperties;
import tersus.model.DataType;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.ModelObject;
import tersus.model.Role;
import tersus.model.commands.ChangeElementNameCommand;
import tersus.model.commands.ChangeModelIdCommand;
import tersus.model.commands.SetPropertyValueCommand;
import tersus.model.indexer.ElementEntry;
import tersus.model.indexer.ModelEntry;
import tersus.model.indexer.RepositoryIndex;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 * 
 */
public class RenameWizard extends Wizard
{
	ModelObject obj;

	private String modelName;

	private String elementName;

	private String constantValue;

	private String errorMessage;

	private boolean syncPackageName;

	private List<ElementEntry> potentialReferencesToRename;

	private boolean syncReferences;

	private String initialElementName;

	public RenameWizard(ModelObject obj)
	{
		super();
		this.obj = obj;
		setWindowTitle("Rename");
	}

	public void addPages()
	{
		addPage(new RenameWizardPage(this));
	}

	public boolean performFinish()
	{
		CompoundCommand command = new CompoundCommand();

		if (elementNameChanged())
			command.add(new ChangeElementNameCommand(getElement(), Role.get(elementName)));

		if (constantValueChanged() || (isConstant() && !isConstantValueSet() && modelNameChanged()))
			command.add(new SetPropertyValueCommand(getRepository(), getModel().getId(),
					BuiltinProperties.VALUE, getConstantValue()));
		
		if (modelNameChanged())
		{
			Model parent = null;

			if (obj instanceof ModelElement)
				parent = ((ModelElement) obj).getParentModel();

			if (syncReferences)
			{
				for (ElementEntry refEntry : getPotentialReferencesToRename())
				{
					ModelElement reference = getRepository().getUpdatedRepositoryIndex()
							.getModelElement(refEntry);
					if (!(parent != null && reference.getParentModel() == parent))
						command.add(new ChangeElementNameCommand(reference, Role.get(modelName)));
				}
			}

			command.add(getRenameModelCommand());

			if (isSyncPackageName())
				command.add(getRenamePackageCommand());

		}
		
		if (command.canExecute())
		{
			getCommandStack().execute(command);
			return true;
		}
		else
			return false;

	}

	private RenamePackageCommand getRenamePackageCommand()
	{
		tersus.model.Package pkg = getModel().getPackage();
		return new RenamePackageCommand((WorkspaceRepository) (getModel().getRepository()), pkg
				.getPackageId(), modelName);
	}

	private CommandStack getCommandStack()
	{
		IProject project = getRepository().getProject();
		RepositoryManager manager = RepositoryManager.getRepositoryManager(project);
		return manager.getCommandStack();
	}

	public ModelObject getModelObject()
	{
		return obj;
	}

	public void setModelName(String modelName)
	{
		this.modelName = modelName;
	}

	public void setElementName(String elementName)
	{
		this.elementName = elementName;
	}

	public void validate()
	{
		errorMessage = null;

		if (modelNameChanged())
		{
			errorMessage = getRenameModelCommand().validate();
			if (errorMessage == null && isSyncPackageName())
				errorMessage = getRenamePackageCommand().validate();

			if (errorMessage == null && syncReferences)
			{
				ModelId invalidModelId = changeModelNameValidation();
				if (invalidModelId != null)
				{
					errorMessage = "There is an existing element called '" + modelName + "' in: "
							+ invalidModelId;
				}
			}
		}
		if (errorMessage == null && elementNameChanged())
		{
			if (!Role.isValidString(elementName))
				errorMessage = "Invalid element name '" + elementName + "'";
			else if (getElement().getParentModel().getElement(Role.get(elementName)) != null)
				errorMessage = "There is an existing element called '" + elementName + "'";
		}
		if (errorMessage == null && constantValueChanged())
		{
			if (getConstantValue().isEmpty())
				errorMessage = "Constant value has not been specified.";
		}
	}

	private ModelId changeModelNameValidation()
	{
		RepositoryIndex index = getRepository().getUpdatedRepositoryIndex();
		for (ElementEntry refEntry : getPotentialReferencesToRename())
		{
			ModelEntry parent = index.getModelEntry(refEntry.getParentId());
			for (ElementEntry e : index.getElementEntries(parent.getId()))
			{
				if (e != refEntry && e.getRole().toString().equals(modelName))
				{
					return parent.getId();
				}
			}
		}
		return null;
	}

	ChangeModelIdCommand getRenameModelCommand()
	{
		WorkspaceRepository repository = getRepository();

		return new ChangeModelIdCommand(repository.getModel(getModel().getId(), true), modelName);
	}

	private WorkspaceRepository getRepository()
	{
		if (getModel() != null)
			return (WorkspaceRepository) getModel().getRepository();
		else
			return (WorkspaceRepository) getElement().getParentModel().getRepository();
	}

	public String getErrorMessage()
	{
		return errorMessage;
	}

	public boolean isReady()
	{
		return errorMessage == null
				&& (elementNameChanged() || modelNameChanged() || constantValueChanged());
	}

	private boolean elementNameChanged()
	{
		return (getElement() != null && elementName != null && !getElement().getRole().toString()
				.equals(elementName));
	}

	private boolean constantValueChanged()
	{
		return (getConstantValue() != null && !getConstantValue().equals(
				getModel().getProperty(BuiltinProperties.VALUE)));
	}
	
	public boolean isConstant()
	{
		return getModel() != null
				&& Boolean.TRUE.equals(getModel().getProperty(BuiltinProperties.CONSTANT));
	}
	
	private boolean isConstantValueSet()
	{
		if (isConstant() && getModel() instanceof DataType)
		{
			return ((DataType)getModel()).isValueSet();
		}
		
		return false;
	}
	

	ModelElement getElement()
	{
		if (obj instanceof ModelElement)
			return (ModelElement) obj;
		else
			return null;
	}

	private boolean modelNameChanged()
	{
		return (getModel() != null && modelName != null && !getModel().getName().equals(modelName));
	}

	Model getModel()
	{
		if (obj instanceof ModelElement)
			return ((ModelElement) obj).getReferredModel();
		else
			return (Model) obj;
	}

	public void setSyncPackageName(boolean syncPackageName)
	{
		this.syncPackageName = syncPackageName;
	}

	public boolean isSyncPackageName()
	{
		return syncPackageName;
	}

	public boolean renameOtherReferencesIsRelevant()
	{
		if (getModel() == null)
			return false;
		if (getModel().isReadOnly())
			return false;
		return getPotentialReferencesToRename().size() > 0;
	}

	public List<ElementEntry> getPotentialReferencesToRename()
	{
		if (potentialReferencesToRename == null)
		{
			potentialReferencesToRename = new ArrayList<ElementEntry>();
			for (ElementEntry e : getRepository().getUpdatedRepositoryIndex().getReferenceEntries(
					getModel().getId()))
			{
				boolean sameElement = e != null && getElement() != null
						&& e.getRole() == getElement().getRole()
						&& e.getParentId().equals(getElement().getParentModel().getId());
				if (!sameElement && !getRepository().isReadOnly(e.getParentId().getPackageId())
						&& e.getRole().toString().equals(getModel().getName()))
					potentialReferencesToRename.add(e);
			}
		}
		return potentialReferencesToRename;
	}

	public void setSyncReferences(boolean b)
	{
		this.syncReferences = b;
	}

	public void setInitialElementName(String initialElementName)
	{
		this.initialElementName = initialElementName;

	}

	public String getInitialElementName()
	{
		return initialElementName;
	}

	public String getConstantValue()
	{
		return constantValue;
	}

	public void setConstantValue(String constantValue)
	{
		this.constantValue = constantValue;
	}

	public String getModelName()
	{
		return modelName;
	}

}
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.wizards;

import tersus.model.BuiltinModels;
import tersus.model.ModelId;

/**
 * @author Youval Bronicki
 *  
 */
public class NewSystemWizard extends NewModelWizard
{


    public static final String ID = "tersus.editor.wizards.NewSystem";
    public static final String WIZARD_NAME="NewSystemWizard";


 


    protected ModelId getTemplateModelId()
    {
        return BuiltinModels.SYSTEM_TEMPLATE_ID;
    }





    /* (non-Javadoc)
     * @see tersus.editor.wizards.NewObjectWizard#getWizardName()
     */
    protected String getWizardName()
    {
        return WIZARD_NAME;
    }
}
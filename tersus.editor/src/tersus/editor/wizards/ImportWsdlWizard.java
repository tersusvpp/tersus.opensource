/************************************************************************************************
 * Copyright (c) 2003-2021 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.wizards;

import org.eclipse.core.resources.IProject;
import org.eclipse.gef.commands.Command;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;

import tersus.editor.RepositoryManager;
import tersus.editor.explorer.ProjectEntry;
import tersus.model.PackageId;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Ofer Brandes
 * @author Liat Shiff
 */
public class ImportWsdlWizard extends Wizard implements IImportWizard
{
	public static final String ID = "tersus.editor.wizards.ImportWsdlWizard";
	public static final String WIZARD_NAME = "Import WSDL";
	
	private String errorMessage;
	
	private String wsdlFilePath;
	
	private IProject project;
	
	private PackageId parentPackageId;

	public void init(IWorkbench workbench, IStructuredSelection selection)
	{
		setWindowTitle(WIZARD_NAME);
		
		if (selection.size() == 1 && selection.getFirstElement() instanceof ProjectEntry)
			setProject(((ProjectEntry)selection.getFirstElement()).getProject());
	}

	protected Command getCommand()
	{
		return null;
	}

	protected String getWizardName()
	{
		return WIZARD_NAME;
	}

	protected WorkspaceRepository getRepository()
	{
		return getRepositoryManager().getRepository();
	}

	private RepositoryManager getRepositoryManager()
	{
		return RepositoryManager.getRepositoryManager(getProject());
	}

	public void validate()
	{
		errorMessage = null;
		if (getProject() == null)
			errorMessage = "A location (target project) must be selected before starting this operation";
		else if (wsdlFilePath == null || wsdlFilePath.trim().length() == 0)
		{
			errorMessage = "A WSDL URI/file must be specified";
		}
	}

	public void addPages()
	{
		addPage(new ImportWsdlWizardPage(this));
	}

	public boolean performFinish()
	{
		try
		{

			ImportWsdlCommand command = new ImportWsdlCommand(getProject().getName(), wsdlFilePath,
					getRepository());
			getRepositoryManager().getCommandStack().execute(command);
			return true;
		}
		catch (Exception e)
		{
			TersusWorkbench.log(e);
			MessageDialog.openError(getShell(), "Error", "Import Failed");
			return false;
		}
	}

	public String getErrorMessage()
	{
		return errorMessage;
	}

	public boolean isReady()
	{
		return errorMessage == null && wsdlFilePath != null && getProject() != null;
	}

	public String getLocation()
	{
		String location = "";
		if (project != null)
		{
			location += project.getName();
			if (parentPackageId != null)
			{
				location += "/" + parentPackageId.getPath();
			}
		}
		return location;
	}

	public IProject getProject()
	{
		return project;
	}

	public void setProject(IProject project)
	{
		this.project = project;
	}

	public void setSampleFilePath(String wsdlFilePath)
	{
		this.wsdlFilePath = wsdlFilePath;
	}
}

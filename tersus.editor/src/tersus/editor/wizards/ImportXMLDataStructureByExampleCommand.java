/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.wizards;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Stack;

import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.gef.commands.Command;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import tersus.editor.RepositoryManager;
import tersus.model.BuiltinModels;
import tersus.model.Composition;
import tersus.model.DataElement;
import tersus.model.DataType;
import tersus.model.ModelException;
import tersus.model.ModelId;
import tersus.model.Package;
import tersus.model.PackageId;
import tersus.model.Role;
import tersus.util.DateUtil;
import tersus.util.NumberUtil;
import tersus.util.SpecialRoles;
import tersus.workbench.WorkspaceRepository;

/**
 * 
 * Creating models from example XML documents.
 * 
 * @author Ofer Brandes
 *  
 */

// NICE3 Try to find better names for the class and its methods.
public class ImportXMLDataStructureByExampleCommand extends Command
{
    private String projectName;

    private PackageId packageId;
    
    private String sampleFilePath;

    private DataType topModel;

    public ImportXMLDataStructureByExampleCommand(String projectName,
            PackageId packageId, String sampleFilePath)
    {
        super();
        this.projectName = projectName;
        this.packageId = packageId;
        this.sampleFilePath = sampleFilePath;
    }
    
    public void execute()
    {
        try
        {
            run();
        }
        catch (SAXException e)
        {
            Exception cause = e.getException();
            cause.printStackTrace();
            throw new XMLImportEception("Failed to import XML data structure",cause);
        }
        catch (Exception e)
        {
            throw new XMLImportEception("Failed to import XML data structure",e);
        }
    }
    /**
     * 
     * Creates a data type hierarchy from an example XML document.
     * 
     * Existing models in the target package can be referenced (and may get
     * updated if previously unknown elements or attributes are encountered).
     * Specifically, re-creating a hierarchy from the same input XML document
     * does not create nor modify any model. To avoid inconsistencies and
     * allow for proper 'undo', it is assumed all modified models are saved
     * prior to invoking this method (and once again once it finishes
     * successfully).
     * 
     * Standard SAX parser is used to parse the input XML document.
     * 
     * @param fileName
     *            The name of a file containing the input example XML Document
     *            (relative or absolute path).
     * @param projectName
     *            The name of the project to which the target package belongs.
     * @param packageId
     *            The name of the target package to which created models are to
     *            be added.
     * 
     * @return The top model in the created hierarchy (a data type).
     * 
     * @see ParseXMLHandler.
     */
    public void run() throws IOException, SAXException
    {

        XMLReader xr = XMLReaderFactory.createXMLReader();
        XmlDocumentHandler parser = new XmlDocumentHandler();
        try
        {
            xr.setContentHandler(parser);
            xr.setErrorHandler(parser);

            InputStream xmlDocument = new FileInputStream(sampleFilePath);
            xr.parse(new InputSource(xmlDocument));

        }
        finally
        {
            parser.dispose();
        }

        topModel = parser.getTopModel();
    }

    /**
     * 
     * A SAX handler to parse an XML document while creating according to its
     * structure a hierarchy of data types.
     *  
     */

    //	NICE3 Algorithm needs to be better documented (e.g. "memeory" implemented
    // through the 'models' stack).
    private class XmlDocumentHandler extends DefaultHandler
    {
        private static final boolean compactContentOnlyElements = true;

        WorkspaceRepository repository = null;

        private Stack models = null;

        private DataType topModel = null;

        /**
         * 
         * Creates an instance of a SAX handler that parses an XML document
         * while creating according to its structure a hierarchy of data types.
         *  
         */
        private XmlDocumentHandler()
        {
            super();

            IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
            repository = RepositoryManager.getRepositoryManager(root
                    .getProject(projectName)).getRepository();
        }

        /**
         * 
         * Get the top data type created according to the parsed XML document.
         *  
         */
        private DataType getTopModel()
        {
            return (topModel);
        }

        /**
         * 
         * Done with the repository.
         *  
         */
        public void dispose()
        {
        }

        /**
         * 
         * A data type (one of the ones created while parsing the XML document)
         * and all the data elements that have been encountered under it.
         * 
         * This class is used to check whether a data element has already been
         * encountered (under the same data type), so further information on the
         * same element results in updating the element instead of creating a
         * new one.
         * 
         * Notice that not all data elements that exist under a data type are
         * listed, only those encountered while parsing the XML document (if the
         * model already exists in the repository, it may contain additional
         * elements to those created due to the "inspiration" of the currently
         * parsed XML document).
         *  
         */
        private class ModelInfo
        {
            private DataType model;

            private HashMap elements;

            private ModelInfo(DataType model)
            {
                this.model = model;
                this.elements = new HashMap();
            }
        }

        /**
         * Handles a notification of the start of an XML element.
         * 
         * The start of an XML element triggers the creation of a data type with
         * the same name (or potentially updating of that model if it already
         * exists).
         * 
         * Each XML attribute of the XML element yields a data element.
         * 
         * @param uri
         *            Namespace URI (currently ignored).
         * @param name
         *            Element's local name (currently ignored).
         * @param qName
         *            Element's raw XML 1.0 name (used as the name of the
         *            created model).
         * @param atts
         *            The element's attributes.
         */
        public void startElement(String uri, String name, String qName,
                Attributes atts)
        {
            //			if ("".equals (uri))
            //				System.out.println("Start element: " + qName);
            //			else
            //				System.out.println("Start element: {" + uri + "}" + name);

            // Get the existing data type (or create a new data type, if does
            // not exist)
            if (models == null)
                models = new Stack(); // NICE2 Move to the constructor?

            ModelId modelId = new ModelId(packageId, qName);
            DataType model = (DataType) repository.getModel(modelId, true);

            if (model == null)
            {
                model = (DataType) repository.newModel(modelId, BuiltinModels.DATA_STRUCTURE_TEMPLATE_ID,false);
            }
            else
            {
                if (model.getComposition() != Composition.COLLECTION)
                    throw new ModelException("Model " + modelId
                            + " already exists as " + model.getComposition()
                            + " - cannot model an XML element");
            }

            models.push(new ModelInfo(model));

            // Each XML attribute is modeled as a text leaf data element
            for (int i = 0; i < atts.getLength(); i++)
            {
                //				System.out.println("\t"+atts.getQName(i)+" =
                // "+atts.getValue(i));

                ModelId leafId = getLeafId(atts.getValue(i));
                DataType leaf = (DataType) repository.getModel(leafId, true);
                if (leaf == null)
                    throw new ModelException("Missing model: " + leafId);

                String role = SpecialRoles.DEPENDENT_MODEL_ROLE_PREFIX + atts.getQName(i);

                addDataElement((ModelInfo) models.peek(), leaf, role, false);
            }

        }

        /**
         * Handles a notification of character data inside an XML element.
         * 
         * The character data triggers the creation of a leaf data element of
         * the containing data type with the special role CONTENT_LEAF_ROLE
         * (whose type corresponds to the type of the XML content -
         * 
         * @see getLeafId()).
         * 
         * @param ch
         *            A characters array containing the element's data.
         * @param start
         *            The start position in the character array.
         * @param length
         *            The number of characters to use from the characters array.
         */
        public void characters(char ch[], int start, int length)
        {
            String content = (new String(ch, start, length)).trim();
            if (content.length() == 0)
                return;

            //			System.out.print("\tCharacters: \"");
            //			for (int i = start; i < start + length; i++) {
            //				switch (ch[i]) {
            //				case '\\':
            //				System.out.print("\\\\");
            //				break;
            //				case '"':
            //				System.out.print("\\\"");
            //				break;
            //				case '\n':
            //				System.out.print("\\n");
            //				break;
            //				case '\r':
            //				System.out.print("\\r");
            //				break;
            //				case '\t':
            //				System.out.print("\\t");
            //				break;
            //				default:
            //				System.out.print(ch[i]);
            //				break;
            //				}
            //			}
            //			System.out.print("\"\n");

            // Textual content is modeled a leaf data element

            ModelId leafId = getLeafId(content);
            DataType leaf = (DataType) repository.getModel(leafId, true);
            if (leaf == null)
                throw new ModelException("Missing model: " + leafId);

            addDataElement((ModelInfo) models.peek(), leaf,
                    SpecialRoles.XML_CONTENT_LEAF_ROLE, false);
        }

        /**
         * Handles a notification of the end of an XML element.
         * 
         * The end of an XML element triggers making the current data type a
         * data element of the containing data type (or potentially updating the
         * properties of that data element if it already exists).
         * 
         * If it is the top data type, parsing is done (@see getTopModel()).
         * 
         * @param uri
         *            Namespace URI (currently ignored).
         * @param name
         *            Element's local name (currently ignored).
         * @param qName
         *            Element's raw XML 1.0 name (used as the name of the
         *            created model).
         */
        public void endElement(String uri, String name, String qName)
        {
            //			if ("".equals (uri))
            //				System.out.println("End element: " + qName);
            //			else
            //				System.out.println("End element: {" + uri + "}" + name);

            DataType model = ((ModelInfo) models.pop()).model;

            // Convert a data element with "Content" child only to become atomic
            if (compactContentOnlyElements)
            {
                if ((model.getElements().size() == 1)
                        && (model.getElement(Role
                                .get(SpecialRoles.XML_CONTENT_LEAF_ROLE)) != null))
                // NICE2 Anything more to check (e.g. gurantee it is
                // non-repetitive)?
                {
                    if (model.isModified()) // We are assuming it's a new model
                        
                    {
                        repository.removeModel(model);
                        model = (DataType) model.getElement(
                            Role.get(SpecialRoles.XML_CONTENT_LEAF_ROLE))
                            .getReferredModel();
                    }
                }
            }

            // Current data model is a data element of the enclosing data model
            if (!models.empty())
                addDataElement((ModelInfo) models.peek(), model, qName, true);

            // Current data model is the top data model - done with parsing
            else
            {
                topModel = model;
                Package pkg = topModel.getPackage();
                dispose();
            }
        }

        /**
         * 
         * Add a data type as a data element to a parent data type (or update
         * the properties of the corresponding data element if it already
         * exists)
         * 
         * @param currentModel
         *            Parent data type.
         * @param subModel
         *            Child data type to be added as a data element of the
         *            parent data type.
         * @param role
         *            Role of the data element.
         * @param mayBeRepetitive
         *            Indication whether data element may be a repetitive one
         *            (set to 'true' if the element represents an XML element,
         *            'false' if represents character data content or an XML
         *            attribute).
         */
        private void addDataElement(ModelInfo currentModel, DataType subModel,
                String role, boolean mayBeRepetitive)
        {
            DataType model = currentModel.model;

            //			System.out.println("\t"+model.getId()+": "+role+" =
            // "+subModel.getId());

            // If data element already exists as a child of current data type,
            // use it
            DataElement modelElement = (DataElement) model.getElement(Role
                    .get(role));

            boolean isNew = (modelElement == null);

            if (isNew)
            {
                modelElement = new DataElement();
                modelElement.setRole(Role.get(role));
                modelElement.setModel(subModel);
            }
            else
            {
                ModelId existingId = modelElement.getRefId();
                ModelId newId = subModel.getId();
                if (!existingId.equals(newId))
                {
                    if (newId.equals(BuiltinModels.TEXT_ID))
                        modelElement.setReferredModel(subModel);
                    else if(!existingId.equals(BuiltinModels.TEXT_ID))
                        throw new ModelException(model.getId() + ": '" + role
                            + "' already exists with type " + existingId + " (new type "+newId+")");
                }
            }

            // Check whether data element is repetitive in the imported XML
            // document
            Long occurences = (Long) currentModel.elements.get(role);

            if (occurences == null)
                occurences = new Long(1);
            else
                occurences = new Long(occurences.longValue() + 1);

            currentModel.elements.put(role, occurences);

            if (occurences.longValue() > 1)
                if (mayBeRepetitive)
                {
                    //  'true' may override 'false', but not vice versa
                    modelElement.setRepetitive(true);
                }

            // No need to set the 'position' and 'size' properties (meaningless
            // inside data types)

            if (isNew)
                model.addElement(modelElement); // This also sets the element's 'parentModel' property
        }

        /**
         * 
         * Get the built-in basic data type (e.g. Number or Date) corresponding
         * to the type of the content of an XML element.
         * 
         * @param leafContent
         *            Character data inside an XML element.
         * @return The Id of an atomic built-in basic data type that fits
         *         'leafContent' (Date if fits, else Number if fits, else Text).
         */
        private ModelId getLeafId(String leafContent)
        {
            ModelId leafId = BuiltinModels.TEXT_ID; // Default is a text
            // leaf

            if (DateUtil.formatOf(leafContent) != null)
                leafId = BuiltinModels.DATE_ID;

            else if (NumberUtil.isNumber(leafContent))
                leafId = BuiltinModels.NUMBER_ID;

            return (leafId);
        }

        public void error(SAXParseException e) throws SAXException
        {
            throw e;
        }
    }
    public boolean canUndo()
    {
        return false;
    }
}
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.wizards;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.ModelId;

/**
 * @author Youval Bronicki
 *  
 */
public class NewActionWizard extends NewModelWizard
{


    public static final String ID = "tersus.editor.wizards.NewAction";
    public static final String WIZARD_NAME="NewActionWizard";

    protected Model getTemplateModel()
    {
        return getRepository().getModel(BuiltinModels.ACTION_TEMPLATE_ID, true);
    }

    protected String getWizardName()
    {
        return WIZARD_NAME;
    }
    
    protected ModelId getTemplateModelId()
    {
        return BuiltinModels.ACTION_TEMPLATE_ID;
    }
}
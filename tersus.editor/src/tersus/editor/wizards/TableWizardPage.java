package tersus.editor.wizards;

import java.util.HashMap;

import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.ColumnViewerEditor;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationStrategy;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TableViewerEditor;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;

import tersus.eclipse.utils.ListContentProvider;
import tersus.editor.Debug;
import tersus.editor.EditorPlugin;
import tersus.editor.TersusEditor;
import tersus.editor.wizards.ImportStructureFromDB.ImportStructureFromDBWizard;
import tersus.model.BuiltinModels;
import tersus.model.BuiltinProperties;
import tersus.model.DataElement;
import tersus.model.DataType;
import tersus.model.Filtering;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.commands.TableFieldDescriptor;
import tersus.util.Trace;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

public class TableWizardPage extends WizardPage implements IWizardPage
{
	public static final String NAME = "Main Page";

	private static final String LABEL_KEY = "Label";

	private static final Image CHECKED = ImageDescriptor.createFromFile(TersusEditor.class,
			"icons/checked.gif").createImage();

	private static final Image UNCHECKED = ImageDescriptor.createFromFile(TersusEditor.class,
			"icons/unchecked.gif").createImage();

	private TableWizard wizard;

	private Label tableName;

	private Text displayName;

	protected TableViewer fieldViewer;

	private Button addFieldButton;

	private Button ImportFieldsButton;

	private Button selectAllButton;

	private Button deselectAllButton;

	private CheckboxCellEditor showRowEditor;

	private TextCellEditor elementNameEditor;

	private ComboBoxCellEditor dataTypeEditor;

	private TextCellEditor validValuesEditor;

	private ModelId[] dataTypes;

	private String[] dataTypeNames;

	private String[] pkTypeNames;

	private ComboBoxCellEditor pkEditor;

	private String[] filteringNames;

	private ComboBoxCellEditor filteringEditor;

	private Button defaultOptimization;

	private Button showPagingHeaderGroup;

	private Button showActionHeaderGroup;

	private Button showCountFooterRow;

	private Text numberOfPagesToDisplay;

	private Text numberOfRecordsPerPage;

	private Group detailsGroup;

	private IDialogSettings settings;

	public TableWizardPage(TableWizard wizard)
	{
		super(NAME);
		setTitle("Create / Modify Table");
		// setDescription("--");
		this.wizard = wizard;

		dataTypes = TableFieldDescriptor.getGenericDataTypes();
		dataTypeNames = new String[dataTypes.length];
		for (int i = 0; i < dataTypes.length; i++)
		{
			dataTypeNames[i] = dataTypes[i].getName();
		}

		int nPKTypes = 4;
		pkTypeNames = new String[nPKTypes];
		for (int i = 0; i < nPKTypes; i++)
		{
			pkTypeNames[i] = getPKLabel(i);
		}

		int filteringTypes = 5;
		filteringNames = new String[filteringTypes];
		for (int i = 0; i < filteringTypes; i++)
		{
			filteringNames[i] = Filtering.getFilteringName(i);
		}

		settings = EditorPlugin.getDefault().getDialogSettings();
	}

	public TableWizardPage(String pageName)
	{
		super(pageName);
		// TODO Auto-generated constructor stub
	}

	public void createControl(Composite parent)
	{
		Composite container = new Composite(parent, SWT.NULL);
		setLayout(container, 1, 15);
		addTableDetails(container);
		addButtons(container);
		addFields(container);
		addAdvanced(container);
		tableName.setText(wizard.getTableName());
		displayName.setText(wizard.getDisplayName());
	}

	private void addButtons(Composite parent)
	{
		Composite buttonBar = new Composite(parent, SWT.NULL);
		buttonBar.setLayout(new RowLayout());

		addFieldButton = new Button(buttonBar, SWT.PUSH);
		addFieldButton.setText("&Add field");
		addFieldButton.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				TableFieldDescriptor descriptor = new TableFieldDescriptor("New Field",
						BuiltinModels.TEXT_ID);
				wizard.addField(descriptor);
				validateAll();
			}

		});

		addImportFieldsButton(buttonBar);
		addSelectAllButton(buttonBar);
		addDeselectAllButton(buttonBar);

		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = false;
		gridData.verticalAlignment = GridData.VERTICAL_ALIGN_BEGINNING;
		buttonBar.setLayoutData(gridData);

	}

	private void addImportFieldsButton(Composite buttonBar)
	{
		ImportFieldsButton = new Button(buttonBar, SWT.PUSH);
		ImportFieldsButton.setText("&Import fields");
		ImportFieldsButton.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				if (Debug.OTHER)
					Trace.push("importTableStructureWizardAction.run()");
				try
				{
					Model recordModel = wizard.getRecordElement().getReferredModel();

					HashMap<ModelElement, ModelElement> oldElements = new HashMap<ModelElement, ModelElement>();

					for (ModelElement element : recordModel.getElements())
					{
						oldElements.put(element, element);
					}

					ImportStructureFromDBWizard importStructureFromDBWizard = new ImportStructureFromDBWizard(
							wizard.getRecordElement(), getActiveEditor(), getShell(), false, true);
					WizardDialog dialog = new WizardDialog(getShell(), importStructureFromDBWizard);
					dialog.setMinimumPageSize(1150, 500);
					dialog.open();

					if (dialog.getReturnCode() == Window.OK)
					{
						setImportFieldFromDBResualts(oldElements, recordModel);
						validateAll();
						wizard.addToImportFromDBCounter();
					}
				}
				finally
				{
					if (Debug.OTHER)
						Trace.pop();
				}
			}
		});
	}

	private void addSelectAllButton(Composite buttonBar)
	{
		selectAllButton = new Button(buttonBar, SWT.PUSH);
		selectAllButton.setText("&Select All");

		selectAllButton.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				setShowRowsFields(true);
			}
		});
	}

	private void addDeselectAllButton(Composite buttonBar)
	{
		deselectAllButton = new Button(buttonBar, SWT.PUSH);
		deselectAllButton.setText("&Deselect All");

		deselectAllButton.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				setShowRowsFields(false);
			}
		});
	}

	private void setShowRowsFields(boolean value)
	{
		ListContentProvider<TableFieldDescriptor> contentProvider = (ListContentProvider<TableFieldDescriptor>) fieldViewer
				.getContentProvider();

		for (Object obj : contentProvider.getElements(fieldViewer.getInput()))
		{
			if (obj instanceof TableFieldDescriptor)
			{
				TableFieldDescriptor fieldDescriptor = (TableFieldDescriptor) obj;
				
				if (!fieldDescriptor.isInitiallyShown())
					fieldDescriptor.setShowRow(value);
			}
		}
	}

	private void setImportFieldFromDBResualts(HashMap<ModelElement, ModelElement> oldElements,
			Model recordModel)
	{
		Object tName = wizard.getRecordElement().getReferredModel().getProperty(
				BuiltinProperties.TABLE_NAME);
		if (tName != null && tName instanceof String)
		{
			wizard.setTableName((String) tName);
			tableName.setText(wizard.getTableName());
		}

		for (ModelElement element : recordModel.getElements())
		{
			if (!oldElements.keySet().contains(element))
			{
				DataElement dataElement = (DataElement) element;
				String name = dataElement.getElementName();
				ModelId dataType = dataElement.getModelId();
				DataType dataModel = (DataType) recordModel.getRepository()
						.getModel(dataType, true);
				boolean mandatory = Boolean.TRUE.equals(dataElement
						.getProperty(BuiltinProperties.MANDATORY));
				String validValues = (String) dataModel.getProperty(BuiltinProperties.VALID_VALUES);
				int primaryKey = TableFieldDescriptor.NOT_PK;
				if (dataElement.isPrimaryKey())
					primaryKey = TableFieldDescriptor.DATABASE_GENERATED_PK;

				Object isElementPrimaryKey = dataElement.getProperty(BuiltinProperties.PRIMARY_KEY);

				int filtering = 0;

				TableFieldDescriptor field = new TableFieldDescriptor(name, name, null,
						dataType, primaryKey, mandatory, validValues, filtering, false,
						false, isElementPrimaryKey != null ? ((Boolean) isElementPrimaryKey)
								.booleanValue() : false);

				field.saveInitialState();
				field.setDataType(field.getGenericDataType((WorkspaceRepository) recordModel
						.getRepository())); // Non-generic data types are
				// displayed to the user as the
				// corresponding generic data types
				wizard.addField(field);
			}
		}
	}

	private TersusEditor getActiveEditor()
	{
		IWorkbenchPage page = TersusWorkbench.getActiveWorkbenchWindow().getActivePage();
		IEditorPart activeEditor = page.getActiveEditor();

		if (activeEditor instanceof TersusEditor)
			return (TersusEditor) activeEditor;

		return null;
	}

	private Composite addTableDetails(Composite container)
	{
		Composite tableDetails = new Composite(container, SWT.NULL);
		setLayout(tableDetails, 4, 20);
		addTableNameField(tableDetails);
		addDisplayNameField(tableDetails);

		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = false;
		gridData.verticalAlignment = GridData.VERTICAL_ALIGN_BEGINNING;
		tableDetails.setLayoutData(gridData);

		setControl(container);

		return tableDetails;
	}

	private Composite addAdvanced(Composite container)
	{
		Button advancedButtun = new Button(container, SWT.PUSH);
		advancedButtun.setText("A&dvanced >>");
		advancedButtun.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				if (detailsGroup != null)
				{
					detailsGroup.setVisible(!detailsGroup.isVisible());
					settings.put("AdvancedOpen", detailsGroup.isVisible());
				}
			}
		});

		detailsGroup = new Group(container, SWT.NONE | container.getStyle());
		detailsGroup.setText("Table Configuration Details");
		GridLayout layout = new GridLayout(1, true);
		layout.horizontalSpacing = 1;
		layout.verticalSpacing = 8;
		detailsGroup.setLayout(layout);

		String isAdvancedOpenString = settings.get("AdvancedOpen");
		if (isAdvancedOpenString == null)
		{
			settings.put("AdvancedOpen", false);
			detailsGroup.setVisible(settings.getBoolean("AdvancedOpen"));
		}
		else
			detailsGroup.setVisible(settings.getBoolean("AdvancedOpen"));

		Canvas checkBoxContainer = new Canvas(detailsGroup, SWT.LEFT_TO_RIGHT);
		GridLayout checkBoxLayout = new GridLayout(3, true);
		checkBoxLayout.numColumns = 3;
		checkBoxContainer.setLayout(checkBoxLayout);

		Composite con1 = new Composite(checkBoxContainer, SWT.NULL);
		setLayout(con1, 2, 9);
		addDefaultOptimizationCheckbox(con1);
		addShowActionGroupCheckbox(con1);

		Composite con2 = new Composite(checkBoxContainer, SWT.NULL);
		setLayout(con2, 2, 9);
		addShowSearchGroupCheckbox(con2);
		addShowCountFooterRowCheckbox(con2);

		Composite inputFieldsContainer = new Composite(detailsGroup, SWT.LEFT_TO_RIGHT);
		GridLayout inputFieldsLayout = new GridLayout(2, true);
		inputFieldsContainer.setLayout(inputFieldsLayout);

		addNumberOfPagesToDisplayField(inputFieldsContainer);
		addNumberOfRecordPerPage(inputFieldsContainer);
		return detailsGroup;
	}

	private Button createCheckBoxButton(Composite container, String text)
	{
		Button button = new Button(container, SWT.CHECK);
		button.setText(text);
		button.setLayoutData(new GridData(SWT.FILL, SWT.LEFT_TO_RIGHT, true, true,
				GridData.HORIZONTAL_ALIGN_END, GridData.VERTICAL_ALIGN_CENTER));

		return button;
	}

	private void addDefaultOptimizationCheckbox(Composite container)
	{
		defaultOptimization = createCheckBoxButton(container, "Default optimization");
		defaultOptimization.setSelection(wizard.getTableConfiguration().defaultOptimization());
		defaultOptimization.addSelectionListener(new SelectionListener()
		{
			public void widgetSelected(SelectionEvent e)
			{
				wizard.getTableConfiguration().setDefaultOptimization(
						defaultOptimization.getSelection());
			}

			public void widgetDefaultSelected(SelectionEvent e)
			{
				wizard.getTableConfiguration().setDefaultOptimization(
						defaultOptimization.getSelection());
			}
		});
	}

	private void addShowSearchGroupCheckbox(Composite container)
	{
		showPagingHeaderGroup = createCheckBoxButton(container, "Show paging header group");
		showPagingHeaderGroup.setSelection(wizard.getTableConfiguration().showPagingHeaderGroup());
		showPagingHeaderGroup.addSelectionListener(new SelectionListener()
		{
			public void widgetSelected(SelectionEvent e)
			{
				wizard.getTableConfiguration().setShowPagingHeaderGroup(
						showPagingHeaderGroup.getSelection());
			}

			public void widgetDefaultSelected(SelectionEvent e)
			{
				wizard.getTableConfiguration().setShowPagingHeaderGroup(
						showPagingHeaderGroup.getSelection());
			}
		});
	}

	private void addShowActionGroupCheckbox(Composite container)
	{
		showActionHeaderGroup = createCheckBoxButton(container, "Show action header group");
		showActionHeaderGroup.setSelection(wizard.getTableConfiguration().showActionHeaderGroup());
		showActionHeaderGroup.addSelectionListener(new SelectionListener()
		{
			public void widgetSelected(SelectionEvent e)
			{
				wizard.getTableConfiguration().setShowActionHeaderGroup(
						showActionHeaderGroup.getSelection());
			}

			public void widgetDefaultSelected(SelectionEvent e)
			{
				wizard.getTableConfiguration().setShowActionHeaderGroup(
						showActionHeaderGroup.getSelection());
				wizard.getTableConfiguration().setShowActionHeaderGroup(
						showActionHeaderGroup.getSelection());
			}
		});
	}

	private void addShowCountFooterRowCheckbox(Composite container)
	{
		showCountFooterRow = createCheckBoxButton(container, "Show count footer row");
		showCountFooterRow.setSelection(wizard.getTableConfiguration().showCountFooterRow());
		showCountFooterRow.addSelectionListener(new SelectionListener()
		{
			public void widgetSelected(SelectionEvent e)
			{
				wizard.getTableConfiguration().setShowCountFooterRow(
						showCountFooterRow.getSelection());
			}

			public void widgetDefaultSelected(SelectionEvent e)
			{
				wizard.getTableConfiguration().setShowCountFooterRow(
						showCountFooterRow.getSelection());
			}
		});
	}

	private void addNumberOfPagesToDisplayField(Composite container)
	{
		String labelText = "&Number of pages to display:";
		numberOfPagesToDisplay = createLabeledTextField(container, labelText);
		numberOfPagesToDisplay.setBounds(0, 0, 30, 20);

		Integer numberOfPages = Integer.valueOf(wizard.getTableConfiguration()
				.numberOfPagesToDisplay());
		numberOfPagesToDisplay.setText(numberOfPages != null ? numberOfPages.toString() : null);

		numberOfPagesToDisplay.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				validate(displayName.getText(), null, numberOfPagesToDisplay.getText(),
						numberOfRecordsPerPage != null ? numberOfRecordsPerPage.getText() : null);
				if (wizard.isReady())
					validateAll();

				if (wizard.isReady())
				{
					String numberOfPagesString = numberOfPagesToDisplay.getText();
					if (numberOfPagesString == null || numberOfPagesString.length() == 0)
						wizard.getTableConfiguration().setNumberOfPagesToDisplay(0);
					else
						wizard.getTableConfiguration().setNumberOfPagesToDisplay(
								Integer.parseInt(numberOfPagesToDisplay.getText()));
				}
			}
		});
	}

	private void addNumberOfRecordPerPage(Composite container)
	{
		String labelText = "N&umber of records per page:";
		numberOfRecordsPerPage = createLabeledTextField(container, labelText);
		numberOfRecordsPerPage.setBounds(0, 0, 30, 20);

		Integer numberOfRecords = Integer.valueOf(wizard.getTableConfiguration()
				.numberOfRecordsPerPage());
		numberOfRecordsPerPage.setText(numberOfRecords.toString());

		numberOfRecordsPerPage.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				validate(displayName.getText(), null, numberOfPagesToDisplay.getText(),
						numberOfRecordsPerPage.getText());
				if (wizard.isReady())
					validateAll();

				if (wizard.isReady())
					wizard.getTableConfiguration().setNumberOfRecordsPerPage(
							Integer.parseInt(numberOfRecordsPerPage.getText()));
			}
		});
	}

	private void addFields(Composite container)
	{
		configureFields(container);
		fieldViewer.setContentProvider(new ListContentProvider<TableFieldDescriptor>(fieldViewer));
		fieldViewer.setInput(wizard.getFields());

		validateAll();
	}

	public void configureFields(Composite container)
	{
		Composite tableContainer = new Composite(container, SWT.NULL);

		GridData gridData = new GridData(GridData.FILL_BOTH);
		gridData.widthHint = 50;
		gridData.heightHint = 180;
		gridData.verticalSpan = 4;
		tableContainer.setLayoutData(gridData);

		fieldViewer = new TableViewer(new Table(tableContainer, SWT.BORDER | SWT.FULL_SELECTION
				| SWT.V_SCROLL));
		fieldViewer.getTable().setSize(730, 180);

		TableViewerColumn showCol = new TableViewerColumn(fieldViewer, SWT.HORIZONTAL);
		showCol.getColumn().setWidth(45);
		showCol.getColumn().setText("Show");
		showCol.setLabelProvider(new CellLabelProvider()
		{
			@Override
			public void update(ViewerCell cell)
			{
				TableFieldDescriptor descriptor = (TableFieldDescriptor) cell.getElement();

				if (descriptor.getShowRow())
					cell.setImage(CHECKED);
				else
					cell.setImage(UNCHECKED);
			}

		});

		showRowEditor = new CheckboxCellEditor(null, SWT.CHECK | SWT.READ_ONLY);
		showCol.setEditingSupport(new TableWizardEditingSupoort(showRowEditor)
		{

			@Override
			protected Object getValue(TableFieldDescriptor descriptor)
			{
				Boolean b = new Boolean(!descriptor.getShowRow());
				return b;
			}

			@Override
			protected void setValue(Object element, Object value)
			{
				((TableFieldDescriptor) element)
						.setShowRow(Boolean.FALSE.equals(((Boolean) value)));
				editor.setValue(Boolean.TRUE);
				System.out.print("");
			}

			@Override
			protected boolean canEdit(Object element)
			{
				descriptor = (TableFieldDescriptor) element;

				if (descriptor.isInitiallyShown())
					return false;

				return true;
			}
		});

		TableViewerColumn elementNameCol = new TableViewerColumn(fieldViewer, SWT.NONE);
		elementNameCol.getColumn().setWidth(150);
		elementNameCol.getColumn().setText("Element Name");
		elementNameCol.setLabelProvider(new CellLabelProvider()
		{
			public void update(ViewerCell cell)
			{
				cell.setText(((TableFieldDescriptor) cell.getElement()).getName());
				cell.setImage(null);
			}
		});
		elementNameEditor = new TextCellEditor(fieldViewer.getTable());
		elementNameCol.setEditingSupport(new TableWizardEditingSupoort(elementNameEditor)
		{
			@Override
			protected Object getValue(TableFieldDescriptor descriptor)
			{
				return descriptor.getName();
			}

			@Override
			protected void setValue(Object element, Object value)
			{
				TableFieldDescriptor descriptor = (TableFieldDescriptor) element;
				validateAll();

				descriptor.setName((String) value);

				validateAll();
			}
		});

		TableViewerColumn dataTypeCol = new TableViewerColumn(fieldViewer, SWT.NONE);
		dataTypeCol.getColumn().setWidth(100);
		dataTypeCol.getColumn().setText("Type");
		dataTypeCol.setLabelProvider(new CellLabelProvider()
		{
			public void update(ViewerCell cell)
			{
				String s = ((TableFieldDescriptor) cell.getElement()).getDataType().getName();
				cell.setText(s);
				cell.setImage(null);

			}
		});
		dataTypeEditor = new ComboBoxCellEditor(getTable(), dataTypeNames);
		dataTypeCol.setEditingSupport(new TableWizardEditingSupoort(dataTypeEditor)
		{

			@Override
			protected Object getValue(TableFieldDescriptor descriptor)
			{
				for (int i = 0; i < dataTypes.length; i++)
				{
					if (dataTypes[i].equals(descriptor.getDataType()))
						return Integer.valueOf(i);
				}
				return 0;
			}

			@Override
			protected void setValue(Object element, Object value)
			{
				validateAll();
				TableFieldDescriptor descriptor = (TableFieldDescriptor) element;
				int index = ((Integer) value).intValue();
				if (index >= 0 && index < dataTypes.length)
					descriptor.setDataType(dataTypes[index]);
				validateAll();
			}
		});

		TableViewerColumn pkCol = new TableViewerColumn(fieldViewer, SWT.NONE);
		pkCol.getColumn().setWidth(100);
		pkCol.getColumn().setText("Primary Key");
		pkCol.setLabelProvider(new CellLabelProvider()
		{
			public void update(ViewerCell cell)
			{
				TableFieldDescriptor descriptor = (TableFieldDescriptor) cell.getElement();
				cell.setText(getPKLabel(descriptor.getPrimaryKey()));
				cell.setImage(null);
			}
		});
		pkEditor = new ComboBoxCellEditor(getTable(), pkTypeNames);
		pkCol.setEditingSupport(new TableWizardEditingSupoort(pkEditor)
		{
			@Override
			protected Object getValue(TableFieldDescriptor descriptor)
			{
				return descriptor.getPrimaryKey();
			}

			@Override
			protected void setValue(Object element, Object value)
			{
				TableFieldDescriptor descriptor = (TableFieldDescriptor) element;
				int index = ((Integer) value).intValue();
				descriptor.setPrimaryKey(index);
				validateAll();
			}
		});

		TableViewerColumn validValuesCol = new TableViewerColumn(fieldViewer, SWT.NONE);
		validValuesCol.getColumn().setWidth(230);
		validValuesCol.getColumn().setText("Valid Values");
		validValuesCol.setLabelProvider(new CellLabelProvider()
		{
			public void update(ViewerCell cell)
			{
				String validValues = ((TableFieldDescriptor) cell.getElement()).getValidValues();
				cell.setText(validValues == null ? "" : validValues);
				cell.setImage(null);
			}
		});
		validValuesEditor = new TextCellEditor(fieldViewer.getTable());
		validValuesCol.setEditingSupport(new TableWizardEditingSupoort(validValuesEditor)
		{
			@Override
			protected Object getValue(TableFieldDescriptor descriptor)
			{
				String validValues = descriptor.getValidValues();
				return (validValues == null ? "" : validValues);
			}

			@Override
			protected void setValue(Object element, Object value)
			{
				TableFieldDescriptor descriptor = (TableFieldDescriptor) element;
				validateAll();

				descriptor.setValidValues((String) value);

				validateAll();
			}
		});

		TableViewerColumn filteringCol = new TableViewerColumn(fieldViewer, SWT.NONE);
		filteringCol.getColumn().setWidth(100);
		filteringCol.getColumn().setText("Filtering");
		filteringCol.setLabelProvider(new CellLabelProvider()
		{
			public void update(ViewerCell cell)
			{
				TableFieldDescriptor descriptor = (TableFieldDescriptor) cell.getElement();
				cell.setText(Filtering.getFilteringName(descriptor.getFiltering()));
				cell.setImage(null);
			}
		});
		filteringEditor = new ComboBoxCellEditor(getTable(), filteringNames);
		filteringCol.setEditingSupport(new TableWizardEditingSupoort(filteringEditor)
		{
			@Override
			protected Object getValue(TableFieldDescriptor descriptor)
			{
				return descriptor.getFiltering();
			}

			@Override
			protected void setValue(Object element, Object value)
			{
				TableFieldDescriptor descriptor = (TableFieldDescriptor) element;
				int filterType = ((Integer) value).intValue();
				descriptor.setFiltering(filterType);
				validateAll();
			}
		});

		TableViewerEditor.create(fieldViewer,
				new ColumnViewerEditorActivationStrategy(fieldViewer),
				ColumnViewerEditor.TABBING_HORIZONTAL
						| ColumnViewerEditor.TABBING_MOVE_TO_ROW_NEIGHBOR
						| ColumnViewerEditor.TABBING_VERTICAL);

		Table table = getTable();

		table.setLinesVisible(true);
		table.setHeaderVisible(true);

		TableLayout layout = new TableLayout();
		table.setLayout(layout);
	}

	private Table getTable()
	{
		Table table = fieldViewer.getTable();
		return table;
	}

	private void setLayout(Composite container, int columns, int verticalSpacing)
	{
		GridLayout layout = new GridLayout();
		layout.numColumns = columns;
		layout.verticalSpacing = verticalSpacing;
		container.setLayout(layout);
	}

	private void addTableNameField(Composite container)
	{
		Label label = new Label(container, SWT.NULL);
		label.setText("&Database Table Name: ");
		GridData gd = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_CENTER);
		gd.horizontalSpan = 1;
		label.setLayoutData(gd);

		tableName = new Label(container, SWT.SINGLE);
		tableName.setLayoutData(new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_CENTER));
	}

	private void addDisplayNameField(Composite container)
	{
		String labelText = "&Display Name:";
		displayName = createLabeledTextField(container, labelText);
		displayName.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				wizard.setDisplayName(displayName.getText());
				validate(displayName.getText(), null,
						numberOfPagesToDisplay != null ? numberOfPagesToDisplay.getText() : null,
						numberOfRecordsPerPage != null ? numberOfRecordsPerPage.getText() : null);
			}
		});

		displayName.addMouseListener(new MouseListener()
		{
			public void mouseDoubleClick(MouseEvent e)
			{
			}

			public void mouseDown(MouseEvent e)
			{
				validate(displayName.getText(), null,
						numberOfPagesToDisplay != null ? numberOfPagesToDisplay.getText() : null,
						numberOfRecordsPerPage != null ? numberOfRecordsPerPage.getText() : null);
				setErrorMessage(wizard.getErrorMessage());
				setPageComplete(wizard.isReady());
			}

			public void mouseUp(MouseEvent e)
			{
			}
		});
	}

	public String getDisplayName()
	{
		return displayName.getText();
	}

	private Text createLabeledTextField(Composite container, String labelText)
	{
		Label label = new Label(container, SWT.NULL);
		label.setText(labelText);
		GridData gd = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_CENTER);
		gd.horizontalSpan = 1;
		label.setLayoutData(gd);

		Text textField = new Text(container, SWT.BORDER | SWT.SINGLE);
		textField.setLayoutData(new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_CENTER));
		textField.setData(LABEL_KEY, label);
		return textField;
	}

	abstract class TableWizardEditingSupoort extends EditingSupport
	{
		protected CellEditor editor;
		protected TableFieldDescriptor descriptor;

		public TableWizardEditingSupoort(CellEditor editor)
		{
			super(fieldViewer);
			this.editor = editor;
		}

		@Override
		protected boolean canEdit(Object element)
		{
			descriptor = (TableFieldDescriptor) element;
			return true;
		}

		@Override
		protected CellEditor getCellEditor(Object element)
		{
			descriptor = (TableFieldDescriptor) element;
			return editor;
		}

		@Override
		protected Object getValue(Object element)
		{
			return getValue((TableFieldDescriptor) element);
		}

		protected abstract Object getValue(TableFieldDescriptor descriptor);

		@Override
		protected abstract void setValue(Object element, Object value);
	}

	private boolean validate(Object value, TableFieldDescriptor descriptor, String numberOfPages,
			String numberOfRecords)
	{
		wizard.validate(value, descriptor, numberOfPages, numberOfRecords);
		setErrorMessage(wizard.getErrorMessage());
		setPageComplete(wizard.isReady());

		return wizard.isReady();
	}

	private boolean validateAll()
	{
		wizard.validateAll();
		setErrorMessage(wizard.getErrorMessage());
		setPageComplete(wizard.isReady());

		return wizard.isReady();
	}

	public static String getPKLabel(int type)
	{
		switch (type)
		{
			case TableFieldDescriptor.DATABASE_GENERATED_PK:
				return "Database Generated";
			case TableFieldDescriptor.PK:
				return "User Input";
			case TableFieldDescriptor.MODEL_GENERATED_PK:
				return "Model Generated";
			default:
				return "";
		}
	}
}

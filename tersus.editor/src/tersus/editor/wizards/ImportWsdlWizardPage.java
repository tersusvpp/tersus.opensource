/************************************************************************************************
 * Copyright (c) 2003-2021 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.wizards;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import tersus.editor.EditorMessages;
import tersus.workbench.TersusWorkbench;

/**
 * @author Ofer Brandes
 * 
 */
public class ImportWsdlWizardPage extends WizardPage implements IWizardPage
{
	private Combo projectCombo;

	private Text wsdlFileText;

	private ImportWsdlWizard wizard;

	/**
	 * @param wizardName
	 * @param selection
	 */
	public ImportWsdlWizardPage(ImportWsdlWizard wizard)
	{
		super(ImportWsdlWizard.WIZARD_NAME);
		setTitle(EditorMessages.getString(ImportWsdlWizard.WIZARD_NAME));
		setDescription(EditorMessages.getString("Create a data structure model reprsenting the imported WSDL file."));

		this.wizard = wizard;
	}

	public void createControl(Composite parent)
	{
		Composite container = new Composite(parent, SWT.NULL);
		addComponents(container);
		initialize();
		setPageComplete(false);
		setControl(container);
	}

	protected void addComponents(final Composite container)
	{
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 3;
		layout.verticalSpacing = 9;

		Label label = new Label(container, SWT.NULL);
		label.setText("&Project:");

		projectCombo = new Combo(container, SWT.DROP_DOWN | SWT.READ_ONLY);
		GridData gd1 = new GridData(GridData.FILL_HORIZONTAL);
		gd1.horizontalSpan = 2;
		projectCombo.setLayoutData(gd1);

		Label wsdlFileLabel = new Label(container, SWT.WRAP);
		wsdlFileLabel.setText("&Wsdl URI/file:");
		wsdlFileText = new Text(container, SWT.BORDER | SWT.SINGLE);
		GridData gd2 = new GridData(GridData.FILL_HORIZONTAL);
		wsdlFileText.setLayoutData(gd2);
		wsdlFileText.addModifyListener(new ModifyListener()
		{

			public void modifyText(ModifyEvent e)
			{
				dialogChanged();
			}
		});
		Button browseButton = new Button(container, SWT.PUSH);
		browseButton.setText("Browse...");
		browseButton.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				FileDialog dialog = new FileDialog(container.getShell());
				wsdlFileText.setText(dialog.open());
			}
		});

	}

	private void dialogChanged()
	{
		getImportWizard().setSampleFilePath(wsdlFileText.getText());
		getImportWizard().validate();
		setErrorMessage(getImportWizard().getErrorMessage());
		setPageComplete(getImportWizard().isReady());
	}

	private void initialize()
	{
		initProjectComboBox();
	}

	protected void initProjectComboBox()
	{
		for (IProject project : TersusWorkbench.getWorkspace().getRoot().getProjects())
		{
			if (project.isOpen() && TersusWorkbench.isTersusProject(project))
			{
				projectCombo.add(project.getName());
				// setPageComplete(isPageComplete());
			}
		}

		if (wizard.getProject() != null)
		{
			projectCombo.setText(wizard.getProject().getName());
			dialogChanged();
		}
		else
			setPageComplete(false);

		projectCombo.addModifyListener(new ModifyListener()
		{
			public void modifyText(ModifyEvent e)
			{
				IProject project = TersusWorkbench.getWorkspace().getRoot().getProject(
						projectCombo.getText());
				wizard.setProject(project);
				
				dialogChanged();
				
				if (project != null)
					setPageComplete(true);
			}
		});
	}

	protected ImportWsdlWizard getImportWizard()
	{
		return (ImportWsdlWizard) getWizard();
	}
}

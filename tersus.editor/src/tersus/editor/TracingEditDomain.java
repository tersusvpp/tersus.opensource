/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor;

import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.ui.IEditorPart;

import tersus.editor.useractions.DoubleClick;
import tersus.editor.useractions.MouseDown;
import tersus.editor.useractions.MouseDrag;
import tersus.editor.useractions.MouseHover;
import tersus.editor.useractions.MouseUp;
import tersus.util.Trace;

/**
 * 
 */
public class TracingEditDomain extends DefaultEditDomain
{

	/**
	 * @param editorPart
	 */
	public TracingEditDomain(IEditorPart editorPart)
	{
		super(editorPart);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.EditDomain#mouseDoubleClick(org.eclipse.swt.events.MouseEvent, org.eclipse.gef.EditPartViewer)
	 */
	public void mouseDoubleClick(MouseEvent mouseEvent, EditPartViewer viewer)
	{
		if (Debug.USER_ACTIONS)
			Trace.push(new DoubleClick(mouseEvent));
		try
		{
			super.mouseDoubleClick(mouseEvent, viewer);
		}
		finally
		{
			if (Debug.USER_ACTIONS)
				Trace.pop();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.EditDomain#mouseDown(org.eclipse.swt.events.MouseEvent, org.eclipse.gef.EditPartViewer)
	 */
	public void mouseDown(MouseEvent mouseEvent, EditPartViewer viewer)
	{
		if (Debug.USER_ACTIONS)
			Trace.push(new MouseDown(mouseEvent));
		try
		{
			super.mouseDown(mouseEvent, viewer);
		}
		finally
		{
			if (Debug.USER_ACTIONS)
				Trace.pop();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.EditDomain#mouseDrag(org.eclipse.swt.events.MouseEvent, org.eclipse.gef.EditPartViewer)
	 */
	public void mouseDrag(MouseEvent mouseEvent, EditPartViewer viewer)
	{
		if (Debug.USER_ACTIONS)
			Trace.push(new MouseDrag(mouseEvent));
		try
		{
			super.mouseDrag(mouseEvent, viewer);
		}
		finally
		{
			if (Debug.USER_ACTIONS)
				Trace.pop();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.EditDomain#mouseHover(org.eclipse.swt.events.MouseEvent, org.eclipse.gef.EditPartViewer)
	 */
	public void mouseHover(MouseEvent mouseEvent, EditPartViewer viewer)
	{
		if (Debug.USER_ACTIONS)
			Trace.push(new MouseHover(mouseEvent));
		try
		{
			super.mouseHover(mouseEvent, viewer);
		}
		finally
		{
			if (Debug.USER_ACTIONS)
				Trace.pop();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.EditDomain#mouseMove(org.eclipse.swt.events.MouseEvent, org.eclipse.gef.EditPartViewer)
	 */
	public void mouseMove(MouseEvent mouseEvent, EditPartViewer viewer)
	{
		//Move events are VERY numerous, and don't seem to be interesting
		//		if (Debug.USER_ACTIONS)
		//			Trace.add(new MouseMove(mouseEvent));
		super.mouseMove(mouseEvent, viewer);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.EditDomain#mouseUp(org.eclipse.swt.events.MouseEvent, org.eclipse.gef.EditPartViewer)
	 */
	public void mouseUp(MouseEvent mouseEvent, EditPartViewer viewer)
	{
		if (Debug.USER_ACTIONS)
			Trace.push(new MouseUp(mouseEvent));
		try
		{
			super.mouseUp(mouseEvent, viewer);
		}
		finally
		{
			if (Debug.USER_ACTIONS)
				Trace.pop();
		}
	}

}

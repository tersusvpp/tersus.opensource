/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.IPersistableElement;

import tersus.model.ModelId;
import tersus.workbench.WorkspaceRepository;

/**
 * Identifies a 'physical' (using its ModelId and its location. The location is
 * currently the containing project (the same project can't contain 2 models
 * with the same id).
 * 
 * @author Youval Bronicki
 *  
 */
public class ConcreteModelIdentifier implements IEditorInput, IPersistableElement
{

    public String getFactoryId()
    {
        return ConcreteModelIdentifierFactory.FACTORY_ID;
    }
    public void saveState(IMemento memento)
    {
        ConcreteModelIdentifierFactory.save(this, memento);
    }
    private ModelId modelId;

    private IProject project;

    private int hash = 0;

    /**
     * @param project2
     * @param modelId2
     */
    public ConcreteModelIdentifier(IProject project, ModelId modelId)
    {
        this.project = project;
        this.modelId = modelId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.IEditorInput#exists()
     */
    public boolean exists()
    {
        WorkspaceRepository repository = null;
        repository = RepositoryManager.getRepositoryManager(project).getRepository();
        return repository.getModel(modelId, true) != null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.IEditorInput#getImageDescriptor()
     */
    public ImageDescriptor getImageDescriptor()
    {
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.IEditorInput#getName()
     */
    public String getName()
    {
        return modelId.getName();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.IEditorInput#getPersistable()
     */
    public IPersistableElement getPersistable()
    {
        return this;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.IEditorInput#getToolTipText()
     */
    public String getToolTipText()
    {
        return modelId.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
     */
    public Object getAdapter(Class adapter)
    {
		if (adapter == IProject.class)
			return getProject();
		else 
		    return null;
    }

    public ModelId getModelId()
    {
        return modelId;
    }

    public IProject getProject()
    {
        return project;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj)
    {
        if (obj instanceof ConcreteModelIdentifier)
            return ((ConcreteModelIdentifier) obj).project.equals(project)
                    && ((ConcreteModelIdentifier) obj).modelId.equals(modelId);
        else
            return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    public int hashCode()
    {
        if (hash == 0)
            hash = (project.getName() + "/" + modelId).hashCode();
        return hash;
    }
}
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor;

import org.eclipse.core.runtime.Platform;

/**
 * 
 */
public class Debug
{



	private static final String PLUGIN_ID = "tersus.editor";

	public static boolean DEBUG;
	public static boolean HANDLES;
	public static boolean PERSISTENCE;
	public static boolean SELECT;
	public static boolean LAYOUT;
	public static boolean REFRESH;
	public static boolean WARNING;
	public static boolean LINKS;
	public static boolean USER_ACTIONS;
	public static boolean DND;
	public static boolean PAINT;
	public static boolean PART_CREATION;
	public static boolean REQUEST_HANDLING;
	public static boolean PRINT_CALLER;
	public static boolean ZOOM;
	public static boolean CREATE;
	public static boolean OTHER;

	static {
		DEBUG = getBooleanDebugProperty("debug");
		if (DEBUG)
		{
			HANDLES = getBooleanDebugProperty("handles");
			SELECT = getBooleanDebugProperty("select");
			LAYOUT = getBooleanDebugProperty("layout");
			REFRESH = getBooleanDebugProperty("refresh");
			WARNING = getBooleanDebugProperty("warning");
			PAINT = getBooleanDebugProperty("paint");
			PART_CREATION = getBooleanDebugProperty("part_creation");
			REQUEST_HANDLING = getBooleanDebugProperty("request_handling");
			PRINT_CALLER = getBooleanDebugProperty("print_caller");
			DND = getBooleanDebugProperty("DND");
			USER_ACTIONS = getBooleanDebugProperty("user_actions");
			LINKS = getBooleanDebugProperty("links");
			ZOOM = getBooleanDebugProperty("zoom");
			CREATE = getBooleanDebugProperty("create");
			PERSISTENCE = getBooleanDebugProperty("persistence");
			OTHER = getBooleanDebugProperty("other");
		}
	}

	/**
	 * @param PAINT
	 * @param string
	 * @return
	 */
	private static boolean getBooleanDebugProperty(String propertyName)
	{

		String propertyValue = Platform.getDebugOption(PLUGIN_ID + "/" + propertyName);
		if (propertyValue == null)
			return false;
		else
			return Boolean.valueOf(propertyValue).booleanValue();
	}

}

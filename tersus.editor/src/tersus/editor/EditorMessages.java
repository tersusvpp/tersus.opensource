/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;


public class EditorMessages
{
	private static final String RESOURCE_BUNDLE = "tersus.editor.messages"; //$NON-NLS-1$
	private static ResourceBundle bundle = ResourceBundle.getBundle(RESOURCE_BUNDLE);
	

//	-------------------------------------
//	 Wizard Labels
//	-------------------------------------

    public static final String NewPackageWizard_PageTitle = getString("NewPackageWizard.pageTitle");
    public static final String NewPackageWizard_PageDescription = getString("NewPackageWizard.pageDescription");

    public static final String NewProjectWizard_PageTitle = getString("NewProjectWizard.pageTitle");
    public static final String NewProjectWizard_PageDescription = getString("NewProjectWizard.pageDescription");

	public static final String Create_From_XML_Error_Title = getString("CreateFromXMLErrorTitle");
	public static final String Create_From_XML_Error_Message = getString("CreateFromXMLErrorMessage");
	public static final String CreateFromXml_Title = getString("CreateFromXML.Title");
	public static final String CreateFromXml_importTask = getString("CreateFromXML.importTask");
	public static final String CreateFromXml_creatingFolder = getString("CreateFromXML.creatingFolder");
	public static final String CreateFromXml_cannotCopy = getString("CreateFromXML.cannotCopy");
	public static final String CreateFromXml_importProblems = getString("CreateFromXML.importProblems");
	public static final String CreateFromXml_openStreamError = getString("CreateFromXML.openStreamError");
	public static final String CreateFromXml_closeStreamError = getString("CreateFromXML.closeStreamError");
	public static final String CreateFromXml_coreImportError = getString("CreateFromXML.coreImportError");
	public static final String CreateFromXml_targetSameAsSourceError = getString("CreateFromXML.targetSameAsSourceError");
	public static final String CreateFromXml_emptyString = getString("CreateFromXML.emptyString");	
	public static final String CreateFromXml_SelectTargetProjectLabel = getString("CreateFromXML.SelectTargetProjectLabel");
	public static final String CreateFromXml_SelectTargetPackageLabel = getString("CreateFromXML.SelectTargetPackageLabel");
	public static final String CreateFromXml_SelectSaveInputLabel = getString("CreateFromXML.SelectSaveInputLabel");
	public static final String CreateFromXml_OptionsLabel = getString("CreateFromXML.OptionsLabel");
	public static final String CreateFromXml_SelectCopy = getString("CreateFromXML.SelectCopy");
	public static final String CreateFromXml_ChooseProjectDialog_title = getString("CreateFromXML.ChooseProjectDialog.title");
	public static final String CreateFromXml_ChooseProjectDialog_description = getString("CreateFromXML.ChooseProjectDialog.description");
	public static final String CreateFromXml_selectTypes = getString("CreateFromXML.selectTypes");
    public static final String ModelInUseDialog_Title = getString("ModelInUseDialog.title");
    public static final String ModelInUseDialog_Message_Key = "ModelInUseDialog.message";
    public static final String PackageInUseDialog_Title = getString("PackageInUseDialog.title");
    public static final String PackageInUseDialog_Message_Key = "PackageInUseDialog.message";
	public static final String CreateFromXml_selectAll = getString("CreateFromXML.selectAll");
	public static final String CreateFromXml_deselectAll = getString("CreateFromXML.deselectAll");
	public static final String CreateFromXml_FileImport_selectSourceTitle = getString("CreateFromXML.FileImport.selectSourceTitle");
	public static final String CreateFromXml_FileImport_selectSource = getString("CreateFromXML.FileImport.selectSource");
	public static final String CreateFromXml_FileImport_sourceEmpty = getString("CreateFromXML.FileImport.sourceEmpty");
	public static final String CreateFromXml_fileSystemTitle = getString("CreateFromXML.fileSystemTitle");
	public static final String CreateFromXml_FileImport_importFileSystem = getString("CreateFromXML.FileImport.importFileSystem");
	public static final String CreateFromXml_FileImport_overwriteExisting = getString("CreateFromXML.FileImport.overwriteExisting");
    public static final String Rename_Dialog_Title = getString("RenameDialog.title");
    public static final String Rename_Dialog_Message = getString("RenameDialog.message");
    public static final String Error_Package_Exists_Key = "Error.packageExists";
    public static final String Error_Project_Exists_Key = "Error.projectExists";
    public static final String Error_Project_Exists_In_File_System = "Error.projectExistsInFileSystem";
    public static final String Error_Model_Exists_Key = "Error.modelExists";
    public static final String Error_Package_Name_Not_Specified = getString("Error.packageNameNotSpecified");
    public static final String Error_Model_Name_Not_Specified = getString("Error.modelNameNotSpecified");
    public static final String Error_Invalid_Package_Name = getString("Error.invalidPackageName");
    public static final String Error_Invalid_Model_Name = getString("Error.invalidModelName");
    public static final String Error_Same_Name = getString("Error.sameName");
	public static final String CreateFromXml_FileImport_createComplete = getString("CreateFromXML.FileImport.createComplete");
	public static final String CreateFromXml_FileImport_createSelectedFolders = getString("CreateFromXML.FileImport.createSelectedFolders");
	public static final String CreateFromXml_browse = getString("CreateFromXML.browse");
	public static final String CreateFromXml_FileImport_invalidSource = getString("CreateFromXML.FileImport.invalidSource");
	public static final String CreateFromXml_FileImport_importProblems = getString("CreateFromXML.FileImport.importProblems");
	public static final String CreateFromXml_information = getString("CreateFromXML.information");
	public static final String CreateFromXml_FileImport_noneSelected = getString("CreateFromXML.FileImport.noneSelected");
	public static final String CreateFromXml_FileImport_fromDirectory = getString("CreateFromXML.FileImport.fromDirectory");
	public static final String CreateFromXml_filterSelections = getString("CreateFromXML.filterSelections");
	public static final String CreateFromXml_SelectFolderDialog_Title = getString("CreateFromXML.SelectFolderDialog.Title");
	public static final String CreateFromXml_SelectFolderDialog_Description = getString("CreateFromXML.SelectFolderDialog.Description");
	public static final String CreateFromXml_importTitle = getString("CreateFromXML.importTitle");
	public static final String CreateFromXml_defaultPackageRootLabel = getString("CreateFromXML.defaultPackageRootLabel");
	
	
//	---------------------------------------------
//	Palette
//	---------------------------------------------

	public static final String Palette_Trigger_Label = getString("Palette_Trigger_Label");
	public static final String Palette_Trigger_Description = getString("Palette_Trigger_Description");
	public static final String Palette_IO_Trigger_Label = getString("Palette_IO_Trigger_Label");
	public static final String Palette_IO_Trigger_Description = getString("Palette_IO_Trigger_Description");
	public static final String Palette_Exit_Label = getString("Palette_Exit_Label");
	public static final String Palette_Exit_Description = getString("Palette_Exit_Description");
	public static final String Palette_Error_Label = getString("Palette_Error_Label");
	public static final String Palette_Error_Description = getString("Palette_Error_Description");
	public static final String Palette_Input_Slot_Label = getString("Palette_Input_Slot_Label");
	public static final String Palette_Input_Slot_Description = getString("Palette_Input_Slot_Description");
	public static final String Slot_Drawer_Label = getString("Slot_Drawer_Label");
	public static final String ConnectionCommand_Label = getString("ConnectionCommand_Label");
	public static final String Palette_ConnectionCreationTool_Description =
		getString("Palette_ConnectionCreationTool_Description");
	public static final String Palette_ConnectionCreationTool_Label = getString("Palette_ConnectionCreationTool_Label");
	public static final String Tersus_Category_ControlGroup_Label = getString("Tersus_Category_ControlGroup_Label");


	
//	---------------------------------------------
//	 Commands
//	---------------------------------------------
	
	public static final String Remove_Element_Command_Label = getString("Remove_Element_Command_Label");
	public static final String ChangeRoleAction_Label = getString("ChangeRoleAction_Label");
	public static final String ChangeRoleAction_Tooltip = getString("ChangeRoleAction_Tooltip");
	
//	---------------------------------------------
//	 Project Template Wizard
//	---------------------------------------------

	public static final String WizardNewTemplateProject_windowTitle = getString("WizardNewTemplateProject.windowTitle");
	public static final String WizardNewTemplateProject_title = getString("WizardNewTemplateProject.title");
	public static final String WizardNewTemplateProject_description = getString("WizardNewTemplateProject.description");
	public static final String WizardNewTemplateProject_referenceTitle = getString("WizardNewTemplateProject.referenceTitle");
	public static final String WizardNewTemplateProject_referenceDescription = getString("WizardNewTemplateProject.referenceDescription");
	public static final String WizardNewTemplateProject_errorOpeningPage = getString("WizardNewTemplateProject.errorOpeningPage");
	public static final String WizardNewTemplateProject_errorOpeningWindow = getString("WizardNewTemplateProject.errorOpeningWindow");
	public static final String WizardNewTemplateProject_errorMessage = getString("WizardNewTemplateProject.errorMessage");
	public static final String WizardNewTemplateProject_internalError = getString("WizardNewTemplateProject.internalError");
	public static final String WizardNewTemplateProject_caseVariantExistsError = getString("WizardNewTemplateProject.caseVariantExistsError");
	public static final String WizardNewTemplateProject_perspSwitchTitle = getString("WizardNewTemplateProject.perspSwitchTitle");
	public static final String WizardNewTemplateProject_perspSwitchMessage = getString("WizardNewTemplateProject.perspSwitchMessage");
	public static final String WizardNewTemplateProject_pageTitle = getString("WizardNewTemplateProject.pageTitle");
	public static final String WizardNewTemplateProject_fileDescription = getString("WizardNewTemplateProject.fileDescription");
	public static final String WizardNewTemplateProject_folderDescription = getString("WizardNewTemplateProject.folderDescription");
	public static final String WizardNewTemplateProject_locationError = getString("WizardNewTemplateProject.locationError");
	public static final String WizardNewTemplateProject_defaultLocationError = getString("WizardNewTemplateProject.defaultLocationError");
	public static final String WizardNewTemplateProject_projectNameEmpty = getString("WizardNewTemplateProject.projectNameEmpty");
	public static final String WizardNewTemplateProject_projectLocationEmpty = getString("WizardNewTemplateProject.projectLocationEmpty");
	public static final String WizardNewTemplateProject_projectExistsMessage = getString("WizardNewTemplateProject.projectExistsMessage");
	public static final String WizardNewTemplateProject_projectLocationExistsMessage = getString("WizardNewTemplateProject.projectLocationExistsMessage");
	public static final String WizardNewTemplateProject_projectContentsLabel = getString("WizardNewTemplateProject.projectContentsLabel");
	public static final String WizardNewTemplateProject_projectContentsGroupLabel = getString("WizardNewTemplateProject.projectContentsGroupLabel");
	public static final String WizardNewTemplateProject_templateContentsGroupLabel = getString("WizardNewTemplateProject.templateContentsGroupLabel");
	public static final String WizardNewTemplateProject_useDefaultLabel = getString("WizardNewTemplateProject.useDefaultLabel");
	public static final String WizardNewTemplateProject_nameLabel = getString("WizardNewTemplateProject.nameLabel");
	public static final String WizardNewTemplateProject_directoryLabel = getString("WizardNewTemplateProject.directoryLabel");
	public static final String WizardNewTemplateProject_locationLabel = getString("WizardNewTemplateProject.locationLabel");
	public static final String WizardNewTemplateProject_browseLabel = getString("WizardNewTemplateProject.browseLabel");

	public static final String FILES_OUT_OF_SYNC = "The files listed below have been modified in the file system. You can save your models after refreshing the models folder and deleting conflicing files, but the changes made externally will be lost.";

// -----------------
// Actions
// ---------------
   public static final String SetSlotDataTypes_Label = getString("SetSlotDataTypes.Label");


	

	private EditorMessages()
	{
		// prevent instantiation of class
	}
	public static String format(String key, Object[] args)
	{
		return MessageFormat.format(getString(key), args);
	}

	public static String getString(String key)
	{
		try
		{
			return bundle.getString(key);
		}
		catch (MissingResourceException e)
		{
			return key;
		}
	}
 
    public static String format(String key, Object value)
    {
        return format (key, new Object[]{value});
    }
}

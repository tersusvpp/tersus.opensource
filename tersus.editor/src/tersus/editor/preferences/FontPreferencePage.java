/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.preferences;


import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FontFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import tersus.editor.*;
import tersus.editor.EditorPlugin;



public class FontPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage
{

	public FontPreferencePage()
	{
		super(GRID);
		setPreferenceStore(EditorPlugin.getDefault().getPreferenceStore());
		setDescription("Fonts for diagram objects");
	}


	public void createFieldEditors()
	{
		addField(new FontFieldEditor(Preferences.FLOW_FONT_LARGE, EditorMessages.getString(Preferences.FLOW_FONT_LARGE), getFieldEditorParent()));
		addField(new FontFieldEditor(Preferences.FLOW_FONT_MEDIUM, EditorMessages.getString(Preferences.FLOW_FONT_MEDIUM), getFieldEditorParent()));
		addField(new FontFieldEditor(Preferences.FLOW_FONT_SMALL, EditorMessages.getString(Preferences.FLOW_FONT_SMALL), getFieldEditorParent()));
		addField(new FontFieldEditor(Preferences.SLOT_LABEL_FONT, EditorMessages.getString(Preferences.SLOT_LABEL_FONT), getFieldEditorParent()));
	}

	public void init(IWorkbench workbench)
	{
	}

}
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.preferences;


import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import tersus.editor.EditorMessages;
import tersus.editor.EditorPlugin;


/**
 * A placeholder preference page (currently empty), added to allow 
 * the grouping of preference pages under a 'Tersus' category.
 *
 */

public class ValidationPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage
{

	public ValidationPreferencePage()
	{
		super(GRID);
		setPreferenceStore(EditorPlugin.getDefault().getPreferenceStore());
		setDescription("Validation Preferences");
	}


	public void createFieldEditors()
	{
		addField(new BooleanFieldEditor(Preferences.VALIDATE_ON_SAVE, EditorMessages.getString(Preferences.VALIDATE_ON_SAVE), getFieldEditorParent()));
		addField(new BooleanFieldEditor(Preferences.ALWAYS_NOTIFY_ON_VALIDATION_PROBLEMS, EditorMessages.getString(Preferences.ALWAYS_NOTIFY_ON_VALIDATION_PROBLEMS), getFieldEditorParent()));
		addField(new BooleanFieldEditor(Preferences.INCLUDE_UNUSED_MODELS, EditorMessages.getString(Preferences.INCLUDE_UNUSED_MODELS), getFieldEditorParent()));
	}

	public void init(IWorkbench workbench)
	{
	}

}
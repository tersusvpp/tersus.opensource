/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.preferences;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.RGB;

import tersus.editor.EditorPlugin;

public abstract class Preferences
{
    public static final String TRANSPARENT_IMAGE_BACKGROUND = "TRANSPARENT_IMAGE_BACKGROUND";
    public static final String AUTOMATICALLY_RESIZE_IMAGES = "AUTOMATICALLY_RESIZE_IMAGES";

    public static final String SYSTEM_COLOR_1 = "SYSTEM_COLOR_1";
    public static final String SYSTEM_COLOR_2 = "SYSTEM_COLOR_2";
    public static final String PROCESS_COLOR_1 = "PROCESS_COLOR_1";
    public static final String PROCESS_COLOR_2 = "PROCESS_COLOR_2";
    public static final String DATA_COLOR_1 = "DATA_COLOR_1";
    public static final String NOTE_COLOR = "NOTE_COLOR";
    public static final String VALIDATE_ON_SAVE = "VALIDATE_ON_SAVE";
    public static final String ALWAYS_NOTIFY_ON_VALIDATION_PROBLEMS = "ALWAYS_NOTIFY_ON_VALIDATION_PROBLEMS";
    public static final String INCLUDE_UNUSED_MODELS = "INCLUDE_UNUSED_MODELS";
    public static final String DATA_COLOR_2 = "DATA_COLOR_2";
    public static final String MANDATORY_TRIGGER_COLOR = "MANDATORY_TRIGGER_COLOR";
    public static final String NON_MANDATORY_TRIGGER_COLOR = "NON_MANDATORY_TRIGGER_COLOR";
    public static final String EXIT_COLOR = "EXIT_COLOR";
    public static final String INPUT_COLOR = "INPUT_COLOR";
    public static final String ERROR_COLOR = "ERROR_COLOR";
    public static final String HIGHLIGHT_COLOR = "HIGHLIGHT_COLOR";

    public static final String SUB_DATA_PREFERRED_OFFSET = "SUB_DATA_PREFERRED_OFFSET";
    public static final String SUB_DATA_MIN_OFFSET = "SUB_DATA_MIN_OFFSET";

    public static final String MAX_SLOT_SIZE = "MAX_SLOT_SIZE";
    public static final String MIN_SLOT_SIZE = "MIN_SLOT_SIZE";

    public static final String MAX_ARROW_HEAD_SIZE = "MAX_ARROW_HEAD_SIZE";

    public static final String FLOW_FONT_LARGE = "FLOW_FONT_LARGE";
    public static final String FLOW_FONT_MEDIUM = "FLOW_FONT_MEDIUM";
    public static final String FLOW_FONT_SMALL = "FLOW_FONT_SMALL";

    public static final String SLOT_LABEL_FONT = "SLOT_LABEL_FONT";

    public static final String ZOOM_ANIMATION_STEPS = "ZOOM_ANIMATION_STEPS";
    public static final String ZOOM_ANIMATION_DURATION = "ZOOM_ANIMATION_DURATION";
    public static final String ZOOM_MARGIM = "OBJECT_BORDER_SIZE_WHEN_ZOOM";

    public static final String HOSTING_UPLOAD_URL = "HOSTING_UPLOAD_URL";

    //TODO: move defaults to an external file
    static final String SYSTEM_COLOR_1_DEFAULT = "255,255,221";
    static final String SYSTEM_COLOR_2_DEFAULT = "255,255,200";
    static final String PROCESS_COLOR_1_DEFAULT = "213,213,255";
    static final String PROCESS_COLOR_2_DEFAULT = "236,235,254";
    static final String DATA_COLOR_1_DEFAULT = "201,233,171";
    static final String DATA_COLOR_2_DEFAULT = "222,241,205";
    static final String MANDATORY_TRIGGER_COLOR_DEFAULT = "50,255,50";
    static final String NON_MANDATORY_TRIGGER_COLOR_DEFAULT = "210,255,60";
    static final String EXIT_COLOR_DEFAULT = "192,192,192";
    static final String INPUT_COLOR_DEFAULT = "192,192,192";
    static final String ERROR_COLOR_DEFAULT = "255,0,0";
    static final String HIGHLIGHT_COLOR_DEFAULT = "255,106,34";
    static final String NOTE_COLOR_DEFAULT = "255,255,225";

    public static String TRANSPARENT_IMAGE_BACKGROUND_DEFAULT = "false";
    public static String AUTOMATICALLY_RESIZE_IMAGES_DEFAULT = "false";
    public static String VALIDATE_ON_SAVE_DEFAULT = "true";
    public static String ALWAYS_NOTIFY_ON_VALIDATION_PROBLEMS_DEFAULT = "true";
    public static String INCLUDE_UNUSED_MODELS_DEFAULT = "false";
    static final int MAX_SLOT_SIZE_DEFAULT = 8;
    static final int MIN_SLOT_SIZE_DEFAULT = 4;
    static final int MAX_ARROW_HEAD_SIZE_DEFAULT = 6;

    static final int ZOOM_ANIMATION_STEPS_DEFAULT = 10;
    static final int ZOOM_ANIMATION_DURATION_DEFAULT = 250;
    static final int ZOOM_MARGIM_DEFAULT = 5;

    static final int SUB_DATA_PREFERRED_OFFSET_DEFAULT = 5;
    static final int SUB_DATA_MIN_OFFSET_DEFAULT = 3;

    public static final String HOSTING_UPLOAD_URL_DEFAULT = "http://hosting.tersus.com/Upload";
    public static final String HELP_URL = "http://infocenter.tersus.com/index.jsp?topic=/tersus.help/html/";

    // Palette preferences
    public static final String PALETTE_DOCK_LOCATION = "Dock Location";
    public static final String PALETTE_WIDTH = "Palette Size";
    public static final String PALETTE_STATE = "Palette State";
    private static final int PALETTE_WIDTH_DEFAULT = 150;
    private static final int PALETTE_STATE_DEFAULT = 4;

    //  Browser
    
    /*
     * Removed Tersus-Specific browser settings on 2009-06-07 (planning to remove commented code in a few weeks if we don't see problems)
     
    public static final String BROWSER_PATH = "BROWSER_PATH";
    public static final String DEFUALT_BROWSER_PATH = "C:/Program Files/Internet Explorer/iexplore.exe";
*/
    /**
     * @param store
     */
    public static void initializeDefaultPreferences(IPreferenceStore store)
    {
        store.setDefault(SYSTEM_COLOR_1, SYSTEM_COLOR_1_DEFAULT);
        store.setDefault(SYSTEM_COLOR_2, SYSTEM_COLOR_2_DEFAULT);
        store.setDefault(PROCESS_COLOR_1, PROCESS_COLOR_1_DEFAULT);
        store.setDefault(PROCESS_COLOR_2, PROCESS_COLOR_2_DEFAULT);
        store.setDefault(DATA_COLOR_1, DATA_COLOR_1_DEFAULT);
        store.setDefault(DATA_COLOR_2, DATA_COLOR_2_DEFAULT);
        store.setDefault(MANDATORY_TRIGGER_COLOR, MANDATORY_TRIGGER_COLOR_DEFAULT);
        store.setDefault(NON_MANDATORY_TRIGGER_COLOR, NON_MANDATORY_TRIGGER_COLOR_DEFAULT);
        store.setDefault(EXIT_COLOR, EXIT_COLOR_DEFAULT);
        store.setDefault(INPUT_COLOR, INPUT_COLOR_DEFAULT);
        store.setDefault(ERROR_COLOR, ERROR_COLOR_DEFAULT);
        store.setDefault(HIGHLIGHT_COLOR, HIGHLIGHT_COLOR_DEFAULT);
        store.setDefault(NOTE_COLOR, NOTE_COLOR_DEFAULT);

        store.setDefault(MAX_SLOT_SIZE, MAX_SLOT_SIZE_DEFAULT);
        store.setDefault(MIN_SLOT_SIZE, MIN_SLOT_SIZE_DEFAULT);
        store.setDefault(MAX_ARROW_HEAD_SIZE, MAX_ARROW_HEAD_SIZE_DEFAULT);

        store.setDefault(ZOOM_ANIMATION_STEPS, ZOOM_ANIMATION_STEPS_DEFAULT);
        store.setDefault(ZOOM_ANIMATION_DURATION, ZOOM_ANIMATION_DURATION_DEFAULT);
        store.setDefault(ZOOM_MARGIM, ZOOM_MARGIM_DEFAULT);

        store.setDefault(SUB_DATA_MIN_OFFSET, SUB_DATA_MIN_OFFSET_DEFAULT);
        store.setDefault(SUB_DATA_PREFERRED_OFFSET, SUB_DATA_PREFERRED_OFFSET_DEFAULT);

        store.setDefault(TRANSPARENT_IMAGE_BACKGROUND, TRANSPARENT_IMAGE_BACKGROUND_DEFAULT);
        store.setDefault(AUTOMATICALLY_RESIZE_IMAGES, AUTOMATICALLY_RESIZE_IMAGES_DEFAULT);
        store.setDefault(VALIDATE_ON_SAVE, VALIDATE_ON_SAVE_DEFAULT);
        store.setDefault(ALWAYS_NOTIFY_ON_VALIDATION_PROBLEMS, ALWAYS_NOTIFY_ON_VALIDATION_PROBLEMS_DEFAULT);
        store.setDefault(INCLUDE_UNUSED_MODELS, INCLUDE_UNUSED_MODELS_DEFAULT);

        store.setDefault(HOSTING_UPLOAD_URL, HOSTING_UPLOAD_URL_DEFAULT);
        store.setDefault(PALETTE_WIDTH, PALETTE_WIDTH_DEFAULT);
        store.setDefault(PALETTE_STATE, PALETTE_STATE_DEFAULT);

        String fontName = "MS Sans Serif";
        String smallFont = "Small Fonts";
        PreferenceConverter.setDefault(store, FLOW_FONT_LARGE,
                new FontData[] { new FontData(fontName, 16, SWT.NORMAL) });
        PreferenceConverter.setDefault(store, FLOW_FONT_MEDIUM,
                new FontData[] { new FontData(fontName, 12, SWT.NORMAL) });
        PreferenceConverter
                .setDefault(store, FLOW_FONT_SMALL, new FontData[] { new FontData(fontName, 8, SWT.NORMAL) });
        PreferenceConverter.setDefault(store, SLOT_LABEL_FONT,
                new FontData[] { new FontData(smallFont, 7, SWT.NORMAL) });
/*
 *         Removed Tersus-Specific browser settings on 2009-06-07 (planning to remove commented code in a few weeks if we don't see problems)
 
        store.setDefault(BROWSER_PATH, DEFUALT_BROWSER_PATH);
*/
    }

    public static Color getColor(String name)
    {
        EditorPlugin plugin = EditorPlugin.getDefault();
        RGB rgb = PreferenceConverter.getColor(plugin.getPreferenceStore(), name);
        Color color = plugin.getColor(rgb);
        return color;
    }

    public static Font getFont(String name)
    {
        EditorPlugin plugin = EditorPlugin.getDefault();
        FontData fd[] = PreferenceConverter.getFontDataArray(plugin.getPreferenceStore(), name);
        Font font = plugin.getFont(fd);
        return font;
    }

    public static int getInt(String name)
    {
        EditorPlugin plugin = EditorPlugin.getDefault();
        int value = plugin.getPreferenceStore().getInt(name);
        return value;
    }

    public static String getString(String name)
    {
        EditorPlugin plugin = EditorPlugin.getDefault();
        String value = plugin.getPreferenceStore().getString(name);
        return value;
    }

    public static boolean getBoolean(String name)
    {
        EditorPlugin plugin = EditorPlugin.getDefault();
        boolean value = plugin.getPreferenceStore().getBoolean(name);
        return value;
    }

    public static void setValue(String name, int value)
    {
        EditorPlugin plugin = EditorPlugin.getDefault();
        plugin.getPreferenceStore().setValue(name, value);
    }

    public static void setValue(String name, String value)
    {
        EditorPlugin plugin = EditorPlugin.getDefault();
        plugin.getPreferenceStore().setValue(name, value);
    }

}

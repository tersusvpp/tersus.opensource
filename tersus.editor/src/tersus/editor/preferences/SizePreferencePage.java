/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.preferences;


import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import tersus.editor.*;
import tersus.editor.EditorPlugin;



public class SizePreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage
{

	public SizePreferencePage()
	{
		super(GRID);
		setPreferenceStore(EditorPlugin.getDefault().getPreferenceStore());
		setDescription("Sizes for diagram objects");
	}


	public void createFieldEditors()
	{
		addField(new IntegerFieldEditor(Preferences.MAX_SLOT_SIZE, EditorMessages.getString("MAX_SLOT_SIZE"), getFieldEditorParent()));
		addField(new IntegerFieldEditor(Preferences.MIN_SLOT_SIZE, EditorMessages.getString("MIN_SLOT_SIZE"), getFieldEditorParent()));
		addField(new IntegerFieldEditor(Preferences.MAX_ARROW_HEAD_SIZE, EditorMessages.getString("MAX_ARROW_HEAD_SIZE"), getFieldEditorParent()));
	}

	public void init(IWorkbench workbench)
	{
	}

}
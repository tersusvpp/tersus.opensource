/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ColorFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import tersus.editor.*;
import tersus.editor.EditorPlugin;

/**
 * This class represents a preference page that
 * is contributed to the Preferences dialog. By 
 * subclassing <samp>FieldEditorPreferencePage</samp>, we
 * can use the field support built into JFace that allows
 * us to create a page that is small and knows how to 
 * save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They
 * are stored in the preference store that belongs to
 * the main plug-in class. That way, preferences can
 * be accessed directly via the preference store.
 */

public class ColorPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage
{

	public ColorPreferencePage()
	{
		super(GRID);
		setPreferenceStore(EditorPlugin.getDefault().getPreferenceStore());
		setDescription("Colors for diagram objects");
	}

	/**
	 * Creates the field editors. Field editors are abstractions of
	 * the common GUI blocks needed to manipulate various types
	 * of preferences. Each field editor knows how to save and
	 * restore itself.
	 */

	public void createFieldEditors()
	{
		addField(
			new ColorFieldEditor(
				Preferences.SYSTEM_COLOR_1,
				EditorMessages.getString("SYSTEM_COLOR_1"),
				getFieldEditorParent()));
		addField(
			new ColorFieldEditor(
				Preferences.SYSTEM_COLOR_2,
				EditorMessages.getString("SYSTEM_COLOR_2"),
				getFieldEditorParent()));
		addField(
			new ColorFieldEditor(
				Preferences.PROCESS_COLOR_1,
				EditorMessages.getString("PROCESS_COLOR_1"),
				getFieldEditorParent()));
		addField(
			new ColorFieldEditor(
				Preferences.PROCESS_COLOR_2,
				EditorMessages.getString("PROCESS_COLOR_2"),
				getFieldEditorParent()));
		addField(
			new ColorFieldEditor(
				Preferences.DATA_COLOR_1,
				EditorMessages.getString("DATA_COLOR_1"),
				getFieldEditorParent()));
		addField(
			new ColorFieldEditor(
				Preferences.DATA_COLOR_2,
				EditorMessages.getString("DATA_COLOR_2"),
				getFieldEditorParent()));
		addField(
			new ColorFieldEditor(
				Preferences.MANDATORY_TRIGGER_COLOR,
				EditorMessages.getString("MANDATORY_TRIGGER_COLOR"),
				getFieldEditorParent()));
        addField(
                new ColorFieldEditor(
                    Preferences.NON_MANDATORY_TRIGGER_COLOR,
                    EditorMessages.getString("NON_MANDATORY_TRIGGER_COLOR"),
                    getFieldEditorParent()));
		addField(
			new ColorFieldEditor(
				Preferences.EXIT_COLOR,
				EditorMessages.getString("EXIT_COLOR"),
				getFieldEditorParent()));
		addField(
			new ColorFieldEditor(
				Preferences.INPUT_COLOR,
				EditorMessages.getString("INPUT_COLOR"),
				getFieldEditorParent()));
		addField(
			new ColorFieldEditor(
				Preferences.ERROR_COLOR,
				EditorMessages.getString("ERROR_COLOR"),
				getFieldEditorParent()));
		addField(
			new ColorFieldEditor(
				Preferences.HIGHLIGHT_COLOR,
				EditorMessages.getString("HIGHLIGHT_COLOR"),
				getFieldEditorParent()));
				
		addField(
				new ColorFieldEditor(
					Preferences.NOTE_COLOR,
					EditorMessages.getString("NOTE_COLOR"),
					getFieldEditorParent()));
		addField(
			new BooleanFieldEditor(
				Preferences.TRANSPARENT_IMAGE_BACKGROUND,
				EditorMessages.getString("TRANSPARENT_IMAGE_BACKGROUND"),
				getFieldEditorParent()));
	}

	public void init(IWorkbench workbench)
	{
	}
	/* (non-Javadoc)
	 * @see org.eclipse.jface.preference.PreferencePage#performDefaults()
	 */
	protected void performDefaults()
	{
		// TODO Review this auto-generated method stub
		super.performDefaults();
	}

}
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor.preferences;


import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import tersus.editor.EditorMessages;
import tersus.editor.EditorPlugin;


/**
 * A placeholder preference page (currently empty), added to allow 
 * the grouping of preference pages under a 'Tersus' category.
 *
 */

public class GeneralPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage
{

	public GeneralPreferencePage()
	{
		super(GRID);
		setPreferenceStore(EditorPlugin.getDefault().getPreferenceStore());
		setDescription("General Preferences");
	}


	public void createFieldEditors()
	{
		IntegerFieldEditor maxSpeedControl = new IntegerFieldEditor(Preferences.ZOOM_ANIMATION_STEPS, EditorMessages.getString(Preferences.ZOOM_ANIMATION_STEPS), getFieldEditorParent());
		maxSpeedControl.setValidRange(1,50);
		addField(maxSpeedControl);
        IntegerFieldEditor animationDurationControl = new IntegerFieldEditor(Preferences.ZOOM_ANIMATION_DURATION, EditorMessages.getString(Preferences.ZOOM_ANIMATION_DURATION), getFieldEditorParent());
        animationDurationControl.setValidRange(10,10000);
        addField(animationDurationControl);
		IntegerFieldEditor borderSizeWhenZoom = new IntegerFieldEditor(Preferences.ZOOM_MARGIM, EditorMessages.getString(Preferences.ZOOM_MARGIM), getFieldEditorParent());
		borderSizeWhenZoom.setValidRange(0,15);
		addField(borderSizeWhenZoom);
		addField(new BooleanFieldEditor(Preferences.AUTOMATICALLY_RESIZE_IMAGES, EditorMessages.getString(Preferences.AUTOMATICALLY_RESIZE_IMAGES), getFieldEditorParent()));
/*
 * 			Removed Tersus-Specific browser settings on 2009-06-07 (planning to remove commented code in a few weeks if we don't see problems)
 * 
         addField(
                new FileFieldEditor(
                    Preferences.BROWSER_PATH,
                    "Browser",
                    getFieldEditorParent()));
		*/
		StringFieldEditor uploadURLField = new StringFieldEditor(Preferences.HOSTING_UPLOAD_URL, EditorMessages.getString(Preferences.HOSTING_UPLOAD_URL), getFieldEditorParent());
		uploadURLField.setValidateStrategy(StringFieldEditor.VALIDATE_ON_KEY_STROKE);
		addField(uploadURLField);
	}

	public void init(IWorkbench workbench)
	{
	}

}
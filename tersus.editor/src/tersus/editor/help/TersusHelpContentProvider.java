package tersus.editor.help;

import java.util.List;

import org.eclipse.help.HelpSystem;
import org.eclipse.help.IContext;
import org.eclipse.help.IContextProvider;

import tersus.editor.TersusEditor;
import tersus.editor.editparts.TersusEditPart;
import tersus.model.Model;

public class TersusHelpContentProvider implements IContextProvider
{
	private final TersusEditor editor;

	public TersusHelpContentProvider(TersusEditor editor)
	{
		this.editor = editor;
	}

	public IContext getContext(Object target)
	{
		List<?> selectedEditParts = editor.getViewer().getSelectedEditParts();
		
		if (selectedEditParts.size() > 0)
		{
			TersusEditPart firstPart = (TersusEditPart) selectedEditParts.get(0);
			
			if (firstPart.getTersusModel() instanceof Model)
			{
				Model selectedModel = firstPart.getTersusModel();
				String pluginId = getPluginId(selectedModel);
				return HelpSystem.getContext("tersus.help.".concat(pluginId));
			}
		}
		return HelpSystem.getContext("tersus.help.main_help");
	}

	public int getContextChangeMask()
	{
		return IContextProvider.SELECTION;
	}

	public String getSearchExpression(Object target)
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	private String getPluginId(Model selectedModel)
	{
		String pluginString = selectedModel.getPlugin();
		
		int index = pluginString.lastIndexOf('/');
		if (index != -1)
		{
			String pluginName = pluginString.subSequence(index + 1, pluginString.length()).toString();
			pluginString = pluginString.subSequence(0, index - 1).toString();
			index = pluginString.lastIndexOf('/');
			
			if (index !=  -1)
			{
				String categoryName = pluginString.subSequence(index + 1, pluginString.length()).toString();
				String pluginId = categoryName.concat("_" + pluginName);
				
				return pluginId;
			}
		}
		
		return null;
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor;

import org.eclipse.gef.ui.palette.DefaultPaletteViewerPreferences;
import org.eclipse.gef.ui.palette.PaletteViewerPreferences;
import org.eclipse.jface.preference.IPreferenceStore;

/**
 * @author Youval Bronicki
 *  
 */
public class TersusPaletteViewerPreferences extends
        DefaultPaletteViewerPreferences implements PaletteViewerPreferences
{

    private static IPreferenceStore tersusPreferenceStore = EditorPlugin.getDefault().getPreferenceStore();
    /**
     *  
     */
    public TersusPaletteViewerPreferences()
    {
        super(tersusPreferenceStore);
        tersusPreferenceStore.setDefault(PREFERENCE_COLUMNS_ICON_SIZE, false);
        tersusPreferenceStore.setDefault(PREFERENCE_ICONS_ICON_SIZE, false);
        tersusPreferenceStore.setDefault(PREFERENCE_LIST_ICON_SIZE, false);
        tersusPreferenceStore.setDefault(PREFERENCE_LAYOUT, LAYOUT_ICONS);
        tersusPreferenceStore.setDefault(PREFERENCE_AUTO_COLLAPSE,
                COLLAPSE_ALWAYS);
    }

}
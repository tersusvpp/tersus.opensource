/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor;

import org.eclipse.jface.resource.ImageDescriptor;

/**
 * @author Youval Bronicki
 *
 */
public interface Images
{
    ImageDescriptor TRIGGER16 = ImageDescriptor.createFromFile(TersusEditor.class,
    "icons/trigger.16.gif");
    ImageDescriptor EXIT16 = ImageDescriptor.createFromFile(TersusEditor.class,
    "icons/exit.16.gif");
    ImageDescriptor ERROR16 = ImageDescriptor.createFromFile(TersusEditor.class,
    "icons/error.16.gif");

}

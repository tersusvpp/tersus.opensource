package tersus.editor;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.dnd.AbstractTransferDropTargetListener;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;

import tersus.ProjectStructure;
import tersus.editor.editparts.FlowEditPart;
import tersus.editor.editparts.SubFlowEditPart;
import tersus.model.BuiltinModels;
import tersus.model.FlowModel;
import tersus.model.MetaProperty;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Path;
import tersus.model.Role;
import tersus.model.commands.MultiStepCommand;
import tersus.util.FileUtils;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * Handles dropping of files into the editor. Note that this DropTargetListener circumvents the
 * whole Request + EditPolicy mechanism by overriding updateTargetEditPart() and handleDrop(). We do
 * this because we plan to launch a wizard rather directly modify the model.
 * 
 * @author youval
 * 
 */
public class FileDropTargetListener extends AbstractTransferDropTargetListener
{

	@Override
	protected void updateTargetEditPart()
	{
		setTargetEditPart(getViewer().findObjectAt(getDropLocation()));
	}

	private TersusEditor editor;

	public FileDropTargetListener(TersusEditor editor, EditPartViewer viewer)
	{
		super(viewer, FileTransfer.getInstance());
		this.editor = editor;
	}

	public FileDropTargetListener(EditPartViewer viewer, Transfer xfer)
	{
		super(viewer, xfer);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void updateTargetRequest()
	{
		DropTargetEvent e = getCurrentEvent();
		if (validateEditPart())
		{
			for (TransferData x : e.dataTypes)
			{
				if (FileTransfer.getInstance().isSupportedType(x))
				{
					e.detail = DND.DROP_COPY;
				}

			}
		}

	}

	private boolean validateEditPart()
	{
		if (!(getTargetEditPart() instanceof FlowEditPart))
			return false;
		if (((FlowEditPart) getTargetEditPart()).getFlowModel().isReadOnly())
			return false;
		return true;

	}

	@Override
	protected void handleDrop()
	{
		updateTargetEditPart();
		if (getCurrentEvent().data instanceof String[])
		{
			String[] files = (String[]) getCurrentEvent().data;
			getCurrentEvent().detail = DND.DROP_NONE;
			for (String file : files)
			{
				if (files.length == 1)
				{
					if (importFile(file))
						getCurrentEvent().detail = DND.DROP_COPY;
						
				}
			}

		}
		// Put the focus on the editor after a successful drop
		getViewer().getControl().getDisplay().asyncExec(new Runnable()
		{

			public void run()
			{
				editor.getEditorSite().getPage().activate(editor);
			}
		});

	}

	private boolean importFile(final String file)
	{
		final FlowModel parentModel = ((SubFlowEditPart) getTargetEditPart()).getFlowModel();

		final File sourceFile = new File(file);
		MultiStepCommand command = null;
		if (isImageFile(sourceFile) || isSoundFile(sourceFile))
			command = getImportCommand(parentModel, sourceFile);

		if (command != null)
			getViewer().getEditDomain().getCommandStack().execute(command);
		
		return command != null;
	}

	private MultiStepCommand getImportCommand(final FlowModel parentModel, final File sourceFile)
	{

		final IFolder folder;
		IProject project = editor.getRepository().getProject();
		final IFolder webFolder = project.getFolder(ProjectStructure.WEB);
		final IFile targetFile;
		String relativePath = null;
		try
		{
			if (FileUtils.contains(webFolder.getLocation().toFile(), sourceFile))
			{
				String webFolderPath = webFolder.getLocation().toFile().getCanonicalPath();
				relativePath = sourceFile.getCanonicalPath().substring(webFolderPath.length() + 1);
				targetFile = webFolder.getFile(new org.eclipse.core.runtime.Path(relativePath));
				folder = (IFolder) targetFile.getParent();
			}
			else
			{
				if (isImageFile(sourceFile))
					folder = webFolder.getFolder(ProjectStructure.IMAGES);
				else
					folder = webFolder.getFolder(ProjectStructure.SOUNDS);
				targetFile = folder.getFile(sourceFile.getName());
				relativePath = targetFile.getFullPath().toString()
						.substring(webFolder.getFullPath().toString().length() + 1);
				if (targetFile.exists())
					return null; // Can't import
			}
		}
		catch (IOException e)
		{
			TersusWorkbench.log(e);
			return null;
		}
		final boolean createFile = !targetFile.exists();
		final boolean createFolder = !folder.exists();

		final String url = relativePath;
		MultiStepCommand command = new MultiStepCommand(
				(WorkspaceRepository) parentModel.getRepository(), "Import File")
		{

			@Override
			public void redo()
			{
				if (createFile)
					copyFile();
				super.redo();
			}

			protected void run()
			{
				if (createFile)
					copyFile();
				if (isImageFile(sourceFile))
				{
					String iconProperty = null;
					if (parentModel.getElement(Role.get("<Icon>")) != null)
						iconProperty = "html.icon";
					else if (parentModel.getElement(Role.get("<src>")) != null)
						iconProperty = "html.src";
					if (iconProperty != null)
					{
						setProperty(parentModel, iconProperty, url);
					}
					else
					{
						ModelId newModelId = repository.getNewModelId(parentModel.getPackageId(),
								"Image");
						Model template = repository.getModel(BuiltinModels.IMAGE_TEMPLATE_ID, true);
						createModelFromPrototype(template, newModelId);
						Model imageModel = repository.getModel(newModelId, true);
						imageModel.addMetaProperty(new MetaProperty("html.src", String.class, true,
								false, true));
						imageModel.setProperty("html.src", url);

						addSubModel(parentModel, imageModel, imageModel.getName(), 0.25, 0.25);
					}
				}
				else
				{
					ModelId topActionId = repository.getNewModelId(parentModel.getPackageId(),
							"Play " + sourceFile.getName());
					createModelFromPrototype(
							repository.getModel(BuiltinModels.ACTION_TEMPLATE_ID, true),
							topActionId);
					FlowModel topAction = (FlowModel) repository.getModel(topActionId, true);
					ModelId playActionId = repository.getNewModelId(parentModel.getPackageId(),
							"Play Sound");
					createModelFromPrototype(
							repository.getModel(BuiltinModels.PLAY_SOUND_ID, true), playActionId);
					addSubModel(topAction, repository.getModel(playActionId, true),
							playActionId.getName(), 0.5, 0.3);
					ModelId soundURLId = repository.getNewModelId(parentModel.getPackageId(),
							"SOUND_URL");
					addConstantModel(soundURLId, BuiltinModels.TEXT_CONSTANT_ID, url);
					addSubModel(topAction, repository.getModel(soundURLId, true), "SOUND_URL", 0.2,
							0.5, 0.3, 0.1);
					addLink(topAction, new Path("SOUND_URL"), new Path(playActionId.getName()
							+ "/<URL>"));
					addSubModel(parentModel, topAction, topAction.getName(), 0.4, 0.4);
				}
			}

			private void copyFile()
			{
				FileInputStream source = null;
				try
				{
					if (createFolder)
						folder.create(true, true, null);
					source = new FileInputStream(sourceFile);
					targetFile.create(source, true, null);
					source.close();
					source = null;
				}

				catch (Exception e)
				{
					TersusWorkbench.log(e);
					return;
				}
				finally
				{
					FileUtils.forceClose(source);
				}
			}

			@Override
			public void undo()
			{
				super.undo();
				if (createFile)
				{
					try
					{
						targetFile.delete(true, null);
						if (createFolder)
							folder.delete(true, null);
					}
					catch (Exception e)
					{
						TersusWorkbench.log(e);
					}
				}
			}
		};
		return command;
	}

	private boolean isImageFile(File sourceFile)
	{
		String name = sourceFile.getName();
		String extension = name.substring(name.lastIndexOf(".") + 1).toLowerCase();
		return extension.equals("jpg") || extension.equals("jpeg") || extension.equals("png")
				|| extension.equals("gif");
	}

	private boolean isSoundFile(File sourceFile)
	{
		String name = sourceFile.getName();
		String extension = name.substring(name.lastIndexOf(".") + 1).toLowerCase();
		return extension.equals("mp3") || extension.equals("ogg") || extension.equals("wav");
	}

}

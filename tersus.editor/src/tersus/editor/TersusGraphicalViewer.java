/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor;

import java.util.Collection;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.dnd.DelegatingDropAdapter;
import org.eclipse.gef.ui.parts.DomainEventDispatcher;
import org.eclipse.gef.ui.parts.ScrollingGraphicalViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.widgets.Event;

import tersus.InternalErrorException;
import tersus.editor.editparts.TersusEditPart;
import tersus.util.Trace;
import tersus.workbench.WorkspaceRepository;

/**
 * 
 */
public class TersusGraphicalViewer extends ScrollingGraphicalViewer
{
    private TersusEditor editor;
	/**
	 * 
	 */
	public TersusGraphicalViewer()
	{
		super();
	}

	public DelegatingDropAdapter getDropAdapter()
	{
		return getDelegatingDropAdapter();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.EditPartViewer#reveal(org.eclipse.gef.EditPart)
	 */
	public void reveal(EditPart part)
	{
	    getEditor().activate();
		// TODO Reveal the part by bringing it to front?
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.EditPartViewer#findObjectAtExcluding(org.eclipse.draw2d.geometry.Point, java.util.Collection, org.eclipse.gef.EditPartViewer.Conditional)
	 */
	public EditPart findObjectAtExcluding(Point pt, Collection exclude, Conditional condition)
	{
		boolean tracePushed = false;
		if (Debug.SELECT)
		{
			Trace.push("findObjectAtExcluding pt=" + pt + " excludes=" + exclude + " condition=" + condition);
			tracePushed = true;
		}
		try
		{
			EditPart part = super.findObjectAtExcluding(pt, exclude, condition);
			if (Debug.SELECT)
				Trace.add("EditPart:" + part);
			return part;
		}
		finally
		{
			if (tracePushed)
				Trace.pop();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.ui.parts.GraphicalViewerImpl#getEventDispatcher()
	 */

	public void sendMouseEvent(Event e)
	{
		DomainEventDispatcher dispatcher = getEventDispatcher(); // Found no alternative for this deprecated method
		MouseEvent me = new MouseEvent(e);
		switch (e.type)
		{
			case SWT.MouseDoubleClick :
				dispatcher.dispatchMouseDoubleClicked(me);
				break;
			case SWT.MouseDown :
				dispatcher.dispatchMousePressed(me);
				break;
			case SWT.MouseUp :
				dispatcher.dispatchMouseReleased(me);
				break;
			case SWT.MouseMove :
				dispatcher.dispatchMouseMoved(me);
				break;
			case SWT.MouseHover :
				dispatcher.dispatchMouseHover(me);
				break;
			default :
				throw new InternalErrorException("Unsupported Event type" + e.type);
		}
	}

	/**
	 * @return
	 */
	public WorkspaceRepository getRepository()
	{
		TersusEditPart topEditPart = (TersusEditPart)getContents();
		return (WorkspaceRepository) topEditPart.getTersusModel().getRepository();

	}

    public TersusEditor getEditor()
    {
        return editor;
    }

    public void setEditor(TersusEditor editor)
    {
        this.editor = editor;
    }
}
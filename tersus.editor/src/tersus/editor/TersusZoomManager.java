/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor;

import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Viewport;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.jface.dialogs.MessageDialog;

import tersus.editor.editparts.TersusEditPart;
import tersus.editor.figures.FlowRectangle;
import tersus.editor.figures.PrecisionRectangle;
import tersus.editor.preferences.Preferences;
import tersus.util.Trace;

/**
 * 
 */
public class TersusZoomManager
{

	TersusRootEditPart root;
	Viewport viewport;

	public TersusZoomManager(TersusRootEditPart root, Viewport viewport)
	{
		this.root = root;
		this.viewport = viewport;
	}
	/* (non-Javadoc)
	 * @see org.eclipse.gef.editparts.ZoomManager#setZoom(double)
	 */
	public void zoom(double zoom)
	{
	
		try
		{
			zoomNoUpdate(zoom);
		}
		catch (ZoomLimitReachedException e)
		{
			MessageDialog.openInformation(root.getViewer().getControl().getShell(), "Zoom limit reached", "Zoom limit reached");
		}
		
		
		List selected = root.getViewer().getSelectedEditParts();
		if (selected.size() == 1)
		{
			viewport.validate();
			IFigure selectedFigure = ((TersusEditPart)selected.get(0)).getFigure();
			
			Point currentCenter = selectedFigure.getBounds().getCenter();
			selectedFigure.translateToAbsolute(currentCenter);
			Point visibleAreaCenter = viewport.getBounds().getCenter();
			viewport.translateToAbsolute(visibleAreaCenter);
			Dimension dif = currentCenter.getDifference(visibleAreaCenter);
			Point viewLocation = viewport.getViewLocation().getCopy();
			viewLocation.x += dif.width;
			viewLocation.y += dif.height;
			viewport.setViewLocation(viewLocation);
						
		}
		
		viewport.getUpdateManager().performUpdate();
	}
	private void zoomNoUpdate(double zoom)
	{
		FlowRectangle root = getRoot();
		PrecisionRectangle b = root.getPreciseBounds();
		//TODO: handle extreme cases (w or h too small or too large)
		double h = Math.round(b.preciseHeight * zoom);
		double w = h * getRoot().getWidthProportion();
		if (h>Integer.MAX_VALUE/100 || w > Integer.MAX_VALUE/100)
			throw new ZoomLimitReachedException();
		if (Debug.ZOOM)
			Trace.add("RootFigure current bounds:" + b);
		root.setBounds(b.preciseX, b.preciseY, w, h);
		if (Debug.ZOOM)
			Trace.add("RootFigure zoomed bounds:" + b);
		viewport.invalidateTree();
		viewport.repaint();
	}
	/**
	 * @return
	 */
	private FlowRectangle getRoot()
	{
		return (FlowRectangle) ((GraphicalEditPart) root.getContents()).getFigure();
	}

	public void zoomToFigure(IFigure figure)
	{
		Rectangle visibleArea = viewport.getBounds();
		if (visibleArea.width*visibleArea.height == 0)
			return;
		Rectangle figureBounds = figure.getBounds().getCopy();
		figure.translateToAbsolute(figureBounds);
		Point currentCenter = figureBounds.getCenter();
		Point originalCenter = figureBounds.getCenter();
		Point viewLocation = viewport.getViewLocation();
		Point visibleAreaCenter = visibleArea.getCenter();
		Point targetCenter = new Point();
		figureBounds = figure.getBounds().getCopy();
		int figureHeight = figureBounds.height;
		int figureWidth = figureBounds.width;
		double ratioX = figureWidth / (double) visibleArea.width;
		double ratioY = figureHeight / (double) visibleArea.height;
		double initialRatio = Math.max(ratioX, ratioY);
		double preferredMargin = Preferences.getInt(Preferences.ZOOM_MARGIM)/100.0;

		double minimumMargin =
			ratioX > ratioY
				? figureBounds.x / (double) visibleArea.width
				: figureBounds.y / (double) visibleArea.height;
		double zoomMargin = Math.min(minimumMargin, preferredMargin);
		double finalRatio = 1 - zoomMargin * 2;
		int iterationCount = Preferences.getInt(Preferences.ZOOM_ANIMATION_STEPS);
		for (int i = 1; i <= iterationCount; i++)
		{
			if (Debug.ZOOM)
				Trace.push("zoom: i=" + i);

			if (i > 1)
				try
				{
					Thread.sleep(Math.max(1,Preferences.getInt(Preferences.ZOOM_ANIMATION_DURATION) / iterationCount)); //TODO: extract preference
				}
				catch (InterruptedException e)
				{
				}
			figureBounds = figure.getBounds().getCopy();
			figureHeight = figureBounds.height;
			figureWidth = figureBounds.width;
			ratioX = figureWidth / (double) visibleArea.width;
			ratioY = figureHeight / (double) visibleArea.height;
			double currentRatio = Math.max(ratioX, ratioY);
			double targetRatio = (finalRatio * i + initialRatio * (iterationCount - i)) / iterationCount;
			double zoom = targetRatio / currentRatio;
			if (Debug.ZOOM)
				Trace.add("Current ratio:" + currentRatio + " Target ratio:" + targetRatio + " zoom:" + zoom);
			zoomNoUpdate(zoom);
			viewport.validate(); // recalculate the layout
			figureBounds = figure.getBounds().getCopy();
			visibleArea = viewport.getBounds();
			figure.translateToAbsolute(figureBounds);
			if (Debug.ZOOM)
				Trace.add("currentBounds=" + figureBounds);
			currentCenter = figureBounds.getCenter();
			if (Debug.ZOOM)
				Trace.add("currentCenter=" + currentCenter);
			targetCenter.x = (originalCenter.x * (iterationCount - i) + visibleAreaCenter.x * i) / iterationCount;
			targetCenter.y = (originalCenter.y * (iterationCount - i) + visibleAreaCenter.y * i) / iterationCount;
			if (Debug.ZOOM)
				Trace.add("targetCenter=" + targetCenter);
			Dimension dif = currentCenter.getDifference(targetCenter);
			viewLocation = viewport.getViewLocation();
			if (Debug.ZOOM)
				Trace.add("Viewport move delta=" + dif);
			if (Debug.ZOOM)
				Trace.add("current viewLocation=" + viewLocation);
			viewLocation.x += dif.width;
			viewLocation.y += dif.height;
			if (Debug.ZOOM)
				Trace.add("target viewLocation=" + viewLocation);
			viewport.setViewLocation(viewLocation);
			viewport.getUpdateManager().performUpdate();
			if (Debug.ZOOM)
				Trace.add("new figureCenter=" + figure.getBounds().getCenter());
			if (Debug.ZOOM)
				Trace.pop();
		}
	}

}

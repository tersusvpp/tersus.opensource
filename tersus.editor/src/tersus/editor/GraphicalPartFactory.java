/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import tersus.editor.editparts.*;
import tersus.editor.figures.NoteEditPart;
import tersus.editor.properties.ModelObjectWrapper;
import tersus.model.DataElement;
import tersus.model.DataType;
import tersus.model.FlowModel;
import tersus.model.Link;
import tersus.model.Model;
import tersus.model.Note;
import tersus.model.Slot;
import tersus.model.SubFlow;
import tersus.util.Misc;
import tersus.util.Trace;
public class GraphicalPartFactory implements EditPartFactory
{
	public EditPart createEditPart(EditPart iContext, Object iModel)
	{
	    if (iModel == null)
	        return null;
		EditPart editPart = null;
		ModelObjectWrapper wrapper = (ModelObjectWrapper) iModel;
		
		if (wrapper.isMissingModel())
			editPart = new MissingModelEditPart();
		else if (wrapper.getMode() == ModelObjectWrapper.MODE_DATA_ELEMENT)
			editPart = new SubDataEditPart();
		else if (wrapper.getElement() instanceof SubFlow)
			editPart = new SubFlowEditPart();
		else if (wrapper.getElement() instanceof Slot)
			editPart = new SlotEditPart();
		else if (wrapper.getElement() instanceof Link)
			editPart = new LinkEditPart();
		else if (wrapper.getElement() == null && wrapper.getModel() instanceof FlowModel)
			editPart = new TopFlowEditPart();
		else if (wrapper.getElement() instanceof DataElement)
		{
			Model parentModel = wrapper.getElement().getParentModel();
			if (parentModel instanceof FlowModel)
				editPart = new FlowDataEditPart();
			else
			{
				Misc.assertion(parentModel instanceof DataType);
				editPart = new SubDataEditPart();
			}
		}
		else if (wrapper.getElement() instanceof Note)
		    editPart = new NoteEditPart();
		else
			Misc.assertion(false); //Unexpected model type
		
		if (editPart != null)
			editPart.setModel(iModel);
		
		if (Debug.OTHER)
			Trace.add(
				"GraphicalPartFactory.createEditPart() context="
					+ iContext
					+ " model="
					+ iModel
					+ " result="
					+ editPart);
		return editPart;
	}
}

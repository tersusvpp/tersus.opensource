/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.editor;

import java.util.Iterator;

import org.eclipse.gef.requests.CreationFactory;

import tersus.InternalErrorException;
import tersus.model.MetaProperty;
import tersus.model.ModelObject;

/**
 * 
 */
public class ModelObjectFactory implements CreationFactory {

    private ModelObject template;
    
    /**
     * @param template
     */
    public ModelObjectFactory(ModelObject template)
    {
        super();
        this.template = template;
    }
	/* (non-Javadoc)
	 * @see org.eclipse.gef.requests.CreationFactory#getNewObject()
	 */
	public Object getNewObject() {
	    ModelObject newObject;
        try
        {
            newObject = (ModelObject)template.getClass().newInstance();
        }
        catch (Exception e)
        {
            throw new InternalErrorException("Failed to instantiate "+template.getClass().getName(),e);
        }
        for (Iterator i=template.getMetaProperties().iterator(); i.hasNext();)
	    {
	        MetaProperty p = (MetaProperty)i.next();
	        if (template.isPropertySet(p.name))
	            newObject.setProperty(p.name, template.getProperty(p.name));
	        
	    }
	    return newObject;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.requests.CreationFactory#getObjectType()
	 */
	public Object getObjectType() {
		return null;
	}

    public ModelObject getTemplate()
    {
        return template;
    }
}

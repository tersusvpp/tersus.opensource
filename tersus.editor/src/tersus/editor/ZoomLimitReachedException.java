package tersus.editor;

public class ZoomLimitReachedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ZoomLimitReachedException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ZoomLimitReachedException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public ZoomLimitReachedException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public ZoomLimitReachedException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

}

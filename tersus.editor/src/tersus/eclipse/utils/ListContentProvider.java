/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.eclipse.utils;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.Display;

import tersus.util.IObservableList;
import tersus.util.ListListener;
import tersus.util.ObservableList;

public class ListContentProvider<T> implements IStructuredContentProvider, ListListener<T>
{
	protected static Object[] NONE = new Object[]
	{};
	protected TableViewer viewer;
	protected IObservableList<T> input;

	public ListContentProvider(TableViewer viewer)
	{
		this.viewer = viewer;
	}

	protected TableViewer getViewer()
	{
		return viewer;
	}

	@SuppressWarnings("unchecked")
	public Object[] getElements(Object inputElement)
	{
		if (inputElement instanceof ObservableList)
		{
			return ((IObservableList<T>) inputElement).getContent().toArray();
		}
		else
			return NONE;
	}

	public void dispose()
	{
		if (input != null)
			input.removeListener(this);
	}

	@SuppressWarnings("unchecked")
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
	{
		if (oldInput instanceof IObservableList)
			((IObservableList) oldInput).removeListener(this);
		if (newInput instanceof IObservableList)
		{
			((IObservableList) newInput).addListener(this);
			input = (IObservableList) newInput;
		}
	}

	public void contentChanged(IObservableList<T> list)
	{
		Display display = viewer.getControl().getDisplay();
		display.syncExec(new Runnable()
		{
			public void run()
			{
				viewer.refresh();
			}
		});
	}

	public void itemChanged(T item, String propertyName, Object oldValue, Object newValue)
	{
		Display display = viewer.getControl().getDisplay();
		display.syncExec(new Runnable()
		{
			public void run()
			{
				viewer.refresh();
			}
		});
		
	}

	public void itemAdded(final T item)
	{
		Display display = viewer.getControl().getDisplay();
		display.syncExec(new Runnable()
		{
			public void run()
			{
				viewer.add(item);
			}
		});
	}
}

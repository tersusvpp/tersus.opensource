/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.reloadStyles = function()
{
	var linkElement = document.getElementById('main_stylesheet');
	if (linkElement)
	{
		if(window.tersusOriginalStyleURL == null)
			window.tersusOriginalStyleURL = linkElement.href;
		
		linkElement.href=window.tersusOriginalStyleURL+ '&_x=' +(new Date()).getTime();
	}	
}

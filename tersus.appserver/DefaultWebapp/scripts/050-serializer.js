/*********************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function Serializer() {};
Serializer.prototype.DEBUG = false;
Serializer.prototype.Tokens = {};
Serializer.prototype.Tokens.START_OBJECT = '{';
Serializer.prototype.Tokens.END_OBJECT = '}';
Serializer.prototype.Tokens.START_ARRAY = '[';
Serializer.prototype.Tokens.END_ARRAY = ']';
Serializer.prototype.Tokens.OBJECT_SEPARATOR = ',';
Serializer.prototype.Tokens.ARRAY_SEPARATOR = ',';
Serializer.prototype.Tokens.ASSIGN = ':';
Serializer.prototype.Tokens.QUOTE = '"';
Serializer.prototype.addWhitespace = true;

Serializer.prototype.serialize = function serialize(value)
{
	var output = [];
	var stack = [];
	if (value.constructor == Array)
		this._serializeList(value, output, stack);
	else
		this._serializeValue(value, output, stack);
	var serialization = output.join('');
	return serialization;
};
Serializer.prototype._serializeList = function _serializeList(values, output, stack)
{
	output.push(this.Tokens.START_ARRAY);
	for (var i=0; i<values.length; i++)
	{
		if (i>0)
		{
			output.push(this.Tokens.ARRAY_SEPARATOR);
			if (this.addWhitespace)
				output.push('\n');
		}
		this._serializeValue(values[i], output, stack);
	};
	output.push(this.Tokens.END_ARRAY);
};
/*
 Serializes a value hierarchy into an output buffer
 value - the value to be serialized
 output - an array of strings/primitive values serving as the output buffer
*/
Serializer.prototype._serializeValue  = function _serializeValue(value, output, stack)
{
	if (value.isDataNode)
	{
		this._serializeValue(value.value, output, stack);
		return;
	};
	if (value.isNode)
	{
		for (var i=0;i<stack.length;i++)
		{
			var n = stack[i];
			if (n == value)
				return;
		}
	}
	stack.push(value);
	if (value.isSecret && this.hideSecretValues)
	{
		output.push('*****');
	}
	else if (value.isNumber || value.isBooleanValue)
	{
		if (value.leafValue != null)
			output.push(value.leafValue); // No need for quoting or escaping
	}
	else if (value.serialize)
	{
		this._quote(value.serialize(),output);
	}
	else if (value.isNothing)
	{
		output.push(this.Tokens.START_OBJECT);
		output.push(this.Tokens.END_OBJECT);
	}
	else if (value.isLeaf)
	{
		if (value.leafValue !== null && value.leafValue !== undefined)
			this._quote(value.leafValue, output);
		else
			output.push('null');
	}
	else
	{
		output.push(this.Tokens.START_OBJECT);
		var childCount = 0;
		if (value.elements)
		{
			for (var role in value.elements)
			{
				var element = value.elements[role];
				if (element.isAncestorReference)
					continue;
				if (element.isSubFlow && ! element.isDisplayElement)
					continue;
				if (element.isRepetitive)
				{
					var childValues = element.getChildren(value);
					if (childValues.length == 0)
						continue;
					if (childCount >0)
						output.push(this.Tokens.OBJECT_SEPARATOR);
					this._quote(element.role, output);
					output.push(this.Tokens.ASSIGN);
					this._serializeList(childValues, output, stack);
				}
				else
				{
					var childValue = element.getChild(value);
					if (! childValue)
						continue;
					if (childValue.isPopup)
						continue; // A pop is realy an action here, and it may be closed.  We don't want to serialize it
					if (childCount >0)
						output.push(this.Tokens.OBJECT_SEPARATOR);
					this._quote(element.role, output);
					output.push(this.Tokens.ASSIGN);
					this._serializeValue(childValue, output, stack);
				}
				childCount++;
			}
		}
		output.push(this.Tokens.END_OBJECT);
	}
	stack.pop();
};

Serializer.prototype._quote= function _quote(value, output)
{
	output.push(this.Tokens.QUOTE);
	var s = value.toString(); 
	var l = s.length;
	var escaped = "";
    for (var i = 0; i < l; i++)
    {
    	c = s.charAt(i);
    	switch (c)
    	{
      		case '\\':
				escaped += '\\\\';
                break;
      		case '\t':
				escaped += '\\t';
                break;
			case '\r':
				escaped += '\\r';
				break;
			case '\n':
				escaped += '\\n';
				break;
			case this.Tokens.QUOTE:
				escaped += "\\"+this.Tokens.QUOTE;
				break;
			default:
				escaped += c;
          }
	}
	//TODO complete escaping (use JSON http://www.crockford.com/JSON)
	
	output.push(escaped);
	output.push(this.Tokens.QUOTE);
};

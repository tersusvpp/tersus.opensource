/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 
function DataValue() {};
DataValue.prototype = new tersus.Node();
DataValue.prototype.constructor = DataValue;
DataValue.prototype.isDataValue = true;
DataValue.prototype.sqlType = 'NONE';
DataValue.prototype.toString = function toString()
{
	if (this.leafValue)
	{
		return ''+this.leafValue;
	}
	else
		return 'Composite Value: '+this.modelId;
};
DataValue.prototype.formatString = function (props)
{
	return this.toString();
};
DataValue.prototype.formatInput = function (props)
{
	return this.formatString(props);
};
DataValue.prototype.parseInput = function()
{
	internalError("parseInput not implemented for "+this.constructor.name);
};
DataValue.prototype.addDataElement = function addDataElement(role,modelId)
{
	var element;
	var getter = this.getGetter(role);
	if (getter)
		element = new MethodElement(this, role, modelId);
	else
		element = new DataElement(role, modelId);
	return this.addElement(element);
};


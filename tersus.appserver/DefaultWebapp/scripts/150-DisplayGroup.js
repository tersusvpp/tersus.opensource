/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.DisplayGroup = function () {};
tersus.DisplayGroup.prototype = new DisplayNode();
tersus.DisplayGroup.prototype.constructor = tersus.DisplayGroup;
tersus.DisplayGroup.prototype.isDisplayGroup = true;
tersus.DisplayGroup.prototype.createViewNode = function ()
{
	
};
tersus.DisplayGroup.prototype.initViewNode = function ()
{
	
};
tersus.DisplayGroup.prototype.attachViewNode = function ()
{
	this.viewNode = this.parent.viewNode;
};
tersus.DisplayGroup.prototype.removeChildViewNode = function (child)
{
	if (this.parent)
		this.parent.removeChildViewNode(child);
};
tersus.DisplayGroup.prototype.addChildHTMLPrefix = function (htmlV, child)
{
	if (this.parent)
		this.parent.addChildHTMLPrefix(htmlV,child);
};
tersus.DisplayGroup.prototype.addChildHTMLSuffix = function (htmlV, child)
{
	if (this.parent)
		this.parent.addChildHTMLSuffix(htmlV,child);
}
tersus.DisplayGroup.prototype.appendChildViewNode = function (child)
{
	var displayOrder = this.element.displayOrder + child.viewNode.getAttribute('display_order')*0.01;
	child.viewNode.setAttribute('display_order',displayOrder);
	this.parent.appendChildViewNode(child,this.element);
	child.refreshVisibility();
};
tersus.DisplayGroup.prototype.refreshVisibility = function ()
{
	this.forDisplayChildren( function(child) {child.refreshVisibility();});
};

tersus.DisplayGroup.prototype.isGroup = true;
tersus.DisplayGroup.prototype.onDelete = function()
{
	this.forDisplayChildren( function(child) {child.onDelete();});
};
tersus.DisplayGroup.prototype.applyColOrder = function(row)
{
	if (this.parent.applyColOrder)
		this.parent.applyColOrder(row);
};

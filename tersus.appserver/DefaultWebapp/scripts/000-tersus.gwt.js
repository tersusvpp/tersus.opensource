
var $wnd = window;
var $doc = $wnd.document;
var $moduleName, $moduleBase;
var $stats = $wnd.__gwtStatsEvent ? function(a) {return $wnd.__gwtStatsEvent(a);} : null,
$sessionId = $wnd.__gwtStatsSessionId ? $wnd.__gwtStatsSessionId : null;
$stats && $stats({moduleName:'tersus.gwt.Util',sessionId:$sessionId,subSystem:'startup',evtGroup:'moduleStartup',millis:(new Date()).getTime(),type:'moduleEvalStart'});

var _, N8000000000000000_longLit = [0, -9223372036854775808], P0_longLit = [0, 0], P3e8_longLit = [1000, 0], P1000000_longLit = [16777216, 0], P7fffffffffffffff_longLit = [4294967295, 9223372032559808512];
function nullMethod(){
}

function equals(other){
  return this === (other == null?null:other);
}

function hashCode_0(){
  return this.$H || (this.$H = ++sNextHashId);
}

function Object_0(){
}

_ = Object_0.prototype = {};
_.equals$ = equals;
_.hashCode$ = hashCode_0;
_.typeMarker$ = nullMethod;
_.typeId$ = 1;
function $setStackTrace(stackTrace){
  var c, copy, i;
  copy = initDim(_3Ljava_lang_StackTraceElement_2_classLit, 39, 13, stackTrace.length, 0);
  for (i = 0 , c = stackTrace.length; i < c; ++i) {
    if (!stackTrace[i]) {
      throw $NullPointerException(new NullPointerException);
    }
    copy[i] = stackTrace[i];
  }
}

function Throwable(){
}

_ = Throwable.prototype = new Object_0;
_.typeId$ = 3;
function Exception(){
}

_ = Exception.prototype = new Throwable;
_.typeId$ = 4;
function RuntimeException(){
}

_ = RuntimeException.prototype = new Exception;
_.typeId$ = 5;
function $JavaScriptException(this$static, e){
  $fillInStackTrace();
  this$static.e = e;
  $createStackTrace(this$static);
  return this$static;
}

function JavaScriptException(){
}

_ = JavaScriptException.prototype = new RuntimeException;
_.typeId$ = 6;
_.e = null;
function equals__devirtual$(this$static, other){
  return this$static.typeMarker$ == nullMethod || this$static.typeId$ == 2?this$static.equals$(other):(this$static == null?null:this$static) === (other == null?null:other);
}

function hashCode__devirtual$(this$static){
  return this$static.typeMarker$ == nullMethod || this$static.typeId$ == 2?this$static.hashCode$():this$static.$H || (this$static.$H = ++sNextHashId);
}

function Scheduler(){
}

_ = Scheduler.prototype = new Object_0;
_.typeId$ = 0;
function entry_0(jsFunction){
  return function(){
    return entry0(jsFunction, this, arguments);
  }
  ;
}

function entry0(jsFunction, thisObj, arguments_0){
  var initialEntry;
  initialEntry = entryDepth++ == 0;
  try {
    return jsFunction.apply(thisObj, arguments_0);
  }
   finally {
    initialEntry && $flushFinallyCommands(($clinit_12() , INSTANCE));
    --entryDepth;
  }
}

var entryDepth = 0, sNextHashId = 0;
function $clinit_12(){
  $clinit_12 = nullMethod;
  INSTANCE = $SchedulerImpl(new SchedulerImpl);
}

function $SchedulerImpl(this$static){
  $clinit_12();
  this$static.flusher = $SchedulerImpl$1(new SchedulerImpl$1, this$static);
  $SchedulerImpl$2(new SchedulerImpl$2, this$static);
  this$static.deferredCommands = [];
  this$static.incrementalCommands = [];
  this$static.finallyCommands = [];
  return this$static;
}

function $flushFinallyCommands(this$static){
  var oldFinally;
  oldFinally = this$static.finallyCommands;
  this$static.finallyCommands = [];
  runScheduledTasks(oldFinally, this$static.finallyCommands);
}

function $flushPostEventPumpCommands(this$static){
  var oldDeferred;
  oldDeferred = this$static.deferredCommands;
  this$static.deferredCommands = [];
  runScheduledTasks(oldDeferred, this$static.incrementalCommands);
  this$static.incrementalCommands = runRepeatingTasks(this$static.incrementalCommands);
}

function $isWorkQueued(this$static){
  return this$static.deferredCommands.length > 0 || this$static.incrementalCommands.length > 0;
}

function execute(cmd){
  return cmd.execute();
}

function runRepeatingTasks(tasks){
  var canceledSomeTasks, i, length_0, newTasks, start, t;
  canceledSomeTasks = false;
  length_0 = tasks.length;
  start = (new Date).getTime();
  while ((new Date).getTime() - start < 100) {
    for (i = 0; i < length_0; ++i) {
      t = tasks[i];
      if (!t) {
        continue;
      }
      if (!t[0].execute()) {
        tasks[i] = null;
        canceledSomeTasks = true;
      }
    }
  }
  if (canceledSomeTasks) {
    newTasks = [];
    for (i = 0; i < length_0; ++i) {
      if (!tasks[i]) {
        continue;
      }
      newTasks[newTasks.length] = tasks[i];
    }
    return newTasks;
  }
   else {
    return tasks;
  }
}

function runScheduledTasks(tasks, rescheduled){
  var $e0, i, j, t;
  for (i = 0 , j = tasks.length; i < j; ++i) {
    t = tasks[i];
    try {
      t[1]?t[0].execute() && (rescheduled[rescheduled.length] = t , undefined):t[0].nullMethod();
    }
     catch ($e0) {
      $e0 = caught($e0);
      if (!instanceOf($e0, 2))
        throw $e0;
    }
  }
}

function scheduleFixedDelayImpl(cmd, delayMs){
  $clinit_12();
  $wnd.setTimeout(function(){
    var ret = $entry(execute)(cmd);
    ret && $wnd.setTimeout(arguments.callee, delayMs);
  }
  , delayMs);
}

function SchedulerImpl(){
}

_ = SchedulerImpl.prototype = new Scheduler;
_.typeId$ = 0;
_.flushRunning = false;
_.shouldBeRunning = false;
var INSTANCE;
function $SchedulerImpl$1(this$static, this$0){
  this$static.this$0 = this$0;
  return this$static;
}

function execute_0(){
  this.this$0.flushRunning = true;
  $flushPostEventPumpCommands(this.this$0);
  this.this$0.flushRunning = false;
  return this.this$0.shouldBeRunning = $isWorkQueued(this.this$0);
}

function SchedulerImpl$1(){
}

_ = SchedulerImpl$1.prototype = new Object_0;
_.execute = execute_0;
_.typeId$ = 0;
_.this$0 = null;
function $SchedulerImpl$2(this$static, this$0){
  this$static.this$0 = this$0;
  return this$static;
}

function execute_1(){
  this.this$0.flushRunning && scheduleFixedDelayImpl(this.this$0.flusher, 1);
  return this.this$0.shouldBeRunning;
}

function SchedulerImpl$2(){
}

_ = SchedulerImpl$2.prototype = new Object_0;
_.execute = execute_1;
_.typeId$ = 0;
_.this$0 = null;
function extractNameFromToString(fnToString){
  var index, start, toReturn;
  toReturn = '';
  fnToString = $trim(fnToString);
  index = fnToString.indexOf('(');
  if (index != -1) {
    start = fnToString.indexOf('function') == 0?8:0;
    toReturn = $trim(fnToString.substr(start, index - start));
  }
  return toReturn.length > 0?toReturn:'anonymous';
}

function $collect(this$static){
  var seen = {};
  var toReturn = [];
  var callee = arguments.callee.caller.caller;
  while (callee) {
    var name_0 = this$static.extractName(callee.toString());
    toReturn.push(name_0);
    var keyName = ':' + name_0;
    var withThisName = seen[keyName];
    if (withThisName) {
      var i, j;
      for (i = 0 , j = withThisName.length; i < j; i++) {
        if (withThisName[i] === callee) {
          return toReturn;
        }
      }
    }
    (withThisName || (seen[keyName] = [])).push(callee);
    callee = callee.caller;
  }
  return toReturn;
}

function $createStackTrace(e){
  var i, j, stack, stackTrace;
  stack = (instanceOfJso(e.e)?dynamicCastJso(e.e):null , []);
  stackTrace = initDim(_3Ljava_lang_StackTraceElement_2_classLit, 39, 13, stack.length, 0);
  for (i = 0 , j = stackTrace.length; i < j; ++i) {
    stackTrace[i] = $StackTraceElement(new StackTraceElement, 'Unknown', stack[i], 'Unknown source', 0);
  }
  $setStackTrace(stackTrace);
}

function $fillInStackTrace(){
  var i, j, stack, stackTrace;
  stack = $collect(new StackTraceCreator$Collector);
  stackTrace = initDim(_3Ljava_lang_StackTraceElement_2_classLit, 39, 13, stack.length, 0);
  for (i = 0 , j = stackTrace.length; i < j; ++i) {
    stackTrace[i] = $StackTraceElement(new StackTraceElement, 'Unknown', stack[i], 'Unknown source', 0);
  }
  $setStackTrace(stackTrace);
}

function extractName(fnToString){
  return extractNameFromToString(fnToString);
}

function StackTraceCreator$Collector(){
}

_ = StackTraceCreator$Collector.prototype = new Object_0;
_.extractName = extractName;
_.typeId$ = 0;
function $append(a, x){
  a[a.explicitLength++] = x;
}

function $append_0(a, x){
  a[a.explicitLength++] = x == null?'null':x;
}

function $appendNonNull(a, x){
  a[a.explicitLength++] = x;
}

function $replace(a, start, end, toInsert){
  var s;
  s = $takeString(a);
  $appendNonNull(a, s.substr(0, start - 0));
  a[a.explicitLength++] = toInsert == null?'null':toInsert;
  $appendNonNull(a, s.substr(end, s.length - end));
}

function $takeString(a){
  var s = a.join('');
  a.length = a.explicitLength = 0;
  return s;
}

function $toString(a){
  var s;
  s = $takeString(a);
  a[a.explicitLength++] = s;
  return s;
}

function $clinit_24(){
  $clinit_24 = nullMethod;
  instance = ($clinit_24() , new LocaleInfo);
}

function $ensureNumberConstants(this$static){
  !this$static.numberConstants && (this$static.numberConstants = new NumberConstantsImpl_);
}

function $getDateTimeConstants(this$static){
  !this$static.dateTimeConstants && (this$static.dateTimeConstants = $DateTimeConstantsImpl_(new DateTimeConstantsImpl_));
  return this$static.dateTimeConstants;
}

function LocaleInfo(){
}

_ = LocaleInfo.prototype = new Object_0;
_.typeId$ = 0;
_.dateTimeConstants = null;
_.numberConstants = null;
var instance;
function $clinit_27(){
  $clinit_27 = nullMethod;
  $ensureNumberConstants(($clinit_24() , $clinit_24() , instance));
}

function $NumberFormat(this$static, numberConstants, pattern, cdata, userSuppliedPattern){
  $clinit_27();
  if (!cdata) {
    throw $IllegalArgumentException(new IllegalArgumentException, 'Unknown currency code');
  }
  this$static.numberConstants = numberConstants;
  this$static.pattern = pattern;
  this$static.currencyCode = cdata[0];
  this$static.currencySymbol = cdata[1];
  $parsePattern(this$static, this$static.pattern);
  if (!userSuppliedPattern && this$static.isCurrencyFormat) {
    this$static.minimumFractionDigits = cdata[2] & 7;
    this$static.maximumFractionDigits = this$static.minimumFractionDigits;
  }
  return this$static;
}

function $addExponentPart(this$static, exponent, result){
  var exponentDigits, i, len;
  $append_0(result.data, 'E');
  if (exponent < 0) {
    exponent = -exponent;
    $append_0(result.data, '-');
  }
  exponentDigits = '' + exponent;
  len = exponentDigits.length;
  for (i = len; i < this$static.minExponentDigits; ++i) {
    $append_0(result.data, '0');
  }
  for (i = 0; i < len; ++i) {
    $append_1(result, exponentDigits.charCodeAt(i));
  }
}

function $format(this$static, number){
  var isNegative, result;
  result = $StringBuffer(new StringBuffer);
  if (isNaN(number)) {
    $append_0(result.data, '\uFFFD');
    return $toString(result.data);
  }
  isNegative = number < 0 || number == 0 && 1 / number < 0;
  $append_3(result, isNegative?this$static.negativePrefix:this$static.positivePrefix);
  if (!isFinite(number)) {
    $append_0(result.data, '\u0221');
  }
   else {
    isNegative && (number = -number);
    number *= this$static.multiplier;
    this$static.useExponentialNotation?$subformatExponential(this$static, number, result):$subformatFixed(this$static, number, result, this$static.minimumIntegerDigits);
  }
  $append_3(result, isNegative?this$static.negativeSuffix:this$static.positiveSuffix);
  return $toString(result.data);
}

function $getDigit(ch){
  if (48 <= ch && ch <= 57) {
    return ch - 48;
  }
   else {
    return 48 <= ch && ch <= 57?ch - 48:-1;
  }
}

function $parse(this$static, text){
  var pos, result;
  pos = initValues(_3I_classLit, 34, -1, [0]);
  result = $parse_0(this$static, text, pos);
  if (pos[0] == 0 || pos[0] != text.length) {
    throw $NumberFormatException(new NumberFormatException, text);
  }
  return result;
}

function $parse_0(this$static, text, inOutPos){
  var gotNegative, gotNegativePrefix, gotNegativeSuffix, gotPositive, gotPositivePrefix, gotPositiveSuffix, ret, tempPos, valueOnly;
  ret = 0;
  gotPositivePrefix = $startsWith(text, this$static.positivePrefix, inOutPos[0]);
  gotNegativePrefix = $startsWith(text, this$static.negativePrefix, inOutPos[0]);
  gotPositiveSuffix = $endsWith(text, this$static.positiveSuffix);
  gotNegativeSuffix = $endsWith(text, this$static.negativeSuffix);
  gotPositive = gotPositivePrefix && gotPositiveSuffix;
  gotNegative = gotNegativePrefix && gotNegativeSuffix;
  if (gotPositive && gotNegative) {
    this$static.positivePrefix.length > this$static.negativePrefix.length?(gotNegative = false):this$static.positivePrefix.length < this$static.negativePrefix.length?(gotPositive = false):this$static.positiveSuffix.length > this$static.negativeSuffix.length?(gotNegative = false):this$static.positiveSuffix.length < this$static.negativeSuffix.length?(gotPositive = false):(gotNegative = false);
  }
   else if (!gotPositive && !gotNegative) {
    throw $NumberFormatException(new NumberFormatException, text + ' does not have either positive or negative affixes');
  }
  valueOnly = null;
  if (gotPositive) {
    inOutPos[0] += this$static.positivePrefix.length;
    valueOnly = $substring_0(text, inOutPos[0], text.length - this$static.positiveSuffix.length);
  }
   else {
    inOutPos[0] += this$static.negativePrefix.length;
    valueOnly = $substring_0(text, inOutPos[0], text.length - this$static.negativeSuffix.length);
  }
  if ($equals_1(valueOnly, '\u0221')) {
    inOutPos[0] += 1;
    ret = Infinity;
  }
   else if ($equals_1(valueOnly, '\uFFFD')) {
    inOutPos[0] += 1;
    ret = NaN;
  }
   else {
    tempPos = initValues(_3I_classLit, 34, -1, [0]);
    ret = $parseNumber(this$static, valueOnly, tempPos);
    inOutPos[0] += tempPos[0];
  }
  gotPositive?(inOutPos[0] += this$static.positiveSuffix.length):gotNegative && (inOutPos[0] += this$static.negativeSuffix.length);
  gotNegative && (ret = -ret);
  return ret;
}

function $parseAffix(this$static, pattern, start, affix, inNegativePattern){
  var ch, inQuote, len, pos;
  $replace_0(affix, 0, $toString(affix.data).length, '');
  inQuote = false;
  len = pattern.length;
  for (pos = start; pos < len; ++pos) {
    ch = pattern.charCodeAt(pos);
    if (ch == 39) {
      if (pos + 1 < len && pattern.charCodeAt(pos + 1) == 39) {
        ++pos;
        $append_0(affix.data, "'");
      }
       else {
        inQuote = !inQuote;
      }
      continue;
    }
    if (inQuote) {
      $appendNonNull(affix.data, String.fromCharCode(ch));
    }
     else {
      switch (ch) {
        case 35:
        case 48:
        case 44:
        case 46:
        case 59:
          return pos - start;
        case 164:
          this$static.isCurrencyFormat = true;
          if (pos + 1 < len && pattern.charCodeAt(pos + 1) == 164) {
            ++pos;
            $append_3(affix, this$static.currencyCode);
          }
           else {
            $append_3(affix, this$static.currencySymbol);
          }

          break;
        case 37:
          if (!inNegativePattern) {
            if (this$static.multiplier != 1) {
              throw $IllegalArgumentException(new IllegalArgumentException, 'Too many percent/per mille characters in pattern "' + pattern + '"');
            }
            this$static.multiplier = 100;
          }

          $append_0(affix.data, '%');
          break;
        case 8240:
          if (!inNegativePattern) {
            if (this$static.multiplier != 1) {
              throw $IllegalArgumentException(new IllegalArgumentException, 'Too many percent/per mille characters in pattern "' + pattern + '"');
            }
            this$static.multiplier = 1000;
          }

          $append_0(affix.data, '\u2030');
          break;
        case 45:
          $append_0(affix.data, '-');
          break;
        default:$appendNonNull(affix.data, String.fromCharCode(ch));
      }
    }
  }
  return len - start;
}

function $parseNumber(this$static, text, pos){
  var $e0, ch, decimal, digit, grouping, normalizedText, ret, sawDecimal, sawDigit, sawExponent, scale;
  sawDecimal = false;
  sawExponent = false;
  sawDigit = false;
  scale = 1;
  decimal = this$static.isCurrencyFormat?'.':this$static.numberConstants.decimalSeparator;
  grouping = this$static.isCurrencyFormat?',':this$static.numberConstants.groupingSeparator;
  normalizedText = $StringBuffer(new StringBuffer);
  for (; pos[0] < text.length; ++pos[0]) {
    ch = text.charCodeAt(pos[0]);
    digit = $getDigit(ch);
    if (digit >= 0 && digit <= 9) {
      $appendNonNull(normalizedText.data, String.fromCharCode(digit + 48 & 65535));
      sawDigit = true;
    }
     else if (ch == decimal.charCodeAt(0)) {
      if (sawDecimal || sawExponent) {
        break;
      }
      $appendNonNull(normalizedText.data, '.');
      sawDecimal = true;
    }
     else if (ch == grouping.charCodeAt(0)) {
      if (sawDecimal || sawExponent) {
        break;
      }
      continue;
    }
     else if (ch == 69) {
      if (sawExponent) {
        break;
      }
      $appendNonNull(normalizedText.data, 'E');
      sawExponent = true;
    }
     else if (ch == 43 || ch == 45) {
      $appendNonNull(normalizedText.data, String.fromCharCode(ch));
    }
     else if (ch == 37) {
      if (scale != 1) {
        break;
      }
      scale = 100;
      if (sawDigit) {
        ++pos[0];
        break;
      }
    }
     else if (ch == 8240) {
      if (scale != 1) {
        break;
      }
      scale = 1000;
      if (sawDigit) {
        ++pos[0];
        break;
      }
    }
     else {
      break;
    }
  }
  try {
    ret = __parseAndValidateDouble($toString(normalizedText.data));
  }
   catch ($e0) {
    $e0 = caught($e0);
    if (instanceOf($e0, 3)) {
      throw $NumberFormatException(new NumberFormatException, text);
    }
     else 
      throw $e0;
  }
  ret = ret / scale;
  return ret;
}

function $parsePattern(this$static, pattern){
  var affix, pos;
  pos = 0;
  affix = $StringBuffer(new StringBuffer);
  pos += $parseAffix(this$static, pattern, pos, affix, false);
  this$static.positivePrefix = $toString(affix.data);
  pos += $parseTrunk(this$static, pattern, pos, false);
  pos += $parseAffix(this$static, pattern, pos, affix, false);
  this$static.positiveSuffix = $toString(affix.data);
  if (pos < pattern.length && pattern.charCodeAt(pos) == 59) {
    ++pos;
    pos += $parseAffix(this$static, pattern, pos, affix, true);
    this$static.negativePrefix = $toString(affix.data);
    pos += $parseTrunk(this$static, pattern, pos, true);
    pos += $parseAffix(this$static, pattern, pos, affix, true);
    this$static.negativeSuffix = $toString(affix.data);
  }
   else {
    this$static.negativePrefix = '-' + this$static.positivePrefix;
    this$static.negativeSuffix = this$static.positiveSuffix;
  }
}

function $parseTrunk(this$static, pattern, start, ignorePattern){
  var ch, decimalPos, digitLeftCount, digitRightCount, effectiveDecimalPos, groupingCount, len, loop, n, pos, totalDigits, zeroDigitCount;
  decimalPos = -1;
  digitLeftCount = 0;
  zeroDigitCount = 0;
  digitRightCount = 0;
  groupingCount = -1;
  len = pattern.length;
  pos = start;
  loop = true;
  for (; pos < len && loop; ++pos) {
    ch = pattern.charCodeAt(pos);
    switch (ch) {
      case 35:
        zeroDigitCount > 0?++digitRightCount:++digitLeftCount;
        groupingCount >= 0 && decimalPos < 0 && ++groupingCount;
        break;
      case 48:
        if (digitRightCount > 0) {
          throw $IllegalArgumentException(new IllegalArgumentException, "Unexpected '0' in pattern \"" + pattern + '"');
        }

        ++zeroDigitCount;
        groupingCount >= 0 && decimalPos < 0 && ++groupingCount;
        break;
      case 44:
        groupingCount = 0;
        break;
      case 46:
        if (decimalPos >= 0) {
          throw $IllegalArgumentException(new IllegalArgumentException, 'Multiple decimal separators in pattern "' + pattern + '"');
        }

        decimalPos = digitLeftCount + zeroDigitCount + digitRightCount;
        break;
      case 69:
        if (!ignorePattern) {
          if (this$static.useExponentialNotation) {
            throw $IllegalArgumentException(new IllegalArgumentException, 'Multiple exponential symbols in pattern "' + pattern + '"');
          }
          this$static.useExponentialNotation = true;
          this$static.minExponentDigits = 0;
        }

        while (pos + 1 < len && pattern.charCodeAt(pos + 1) == 48) {
          ++pos;
          !ignorePattern && ++this$static.minExponentDigits;
        }

        if (!ignorePattern && digitLeftCount + zeroDigitCount < 1 || this$static.minExponentDigits < 1) {
          throw $IllegalArgumentException(new IllegalArgumentException, 'Malformed exponential pattern "' + pattern + '"');
        }

        loop = false;
        break;
      default:--pos;
        loop = false;
    }
  }
  if (zeroDigitCount == 0 && digitLeftCount > 0 && decimalPos >= 0) {
    n = decimalPos;
    n == 0 && ++n;
    digitRightCount = digitLeftCount - n;
    digitLeftCount = n - 1;
    zeroDigitCount = 1;
  }
  if (decimalPos < 0 && digitRightCount > 0 || decimalPos >= 0 && (decimalPos < digitLeftCount || decimalPos > digitLeftCount + zeroDigitCount) || groupingCount == 0) {
    throw $IllegalArgumentException(new IllegalArgumentException, 'Malformed pattern "' + pattern + '"');
  }
  if (ignorePattern) {
    return pos - start;
  }
  totalDigits = digitLeftCount + zeroDigitCount + digitRightCount;
  this$static.maximumFractionDigits = decimalPos >= 0?totalDigits - decimalPos:0;
  if (decimalPos >= 0) {
    this$static.minimumFractionDigits = digitLeftCount + zeroDigitCount - decimalPos;
    this$static.minimumFractionDigits < 0 && (this$static.minimumFractionDigits = 0);
  }
  effectiveDecimalPos = decimalPos >= 0?decimalPos:totalDigits;
  this$static.minimumIntegerDigits = effectiveDecimalPos - digitLeftCount;
  if (this$static.useExponentialNotation) {
    this$static.maximumIntegerDigits = digitLeftCount + this$static.minimumIntegerDigits;
    this$static.maximumFractionDigits == 0 && this$static.minimumIntegerDigits == 0 && (this$static.minimumIntegerDigits = 1);
  }
  this$static.groupingSize = groupingCount > 0?groupingCount:0;
  this$static.decimalSeparatorAlwaysShown = decimalPos == 0 || decimalPos == totalDigits;
  return pos - start;
}

function $subformatExponential(this$static, number, result){
  var exponent, i, minIntDigits;
  if (number == 0) {
    $subformatFixed(this$static, number, result, this$static.minimumIntegerDigits);
    $addExponentPart(this$static, 0, result);
    return;
  }
  exponent = round_int(floor(Math.log(number) / Math.log(10)));
  number /= Math.pow(10, exponent);
  minIntDigits = this$static.minimumIntegerDigits;
  if (this$static.maximumIntegerDigits > 1 && this$static.maximumIntegerDigits > this$static.minimumIntegerDigits) {
    while (exponent % this$static.maximumIntegerDigits != 0) {
      number *= 10;
      --exponent;
    }
    minIntDigits = 1;
  }
   else {
    if (this$static.minimumIntegerDigits < 1) {
      ++exponent;
      number /= 10;
    }
     else {
      for (i = 1; i < this$static.minimumIntegerDigits; ++i) {
        --exponent;
        number *= 10;
      }
    }
  }
  $subformatFixed(this$static, number, result, minIntDigits);
  $addExponentPart(this$static, exponent, result);
}

function $subformatFixed(this$static, number, result, minIntDigits){
  var decimal, decimalIndex, digitLen, exponentIndex, fixedString, fracLen, fracPart, fracValue, fractionPresent, grouping, i, intPart, intValue, len, power;
  power = Math.pow(10, this$static.maximumFractionDigits);
  fixedString = number.toFixed(this$static.maximumFractionDigits + 3);
  intValue = 0;
  fracValue = 0;
  exponentIndex = fixedString.indexOf(fromCodePoint(101));
  if (exponentIndex != -1) {
    intValue = Math.floor(number);
  }
   else {
    decimalIndex = fixedString.indexOf(fromCodePoint(46));
    len = fixedString.length;
    decimalIndex == -1 && (decimalIndex = len);
    decimalIndex > 0 && (intValue = __parseAndValidateDouble(fixedString.substr(0, decimalIndex - 0)));
    if (decimalIndex < len - 1) {
      fracValue = __parseAndValidateDouble(fixedString.substr(decimalIndex + 1, fixedString.length - (decimalIndex + 1)));
      fracValue = ~~((~~Math.max(Math.min(fracValue, 2147483647), -2147483648) + 500) / 1000);
      if (fracValue >= power) {
        fracValue -= power;
        ++intValue;
      }
    }
  }
  fractionPresent = this$static.minimumFractionDigits > 0 || fracValue > 0;
  intPart = '' + intValue;
  grouping = this$static.isCurrencyFormat?',':this$static.numberConstants.groupingSeparator;
  decimal = this$static.isCurrencyFormat?'.':this$static.numberConstants.decimalSeparator;
  digitLen = intPart.length;
  if (intValue > 0 || minIntDigits > 0) {
    for (i = digitLen; i < minIntDigits; ++i) {
      $append_0(result.data, '0');
    }
    for (i = 0; i < digitLen; ++i) {
      $append_1(result, intPart.charCodeAt(i));
      digitLen - i > 1 && this$static.groupingSize > 0 && (digitLen - i) % this$static.groupingSize == 1 && $append_0(result.data, grouping);
    }
  }
   else 
    !fractionPresent && $append_0(result.data, '0');
  (this$static.decimalSeparatorAlwaysShown || fractionPresent) && $append_0(result.data, decimal);
  fracPart = '' + Math.floor(fracValue + power + 0.5);
  fracLen = fracPart.length;
  while (fracPart.charCodeAt(fracLen - 1) == 48 && fracLen > this$static.minimumFractionDigits + 1) {
    --fracLen;
  }
  for (i = 1; i < fracLen; ++i) {
    $append_1(result, fracPart.charCodeAt(i));
  }
}

function NumberFormat(){
}

_ = NumberFormat.prototype = new Object_0;
_.typeId$ = 0;
_.currencyCode = null;
_.currencySymbol = null;
_.decimalSeparatorAlwaysShown = false;
_.groupingSize = 3;
_.isCurrencyFormat = false;
_.maximumFractionDigits = 3;
_.maximumIntegerDigits = 40;
_.minExponentDigits = 0;
_.minimumFractionDigits = 0;
_.minimumIntegerDigits = 1;
_.multiplier = 1;
_.negativePrefix = '-';
_.negativeSuffix = '';
_.numberConstants = null;
_.pattern = null;
_.positivePrefix = '';
_.positiveSuffix = '';
_.useExponentialNotation = false;
function $getRFCTimeZoneString(this$static){
  var data, offset;
  offset = -this$static.standardOffset;
  data = initValues(_3C_classLit, 32, -1, [43, 48, 48, 48, 48]);
  if (offset < 0) {
    data[0] = 45;
    offset = -offset;
  }
  data[1] += ~~(~~(offset / 60) / 10);
  data[2] += ~~(offset / 60) % 10;
  data[3] += ~~(offset % 60 / 10);
  data[4] += offset % 10;
  return String.fromCharCode.apply(null, data);
}

function composeGMTString(offset){
  var data;
  data = initValues(_3C_classLit, 32, -1, [71, 77, 84, 45, 48, 48, 58, 48, 48]);
  if (offset <= 0) {
    data[3] = 43;
    offset = -offset;
  }
  data[4] += ~~(~~(offset / 60) / 10);
  data[5] += ~~(offset / 60) % 10;
  data[7] += ~~(offset % 60 / 10);
  data[8] += offset % 10;
  return String.fromCharCode.apply(null, data);
}

function composePOSIXTimeZoneID(offset){
  var str;
  if (offset == 0) {
    return 'Etc/GMT';
  }
  if (offset < 0) {
    offset = -offset;
    str = 'Etc/GMT-';
  }
   else {
    str = 'Etc/GMT+';
  }
  return str + offsetDisplay(offset);
}

function composeUTCString(offset){
  var str;
  if (offset == 0) {
    return 'UTC';
  }
  if (offset < 0) {
    offset = -offset;
    str = 'UTC+';
  }
   else {
    str = 'UTC-';
  }
  return str + offsetDisplay(offset);
}

function createTimeZone(timeZoneOffsetInMinutes){
  var tz;
  tz = new TimeZone;
  tz.standardOffset = timeZoneOffsetInMinutes;
  tz.timezoneID = composePOSIXTimeZoneID(timeZoneOffsetInMinutes);
  tz.tzNames = initDim(_3Ljava_lang_String_2_classLit, 40, 1, 2, 0);
  tz.tzNames[0] = composeUTCString(timeZoneOffsetInMinutes);
  tz.tzNames[1] = composeUTCString(timeZoneOffsetInMinutes);
  return tz;
}

function offsetDisplay(offset){
  var hour, mins;
  hour = ~~(offset / 60);
  mins = offset % 60;
  if (mins == 0) {
    return '' + hour;
  }
  return '' + hour + ':' + mins;
}

function TimeZone(){
}

_ = TimeZone.prototype = new Object_0;
_.typeId$ = 0;
_.standardOffset = 0;
_.timezoneID = null;
_.tzNames = null;
function $DateTimeConstantsImpl_(this$static){
  this$static.cache = $HashMap(new HashMap);
  return this$static;
}

function $ampms(this$static){
  var args, writer;
  args = dynamicCast($get_1(this$static.cache, 'ampms'), 4);
  if (args == null) {
    writer = initValues(_3Ljava_lang_String_2_classLit, 40, 1, ['AM', 'PM']);
    $put(this$static.cache, 'ampms', writer);
    return writer;
  }
   else {
    return args;
  }
}

function $eraNames(this$static){
  var args, writer;
  args = dynamicCast($get_1(this$static.cache, 'eraNames'), 4);
  if (args == null) {
    writer = initValues(_3Ljava_lang_String_2_classLit, 40, 1, ['Before Christ', 'Anno Domini']);
    $put(this$static.cache, 'eraNames', writer);
    return writer;
  }
   else {
    return args;
  }
}

function $eras(this$static){
  var args, writer;
  args = dynamicCast($get_1(this$static.cache, 'eras'), 4);
  if (args == null) {
    writer = initValues(_3Ljava_lang_String_2_classLit, 40, 1, ['BC', 'AD']);
    $put(this$static.cache, 'eras', writer);
    return writer;
  }
   else {
    return args;
  }
}

function $months(this$static){
  var args, writer;
  args = dynamicCast($get_1(this$static.cache, 'months'), 4);
  if (args == null) {
    writer = initValues(_3Ljava_lang_String_2_classLit, 40, 1, ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']);
    $put(this$static.cache, 'months', writer);
    return writer;
  }
   else {
    return args;
  }
}

function $narrowMonths(this$static){
  var args, writer;
  args = dynamicCast($get_1(this$static.cache, 'narrowMonths'), 4);
  if (args == null) {
    writer = initValues(_3Ljava_lang_String_2_classLit, 40, 1, ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D']);
    $put(this$static.cache, 'narrowMonths', writer);
    return writer;
  }
   else {
    return args;
  }
}

function $narrowWeekdays(this$static){
  var args, writer;
  args = dynamicCast($get_1(this$static.cache, 'narrowWeekdays'), 4);
  if (args == null) {
    writer = initValues(_3Ljava_lang_String_2_classLit, 40, 1, ['S', 'M', 'T', 'W', 'T', 'F', 'S']);
    $put(this$static.cache, 'narrowWeekdays', writer);
    return writer;
  }
   else {
    return args;
  }
}

function $quarters(this$static){
  var args, writer;
  args = dynamicCast($get_1(this$static.cache, 'quarters'), 4);
  if (args == null) {
    writer = initValues(_3Ljava_lang_String_2_classLit, 40, 1, ['1st quarter', '2nd quarter', '3rd quarter', '4th quarter']);
    $put(this$static.cache, 'quarters', writer);
    return writer;
  }
   else {
    return args;
  }
}

function $shortMonths(this$static){
  var args, writer;
  args = dynamicCast($get_1(this$static.cache, 'shortMonths'), 4);
  if (args == null) {
    writer = initValues(_3Ljava_lang_String_2_classLit, 40, 1, ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']);
    $put(this$static.cache, 'shortMonths', writer);
    return writer;
  }
   else {
    return args;
  }
}

function $shortQuarters(this$static){
  var args, writer;
  args = dynamicCast($get_1(this$static.cache, 'shortQuarters'), 4);
  if (args == null) {
    writer = initValues(_3Ljava_lang_String_2_classLit, 40, 1, ['Q1', 'Q2', 'Q3', 'Q4']);
    $put(this$static.cache, 'shortQuarters', writer);
    return writer;
  }
   else {
    return args;
  }
}

function $shortWeekdays(this$static){
  var args, writer;
  args = dynamicCast($get_1(this$static.cache, 'shortWeekdays'), 4);
  if (args == null) {
    writer = initValues(_3Ljava_lang_String_2_classLit, 40, 1, ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']);
    $put(this$static.cache, 'shortWeekdays', writer);
    return writer;
  }
   else {
    return args;
  }
}

function $standaloneMonths(this$static){
  var args, writer;
  args = dynamicCast($get_1(this$static.cache, 'standaloneMonths'), 4);
  if (args == null) {
    writer = initValues(_3Ljava_lang_String_2_classLit, 40, 1, ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']);
    $put(this$static.cache, 'standaloneMonths', writer);
    return writer;
  }
   else {
    return args;
  }
}

function $standaloneNarrowMonths(this$static){
  var args, writer;
  args = dynamicCast($get_1(this$static.cache, 'standaloneNarrowMonths'), 4);
  if (args == null) {
    writer = initValues(_3Ljava_lang_String_2_classLit, 40, 1, ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D']);
    $put(this$static.cache, 'standaloneNarrowMonths', writer);
    return writer;
  }
   else {
    return args;
  }
}

function $standaloneNarrowWeekdays(this$static){
  var args, writer;
  args = dynamicCast($get_1(this$static.cache, 'standaloneNarrowWeekdays'), 4);
  if (args == null) {
    writer = initValues(_3Ljava_lang_String_2_classLit, 40, 1, ['S', 'M', 'T', 'W', 'T', 'F', 'S']);
    $put(this$static.cache, 'standaloneNarrowWeekdays', writer);
    return writer;
  }
   else {
    return args;
  }
}

function $standaloneShortMonths(this$static){
  var args, writer;
  args = dynamicCast($get_1(this$static.cache, 'standaloneShortMonths'), 4);
  if (args == null) {
    writer = initValues(_3Ljava_lang_String_2_classLit, 40, 1, ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']);
    $put(this$static.cache, 'standaloneShortMonths', writer);
    return writer;
  }
   else {
    return args;
  }
}

function $standaloneShortWeekdays(this$static){
  var args, writer;
  args = dynamicCast($get_1(this$static.cache, 'standaloneShortWeekdays'), 4);
  if (args == null) {
    writer = initValues(_3Ljava_lang_String_2_classLit, 40, 1, ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']);
    $put(this$static.cache, 'standaloneShortWeekdays', writer);
    return writer;
  }
   else {
    return args;
  }
}

function $standaloneWeekdays(this$static){
  var args, writer;
  args = dynamicCast($get_1(this$static.cache, 'standaloneWeekdays'), 4);
  if (args == null) {
    writer = initValues(_3Ljava_lang_String_2_classLit, 40, 1, ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']);
    $put(this$static.cache, 'standaloneWeekdays', writer);
    return writer;
  }
   else {
    return args;
  }
}

function $weekdays(this$static){
  var args, writer;
  args = dynamicCast($get_1(this$static.cache, 'weekdays'), 4);
  if (args == null) {
    writer = initValues(_3Ljava_lang_String_2_classLit, 40, 1, ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']);
    $put(this$static.cache, 'weekdays', writer);
    return writer;
  }
   else {
    return args;
  }
}

function DateTimeConstantsImpl_(){
}

_ = DateTimeConstantsImpl_.prototype = new Object_0;
_.typeId$ = 0;
function NumberConstantsImpl_(){
}

_ = NumberConstantsImpl_.prototype = new Object_0;
_.typeId$ = 0;
function $clinit_36(){
  $clinit_36 = nullMethod;
  initValues(_3Ljava_lang_String_2_classLit, 40, 1, ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']);
  initValues(_3Ljava_lang_String_2_classLit, 40, 1, ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']);
}

function $Date(this$static){
  $clinit_36();
  this$static.jsdate = new Date;
  return this$static;
}

function $Date_0(this$static, date){
  $clinit_36();
  this$static.jsdate = new Date(date[1] + date[0]);
  return this$static;
}

function $getTime0(this$static){
  this$static.checkJsDate();
  return this$static.jsdate.getTime();
}

function $setDate(this$static, date){
  this$static.checkJsDate();
  var hours = this$static.jsdate.getHours();
  this$static.jsdate.setDate(date);
  this$static.fixDaylightSavings(hours);
}

function $setTime0(this$static, time){
  this$static.checkJsDate();
  this$static.jsdate.setTime(time);
}

function checkJsDate(){
  (!this.jsdate || typeof this.jsdate != 'object') && throwJsDateException('' + this.jsdate);
}

function equals_1(obj){
  return obj != null && canCast(obj.typeId$, 15) && eq(fromDouble((this.checkJsDate() , this.jsdate.getTime())), fromDouble($getTime0(dynamicCast(obj, 15))));
}

function fixDaylightSavings(hours){
  if (this.jsdate.getHours() % 24 != hours % 24) {
    var d = new Date;
    d.setTime(this.jsdate.getTime());
    var noff = d.getTimezoneOffset();
    d.setDate(d.getDate() + 1);
    var loff = d.getTimezoneOffset();
    var timeDiff = noff - loff;
    if (timeDiff > 0) {
      var year = this.jsdate.getYear() + 1900;
      var month = this.jsdate.getMonth();
      var day = this.jsdate.getDate();
      var badHours = this.jsdate.getHours();
      var minute = this.jsdate.getMinutes();
      var second = this.jsdate.getSeconds();
      badHours + timeDiff / 60 >= 24 && day++;
      var newTime = new Date(year, month, day, hours + timeDiff / 60, minute + timeDiff % 60, second);
      this.jsdate.setTime(newTime.getTime());
    }
  }
}

function hashCode_2(){
  return lowBits_0(xor(fromDouble((this.checkJsDate() , this.jsdate.getTime())), shru(fromDouble((this.checkJsDate() , this.jsdate.getTime())), 32)));
}

function setHours(hours){
  this.checkJsDate();
  this.jsdate.setHours(hours);
  this.fixDaylightSavings(hours);
}

function setMinutes(minutes){
  this.checkJsDate();
  var hours = this.jsdate.getHours() + minutes / 60;
  this.jsdate.setMinutes(minutes);
  this.fixDaylightSavings(hours);
}

function setMonth(month){
  this.checkJsDate();
  var hours = this.jsdate.getHours();
  this.jsdate.setMonth(month);
  this.fixDaylightSavings(hours);
}

function setSeconds(seconds){
  this.checkJsDate();
  var hours = this.jsdate.getHours() + seconds / (60 * 60);
  this.jsdate.setSeconds(seconds);
  this.fixDaylightSavings(hours);
}

function setYear(year){
  this.checkJsDate();
  var hours = this.jsdate.getHours();
  this.jsdate.setFullYear(year + 1900);
  this.fixDaylightSavings(hours);
}

function throwJsDateException(val){
  throw $IllegalStateException(new IllegalStateException, 'jsdate is ' + val);
}

function Date_0(){
}

_ = Date_0.prototype = new Object_0;
_.checkJsDate = checkJsDate;
_.equals$ = equals_1;
_.fixDaylightSavings = fixDaylightSavings;
_.hashCode$ = hashCode_2;
_.setHours_0 = setHours;
_.setMinutes_0 = setMinutes;
_.setMonth_0 = setMonth;
_.setSeconds_0 = setSeconds;
_.setYear = setYear;
_.typeId$ = 7;
_.jsdate = null;
function $clinit_37(){
  $clinit_37 = nullMethod;
  $clinit_36();
}

function $DateRecord(this$static){
  $clinit_37();
  this$static.jsdate = new Date;
  this$static.era = -1;
  this$static.ambiguousYear = false;
  this$static.year = -2147483648;
  this$static.month = -1;
  this$static.dayOfMonth = -1;
  this$static.ampm = -1;
  this$static.hours = -1;
  this$static.minutes = -1;
  this$static.seconds = -1;
  this$static.milliseconds = -1;
  this$static.dayOfWeek = -1;
  this$static.tzOffset = -2147483648;
  return this$static;
}

function $calcDate(this$static, date, strict){
  var adjustment, defaultCenturyStart, offset, orgDayOfMonth, orgMonth;
  this$static.era == 0 && this$static.year > 0 && (this$static.year = -(this$static.year - 1));
  this$static.year > -2147483648 && date.setYear(this$static.year - 1900);
  orgDayOfMonth = (date.checkJsDate() , date.jsdate.getDate());
  $setDate(date, 1);
  this$static.month >= 0 && date.setMonth_0(this$static.month);
  this$static.dayOfMonth >= 0?$setDate(date, this$static.dayOfMonth):$setDate(date, orgDayOfMonth);
  this$static.hours < 0 && (this$static.hours = (date.checkJsDate() , date.jsdate.getHours()));
  this$static.ampm > 0 && this$static.hours < 12 && (this$static.hours += 12);
  date.setHours_0(this$static.hours);
  this$static.minutes >= 0 && date.setMinutes_0(this$static.minutes);
  this$static.seconds >= 0 && date.setSeconds_0(this$static.seconds);
  this$static.milliseconds >= 0 && $setTime0(date, toDouble(add(mul(div(fromDouble((date.checkJsDate() , date.jsdate.getTime())), P3e8_longLit), P3e8_longLit), fromInt(this$static.milliseconds))));
  if (strict) {
    if (this$static.year > -2147483648 && this$static.year - 1900 != (date.checkJsDate() , date.jsdate.getFullYear() - 1900)) {
      return false;
    }
    if (this$static.month >= 0 && this$static.month != (date.checkJsDate() , date.jsdate.getMonth())) {
      return false;
    }
    if (this$static.dayOfMonth >= 0 && this$static.dayOfMonth != (date.checkJsDate() , date.jsdate.getDate())) {
      return false;
    }
    if (this$static.hours >= 24) {
      return false;
    }
    if (this$static.minutes >= 60) {
      return false;
    }
    if (this$static.seconds >= 60) {
      return false;
    }
    if (this$static.milliseconds >= 1000) {
      return false;
    }
  }
  if (this$static.tzOffset > -2147483648) {
    offset = (date.checkJsDate() , date.jsdate.getTimezoneOffset());
    $setTime0(date, toDouble(add(fromDouble((date.checkJsDate() , date.jsdate.getTime())), fromInt((this$static.tzOffset - offset) * 60 * 1000))));
  }
  if (this$static.ambiguousYear) {
    defaultCenturyStart = $Date(new Date_0);
    defaultCenturyStart.setYear((defaultCenturyStart.checkJsDate() , defaultCenturyStart.jsdate.getFullYear() - 1900) - 80);
    compare(fromDouble((date.checkJsDate() , date.jsdate.getTime())), fromDouble((defaultCenturyStart.checkJsDate() , defaultCenturyStart.jsdate.getTime()))) < 0 && date.setYear((defaultCenturyStart.checkJsDate() , defaultCenturyStart.jsdate.getFullYear() - 1900) + 100);
  }
  if (this$static.dayOfWeek >= 0) {
    if (this$static.dayOfMonth == -1) {
      adjustment = (7 + this$static.dayOfWeek - (date.checkJsDate() , date.jsdate.getDay())) % 7;
      adjustment > 3 && (adjustment -= 7);
      orgMonth = (date.checkJsDate() , date.jsdate.getMonth());
      $setDate(date, (date.checkJsDate() , date.jsdate.getDate()) + adjustment);
      (date.checkJsDate() , date.jsdate.getMonth()) != orgMonth && $setDate(date, (date.checkJsDate() , date.jsdate.getDate()) + (adjustment > 0?-7:7));
    }
     else {
      if ((date.checkJsDate() , date.jsdate.getDay()) != this$static.dayOfWeek) {
        return false;
      }
    }
  }
  return true;
}

function setHours_0(hours){
  this.hours = hours;
}

function setMinutes_0(minutes){
  this.minutes = minutes;
}

function setMonth_0(month){
  this.month = month;
}

function setSeconds_0(seconds){
  this.seconds = seconds;
}

function setYear_0(value){
  this.year = value;
}

function DateRecord(){
}

_ = DateRecord.prototype = new Date_0;
_.setHours_0 = setHours_0;
_.setMinutes_0 = setMinutes_0;
_.setMonth_0 = setMonth_0;
_.setSeconds_0 = setSeconds_0;
_.setYear = setYear_0;
_.typeId$ = 8;
_.ambiguousYear = false;
_.ampm = 0;
_.dayOfMonth = 0;
_.dayOfWeek = 0;
_.era = 0;
_.hours = 0;
_.milliseconds = 0;
_.minutes = 0;
_.month = 0;
_.seconds = 0;
_.tzOffset = 0;
_.year = 0;
function createFromSeed(seedType, length_0){
  var array = new Array(length_0);
  if (seedType > 0) {
    var value = [null, 0, false, [0, 0]][seedType];
    for (var i = 0; i < length_0; ++i) {
      array[i] = value;
    }
  }
  return array;
}

function initDim(arrayClass, typeId, queryId, length_0, seedType){
  var result;
  result = createFromSeed(seedType, length_0);
  $clinit_39();
  wrapArray(result, expandoNames_0, expandoValues_0);
  result.typeId$ = typeId;
  result.queryId$ = queryId;
  return result;
}

function initValues(arrayClass, typeId, queryId, array){
  $clinit_39();
  wrapArray(array, expandoNames_0, expandoValues_0);
  array.typeId$ = typeId;
  array.queryId$ = queryId;
  return array;
}

function setCheck(array, index, value){
  if (value != null) {
    if (array.queryId$ > 0 && !canCastUnsafe(value.typeId$, array.queryId$)) {
      throw $ArrayStoreException(new ArrayStoreException);
    }
    if (array.queryId$ < 0 && (value.typeMarker$ == nullMethod || value.typeId$ == 2)) {
      throw $ArrayStoreException(new ArrayStoreException);
    }
  }
  return array[index] = value;
}

function Array_0(){
}

_ = Array_0.prototype = new Object_0;
_.typeId$ = 0;
_.length = 0;
_.queryId$ = 0;
function $clinit_39(){
  $clinit_39 = nullMethod;
  expandoNames_0 = [];
  expandoValues_0 = [];
  initExpandos(new Array_0, expandoNames_0, expandoValues_0);
}

function initExpandos(protoType, expandoNames, expandoValues){
  var i = 0, value;
  for (var name_0 in protoType) {
    if (value = protoType[name_0]) {
      expandoNames[i] = name_0;
      expandoValues[i] = value;
      ++i;
    }
  }
}

function wrapArray(array, expandoNames, expandoValues){
  $clinit_39();
  for (var i = 0, c = expandoNames.length; i < c; ++i) {
    array[expandoNames[i]] = expandoValues[i];
  }
}

var expandoNames_0, expandoValues_0;
function canCast(srcId, dstId){
  return srcId && !!typeIdArray[srcId][dstId];
}

function canCastUnsafe(srcId, dstId){
  return srcId && typeIdArray[srcId][dstId];
}

function dynamicCast(src, dstId){
  if (src != null && !canCastUnsafe(src.typeId$, dstId)) {
    throw $ClassCastException(new ClassCastException);
  }
  return src;
}

function dynamicCastJso(src){
  if (src != null && (src.typeMarker$ == nullMethod || src.typeId$ == 2)) {
    throw $ClassCastException(new ClassCastException);
  }
  return src;
}

function instanceOf(src, dstId){
  return src != null && canCast(src.typeId$, dstId);
}

function instanceOfJso(src){
  return src != null && src.typeMarker$ != nullMethod && src.typeId$ != 2;
}

function round_int(x){
  return ~~Math.max(Math.min(x, 2147483647), -2147483648);
}

var typeIdArray = [{}, {}, {1:1, 10:1, 11:1, 12:1}, {5:1, 10:1}, {5:1, 10:1}, {2:1, 5:1, 10:1}, {2:1, 5:1, 10:1}, {10:1, 12:1, 15:1}, {10:1, 12:1, 15:1}, {2:1, 5:1, 10:1}, {2:1, 5:1, 10:1}, {5:1, 10:1}, {5:1, 10:1}, {2:1, 5:1, 10:1}, {2:1, 5:1, 10:1}, {2:1, 5:1, 10:1}, {2:1, 5:1, 10:1}, {2:1, 5:1, 10:1}, {2:1, 3:1, 5:1, 10:1}, {10:1, 13:1}, {11:1}, {2:1, 5:1, 10:1}, {9:1}, {6:1}, {6:1}, {6:1}, {7:1, 10:1}, {8:1, 10:1}, {6:1}, {2:1, 5:1, 10:1}, {16:1}, {17:1}, {10:1}, {10:1, 14:1}, {10:1}, {10:1}, {10:1}, {10:1}, {10:1}, {10:1}, {4:1, 10:1}, {10:1}];
function init(){
  !!$stats && $stats({moduleName:$moduleName, sessionId:$sessionId, subSystem:'startup', evtGroup:'moduleStartup', millis:(new Date).getTime(), type:'onModuleLoadStart', className:'tersus.gwt.client.Util'});
  $clinit_99();
  $wnd.gwtFormatNumber = formatNumber;
  $wnd.gwtParseNumber = parseNumber;
  $wnd.gwtFormatDateAndTime = formatDateAndTime;
  $wnd.gwtParseDateAndTime = parseDateAndTime;
}

function caught(e){
  if (e != null && canCast(e.typeId$, 5)) {
    return e;
  }
  return $JavaScriptException(new JavaScriptException, e);
}

function add(a, b){
  var newHigh, newLow;
  newHigh = a[1] + b[1];
  newLow = a[0] + b[0];
  return create(newLow, newHigh);
}

function addTimes(accum, a, b){
  if (a == 0) {
    return accum;
  }
  if (b == 0) {
    return accum;
  }
  return add(accum, create(a * b, 0));
}

function compare(a, b){
  var nega, negb;
  if (a[0] == b[0] && a[1] == b[1]) {
    return 0;
  }
  nega = a[1] < 0;
  negb = b[1] < 0;
  if (nega && !negb) {
    return -1;
  }
  if (!nega && negb) {
    return 1;
  }
  if (sub(a, b)[1] < 0) {
    return -1;
  }
   else {
    return 1;
  }
}

function create(valueLow, valueHigh){
  var diffHigh, diffLow;
  valueHigh %= 1.8446744073709552E19;
  valueLow %= 1.8446744073709552E19;
  diffHigh = valueHigh % 4294967296;
  diffLow = Math.floor(valueLow / 4294967296) * 4294967296;
  valueHigh = valueHigh - diffHigh + diffLow;
  valueLow = valueLow - diffLow + diffHigh;
  while (valueLow < 0) {
    valueLow += 4294967296;
    valueHigh -= 4294967296;
  }
  while (valueLow > 4294967295) {
    valueLow -= 4294967296;
    valueHigh += 4294967296;
  }
  valueHigh = valueHigh % 1.8446744073709552E19;
  while (valueHigh > 9223372032559808512) {
    valueHigh -= 1.8446744073709552E19;
  }
  while (valueHigh < -9223372036854775808) {
    valueHigh += 1.8446744073709552E19;
  }
  return [valueLow, valueHigh];
}

function div(a, b){
  var approx, deltaRem, deltaResult, halfa, rem, result;
  if (b[0] == 0 && b[1] == 0) {
    throw $ArithmeticException(new ArithmeticException, '/ by zero');
  }
  if (a[0] == 0 && a[1] == 0) {
    return $clinit_46() , ZERO;
  }
  if (eq(a, ($clinit_46() , MIN_VALUE))) {
    if (eq(b, ONE) || eq(b, NEG_ONE)) {
      return MIN_VALUE;
    }
    halfa = shr(a, 1);
    approx = shl(div(halfa, b), 1);
    rem = sub(a, mul(b, approx));
    return add(approx, div(rem, b));
  }
  if (eq(b, MIN_VALUE)) {
    return ZERO;
  }
  if (a[1] < 0) {
    if (b[1] < 0) {
      return div(neg(a), neg(b));
    }
     else {
      return neg(div(neg(a), b));
    }
  }
  if (b[1] < 0) {
    return neg(div(a, neg(b)));
  }
  result = ZERO;
  rem = a;
  while (compare(rem, b) >= 0) {
    deltaResult = fromDouble(Math.floor(toDoubleRoundDown(rem) / toDoubleRoundUp(b)));
    deltaResult[0] == 0 && deltaResult[1] == 0 && (deltaResult = ONE);
    deltaRem = mul(deltaResult, b);
    result = add(result, deltaResult);
    rem = sub(rem, deltaRem);
  }
  return result;
}

function eq(a, b){
  return a[0] == b[0] && a[1] == b[1];
}

function fromDouble(value){
  if (isNaN(value)) {
    return $clinit_46() , ZERO;
  }
  if (value < -9223372036854775808) {
    return $clinit_46() , MIN_VALUE;
  }
  if (value >= 9223372036854775807) {
    return $clinit_46() , MAX_VALUE;
  }
  if (value > 0) {
    return create(Math.floor(value), 0);
  }
   else {
    return create(Math.ceil(value), 0);
  }
}

function fromInt(value){
  var rebase, result;
  if (value > -129 && value < 128) {
    rebase = value + 128;
    result = ($clinit_45() , boxedValues)[rebase];
    result == null && (result = boxedValues[rebase] = internalFromInt(value));
    return result;
  }
  return internalFromInt(value);
}

function internalFromInt(value){
  if (value >= 0) {
    return [value, 0];
  }
   else {
    return [value + 4294967296, -4294967296];
  }
}

function lowBits_0(a){
  if (a[0] >= 2147483648) {
    return ~~Math.max(Math.min(a[0] - 4294967296, 2147483647), -2147483648);
  }
   else {
    return ~~Math.max(Math.min(a[0], 2147483647), -2147483648);
  }
}

function makeFromBits(highBits, lowBits){
  var high, low;
  high = highBits * 4294967296;
  low = lowBits;
  lowBits < 0 && (low += 4294967296);
  return [low, high];
}

function mod(a, b){
  return sub(a, mul(div(a, b), b));
}

function mul(a, b){
  var a1, a2, a3, a4, b1, b2, b3, b4, res;
  if (a[0] == 0 && a[1] == 0) {
    return $clinit_46() , ZERO;
  }
  if (b[0] == 0 && b[1] == 0) {
    return $clinit_46() , ZERO;
  }
  if (eq(a, ($clinit_46() , MIN_VALUE))) {
    return multByMinValue(b);
  }
  if (eq(b, MIN_VALUE)) {
    return multByMinValue(a);
  }
  if (a[1] < 0) {
    if (b[1] < 0) {
      return mul(neg(a), neg(b));
    }
     else {
      return neg(mul(neg(a), b));
    }
  }
  if (b[1] < 0) {
    return neg(mul(a, neg(b)));
  }
  if (compare(a, TWO_PWR_24) < 0 && compare(b, TWO_PWR_24) < 0) {
    return create((a[1] + a[0]) * (b[1] + b[0]), 0);
  }
  a3 = a[1] % 281474976710656;
  a4 = a[1] - a3;
  a1 = a[0] % 65536;
  a2 = a[0] - a1;
  b3 = b[1] % 281474976710656;
  b4 = b[1] - b3;
  b1 = b[0] % 65536;
  b2 = b[0] - b1;
  res = ZERO;
  res = addTimes(res, a4, b1);
  res = addTimes(res, a3, b2);
  res = addTimes(res, a3, b1);
  res = addTimes(res, a2, b3);
  res = addTimes(res, a2, b2);
  res = addTimes(res, a2, b1);
  res = addTimes(res, a1, b4);
  res = addTimes(res, a1, b3);
  res = addTimes(res, a1, b2);
  res = addTimes(res, a1, b1);
  return res;
}

function multByMinValue(a){
  if ((lowBits_0(a) & 1) == 1) {
    return $clinit_46() , MIN_VALUE;
  }
   else {
    return $clinit_46() , ZERO;
  }
}

function neg(a){
  var newHigh, newLow;
  if (eq(a, ($clinit_46() , MIN_VALUE))) {
    return MIN_VALUE;
  }
  newHigh = -a[1];
  newLow = -a[0];
  if (newLow > 4294967295) {
    newLow -= 4294967296;
    newHigh += 4294967296;
  }
  if (newLow < 0) {
    newLow += 4294967296;
    newHigh -= 4294967296;
  }
  return [newLow, newHigh];
}

function pwrAsDouble(n){
  if (n <= 30) {
    return 1 << n;
  }
   else {
    return pwrAsDouble(30) * pwrAsDouble(n - 30);
  }
}

function shl(a, n){
  var diff, newHigh, newLow, twoToN;
  n &= 63;
  if (eq(a, ($clinit_46() , MIN_VALUE))) {
    if (n == 0) {
      return a;
    }
     else {
      return ZERO;
    }
  }
  if (a[1] < 0) {
    return neg(shl(neg(a), n));
  }
  twoToN = pwrAsDouble(n);
  newHigh = a[1] * twoToN % 1.8446744073709552E19;
  newLow = a[0] * twoToN;
  diff = newLow - newLow % 4294967296;
  newHigh += diff;
  newLow -= diff;
  newHigh >= 9223372036854775807 && (newHigh -= 1.8446744073709552E19);
  return [newLow, newHigh];
}

function shr(a, n){
  var newHigh, newLow, shiftFact;
  n &= 63;
  shiftFact = pwrAsDouble(n);
  newHigh = Math.floor(a[1] / shiftFact);
  newLow = Math.floor(a[0] / shiftFact);
  return create(newLow, newHigh);
}

function shru(a, n){
  var sr;
  n &= 63;
  sr = shr(a, n);
  a[1] < 0 && (sr = add(sr, shl(($clinit_46() , TWO), 63 - n)));
  return sr;
}

function sub(a, b){
  var newHigh, newLow;
  newHigh = a[1] - b[1];
  newLow = a[0] - b[0];
  return create(newLow, newHigh);
}

function toDouble(a){
  return a[1] + a[0];
}

function toDoubleRoundDown(a){
  var diff, magnitute, toSubtract;
  magnitute = round_int(Math.log(a[1]) / ($clinit_46() , LN_2));
  if (magnitute <= 48) {
    return a[1] + a[0];
  }
   else {
    diff = magnitute - 48;
    toSubtract = (1 << diff) - 1;
    return a[1] + (a[0] - toSubtract);
  }
}

function toDoubleRoundUp(a){
  var diff, magnitute, toAdd;
  magnitute = round_int(Math.log(a[1]) / ($clinit_46() , LN_2));
  if (magnitute <= 48) {
    return a[1] + a[0];
  }
   else {
    diff = magnitute - 48;
    toAdd = (1 << diff) - 1;
    return a[1] + (a[0] + toAdd);
  }
}

function xor(a, b){
  return makeFromBits(~~Math.max(Math.min(a[1] / 4294967296, 2147483647), -2147483648) ^ ~~Math.max(Math.min(b[1] / 4294967296, 2147483647), -2147483648), lowBits_0(a) ^ lowBits_0(b));
}

function $clinit_45(){
  $clinit_45 = nullMethod;
  boxedValues = initDim(_3_3D_classLit, 41, 14, 256, 0);
}

var boxedValues;
function $clinit_46(){
  $clinit_46 = nullMethod;
  LN_2 = Math.log(2);
  MAX_VALUE = P7fffffffffffffff_longLit;
  MIN_VALUE = N8000000000000000_longLit;
  NEG_ONE = fromInt(-1);
  ONE = fromInt(1);
  TWO = fromInt(2);
  TWO_PWR_24 = P1000000_longLit;
  ZERO = fromInt(0);
}

var LN_2, MAX_VALUE, MIN_VALUE, NEG_ONE, ONE, TWO, TWO_PWR_24, ZERO;
function $ArithmeticException(this$static, explanation){
  $fillInStackTrace();
  return this$static;
}

function ArithmeticException(){
}

_ = ArithmeticException.prototype = new RuntimeException;
_.typeId$ = 9;
function $ArrayStoreException(this$static){
  $fillInStackTrace();
  return this$static;
}

function ArrayStoreException(){
}

_ = ArrayStoreException.prototype = new RuntimeException;
_.typeId$ = 10;
function Class(){
}

_ = Class.prototype = new Object_0;
_.typeId$ = 0;
function $ClassCastException(this$static){
  $fillInStackTrace();
  return this$static;
}

function ClassCastException(){
}

_ = ClassCastException.prototype = new RuntimeException;
_.typeId$ = 13;
function __parseAndValidateDouble(s){
  var toReturn;
  toReturn = __parseDouble(s);
  if (isNaN(toReturn)) {
    throw $NumberFormatException(new NumberFormatException, 'For input string: "' + s + '"');
  }
  return toReturn;
}

function __parseDouble(str){
  var floatRegex = floatRegex_0;
  !floatRegex && (floatRegex = floatRegex_0 = /^\s*[+-]?\d*\.?\d*([eE][+-]?\d+)?\s*$/i);
  if (floatRegex.test(str)) {
    return parseFloat(str);
  }
   else {
    return Number.NaN;
  }
}

var floatRegex_0 = null;
function $IllegalArgumentException(this$static, message){
  $fillInStackTrace();
  return this$static;
}

function IllegalArgumentException(){
}

_ = IllegalArgumentException.prototype = new RuntimeException;
_.typeId$ = 14;
function $IllegalStateException(this$static, s){
  $fillInStackTrace();
  return this$static;
}

function IllegalStateException(){
}

_ = IllegalStateException.prototype = new RuntimeException;
_.typeId$ = 15;
function $IndexOutOfBoundsException(this$static, message){
  $fillInStackTrace();
  return this$static;
}

function IndexOutOfBoundsException(){
}

_ = IndexOutOfBoundsException.prototype = new RuntimeException;
_.typeId$ = 16;
function floor(x){
  return Math.floor(x);
}

function $NullPointerException(this$static){
  $fillInStackTrace();
  return this$static;
}

function NullPointerException(){
}

_ = NullPointerException.prototype = new RuntimeException;
_.typeId$ = 17;
function $NumberFormatException(this$static, message){
  $fillInStackTrace();
  return this$static;
}

function NumberFormatException(){
}

_ = NumberFormatException.prototype = new IllegalArgumentException;
_.typeId$ = 18;
function $StackTraceElement(this$static, className, methodName, fileName, lineNumber){
  return this$static;
}

function StackTraceElement(){
}

_ = StackTraceElement.prototype = new Object_0;
_.typeId$ = 19;
function $endsWith(this$static, suffix){
  return this$static.lastIndexOf(suffix) != -1 && this$static.lastIndexOf(suffix) == this$static.length - suffix.length;
}

function $equals_1(this$static, other){
  if (!(other != null && canCast(other.typeId$, 1))) {
    return false;
  }
  return String(this$static) == other;
}

function $startsWith(this$static, prefix, toffset){
  if (toffset < 0 || toffset >= this$static.length) {
    return false;
  }
   else {
    return this$static.indexOf(prefix, toffset) == toffset;
  }
}

function $substring_0(this$static, beginIndex, endIndex){
  return this$static.substr(beginIndex, endIndex - beginIndex);
}

function $trim(this$static){
  if (this$static.length == 0 || this$static[0] > ' ' && this$static[this$static.length - 1] > ' ') {
    return this$static;
  }
  var r1 = this$static.replace(/^(\s*)/, '');
  var r2 = r1.replace(/\s*$/, '');
  return r2;
}

function equals_2(other){
  return $equals_1(this, other);
}

function fromCodePoint(codePoint){
  var hiSurrogate, loSurrogate;
  if (codePoint >= 65536) {
    hiSurrogate = 55296 + (codePoint - 65536 >> 10 & 1023) & 65535;
    loSurrogate = 56320 + (codePoint - 65536 & 1023) & 65535;
    return String.fromCharCode(hiSurrogate) + String.fromCharCode(loSurrogate);
  }
   else {
    return String.fromCharCode(codePoint & 65535);
  }
}

function hashCode_3(){
  return getHashCode_0(this);
}

_ = String.prototype;
_.equals$ = equals_2;
_.hashCode$ = hashCode_3;
_.typeId$ = 2;
function $clinit_69(){
  $clinit_69 = nullMethod;
  back = {};
  front = {};
}

function compute(str){
  var hashCode, i, n, nBatch;
  hashCode = 0;
  n = str.length;
  nBatch = n - 4;
  i = 0;
  while (i < nBatch) {
    hashCode = str.charCodeAt(i + 3) + 31 * (str.charCodeAt(i + 2) + 31 * (str.charCodeAt(i + 1) + 31 * (str.charCodeAt(i) + 31 * hashCode))) | 0;
    i += 4;
  }
  while (i < n) {
    hashCode = hashCode * 31 + str.charCodeAt(i++);
  }
  return hashCode | 0;
}

function getHashCode_0(str){
  $clinit_69();
  var key = ':' + str;
  var result = front[key];
  if (result != null) {
    return result;
  }
  result = back[key];
  result == null && (result = compute(str));
  increment();
  return front[key] = result;
}

function increment(){
  if (count_0 == 256) {
    back = front;
    front = {};
    count_0 = 0;
  }
  ++count_0;
}

var back, count_0 = 0, front;
function $StringBuffer(this$static){
  var array;
  this$static.data = (array = [] , array.explicitLength = 0 , array);
  return this$static;
}

function $StringBuffer_0(this$static){
  var array;
  this$static.data = (array = [] , array.explicitLength = 0 , array);
  return this$static;
}

function $append_1(this$static, x){
  $appendNonNull(this$static.data, String.fromCharCode(x));
  return this$static;
}

function $append_2(this$static, x){
  $appendNonNull(this$static.data, String.fromCharCode.apply(null, x));
  return this$static;
}

function $append_3(this$static, x){
  $append_0(this$static.data, x);
  return this$static;
}

function $replace_0(this$static, start, end, toInsert){
  $replace(this$static.data, start, end, toInsert);
  return this$static;
}

function StringBuffer(){
}

_ = StringBuffer.prototype = new Object_0;
_.typeId$ = 20;
function $UnsupportedOperationException(this$static, message){
  $fillInStackTrace();
  return this$static;
}

function UnsupportedOperationException(){
}

_ = UnsupportedOperationException.prototype = new RuntimeException;
_.typeId$ = 21;
function $advanceToFind(iter, o){
  var t;
  while (iter.hasNext()) {
    t = iter.next();
    if (o == null?t == null:equals__devirtual$(o, t)) {
      return iter;
    }
  }
  return null;
}

function add_0(o){
  throw $UnsupportedOperationException(new UnsupportedOperationException, 'Add not supported on this collection');
}

function contains(o){
  var iter;
  iter = $advanceToFind(this.iterator(), o);
  return !!iter;
}

function AbstractCollection(){
}

_ = AbstractCollection.prototype = new Object_0;
_.add = add_0;
_.contains = contains;
_.typeId$ = 0;
function equals_3(obj){
  var entry, entry$iterator, otherKey, otherMap, otherValue;
  if ((obj == null?null:obj) === this) {
    return true;
  }
  if (!(obj != null && canCast(obj.typeId$, 8))) {
    return false;
  }
  otherMap = dynamicCast(obj, 8);
  if (dynamicCast(this, 8).size != otherMap.size) {
    return false;
  }
  for (entry$iterator = $AbstractHashMap$EntrySetIterator(new AbstractHashMap$EntrySetIterator, $AbstractHashMap$EntrySet(new AbstractHashMap$EntrySet, otherMap).this$0); $hasNext_0(entry$iterator.iter);) {
    entry = dynamicCast($next_0(entry$iterator.iter), 6);
    otherKey = entry.getKey();
    otherValue = entry.getValue();
    if (!(otherKey == null?dynamicCast(this, 8).nullSlotLive:otherKey != null && canCast(otherKey.typeId$, 1)?$hasStringValue(dynamicCast(this, 8), dynamicCast(otherKey, 1)):$hasHashValue(dynamicCast(this, 8), otherKey, ~~hashCode__devirtual$(otherKey)))) {
      return false;
    }
    if (!equalsWithNullCheck(otherValue, otherKey == null?dynamicCast(this, 8).nullSlot:otherKey != null && canCast(otherKey.typeId$, 1)?dynamicCast(this, 8).stringMap[':' + dynamicCast(otherKey, 1)]:$getHashValue(dynamicCast(this, 8), otherKey, ~~hashCode__devirtual$(otherKey)))) {
      return false;
    }
  }
  return true;
}

function hashCode_4(){
  var entry, entry$iterator, hashCode;
  hashCode = 0;
  for (entry$iterator = $AbstractHashMap$EntrySetIterator(new AbstractHashMap$EntrySetIterator, $AbstractHashMap$EntrySet(new AbstractHashMap$EntrySet, dynamicCast(this, 8)).this$0); $hasNext_0(entry$iterator.iter);) {
    entry = dynamicCast($next_0(entry$iterator.iter), 6);
    hashCode += entry.hashCode$();
    hashCode = ~~hashCode;
  }
  return hashCode;
}

function AbstractMap(){
}

_ = AbstractMap.prototype = new Object_0;
_.equals$ = equals_3;
_.hashCode$ = hashCode_4;
_.typeId$ = 0;
function $addAllHashEntries(this$static, dest){
  var hashCodeMap = this$static.hashCodeMap;
  for (var hashCode in hashCodeMap) {
    if (hashCode == parseInt(hashCode)) {
      var array = hashCodeMap[hashCode];
      for (var i = 0, c = array.length; i < c; ++i) {
        dest.add(array[i]);
      }
    }
  }
}

function $addAllStringEntries(this$static, dest){
  var stringMap = this$static.stringMap;
  for (var key in stringMap) {
    if (key.charCodeAt(0) == 58) {
      var entry = new_$(this$static, key.substring(1));
      dest.add(entry);
    }
  }
}

function $containsKey(this$static, key){
  return key == null?this$static.nullSlotLive:key != null && canCast(key.typeId$, 1)?$hasStringValue(this$static, dynamicCast(key, 1)):$hasHashValue(this$static, key, ~~hashCode__devirtual$(key));
}

function $get_1(this$static, key){
  return key == null?this$static.nullSlot:key != null && canCast(key.typeId$, 1)?this$static.stringMap[':' + dynamicCast(key, 1)]:$getHashValue(this$static, key, ~~hashCode__devirtual$(key));
}

function $getHashValue(this$static, key, hashCode){
  var array = this$static.hashCodeMap[hashCode];
  if (array) {
    for (var i = 0, c = array.length; i < c; ++i) {
      var entry = array[i];
      var entryKey = entry.getKey();
      if (this$static.equalsBridge(key, entryKey)) {
        return entry.getValue();
      }
    }
  }
  return null;
}

function $hasHashValue(this$static, key, hashCode){
  var array = this$static.hashCodeMap[hashCode];
  if (array) {
    for (var i = 0, c = array.length; i < c; ++i) {
      var entry = array[i];
      var entryKey = entry.getKey();
      if (this$static.equalsBridge(key, entryKey)) {
        return true;
      }
    }
  }
  return false;
}

function $hasStringValue(this$static, key){
  return ':' + key in this$static.stringMap;
}

function $put(this$static, key, value){
  return key == null?$putNullSlot(this$static, value):key == null?$putHashValue(this$static, key, value, ~~getHashCode_0(key)):$putStringValue(this$static, key, value);
}

function $putHashValue(this$static, key, value, hashCode){
  var array = this$static.hashCodeMap[hashCode];
  if (array) {
    for (var i = 0, c = array.length; i < c; ++i) {
      var entry = array[i];
      var entryKey = entry.getKey();
      if (this$static.equalsBridge(key, entryKey)) {
        var previous = entry.getValue();
        entry.setValue(value);
        return previous;
      }
    }
  }
   else {
    array = this$static.hashCodeMap[hashCode] = [];
  }
  var entry = $MapEntryImpl(new MapEntryImpl, key, value);
  array.push(entry);
  ++this$static.size;
  return null;
}

function $putNullSlot(this$static, value){
  var result;
  result = this$static.nullSlot;
  this$static.nullSlot = value;
  if (!this$static.nullSlotLive) {
    this$static.nullSlotLive = true;
    ++this$static.size;
  }
  return result;
}

function $putStringValue(this$static, key, value){
  var result, stringMap = this$static.stringMap;
  key = ':' + key;
  key in stringMap?(result = stringMap[key]):++this$static.size;
  stringMap[key] = value;
  return result;
}

function equalsBridge(value1, value2){
  return (value1 == null?null:value1) === (value2 == null?null:value2) || value1 != null && equals__devirtual$(value1, value2);
}

function AbstractHashMap(){
}

_ = AbstractHashMap.prototype = new AbstractMap;
_.equalsBridge = equalsBridge;
_.typeId$ = 0;
_.hashCodeMap = null;
_.nullSlot = null;
_.nullSlotLive = false;
_.size = 0;
_.stringMap = null;
function equals_4(o){
  var iter, other, otherItem;
  if ((o == null?null:o) === this) {
    return true;
  }
  if (!(o != null && canCast(o.typeId$, 9))) {
    return false;
  }
  other = dynamicCast(o, 9);
  if (other.this$0.size != this.size_0()) {
    return false;
  }
  for (iter = $AbstractHashMap$EntrySetIterator(new AbstractHashMap$EntrySetIterator, other.this$0); $hasNext_0(iter.iter);) {
    otherItem = dynamicCast($next_0(iter.iter), 6);
    if (!this.contains(otherItem)) {
      return false;
    }
  }
  return true;
}

function hashCode_5(){
  var hashCode, iter, next;
  hashCode = 0;
  for (iter = this.iterator(); iter.hasNext();) {
    next = iter.next();
    if (next != null) {
      hashCode += hashCode__devirtual$(next);
      hashCode = ~~hashCode;
    }
  }
  return hashCode;
}

function AbstractSet(){
}

_ = AbstractSet.prototype = new AbstractCollection;
_.equals$ = equals_4;
_.hashCode$ = hashCode_5;
_.typeId$ = 0;
function $AbstractHashMap$EntrySet(this$static, this$0){
  this$static.this$0 = this$0;
  return this$static;
}

function contains_0(o){
  var entry, key, value;
  if (o != null && canCast(o.typeId$, 6)) {
    entry = dynamicCast(o, 6);
    key = entry.getKey();
    if ($containsKey(this.this$0, key)) {
      value = $get_1(this.this$0, key);
      return $equals_2(entry.getValue(), value);
    }
  }
  return false;
}

function iterator(){
  return $AbstractHashMap$EntrySetIterator(new AbstractHashMap$EntrySetIterator, this.this$0);
}

function size_0(){
  return this.this$0.size;
}

function AbstractHashMap$EntrySet(){
}

_ = AbstractHashMap$EntrySet.prototype = new AbstractSet;
_.contains = contains_0;
_.iterator = iterator;
_.size_0 = size_0;
_.typeId$ = 22;
_.this$0 = null;
function $AbstractHashMap$EntrySetIterator(this$static, this$0){
  var list;
  this$static.this$0 = this$0;
  list = $ArrayList(new ArrayList);
  this$static.this$0.nullSlotLive && $add(list, $AbstractHashMap$MapEntryNull(new AbstractHashMap$MapEntryNull, this$static.this$0));
  $addAllStringEntries(this$static.this$0, list);
  $addAllHashEntries(this$static.this$0, list);
  this$static.iter = $AbstractList$IteratorImpl(new AbstractList$IteratorImpl, list);
  return this$static;
}

function hasNext(){
  return $hasNext_0(this.iter);
}

function next_0(){
  return dynamicCast($next_0(this.iter), 6);
}

function AbstractHashMap$EntrySetIterator(){
}

_ = AbstractHashMap$EntrySetIterator.prototype = new Object_0;
_.hasNext = hasNext;
_.next = next_0;
_.typeId$ = 0;
_.iter = null;
_.this$0 = null;
function equals_5(other){
  var entry;
  if (other != null && canCast(other.typeId$, 6)) {
    entry = dynamicCast(other, 6);
    if (equalsWithNullCheck(this.getKey(), entry.getKey()) && equalsWithNullCheck(this.getValue(), entry.getValue())) {
      return true;
    }
  }
  return false;
}

function hashCode_6(){
  var keyHash, valueHash;
  keyHash = 0;
  valueHash = 0;
  this.getKey() != null && (keyHash = hashCode__devirtual$(this.getKey()));
  this.getValue() != null && (valueHash = hashCode__devirtual$(this.getValue()));
  return keyHash ^ valueHash;
}

function AbstractMapEntry(){
}

_ = AbstractMapEntry.prototype = new Object_0;
_.equals$ = equals_5;
_.hashCode$ = hashCode_6;
_.typeId$ = 23;
function $AbstractHashMap$MapEntryNull(this$static, this$0){
  this$static.this$0 = this$0;
  return this$static;
}

function getKey(){
  return null;
}

function getValue(){
  return this.this$0.nullSlot;
}

function setValue(object){
  return $putNullSlot(this.this$0, object);
}

function AbstractHashMap$MapEntryNull(){
}

_ = AbstractHashMap$MapEntryNull.prototype = new AbstractMapEntry;
_.getKey = getKey;
_.getValue = getValue;
_.setValue = setValue;
_.typeId$ = 24;
_.this$0 = null;
function $AbstractHashMap$MapEntryString(this$static, key, this$0){
  this$static.this$0 = this$0;
  this$static.key = key;
  return this$static;
}

function getKey_0(){
  return this.key;
}

function getValue_0(){
  return this.this$0.stringMap[':' + this.key];
}

function new_$(this$outer, key){
  return $AbstractHashMap$MapEntryString(new AbstractHashMap$MapEntryString, key, this$outer);
}

function setValue_0(object){
  return $putStringValue(this.this$0, this.key, object);
}

function AbstractHashMap$MapEntryString(){
}

_ = AbstractHashMap$MapEntryString.prototype = new AbstractMapEntry;
_.getKey = getKey_0;
_.getValue = getValue_0;
_.setValue = setValue_0;
_.typeId$ = 25;
_.key = null;
_.this$0 = null;
function add_1(obj){
  $add_0(this, this.size_0(), obj);
  return true;
}

function checkIndex(index, size){
  (index < 0 || index >= size) && indexOutOfBounds(index, size);
}

function equals_6(o){
  var elem, elemOther, iter, iterOther, other;
  if ((o == null?null:o) === this) {
    return true;
  }
  if (!(o != null && canCast(o.typeId$, 7))) {
    return false;
  }
  other = dynamicCast(o, 7);
  if (this.size_0() != other.size) {
    return false;
  }
  iter = $AbstractList$IteratorImpl(new AbstractList$IteratorImpl, dynamicCast(this, 7));
  iterOther = $AbstractList$IteratorImpl(new AbstractList$IteratorImpl, other);
  while (iter.i < iter.this$0.size) {
    elem = $next_0(iter);
    elemOther = $next_0(iterOther);
    if (!(elem == null?elemOther == null:equals__devirtual$(elem, elemOther))) {
      return false;
    }
  }
  return true;
}

function hashCode_7(){
  var iter, k, obj;
  k = 1;
  iter = $AbstractList$IteratorImpl(new AbstractList$IteratorImpl, dynamicCast(this, 7));
  while (iter.i < iter.this$0.size) {
    obj = $next_0(iter);
    k = 31 * k + (obj == null?0:hashCode__devirtual$(obj));
    k = ~~k;
  }
  return k;
}

function indexOutOfBounds(index, size){
  throw $IndexOutOfBoundsException(new IndexOutOfBoundsException, 'Index: ' + index + ', Size: ' + size);
}

function iterator_0(){
  return $AbstractList$IteratorImpl(new AbstractList$IteratorImpl, dynamicCast(this, 7));
}

function AbstractList(){
}

_ = AbstractList.prototype = new AbstractCollection;
_.add = add_1;
_.equals$ = equals_6;
_.hashCode$ = hashCode_7;
_.iterator = iterator_0;
_.typeId$ = 0;
function $AbstractList$IteratorImpl(this$static, this$0){
  this$static.this$0 = this$0;
  return this$static;
}

function $hasNext_0(this$static){
  return this$static.i < this$static.this$0.size;
}

function $next_0(this$static){
  if (this$static.i >= this$static.this$0.size) {
    throw $NoSuchElementException(new NoSuchElementException);
  }
  return $get_2(this$static.this$0, this$static.i++);
}

function hasNext_0(){
  return this.i < this.this$0.size;
}

function next_1(){
  return $next_0(this);
}

function AbstractList$IteratorImpl(){
}

_ = AbstractList$IteratorImpl.prototype = new Object_0;
_.hasNext = hasNext_0;
_.next = next_1;
_.typeId$ = 0;
_.i = 0;
_.this$0 = null;
function $ArrayList(this$static){
  this$static.array = initDim(_3Ljava_lang_Object_2_classLit, 38, 0, 0, 0);
  return this$static;
}

function $add(this$static, o){
  setCheck(this$static.array, this$static.size++, o);
  return true;
}

function $add_0(this$static, index, o){
  (index < 0 || index > this$static.size) && indexOutOfBounds(index, this$static.size);
  this$static.array.splice(index, 0, o);
  ++this$static.size;
}

function $get_2(this$static, index){
  checkIndex(index, this$static.size);
  return this$static.array[index];
}

function $indexOf_1(this$static, o, index){
  for (; index < this$static.size; ++index) {
    if (equalsWithNullCheck(o, this$static.array[index])) {
      return index;
    }
  }
  return -1;
}

function add_2(o){
  return setCheck(this.array, this.size++, o) , true;
}

function contains_1(o){
  return $indexOf_1(this, o, 0) != -1;
}

function size_1(){
  return this.size;
}

function ArrayList(){
}

_ = ArrayList.prototype = new AbstractList;
_.add = add_2;
_.contains = contains_1;
_.size_0 = size_1;
_.typeId$ = 26;
_.size = 0;
function $HashMap(this$static){
  this$static.hashCodeMap = [];
  this$static.stringMap = {};
  this$static.nullSlotLive = false;
  this$static.nullSlot = null;
  this$static.size = 0;
  return this$static;
}

function $equals_2(value1, value2){
  return (value1 == null?null:value1) === (value2 == null?null:value2) || value1 != null && equals__devirtual$(value1, value2);
}

function HashMap(){
}

_ = HashMap.prototype = new AbstractHashMap;
_.typeId$ = 27;
function $MapEntryImpl(this$static, key, value){
  this$static.key = key;
  this$static.value = value;
  return this$static;
}

function getKey_1(){
  return this.key;
}

function getValue_1(){
  return this.value;
}

function setValue_1(value){
  var old;
  old = this.value;
  this.value = value;
  return old;
}

function MapEntryImpl(){
}

_ = MapEntryImpl.prototype = new AbstractMapEntry;
_.getKey = getKey_1;
_.getValue = getValue_1;
_.setValue = setValue_1;
_.typeId$ = 28;
_.key = null;
_.value = null;
function $NoSuchElementException(this$static){
  $fillInStackTrace();
  return this$static;
}

function NoSuchElementException(){
}

_ = NoSuchElementException.prototype = new RuntimeException;
_.typeId$ = 29;
function equalsWithNullCheck(a, b){
  return (a == null?null:a) === (b == null?null:b) || a != null && equals__devirtual$(a, b);
}

function $DateTimeFormat(this$static, pattern, dateTimeConstants){
  this$static.patternParts = $ArrayList(new ArrayList);
  this$static.pattern = pattern;
  this$static.dateTimeConstants = dateTimeConstants;
  $parsePattern_0(this$static, pattern);
  return this$static;
}

function $addPart(this$static, buf, count){
  var oldLength;
  if ($toString(buf.data).length > 0) {
    $add(this$static.patternParts, $DateTimeFormat$PatternPart(new DateTimeFormat$PatternPart, $toString(buf.data), count));
    oldLength = $toString(buf.data).length;
    0 < oldLength?$replace(buf.data, 0, oldLength, ''):0 > oldLength && $append_2(buf, initDim(_3C_classLit, 32, -1, 0 - oldLength, 1));
  }
}

function $format_1(this$static, date, timeZone){
  var ch, diff, i, j, keepDate, keepTime, n, toAppendTo, trailQuote;
  diff = ((date.checkJsDate() , date.jsdate.getTimezoneOffset()) - timeZone.standardOffset) * 60000;
  keepDate = $Date_0(new Date_0, add(fromDouble((date.checkJsDate() , date.jsdate.getTime())), fromInt(diff)));
  keepTime = keepDate;
  if ((keepDate.checkJsDate() , keepDate.jsdate.getTimezoneOffset()) != (date.checkJsDate() , date.jsdate.getTimezoneOffset())) {
    diff > 0?(diff -= 86400000):(diff += 86400000);
    keepTime = $Date_0(new Date_0, add(fromDouble((date.checkJsDate() , date.jsdate.getTime())), fromInt(diff)));
  }
  toAppendTo = $StringBuffer_0(new StringBuffer);
  n = this$static.pattern.length;
  for (i = 0; i < n;) {
    ch = this$static.pattern.charCodeAt(i);
    if (ch >= 97 && ch <= 122 || ch >= 65 && ch <= 90) {
      for (j = i + 1; j < n && this$static.pattern.charCodeAt(j) == ch; ++j) {
      }
      $subFormat(this$static, toAppendTo, ch, j - i, keepDate, keepTime, timeZone);
      i = j;
    }
     else if (ch == 39) {
      ++i;
      if (i < n && this$static.pattern.charCodeAt(i) == 39) {
        $appendNonNull(toAppendTo.data, "'");
        ++i;
        continue;
      }
      trailQuote = false;
      while (!trailQuote) {
        j = i;
        while (j < n && this$static.pattern.charCodeAt(j) != 39) {
          ++j;
        }
        if (j >= n) {
          throw $IllegalArgumentException(new IllegalArgumentException, "Missing trailing '");
        }
        j + 1 < n && this$static.pattern.charCodeAt(j + 1) == 39?++j:(trailQuote = true);
        $append_3(toAppendTo, $substring_0(this$static.pattern, i, j));
        i = j + 1;
      }
    }
     else {
      $appendNonNull(toAppendTo.data, String.fromCharCode(ch));
      ++i;
    }
  }
  return $toString(toAppendTo.data);
}

function $formatFractionalSeconds(buf, count, date){
  var time, value;
  time = fromDouble((date.checkJsDate() , date.jsdate.getTime()));
  compare(time, P0_longLit) < 0?(value = 1000 - lowBits_0(mod(neg(time), P3e8_longLit))):(value = lowBits_0(mod(time, P3e8_longLit)));
  if (count == 1) {
    value = ~~((value + 50) / 100) < 9?~~((value + 50) / 100):9;
    $appendNonNull(buf.data, String.fromCharCode(48 + value & 65535));
  }
   else if (count == 2) {
    value = ~~((value + 5) / 10) < 99?~~((value + 5) / 10):99;
    $zeroPaddingNumber(buf, value, 2);
  }
   else {
    $zeroPaddingNumber(buf, value, 3);
    count > 3 && $zeroPaddingNumber(buf, 0, count - 3);
  }
}

function $formatMonth(this$static, buf, count, date){
  var value;
  value = (date.checkJsDate() , date.jsdate.getMonth());
  switch (count) {
    case 5:
      $append_3(buf, $narrowMonths(this$static.dateTimeConstants)[value]);
      break;
    case 4:
      $append_3(buf, $months(this$static.dateTimeConstants)[value]);
      break;
    case 3:
      $append_3(buf, $shortMonths(this$static.dateTimeConstants)[value]);
      break;
    default:$zeroPaddingNumber(buf, value + 1, count);
  }
}

function $getNextCharCountInPattern(pattern, start){
  var ch, next;
  ch = pattern.charCodeAt(start);
  next = start + 1;
  while (next < pattern.length && pattern.charCodeAt(next) == ch) {
    ++next;
  }
  return next - start;
}

function $identifyAbutStart(this$static){
  var abut, i, len;
  abut = false;
  len = this$static.patternParts.size;
  for (i = 0; i < len; ++i) {
    if ($isNumeric(dynamicCast($get_2(this$static.patternParts, i), 16))) {
      if (!abut && i + 1 < len && $isNumeric(dynamicCast($get_2(this$static.patternParts, i + 1), 16))) {
        abut = true;
        dynamicCast($get_2(this$static.patternParts, i), 16).abutStart = true;
      }
    }
     else {
      abut = false;
    }
  }
}

function $isNumeric(part){
  var i;
  if (part.count <= 0) {
    return false;
  }
  i = 'MLydhHmsSDkK'.indexOf(fromCodePoint(part.text.charCodeAt(0)));
  return i > 1 || i >= 0 && part.count < 3;
}

function $matchString(text, start, data, pos){
  var bestMatch, bestMatchLength, count, i, length_0, textInLowerCase;
  count = data.length;
  bestMatchLength = 0;
  bestMatch = -1;
  textInLowerCase = text.substr(start, text.length - start).toLowerCase();
  for (i = 0; i < count; ++i) {
    length_0 = data[i].length;
    if (length_0 > bestMatchLength && textInLowerCase.indexOf(data[i].toLowerCase()) == 0) {
      bestMatch = i;
      bestMatchLength = length_0;
    }
  }
  bestMatch >= 0 && (pos[0] = start + bestMatchLength);
  return bestMatch;
}

function $parse_1(this$static, text, start, date, strict, tzOffset){
  var abutPass, abutPat, abutStart, cal, count, i, parsePos, part, s;
  cal = $DateRecord(new DateRecord);
  cal.tzOffset = tzOffset;
  parsePos = initValues(_3I_classLit, 34, -1, [start]);
  abutPat = -1;
  abutStart = 0;
  abutPass = 0;
  for (i = 0; i < this$static.patternParts.size; ++i) {
    part = dynamicCast($get_2(this$static.patternParts, i), 16);
    if (part.count > 0) {
      if (abutPat < 0 && part.abutStart) {
        abutPat = i;
        abutStart = start;
        abutPass = 0;
      }
      if (abutPat >= 0) {
        count = part.count;
        if (i == abutPat) {
          count -= abutPass++;
          if (count == 0) {
            return 0;
          }
        }
        if (!$subParse(this$static, text, parsePos, part, count, cal)) {
          i = abutPat - 1;
          parsePos[0] = abutStart;
          continue;
        }
      }
       else {
        abutPat = -1;
        if (!$subParse(this$static, text, parsePos, part, 0, cal)) {
          return 0;
        }
      }
    }
     else {
      abutPat = -1;
      if (part.text.charCodeAt(0) == 32) {
        s = parsePos[0];
        $skipSpace(text, parsePos);
        if (parsePos[0] > s) {
          continue;
        }
      }
       else if ($startsWith(text, part.text, parsePos[0])) {
        parsePos[0] += part.text.length;
        continue;
      }
      return 0;
    }
  }
  if (!$calcDate(cal, date, strict)) {
    return 0;
  }
  return parsePos[0] - start;
}

function $parseInt(text, pos){
  var ch, ind, ret;
  ret = 0;
  ind = pos[0];
  ch = text.charCodeAt(ind);
  while (ch >= 48 && ch <= 57) {
    ret = ret * 10 + (ch - 48);
    ++ind;
    if (ind >= text.length) {
      break;
    }
    ch = text.charCodeAt(ind);
  }
  ind > pos[0]?(pos[0] = ind):(ret = -1);
  return ret;
}

function $parsePattern_0(this$static, pattern){
  var buf, ch, count, i, inQuote;
  buf = $StringBuffer_0(new StringBuffer);
  inQuote = false;
  for (i = 0; i < pattern.length; ++i) {
    ch = pattern.charCodeAt(i);
    if (ch == 32) {
      $addPart(this$static, buf, 0);
      $appendNonNull(buf.data, ' ');
      $addPart(this$static, buf, 0);
      while (i + 1 < pattern.length && pattern.charCodeAt(i + 1) == 32) {
        ++i;
      }
      continue;
    }
    if (inQuote) {
      if (ch == 39) {
        if (i + 1 < pattern.length && pattern.charCodeAt(i + 1) == 39) {
          $appendNonNull(buf.data, String.fromCharCode(ch));
          ++i;
        }
         else {
          inQuote = false;
        }
      }
       else {
        $appendNonNull(buf.data, String.fromCharCode(ch));
      }
      continue;
    }
    if ('GyMLdkHmsSEcDahKzZv'.indexOf(fromCodePoint(ch)) > 0) {
      $addPart(this$static, buf, 0);
      $appendNonNull(buf.data, String.fromCharCode(ch));
      count = $getNextCharCountInPattern(pattern, i);
      $addPart(this$static, buf, count);
      i += count - 1;
      continue;
    }
    if (ch == 39) {
      if (i + 1 < pattern.length && pattern.charCodeAt(i + 1) == 39) {
        $appendNonNull(buf.data, "'");
        ++i;
      }
       else {
        inQuote = true;
      }
    }
     else {
      $appendNonNull(buf.data, String.fromCharCode(ch));
    }
  }
  $addPart(this$static, buf, 0);
  $identifyAbutStart(this$static);
}

function $parseTimeZoneOffset(text, pos, cal){
  var offset, sign, st, value;
  if (pos[0] >= text.length) {
    cal.tzOffset = 0;
    return true;
  }
  switch (text.charCodeAt(pos[0])) {
    case 43:
      sign = 1;
      break;
    case 45:
      sign = -1;
      break;
    default:cal.tzOffset = 0;
      return true;
  }
  ++pos[0];
  st = pos[0];
  value = $parseInt(text, pos);
  if (value == 0 && pos[0] == st) {
    return false;
  }
  if (pos[0] < text.length && text.charCodeAt(pos[0]) == 58) {
    offset = value * 60;
    ++pos[0];
    st = pos[0];
    value = $parseInt(text, pos);
    if (value == 0 && pos[0] == st) {
      return false;
    }
    offset += value;
  }
   else {
    offset = value;
    offset < 24 && pos[0] - st <= 2?(offset *= 60):(offset = offset % 100 + ~~(offset / 100) * 60);
  }
  offset *= sign;
  cal.tzOffset = -offset;
  return true;
}

function $skipSpace(text, pos){
  while (pos[0] < text.length && ' \t\r\n'.indexOf(fromCodePoint(text.charCodeAt(pos[0]))) >= 0) {
    ++pos[0];
  }
}

function $subFormat(this$static, buf, ch, count, adjustedDate, adjustedTime, timezone){
  var value, value_0, value_1, value_2, value_3, value_4, value_5, value_6, value_7, value_8, value_9, value_10, value_11;
  switch (ch) {
    case 71:
      value = (adjustedDate.checkJsDate() , adjustedDate.jsdate.getFullYear() - 1900) >= -1900?1:0;
      count >= 4?$append_3(buf, $eraNames(this$static.dateTimeConstants)[value]):$append_3(buf, $eras(this$static.dateTimeConstants)[value]);
      break;
    case 121:
      value_0 = (adjustedDate.checkJsDate() , adjustedDate.jsdate.getFullYear() - 1900) + 1900;
      value_0 < 0 && (value_0 = -value_0);
      count == 2?$zeroPaddingNumber(buf, value_0 % 100, 2):$append(buf.data, value_0);
      break;
    case 77:
      $formatMonth(this$static, buf, count, adjustedDate);
      break;
    case 107:
      value_1 = (adjustedTime.checkJsDate() , adjustedTime.jsdate.getHours());
      value_1 == 0?$zeroPaddingNumber(buf, 24, count):$zeroPaddingNumber(buf, value_1, count);
      break;
    case 83:
      $formatFractionalSeconds(buf, count, adjustedTime);
      break;
    case 69:
      value_2 = (adjustedDate.checkJsDate() , adjustedDate.jsdate.getDay());
      count == 5?$append_3(buf, $narrowWeekdays(this$static.dateTimeConstants)[value_2]):count == 4?$append_3(buf, $weekdays(this$static.dateTimeConstants)[value_2]):$append_3(buf, $shortWeekdays(this$static.dateTimeConstants)[value_2]);
      break;
    case 97:
      (adjustedTime.checkJsDate() , adjustedTime.jsdate.getHours()) >= 12 && (adjustedTime.checkJsDate() , adjustedTime.jsdate.getHours()) < 24?$append_3(buf, $ampms(this$static.dateTimeConstants)[1]):$append_3(buf, $ampms(this$static.dateTimeConstants)[0]);
      break;
    case 104:
      value_3 = (adjustedTime.checkJsDate() , adjustedTime.jsdate.getHours()) % 12;
      value_3 == 0?$zeroPaddingNumber(buf, 12, count):$zeroPaddingNumber(buf, value_3, count);
      break;
    case 75:
      value_4 = (adjustedTime.checkJsDate() , adjustedTime.jsdate.getHours()) % 12;
      $zeroPaddingNumber(buf, value_4, count);
      break;
    case 72:
      value_5 = (adjustedTime.checkJsDate() , adjustedTime.jsdate.getHours());
      $zeroPaddingNumber(buf, value_5, count);
      break;
    case 99:
      value_6 = (adjustedDate.checkJsDate() , adjustedDate.jsdate.getDay());
      count == 5?$append_3(buf, $standaloneNarrowWeekdays(this$static.dateTimeConstants)[value_6]):count == 4?$append_3(buf, $standaloneWeekdays(this$static.dateTimeConstants)[value_6]):count == 3?$append_3(buf, $standaloneShortWeekdays(this$static.dateTimeConstants)[value_6]):$zeroPaddingNumber(buf, value_6, 1);
      break;
    case 76:
      value_7 = (adjustedDate.checkJsDate() , adjustedDate.jsdate.getMonth());
      count == 5?$append_3(buf, $standaloneNarrowMonths(this$static.dateTimeConstants)[value_7]):count == 4?$append_3(buf, $standaloneMonths(this$static.dateTimeConstants)[value_7]):count == 3?$append_3(buf, $standaloneShortMonths(this$static.dateTimeConstants)[value_7]):$zeroPaddingNumber(buf, value_7 + 1, count);
      break;
    case 81:
      value_8 = ~~((adjustedDate.checkJsDate() , adjustedDate.jsdate.getMonth()) / 3);
      count < 4?$append_3(buf, $shortQuarters(this$static.dateTimeConstants)[value_8]):$append_3(buf, $quarters(this$static.dateTimeConstants)[value_8]);
      break;
    case 100:
      value_9 = (adjustedDate.checkJsDate() , adjustedDate.jsdate.getDate());
      $zeroPaddingNumber(buf, value_9, count);
      break;
    case 109:
      value_10 = (adjustedTime.checkJsDate() , adjustedTime.jsdate.getMinutes());
      $zeroPaddingNumber(buf, value_10, count);
      break;
    case 115:
      value_11 = (adjustedTime.checkJsDate() , adjustedTime.jsdate.getSeconds());
      $zeroPaddingNumber(buf, value_11, count);
      break;
    case 122:
      count < 4?$append_3(buf, timezone.tzNames[0]):$append_3(buf, timezone.tzNames[1]);
      break;
    case 118:
      $append_3(buf, timezone.timezoneID);
      break;
    case 90:
      count < 4?$append_3(buf, $getRFCTimeZoneString(timezone)):$append_3(buf, composeGMTString(timezone.standardOffset));
      break;
    default:return false;
  }
  return true;
}

function $subParse(this$static, text, pos, part, digitCount, cal){
  var ch, start, value;
  $skipSpace(text, pos);
  start = pos[0];
  ch = part.text.charCodeAt(0);
  value = -1;
  if ($isNumeric(part)) {
    if (digitCount > 0) {
      if (start + digitCount > text.length) {
        return false;
      }
      value = $parseInt(text.substr(0, start + digitCount - 0), pos);
    }
     else {
      value = $parseInt(text, pos);
    }
  }
  switch (ch) {
    case 71:
      value = $matchString(text, start, $eras(this$static.dateTimeConstants), pos);
      cal.era = value;
      return true;
    case 77:
      return $subParseMonth(this$static, text, pos, cal, value, start);
    case 76:
      return $subParseStandaloneMonth(this$static, text, pos, cal, value, start);
    case 69:
      return $subParseDayOfWeek(this$static, text, pos, start, cal);
    case 99:
      return $subParseStandaloneDay(this$static, text, pos, start, cal);
    case 97:
      value = $matchString(text, start, $ampms(this$static.dateTimeConstants), pos);
      cal.ampm = value;
      return true;
    case 121:
      return $subParseYear(text, pos, start, value, part, cal);
    case 100:
      if (value <= 0) {
        return false;
      }

      cal.dayOfMonth = value;
      return true;
    case 83:
      return $subParseFractionalSeconds(value, start, pos[0], cal);
    case 104:
      value == 12 && (value = 0);
    case 75:
    case 72:
      cal.hours = value;
      return true;
    case 107:
      cal.hours = value;
      return true;
    case 109:
      cal.minutes = value;
      return true;
    case 115:
      cal.seconds = value;
      return true;
    case 122:
    case 90:
    case 118:
      return $subParseTimeZoneInGMT(text, start, pos, cal);
    default:return false;
  }
}

function $subParseDayOfWeek(this$static, text, pos, start, cal){
  var value;
  value = $matchString(text, start, $weekdays(this$static.dateTimeConstants), pos);
  value < 0 && (value = $matchString(text, start, $shortWeekdays(this$static.dateTimeConstants), pos));
  if (value < 0) {
    return false;
  }
  cal.dayOfWeek = value;
  return true;
}

function $subParseFractionalSeconds(value, start, end, cal){
  var a, i;
  i = end - start;
  if (i < 3) {
    while (i < 3) {
      value *= 10;
      ++i;
    }
  }
   else {
    a = 1;
    while (i > 3) {
      a *= 10;
      --i;
    }
    value = ~~((value + (a >> 1)) / a);
  }
  cal.milliseconds = value;
  return true;
}

function $subParseMonth(this$static, text, pos, cal, value, start){
  if (value < 0) {
    value = $matchString(text, start, $months(this$static.dateTimeConstants), pos);
    value < 0 && (value = $matchString(text, start, $shortMonths(this$static.dateTimeConstants), pos));
    if (value < 0) {
      return false;
    }
    cal.month = value;
    return true;
  }
   else if (value > 0) {
    cal.month = value - 1;
    return true;
  }
  return false;
}

function $subParseStandaloneDay(this$static, text, pos, start, cal){
  var value;
  value = $matchString(text, start, $standaloneWeekdays(this$static.dateTimeConstants), pos);
  value < 0 && (value = $matchString(text, start, $standaloneShortWeekdays(this$static.dateTimeConstants), pos));
  if (value < 0) {
    return false;
  }
  cal.dayOfWeek = value;
  return true;
}

function $subParseStandaloneMonth(this$static, text, pos, cal, value, start){
  if (value < 0) {
    value = $matchString(text, start, $standaloneMonths(this$static.dateTimeConstants), pos);
    value < 0 && (value = $matchString(text, start, $standaloneShortMonths(this$static.dateTimeConstants), pos));
    if (value < 0) {
      return false;
    }
    cal.month = value;
    return true;
  }
   else if (value > 0) {
    cal.month = value - 1;
    return true;
  }
  return false;
}

function $subParseTimeZoneInGMT(text, start, pos, cal){
  if ($startsWith(text, 'GMT', start)) {
    pos[0] = start + 3;
    return $parseTimeZoneOffset(text, pos, cal);
  }
  return $parseTimeZoneOffset(text, pos, cal);
}

function $subParseYear(text, pos, start, value, part, cal){
  var ambiguousTwoDigitYear, ch, date, defaultCenturyStartYear;
  ch = 32;
  if (value < 0) {
    ch = text.charCodeAt(pos[0]);
    if (ch != 43 && ch != 45) {
      return false;
    }
    ++pos[0];
    value = $parseInt(text, pos);
    if (value < 0) {
      return false;
    }
    ch == 45 && (value = -value);
  }
  if (ch == 32 && pos[0] - start == 2 && part.count == 2) {
    date = $Date(new Date_0);
    defaultCenturyStartYear = (date.checkJsDate() , date.jsdate.getFullYear() - 1900) + 1900 - 80;
    ambiguousTwoDigitYear = defaultCenturyStartYear % 100;
    cal.ambiguousYear = value == ambiguousTwoDigitYear;
    value += ~~(defaultCenturyStartYear / 100) * 100 + (value < ambiguousTwoDigitYear?100:0);
  }
  cal.year = value;
  return true;
}

function $zeroPaddingNumber(buf, value, minWidth){
  var b, i;
  b = 10;
  for (i = 0; i < minWidth - 1; ++i) {
    value < b && $appendNonNull(buf.data, '0');
    b *= 10;
  }
  $append(buf.data, value);
}

function DateTimeFormat(){
}

_ = DateTimeFormat.prototype = new Object_0;
_.typeId$ = 0;
_.dateTimeConstants = null;
_.pattern = null;
function $DateTimeFormat$PatternPart(this$static, txt, cnt){
  this$static.text = txt;
  this$static.count = cnt;
  this$static.abutStart = false;
  return this$static;
}

function DateTimeFormat$PatternPart(){
}

_ = DateTimeFormat$PatternPart.prototype = new Object_0;
_.typeId$ = 30;
_.abutStart = false;
_.count = 0;
_.text = null;
function TersusNumberConstants(){
}

_ = TersusNumberConstants.prototype = new Object_0;
_.typeId$ = 0;
_.decimalSeparator = '.';
_.groupingSeparator = ',';
function $clinit_98(){
  $clinit_98 = nullMethod;
  $clinit_27();
}

function $TersusNumberFormat(this$static, numberConstants, pattern, cdata){
  $clinit_98();
  $NumberFormat(this$static, numberConstants, pattern, cdata, true);
  return this$static;
}

function TersusNumberFormat(){
}

_ = TersusNumberFormat.prototype = new NumberFormat;
_.typeId$ = 31;
function $clinit_99(){
  $clinit_99 = nullMethod;
  numberConstants_0 = new TersusNumberConstants;
  numberFormats = $HashMap(new HashMap);
}

function formatDateAndTime(time, pattern, timeZoneOffset){
  var date, format, tz, timeZone;
  date = $Date_0(new Date_0, fromDouble(time));
  format = $DateTimeFormat(new DateTimeFormat, pattern, $getDateTimeConstants(($clinit_24() , $clinit_24() , instance)));
  if (timeZoneOffset > -10000) {
    tz = createTimeZone(timeZoneOffset);
    return $format_1(format, date, tz);
  }
   else 
    return timeZone = createTimeZone((date.checkJsDate() , date.jsdate.getTimezoneOffset())) , $format_1(format, date, timeZone);
}

function formatNumber(num, pattern){
  var format;
  numberConstants_0.decimalSeparator = $wnd.tersus.DECIMAL_POINT;
  numberConstants_0.groupingSeparator = $wnd.tersus.THOUSANDS_SEPARATOR;
  format = dynamicCast($get_1(numberFormats, pattern), 17);
  if (!format) {
    format = $TersusNumberFormat(new TersusNumberFormat, numberConstants_0, pattern, ['USD', 'US$', 2, 'US$']);
    $put(numberFormats, pattern, format);
  }
  return $format(format, num);
}

function parseDateAndTime(text, pattern, timeZoneOffset){
  var date, format, rc;
  format = $DateTimeFormat(new DateTimeFormat, pattern, $getDateTimeConstants(($clinit_24() , $clinit_24() , instance)));
  date = $Date_0(new Date_0, P0_longLit);
  timeZoneOffset > -10000?(rc = $parse_1(format, text, 0, date, false, timeZoneOffset)):(rc = $parse_1(format, text, 0, date, false, -2147483648));
  if (rc <= 0 || rc != text.length)
    return -1;
  return toDouble(fromDouble((date.checkJsDate() , date.jsdate.getTime())));
}

function parseNumber(text, pattern){
  var format;
  numberConstants_0.decimalSeparator = $wnd.tersus.DECIMAL_POINT;
  numberConstants_0.groupingSeparator = $wnd.tersus.THOUSANDS_SEPARATOR;
  format = dynamicCast($get_1(numberFormats, pattern), 17);
  if (!format) {
    format = $TersusNumberFormat(new TersusNumberFormat, numberConstants_0, pattern, ['USD', 'US$', 2, 'US$']);
    $put(numberFormats, pattern, format);
  }
  return $parse(format, text);
}

var numberConstants_0, numberFormats;
var $entry = entry_0;
function gwtOnLoad(errFn, modName, modBase){
  $moduleName = modName;
  $moduleBase = modBase;
  if (errFn)
    try {
      $entry(init)();
    }
     catch (e) {
      errFn(modName);
    }
   else {
    $entry(init)();
  }
}

var _3I_classLit = new Class, _3Ljava_lang_StackTraceElement_2_classLit = new Class, _3Ljava_lang_String_2_classLit = new Class, _3C_classLit = new Class, _3_3D_classLit = new Class, _3Ljava_lang_Object_2_classLit = new Class;
//$stats && $stats({moduleName:'tersus.gwt.Util',sessionId:$sessionId,subSystem:'startup',evtGroup:'moduleStartup',millis:(new Date()).getTime(),type:'moduleEvalEnd'});
//if ($wnd.tersus_gwt_Util) $wnd.tersus_gwt_Util.onScriptLoad();
init();


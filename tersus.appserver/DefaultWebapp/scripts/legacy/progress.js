tersus.progress.set = function (msg, cancellationFunc)
{
    if (tlog) tlog("tersus.progress.set('"+msg+"')");
    
    var p = tersus.progress;
    setTimeout(function(){tersus.progress.setStatus(msg);},0);
    var windows = tersus.getWindows();
    for (var i=0;i<windows.length;i++)
    {
        // Create or Update a progress bar for each window of the applications
        var w = windows[i];
        var body;
        try
        {
            var doc = w.document;
            var body = doc.body;
        }
        catch (e)
        {
            body = null;
        }
        if (body)
        {
            if (window.browser == 'IE')
                body.style.cursor='wait';
            if (!body.className)
                body.className = 'progress';
            else if (body.className.search(/\bprogress\b/) < 0)
                body.className += ' progress';
        }
    }
    tersus.progress.message = msg;
    p.active = true; 
};

tersus.progress.clear = function()
{
    if (tlog) tlog("tersus.progress.clear()");
    var p = tersus.progress;
    if (isBusy())
    {
        if (tlog) tlog("tersus.progress.clear() : still busy ..");
        return;
    }
    setTimeout(function(){tersus.progress.setStatus('Done');},0);
    // Hide progress bar in each window
    var windows = tersus.getWindows();
    var bodies = [];
    for (var i=0;i<windows.length;i++)
    {
        var w = windows[i];
        var body;
        try
        {
            body = w.document.body;
        }
        catch (e)
        {
            body = null; // Exception means window is not accessible
        }
        if (body)
        {
            bodies.push(body);
            var c = body.className;
            if (c == 'progress')
                body.className = '';
            else
                body.className = c.replace(/\bprogress\b/, '').trim();
            if (window.browser == 'IE')
                body.style.cursor = ''; 
            var b = w.document.getElementById('progressBar');
            if (b)
            {
                b.style.display = 'none';
                if (tlog) tlog("hiding progress bar in "+w.name);
            }
        }
        
    }
//  w.setTimeout(function(){for (var i=0;i<bodies.length;i++) bodies[i].style.cursor='';},0);
    
    p.active = false;
};

//	written	by Tan Ling	Wee	on 2 Dec 2001
//	last updated 28 Jul 2003
//	email : fuushikaden@yahoo.com
//	website : www.pengz.com
//	TabSize: 4
//
//	modified by ALQUANTO 30 July 2003 - german language included.
//									  - modified languageLogic with the ISO-2letter-strings
//									  - changes in in showCalendar: defaultLanguage is already set...
//									  - js and html corrected... more xhtml-compliant... simplier css
//	email: popcalendar@alquanto.de
//
//	modified by PinoToy 25 July 2003  - new logic for multiple languages (English, Spanish and ready for more).
//									  - changes in popUpMonth & popDownMonth methods for hidding	popup.
//									  - changes in popDownYear & popDownYear methods for hidding	popup.
//									  - new logic for disabling dates in	the past.
//									  - new method showCalendar, dynamic	configuration of language, enabling	past & position.
//									  - changes in the styles.
//	email  : pinotoy@yahoo.com
// No Copyright notice - assumed to be public domain
// Modified by Tersus, October 2004 - Image locations + Week starts on Monday
//                       January 2005 - Extract 'formatDate' function
//                                    - Changed 'popupCaledar' function to accept a callback in addition to the target node.
//                       May 2006     - changed 'popupCaledar' function to take into account scrolling and window size (added functions visible_size, screen_position at the end of the file)
//						 Aug 2006     - fixed formatDate(y,m,d, format) - used to add 1 to the month when outputing in numeric format
//						 Jan 2008     - added Hebrew translation
//                                    - added setCalendarStartOnSunday() to enable externalizing the setup.
//						 Jun 2008     - fixed arrows and drop-downs from right-to-left mode
//                       Sep 2008     - Using 'display:none' instead of 'visibility:hidden' to prevent scrolling problems
//						 Aug 2009	  - Refreshing current date and time when showing calendar	
//                                    - Extracted parseDate as a separate function
//						 Sep 2009     - Dutch localization (translation by JP van de Giessen)
//                       Dec 2009     - Adjustments for standards-mode rendering
//						 Jan 2010     - en_GB, en_US, de_DE and pl_PL localization (translation by NeoPrimus sp. z o.o)
//                       Sep 2011     - Fixing positioning of calendar when content is scrolled. Current version depends on Tersus external function getScroll (which in turn depends on jQuery)
//						 Oct 2012     - zh_CN localization (translation by Stefen Qu)
	var language = 'en';	// Default Language: en - english ; es - spanish; de - german
	var enablePast = 1;		// 0 - disabled ; 1 - enabled
	var fixedX = -1;		// x position (-1 if to appear below control)
	var fixedY = -1;		// y position (-1 if to appear below control)
	var showWeekNumber = 1;	// 0 - don't show; 1 - show
	var showToday = 1;		// 0 - don't show; 1 - show
	var imgDir = 'images/calendar/';		// directory for images ... e.g. var imgDir="/img/"
	var dayName = '';

	var gotoString = {
		en : 'Go To Current Month',
		en_GB : 'Go To Current Month',
		en_US : 'Go To Current Month',
		es : 'Ir al Mes Actual',
		de : 'Gehe zu aktuellem Monat',
		de_DE : 'Gehe zu aktuellem Monat',
		nl : 'Ga naar huidige Maand',
		he : 'עבור לחודש נוכחי',
		pl_PL : 'Bieżący miesiąc',
		zh_CN : '转到本月'
	};
	var todayString = {
		en : 'Today is',
		en_GB : 'Today is',
		en_US : 'Today is',
		es : 'Hoy es',
		de : 'Heute ist',
		de_DE : 'Heute ist',
		nl : 'Vandaag is',
		he : 'היום - יום',
		pl_PL : 'Dzisiaj jest',
		zh_CN : '今天是周'		// 中文里一二三四五六日可以看成是星期几的缩写，但前面不冠上“星期”或“周”就难以理解。
	};
	var weekString = {
		en : 'Wk',
		en_GB : 'Wk',
		en_US : 'Wk',
		es : 'Sem',
		de : 'KW',
		de_DE : 'KW',
		nl : 'Wk',
		he : 'שבוע',
		pl_PL : 'Tydz',
		zh_CN : '周'
	};
	var scrollLeftMessage = {
		en : 'Click to scroll to previous month. Hold mouse button to scroll automatically.',
		en_GB : 'Click to scroll to previous month. Hold mouse button to scroll automatically.',
		en_US : 'Click to scroll to previous month. Hold mouse button to scroll automatically.',
		es : 'Presione para pasar al mes anterior. Deje presionado para pasar varios meses.',
		de : 'Klicken um zum vorigen Monat zu gelangen. Gedr?ckt halten, um automatisch weiter zu scrollen.',
		de_DE : 'Klicken um zum vorigen Monat zu gelangen. Gedr?ckt halten, um automatisch weiter zu scrollen.',
		nl : 'Klik om naar de vorige maand te gaan. Ingedrukt houden om automatisch verder te scrollen.',
		he : 'לחץ למעבר לחודש הקודם. לחץ והחזק לגלילה אוטומטית.',
		pl_PL : 'Kliknij aby przejść do poprzedniego miesiąca. Przytrzymaj przycisk myszy aby przewijać automatycznie.',
		zh_CN : '点击转到上月. 按住鼠标来连续翻页.'		// 感觉滚动到、自动滚动不是很好听。
	};
	var scrollRightMessage = {
		en : 'Click to scroll to next month. Hold mouse button to scroll automatically.',
		en_GB : 'Click to scroll to next month. Hold mouse button to scroll automatically.',
		en_US : 'Click to scroll to next month. Hold mouse button to scroll automatically.',
		es : 'Presione para pasar al siguiente mes. Deje presionado para pasar varios meses.',
		de : 'Klicken um zum nächsten Monat zu gelangen. Gedr?ckt halten, um automatisch weiter zu scrollen.',
		de_DE : 'Klicken um zum nächsten Monat zu gelangen. Gedr?ckt halten, um automatisch weiter zu scrollen.',
		nl : 'Klik om naar de volgende maand te gaan. Ingedrukt houden om automatisch verder te scrollen.',
		he : 'לחץ למעבר לחודש הבא. לחץ והחזק לגלילה אוטומטית.',
		pl_PL : 'Kliknij aby przejść do następnego miesiąca. Przytrzymaj przycisk myszy aby przewijać automatycznie.',
		zh_CN : '点击转到下月. 按住鼠标来连续翻页.'
	};
	var selectMonthMessage = {
		en : 'Click to select a month.',
		en_GB : 'Click to select a month.',
		en_US : 'Click to select a month.',
		es : 'Presione para seleccionar un mes.',
		de : 'Klicken um Monat auszuwählen.',
		de_DE : 'Klicken um Monat auszuwählen',
		nl : 'Klik om maand te selecteren.',
		he : 'לחץ לבחירת חודש.',
		pl_PL : 'Kliknij aby wybrać miesiąc.',
		zh_CN : '点击选择月份'
	};
	var selectYearMessage = {
		en : 'Click to select a year.',
		en_GB : 'Click to select a year.',
		en_US : 'Click to select a year.',
		es : 'Presione para seleccionar un ano.',
		de : 'Klicken um Jahr auszuwählen.',
		de_DE : 'Klicken um Jahr auszuwählen',
		nl : 'Klik om jaar te selecteren.',
		he : 'לחץ לבחירת שנה.',
		pl_PL : 'Kliknij aby wybrać rok.',
		zh_CN : '点击选择年份'
	};
	var selectDateMessage = {		// do not replace [date], it will be replaced by date.
		en : 'Select [date] as date.',
		en_GB : 'Select [date] as date.',
		en_US : 'Select [date] as date.',
		es : 'Seleccione [date] como fecha.',
		de : 'Wähle [date] als Datum.',
		de_DE : 'Wähle [date] als Datum.',
		nl : 'Kies [date] als Datum.',
		he : 'בחבר ב [date] כתאריך.',
		pl_PL : 'Wybierz [date] jako datę.',
		zh_CN : '选取 [date] 作为日期.'
	};
	var	monthName = {
		en : new Array('January','February','March','April','May','June','July','August','September','October','November','December'),
		en_GB : new Array('January','February','March','April','May','June','July','August','September','October','November','December'),
		en_US : new Array('January','February','March','April','May','June','July','August','September','October','November','December'),
		es : new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'),
		de : new Array('Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'),
		de_DE : new Array('Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'),
		nl : new Array('Januari','Februari','Maart','April','Mei','Juni','Juli','Augustus','September','Oktober','November','December'),
		he : new Array('ינואר','פברואר','מרץ','אפריל','מאי','יוני','יולי','אוגוסט','ספטמבר','אוקטובר','נובמבר','דצמבר'),
		pl_PL : new Array('Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Pańdziernik','Listopad','Grudzień'),
		zh_CN : new Array('一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月')
	};
	var	monthName2 = {
		en : new Array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'),
		en_GB : new Array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'),
		en_US : new Array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'),
		es : new Array('ENE','FEB','MAR','ABR','MAY','JUN','JUL','AGO','SEP','OCT','NOV','DIC'),
		de : new Array('JAN','FEB','MRZ','APR','MAI','JUN','JUL','AUG','SEP','OKT','NOV','DEZ'),
		de_DE : new Array('JAN','FEB','MRZ','APR','MAI','JUN','JUL','AUG','SEP','OKT','NOV','DEZ'),
		nl : new Array('Jan','Feb','Mar','Apr','Mei','Jun','Jul','Aug','Sep','Okt','Nov','Dec'),
		he : new Array('ינו','פבר','מרץ','אפר','מאי','יונ','יול','אוג','ספט','אוק','נוב','דצמ'),
		pl_PL : new Array('Sty','Lut','Mar','Kwi','Maj','Cze','Lip','Sie','Wrz','Paz','Lis','Gru'),
		zh_CN : new Array('一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月')
	};

	function cal_pixels(n)
	{
		return n + 'px';
	}
	function setCalendarStartOnSunday(startOnSunday)
	{
		if (startOnSunday) {
			startAt = 0;
			dayName = {
				en : new Array('Sun','Mon','Tue','Wed','Thu','Fri','Sat'),
				en_GB : new Array('Sun','Mon','Tue','Wed','Thu','Fri','Sat'),
				en_US : new Array('Sun','Mon','Tue','Wed','Thu','Fri','Sat'),
				es : new Array('Dom','Lun','Mar','Mie','Jue','Vie','Sab'),
				de : new Array('So','Mo','Di','Mi','Do','Fr','Sa'),
				de_DE : new Array('So','Mo','Di','Mi','Do','Fr','Sa'),
				nl : new Array('Zo','Ma','Di','Wo','Do','Vr','Za'),
				he : new Array('א','ב','ג','ד','ה','ו','ש'),
				pl_PL : new Array('N','Pon','Wt','Śr','Czw','Pt','Sob'),
				zh_CN : new Array('日','一','二','三','四','五','六')
			};
		}
		else {
			startAt = 1;
			dayName = {
				en : new Array('Mon','Tue','Wed','Thu','Fri','Sat','Sun'),
				en_GB : new Array('Mon','Tue','Wed','Thu','Fri','Sat','Sun'),
				en_US : new Array('Mon','Tue','Wed','Thu','Fri','Sat','Sun'),
				es : new Array('Lun','Mar','Mie','Jue','Vie','Sab','Dom'),
				de : new Array('Mo','Di','Mi','Do','Fr','Sa','So'),
				de_DE : new Array('Mo','Di','Mi','Do','Fr','Sa','So'),
				nl : new Array('Ma','Di','Wo','Do','Vr','Za','Zo'),
				he : new Array('ב','ג','ד','ה','ו','ש','א'),
				pl_PL : new Array('Pon','Wt','Śr','Czw','Pt','Sob','N'),
				zh_CN : new Array('一','二','三','四','五','六','日')
			};
		}
	};
	setCalendarStartOnSunday(false);

function getCalendarDirection()
{
	var node = document.getElementById('calendar');
	if (node.currentStyle)
		return node.currentStyle['direction'];
	else if (window.getComputedStyle)
		return getComputedStyle(node,null).getPropertyValue('direction');
};
	
function setToday()
{
 	today    = new Date();
 	dateNow  = today.getDate();
	monthNow = today.getMonth();
	yearNow  = today.getFullYear();
};
	var crossobj, crossMonthObj, crossYearObj, monthSelected, yearSelected, dateSelected, omonthSelected, oyearSelected, odateSelected, monthConstructed, yearConstructed, intervalID1, intervalID2, timeoutID1, timeoutID2, targetNode, valueHandler, ctlNow, dateFormat, nStartingYear, selDayAction, isPast;
	var visYear  = 0;
	var visMonth = 0;
	var bPageLoaded = false;
	var ie  = document.all;
	var dom = document.getElementById;
	var ns4 = document.layers;
	setToday();
	
	var imgsrc   = new Array('drop1.gif','drop2.gif','left1.gif','left2.gif','right1.gif','right2.gif');
	var img      = new Array();
	var bShow    = false;

	/* hides <select> and <applet> objects (for IE only) */
	function hideElement( elmID, overDiv ) {
		if(ie) {
			for(i = 0; i < document.all.tags( elmID ).length; i++) {
				obj = document.all.tags( elmID )[i];
				if(!obj || !obj.offsetParent) continue;

				// Find the element's offsetTop and offsetLeft relative to the BODY tag.
				objLeft   = obj.offsetLeft;
				objTop    = obj.offsetTop;
				objParent = obj.offsetParent;

				while(objParent.tagName.toUpperCase() != 'BODY') {
					objLeft  += objParent.offsetLeft;
					objTop   += objParent.offsetTop;
					objParent = objParent.offsetParent;
				}

				objHeight = obj.offsetHeight;
				objWidth  = obj.offsetWidth;

				if((overDiv.offsetLeft + overDiv.offsetWidth) <= objLeft);
				else if((overDiv.offsetTop + overDiv.offsetHeight) <= objTop);
				/* CHANGE by Charlie Roche for nested TDs*/
				else if(overDiv.offsetTop >= (objTop + objHeight + obj.height));
				/* END CHANGE */
				else if(overDiv.offsetLeft >= (objLeft + objWidth));
				else {
					obj.style.display = 'none';
				}
			}
		}
	}

	/*
	* unhides <select> and <applet> objects (for IE only)
	*/
	function showElement(elmID) {
		if(ie) {
			for(i = 0; i < document.all.tags( elmID ).length; i++) {
				obj = document.all.tags(elmID)[i];
				if(!obj || !obj.offsetParent) continue;
				obj.style.display = '';
			}
		}
	}

	function HolidayRec (d, m, y, desc) {
		this.d = d;
		this.m = m;
		this.y = y;
		this.desc = desc;
	}

	var HolidaysCounter = 0;
	var Holidays = new Array();

	function addHoliday (d, m, y, desc) {
		Holidays[HolidaysCounter++] = new HolidayRec (d, m, y, desc);
	}
	if (dom) {
		for	(i=0;i<imgsrc.length;i++) {
			img[i] = new Image;
			img[i].src = imgDir + imgsrc[i];
		}
		document.write ('<div onclick="bShow=true" id="calendar" style="z-index:+999;position:absolute;display:none;"><table width="'+((showWeekNumber==1)?250:220)+'" style="font-family:Arial;font-size:11px;border: 1px solid #A0A0A0;" bgcolor="#ffffff"><tr bgcolor="#000066"><td><table width="'+((showWeekNumber==1)?248:218)+'"><tr><td style="padding:2px;font-family:Arial;font-size:11px;"><font color="#ffffff' + '' /*C9D3E9*/ +'"><b><span id="caption"></span></b></font></td><td align="right"><a href="javascript:hideCalendarAndFocus()"><img src="'+imgDir+'close.gif" width="15" height="13" border="0" /></a></td></tr></table></td></tr><tr><td style="padding:5px" bgcolor="#ffffff"><span id="content"></span></td></tr>');

		if (showToday == 1) {
			document.write ('<tr bgcolor="#f0f0f0"><td style="padding:5px" align="center"><span id="lblToday"></span></td></tr>');
		}
			
		document.write ('</table></div><div id="selectMonth" style="z-index:+999;position:absolute;display:none;"></div><div id="selectYear" style="z-index:+999;position:absolute;display:none;"></div>');
	}

	var	styleAnchor = 'text-decoration:none;color:black;';
	var	styleLightBorder = 'border:1px solid #a0a0a0;';

	function swapImage(srcImg, destImg) {
		if (ie) document.getElementById(srcImg).setAttribute('src',imgDir + destImg);
	}
	
	
	function initToday()
	{
		setToday();
		if (showToday == 1) {
			document.getElementById('lblToday').innerHTML =	'<font color="#000066">' + todayString[language] + ' <a onmousemove="window.status=\''+gotoString[language]+'\'" onmouseout="window.status=\'\'" title="'+gotoString[language]+'" style="'+styleAnchor+'" href="javascript:monthSelected=monthNow;yearSelected=yearNow;constructCalendar();">'+dayName[language][(today.getDay()-startAt==-1)?6:(today.getDay()-startAt)]+', ' + dateNow + ' ' + monthName[language][monthNow].substring(0,3) + ' ' + yearNow + '</a></font>';
		}
	
	}

	function initPopupCalendar() {
		if (!ns4)
		{

			crossobj=(dom)?document.getElementById('calendar').style : ie? document.all.calendar : document.calendar;
			hideCalendar();

			crossMonthObj = (dom) ? document.getElementById('selectMonth').style : ie ? document.all.selectMonth : document.selectMonth;

			crossYearObj = (dom) ? document.getElementById('selectYear').style : ie ? document.all.selectYear : document.selectYear;

			monthConstructed = false;
			yearConstructed = false;
			initToday();
			if (getCalendarDirection() == 'rtl')
			{
				var next1='left1.gif';var next2='left2.gif';var prev1='right1.gif';var prev2='right2.gif';
			}
			else
			{	
				var prev1='left1.gif';var prev2='left2.gif';var next1='right1.gif';var next2='right2.gif';
			}
			sHTML1 = '<span id="spanLeft" style="border:1px solid #36f;cursor:pointer" onmouseover="swapImage(\'changeLeft\',\''+prev2+'\');this.style.borderColor=\'#8af\';window.status=\''+scrollLeftMessage[language]+'\'" onclick="decMonth()" onmouseout="clearInterval(intervalID1);swapImage(\'changeLeft\',\''+prev1+'\');this.style.borderColor=\'#36f\';window.status=\'\'" onmousedown="clearTimeout(timeoutID1);timeoutID1=setTimeout(\'StartDecMonth()\',500)" onmouseup="clearTimeout(timeoutID1);clearInterval(intervalID1)">&nbsp<img id="changeLeft" src="'+imgDir+prev1+'" width="10" height="11" border="0">&nbsp</span>&nbsp;';
			sHTML1 += '<span id="spanRight" style="border:1px solid #36f;cursor:pointer" onmouseover="swapImage(\'changeRight\',\''+next2+'right2.gif\');this.style.borderColor=\'#8af\';window.status=\''+scrollRightMessage[language]+'\'" onmouseout="clearInterval(intervalID1);swapImage(\'changeRight\',\''+next1+'\');this.style.borderColor=\'#36f\';window.status=\'\'" onclick="incMonth()" onmousedown="clearTimeout(timeoutID1);timeoutID1=setTimeout(\'StartIncMonth()\',500)" onmouseup="clearTimeout(timeoutID1);clearInterval(intervalID1)">&nbsp<img id="changeRight" src="'+imgDir+next1+'" width="10" height="11" border="0">&nbsp</span>&nbsp;';
			sHTML1 += '<span id="spanMonth" style="border:1px solid #36f;cursor:pointer" onmouseover="swapImage(\'changeMonth\',\'drop2.gif\');this.style.borderColor=\'#8af\';window.status=\''+selectMonthMessage[language]+'\'" onmouseout="swapImage(\'changeMonth\',\'drop1.gif\');this.style.borderColor=\'#36f\';window.status=\'\'" onclick="popUpMonth()"></span>&nbsp;';
			sHTML1 += '<span id="spanYear" style="border:1px solid #36f;cursor:pointer" onmouseover="swapImage(\'changeYear\',\'drop2.gif\');this.style.borderColor=\'#8af\';window.status=\''+selectYearMessage[language]+'\'" onmouseout="swapImage(\'changeYear\',\'drop1.gif\');this.style.borderColor=\'#36f\';window.status=\'\'" onclick="popUpYear()"></span>&nbsp;';

			document.getElementById('caption').innerHTML = sHTML1;

			bPageLoaded=true;
		}
	}

	function hideCalendar() {
		crossobj.display = 'none';
		if (crossMonthObj != null) crossMonthObj.display = 'none';
		if (crossYearObj  != null) crossYearObj.display = 'none';
		showElement('SELECT');
		showElement('APPLET');
	}
	
	function hideCalendarAndFocus() {
		hideCalendar();
		targetNode.focus();
	}

	function padZero(num) {
		return (num	< 10) ? '0' + num : num;
	}

	function constructDate(d,m,y) {
		return formatDate(y,m+1,d, dateFormat);
	}
	/*
	Creates a date string according to a given format
	y: full year (e.g. 1999, 2004);
	m: month number (1..12)
	d: day in month (1..31)
	*/
	function formatDate(y,m,d, format) {
		sTmp = format;
		sTmp = sTmp.replace ('dd','<e>');
		sTmp = sTmp.replace ('d','<d>');
		sTmp = sTmp.replace ('<e>',padZero(d));
		sTmp = sTmp.replace ('<d>',d);
		sTmp = sTmp.replace ('mmmm','<p>');
		sTmp = sTmp.replace ('mmm','<o>');
		sTmp = sTmp.replace ('mm','<n>');
		sTmp = sTmp.replace ('m','<m>');
		sTmp = sTmp.replace ('<m>',m);
		sTmp = sTmp.replace ('<n>',padZero(m));
		sTmp = sTmp.replace ('<o>',monthName[language][m-1]);
		sTmp = sTmp.replace ('<p>',monthName2[language][m-1]);
		sTmp = sTmp.replace ('yyyy',y);
		return sTmp.replace ('yy',padZero(y%100));
	}

	function closeCalendar() {
		hideCalendarAndFocus();
		targetNode.value = constructDate(dateSelected,monthSelected,yearSelected);
		if (valueHandler)
			valueHandler(yearSelected, monthSelected+1, dateSelected);
		valueHandler = null; // clean up
	}

	/*** Month Pulldown	***/
	function StartDecMonth() {
		intervalID1 = setInterval("decMonth()",80);
	}

	function StartIncMonth() {
		intervalID1 = setInterval("incMonth()",80);
	}

	function incMonth () {
		monthSelected++;
		if (monthSelected > 11) {
			monthSelected = 0;
			yearSelected++;
		}
		constructCalendar();
	}

	function decMonth () {
		monthSelected--;
		if (monthSelected < 0) {
			monthSelected = 11;
			yearSelected--;
		}
		constructCalendar();
	}

	function constructMonth() {
		popDownYear()
		if (!monthConstructed) {
			sHTML = "";
			for (i=0; i<12; i++) {
				sName = monthName[language][i];
				if (i == monthSelected){
					sName = '<b>' + sName + '</b>';
				}
				sHTML += '<tr><td id="m' + i + '" onmouseover="this.style.backgroundColor=\'#909090\'" onmouseout="this.style.backgroundColor=\'\'" style="cursor:pointer" onclick="monthConstructed=false;monthSelected=' + i + ';constructCalendar();popDownMonth();event.cancelBubble=true"><font color="#000066">&nbsp;' + sName + '&nbsp;</font></td></tr>';
			}

			document.getElementById('selectMonth').innerHTML = '<table width="70" style="font-family:Arial;font-size:11px;border:1px solid #a0a0a0;" bgcolor="#f0f0f0" cellspacing="0" onmouseover="clearTimeout(timeoutID1)" onmouseout="clearTimeout(timeoutID1);timeoutID1=setTimeout(\'popDownMonth()\',100);event.cancelBubble=true">' + sHTML + '</table>';

			monthConstructed = true;
		}
	}

	function popUpMonth() {
		if (visMonth == 1) {
			popDownMonth();
			visMonth--;
		} else {
			constructMonth();
			crossMonthObj.display='';
			if (getCalendarDirection() == 'rtl')
				crossMonthObj.left = cal_pixels(parseInt(crossobj.left) + 132);
			else
				crossMonthObj.left = cal_pixels(parseInt(crossobj.left) + 50);
			crossMonthObj.top =	cal_pixels(parseInt(crossobj.top) + 26);
			hideElement('SELECT', document.getElementById('selectMonth'));
			hideElement('APPLET', document.getElementById('selectMonth'));
			visMonth++;
		}
	}

	function popDownMonth() {
		crossMonthObj.display = 'none';
		visMonth = 0;
	}

	/*** Year Pulldown ***/
	function incYear() {
		for	(i=0; i<7; i++) {
			newYear	= (i + nStartingYear) + 1;
			if (newYear == yearSelected)
				txtYear = '<span style="color:#006;font-weight:bold;">&nbsp;' + newYear + '&nbsp;</span>';
			else
				txtYear = '<span style="color:#006;">&nbsp;' + newYear + '&nbsp;</span>';
			document.getElementById('y'+i).innerHTML = txtYear;
		}
		nStartingYear++;
		bShow=true;
	}

	function decYear() {
		for	(i=0; i<7; i++) {
			newYear = (i + nStartingYear) - 1;
			if (newYear == yearSelected)
				txtYear = '<span style="color:#006;font-weight:bold">&nbsp;' + newYear + '&nbsp;</span>';
			else
				txtYear = '<span style="color:#006;">&nbsp;' + newYear + '&nbsp;</span>';
			document.getElementById('y'+i).innerHTML = txtYear;
		}
		nStartingYear--;
		bShow=true;
	}

	function selectYear(nYear) {
		yearSelected = parseInt(nYear + nStartingYear);
		yearConstructed = false;
		constructCalendar();
		popDownYear();
	}

	function constructYear() {
		popDownMonth();
		sHTML = '';
		if (!yearConstructed) {
			sHTML = '<tr><td align="center" onmouseover="this.style.backgroundColor=\'#909090\'" onmouseout="clearInterval(intervalID1);this.style.backgroundColor=\'\'" style="cursor:pointer" onmousedown="clearInterval(intervalID1);intervalID1=setInterval(\'decYear()\',30)" onmouseup="clearInterval(intervalID1)"><font color="#000066">-</font></td></tr>';

			j = 0;
			nStartingYear =	yearSelected - 3;
			for ( i = (yearSelected-3); i <= (yearSelected+3); i++ ) {
				sName = i;
				if (i == yearSelected) sName = '<b>' + sName + '</b>';
				sHTML += '<tr><td id="y' + j + '" onmouseover="this.style.backgroundColor=\'#909090\'" onmouseout="this.style.backgroundColor=\'\'" style="cursor:pointer" onclick="selectYear('+j+');event.cancelBubble=true"><font color="#000066">&nbsp;' + sName + '&nbsp;</font></td></tr>';
				j++;
			}

			sHTML += '<tr><td align="center" onmouseover="this.style.backgroundColor=\'#909090\'" onmouseout="clearInterval(intervalID2);this.style.backgroundColor=\'\'" style="cursor:pointer" onmousedown="clearInterval(intervalID2);intervalID2=setInterval(\'incYear()\',30)" onmouseup="clearInterval(intervalID2)"><font color="#000066">+</font></td></tr>';

			document.getElementById('selectYear').innerHTML = '<table width="44" cellspacing="0" bgcolor="#f0f0f0" style="font-family:Arial;font-size:11px;border:1px solid #a0a0a0;" onmouseover="clearTimeout(timeoutID2)" onmouseout="clearTimeout(timeoutID2);timeoutID2=setTimeout(\'popDownYear()\',100)">' + sHTML + '</table>';

			yearConstructed = true;
		}
	}

	function popDownYear() {
		clearInterval(intervalID1);
		clearTimeout(timeoutID1);
		clearInterval(intervalID2);
		clearTimeout(timeoutID2);
		crossYearObj.display= 'none';
		visYear = 0;
	}

	function popUpYear() {
		var leftOffset
		if (visYear==1) {
			popDownYear();
			visYear--;
		} else {
			constructYear();
			crossYearObj.display	= '';
			leftOffset = parseInt(crossobj.left) + document.getElementById('spanYear').offsetLeft;
			if (ie) leftOffset += 6;
			if (getCalendarDirection() == 'rtl')
				leftOffset += 32;
			crossYearObj.left = cal_pixels(leftOffset);
			crossYearObj.top = cal_pixels(parseInt(crossobj.top) + 26);
			visYear++;
		}
	}

	/*** calendar ***/
	function WeekNbr(n) {
		// Algorithm used:
		// From Klaus Tondering's Calendar document (The Authority/Guru)
		// http://www.tondering.dk/claus/calendar.html
		// a = (14-month) / 12
		// y = year + 4800 - a
		// m = month + 12a - 3
		// J = day + (153m + 2) / 5 + 365y + y / 4 - y / 100 + y / 400 - 32045
		// d4 = (J + 31741 - (J mod 7)) mod 146097 mod 36524 mod 1461
		// L = d4 / 1460
		// d1 = ((d4 - L) mod 365) + L
		// WeekNumber = d1 / 7 + 1

		year = n.getFullYear();
		month = n.getMonth() + 1;
		if (startAt == 0) {
			day = n.getDate() + 1;
		} else {
			day = n.getDate();
		}

		a = Math.floor((14-month) / 12);
		y = year + 4800 - a;
		m = month + 12 * a - 3;
		b = Math.floor(y/4) - Math.floor(y/100) + Math.floor(y/400);
		J = day + Math.floor((153 * m + 2) / 5) + 365 * y + b - 32045;
		d4 = (((J + 31741 - (J % 7)) % 146097) % 36524) % 1461;
		L = Math.floor(d4 / 1460);
		d1 = ((d4 - L) % 365) + L;
		week = Math.floor(d1/7) + 1;

		return week;
	}
	function WeekYear(n) {
		// Algorithm used:
		// From Klaus Tondering's Calendar document (The Authority/Guru)
		// http://www.tondering.dk/claus/calendar.html
		// a = (14-month) / 12
		// y = year + 4800 - a
		// m = month + 12a - 3
		// J = day + (153m + 2) / 5 + 365y + y / 4 - y / 100 + y / 400 - 32045
		// d4 = (J + 31741 - (J mod 7)) mod 146097 mod 36524 mod 1461
		// L = d4 / 1460
		// d1 = ((d4 - L) mod 365) + L
		// WeekNumber = d1 / 7 + 1

		year = n.getFullYear();
		month = n.getMonth() + 1;
		if (startAt == 0) {
			day = n.getDate() + 1;
		} else {
			day = n.getDate();
		}

		a = Math.floor((14-month) / 12);
		y = year + 4800 - a;
		m = month + 12 * a - 3;
		b = Math.floor(y/4) - Math.floor(y/100) + Math.floor(y/400);
		J = day + Math.floor((153 * m + 2) / 5) + 365 * y + b - 32045;
		d4 = (((J + 31741 - (J % 7)) % 146097) % 36524) % 1461;
		L = Math.floor(d4 / 1460);
		d1 = ((d4 - L) % 365) + L;
		week = Math.floor(d1/7) + 1;
		if (week == 1 && month == 12)
			return year+1;
		if (week >= 52 && month == 1)
			return year-1;
		return year;
	}
	function constructCalendar () {
		var aNumDays = Array (31,0,31,30,31,30,31,31,30,31,30,31);
		var dateMessage;
		var startDate = new Date (yearSelected,monthSelected,1);
		var endDate;

		if (monthSelected==1) {
			endDate = new Date (yearSelected,monthSelected+1,1);
			endDate = new Date (endDate - (24*60*60*1000));
			numDaysInMonth = endDate.getDate();
		} else {
			numDaysInMonth = aNumDays[monthSelected];
		}

		datePointer = 0;
		dayPointer = startDate.getDay() - startAt;
		
		if (dayPointer<0) dayPointer = 6;

		sHTML = '<table border="0" style="font-family:verdana;font-size:10px;"><tr>';

		if (showWeekNumber == 1) {
			sHTML += '<td width="27"><b>' + weekString[language] + '</b></td><td width="1" rowspan="7" bgcolor="#d0d0d0" style="padding:0px"><img src="'+imgDir+'divider.gif" width="1"></td>';
		}

		for (i = 0; i<7; i++) {
			sHTML += '<td width="27" align="right"><b><font color="#000066">' + dayName[language][i] + '</font></b></td>';
		}

		sHTML += '</tr><tr>';
		
		if (showWeekNumber == 1) {
			sHTML += '<td align="right">' + WeekNbr(startDate) + '&nbsp;</td>';
		}

		for	( var i=1; i<=dayPointer;i++ ) {
			sHTML += '<td>&nbsp;</td>';
		}
	
		for	( datePointer=1; datePointer <= numDaysInMonth; datePointer++ ) {
			dayPointer++;
			sHTML += '<td align="right">';
			sStyle=styleAnchor;
			if ((datePointer == odateSelected) && (monthSelected == omonthSelected) && (yearSelected == oyearSelected))
			{ sStyle+=styleLightBorder }

			sHint = '';
			for (k = 0;k < HolidaysCounter; k++) {
				if ((parseInt(Holidays[k].d) == datePointer)&&(parseInt(Holidays[k].m) == (monthSelected+1))) {
					if ((parseInt(Holidays[k].y)==0)||((parseInt(Holidays[k].y)==yearSelected)&&(parseInt(Holidays[k].y)!=0))) {
						sStyle+= 'background-color:#fdd;';
						sHint += sHint=="" ? Holidays[k].desc : "\n"+Holidays[k].desc;
					}
				}
			}

			sHint = sHint.replace('/\"/g', '&quot;');

			dateMessage = 'onmousemove="window.status=\''+selectDateMessage[language].replace('[date]',constructDate(datePointer,monthSelected,yearSelected))+'\'" onmouseout="window.status=\'\'" ';


			//////////////////////////////////////////////
			//////////  Modifications PinoToy  //////////
			//////////////////////////////////////////////
			if (enablePast == 0 && ((yearSelected < yearNow) || (monthSelected < monthNow) && (yearSelected == yearNow) || (datePointer < dateNow) && (monthSelected == monthNow) && (yearSelected == yearNow))) {
				selDayAction = '';
				isPast = 1;
			} else {
				selDayAction = 'href="javascript:dateSelected=' + datePointer + ';closeCalendar();"';
				isPast = 0;
			}

			if ((datePointer == dateNow) && (monthSelected == monthNow) && (yearSelected == yearNow)) {	///// today
				sHTML += "<b><a "+dateMessage+" title=\"" + sHint + "\" style=\"white-space:nowrap;"+sStyle+"\" "+selDayAction+"><font color=#ff0000>&nbsp;" + datePointer + "</font>&nbsp;</a></b>";
			} else if (dayPointer % 7 == (startAt * -1)+1) {									///// SI ES DOMINGO
				if (isPast==1)
					sHTML += "<a "+dateMessage+" title=\"" + sHint + "\" style='"+sStyle+"' "+selDayAction+">&nbsp;<font color=#909090>" + datePointer + "</font>&nbsp;</a>";
				else
					sHTML += "<a "+dateMessage+" title=\"" + sHint + "\" style='"+sStyle+"' "+selDayAction+">&nbsp;<font color=#54A6E2>" + datePointer + "</font>&nbsp;</a>";
			} else if ((dayPointer % 7 == (startAt * -1)+7 && startAt==1) || (dayPointer % 7 == startAt && startAt==0)) {	///// SI ES SABADO
				if (isPast==1)
					sHTML += "<a "+dateMessage+" title=\"" + sHint + "\" style='"+sStyle+"' "+selDayAction+">&nbsp;<font color=#909090>" + datePointer + "</font>&nbsp;</a>";
				else
					sHTML += "<a "+dateMessage+" title=\"" + sHint + "\" style='"+sStyle+"' "+selDayAction+">&nbsp;<font color=#54A6E2>" + datePointer + "</font>&nbsp;</a>";
			} else {																			///// CUALQUIER OTRO DIA
				if (isPast==1)
					sHTML += "<a "+dateMessage+" title=\"" + sHint + "\" style='"+sStyle+"' "+selDayAction+">&nbsp;<font color=#909090>" + datePointer + "</font>&nbsp;</a>";
				else
					sHTML += "<a "+dateMessage+" title=\"" + sHint + "\" style='"+sStyle+"' "+selDayAction+">&nbsp;<font color=#000066>" + datePointer + "</font>&nbsp;</a>";
			}

			sHTML += '';
			if ((dayPointer+startAt) % 7 == startAt) {
				sHTML += '</tr><tr>';
				if ((showWeekNumber == 1) && (datePointer < numDaysInMonth)) {
					sHTML += '<td align="right">' + (WeekNbr(new Date(yearSelected,monthSelected,datePointer+1))) + '&nbsp;</td>';
				}
			}
		}

		document.getElementById('content').innerHTML   = sHTML
		document.getElementById('spanMonth').innerHTML = '&nbsp;' +	monthName[language][monthSelected] + '&nbsp;<img id="changeMonth" src="'+imgDir+'drop1.gif" width="12" height="10" border="0">'
		document.getElementById('spanYear').innerHTML  = '&nbsp;' + yearSelected	+ '&nbsp;<img id="changeYear" src="'+imgDir+'drop1.gif" width="12" height="10" border="0">';
	}

	function showCalendar(ctl, ctl2, format, lang, past, fx, fy) {
		if (lang != null && lang != '') language = lang;
		if (past != null) enablePast = past;
		else enablePast = 0;
		if (fx != null) fixedX = fx;
		else fixedX = -1;
		if (fy != null) fixedY = fy;
		else fixedY = -1;

		if (showToday == 1) {
			document.getElementById('lblToday').innerHTML = '<font color="#000066">' + todayString[language] + ' <a onmousemove="window.status=\''+gotoString[language]+'\'" onmouseout="window.status=\'\'" title="'+gotoString[language]+'" style="'+styleAnchor+'" href="javascript:monthSelected=monthNow;yearSelected=yearNow;constructCalendar();">'+dayName[language][(today.getDay()-startAt==-1)?6:(today.getDay()-startAt)]+', ' + dateNow + ' ' + monthName[language][monthNow].substring(0,3) + ' ' + yearNow + '</a></font>';
		}
		popUpCalendar(ctl, ctl2, null, format);
	}

	/*
		Pops up the calendar.
		ctl: the document node used to position the popup calendar
		target: the document node in which the formatted value should be placed (and from which the initial value should be taken)
		handler: a function to be called when a date has been selected. 
		         the function accepts 3 arguments (y, m, d) where y is the full year (e.g. 2004)
		         m is the month (1..12) and d is the day (1..31)
	*/
	
	function parseDate(text, format)
	{
		var formatChar = ' ';
		var aFormat = format.split(formatChar);
		if (aFormat.length < 3) {
			formatChar = '/';
			aFormat = format.split(formatChar);
			if (aFormat.length < 3) {
				formatChar = '.';
				aFormat = format.split(formatChar);
				if (aFormat.length < 3) {
					formatChar = '-';
					aFormat = format.split(formatChar);
					if (aFormat.length < 3) {
						return null;					// invalid date format

					}
				}
			}
		}

		var tokensChanged = 0;
		var aData =	text.split(formatChar);			// use user's date

		var y, m, d;
		for (var i=0; i<3; i++) {
			if ((aFormat[i] == "d") || (aFormat[i] == "dd")) {
				d = parseInt(aData[i], 10);
				tokensChanged++;
			} else if ((aFormat[i] == "m") || (aFormat[i] == "mm")) {
				m = parseInt(aData[i], 10) - 1;
				tokensChanged++;
			} else if (aFormat[i] == "yyyy") {
				y = parseInt(aData[i], 10);
				tokensChanged++;
			} else if (aFormat[i] == "mmm") {
				for (j=0; j<12; j++) {
					if (aData[i] == monthName[language][j]) {
						m=j;
						tokensChanged++;
					}
				}
			} else if (aFormat[i] == "mmmm") {
				for (j=0; j<12; j++) {
					if (aData[i] == monthName2[language][j]) {
						m = j;
						tokensChanged++;
					}
				}
			}
		}

		if ((tokensChanged != 3)|| isNaN(d) || isNaN(m) || isNaN(y)) 
			return null; // Invalid format or invalid date string

		return {d:d, m:m, y:y};
	}
	function popUpCalendar(ctl, target, handler, format) {
		initToday();
		var leftpos = 0;
		var toppos  = 0;
		if (bPageLoaded) {
			if (crossobj.display == 'none') {
				valueHandler = handler;
				targetNode = target;
				dateFormat = format;
	
				var date = parseDate(target.value, format);
				if (date == null)
				{
					date = {d:dateNow, m:monthNow, y:yearNow};
				}
				dateSelected = date.d;
				monthSelected = date.m;
				yearSelected = date.y;

				odateSelected  = dateSelected;
				omonthSelected = monthSelected;
				oyearSelected  = yearSelected;

				var p = screen_position(ctl);
				var s = visible_size(ctl);
				var window_size = page_size(window);
				
				
				crossobj.left = cal_pixels(0);
				crossobj.top = cal_pixels(0);
				constructCalendar (1, monthSelected, yearSelected);
				crossobj.display = '';
				var cal=document.getElementById('calendar');
				var calSize = visible_size(cal);
				var scroll = getScroll(cal);
				var left = (fixedX == -1) ? p.left  : fixedX;
				var left0=left;
				var top = (fixedY == -1) ? p.top + s.height + 2 : fixedY;
				if (left + calSize.width >window_size.width-10)
					left = window_size.width-calSize.width-10;
				if (left<0)
					left=0;
				if (top + calSize.height > window_size.height-10)
					top = window_size.height-10-calSize.height-10;
				if (top<0)
					top=0;
				crossobj.left = cal_pixels(left-scroll.left);
				crossobj.top = cal_pixels(top-scroll.top);

				hideElement('SELECT', document.getElementById('calendar'));
				hideElement('APPLET', document.getElementById('calendar'));			

				if (ctl == target) // True if 'Date Input'. False if 'Prompt for Date'. 
					bShow = true;
			} else {
				hideCalendar();
				if (ctlNow!=ctl) popUpCalendar(ctl, target, handler, format);
			}
			ctlNow = ctl;
		}
	}

	document.onkeydown = function hidecal1 (e) {
		if (window.event && (window.event.keyCode == 27 || window.event.keyCode == 9)) // IE
			hideCalendar();
		else if (e && e.keyCode && (e.keyCode == 27 || e.keyCode == 9)) // Mozilla
			hideCalendar();
		
		
	}
	document.onclick = function hidecal2 () {
		if (!bShow) hideCalendar();
		bShow = false;
	}
	function visible_size(node)
	{
		var width = node.offsetWidth;
		var height = node.offsetHeight;
		if (node == node.ownerDocument.body && node.clientHeight)
		{
			height = node.clientHeight;
		};
		//TODO handle width
		return {'height':height,'width':width};
	};
	if (window.getScreenPosition)
		screen_position=getScreenPosition;
	else
	screen_position = function(node)
	{
		var x = 0;
		var y = 0;
		var currentNode = node;
		do
		{
			x+=currentNode.offsetLeft;
			y+=currentNode.offsetTop;
			currentNode = currentNode.offsetParent;
		}
		while (currentNode);
		currentNode = node;
		do
		{	
			if (currentNode.scrollLeft != undefined)
			{	
		  		x -= currentNode.scrollLeft;
  				y -= currentNode.scrollTop;
	  			currentNode = currentNode.offsetParent;
	  		}
	  		else
		  		currentNode = null;
		}
		while (currentNode);
		return {'left':x, 'top':y};
	};
function page_size(w)
{
	var size = {};
	if (w.innerHeight) 
	{
		size.width = w.innerWidth;
		size.height = w.innerHeight;
	}
	else if (w.document.documentElement && w.document.documentElement.clientHeight)
	{
		size.width = w.document.documentElement.clientWidth;
		size.height = w.document.documentElement.clientHeight;
	}
	else if (w.document.body) 
	{
		size.width = w.document.body.clientWidth;
		size.height = w.document.body.clientHeight;
	}
	return size;
};

function page_scroll(w)
{
	var scroll = {};
	if (w.pageYOffset) 
	{
		scroll.x = w.pageXOffset;
		scroll.y = w.pageYOffset;
	}
	else if (w.document.documentElement && w.document.documentElement.scrollTop)
	{
		scroll.x = w.document.documentElement.scrollLeft;
		scroll.y = w.document.documentElement.scrollTop;
	}
	else if (w.document.body) // all other Explorers
	{
		scroll.x = w.document.body.scrollLeft;
		scroll.y = w.document.body.scrollTop;
	}
	return scroll;
};

if (!window.getScroll)
{
	if (opener.getScroll)
		getScroll=opener.getScroll;
}	

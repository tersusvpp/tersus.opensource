/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function initNav()
{
 window.rtlSuffix = '';
viewTabTrailer = '<td width="100%" align="right" valign="bottom" class="nopad"><img src="images/innerNav/bottomLineRightPart.gif" alt=""  style="width:5px;height:1px;border:none;vertical-align:bottom;"></td>';
if (window.textDirection == 'rtl')
{
	window.rtlSuffix = '_rtl';
	viewTabTrailer = '<td width="100%" align="left" valign="bottom"><img src="images/innerNav/bottomLineRightPart_rtl.gif" alt="" style="width:5px;height:1px;border:none;"></td>';
}
viewTabSeparator = '<td class="nopad"><img src="images/spacer.gif" alt="" style="width:2px;height:1px;border:none;"></td>';
perspectiveTabSeparator ='<tr><td style="height:3px;"><img src="images/spacer.gif" alt="" style="display:block;width:1px;height:1px;border:none;"></td></tr>';
};
initNav();
tersus.nav = {};

function navigation_setup()
{
	tersus.load();
}
tersus.reloadView = function ()
{
	tersus.setPerspectiveAndView();
	refreshPerspectiveTabs();
	refreshViewTabs();
	refreshContent();
};

function refreshViewTabs()
{

	var tabs = document.getElementById('main.viewTabs');
	if (! tabs)
		return;
	var views = perspective.views;
	var tabRow = tabs.parentNode;
	var content = '';
	if (views.length > 1 || !tersus.compactNavigationTabs)
	{
		content += '<table class="tl" style="width:100%;height:22px;"><tr>';
		for (var i=0; i< views.length; i++)
		{
			var v = views[i];
			if (i>0)
				content += viewTabSeparator;
			var tab;
			if (v == window.view)
				tab = viewTabSelected(v.name);
			else
				tab = viewTabNormal(v.name);
			
			content += tab;
		}
		content += viewTabTrailer;
		content += '</tr></table>';
	}
	tabs.innerHTML = content;
};

function refreshPerspectiveTabs()
{	
	var table = document.getElementById('main.perspectiveTabs');
	if (! table)
		return;
	if (perspectives.length == 1 && tersus.compactNavigationTabs)
	{
		table.parentNode.removeChild(table);
		return;
	}
	var content = '<table  style="width:100px" class="tl">';
	for (var i=0; i<perspectives.length; i++)
	{
		var perspective = perspectives[i];
		if (i>0)
			content += perspectiveTabSeparator;
		if (perspective == window.perspective)
			content += perspectiveTabSelected(perspective.name);
		else
			content += perspectiveTabNormal(perspective.name);
	}
	content += '</table>';
	table.innerHTML = content;
};

function viewTabNormal(name)
{
	return '<td valign="bottom" class="nopad"><table class="tl">'
	+'<tr class="innerNavTabNormalBGI" onclick="selectView(\''+name+'\')">'
	+'<td class="nopad"><img src="images/innerNav/L'+rtlSuffix+'.gif" alt="" style="display:block;width:15px;height:20px;border:none;"></td>'
	+'<td class="innerNavTabNormal" nowrap>'+name+'</td>'
	+'<td class="nopad"><img src="images/innerNav/R'+rtlSuffix+'.gif" alt="" style="display:block;width:6px;height:20px;border:none;"></td>'
	+'</tr></table></td>';
	
};

function viewTabSelected(name)
{
	return '<td style="height:22px;vertical-align:bottom;padding:0;"><table class="tl">'
	+'<tr class="innerNavTabSelectedBGI nopad" onclick="selectView(\''+name+'\')">'
	+'<td class="nopad"><img src="images/innerNav/L_d'+rtlSuffix+'.gif" alt="" style="display:block;width:17px;height:22px;border:none;"></td>'
	+'<td class="innerNavTabSelected nopad" nowrap>'+name+'</td>'
	+'<td class="nopad"><img src="images/innerNav/R_d'+rtlSuffix+'.gif" alt=""  style="display:block;width:6px;height:22px;border:none;"></td>'
	+'</tr></table></td>';

};

function perspectiveTabNormal(name)
{
	return '<tr><td class="nopad"><table class="tl"><tr>'
	+'<td valign="top" class="nopad"><img src="images/spacer.gif" alt="" style="display:block;width:9px;height:1px;border:none;"></td>'
	+'<td style="width:100%;height:63px" class="leftNavTabNormal nopad" onclick="selectPerspective(\''+name+'\')">'+name+'</td>'
	+'</tr></table></td></tr>';
};

function perspectiveTabSelected(name)
{
	return '<tr><td style="height:63px;" class="leftNavTabSelected nopad" onclick="selectPerspective(\''+name+'\')">'+name+'</td></tr>';
};


function forceReload()
{
	tersus.history.location = null;
	window.view = null;
};
function selectView(name)
{
	forceReload();
/*	var view = getView(name);
	if (view == window.view)
		tersus.nav.reload();
	else
	{
		var perspective = window.perspective;
		var newURL = tersus.nav.createURL(perspective.name, view.name);
		tersus.gotoURL(newURL, false, true);
	}*/
	tersus.switchView(null,name);
};




function selectPerspective(name)
{
	forceReload();
	var perspective = getPerspective(name);
	var view = perspective.views[0];
	tersus.switchView(perspective.name,view.name);
};

function setPerspective(p)
{
	window.perspective = p;
	window.view = window.perspective.views[0];
	refreshPerspectiveTabs();
	refreshViewTabs();
	refreshContent();
};

function refreshContent()
{
	var req = createXMLHTTPRequest();
	var viewPath = view.path;
	var contentArea = window.document.getElementById('main.content');
	tersus.loadView(contentArea,view);
	
};



tersus.getCurrentViewURL = function()
{
	return tersus.nav.createURL(window.perspective.name,window.view.name);
};
tersus.nav.createURL  = function(perspectiveName, viewName)
{
	return tersus.rootURL+'#'+tersus.PERSPECTIVE+'='+encodeURIComponent(perspectiveName)+'&'+tersus.VIEW+'='+encodeURIComponent(viewName);
};
tersus.getInternalParameters = function()
{
	var params = {};
	for (var p in tersus.searchParams)
	{
		if (p!= tersus.PERSPECTIVE && p != tersus.VIEW)
			params[p] = tersus.searchParams[p];
	}
	return params;
};




var setDirection = tersus.setDirection;
tersus.setDirection = function tersus_nav_setDirection (direction)
{
	setDirection(direction);
	initNav();
	refreshViewTabs();
	refreshPerspectiveTabs();
};

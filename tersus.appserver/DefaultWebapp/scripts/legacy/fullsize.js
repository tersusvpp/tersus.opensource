// overrides _onResize to ensure we are using the full size of the display.  This is not needed in "modern" ui because we can set width="100%" on the body
tersus._onResize = function onResize()
{
    $('body,#tersus\\.content').css('min-height',window.innerHeight-7+'px'); // -7 pixels so vertical scroll is not created when not needed (e.g. Pop-in)  
    $('body').css('box-sizing','border-box');
    if (window.currentRootDisplayNode)
        window.currentRootDisplayNode.onResize();
};
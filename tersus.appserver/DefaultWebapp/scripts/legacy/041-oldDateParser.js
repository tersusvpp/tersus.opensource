DATE_FORMAT='mmmm dd yyyy';
tersus.Date.prototype.parseInput = function (str)
{
    if (str == null || str == '')
        return null;
    var d = parseDate(str, DATE_FORMAT);
    if (d == null)
        return null;
    var constructor = this.constructor;
    var node = new constructor();
    var date = new tersus.DateValue();
    date.year=d.y;
    date.month=d.m+1;
    date.day=d.d;
    node.leafValue=date;
    return node;
};


tersus.Date.prototype.validateInput = function (s, props)
{
    if ( s == null || s == '')
        return null;
    if (parseDate(s, DATE_FORMAT))
        return null;
    else
        return tersus.Messages.INVALID_DATE;
};
(function($, undefined)
{
//    var endEvents = $.map(['webkitAnimationEnd','webkitTransitionEnd','transitionend','animationend','otransitionend','oanimationend']
    var e=[];
    if ('TransitionEvent' in window)
        e.push('transitionend');
    else if ('WebkitTransitionEnd' in window)
        e.push ('webkitTransitionEnd');
    if ('AnimationEvent' in window)
        e.push('animationend');
    else if ('WebKitAnimationEvent' in window)
        e.push ('webkitAnimationEnd');
    var endEvents = e.join(' ');
    $.fn.animationEnd = function( callback )
    {
        if(endEvents)
        {
            this.one(endEvents, null, null, callback);
        }
        else
        {
            setTimeout(callback,1);
            return this;
        }
    };
    tersus.transition = function($from, $to, activeClass, animationClass, callback)
    {
    	var f = function()
    	{
    		$from.add( $to ).removeClass("out in reverse " + animationClass );
    		$from.removeClass(activeClass );
    		if (callback)
    			callback();
    		tersus.transitionEnd=null;
    	};
    	
    	if (tersus.transitionEnd)
    	{
    		tersus.transitionEnd.call();
    		tersus.transitionEnd=f;
    	}
    	else
    	{
    		tersus.transitionEnd=f;
    		$(window).animationEnd(function(){if (tersus.transitionEnd) tersus.transitionEnd();});
    	}
    	$from.addClass( animationClass + " out" );
    	$to.addClass(activeClass + " " + animationClass + " in");
    };
 
})(jQuery);

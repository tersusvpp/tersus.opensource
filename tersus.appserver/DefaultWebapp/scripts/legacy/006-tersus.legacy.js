(function()
{
    var base=tersus.init;
    tersus.init = function()
    {
        base();
        if (tersus.useIFrames)
            tersus.async.exec(tersus.initServiceResponseFrames);
        if (window.browser == 'IE')
            window.document.body.onunload = tersus.onUnload;
        else
            window.onunload = tersus.onUnload;
        tersus.createClassNameList();
    };
})();
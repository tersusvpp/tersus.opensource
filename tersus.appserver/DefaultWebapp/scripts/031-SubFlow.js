/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 
function SubFlow(role, modelId)
{
	this.role = role;
	this.modelId = modelId;
};
SubFlow.prototype = new Element();
SubFlow.prototype.constructor = SubFlow;
SubFlow.prototype.isSubFlow = true;
SubFlow.prototype.createChild = function createChild(parent, modelId)
{
	debugAssertion(modelId == null, "Expecting SubFlow.createChild to always receive null modelId");
	var child = this.createChildInstance(modelId);
	child.mainWindow = parent.mainWindow;
	child.currentWindow = parent.currentWindow;
	child.parent = parent;
	child.element = this;
	if (child.initialize)
		child.initialize();
	return child;
};
SubFlow.prototype.init = function SubFlow_init(parent)
{
	if (! this.isRepetitive && ! tersus.Conventions.isEventHandler(this))
	{
		var child = this.createChild(parent);
		this.addChild(parent, child);
		child.setInitialStatus();
	}
};
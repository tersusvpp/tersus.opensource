/*********************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function startTrace(silent)
{
	closeTrace();
	
	window.trace = new Trace();
	window.trace.open(silent);
};
function showTrace()
{
	var traceWindow = window.open("","traceWindow");
	traceWindow.document.write('<textarea cols="120" rows="80">' + trace.buffer.join('') + '</textarea>');
	window.trace.window = traceWindow;
	setTimeout(function(){traceWindow.focus();}, 100);
};
function stopTrace(silent)
{
	if (!silent)
	{
		if (! window.trace)
			tersus.notify('Message','Tracing not started');
		else
			tersus.notify('Message',"Trace file '"+window.trace.filename+"' closed.");
	}
	closeTrace();
};
function closeTrace()
{
	if (window.trace)
	{
		window.trace.flush();
		if (window.trace.window) window.trace.window.close();
	}
	deleteCookie(Trace.COOKIE_NAME);
	window.trace = null;
};

function initTrace()
{
	var file = getCookie(Trace.COOKIE_NAME);
	if (file)
	{
		tersus.notify("Message",'Using trace file ' + file);
		window.trace = new Trace();
		window.trace.filename=file;
	}
};
function Trace() 
{

	this.buffer = [];
	this.elementStack = [];
	this.indentation = '';
};

Trace.prototype.serializer = new Serializer();
Trace.prototype.serializer.addWhitespace=false;
Trace.prototype.serializer.hideSecretValues=true;

Trace.COOKIE_NAME='tersus_trace_file';
Trace.prototype.open = function(silent)
{
	this.filename = this.openTraceFile();
	setCookie(Trace.COOKIE_NAME, this.filename);	
	if (!silent)
		tersus.notify('Message',"Tracing started. The trace file is '"+this.filename +"'");
	
};
Trace.prototype.openTraceFile = function()
{
	var req = createXMLHTTPRequest();
	var url = tersus.getTraceBaseURL()+'NewFile';
	req.open('POST', url, false);
	req.send(null);
	if (req.status != 200)
	{
		tersus.notify('Message','Failed to create trace file');
		return null;
	}
	else
	{
		var filename=''+req.responseText;
		return filename;
	}
};
Trace.prototype.flush=function()
{
	if (this.buffer.length == 0)
		return;
	var prefix = "// Client side trace\n";
	var content = prefix + this.buffer.join('');
	this.buffer.splice(0, this.buffer.length); // remove all
	var url = tersus.getTraceBaseURL()+encodeURIComponent(this.filename);
	var req = createXMLHTTPRequest();
	req.open('POST', url, false);
	req.send(content);
	if (req.readyState == 4 && req.status != 200)
	{
		tersus.notify('Message','Failed to write to trace file');
	}
	
};
Trace.prototype.close = function()
{
	this.traceWindow.close();
};
Trace.prototype.STARTED= 'Started';
Trace.prototype.FINISHED= 'Finished';
Trace.prototype.WAITING= 'Waiting';
Trace.prototype.RESUMED= 'Resumed';
Trace.prototype.PAUSED = 'Paused';
Trace.prototype.RESPONDED = 'Responded';
Trace.prototype.INVOKED = 'Invoked';
Trace.prototype.VALUE = 'Value';
Trace.prototype.ACTIVATED = 'Activated';
Trace.prototype.SET = 'Set';
Trace.prototype.ACCUMULATED = 'Accumulated';
Trace.prototype.REMOVED = 'Removed';
Trace.prototype.CREATED = 'Created';
Trace.prototype.CLICKED = 'Clicked';
Trace.prototype.SQL = 'SQL';

Trace.prototype.location = function (node, element, suffix)
{
	return new tersus.RuntimeLocation(node,element, suffix);
};
Trace.prototype.started = function(node) 
{
	if (node.isDisplayNode && ! node.isButton)
		return; // We don't really care about 'start' of display nodes
	this.trace(node.type, this.STARTED, this.location(node), this.getTriggerValues(node));
};
Trace.prototype.getTriggerValues = function(node)
{
	var details=[];
	var count = 0;
	if (node.triggers)
	{
		details.push('{');
		for (var i=0; i<node.triggers.length; i++)
		{
			var trigger = node.triggers[i];
			var serialization = null;
			if (trigger.isRepetitive)
			{
				var values  = trigger.getChildren(node);
				if (values.length > 0)
					serialization = this.serializer.serialize(values);
			}
			else
			{
				var value = trigger.getChild(node);
				if (value != null)
					serialization = this.serializer.serialize(value);
			}
			if (serialization != null)
			{
				
				if (count>0)
				{
					details.push(',');
				}
				++count;
				this.serializer._quote(trigger.role, details);
				details.push(':');
				details.push(serialization);
			}
		}
		details.push('}');
	}
	return details.join('');
};
Trace.prototype.resumed = function(node) 
{
	//No tracing for 'resumed' - nothing interesting here (?)
};
Trace.prototype.finished = function(node)
{
	this.trace(node.type, this.FINISHED, this.location(node));
};
Trace.prototype.waiting = function(node)
{
	//No tracing for 'waiting' - nothing interesting here (?)
};
Trace.prototype.charged = function(node, exit, value)
{
	var details = null;
	if (value != null)
	{
		details = this.serializer.serialize(value);
	}
	var event = this.SET;
	if (exit.isRepetitive)
		event = this.ACCUMULATED;
	this.trace(exit.type, event, this.location(node,exit),details,null);
	
};
Trace.prototype.sql = function(node, sql, values)
{
	var l = [];
	if (values)
	{
		l.push(' values: ');
		for (var i=0;i<values.length;i++)
		{
			if (i>0)
				l.push(',');
			var v = values [i];
			if (v == null)
				l.push('null');
			else if (typeof(v) == 'string')
			{
				this.serializer._quote(v,l);
			}
			else
				l.push(v);
		}
	}
	var valuesStr = l.join('');
	this.trace(null,this.SQL,this.location(node),"Executing statement: '" + sql + "'" + valuesStr, null);
}

Trace.prototype.paused = function(node)
{
	//No tracing for 'pause' - we want the trace to simulate the synchronous behavior
};
Trace.prototype.flow = function(flow,parent, details)
{
	this.trace(flow.type, this.ACTIVATED, this.location(parent,flow), details);
};

Trace.prototype.flowValue = function(flow, parent, value)
{
	var lastElement = flow.target.getElement(parent);
	if (!lastElement)
		return;//This can happen when elements are excluded from the client-side model because of permissions
	var eventType = '?';
	if (flow.operation == Operation.REPLACE)
		eventType = this.SET;
	else if (flow.operation == Operation.REMOVE)
		eventType = this.REMOVED;
	else if (flow.operation == Operation.ADD)
		eventType = this.ACCUMULATED;
	var pp = this.location(parent).path.str;
	var displayPath = pp ? pp + "/"+flow.target : flow.target;
	this.trace(lastElement.type, eventType, this.location(parent,flow,"Value"),this.serializer.serialize(value),null,displayPath);
};
Trace.prototype.link = function(flow, filename, comment)
{
	this.trace(null,this.LINK, this.location(flow), "'"+filename+"' - "+ comment);
};

Trace.prototype.invoked = function(node)
{
	var details = null;
	this.trace(node.type, this.INVOKED, this.location(node), this.getTriggerValues(node));
};

Trace.prototype.activated = function(node)
{
	this.trace(node.type, this.ACTIVATED, this.location(node.parent, node.element), this.serializer.serialize(node.value));
}

Trace.prototype.responded = function(node)
{
	this.trace(node.type, this.RESPONDED, this.location(node));
};

Trace.prototype.trace = function(objectType, event, location, details, comment, displayPath)
{
	if (location && (location.baseModelId != this.lastBaseModelId || this.buffer.length==0) )
	{
		// We need to write out the base model id either if this is the first record or if the base model id changed
		this.buffer.push('@');
		this.buffer.push(location.baseModelId);
		this.buffer.push('\n');
		this.lastBaseModelId = location.baseModelId;
	}
	if (!objectType)
		objectType = "-";
	if (!details)
		details = "-";
	if (! comment)
		comment = "-";
	var path = location.path?location.path.str:null;
	if (! path)
		path = "-";
	if (!displayPath)
		displayPath = "-";
	var now=new Date();
	var time = Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
	
	this.buffer.push(objectType,"\t",event,"\t",path,"\t");
	this.addEscaped(details);
	this.buffer.push("\t",comment,"\t",displayPath,"\t",time,"\n");
};
Trace.prototype.addEscaped = function (s)
{
	var s1 = s.replace(/\n/g,'\\n').replace(/\r/g,'\\r').replace(/\t/g,'\\t');
	this.buffer.push(s1);
};
Trace.prototype.event = function(node, event)
{
	if (event == Events.ON_CLICK)
	{
		var location = this.location(node);
		var displayPath = location.path.str;
		location.path = null; // We send a location with an empty path to 'force' the event to appear as a top level event
		this.trace(node.type,this.CLICKED,location,null/*details*/,null/*comment*/,displayPath); 
	}
};

initTrace();

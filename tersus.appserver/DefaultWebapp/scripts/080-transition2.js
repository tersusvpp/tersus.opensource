tersus.transition2 = function($from, $to, activeClass, fromTransitionClass, toTransitionClass, transitionEndClass,  callback) {
    if ($from && $to && $from[0] == $to[0]) {
        callback();
        return;
    }
    if ($from && $from.length > 0 && $.support.transition) {
        $to.addClass(toTransitionClass).addClass(activeClass);
        $from.addClass(fromTransitionClass);
        $to[0].offsetWidth;// force reflow
        $from[0].offsetWidth;
        $to.one($.support.transition.end, function() {
            $to.removeClass(transitionEndClass+" "+toTransitionClass);
            $from.removeClass(transitionEndClass+" "+fromTransitionClass+" "+activeClass);
            if (callback)
                callback();
        });
        $to.addClass(transitionEndClass);
        $from.addClass(transitionEndClass);
    } else {
        $from.removeClass(activeClass);
        $to.addClass(activeClass);
        if (callback)
            tersus.async.exec(callback);
    }
};
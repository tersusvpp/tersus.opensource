/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 
function Trigger(role, modelId)
{
	this.role = role;
	if (modelId)
		this.modelId = modelId;
};
Trigger.prototype = new Element();
Trigger.prototype.constructor = Trigger;
Trigger.prototype.type='Trigger';

Trigger.prototype.replaceChild = function(parent, value)
{
	if (parent.isInputSet) // Special case: trigger of a repetitive sub-process - we accumulate the value
	{
		if (! parent.children[this.role])
			parent.children[this.role] = [];
		parent.children[this.role].push(child);
		
	}
	else
		Element.prototype.replaceChild.call(this, parent, value);
};
Trigger.prototype.removeChildren = function(parent)
{
	makeAssertion( this.isRepetitive);
	if (parent.children)
		parent.children[this.role] = [];
};
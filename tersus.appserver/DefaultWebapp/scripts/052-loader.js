/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.PERSPECTIVE = 'Perspective';
tersus.VIEW = 'View';
tersus.preloadAll = true;
tersus.onResize = function()
{
	tersus.async.exec(tersus._onResize, false /* don't wait */);
};


tersus.initialResize = function()
{
	tersus.onResize();
	if (rootDisplayNode.status == FlowStatus.WAITING_FOR_INPUT)
		$(window).unbind('ter-process-end',tersus.initialResize);
};
$(window).bind('ter-process-end',tersus.initialResize); 
tersus._onResize = function onResize()
{
    if (window.currentRootDisplayNode)
	window.currentRootDisplayNode.onResize();
    tersus.unregisterPopupNodes();
    for (var popupId in tersus.popupNodes)
    {
        node = tersus.popupNodes[popupId].onResize();
    }

};
function getPerspective(name)
{
	for (var i=0; i<window.perspectives.length; i++)
	{
		if (window.perspectives[i].name == name)
		{
			return window.perspectives[i];
		}
	}
	return null;
};
function getView(name)
{
	for (var i=0; i< window.perspective.views.length; i++)
	{
		var v = perspective.views[i];
		if (v.name == name)
		{
			return  v;
		}
	}
	return null;
};

tersus.selectView=function(targets)
{
    var width = $(window).width();
    var height = $(window).height();
    if (width < 768 || (height < 768 && width < 1024)) // Mobile
        return targets.mobileView || targets.tabletView || targets.desktopView || targets.defaultView;
    else if (width <= 1024) // Tablet
        return targets.tabletView || targets.desktopView || targets.defaultView;
    else //Desktop
        return targets.desktopView || targets.tabletView || targets.defaultView;
};

tersus.setPerspectiveAndView = function()
{
	var perspectiveName = tersus.searchParams[tersus.PERSPECTIVE];
	var viewName = tersus.searchParams[tersus.VIEW];
	window.perspective = getPerspective(perspectiveName);
	if (!window.perspective)
		window.perspective = window.perspectives[0];
	window.view = getView(viewName);
	if (!window.view)
	{
	    var targets = {};
	    var views = window.perspective.views;
	    targets.defaultView=views[0];
	    for (var i=0;i<views.length;i++)
	    {
	        var v = views[i];
	        if (v.name == '<Mobile View>')
	            targets.mobileView=v;
	        else if (v.name == '<Tablet View>')
	            targets.tabletView=v;
            else if (v.name == '<Desktop View>')
                targets.desktopView= v;
	    }
		window.view = tersus.selectView(targets);
	}
};

tersus.load = function load()
{
	var cache = window.applicationCache;
	if (cache) 
	{
		if (tlog) tlog('tersus.load: cacheStatus='+cache.status);
		switch (cache.status)
		{
			case cache.UPDATEREADY:
				tersus.reloadApplication(); 
				return;
			case cache.CHECKING:
			case cache.DOWNLOADING:
			case cache.UPDATING:
			cache.onupdateready = function(){if(tlog) tlog('OnUpdateReady');setTimeout(tersus.reloadApplication,50);};
			cache.oncached=function(){if(tlog) tlog('OnCached');applicationCache.onnoupdate=null;tersus.load();};
			cache.onnoupdate=function(){if(tlog) tlog('OnUpdate');applicationCache.onnoupdate=null;tersus.load();};
			cache.onerror=function(){if(tlog) tlog('OnError');applicationCache.onerror=null;tersus.load();};
			return;	
		}
	}
	if (tlog) tlog('load()');
	tersus.init();
	if (tlog) tlog('init() finished');
	tersus.progress.set('Loading application ...');
	if (tlog) 
		tlog ('tersus.lastTimestamp:'+tersus.lastTimestamp)
	if (!tersus.lastTimestamp)
		tersus.reloadView();
	else if (window.localStorage && tersus.useLocalStorage && navigator.onLine == false && !tersus.isLocalHost(document.domain))
	{
		tersus.reloadView();
	}
	else
	{
		tersus.getTimestamp(tersus.handleLoadTimestampResponse);
	}
	if (tlog) tlog('load() returns');
	
};
tersus.handleLoadTimestampResponse = function()
{
	var req = tersus.getTimestampReq;
	if (tlog) tlog('Ready state:'+req.readyState);
	if (req.readyState != 4)
		return;
	if (tlog) tlog('Status:'+req.status);
		
	if (req.status == 200 || req.responseText)
	{
		var r = tersus.parseTimestampResponse(req);
		var currentTimestamp = r.timestamp;
		window.userName = r.userName;
		tersus.setLastTimestamp(currentTimestamp);
		tersus.reloadView();
	}
	else if(req.status == 0 && window.localStorage && tersus.useLocalStorage)
		tersus.reloadView();
	else if (req.status == 0 && navigator.onLine == false)
		tersus.reloadView();
};
tersus.reloadView = function()
{
	tersus.setPerspectiveAndView();
	tersus.loadView(document.getElementById('tersus.content'),view);
};
tersus.loader = {};
tersus.loadView = function loadView(targetElement, view)
{
	tersus.progress.set('Loading view - '+view.name);
	tersus.async.exec(function(){ tersus._loadView(targetElement, view);},false);
}
tersus._loadView = function loadView(targetElement, view)
{
	tersus.progress.set('Loading view - '+view.name);
	tersus.loader.currentView = view;
	tersus.loader.targetElement = targetElement;
	if (window.rootDisplayNode)
	{
		window.rootDisplayNode.destroyHierarchy();
		window.rootDisplayNode = null;
	    $(targetElement).empty(); // destroyHierarchy doesn't delete DOM Nodes
	}
	if (window.initPopupCalendar && ! tersus.popupCalendarInitialized)
	{
		tersus.popupCalendarInitialized = true;
		initPopupCalendar();
	}
	tersus.viewPath = view.path;
	if (tersus.repository.checkLoaded(view.id))
		tersus.loader.load();
	else
		if (tersus.preloadAll)
			tersus.repository.loadPackages([""],tersus.loader.load);
		else
			tersus.repository.loadPackages([tersus.getPackageId(view.id)],tersus.loader.load);
	tersus.alertMode = {interactive:true};
}
tersus.loader.load = function load()
{
	var view = tersus.loader.currentView;
	var targetElement = tersus.loader.targetElement;
	var root = window.rootDisplayNode;
	try
	{
		targetElement.innerHTML = '';
		if (!tersus.repository.isLoaded(view.id))
		{
			targetElement.innerHTML = 'View '+view.id+' not found';
			tersus.progress.clear();
			return;
		}
		if (! root)
		{
			var constructor = tersus.repository.get(view.id)
			window.currentRootDisplayNode = window.rootDisplayNode = root =  new constructor();
			root.mainWindow = window;
			root.currentWindow = window;
		}
		if (root.preload(tersus.loader.load))
		{
			root.create();
			root.doStart();
			if (window.trace)
				window.trace.flush();
			
			var topNode = root.wrapperNode ? root.wrapperNode : root.viewNode;	
			if (!topNode.parentNode || !topNode.parentNode.tagName)
				targetElement.appendChild(topNode);
			tersus.progress.clear();
			if (!is_touch_device())
				window.setTimeout(function(){window.rootDisplayNode.focusOnFirstField();},0);
			window.setTimeout(function(){$(window).trigger('ter-process-end');},10);
			
		}
	}
	catch (e)
	{
			targetElement.innerHTML = escapeHTML(e.message);
	}
}
tersus.switchView = function(perspective,view,parameters)
{
	if (!perspective && window.perspectives.length > 1)
		perspective = window.perspective.name;
	if (perspective)
		var url = tersus.baseURL+'#Perspective='+encodeURIComponent(perspective)+'&View='+encodeURIComponent(view);
	else
		var url = tersus.baseURL+'#View='+encodeURIComponent(view);
	if (parameters)
		url+='&'+parameters;
	tersus.gotoURL(url, false, true);

};

function openTools()
{
	var w = 250;
	var h = 220;
	var leftPx = window.screenX+window.innerWidth;
	if (leftPx  > screen.availWidth -w -10)
		leftPx = screen.availWidth -w -10;
	if (leftPx<0) leftPx=0;
		
	var topPx = window.screenY;
	var popup = window.open("tools.html","Tools","top="+topPx+",left="+leftPx+"toolbar=no,location=no,status=no,menubar=no,width=" + w + ",height=" + h); 
	popup.focus();
};
function showContent()
{
	var w = window.open("","","");
	w.document.write('<textarea cols="80" rows="50">'+document.getElementById("main.content").innerHTML+"</textarea>");
	setTimeout(function(){w.focus();},0);
};


function removeSuffix(str, separator)
{
	var index = str.indexOf(separator);
	if (index >=0)
		return str.substring(0,index);
	else
		return str;
};
	
// Extract rootURL and baseURL
var fullURL = window.location.href;
tersus.baseURL = removeSuffix(removeSuffix(fullURL,'#'),'?');
tersus.rootURL = tersus.baseURL.substring(0, tersus.baseURL.length-tersus.urlPathSuffix.length+1);
tersus.firstURL = fullURL;


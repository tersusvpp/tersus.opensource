/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 
 tersus.defaultServiceTimeout = 0;
 tersus.serviceTimeoutOverhead = 1;
 
//Service Node

function ServiceNode() {};
ServiceNode.prototype = new FlowNode();
ServiceNode.prototype.constructor = ServiceNode;
ServiceNode.prototype.type = 'Service';
ServiceNode.prototype.secure = false; // true means service must be invoked under HTTPS
ServiceNode.prototype.excludedTriggers= new tersus.Set(['<URL>']);
ServiceNode.prototype.start = function startService()
{	
	if (this.secure && !tersus.rootURL.match(/^https:/))
	{
		tersus.error('The service "'+this.modelName+'" must  be used in a secure context');
		return;
	} 
	this.pause();
	this.retries = 0;
	this.callService();
};
ServiceNode.prototype.started = function serviceStarted()
{
	this.setStatus(FlowStatus.STARTED);
	if (window.trace)
	{
		window.trace.invoked(this);
		window.trace.flush();
	}
};
ServiceNode.prototype.isService = true;
/*
	Invokes a service that contains file arguments.
	This is a special case for the following reasons:
	(1) We must use the orginial "File Input Node" to send the file 
	(2) For this reason, we use the "Form" node that is already part of the document
	(3) We need to use "form/multipart" encoding
	
	
	In this case, we can't create a separate form, 
	because there is no way to copy the FileInput node's content to another
	file input node (it is possible in Mozilla, but not in IE)
	
	Using XMLHTTPRequest is out of the question because JavaScript can't read
	local files.
*/

ServiceNode.prototype.callService = function  callService()
{
	this.explicitURL = this.getLeafChild('<URL>');
	var baseURL = tersus.serverURL ? tersus.serverURL+'/':tersus.rootURL;
	this.url = this.explicitURL ? this.explicitURL : baseURL+'Service';
	
	var host = document.domain;
 	var urlHost = this.url.match(/\/\/([^:/]+)[:/]/)[1];
 	if (urlHost != null)
 		host=urlHost;
    if (host == '')
    	modelError('Missing <URL> for service invocation, or attempt to access server-side database in native application',this);
	if (navigator.onLine == false)
	{
		var host = document.domain;
		if(! tersus.isLocalHost(host))
		{
			this.serviceError = "Offline - can't access server";
			this.handleResponse({});
			return;
		}
	}
	if (tlog)
		tlog("Calling service:"+this.modelName);
	if ( !tersus.useXMLHTTPRequest) // Legacy workaround for HTTP issues
        this.callIFrameService();
	else if (this.findFileTrigger())
	{
	    if (window.FileList)
	    {
	        if (!this.callMultiPartService())
		        this.callIFrameService(); // Workaround for problems with pop-up window on IE10/IE11	
	    }	
	    else
	        this.callIFrameService(); //Legacy browsers - no File API
	}
	else
		this.callRegularService();
};
ServiceNode.prototype.cancellable = true;
ServiceNode.prototype.getCancellationFunc = function getCancellationFunc()
{
	if (this.cancellable == 'false' || !this.cancellable)
		return null;
	var serviceNode = this;
	var cancellation = function () { 
			tersus.progress.set('Request cancelled.');
			serviceNode.cancel();
			};
	return cancellation;
};
ServiceNode.prototype.cancel = function cancel()
{
	if (this.responseReceived)
		return;
	this.abort();
	if (this.responseReceived)
		return;
	this.handleResponse({});
	this.cancelled = true;
};
ServiceNode.prototype.abort = function abort()
{
	var req = createXMLHTTPRequest();
	var serviceNode = this;
	var url = tersus.rootURL+'Abort';
	req.open('POST', url, true);
	req.setRequestHeader('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
	req.send('_userRequestId='+window.encodeURIComponent(this.getNodeId()));
};
ServiceNode.prototype.setProgressMessage = function()
{
	if (tersus.progress.active)
	{
		var message = null;
		if (this.progressMessage && this.progressMessage != ' ')
			message = this.progressMessage;
		else if (tersus.progressMessage)
			message = tersus.progressMessage;
		else
			message = 'Service: '+this.modelName;
		tersus.progress.set(message, this.getCancellationFunc());
	}
};
ServiceNode.prototype.callRegularService = function getService()
{
	var paramString = this.getParamString();
	this.nodesToRemove = [];
	
	// We wrap the call to XMLHTTPRequest in setTimeout to avoid a firefox bug:
	// see http://www.highdots.com/forums/javascript/xmlhttprequest-firefox-problem-577724.html
	// However, we suppress this behavior if we're inside 'onUnload' (or the service will never be invoked)
	var serviceNode = this;
	var f = function() {
		serviceNode.setProgressMessage();
		serviceNode.callXMLHTTPService(paramString);
	};
	if (tersus.isUnloading)
		f();
	else
		tersus.async.exec(f, false/* don't wait */);
};


ServiceNode.prototype.callXMLHTTPService = function callXMLHTTPService(paramString)
{
	var req = createXMLHTTPRequest();
	var nodeId = this.getNodeId();
	var modelName = this.modelName;
	var url = this.url;
	if (! tersus.isUnloading) // If we're unloading we won't be able to process the response
	{
		req.onreadystatechange= function(){
			ServiceNode.handleXMLHTTPResponse(req, nodeId, url, modelName);
		};
	}

	req.open('POST', this.url, true);
	req.timeout = this.getTimeoutMilliseconds();
	req.setRequestHeader('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
	req.send(paramString);
};
ServiceNode.prototype.callMultiPartService = function getService()
{
    var data = this.prepareFormData();
    if (!data)
    	return false; /* can't use FormData (popup file in IE)*/
    this.nodesToRemove = [];
    
    // We wrap the call to XMLHTTPRequest in setTimeout to avoid a firefox bug:
    // see http://www.highdots.com/forums/javascript/xmlhttprequest-firefox-problem-577724.html
    // However, we suppress this behavior if we're inside 'onUnload' (or the service will never be invoked)
    var serviceNode = this;
    var f = function() {
        serviceNode.setProgressMessage();
        serviceNode.callXHR2Service(data);
    };
    if (tersus.isUnloading)
        f();
    else
        tersus.async.exec(f, false/* don't wait */);
    return true;
};
ServiceNode.prototype.prepareFormData = function()
{
    var location = new tersus.RuntimeLocation(this);
    var serializer = new Serializer();
    var d = new FormData();
    if (!this.explicitURL)
    {
        d.append("_baseModelId",location.baseModelId);
        d.append("_path",this.getServicePath(location));
	d.append("_id", this.modelId);
        d.append("_userRequestId", ''+this.getNodeId());
    }
    else
    {
        d.append('_output','json');
    }
    if (window.trace)
    {
        d.append('_traceFileName',this.getTraceFileName());
    }   
    if (this.triggers)
    {
        for (var i=0; i< this.triggers.length; i++)
        {
            var trigger = this.triggers[i];
            var paramName = trigger.role;
            if (trigger.isRepetitive)
            {
                var values = trigger.getChildren(this);
                if (values.length > 0)
                {
                    d.append(trigger.role, serializer.serialize(values)); 
                }
            }
            else
            {
                var value = trigger.getChild(this);
                if (value != null)
                {
                    if (value.isFile && value.file)
                    {
                    	if (value.window && value.window !== window)
                    		return null; // Using a file from a file input field in a popup not supported in IE10/IE11.
                    // Note - it is not clear what happens when files are dropped on a pop-up, but the fall-back of IFRAME loading won't work 
                        d.append(trigger.role, value.file);
                    }
                    else if (value.isLeaf)
                        d.append(trigger.role, value.serialize ? value.serialize() : value.leafValue);
                    else
                        d.append(trigger.role, serializer.serialize(value));
                }
            }
        }
    }
    if (!this.explicitURL)
    {
        d.append('_sig','END');
    }
    return d;
};
ServiceNode.prototype.getTimeoutMilliseconds = function()
{
    var timeout = tersus.defaultServiceTimeout*1000;
    if (this.serviceTimeout)
    {
		timeout = this.serviceTimeout*1000 + tersus.serviceTimeoutOverhead*1000;
    }
    return timeout;
    
};

ServiceNode.prototype.callXHR2Service = function callXHR2Service(data)
{
    var req = createXMLHTTPRequest();
    var nodeId = this.getNodeId();
    var modelName = this.modelName;
    var url = this.url;
    if (! tersus.isUnloading) // If we're unloading we won't be able to process the response
    {
        req.onreadystatechange= function(){
            ServiceNode.handleXMLHTTPResponse(req, nodeId, url, modelName);
        };
    }
    req.open('POST', this.url, true);
    req.timeout = this.getTimeoutMilliseconds();
    req.send(data);    
};

ServiceNode.dummy = function () {};
ServiceNode.handleXMLHTTPResponse = function(req, nodeId, url, modelName)
{
	if (req.readyState != 4)
		return;
	req.onreadystatechange = ServiceNode.dummy;
	var status  = 'Error';
	var statusText = 'Unknown';
	try
	{
		status = req.status;
		statusText = req.statusText;
	}
	catch (e)
	{
	}
	var serviceNode = tersus.findNode(nodeId);
	if (!serviceNode)
		return;
	if (serviceNode.cancelled)
		return;
	delete FlowNode.map[nodeId]; 
	if (status == 400 && serviceNode.retries < tersus.SERVICE_MAX_RETRIES)
	{
		serviceNode.retries++;
		setTimeout(function(){serviceNode.callRegularService();}, tersus.SERVICE_RETRY_DELAY);
		return;
	}
	if (status != 200)
	{
		var message;
		if (status == 400)
		{
			message = 'The server has reported "BAD REQUEST".  This is possibly a result of a temporary network problem.';
			if (statusText != 'OK') // Safari bug - reports 'OK' instead of server message
				message += '\n\nOriginal server message: '+statusText;
		}
		else
			message = 'Failed to execute service (url='+url+', modelName='+modelName +',status='+status+',message='+statusText+')';
		serviceNode.serviceError = message;
		setTimeout(function() {serviceNode.handleResponse({});},0);
		return;
	}

	if (tersus.progress.active)
		tersus.progress.set('Processing service response ...');
	var responseText = req.responseText;
	
	/* If there is a sizeable response, we expect processing to take significant time
	   (during which there is no opportunity for the progress bar to appear, so we make
	   force progress bar to be visible  */
//	if (responseText.length > tersus.progress.responseLengthThreshold)
//		tersus.progress.show();
	setTimeout( function()
		{
			if (tlog) tlog('Parsing response ('+responseText.length+' characters)');
			var exitValues = eval("var output = "+responseText+";output;");
			if (tlog) tlog('Done parsing response');
			if (exitValues._ERROR)
			{
				serviceNode.serviceError=exitValues._ERROR;
				serviceNode.handleResponse({});
			}
			else
			{
				if (exitValues['<Done>'] == null)
					exitValues['<Done>']={};
				serviceNode.handleResponse(exitValues);
			}
		},0);
};

tersus.serviceRedirectionMap = {};
ServiceNode.prototype.getServicePath = function(location)
{
    //Typically returns the path (based on location), but may redirect for backward compatibility
    return tersus.serviceRedirectionMap[this.modelId] || location.path.str;
};	
ServiceNode.prototype.getParamString = function()
{
	var location = new tersus.RuntimeLocation(this);
	var serializer = new Serializer();
	if (!this.explicitURL)
	{
		var paramString = "_baseModelId="+encodeURIComponent(location.baseModelId);
		paramString += "&_path="+encodeURIComponent(this.getServicePath(location));
		paramString += "&_id="+encodeURIComponent(this.modelId);
		paramString+='&_userRequestId='+this.getNodeId();
	}
	else
	{
		var paramString = '_output=json';
	}
	if (window.trace)
	{
		paramString+='&_traceFileName='+encodeURIComponent(this.getTraceFileName());
	}	
	if (this.triggers)
	{
		for (var i=0; i< this.triggers.length; i++)
		{
			var trigger = this.triggers[i];
			var paramName = trigger.role;
			var paramValue = null;
			if (trigger.isRepetitive)
			{
				var values = trigger.getChildren(this);
				if (values.length > 0)
				{
					paramValue = serializer.serialize(values); 
				}
			}
			else
			{
				var value = trigger.getChild(this);
				if (value != null)
				{
					if (value.isLeaf)
						paramValue = value.serialize ? value.serialize() : value.leafValue;
					else
						paramValue = serializer.serialize(value);
				}
			}
			if (paramValue != null)
			{	
				if (paramString.length > 0)
					paramString+= '&';
				paramString += encodeURIComponent(paramName);
				paramString += '=';
				paramString += encodeURIComponent(paramValue);
			}
		}
	}
	if (!this.explicitURL)
	{
		paramString += '&_sig=END';
	}
	if (tlog)
		tlog('Done creating paramString');
	return paramString;
};
ServiceNode.prototype.callIFrameService = function callIFrameService()
{
	var form = this.prepareForm();
	if ( ! form)
	{
		this.continueExecution();
		return;
	};
	this.addTersusParameters(form);
	
	var iframe = this.getResponseFrame();
	
	form.action = this.url;
	form.method = 'POST';
	form.target  = iframe.name;
	this.callbackName = iframe.name+"_cb";
	var serviceNode = this;
	this.mainWindow[this.callbackName] = function callback(exitValues)
	{
		serviceNode.mainWindow[serviceNode.callbackName] = null;
		if (serviceNode.cancelled)
			return;
		tersus.progress.set('Processing service response ...');
		setTimeout( function()
		{
			if (exitValues._ERROR)
			{
				serviceNode.serviceError=exitValues._ERROR;
				serviceNode.handleResponse({});
			}
			else
				serviceNode.handleResponse(exitValues);
		},0);
	};
	var callbackField = addHiddenField(form, '_js_callback', 'parent.'+this.callbackName);
	form.appendChild(callbackField);
	this.nodesToRemove.push(callbackField);
	if (tersus.IFRAME_SERVICE_TIMEOUT)
	{
		setTimeout(function (){
			if (!serviceNode.responseReceived)
			{
				tersus.error('Server Error: Timeout');
				serviceNode.cancel();
			}
		}
		,tersus.IFRAME_SERVICE_TIMEOUT);
	};
	var sigField = addHiddenField(form, '_sig', 'END');
	form.appendChild(sigField);
	this.nodesToRemove.push(sigField);
	try
	{
		var nodeId = this.getNodeId();
		if (!tersus.pendingServiceCalls)
			tersus.pendingServiceCalls = new tersus.Set([nodeId]);
		else
			tersus.pendingServiceCalls.add(nodeId);
		form.submit();
		this.setProgressMessage();	
	}
	catch (e)
	{
		var msg;
		var fileTrigger = this.findFileTrigger();
		var fileName = this.getChild(fileTrigger.role).getFileName();
		if (e.name=='TypeError') // This happens in IE when a user types an invalid file name
			msg = 'Please check the file name "'+fileName + '"';
		else
			msg = 'An error occurred when invoke the service ('+e.message+').';
		tersus.error(msg);
		this.continueExecution();
	}
	for (var i=0; i< this.nodesToRemove.length; i++)
	{
		var node = this.nodesToRemove[i];
		node.parentNode.removeChild(node);
	}
};
ServiceNode.prototype.prepareForm = function prepareForm()
{
	var serializer = new Serializer();
	var form;
	var fileTrigger = this.findFileTrigger();
	this.nodesToRemove = [];
	
	if (fileTrigger)
	{
		var fileInputDisplayNode = this.getChild(fileTrigger.role).inputFieldDisplayNode;
		if ( fileInputDisplayNode.currentWindow.closed)
		{
			tersus.error('Tersus Application Error: Reference to a file input field in closed popup window.\n');
			return null;
		}
		
		form = fileInputDisplayNode.viewNode;
		setEncType(form,'multipart/form-data'); 
		var fileInputNode = fileInputDisplayNode.fileNode;
		fileInputNode.name = fileTrigger.role;
	}
	else
	{
		form = this.mainWindow.document.createElement('form');
		if (! window.showServiceInvocations)
		{
			this.nodesToRemove.push(form);
		}
		this.mainWindow.document.body.appendChild(form);
	}

	if (this.triggers)
	{
		for (var i=0; i< this.triggers.length; i++)
		{
			var trigger = this.triggers[i];
			if (this.excludedTriggers.contains(trigger.role))
				continue;
			if (trigger != fileTrigger) // If fileTrigger is non-null, it is already part of the form
			{
				if (trigger.isRepetitive)
				{
					var values = trigger.getChildren(this);
					if (values.length > 0)
					{
						var serialization = serializer.serialize(values); 
						var inputField = form.ownerDocument.createElement('input');
						inputField.type = 'hidden';
						inputField.name = trigger.role;
						inputField.value = serialization;
						form.appendChild(inputField);
						if (fileTrigger)
							this.nodesToRemove.push(inputField);
					}
				}
				else
				{
					var value = trigger.getChild(this);
					if (value != null)
					{
						var serialization;
						if (value.isLeaf)
							serialization = value.serialize ? value.serialize() : value.leafValue;
						else
							serialization = serializer.serialize(value);
						var inputField = form.ownerDocument.createElement('input');
						inputField.type = 'hidden';
						inputField.name = trigger.role;
						inputField.value = serialization;
						form.appendChild(inputField);
						if (fileTrigger)
							this.nodesToRemove.push(inputField);
					}
				}	
			}
		}
	}	
	return form;
};

ServiceNode.prototype.addTersusParameters = function(form)
{
	var location = new tersus.RuntimeLocation(this);
	if (this.explicitURL)
	{
		var outputField = addHiddenField(form, '_output', 'json');
		form.appendChild(outputField);
		this.nodesToRemove.push(outputField);
	
	}
	else
	{
		var baseModelIdField = addHiddenField(form, '_baseModelId', location.baseModelId);
		form.appendChild(baseModelIdField);
		this.nodesToRemove.push(baseModelIdField);
		var pathField = addHiddenField(form, '_path', this.getServicePath(location));
		form.appendChild(pathField);
		var idField = addHiddenField(form, '_id', this.modelId);
		form.appendChild(idField);
		this.nodesToRemove.push(pathField);
	}
	if (window.trace)
	{
		window.trace.flush();
		var traceFileField = addHiddenField(form, '_traceFileName', this.getTraceFileName());
		form.appendChild(traceFileField);
		var requestIdField = addHiddenField(form, '_userRequestId', this.getNodeId());
		form.appendChild(traceFileField);
		this.nodesToRemove.push(traceFileField);
		this.nodesToRemove.push(requestIdField);
	}
};
ServiceNode.prototype.getTraceFileName = function()
{
	return window.trace.filename;
};

ServiceNode.prototype.handleResponse = function handleResponse(response)
{
	if (tlog) tlog('Processing response for service: '+	this.modelName);
	if (tersus.pendingServiceCalls)
		tersus.pendingServiceCalls.remove(this.getNodeId());
	var start = new Date();
	this.responseReceived = true;
	if (window.trace)
		window.trace.responded(this);
		
	for (var exitRole in response)
	{
        var exit = this.getElement(exitRole);
        if (!exit)
        	continue; // It's possible that the exit is missing because of permission issues
        if (exit.isRepetitive)
        {
        	var values = response[exitRole];
        	for (var i=0; i< values.length; i++)
        	{
        		var dataValue  = exit.createChild(this);
        	    dataValue.copyObject(values[i]);
        	}
        }
        else
        {
        	var value = response[exitRole];
        	var dataValue  = exit.createChild(this);
       	    dataValue.copyObject(value);
        }
        			
	};
	if (this.status != FlowStatus.PAUSED)
		tersus.error('Unexpected status "'+this.status+'" for ' + this.getPath());
	this.continueExecution();
	var end = new Date();
	window.duration = "Response processed in " + (end - start) + " ms";
	
};

ServiceNode.prototype.resume = function resume()
{
	if (this.serviceError)
		throw this.serviceError;
};
/* Creates an iframe for invoking the service */

ServiceNode.prototype.getResponseFrame = function getResponseFrame()
{
	var iframe = tersus.nextServiceResponseFrame();
	this.iframeId =iframe.id;
	return iframe;
};
/*
	returns the unique trigger that holds a 'File Input' value
	If there is no such trigger, returns null.
	If there is more than one trigger, an error is reported.
*/
ServiceNode.prototype.findFileTrigger = function findFileTrigger()
{
	var fileTrigger = null;
	if (this.triggers)
	{
		for (var i=0; i<this.triggers.length; i++)
		{
			//TODO consider using the modelId of the trigger to identify the file
			var trigger = this.triggers[i];
			if (trigger.isRepetitive)
				continue;
			var child = this.getChild(trigger.role); 
			if (child && child.isFile)
			{
				if (fileTrigger)
				{
					modelError("Too many file input values");
					return null;
				}
				else
					fileTrigger = trigger;
			}
		}
	}
	return fileTrigger;
};

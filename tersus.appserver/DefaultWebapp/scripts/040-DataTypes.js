/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 DATE_FORMAT='yyyy-MM-dd';
if (! tersus) tersus = {};
tersus.DECIMAL_POINT='.';
tersus.THOUSANDS_SEPARATOR=',';
if (! tersus.Messages) tersus.Messages = {};
tersus.Messages.INVALID_NUMBER = 'Please type a valid number';
tersus.Messages.INVALID_DATE = 'Please type a valid date in format '+DATE_FORMAT;

tersus.Nothing = function tersus_Nothing() {};
tersus.Nothing.prototype = new DataValue();
tersus.Nothing.prototype.constructor = tersus.Nothing;
tersus.Nothing.prototype.pluginFunc = tersus.Nothing;
tersus.Nothing.prototype.isLeaf = true;
tersus.Nothing.prototype.isNothing = true;
tersus.Nothing.prototype.toString = function toString()
{
	return '<Nothing>';
}
tersus.Nothing.prototype.serialize = function() 
{
	return 'Nothing';
};
tersus.Nothing.prototype.isInstance = function(value)
{
	return value != null && value.isNothing;
};

tersus.Text = function tersus_Text() {};
tersus.Text.prototype = new DataValue();
tersus.Text.prototype.constructor = tersus.Text;
tersus.Text.prototype.isLeaf = true;
tersus.Text.prototype.pluginFunc = tersus.Text;
tersus.Text.prototype.isText = true;
tersus.Text.prototype.sqlType = 'TEXT';
tersus.Text.prototype.parseInput = function tersus_Text_parseInput(str)
{
	if (str == '')
		return null;
	
	var constructor = this.constructor;
	var text = new constructor();
	text.leafValue = str.replace (/\r/g,'');
	return text;
};
tersus.Text.prototype.isInstance = function(value)
{
	return value != null && value.isText;
};
tersus.Text.prototype.formatString = function tersus_Text_formatString(properties)
{
	if (this.leafValue == null)
		return '';
	var s = this.leafValue.toString();
	return s;
};
tersus.Text.prototype.formatInput = function tersus_Text_formatInput(properties)
{
	if (this.leafValue == null)
		return '';
	var s = this.leafValue.toString();
	return s;
};
tersus.Text.prototype.setLeaf = function(value)
{
	if (value == null)
		this.leafValue == null;
	else this.leafValue = ''+value;
};
tersus.Text.prototype.parseString = tersus.Text.prototype.setLeaf;
tersus.Text.prototype.toString=tersus.Text.prototype.serialize= function(){return this.leafValue;}
tersus.SecretText = tersus.Text.__subclass('tersus.SecretText');
tersus.SecretText.prototype.isSecret=true;

tersus.notEmpty = function (value)
{
	return value !== null && value !== undefined && value !== ' ';
};
tersus.isNumber = function tersus_isNumber(value)
{
	if (value == null)
		return false;
	var str = value.toString();
	var re = /^\s*[-+]?(\d+(\.\d*)?|(\.\d+))([eE][-+]?\d+)?\s*$/;
	return str.match(re);
};
tersus.isNumberWithSeparators = function tersus_isNumber(value)
{
	if (value == null)
		return false;
	var str = value.toString();
	var reStr = '^[-]?((\\d{1,3}(['+tersus.THOUSANDS_SEPARATOR+']\\d{3})*)|\\d*)(['+tersus.DECIMAL_POINT+']\\d*)?$';
	var re = new RegExp(reStr);
	return str.match(re);
};

tersus.Number = function tersus_Number() {};
tersus.Number.prototype = new DataValue();
tersus.Number.prototype.constructor = tersus.Number;
tersus.Number.prototype.isLeaf = true;
tersus.Number.prototype.sqlType = 'NUMERIC';
tersus.Number.prototype.pluginFunc = tersus.Number;
tersus.Number.prototype.isNumber = true;
tersus.Number.prototype.isInstance = function(value)
{
	return value != null && value.isNumber;
};
tersus.Number.prototype.parseInput = function(s, props) 
{
	if (s == null || s == '')
		return null;
	if (props.currencySymbol)
		s = s.replace(props.currencySymbol, '');
	if (s == '')
		return null;
	var s1 = null;
	if (tersus.permissiveNumberInput)
	{
		var s0 = s.replace(/[,]/,'.');
		if (tersus.isNumber(s0))
			s1 = s0;
	}
	
	if (s1 == null) // Not permissive or permissive format doesn't match
	{
		s1 = s.replace(new RegExp('['+tersus.THOUSANDS_SEPARATOR+']','g'),'');
		s1 = s1.replace(new RegExp('['+tersus.DECIMAL_POINT+']','g'),'.');
	}
	var x = parseFloat(s1);
	if (isNaN(x))
		return null;
	var number = new (this.constructor)();
	number.leafValue = x;
	return number;
};

tersus.Number.prototype.serialize = tersus.Number.prototype.toString = function()
{
	if (this.leafValue === null || this.leafValue === undefined)
		return null;
	else
		return this.leafValue.toString();
}

tersus.Number.prototype.validateInput = function (s, props)
{
	if ( s == null || s == '')
		return null;
	if (props.currencySymbol)
		s = s.replace(props.currencySymbol,'');
	if (tersus.permissiveNumberInput)
	{
		var s0 = s.replace(/[,]/,'.');
		if (tersus.isNumber(s0))
			return null;
	}
	
	if (tersus.isNumberWithSeparators(s))
		return null;
	else
		return tersus.Messages.INVALID_NUMBER;
};
tersus.Number.prototype.formatInput = function tersus_Number_formatInput(props)
{
	return this.formatString(props, true);
};
tersus.Number.prototype.formatString = function tersus_Number_formatString(props, noRounding)
{
    if (props.format)
        return gwtFormatNumber(this.leafValue,props.format);
    // TODO move formatter to legacy script (and use default format when props.format is missing)
//	return ''+this.leafValue;
	var formatter = new NumberFormat();
	if (props)
	{
		if (!noRounding && tersus.isNumber(props.decimalPlaces))
			formatter.setPlaces(props.decimalPlaces);
		var useSeparators =  (true == props.useThousandsSeparator);
		formatter.setSeparators(useSeparators, tersus.THOUSANDS_SEPARATOR, tersus.DECIMAL_POINT);
		if (props.currencySymbol && props.currencySymbol != ' ')
		{
			formatter.setCurrencyValue(props.currencySymbol);
			formatter.setCurrency(true);
		}
		if (props.currencyPosition && NumberFormat.prototype[props.currencyPosition])
			formatter.setCurrencyPosition(NumberFormat.prototype[props.currencyPosition]);
	}
	formatter.setNumber(this.leafValue);
	if (props && props.showPercentage)
		return formatter.toPercentage();
	else
		return formatter.toFormatted();
	
	
};
tersus.Number.prototype.parseString = function tersus_Number_parseString (str, element)
{
	var format;
	if (element) format = element.format;
	if (!format) format = this.format;
	if (format)
	{
		try
		{
		 	this.leafValue = gwtParseNumber(str, format);
		}
		catch (e)
		{
			throw {message:'Invalid number format. Format="'+format+'" text="'+str+'"'};
		}
		
	}
	else
	{
		if (! tersus.isNumber(str))
			throw {message:'Invalid number format: text="'+str+'"'}; 
		this.leafValue = parseFloat(str);
	}
};
tersus.Boolean = function tersus_Boolean() {};
tersus.Boolean.prototype = new DataValue();
tersus.Boolean.prototype.constructor = tersus.Boolean;
tersus.Boolean.prototype.isLeaf = true;
tersus.Boolean.prototype.pluginFunc = tersus.Boolean;
tersus.Boolean.prototype.YES_STR = 'Yes';
tersus.Boolean.prototype.TRUE_STR = 'true';
tersus.Boolean.prototype.NO_STR = 'No';
tersus.Boolean.prototype.leafValue = null;
tersus.Boolean.prototype.sqlType = 'CHAR(1)';
tersus.Boolean.prototype.setLeaf = function (v)
{
	if (v == null)
		this.leafValue = null;
	else if (v == true || v == this.YES_STR || v == this.TRUE_STR)
		this.leafValue = true;
	else
		this.leafValue = false;
};
tersus.Boolean.prototype.parseString = function tersus_Boolean_parseString(str, element)
{
	this.leafValue = (str == this.YES_STR || str == this.TRUE_STR);
};

tersus.Boolean.prototype.serialize = function ()
{
	if (this.leafValue == null)
		return 'null';
	else
		return this.leafValue? this.YES_STR: this.NO_STR;
		
};
tersus.Boolean.prototype.toString = tersus.Boolean.prototype.serialize;
tersus.Boolean.prototype.isBooleanValue = true;
tersus.Boolean.prototype.isInstance = function(value)
{
	return value != null && value.isBooleanValue;
};


tersus.DateValue = function tersus_DateValue(dateStr)
{
	if (dateStr)
	{
		var parts = dateStr.split('-');
		this.year = parseInt(parts[0],10);
		this.month = parseInt(parts[1],10);
		this.day = parseInt(parts[2],10);
	}
};
tersus.DateValue.prototype.formatString = function formatString(format)
{
    if (window.formatDate) //legacy
        return formatDate(this.year, this.month, this.day, DATE_FORMAT);
    else
    {
        format = format || DATE_FORMAT;
        return gwtFormatDateAndTime(this.toLocalDate(), format, -10000);
    }
}
tersus.DateValue.prototype.toString = function toString()
{
	var p2 = tersus.DateAndTime.pad2;
	return this.year+'-'+p2(this.month)+'-'+p2(this.day);
}
tersus.DateValue.prototype.isTersusDate = true;
tersus.DateValue.prototype.toLocalDate = function()
{
	var d= new Date(this.year, this.month-1,this.day);
	return d;
};

tersus.DateValue.prototype.equals = function (other)
{
	return other && other.isTersusDate && other.year == this.year && other.month == this.month && other.day == this.day;
};
function createDate(year, month, day)
{
	var date = new tersus.DateValue();
	date.year = year;
	date.month = month;
	date.day = day;
	return date;
};

tersus.Date = function tersus_Date() {};
tersus.Date.prototype = new DataValue();
tersus.Date.prototype.constructor = tersus.Date;
tersus.Date.prototype.isLeaf = true;
tersus.Date.prototype.sqlType = 'Date';
tersus.Date.prototype.pluginFunc = tersus.Date;
tersus.Date.prototype.isDate = true;
tersus.Date.prototype.isInstance = function(value)
{
	return value != null && value.isDate;
};
tersus.Date.prototype.setLeaf = function tersus_Date_setLeaf(value)
{
	if (value.isTersusDate)
		this.leafValue = value;
	else
		this.leafValue = new tersus.DateValue(value);
};

tersus.Date.prototype.format = 'yyyy-MM-dd';
tersus.Date.prototype.parseString = function tersus_Date_parseString(str, element)
{
	var format;
	if (element)
		format = element.format;
	if (!format)
		format = this.format;	
	this.parseDateString(str, format);
};
tersus.Date.prototype.parseDateString = function tersus_Date_parseString(str, format)
{
    var time =  gwtParseDateAndTime(str, format, -100000); // Using local time zone
    if (time == -1)
        throw {message:'Invalid date format. Format:"'+format+'" text="'+str+'"'};
    var d = new Date(time); // Using 
    var date = new tersus.DateValue();
    date.year=d.getFullYear();
    date.month=d.getMonth()+1;
    date.day=d.getDate();
    this.leafValue=date;    
};

tersus.Date.prototype.parseInput = function (str)
{
    if (str == null || str == '')
        return null;
    var constructor = this.constructor;
    var node = new constructor();
    var date = new tersus.DateValue();
    node.parseDateString(str, DATE_FORMAT);
    return node;
};


tersus.Date.prototype.getYear = function tersus_Date_getYear()
{
	if (this.leafValue)
		return this.leafValue.year;
	else
		return null;
};
tersus.Date.prototype.getMonth = function tersus_Date_getMonth()
{
	if (this.leafValue)
		return this.leafValue.month;
	else
		return null;
};
tersus.Date.prototype.getDay = function tersus_Date_getDay()
{
	if (this.leafValue)
		return this.leafValue.day;
	else
		return null;
};

tersus.Date.prototype.formatString = function(props)
{
	var dateFormat = null;
	if (props)
	{
		var dateFormat = props.format || props.dateFormat; /* props.dateFormat is un un-document legacy that has likely never used, but support is retained*/
	}	
	if (! dateFormat)
		dateFormat = DATE_FORMAT;
	var date = this.leafValue;
	var s;
	if (date == null)
		s =  '';
	else if (date.isTersusDate) 
	    s=date.formatString(dateFormat);
	else
		s = date.toString();
	return s;

};

Date.prototype.equals = function (other)
{
	return other.getTime && this.getTime() == other.getTime();
};

tersus.getDate = function(o)
{
	if (o && o.leafValue)
	{
		if (o.leafValue.isTersusDate)
			return o.leafValue.toLocalDate();
		else 
			return o.leafValue;
	}
		
};

tersus.DateAndTime = function tersus_DateAndTime() {};
tersus.DateAndTime.prototype = new DataValue();
tersus.DateAndTime.prototype.constructor = tersus.DateAndTime;
tersus.DateAndTime.prototype.isLeaf = true;
tersus.DateAndTime.prototype.pluginFunc = tersus.DateAndTime;
tersus.DateAndTime.prototype.isDateAndTime = true;
tersus.DateAndTime.prototype.sqlType = 'Timestamp';
tersus.DateAndTime.prototype.isInstance = function(value)
{
	return value != null && value.isDateAndTime;
};
tersus.DateAndTime.prototype.formatString = function(props)
{
    var date = this.leafValue;
    if (props.format)
        return gwtFormatDateAndTime(date, props.format, -10000);
	var	dateFormat = DATE_FORMAT;
	var s;
	if (date == null)
		s =  '';
	else
	{
	    //TODO clean this up.  The code in this file should not refer to formatDate (from popcalendar) - need some other way to allow for backward compatability
	    if (window.formatDate)
	        s = window.formatDate(date.getFullYear(), date.getMonth()+1, date.getDate(), dateFormat);
	    else
	        s = gwtFormatDateAndTime(date, dateFormat, -10000);
		var p2 = tersus.DateAndTime.pad2;
		s += ' - '+p2(date.getHours())+':'+p2(date.getMinutes())+':'+p2(date.getSeconds());
	}
	return s;
};
tersus.DateAndTime.prototype.re = new RegExp("([0-9]+)-([0-9]+)-([0-9]+) ([0-9]+):([0-9]+)(:([0-9]+)(:([0-9]+))?)?");
tersus.DateAndTime.pad2 = function(value)
{
	if (value < 10)
		return '0'+value;
	else
		return value;
};
tersus.DateAndTime.pad3 = function(value)
{
	if (value < 10)
		return '00' + value;
	else if (value < 100)
		return '0' + value;
	else
		return value;
};
tersus.DateAndTime.prototype.format='yyyy-MM-dd HH:mm:ss:SSS';
tersus.DateAndTime.prototype.formatAsGMT=false;
tersus.DateAndTime.prototype.W3CFORMATS = ["yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ","yyyy-MM-dd'T'HH:mm:ssZ","yyyy-MM-dd'T'HH:mmZ"];
tersus.DateAndTime.prototype.W3CFORMAT="yyyy-MM-dd'T'HH:mm:ss.SSSSSSSTZD";
tersus.DateAndTime.prototype.setLeaf = function tersus_Date_setLeaf(value)
{
	if (value.constructor==Date)
		this.leafValue =value;
	else
	{
		var d = this.re.exec(value);
		if (d == null)
			internalError("Failed to parse date/time string '"+value+"'");
		var date =  this.leafValue = new Date(0);
		date.setUTCFullYear(d[1]);
		date.setUTCMonth(d[2]-1);
		date.setUTCDate(d[3]);
		date.setUTCHours(d[4]);
		date.setUTCMinutes(d[5]);
		if (d[7])
			date.setUTCSeconds(d[7]);
		if (d[9])
		date.setUTCMilliseconds(d[9]);
	};	
};

tersus.parseDateTimeString = function(str, format, tzOffset)
{
	var time;
	if (format == tersus.DateAndTime.prototype.W3CFORMAT)
	{
		var s1=str.replace(/z/i,'+00:00');
		for (var i=0;i<tersus.DateAndTime.prototype.W3CFORMATS.length;i++)
		{
			time =  gwtParseDateAndTime(s1,tersus.DateAndTime.prototype.W3CFORMATS[i], 0);
			if (time != -1)
				break;
		}
	}
	else
	{
		time =  gwtParseDateAndTime(str,format,tzOffset);
	}
	if (time == -1)
			throw {message:'Invalid date/time format. Format:"'+format+'" text="'+str+'"'};
    return time;
};
tersus.DateAndTime.prototype.parseString = function tesus_DateAndTime_parseString(str, element)
{
	var format = this.format;
	var formatAsGMT = this.formatAsGMT;
	if (element)
	{
		if (element.format != null)
			format = element.format;
		if (element.formatAsGMT != null)
			formatAsGMT = element.formatAsGMT;
	}
	
        var time = tersus.parseDateTimeString(str, format,  formatAsGMT?0:-100000);
	this.leafValue = new Date(time);
};

tersus.DateAndTime.prototype.serialize = function(noquote)
{
	var d = this.leafValue;
	var p2 = tersus.DateAndTime.pad2;
	var p3 = tersus.DateAndTime.pad3;
	var q = noquote?'':'"';
	return d.getUTCFullYear()+"-"+p2(d.getUTCMonth()+1)+"-"+p2(d.getUTCDate())+" "+p2(d.getUTCHours())+":"+p2(d.getUTCMinutes())+":"+p2(d.getUTCSeconds())+":"+p3(d.getUTCMilliseconds());
};

File = function File(ownerField)
{
	this.inputFieldDisplayNode = ownerField;
};
File.prototype = new DataValue();
File.prototype.constructor = File;
File.prototype.pluginFunc = File;
// TODO: when IE8.0/IE9.0 support can be dropped, move inputFieldDisplayNode to legacy script
File.prototype.isFile = true;
File.prototype.getFileName = function getFileName()
{
    if (this.file)
        return this.file.name;
	if (! this.inputFieldDisplayNode)
	{
		if (this.children && this.children['File Name'])
			return this.children['File Name'];
		else
			return null;
	}
	if ( this.inputFieldDisplayNode.currentWindow.closed)
	{
		tersus.error('Tersus Application Error: Reference to a file input field in closed popup window.\n');
		return null;
	}
	if (! this.inputFieldDisplayNode.fileNode)
		return null;
	if (this.inputFieldDisplayNode.fileNode.value)
		return this.inputFieldDisplayNode.fileNode.value;
	else if (this.children)
		return this.children['File Name'];
	else
		return null;
	
};

File.prototype.addGetter('File Name',File.prototype.getFileName);
File.prototype.toString = function toString()
{
	return 'Input File '+ this.getFileName();
};

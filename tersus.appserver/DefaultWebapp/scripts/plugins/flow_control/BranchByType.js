/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.BranchByType = function BranchByType(){};
tersus.BranchByType.prototype = new ActionNode();
tersus.BranchByType.prototype.constructor = tersus.BranchByType;
tersus.BranchByType.prototype.start = function()
{
	var input = this.getChild('<Input>');
	var otherExit = null;
	if (input)
	{
		for (var i=0; i<this.exits.length; i++)
		{
			var exit = this.exits[i];
			if (exit.role == '<Other>')
				otherExit = exit;
			else
			{
				var c = exit.getChildConstructor();
				if (c && c.prototype.isInstance(input))
				{
					this.chargeExit(exit, input);
					return;
				}
			}
		}
		if (otherExit)
			this.chargeExit(otherExit, input);
	}
};
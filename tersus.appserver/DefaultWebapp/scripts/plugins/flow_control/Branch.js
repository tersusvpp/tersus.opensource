/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function Branch() {};
Branch.prototype = new ActionNode();
Branch.prototype.constructor = Branch;
Branch.prototype.start = function ()
{
	var dataTrigger = this.getElement('<Data>');
	var selectorTrigger = this.getElement('<Selector>');
	if (! selectorTrigger) // For backward compatability
		selectorTrigger = this.getElement('<Value>');
	var selectorValue = selectorTrigger.getChild(this);
	var dataValue;
	if (dataTrigger)
		dataValue = dataTrigger.getChild(this);
	else
		dataValue = selectorValue;
	var defaultExit = this.getElement('<Other>');
	for (var i=0; i<this.exits.length; i++)
	{
		var exit = this.exits[i];
		if (exit != defaultExit)
		{
			var valueStr = exit.valueStr;
			if (valueStr == undefined)
				valueStr = exit.role;
			valueStr = valueStr.replace(/\\t/g, '\t');
			valueStr = valueStr.replace(/\\n/g, '\n');
			valueStr = valueStr.replace(/\\r/g, '\r');
			valueStr = valueStr.replace(/\\\\/g, '\\');

			if (selectorValue.toString() == valueStr)
			{
				this.chargeExit(exit, dataValue);
				return;
			}
			
		}
	}
	if (defaultExit)
		this.chargeExit(defaultExit, dataValue);
};
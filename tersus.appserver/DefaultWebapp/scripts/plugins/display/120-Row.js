/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function Row() {};
Row.prototype = new DisplayNode();
Row.prototype.constructor = Row;
Row.prototype.defaultTag = 'tr';
Row.prototype.attachViewNode = function Row_createViewNode()
{
	this.setDisplayOrder();
	if (this.parent.viewNode.tagName != 'TABLE')
	{
		this.tableNode  = this.createHTMLElement('table');
		this.tbodyNode = this.createHTMLElement('tbody');
		this.tableNode.setAttribute('display_order',this.viewNode.getAttribute('display_order'));
		this.tableNode.appendChild(this.tbodyNode);
		this.tbodyNode.appendChild(this.viewNode);
		if(this.sharedProperties.width != undefined && this.sharedProperties.width != ' ')
			this.tableNode.style.width = this.sharedProperties.width;
		if (this.sharedProperties.styleClass != undefined && this.sharedProperties.styleClass != ' ')
			this.tableNode.className = this.sharedProperties.styleClass;
		else if (this.defaultStyleClass)
			this.tableNode.className = this.defaultStyleClass;
			
		var rowNode = this.viewNode;
		
		// We temporarily set the view node to the table node, because this is the node
		// we want to append to the parent
		
		// This is not very clean, and it would have probably have been more elegant
		// if we distinguished between 'contentNode' and 'viewNode', but this refactoring is currently 
		// out of scope, especially because we'll have to decide what to do with
		// things like style class etc. (which currently apply to the content node)
		// We have to be careful with backward compatability.
		
		// We DON'T simply append our table to the parent view node, because the parent node
		// may have special logic in appendChildViewNode - specifically - we want to retain the behavior of DisplayGroup.

		this.viewNode = this.tableNode;
		this.parent.appendChildViewNode(this);
		this.viewNode = rowNode;		
	}
	else
	{
		this.parent.appendChildViewNode(this);
	}
	if (this.parent.sharedProperties.zebraTable == true)
	{
		var v = this.viewNode;
		var parity = v.parentNode.childNodes.length & 1;
		var cn = v.className;
		cn = cn ? cn : '';
		this.extraClassName =  (parity ? ' odd':' even');
		v.className = cn + this.extraClassName; 
	}
}
Row.prototype.setEventHandlers = function Row_setEventHandlers()
{
	DisplayNode.prototype.setEventHandlers.call(this);

	if (this.isSelectable())	
		this.registerEventHandler(Events.ON_CLICK);	
};
Row.prototype.isSelectable = function()
{
	return this.sharedProperties.selectable==true || this.parent.sharedProperties.selectable==true; 
}
Row.prototype.select = function()
{
	if (this.isSelectable())
	{
		var table = this.parent;
		table.select(this);
	}
};
Row.prototype.onClick = function (domNode, callback)
{
	this.select();
	DisplayNode.prototype.onClick.call(this,domNode, callback);
};
Row.prototype.onChildFocus = function (child)
{
	this.select();	
};
Row.prototype.onDelete = function ()
{
	DisplayNode.prototype.onDelete.call(this);
	if (this.parent.selectedRow == this)
		this.parent.selectedRow = null;
};
Row.prototype.removeChildViewNode = function removeChildViewNode(child)
{
	var c = child.viewNode;
	var p = this.viewNode;
	if (c.parentNode == p) // c is a cell (TD or TH)
		p.removeChild(c);
	else if (p == c.parentNode.parentNode) // c is wrapped in a cell
		p.removeChild(c.parentNode);
};

Row.prototype.appendChildViewNode = function appendChildViewNode(child, childElement)
{
	if (!childElement)
		childElement = child.element;
	var cellNode;
	if (child.viewNode.tagName == 'TD' || child.viewNode.tagName == 'TH')
		cellNode = child.viewNode;
	else
	{
		var cellNode = this.createHTMLElement('td');
		cellNode.setAttribute('display_order',child.viewNode.getAttribute('display_order'));
		cellNode.appendChild(child.viewNode);
	}
	if (child.element && child.element.properties)
	{
		var elementProperties = child.element.properties;
		if (tersus.isNumber(elementProperties.colSpan))
			cellNode.colSpan = elementProperties.colSpan;
		if (tersus.isNumber(elementProperties.rowSpan))
			cellNode.rowSpan = elementProperties.rowSpan;
		if (tersus.notEmpty(elementProperties.cellWidth))
			cellNode.style.width=elementProperties.cellWidth;
		if (elementProperties.cellStyleClass && elementProperties.cellStyleClass != ' ')
			cellNode.className=elementProperties.cellStyleClass;
	}
	if (child.colSpan)
		cellNode.colSpan = child.colSpan;
	DisplayNode.appendChildNode(this.viewNode,cellNode);
	if (cellNode.parentNode.childNodes.length == 1)
	{
		var c = cellNode.className;
		c = c + ' first';
		cellNode.className=c;
		if (cellNode == child.viewNode)
		{
			child.baseClassName = cellNode.className;
			child.extraClassName = ' first';
		}
	}
	var p = this.parent;
	if (p.applyColOrder && p.headerRow && this.viewNode.childNodes.length == p.headerRow.childNodes.length)
	{
		p.applyColOrder(this.viewNode);
	}
	if (p.hiddenColumns != null && p.hiddenColumns[p.columnNames[childElement.role]] != null)
		cellNode.style.display='none';
};

Row.prototype.deleteMe = function()
{
	DisplayNode.prototype.deleteMe.call(this);
	if (this.parent && this.parent.selectedRow == this)
		this.parent.selectedRow = null;
};
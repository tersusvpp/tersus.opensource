/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.VSplitPane = function tersus_VSplitPane(){};
tersus.VSplitPane.prototype = new SplitPane();
tersus.VSplitPane.prototype.constructor = tersus.VSplitPane;
tersus.VSplitPane.prototype.bottomPaneHeight = 150;
tersus.VSplitPane.prototype.minHeight=20;
tersus.VSplitPane.prototype.contentPaneStyle = "vSplitPaneContent";
tersus.VSplitPane.prototype.dividerStyle = "vSplitPaneDivider";
tersus.VSplitPane.prototype.onResize = function ()
{
	if (this.sharedProperties.height)
		this.setDimensions();
	else
	{
		var d = this.viewNode.ownerDocument;
		var body = this.viewNode.ownerDocument.body;
		var height = getVisibleSize(body).height;
	
		var currentBottom = getOffsets(this.viewNode).top + this.viewNode.offsetHeight;
		if (window.browser != 'IE')
			currentBottom += 0;
		
		var delta = height-currentBottom;
		var currentHeight = this.bottomPaneHeight + this.topPaneHeight;
		if (delta+currentHeight >= 2*this.minHeight)
		{
			this.topPaneHeight = Math.max(this.minHeight, this.topPaneHeight+delta);
			this.bottomPaneHeight = currentHeight+delta-this.topPaneHeight;
			this.setPaneHeight(this.pane1,this.topPaneHeight);
			this.setPaneHeight(this.pane2,this.bottomPaneHeight);
		}
		var pane = this;
		var resizeChildren = function(){pane.onResizeChildren()};
		this.currentWindow.setTimeout(resizeChildren,10);
	}
};
tersus.VSplitPane.prototype.setDimensions = function()
{
	if (this.sharedProperties.height)
		this.height =parseInt(this.sharedProperties.height);
	
	this.divider.style.height = this.dividerThickness+ 'px';
	if (this.sharedProperties.bottomPaneHeight)
	{
		this.bottomPaneHeight=parseInt(this.sharedProperties.bottomPaneHeight);
	}
	this.topPaneHeight = this.height - this.bottomPaneHeight;
	this.setPaneHeight(this.pane1, this.topPaneHeight);
	this.setPaneHeight(this.pane2,this.bottomPaneHeight);
};
tersus.VSplitPane.prototype.resize = function(delta)
{
	if (this.topPaneHeight+delta.y >= this.minHeight && this.bottomPaneHeight-delta.y >= this.minHeight)
	{
		this.topPaneHeight += delta.y;
		this.bottomPaneHeight -= delta.y;
		this.setPaneHeight(this.pane1, this.topPaneHeight);
		this.setPaneHeight(this.pane2,this.bottomPaneHeight);
		this.onResizeChildren();
	}
};

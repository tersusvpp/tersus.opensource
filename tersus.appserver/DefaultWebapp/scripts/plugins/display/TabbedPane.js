/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function TabbedPane() {};
TabbedPane.prototype = new Pane();
TabbedPane.prototype.constructor = TabbedPane;
TabbedPane.prototype.reset = function()
{
	this.selectedTab = this.selectedTabRole = null;
	DisplayNode.prototype.reset.call(this);
};
TabbedPane.prototype.createSpacerDiv = function TabbedPane_createSpacerDiv()
{
	var div = this.createHTMLElement('div');
	div.style.clear='both';
	var img = this.createHTMLElement('img');
	img.src="images/spacer.gif";
	if (document.compatMode != 'BackCompat')
		img.className='spacer_image';
	div.className='spacer_div';
	div.appendChild(img);
	return div;
};
TabbedPane.prototype.createViewNode = function TabbedPane_crateViewNode()
{
	var direction = getActualStyle(this.currentWindow.document.body, 'direction');
	var rtlSuffix = (direction == 'rtl') ? '_rtl':''; // Support for right-to-left languages
	DisplayNode.prototype.createViewNode.call(this);
	var header = this.createHTMLChildElement(this.viewNode,'div', 'tabbedPaneHeader'+rtlSuffix);
	header.appendChild(this.createSpacerDiv());
	this.tabList = this.createHTMLChildElement(header,'ul');
	this.defaultElement = this.displayElements[0];
	this.tabs = {};
	this.links = {};
	var footer = this.createSpacerDiv();
	footer.className="footer";
	header.appendChild(footer);
	var contentTR = this.createHTMLChildElement(this.viewNode, 'div', 'tabbedPaneContentTR'+rtlSuffix);
	var contentBR = this.createHTMLChildElement(contentTR, 'div', 'tabbedPaneContentBR'+rtlSuffix);
	var contentTL = this.createHTMLChildElement(contentBR, 'div', 'tabbedPaneContentTL'+rtlSuffix);
	var contentBL = this.createHTMLChildElement(contentTL, 'div', 'tabbedPaneContentBL'+rtlSuffix);
	contentBL.appendChild(this.createSpacerDiv());
	this.contentNode = this.createHTMLChildElement(contentBL,'div','tabbedPaneContent'+rtlSuffix);
	var width = this.sharedProperties.width
	if ( width !== null && width !== undefined && width != ' ')
	{
		if (window.browser == 'IE')
			this.contentNode.style.width='100%';
		if (width != '100%' || window.browser != 'Mozilla')
			this.viewNode.style.width = this.sharedProperties.width;
	}
	var cHeight = this.sharedProperties.contentHeight;
	if ( cHeight !== null && cHeight !== undefined && cHeight != ' ')
	{
		this.cHeight = cHeight;
		this.contentNode.style.height=cHeight;
	}
	contentBL.appendChild(this.createSpacerDiv());
};

TabbedPane.prototype.addTab = function (tabCaption, childNode, previousTab)
{
	var childId = childNode.getNodeId();
	var item = this.createHTMLElement('li');
	var nextTab = null;
	if (previousTab)
		nextTab = previousTab.nextSibling;
	else
		nextTab = this.tabList.firstChild;
	item.childId=childId;
	this.tabList.insertBefore(item, nextTab);
	var div = this.createHTMLChildElement(item,'div');
	var link = this.createHTMLChildElement(div,'a');
	this.tabs[childId]=item;
	this.links[childId]=link;
	link.href="javascript:tersus.findNode("+this.getNodeId()+").selectTab("+childId+")";
	link.innerHTML=tersus.escapeHTML(tabCaption);
	if ((! this.selectedTab) && (childNode.element == this.defaultElement) )
	{
		item.className='selected';
		this.selectedTab = item;
		this.selectedChild = childNode;
	}
	var pane = this;
	if (this.sharedProperties.adjustBottom == true)
		tersus.async.exec(function() {pane.onResize();}, false /* don't wait */);
	
};

TabbedPane.prototype.fireOnSelect = function (childNode)
{
	window.engine.handleEvent(childNode, Events.ON_SELECT);
};
TabbedPane.prototype.selectTab = function (nodeId)
{
    this.select(nodeId);
    $(window).trigger('ter-process-end');
};

TabbedPane.prototype.select = function (nodeId)
{
	var childNode = tersus.findNode(nodeId);
	if (!childNode)
		return;
	this.links[nodeId].blur();
	this.setSelectedTab(childNode);
	if (childNode.hasEventHandler(Events.ON_SELECT))
	{
		this.fireOnSelect(childNode);
	}
};

TabbedPane.prototype['get<Selected Pane>'] = function()
{
	return this.selectedChild;
};
TabbedPane.prototype.setSelectedTab  = TabbedPane.prototype['set<Selected Pane>'] = function (childNode)
{
	var nodeId = childNode.getNodeId();
	var tab = this.tabs[nodeId];
	if (tab === null || tab === undefined)
		modelError('Error in TabbedPane.setSelectedTab: no tab called '+tab+' in '+ this.modelId);
	if (this.selectedTab == tab)
	{
		return;
	}
	this.selecting = true;
	if (this.selectedTab)
	{
		this.selectedTab.className = 'normal';
		this.selectedChild.viewNode.style.display='none';
	}
	tab.className='selected';
	if (childNode)
	{
		childNode.viewNode.style.display = '';
		//reattach the child node to reset the parent height
		var pane = childNode.viewNode;
		var parent = pane.parentNode;
	}
	
	this.selectedChild = childNode;
	this.selectedTab = tab;
	
	if (this.selectedChild)
		this.selectedChild.onResize();

};

TabbedPane.prototype.setSelectedTab.requiresNodes = true;

TabbedPane.prototype.removeChildViewNode = function (child)
{
	DisplayNode.prototype.removeChildViewNode.call(this,child);
	var tabItem = this.tabs[child.getNodeId()];
	if (tabItem && this.tabList)
		this.tabList.removeChild(tabItem);
	if (this.selectedTab == tabItem)
	{
		this.selectedTab == null;
		this.selectedChild = null;
	}
};
TabbedPane.prototype.appendChildViewNode = function (child)
{
	this.contentNode.appendChild(child.viewNode);
	var previous = this.findPreviousSibling(child);
	var previousTab = null;
	if (previous)
		previousTab = this.tabs[previous.getNodeId()];
	this.addTab(child.getCaption(), child, previousTab);
	if (child == this.selectedChild)
	{
		child.viewNode.style.display='';
		this.selectedChild = child;
	}
	else
		child.viewNode.style.display='none';

	var tabbedPane = this;
	child.captionChanged = function()
	{
		tabbedPane.links[child.getNodeId()].innerHTML = tersus.escapeHTML(this.getCaption());
	};
		
};
TabbedPane.prototype.onResize = function()
{
	if (this.sharedProperties.adjustBottom == true)
	{
		this.adjustBottom();
	};
	if (this.selectedChild)
		this.selectedChild.onResize();
};

TabbedPane.prototype.adjustHeight = function (delta)
{
	tersus.adjustHeight(this.contentNode, delta);
};

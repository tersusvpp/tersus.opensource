/*********************************************************************************************************
 * Copyright (c) 2003-2014 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
//Table - a simple table
tersus.Table2 = function Table2() {};
tersus.Table2.prototype = new DisplayNode();
tersus.Table2.prototype.constructor = tersus.Table2;
tersus.Table2.prototype.getTag = function()
{
	return 'table';
};
tersus.Table2.prototype.reset = function reset()
{
	DisplayNode.prototype.reset.call(this);
	this.selectedRow = null;
	this.visibleColumns = {};
	if (this.headerRow)
	{
		var headerCells=this.headerRow.childNodes;
		for (var i=0;i<headerCells.length;i++)
		{
			headerCells[i].style.display='';
		}
	}
}
tersus.Table2.prototype.createViewNode = function createViewNode()
{
	this.gridPending=true;
	this.columnNames = [];
	this.sortOrder = null;
	this.visibleColumns = null;
	this.hiddenColumns = {};
	DisplayNode.prototype.createViewNode.call(this);

	this.setProperties(); // In Mozilla, we have to set the styleclass before creating the body of the table
	this.createCaption();
	this.createHeader();
	this.createFooter();
	this.bodyNode = this.currentWindow.document.createElement('tbody');
	this.viewNode.appendChild(this.bodyNode);
	this.viewNode.id = this.getNodeId();
};
tersus.Table2.prototype.createCaption = function createCaption()
{
	if ( this.sharedProperties.showCaption == false) // default = true
	{
		return;
	}
	else
		if ( ! this.getRowPrototype())
			return;
	this.captionDOMElement = this.createHTMLElement('caption');
	this.captionDOMElement.innerHTML = '<div>'+tersus.escapeHTML(this.getCaption())+'</div>';
	this.viewNode.appendChild(this.captionDOMElement);
};
tersus.Table2.prototype.captionChanged = function()
{
	if (this.captionDOMElement)
		this.captionDOMElement.innerHTML = '<div>'+tersus.escapeHTML(this.getCaption())+'</div>';
};

tersus.Table2.prototype.baseProperties = {sortable:null,scrollable:null,resizablecolumns:true, moveablecolumns:true};

tersus.Table2.prototype.initGrid = function()
{
	this.gridNode =  this.wrapperNode || this.viewNode;

	// backwards compatibility for html.sort property
	var sortable = this.prop('sortable');
	if (sortable == null)
	{
		switch (this.prop('sort'))
		{
		case 'single':
			sortable = true;
			break;
		case 'multiple':
			sortable = 'multi';
			break;
		default:
			sortable = false;
		}
	}
	var self=this;
	$(this.gridNode).datagrid(
			{
				scrollable:this.prop('scrollable'),
				resizableColumns : this.prop('resizablecolumns'),
				movableColumns:this.prop('movablecolumns'),
				sortable :  sortable,
				movecolumn:function(e){self.handleColChange();},
				sort:function(e){self.handleColChange();}
			});
	this.updateColumnState();
	if (this.pending_hidden_cols !== undefined)
	{
		this.setHiddenColumns(this.pending_hidden_cols);
		delete this.pending_hidden_cols;
	}
	if (this.pendingVisibleColumns !== undefined)
	{
		this.setVisibleColumns(this.pendingVisibleColumns)
		delete this.pendingVisibleColumns;
	}
	if (this.pendingSortOrder !== undefined)
	{
		this.setSortOrder(this.pendingSortOrder);
		delete this.pendingSortOrder;
	}
	this.gridPending=false;
};
tersus.Table2.hiddenTables = [];
tersus.Table2.prototype.createChildren = function()
{
	DisplayNode.prototype.createChildren.call(this);
	var self=this;
	$(this.viewNode).css('width','100%');
	if ($(this.viewNode).is(':visible'))
	{
		setTimeout(function() {
			self.initGrid();
		},0);
	}
	else
	{
		tersus.Table2.hiddenTables.push(this);
	}

};
$(function() {
	$(window).on('ter-process-end', function() {
		var newList = [];
		$.each(tersus.Table2.hiddenTables, function()
				{
			if ($(this.viewNode).is(':visible'))
			{
				this.initGrid();
			}
			else
				newList.push(this);
				});
		tersus.Table2.hiddenTables = newList;
	});

});
tersus.Table2.prototype.addHeaderCells = function addHeaderCells(headerRow,node)
/* Adds header cells based on the structure of the given node */
{
	for (var i=0; i< node.displayElements.length; i++)
	{	
		var columnElement = node.displayElements[i];
		var columnNodePrototype = columnElement.getChildConstructor().prototype;
		if (columnNodePrototype.isDisplayGroup)
		{
			this.addHeaderCells(headerRow, columnNodePrototype);
		}
		else
		{
			var labelHTML = columnElement.properties.caption;
			if (!labelHTML || labelHTML == ' ')
				labelHTML = tersus.escapeHTML(columnElement.role)
				labelHTML = this.translate(labelHTML);
			var headerCell = this.createHTMLElement('th');
			headerCell.setAttribute("element_name", columnElement.role);
			var columnName = columnElement.properties.sortColumnName;
			if (!columnName)
				columnName = columnElement.role;
			if (!tersus.useSQLQuotedIdentifiers)
				columnName = tersus.sqlize(columnName);
			headerCell.setAttribute("column_name", columnName);
			this.columnNames.push(tersus.useSQLQuotedIdentifiers ? columnName : columnName.toLowerCase());
			headerCell.innerHTML = labelHTML;

			if (headerRow.childNodes.length == 0)
				headerCell.className='first';
			headerRow.appendChild(headerCell);
		}
	}
};
tersus.Table2.prototype.getRowPrototype = function()
{
	var output = null;
	if (this.subFlowList)
	{
		for (var i=0; i< this.subFlowList.length; i++)
		{
			var subFlow = this.subFlowList[i];
			if (subFlow.isDisplayElement && subFlow.role != '<Header>' && subFlow.role != '<Footer>')
			{
				if (output)
					return null;
				else
					output = subFlow;
			}
		}	
	}
	return output;
};
tersus.Table2.prototype.createHeader = function createHeader()
{
	if ( this.sharedProperties.showHeadings == false) // default = true
		return;
	var rowElement = this.getRowPrototype();
	if (! rowElement) return;
	var rowPrototype = tersus.repository.get(rowElement.modelId).prototype;
	if (! rowPrototype.subFlowList) return;
	var headerRow = this.createHTMLElement('tr');
	this.columnList = [];
	this.addHeaderCells(headerRow, rowPrototype);
	var tableHeader = this.createHTMLElement('thead');
	tableHeader.appendChild(headerRow);
	this.header = tableHeader;
	this.headerRow = headerRow;
	this.viewNode.appendChild(tableHeader);
};
tersus.Table2.prototype.createFooter = function createHeader()
{
	if ( this.getElement('<Footer>') == null) 
		return;
	this.viewNode.appendChild(this.createHTMLElement('tfoot'));
};

tersus.Table2.prototype.removeChildViewNode = function removeChildViewNode(child)
{
	var c = child.viewNode;
	var row = null;
	if (c.tagName == 'TR' )
		row = c;
	else if (c.parentNode && c.parentNode.tagName == 'TR')
		row = c.parentNode;
	else if (c.parentNode && c.parentNode.parentNode && c.parentNode.parentNode.tagName== 'TR')
		row = c.parentNode.parentNode;
	if (row && row.parentNode)
		row.parentNode.removeChild(row);
};

tersus.Table2.prototype.applyColOrder = function (row)
{
	$(row).data('tracked',false);
};

tersus.Table2.prototype.appendChildViewNode = function appendChildViewNode(child, childElement)
{
	var element = childElement?childElement:child.element;
	var target = 'tbody';
	if (element.role == '<Header>')
		target='thead';
	if (element.role == '<Footer>')
		target='tfoot';
	var row = null;
	if (child.viewNode.tagName == 'TR')
	{
		row = child.viewNode;
	}
	else
	{
		var row = this.createHTMLElement('TR');
		if (child.viewNode.tagName == 'TH' || child.viewNode.tagName == 'TD')
			row.appendChild(child.viewNode);
		else
		{
			var cell = this.createHTMLChildElement(row,'TD');
			cell.appendChild(child.viewNode);
		}
		row.setAttribute('display_order', child.viewNode.getAttribute('display_order'));
	}
	/*
// Special logic for suppressing column hiding.  Probably not needed because columns are hidden only when number of cells match header cells
    if (element.prop('suppressColumnHiding'))
    {
	$(row).data('datagrid.nohide',true);
    }
	 */

	/* There may be a bug in handling display order in the case of a table with multiple row elements. This may or many not be an important use case and may or may not be a new bug */
	if (this.gridNode)
		$(this.gridNode).datagrid('appendRow',target,row);
	else
	{
		// Before datagrid is initialized - add via regular API
		var targetNode = $(this.viewNode).children(target)[0];
		if (targetNode)
			DisplayNode.appendChildNode(targetNode,row); 
	}

};

tersus.Table2.prototype['get<Selected Row>'] = function ()
{
	if (this.selectedRow)
		return this.selectedRow;
	else
		return null;
};
tersus.Table2.prototype['set<Selected Row>'] = tersus.Table2.prototype.select = function (row)
{
	var selectedClassName = 'selected';
	if (this.selectedRow)
	{
		this.selectedRow.removeStyleClass(selectedClassName);
	}
	this.selectedRow = row;
	if (row && row.viewNode)
	{
		row.addStyleClass(selectedClassName);
	}
};
tersus.Table2.prototype['remove<Selected Row>'] = function ()
{
	var selectedClassName = 'selected';
	if (this.selectedRow)
	{
		this.selectedRow.removeStyleClass(selectedClassName);
		this.selectedRow = null;
	}
};
tersus.Table2.prototype.select.requiresNodes = true;
tersus.Table2.prototype.outputColList = function(v, s)
{
	var self=this;
	var colList = $.map(v,function(a,i){
		var suffix = s && s[i];
		var columnName = self.columnNames[a];
		if (tersus.useSQLQuotedIdentifiers)
			columnName = '"'+columnName+'"'; 
		return suffix ? columnName+ ' ' + suffix  : columnName;}).join(',');
	return (colList ? colList : null);
};
tersus.Table2.prototype.parseColList = function(str)
{
	if (str.indexOf('"')>=0)
		re = / *"([^"]+)"( +([a-z]+))? *(,|$)/gi
			else 
				re = / *([^, ]+)( +([a-z]+))? *(,|$)/gi;
			var out=[];
			while (result = re.exec(str))
			{
				var columnName = result[1];
				if (! tersus.useSQLQuotedIdentifiers)
					columnName = columnName.toLowerCase();
				var found=false;
				for (var j=0;j<this.columnNames.length;j++)
				{
					if (this.columnNames[j] == columnName)
					{
						out.push({colIndex:j,suffix:result[3]});
						found = true;
					}
				}
				if (!found)
					throw 'Unknown column:'+result[1];
			}
			return out;
};

tersus.Table2.prototype.updateSortOrder = function()
{
	/* Convert a vector of 'up'/'down'/'' in visual order to an SQL-compatible "order-by" clause using column names */
	var sort =  $(this.gridNode).datagrid('sort');
	var v=[]; /* column indexes */
	var s=[]; /* suffixes */
	var p =  $(this.gridNode).datagrid('permutation');
	$.each(sort, function(i,dir) {
		if (dir)
		{
			v.push(p[i]);
			s.push(dir == 'up' ?  'ASC': 'DESC');
		}
	});
	this.sortOrder = this.outputColList(v,s);
};
tersus.Table2.prototype.updateVisibleColumns = function()
{
	var visible =  $(this.gridNode).datagrid('visibility');
	/* Output current permutation after filtering out hidden columns */
	this.visibleColumns =  this.outputColList( $.map($(this.gridNode).datagrid('permutation'),function(a,i) {
		if (visible[a])
			return a;
	}));
};
tersus.Table2.prototype.updateColumnState = function()
{
	this.updateSortOrder();
	this.updateVisibleColumns();
};
tersus.Table2.prototype['get<Sort Order>'] = tersus.Table2.prototype.getSortOrder = function()
{
	return this.pendingSortOrder || this.sortOrder;
};
tersus.Table2.prototype['set<Sort Order>'] = function (sort_order)
{
	if (this.gridPending)
		this.pendingSortOrder = sort_order;
	else
		this.setSortOrder(sort_order);
};
tersus.Table2.prototype.setSortOrder = function (sort_order)
{
	var l = this.parseColList(sort_order);
	/* step one: ensure compatibility with permutation */
	var p =  $(this.gridNode).datagrid('permutation');
	var prev;
	var sortLogical = $.map(p, function(){return ''});
	var permChanged = false;
	$.each(l, function(i,spec)
			{
		sortLogical[spec.colIndex] = (spec.suffix && spec.suffix.toLowerCase().charAt(0)=='d') ? 'down' : 'up';
		if (prev)
		{
			var iprev, icurr;
			$.each(p, function(i,a) {
				if (a==spec.colIndex)
					icurr=i;
				if (a==prev.colIndex)
					iprev=i;
			});
			if (icurr < iprev)
			{
				p.splice(icurr,1);/* Remove from current position  */
				p.splice(iprev,0,spec.colIndex); /* Add after  previous element's position */
				permChanged=true;
			}

		}
		prev = spec;
			});
	var sortVisible = $.map(p,function(col,i){
		return sortLogical[col];});

	if (permChanged)
		$(this.gridNode).datagrid('permutation',p);
	$(this.gridNode).datagrid('sort',sortVisible);
	this.updateColumnState();
};
tersus.Table2.prototype['remove<Sort Order>'] = function ()
{
	this['set<Sort Order>']('');
};
tersus.Table2.prototype['set<Hidden Columns>'] = function(hidden_cols)
{
	if (this.gridPending)
		this.pending_hidden_cols = hidden_cols;
	else
		this.setHiddenColumns(hidden_cols);
};
tersus.Table2.prototype.setHiddenColumns = function(hidden_cols)
{
	var hidden = this.parseColList(hidden_cols);
	var v = $.map(this.columnNames,function(){return 1;}); // vector of ones
	$.each(hidden, function(i,a) {
		v[a.colIndex]=0;
	});
	$(this.gridNode).datagrid('visibility',v);
	this.updateColumnState();
};

tersus.Table2.prototype['remove<Hidden Columns>'] = function()
{
	this['set<Hidden Columns>']('');
};

tersus.Table2.prototype['get<Hidden Columns>'] =  function()
{
	if (this.pending_hidden_cols)
		return this.pending_hidden_cols;
	var visibility =  $(this.gridNode).datagrid('visibility');
	var v = $.map(visibility, function(a,i) { if (a==0) return i;});
	return this.outputColList(v);
};
tersus.Table2.prototype['get<All Columns>'] =  function()
{
	return this.columnNames.join(',');
};


tersus.Table2.prototype['get<Visible Columns>'] =  function()
{
	return this.pendingVisibleColumns || this.visibleColumns;
};
tersus.Table2.prototype['set<Visible Columns>'] = function(visible_cols)
{
	if (this.gridPending)
		this.pendingVisibleColumns = visible_cols;
	else
		this.setVisibleColumns(visible_cols);
};
tersus.Table2.prototype.setVisibleColumns = function(visible_cols)
{
	var cols = this.parseColList(visible_cols).map(function(a,i){return a.colIndex;});
	var v = $.map(this.columnNames,function(){return 0;}); // vector of zeros
	$.each(cols, function(i,a) {
		v[a]=1;
	});
	var p = cols.slice(0);
	var s = new tersus.Set(cols);
	for (var i=0;i<v.length;i++)
	{
		if (! s.contains(i))
			p.push(i);
	}
	$(this.gridNode).datagrid('visibility',v);
	$(this.gridNode).datagrid('permutation',p);
	this.updateColumnState();
};
tersus.Table2.prototype['remove<Visible Columns>'] = function()
{
	/* Resets <Visible Columns> - clears visibility and resets permutation */
	var v = $.map(this.columnNames,function(){return 1;}); // vector of ones
	var p = $.map(this.columnNames, function(a,i) {return i;}); // Identity permutation

	$(this.gridNode).datagrid('visibility',v);
	$(this.gridNode).datagrid('permutation',p);
	this.updateColumnState();
};
tersus.Table2.prototype.handleColChange = function()
{
	var prevSortOrder = this.sortOrder;
	var prevVisibleColumns = this.visibleColumns;
	this.updateColumnState();
	if (prevVisibleColumns != this.visibleColumns)
		tersus.invokeAction('<On Move Column>',null, null,this,true);
	if (prevSortOrder != this.sortOrder)
		tersus.invokeAction('<On Sort>', null, null, this, true);
};
tersus.Table2.prototype.setStyleClass = function(c)
{
	DisplayNode.prototype.setStyleClass.call(this,c);
	if (this.gridNode)
		$(this.gridNode).datagrid('setTableClass',this.styleClass);
};
tersus.Table2.prototype.onResize = function()
{
	if (this.gridNode)
	{
		var self = this;
		$(this.gridNode).datagrid('adjustSize',function(){self.onResizeChildren();});
	}
};

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.isIE6 = navigator.userAgent.indexOf('MSIE 6') >=0;
if ( tersus.isIPhone)
{
	tersus.Dialog = tersus.PopIn; // On iPhone (and iPhone simulation) we use a full pop-in for instead of a Dialog
}
else
{
	tersus.Dialog = tersus.PopIn.__subclass('Dialog');
	tersus.Dialog.prototype.setStyleClass = function(value)
	{
		if (this.pluginVersion>0)
			DisplayNode.prototype.setStyleClass.call(this,value);
	};
	
	tersus.Dialog.prototype.createViewNode = function()
	{
		// create mask (this gives modality by blocking UI access to existing content)
		clearSelection(this.currentWindow); // sometimes the original selection is extended to newly created elements
		var d = this.currentWindow.document;
		var r = this.getCurrentRootViewNode();
		var ml = this.createHTMLChildElement(r,'div');
		ml.className='dialog_mask';
		$(ml).css('z-index',++ tersus.topZindex);
		this.maskLayer = ml;
		// create dialog div
		var template = tersus.repository.getHTMLTemplate(this.templateURL);
		var dialog = importNode(d,template);
		var pStyle=this.sharedProperties.dialogPosition;
		if (pStyle)
		{
			$(dialog).attr('style',pStyle);
		}
		this.dialogLayer = dialog;
		dialog.style.position='absolute';
		var c = this.sprop('styleClass');
		dialog.className='dialog';
		
		this.viewNode = tersus.findChildNodesByClassName(dialog,'dialogBody')[0];
		if (this.viewNode)
		{
			this.header = tersus.findChildNodesByClassName(dialog,'dialogHeader')[0];
			this.footer = tersus.findChildNodesByClassName(dialog,'dialogFooter')[0];
		}
		else
			this.viewNode = dialog;
		this.isOpen = true;
		r.appendChild(dialog);
		$(dialog).css('z-index',++ tersus.topZindex);
		if (this.header)
		  this.header.innerHTML = tersus.escapeHTML(this.getPopupTitle());
		var w = this.sharedProperties.windowWidth;
		var h = this.sharedProperties.windowHeight;
		if ( ! w || w == ' ' || w == 'auto' ||  ! h || h == ' ' || h == 'auto' )
		{
		    /* Created "hiding" div in which dialog initialization will take place (to be removed on first resize)
		     * 
		     * Using a 0px X 0px div rather than a hidden (display:none) one because size calculations don't work on hidden elements
		     * 
		     */
		    var $c = $('<div style="position:absolute;overflow:hidden;top:0px;left:0px;height:0px;width:0px">', this.currentWindow.document);
		    $c.append($(dialog));
		    $(r).append($c);
		    var self=this;
		    tersus.async.exec(function(){self.onResize();$(r).append($(dialog));$c.remove()},true);
		}
		else
		      this.onResize();

		this.hideSelectNodes();
	};
	tersus.Dialog.prototype.onResize = function()
	{
		if (! (this.isOpen && this.currentWindow && ! this.currentWindow.closed && this.viewNode))
			return;
		var d = this.viewNode.ownerDocument;
	
		var s = getViewportSize(d);
		// Prevent infinite loop in Internet Explorer ...
		if (this.lastSize && this.lastSize.height == s.height && this.lastSize.width == s.width)
			return; // no change in size - nothing to do
		this.lastSize = s;
		var th = s.height - 8;
		var tw = s.width - 8;
	
        var dl = this.dialogLayer;
		var w = this.sharedProperties.windowWidth;
		if ( ! w || w == ' ' || w == 'auto')
		    w = $(dl).outerWidth(false);
		var h = this.sharedProperties.windowHeight;
		var hAuto = false;
		if ( ! h || h == ' ' || h == 'auto')
		{
		    h = $(this.viewNode).height();
		    th -= 82;
		    hAuto = true;
		}
		w = Math.min(w,tw);
		h = Math.min(h,th);
		$(dl).css('box-sizing','border-box').width(w);
		var pStyle=this.sharedProperties.dialogPosition;
		if (!pStyle)
		{
			var leftPx = parseInt((tw-w)/2);
			var topPx = parseInt((th-h)/2);
			var scroll = getWindowScroll(this.currentWindow);
			dl.style.top = (topPx + scroll.top )+'px';
			dl.style.left = (leftPx + scroll.left)+'px';
		}
		var ml = this.maskLayer;
		ml.style.height = s.height + 'px';
		ml.style.width = '100%';
		ml.style.position = 'absolute';
		ml.style.top = '0px';
		ml.style.left = '0px';

		var delta = 0;
		if (this.header)
			delta += getHeight(this.header);
		if (this.footer)
			delta += getHeight(this.footer);
		if (hAuto)
			delta += getHeight(this.viewNode);
		if (h-delta>20)
		{
		    $(this.viewNode).css('box-sizing','border-box').height(h-delta);
		}
		this.onResizeChildren();
	}
	tersus.Dialog.prototype.doClose = function() 
	{
		delete tersus.popupNodes[this.getNodeId()];
	
		if (this.isOpen)
		{
			this.resumeNavigation();
			this.viewNode = null;
			this.header = null;
			var dl = this.dialogLayer;
			var ml = this.maskLayer;
			this.dialogLayer=null;
			this.maskLayer=null;
			dl.parentNode.removeChild(dl);
			ml.parentNode.removeChild(ml);
		}
		this.isOpen = false;
		this.restoreSelectNodes();
	};
	tersus.Dialog.prototype.hideSelectNodes = function()
	{
	   	if (!tersus.isIE6)
	   		return;
		var l = this.currentWindow.document.body.getElementsByTagName('select');
		this.selectNodes = [];
		for (var i=0;i<l.length;i++)
			this.selectNodes.push(l[i]);
		for (var i=0;i<this.selectNodes.length;i++)
		{
			var n = this.selectNodes[i];
			var placeHolder = this.createHTMLElement('span');
			n.placeHolder=placeHolder;
			n.parentNode.replaceChild(placeHolder,n);			
		}
	
	};
 	tersus.Dialog.prototype.restoreSelectNodes = function()
	{
		if (this.selectNodes)
		{
			for (var i =0; i<this.selectNodes.length; i++)
			{
				var n =	this.selectNodes[i];
				var placeHolder = n.placeHolder;
				placeHolder.parentNode.replaceChild(n, placeHolder);
				n.placeHolder = null;
			}
		};
	};
	tersus.Dialog.prototype.setEventHandlers = function()
	{
		DisplayNode.prototype.setEventHandlers.call(this);
		this.dragNode = this.header ? this.header : this.viewNode;
	
		this.registerEventHandler(Events.ON_MOUSEDOWN, this.dragNode);
	};
	tersus.Dialog.prototype.onMouseDown = function(domNode, callback)
	{
		var atts = tersus.currentEventAttributes;
		if (!atts) return;
		callback();
		if (this.checkTargetElement(atts.targetElement))
		{
			var p = getScreenPosition(this.dialogLayer);
			this.dialogLayer.style.position='absolute';
			this.dialogLayer.style.top  = (p.top - getScroll(this.dialogLayer).top)+'px';
			this.dialogLayer.style.left = (p.left  - getScroll(this.dialogLayer).left)+'px';
			this.baseP = p;
			this.baseMouseP = {x:atts.clientX,y:atts.clientY};
			var node = this;
			var b = this.viewNode.ownerDocument.body;
			b.style.cursor = 'pointer';
			b.onmouseup = function(e)
			{
				node.onDrag(e);
				node.endDrag();
				return false; // disable default action (image drag) 
			};
			b.onmousemove = function(e)
			{
				node.onDrag(e);
				return false; //disable default action (image drag)
			}
			return false; //disable default action (image drag)
		}
		return true;// Don't disable default action
	};

	// Checks whether an element is a "good" drag source (doesn't have its own event handling logic)
	tersus.Dialog.prototype.checkTargetElement = function (targetElement)
	{
		if (targetElement == this.dragNode)
			return true;
		var n = targetElement;
		while (n && n != this.dragNode)
		{
			if (n.onclick || n.onmousedown || n.onmouseup || n.onmousemove || n.ondoubleclick)
				return false;
			if (n.tagName == 'INPUT' || n.tagName == 'TEXTAREA' || n.tagName == 'SELECT' || n.tagName == 'OPTION')
				return false;
			n = n.parentNode;
		}
		return true;
	};
	tersus.Dialog.prototype.endDrag = function()
	{
		var b = this.viewNode.ownerDocument.body;
		b.onmousemove = null;
		b.onmouseup = null;
		b.style.cursor = 'default';
	
	
	};
	tersus.Dialog.prototype.onDrag = function(e)
	{
		var atts;
		if (e)
			atts = getEventAttributes(e);
		else
			atts = getEventAttributes(this.currentWindow.event);
		if (!atts) return;
		this.dialogLayer.style.top = (this.baseP.top + atts.clientY - this.baseMouseP.y - getScroll(this.dialogLayer).top)+'px';
		this.dialogLayer.style.left = (this.baseP.left + atts.clientX - this.baseMouseP.x - getScroll(this.dialogLayer).left)+'px';
	
	}
}

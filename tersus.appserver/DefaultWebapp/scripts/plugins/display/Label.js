/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function Label() {};
Label.prototype = new tersus.DisplayField();
Label.prototype.constructor = Label;
Label.prototype.defaultStyleClass='label';
Label.prototype.textTranslation=true;

Label.prototype.initialize = function ()
{
	this.value = new tersus.Text();
	this.value.leafValue = this.getBaseCaption();
};
/*
//TODO(x) delete this
Label.prototype.initViewNode = function()
{
	this.value = new tersus.Text();
	this.value.leafValue = this.getCaption();
	tersus.GenericField.prototype.initViewNode.call(this);
}*/
Label.prototype.captionChanged = function()
{
	if (this.viewNode)
	{
		var caption = this.getBaseCaption();
		if (! caption.isNode)
		{
			var text = new tersus.Text();
			text.leafValue = caption;
			caption = text;
		}
		this.setFieldValue(caption);
	}
};

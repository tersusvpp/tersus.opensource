/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 
function Button(){};
Button.prototype = new DisplayNode();
Button.prototype.constructor = Button;
Button.prototype.type = 'Button';
Button.prototype.defaultTag = 'button';
Button.prototype.isButton = true;
Button.prototype.destroy = function()
{
	this.isDestroyed = true;
	DisplayNode.prototype.destroy.call(this);
};
Button.prototype.createChildren = function Button_createChildren()
{
	// we don't create the sub-hierarchy for a button.
};
Button.prototype.createViewNode = function Button_createViewNode()
{
    var tag = this.getTag();
    if (tag=='button')
    {
        var type=this.prop('type') || 'button';
        this.viewNode=$('<button type='+type+'/>',this.currentWindow.document)[0]; // Work-around for IE8 limitation on setting button type
    }
    else
        this.viewNode = this.createHTMLElement(tag);
    this.viewNode.setAttribute('lid',this.getNodeId());
	this.viewNode.innerHTML = tersus.escapeHTML(this.getCaption());
};
Button.prototype.captionChanged = function()
{
	if (this.viewNode)
		this.viewNode.innerHTML = tersus.escapeHTML(this.getCaption());
};
Button.prototype.setEventHandlers = function Button_setEventHandlers()
{
	DisplayNode.prototype.setEventHandlers.call(this);
	this.registerEventHandler(Events.ON_CLICK);
}
Button.prototype.doStart = function Button_start() 
{
	// We override doStart and not start because we don't want to trace 'Started' events for buttons
	this.waiting();
};
Button.prototype.onClick = function(domNode, callback)
{
	queueEvent(this, this.handleClick, [], new Date(), "Button clicked ["+this.getPath().str+"]", callback);
}; 
Button.prototype.handleClick = function tersus_Button_handleClick() 
{
	if (this.isDestroyed || !this.currentWindow || this.currentWindow.closed || this.disabled || this.status != FlowStatus.WAITING_FOR_INPUT)
	{
		return;
	}
	
	var validate = this.sharedProperties.validate != false; // null considered true
	var validationContext = this.getValidationContext();
	var valid = true;
	if (validate)
	{
		if (tlog) tlog('button.onclick:validating');
		valid = validationContext.validate();
	}
	if (valid)
	{
		if (tlog) tlog('button.onclick: starting handler');
		this.children = null;
		this.clicked = true;
		this.continueExecution();
		if (tlog) tlog('button.onclick: handler done');
	}
	else
		if (tlog) tlog('button.onclick: not valid');
	
};

Button.prototype.validate = function validate()
{
	return true; // Do not traverse children
};
Button.prototype.resume = function resumeButton()
{
	if (this.clicked)
	{
		this.clicked = false;
		this.started();
		FlowNode.prototype.start.call(this);
	}
	else 
		FlowNode.prototype.resume.call(this);
};
Button.prototype['get<Disabled>'] = function ()
{
	return this.disabled;
};

Button.prototype['set<Disabled>'] = function (disabled)
{
	this.disabled = disabled?true:false;
	if (this.viewNode)
	{
		this.viewNode.disabled=this.disabled;
		if (window.browser == 'IE') // IE 6.0 doesn't support the :disable CSS pseudoclass, so we use an explicit style as a workaround
		{
			if (disabled && !this.viewNode.className)
				this.viewNode.className = 'disabled';
			if (!disabled && this.viewNode.className=='disabled')
				this.viewNode.className = '';
		}
	}
};

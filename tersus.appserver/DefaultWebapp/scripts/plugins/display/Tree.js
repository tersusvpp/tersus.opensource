/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function Tree() { this.selectedItem = null};
Tree.prototype = new DisplayNode();
Tree.prototype.constructor = Tree;
Tree.prototype.defaultTag = 'div';
Tree.prototype.start = function startTree()
{
	this.root = new TreeItem();
	this.root.attach(this,null);
	this.setStatus(FlowStatus.WAITING_FOR_INPUT);
};
Tree.prototype.refresh = function()
{
	if (this.root.childItems)
	{
		for (var i=0; i< this.root.childItems.length; i++)
			this.root.childItems[i].onDelete();
	}
	this.childItems = null;
	this.root.loadChildren();
};
Tree.prototype.findById = function (id)
{
	if (this.nodeId == id)
		return this;
	else if (this.root)
		return this.root.findById(id);
	else
		return null;
};
Tree.prototype['get<Selected Item>'] = function()
{
 	if (this.selectedItem)
 		return this.selectedItem;
 	else
 		return null;
};
Tree.prototype.scanChildren = function(filter, alreadyScanned, outputList)
{
	if (filter.match(this))
		outputList.push(this);
	if (this.root)
		this.root.scanChildren(filter, alreadyScanned,outputList);		
};
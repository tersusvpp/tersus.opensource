/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.HTMLEditor = function tersus_HTMLEditor(){};
tersus.HTMLEditor.prototype = new DisplayNode();
tersus.HTMLEditor.prototype.constructor = tersus.HTMLEditor;
tersus.HTMLEditor.prototype.defaultTag = 'textarea';
tersus.HTMLEditor.prototype.createViewNode = function()
{
	DisplayNode.prototype.createViewNode.call(this);
	this.open();
};
tersus.HTMLEditor.commandMap = { 'mceImage':'<Insert Image>'}
tersus.HTMLEditor.execCommandHandler = function tersus_html_editor_command_handler(editor_id, elm, command, user_interface, value)
{
	if (!elm)
		return false;
	if ( ! tersus.HTMLEditor.commandMap[command])
		return false;
	var w = getParentWindow(elm).parent;
	var tMCE = tersus.HTMLEditor.getTinyMCE(w);
	if (tMCE)
	{
		var editorInstance = tMCE.getInstanceById(editor_id);
		var nodeId = editor_id.match(/mce_(\d+)/)[1];
		var node = tersus.findNode(nodeId);

		var element = node.getElement(tersus.HTMLEditor.commandMap[command]);
		if (element)
		{
			var child = element.createChild(node);
			resumeRoot(); // child will be executerd
			if (child.isButton) // we need to simulate pushing the button
				child.onClick();


			return true;
		}

	}

	return false;

};
tersus.HTMLEditor.defaultSettings =
{
	mode : "none"
	,theme : "advanced"
	,content_css : "Styles?timestamp="+tersus.lastStyleModification
	,execcommand_callback : tersus.HTMLEditor.execCommandHandler
};
tersus.HTMLEditor.prototype.createChildren = function()
{ // supressing child creation - child elements represent components of the externally defined editor
  // in the future we may support adding elements to the display
};
tersus.HTMLEditor.loaded = false;
tersus.HTMLEditor.loading = false;
window.tinyMCE_onload =  function tinyMCE_onload()
{
	var w = window;
	if (tersus.HTMLEditor.customizeTinyMCE)
		tersus.HTMLEditor.customizeTinyMCE(w);
	w.tinymce.dom.Event.domLoaded=true;
	var s = {}; // settings for tinyMCE
	var d = tersus.HTMLEditor.defaultSettings;
	for (p in d)
		s[p]= d[p];
	var u = tersus.HTMLEditor.settings; // user settings
	if (u)
	{
		for (p in u)
			s[p]=u[p];
	}
	w.tinyMCE.init(s);
	w.tinyMCE_loaded = true;
	w.tinyMCE_loading = false;
	tersus.foreach(w.tinyMCE_openQueue, function(node)
	{
		node.showEditor();
	});
}
tersus.HTMLEditor.prototype.open = function()
{
	var node = this;
	var w = this.currentWindow;
	if (!w.tinyMCE_loaded && !w.tinyMCE_loading)
	{
		w.tinyMCE_loading = true;
		w.tinyMCE_openQueue = [];
		tersus.loadScript('tiny_mce/tiny_mce_src.js',w);
	}
 	if (w.tinyMCE_loaded)
		tersus.async.exec( function () {node.showEditor();}, false /* don't wait */);
 	else
	 	w.tinyMCE_openQueue.push(node);
	this.open = true;
};
tersus.HTMLEditor.prototype.showEditor = function()
{
	var tMCE = this.getTinyMCE();
	if (tMCE)
	{
		var id = 'mce_'+this.getNodeId();
		this.editorId = this.viewNode.id = this.viewNode.name = id;
        tMCE.execCommand('mceAddControl', false, id);
//		tinyMCE.execCommand('mceFocus',false,this.getEditorId());
//		editorInstance.select();
	}

	else
		modelExecutionError("HTML Editor not available",this);
};
tersus.HTMLEditor.prototype.getEditor = function()
{
	var tMCE = this.getTinyMCE();
	if (tMCE && this.open)
		return tMCE.get(this.editorId);
	else
		return null;
};
tersus.HTMLEditor.prototype.invokeAction  = function(action, caller)
{
	//Todo - some actions (e.g. focus) may need to be invoked as global command
	//     - some actions may be implemented in the model
	var editor = this.getEditor();
	if (editor)
	{
		var value = caller.getLeafChild('Value');
		editor.execCommand(action, false, value);
		editor.execCommand('mceRepaint');
	}
};
tersus.HTMLEditor.prototype.destroy = function()
{
	if (this.open)
	{
		var tMCE = this.getTinyMCE();
		var node = this;
		if (tMCE)
		{
			// Because of timing issues it is possible that tinyMCE still doesn't know about our editor.
			// In such cases we don't remove it (and there's a danger of memory leak - probably mainly on older browsers)
			if (this.editorId && tMCE.get(this.editorId))
				tMCE.execCommand('mceRemoveControl', false, node.editorId);
		}
		this.open = false;
	}

	DisplayNode.prototype.destroy.call(this);
};
tersus.HTMLEditor.getTinyMCE = function (w)
{
	var tMCE = null;
	try
	{
		if (w && ! w.closed)
			tMCE = w.tinyMCE;
	}
	catch (e)
	{
	}
	return tMCE;
};
tersus.HTMLEditor.prototype.getTinyMCE = function()
{
	return tersus.HTMLEditor.getTinyMCE(this.currentWindow);
};
tersus.HTMLEditor.prototype['get<HTML Text>'] = function()
{
	var editor = this.getEditor();
	if (editor)
		return editor.getContent();
};
tersus.HTMLEditor.prototype['get<Selected HTML Text>'] = function()
{
	var editor = this.getEditor();
	if (editor)
	{
		var sel = new TinyMCE_Selection(editor);
		return sel.getSelectedHTML();
	}
};
tersus.HTMLEditor.prototype['get<Selected HTML Element>'] = function()
{
	var editor = this.getEditor();
	if (editor)
	{
		var sel = editor.selection;
		if (sel)
			return sel.getNode();
	}
};
tersus.HTMLEditor.setHTML = tersus.HTMLEditor.prototype['set<HTML Text>'] = function(text)
{
	if (!this.open)
	{
		tersus.error('Editor Not Open');
		return;
	}
	var node = this;
	var f = function()
	{
		var editor = node.getEditor();
		if (editor)
			setTimeout(function(){editor.setContent(text);},100);
		else
			setTimeout(f, 100);
	}
	setTimeout(f, 100);
};

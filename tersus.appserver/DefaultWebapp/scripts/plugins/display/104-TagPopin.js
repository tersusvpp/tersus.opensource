tersus.TagPopin = tersus.FullScreen.__subclass('TagPopin');
tersus.TagPopin.prototype.getCurrentRootViewNode = function()
{
	var root;
	var doc = this.currentWindow.document;
	root = doc.getElementById('root');
	if (! root)
		root = doc.getElementById('tersus.content');
	return root;
};
tersus.TagPopin.prototype.attachViewNode = function()
{
	this.originalRoot = this.getCurrentRootViewNode();
	this.rootNode = this.wrapperNode? this.wrapperNode:this.viewNode;
	this.originalRoot.parentNode.appendChild(this.rootNode, this.originalRoot);
};
tersus.TagPopin.prototype.isPopup = true;
tersus.TagPopin.prototype.isPopin = true;
tersus.TagPopin.prototype.start = function()
{
	if (this.element && ! this.element.prepareForHTML)
		this.element.prepareForHTML = DisplayElement.prototype.prepareForHTML;
	var w = this.currentWindow;
	var doc = w.document;
	this.originalRootDisplayNode = w.currentRootDisplayNode;
	tersus.popupNodes[this.getNodeId()]=this;
	clearSelection(w);
	this.create();
	w.currentRootDisplayNode = this;		
	FlowNode.prototype.start.call(this);
	this.isOpen = true;
	if (tersus.isIPhone)
		window.scrollTo(0,1);
	var w =this.rootNode;
	var cn = w.className;
	setTimeout(function() {w.className = cn +' open';},10);
};
tersus.TagPopin.prototype.destroy = function()
{
	if (! this.isDestroyed)
	{
		this.isDestroyed = true;
		DisplayNode.prototype.destroy.call(this);
		this.doClose();
	}
};
tersus.TagPopin.prototype.closeWindow = function() 
{
	this.destroyHierarchy();
};
tersus.TagPopin.prototype.doClose = function()
{
	delete tersus.popupNodes[this.getNodeId()];
	
	if (this.isOpen)
	{
		var doc = this.currentWindow.document;
//		currentRoot.parentNode.replaceChild(this.originalRoot, currentRoot);
		var r = this.rootNode;
		r.className = r.className.replace(/open/,'closed');
		setTimeout(function(){r.parentNode.removeChild(r);},500);
		this.currentWindow.currentRootDisplayNode = this.originalRootDisplayNode;
		this.viewNode = null;
		this.rootNode = null;
	}
	this.isOpen = false;
};

tersus.TagPopin.prototype.findById = function tersus_popIn_FindById(id, alreadyScanned)
{
	//Traverse only if its window is open
	if (this.isOpen)
		return tersus.Node.prototype.findById.call(this,id, alreadyScanned);
	else
		return null;
};
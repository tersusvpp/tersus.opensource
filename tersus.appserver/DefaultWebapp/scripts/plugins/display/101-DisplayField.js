/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.DisplayField = function tersus_DisplayField() {};
tersus.DisplayField.prototype = new tersus.GenericField();
tersus.DisplayField.prototype.constructor = tersus.DisplayField;
tersus.DisplayField.prototype.explicitReadOnly = true;
TextDisplay = NumberDisplay = DateDisplay = tersus.DisplayField; // backward compatability

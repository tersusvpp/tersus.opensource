/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 
FileInputField = function FileInputField() {
};
FileInputField.prototype = new DisplayNode();
FileInputField.prototype.constructor = FileInputField;

FileInputField.prototype.isFileInputField = true;

FileInputField.prototype.createViewNode = function createViewNode()
{
    this.fileNode = this.currentWindow.document.createElement('input');
    this.fileNode.type = 'file';
    if (this.sharedProperties.accept)
    	this.fileNode.accept=this.sharedProperties.accept;
    this.viewNode = this.currentWindow.document.createElement('form');
    this.viewNode.className='inline';
    this.viewNode.appendChild(this.fileNode);
    if (this.sharedProperties.size)
    	this.viewNode.size = this.sharedProperties.size;
};

FileInputField.prototype._onChange = function()
{
    this.inputFile = null;
};
FileInputField.prototype['get<Value>'] = function getFieldValue()
{
	if (!this.fileNode || this.fileNode.value == "")
		if (this.children && this.children['<Value>'])
			return this.children['<Value>'];
		else
			return null;
	if (! this.inputFile)
	{
		var element = this.getElement('<Value>');
		if (element)
		{
			this.inputFile = element.createChildInstance();
			this.inputFile.inputFieldDisplayNode = this;
		        this.inputFile.window=this.currentWindow;
			var files = this.fileNode.files;
			if (files && files.length == 1)
			    this.inputFile.file = files.item(0);
		}
	}
	return this.inputFile;
};

FileInputField.prototype.registerEventHandler = function(event, domNode)
{
	if (!domNode)
		domNode = this.fileNode;
	DisplayNode.prototype.registerEventHandler.call(this,event,domNode);
};


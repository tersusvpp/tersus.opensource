/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 
function Pane() {};
Pane.prototype = new DisplayNode();
Pane.prototype.constructor = Pane;
Pane.prototype.defaultTag = 'div';
Pane.prototype.minHeight = 50;
Pane.prototype.setHeight = function(newHeight)
{
	this.viewNode.style.height = newHeight;
};
Pane.prototype.adjustBottom = function()
{
	var d = this.viewNode.ownerDocument;
	var body = this.viewNode.ownerDocument.body;
	var height = getVisibleSize(body).height;
	var margins = parseInt(getActualStyle(body,'margin-top'))+parseInt(getActualStyle(body,'margin-bottom'));
	height-=margins;

	var currentHeight = getVisibleSize(this.viewNode).height;
	var currentBottom = getOffsets(this.viewNode).top + currentHeight;
	if (window.browser != 'IE')
	{
		var paddingBottom = parseInt(getActualStyle(this.viewNode,'padding-bottom'));
		var paddingTop = parseInt(getActualStyle(this.viewNode,'padding-top'));
		currentBottom += paddingBottom + paddingTop;
	}
	
	var bottomMargin = tersus.isNumber(this.sharedProperties.adjustBottomMargin) ? parseInt(this.sharedProperties.adjustBottomMargin) :0;
	var delta = height-currentBottom - bottomMargin;
	
	if (delta+currentHeight >= this.minHeight)
		this.adjustHeight(delta);
};
Pane.prototype.adjustHeight = function(delta)
{
	tersus.adjustHeight(this.viewNode, delta);
};

Pane.prototype.setProperties = function()
{
	DisplayNode.prototype.setProperties.call(this);
	var pane = this;
	if (this.sharedProperties.adjustBottom == true)
		tersus.async.exec(function() {pane.onResize();}, false /* don't wait */);
};
Pane.prototype.onResize = function()
{
	if (this.sharedProperties.adjustBottom == true)
		this.adjustBottom();
	DisplayNode.prototype.onResize.call(this);
};
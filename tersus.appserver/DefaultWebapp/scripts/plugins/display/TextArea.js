/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.TextArea = function tersus_TextArea() {};
tersus.TextArea.prototype = new tersus.GenericField();
tersus.TextArea.prototype.constructor = tersus.TextArea;
tersus.TextArea.prototype.inputFieldTag = 'textarea';
TextArea=tersus.TextArea; // compatibility with very old models


/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Checkbox = function tersus_Checkbox () {};
tersus.Checkbox.prototype = new tersus.PushButton();
tersus.Checkbox.prototype.constructor = tersus.Checkbox;
tersus.Checkbox.prototype.buttonType='checkbox';
// <Value> property: for backwards compatability
tersus.Checkbox.prototype['get<Value>'] = tersus.PushButton.prototype['get<Checked>'];
tersus.Checkbox.prototype['set<Value>'] = tersus.PushButton.prototype['set<Checked>'];
tersus.Checkbox.prototype['remove<Value>'] = tersus.PushButton.prototype['remove<Checked>'];
tersus.Checkbox.prototype.labelClicked = function(domNode, callback)
{
	if (this.button && ! this.button.disabled)
	{
		this.button.checked = ! this.button.checked;
		this.onChange(domNode, callback);
	}
}

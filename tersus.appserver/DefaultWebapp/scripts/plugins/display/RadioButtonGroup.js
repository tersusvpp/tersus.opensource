/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.RadioButtonGroup = function tersus_RadioButtonGroup(){};
tersus.RadioButtonGroup.prototype = new tersus.DisplayGroup();
tersus.RadioButtonGroup.prototype.constructor = tersus.DisplayGroup;
tersus.RadioButtonGroup.prototype.isRadioButtonGroup = true;
tersus.RadioButtonGroup.prototype['get<Value>'] = function()
{
	if (! this.selectedButton)
		return null;
	else
		return this.selectedButton.getButtonValue();
};
tersus.RadioButtonGroup.prototype.setSelectedValue = tersus.RadioButtonGroup.prototype['set<Value>'] = function(value)
{
	var found = false;
	var op = function(node)
	{
		if (node.isRadioButton &&  compareValues(node.getButtonValue(), value)	)
		{	
			node.setChecked(true);
			found = true;
			return true; // stop traversal
		}
	};
	tersus.traverseHierarchy(this, DisplayNode.displayChildrenIterator, op);
	if ( ! found && tersus.settings.debug)
		modelExecutionError("Tried to set radio button group <Value> as '"+value+"', but the group contains no radio button with this value",this);
};
tersus.RadioButtonGroup.prototype['remove<Value>']= function()
{
	tersus.traverseHierarchy(this, DisplayNode.displayChildrenIterator, function (node){if (node.isRadioButton) node.setChecked(false);return true;});
};
tersus.RadioButtonGroup.prototype.setSelectedValue.requiresNodes = true;

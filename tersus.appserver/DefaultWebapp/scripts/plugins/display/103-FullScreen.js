tersus.FullScreen = tersus.HTMLTag.__subclass('FullScreen');
tersus.FullScreen.prototype.onResize = function onResize()
{
	var d = this.currentWindow.document;
	var s = getViewportSize(d);
/*	if (this.lastSize && this.lastSize.height == s.height && this.lastSize.width == s.width)
		return; // no change in size - nothing to do
	this.lastSize = s;*/

	var top = this.viewNode;
	var children = top.childNodes;
	var body;
	for (var i=0;i<children.length;i++)
	{
		var c=children[i];
		var cn = c.className;
		if (cn.indexOf('body')>=0)
			body = c;	
	}
	if (body)
	{
		var delta = top.offsetHeight - body.clientHeight;
		var height = s.height;	 //client height of body

		//Because clientHeight includes padding, but style.height doesn't include padding, we need to add padding to delta
		delta += getMeasure(body,'padding-bottom');
		delta += getMeasure(body,'padding-top');
		
		
		//Similarly, we need to remove body padding from the available height
		height -= parseInt(getActualStyle(d.body,'margin-bottom'));
		height -= parseInt(getActualStyle(d.body,'margin-top'));
		var newHeight = height-delta;
		if (newHeight>0 && newHeight < 3000)
		{
			body.style.height = newHeight+'px';
		}
	}
	this.onResizeChildren();
};

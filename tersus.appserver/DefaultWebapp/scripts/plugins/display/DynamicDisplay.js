/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.DynamicDisplay = function tersus_DynamicDisplay() {};
tersus.DynamicDisplay.prototype = new DisplayNode();
tersus.DynamicDisplay.prototype.constructor = tersus.DynamicDisplay;
tersus.DynamicDisplay.prototype.defaultTag = 'span';
tersus.DynamicDisplay.prototype.content = null;
tersus.DynamicDisplay.prototype.initViewNode = function()
{
	this.viewNode.style.display='none';
};

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.DateField = function tersus_DateField() {};
tersus.DateField.prototype = new tersus.GenericField();
tersus.DateField.prototype.constructor = tersus.DateField;
tersus.DateField.prototype.isEditable = true;
tersus.DateField.popCalendar = function()
{
	var documentNode = this;
	var node = DisplayNode.findNodeForDocumentNode(documentNode);
	if (!node)
		return;
	var valueHandler = function valueHandler(year, month, day)
	{
		$(node.inputField).trigger('change');
	};
	node.currentWindow.popUpCalendar(this, this, valueHandler, DATE_FORMAT);
};
tersus.DateField.prototype.initWritable = function ()
{
	tersus.GenericField.prototype.initWritable.call(this);
	this.inputField.id = this.getNodeId();
	this.inputField.onclick = tersus.DateField.popCalendar;
	if (!tersus.isNumber(this.sharedProperties.size))
		this.inputField.size = 10;
};

tersus.DateField.prototype.onChange = function(domNode, callback)
{
	tersus.GenericField.prototype.onChange.call(this, domNode, callback);
	hideCalendar();
};
tersus.DateInputField = DateInputField = tersus.DateField; // Backward compatability


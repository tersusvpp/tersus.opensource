/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 
tersus.MenuButton = function tersus_MenuButton() {};
tersus.MenuButton.prototype = new DisplayNode();
tersus.MenuButton.prototype.constructor = tersus.MenuButton;
tersus.MenuButton.prototype.defaultTag = 'button';
tersus.MenuButton.prototype.defaultStyleClass='menuButton';
tersus.MenuButton.prototype['set<Value>'] = function (value)
{
	this.value = value;
};
tersus.MenuButton.prototype['set<Value>'].requiresNodes = true;
tersus.MenuButton.prototype.createViewNode = function Button_createViewNode()
{
	DisplayNode.prototype.createViewNode.call(this);
	this.viewNode.innerHTML = tersus.escapeHTML(this.getCaption());
};
tersus.MenuButton.prototype.captionChanged = function()
{
	if (this.viewNode)
		this.viewNode.innerHTML = tersus.escapeHTML(this.getCaption());
};
tersus.MenuButton.prototype.setEventHandlers = function Button_setEventHandlers()
{
	this.viewNode.setAttribute('lid', this.getNodeId());
	this.viewNode.onclick = tersus.MenuButton.onclick;
	this.viewNode.onmousedown = tersus.MenuButton.mousedown;
	this.viewNode.onblur = tersus.MenuButton.blur;
	this.viewNode.onkeydown = tersus.MenuButton.keydown;
	this.viewNode.onkeyup = tersus.MenuButton.keyup;
};
tersus.MenuButton.prototype.getOptionText = tersus.GenericField.prototype.getOptionText;
tersus.MenuButton.onclick = function()
{
	var node = DisplayNode.findNodeForDocumentNode(this);
	if (node && ! node.isActive)
	{
		node.activate();
	}
};
tersus.MenuButton.keydown = function(evt)
{
	var node = DisplayNode.findNodeForDocumentNode(this);
	if (!node || ! node.isActive)
		return true;
	if (!evt) evt = window.event;
	if (!evt) return;
	var k=evt.keyCode;
	var option = node.highlightedOption;
	switch(k)
	{
		case 27: //Escape
		case 9: //Tab
			node.deactivate();break;
		case 37: case 38: // Left/Up
			node.highlight(option == null? -1:option-1);
			break; // prevent scrolling
		case 39: case 40: // Right/Down
			node.highlight(option == null?0:option+1);
			break;
		default:
	}
};
tersus.MenuButton.keyup = function(evt)
{
	var node = DisplayNode.findNodeForDocumentNode(this);
	if (!node || ! node.isActive)
	{
		return true;
	}
	if (!evt) evt = window.event;
	if (!evt) 
	{
		return;
	}
	var k=evt.keyCode;
	if (k == 13 && node.highlightedOption != null)
		node.select();
/*	if( k ==38 || k ==40) // Up|Down
	{
		var button = node.viewNode;
		node.dontDeactivate = true;
		button.blur();
		setTimeout(function(){button.focus();node.dontDeactivate=false;},10);
	}*/
};
tersus.MenuButton.highlight = function()
{
	var nodeId = this.getAttribute('nodeId');
	if (!nodeId)
		return;
	var node = tersus.findNode(nodeId);
	if (! node)
		return;
	node.mouseIn = true;
	node.highlight(this.getAttribute('optionIndex'));
};
tersus.MenuButton.unhighlight = function()
{
	this.className = '';
	var nodeId = this.getAttribute('nodeId');
	if (!nodeId)
		return;
	var node = tersus.findNode(nodeId);
	if (! node)
		return;
	node.mouseIn = false;
	node.clearHighlight();
};
tersus.MenuButton.select = function()
{
	var nodeId = this.getAttribute('nodeId');
	if (!nodeId)
		return;
	var node = tersus.findNode(nodeId);
	if (node)
	    node.select();
};
tersus.MenuButton.prototype.highlight = function(index)
{
	if (index <0)
		index = this.options.length -1;
	if (index >=this.options.length)
		index = 0;
	this.clearHighlight();
	this.highlightedOption = index;
	this.optionRows[this.highlightedOption].className = 'highlight';
};
tersus.MenuButton.prototype.clearHighlight = function()
{
	if (this.highlightedOption != null)
	{
		this.optionRows[this.highlightedOption].className = '';
		this.highlightedOption = null;
	}
};
tersus.MenuButton.prototype.select = function ()
{
	if (checkBusy())
		return;
	this.deactivate();
	this.value = this.options[this.highlightedOption];
	this.queueEvent(Events.ON_SELECT);
};
tersus.MenuButton.mousedown = function()
{
	var node = DisplayNode.findNodeForDocumentNode(this);
	if (node && node.activate)
		node.activate();
};
tersus.MenuButton.blur = function()
{
	var node = DisplayNode.findNodeForDocumentNode(this);
	if (node && node.isActive && !node.mouseIn)
		setTimeout(function(){	node.deactivate();},50);
};
tersus.MenuButton.prototype.deactivate = function ()
{
	var t = this.optionsTable;
	if (t && t.parentNode)
		t.parentNode.removeChild(t);
	this.optionsTable = null;
	this.isActive = false;
	if (window.isWebKit)
	{
		tersus.MenuButton.currentNodeId = null;
		var w=this.currentWindow;
		w.document.removeEventListener('click',tersus.MenuButton.deactivator, false);
	}
};
tersus.MenuButton.deactivator = function()
{
	var node = tersus.findNode(tersus.MenuButton.currentNodeId);
	if (!node)
		return;
	if (node && ! node.mouseId)
		node.deactivate();
};
tersus.MenuButton.prototype.activate = function ()
{
	if (this.isActive)
		return;
	if (checkBusy())
		return;
	this.isActive = true;
	if (window.isWebKit)
	{
		var current = tersus.findNode(tersus.MenuButton.currentNodeId);
		if (current)
			current.deactivate();
		var w = this.currentWindow;
		tersus.MenuButton.currentNodeId = this.getNodeId();
		setTimeout(function()
		{
			w.document.addEventListener('mouseup',  tersus.MenuButton.deactivator,false);
		},50);
	}
	this.highlightedOption = null;
	this.mouseIn = false;
	var t = this.optionsTable = this.createHTMLElement('table');
	t.className='options';
	var tb = this.optionsTbody = this.createHTMLChildElement(t,'tbody');
	this.optionRows = [];
	for (var i=0; i<this.options.length ;i++)
	{
		var row = this.createHTMLChildElement(tb,'tr');
		row.onmouseOver = tersus.MenuButton.highlight;
		row.onmouseOut = tersus.MenuButton.unhighligh;
		row.setAttribute('optionIndex', i);
		row.onmouseover = tersus.MenuButton.highlight;
		row.onmouseout = tersus.MenuButton.unhighlight;
		row.onmouseup =  tersus.MenuButton.select;
		row.setAttribute('nodeId',this.getNodeId());
		var cell = this.createHTMLChildElement(row,'td');
		cell.innerHTML = this.translate(tersus.escapeHTML(this.getOptionText(this.options[i])));
		this.optionRows[i] = row;
		
	}
	var backgroundColor = getActualBackground(this.viewNode);
	if (backgroundColor == null) backgroundColor = 'white';
	t.style.backgroundColor=backgroundColor;
	
	t.style.position='absolute';
	$(t).css('z-index',++ tersus.topZindex)
	var d = this.viewNode.ownerDocument;
	d.body.appendChild(t);
	d.body.appendChild(t);
	var buttonPosition=getScreenPosition(this.viewNode);
	var x = buttonPosition.left;
	var y = (buttonPosition.top+this.viewNode.offsetHeight-1);
    t.style.top= (y - getScroll(t).top)+'px';
    t.style.left = (x - getScroll(t).left)+'px';
};

tersus.MenuButton.prototype.addOption = tersus.MenuButton.prototype['add<Options>'] = function tersus_MenuButton_addOption(value)
{
	if (!this.options) this.options=[];
	this.options.push(value);
};
tersus.MenuButton.prototype.addOption.requiresNodes = true;

tersus.MenuButton.prototype.getOptions = tersus.MenuButton.prototype['get<Options>'] = function tersus_MenuButton_getOptions()
{
	if (!this.options) this.options=[];
	return this.options;
};
tersus.MenuButton.prototype.removeOptions = tersus.MenuButton.prototype['remove<Options>'] = function tersus_MenuButton_removeOptions()
{
	this.options = [];
};
tersus.MenuButton.prototype['get<Disabled>'] = function ()
{
	return this.disabled;
};

tersus.MenuButton.prototype['set<Disabled>'] = function (disabled)
{
	this.disabled = disabled?true:false;
	if (this.viewNode)
	{
		this.viewNode.disabled=this.disabled;
		if (window.browser == 'IE') // IE 6.0 doesn't support the :disable CSS pseudoclass, so we use an explicit style as a workaround
		{
			if (disabled && !this.viewNode.className)
				this.viewNode.className = 'disabled';
			if (!disabled && this.viewNode.className=='disabled')
				this.viewNode.className = '';
		}
	}
};

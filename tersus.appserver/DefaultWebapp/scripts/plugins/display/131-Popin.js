/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.PopIn = function tersus_PopIn(){};
tersus.PopIn.prototype = new Popup();
tersus.PopIn.prototype.constructor = tersus.PopIn;
tersus.PopIn.prototype.isPopIn = true;
tersus.PopIn.prototype.templateURL='popin.html';
tersus.PopIn.prototype.onload = function()
{
	Popup.prototype.onload.call(this);
	if (this.sprop('templateURL'))
		this.templateURL = this.sprop('templateURL');
	tersus.repository.loadHTMLTemplate(this.templateURL);
};

tersus.PopIn.prototype.create = function () {
	tersus.notify('','Pop-in created');
}
tersus.PopIn.prototype.getCurrentRootViewNode = function()
{
	var root;
	var doc = this.currentWindow.document;
	root = doc.getElementById('root');
	if (! root)
		root = doc.getElementById('tersus.content');
	return root;
};
tersus.PopIn.prototype.setTitle = tersus.PopIn.prototype.captionChanged = function()
{
    if (this.header)
        this.header.innerHTML = tersus.escapeHTML(this.getCaption());
};
tersus.PopIn.prototype.createViewNode = function()
{
	var w = this.currentWindow;
	var doc = w.document;
	clearSelection(w);
	this.originalRoot = this.getCurrentRootViewNode();
	this.originalRootDisplayNode = w.currentRootDisplayNode;
	w.currentRootDisplayNode = this;
	var template = tersus.repository.getHTMLTemplate(this.templateURL);
	var newRoot = importNode(doc,template);
	newRoot.id = 'root';
	this.originalRoot.parentNode.replaceChild(newRoot, this.originalRoot);
	this.originalBodyStyle = doc.body.className;
	if (this.originalBodyStyle == '')
		doc.body.className = 'popin';
	else if (! this.originalBodyStyle.match(/popin/))
		doc.body.className='popin '+this.originalBodyStyle;
	this.setViewNode();
};
tersus.PopIn.prototype.disableNavigation = function()
{
	this.previousFixedURL  = tersus.history.fixedURL;
	if (this.sharedProperties.disableNavigation != false)
	{
		tersus.history.fixedURL = window.location.href;
	}
	else
		tersus.history.fixedURL = null;


};

tersus.PopIn.prototype.resumeNavigation = function()
{
	tersus.history.fixedURL = this.previousFixedURL;
};

tersus.PopIn.prototype.doClose = function()
{
	delete tersus.popupNodes[this.getNodeId()];

	if (this.isOpen)
	{
		this.resumeNavigation();
		var doc = this.currentWindow.document;
		var currentRoot = doc.getElementById('root');
		currentRoot.parentNode.replaceChild(this.originalRoot, currentRoot);
		doc.body.className=this.originalBodyStyle;
		this.currentWindow.currentRootDisplayNode = this.originalRootDisplayNode;
		this.viewNode = null;
		this.footer = null;
		this.header = null;

	}
	this.isOpen = false;
};

tersus.PopIn.prototype.start = function()
{
	tersus.popupNodes[this.getNodeId()]=this;
	this.disableNavigation();
	this.createViewNode();
	this.initViewNode();
	if (!this.initElement)
		this.createChildren();
	this.isOpen = true;
	FlowNode.prototype.start.call(this);
	var node = this;
	if (tersus.isIPhone)
		window.scrollTo(0,1);
	var activeElement = this.currentWindow.document.activeElement;

	// Remove focus to prevent selection from "smearing" into popin.  Do not blur BODY as in IE7 it causes the browser window to be pushed back
	if (activeElement && activeElement.blur && activeElement.tagName != 'BODY')
		activeElement.blur();
	this.currentWindow.setTimeout(function(){node.focusOnFirstField();}, 100);

};
tersus.PopIn.prototype.findById = function tersus_popIn_FindById(id, alreadyScanned)
{
	//Traverse the popup only if its window is open
	if (this.isOpen)
		return tersus.Node.prototype.findById.call(this,id, alreadyScanned);
	else
		return null;
};

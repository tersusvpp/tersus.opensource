/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 
function SplitPane() {};
SplitPane.prototype = new Pane();
SplitPane.prototype.constructor = SplitPane;
SplitPane.prototype.defaultTag = 'div';
SplitPane.prototype.defaultStyleClass = 'splitPane';
SplitPane.prototype.height=400;
SplitPane.prototype.minWidth=50;
SplitPane.prototype.leftWidth = 200;
SplitPane.prototype.borderWidth=1;
SplitPane.prototype.paddingWidth=5;
SplitPane.prototype.dividerThickness=5;
SplitPane.prototype.contentPaneStyle = "splitPaneContent";
SplitPane.prototype.dividerStyle = "splitPaneDivider";
SplitPane.prototype.setDimensions = function()
{
	if (this.sharedProperties.height)
		this.height =parseInt(this.sharedProperties.height);
	this.setHeight(this.height);
	this.divider.style.width = this.dividerThickness + 'px';
	if (this.sharedProperties.leftPaneWidth)
	{
		this.leftWidth=parseInt(this.sharedProperties.leftPaneWidth);
	}
	
};
SplitPane.prototype.setHeight = function(height)
{
	this.height = height;
	this.viewNode.style.height = '';
	this.pane1.style.height = '';
	this.pane2.style.height = '';
	this.viewNode.style.height = this.height+ 'px';
	this.setPaneHeight(this.pane1,this.height);
	this.setPaneHeight(this.pane2,this.height);
	this.divider.style.height = this.height+ 'px';
};
SplitPane.prototype.createViewNode = function createViewNode()
{
	DisplayNode.prototype.createViewNode.call(this);
	if (this.sharedProperties.paddingWidth)
		this.paddingWidth =parseInt(this.sharedProperties.paddingWidth);
	if (this.sharedProperties.borderWidth)
		this.borderWidth =parseInt(this.sharedProperties.borderWidth);
	this.viewNode.style.borderWidth = this.borderWidth;
	
	this.pane1 = this.createHTMLElement('div');
	this.pane2 = this.createHTMLElement('div');
	this.divider = this.createHTMLElement('div');
	this.direction = getActualStyle(this.currentWindow.document.body, 'direction');
	this.setPaneProperties(this.pane1);
	this.setPaneProperties(this.pane2);
	this.divider.className = this.dividerStyle;
	if (this.direction == 'rtl')
		this.divider.className+=' rtl';
	
	this.divider.setAttribute('SplitPaneId', this.getNodeId());
	this.setDimensions();
	this.attachContent();
	var splitPane = this;
	this.divider.onmousedown = SplitPane.beginDrag;
	tersus.async.exec( function() {splitPane.onResize();}, false /* don't wait */);
};

SplitPane.prototype.reset = function reset()
{
	DisplayNode.prototype.reset.call(this);
	this.pane1 = null;
	this.pane2 = null;
	this.viewNode.innerHTML='';
};
SplitPane.prototype.appendChildViewNode = function appendChildViewNode(child)
{
	if (! this.leftPaneContent)
	{
		this.leftPaneContent = child.viewNode;
		this.pane1.appendChild(this.leftPaneContent);
	}
	else if (! this.rightPaneConent)
	{
		this.rightPaneContent = child.viewNode;
		this.pane2.appendChild(this.rightPaneContent);
	}
	else
		modelError('More than 2 child nodes in '+this.modelId + ' (path = '+this.getPath()+')');
	
};
SplitPane.prototype.setPaneProperties = function setPaneProperties(pane, width)
{
		pane.className=this.contentPaneStyle;
		if (this.direction == 'rtl')
			pane.className+=' rtl';
		
		pane.style.padding = this.paddingWidth + 'px';

};
SplitPane.prototype.detachContent = function detachContent()
{
	if (this.contentAttached)
	{
		this.contentAttached = false;
		if (this.pane1.parentNode == this.viewNode)
			this.viewNode.removeChild(this.pane1);
		if (this.divider.parentNode == this.viewNode)
			this.viewNode.removeChild(this.divider);
		if (this.pane2.parentNode == this.viewNode)
			this.viewNode.removeChild(this.pane2);
	}
};
SplitPane.prototype.attachContent = function attachContent()
{
	if (! this.contentAttached)
	{
		this.contentAttached = true;
		this.viewNode.appendChild(this.pane1);
		this.viewNode.appendChild(this.divider);
		this.viewNode.appendChild(this.pane2);
	}
};

SplitPane.prototype.onResize = function onResize()
{
	if (this.contentAttached && ! this.resizing && this.viewNode.offsetWidth > 0)
	{
		this.resizing = true;
		if (this.sharedProperties.height)
			this.setHeight(parseInt(this.sharedProperties.height));
		else
		{
			var node = this.viewNode.parentNode;
			while (node && node.style)
			{
				var parentHeight = node.clientHeight;
				if (parentHeight)
				{
					this.setHeight(parentHeight-2*this.borderWidth);
					break;
				}
				node = node.parentNode;
			}
		}
		var parentWidth = 0;
		
		// Detach content so that the parent width is exactly the available width
		this.detachContent();
		// Clear explicit widths - to be recalculated
		this.viewNode.style.width=null;
		this.pane1.style.width=null;
		this.pane2.style.width = null;
		// Calculating the available width is browser specific
			parentWidth=this.viewNode.offsetWidth;
		if (window.browser == 'Mozilla')
		{
			//In mozilla (as of 2005), we need to set the width property of the parent div in order to make the initial rendering correct
			this.viewNode.style.width=(parentWidth-2*this.borderWidth)+'px';
		}
		
		this.availableWidth = parentWidth - 2*this.borderWidth-this.dividerThickness;
		
		if (this.availableWidth <2*this.minWidth)
		{
			this.leftWidth = Math.round(this.availableWidth/2);
		}
		else if (this.leftWidth + this.minWidth > this.availableWidth)
		{
			this.leftWidth = this.availableWidth - this.minWidth;
		}
		//set explicit widths
		this.rightWidth= this.availableWidth - this.leftWidth-4;
		this.setPaneWidth(this.pane1, this.leftWidth);
		this.setPaneWidth(this.pane2, this.rightWidth);
		if (this.sharedProperties.adjustBottom == true)
		{
			this.adjustBottom();
		};
		//re-attach content
		this.attachContent();
		this.onResizeChildren();
		this.resizing = false;
	}
};

SplitPane.prototype.setPaneWidth = function setPaneWidth(pane,width)
{
		if (window.browser == 'IE' && ieLevel<7) // Traditional box model - width includes padding 
		{
			pane.style.width=width+'px';
		}
		else
		{
			pane.style.width=(width-2*this.paddingWidth)+'px';
		}
};

SplitPane.prototype.setPaneHeight = function setPaneHeight(pane,height)
{
		if (window.browser == 'IE' && ieLevel <7 )
		{
			// In IE, the hight include padding
			pane.style.height=height + 'px';
		}
		else
		{
			pane.style.height=(height -2*this.paddingWidth)+'px';
		}
};
SplitPane.beginDrag = function(e)
{
	var splitPane = tersus.findNode(this.getAttribute('SplitPaneId'));
	splitPane.beginDrag(e);
};
SplitPane.finishDrag = function (e)
{
	var splitPane=tersus.findNode(this.documentElement.getAttribute('SplitPaneId'));
	this.onmouseup = null;
	this.onmousemove = null;
	splitPane.viewNode.className=splitPane.baseClassName;
	
};
SplitPane.performDrag = function (e)
{
	var splitPane=tersus.findNode(this.documentElement.getAttribute('SplitPaneId'));
	var win = splitPane.currentWindow;
	var currentPosition = getEventPosition(win,e);
	var delta = {};
	delta.x = currentPosition.x - splitPane.lastPosition.x;
	delta.y = currentPosition.y - splitPane.lastPosition.y;
	splitPane.lastPosition = currentPosition;		
	splitPane.resize(delta);
};
SplitPane.prototype.beginDrag = function beginDrag(e)
{
	var splitPane = this;
	var win = this.currentWindow;
	var doc = win.document;
	doc.documentElement.setAttribute('SplitPaneId', this.getNodeId());
	splitPane.lastPosition = getEventPosition(win, e);
	splitPane.viewNode.className+=' resizing';
	doc.onmousemove =SplitPane.performDrag;
	doc.onmouseup=SplitPane.finishDrag;
};

SplitPane.prototype.resize = function resize(delta)
{
	if (this.leftWidth+delta.x >= this.minWidth && this.rightWidth-delta.x >= this.minWidth)
	{
		if (this.direction == 'ltr')
		{
			this.leftWidth += delta.x;
			this.rightWidth -= delta.x;
		}
		else
		{
			this.leftWidth -= delta.x;
			this.rightWidth += delta.x;
		}
			
		if (delta.x >0)
		{
			this.setPaneWidth(this.pane2, this.rightWidth);
			this.setPaneWidth(this.pane1, this.leftWidth);
		}
		else
		{
			this.setPaneWidth(this.pane1, this.leftWidth);
			this.setPaneWidth(this.pane2, this.rightWidth);
		}
		
		this.onResizeChildren();
		
	}
};

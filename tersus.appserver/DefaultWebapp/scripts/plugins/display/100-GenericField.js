/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.GenericField = function tersus_GenericField() {
};
tersus.GenericField.prototype = new DisplayNode();
tersus.GenericField.prototype.constructor = tersus.GenericField;
tersus.GenericField.prototype.currentReadOnly = null;
tersus.GenericField.prototype.value = null;
tersus.GenericField.prototype.isEditable = true;
tersus.GenericField.prototype.inputFieldTag = 'input';
tersus.GenericField.prototype.defaultTag = 'span';
tersus.GenericField.prototype.textTranslation = false;
tersus.GenericField.prototype.emptyOptionText = null;
tersus.GenericField.EMPTY_OPTION_TEXT = '<Empty Option Text>';
tersus.GenericField.prototype.init = function ()
{
	if (this.sharedProperties && tersus.isNumber(this.sharedProperties.maxLength))
    	this.maxLength = this.sharedProperties.maxLength;
};
tersus.GenericField.prototype.addGetter(tersus.GenericField.EMPTY_OPTION_TEXT, function()
{
	return this.emptyOptionText;
});
tersus.GenericField.prototype.updateOptions = function()
{
 	if (this.inputField && this.isChooser)
	{
		this.inputField.innerHTML = '';
		for (var i=0; i<this.options.length; i++)
		{
			var option = this.options[i];
			this.addOptionElement(option, i);
		}
	}
 }
tersus.GenericField.prototype.addRemover(tersus.GenericField.EMPTY_OPTION_TEXT, function()
{
	if (this.emptyOptionText != null)
	{
		if (this.options)
			this.options.splice(0,1);//remove null from beginning of list
		this.emptyOptionText = null;
		this.updateOptions();
	}
});
tersus.GenericField.prototype.addSetter(tersus.GenericField.EMPTY_OPTION_TEXT,function(text)
{
	if (this.emptyOptionText == null)
	{
		if (this.options)
			this.options.splice(0,0,null); // add null to beginning of list
		else
			this.options = [null];
	}
	this.emptyOptionText = text;
	this.updateOptions();
});
tersus.GenericField.prototype.addGetter('<Max Length>', function()
{
	return this.maxLength;
});
tersus.GenericField.prototype.addSetter('<Max Length>', function(length)
{
	this.maxLength = length;
	if (this.inputField)
		this.inputField.maxLength = this.maxLength;
	// Reset value in order to truncate if needed
	this.setFieldValue(this.getFieldValue());

});
tersus.GenericField.prototype.reformat = function()
{
	this.setValueString();
};
tersus.GenericField.prototype.formatProperties = {
	currencySymbol:'<Currency Symbol>',
	currencyPosition:'<Currency Position>',
	useThousandsSeparator:'<Use Thousands Separator>',
	decimalPlaces:'<Decimal Places>'
};
tersus.GenericField.prototype.createFormatProps = function()
{
	this.formatProps = {};
	for (var p in this.formatProperties)
	{
		this.formatProps[p]=this.sprop(p);
	}
};

tersus.GenericField.addFormatAccessor  = function (pt, prop, elementName)
{
	pt.addSetter(elementName, function(v)
	{
		if (! this.formatProps)
			this.createFormatProps();
		this.formatProps[prop] = v;
		this.reformat();
	});
	pt.addGetter(elementName, function()
	{
		if (this.formatProps)
			return this.formatProps[prop];
		else
			return this.sprop(prop);
	});
};
for (var p in tersus.GenericField.prototype.formatProperties)
{
	tersus.GenericField.addFormatAccessor(tersus.GenericField.prototype, p,tersus.GenericField.prototype.formatProperties[p] );
};

tersus.GenericField.prototype.onClick  = function (domNode, callback)
{
	if (domNode == this.browseButton)
		this.queueEvent(Events.ON_BROWSE, null, null, callback);
	else if (this.currentReadOnly)
		DisplayNode.prototype.onClick.call(this,domNode, callback);
	if (this.hyperlink)
		tersus.preventDefaultEventHandling();
};
tersus.GenericField.prototype['remove<Value>'] = function removeValue()
{
	this.value = null;
	if (this.viewNode)
	{
		if (this.inputField)
		{
			if (this.isChooser)
			{
				this.setSelectedIndex(0);
				if (this.options.length>0)
					this.value = this.options[0];
			}
			else
				this.inputField.value = '';
		}
		else if (this.hyperlink)
			this.hyperlink.innerHTML='';
		else
			this.viewNode.innerHTML = '';

	}
};

tersus.GenericField.prototype.updateSelfReadOnly = function tersus_GenericField_updateSelfReadOnly(readOnly)
{
	if (this.currentReadOnly == readOnly)
		return; // no change
	this.currentReadOnly = readOnly;
	if (this.inputField)
	{
		/* When we switch to read only mode, we get the current value from the input field, with the exception
		  of a chooser whose value doesn't match the options (selectedIndex<0) */
		var update = true;
		if (this.isChooser)
		{
			if (this.tmpSelectedIndex != null)
				update = this.tmpSelectedIndex != -1; // IE workaround
			else
				update = this.inputField.selectedIndex != -1;
		}
   		if (update)
       		this.value = this.getFieldValue();
    }
	if (this.viewNode)
	{
		this.viewNode.innerHTML = '';
		if (readOnly)
		{
			if (this.hasEventHandler(Events.ON_CLICK))
				this.initLink()
			else
				this.initDisplay()
		}
		else
			this.initWritable();
	}
	this.setValueString();
};

tersus.GenericField.prototype.createViewNode = function tersus_GenericField_createViewNode()
{
	if (this.setDefaultProperties)
		this.setDefaultProperties();

    if (this.sharedProperties.inputFieldTag && this.sharedProperties.inputFieldTag != ' ')
    	this.inputFieldTag = this.sharedProperties.inputFieldTag;
    var optionsElement = this.getElement('<Options>');
    if (optionsElement)
    {
    	this.optionsElement = optionsElement;
    	this.isChooser = true;
    	this.options = [];
    	this.inputFieldTag = 'select';
    }
    DisplayNode.prototype.createViewNode.call(this);
};
tersus.GenericField.prototype.initViewNode = function tersus_GenericField_initViewNode()
{
    DisplayNode.prototype.initViewNode.call(this);
    this.updateSelfReadOnly(this.computedReadOnly());
   	if (this.hasEventHandler(Events.ON_CLICK))
   		this.registerEventHandler(Events.ON_CLICK,this.viewNode);

};
tersus.GenericField.prototype.attachHTMLNode = function (e)
{
	this.viewNode = e;
	if (e.firstChild && e.firstChild.tagName == 'INPUT')
		this.inputField = e.firstChild;
   	this.registerEventHandler(Events.ON_CHANGE,this.inputField);
	this.registerEventHandler(Events.ON_FOCUS,this.inputField);
   	if (this.hasEventHandler(Events.ON_BLUR))
	   	this.registerEventHandler(Events.ON_BLUR,this.inputField);
   	if (this.hasEventHandler(Events.ON_KEYPRESS))
	   	this.registerEventHandler(Events.ON_KEYPRESS,this.inputField);
   	if (this.hasEventHandler(Events.ON_KEYDOWN))
	   	this.registerEventHandler(Events.ON_KEYDOWN,this.inputField);
   	if (this.hasEventHandler(Events.ON_KEYUP))
	   	this.registerEventHandler(Events.ON_KEYUP,this.inputField);
   	if (this.hasEventHandler(Events.ON_CLICK))
	   	this.registerEventHandler(Events.ON_CLICK,this.hyperlink);

}

tersus.GenericField.prototype.setViewNode = function tersus_GenericField_setViewNode(viewNode)
{
	if (viewNode.tagName == 'INPUT')
	{
		this.viewNode = this.createHTMLElement(this.getTag());
		viewNode.parentNode.replaceChild(this.viewNode, viewNode);
		this.viewNode.appendChild(viewNode);
		this.inputField = viewNode;
	}
	else
		this.viewNode = viewNode;
};
tersus.GenericField.nodesToAdjust = [];
tersus.GenericField.prototype.disabled = false;
tersus.GenericField.prototype.initWritable = function tersus_GenericField_initWritable()
{
	tersus.removeEventHandlers(this.viewNode);
    this.inputField = this.createHTMLElement(this.inputFieldTag);
    if (!this.isEditable)
    {
    	this.inputField.readOnly = true;
    }
    this.inputField.disabled = this.disabled;
    this.hyperlink = null;
    if (! this.isChooser && tersus.isNumber(this.sharedProperties.size))
    	this.inputField.size = this.sharedProperties.size;
    if (this.maxLength != null)
    	this.inputField.maxLength = this.maxLength;
    if (this.sharedProperties.width != undefined && this.sharedProperties.width != ' ')
    	this.inputField.style.width = this.sharedProperties.width;
    if (this.sharedProperties.styleClass && this.sharedProperties.styleClass != ' ')
	    this.inputField.className = this.sharedProperties.styleClass;
    if (this.sharedProperties.height && this.sharedProperties.height != ' ')
	    this.inputField.style.height = this.sharedProperties.height;
    var rows = this.sharedProperties.numberOfRows;
	if (rows && tersus.isNumber(rows))
		this.inputField.rows = parseInt(rows);
	var cols = this.sharedProperties.numberOfColumns;
	if (cols && tersus.isNumber(cols))
		this.inputField.cols = cols;
	if (this.isChooser)
	{
		this.inputField.setAttribute('data-role','none');
		for (var i=0; i<this.options.length; i++)
		{
			var option = this.options[i];
			this.addOptionElement(option, i);
		}
		if (this.sharedProperties.size && this.sharedProperties.size != ' ')
			this.inputField.style.width = (2.5+0.57*this.sharedProperties.size) + "em"; // Trying to emulate the 'size' property of text input
		else if (this.sharedProperties.width && this.sharedProperties.width != ' ')
			this.inputField.style.width = this.sharedProperties.width;

	};

    this.viewNode.innerHTML = '';
    this.viewNode.appendChild(this.inputField);
    if (this.hasEventHandler(Events.ON_BROWSE))
    {
	    this.viewNode.style.whiteSpace='nowrap';
		this.viewNode.style.position='relative';
    	this.inputField.className = 'pf_txt';

    	this.browseButton = this.createHTMLElement('button','pf_browse');
    	this.browseButton.innerHTML = '<img src="images/spacer.gif"/>';
	    this.browseButton.type='button';
    	this.browseButton.id = this.getNodeId();
    	this.registerEventHandler(Events.ON_CLICK, this.browseButton);
	    this.viewNode.appendChild(this.browseButton);
	    var adjustList = tersus.GenericField.nodesToAdjust;
	    if (adjustList.length == 0)
		    setTimeout(tersus.GenericField.adjustPositions,10);
		adjustList.push(this);
	    //Adjust the position of the button based on the input field
    }
    this.inputField.setAttribute('node_id',this.getNodeId());
   	this.registerEventHandler(Events.ON_CHANGE,this.inputField);
	this.registerEventHandler(Events.ON_FOCUS,this.inputField);
   	if (this.hasEventHandler(Events.ON_BLUR))
	   	this.registerEventHandler(Events.ON_BLUR,this.inputField);
   	if (this.hasEventHandler(Events.ON_KEYPRESS))
	   	this.registerEventHandler(Events.ON_KEYPRESS,this.inputField);
   	if (this.hasEventHandler(Events.ON_KEYDOWN))
	   	this.registerEventHandler(Events.ON_KEYDOWN,this.inputField);
   	if (this.hasEventHandler(Events.ON_KEYUP))
	   	this.registerEventHandler(Events.ON_KEYUP,this.inputField);
};

tersus.GenericField.prototype.onChange = function(domNode, callback)
{
	this.validateFieldValue();
	DisplayNode.prototype.onChange.call(this, domNode, callback);
};
tersus.GenericField.prototype.onResize = function()
{
	if (this.browseButton)
	{
	    var adjustList = tersus.GenericField.nodesToAdjust;
	    this.browseButton.style.top='';
	    this.browseButton.style.left='';
	    if (adjustList.length == 0)
		    setTimeout(tersus.GenericField.adjustPositions,10);
		adjustList.push(this);
	}
};
tersus.GenericField.adjustPositions = function()
{
	var list = tersus.GenericField.nodesToAdjust;
	tersus.GenericField.nodesToAdjust = [];
	for (var i=0; i<list.length; i++)
	{
		var node = list[i];
		node.adjustPosition();
	}
};
/**
 * adjustPosition: align the browse button with the input field
 */
tersus.GenericField.prototype.adjustPosition = function()
{
	if (!this.inputField)
		return; // The field may have become read-only
	var direction = getActualStyle(this.inputField,'direction');
	var $b = $(this.browseButton);
	var $i= $(this.inputField);
	$b.height($i.innerHeight()).css('left','0px').css('top','0px');
	var inputOffset = getOffsets(this.inputField);
	var inputTop=inputOffset.top;
	var buttonOffset = getOffsets(this.browseButton);
	var buttonTop = buttonOffset.top;
	var deltaTop = inputTop - buttonTop;
	this.browseButton.style.top = deltaTop+'px';
	if (direction == 'rtl')
	{
		var inputLeft = inputOffset.left;
		var buttonRight = buttonOffset.left + this.browseButton.offsetWidth-2;
		var deltaLeft = inputLeft - buttonRight;
		this.browseButton.style.borderRightWidth='0';
		this.browseButton.style.borderLeftWidth='2px';

	}
	else
	{
		var inputRight=inputOffset.left+this.inputField.offsetWidth -2; //Don't know why, but we need to substract 2
		var buttonLeft = buttonOffset.left;
		var deltaLeft = inputRight - buttonLeft;
		this.browseButton.style.borderRightWidth='2px';
		this.browseButton.style.borderLeftWidth='0px';
	}
	this.browseButton.style.left = deltaLeft+'px';
};
tersus.GenericField.prototype.getDefaultContent = function()
{
	return '';
};
tersus.GenericField.prototype.initLink = function tersus_GenericField_initLink()
{
	if (this.inputField)
		this.value = this.getFieldValue();
    this.inputField = null
    this.browseButton = null;
    this.hyperlink = this.createHTMLElement('a');
    var id = this.getNodeId();
    this.hyperlink.href='';
   	this.hyperlink.innerHTML = this.getDefaultContent();
    this.viewNode.appendChild(this.hyperlink);
};
tersus.GenericField.prototype.initDisplay = function tersus_GenericField_initDisplay()
{
	this.hyperlink = null;
	this.inputField = null;
	this.browseButton = null;
	DisplayNode.prototype.setEventHandlers.call(this);
};
tersus.GenericField.prototype.getFieldValue = tersus.GenericField.prototype['get<Value>'] = function tersus_GenericField_getFieldValue()
{
	if (this.inputField)
	{
		if (this.isChooser)
			this.value = this.getSelectedOption();
		else
		{
			var valueElement = this.getElement('<Value>');
			var valuePrototype = valueElement.getChildConstructor().prototype;
			this.value = valuePrototype.parseInput(this.inputField.value, this.formatProps ? this.formatProps : this.sharedProperties);
		}
	}
	return this.value;
};
tersus.GenericField.prototype.setFieldValue = tersus.GenericField.prototype['set<Value>'] = function tersus_GenericField_setFieldValue(value)
{
	this.value = value;
	// Truncate to maxLength if needed (only if this is a text field)
	if (this.maxLength && value && value.isText && value.leafValue && value.leafValue.length > this.maxLength)
	{
		this.value = new value.constructor();
		this.value.leafValue =  value.leafValue.substring(0,this.maxLength);
	}

	if (this.isChooser && ! this.currentReadOnly)
		this.setSelectedIndex(this.findIndex(this.value));
	else
		this.setValueString();
};
tersus.GenericField.prototype.setValueString = function()
{
	this.valueStr = '';
	if (this.value != null)
	{
		if (this.isChooser)
			this.valueStr = this.getOptionText(this.value);
		else if (this.inputField)
			this.valueStr = this.value.formatInput(this.formatProps ? this.formatProps : this.sharedProperties);
		else
			this.valueStr = this.value.formatString(this.formatProps ? this.formatProps : this.sharedProperties);
	}
	if (this.inputField)
	{
		if (!this.isChooser) // For chooser we've already set the selected index
			this.inputField.value = this.valueStr;
	}
	else if (this.hyperlink)
		this.hyperlink.innerHTML = this.escapeHTML(this.translate(this.valueStr));
	else if (this.viewNode)
		this.viewNode.innerHTML =  this.escapeHTML(this.translate(this.valueStr));
};
tersus.GenericField.prototype.setValueStringOpt = function()
{
	this.valueStr = '';
	if (this.value != null)
	{
		if (this.isChooser)
			this.valueStr = this.getOptionText(this.value);
		else if (this.optimizeFormatString)
			this.valueStr = this.value.leafValue;
		else if (this.inputField && this.valueFormatInput)
			this.valueStr = this.value.formatInput(this.sharedProperties);
		else if (this.value.formatString)
			this.valueStr = this.value.formatString(this.sharedProperties);
		else
			this.valueStr = this.value.toString();
	}
	if (this.inputField)
	{
		this.inputField.value = this.valueStr;
	}
	else if (this.hyperlink)
		this.hyperlink.innerHTML = this.escapeHTML(this.translate(this.valueStr));
	else if (this.viewNode)
		this.viewNode.innerHTML =  this.escapeHTML(this.translate(this.valueStr));
};
tersus.GenericField.prototype.setFieldValue.requiresNodes = true;
tersus.GenericField.prototype.validateFieldValue = function()
{
	var valueElement = this.getElement('<Value>');
	var valuePrototype = valueElement.getChildConstructor().prototype;
	if (valuePrototype.validateInput)
	{
		var errorMessage =  valuePrototype.validateInput(this.inputField.value, this.formatProps ? this.formatProps : this.sharedProperties);
		this.setValidationMessage(errorMessage);
	}
};
tersus.GenericField.prototype.setEventHandlers = function ()
{
};
tersus.GenericField.prototype.showValidationMessage = function (message)
{
	var targetNode = this.viewNode;
	if (this.inputField)
		targetNode = this.inputField;
	if (this.currentWindow.alert)
    this.currentWindow.alert(message);
	if (targetNode.focus && !targetNode.disabled)
		targetNode.focus();
	if (targetNode.select)
		targetNode.select();
};

tersus.GenericField.prototype.tmpSelectedIndex = null;
tersus.GenericField.prototype.getSelectedOption  = function tersus_GenericField_getSelectedOption()
{
	var selectedIndex;
	if (this.tmpSelectedIndex != null) // In IE, the selected index is updated asynchronously, so we use tmpSelectedIndex if it is set
		selectedIndex = this.tmpSelectedIndex;
	else
		selectedIndex = this.inputField.selectedIndex;
	if (selectedIndex < 0)
		return null;
	return this.options[selectedIndex];
};

tersus.GenericField.prototype.setSelectedIndex = function tersus_GenericField_setSelectedIndex(selectedIndex)
{
	if (!this.inputField)
		return;
	if (window.browser == 'IE') // IE needs a delay in order to recognize any newly added option/s
	{
		var inputField = this.inputField;
		var node = this;
		this.tmpSelectedIndex = selectedIndex;
		setTimeout(function(){
		inputField.selectedIndex = selectedIndex;
		node.tmpSelectedIndex = null;}
		, 10);
	}
	else
		this.inputField.selectedIndex = selectedIndex;
};

tersus.GenericField.prototype.getOptions = tersus.GenericField.prototype['get<Options>'] = function tersus_GenericField_getOptions()
{
	if ( !this.options)
		return [];
	if (this.emptyOptionText != null)
	{
		return this.options.slice(1); // Ignoring the first item in the array, which is the empty option (null)
	}
	return this.options;
};
tersus.GenericField.prototype.reset = function()
{
	/* For choosers, the basic behavior sometimes leaves the value set as follows:
	 * When <Empty Option Text> is removed, the empty option is removed as the first on the list of options
	 * Then, clearing <Value> sets it as the first option
	 */
	DisplayNode.prototype.reset.call(this);
	this.value = null;
};
tersus.GenericField.prototype.addOption = tersus.GenericField.prototype['add<Options>'] = function tersus_GenericField_addOption(value)
{
	if (! this.options)
		this.options = [];
	this.options.push(value);
	if (this.inputField)
	{
		this.addOptionElement(value, this.options.length -1 );
	}
};
tersus.GenericField.prototype.addOptionElement = function tersus_GenericField_addOptionElement(value, index)
{
	var optionNode = this.createHTMLElement('option');
	optionNode.innerHTML = this.translate(tersus.escapeHTML(this.getOptionText(value)));
	this.inputField.appendChild(optionNode);
	/* Check if the new option matches the value (which may have been set previously)
	 In case of a match, select the new option */
	if (this.value != null &&  compareValues(this.value, value))
		this.setSelectedIndex(index);
};
tersus.GenericField.prototype.deepCopy = function deepCopy(value)
{
	var selectedIndex = null;
	this.emptyOptionText = value.getLeafChild(tersus.GenericField.EMPTY_OPTION_TEXT);
	this.options = this.emptyOptionText == null ? [] : [null];

	for (var elementRole in value.elements)
	{
		if (elementRole == tersus.GenericField.EMPTY_OPTION_TEXT)
			continue;
		var element = this.getElement(elementRole);
		if (element)
		{
			if (element.isRepetitive)
			{
				var childValues = value.getChildren(elementRole);
				if (elementRole == '<Options>') // Special optimization - setting the options of a large chooser as one chunk of HTML
				{
					var html = [];
					if (this.emptyOptionText != null)
					{
						html.push('<OPTION>');
						html.push(tersus.escapeHTML(this.translate(this.emptyOptionText)));
						html.push('</OPTION>');
					}
					if (childValues.length > 0)
					{
						var optimize_get_text = false;
						var optionText;
						var v0 = childValues[0];
						this.getOptionText(v0);
						var textElement = v0.__textElement;
						if (textElement && textElement.modelId == BuiltinModels.TEXT_ID && ! this.textTranslation)
							optimize_get_text = true;
						for (var i=0; i<childValues.length;i++)
						{
							var v = childValues[i];
							this.options.push(v);
							if (this.inputField)
							{
								var optionText = '';
								html.push('<OPTION>');
								if (optimize_get_text)
								{
									var c = v.children;
									if (c)
									{
										var tr = v.__textElementRole;
										if (tr)
										{
											var tn = c[tr];
											if (tn)
												optionText =this.escapeHTML(tn.leafValue); // Can be further optimized by checking in advance if escapeHTML is required
										}
										else
											optionText = this.escapeHTML(this.getOptionText(v)); // No text element found - reverting to un-optimized
									}
									html.push(optionText);
								}
								else
									html.push(tersus.escapeHTML(this.translate(this.getOptionText(v))));
								html.push('</OPTION>');
								if (this.value != null &&  compareValues(this.value, v))
								{
									selectedIndex = this.options.length-1; // Takes care of emptyOptionText as well
								}
							}
						}
						if (this.inputField)
						{
							var f=this.inputField;
							if (window.browser == 'IE')
							{
								// Don't remember why, but we need to re-recreate the input field in IE
								var oh = f.outerHTML;
								var p = f.parentNode;
								var ps = f.previousSibling;
								var endTag = '</SELECT>';
								f.outerHTML = oh.substring(0,oh.length-endTag.length) + html.join('') + endTag;
								if (ps)
									this.inputField = ps.nextSibling;
								else
									this.inputField = p.firstChild;
								this.attachHTMLNode(this.viewNode); // re-registers event handlers, which are lost when the input field is re-recreated.

							}
							else
								f.innerHTML = html.join('');
						}
					}
					if (this.inputField && selectedIndex != null)
						this.setSelectedIndex(selectedIndex);

				}
				else
					element.setChildren(this, childValues, Operation.REPLACE);
			}
			else
			{
				var childValue = value.getChild(elementRole);
				if (childValue != null)
				{
					element.replaceChild(this, childValue);
				}
			}
		}
	}

};

tersus.GenericField.prototype.addOption.requiresNodes = true;
tersus.GenericField.prototype.removeOptions = tersus.GenericField.prototype['remove<Options>'] = function tersus_GenericField_removeOptions()
{
	if (this.emptyOptionText != null)
		this.options=[null];
	else
		this.options = [];
	this.updateOptions();
};

tersus.GenericField.prototype.findIndex = function tersus_GenericField_findIndex(value)
{
	if (this.options)
		for (var i=0; i<this.options.length; i++)
		{
			var option = this.options[i];
			if (compareValues(option, value))
				return i;
		}
	return -1;
};
tersus.GenericField.prototype.getOptionText = function tersus_GenericField_getOptionText(value)
{
	var outputValue = null;
	if (value && value.elementList)
	{
		var textElementRole = value.__textElementRole;
		if (textElementRole == null)
		{
			for (var i=0; i< value.elementList.length; i++)
			{
				var element = value.elementList[i];
				if (i==0)
				{
					textElementRole = element.role; // If no text element is found, the first element is used
					value.constructor.prototype.__textElement = element;
				}
				if (element.modelId == BuiltinModels.TEXT_ID)
				{
					textElementRole = element.role;
					value.constructor.prototype.__textElement = element;
					break;
				}
			}
		value.constructor.prototype.__textElementRole = textElementRole;
		}
		if (textElementRole && value.children)
			outputValue = value.children[textElementRole];
	}
	else
		outputValue = value;
	var text = '';
	if (outputValue != null)
		text =  outputValue.formatString(this.sharedProperties);
	else if (this.emptyOptionText != null)
		text = this.emptyOptionText;
	return text;
};
tersus.GenericField.prototype['get<Disabled>'] = function ()
{
	return this.disabled;
};

tersus.GenericField.prototype['set<Disabled>'] = function (disabled)
{
	this.disabled = disabled?true:false;
	if (this.inputField)
	{
		this.inputField.disabled=this.disabled;
	}
};
tersus.GenericField.prototype.getClickTarget = function()
{
	if (this.inputField)
		return this.inputField;
	else if (this.hyperlink)
		return this.hyperlink;
	else
		return this.viewNode;
};
tersus.GenericField.prototype.focus = function()
{
	if (this.inputField)
		this.inputField.focus();
	else if (this.viewNode.focus)
		this.viewNode.focus();
};
TextInputField = NumberInputField = tersus.GenericField; //backward compatability

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/

//Popup - opens a pop-up window

function Popup() {};
Popup.prototype = new DisplayNode();
Popup.prototype.constructor = Popup;
Popup.prototype.create = function create_popup()
{
	tersus.notify('','Popup '+this.element.role+' ('+this.modelId+') created');
};
Popup.WAITING_TO_POP_UP = 'Waiting to pop-up';
Popup.POPPED_UP = 'Popped-up';
Popup.prototype.isPopup = true;
Popup.prototype.start = function()
{
	this.popupStatus = Popup.WAITING_TO_POP_UP;
	tersus.popupNodes[this.getNodeId()]=this;
	this.pause(); // Processing will start once the pop up loads
	this.currentWindow = this.createWindow();
	tersus.registerWindow(this.currentWindow);
	if (! this.currentWindow)
	{
		tersus.error('Your browser blocks pop-up windows.\nTo use this application, please configure the browser to allow this site to open pop-up windows.');
		return;
	}
	this.currentWindow.focus();
}
Popup.prototype.findById = function popup_FindById(id, alreadyScanned)
{
	//Traverse the popup only if its window is open
	if (this.currentWindow && ! this.currentWindow.closed)
		return tersus.Node.prototype.findById.call(this,id, alreadyScanned);
	else
		return null;
};

// resume() is overriden to support the initial pause while waiting for the popup to pop)
Popup.prototype.resume = function resumePopup()
{
	if (this.popupStatus == Popup.WAITING_TO_POP_UP)
	{
		this.popupStatus = Popup.POPPED_UP;
		this.currentWindow.currentRootDisplayNode = this;
		this.currentWindow.focus();
		tersus.createEventDispatchers(this.currentWindow);
		this.setViewNode();
		this.createChildren();
		FlowNode.prototype.start.call(this);
		var node = this;
		this.currentWindow.setTimeout(function(){node.focusOnFirstField();}, 100);
	}
	else
	{
		this.runLoop();
	}
};
Popup.prototype.getPopupTitle = function getPopupTitle()
{
	return this.getCaption();
};

Popup.prototype.setTitle = function()
{
	try
	{
		if (this.currentWindow && ! this.currentWindow.closed)
		{
			this.currentWindow.document.title = this.getPopupTitle();
			if (this.header)
				this.header.innerHTML = tersus.escapeHTML(this.getPopupTitle());
		}
	}
	catch (e)
	{
	}
};
Popup.prototype.captionChanged = Popup.prototype.setTitle;
Popup.prototype.setViewNode = function setViewNode()
{
	this.viewNode = this.currentWindow.document.getElementById('dialogBody');
	if (this.viewNode)
	{
		this.header = this.currentWindow.document.getElementById('dialogHeader');
		this.footer = this.currentWindow.document.getElementById('dialogFooter');
	}
	else
	{
		this.viewNode = this.currentWindow.document.getElementById('root');
		if (! this.viewNode)
			this.viewNode = this.currentWindow.document.body;
	}
	this.setTitle();
	var nodeId = this.getNodeId();
	this.isOpen = true;
	this.onResize();
};
Popup.prototype.templateURL = document.compatMode == 'BackCompat' ? 'popup.quirksmode.html' : 'popup.html';
Popup.prototype.createWindow = function Popup_createWindow(){
	if (this.sprop('templateURL'))
		this.templateURL = this.sprop('templateURL');

	var options = this.sharedProperties.windowOptions;
	if (! options)
		options = 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes';
	var w = this.sharedProperties.windowWidth;
	if ( ! w || w == ' ')
		w = 350;
	var h = this.sharedProperties.windowHeight;
	if (! h || h == ' ')
		h = 265;
	var leftPx = parseInt((screen.availWidth-w-10)/2);
	var topPx = parseInt((screen.availHeight-h-30)/2);
	if (leftPx<0) leftPx=0;
	if (topPx<0) topPx=0;
	var targetWindow = this.getTargetWindow();
	var skeletonURL = tersus.rootURL+this.templateURL+'?nodeId='+this.getNodeId();
	this.currentWindow.mainTersusWindow = this.mainWindow;
	var popup = this.currentWindow.open(skeletonURL,targetWindow,"top="+topPx+",left="+leftPx+",width=" + w + ",height=" + h+","+options);
	return popup;
};

Popup.prototype.getTargetWindow = function getTargetWindow()
{
	//TODO support mutltipe popup windows
	if (this.sharedProperties.targetWindow && this.sharedProperties.targetWindow!=' ')
		return this.sharedProperties.targetWindow;
	else if (this.currentWindow == this.mainWindow)
		return "popup";
	else
		return this.currentWindow.name+"_popup";
};
Popup.prototype.destroy = function()
{
	if (! this.isDestroyed)
	{
		this.isDestroyed = true;
		DisplayNode.prototype.destroy.call(this);
		this.doClose();
	}
};
Popup.prototype.closeWindow = function()
{
	this.destroyHierarchy();
};
Popup.prototype.doClose = function ()
{
	delete tersus.popupNodes[this.getNodeId()];
	tersus.unregisterWindow(this.currentWindow);
	if (this.currentWindow && ! this.currentWindow.closed)
		this.currentWindow.close();
	this.isOpen = false;
	this.viewNode = null;
	this.footer = null;
	this.header = null;
};
Popup.prototype.onResize = function()
{
	if (! (this.isOpen && this.currentWindow && ! this.currentWindow.closed))
		return;
	var d = this.viewNode.ownerDocument;
	var s = getViewportSize(d);
	if (this.lastSize && this.lastSize.height == s.height && this.lastSize.width == s.width)
		return; // no change in size - nothing to do
	this.lastSize = s;
	var height = s.height;	 //client height of body
	var delta = d.getElementById('root').offsetHeight - this.viewNode.clientHeight;

	//Because clientHeight includes padding, but style.height doesn't include padding, we need to add padding to delta
	delta += getMeasure(this.viewNode,'padding-bottom');
	delta += getMeasure(this.viewNode,'padding-top');



	//Similarly, we need to remove body padding from the available height
	height -= parseInt(getActualStyle(d.body,'margin-bottom'));
	height -= parseInt(getActualStyle(d.body,'margin-top'));
//	height-=1;
	var newHeight = height-delta;
	if (newHeight>0 && newHeight < 3000)
	{
		this.viewNode.style.height = newHeight+'px';
	}
	this.onResizeChildren();
};
Popup.prototype.getValidationContext = function Popup_getValidationContext()
{
	return this;
};
Popup.prototype.computedReadOnly = function Popup_computedReadOnly()
{
	return this.explicitReadOnly;
};
Popup.prototype.parentReadOnlyChanged = function Popup_parentReadOnlyChanged(readOnly)
{
};

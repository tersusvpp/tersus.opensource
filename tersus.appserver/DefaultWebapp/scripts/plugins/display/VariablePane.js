/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function VariablePane(){};
VariablePane.prototype = new DisplayNode();
VariablePane.prototype.constructor = VariablePane;
VariablePane.prototype.defaultTag='div';
VariablePane.prototype.preloadOnStart=false;

VariablePane.prototype.createChildren = function()
{
};
VariablePane.prototype.createSubFlows = function()
{
	//initializes sub flows
	if (this.subFlowList)
	{
		for (var i=0;i<this.subFlowList.length; i++)
		{
			var subFlow = this.subFlowList[i];
			if (!subFlow.isDisplayElement)
				subFlow.init(this);
		}
	}
};

VariablePane.prototype.addDisplayElement = function addElement(role, modelId, properties)
{
	if (role.charAt(0) == '<' && this.getGetter(role))
	{
		var element = new MethodElement(this, role, modelId);
	}
	else
		var element = new VariableNodeDisplayElement(role, modelId);
	if (! this.displayElements)
		this.displayElements = [element];
	else
		this.displayElements.push(element);
	this.registerSubFlow(element);
	element.properties = properties;
	return this.addElement(element);
};
VariablePane.prototype['get<Current Content>'] = function()
{
	if (this.children)
	{
		for (var i=0; i<this.displayElements.length; i++)
		{
			var e = this.displayElements[i];
			var c = e.getChild(this);
			if (c != null)
				return c;
		}
	}
	return null;
}

function VariableNodeDisplayElement(role, modelId)
{
	this.role = role;
	this.modelId = modelId;
}

VariableNodeDisplayElement.prototype = new DisplayElement();
VariableNodeDisplayElement.prototype.constructor = VariableNodeDisplayElement;
VariableNodeDisplayElement.prototype.createChild = function createChild(parent, modelId, value)
{
	if (parent.currentWindow) // parent is a real display node
	{
		//TODO fix this so that 'onDelete()' is called for all children
		// (currently, some cleanup may be skipped)
		parent.children = [];
		parent.viewNode.innerHTML = '';
	}
	var child = DisplayElement.prototype.createChild.call(this,parent, modelId, value);
	return child;
};
VariableNodeDisplayElement.prototype.replaceChild = function replaceChild(parent, value)
{
	//TODO(x) complete the refactoring - replace deepCopy with copyValue. Compare with base implementation to see if it's needed at all
	if (parent.currentWindow) // parent is a real display node
	{
		var child = this.getChild(parent);
		if (child)
			child.deepCopy(value);
		else
			child = this.createChild(parent, this.modelId, value);		
	}
	else
	{
		Element.prototype.replaceChild.call(this, parent, value);
	}
};
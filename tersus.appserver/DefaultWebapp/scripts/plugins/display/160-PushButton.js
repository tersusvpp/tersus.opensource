/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.PushButton = function (){};
tersus.PushButton.prototype = new DisplayNode();
tersus.PushButton.prototype.defaultTag = 'span';
tersus.PushButton.prototype.getButtonName = function ()
{
	return null;
};
tersus.PushButton.prototype.initViewNode = function tersus_PushButton_initViewNode()
{
    DisplayNode.prototype.initViewNode.call(this);
    this.updateSelfReadOnly(this.computedReadOnly());
};

tersus.PushButton.prototype.createViewNode = function ()
{
    var name = this.getButtonName();
    var $button=$('<input>',this.currentWindow.document).attr('type',this.buttonType).attr('data-role','none').addClass(this.buttonType);
    if (name != null)
    	$button.attr('name',name);
    this.button = $button[0];
    if (this.getElement('<Caption>'))
    {
    	DisplayNode.prototype.createViewNode.call(this);
    	this.viewNode.appendChild(this.button);
    	this.label = this.createHTMLElement('span',this.buttonType+'Label');
    	this.label.innerHTML = tersus.escapeHTML(this.getCaption());
    	this.viewNode.appendChild(this.label);
    }
    else
    	this.viewNode = this.button;
};
tersus.PushButton.prototype.getChecked = tersus.PushButton.prototype['get<Checked>'] = function ()
{
	if (this.viewNode)
		return this.button.checked;
};
tersus.PushButton.prototype.setChecked = tersus.PushButton.prototype['set<Checked>'] = function (value)
{
	if (this.viewNode)
		this.button.checked = value;
	if (window.browser=='IE')
	{
		// In IE 6.0, setting 'checked' before the button is rendered 
		// has no visual effect
		var button = this.button;
		tersus.async.exec(function(){ button.checked = value;}, false /* don't wait */);
	}
};
tersus.PushButton.prototype['remove<Checked>'] = function ()
{
	if (this.viewNode)
		this.button.checked = false;
};

tersus.PushButton.prototype.getClickTarget = function()
{
	return this.button;
};
tersus.PushButton.prototype.setEventHandlers = function()
{
	this.registerEventHandler(Events.ON_CLICK, this.button);
	if (this.label)
		this.registerEventHandler(Events.ON_CLICK, this.label);
};

tersus.PushButton.prototype.onClick = function(domNode, callback)
{
	if (domNode == this.button)
	{
		DisplayNode.prototype.onClick.call(this, domNode, callback);
		this.onChange(domNode, callback);
	}
	else if (domNode == this.label)
		this.labelClicked(domNode, callback);
};

tersus.PushButton.prototype.captionChanged = function()
{
	if (this.label)
		this.label.innerHTML = tersus.escapeHTML(this.getCaption());
};
tersus.PushButton.prototype.updateSelfReadOnly = function tersus_PushButton_updateSelfOnly(readOnly)
{
	if (this.currentReadOnly == readOnly)
		return; // no change
	this.currentReadOnly = readOnly;
	if (this.button)
	{
		if (readOnly)
		{
			this.button.disabled = true;
		}
		else
			this.button.disabled = false;
	}
};

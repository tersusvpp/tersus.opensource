/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Layer = function tersus_Layer() {};
tersus.Layer.prototype = new Pane();
tersus.Layer.prototype.constructor = tersus.Layer;
tersus.Layer.prototype.attachViewNode = function()
{
	this.currentWindow.currentRootDisplayNode.viewNode.appendChild(this.viewNode);
	this.viewNode.style.position='absolute';
	$(this.viewNode).css('z-index',  ++ tersus.topZindex);
};
tersus.Layer.prototype.onDelete = function onDelete()
{
	this.destroyHierarchy();
	if (this.viewNode && this.viewNode.parentNode)
		this.viewNode.parentNode.removeChild(this.viewNode);
};

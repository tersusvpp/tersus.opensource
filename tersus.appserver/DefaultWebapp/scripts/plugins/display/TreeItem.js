/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function TreeItem() {};
TreeItem.prototype = new DataValue();
TreeItem.prototype.constructor = TreeItem;
TreeItem.prototype.getText = function getText() 
{
	var textNode = this.getChild('<Text>');
	if (textNode)
		return textNode.leafValue;
	else
		return '';
};
TreeItem.prototype.getNodeId = DisplayNode.prototype.getNodeId;
TreeItem.prototype.getImage = function getImage()
{
	var imageNode = this.getChild('<Image Path>');
	if (imageNode)
		return imageNode.leafValue;
	else
		return null;
};

TreeItem.prototype.isExpandable = function()
{
	if (this.childItems)
		return this.childItems.length > 0;
	var element = this.getElement('<Expandable>');
	if (! element)
		return true; // backward compatability
	var expandable = element.getChild(this); 
	return (expandable && expandable.leafValue);
}


TreeItem.COLLAPSED = 'Collapsed';
TreeItem.EXPANDED = 'Expanded';
TreeItem.ATOMIC = 'Atomic';
TreeItem.select = function()
{
	var item = DisplayNode.findNodeForDocumentNode(this);
	if (item)
		item.select();
};
TreeItem.toggle = function()
{
	var item = DisplayNode.findNodeForDocumentNode(this);
	if (item)
		item.toggle();
};
TreeItem.openItem = function()
{
	var treeItem = DisplayNode.findNodeForDocumentNode(this);
	if (treeItem)
		treeItem.openItem();
};
TreeItem.prototype.attach = function attach(tree,parentItem)
{
	this.tree = tree;
	var doc = tree.currentWindow.document;
	this.isRoot = ! parentItem;	
	if (parentItem)
	{
		this.parentItem = parentItem;
		this.viewNode = this.tree.createHTMLElement('div','tree_item');
		this.headerNode = this.tree.createHTMLChildElement(this.viewNode,'span','tree_item_header');
		var thisItem = this;
		this.image = this.tree.createHTMLChildElement(this.headerNode, 'img', 'tree_item_collapsed');
		this.image.src = 'images/spacer.gif';
		this.icon = this.tree.createHTMLChildElement(this.headerNode, 'img', 'tree_item_icon');
		this.icon.style.backgroundImage = 'url('+encodeURI(this.getImage())+')';
		this.icon.src = 'images/spacer.gif';
		this.headerText = this.tree.createHTMLChildElement(this.headerNode, 'span', 'tree_item_header_text');
		this.headerText.innerHTML = tersus.escapeHTML(this.getText());
		var id = this.getNodeId();
		this.icon.id = id;
		this.headerText.id = id;
		this.icon.onclick = TreeItem.select;
		this.headerText.onclick= TreeItem.select;
		this.icon.ondblclick = TreeItem.openItem;
		this.headerText.ondblclick = TreeItem.openItem;
		this.resetExpandable();
		parentItem.getOrCreateContentNode().appendChild(this.viewNode);
	}
	else
	{
		this.viewNode = this.contentNode = doc.createElement('div');
		this.expand();
		
		tree.appendChildViewNode(this);
	}
};
TreeItem.prototype.getOrCreateContentNode = function()
{
	if (!this.contentNode)
		this.contentNode = this.tree.createHTMLChildElement(this.viewNode,'div', 'tree_item_content');
	return this.contentNode;
		
};
TreeItem.prototype.resetExpandable = function()
{
	this.image.onclick = null;
	if (this.isExpandable())
	{
		this.image.id = this.getNodeId();
		this.collapse();
		this.image.onclick = TreeItem.toggle;
	}
	else
		this.atomic();
};
TreeItem.prototype.findById = function (id)
{
	if (this.nodeId == id)
		return this;
	if (this.childItems)
	{
		for (var i=0; i<this.childItems.length; i++)
		{
			var child = this.childItems[i];
			if (child)
			{
				var node = child.findById(id);
				if (node)
					return node;
			}
		}
	}
	return null;
};
TreeItem.prototype.loadChildren = function ()
{
	var item = this;
	var loaderElement = this.tree.getElement('<Get Children>');
	var process = new BackgroundProcess(this.tree, loaderElement);
	if (! this.isRoot) // The root item is not a 'real' item (as defined in the model)
		process.setTriggerValue('<Parent>', this);
	process.whenDone(function()
	{
		item.childItems = process.getExitValues('<Children>');
		if (item.childItems.length >0)
		{
			for (var i=0; i<item.childItems.length; i++)
			{
				var child = item.childItems[i];
				child.attach(item.tree,item);
			}
			item.state = TreeItem.EXPANDED;
		
			if (item.headerNode)
			{
				item.getOrCreateContentNode().style.display = 'block';
			}
			if (item.image)
				item.image.className='tree_item_expanded';
		}
		else
		{
			item.atomic();
		}
		
		if (item.onExpand)
			item.onExpand();
	});
	process.start();

};
TreeItem.prototype.expand  = function expand()
{
		if (! this.childItems)
		{
			this.loadChildren();
		}
		else
		{
			this.state = TreeItem.EXPANDED;
			if (this.headerNode)
			{
				this.getOrCreateContentNode().style.display = 'block';
			}
			if (this.image)
				this.image.className='tree_item_expanded';
			if (this.onExpand)
				this.onExpand();
		}
};
TreeItem.prototype.atomic = function()
{
		this.state = TreeItem.ATOMIC;
		if (this.image)
			this.image.className='tree_item_atomic';

};

TreeItem.prototype.collapse = function collapse()
{
		this.state = TreeItem.COLLAPSED;
		if (this.image)
			this.image.className='tree_item_collapsed';
		if (this.contentNode)
			this.contentNode.style.display='none';
		if (this.childItems)
		{
			for (var i=0; i< this.childItems.length; i++)
				this.childItems[i].onDelete();
		}
		this.childItems = null;

};
TreeItem.prototype.toggle = function toggle()
{
	if (this.state == TreeItem.COLLAPSED)
		this.expand();
	else
		this.collapse();
};

TreeItem.prototype.select = function select()
{
	if (!this.selected)
	{
		if (this.tree.selectedItem)
			this.tree.selectedItem.deselect();
		this.tree.selectedItem = this;
		this.headerText.className = 'tree_item_header_text_selected';
		this.selected = true;
	};
};
TreeItem.prototype.deselect = function deselect()
{
	this.headerText.className = 'tree_item_header_text';
	this.selected = false;
};
TreeItem.prototype.openItem = function openItem()
{
	var openSubFlow = this.tree.getEventHandler(Events.OPEN);
	if (openSubFlow)
	{
		var child = openSubFlow.createChild(this.tree);
		child.setChild('<Selected Item>', this, Operation.REPLACE);
		child.readyToStart();
		window.rootDisplayNode.doResume();
	};
		
};
TreeItem.prototype.scanChildren = function(filter, alreadyScanned,outputList)
{
	var id = this.getNodeId();
	if (alreadyScanned[id])
		return;
	alreadyScanned[id]=true;
	if (filter.match(this))
		outputList.push(this);
	if (this.childItems)
	{
		for (var i=0; i< this.childItems.length; i++)
			this.childItems[i].scanChildren(filter, alreadyScanned,outputList);		
	}
};

TreeItem.prototype.refresh = function()
{
	this.collapse();
	this.loadChildren();
	this.resetExpandable();
	
};
/* Returns true if this item and its ancestors are all expanded */
TreeItem.prototype.isExpandedAndVisible = function()
{
	if (this.state != TreeItem.EXPANDED)
		return false;
	else if (! this.parentItem)
		return true;
	else
		return this.parentItem.isExpandedAndVisible();
}
TreeItem.prototype.onDelete = function ()
{
	if (this.parentItem)
	{
		this.parentItem.contentNode.removeChild(this.viewNode);
	}
};

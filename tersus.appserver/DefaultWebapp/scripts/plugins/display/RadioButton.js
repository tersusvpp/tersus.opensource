/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.RadioButton = function tersus_RadioButton(){};
tersus.RadioButton.prototype = new tersus.PushButton();
tersus.RadioButton.prototype.constructor = tersus.RadioButton;
tersus.RadioButton.prototype.isRadioButton = true;
tersus.RadioButton.prototype.buttonType='radio';

tersus.RadioButton.prototype.getGroup = function()
{
	if (! this.group)
	{
		this.group = this.parent;
		while (this.group && ! this.group.isRadioButtonGroup)
		{
			this.group = this.group.parent;
		}
	}
	return this.group;
};
tersus.RadioButton.prototype.getButtonName = function()
{
	var group = this.getGroup();
	if (group)
		return group.getNodeId();
	else
		return this.getNodeId();
};
tersus.RadioButton.prototype.getButtonValue = tersus.RadioButton.prototype['get<Value>'] = function()
{
	if (this.value != undefined)
		return this.value;
	else
	{
		var c = tersus.repository.get(BuiltinModels.TEXT_ID);
		var node = new c();
        node.prototype = c.prototype;
        node.setLeaf(this.getCaption());
		return node;
	}
};
tersus.RadioButton.prototype['set<Value>'] = function (value)
{
	this.value = value;
};
tersus.RadioButton.prototype.labelClicked = function (domNode, callback)
{
	if (this.button && ! this.button.disabled)
	{
		if ( ! this.button.checked)
		{
			this.button.checked = true;
			this.onChange(domNode, callback);
		}
	}
};

tersus.RadioButton.prototype['set<Value>'].requiresNodes = true;
tersus.RadioButton.prototype.hasOnChange = true;
tersus.RadioButton.prototype.onChange = function(domNode, callback)
{
	if (this.button && this.button.checked)
	{
		var group = this.getGroup();
		if (group)
		{
			if (group && group.selectedButton == this)
				return; // was already checked
			group.selectedButton = this;
		}
		DisplayNode.prototype.onChange.call(this, domNode, callback);
		if (group)
			group.onChange(domNode, callback);
	}
	else
		DisplayNode.prototype.onChange.call(this);
	
};
tersus.RadioButton.prototype.setChecked =tersus.RadioButton.prototype['set<Checked>'] = function(checked)
{
	if (this.currentWindow)
	{
		var wasChecked = this.getChecked();
	}
	tersus.PushButton.prototype.setChecked.call(this,checked);
	if (checked && !wasChecked)
	{
		var group = this.getGroup();
		if (group)
			group.selectedButton = this;
	}
	else if (!checked && wasChecked)
	{
		var group = this.getGroup();
		if (group && group.selectedButton == this)
			group.selectedButton = null;
		
	}
};


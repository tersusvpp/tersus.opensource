/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.DialogFooter = function tersus_DialogFooter() {};
tersus.DialogFooter.prototype = new Row();
tersus.DialogFooter.prototype.constructor = tersus.DialogFooter;
tersus.DialogFooter.prototype.attachViewNode = function DialogFooter_attachViewNode()
{
	this.tableNode  = this.createHTMLElement('table');
	this.tbodyNode = this.createHTMLElement('tbody');
	this.tableNode.appendChild(this.tbodyNode);
	this.tbodyNode.appendChild(this.viewNode);
	if (this.parent&& this.parent.footer)
		this.parent.footer.appendChild(this.tableNode);

}
PopupFooter=tersus.DialogFooter; // For backward compatability
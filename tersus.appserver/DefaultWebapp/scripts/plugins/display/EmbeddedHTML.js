/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.EmbeddedHTML = function tersus_EmbeddedHTML(){};
tersus.EmbeddedHTML.prototype = new DisplayNode();
tersus.EmbeddedHTML.prototype.constructor = tersus.EmbeddedHTML;
tersus.EmbeddedHTML.prototype.defaultTag = 'div';
tersus.EmbeddedHTML.prototype.onload = function()
{
	tersus.repository.loadHTMLTemplate(tersus.translateResource(this.sharedProperties.templatePath));
};
tersus.EmbeddedHTML.prototype.createViewNode = function()
{
	var template = tersus.repository.getHTMLTemplate(tersus.translateResource(this.sharedProperties.templatePath));
	this.viewNode = importNode(this.currentWindow.document,template);
};
tersus.EmbeddedHTML.prototype.createChildren = function EmbeddedHTML_createChildren()
{
	if (this.displayElements)
	{
		/* Prepare a hash table with the nodes that correspond to different elements*/
		this.childNodeMap = {};
		tersus.EmbeddedHTML.addChildrenToMap(this.viewNode, this.childNodeMap);
		for (var i=0; i<this.displayElements.length; i++)
		{
			var e = this.displayElements[i];
			if ( !e.isRepetitive && ! e.isMethodElement)
			{
				var child = e.createChildWithoutVisuals(this);
				var childViewNode = this.childNodeMap[e.role];
				if (childViewNode)
				{
					child.setViewNode(childViewNode);
					childViewNode.setAttribute('element_name',null);
					child.setEventHandlers();
					child.createChildren();
				}
				else
					tersus.error('Missing element '+e.role+ " in template (path="+this.getPath()+")");

			}
		}
	}

};
tersus.EmbeddedHTML.addChildrenToMap = function(scope, map)
{
	//Not reusing createNodeMap because in this case we don't traverse the whole hierarchy
	var children = scope.childNodes;
	for (var i=0; i< children.length; i++)
	{
		var childNode = children[i];
		if (childNode.nodeType == 1 /*Node.ELEMENT_NODE*/ )
		{
			var childName = childNode.getAttribute('element_name');
			if (childName != null)
				map[childName]=childNode;
			else
				tersus.EmbeddedHTML.addChildrenToMap(childNode, map);
		}
	}
};tersus.EmbeddedElement = function EmbeddedElement(){};
tersus.EmbeddedElement.prototype = new tersus.EmbeddedHTML();
tersus.EmbeddedElement.prototype.constructor = tersus.EmbeddedElement;
tersus.EmbeddedElement.prototype.onload = function()
{
};

tersus.EmbeddedElement.prototype.createViewNode = function tersus_EmbeddedElement_createViewNode()
{
	var parentViewNode = this.parent.viewNode;
	this.templateNode = this.parent.childNodeMap[this.element.role];
	if (!this.templateNode)
		modelError("Failed to locate template node for "+this.getPath());
	this.viewNode = this.templateNode.cloneNode(this.templateNode,true);
	this.viewNode.setAttribute('element_name',null);
	this.viewNode.style.display='';
};
tersus.EmbeddedElement.prototype.attachViewNode = function tersus_EmbeddedElement_attachViewNode()
{
	this.templateNode.parentNode.insertBefore(this.viewNode,this.templateNode);
};

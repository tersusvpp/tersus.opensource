/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.PasswordField = function tersus_PasswordField() {};
tersus.PasswordField.prototype = new tersus.GenericField();
tersus.PasswordField.prototype.constructor = tersus.PasswordField();
tersus.PasswordField.prototype.defaultTag = 'input';
tersus.PasswordField.prototype.updateSelfReadOnly = function tersus_GenericField_updateSelfOnly(readOnly)
{
	if (this.currentReadOnly == readOnly)
		return; // no change
	this.currentReadOnly = readOnly;
   	if (this.inputField)
       	this.value = this.getFieldValue();
	if (this.viewNode)
	{
		if (readOnly)
		{
			this.viewNode.disabled = true;
		}
		else
			this.viewNode.disabled = false;
	}
	this.setValueString();
};

tersus.PasswordField.prototype.createViewNode = function tersus_GenericField_createViewNode()
{
    DisplayNode.prototype.createViewNode.call(this);
    this.viewNode.type = 'password';
    this.inputField = this.viewNode;
};
tersus.PasswordField.prototype.setViewNode = function tersus_PasswordField_createViewNode(viewNode)
{
	this.viewNode = this.inputField = viewNode;
}

tersus.PasswordField.prototype.setEventHandlers =DisplayNode.prototype.setEventHandlers;

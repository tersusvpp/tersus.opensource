/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Image = function TersusImage(){};
tersus.Image.prototype = new DisplayNode();
tersus.Image.prototype.constructor = tersus.Image;
tersus.Image.prototype.getTag = function()
{
	return "img";
};
tersus.Image.prototype['set<Path>'] = function( path)
{
	this.srcPath = path;
	this.viewNode.src = tersus.translateResource(path);
};
tersus.Image.prototype['get<Path>'] = function()
{
	if (this.srcPath)
		return this.srcPath;
	else
		return null;
};

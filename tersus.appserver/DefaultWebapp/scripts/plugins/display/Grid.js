/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Grid = function tersus_Grid() {};
tersus.Grid.prototype = new Table();
tersus.Grid.prototype.constructor = tersus.Grid;
tersus.Grid.prototype.createCaption = tersus.Grid.prototype.createHeader = function() {};

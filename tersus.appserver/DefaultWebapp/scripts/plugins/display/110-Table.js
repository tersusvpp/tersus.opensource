/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 //Table - a simple table
Table = function Table() {this.columnNames={}; };
Table.prototype = new DisplayNode();
Table.prototype.constructor = Table;
Table.prototype.getTag = function()
{
	return 'table';
};
Table.prototype.reset = function reset()
{
	DisplayNode.prototype.reset.call(this);
	this.selectedRow = null;
	this.hiddenColumns = {};
	if (this.headerRow)
	{
		var headerCells=this.headerRow.childNodes;
		for (var i=0;i<headerCells.length;i++)
		{
			headerCells[i].style.display='';
		}
	}
}
Table.prototype.createViewNode = function createViewNode()
{
	this.hiddenColumns = {};
    DisplayNode.prototype.createViewNode.call(this);
    this.sort = (this.sharedProperties.sort == 'multiple' || this.sharedProperties.sort == 'single');
    
	this.setProperties(); // In Mozilla, we have to set the styleclass before creating the body of the table
    this.createCaption();
    this.createHeader();
    this.bodyNode = this.currentWindow.document.createElement('tbody');
    var h = this.sharedProperties.contentHeight;
    if (h && h == ' ')
    	h = null;
    var w = this.sharedProperties.contentWidth;
    if (w && w == ' ')
    	w = null;
 	if (w || h)   
		setScrollableTable(this.bodyNode, w,h); // broswer dependant
    this.viewNode.appendChild(this.bodyNode);
    this.viewNode.id = this.getNodeId();
};
Table.prototype.createCaption = function createCaption()
{
	if ( this.sharedProperties.showCaption == false) // default = true
	{
		return;
	}
	else
		if ( ! this.getRowPrototype())
			return;
	this.captionDOMElement = this.createHTMLElement('caption');
	this.captionDOMElement.innerHTML = '<div>'+tersus.escapeHTML(this.getCaption())+'</div>';
	this.viewNode.appendChild(this.captionDOMElement);
};
Table.prototype.captionChanged = function()
{
	if (this.captionDOMElement)
		this.captionDOMElement.innerHTML = '<div>'+tersus.escapeHTML(this.getCaption())+'</div>';
};

Table.prototype.addHeaderCells = function addHeaderCells(headerRow,node)
/* Adds header cells based on the structure of the given node */
{
	for (var i=0; i< node.displayElements.length; i++)
	{	
		var columnElement = node.displayElements[i];
		var columnNodePrototype = columnElement.getChildConstructor().prototype;
		if (columnNodePrototype.isDisplayGroup)
		{
			this.addHeaderCells(headerRow, columnNodePrototype);
		}
		else
		{
			var labelHTML = columnElement.properties.caption;
			if (!labelHTML || labelHTML == ' ')
				labelHTML = tersus.escapeHTML(columnElement.role)
			labelHTML = this.translate(labelHTML);
			var headerCell = this.createHTMLElement('th');
			headerCell.setAttribute("element_name", columnElement.role);
			var columnName = columnElement.properties.sortColumnName;
			if (!columnName)
				columnName = columnElement.role;
			if (!tersus.useSQLQuotedIdentifiers)
				columnName = tersus.sqlize(columnName);
			headerCell.setAttribute("column_name", columnName);
			this.columnNames[columnElement.role]=tersus.useSQLQuotedIdentifiers ? columnName : columnName.toLowerCase();
			headerCell.innerHTML = labelHTML;
			var col = this.createHTMLChildElement(this.colgroup,'col');
			headerCell.col = col;
			if (this.sort && columnElement.properties.sortable != false)
			{
				headerCell.setAttribute('sort', 'none');
				headerCell.onclick = Table.headerClicked;
			}
			headerCell.onmousedown = Table.mouseDownOnHeader;
			headerCell.onmouseover = Table.updateHeaderCursor;
			headerCell.onmousemove = Table.updateHeaderCursor;
				

			if (headerRow.childNodes.length == 0)
				headerCell.className='first';
			headerRow.appendChild(headerCell);
		}
	}
};
Table.prototype.getRowPrototype = function()
{
	var output = null;
	if (this.subFlowList)
	{
		for (var i=0; i< this.subFlowList.length; i++)
		{
			var subFlow = this.subFlowList[i];
			if (subFlow.isDisplayElement && subFlow.role != '<Header>' && subFlow.role != '<Footer>')
			{
				if (output)
					return null;
				else
					output = subFlow;
			}
		}	
	}
	return output;
};
Table.prototype.createHeader = function createHeader()
{
	if ( this.sharedProperties.showHeadings == false) // default = true
		return;
	var rowElement = this.getRowPrototype();
	if (! rowElement) return;
	var rowPrototype = tersus.repository.get(rowElement.modelId).prototype;
	if (! rowPrototype.subFlowList) return;
	var headerRow = this.createHTMLElement('tr');
	this.colgroup = this.createHTMLChildElement(this.viewNode,'colgroup');
	this.addHeaderCells(headerRow, rowPrototype);
	var tableHeader = this.createHTMLElement('thead');
	tableHeader.appendChild(headerRow);
	this.header = tableHeader;
	this.headerRow = headerRow;
	this.viewNode.appendChild(tableHeader);
};

Table.prototype.removeChildViewNode = function removeChildViewNode(child)
{
	var c = child.viewNode;
	var row = null;
	if (c.tagName == 'TR' )
		row = c;
	else if (c.parentNode && c.parentNode.tagName == 'TR')
		row = c.parentNode;
	else if (c.parentNode && c.parentNode.parentNode && c.parentNode.parentNode.tagName== 'TR')
		row = c.parentNode.parentNode;
	if (row && row.parentNode)
		row.parentNode.removeChild(row);
};
Table.prototype.applyColOrder = function (row)
{
	if (this.colOrder)
	{
		var cells = [];
		for (var i=0;i<row.childNodes.length;i++)
		{
			cells.push(row.childNodes[i]);
		}
		if (cells.length != this.colOrder.length)
		{
			internalError("Failed to apply column order: mismatch between number of cells in row and number of columns in colOrder");
			return;
		}
		for (var i=0;i<cells.length;i++)
		{
			row.removeChild(cells[i]);
		}
		for (var i=0;i<cells.length;i++)
		{
			row.appendChild(cells[this.colOrder[i]]);
		}
	};
};
Table.prototype.getFooter = function()
{
	if (! this.footer)
		this.footer = 	this.header = this.createHTMLChildElement(this.viewNode, 'tfoot');
	return this.footer;
		
};
Table.prototype.appendChildViewNode = function appendChildViewNode(child, childElement)
{
	var element = childElement?childElement:child.element;
	var targetNode = this.bodyNode;
	if (element.role == '<Header>')
		targetNode = this.header;
	if (element.role == '<Footer>')
		targetNode = this.getFooter();
	var row = null;
	if (child.viewNode.tagName == 'TR')
	{
		row = child.viewNode;
	}
	else
	{
		var row = this.createHTMLElement('TR');
		if (child.viewNode.tagName == 'TH' || child.viewNode.tagName == 'TD')
			row.appendChild(child.viewNode);
		else
		{
			var cell = this.createHTMLChildElement(row,'TD');
			cell.appendChild(child.viewNode);
		}
		row.setAttribute('display_order', child.viewNode.getAttribute('display_order'));
	}
	if (this.headerRow && ! element.prop('suppressColumnHiding'))
	{
		var rowCells = row.childNodes;
		var headerCells = this.headerRow.childNodes;
		if (rowCells.length == headerCells.length)
		{
			for (var i=0;i<headerCells.length;i++)
			{
				if (headerCells[i].style.display=='none')
					rowCells[i].style.display= 'none';
			}
		}
	}
		
		 
	if (this.header) // If there is a header, we know we have a single row, so we can directly append the child row and skip display order handling
		targetNode.appendChild(row)
	else
		DisplayNode.appendChildNode(targetNode,row); 
};

Table.prototype['get<Selected Row>'] = function ()
{
	if (this.selectedRow)
		return this.selectedRow;
	else
		return null;
};
Table.prototype['set<Selected Row>'] = Table.prototype.select = function (row)
{
	var selectedClassName = 'selected';
	//TODO(y) - check that row is a row in this table. Report an error if not.
	if (this.selectedRow)
	{
		if (this.pluginVersion>=1)
			this.selectedRow.removeStyleClass(selectedClassName);
		else
			this.selectedRow.viewNode.className = this.selectedRow.baseClassName;
	}
	this.selectedRow = row;
	if (row && row.viewNode)
	{
		if (this.pluginVersion>=1)
			row.addStyleClass(selectedClassName);
		else
			row.viewNode.className = 'selected';
	}
};
Table.prototype['remove<Selected Row>'] = function ()
{
	var selectedClassName = 'selected';
	if (this.selectedRow)
	{
		if (this.pluginVersion>=1)
			this.selectedRow.removeStyleClass(selectedClassName);
		else
			this.selectedRow.viewNode.className = this.selectedRow.baseClassName;
		this.selectedRow = null;
	}
};
Table.prototype.select.requiresNodes = true;
Table.prototype['get<Sort Order>'] = Table.prototype.getSortOrder = function()
{
	var sortOrder = null;
	var headerCells = this.headerRow.childNodes;
	for (var i=0;i<headerCells.length;i++)
	{
		var headerCell = headerCells[i];
		var columnName = headerCell.getAttribute('column_name');
		if (tersus.useSQLQuotedIdentifiers)
			columnName = '"'+columnName+'"';
		var sort = headerCell.getAttribute('sort');
		var sortKey = null;
		if (sort == 'up')
		 	sortKey=' ASC';
		else if (sort == 'down')
			sortKey=' DESC';
		if (sortKey)
		{
			if (sortOrder ==null)
				sortOrder = '';
			else
				sortOrder+=', ';
			sortOrder+=columnName;
			sortOrder+=sortKey;
		}
	}
	return sortOrder;
};
Table.prototype['set<Sort Order>'] = function (sort_order)
{
	//TODO: handle conflicts between column order and sort order
	var headerCells = this.headerRow.childNodes;
	var header_cell_map = {};
	for (var i=0;i<headerCells.length;i++)
	{
		var headerCell = headerCells[i];
		var columnName = headerCell.getAttribute('column_name');
		if (!tersus.useSQLQuotedIdentifers)
			columnName = columnName.toLowerCase();
		this.doSortNoFire(headerCell,'none');
		header_cell_map[columnName]=headerCell;
	}
	var result;
	var re;
	if (sort_order.indexOf('"')>=0)
		re = / *"([^"]+)"( +([a-z]+))? *(,|$)/gi
	else 
		re = / *([^, ]+)( +([a-z]+))? *(,|$)/gi;
	var prevHeaderCell = null;
	while (result = re.exec(sort_order))
	{
		var columnName = result[1];
		if (! tersus.useSQLQuotedIdentifiers)
			columnName = columnName.toLowerCase();
		var dir = result[3];
		dir =  (dir && dir.toLowerCase().charAt(0)=='d') ? 'down' : 'up';
		var headerCell = header_cell_map[columnName];
		this.doSortNoFire(headerCell,dir);
		if (prevHeaderCell)
			this.ensureColumnOrder(prevHeaderCell, headerCell);
		prevHeaderCell = headerCell; 
	};
};
Table.prototype['remove<Sort Order>'] = function ()
{
	var headerCells = this.headerRow.childNodes;
	for (var i=0;i<headerCells.length;i++)
	{
		this.doSortNoFire(headerCells[i],'none');
	}
};
Table.prototype['set<Hidden Columns>'] = function(hidden_cols)
{
	var re;
	if (hidden_cols.indexOf('"')>=0)
		re = / *"([^"]+)" *(,|$)/gi
	else 
		re = / *([^, ]+) *(,|$)/gi;
	var hidden = this.hiddenColumns;
	for (var columnName in hidden)
		hidden[columnName]=0;
	var result;
	while (result = re.exec(hidden_cols))
	{
		var columnName = result[1];
		if (! tersus.useSQLQuotedIdentifiers)
			columnName = columnName.toLowerCase();
		if (hidden[columnName] == null)
		{
			this.hideColumn(columnName);
		}
		else
			hidden[columnName]=1; // Just mark that this column is still hidden
	}
	for (var columnName in hidden)
	{
		if (hidden[columnName] == 0) // was hidden but no longer is
		{
			this.showColumn(columnName);
		}
	}
	this.rebuildColgroup();
};
Table.prototype['remove<Hidden Columns>'] = function()
{
	for (var columnName in this.hiddenColumns)
	{
		this.showColumn(columnName);
	}
	this.rebuildColgroup();

};

Table.prototype['get<Hidden Columns>'] =  function()
{
	var hidden = this.hiddenColumns;
	if (hidden == null)
		hidden = {}; 
	var hiddenCols = null;
	var headerCells = this.headerRow.childNodes;
	for (var i=0;i<headerCells.length;i++)
	{
		var headerCell = headerCells[i];
		var columnName = headerCell.getAttribute('column_name');
		if (hidden[columnName.toLowerCase()])
		{
			if (hiddenCols == null)
				hiddenCols = '';
			else
				hiddenCols+=', ';
			if (tersus.useSQLQuotedIdentifiers)
				columnName = '"'+columnName+'"';	
			hiddenCols+=columnName;
		}
	}
	return hiddenCols;
};
Table.prototype['get<All Columns>'] =  function()
{
	var allCols = null;
	var headerCells = this.headerRow.childNodes;
	for (var i=0;i<headerCells.length;i++)
	{
		var headerCell = headerCells[i];
		var columnName = headerCell.getAttribute('column_name');
		if (tersus.useSQLQuotedIdentifiers)
			columnName = '"'+columnName+'"';
		if (allCols == null)
			allCols = '';
		else
			allCols+=',';
		allCols+=columnName;
	}
	return allCols;
};

Table.prototype.hideColumn = function(columnName)
{
	this.hiddenColumns[columnName] = 1;
	var headerCells = this.headerRow.childNodes;
	for (var i=0;i<headerCells.length;i++)
	{
		var headerCell = headerCells[i];
		var headerColumnName = headerCell.getAttribute('column_name');
		if (!tersus.useSQLQuotedIdentifiers)
			headerColumnName=headerColumnName.toLowerCase();
		if ( columnName == headerColumnName )
		{
			var cw = getVisibleSize(headerCell).width;
			headerCell.col.style.width = cw + 'px';
			if (this.viewNode.style.width)
			{
				var originalWidth = parseInt(this.viewNode.style.width);
				this.viewNode.style.width = (originalWidth-cw) + 'px';
			}

			this.updateColumnVisibility(i,false);
		
			if (this.colgroup == headerCell.col.parentNode)
				this.colgroup.removeChild(headerCell.col);
		}
	}
};
Table.prototype.showColumn = function(columnName)
{
	delete this.hiddenColumns[columnName];
	var headerCells = this.headerRow.childNodes;
	for (var i=0;i<headerCells.length;i++)
	{
		var headerCell = headerCells[i];
		var headerColumnName = headerCell.getAttribute('column_name');
		if (!tersus.useSQLQuotedIdentifiers)
			headerColumnName=headerColumnName.toLowerCase();
		if ( columnName == headerColumnName )
		{
			if (this.viewNode.style.width)
			{
				var cw = parseInt(headerCell.col.style.width);
				if (cw)
				{
					var originalWidth = parseInt(this.viewNode.style.width);
					this.viewNode.style.width = (originalWidth+cw) + 'px';
				}
			}
			else
				headerCell.col.style.width='';

			this.updateColumnVisibility(i,true);
		}
	}
}
Table.prototype.updateColumnVisibility = function(colNum,visible)
{
    var rowElement = this.getRowPrototype();
    var rowId = rowElement && rowElement.modelId;
	var rows = this.viewNode.rows;
	var colStyle = visible ? '' : 'none';

	this.headerRow.childNodes[colNum].style.display=colStyle;
	
	for (var j=0;j<rows.length; j++)
	{
	    var r = rows[j];
	    var n = DisplayNode.findNodeForDocumentNode(r);
	    if (n)
	    {
	    	var hc = null;
    		if (n.modelId === rowId)
    			hc = true;
    		else
    		{
	    		var hc = n.element.properties.enableHiddenColumns;
		    	if (hc == null)
		    		hc = n.sharedProperties.enableHiddenColumns;
		    	if (hc == null)
	    			hc = false;
    		}
		    if (r === this.headerRow || hc)
		    {
				var rowCells = r.childNodes;
				if (rowCells && rowCells[colNum])
					rowCells[colNum].style.display = colStyle;
		    }
	    }
	}

};
Table.prototype.setColWidths = function()
{
	var headerCells = this.headerRow.childNodes;
	for (var i=0;i<headerCells.length;i++)
	{
		var headerCell = headerCells[i];
		var c = headerCell.col;
		if (headerCell.style.display!='none')
		{
		    var w = $(headerCell).outerWidth();
		    
			c.style.width=w;
		}
	}

};
Table.prototype.rebuildColgroup = function()
{
	var headerCells = this.headerRow.childNodes;
	var cg = this.colgroup;
	var w= [];
	var cc = [];
	for (var i=0;i<headerCells.length;i++)
	{
		var headerCell = headerCells[i];
		var c = headerCell.col;
		if (cg == c.parentNode)
			cg.removeChild(c);
		if (headerCell.style.display!='none')
		{
			cg.appendChild(c);
		}
	}
//	this.currentWindow.setTimeout(function(){for (var i=0;i<w.length;i++) cc[i].style.width=w[i];},2000);
};	
Table.prototype.ensureColumnOrder = function (cell1, cell2) // ensures that cell2 is not before cell1
{
	var allSiblings = cell1.parentNode.childNodes;
	var i1=-1;
	var i2=-1;
	for (var i=0;i<allSiblings.length;i++)
	{
		if (allSiblings[i] == cell1)
			i1 = i;
		if (allSiblings[i] == cell2)
			i2 = i;
	}
	if (i2 < i1) // rearrage columns in the table so that cell2 appears right after cell1
	{
		this.moveCells(i2,i1+1);
	};
};
Table.isResizeLeft = function(e,headerCell)
{
	var offsets = getScreenPosition(headerCell);
	var win = getParentWindow(headerCell);
	var p = getEventPosition(win,e);
	return (p.x <= offsets.left +5);
};
Table.isResizeRight = function(e,headerCell)
{
	var offsets = getScreenPosition(headerCell);
	var size = getVisibleSize(headerCell);
	var win = getParentWindow(headerCell);
	var p = getEventPosition(win,e);
	return (p.x >= offsets.left + size.width -5);
};
Table.isResize = function(e, headerCell)
{
	return Table.isResizeRight(e,headerCell)
	 || headerCell.previousSibling && Table.isResizeLeft(e,headerCell);
};
Table.headerClicked = function(e)
{
	if (! Table.isResize(e,this))
	{
		var tableViewNode = this.parentNode.parentNode.parentNode;
		var tableNode = DisplayNode.findNodeForDocumentNode(tableViewNode);
		if (!tableNode)
			return;
		var win = tableNode.currentWindow;
		if (! getEventPosition(win,e).equals(tableNode.mouseDownPosition))
			return; // Ignore click if it's part of a drag operation
		var sort = this.getAttribute('sort');
		if (sort == 'up')
			tableNode.doSort(this,'down');
		else if (sort == 'down')
			tableNode.doSort(this,'none');
		else
			tableNode.doSort(this, 'up');
	}
};
Table.prototype.doSortNoFire = function(headerCell,dir)
{
	headerCell.setAttribute('sort',dir);
	headerCell.className = 'sort_'+dir;
};
Table.prototype.doSort = function(headerCell, dir)
{
	if (this.sharedProperties.sort == 'single')
	{
		var headerCells =headerCell.parentNode.childNodes;
		for (var i=0; i<headerCells.length; i++)
		{
			var other = headerCells[i];
			if (other !== headerCell)
				this.doSortNoFire(other,'none'); 
		}
	}
	
	this.doSortNoFire(headerCell,dir);
	this.fireOnSort();
}
Table.prototype.fireOnSort = function()
{
	this.mainWindow.engine.handleEvent(this, Events.ON_SORT);
};
Table.mouseDownOnHeader = function(e)
{
	var tableViewNode = this.parentNode.parentNode.parentNode;
	var tableNode = DisplayNode.findNodeForDocumentNode(tableViewNode);
	if (!tableNode)
		return;
	var offsets = getScreenPosition(this);
	var size = getVisibleSize(this);
	var win = tableNode.currentWindow;
	clearSelection(win);
	tableNode.mouseDownPosition  = getEventPosition(win,e);
	if (Table.isResizeLeft(e,this))
	{
		var previousCell = this.previousSibling;
		if (previousCell)
			tableNode.beginResize(e,previousCell);
	}
	else if (Table.isResizeRight(e,this))
		tableNode.beginResize(e,this);
	else
		tableNode.beginReorder(e,this);
};
Table.finishDrag = function (e)
{
	var table=tersus.findNode(this.documentElement.getAttribute('tableId'));
	this.documentElement.setAttribute('tableId', null);
	this.onmouseup = null;
	this.onmousemove = null;
	table.viewNode.className=table.baseClassName;
};
Table.updateHeaderCursor = function (e)
{
	var tableId = this.ownerDocument.documentElement.getAttribute('tableId');
	var table = null;
	if (tableId)
		table=tersus.findNode(tableId);
	if (table && table.reorder)
		this.style.cursor='';
	else
	{
		if (Table.isResize(e,this))
		{
			this.style.cursor='e-resize';
		}
		else if (this.getAttribute('sort'))
			this.style.cursor='pointer';
		else
			this.style.cursor='';
	}
};
Table.performResize = function (e)
{
	var table=tersus.findNode(this.documentElement.getAttribute('tableId'));
	var win = table.currentWindow;
	clearSelection(win);
	var currentPosition = getEventPosition(win,e);
	var delta = {};
	delta.x = currentPosition.x - table.lastPosition.x;
	delta.y = currentPosition.y - table.lastPosition.y;
	table.lastPosition = currentPosition;		
	table.resize(delta);
};
Table.prototype.beginResize = function beginResize(e, headerCell)
{
	this.setColWidths();
	var table = this;
	var win = this.currentWindow;
	clearSelection(win);
	var doc = win.document;
	doc.documentElement.setAttribute('tableId', this.getNodeId());
	table.lastPosition = getEventPosition(win, e);
	table.viewNode.className+=' resizing';
	table.resizingHeaderCell = headerCell;
	table.currentColWidth = getVisibleSize(headerCell).width;
	table.currentTableWidth = getVisibleSize(table.viewNode).width;
	doc.onmousemove = Table.performResize;
	doc.onmouseup = Table.finishDrag;
};
Table.prototype.beginReorder = function beginReorder(e, headerCell)
{
	var table = this;
	var win = this.currentWindow;
	clearSelection(win);
	var doc = win.document;
	var feedback = doc.createElement('div');
	feedback.className='drag_feedback';
	doc.documentElement.setAttribute('tableId', this.getNodeId());
	table.reorder = {};
	table.reorder.feedback =feedback;
	table.reorder.headerCell = headerCell;
	table.reorder.startPosition = getEventPosition(win,e);;
	
	doc.onmousemove = Table.feedbackReorder;
	doc.onmouseup = Table.finishReorder;
};

Table.prototype.cancelReorder = function()
{
	if (! this.reorder)
		return;
	if (this.reorder.feedback.parentNode)
		this.reorder.feedback.parentNode.removeChild(this.reorder.feedback);
	this.reorder = null;
	var doc = this.currentWindow.document;
	doc.documentElement.setAttribute('tableId', null);
	doc.onmouseup = null;
	doc.onmousemove = null;
};
Table.finishReorder = function (e)
{
	var table=tersus.findNode(this.documentElement.getAttribute('tableId'));
	if (! table.reorder)
		return;
	var win = table.currentWindow;
	clearSelection(win);
	var headerCell = table.reorder.headerCell;
	var startX = table.reorder.startPosition.x;
	table.cancelReorder();
	var p = getEventPosition(win,e);
	var fromIndex=-1;
	var toIndex =-1;
	if (p.x != startX)
	{
		var headerCells =headerCell.parentNode.childNodes;
		for (var i=0;i<headerCells.length; i++)
		{
			var cell = headerCells[i];
			if (cell.style.display=='none')
				continue;
			if (cell == headerCell)
				fromIndex = i;
			else if (toIndex < 0)
			{
				var left = getScreenPosition(cell).left;
				var width = getVisibleSize(cell).width;
				if (p.x >= left && p.x <left+width)
				{ 
					if (p.x > left+width/2 )
					{
						toIndex = i+1;
					}
					else
						toIndex = i;
				}
			}
		}
		if (toIndex != -1 && toIndex != fromIndex && toIndex != fromIndex+1)
		{
			var originalSortOrder = table.getSortOrder();
			table.moveCells(fromIndex,toIndex);
			var newSortOrder = table.getSortOrder();
			if (originalSortOrder != newSortOrder)
			{
				table.fireOnSort();
			}
		}
	}
};
Table.move = function (parent, from, to)
{
	var children = parent.childNodes;
	var childFrom = children[from];
	var childTo = null;
	if (to < children.length)
	{
		childTo = children[to];
	}
	parent.insertBefore(childFrom,childTo);
};
Array.prototype.moveItem = function(fromIndex,toIndex)
{
	var item=this.splice(fromIndex,1)[0]; 
	var newIndex = (fromIndex<toIndex) ? toIndex-1: toIndex;
	this.splice(newIndex,0, item);
};
Table.prototype.moveCells = function (fromIndex,toIndex)
{
	this.reorderColGroup(fromIndex, toIndex);
	var rows = this.viewNode.rows;
	for (var i=0;i<rows.length; i++)
		Table.move(rows[i],fromIndex, toIndex);

	//Update the 'colOrder' permutation		
	if (!this.colOrder)
	{
		this.colOrder = [];
		for (var i=0;i<this.headerRow.childNodes.length;i++)
		{
			this.colOrder.push(i);
		}
	}
	this.colOrder.moveItem(fromIndex,toIndex);
};
Table.prototype.reorderColGroup = function(fromIndex, toIndex)
{
	// Since there are no 'col' elements for hidden columns, we need to correct the indices
	var headerCells  = this.headerRow.childNodes;
	var from = fromIndex;
	var to = toIndex;
	for (var i=0;i<headerCells.length;i++)
	{
		var c = headerCells[i];
		if (c.style.display == 'none')
		{
			if (i<=fromIndex)
				--from;
			if (i<=toIndex)
				--to;
		}
	}
	Table.move(this.colgroup, from, to);
};
Table.feedbackReorder = function (e)
{
	var table=tersus.findNode(this.documentElement.getAttribute('tableId'));
	var win = table.currentWindow;
	clearSelection(win);
	var headerCell = table.reorder.headerCell;
	var headerCellSize = getVisibleSize(headerCell);
	var p = getEventPosition(win,e);
	var o = getScreenPosition(table.viewNode);
	var w = getVisibleSize(table.viewNode).width;
	var h = getVisibleSize(table.viewNode).height;
	if( p.y<o.top || p.y > o.top+h || p.x<o.left || p.x > o.left + w)
	{
		table.cancelReorder();
		return;
	}
	
	var feedback = table.reorder.feedback;
	var body = win.document.body;
	if (!feedback.parentNode )
	{
		body.appendChild(feedback);
		feedback.innerHTML = headerCell.innerHTML;
		tersus.async.exec(function()
		{
			var d= (window.browser == 'Mozilla')?-2:0; // Probably needs fixing (box-sizing issue on old browsers)
			$(feedback).css({top: getScreenPosition(headerCell).top+d+body.scrollTop
			                    ,left: p.x+body.scrollLeft
			                    ,width: headerCellSize.width
			                    ,height:headerCellSize.height});
		}, false /* don't wait */)
	}
	else
	{
		$(table.reorder.feedback).css('left',p.x+body.scrollLeft);
	}
};
Table.prototype.resize = function(delta)
{
    var MIN_WIDTH=10;
	this.currentColWidth += delta.x;
	this.currentTableWidth += delta.x;
	var table = this;
	if (this.currentColWidth> MIN_WIDTH)
	    setTimeout(function() {table.resizingHeaderCell.col.style.width = table.currentColWidth+'px';table.viewNode.style.width=table.currentTableWidth+'px'},10);
};

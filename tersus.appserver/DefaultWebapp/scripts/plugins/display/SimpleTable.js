/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 //Simple Table - A Table whos rows are based on a repetitive Data Element
function SimpleTable() {};
SimpleTable.prototype = new Table();
SimpleTable.prototype.constructor = SimpleTable;

SimpleTable.prototype.createHeader = function createHeader()
{
	if (! this.rowElement) return;
	var rowPrototype = tersus.repository.get(this.rowElement.modelId).prototype;
	if (! rowPrototype.elementList) return;
	var headerRow = this.createHTMLElement('tr');
	for (var i=0; i< rowPrototype.elementList.length; i++)
	{	
		var columnElement = rowPrototype.elementList[i];
		var label = this.translate(columnElement.role);
		var headerCell = this.createHTMLElement('th');
		headerCell.innerHTML = tersus.escapeHTML(label);
		if (i==0)
			headerCell.className='first';
		headerRow.appendChild(headerCell);
	}
	this.header = this.createHTMLElement('thead');
	var caption = this.createHTMLElement('caption');
	caption.innerHTML = tersus.escapeHTML(this.getCaption());
	this.header.appendChild(headerRow);
	this.viewNode.appendChild(caption);
	this.viewNode.appendChild(this.header);

		
};

// Overload addIntermediateDataElement - and create a SimpleTableRowElement instead
SimpleTable.prototype.addIntermediateDataElement = function addIntermediateDataElement(role, modelId)
{
	if (role.charAt(0) == '<' && this.getGetter(role))
	{
		element = new MethodElement(this, role, modelId);
		return this.addElement(element);
	}
	else
	{
		if (this.rowElement)
			modelError('A SimpleTable can only have one intemediate data element. '+this.modelId + ' has both "'+this.rowElement.role+'" and "'+role+'"');
		this.rowElement = new SimpleTableRowElement(role, modelId);
		return this.addElement(this.rowElement);
	}
};
SimpleTable.ROW_NODE_ID='rowNodeId';
SimpleTable.TABLE_NODE_ID='tableNodeId';

SimpleTable.prototype.isSimpleTable = true;
SimpleTable.handleRowClick = function()
{
	var htmlRow = this;
	var tableNode = tersus.findNode(htmlRow.getAttribute(SimpleTable.TABLE_NODE_ID));
	makeAssertion(tableNode != null,"Simple Table node not found");
	var rowId=htmlRow.getAttribute(SimpleTable.ROW_NODE_ID);
	var rowRecord = null;
	if (tableNode.rowElement)
	{
		var rowNodes=tableNode.rowElement.getChildren(tableNode);
		for (var i=0;i<rowNodes.length;i++)
		{
			if (rowNodes[i].getNodeId() == rowId)
			{
				rowRecord = rowNodes[i];
				break;
			}
		}
	}
	makeAssertion(rowRecord != null,"Simple Table row node not found");
	if (tableNode.selectedRow && tableNode.selectedHTMLRow)
		tableNode.selectedHTMLRow.className = null;
	tableNode.selectedHTMLRow = htmlRow;
	htmlRow.className = 'selected';
	tableNode.selectedRow = rowRecord;
};
//SimpleTableRowElement - An Element of SimpleTable that represents the row content.
//This is a special element for which the 'Add' operation creates rows to the table
function SimpleTableRowElement(role, modelId)
{
	this.role = role;
	this.modelId = modelId;
};
SimpleTableRowElement.prototype = new Element();
SimpleTableRowElement.prototype.constructor = SimpleTableRowElement;
SimpleTableRowElement.prototype.addAChild = function addAChild(parent, value)
{
	if (parent.currentWindow) // if parent is a real display node, add a row to the table
	{
		makeAssertion(parent.isSimpleTable, 'SimpleTableRowElement must be a child of SimpleTable');
		makeAssertion(this.isRepetitive, 'Row element of SimpleTable must be repetitive');
		//Create a body row 
		var row = parent.createHTMLElement('tr');
		var rowPrototype = tersus.repository.get(this.modelId).prototype;
		if (rowPrototype.elementList)
		{
			for (var i=0; i< rowPrototype.elementList.length;i++)
			{
				var cell = parent.createHTMLElement('td');
				var cellElement = rowPrototype.elementList[i];
				var cellValue = cellElement.getChild(value);
				if (cellValue && (cellValue.leafValue !== null && cellValue.leafValue !== undefined))
					cell.innerHTML = tersus.escapeHTML(''+cellValue.leafValue); // convert to String
				row.appendChild(cell);				
			}
		}
		parent.bodyNode.appendChild(row);
		var selectable = parent.sharedProperties.selectable == true; 
		if (selectable)
		{
			row.setAttribute(SimpleTable.ROW_NODE_ID,value.getNodeId());
			row.setAttribute(SimpleTable.TABLE_NODE_ID,parent.getNodeId());
			row.onclick = SimpleTable.handleRowClick;
		}
	}
	Element.prototype.addAChild.call(this, parent, value);

};
SimpleTableRowElement.prototype.removeChildren = function removeChildren(parent)
{
	Element.prototype.removeChildren.call(this, parent);
	if (parent.currentWindow && parent.viewNode) // if parent is a real display node, add a row to the table
	{
		var table=parent.viewNode;
		parent.selectedRow = null;
		if (table && table.tBodies.length)
		{
			var body = table.tBodies[0];
			while (body.rows.length > 0)
			{
				body.removeChild(body.rows[0]);
			}
		}
	}
};
Table.prototype['remove<Selected Row>'] = function ()
{
    if (this.selectedHTMLRow)
        $(this.selectedHTMLRow).removeClass('selected');
    this.selectedRow = null;
    this.selectedHTMLRow = null;
    
        
};

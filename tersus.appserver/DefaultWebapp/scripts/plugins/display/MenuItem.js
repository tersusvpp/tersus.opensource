/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.MenuItem = DisplayNode.__subclass('tersus.MenuItem');
tersus.MenuItem.prototype.onload = function () {
 this.imagePath = this.sprop('imagePath');
 };
tersus.MenuItem.prototype.defaultTag = 'span';
tersus.MenuItem.prototype.defaultStyleClass='menu_item';
tersus.MenuItem['set<Image Path>'] = function(path)
{
	this.imagePath = path;
	if (this.image)
		this.image.src = path;
};
function menuOption()
{
};
tersus.MenuItem.prototype.createViewNode = function()
{
	DisplayNode.prototype.createViewNode.call(this);
	if (this.imagePath)
	{	
		this.image = this.createHTMLChildElement(this.viewNode,'img');
		this.image.src = this.imagePath;
	}
	this.link = this.createHTMLChildElement(this.viewNode,'a');
    this.link.href='javascript:menuOption("'+this.getCaption()+'")';
    this.link.onfocus = function(){this.className='focus';};
    this.link.onblur = function(){this.className='';};
		
	this.captionChanged();
};
tersus.MenuItem.prototype.captionChanged = function()
{
	if (this.link)
		this.link.innerHTML = this.escapeHTML(this.getCaption());
};

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.NewTree = function NewTree(){};
tersus.NewTree.prototype = new DisplayNode();
tersus.NewTree.prototype.constructor = tersus.NewTree;
tersus.NewTree.prototype.defaultTag = 'div';
tersus.NewTree.prototype.defaultStyleClass = 'ntree';
tersus.NewTree.prototype.isNewTree = true;
tersus.NewTree.prototype['get<Selected Item>'] = function()
{
	return this.selectedItem;
};
tersus.NewTree.prototype['set<Selected Item>'] = function (item)
{
    if (item)
    {
        this.selectedItem = item;
        this.selectedItem.select();
    }
};
tersus.NewTree.prototype['set<Selected Item>'] .requiresNodes = true;

tersus.NewTree.prototype['remove<Selected Item>'] = function()
{
	if (this.selectedItem)
	{
		this.selectedItem.deselect();
		this.selectedItem = null;
	}
};
tersus.NewTreeItem = function NewTreeItem(){};
tersus.NewTreeItem.prototype = new DisplayNode();
tersus.NewTreeItem.prototype.constructor = tersus.NewTreeItem;
tersus.NewTreeItem.prototype.defaultTag = 'div';
tersus.NewTreeItem.prototype.defaultStyleClass = 'ntree_item';
tersus.NewTreeItem.TREE_ITEM_ID="tree_item_id";
tersus.NewTreeItem.prototype.COLLAPSED = 'Collapsed';
tersus.NewTreeItem.prototype.EXPANDED = 'Expanded';
tersus.NewTreeItem.prototype.ATOMIC = 'Atomic';
tersus.NewTreeItem.prototype.isNewTreeItem = true;
tersus.NewTreeItem.prototype.getTree = function()
{
	var parent = this.parent;
	while (parent.isNewTreeItem)
	{
		parent = parent.parent;
	};
	if (parent && parent.isNewTree)
	{
		return parent;
	}
	else
		return null;
};
tersus.NewTreeItem.select = function()
{
	var itemId = this.getAttribute(tersus.NewTreeItem.TREE_ITEM_ID);
	var item = tersus.findNode(itemId);
	item.select();
};

tersus.NewTreeItem.prototype.select = function()
{
	var tree = this.getTree();
	if (tree)
	{
		if (tree.selectedItem)
			tree.selectedItem.deselect();
		tree.selectedItem = this;
		if (this.innerHeader)
//			this.innerHeader.viewNode.className = "ntree_item_inner_header_selected";
			this.innerHeader.addStyleClass("ntree_item_inner_header_selected");
	};
};
tersus.NewTreeItem.prototype.deselect = function()
{
		if (this.innerHeader)
//			this.innerHeader.viewNode.className = "ntree_item_inner_header";
			this.innerHeader.removeStyleClass("ntree_item_inner_header_selected");
};
tersus.NewTreeItem.prototype.deleteMe = function()
{
	DisplayNode.prototype.deleteMe.call(this);
	var tree = this.getTree();
	if (tree && tree.selectedItem == this)
		tree.selectedItem = null;
};
tersus.NewTreeItem.prototype.createViewNode = function()
{
	DisplayNode.prototype.createViewNode.call(this);
	this.header = this.createHTMLChildElement(this.viewNode,'span','ntree_item_header');
	this.toggleImage = this.createHTMLChildElement(this.header, 'img', 'ntree_toggle_collapsed');
	this.body = this.createHTMLChildElement(this.viewNode, 'div', 'ntree_item_body');
};
tersus.NewTreeItem.prototype.initViewNode = function()
{
	if (this.defaultStyleClass)
		this.viewNode.className = this.defaultStyleClass;
	this.setProperties();
	this.toggleImage.src = 'images/spacer.gif';
	this.setEventHandlers();
	this.setState(this.COLLAPSED);
};
tersus.NewTreeItem.prototype.onClick = function(domNode, callback)
{
	if (domNode == this.toggleImage)
		this.toggle(callback);
	else
		DisplayNode.prototype.onClick.call(this, callback);
};
tersus.NewTreeItem.prototype.toggle = function(callback)
{
	if (this.state == this.COLLAPSED)
		this.expand(callback);
	else if (this.state == this.EXPANDED)
		this.collapse(callback);
};
tersus.NewTreeItem.prototype.collapse = function(callback)
{
	queueEvent(window.engine, window.engine.handleEvent, [this,Events.ON_COLLAPSE], new Date(), "Tree Item Collapsed", callback);
};
tersus.NewTreeItem.prototype.expand = function(callback)
{
	queueEvent(window.engine, window.engine.handleEvent, [this,Events.ON_EXPAND], new Date(), "Tree Item Expanded", callback);
};
tersus.NewTreeItem.prototype.setEventHandlers = function ()
{
	this.registerEventHandler(Events.ON_CLICK, this.toggleImage);
};
tersus.NewTreeItem.prototype.appendChildViewNode = function (child, childElement)
{
	if (child.element.role == '<Header>')
	{
		this.header.appendChild(child.viewNode);
		child.viewNode.className='ntree_item_inner_header';
		this.innerHeader = child;
		child.viewNode.setAttribute(tersus.NewTreeItem.TREE_ITEM_ID, this.getNodeId());
		child.registerEventHandler(Events.ON_CLICK);
		var item = this;
		var originalClickHandler = child.onClick;
		child.onClick = function(domNode, callback)
		{
			item.select();
			originalClickHandler.call(child, domNode, callback);
		};
		
	}
	else
		DisplayNode.appendChildNode(this.body,child.viewNode);
};

tersus.NewTreeItem.prototype['get<Expanded>'] = function()
{
	return (this.expanded);
};

tersus.NewTreeItem.prototype.setState = function(state)
{
	this.state = state;
	if (state == this.EXPANDED)
	{
		this.expanded = true;
		this.expandable = true;
		this.toggleImage.className = 'ntree_toggle_expanded';
		this.body.style.display='';
	}
	else if (state == this.COLLAPSED)
	{
		this.expanded = false;
		this.expandable = true;
		this.toggleImage.className = 'ntree_toggle_collapsed';
		this.body.style.display='none';
	}
	else if (state == this.ATOMIC)
	{
		this.expandable = false;
		this.toggleImage.className = 'ntree_toggle_atomic';
		this.body.style.display='none';
	}
};
tersus.NewTreeItem.prototype['set<Expanded>'] = function(expanded)
{
	if (expanded == this.expanded)
		return;
	if (this.expandable)
	{
	
		if (expanded)
			this.setState(this.EXPANDED);
		else 
			this.setState(this.COLLAPSED);
	}
	else 
	{
		this.expanded = expanded;
		this.setState(this.ATOMIC);
	}
	
};
tersus.NewTreeItem.prototype['get<Expandable>'] = function ()
{
	return (this.expandable);
};

tersus.NewTreeItem.prototype['set<Expandable>'] = function (expandable)
{
	if (expandable == this.expandable)
		return;
	if (expandable)
	{
		if (this.expanded)
			this.setState(this.EXPANDED);
		else
			this.setState(this.COLLAPSED);
	}
	else
	{
		this.setState(this.ATOMIC);
	};
};

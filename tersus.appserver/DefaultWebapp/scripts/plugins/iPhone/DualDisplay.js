// This is not really a plugin - just a set of functions that implement flipping
tersus.DualDisplay = {

flip:function flip_dual_display(node)
{
	var parent = node.parent.parent.viewNode;

	parent.mode = 3 - parent.mode;
	tersus.DualDisplay.set(parent);
},
init: function init_dual_display(node)
{
	var parent = node.parent.parent.viewNode;
	parent.mode = 1;
	var s = parent.lastChild.style;
	s.display='none';
	setTimeout(function(){tersus.DualDisplay.set(parent);s.display='';},10);
},

set: function set_dual_display(parent)
{
	var option1 = parent.firstChild;
	var option2 = option1.nextSibling;
	var h = getVisibleSize(parent).height;
	if (parent.mode == 1)
	{
		option1.style['-webkit-transform']='none';
		option2.style['-webkit-transform']='translateY('+h+'px)';
		$(option1).css('-moz-transform','none');
		$(option2).css('-moz-transform','translate(0px,'+h+'px)');
	}
	else
	{
		option1.style['-webkit-transform']='translateY(-'+h+'px)';
		option2.style['-webkit-transform']='none';
		$(option1).css('-moz-transform','translate(0px,-'+h+'px)');
		$(option2).css('-moz-transform','none');
	}
}
};

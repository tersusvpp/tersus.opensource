/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.isIE6 = navigator.userAgent.indexOf('MSIE 6') >=0;

 //tersus.ActionDialog = function tersus_ActionDialog() {};
	tersus.ActionDialog = tersus.PopIn.__subclass('Dialog');
	tersus.ActionDialog.prototype.templateURL='actiondialog.html';
	tersus.ActionDialog.prototype.createViewNode = function()
	{
		// create mask (this gives modality by blocking UI access to existing content)
		clearSelection(this.currentWindow); // sometimes the original selection is extended to newly created elements
		var d = this.currentWindow.document;
		var r = this.getCurrentRootViewNode();
		//var ml = this.createHTMLChildElement(r,'div');
		
		// create dialog div
		var template = tersus.repository.getHTMLTemplate(this.templateURL);
		var actioDialogHtml = importNode(d,template);
		this.dialogLayer = actioDialogHtml;
		actioDialogHtml.className='confirm_screenopen';
		this.viewNode = tersus.findChildNodesByClassName(actioDialogHtml,'action_dialog_body')[0];
		this.viewNode = actioDialogHtml;
		this.isOpen = true;
		r.appendChild(actioDialogHtml);
		this.header = tersus.findChildNodesByClassName(actioDialogHtml,'action_dialog_title')[0];
		if (this.header)
		  this.header.innerHTML = tersus.escapeHTML(this.getPopupTitle());
		  
		this.onResize();
		
	};
	tersus.ActionDialog.prototype.onResize = function()
	{
		if (! (this.isOpen && this.currentWindow && ! this.currentWindow.closed && this.viewNode))
			return;
				this.onResizeChildren();
	}
	tersus.ActionDialog.prototype.doClose = function() 
	{
		delete tersus.popupNodes[this.getNodeId()];
	
		if (this.isOpen)
		{
			this.resumeNavigation();
			this.viewNode = null;
			this.header = null;
			var dl = this.dialogLayer;
			//var ml = this.maskLayer;
			this.dialogLayer=null;
			//this.maskLayer=null;
			dl.parentNode.removeChild(dl);
			//ml.parentNode.removeChild(ml);
		}
		this.isOpen = false;
	
	};

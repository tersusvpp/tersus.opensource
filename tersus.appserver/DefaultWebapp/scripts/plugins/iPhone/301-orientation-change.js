tersus.currentOrientation=window.orientation;
window.onorientationchange = function(ev)
{
	if (tersus.currentOrientation === window.orientation)
		return;
	tersus.currentOrientation=window.orientation;
	if (ev)
		tersus.currentEventAttributes = getEventAttributes(ev);
	if (window.rootDisplayNode)
		window.rootDisplayNode.queueEvent(Events.ON_ORIENTATION_CHANGE,{});
	
};

tersus.simulateOrientationChange = function()
{
	if (window.orientation==0 || window.orientation==180)
		window.orientation=90;
	else
		window.orientation=0;
	tersus.invokeAction('<On Orientation Change>',null,null,window.rootDisplayNode);
};

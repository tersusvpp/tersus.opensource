PopupCalendar.prototype.start = function()
{
    var node = this;
    var callback = function datePickerCallback(year, month, day)
    {
        if (year != null && month != null && day != null)
        {
            var date = createDate(year, month, day);
            node.chargeLeafExit('<Date>', date);
        }
        node.readyToResume();
        resumeRoot();
    };

    var date = this.getLeafChild('<Base Date>');

    this.openDatePicker(date,callback);
};

PopupCalendar.prototype.openDatePicker = function(date, callback)
{
    if (date == null)
    {
	var now = new Date();
	date = createDate(now.getFullYear(),now.getMonth()+1,now.getDate());
    }
    var days = { };
    var years = { };
    var months = { 1: 'Jan', 2: 'Feb', 3: 'Mar', 4: 'Apr', 5: 'May', 6: 'Jun', 7: 'Jul', 8: 'Aug', 9: 'Sep', 10: 'Oct', 11: 'Nov', 12: 'Dec' };
    for (var key in months)
    {
	var t = tersus.labelMap['Calendar.'+months[key]];
	if (t) months[key]=t;
    }

    for( var i = 1; i < 32; i += 1 ) {
	days[i] = i;
    }

    for(var i = date.year-20; i < date.year+10; i += 1 ) {
	years[i] = i;
    }

    var prevDisabled = tersus.disabled;
    SpinningWheel.addSlot(years, 'right', date.year);
    SpinningWheel.addSlot(months, '', date.month);
    SpinningWheel.addSlot(days, 'right', date.day);

    /* Block click and touch event from outside the date selector */
    var blocker  = function(e)
    {
        if (!self.wrapper.contains(e.target))
            e.stopPropagation();
    };
    var self = this;
    var unblock = function()
    {
        document.removeEventListener("click", blocker, true);
        document.removeEventListener("touchstart", blocker, true);
        document.removeEventListener("touchmove", blocker, true);
        document.removeEventListener("touchend", blocker, true);
    }
    SpinningWheel.setCancelAction(function(){unblock();callback();});
    SpinningWheel.setDoneAction(function()
				{
				    try
				    {
					var keys = SpinningWheel.getSelectedValues().keys;
					var year = parseInt(keys[0]);
					var month = parseInt(keys[1]);
					var day = parseInt(keys[2]);
					unblock();
					callback(year, month, day);
				    }
				    catch (e)
				    {
					tersus.error(e);
				    }
				});
    SpinningWheel.open();
    this.wrapper = SpinningWheel.swWrapper;
    document.addEventListener("click", blocker, true);
    document.addEventListener("touchstart", blocker, true);
    document.addEventListener("touchmove", blocker, true);
    document.addEventListener("touchend", blocker, true);
};

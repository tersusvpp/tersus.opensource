tersus.newAction('WatchAccelerometer', function()
{
	tersus.WatchAccelerometer.watcherId = this.getNodeId();
	var threshold = this.getLeafChild('<Threshold>');
	if (threshold == null)
		threshold = 0.5;
	document.location='tersus://WatchAccelerometer/'+threshold;
});

tersus.WatchAccelerometer.busy = false;
var count = 0;
tersus.WatchAccelerometer.handleResponse = function(x, y, z, timestamp)
{
	++count;
	if (tersus.WatchAccelerometer.busy)
		return;
	tersus.WatchAccelerometer.busy = true;	
	var node = window.rootDisplayNode;
	var actionElement = node.getElement('<On Acceleration>');
	if (!actionElement)
		modelError('Missing element <On Acceleration>');
	var process = new BackgroundProcess(node, actionElement);
	process.setTriggerLeafValue('<X>',x);
	process.setTriggerLeafValue('<Y>',y);
	process.setTriggerLeafValue('<Z>',z);
	process.setTriggerLeafValue('<Timestamp>',timestamp);
			
	process.whenDone(function()
	{
		tersus.WatchAccelerometer.busy = false;
	});
	process.start();
};

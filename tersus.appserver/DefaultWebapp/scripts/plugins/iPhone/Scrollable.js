/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 
function Scrollable() {};
Scrollable.prototype = new tersus.HTMLTag();
Scrollable.prototype.constructor = Scrollable;
Scrollable.prototype.defaultTag = 'div';
Scrollable.prototype.defaultStyleClass  = 'Scrollable';

//Scrollable.prototype.create = function()
Scrollable.prototype.copyValueOpt = function(value, nodeMap, htmlV, legacyNodes)
{
	
	tersus.HTMLTag.prototype.copyValueOpt.call(this,value, nodeMap, htmlV, legacyNodes)
	var scrollable = this;
	tersus.async.exec(function() {scrollable.calliScroll();} , false /* don't wait */);
};

Scrollable.prototype.calliScroll = function()
{
	if ( iScroll)
	{
		new iScroll(this.viewNode.firstChild);
	
	}
}

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
ScrollPane.prototype = 
function Scrollable() {};
ScrollPane.prototype = new tersus.HTMLTag();
ScrollPane.prototype.constructor = ScrollPane;
ScrollPane.prototype.defaultTag = 'div';
ScrollPane.prototype.defaultClass  = 'scrollPane';
ScrollPane.prototype.defaultWrapperTags='div';
ScrollPane.prototype.defaultWrapperStyleClasses='scrollPaneWrapper';


ScrollPane.preventDefaultSet = false;

ScrollPane.prototype.onResize = function()
{
	this.setDefaultHeight();
	if (this.iScroll)
		this.iScroll.refresh();
	this.onResizeChildren();
}
ScrollPane.prototype.initIScroll = function()
{
	var node = this;

	tersus.async.exec(function()
	{
		node.setDefaultHeight();
		node.iScroll = new iScroll(node.wrapperNode);
	});
};
ScrollPane.prototype.destroy = function()
{
	if (this.iScroll && this.iScroll.destory)
	{
		this.iScroll.destory(true);
		this.iScroll=null;
	}
	tersus.HTMLTag.prototype.destroy.call(this);
};
ScrollPane.prototype.setDefaultHeight = function()
{
	var e = $(this.wrapperNode);
	while (e.length>0)
	{
		if (e.css('overflow-y')=='hidden')
		{
			$(this.wrapperNode).height(e.height());
			return;
		}
		e = e.parent();
	}
	this.iScroll.refresh();
};
ScrollPane.prototype.copyValueOpt = function(value, nodeMap, htmlV, legacyNodes)
{
	tersus.HTMLTag.prototype.copyValueOpt.call(this,value, nodeMap, htmlV, legacyNodes)
	var node = this;
	if (window.iScroll)tersus.async.exec(function(){tersus.async.exec(function() {node.initIScroll();});});
};


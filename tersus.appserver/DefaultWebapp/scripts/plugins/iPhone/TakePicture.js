tersus.SAMPLE_IMAGE='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAIAAAD/gAIDAAAAAXNSR0IArs4c6QAAANNJREFUeNrt3NEJgDAMQEErruGgOoAO6iB1hUoJsfTevx8eERpBS611UVsrAliwYMGCBQsBLFiwYMGChQAWLFiwYMFC0N7Wc/G5HyPe8/XcJstjCAsWLMGCBQvWjCf4oINyZ3F7hcmCBQsWLFiCBQsWLOtO8FKStUiZLFiwYMGCJViwYMGCBUuwYMGCBQuWYH0o7R181nt0kwULFixYggUL1lSH0kG/czVZsGDBggVLsGDB+nPF/+BNFixYsGDBQgALFixYsGAhgAULFixYsBDACukFZWESy0+VwUsAAAAASUVORK5CYII=';
tersus.newAction('TakePicture', function()
{
	if (tersus.simulateCamera != false)
	{
		if(window.confirm('No camera available.  Would you like to use a sample image?'))
		{
			var exit = this.getExit('<Data URL>') || this.getExit('<File URI>');
			this.chargeLeafExit(exit,tersus.SAMPLE_IMAGE);
		}
		else
			this.chargeEmptyExit('<None>');
	}
	else
		modelExecutionError('No camera available.');		
});

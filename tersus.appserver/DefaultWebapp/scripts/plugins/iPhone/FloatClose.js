/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.FloatingClose = function tersus_DialogFooter() {};
tersus.FloatingClose.prototype = new Row();
tersus.FloatingClose.prototype.constructor = tersus.FloatingClose;
tersus.FloatingClose.prototype.attachViewNode = function DialogFooter_attachViewNode()
{
	
	//this.divNode = this.createHTMLElement('div');
	
	//this.divNode.appendChild(this.viewNode);
	if (this.parent&& this.parent.close)
	{
		this.parent.close.appendChild(this.viewNode);
	}

}
//PopupFooter=tersus.FloatingClose; // For backward compatability
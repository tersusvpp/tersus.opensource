/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Create = function tersus_Create() {};
tersus.Create.prototype = new ActionNode();
tersus.Create.prototype.constructor = tersus.Create;
tersus.Create.prototype.start = function ()
{
	var modelId = this.getLeafChild('<Model Id>');
	var exit = this.getElement("<Object>");
	if (exit == null)
		return; // This can happen if user doesn't have permission for target type
	var modelNotFoundExit = this.getElement("<Model Not Found>");

	if (modelId != null)
	{
		var	constructor = tersus.repository.get(modelId);
		if (constructor!=null)
		{
			var value = new constructor();
			this.chargeExit(exit, value);
		}
		else
		{
			if (modelNotFoundExit!=null)
				this.chargeLeafExit(modelNotFoundExit.role,modelId);
			else
				modelExecutionError("Model not found:"+modelId,this);
				
		}
	}
	else
	{
		var value = exit.createChildInstance();
		this.chargeExit(exit, value);
	}
};

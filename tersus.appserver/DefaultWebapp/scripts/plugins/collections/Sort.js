tersus.newAction('Sort', function()
{
	var ORDER_BY='<Order By>';
	var items = this.getTriggerValues(ORDER_BY);
	var orderBy = this.getLeafChild(ORDER_BY);
	
	
	var comparator;
	if (orderBy == null || orderBy.match(/^asc(ending)?$/i))
	{
		comparator = function(o1,o2)
		{
			return tersus.compare(o1, o2);
		};
	}
	else if (orderBy.match(/^desc(ending)?$/i))
	{
		comparator = function(o1, o2)
		{
			return tersus.compare(o2, o1);
		};
	}
	else
	{
		var clauses = orderBy.split(/\s*,\s*/);
		var sortComponents = [];	
		for (var i=0; i< clauses.length; i++)
		{
			var clause = clauses[i];
			var path = clause;
			var desc  = false;
			var parts = clause.split(" ");
			if (parts.length>1)
			{
				var suffix = parts[parts.length-1];
				if (suffix.match(/^asc(ending)?$/i))
				{
					path = $.trim(clause.substring(0, clause.length-suffix.length));
				}
				else if (suffix.match(/^desc(ending)?$/i))
				{
					path = $.trim(clause.substring(0, clause.length-suffix.length));
					desc = true;
				}
			}			
			sortComponents.push(new tersus.Sort.Component(path, desc));
		}
		comparator = function(o1, o2)
		{
			for (var i=0;i<sortComponents.length;i++)
			{
				var cc = sortComponents[i].compare(o1,o2);
				if (cc !=0)
					return cc;
			}
			return 0;
		};

	}
	
	tersus.quicksort(items, comparator);
	
	var e = this.getExit('<Sorted Items>');
	for (var i=0;i<items.length;i++)
	{
		this.accumulateExitValue(e, items[i]);
	};
});
tersus.Sort.Component = function(pathStr, desc)
{
	this.path= new Path(pathStr);
	this.desc=desc;
};
tersus.Sort.Component.prototype.compare = function(o1, o2)
{
	if (this.desc)
	{
		var t = o1;
		o1 = o2;
		o2 = t;
	}
	if (o1 == null && o2 == null)
		return 0;
	if (o1 == null)
		return -1;
	if (o2 == null)
		return 1;
		
	var l1 = o1.getValues(this.path);
	var l2 = o2.getValues(this.path);
	if (l1.length > 1 || l2.length>1)
		throw "Can't compare repetitive elements";
	var obj1 = l1[0] || null;
	var obj2 = l2[0] || null;
	return tersus.compare(obj1, obj2);
};



tersus.swap=function(a, i, j) {	var t=a[i];a[i]=a[j];a[j]=t;};
tersus.partition = function(a, left, right, piv, comp)
{
	var pv=a[piv];
	tersus.swap(a, piv, right-1); // Put pivot value at the end
	var pos=left;
	for(var i=left; i<right-1; i++) {
		if(comp(a[i],pv)<=0) {
			tersus.swap(a, pos, i);
			++pos;
		}
	}
	tersus.swap(a, right-1, pos);

	return pos;
};
tersus._quicksort = function(a, left, right, comp)
{
	if(right-1>left) {
		var piv=left+Math.floor(Math.random()*(right-left));

		piv=tersus.partition(a, left, right, piv, comp); // position is updated in 'partition'

		tersus._quicksort(a, left, piv, comp);
		tersus._quicksort(a, piv+1, right, comp);
	}
};
tersus.quicksort = function(array, comparator)
{
	tersus._quicksort(array, 0, array.length, comparator);
};

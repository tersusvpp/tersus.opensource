/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
tersus.RemoveNumberedItem = function RemoveNumberedItem(){};
tersus.RemoveNumberedItem.prototype = new ActionNode();
tersus.RemoveNumberedItem.prototype.constructor = tersus.RemoveNumberedItem;
tersus.RemoveNumberedItem.prototype.start = function()
{
	var parent = this.getChild('<Parent>');
	if (!parent.elementList)
	{
		if (this.getElement('<Invalid Parent>'))
			this.chargeExit('<Invalid Parent>', parent);
		else
			modelExecutionError("Invalid parent "+parent.modelId,this);
		return;
	}
	
	var e = this.getTargetList(parent);
	if (!e)
	{
		if (this.getElement('<Invalid Parent>'))
			this.chargeExit('<Invalid Parent>', parent);
		else
			modelExecutionError("Invalid parent "+parent.modelId,this);
			
		return;
	}
	
	var index = this.getLeafChild('<Index>');
	if (index < 0 || index > e.getChildren(parent).length)
	{
		if (this.getElement('<Invalid Index>'))
			this.chargeLeafExit('<Invalid Index>', index);
		else
			modelExecutionError("Invalid index "+index,this);
		return;
	}
	
	if (this.getElement('<Removed Item>'))
	{
		var children = e.getChildren(parent);
		this.chargeLeafExit('<Removed Item>', children[index-1]);
	}
	
	e.removeNumberedChild(parent, index);
};

tersus.RemoveNumberedItem.prototype.getTargetList = function(parent)
{
	var elementName = this.getLeafChild('<Element Name>');
	if (parent && parent.elementList)
	{
		var elements = parent.elementList;
		for (var i=0;i<elements.length;i++)
		{
			if (elementName != null)
			{
				if (elements[i].isRepetitive && elementName == elements[i].role)
					return elements[i];
			}
			else if (elements[i].isRepetitive)
					return elements[i];
		}
	}
	return null;
};


tersus.newAction('UniqueItems', function()
{
	var items = this.getTriggerValues();
	var output = [];
	
	for (var i=0;i<items.length;i++)
	{
		var item = items[i];
		var alreadyListed = false;
		for (var j=0;j<output.length;j++)
		{
			if (compareValues(item, output[j]))
			{
				alreadyListed = true;
				break;
			}
		}
		if (!alreadyListed)
			output.push(item);
	}
	var e = this.getExit('<Unique Items>');
	for (var i=0;i<output.length;i++)
	{
		this.accumulateExitValue(e, output[i]);
	};
});


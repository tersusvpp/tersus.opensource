tersus.newAction('Convert',function() {
    var input = this.getChild('<Input>');
    var strict = this.getLeafChild('<Strict>');
    var exit = this.getExit('<Output>');

    if (input.isMap)
    {
	var output = exit.createChild(this);
	output.copyObject(input.leafValue,strict);
    }
    else
	throw "Convert currently only supports map as input";
});

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Last = function tesrsus_Last(){};
tersus.Last.prototype = new ActionNode();
tersus.Last.prototype.constructor = tersus.Last;
tersus.Last.prototype.start = function ()
{
	var trigger = this.getElement('<List>');
	var list = trigger.getChildren(this);
	if (list&& list.length > 0)
	{
		var last = list[list.length-1];
		this.chargeExit('<Last>', last);
		
	}
};

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 //Exits - checks if a trigger value was received

Exists = function Exists() {};
Exists.prototype = new ActionNode();
Exists.prototype.constructor = Exists;
Exists.prototype.start = function startExists()
{
	var value = null;
	if (this.triggers)
	{
		for (var i=0; i< this.triggers.length; i++)
		{
			var trigger = this.triggers[i];
			if (trigger.isRepetitive)
			{
				var values = trigger.getChildren(this);
				if (values && values.length>0)
				{
					value = values[0];
					break;
				}
			}
			else
			{
				value = trigger.getChild(this);
				if (value != null)
					break;
			}
		}
	}
	if (value != null)
	{
		this.chargeExit('<Yes>', value);
		
	}
	else
	{
		this.chargeEmptyExit('<No>');
	}
};
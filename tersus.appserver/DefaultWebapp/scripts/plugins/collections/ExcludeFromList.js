/*********************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
tersus.newAction('ExcludeFromList', function()
{
	var listTrigger = this.getElement('<List>');
	if (!listTrigger)
	{
		modelError("Missing trigger <List>");
		return;
	}
	
	var list = listTrigger.getChildren(this);
	var excludeTrigger = this.getElement('<Exclude>');
	
	if (list)
	{
		var excludeList;
		if (excludeTrigger.isRepetitive)
			excludeList = excludeTrigger.getChildren(this); 
		else
			excludeList = [excludeTrigger.getChild(this)];
		
		
		for (var i = 0; i < list.length; i++)
		{
			var toExclude = false;
			for (var j = 0; j< excludeList.length; j++)
			{	
				if (compareValues(excludeList[j], list[i]))
				{
					toExclude = true;
					break;
				}
			}
			
			if (!toExclude)
				this.accumulateExitValue('<Remaining>', list[i]);
		}
	}
});

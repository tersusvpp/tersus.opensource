/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Count = function tersus_Count(){};
tersus.Count.prototype = new ActionNode();
tersus.Count.prototype.constructor = tersus.Count;
tersus.Count.prototype.start = function ()
{
	var values = this.getTriggerValues();
	this.chargeLeafExit('<Occurrences>',values.length);
};

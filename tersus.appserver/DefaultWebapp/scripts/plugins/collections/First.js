/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.First = function tesrsus_First(){};
tersus.First.prototype = new ActionNode();
tersus.First.prototype.constructor = tersus.First;
tersus.First.prototype.start = function ()
{
	var trigger = this.getElement('<List>');
	var list = trigger.getChildren(this);
	if (list&& list.length > 0)
	{
		var first = list[0];
		this.chargeExit('<First>', first);
		
	}
};

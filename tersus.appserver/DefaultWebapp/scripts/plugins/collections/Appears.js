/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Appears = function Appears(){};
tersus.Appears.prototype = new ActionNode();
tersus.Appears.prototype.constructor = tersus.Appears;
tersus.Appears.prototype.start = function()
{
	var list = this.getTriggerValues('<Item>');
	var item = this.getChild('<Item>');
	var found = false;
	for (var i=0; i<list.length; i++)
	{
		var v = list[i];
		if (compareValues(v, item))
		{
			found = true;
			break;
		}
	}
	if (found)
		this.chargeExit('<Yes>', item);
	else
		this.chargeExit('<No>', item); 
		
};

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Put = function tersus_Put() {};
tersus.Put.prototype = new ActionNode();
tersus.Put.prototype.constructor = tersus.Put;
tersus.Put.prototype.start = function ()
{
	var map = this.getChild("<Map>");
	if (map && map.leafValue)
		map = map.leafValue;
	
	var key = this.getChild("<Key>");
	if (key !== null && key !== undefined && key.leafValue !== undefined)
		key = key.leafValue;
	var value = this.getChild("<Value>");
	if (value !== null && value.leafValue !== undefined)
		value = value.leafValue;
	if (map && (key !== null && key !== undefined))
	{
		if (value === null || value === undefined)
			delete map[key];
		else
			map[key]=value;
	}
};

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.newAction('GetMapEntry', function()
{
	var map = this.getChild("<Map>");
	if (map && map.leafValue)
		map = map.leafValue;
	
	var key = this.getChild("<Key>");
	if (key && key.leafValue != null)
		key = key.leafValue;
		
	var value = map[key];
	if (value)
	{
		if (value.isNode)
		    this.chargeExit('<Value>',value);
	        else
		    this.chargeLeafExit('<Value>',value);
	}
	else
	    this.chargeEmptyExit('<None>',value);	
});

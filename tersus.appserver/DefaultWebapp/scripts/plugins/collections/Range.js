/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function Range() {};
Range.prototype = new ActionNode();
Range.prototype.constructor = Range;
Range.prototype.start = function ()
{
	var limit1 = parseInt(this.getLeafChild('<Limit 1>'));
	var limit2 = parseInt(this.getLeafChild('<Limit 2>'));
	
	var min = Math.min(limit1, limit2);
	var max = Math.max(limit1, limit2);
	for (var i = min; i<=max; i++)
	{
		this.setLeafChild('<Numbers>', i, Operation.ADD);
	}	
};

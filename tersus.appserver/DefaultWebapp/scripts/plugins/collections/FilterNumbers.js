/*********************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
tersus.newAction('FilterNumbers', function()
{
	var numbersTrigger = this.getElement('<Numbers>');
	var numbers = numbersTrigger.getChildren(this);
	var filter = this.getChild('<Filter>');
	
	var filteringNumber = null;
	if (this.triggers)
	{
		for (var i=0; i< this.triggers.length; i++)
		{
			var trigger = this.triggers[i];
			var triggerRole = trigger.role;
			if (!('<Numbers>' == triggerRole) && !('<Filter>' == triggerRole))
			{
				filteringNumber = trigger.getChild(this);
				break;
			}
		};
	}
	
	if (numbers)
	{
		var filterValue = filter.leafValue;
		for (var i=0; i< numbers.length; i++)
		{
			var number = numbers[i].leafValue;
				
			if (filterValue.substring(0,2) == "<=" && number <= filteringNumber)
				this.accumulateExitValue('<Matching>', numbers[i]);
			else if (filterValue.substring(0,1) == "<" && number < filteringNumber)
				this.accumulateExitValue('<Matching>', numbers[i]);
			else if (filterValue.substring(0,1) == "=" && number == filteringNumber)
				this.accumulateExitValue('<Matching>', numbers[i]);
			else if (filterValue.substring(0,2) == ">=" && number >= filteringNumber)
				this.accumulateExitValue('<Matching>', numbers[i]);
			else if (filterValue.substring(0,1) == ">" && number > filteringNumber)
				this.accumulateExitValue('<Matching>', numbers[i]);
		};
	}
});

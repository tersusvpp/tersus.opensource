/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.GetNumberedItem = function tersus_GetNumberedItem() {};
tersus.GetNumberedItem.prototype = new ActionNode();
tersus.GetNumberedItem.prototype.constructor = tersus.GetNumberedItem;
tersus.GetNumberedItem.prototype.start = function ()
{
	var listTrigger = this.getElement('<List>');
	if (!listTrigger)
	{
		modelError("Missing trigger <List>");
		return;
	}
	var list = listTrigger.getChildren(this);
	if (list)
	{
		var index = this.getLeafChild('<Index>');
		if (index == null)
		{
			modelExecutionError('No <Index> specified',this);
			return;
		}
		if (index>=1 && index <= list.length)
			this.chargeExit('<Item>', list[index-1]);
			
		else if (this.getElement('<Invalid Index>'))
			this.chargeLeafExit('<Invalid Index>',index);
		else
		{
			modelExecutionError('Invalid trigger '+index+' (list size is '+list.length + ')', this)
			return;
		}
	}
};

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function FindElements() {};
FindElements.prototype = new ActionNode();
FindElements.prototype.SCOPE_TRIGGER_ROLE='<Scope>';
FindElements.prototype.OPTIONS_TRIGGER_ROLE='<Options>';
FindElements.prototype.MAX_RESULTS='<Max Results>';
FindElements.prototype.constructor = FindElements;

FindElements.prototype.start = function ()
{
	var filter = this.createFilter();
	var items = [];
        var maxResults = this.getLeafChild(this.MAX_RESULTS);
	var scope = this.getChild(this.SCOPE_TRIGGER_ROLE);
	if (scope)	
		scope.scanChildren(filter, {}, items, maxResults);
	else
	{
		// NO scope specified - scan everywhere
		window.rootDisplayNode.scanChildren(filter, {}, items);
		tersus.unregisterPopupNodes();
		for (var popupId in tersus.popupNodes)
		{
			node = tersus.popupNodes[popupId].scanChildren(filter, {}, items);
		}
	}
	if (items.length == 0)
	{
		var exit = this.getElement('<None>');
		if (exit)
			this.chargeEmptyExit(exit);
	}
	else
	{
		for (var i=0; i<items.length; i++)
		{
			var item = items[i];
			this.accumulateExitValue('<Elements>',item);
		}
	};
};

FindElements.prototype.createFilter = function()
{
	var filter = {};
	filter.components = [];
	var exit = this.getElement('<Elements>');
	var options = this.getChildren(this.OPTIONS_TRIGGER_ROLE);
	var ignoreCase=false;
	var useRegex=false;
	var allowPartial=false;
	for (var i=0;i<options.length;i++)
	{
		var v = options[i].leafValue;
		if (v.match(/ignore case/i))
			ignoreCase=true;
		if (v.match(/regular expressions/i))
			useRegex=true;
		if (v.match(/allow partial/i))
			allowPartial=true;
	}
	filter.modelId = exit.modelId;
	if (this.triggers)
		for (var i=0; i< this.triggers.length; i++)
		{
			var trigger = this.triggers[i];
			if (trigger.role != this.SCOPE_TRIGGER_ROLE && trigger.modelId != BuiltinModels.NOTHING_ID && trigger.role != this.OPTIONS_TRIGGER_ROLE && trigger.role != this.MAX_RESULTS)
			{
				var value = trigger.getChild(this);
				
				if (useRegex && value.isText)
				{
					var modifier = ignoreCase?'i':'';
					var pattern=value.leafValue;
					if (!allowPartial)
						pattern='^'+pattern+'$';
					var r = new RegExp(pattern, modifier);
					filter.components.push ({role:trigger.role,value:null,regex:r});
					
				}
				else
					filter.components.push ({role:trigger.role,value:value,regex:null});
			};
		};
	filter.match = function(item)
	{
		if (filter.modelId != tersus.ANYTHING && item.modelId != this.modelId)
			return false;
		for (var i =0; i<this.components.length;i++)
		{
			var component = this.components[i];
			if (component.role == '<Element Name>')
			{
				if (!item.element)
					return false;
				var role = item.element.role;
				if (component.regex)
				{
					if (!component.regex.test(role))
						return false;
				}
				else
				{	
					var testValue = component.value.leafValue;
					if (ignoreCase)
					{
						if (role.toUpperCase() != testValue.toUpperCase())
							return false;
					}
					else
					{
						if (role != component.value.leafValue)
							return false;
					}
				}
			}
			else
			{
				var element = item.getElement(component.role)
				if (element)
				{
					var value = element.getChild(item);
					if (value && value.isDataNode)
						value = value.value;  // Get the underlying value (not the wrapper)
					if (component.regex)
					{
						if (value == null || !component.regex.test(value.leafValue))
							return false;
					}
					else
					{
						if (!compareValues(value, component.value, ignoreCase))
							return false;
					}
						
				}
				else
				{ 
					return false;
				}
			}
		}
		return true;
	};
	return filter;
};

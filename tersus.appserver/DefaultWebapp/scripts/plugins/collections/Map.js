/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Map = function tersus_Map() {};
tersus.Map.prototype = new DataValue();
tersus.Map.prototype.constructor = tersus.Map;
tersus.Map.prototype.isMap = true;
tersus.Map.prototype.isLeaf = true;
tersus.Map.prototype.init = function initMap()
{
	this.leafValue = {};
};

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.newAction('PutMapEntry', function()
{
	var map = this.getChild("<Map>");
	if (map && map.leafValue)
		map = map.leafValue;
	var key = this.getChild("<Key>");
	if (key && key.leafValue != undefined)
		key = key.leafValue;
	var value = this.getChild("<Value>");
	if (map && key != null)
	{
		if (value == null)
			delete map[key];
		else
			map[key]=value;
	}
});

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.InsertNumberedItem = function InsertNumberedItem(){};
tersus.InsertNumberedItem.prototype = new ActionNode();
tersus.InsertNumberedItem.prototype.constructor = tersus.InsertNumberedItem;
tersus.InsertNumberedItem.prototype.start = function()
{
	var item = this.getChild('<Item>');
	var parent = this.getChild('<Parent>');
	if (!parent.elementList)
	{
		if (this.getElement('<Invalid Parent>'))
			this.chargeExit('<Invalid Parent>', parent);
		else
			modelExecutionError("Invalid parent "+parent.modelId,this);
		return;
	}
	var index = this.getLeafChild('<Index>');
	
	indexDelta = 0;
	if (this.pluginVersion && this.pluginVersion >= 1)
	{
		indexDelta = -1;
		index = index + indexDelta;
	}
		
	var e = this.getTargetElement(parent,item);
	if (!e)
	{
			if (this.getElement('<Invalid Item>'))
				this.chargeExit('<Invalid Item>', item);
			else
				modelExecutionError("Invalid item "+item.modelId,this);
			return;
	}
	if (index < 0 || index > e.getChildren(parent).length)
	{
		if (this.getElement('<Invalid Index>'))
			this.chargeLeafExit('<Invalid Index>', index-indexDelta);
		else
			modelExecutionError("Invalid index "+(index-indexDelta),this);
		return;
	}
	e.addAChild(parent, item);
	var children = e.getChildren(parent);
	var child = e.moveChild(parent,children.length-1, index);
	this.chargeExit('<Inserted>',child);
	
};
tersus.InsertNumberedItem.prototype.getTargetElement = function(parent,item)
{
	var triggerId = this.getElement('<Item>').modelId;
	if (parent && parent.elementList)
	{
		var elements = parent.elementList;
		for (var i=0;i<elements.length;i++)
		{
			if (elements[i].isRepetitive && (elements[i].modelId == item.modelId || elements[i].modelId == triggerId))
			{
				return elements[i];
			}
		}
	}
	return null;
};


/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function Today() {};
Today.prototype = new ActionNode();
Today.prototype.constructor = Today();

Today.prototype.start = function()
{
	var now = new Date();
	var d  = now.getDate();
	var m = now.getMonth()+1;
	var y  = now.getFullYear();
	var today = createDate(y,m,d);
	this.chargeLeafExit('<Today>',today);
};

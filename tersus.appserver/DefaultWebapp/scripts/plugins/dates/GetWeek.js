/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function GetWeek() {};
GetWeek.prototype = new FlowNode();
GetWeek.prototype.constructor = GetWeek;
GetWeek.prototype.start = function getWeek()
{
	var date = this.getChild('<Date>');
	if (!date)
		return;
	var year = date.getYear();
	var month = date.getMonth();
	var day = date.getDay();
	// Algorithm from http://www.tondering.dk/claus/calendar.html

	var a = Math.floor((14-month) / 12);
	var y = year + 4800 - a;
	var m = month + 12 * a - 3;
	var b = Math.floor(y/4) - Math.floor(y/100) + Math.floor(y/400);
	var J = day + Math.floor((153 * m + 2) / 5) + 365 * y + b - 32045;
	var d4 = (((J + 31741 - (J % 7)) % 146097) % 36524) % 1461;
	var L = Math.floor(d4 / 1460);
	var d1 = ((d4 - L) % 365) + L;
	var week = Math.floor(d1/7) + 1;
	var weekYear = year;
	if (week == 1 && month == 12)
		weekYear = year+1;
	if (week >= 52 && month == 1)
		weekYear = year-1;
		
	this.setLeafChild('<Year>', weekYear, Operation.REPLACE);
	this.setLeafChild('<Week Number>', week, Operation.REPLACE);		
};

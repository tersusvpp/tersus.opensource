/*********************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
tersus.newAction('TimeDifference', function()
{
	var from = this.getChild('<From>');
	var fromDate = tersus.getDate(from);
	var to = this.getChild('<To>');
	var toDate = tersus.getDate(to);
	
	var differenceMilliseconds = toDate - fromDate;
	var differenceSeconds = differenceMilliseconds/1000.0;
    var differneceMinutes = differenceSeconds/60.0;
    var differenceHours = differneceMinutes/60.0;
    var differenceDays = differenceHours/24.0;
    
    this.chargeLeafExit('<Days>', differenceDays);	
	this.chargeLeafExit('<Hours>', differenceHours);
	this.chargeLeafExit('<Minutes>', differneceMinutes);
	this.chargeLeafExit('<Seconds>', differenceSeconds);
	this.chargeLeafExit('<Milliseconds>', differenceMilliseconds);
});
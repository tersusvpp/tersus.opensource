/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.newAction('SplitDateAndTime', function()
{
	var d = this.getLeafChild('<Date and Time>');
	if (d==null)
		d = this.getLeafChild('<Date>');
	if (d==null)
		return;	
	if (d.isTersusDate)
	{
		var date=d;
		this.chargeLeafExit('<Year>',date.year);	
		this.chargeLeafExit('<Month>',date.month);
		this.chargeLeafExit('<Day>',date.day);
		if (this.elements['<Day of Week>'])
		{
			var d = new Date(date.year, date.month-1, date.day);
			this.chargeLeafExit('<Day of Week>',d.getDay()+1);
		} 
		this.chargeLeafExit('<Hour>',0);
		this.chargeLeafExit('<Minute>',0);
		this.chargeLeafExit('<Second>',0);
		this.chargeLeafExit('<Millisecond>',0);
	}
	else
	{
		this.chargeLeafExit('<Year>',d.getFullYear());	
		this.chargeLeafExit('<Month>',d.getMonth()+1);
		this.chargeLeafExit('<Day>',d.getDate());
		this.chargeLeafExit('<Day of Week>',d.getDay()+1);
		this.chargeLeafExit('<Hour>',d.getHours());
		this.chargeLeafExit('<Minute>',d.getMinutes());
		this.chargeLeafExit('<Second>',d.getSeconds());
		this.chargeLeafExit('<Millisecond>',d.getMilliseconds());
	}
});
tersus.SplitDate = tersus.SplitDateAndTime;


/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.newAction('ConstructDateAndTime', function()
{
	var y = this.getLeafChild('<Year>');
	var m = this.getLeafChild('<Month>');
	var d = this.getLeafChild('<Day>');
	var h = this.getLeafChild('<Hour>');
	var mi = this.getLeafChild('<Minute>');
	var s =  this.getLeafChild('<Second>');
	var ms =  this.getLeafChild('<Millisecond>');
	
	var dt = new Date(y,m-1, d, h, mi, s, ms);
	this.chargeLeafExit('<Date and Time>',dt);
});


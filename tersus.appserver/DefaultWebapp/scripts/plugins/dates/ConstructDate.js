/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.newAction('ConstructDate', function()
{
	var d = new tersus.DateValue();
	d.year = this.getLeafChild('<Year>');
	d.month = this.getLeafChild('<Month>');
	d.day = this.getLeafChild('<Day>');
	this.chargeLeafExit('<Date>',d);	
});


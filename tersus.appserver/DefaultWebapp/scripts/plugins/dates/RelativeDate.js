/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.newAction('RelativeDate', function()
{
	var date = tersus.getDate(this.getChild('<Base Date>')); // Using local timezone
	if (date == null)
		date = new Date();
        else
		date = new Date(date.getTime());
	var y = this.getLeafChild('<Years>');
	var m = this.getLeafChild('<Months>');
	var d = this.getLeafChild('<Days>');
	
	var oy = date.getFullYear();
	var om = date.getMonth();
	var od = date.getDate();
	if (y)
	{
		date.setFullYear(date.getFullYear()+y);
		if (om != date.getMonth()) // overflow because date was Feb 29th.  SetDate(0) brings us back to Feb 28th 
			date.setDate(0);
	}
	if (m)
	{
		date.setMonth(date.getMonth()+m);
		if (date.getDate() != od) // Month overflow - go back to last day of previous month
			date.setDate(0);
	}
	if (d)
		date.setDate(date.getDate()+d);
			
	this.chargeLeafExit('<Date>', createDate(date.getFullYear(), date.getMonth()+1, date.getDate())); //Using local timezone
	
});

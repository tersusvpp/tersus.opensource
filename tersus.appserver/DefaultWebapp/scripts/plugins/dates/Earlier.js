/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Earlier = function tersus_Earlier() {};
tersus.Earlier.prototype = new ActionNode();
tersus.Earlier.prototype.constructor = tersus.Earlier;
tersus.Earlier.prototype.start = function ()
{
	var time1 = this.getChild('<Time 1>');
	var time2 = this.getChild('<Time 2>');
	if (tersus.getDate(time1) < tersus.getDate(time2))
		this.chargeExit('<Yes>', time1);
	else
		this.chargeExit('<No>', time1);
};


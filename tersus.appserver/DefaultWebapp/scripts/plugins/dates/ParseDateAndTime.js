/*********************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
tersus.newAction('ParseDateAndTime', function()
{
	var str = this.getLeafChild('<Text>');
	var tz = this.getLeafChild('<Time Zone>');
	if (tz != null)
	{
		modelExecutionError('<Time Zone> not supported in client side version of Parse Date and Time (consider using <Time Zone Offset>)',this);
		return;
	}
	var tzOffset = this.getLeafChild('<Time Zone Offset>');
	if (tzOffset == null)
		tzOffset=-10000;
	else
		tzOffset=-tzOffset; // GWT timezone offset is reverse from intuitive.

	var format = this.getLeafChild('<Format>');
        var time = tersus.parseDateTimeString(str, format, tzOffset)
	this.chargeLeafExit('<Date and Time>',new Date(time));
});

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.RelativeTime = function tersus_Relative_Time() {};
tersus.RelativeTime.prototype = new ActionNode();
tersus.RelativeTime.prototype.constructor = tersus.RelativeTime;
tersus.RelativeTime.prototype.start = function ()
{
	var baseTime = this.getLeafChild('<Base Time>');
	if (baseTime == null)
		baseTime = new Date();
	else
		baseTime = new Date(baseTime.getTime());

	var delta = 0;
	delta += this.getLeafChild('<Hours>');
	delta *= 60;
	delta += this.getLeafChild('<Minutes>');
	delta *= 60;
	delta += this.getLeafChild('<Seconds>');
	delta *= 1000;
	delta += this.getLeafChild('<Milliseconds>');

	var time = baseTime.getTime() + delta;
	var newTime = new Date(time);
	this.chargeLeafExit('<Time>',newTime);
}

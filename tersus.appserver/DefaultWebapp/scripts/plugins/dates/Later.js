/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Later = function tersus_Later() {};
tersus.Later.prototype = new ActionNode();
tersus.Later.prototype.constructor = tersus.Later;
tersus.Later.prototype.start = function ()
{
	var time1 = this.getChild('<Time 1>');
	var time2 = this.getChild('<Time 2>');
	if (tersus.getDate(time1) > tersus.getDate(time2))
		this.chargeExit('<Yes>', time1);
	else
		this.chargeExit('<No>', time1);
};


/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Latest = function tersus_Latest() {};
tersus.Latest.prototype = new ActionNode();
tersus.Latest.prototype.constructor = tersus.Latest;
tersus.Latest.prototype.start = function ()
{
	var trigger = this.getElement('<Times>');
	var list = trigger.getChildren(this);
	if (list&& list.length > 0)
	{
		var latest = list[0];
		for (var i=1;i<list.length;i++)
		{
			var next = list[i];
			if (tersus.getDate(next) > tersus.getDate(latest))
				latest = next;
		}
		this.chargeExit('<Latest>', latest);
	}
};

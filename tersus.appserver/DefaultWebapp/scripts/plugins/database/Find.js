/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Find =  tersus.DatabaseAction.__subclass('tersus.Find');
tersus.Find.prototype.getTablePrototype = function()
{
	return this.getExit('<Records>').getChildConstructor().prototype;
};
tersus.Find.prototype.run = function()
{
	var exit = this.getExit('<Records>');
	if (!exit)
	{
		modelError('Missing exit <Records>',this);
		return;
	}
	var  p = exit.getChildConstructor().prototype;
	var fl = this.getFieldList(p);
	var triggers  = this.triggers;
	if (triggers == null)
		triggers = [];
	var tableName = this.getTableName(p);
	var sqla = [];
	var explicitColumns = fl == null ? '*' : null;
	this.addSelectFieldsSQL(sqla, tableName, fl, explicitColumns);
	
	var first = true;
	var values = [];
	for (var i=0;i<triggers.length; i++)
	{
		var trigger = triggers[i];
		if (trigger.modelId == BuiltinModels.NOTHING_ID) // Control trigger
			continue;
		if (trigger.role.search(/<.*>$/)>=0) // Distinguished trigger (e.g. <Data Source>)
			continue;
		var field = fl.getField(trigger.role);
		if (first)
		{
			sqla.push('WHERE');
			first = false;
		}
		else
			sqla.push('AND');
		sqla.push(field.sqlName);
		var v = this.getSQLValue(trigger.role);
		if ( v == null)
			sqla.push ('IS NULL');
		else
		{
			sqla.push('= ?');
			values.push(v);
		}
	}
	var orderBy = this.getLeafChild('<Order By>');
	if (orderBy != null)
	{
		sqla.push('ORDER BY');
		sqla.push(orderBy);
	}
	var sql = sqla.join(' ');
	this.execSql(sql, values, function(transaction,results)
	{
		this.outputResults(results, exit);
		this.resumeContext();
	});
};

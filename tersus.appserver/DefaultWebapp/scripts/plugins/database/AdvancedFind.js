/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.AdvancedFind =  tersus.DatabaseAction.__subclass('tersus.AdvancedFind');
tersus.AdvancedFind.prototype.NO_N_MATCHES = 'No Number of Matches';
tersus.AdvancedFind.prototype.NO_POSITION = 'No Position';
tersus.AdvancedFind.prototype.getTablePrototype = function()
{
	var e = this.getExit('<Records>');
	if (e == null)
		e = this.getTrigger('<From>');
	if (e == null)
		e = this.getTrigger('<To>');
	if (e == null)
		e = this.getTrigger('<Start From>');
	if (e == null)
		e = this.getTrigger('<Match>');
	if (e == null)
		return null;
	return e.getChildConstructor().prototype;
};
tersus.AdvancedFind.prototype.run = function()
{
	var ctx =
	{
		condition:[],
		values:[],
		from:this.getChild('<From>'),
		to:this.getChild('<To>'),
		match: this.getChild('<Match>'),
		startFrom:this.getChild('<Start From>'),
		numberOfRecords:this.getChild('<Number of Records>'),
		offset:this.getLeafChild('<Offset>'),
		backward:(true == this.getLeafChild('<Backward>')),
		table:this.getLeafChild('<Table>'),
		columns:this.getLeafChild('<Columns>'),
		filter:this.getLeafChild('<Filter>'),
		givenOrderBy:this.getLeafChild('<Order By>'),
		recordsExit:this.getExit('<Records>'),
		columns:this.getLeafChild('<Columns>'),
		moreExit:this.getExit('<More>'),
		positionExit:this.getExit('<Position>'),
		tablePrototype:this.getTablePrototype(),
		numberOfMatchesExit:this.getExit('<Number of Matches>')
	};
	if (!ctx.offset)
		ctx.offset = 0;
	
	this.setOptimization(ctx);
	this.setFieldList(ctx);
	this.addTable(ctx);
	this.addDynamicFilterConditions(ctx);
	this.addRangeConditions(ctx);
    this.addMatchConditions(ctx);	
	this.setOrderBy(ctx, ctx.backward);
	ctx.needsMatches = ctx.numberOfMatchesExit	 && !ctx.noMatches;
	ctx.needsPosition = ctx.positionExit!=null && !ctx.noPosition;

	if (ctx.needsMatches 
	 || ctx.backward && ctx.needsPosition && ctx.startFrom == null)
    {
    	this.countMatches(ctx, function(ctx,count)
    	{
    		ctx.nMatches = count;
    		if (ctx.needsMatches)
    		{
    			this.chargeLeafExit(ctx.numberOfMatchesExit, count);
    		}
    		if (ctx.recordsExit)
    			this.executeQuery(ctx);
    		else
    			this.resumeContext();
    		
    	});
   	}
    else
  		if (ctx.recordsExit)
   			this.executeQuery(ctx);
   		else
   			this.resumeContext();
   		
};
tersus.AdvancedFind.prototype.setOptimization = function(ctx)
{
	ctx.noPosition = false;
	ctx.noMatches = false;
	var trigger = this.getTrigger('<Optimization>');
	if (!trigger)
		return;
	
	var list = trigger.getChildren(this);
    if (list)
    {
    	for (var i=0;i<list.length;i++)
    	{
    		var opt = list[i].leafValue;
    		if (opt == this.NO_POSITION)
    			ctx.noPosition=true;
    		else if (opt == this.NO_N_MATCHES)
    			ctx.noMatches = true;
    	}
    }
};
tersus.AdvancedFind.prototype.countMatches = function countMatches(ctx, callback)
{
	var sqla = ['SELECT COUNT(1) AS count FROM', ctx.tableSql];
	if (ctx.condition.length > 0)
	{
		sqla.push('WHERE');
		tersus.addAll(sqla, ctx.condition);
	}
	this.execSql(sqla.join(' '),ctx.values, function(transaction, results) 
	{
		var count = results.rows.item(0).count;
		callback.call(this, ctx, count);
	});
};
tersus.AdvancedFind.prototype.setFieldList = function setFieldList (ctx)
{
	if (ctx.tablePrototype != null)
	{
		if (ctx.tablePrototype.isMap)
		{
			ctx.fieldList = new tersus.FieldList();
			ctx.fieldList.addMapFields(ctx.from);
			ctx.fieldList.addMapFields(ctx.to);
			ctx.fieldList.addMapFields(ctx.match);
			ctx.fieldList.addMapFields(ctx.startFrom);
			if (ctx.columns != null)
			{
				tersus.foreach(ctx.columns.split(/ *, */),
				function (name)
				{
						ctx.fieldList.addMapField(name);
				});
			}
			if (ctx.givenOrderBy != null)
			{
				tersus.foreach(ctx.givenOrderBy.split(/\s*,\s*/),
				function (clause)
				{
        			var pp = clause.split(/\s+/);
        			if (pp.length >= 1)
        			{
        				var name = pp[0];
        				ctx.fieldList.addMapField(name);
        			}
        		});
        	}			
		}
		else
			ctx.fieldList = this.getFieldList(ctx.tablePrototype);
	}
};
tersus.AdvancedFind.prototype.addTable = function addTable(ctx)
{
	var rawSql = ctx.table? ctx.table : this.getTableName(ctx.tablePrototype);
	var q = this.analyzeParameterizedSQL(rawSql);
	ctx.tableSql = q.sqlText;
	for (var i=0; i<q.parameters.length; i++)
	{
		ctx.values.push(this.getSQLValue(q.parameters[i]));
	}
};
tersus.AdvancedFind.prototype.addDynamicFilterConditions = function(ctx)
{
	if (! ctx.filter)
		return;
	ctx.condition.push('(');
	var q = this.analyzeParameterizedSQL(ctx.filter);
	ctx.condition.push(q.sqlText);
	ctx.condition.push(')');
	for (var i=0; i<q.parameters.length; i++)
	{
		ctx.values.push(this.getSQLValue(q.parameters[i]));
	}
};



tersus.AdvancedFind.prototype.addMatchConditions = function(ctx)
{
  if (ctx.match == null)
        return;
	var columns = ctx.fieldList.fields;
    for (var i = 0; i < columns.length; i++)
    {
		var column = columns[i];
        var matchValue =  column.getSQLValue(ctx.match);
		if (matchValue != null)
        {
			if (matchValue.indexOf && matchValue.indexOf('%') >= 0)
	            this.addCondition(ctx, column, ' like ', matchValue);
	        else
	            this.addCondition(ctx, column, ' = ', matchValue);
	    }
	}
};       
				

tersus.AdvancedFind.prototype.addRangeConditions = function(ctx)
{
  if (ctx.from == null && ctx.to == null)
        return;

	var columns = ctx.fieldList.fields;
    for (var i = 0; i < columns.length; i++)
    {
		var column = columns[i];
        var fromValue = null;
        var toValue = null;
        if (ctx.from != null)
			fromValue = column.getSQLValue(ctx.from);
		if (ctx.to != null)
			toValue = column.getSQLValue(ctx.to);
		if (fromValue == null && toValue != null)
        {
			if (ctx.condition.length > 0)
				ctx.condition.push("AND");
            ctx.condition.push("(");
            this.addCondition(ctx, column, '<=', toValue);
            this.appendOrIsNull(ctx.condition, column.sqlName);
        }
        else
        {
            if (fromValue != null)
                this.addCondition(ctx, column, '>=', fromValue);
            if (toValue != null)
                this.addCondition(ctx, column, '<=', toValue);
        }
    }	

};
tersus.AdvancedFind.prototype.addCondition = function addCondition(ctx, column, operator, value)
{
	var c = ctx.condition;
    if (c.length > 0 && c[c.length - 1] != '(' )
    {
        c.push(" AND ");
    }
    c.push(column.sqlName);
   	c.push(operator);
    c.push('?');
    //types.add(column.getValueHandler());
    ctx.values.push(value);

};

tersus.AdvancedFind.prototype.appendOrIsNull = function(sqla, columnName)
{
    sqla.push('OR');
    sqla.push(columnName);
    sqla.push(' IS NULL )');
};

tersus.AdvancedFind.prototype.setOrderBy = function setOrderBy(ctx, reverse)
{
    ctx.columnOrder = {};
    ctx.orderedColumns = [];
    if (ctx.recordsExit == null)
    	return;

    this.handleExplicitOrderBy(ctx,reverse);
    if (ctx.fieldList != null)
        this.addPrimaryKeyToSortOrder(ctx,reverse);
};

tersus.AdvancedFind.prototype.handleExplicitOrderBy = function handleExplicitOrderBy (ctx, reverse)
{
   	ctx.columnOrder = {};
   	ctx.orderedColumns=[];
   	ctx.orderBy = [];
    if (ctx.givenOrderBy)
    {
    	var p = ctx.givenOrderBy.split(/\s*,\s*/);
		for (var i=0;i<p.length;i++)
        {
        	var clause = p[i];
        	var pp = clause.split(/\s+/);
            if (pp.length == 0 || pp.length > 2)
            {
            	this.modelExecutionError("Invalid <Order By>: '" + ctx.givenOrderBy + "'", this);
            	return;
            }
            var columnName = pp[0];
            var column = ctx.fieldList.getField(columnName);
            if (column == null)
            {
            	if (ctx.tablePrototype && ctx.tablePrototype.isMap)
            	{	
            		column = new tersus.MapField(columnName);
            		ctx.fieldList.addField(column);	
            	}
            	else
            	{
                	modelExecutionError("Invalid <Order By> ('" + ctx.givenOrderBy
                        + "': unknown column '" + columnName + "'");
                	return;
                }
            }
	
			var desc = pp.length > 1 && pp[1].search(/^desc/i) >= 0;
			if (reverse)
				desc = !desc;
			this.addColumnToSortOrder(ctx, column, desc)
        }
    }
};
tersus.AdvancedFind.prototype.addColumnToSortOrder = function addColumnToSortOrder(ctx, column, desc)
{
    var dir = desc ? 'DESC' : 'ASC';
	ctx.columnOrder[column.sqlName]=desc;
    ctx.orderedColumns.push(column);
    if (ctx.orderBy.length > 0)
    	ctx.orderBy.push(',');
    ctx.orderBy.push(column.sqlName);
    if (desc)
		ctx.orderBy.push(dir);
};
tersus.AdvancedFind.prototype.addPrimaryKeyToSortOrder = function addPrimaryKeyToSortOrder(ctx, reverse)
{
    var pkColumns = ctx.fieldList.primaryKeyFields;
    for (var i = 0; i < pkColumns.length; i++)
    {
        var column = pkColumns[i];
        var columnName = column.sqlName;
    	if (ctx.columnOrder[columnName] == null)
        	this.addColumnToSortOrder(ctx, column, reverse);
    }
};
tersus.AdvancedFind.prototype.addStartFromConditions = function addStartFromConditions(ctx)
{
    if (ctx.startFrom == null)
        return;
    var startFromCondition = [];
    var previousTermsValues = [];
    //var  previousTermsTypes = [];
    var nTerms = 0;
    var previousTermsClause = [];
    for (var i = 0; i < ctx.orderedColumns.length; i++)
    {
        var column =  ctx.orderedColumns[i];
        var columnName = column.sqlName;
        var desc = ctx.columnOrder[columnName];
        if (desc == null)
            continue;
        var value = column.getSQLValue(ctx.startFrom);
        var last = (i == ctx.orderedColumns.length - 1);
        if (value == null)
        {
            if (last)
            {
                if (desc) // If the value of the last column is null, we only care about it if it's sorted desc
                {
                    if (previousTermsClause.length > 0)
                    {
                        previousTermsClause.push("AND");
                        previousTermsClause.push(columnName);
                        previousTermsClause.push("IS NULL");
                    }

                }
                if (startFromCondition.length > 0 && previousTermsClause.length > 0)
                {
                    startFromCondition.push("OR (");
                    tersus.addAll(startFromCondition,previousTermsClause);
                    startFromCondition.push(")");
                }
                else if (previousTermsClause.length > 0)
                {
                	
                    tersus.addAll(startFromCondition,previousTermsClause);
                }
                tersus.addAll(ctx.values,previousTermsValues);
//                tersus.addAll(types,previousTermsTypes);
                ++nTerms;
            }
            else
            {
                if (!desc)
                {
                    if (startFromCondition.length == 0)
                    {
                        startFromCondition.push("(");
                    }
                    else
                    {
                        startFromCondition.push('OR (');
                    }
                    if (previousTermsClause.length > 0)
                    {
                        tersus.addAll(startFromCondition,previousTermsClause);
		                tersus.addAll(ctx.values,previousTermsValues);
	//	                tersus.addAll(types,previousTermsTypes);
                        startFromCondition.push("AND");
                    }
                    startFromCondition.push(columnName);
                    startFromCondition.push("IS NOT NULL )");
                    ++nTerms;
                }
                if (previousTermsClause.length > 0)
                {
                    previousTermsClause.push("AND");
                }
                previousTermsClause.push(columnName);
                previousTermsClause.push(" IS NULL");

            }
        }
        else
        {
            if (startFromCondition.length > 0 && previousTermsClause.length > 0)
            {
                startFromCondition.push("OR (");
                tersus.addAll(startFromCondition,previousTermsClause);
                tersus.addAll(ctx.values,previousTermsValues);
//                tersus.addAll(types,previousTermsTypes);
                startFromCondition.push("AND (");
            }
            else if (previousTermsClause.length > 0)
            {
 				tersus.addAll(startFromCondition,previousTermsClause); 
 				startFromCondition.push("AND ((");
 				tersus.addAll(ctx.values,previousTermsValues);
//              tersus.addAll(types,previousTermsTypes);
 			}         	
            else
                startFromCondition.push("((");
            startFromCondition.push(columnName);

            ctx.values.push(value);
            //types.push(column.getValueHandler());
            if (last)
            {
                if (!desc)
                    startFromCondition.push(">= ? ))");
                else
                {
                    startFromCondition.push("<= ?");
                    // we need to explicitly include null values
                    this.appendOrIsNull(startFromCondition, columnName);
                    startFromCondition.push(')');
                }
            }
            else
            {
                if (!desc)
                    startFromCondition.push("> ? ))");
                else
                {
                    startFromCondition.push("< ?");
                    // we need to explicitly include null values
                    this.appendOrIsNull(startFromCondition, columnName);
                    startFromCondition.push(')');
                }

                if (previousTermsClause.length > 0)
                    previousTermsClause.push("AND");
                previousTermsValues.push(value);
                previousTermsClause.push(columnName);
                previousTermsClause.push("= ?");
                //previousTermsTypes.push(column.getValueHandler());
            }
            ++nTerms;
        }
    }
    if (nTerms == 0)
        return;
    if (ctx.condition.length == 0)
        ctx.condition = startFromCondition;
    else
    {
        ctx.condition.push('AND (');
        tersus.addAll(ctx.condition, startFromCondition);
        ctx.condition.push(')');
    }

};
tersus.AdvancedFind.prototype.executeQuery = function(ctx)
{
	this.addStartFromConditions(ctx);
	var sqla = [];
	var explicitColumns = ctx.columns == null && ctx.tablePrototype.isMap ? '*' : ctx.columns;
	this.addSelectFieldsSQL(sqla, ctx.tableSql, ctx.fieldList, explicitColumns);
		
	if (ctx.condition.length > 0)
	{
		sqla.push('WHERE');
		tersus.addAll(sqla, ctx.condition);
	}
	 
	if (ctx.orderBy.length > 0)
	{
		sqla.push('ORDER BY');
		tersus.addAll(sqla,ctx.orderBy);
	}
	var extra = 0; 
	if (ctx.numberOfRecords > 0)
	{
		if (ctx.moreExit != null)
			extra = 1;
		sqla.push('LIMIT');
		sqla.push(extra + parseInt(ctx.numberOfRecords));
		sqla.push('OFFSET')
		sqla.push(ctx.offset);
	}
	var sql = sqla.join(' ');
	this.execSql(sql, ctx.values, function(transaction,results)
	{
		var nrows = results.rows.length;
		var nrecs = (ctx.numberOfRecords > 0 && nrows> ctx.numberOfRecords) ? ctx.numberOfRecords : nrows;
		if (ctx.moreExit && nrows > nrecs)
			this.chargeEmptyExit(ctx.moreExit);
		this.chargeLeafExit('<Number of Records Returned>', nrecs);
		if (ctx.recordsExit != null)
			this.outputResults(results, ctx.recordsExit, nrecs, ctx.backward);
		this.getPosition(ctx);
	});
};
tersus.AdvancedFind.prototype.getPosition = function(ctx)
{
	if (! ctx.needsPosition)
	{
		this.resumeContext();
		return;
	}
	if (ctx.startFrom == null)
	{
		var pos = ctx.backward ? ctx.nMatches - ctx.numberOfRecords-ctx.offset : ctx.offset;
		this.chargeLeafExit(ctx.positionExit,pos);
		this.resumeContext();
		return;
	}

    ctx.condition=[];
    ctx.values=[];
    this.addDynamicFilterConditions(ctx);
    this.addRangeConditions(ctx);
    this.addMatchConditions(ctx);
    this.setOrderBy(ctx, true); // Regardless of <Backward>, we count all records before our own record
    this.addStartFromConditions(ctx);
    this.countMatches(ctx, function(ctx, count)
    {
    	var startFromPosition = count - 1; // If <Start From> is the first record we have count=1 and startFromPosition = 0);
    	var pos = ctx.backward ? 
    						startFromPosition+1-ctx.numberOfRecords-ctx.offset :
    						startFromPosition + ctx.offset;
		this.chargeLeafExit(ctx.positionExit,pos);
		this.resumeContext();
		return;
    });
};


 
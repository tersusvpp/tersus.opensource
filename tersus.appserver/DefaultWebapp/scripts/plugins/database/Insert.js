/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Insert = tersus.DatabaseAction.__subclass('tersus.Insert');
tersus.Insert.prototype.getTablePrototype = function()
{
	return this.getTrigger('<Record>').getChildConstructor().prototype;
};
tersus.Insert.prototype.run = function()
{
	var record = this.getChild('<Record>');
	var tableName = this.getTableName(record);
	var fieldList = this.getFieldList(record.constructor.prototype);
	var dupExit = this.getExit('<Duplicate>');
	if (dupExit)
		this.checkDup(record,tableName,fieldList);
	else
		this.insert(record,tableName,fieldList);
};
tersus.Insert.prototype.checkDup = function(record,tableName, fieldList)
{
	var sqla = [];
	var values = [];
	sqla.push("SELECT COUNT(1) as count FROM ");
	sqla.push(tableName);
	this.addPrimaryKeyWhereClause(record,fieldList,sqla, values);
	var sql = sqla.join(' ');
	this.execSql(sql, values, function(transaction,results)
	{
		var count = results.rows.item(0)['count'];
		if (count > 0)
		{
			this.chargeExit('<Duplicate>', record);
			this.resumeContext();			
		}
		else
			this.insert(record,tableName,fieldList);
	});
	
};
tersus.Insert.prototype.insert = function(record, tableName, fieldList)
{
	
	var sqla = []; // 'buffer' for creating the sql
	sqla.push('INSERT INTO');
	sqla.push(tableName);
	sqla.push('(');
	var values = []; // values
	var fields = fieldList.fields;
	var nfields = fields.length;
	var n = 0;
	for (var j=0;j<nfields;j++)
	{
		var field = fields[j];
		var v = field.getSQLValue(record);
		if (v != null)
		{
			if (n>0)
				sqla.push(',');
			sqla.push(field.sqlName);
			n++;
			values.push(v);
		}
	}
	sqla.push(')');
	sqla.push('VALUES (?');
	for (var i=1;i<n;i++)
		sqla.push(', ?');
	sqla.push(')');
	var sql = sqla.join(' ');
	this.execSql(sql, values, function(transaction,results)
	{
		this.chargeExit('<Inserted>',record);
		this.resumeContext();
	});
			
};
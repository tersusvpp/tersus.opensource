/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Delete = tersus.DatabaseAction.__subclass('tersus.Delete');
tersus.Delete.prototype.getTablePrototype = function()
{
	return this.getTrigger('<Record>').getChildConstructor().prototype;
};
tersus.Delete.prototype.run = function()
{
	var record = this.getChild('<Record>');
	var tableName = this.getTableName(record);
	var fieldList = this.getFieldList(record.constructor.prototype);
	var sqla = []; // 'buffer' for creating the sql
	var values = []; // values
	sqla.push('DELETE FROM ');
	sqla.push(tableName);
	this.addPrimaryKeyWhereClause(record,fieldList,sqla, values);
	var sql = sqla.join(' ');
	this.execSql(sql, values, function(transaction,results)
	{
		this.handleResults(results,record,tableName);
		this.resumeContext();
	});
			
};
tersus.Delete.prototype.handleResults = function(results, record, tableName)
{
	var count = results.rowsAffected;
	if (count == 1)
		this.chargeExit('<Deleted>',record);
	else if (count == 0)
		this.chargeExit('<Not Found>',record);
	else
		modelExecutionError("Delete resulted in deleting multiple records in table "
                                    + tableName
                                    + ". Check primary key settings", this);
                                    
};
/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
tersus.Field = function Field(element, path)
{
	if (element != null)
	{
		this.name = element.role;
		this.sqlName = element.columnName ? element.columnName : tersus.sqlize(element.role);
		this.path = path;
		this.element = element;
	}
};
tersus.Field.prototype.constructor = tersus.Field;
tersus.Field.prototype.setValue = function (obj, value)
{
	obj.setValue(this.path, value, Operation.REPLACE);
};
tersus.Field.prototype.setLeafValue = function (obj, leafValue)
{
	var value = this.element.createChildInstance();
	value.setLeaf(leafValue);
	obj.setValue(this.path, value, Operation.REPLACE);
};
tersus.Field.prototype.getSQLDef = function()
{
	var childP = this.element.getChildConstructor().prototype;
	return this.sqlName+ ' ' + childP.sqlType;
};
tersus.Field.prototype.getSQLValue = function (obj)
{
	var values = obj.getValues(this.path);
	if (values && values.length == 1)
	{
		var v = values[0];
		if (v == null)
		  	return v;
		return v.isDateAndTime ? v.serialize() : v.leafValue; // SQLite doesn't support date and time - need to serialize
	}
	else 
		return null;
};

tersus.MapField = tersus.Field.__subclass('MapField', function(sqlName)
{
	this.name = this.sqlName=sqlName;
});
tersus.MapField.prototype.getSQLValue = function (obj)
{
	if (obj == null)
		return null;
	var map = obj.leafValue;
	if (map == null)
		return null;
	var v = map[this.sqlName];
	if (v == null)
		  return v;
	return v.isDateAndTime ? v.serialize() : v.leafValue; // SQLite doesn't support date and time - need to serialize
};

tersus.FieldList = function(model)
{
	this.initialize(model);
};
tersus.FieldList.prototype.constructor = tersus.FieldList;
tersus.FieldList.prototype.initialize = function(model)
{
	this.fields = [];
	this.primaryKeyFields=[];
	this.fieldsByName={};
	if (model)
	{
		this.addFields(model);
		for (var i=0; i<this.fields.length;i++)
		{
			var f = this.fields[i];
			if (f.element.isPrimaryKey)
			{
				this.primaryKeyFields.push(f);
				f.isPrimaryKey = true;
			}
		}
		if (this.primaryKeyFields.length == 0 && this.fields.length >0)
		{
			this.primaryKeyFields.push(this.fields[0]); // Todo - remove this sometime in the future?
			this.primaryKeyFields[0].isPrimaryKey=true;
		}
	}
};
tersus.FieldList.prototype.addMapFields = function(obj)
{
	if (obj == null)
		return;
	var map = obj.leafValue;
	if (map == null)
		return;
	for (var name in map)
		this.addMapField(name);
};
tersus.FieldList.prototype.getField = function getField(name)
{
	return this.fieldsByName[tersus.sqlize(name)];
};

tersus.FieldList.prototype.addMapField = function(name)
{
	if (this.getField(name))
		return;
	this.addField(new tersus.MapField(name));
}
tersus.FieldList.prototype.addField = function(f)
{
	this.fields.push(f);
	if (this.fieldsByName[f.sqlName] != null)
			modelError('Ambiguous column name '+f.sqlName, this);	
	this.fieldsByName[f.sqlName] = f;
};
tersus.FieldList.prototype.addFields = function addFields(root,prefixPath)
{	
	if (prefixPath == null)
		prefixPath = new Path('');
	var elements = root.elementList;
	if (!elements)
		return;
	var n=elements.length;
	for (var i=0;i<n;i++)
	{
		var e = elements[i];
		var c = e.getChildConstructor().prototype;
		var p = prefixPath.clone();
		p.addSegment(e.role);
		if (c.isLeaf)
			this.addField(new tersus.Field(e, p));
		else
			this.addFields(c,p);
	} 
};

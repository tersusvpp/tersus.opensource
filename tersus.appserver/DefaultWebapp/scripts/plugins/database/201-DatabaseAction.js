/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.databases = {};
tersus.maxDatabaseSize=5000000;
tersus.DatabaseAction = ActionNode.__subclass('tersus.DatabaseAction');
tersus.DatabaseAction.prototype.getDB = function()
{
	var datasource = this.getLeafChild('<Data Source>');
	if (!datasource)
		datasource = 'default';
	if (datasource.charAt(0) != '/')
	{
		var m = tersus.rootURL.match(/^(?:https?:\/\/[^/]+\/|file:\/+)([^/]+)\/$/);
		if (m && m.length > 0)
			datasource=m[1]+'_'+datasource;
	}

	var db = tersus.databases[datasource];
	if (! db)
	{
		db = openDatabase(datasource, 1.0, datasource, tersus.maxDatabaseSize);
		if (db == null) // Workaround for phonegap issue with quota update
		{
			for (var i=0;i<=tersus.maxDatabaseSize/1000000;i++)
			{
				db = openDatabase(datasource, 1.0, datasource, tersus.maxDatabaseSize);
				if (db != null)
					break;
			}
		}
		tersus.databases[datasource]=db;
	}
	return db;
};

tersus.DatabaseAction.prototype.getSQLStatememt = function()
{
	var sql = this.getLeafChild('<SQL Statement>');
	if (!sql)
		modelError('Missing <SQL Statement>',this);
	return sql;
};

tersus.DatabaseAction.prototype.analyzeParameterizedSQL = function(str)
{
	var t = str;
	var i=0;
	var l = t.length;
	var out = [];
	var keys = [];
	while (i<l&& i>= 0)
	{
		var n = t.indexOf('${',i);
		if (n == -1)
		{
			out.push(t.substring(i));
			i = n;
		}
		else
		{
			out.push(t.substring(i,n));
			var b = n+ 2;
			var e=t.indexOf('}', b);
			if (e<0)
			{
				modelExecutionError("Invalid template: missing '}' for '${'.  Position="
							+ n
							+ " Text="
							+ t.substring(n, n + 20));
				return null;
			}
			var key=t.substring(b,e);
			out.push('?');
			keys.push(key);
			i = e+1;
		}

	}
	var result = {};
	result.sqlText = out.join('');
	result.parameters = keys;
	return result;
};

tersus.DatabaseAction.prototype.getFieldList = function(model)
{
	if (!model.fieldList)
	{
		model.fieldList = new tersus.FieldList(model);
	}
	return model.fieldList;
};
tersus.DatabaseAction.prototype.getOutputColumnsExit = function getOutputColumnsExit()
{
	return this.getExit('<Output Columns>');
};
tersus.DatabaseAction.prototype.outputColumns = function outputColumns(colMap)
{
	var exit = this.getOutputColumnsExit();
	if (exit)
	{
		for (var name in colMap)
		{
			var col = exit.createChildInstance();
			col.setLeafChild('Name',name, Operation.REPLACE);
			col.setLeafChild('Type',colMap[name], Operation.REPLACE);
			this.accumulateExitValue(exit, col);
		}
	}
};
tersus.DatabaseAction.prototype.outputResultAsMap = function outputResultAsMap(exit, row, columns)
{
	var out = exit.createChildInstance();
	var map = {};
	out.leafValue = map;
	for (var key in row)
	{
		var value = row[key];
		if (value == null)
		{
			if (columns[key] == null)
				columns[key] = '?';
			continue;
		}
		var type =	typeof(value);
		var c;
		if (type == 'string')
		{
			c = tersus.repository.get(BuiltinModels.TEXT_ID);
		}
		else if (type == 'number')
		{
			c = tersus.repository.get(BuiltinModels.NUMBER_ID);
		}
		else
			throw {message:'Unexpected database return type '+type};
		var node = new c();
		node.prototype = c.prototype;
		node.setLeaf(value);
		map[key]=node;
		if (columns)
		{
			if (columns[key] == null || columns[key]=='?')
				columns[key] = c.prototype.modelName;
			else if (columns[key] != c.prototype.modelName)
				columns[key] = 'Anything';
		}

	}
	if (exit.isRepetitive)
		this.accumulateExitValue(exit, out);
	else
		this.chargeExit(exit,out);

};

tersus.DatabaseAction.prototype.getTableNameOfModel = function(record)
{
	return record.tableName  ? record.tableName : tersus.sqlize(record.modelName);
};
tersus.DatabaseAction.prototype.getTableName = function getTableName(record)
{
	var n = this.getLeafChild('<Table Name>');
	if (n == null)
		n = this.getTableNameOfModel(record);
	return n;
};

tersus.DatabaseAction.prototype.addPrimaryKeyWhereClause = function createPrimaryKeyFilter(record,fieldList,sqla, values)
{
	sqla.push("WHERE");
	var pk = fieldList.primaryKeyFields;
	var npk = pk.length;
	for (var i=0;i<npk;i++)
	{
		if (i>0)
			sqla.push('AND');
		var f = pk[i];
		var v = f.getSQLValue(record);
		sqla.push(f.sqlName);
		if (v == null)
			sqla.push('IS NULL');
		else
		{
			sqla.push('= ?');
			values.push(v);
		}
	}

};
tersus.DatabaseAction.prototype.addSelectFieldsSQL = function addSelectFieldsSQL(sqla, tableName, fieldList, explicitColumns)
{
	sqla.push('SELECT');
	if (explicitColumns)
		sqla.push(explicitColumns)
	else
	{
		var fields = fieldList.fields;
		var n = fields.length;
		for (var i=0;i<n; i++)
		{
			if (i>0)
				sqla.push(',');
			sqla.push(fields[i].sqlName);
		}
	}
	sqla.push('FROM');
	sqla.push(tableName);
};
tersus.DatabaseAction.prototype.outputResults = function(results, exit, maxRows, reverse)
{
	var nRows = 0;
	var rows = results.rows;
	if (rows)
		nRows =  rows.length;
	if (nRows == 0)
	{
		this.chargeEmptyExit('<None>');
		return;
	}
	if (maxRows && nRows > maxRows)
		nRows = maxRows;
	var outputColumnsExit = this.getOutputColumnsExit();
	var outputPrototype = exit.getChildConstructor().prototype;
	if (outputPrototype.isMap)
	{
		var colMap=null;
		if (outputColumnsExit)
			colMap = {};
		for (var i=0;i<nRows; i++)
		{
			this.outputResultAsMap(exit, rows.item(i), colMap);
		}
		if (outputColumnsExit)
		{
			this.outputColumns(colMap);
		}
	}
	else
	{
		var fields = this.getFieldList(outputPrototype).fields;
		var nFields = fields.length;
		if (reverse)
		{
			for (var i=nRows -1; i>=0; i--)
				this.outputResult(rows.item(i), fields, nFields, exit);
		}
		else
		{
			for (var i=0;i<nRows; i++)
				this.outputResult(rows.item(i), fields, nFields, exit);
		}
	}

};
tersus.DatabaseAction.prototype.outputResult = function(item, fields, nfields, exit)
{
	var out = exit.createChildInstance();
	if (out.isLeaf)
	{
		for (var k in item)
			out.setLeaf(item[k]);
	}
	else // composite
	{
		for (var j=0;j<nfields;j++)
		{
			var field = fields[j];
			var v = item[field.sqlName];
			if (v != null)
				field.setLeafValue(out,v);
		}
	}
	if (exit.isRepetitive)
		this.accumulateExitValue(exit, out);
	else
		this.chargeExit(exit,out);
};

tersus.DatabaseAction.prototype.start = function()
{
	this.pause();
	if (this.tableOK)
	{
		this.run();
		return;
	}
	this.tableOK = true;
	var p = this.getTablePrototype();
	if (!p || p.tableOK || p.isMap)
	{
		this.run();
	}
	else
	{
		p.tableOK = true;
		this.getTableDef(this.getTableNameOfModel(p), this.updateTableAndRun, this);
	}
};
tersus.DatabaseAction.prototype.updateTableAndRun = function (tableDef)
{
	var p = this.getTablePrototype();
	var fieldList = this.getFieldList(p);
	var fields = fieldList.fields;
	var nfields = fields.length;
	var batch = [];
	if (tableDef == null)
	{
		var sql = 'CREATE TABLE '+this.getTableNameOfModel(p) + ' (';
		for (var i=0;i<nfields; i++)
		{
			var field = fields[i];
			if (i>0)
				sql += ', ';
			sql+=field.getSQLDef();
		}
		var pk = fieldList.primaryKeyFields;
		if (pk.length > 0)
		{
			sql += ', PRIMARY KEY (';
			for (var i=0;i<pk.length;i++)
			{
				if (i>0)
					sql += ',';
				sql += pk[i].sqlName;
			}
			sql+=')';
		}
		sql+=')';
		batch.push(sql);
	}
	else
	{
		for (var i=0;i<nfields;i++)
		{
			var field = fields[i];
			if (tableDef.columns[field.sqlName] == null)
				batch.push('ALTER TABLE '+p.tableName+' ADD COLUMN '+field.getSQLDef());
		}
	}
	if (batch.length > 0)
		this.execSqlBatch(batch, function(){this.run();});
	else
		this.run();
};

tersus.DatabaseAction.prototype.resumeContext = function()
{
	this.continueExecution();
};
tersus.DatabaseAction.prototype.execSql = function (sql, values, callback)
{
	if (window.trace)
	{
		window.trace.sql(this, sql, values);
	}
	var errorHandler = function(transaction,error)
	{
		tersus.error('Error: '+error.message+' ['+error.code+']');
		return true;
	};
	var node = this;
	var dataHandler = function(transaction, results)
	{
		callback.call(node,transaction, results);
	};
	this.getDB().transaction(function(transaction){ transaction.executeSql(sql, values, dataHandler, errorHandler);});
};
tersus.DatabaseAction.prototype.execSqlBatch = function (commands, callback, index)
{
	if (index == null)
		index = 0;
	if (commands.length != null && index >= commands.length)
	{
		tersus.error('Invalid index in execSqlBatch');
	}
	this.execSql(commands[index], [], function()
	{
		if (index+1==commands.length)
			callback.call(this);
		else
			this.execSqlBatch(commands, callback, index+1);
	});
};
tersus.DatabaseAction.prototype.getTableDef = function(tableName, callback, obj)
{
	var errorHandler = function(transaction,error)
	{
		tersus.error('Error: '+error.message+' ['+error.code+']');
		return true;
	};
	var dataHandler = function(transaction, results)
	{
		if (!results || !results.rows || results.rows.length == 0)
		{
			callback.call(obj,null);
			return;
		}

		var re0 = /create table [^(]*\((.*)\)$/i;
		var re1 = /\s*(?:(?:(\w+)(?:\s+(\w+))?(?:\s*\(\s*(\d+)\s*\)\s*)?)|(?:primary\s*key\s*\(\s*([^)]*)\s*\)))((?:\s|\w)*)(?:,|$)/ig;
		var row = results.rows.item(0);
		var tableName = row.name;
		var sql = row.sql;
		var tableSpec= re0.exec(sql)[1];
		var tableDef = {name:tableName,columns:{}};
		do
		{
			var m = re1.exec(tableSpec);
			if (m!= null)
			{
				if (m[1] != null) // Column definition
				{
					var col = {name:m[1], type:m[2], size:m[3]};
					if (m[5] && /primary\s+key/i.test(m[5]))
						col.isPrimaryKey=true;
					tableDef.columns[col.name]=col;
				}
				else if (m[4] != null) // Primary key definition
				{
					var pk = m[4].split(/\s*,\s*/);
					for (var j=0; j<pk.length;j++)
					{
						var c = pk[j];
						if (c && tableDef.columns[c])
							tableDef.columns[c].isPrimaryKey=true;
					}
				}

			}
		} while (m!= null);
		callback.call(obj, tableDef);
	};
	this.getDB().transaction(function(transaction) {
		transaction.executeSql("select name, sql from sqlite_master where type=? and name=?",['table', tableName],  dataHandler, errorHandler);
	});
};
tersus.DatabaseAction.prototype.getSQLValue = function getSQLValue(role)
{
	  var v = this.getChild(role);
	  if (v == null)
	  	return v;
	  return v.isDateAndTime ? v.serialize() : v.leafValue; // SQLite doesn't support date and time - need to serialize

};

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.DatabaseUpdate = tersus.DatabaseAction.__subclass('tersus.DatabaseUpdate');
tersus.DatabaseUpdate.prototype.start = function()
{
	var sql = this.getSQLStatememt();
	var q = this.analyzeParameterizedSQL(sql);
	var values = [];
	for (var i=0; i<q.parameters.length; i++)
	{
		values.push(this.getSQLValue(q.parameters[i]));
	}
	this.pause();
	this.execSql(q.sqlText, values, function(transaction,results)
	{
		this.handleResults(results);
		this.resumeContext();
	});
};
tersus.DatabaseUpdate.prototype.handleResults = function(results)
{
	if (results)
	{
		if (results.rowsAffected != null)
			this.chargeLeafExit('<Number of Records Affected>',results.rowsAffected);
//		if (results.insertId != null)
//			this.chargeLeafExit('<Insert Id>', results.insertId);
		}
};
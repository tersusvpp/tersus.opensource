/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.DatabaseQuery = tersus.DatabaseAction.__subclass('tersus.DatabaseQuery');
tersus.DatabaseQuery.prototype.start = function()
{
	var sql = this.getSQLStatememt();
	var q = this.analyzeParameterizedSQL(sql);
	var values = [];
	for (var i=0; i<q.parameters.length; i++)
	{
		values.push(this.getSQLValue(q.parameters[i]));
	}
	this.pause();
	this.execSql(q.sqlText, values, function(transaction,results)
	{
		this.outputResults(results, this.getExit('<Results>'));
		this.resumeContext();
	});
};

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.SequenceNumber = tersus.DatabaseAction.__subclass('tersus.SequenceNumber');
tersus.SequenceNumber.prototype.getTableName = function()
{
	var tableName = this.getLeafChild('<Table Name>');
	if (tableName == null)
		tableName = 'Sequence_Numbers';
	return tableName;
};
tersus.SequenceNumber.prototype.start = function()
{
	this.pause();
	if (this.tableOK)
	{
		this.run();
		return;
	}
	this.tableOK = true;
	this.getTableDef(this.getTableName(),function(tableDef)
	{
		if (tableDef != null)
			this.run();
		else
			this.execSql('CREATE TABLE '+this.getTableName()+' (Name TEXT PRIMARY Key, Value Integer)',[], this.run);
	},this);
};
tersus.SequenceNumber.prototype.run = function()
{
	var sql = 'SELECT Value from '+this.getTableName()+' WHERE Name=?';
	var key = this.getLeafChild('<Key>');
	if (key == null)
		key = this.modelName;
	
	var values = [key];
	this.execSql(sql, values, function(transaction,results)
	{
		var rows = results.rows;
		if (rows.length == 0)
		{
			this.chargeLeafExit('<Next>',1);
			this.execSql('INSERT INTO '+this.getTableName() +' (Name, Value) VALUES (?,?)', [key, 1], this.resumeContext);
		}
		else
		{
			var seq = rows.item(0).Value + 1;
			this.chargeLeafExit('<Next>',seq);
			this.execSql('UPDATE '+this.getTableName() + ' SET Value= ? Where Name = ?', [seq, key], this.resumeContext); 
		}
	});
};

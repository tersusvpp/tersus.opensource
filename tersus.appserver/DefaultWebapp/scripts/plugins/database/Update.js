/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Update = tersus.DatabaseAction.__subclass('tersus.Update');
tersus.Update.prototype.getTablePrototype = function()
{
	return this.getTrigger('<Record>').getChildConstructor().prototype;
};
tersus.Update.prototype.run = function()
{
	var record = this.getChild('<Record>');
	var tableName = this.getTableName(record);
	var fieldList = this.getFieldList(record.constructor.prototype);
	var sqla = []; // 'buffer' for creating the sql
	var values = []; // values
	sqla.push('UPDATE '+tableName + " SET ");
	var fields = fieldList.fields;
	var nfields = fields.length;
	var n = 0;
	var first = false;
	for (var j=0;j<nfields;j++)
	{
		var field = fields[j];
		if (!field.isPrimaryKey)
		{
			if (n++>0)
				sqla.push(',');
			sqla.push(field.sqlName);
			var v = field.getSQLValue(record);
			if (v != null)
			{
				sqla.push('= ?');
				values.push(v);
			}
			else
				sqla.push (' = NULL');
		}
	}
	this.addPrimaryKeyWhereClause(record,fieldList,sqla, values);
	var sql = sqla.join(' ');
	this.execSql(sql, values, function(transaction,results)
	{
		this.handleResults(results,record,tableName);
		this.resumeContext();
	});
			
};
tersus.Update.prototype.handleResults = function(results, record, tableName)
{
	var count = results.rowsAffected;
	if (count == 1)
		this.chargeExit('<Updated>',record);
	else if (count == 0)
		this.chargeExit('<Not Found>',record);
	else
		modelExecutionError("Update resulted in updating multiple records in table "
                                    + tableName
                                    + ". Check primary key settings", this);
                                    
};
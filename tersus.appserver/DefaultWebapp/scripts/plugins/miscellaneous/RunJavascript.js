/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.newAction('RunJavascript', function()
{
	var scriptText = this.getLeafChild('<Script Text>');
	var triggerList = [];
	var valueList = [];
	var output;
	var exitList = [];
	var exitMap = {};
	triggerList.push('_node');
	valueList.push(this);
	for (var i=0; i<this.triggers.length; i++)
	{
		var trigger = this.triggers[i];
		if (trigger.role.charAt(0) != '<' ) // ignore 'special' triggers
		{
			triggerList.push(tersus.sqlize(trigger.role));
			//currently handling only leaf data types
			if (trigger.isRepetitive)
			{
				var array = [];
				var values = trigger.getChildren(this);
				var l = values.length;
				for (var j=0;j<l;j++)
				{
					var v = values[j];
					if (v != null)
						array.push(v.leafValue);
				}
				valueList.push(array);
			}
			else
			{
				var value = this.getLeafChild(trigger.role);
				valueList.push(value);
			}
		}
	}
	var functionText = 'var func = function('+triggerList.join(',')+'){';
	var packingText = '';
	functionText += '\nvar _out={};';
	if ( this.exits)
	{
		for (var i=0; i<this.exits.length; i++)
		{
			var exit = this.exits[i];
			if (exit.role.charAt(0) != '<') // ignore 'special' exits
			{
				var varName= tersus.sqlize(exit.role)
				functionText+='var '+varName+';'
				packingText+='_out["'+exit.role+'"]='+varName+';'
			}
		}
	}
	functionText += scriptText;
	functionText += ';'
	functionText += packingText;
	functionText+='return _out;};\nfunc;';
	var f  = eval(functionText);
	var out = f.apply(this,valueList);
	for (var exitName in out)
	{
		var v=out[exitName];
		if(v!=null)
		{
			var exit = this.getExit(exitName);
			if (exit.isRepetitive)
			{
				var l = v.length;
				for (var j=0;j<l;j++)
				{
					this.chargeLeafExit(exit, v[j]);
				}
			}
			else
			{
				this.chargeLeafExit(exitName, v);
			}
		}
	};
});		


tersus.newAction('ReadResource', function tersus_ReadResource_Start()
{
	var url = this.getLeafChild('<URL>');
	var exit = this.getExit('<Content>');
	if (exit.modelId != BuiltinModels.TEXT_ID)
		modelError('<Content> must be of type Text in client side',this);
	var notFoundExit = this.getExit('<Not Found>');
	var notAuthorizedExit = this.getExit('<Not Authorized>');
	var node = this;
	var errorCallback = null;
	var callback = function(text)
	{
		if (text != null)
			node.chargeLeafExit(exit,text);
		node.continueExecution();
	};
	var errorCallback = function (status, statusText, e)
	{
		if (notFoundExit && ( this.pluginVersion==0 || status == 404 ))
			node.chargeEmptyExit(notFoundExit);
		else if (status == 403 && notAuthorizedExit)
			node.chargeEmptyExit(notAuthorizedExit);
		else
			node.resume = function()
			{
			  throw 'Failed to load "'+url+'" - status='+status+' message='+statusText+' ex='+e;
			 };
			
		node.continueExecution();
			
	}
	this.pause();
	tersus.loadURL(url, callback, false, errorCallback, null); 
});
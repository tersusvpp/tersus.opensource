/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.DynamicInvocation = function tersus_DynamicInvocation() {};
tersus.DynamicInvocation.prototype = new FlowNode();
tersus.DynamicInvocation.prototype.constructor = tersus.DynamicInvocation;
tersus.DynamicInvocation.prototype.doStart = function ()
{
	this.started();
	var modelId = null;
	modelId = this.getLeafChild('<Model Id>');
	if (! modelId)
	{
		modelExecutionError('<Model Id> is missing',this);
		return;
	}
	/* We create a process of the given model, copy trigger values on to it, and then invoke it
	 * (including outgoing links).
	 *
	 */
	
	var	constructor = tersus.repository.get(modelId);
	if (! constructor)
	{
		modelExecutionError('Model \''+modelId+'\' is missing',this);
		return;
	}
	var process = new constructor();
	process.element = this.element;
	process.parent = this.parent;
	process.mainWindow = this.mainWindow;
	process.currentWindow = this.currentWindow;
	if (process.triggers)
	{
		for (var i = 0; i < process.triggers.length; i++)
		{
			var trigger = process.triggers[i];
			var dTrigger = this.getElement(trigger.role);
			if (dTrigger)
			{
				if (trigger.isRepetitive)
				{
					var values = dTrigger.getChildren(this);
					dTrigger.removeChildren(this);
					for (var j=0; j< values.length; j++)
					{
						var value = values[j];
						trigger.addAChild(process,value)
					}
				}		
				else
				{
					var value = dTrigger.getChild(this);
					if (value)
						trigger.replaceChild(process,value);
				}
			}
		}
	}
	var node = this;
	process.whenDone = function()
	{
		node.finished();
	};
	process.setStatus(FlowStatus.READY_TO_START);
	process.invoke();
};

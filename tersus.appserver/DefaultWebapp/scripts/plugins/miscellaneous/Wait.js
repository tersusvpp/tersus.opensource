/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Wait = function tersus_Wait() {};
tersus.Wait.prototype = new ActionNode();
tersus.Wait.prototype.constructor = tersus.Wait;
tersus.Wait.prototype.done = function ()
{
	this.continueExecution();
};
tersus.Wait.prototype.resume = function()
{
};
tersus.Wait.DELAY=50;
tersus.Wait.prototype.start = function ()
{
	var time = this.getLeafChild('<Milliseconds>');
	var action = this;
	/* Not sure timing here is always correct.  We should probably always wait until the execution context is not busy
	   But there is no mechanism to do it and the required behavior is not fully defined */
	var done = function()
	{
		tersus.async.exec(function(){action.done();}, false /* don't wait */); // already waiting in checkResume  
	};
	if (time != null)
		setTimeout(done,time);  
	else
	{
		var checkResume = function()
		{
			if (tlog) tlog('browser location='+window.location.href+' tersus location='+tersus.history.location);
			tersus.history.checkLocation();
			if (isBusy() || tersus.async.isBusy())
				setTimeout(checkResume,tersus.Wait.DELAY);
			else
				tersus.async.exec(done, false /* don't wait */ ); // Looks like a redundant call to 'async' - not sure.  
		};
		tersus.async.exec(checkResume, false /* don't wait */);
	}
	if (window.trace)
		window.trace.flush();
	this.pause();
};

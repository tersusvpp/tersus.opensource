/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.CancelTimer = function tersus_CancelTimer() {};
tersus.CancelTimer.prototype = new ActionNode();
tersus.CancelTimer.prototype.constructor = tersus.CancelTimer;
tersus.CancelTimer.prototype.start = function()
{
	var timerId = this.getLeafChild("<Timer Id>");
	if (! timerId)
	{
		modelError("Missing <Timer Id> in \"Cancel Timer\" "+this.modelId);
		return;
	}
	if (tlog) tlog("Cancelled timer "+timerId);
	clearInterval(timerId);
};

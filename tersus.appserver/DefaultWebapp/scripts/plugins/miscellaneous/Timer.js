/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Timer = function tersus_Timer() {};
tersus.Timer.prototype = new ActionNode();
tersus.Timer.prototype.constructor = tersus.Timer;
tersus.Timer.prototype.start = function()
{
	if (!this.subFlowList || this.subFlowList.length == 0)
	{
		modelError("Missing activity in Timer "+this.modelId);
		return;
	}
	if (this.subFlowList.length > 1)
	{
		modelError("Ambiguous activity in Timer "+this.modelId + " (more than one sub-process)");
		return;
	}
	var activityElement = this.subFlowList[0];
	var interval = this.getLeafChild('<Interval>');
	if (!interval)
		modelError("Missing <Interval> for Timer "+this.modelId);
	var timer = this;
	var invoker = function()
	{
		if (timer.busy)
			return;
		timer.busy = true;
		var process = new BackgroundProcess(timer, activityElement);
		if (timer.links)
		{
			for (var i = 0; i < timer.links.length; i++)
			{
				var link = timer.links[i];
				if (link.source.segments.length != 1 || link.target.segments.length != 2)
				{
					modelError("Invalid link "+link.role + " in Timer " + this.modelId);
					return;
				}	
				var outerTriggerName = link.source.segments[0];
				var innerTriggerName = link.target.segments[1];
				var value = timer.getChild(outerTriggerName);
				if (value)
					process.setTriggerValue(innerTriggerName, value);
			}
		}
		process.whenDone(function()
		{
			timer.busy = false;
		});
		process.start();
		
	};
	var timerId = window.setInterval(invoker, interval);
	if (tlog) tlog("Set timer "+timerId + " [" + this.modelId+"]");
	this.chargeLeafExit('<Timer Id>', timerId);
};

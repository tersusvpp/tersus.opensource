/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function Equal() {};
Equal.prototype = new ActionNode();
Equal.prototype.constructor = Equal;
Equal.prototype.YES='<Yes>';
Equal.prototype.NO='<No>';
Equal.prototype.start = function ()
{
	var values = this.getTriggerValues();
	var lastValue = null;
	var allEqual = true;
	for (var j=0; j<values.length && allEqual; j++)
	{
		var value = values[j];
		if (lastValue == null)
			lastValue = value;
		else if (! compareValues(lastValue, value))
		{
			allEqual = false;
		}
	}
	if (allEqual)
	{
		if (this.getElement(this.YES))
			this.chargeExit(this.YES, lastValue);
	}
	else
	{
		if (this.getElement(this.NO) != null)
		{
			this.chargeEmptyExit(this.NO);
		}
	}	
};

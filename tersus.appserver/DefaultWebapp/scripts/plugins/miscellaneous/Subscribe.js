/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Subscribe = ActionNode.__subclass('Subscribe');
tersus.Subscribe.loadURL = tersus.loadURL; // overridden when notification server is on another host
tersus.Subscribe.prototype.ON_MESSAGE =  '<On Message>';
tersus.Subscribe.prototype.MESSAGE =  '<Message>';
tersus.Subscribe.prototype.CHANNEL_T =  '<Channel>';
tersus.Subscribe.prototype.CHANNEL_D =  'Channel';
tersus.Subscribe.prototype.SOURCE =  'Source';
tersus.Subscribe.prototype.TIMESTAMP =  'Timestamp';
tersus.Subscribe.prototype.TYPE =  'Type';
tersus.Subscribe.prototype.CONTENT =  'Content';
tersus.Subscribe.pendingSubscriptions = null;
tersus.Subscribe.subscriptions = {};
tersus.Subscribe.clientId = null;

tersus.Subscribe.updateSubscriptions = function()
{
	if (!tersus.Subscribe.pendingSubscriptions)
		return;
	var a = [];
	var p = tersus.Subscribe.pendingSubscriptions;
	for (var channel in p)
	{
		if (p[channel]='ADD')
			a.push('+'+channel);
		else
			a.push('-'+channel);
	}
	tersus.Subscribe.pendingSubscriptions = null;
	if (a.length > 0)
	{
		var channels= encodeURIComponent(a.join(','));
		var url = tersus.notificationServer+'?action=subscribe&clientId='+tersus.Subscribe.clientId+'&channels='+channels+'&timestamp='+(new Date()).getTime();
		if (tersus.notificationServerAppId)
			url+='&appId='+tersus.notificationServerAppId;

		tersus.Subscribe.loadURL(url, false); //TODO better error handling?
	}
};
tersus.Subscribe.reconnect = function(timeout)
{
		tersus.Subscribe.clientId = null;
		tersus.Subscribe.pendingSubscriptions = {};
		for (var channel in tersus.Subscribe.subscriptions)
			tersus.Subscribe.pendingSubscriptions[channel]='ADD';
		setTimeout(tersus.Subscribe.connect,timeout);
};

tersus.Subscribe.handleResponse = function(responseText)
{
	var p = null;var l = null;
	if(responseText)
	{
		// we expect the second line to contain the response: first line is for "keep-alive" dots
		var ll = responseText.split(/[\n]/);
		if (ll.length >=2)
			l = ll[1];
		else if (ll.length>=1)
			l = ll[0];
		if (l!= null)
			p = l.split(/[\t]/);
	}
	if (!p || p.length == 0 || ! p[0])
	{
		tersus.Subscribe.reconnect(10000);
	}
	else if (p[0] == 'CONNECTED')
	{
		tersus.Subscribe.clientId = p[1];
		tersus.Subscribe.poll();
		tersus.Subscribe.updateSubscriptions();
	}
	else if (p[0] == 'RECONNECT')
	{
		tersus.Subscribe.reconnect(500);
	}
	else if (p[0] == 'MESSAGE')
	{
		//Not using p because split doesn't work consistently across browsers
		pp = l.match(/MESSAGE\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)/);
		var timestamp = new Date(); // TODO parse pp[1] for timestamp
		var channel = tersus.Subscribe.unescapeJava(pp[2]);
		var source = tersus.Subscribe.unescapeJava(pp[3]);
		var type = tersus.Subscribe.unescapeJava(pp[4]);
		var message = tersus.Subscribe.unescapeJava(pp[5]);
		var nodeIds = tersus.Subscribe.subscriptions[channel];
		if (nodeIds)
		{
			for (var i=0; i<nodeIds.length; i++)
			{
				var node = tersus.findNode(nodeIds[i]);
				if (node)
					node.onMessage(timestamp, channel, source, type, message);
			}
		}
		tersus.Subscribe.poll();
	}
	else if (p[0] !== 'OK')
  {
		tersus.Subscribe.reconnect(10000);
	}
};
tersus.Subscribe.repoll = function(status, statusText, e)
{
	setTimeout(tersus.Subscribe.poll,1000);
};
tersus.Subscribe.handleError = function(status, statusText, e)
{
	setTimeout(tersus.Subscribe.poll,10000);
};
tersus.Subscribe.poll = function()
{
	var url = tersus.notificationServer+'?action=connect&clientId='+tersus.Subscribe.clientId+'&timestamp='+(new Date()).getTime();
	tersus.Subscribe.xhr = tersus.Subscribe.loadURL(url, true);
}
tersus.Subscribe.disconnected = true; // indicates that client is neither connected nor connecting to notification server
tersus.Subscribe.unescapeJava = function(str)
{
	if (str.search(/[^\\]["]/) >= 0)
		throw new tersus.Exception('Invalid string literal ['+str+']');
	var s = '"'+str+'"';
	return eval (s);

};
tersus.Subscribe.prototype.start = function()
{
	if (!tersus.notificationServer)
		tersus.notificationServer = tersus.rootURL+'Notifier';
	var channel = this.channel = this.getLeafChild(this.CHANNEL_T);
	var list = tersus.Subscribe.subscriptions[channel];
	if (!list)
	{
		list = [];
		tersus.Subscribe.subscriptions[channel] = list;
		if (!tersus.Subscribe.pendingSubscriptions)
		{
			tersus.Subscribe.pendingSubscriptions = {};
		}
		tersus.Subscribe.pendingSubscriptions[channel]='ADD';
	}
	list.push(this.getNodeId());
	if (tersus.Subscribe.disconnected)
	{
		tersus.Subscribe.connect();
	}
	else
	{
		setTimeout(tersus.Subscribe.updateSubscriptions,1000);
	}
	this.chargeLeafExit('<Subscription Id>',this.getNodeId());
};
tersus.Subscribe.connect = function()
{
	if (!tersus.Subscribe.connectRetries)
		tersus.Subscribe.connectRetries = 0;
	if (tersus.Subscribe.loadURL == null)
	{
		if (tersus.Subscribe.connectRetries < 20)
		{
			setTimeout(tersus.Subscribe.connect,2000);
			return;
		}
		else
		{
			tersus.error('Failed to connect to notification server: COMMFrame not loaded');
			return;
		}
	}
	tersus.Subscribe.disconnected = false;
	var url = tersus.notificationServer+'?action=connect&timestamp='+(new Date()).getTime();
	tersus.Subscribe.loadURL(url, false);
};

tersus.Subscribe.prototype.onMessage = function(timestamp,channel,source, type,content)
{
	var eventElement = this.getElement(this.ON_MESSAGE);
	if (!eventElement)
	{
		modelError('Missing sub-process "'+this.ON_MESSAGE+'"',this);
		return;
	}
	var messageTrigger = eventElement.getChildConstructor().prototype.getTrigger(this.MESSAGE);
	var message = messageTrigger.createChildInstance();
	timestamp = new Date(timestamp.getTime());
	message.setLeafChild(this.TIMESTAMP,timestamp,Operation.REPLACE);
	message.setLeafChild(this.CHANNEL_D,channel,Operation.REPLACE);
	message.setLeafChild(this.SOURCE,source,Operation.REPLACE);
	message.setLeafChild(this.TYPE,type,Operation.REPLACE);
	message.setLeafChild(this.CONTENT,content,Operation.REPLACE);
	var p = {};
	p[this.MESSAGE]=message;
	this.queueEvent(new tersus.Event(this.ON_MESSAGE), p);
};
tersus.NOTIFICATIONS_FRAME = 'notifications';
tersus.Subscribe.loadURL = function(url, useErrorHandler)
{
	return tersus.loadURL(url, tersus.Subscribe.handleResponse, false, useErrorHandler? tersus.Subscribe.handleError : tersus.Subscribe.commError);
}
tersus.Subscribe.loadURLSafari =  function(url, useErrorHandler)
{
	document.domain=tersus.Subscribe.parentDomain;
	var f = document.getElementById(tersus.NOTIFICATIONS_FRAME);
	var fw = getIFrameWindow(f);
	fw.loadURLSafari(url,useErrorHandler);
	document.domain=tersus.Subscribe.mainHost;
};
tersus.Subscribe.prototype.onload = function()
{
	if (tersus.notificationServer)
	{
   		tersus.Subscribe.mainHost = document.domain;
   		var nsHost = tersus.notificationServer.match(/\/\/([^:/]+)[:/]/)[1];
 		tersus.Subscribe.parentDomain = commonSuffix(nsHost,tersus.Subscribe.mainHost).match(/[^.]*.(.*)/)[1];
   		tersus.Subscribe.loadURL = null;
        var iframe = window.createAnIFrame(window, tersus.NOTIFICATIONS_FRAME,'notification.html');
	}
};
tersus.Subscribe.commError = function()
{
  tersus.error('An error occurred when communicating with notification server');
};

function commonSuffix(s1,s2)
{
	var l1=s1.length;
	var l2=s2.length;
	for (var l = Math.min(l1,l2); l>=0; l--)
	{
		var suf1 = s1.substr(l1-l,l);
		var suf2 = s2.substr(l2-l,l);
		if (suf1 == suf2)
			return suf1;
	}
	return null;
}

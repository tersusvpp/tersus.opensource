/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
tersus.newAction('ParseJSON', function() {
    var text = this.getLeafChild('<JSON Text>');
    var exit = this.getExit('<Value>');
    
    
    var obj;
    try
    {
	if (exit.getChildConstructor().isMap)
	    obj = JSON.parse(text);
	else // Legacy - more lenient parser
	    obj = eval('var x='+text+';x;');
    }
    catch (e)
    {
	throw new tersus.Exception("Error parsing JSON Text ("+e+") text:\n"+text,e);
    }
    if (exit.isRepetitive)
    {
	var values = obj;
    	for (var i=0; i< values.length; i++)
    	{
    	    var dataValue  = exit.createChild(this);
    	    dataValue.copyObject(values[i]);
    	}
    }
    else
    {
	var dataValue = exit.createChild(this);
	dataValue.copyObject(obj);
    }
    
});

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.ModelElement = function tersus_ModelElement() {};
tersus.ModelElement.prototype = new DataValue();
tersus.ModelElement.prototype.constructor = tersus.ModelElement;
tersus.ModelElement.prototype.pluginFunc = tersus.ModelElement;
tersus.ModelElement.prototype.toString = function()
{
	return 'ModelElement[name='+this.element.role+',owner='+this.owner.modelId + ",modelId="+this.element.modelId+']';
};
tersus.ModelElement.prototype.addGetter('Model Id',function()
{
	if (! this.element)
	{
		modelError("Invalid instnace of "+this.modelId+"  Use 'Get Model' to obtain models and model elements");
		return null;
	}
	return this.element.modelId;
});
tersus.ModelElement.prototype.addGetter('Element Name',function()
{
	if (! this.element)
	{
		modelError("Invalid instnace of "+this.modelId+"  Use 'Get Model' to obtain models and model elements");
		return null;
	}
	return this.element.role;
});

tersus.ModelElement.prototype.addGetter('Repetitive',function()
{
	if (! this.element)
	{
		modelError("Invalid instnace of "+this.modelId+"  Use 'Get Model' to obtain models and model elements");
		return null;
	}
	return this.element.isRepetitive;
});
tersus.ModelElement.prototype.addGetter('Type',function()
{
	if (! this.element)
	{
		modelError("Invalid instnace of "+this.modelId+"  Use 'Get Model' to obtain models and model elements");
		return null;
	}
	if (this.element.isDisplayElement)
		return "Display";
	else if (this.element.isSubFlow)
		return "SubProcess";
});

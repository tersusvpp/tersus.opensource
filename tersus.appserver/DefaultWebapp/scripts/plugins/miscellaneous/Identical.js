/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
tersus.newAction('Identical', function()
{
	var values = this.getTriggerValues();
	var lastValue = null;
	var allEqual = true;
        var firstValue = values[0];
        if (values.length>1)
        {
	    for (var j=1; j<values.length && allEqual; j++)
	    {
		var value = values[j];
	        allEqual = (firstValue === value);
	    }
	}
	if (allEqual)
	{
	    this.chargeExit("<Identical>", firstValue);
	}
	else
	{
	    this.chargeEmptyExit("<Not Identical>");
	}	
});

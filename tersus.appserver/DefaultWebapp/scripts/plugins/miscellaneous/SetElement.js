/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.SetElement = function tersus_SetElement() {};
tersus.SetElement.prototype = new ActionNode();
tersus.SetElement.prototype.constructor = tersus.SetElement;
tersus.SetElement.prototype.start = function ()
{
	var elementName = this.getLeafChild('<Element Name>');
	var parent = this.getChild('<Parent>');
	var value = this.getChild('<Value>');
	
	if (!parent)
	{
		modelExecutionError("Missing <Parent>",this);
		return;
	}
	if (!elementName)
	{
		modelExecutionError("Missing <Element Name>",this);
		return;
	}		
	var element = parent.getElement(elementName);
	if (element == null)
	{
		modelExecutionError("No element called "+elementName + " in "+ parent.modelId,this);
		return;
	}
	if (element.isRepetitive)
	{
		if (value != null)
			element.addAChild(parent,value);
		else
			element.removeChildren(parent);
	}
	else
	{
		if (value != null)
			element.replaceChild(parent,value, true /* reset display elements before copying new */);
		else
			element.removeChild(parent);
	}	
};

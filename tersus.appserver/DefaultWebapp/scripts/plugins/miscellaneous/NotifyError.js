tersus.newAction('NotifyError', function()
{
    var message = this.getLeafChild('<Message>');
    var details = this.getLeafChild('<Details>');
    if (!message)
    {
        var error = this.getChild('<Error>');
        if (error)
        {
            if (error.isText)
                message = error.leafValue;
            else
            {
                message == error.getLeafChild('Message');
                details = error.getLeafChild('Details');
            }
        }
    }
    if (!message) message = 'Unknown Error';
    throw ({message:message, details:details});
            
});

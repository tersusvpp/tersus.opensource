/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Model = function tersus_Model() {};
tersus.Model.prototype = new DataValue();
tersus.Model.prototype.constructor = tersus.Model;
tersus.Model.prototype.pluginFunc = tersus.Model;
tersus.Model.prototype.toString = function()
{
	return 'Model[id='+this.model.modelId+']';
};
tersus.Model.prototype.addGetter('Plugin',function()
{
	if (! this.model)
	{
		modelError("Invalid instnace of "+this.modelId+"  Use 'Get Model' to obtain models and model elements");
		return null;
	}
	return this.model.plugin;
});
tersus.Model.prototype.addGetter('Model Id', function()
{
	if (! this.model)
	{
		modelError("Invalid instnace of "+this.modelId+"  Use 'Get Model' to obtain models and model elements");
		return null;
	}
	return this.model.modelId;
});
tersus.Model.prototype.addGetter('Model Name', function()
{
	if (! this.model)
	{
		modelError("Invalid instnace of "+this.modelId+"  Use 'Get Model' to obtain models and model elements");
		return null;
	}
	return this.model.modelName;
});
tersus.Model.prototype.addGetter('Elements',function()
{
	if (! this.model)
	{
		modelError("Invalid instnace of "+this.modelId+"  Use 'Get Model' to obtain models and model elements");
		return [];
	}
	if (! this.elementsWrappers)
	{
		var elementsElement = this.getElement('Elements');
		this.elementWrappers = [];
		if (this.model.elementList)
		{
			for (var i=0;i<this.model.elementList.length;i++)
			{
				var element = this.model.elementList[i];
				if (!element.wrapper)
				{
					element.wrapper = elementsElement.createChildInstance();
					element.wrapper.element = element;
				}
				this.elementWrappers.push(element.wrapper);
			}
		}
	}
	return this.elementWrappers;
});

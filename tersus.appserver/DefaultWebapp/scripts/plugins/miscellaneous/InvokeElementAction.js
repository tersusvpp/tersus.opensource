/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.InvokeElementAction = function() {};
tersus.InvokeElementAction.prototype = new ActionNode();
tersus.InvokeElementAction.prototype.constructor = tersus.InvokeElementAction;
tersus.InvokeElementAction.prototype.start = function()
{
	var targetElement = this.getChild('<Element>');
	var action = this.getLeafChild('<Action>');
	if (targetElement.invokeAction)
	{
		var output = targetElement.invokeAction(action, this);
		// Here we would need to pause the current process, run the target action, and when that action is completed, resume the current action
	}
};


/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.CreateFromTemplate = function() {};
tersus.CreateFromTemplate.prototype = new ActionNode();
tersus.CreateFromTemplate.prototype.constructor = tersus.CreateFromTemplate;
tersus.CreateFromTemplate.prototype.start = function()
{
	var t = this.getLeafChild('<Template>');
	var i=0;
	var l = t.length;
	var out = [];
	while (i<l&& i>= 0)
	{
		var n = t.indexOf('${',i);
		if (n == -1)
		{
			out.push(t.substring(i));
			i = n;
		}
		else
		{
			out.push(t.substring(i,n));
			var b = n+ 2;
			var e=t.indexOf('}', b);
			if (e<0)
			{
				modelExecutionError("Invalid template: missing '}' for '${'.  Position="
							+ n
							+ " Text="
							+ t.substring(n, n + 20));
				return null;
			}
			var key=t.substring(b,e);
			out.push(this.getLeafChild(key));
			i = e+1;
		}

	}
	this.chargeLeafExit('<Text>',out.join(''));
}


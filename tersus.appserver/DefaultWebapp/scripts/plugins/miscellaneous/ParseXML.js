tersus.newAction('ParseXML', function()
{
	var xml = this.getLeafChild('<XML Document>');
	var exit = this.getExit('<Parsed Tree>');
	var allowUnknownElements = false; // When adding trigger, make sure to add equivalent in server side
	
	var doc;
	if (window.DOMParser)
	{
		var parser = new DOMParser();
		doc = parser.parseFromString (xml, "text/xml");
	} 
	else if (window.ActiveXObject)
	{
	    doc = new ActiveXObject ("Microsoft.XMLDOM");
	    doc.async = false;
	    doc.loadXML(xml);
    }
 	
 	var node = this.convert(doc.documentElement,exit, allowUnknownElements);
 	if (!node.isLeaf || node.leafValue != null)
 		this.chargeExit(exit,node);	
});
tersus.ParseXML.prototype.convert = function (domElement, element, allowUnknownElements)
{
	var text;
	var nodeConstructor = element.getChildConstructor();
	var prototype = nodeConstructor.prototype;
	var node = null;
	if (prototype.isLeaf)
		text = [];
	else
	{
		node = new nodeConstructor(); // Composite is always created
		for (var i=0;i<domElement.attributes.length;i++)
		{
			var attribute = domElement.attributes[i];
			if (attribute.value)
			{
				var localName = attribute.localName ? attribute.localName : attribute.baseName; // baseName is used in IE
				var elementHandler = this.getElementHandler(node,attribute.nodeName, attribute.localName, attribute.namespaceURI, true);
				if (elementHandler != null)
				{
			 		try
					{
						elementHandler.createChild(node).parseString(attribute.value, elementHandler);
					}
					catch (e)
					{
						throw {message:'Error parsing XML attribute: tagName='+domElement.tagName+' attribute:'+attribute.nodeName+ ' modelId='+elementHandler.modelId+'\nmessage='+e.message};
					}
				}
				else if (!allowUnknownElements && attribute.name.indexOf('xmlns')!=0)
			 		throw {message:'Error parsing XML: Unrecognized attribute ' + attribute.name + ' in '+prototype.modelId};
				
			}
		}		
	}
	for (var i=0;i<domElement.childNodes.length;i++)
	{
		var cn = domElement.childNodes[i];
		if (cn.nodeType == 1 || cn.nodeType == 2)
		{
			var localName = cn.localName ? cn.localName : cn.baseName; //naseName in IE
			var elementHandler = this.getElementHandler(node,cn.nodeName, localName, cn.namespaceURI, cn.nodeType==2);
			if (elementHandler)
			{
				if (cn.nodeType == 1)
				{
					var childNode = this.convert(cn, elementHandler, allowUnknownElements);
					if (childNode != null)
						elementHandler.addChild(node, childNode); 
				}
			}
			else if (!allowUnknownElements)//TODO support permissive parsing
			 	throw {message:'Error parsing XML: Unrecognized element ' + cn.nodeName + ' in '+prototype.modelId};
		}
		else if (text && cn.nodeType == 3) // Text Node
		{
			if (cn.nodeValue)
				text.push(cn.nodeValue);
		}
	}
	if (text && text.length>0) // Leaf node with actual text
	{
		node = new nodeConstructor();
		try
		{
			node.parseString(text.join(''), element);
		}
		catch (e)
		{
			throw {message:'Error parsing XML content: tagName='+domElement.tagName+' modelId='+node.modelId+'\nmessage='+e.message};
		}
	}
	return node;
};

tersus.ParseXML.prototype.getElementHandler = function(parent, name, localName, namespaceURI, isAttribute)
{
	if (!parent.elements)
		return;
	if (namespaceURI != null)
	{
		var elementName = isAttribute ?  '#'+localName : localName;
		for (var elementRole in parent.elements)
		{
			var e = parent.elements[elementRole];
			if (! e.xmlNamespace)
			{
				e.xmlNamespace = e.getChildConstructor().prototype.xmlNamespace;
			}
			if (e.xmlNamespace == namespaceURI)
			{
				if (! e.xmlName)
					e.xmlName = elementRole.substr(elementRole.lastIndexOf(':')+1);
				if (e.xmlName == elementName)
					return e;
			} 
		}
	};
	// Not found with explicit namespaceURI - search by raw name
	var elementName = isAttribute ?  '#'+name : name;
	return parent.elements[elementName];
};

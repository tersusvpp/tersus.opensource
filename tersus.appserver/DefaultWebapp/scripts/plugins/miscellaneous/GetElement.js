/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.GetElement = function tersus_GetElement() {};
tersus.GetElement.prototype = new ActionNode();
tersus.GetElement.prototype.constructor = tersus.GetElement;
tersus.GetElement.prototype.start = function ()
{
	var elementName = this.getLeafChild('<Element Name>');
	var exit = this.getExit('<Element>');
	var parent = this.getChild('<Parent>');
	if (parent && parent.leafValue && parent.leafValue.tagName) // Special handling of html node attributes
	{
		var attributeValue = parent.leafValue.getAttribute(elementName);
		this.chargeLeafExit('<Element>',attributeValue);
	}
	else if (parent != null && parent.getChild)
	{
		
		var element = parent.getElement(elementName);
		var values = null;
		if (element)
		{
			if (element.isRepetitive)
			{
				values = element.getChildren(parent);
				if (values)
				{
					if (!exit.isRepetitive)
						for (var i=0;i<values.length;i++)
							this.chargeExit(exit, values[i]);
					else
						for (var i=0;i<values.length;i++)
							this.accumulateExitValue(exit, values[i]);
											 
				}
			}
			else
			{
				var value = element.getChild(parent);
				if (value != null)
				{
					if (!exit.isRepetitive)
						this.chargeExit(exit, value);
					else
						this.accumulateExitValue(exit, value);
				}
			}
		}
	}
};

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Unsubscribe = tersus.CallJavaApplet.__subclass('Unsubscribe');

tersus.Unsubscribe.prototype.start = function()
{
	var id = this.getLeafChild('<Subscription Id>');
	var node = tersus.findNode(id);
	if (node)
	{
		var channel = node.channel;
		if (channel)
		{
			var list = tersus.Subscribe.subscriptions[channel];
			if (list)
			{
				for (var i=0;i<list; i++)
				{
					if (list[i] == id)
					{
						list.splice(id,1);
						if (list.length == 0)
						{
							delete tersus.Subscribe.subscriptions[id];
							tersus.Subscribe.pendingSubscriptions[channel] = 'REMOVE';
							setTimeout(tersus.updateSubscriptions,1000); // we're delaying updating of subscriptions in order to use one server request for numerous subscriptions
						}
						break;
					}
				}
			}
		}
		
	}

};

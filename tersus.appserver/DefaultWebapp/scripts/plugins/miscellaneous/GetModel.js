/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.GetModel = function tersus_GetModel() {};
tersus.GetModel.prototype = new ActionNode();
tersus.GetModel.prototype.constructor = tersus.GetModel;
tersus.GetModel.prototype.start = function ()
{
	var modelId = null;
	var modelIdRole =  '<Model Id>';
	var instanceRole = '<Instance>';
	var levelRole = '<Level>';
	var modelIdTrigger = this.getElement(modelIdRole);
	var instanceTrigger = this.getElement(instanceRole);
	var levelTrigger = this.getElement(levelRole);
	
	if (((modelIdTrigger ? 1 : 0) + (instanceTrigger ? 1 : 0) + (levelTrigger ? 1 : 0)) > 1)
		modelError(modelIdRole + ", " + instanceRole + " and " + levelRole + "  must be used separately");
	if (modelIdTrigger)
		modelId = this.getLeafChild(modelIdRole);
	else
		if (instanceTrigger)
		{
			instance = this.getChild(instanceRole);
			if (instance)
				modelId = instance.modelId;
		}
		else
		{
			var level = 1;
			if (levelTrigger)
				level = this.getLeafChild(levelRole);
			
			if (level < 0)
				modelId = window.rootDisplayNode.modelId;
			else
			{
				var self = this;
				var parent = this;
				while (level-- > 0 && parent != null)
				{
					self = parent;
					parent = self.parent;
				}
				modelId = (parent != null ? parent: self).modelId;
			}
		}
	
	var model = null;
	if (modelId)
		model = tersus.repository.get(modelId).prototype;
	if (model)
	{
		if (! model.wrapper)
		{
			var exit =  this.getExit('<Model>');
			model.wrapper = exit.createChildInstance();
			model.wrapper.model = model;
		}
		this.chargeExit('<Model>',model.wrapper);
	}
};

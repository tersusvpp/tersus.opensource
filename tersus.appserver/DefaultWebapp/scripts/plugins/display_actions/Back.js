/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Back = function tersus_GetElmentName() {};
tersus.Back.prototype = new ActionNode();
tersus.Back.prototype.constructor = tersus.Back;
tersus.Back.prototype.start = function ()
{
	setTimeout(function () {window.history.back();},0);
};

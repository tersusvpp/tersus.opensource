/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 

function PopupCalendar(){};
PopupCalendar.prototype = new FlowNode();
PopupCalendar.prototype.constructor = PopupCalendar;

PopupCalendar.prototype.start = function()
{
	//Find the containing view node - to get the location
	var visualParent = this.parent;
	while (! visualParent.viewNode)
		visualParent = visualParent.parent;
	
	// Create a hidden field to hold the value and 'intercept' value change
 	var date = this.getChild('<Base Date>');
	var node = this;
	if ($.fn.datepicker && tersus.UIDateField)
	{
	    var $f = $('<input type="hidden">').insertBefore($(visualParent.viewNode)); // May need to insert after for RTL
	    if (date != null)
	        $f[0].value = date.formatString();
	    $f.datepicker(
	    {
	        dateFormat:tersus.UIDateField.prototype.convertFormat(DATE_FORMAT)
	        ,onSelect:function(text, dp)
	        {
	            var date=createDate(dp.currentYear, dp.currentMonth+1, parseInt(dp.currentDay));
	            node.chargeLeafExit('<Date>', date);
	            node.continueExecution();
	        }
	        ,onClose:function()
	        {
	            if (node.status === FlowStatus.PAUSED)
	            {
	                node.continueExecution();
	            };
	        }
	    });
	    node.pause();
	    $f.datepicker("show");
	}
	else
	{
	    var hiddenField = visualParent.hiddenDateFieldForPopupCalendar;
	    if ( ! hiddenField)
	    {
	        hiddenField = visualParent.createHTMLElement('input');
	        hiddenField.type = 'hidden';
	        this.currentWindow.document.body.appendChild(hiddenField);
	        visualParent.hiddenDateFieldForPopupCalendar = hiddenField;
	    }
	    if (date != null)
	        hiddenField.value = date.formatString();
    	var valueHandler = function handleDate(year, month, day)
    	{
    		var date = createDate(year, month, day);
    		node.chargeLeafExit('<Date>', date);
    		node.readyToResume();
    		resumeRoot();
    	};
	    var posElement  = visualParent.viewNode;
	    while (posElement && $(posElement).css('display')=='inline')
		posElement = posElement.parentElement;
	    visualParent.currentWindow.popUpCalendar(posElement, hiddenField, valueHandler, DATE_FORMAT);
    }
};

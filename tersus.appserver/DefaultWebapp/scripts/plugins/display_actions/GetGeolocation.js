tersus.newAction('GetGeolocation',function()
{
	var position = null;
	var enableHighAccuracy = this.getLeafChild('<Enable High Accuracy>') || false;
	var timeout = this.getLeafChild('<Timeout>');
	var maxAge = this.getLeafChild('<Maximum Age>');
	
	var options = {enableHighAccuracy:enableHighAccuracy, maximumAge:maxAge, timeout:timeout};
	var gl = navigator.geolocation;
	if (gl && gl.getCurrentPosition)
	{
		this.pause();
		this.errorMessage=null;
		var nodeId = this.getNodeId();
		gl.getCurrentPosition(function(p){tersus.GetGeolocation.handleResponse(p,null,nodeId);}
		, function(error){tersus.GetGeolocation.handleResponse(null, error,nodeId);}
		,options);
	}
  	else
		this.chargeEmptyExit('<None>');	
});

tersus.GetGeolocation.prototype.resume = function()
{
	if (this.errorMessage)
		throw this.errorMessage;
};
tersus.GetGeolocation.handleResponse = function (p,error, nodeId)
{
	var node = tersus.findNode(nodeId);
	if (!node && node.status != FlowStatus.PAUSED)
		return;
	if (error)
	{
	    if (error.code == error.TIMEOUT && node.elements && node.elements['<Timed Out>'] )
	        node.chargeEmptyExit('<Timed Out>');
	    else if (error.code == error.PERMISSION_DENIED && node.elements && node.elements['<Permission Denied>'])
            node.chargeEmptyExit('<Permission Denied>');
	    else
	        node.errorMessage=error.message;
	}
	else 
	{
		if (p)
		{
			var exit = node.getExit('<Geolocation>');
   	     	if (exit)
   	     	{   	     		
  				var loc = exit.createChildInstance();
  				var cElement = loc.getElement('Coordinates');
  				var cStruct = cElement.createChild(loc);
  				var c = p.coords;
  				cStruct.setLeafChild('Latitude',c.latitude, Operation.REPLACE);
  				cStruct.setLeafChild('Longitude',c.longitude, Operation.REPLACE);
  				cStruct.setLeafChild('Accuracy',c.accuracy, Operation.REPLACE);
  				cStruct.setLeafChild('Altitude',c.altitude, Operation.REPLACE);
  				cStruct.setLeafChild('Altitude Accuracy',c.altitude_accuracy, Operation.REPLACE);
  				cStruct.setLeafChild('Heading',c.heading, Operation.REPLACE);
  				cStruct.setLeafChild('Speed',c.speed, Operation.REPLACE);
  				
  				
  				var ts = p.timestamp;
  				if (ts)
  				{
  					var dt = new Date();
  					dt.setTime(ts);
  					loc.setLeafChild('Timestamp',dt,  Operation.REPLACE);
  				}
  				node.chargeExit(exit,loc);
  			}
  		}
  		else
			node.chargeEmptyExit('<None>');
	}	
	node.continueExecution();
};
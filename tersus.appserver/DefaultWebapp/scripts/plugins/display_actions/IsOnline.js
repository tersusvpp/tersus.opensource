/*********************************************************************************************************
 * Copyright (c) 2003-2016 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
tersus.newAction('IsOnline',function()
{
	node = this;
	var callbackYes = function(text)
	{
		node.chargeEmptyExit('<Yes>')			
		node.continueExecution();
	};
	var callbackNo = function (status, statusText, e)
	{
		node.chargeEmptyExit('<No>')			
		node.continueExecution();
	};

	if (navigator.onLine)
	{
		var explicitURL = this.getLeafChild('<URL>');
		var baseURL = tersus.serverURL ? (tersus.serverURL + '/') : tersus.rootURL;
		var url = this.explicitURL ? this.explicitURL : (baseURL + 'blank.html?' + (new Date()).getTime());
    	var timeout = this.getLeafChild('<Timeout>');
    	this.pause();
    	tersus.loadURL(url, callbackYes, false, callbackNo, null, timeout); 
	}
	else
		callbackNo();			
});


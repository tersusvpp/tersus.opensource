tersus.newAction('Print',function()
{
	var title = this.getLeafChild('<Title>');
	var heading = this.getLeafChild('<Heading>');
	var content = this.getChild('<Content>');
	var style = this.currentWindow.document.getElementById('main_stylesheet');
	var domNode = content.wrapperNode ? content.wrapperNode : content.viewNode;
	if (domNode == null)
		throw {message:'<Content> must be a reference to a rendered display element. Data-only display elements cannot be printed.'};
	
	var values = {Title:title?tersus.escapeHTML(title):'', Heading: heading? '<h1>'+tersus.escapeHTML(heading)+'</h1>' : '', Content:tersus.outerHTML(domNode), Style: tersus.outerHTML(style) };
	var template='<html><head><title>${Title}</title>${Style}</head><body>${Heading}${Content}<script type="text/javascript">window.popupTest="OK";</script></body></html>';
	var html = template.replace(/[$]{([^{}]+)}/g,
        function (all, token) {
          return values[token];
        }
	);
	var w = window.open();
	if (w && w.focus)
		tersus.async.exec(
			function() {
			    w.document.write(html);
			    w.document.close();
			    setTimeout(function(){
			    	w.print();
					w.close();
				},50);
			}, false /* don't wait */
		);
	else
    	throw {message:"Cannot print because pop-up windows are blocked by the browser. Please enable pop-up windows for this application."};
});

tersus.outerHTML = function(node)
{
	var html;
	if (node.outerHTML)
		return node.outerHTML;
	else if (window.XMLSerializer)
	{
		return (new XMLSerializer()).serializeToString(node);
	}
	else
		throw {message:"Cannot print: no outerHTML or XMLSerializer"};
};
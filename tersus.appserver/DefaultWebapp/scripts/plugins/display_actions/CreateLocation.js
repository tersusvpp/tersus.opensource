/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.CreateLocation = function tersus_CreateLocation() {};
tersus.CreateLocation.prototype = new ActionNode();
tersus.CreateLocation.prototype.RELATIVE='<Relative>';
tersus.CreateLocation.prototype.BASE_URL='<Base URL>';
tersus.CreateLocation.prototype.constructor = tersus.CreateLocation;
tersus.CreateLocation.prototype.start = function ()
{
	var relative = this.getTriggerLeafValue(this.RELATIVE);
	var baseURL = this.getTriggerLeafValue(this.BASE_URL);
	if (relative && baseURL)
	{
		modelExecutionError(this.RELATIVE + ' and '+ this.BASE_URL+'should not be used together in Create Location',this);
		relative = false;
	}
	var params;
	if (relative)
		params = tersus.getInternalParameters();
	else
		params = {};
	if (this.triggers)
	{
		for (var i=0; i< this.triggers.length; i++)
		{
			var trigger = this.triggers[i];
			if (trigger.role != this.RELATIVE && trigger.modelId != BuiltinModels.NOTHING_ID)
			{
				var value = trigger.getChild(this);
				if (value != null)
				{
					if (value.serialize)
						value = value.serialize();
					else
						value = value.leafValue;
				}
				params[trigger.role]= value;
			}
		}
	}
	
	var URL;
	if (baseURL)
	{
		URL = baseURL;
		if (URL.indexOf('#') < 0 && URL.indexOf('?')<0)
			URL+='?';
	}
	else
	{
		URL = tersus.getCurrentViewURL();
		if (URL.indexOf('#') <0)
			URL += '#';
	}
	
	var lastChar = URL.charAt(URL.length-1);
	var first =  lastChar == '#' || lastChar == '?';
	for (var p in params)
	{
		var v = params[p];
		if (v !== null && v !== undefined)
		{
			if (! first)
				URL +='&';
			URL += encodeURIComponent(p)+'='+encodeURIComponent(v);
			first = false;
		}
	}
	this.chargeLeafExit('<URL>',URL);		
};

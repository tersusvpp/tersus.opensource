/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
tersus.appletName = 'tersus.applets.Base';
tersus.appletArchives = 'applets.jar';
tersus.CallJavaApplet = ActionNode.__subclass('CallJavaApplet');
tersus.CallJavaApplet.separateJVM=true;
tersus.CallJavaApplet.prototype.start = function()
{
	var appletName = this.getLeafChild('<Applet Class Name>');
	if (appletName == null)
		appletName = tersus.appletName;
	var archives = this.getLeafChild('<Archives>');
	if (archives == null)
		archives = tersus.appletArchives;
	var methodName = this.getLeafChild('<Method Name>');
	var args = this.prepareArguments();
	this.separateJVM = this.getLeafChild('<Separate JVM>');
	if (this.separateJVM == null)
		this.separateJVM = tersus.CallJavaApplet.separateJVM;

	this.waitForApplet(appletName,methodName,args, archives);
};
tersus.CallJavaApplet.prototype.waitForApplet = function waitForApplet(appletName,methodName,args, archives)
{
	if (document.applets[appletName])
	{
		var output =  this.callApplet(appletName,methodName, args);
		if (output == '__ASYNC')
			this.pause();
		else
		{
			if (output != null)
				this.handleOutput(output);
			delete FlowNode.map[this.nodeId];
		}

	}
	else
	{
		this.pause();
		var rootContext = (this.getExecutionContext() == window.rootDisplayNode);
		if (rootContext)
		{
			tersus.progress.set('Loading applet '+appletName);
		}
		var node = this;
		setTimeout(function()
		{
			node.createApplet(appletName,archives);
			node.startTime = new Date();
			node.called=false;
			node.timerId = setInterval(function ()
			{
				var active = false;

				try
				{
					active = document.applets[appletName] && document.applets[appletName].isActive();
				}
				catch ( e)
				{
					active = false;
				}

				if (((new Date())-node.startTime) > 60000 || active)
				{
					clearInterval(node.timerId);
					if (! node.called) // Prevent duplicate calling
					{
						node.called=true;
						var output = node.callApplet(appletName,methodName, args);
						if (output != '__ASYNC')
							node.handleResponse(output);
					}
					// Note that sometimes the timer is invoked one extra time to clearing the timer is not enough
				}
			},100);
		},10);
	}
};
tersus.CallJavaApplet.prototype.createApplet = function(appletName,archives)
{
	if (!archives)
		archives = '';
	var applet = document.createElement('applet');
	applet.codebase='.';
	applet.width=1;
	applet.height=1;
	applet.setAttribute('id',appletName);
	applet.setAttribute('name',appletName);
	applet.setAttribute('mayscript','true');
	applet.setAttribute('separate_jvm',this.separateJVM);
	applet.code=appletName;
	applet.archive=archives;
	var c = document.getElementById('applet_container');
	if (!c)
	{
		c = document.createElement('div');
		c.id='applet_container';
		document.body.appendChild(c);
	}
	c.appendChild(applet);
};

tersus.CallJavaApplet.prototype.prepareArguments = function()
{
	var st = this.triggers.concat([]);
	var args = [];
	st.sort(function(a,b){ if (a.role < b.role) return -1; else if (a.role == b.role) return 0; else return 1;});
	for (var i=0;i<st.length;i++)
	{
		var t = st[i];
		if (t.role.charAt(0) != '<' && t.modelId != BuiltinModels.NOTHING_ID  )
		{
			if (t.isRepetitive)
			{
				var a = [];
				var l =t.getChildren(this);
				if (l)
				{
					for (var j=0;j<l.length; j++)
					{
						var v = l[j];
						if (v!= null &&  v.leafValue != null)
							a.push(v.leafValue);
						 else
						  	a.push(v);
					}
				}
				args.push(a);
			}
			else
			{
				var v = t.getChild(this);
				if (v!= null && v.leafValue != null)
					args.push(v.leafValue);
				else
					args.push(v);
			}
		}
	}
	return args;
};
tersus.CallJavaApplet.prototype.callApplet = function(appletName,methodName,args)
{
	var rootContext = (this.getExecutionContext() == window.rootDisplayNode);
	if (! document.applets[appletName])
	{
		if (rootContext)
			tersus.progress.clear();
		modelExecutionError('Applet '+appletName + ' not loaded',this);
		return;
	}
	var applet = document.applets[appletName];
	if (rootContext)
		tersus.progress.set("Calling "+methodName);
	window.tersus_CallJavaApplet_caller = this;
	window.tersus_CallJavaApplet_caller_id = this.getNodeId();
	if (window.browser == 'IE')
	{
		var invocation = 'applet.'+methodName+'(';
		for (i=0;i<args.length;i++)
		{
			if (i>0)
				invocation += ',';
			invocation+='args['+i+']';
		}
		invocation += ')';
		var output = eval(invocation);
	}
	else
		var output = applet[methodName].apply(applet,args);
	window.tersus_CallJavaApplet_caller = null;
	window.tersus_CallJavaApplet_caller_id = null;

	return output;

};
tersus.CallJavaApplet.prototype.isJavaException = function (e)
{
	if ( typeof e === 'object' &&  'getMessage' in e && 'getClass' in e )
	{
		var n = e.getClass().getName()+'';
		return n.match(/Exception|Error/) ? true: false;
	}
	else
		return false;
};
tersus_handle_applet_response = function (nodeId, output, exitName)
{
	if (tlog)
		tlog('tersus_handle_applet_response id='+nodeId);
	var node = tersus.findNode(nodeId);
	if (!node)
	{
		tersus.error('Applet node #'+nodeId+ ' not found');
		return;
	}
	if (tlog)
		tlog('Processing applet response for '+node.modelName+' ['+nodeId+']');
	node.handleResponse(output,exitName);
};
tersus_handle_applet_error = function (nodeId, error)
{
	error = ''+error; //convert to JS string
	if (tlog)
		tlog('tersus_handle_applet_error id='+nodeId);
	var node = tersus.findNode(nodeId);
	if (!node)
	{
		tersus.error('Applet node #'+nodeId+ ' not found');
		return;
	}
	if (tlog)
		tlog('Processing applet response for '+node.modelName+' ['+nodeId+']');

	if (error.match(/^{/))
		error = eval('var e='+error+';e;'); //TODO use string JSON parse
	node.appletError = error;
	node.handleResponse(null,null);
};
tersus_charge_applet_leaf_exit = function (nodeId, exitName, value)
{
	var node = tersus.findNode(nodeId);
	if (!node)
	{
		tersus.error('Applet node #'+nodeId+ ' not found');
		return;
	}
	node.chargeLeafExit(exitName, value);
};
tersus.CallJavaApplet.prototype.handleOutput = function(output, exitName)
{
	if (!exitName)
		exitName = '<Output>';
	if (output != null)
		this.chargeLeafExit(exitName,output);
}
tersus.CallJavaApplet.prototype.handleResponse = function(output,exitName)
{
	var executionContext = this.getExecutionContext();
	if (executionContext == window.rootDisplayNode)
		tersus.progress.set('Processing response');
	if (output)
	{
		try
		{
			if (this.isJavaException(output))
			{
				this.appletError = output;
			}
			else
			{
				this.handleOutput(output,exitName);
			}
		}
		catch (e)
		{
        	if (!this.appletError)
        		this.appletError = e;
        }
	}
	this.continueExecution();


	this.destroy(); // Important - removes node from the node map
	this.children=null; //To be on the safe side in case an applet holds a reference to the node
	this.parent = null; //To be on the safe side ..
};
tersus.CallJavaApplet.prototype.resume = function()
{
	if (this.appletError)
		throw this.appletError;
}

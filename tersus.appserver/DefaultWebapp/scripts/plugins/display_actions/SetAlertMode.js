/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.SetAlertMode = function tersus_SetAlertMode() {};
tersus.SetAlertMode.prototype = new ActionNode();
tersus.SetAlertMode.prototype.constructor = tersus.SetAlertMode;
tersus.SetAlertMode.prototype.start = function ()
{
	tersus.alertMode.interactive = this.getLeafChild('<Interactive>');
	var errors = this.getLeafChild('<Errors>');
    if (errors || (errors == null))
    	tersus.errors = null;
    else
    	tersus.errors = [];
	tersus.alertMode.confirm = this.getLeafChild('<Confirm>');
}


/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function GetTableSelection() {};
GetTableSelection.prototype = new FlowNode();
GetTableSelection.prototype.constructor = GetTableSelection;
GetTableSelection.prototype.start = function getTableSelection()
{
	var table = this.getChild('<Table>');
	var selection = table.selectedRow;
	if (selection)
		this.setChild('<Selected Row>', selection, Operation.REPLACE);
	else
		this.setLeafChild('<None Selected>', 'none', Operation.REPLACE);
};

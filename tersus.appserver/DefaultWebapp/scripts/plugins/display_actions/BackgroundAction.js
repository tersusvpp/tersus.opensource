/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.BackgroundAction = function tersus_BackgroundAction() {};
tersus.BackgroundAction.prototype = new ActionNode();
tersus.BackgroundAction.prototype.constructor = tersus.BackgroundAction;
tersus.BackgroundAction.prototype.start = function tersus_BackgroundAction_start()
{
	if (this.parent.isBackground)
	{
		// This is the "final" invocation (in the background)
		ActionNode.prototype.start.call(this);
	}
	else
	{
		// This is the "original" invocation that should start a background process
		var process = new BackgroundProcess(this.parent, this.element);
		if (this.triggers)
			for (var i = 0; i < this.triggers.length; i++)
			{
				var trigger = this.triggers[i];
			
				if (trigger.isRepetitive)
				{
					internalError("Repetitive triggers not supported in Background Action");
				}
				else
				{
					var value = this.getChild(trigger.role);
					if (value)
						process.setTriggerValue(trigger.role, value);
				}
			}
		setTimeout (function(){
		tersus.async.exec(function (){process.start()}, false /* don't wait */);},0);// Using setTimeout to ensure we're not continuing in same 'burst' of Javascript
	}
};

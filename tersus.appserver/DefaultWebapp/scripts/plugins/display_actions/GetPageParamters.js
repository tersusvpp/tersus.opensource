/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function GetPageParameters() {};
GetPageParameters.prototype = new FlowNode();
GetPageParameters.prototype.constructor = GetPageParameters();

GetPageParameters.prototype.start = function()
{
	for (var i=0; i< this.exits.length; i++)
	{
		var exit = this.exits[i];
		var paramValue = tersus.searchParams[exit.role];
		if (paramValue)
		{
			this.setLeafChild(exit.role, paramValue, Operation.REPLACE);
		}
	}
};
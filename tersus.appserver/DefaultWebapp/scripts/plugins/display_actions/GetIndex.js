/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.newAction('GetIndex', function()
{
	var e = this.getChild('<Element>');
	if (e.element && e.element.isRepetitive)
	{
		var siblings = e.element.getChildren(e.parent);
		for (var i=0;i<siblings.length;i++)
		{
			if (siblings[i] == e)
				this.chargeLeafExit('<Index>',this.pluginVersion>0 ? i+1 : i);
		}
	}
	
});


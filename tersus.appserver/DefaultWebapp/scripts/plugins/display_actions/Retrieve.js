/*********************************************************************************************************
 * Copyright (c) 2003-2011 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/

var m = tersus.baseURL.match('^(http|https)://[^/]+(:[0-9]+)?/([^/]*/)');
tersus.appScopePrefix =  (m && m.length>3)? m[3] : '';
tersus.newAction('Retrieve',function()
{	
	var key = this.getLeafChild('<Key>');
	var prefix = '';
	if (key)
	{
        if (tersus.useFilesystemForStorage)
        {
            this.retrieveFromFileSystem(key);
            return;
        }
		var type = this.getLeafChild('<Storage Type>') || 'LOCAL';
		var s;
		if (type == 'LOCAL')
		{
			s = window.localStorage;
			if (s == null)
				throw 'No local storage available';
			key = tersus.appScopePrefix+key;
		}
		else if (type == 'SESSION')
		{
			s = window.sessionStorage;
			if (s == null)
				throw 'No session storage available';
		}
		if (tlog) tlog('Retrieving :'+key + ' from '+type + ' storage');
		
		var value = s.getItem(key,value);
		if (tlog) tlog('Value :'+ (value == null ? 'null' : (value.length + 'characters')));
		
		if (value != null && value.length>0)
			this.chargeLeafExit('<Value>',value);
		else
		{
			this.chargeEmptyExit('<None>');
		}
	}
});

$.extend(tersus.Retrieve.prototype, {
    getErrorMessage: function(code)
    {
        switch(code)
        {
            case FileError.NOT_FOUND_ERR: return 'Not found'; 
            case FileError.SECURITY_ERR: return 'Security error';
            case FileError.ABORT_ERR: return 'Aborted';
            case FileError.NOT_READABLE_ERR: return 'Not redable';
            case FileError.ENCODING_ERR: return 'Encoding error';
            case FileError.NO_MODIFICATION_ALLOWED_ERR: return 'No modification allowed';
            case FileError.INVALID_STATE_ERR: return'Invalid state';
            case FileError.SYNTAX_ERR: return 'Syntax error';
            case FileError.INVALID_MODIFICATION_ERR: return 'Invalid modification';
            case FileError.QUOTA_EXCEEDED_ERR: return 'Quota exceeded';
            case FileError.TYPE_MISMATCH_ERR: return 'Type mismatch';
            case FileError.PATH_EXISTS_ERR: return 'Path exists';
            default: return 'Unknown error';
        }   
    }
    ,retrieveFromFileSystem:function(key)
    {
        var self = this;
        var fail = function(error)
        {
          self.resume = function()
          { 
              throw 'Error retrieving file:'+self.getErrorMessage(error.code)+' (code='+error.code+')';
          };
          self.continueExecution();
       
        };
        var done = function(value)
        {
            self.resume = function()
            {
                if (value)
                    this.chargeLeafExit('<Value>', value);
                else
                    this.chargeEmptyExit('<None>');
                self.finished();
            }
            self.continueExecution();
        };
        var fsSuccess = function(fileSystem)
        {
            var fileEntrySuccess = function(fileEntry)
            {
                var fileSuccess = function(file)
                {
                    var reader = new FileReader();
                    reader.onloadend = function (evt)
                    {
                        
                        if (reader.error)
                            fail(reader.error);
                        else
                        {
                            done(reader.result);
                        }
                    };
                    reader.readAsText(file);
                };
                fileEntry.file(fileSuccess, fail);
            };
            var fileEntryError = function(error)
            {
                if (error.code == FileError.NOT_FOUND_ERR)
                   done(null);
                else
                    fail(error);
            }
            fileSystem.root.getFile(key, {create:false}, fileEntrySuccess, fileEntryError);
        };
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, fsSuccess, function(evt)
                {
                    fail(evt.target.error);
                });
        self.pause();
    }
});

/*********************************************************************************************************
 * Copyright (c) 2003-2022 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
tersus.newAction('DetectBrowser',function()
{
	var userAgent = navigator.userAgent;

	var triggerBrowsers = this.getTrigger('<Browsers>');
	if (triggerBrowsers)
	{
		var browsers = triggerBrowsers.getChildren(this);
	}

	var userAgent = navigator.userAgent;
	var browserName = getBrowserName(browsers, userAgent);
	
	if (!browserName)
	{
		var exitUnknown = this.getExit('<Unknown>');
		if (exitUnknown)
			this.chargeEmptyExit(exitUnknown);			
	}
	else
	{
		var exitBrowser = this.getExit('<Browser>');
		if (exitBrowser)
			this.chargeLeafExit(exitBrowser,browserName);		

		var exitExplicitBrowserName=this.getExit(browserName);
		if (exitExplicitBrowserName)
			this.chargeEmptyExit(exitExplicitBrowserName);
	}

	var exitUserAgent = this.getExit('<User Agent>');
	if (exitUserAgent)
		this.chargeLeafExit(exitUserAgent,userAgent);
		
	this.continueExecution();
});

getBrowserName = function (browsers, userAgent)
{
	var browserName;
	if (browsers)
	{
		for (var i = 0; i < browsers.length; i++)
		{
			try {
				var b = browsers[i];
				var bName = b.getChild('Name').leafValue;
				var bRegExp = b.getChild('RegExp').leafValue;
				var re = new RegExp(bRegExp);
				if (userAgent.match(re))
					browserName = bName;
			} catch (e) {
				// TODO: handle exception
			}
		}
	}
	if (!browserName)
		if(userAgent.match(/edg/i))
			browserName = 'Edge';
		else
		    if(userAgent.match(/chrome|chromium|crios/i))
					browserName = 'Chrome';
		    else
		    	if(userAgent.match(/firefox|fxios/i))
		    		browserName = 'Firefox';
		    	else
		    		if(userAgent.match(/safari/i))
		    			browserName = 'Safari';
		    		else
		    			if(userAgent.match(/opr\//i))
		    				browserName = 'Opera';
		    			else
		    				if(userAgent.match(/edg/i))
		    					browserName = 'Edge';
		    				else
								if(userAgent.match(/edg/i))
									browserName = 'Edge';
			    				else
									if(userAgent.match(/msie|trident/i))
										browserName = 'Internet Explorer';
						

    return browserName;
};

tersus.DetectBrowser.prototype.resume = function()
{
	if (this.errorMessage)
		throw this.errorMessage;
};

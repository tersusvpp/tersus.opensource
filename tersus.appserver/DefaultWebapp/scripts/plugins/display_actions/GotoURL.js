/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
tersus.GotoURL = ServiceNode.__subclass('tersus.GotoURL');
tersus.GotoURL.prototype.excludedTriggers=new tersus.Set(['<URL>','<Method>', '<Replace>','<Reload>', '<Target Window>']);
tersus.GotoURL.prototype.isAction=true;
tersus.GotoURL.prototype.start = function ()
{
	var urlTrigger = this.getLeafChild('<URL>');
	if (urlTrigger == null)
		var url = tersus.rootURL;
	else {
		var fullUrlObject = new URL(urlTrigger,tersus.rootURL);
		var url = fullUrlObject.href;
	}
	var replace = this.getLeafChild('<Replace>');
	var reload = this.getLeafChild('<Reload>');
	if (reload != false)
		reload = true;
	var targetWindow = this.getLeafChild('<Target Window>');
	if (targetWindow != null)
		if (this.pluginVersion && this.pluginVersion >= 1)
			targetWindow=tersus.sqlize(targetWindow);
	var method = this.getLeafChild('<Method>');

	if (method && method.search('^post$','i') >=0)
	{
		var form = this.prepareForm();
		if (targetWindow)
			form.target  = targetWindow;
		if (url.indexOf('?')>=0)
		{
			modelExecutionError('POST parameters are not part of the URL and should be provided using separate triggers');
			return;
		}
		form.action = url;
		form.method='POST';
		form.submit();
		for (var i=0; i< this.nodesToRemove.length; i++)
		{
			var node = this.nodesToRemove[i];
			node.parentNode.removeChild(node);
		}
	}
	else
	{
		var serializer;
		if (this.triggers)
		{
			var n = this.triggers.length;
			for (var i=0;i<n;i++)
			{
				var t = this.triggers[i];
				if (this.excludedTriggers.contains(t.role) || t.modelId == BuiltinModels.NOTHING_ID)
					continue;
				var value = t.getChild(this);
				if (value == null)
					continue;
				if (url.indexOf('#') < 0 && url.indexOf('?')<0)
					url+='?';
				else
					url += '&';
				url += encodeURIComponent(t.role)+'='+encodeURIComponent(value.leafValue);
			}
		}
		if (targetWindow)
		{
			var w = window.open(url,targetWindow);
			if (w && w.focus)
				tersus.async.exec(function() {w.focus();}, false /* don't wait */); // Needed in Firefox
			else
				tersus.error('Your browser blocks pop-up windows.\nTo use this application, please configure the browser to allow this site to open pop-up windows.');
		}
		else
			tersus.async.exec(function () {tersus.gotoURL(url, replace, reload);}, false /* don't wait */);
	}
};

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 //Alert - displays something

Alert = function Alert() {};
Alert.prototype = new ActionNode();
Alert.prototype.constructor = Alert;

Alert.prototype.start = function startAlert()
{
	if (this.children && this.children['<Message>'] && tersus.alertMode.interactive)
	{
		var message =  tersus.unescapeString(''+this.getLeafChild('<Message>'));
		if (navigator.notification)
		{
			//Phonegap implementation
			this.pause();
			var self = this;
			var callback = function()
			{
				self.resume = function(){ this.finished();};
				self.continueExecution();
			};
			navigator.notification.alert(this.translate(message), callback, this.translate('Alert'), this.translate('OK'));
		}
		else
		{
			//Native browser 'alert'
			 if (this.currentWindow.alert)
				this.currentWindow.alert(this.translate(message));
		}
	}
};

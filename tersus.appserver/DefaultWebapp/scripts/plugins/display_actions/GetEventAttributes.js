/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.GetEventAttributes = function tersus_GetEventAttributes() {};
tersus.GetEventAttributes.prototype = new ActionNode();
tersus.GetEventAttributes.prototype.constructor = tersus.GetEventAttributes;
tersus.GetEventAttributes.prototype.start = function()
{
	var atts = tersus.currentEventAttributes;
	if (atts)
	{
		this.chargeLeafExit('<Event Type>', atts.type);
		this.chargeLeafExit('<Key Code>', atts.keyCode);
		this.chargeLeafExit('<Mouse Button>',atts.button);
		this.chargeLeafExit('<Mouse X>',atts.clientX);
		this.chargeLeafExit('<Mouse Y>',atts.clientY);
		this.chargeLeafExit('<Control Key>', atts.ctrlKey==true);
        this.chargeLeafExit('<Shift Key>', atts.shiftKey==true);
        this.chargeLeafExit('<Alt Key>', atts.altKey==true);
        this.chargeLeafExit('<Meta Key>', atts.metaKey==true);
		var e = this.getExit('<Target Element>');
		if (e)
			this.chargeExit(e, DisplayNode.findNodeForDocumentNode(atts.targetElement));
	    var e = this.getExit('<Files>');
	    if (e && atts.files)
	    {
            for (var i=0;i<atts.files.length;i++)
            {
                var file = e.createChildInstance();
                file.file = atts.files[i]
                file.setLeafChild('Content Type', file.file.type, Operation.REPLACE);
                this.accumulateExitValue(e, file);
            }
	    }
	}
};

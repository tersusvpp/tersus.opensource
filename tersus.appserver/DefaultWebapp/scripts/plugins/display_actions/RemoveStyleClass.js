/*********************************************************************************************************
 * Copyright (c) 2003-2013 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/

tersus.newAction('RemoveStyleClass',function()
{	
	var element = this.getChild('<Element>');
	var value = this.getLeafChild('<Style Class>');
	if (!element || !value || !element.isDisplayNode)
		return;
	element.removeStyleClass(value);
});

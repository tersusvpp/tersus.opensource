/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function Refresh() {};
Refresh.prototype = new ActionNode();
Refresh.prototype.constructor = Refresh;
Refresh.prototype.start = function ()
{
	var element = this.getChild('<Element>');
	if (element && element.refresh)
	{
		element.refresh();
		rootDisplayNode.onResize(); // Added for mobile widgets. Can probably be removed once we get rid of jQuery Mobile scripting
	}
		
};

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Click = function Click() {};
tersus.Click.prototype = new ActionNode();
tersus.Click.prototype.constructor = tersus.Click;
tersus.Click.prototype.start = function ()
{
	var element = this.getChild('<Element>');
	var queue = this.getLeafChild('<Queue>');
	var action = this;
	var targetNode;
	if (element && element.getClickTarget)
		targetNode = element.getClickTarget();
	else if (element & element.eventTargets && element.eventTargers.onClick)
		targetNode= element.eventTargets.onClick;
	else if (element && element.viewNode)
	{
		targetNode = element.viewNode;
	}
	else 
	{
		targetNode = this.getLeafChild('<View Node>');
	}
	if (targetNode)
	{
		this.pause();
		var resume = function()
		{
		  	action.continueExecution();
		}
		var click = function() {
			if (is_touch_device())
				$(targetNode).trigger('vclick');
			else
				$(targetNode).click();
		 	tersus.async.exec(resume, false /* don't wait */);
		};
		tersus.async.exec(click, false /* don't wait  */); // not sure this behavior is correct
		/*
		 * Proposed improvement:
		 *  - If 'Click' runs in main thread it exits immediately and target is clicked later.
		    - If 'Click' runs in background, it pauses, and target is clicked when root contet is not busy ('wait=true') 
		  */
	}
}
tersus.Click.prototype.resume = function()
{
};

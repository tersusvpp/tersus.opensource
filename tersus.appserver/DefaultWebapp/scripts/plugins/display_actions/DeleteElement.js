/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.DeleteElement = function DeleteElement(){};
tersus.DeleteElement.prototype = new ActionNode();
tersus.DeleteElement.prototype.constructor = tersus.DeleteElement;
tersus.DeleteElement.prototype.start = function()
{
	var e = this.getChild('<Element>');
	if (e && e.deleteMe)
		e.deleteMe();
	else
		internalError("Failed to delete child in "+this.getPath());
};

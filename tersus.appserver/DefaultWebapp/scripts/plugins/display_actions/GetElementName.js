/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.GetElementName = function tersus_GetElementName() {};
tersus.GetElementName.prototype = new ActionNode();
tersus.GetElementName.prototype.constructor = tersus.GetElementName;
tersus.GetElementName.prototype.start = function ()
{
	var displayElement = this.getChild('<Element>');
	if (displayElement && displayElement.element)
	{
		var name = displayElement.element.role;
		this.chargeLeafExit('<Name>',name);
	}
};

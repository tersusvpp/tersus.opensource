/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Logout = function() {};
tersus.Logout.prototype = new ActionNode();
tersus.Logout.prototype.constructor = tersus.Logout;
tersus.Logout.prototype.start = function ()
{
	
	tersus.logout();
	tersus.async.exec(tersus.reloadApplication, false /* don't wait */);
};

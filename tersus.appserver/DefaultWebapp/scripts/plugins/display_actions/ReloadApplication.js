/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.ReloadApplication = function() {};
tersus.ReloadApplication.prototype = new ActionNode();
tersus.ReloadApplication.prototype.constructor = tersus.ReloadApplication;
tersus.ReloadApplication.prototype.start = function ()
{
	tersus.async.exec(tersus.reloadApplication, false /* don't wait */);
};

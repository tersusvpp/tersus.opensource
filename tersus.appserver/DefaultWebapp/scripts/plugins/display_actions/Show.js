/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function Show() {};
Show.prototype = new ActionNode();
Show.prototype.constructor = Show;
Show.prototype.start = function ()
{
	var item = this.getChild('<Item>');
	if (item.select)
		item.select();
	var scrollableNode = item.viewNode;
	while (scrollableNode)
	{
		if (scrollableNode == this.mainWindow.rootDisplayNode.viewNode)
		{
			scrollableNode = this.mainWindow.document.body; // workaround for a bug in detecting the scrollable node in IE
			break;
		}
		if (isScrollable(scrollableNode))
			break;
		else
			scrollableNode = scrollableNode.parentNode;
	}
	if (scrollableNode)
	{
		var s = scrollableNode;
		var sSize = getVisibleSize(s);
		var v = item.viewNode;
		/*
			offsetTop = position of top relative to containing
		*/

		var visible = (getOffsets(v).top >= getOffsets(s).top + s.scrollTop)
					&& (getOffsets(v).top+v.offsetHeight <= getOffsets(s).top + s.scrollTop +sSize.height);
		if (! visible)
		{
			var scrollTarget;
			var scrollToTop = v.offsetTop - s.offsetTop;
			var scrollToMiddle = scrollToTop - sSize.height/2;
			var scrollToBottom = scrollToTop  + (sSize.height-v.offsetHeight);
			if (v.offsetHeight >= sSize.height)
				scrollTarget = scrollToTop;
			else if (v.offsetHeight >= sSize.height/2)
				scrollTarget =scrollToBottom;
			else
				scrollTarget = scrollToMiddle;
			var scrollTo = Math.max(0, scrollTarget);
			var scrollTo = Math.min(scrollTo, s.scrollHeight - sSize.height);
			s.scrollTop = scrollTo;
		}
		var u = function ()
		{
			var message = "";
			message += "viewNode.scrollTop="+v.scrollTop+"\n";
			message += "viewNode.scrollHeight="+v.scrollHeight+"\n";
			message += "viewNode.offsetTop="+v.offsetTop+"\n";
			message += "viewNode.offsetHeight="+v.offsetHeight+"\n";
			message += "scrollableNode.scrollTop="+scrollableNode.scrollTop+"\n";
			message += "scrollableNode.scrollHeight="+scrollableNode.scrollHeight+"\n";
			message += "scrollableNode.offsetTop="+scrollableNode.offsetTop+"\n";
			message += "scrollableNode.offsetHeight="+scrollableNode.offsetHeight+"\n";
			tersus.notify('Info',message);
		};
		this.mainWindow.showInfo = u;
		this.mainWindow.scrollableNode = scrollableNode;
	}
};

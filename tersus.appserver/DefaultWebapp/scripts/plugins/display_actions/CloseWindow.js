/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 

//CloseWindow - closes the currentWindow

function CloseWindow(){};
CloseWindow.prototype = new ActionNode();
CloseWindow.prototype.constructor = CloseWindow();
CloseWindow.prototype.start = function()
{
	// Find the containing node that has a closeWindow method
	var node = this.getChild('<Dialog>');
	if (!node)
		node = this;
	while (node && !node.closeWindow) 
		node = node.parent;
	if (node && node.closeWindow)
		node.closeWindow();
};

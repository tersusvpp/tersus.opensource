/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.newAction('SetStyleAttribute', function()
{
	var e = this.getChild('<Element>');
	var n = this.getLeafChild('<Attribute>');
	var a = tersus.getJsAttributeName(n);
	var v = this.getLeafChild('<Value>');
	if (e && e.viewNode && a)
	{
		 e.viewNode.style[a] = v;
	}
});


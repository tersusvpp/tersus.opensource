/*******************************************************************************
 * Copyright (c) 2003-2011 Tersus Software Ltd. , All rights reserved.
 * 
 * This program is made available under the terms of the GNU Lesser General
 * Public License, version 2.1, which is part of this distribution and is
 * available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 ******************************************************************************/

tersus.newAction('Transition2', function() {
    var element = this.getChild('<Element>');
    element = element.currentDialog || element;
    var transitionClass = this.getLeafChild('<Transition>');
    var activeClass = 'active';
    var $element = $(element.wrapperNode || element.viewNode);
    var $element = $element.data('replaced-by')|| $element;
    var $prev = $element.parent().children('.' + activeClass);

    this.pause();
    var self = this;
    tersus.transition2($prev, $element, activeClass, transitionClass+' out', transitionClass+' in', 'end',
            function() {
                self.continueExecution();
            });
    /*
     * onResize must be called after the transition is invoked, because size
     * calculations don't work for hidden elements
     */

    element.onResize();

});
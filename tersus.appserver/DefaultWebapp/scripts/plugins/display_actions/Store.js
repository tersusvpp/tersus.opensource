/*******************************************************************************
 * Copyright (c) 2003-2011 Tersus Software Ltd. , All rights reserved.
 * 
 * This program is made available under the terms of the GNU Lesser General
 * Public License, version 2.1, which is part of this distribution and is
 * available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 ******************************************************************************/


/* Patch to avoid local storage on iOS 5 */
tersus.useFilesystemForStorage = tersus.isNative && window.requestFileSystem &&
    (/(iPhone|iPod|iPad)/i.test(navigator.userAgent)) &&
    (/OS 5_\d(_\d)? like Mac OS X/i.test(navigator.userAgent));

tersus.newAction('Store',function()
{	
	var key = this.getLeafChild('<Key>');
	var value = this.getLeafChild('<Value>');
	var prefix = '';
	if (key)
	{
        if (tersus.useFilesystemForStorage)
        {
            if (value == null)
                this.removeFromFileSystem(key,value);
            else
                this.storeInFileSystem(key, value);
            return;
        }
        var type = this.getLeafChild('<Storage Type>') || 'LOCAL';
		var s;
		if (type == 'LOCAL')
		{
			s = window.localStorage;
			if (s == null)
				throw 'No local storage available';
			key = tersus.appScopePrefix+key;
		}
		else if (type == 'SESSION')
		{
			s = window.sessionStorage;
			if (s == null)
				throw 'No session storage available';
		}
		if (value == null)
			s.removeItem(key)
		else
			s.setItem(key,value);
	}
});
$.extend(tersus.Store.prototype, {
        getErrorMessage: function(code)
        {
            switch(code)
            {
                case FileError.NOT_FOUND_ERR: return 'Not found'; 
                case FileError.SECURITY_ERR: return 'Security error';
                case FileError.ABORT_ERR: return 'Aborted';
                case FileError.NOT_READABLE_ERR: return 'Not redable';
                case FileError.ENCODING_ERR: return 'Encoding error';
                case FileError.NO_MODIFICATION_ALLOWED_ERR: return 'No modification allowed';
                case FileError.INVALID_STATE_ERR: return'Invalid state';
                case FileError.SYNTAX_ERR: return 'Syntax error';
                case FileError.INVALID_MODIFICATION_ERR: return 'Invalid modification';
                case FileError.QUOTA_EXCEEDED_ERR: return 'Quota exceeded';
                case FileError.TYPE_MISMATCH_ERR: return 'Type mismatch';
                case FileError.PATH_EXISTS_ERR: return 'Path exists';
                default: return 'Unknown error';
            }   
        }
        ,storeInFileSystem:function(key, value)
        {
            var self = this;
            var fail = function(error)
            {
              self.resume = function()
              { 
                  throw 'Error writing file:'+self.getErrorMessage(error.code)+' (code='+error.code+')';
              };
              self.continueExecution();
           
            };
            var fsSuccess = function(fileSystem)
            {
                var fileEntrySuccess = function(fileEntry)
                {
                    var writerSuccess = function (writer)
                    {
                        
                        writer.onwrite = function(evt)
                        {
                            self.resume = function()
                            {
                                self.finished();
                            };          
                            self.continueExecution();
                         };
                         writer.onerror = function()
                         {
                             fail(writer.error);
                         }
                         writer.write(value);
                    };
                    
                    fileEntry.createWriter(writerSuccess, fail);
                };
                fileSystem.root.getFile(key, {create:true}, fileEntrySuccess, fail);
            };
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, fsSuccess, function(evt)
                    {
                        fail(evt.target.error);
                    });
            self.pause();
        }
        ,removeFromFileSystem:function(key)
        {
            var self = this;
            var fail = function(error)
            {
              self.resume = function()
              { 
                  throw 'Error removing file:'+self.getErrorMessage(error.code)+' (code='+error.code+')';
              };
              self.continueExecution();
           
            };
            var done = function()
            {
                self.resume = function()
                {
                    self.finished();
                }
                self.continueExecution();
            };
            var fsSuccess = function(fileSystem)
            {
                var fileEntrySuccess = function(fileEntry)
                {
                    var removeSuccess = function ()
                    {
                        done();
                    };
                    fileEntry.remove(removeSuccess, fail);
                };
                var fileEntryError = function(error)
                {
                    if (error.code == FileError.NOT_FOUND_ERR)
                       done();
                    else
                        fail(error);
                }
                fileSystem.root.getFile(key, {create:false}, fileEntrySuccess, fileEntryError);
            };
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, fsSuccess, function(evt)
                    {
                        fail(evt.target.error);
                    });
            self.pause();
        }

        
});

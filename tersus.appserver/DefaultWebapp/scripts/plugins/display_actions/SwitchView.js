/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function SwitchView() {};
SwitchView.prototype = new FlowNode();
SwitchView.prototype.constructor = SwitchView();

SwitchView.prototype.start = function()
{
	var view = this.getLeafChild('<View>');
	var perspective = this.getLeafChild('<Perspective>');
	var parameters = ''
	for (var i=0; i<this.triggers.length; i++)
	{
		var trigger = this.triggers[i];
		if (trigger.role.charAt(0) != '<') // ignore 'special' triggers
		{
			var value = this.getLeafChild(trigger.role);
			if (value)
			{
				if (parameters)
					parameters += '&';
				parameters+=encodeURIComponent(trigger.role)+'='+encodeURIComponent(value);
			}
		}
	}
	
	tersus.switchView(perspective,view,parameters);
};

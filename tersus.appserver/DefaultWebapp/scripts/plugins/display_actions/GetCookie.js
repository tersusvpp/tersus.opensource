/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function GetCookie() {};
GetCookie.prototype = new ActionNode();
GetCookie.prototype.constructor = GetCookie;
GetCookie.prototype.start = function ()
{
	var name = this.getLeafChild('<Name>');
	if (name)
	{
		name=tersus.sqlize(name);
		var value;
		if (tersus.isNative && window.localStorage)
			value = localStorage.getItem('Cookie.'+name,value);
		else
			value = getCookie(name);
			
		if (value != null && value.length>0)
			this.chargeLeafExit('<Value>',value);
	}
};

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 
//Confirm - displays a confirmation message and retrieves user input
function Confirm() {};
Confirm.prototype = new ActionNode();
Confirm.prototype.constructor = Confirm;

Confirm.prototype.start = function startConfirm()
{
	var message =  tersus.unescapeString('' +this.getLeafChild('<Message>'));
	var response;
	if (tersus.alertMode.interactive)
		response = this.currentWindow.confirm(this.translate(message));
	else
		response = tersus.alertMode.confirm;
	if (response)
		this.chargeLeafExit('<OK>', message);
	else
		this.chargeLeafExit('<Cancel>', message);
};
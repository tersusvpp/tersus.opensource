tersus.newAction('PlaySound',function ()
{
	var url=this.getLeafChild('<URL>');
	if (url==null)
		return;
	var e = window.soundElement;
	if (e)
	{
		document.body.removeChild(e);
	}
	var e = document.createElement('embed');
	window.soundElement=e;
	e.setAttribute("src",url);
	e.setAttribute("hidden",true);
	e.setAttribute("autostart",true);
	document.body.appendChild(e);
});

/*********************************************************************************************************
 * Copyright (c) 2003-2011 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/

tersus.newAction('Transition',function()
{	
	var element = this.getChild('<Element>');
	element = element.currentDialog || element;
	var animationClass = this.getLeafChild('<Animation Class>');
	var activeClass='ter-active';
	var $element =  $(element.wrapperNode || element.viewNode);
	var $prev = $element.parent().children('.'+activeClass);
	
	this.pause();
	var self=this;
	tersus.transition($prev, $element, activeClass, animationClass, function(){
		self.continueExecution();
	});
	/*
		onResize must be called after the transition is invoked, because size calculations don't work for hidden elements
	*/
	
	element.onResize(); 
	
});

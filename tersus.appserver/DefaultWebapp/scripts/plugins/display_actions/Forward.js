/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Forward = function tersus_GetElmentName() {};
tersus.Forward.prototype = new ActionNode();
tersus.Forward.prototype.constructor = tersus.Forward;
tersus.Forward.prototype.start = function ()
{
	setTimeout(function () {window.history.forward();},0);
};

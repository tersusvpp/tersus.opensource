/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.ShowFile = function ShowFile() {};
tersus.ShowFile.useIFrame=true;
tersus.ShowFile.prototype = new ServiceNode();
tersus.ShowFile.prototype.constructor = tersus.ShowFile;
tersus.ShowFile.prototype.start = function startService()
{	
	if (tersus.alertMode.interactive)
	this.callService();
};
tersus.ShowFile.prototype.getTraceFileName = function()
{
	return this.traceFileName;
};
tersus.ShowFile.iframe = null;
tersus.ShowFile.prototype.callService = function callService()
{
	if (window.trace)
	{
		this.traceFileName = window.trace.openTraceFile();
		window.trace.link(this,this.traceFileName, "contains the trace of the server side process");
	}
	var form = this.prepareForm();
	this.addTersusParameters(form);

	form.action = 'File';
	form.method = 'POST';
        var newFrame=false;
	if (tersus.ShowFile.useIFrame)
	{
		if (tersus.ShowFile.iframe == null)
	        {
		    newFrame=true;
		    tersus.ShowFile.iframe = createAnIFrame(window,'_showFile');
		}
		form.target='_showFile';
	}
	
	setEncType(form,'multipart/form-data'); 
	var sigField = addHiddenField(form, '_sig', 'END');
	form.appendChild(sigField);
	this.nodesToRemove.push(sigField);
        var self=this;
    var submit = function()
    {
	form.submit();
	for (var i=0; i< self.nodesToRemove.length; i++)
	{
		var node = self.nodesToRemove[i];
		node.parentNode.removeChild(node);
	};
    };
    if (newFrame)
	setTimeout(submit,100); /* In chrome 33, a newly created IFRAME is not immediately available as a form target */
    else 
	submit();

};

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.newAction('GetStyleAttribute', function()
{
	var e = this.getChild('<Element>');
	var n = this.getLeafChild('<Attribute>');
	var a = tersus.getJsAttributeName(n);
	if (e && e.viewNode && a)
	var v = e.viewNode.style[a];
	this.chargeLeafExit('<Value>',v);
});


/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function SetCookie() {};
SetCookie.prototype = new ActionNode();
SetCookie.prototype.constructor = SetCookie;
SetCookie.prototype.start = function ()
{
	var name = tersus.sqlize(this.getLeafChild('<Name>'));
	var value = this.getLeafChild('<Value>');
	
	if (tersus.isNative && window.localStorage)
	{
		localStorage.setItem('Cookie.'+name,value);
		return;
	}

	var expiresElement = this.getElement('<Expires>');
	var expires = null;
	var neverExpires = new Date(2080,0,0);
	if (expiresElement)
	{
		expires = expiresElement.getChild(this);
		if (expires == null)
			expires = neverExpires;
		else 
			if (expires.toGMTString)
				expires = expires.toGMTString();
	}
	else
	{
		var pluginVersion = this.pluginVersion ? this.pluginVersion : 0;
		if (pluginVersion < 1)
			expires = neverExpires; 
	}

	var domain = this.getLeafChild('<Domain>');
	var path = this.getLeafChild('<Path>');
	var secure = this.getLeafChild('<Secure>');
	if (value == null)
		deleteCookie(name);
	else
		setCookie(name,value, expires, path, domain, secure);
};

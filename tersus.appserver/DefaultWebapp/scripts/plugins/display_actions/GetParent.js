/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.GetParent = function GetParent(){};
tersus.GetParent.prototype = new ActionNode();
tersus.GetParent.prototype.constructor = tersus.GetParent;
tersus.GetParent.prototype.start = function()
{
	var trigger = this.getTrigger('<Element>');
	var parent = null;
	if (trigger != null) // Get the parent of the specified element
	{
		var element = trigger.getChild(this);
		if (element)
		{
			parent = element.parent;
		}
	}
	else // Get the DisplayNode that contains this action
	{
		parent = this;
		while (parent && !parent.isDisplayNode)
			parent = parent.parent;
	}
	if (parent)
		this.chargeExit('<Parent>',parent);	
};

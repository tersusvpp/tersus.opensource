/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.FireEvent = function FireEvent() {};
tersus.FireEvent.prototype = new ActionNode();
tersus.FireEvent.prototype.constructor = tersus.FireEvent;
tersus.FireEvent.prototype.start = function ()
{
	var element = this.getChild('<Element>');
	var eventName = this.getLeafChild('<Event Name>');
	var parameters = {};
	for (var i = 0; i < this.triggers.length; i++)
	{
		var t = this.triggers[i];
		if (t.role.search('<') < 0)
		{
			var v;
			if ( t.isRepetitive)
				v =  t.getChildren(this);
			else
				v = t.getChild(this);
			if (v != null)
				parameters[t.role]=v;
		}
	}
	tersus.invokeAction(eventName, parameters, null, element, true);
};


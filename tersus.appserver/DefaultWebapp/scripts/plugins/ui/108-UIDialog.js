tersus.UIWidget('UIDialog','UIBase', {
	isDialog:true
	,defaultOptions:'cover backdrop'
	,start: function()
	{
		this.setup();
		this.create();
		FlowNode.prototype.start.call(this);
		this.isOpen = true;
		//this.currentWindow.scrollTo(0,1);
        tersus.popupNodes[this.getNodeId()]=this;
		this.open();
	}
    ,getContainer:function()
    {
        var c= this.getChild('<Container>');
        if (c && c.viewNode)
            return c.viewNode;
        var selector = (c && c.isText) ? c.leafChild : this.defaultContainer;
        var $container;
        if (! selector)
            $container=$('#tersus\\.content');
        else
        {
            var n = this.parent;
            while (n && !n.viewNode)
                n = n.parent;
            var error;
            if ( !n || !n.viewNode)
               throw 'Dialog container not found (n.viewNode is null)';
                
            $container = $(n.viewNode).closest(selector);
        }
        if ($container.length>0)
            return $container[0];
        
        throw 'Dialog container not found (selector: "'+selector+'")';
    }
	,setup:function()
	{
		if (this.element && ! this.element.prepareForHTML)
			this.element.prepareForHTML = DisplayElement.prototype.prepareForHTML; //TODO checkif needed
		clearSelection(window);
	
		this.container = this.getContainer();
		var str = (this.getLeafChild('<Options>') || this.defaultOptions).trim(); 
		var self=this;
		this.options={};
		$.each(str.split(/\s+/), function(i, v) { self.options[v]=true;}); // Set 'true' for all options in the string
		this.transitionClass = this.getLeafChild('<Transition>') || this.transitionClass;
		if (this.options.replace)
		{
		   this.open = this.replace;
		   this.close = this.replaceBack;
		}
	}

	,open:function()
	{
		var self=this;
		var $dlg = $(this.wrapperNode);
		if ($.support.transition)
		{
		    $dlg.addClass(this.transitionClass+' dlg open');
		    $dlg[0].offsetWidth; //reflow
		    $dlg.one($.support.transition.end, function() { $dlg.removeClass(this.transitionClass+' dlg open end');self.onResize();});
		    $dlg.addClass('end');
		}
	}
	,close:function()
	{
	    var self=this;
        var $dlg = $(this.wrapperNode);
        if ($.support.transition)
        {
            $dlg.addClass(this.transitionClass+' dlg close');
            $dlg[0].offsetWidth; //reflow
            $dlg.one($.support.transition.end, function() { $dlg.remove()});
            $dlg.addClass('end');
        }
        else
            $dlg.remove();
        this.removeBackdrop();
	}
    ,addBackdrop:function()
    {
        if (this.options.replace || !this.options.backdrop)
            return;
        var animate=$(this.wrapperNode).hasClass('fade') ? ' fade' : '';
        var $backdrop = this.$backdrop = $('<div class="modal-backdrop'+animate+'">');
        $(this.container).append($backdrop);
        if (animate)
        {
            $backdrop[0].offsetWidth;
            $backdrop.addClass("in");
        }
        if (this.options['backdrop-dismiss'])
        {
            var e= $.event.special.vclick ? 'vclick' : 'click';
            var self=this;
            $backdrop.on(e, function() {self.closeWindow();});
        }
    }
	,removeBackdrop:function()
	{
	    var $backdrop = this.$backdrop;
	    if (!$backdrop)
	        return;
        if ($.support.transition && $backdrop.hasClass('in'))
        {   
           $backdrop.on($.support.transition.end, function() {$backdrop.remove()});
           $backdrop.removeClass('in');
        }
        else
            $backdrop.remove();
	}
	,replace:function()
	{
		this.$rpl = $(this.container).children('.active').first(); /* The element that the dialog is replacing */
        var $dlg = $(this.wrapperNode);
		this.$rpl.data('replaced-by',$dlg);
	    var self=this;
	    self.pause();
		tersus.transition2(this.$rpl, $dlg, 'active', this.transitionClass +' rpl open', this.transitionClass+ ' dlg open','end', function(){self.continueExecution();});
	}
	,replaceBack:function()
	{
		var $dlg = $(this.wrapperNode);
		this.$rpl.data('replaced-by',null);
		tersus.transition2($dlg, this.$rpl, 'active', this.transitionClass+ ' dlg close', this.transitionClass +' rpl close', 'end', function(){$dlg.remove();});
	}
	,attachViewNode:function()
	{
	    this.addBackdrop();
	    $(this.container).append($(this.wrapperNode));

	}

	,destroy: function()
	{
		if (! this.isDestroyed)
		{
			this.isDestroyed = true;
			this.supr('destroy');
			if (this.viewNode)
			    this.doClose();
		}
	}
	,closeWindow: function() 
	{
		this.destroyHierarchy();
	}
	
	,doClose: function()
	{
		this.close();
        delete tersus.popupNodes[this.getNodeId()];
		this.isOpen = false;
	}
        ,refresh: function()
        {
	    var wasActive =$(this.wrapperNode).hasClass('active');
            tersus.UIBase.prototype.refresh.call(this);
	    if (wasActive)
		$(this.wrapperNode).addClass('active');
	}

});

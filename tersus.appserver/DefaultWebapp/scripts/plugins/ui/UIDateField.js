(function( $, undefined ) {
    tersus.UIWidget('UIDateField','UIField',{
    defaultTag:'input'
	,inputType:'date'
	,convertFormat:function(format)
	{
            //Year: yyyy->yy, yy->y
            format = format.replace(/yyyy/g,"YY").replace(/yy/g,'Y').replace(/Y/g,'y');
            //Month: MMMM -> MM (Full Name), MMM -> M (short Name), MM->mm (padded number), M->m number
            format=format.replace(/MMMM/g,"&&&&").replace(/MMM/g,"&&&").replace(/M/g,"m").replace(/[&]/g,"M");
            //Day of month: EEEE->DD, EEE/EE/E->D
            format=format.replace(/EEEE/g,"DD").replace(/E+/g,"D");
            return format;
	}

/*	Use native calendar

	,attachWidget:function()
	{
            //TODO clean this up
            var $v = $(this.viewNode);
            var self=this;
	    if (this.inputField)
	    {
		if (window.SpinningWheel)
		{
		    $v.on('focus', function(event)
			  {
			      if (self.computedReadOnly())
				  return;
                              var date = self.getFieldValue() && self.getFieldValue().leafValue;
                              var callback = function(year, month, day)
                              {
				  var date = createDate(year, month, day);                            
				  self.setLeafChild('<Value>',date, Operation.REPLACE);
                              };
                              this.blur();
                              PopupCalendar.prototype.openDatePicker(date, callback);
			  });
		}
		else // use jQuery UI Datepicker
		{
		    $v.datepicker({
			dateFormat:this.convertFormat(DATE_FORMAT),
			onSelect:function(text, inst) 
			{
			    if (self.inputField) 
			    {
				var $input = $(self.inputField);
				$input.trigger('change');
				$input.focus();

				  // focus on next focusable element
//				var $a = $(':focusable');
//				setTimeout(function(){$a.eq($a.index($input)+1).focus()},50);

				
			    }
			}
		    });
		    if (this.computedReadOnly())
			$v.datepicker('disable');
		}
	    }
	}
	,detachWidget:function()
	{
	    
	    if (this.inputField && window.SpinningWheel)
		$(this.viewNode).datepicker('destroy');
	    
	}
	,readOnlyChanged:function(ro)
	{
	    if (!window.SpinningWheel)
	    {
		if (this.computedReadOnly())
		    $(this.viewNode).datepicker('disable');
		else
		    $(this.viewNode).datepicker('enable');
	    }

	}
*/
    });})(jQuery);

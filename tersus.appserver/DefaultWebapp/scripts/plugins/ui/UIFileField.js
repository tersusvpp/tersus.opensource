(function( $, undefined ) {
    function getFile()
    {
        if (!this.fileValue && this.inputField)
        {
            //Create a File instance based on the input field's selection
            var element = this.getElement('<Value>');
            if (element)
            {
                    this.fileValue = element.createChildInstance();
                    this.fileValue.inputFieldDisplayNode = this; //Legacy API needed for IE8.0, IE9.0
                    var files = this.inputField.files;
                    if (files && files.length == 1)
                        this.fileValue.file = files[0];
            }
        }
        return this.fileValue;
    };
tersus.UIWidget('UIFileField','UIField',{
    inputType:'file'
    ,fileValue:null
    ,'get<Value>':getFile
    ,getFieldValue:getFile
    ,_onChange:function(){this.fileValue=null;}
});
})(jQuery);
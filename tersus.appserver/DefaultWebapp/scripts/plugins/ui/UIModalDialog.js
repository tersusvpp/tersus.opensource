tersus.UIWidget('UIModalDialog','UIDialog', {
    defaultOptions:'cover backdrop backdrop-dismiss'
    ,open:function()
    {
        var $dlg=$(this.wrapperNode);
        $dlg[0].offsetWidth;
        $dlg.addClass('in');
    }
    ,close:function()
    {
        var $dlg=$(this.wrapperNode);
        if ($.support.transition)
        {
            $dlg.one($.support.transition.end, function(){$dlg.remove();});
            $dlg.removeClass('in');
        }
        else
            $dlg.remove();
        this.removeBackdrop();
    }
    ,refresh:function()
    {
	var wasActive = $(this.wrapperNode).hasClass('in');
	tersus.UIDialog.prototype.refresh.call(this);
	if (wasActive)
	    $(this.wrapperNode).addClass('in');

    }
});

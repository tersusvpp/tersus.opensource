
(function( $, undefined ) {
var setter = tersus.setter = function(f, requiresNodes)
{
	if (requiresNodes)
		f.requiresNodes = true;
	return f;
	
};

$.fn.tersus = function(options)
{
	return this.map(function(ix, e) {return DisplayNode.findNodeForDocumentNode(e);});
};

tersus.UIWidget = function(name, base, options)
{
	if (tersus[name])
		throw 'Duplicate definition of \'tersus.\''+name;
		
	var c = tersus[base].__subclass('tersus.'+name);
	tersus[name]=c;
	var p = c.prototype;
	$.extend(p,{
        postAttach:function()
        {
            tersus.HTMLTag.prototype.postAttach.call(this);
            if (this.attach)
                this.attach();
        }});

	$.extend(p,options);
};

tersus.UIWidget('UIBase','HTMLTag', {
    defaultTag:'div'
});

})( jQuery);

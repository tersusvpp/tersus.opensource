(function( $, undefined ) {
    tersus.UIWidget('UIHTMLDisplay','UIBase', {
        defaultTag:'div'
	,setValueString:function()
	{
        this.valueStr = '';
        if (this.value != null)
        {
             this.valueStr = this.value.toString();
        }
        if (this.viewNode)
        {
            if (this.textContainer)
                this.textContainer.innerHTML = this.valueStr;
        }
	}
	,addHTMLTagContent: function(htmlV, displayChildren, prep) 
	{
            htmlV.push(this.valueStr);
	}
});
})(jQuery);

(function($, undefined) {
    tersus.UIWidget('UIRadioButtonGroup', 'UIBase', {
        defaultTag : 'div',
        isRadioButtonGroup : true,
        groupName : null,
        selectedButton : null,
        getGroupName : function() {
            if (!this.groupName)
                this.groupName = '' + this.getNodeId();
            return this.groupName;
        },
        
        setSelectedValue: function(value)
        {
                var found = false;
                var self = this;
                var op = function(node)
                {
                        if (node.isRadioButton &&  compareValues(node.getValue(), value)  )
                        {       
                                node.setChecked(true);
                                self.selectedButton=node;
                                found = true;
                                return true; // stop traversal
                        }
                };
                tersus.traverseHierarchy(this, DisplayNode.displayChildrenIterator, op);
        },

        'get<Value>':function()
        {
                if (! this.selectedButton)
                        return null;
                else
                        return this.selectedButton.getValue();
        },
        
        'set<Value>' : setter(function(value){
            this.setSelectedValue(value);
        },true),
        
        'remove<Value>': function()
        {
            if (this.selectedButton)
            {
                this.selectedButton.setChecked(false);
                this.selectedButton = null;
            }
        }

    });
})(jQuery);
tersus.HTMLEditor2 = {
	keyMap:{
		8:'BS'
		,9:'TAB'
		,27:'ESC'
		,32:'SPC'
		,38:'UP'
		,37:'LEFT'
		,39:'RIGHT'
		,40:'DOWN'
		,46:'DEL'
	},
	convertKeyCode:function(code)
	{
		if (code>=48 && code<=90)
			return String.fromCharCode(code);
		return tersus.HTMLEditor2.keyMap[code];
	},
	
	saveSelection: function() {
	    if (window.getSelection) {
	        sel = window.getSelection();
	        if (sel.getRangeAt && sel.rangeCount) {
	            var ranges = [];
	            for (var i = 0, len = sel.rangeCount; i < len; ++i) {
	                ranges.push(sel.getRangeAt(i));
	            }
	            tersus.HTMLEditor2.savedSelection=ranges;
	        }
	    } else if (document.selection && document.selection.createRange) {
	    	tersus.HTMLEditor2.savedSelection=document.selection.createRange();
	    }
	},

	restoreSelection:function () {
	    if (tersus.HTMLEditor2.savedSelection) {
	        if (window.getSelection) {
	            sel = window.getSelection();
	            sel.removeAllRanges();
	            for (var i = 0, len = tersus.HTMLEditor2.savedSelection.length; i < len; ++i) {
	                sel.addRange(tersus.HTMLEditor2.savedSelection[i]);
	            }
	        } else if (document.selection && tersus.HTMLEditor2.savedSelection.select) {
	        	tersus.HTMLEditor2.savedSelection.select();
	        }
	       // tersus.HTMLEditor2.savedSelection = null;
	    }
	},

	getSelectionElements:function()
	{
	    var elements = [];
	    var range, containerEl, links, linkRange;
	    if (window.getSelection) {
	        sel = window.getSelection();
	        if (sel.getRangeAt && sel.rangeCount) {
	            linkRange = document.createRange();
	            for (var r = 0; r < sel.rangeCount; ++r) {
	                range = sel.getRangeAt(r);
	                containerEl = range.commonAncestorContainer;
	                if (containerEl.nodeType != 1) {
	                    containerEl = containerEl.parentNode;
	                }
	                elements.push(containerEl);
	            }
	            linkRange.detach();
	        }
	    } else if (document.selection && document.selection.type != "Control") {
	        range = document.selection.createRange();
	        containerEl = range.parentElement();
	        elements.push(containerEl);
	    }
	    return elements;
	},
	setSelectionLinkTarget:function(target)
	{
		tersus.HTMLEditor2.getSelectionElement('a').attr('target', target);
	},

	getSelectionElement:function(tagName, w)
	{
		w = w || window;
		tagName=tagName.toUpperCase();
		var n;
		if (w.getSelection)
		{
			var sel = window.getSelection();
			if (sel.rangeCount==1)
			{
				var r = sel.getRangeAt(0);
				if(r.startContainer !== r.endContainer || r.startOffset != r.endOffset)
				{
					var n = r.endContainer;
					if (n.hasChildNodes() && r.endOffset >0)
						n = n.childNodes[r.endOffset-1];
	
					while (n && n.tagName != tagName)
					{
						n = n.parentNode;
					}
				}
			}
		}
		return $(n);
	},


	getSelectionStartNode:function(w)
	{
		w = w || window;
		if (w.getSelection)
		{
			var sel= window.getSelection();
			if (sel.rangeCount>0)
			{
				var range = sel.getRangeAt(0);
				var node = range.startContainer;
				if (node.hasChildNodes() && range.startOffset >0)
					node = node.childNodes[range.startOffset-1];
				return node;
			}
		}
	},
	
	setImageAttributes:function(alt, width, height)
	{
		var node = tersus.HTMLEditor2.getSelectionStartNode();
		if (node && node.tagName=='IMG')
		{
			width = width>=0 ? width: '';
			height= height>=0 ? height : '';
			$(node).attr('alt',alt).width(width).height(height);
		}
	},

	getImageAttributes:function(_node)
	{
		var $img = tersus.HTMLEditor2.getSelectionElement('img');
		_node.setLeafChild('alt', $img.attr('alt'), Operation.REPLACE);
		_node.setLeafChild('src', $img.attr('src'), Operation.REPLACE);
		_node.setLeafChild('width', $img.width(), Operation.REPLACE);
		_node.setLeafChild('height', $img.height(), Operation.REPLACE);
	},
	commands: {
		bold:{style:'bold', description:'Bold', keyBinding:'Ctrl-B'},
		italic:{style:'italic', description:'Italic', keyBinding:'Ctrl-I'},
		underline:{style:'underline', description:'Underline', keyBinding:'Ctrl-U'},
		justifyLeft:{style:'align-left', description:'Align Left', keyBinding:'Ctrl-Shift-L'},
		justifyCenter:{style:'align-center', description:'Align Center', keyBinding:'Ctrl-Shift-E'},
		justifyRight:{style:'align-right', description:'Align RIght', keyBinding:'Ctrl-Shift-R'},
		insertOrderedList:{style:'ordered-list', description:'Bulleted List', keyBinding:'Ctrl-Shift-8'},
		insertUnorderedList:{style:'unordered-list', description:'Numbered List', keyBinding:'Ctrl-Shift-7'},
		outdent:{style:'outdent', description:'Indent Less'},
		indent:{style:'indent', description:'Indent More'},
		removeFormat:{style:'clean', description:'Remove Formatting'}
	},
	defaultCommands:['bold','italic','underline','justifyLeft','justifyCenter','justifyRight','insertOrderedList','insertUnorderedList','outdent','indent','removeFormat']
};

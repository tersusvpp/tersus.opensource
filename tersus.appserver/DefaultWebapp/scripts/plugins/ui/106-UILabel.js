(function( $, undefined ) {
    tersus.UIWidget('UILabel','UIBase', {
        defaultTag:'span'
        ,addHTMLTagContent:function(htmlV, displayChildren, prep)
        {
                if (this.getElement('<Caption>'))
                    htmlV.push(this.getCaption());
        }
        ,captionChanged:function()
        {
            if (this.getElement('<Caption>'))
                $(this.viewNode).text(this.getCaption());
        }
    
    });
})(jQuery);
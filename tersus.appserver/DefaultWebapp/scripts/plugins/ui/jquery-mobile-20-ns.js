//>>excludeStart("jqmBuildExclude", pragmas.jqmBuildExclude);
//>>description: The mobile namespace on the jQuery object
//>>label: Namespace
//>>group: Core
define([ "jquery" ], function( jQuery ) {
(function( $ ) {
	$.mobile = {};
}( jQuery ));
//>>excludeStart("jqmBuildExclude", pragmas.jqmBuildExclude);
});
//>>excludeEnd("jqmBuildExclude");
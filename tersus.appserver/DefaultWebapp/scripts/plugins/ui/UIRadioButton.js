(function($, undefined) {
    tersus.UIWidget('UIRadioButton', 'UIBase', {
        defaultTag : 'input',
        inputType : 'radio',
        isRadioButton : true,
        value : null,
        group : null,
        setGroup : function() {
            var n = this.parent;
            while (n && !n.isRadioButtonGroup)
                n = n.parent;
            this.group = n;
        },
        setChecked : function(bool) {
            $(this.viewNode).attr('checked', bool);
        },
        attach : function() {
            this.setGroup();
            if (this.group)
                $(this.viewNode).attr('name', this.group.getGroupName());
            var self = this;
            $(this.viewNode).on('change', function() {
                if (self.group && $(self.viewNode).attr('checked'))
                    self.group.selectedButton = self;
            });
        },
        'set<Value>' : setter(function(value) {
            this.value = value;
        }, true),
        getValue : function() {
            return this.value;
        },
        'get<Value>' : function() {
            return this.getValue();
        }

    });
})(jQuery);

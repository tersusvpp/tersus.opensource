(function( $, undefined ) {
tersus.UIWidget('UIField','UIBase',{
   defaultTag:'input'
    ,readOnlyChanged:function(){}
    ,inputType:'text'
    ,attach: function()
    {
 		this.setValueString();
		this.updateSelfReadOnly(this.computedReadOnly());
    }
    ,updateSelfReadOnly: function(ro) {
       this.currentReadOnly=ro;
        if (!this.viewNode)
            return;
        var $v = $(this.viewNode);
        var roTag=this.prop('readonlytag') || this.defaultTag;
        var rwTag=this.getTag();
	if (this.detachWidget) this.detachWidget();
        if (roTag == rwTag)
        {
            if (ro)
                $v.attr('readonly',true);
            else
                $v.removeAttr('readonly');
	    this.readOnlyChanged();
        }
        else
        {
            if (ro)
            {
                this.getFieldValue(); // Copies input field value into this.value
                $f = $('<'+roTag+'>');
            }
            else
            {
		var type = this.prop('type') || this.inputType;
                var typeAttr = type ?' type="'+type+'"':'';
                var $f=$('<'+rwTag +typeAttr+'>');
            }
            $f.attr('lid',this.getNodeId());
            $f.attr('display_order',$v.attr('display_order'));
            $v.replaceWith($f);
            this.textContainer=this.viewNode=$f[0];
            this.inputField = ro ? undefined: this.viewNode;
            this.setValueString(); //Sets new element content based on this.value
        }
	if (this.attachWidget) this.attachWidget();
    }});
})(jQuery);

(function($, undefined) {

    var setter = tersus.setter;

    tersus.UIWidget('UISelectionField', 'UIBase', {
        defaultTag : 'select',
        attach : function() {
            this.setSelectedIndex(this.findIndex(this.value));
        },
        'set<Value>' : setter(function(value) {
            this.value = value;
            this.setSelectedIndex(this.findIndex(this.value));
        }, true),
        'remove<Value>' : function() {
            if (this.options && this.options.length > 0)
                this.value = this.options[0];
            this.setSelectedIndex(0);
        },
        getValue : function() {
            var selectedIndex;
            // TODO: check behavior on IE. The scenario is probably getting the value immediately after setting it
            /*
             * Old code: if (this.tmpSelectedIndex != null) // In IE, the selected index is updated asynchronously, so we use tmpSelectedIndex if it is set
             * selectedIndex = this.tmpSelectedIndex; else
             */
            var selectedIndex = this.getSelectedIndex();
            if (selectedIndex < 0)
                return null;
            return this.options[selectedIndex];
        },
        'get<Value>' : function() {
            return this.getValue();
        },
        findIndex : function(value) {
            if (this.options)
                for ( var i = 0; i < this.options.length; i++) {
                    var option = this.options[i];
                    if (compareValues(option, value))
                        return i;
                }
            return -1;
        },
        getSelectedIndex : function() {
            var index = this.viewNode && this.viewNode.selectedIndex;
            return index >= 0 ? index : -1; // Converts null to -1
        },
        setSelectedIndex : function(index) {
            if (this.viewNode)
                this.viewNode.selectedIndex = Math.max(index, 0);
        },
        'get<Placeholder Text>' : function() {
            return this.emptyOptionText;
        },
        'set<Placeholder Text>' : function(text) {
            var isNew = this.emptyOptionText == null;
            this.emptyOptionText = text;
            if (isNew) {
                if (this.options)
                    this.options.splice(0, 0, null); // add null to beginning of list
                else
                    this.options = [ null ];
            }
            if (this.viewNode) {
                if (isNew)
                    $(this.viewNode).prepend($("<option>").text(this.translate(this.getOptionText(null))));
                else
                    $(this.viewNode).find('option').first().text(this.translate(this.getOptionText(null)));
                if (this.value == null)
                    this.setSelectedIndex(0);
            }
        },
        'remove<Placeholder Text>' : function() {
            if (this.emptyOptionText != null) {
                this.emptyOptionText = null;
                this.options.splice(0, 1); // remove first
                if (this.viewNode) {
                    $(this.viewNode).find('option').first().remove();
                }
            }

        },
        'remove<Options>' : function() {
            this.options = this.emptyOptionText == null ? [] : [ null ];
            if (this.viewNode) {
                var self = this;
                $(this.viewNode).find('option').filter(function(i) {
                    return i > 0 || null == self.emptyOptionText;
                }).remove();
            }
        },
        'add<Options>' : setter(function(option) {
            if (!this.options)
                this.options = [];
            this.options.push(option);
            if (this.viewNode) {
                $(this.viewNode).append($("<option>").text(this.translate(this.getOptionText(option))).val(this.options.length));
                if (this.value != null && compareValues(this.value, option) || this.options.length == 1) {
                    this.setSelectedIndex(this.options.length - 1);
                }
            }
        }, true),

        addHTMLTagContent : function(htmlV, displayChildren, prep) // Need to add <option> elements, which are not part of the Tersus display hierarchy
        {
            if (this.options) {
                for ( var i = 0; i < this.options.length; i++) {
                    htmlV.push("<option>");
                    htmlV.push(this.escapeHTML(this.translate(this.getOptionText(this.options[i]))));
                    htmlV.push("</option>");
                }
            }
        },
        'get<Options>' : function() {
            if (!this.options)
                return [];
            if (this.emptyOptionText != null) {
                return this.options.slice(1); // Ignoring the first item in the array, which is the empty option (null)
            }
            return this.options;
        },
        getOptionText : function(value) { /* Logic for selecting the text to display for (potentially) a data structure */
            var outputValue = null;
            if (value && value.elementList) {
                var textElementRole = value.__textElementRole;
                if (textElementRole == null) {
                    for ( var i = 0; i < value.elementList.length; i++) {
                        var element = value.elementList[i];
                        if (i == 0) {
                            textElementRole = element.role; // If no text element is found, the first element is used
                            value.constructor.prototype.__textElement = element;
                        }
                        if (element.modelId == BuiltinModels.TEXT_ID) {
                            textElementRole = element.role;
                            value.constructor.prototype.__textElement = element;
                            break;
                        }
                    }
                    value.constructor.prototype.__textElementRole = textElementRole;
                }
                if (textElementRole && value.children)
                    outputValue = value.children[textElementRole];
            } else
                outputValue = value;
            var text = '';
            if (outputValue != null)
                text = outputValue.formatString(this.sharedProperties);
            else if (this.emptyOptionText != null)
                text = this.emptyOptionText;
            return text;
        }
	,updateSelfReadOnly: function(ro) {
       this.currentReadOnly=ro;
        if (!this.viewNode)
            return;
        var $v = $(this.viewNode);
        var roTag=this.prop('readonlytag') || this.defaultTag;
        var rwTag=this.getTag();
        if (roTag == rwTag)
        {
            if (ro)
                $v.prop('disabled',true);
            else
                $v.prop('disabled',false);
        }
        else
        {
            if (ro)
            {
                this.value = this.getValue();
                $f = $('<'+roTag+'>');
		this.textContainer=this.viewNode=$f[0];
		this.setValueString(); //Sets new element content based on this.value
		$f.attr('lid',this.getNodeId());
		$f.attr('display_order',$v.attr('display_order'));
		$v.replaceWith($f);
            }
            else
            {
                var $f=$('<'+rwTag+'>');
		var options = this.options;
		delete this.options;
		delete this.textContainer;
		$v.replaceWith($f);
		this.viewNode=$f[0];
		for (var i=0;i<options.length;i++)
		    this['add<Options>'].call(this,options[i]);
            }
        }
    }});

})(jQuery);

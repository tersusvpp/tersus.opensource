(function( $, undefined ) {
    tersus.UIWidget('UIButton','UILabel', {
        defaultTag:'button'
        ,inputType:'button'
        /* Should the whole <Disable> mechanism go to the base class? */
        ,onClick:function(domNode, callback){
            if (this.disabled) // Used to allow disabling other elements
                return;
            DisplayNode.prototype.onClick.call(this, domNode, callback);
        }
        ,'get<Disabled>':function () {return this.disabled;}
        ,'set<Disabled>':function (disabled)
        {
                this.disabled = disabled;
                $(this.viewNode).attr('disabled',disabled);
        }
    });
})(jQuery);
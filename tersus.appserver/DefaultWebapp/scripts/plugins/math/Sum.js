/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Sum = function tersus_Add() {};
tersus.Sum.prototype = new ActionNode();
tersus.Sum.prototype.constructor = tersus.Sum;
tersus.Sum.prototype.start = function ()
{
	var values = this.getTriggerValues();
	var sum = 0;
	for (var j=0; j<values.length; j++)
	{
		var value = values[j];
		if (value.leafValue != null)
			sum += parseFloat(value.leafValue);
	}
	this.chargeLeafExit('<Sum>',sum);
};

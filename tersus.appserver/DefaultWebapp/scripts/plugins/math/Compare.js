/*********************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
tersus.newAction('Compare', function()
{
	var x = this.getLeafChild('<X>');
	var y = this.getLeafChild('<Y>');
	
	if (x<y)
		this.chargeLeafExit('<X<Y>',x);
	if (x>y)
		this.chargeLeafExit('<X>Y>',x);
	if (x==y)
		this.chargeLeafExit('<X=Y>',x);
});
/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Positive = function tersus_Positive() {};
tersus.Positive.prototype = new ActionNode();
tersus.Positive.prototype.constructor = tersus.Positive;
tersus.Positive.prototype.start = function ()
{
	var x = this.getLeafChild('<X>');
	if (x>0)
		this.chargeLeafExit('<Yes>',x)
	else
		this.chargeLeafExit('<No>',x);
};

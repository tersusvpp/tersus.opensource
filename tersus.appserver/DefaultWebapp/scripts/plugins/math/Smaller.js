/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Smaller = function tersus_Smaller() {};
tersus.Smaller.prototype = new ActionNode();
tersus.Smaller.prototype.constructor = tersus.Smaller;
tersus.Smaller.prototype.start = function ()
{
	var x = this.getLeafChild('<X>');
	var y = this.getLeafChild('<Y>');
	if (x < y)
		this.chargeLeafExit('<Yes>',x);
	else
		this.chargeLeafExit('<No>',x);
};

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Divide = function tersus_Divide() {};
tersus.Divide.prototype = new ActionNode();
tersus.Divide.prototype.constructor = tersus.Divide;
tersus.Divide.prototype.start = function ()
{
	var x = this.getLeafChild('<X>');
	var y = this.getLeafChild('<Y>');
	if (this.getElement('<Ratio>'))
		this.chargeLeafExit('<Ratio>', x/y);
	if (this.getElement('<Quotient>'))
		this.chargeLeafExit('<Quotient>',parseInt(x/y));
	if (this.getElement('<Remainder>'))
		this.chargeLeafExit('<Remainder>',x%y);
};

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Round = function tersus_Round() {};
tersus.Round.prototype = new ActionNode();
tersus.Round.prototype.constructor = tersus.Round;
tersus.Round.prototype.start = function ()
{
	var number = this.getLeafChild('<Number>');
	if (number == null)
		modelExecutionError('Missng value for <Number>', this);
	var precision = this.getLeafChild('<Precision>');
	if (!precision)
		precision =0;
	if (precision!=Math.round(precision))
		modelExecutionError('<Precision> must be an integer', this);
	var factor = Math.pow(10, precision);
	var rounded = Math.round(number * factor)/factor;
	this.chargeLeafExit('<Rounded>',rounded);
};

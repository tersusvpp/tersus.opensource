/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Minimum = function tersus_Minimum() {};
tersus.Minimum.prototype = new ActionNode();
tersus.Minimum.prototype.constructor = tersus.Minimum;
tersus.Minimum.prototype.start = function ()
{
	var values = this.getTriggerValues();
	if (values.length == 0)
	{
		modelExecutionError("No values specified - can't compute minimum",this);
		return;
	}
	var minimum = values[0].leafValue;
	for (var j=1; j<values.length; j++)
	{
		var value = values[j];
		if (value && value.leafValue < minimum)
			minimum = value.leafValue;
	}
	this.chargeLeafExit('<Minimum>',minimum);
};

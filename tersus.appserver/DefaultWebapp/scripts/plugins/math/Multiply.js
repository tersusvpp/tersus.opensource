/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Multiply = function tersus_Multiply() {};
tersus.Multiply.prototype = new ActionNode();
tersus.Multiply.prototype.constructor = tersus.Multiply;
tersus.Multiply.prototype.start = function ()
{
	var values = this.getTriggerValues();
	var product = 1;
	for (var j=0; j<values.length; j++)
	{
		var value = values[j];
		if (value.leafValue != null)
			product *= parseFloat(value.leafValue);
	}
	this.chargeLeafExit('<Product>',product);
};

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Substract = function tersus_Substract() {};
tersus.Substract.prototype = new ActionNode();
tersus.Substract.prototype.constructor = tersus.Substract;
tersus.Substract.prototype.start = function ()
{
	var x = parseFloat(this.getLeafChild('<X>'));
	var y = parseFloat(this.getLeafChild('<Y>'));
	this.chargeLeafExit('<Difference>',x-y);
};

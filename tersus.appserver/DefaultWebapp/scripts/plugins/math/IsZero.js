/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.IsZero = function tersus_IsZero() {};
tersus.IsZero.prototype = new ActionNode();
tersus.IsZero.prototype.constructor = tersus.IsZero;
tersus.IsZero.prototype.start = function ()
{
	var x = this.getLeafChild('<X>');
	if (x==0)
		this.chargeLeafExit('<Yes>',x)
	else
		this.chargeLeafExit('<No>',x);
};

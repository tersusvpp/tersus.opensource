/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Negative = function tersus_Negative() {};
tersus.Negative.prototype = new ActionNode();
tersus.Negative.prototype.constructor = tersus.Negative;
tersus.Negative.prototype.start = function ()
{
	var x = this.getLeafChild('<X>');
	if (x<0)
		this.chargeLeafExit('<Yes>',x)
	else
		this.chargeLeafExit('<No>',x);
};

/*********************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
tersus.newAction('Average', function()
{
	var numbers = this.getTriggerValues();
	
	if (numbers&& numbers.length > 0)
	{
		var sum = 0;
		for (var i=0; i<numbers.length; i++)
		{
			var value = numbers[i];
			if (value.leafValue != null)
			{
				sum += parseFloat(value.leafValue);
			}
		}
		this.chargeLeafExit('<Mean>', sum/numbers.length);
	}
});
/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.SquareRoot = function tersus_SquareRoot() {};
tersus.SquareRoot.prototype = new ActionNode();
tersus.SquareRoot.prototype.constructor = tersus.SquareRoot;
tersus.SquareRoot.prototype.start = function ()
{
	var x = this.getLeafChild('<X>');
	if (x != 0)
		this.chargeLeafExit('<Square Root>',Math.sqrt(x))
};
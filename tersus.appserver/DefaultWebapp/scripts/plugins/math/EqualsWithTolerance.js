/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.EqualWithTolerance = function tersus_EqualWithTolerance() {};
tersus.EqualWithTolerance.prototype = new ActionNode();
tersus.EqualWithTolerance.prototype.constructor = tersus.EqualWithTolerance;
tersus.EqualWithTolerance.prototype.start = function ()
{
	var x = this.getLeafChild('<X>');
	var y = this.getLeafChild('<Y>');
	var tolerance = this.getLeafChild('<Tolerance>');
	if (x == null)
		modelExecutionError('Missng value for <X>', this);
	if (y == null)
		modelExecutionError('Missng value for <Y>', this);
	if (tolerance == null)
		modelExecutionError('Missng value for <Tolerance>', this);
	if (Math.abs(x-y) < tolerance)
		this.chargeLeafExit('<Yes>',x)
	else
		this.chargeLeafExit('<No>',x);
};


tersus.Widget('ScrollPane2','HTMLTag', {
	defaultTag:'div'
	,defaultWrapperTags:'div'
	,attach:function()
	{
        var zoomable = this.prop('zoomable');
		var useTouchScroll = !zoomable && is_touch_device() && 
			$('<div style="overflow:scroll;-webkit-overflow-scrolling:auto;">').css('-webkit-overflow-scrolling') =='auto';
		var $w = $(this.wrapperNode);
		if (useTouchScroll)
		{
			$w.css('overflow','scroll');
		}
		else if (window.iScroll) // Scrolling using iScroll where supported
		{
			$(this.wrapperNode).css('overflow:hidden');
			this.iScroll = new iScroll(this.wrapperNode,{ hScrollbar: false, vScrollbar: false, onBeforeScrollStart:this.onBeforeScrollStart, zoom:zoomable, zoomMax:this.prop('zoommax') });
			var node=this;
			node.iScrollRefresh = function()
			{
				setTimeout(function(){if (node.iScroll) node.iScroll.refresh();},100);
			};	
			$(window).bind('ter-process-end', node.iScrollRefresh);
		}
		else // Overflow:hidden with no scrolling support other than the
                // browser's default
		{
			$(this.wrapperNode).css('overflow:hidden');
		}
	
	}
    ,'get<Double Tap Zoom>':function()
    {
        return this.iScroll && this.iScroll.options.doubleTapZoom;
    }

    ,'set<Double Tap Zoom>':function(value)
            {
                if (this.iScroll)
                    this.iScroll.options.doubleTapZoom = value;
             }
    ,'get<Max Zoom>':function()
    {
        return this.iScroll && this.iScroll.options.maxZoom;
    }

    ,'set<Max Zoom>':function(value)
            {
                if (this.iScroll)
                    this.iScroll.options.maxZoom = value;
             }
	,onResize: function()
	{
		tersus.HTMLTag.prototype.onResize.call(this);
		if (this.iScroll)
		{
			this.iScroll.refresh();
		}
	}
	,destroy:function()
	{
		if (this.iScroll && this.iScroll.destory)
		{
			this.iScroll.destory(true);
			this.iScroll=null;
		}
		if (this.iScrollRefresh)
			$(window).unbind('ter-process-end', this.iScrollRefresh);
		
		tersus.HTMLTag.prototype.destroy.call(this);
	}
	
	,onBeforeScrollStart:function(e) /*
                                         * iScroll Hook to detect cases where we
                                         * don't want to scroll
                                         */
	{
		if (   e.target.tagName == "SELECT"
            || e.target.tagName == "INPUT"
            || e.target.tagName == "BUTTON"
            || e.target.tagName == "A"
            || e.target.tagName == "TEXTAREA") {
            return true;
        }
		e.preventDefault();
	}
});
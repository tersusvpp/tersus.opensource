tersus.Widget('Dialog3','HTMLTag', {
	isDialog:true
	

	,start: function()
	{
		this.setup();
		tersus.popupNodes[this.getNodeId()]=this;
		this.create();
		FlowNode.prototype.start.call(this);
		this.isOpen = true;
		this.currentWindow.scrollTo(0,1); // Shouldn't this be dependent on scope?
		var self = this;
		tersus.popupNodes[this.getNodeId()]=this;
		setTimeout(function(){self[self.placement].call(self);},0);
	}
	,defaultOptions: { 'Floating':'pop,float','View':'slide,replace','Embedded':'slide,cover','Page':'slide,replace'}
	,setup:function()
	{
		if (this.element && ! this.element.prepareForHTML)
			this.element.prepareForHTML = DisplayElement.prototype.prepareForHTML;
		var w = this.currentWindow;
		var doc = w.document;
		
		clearSelection(w);
	
		this.scope = this.getChild('<Scope>');
		if (this.scope && this.scope.isText)
		{
			this.mode = this.scope.leafValue;
			if (this.mode == 'Page')
			{
				//Find containing page 
				this.scope=this.parent;
				while (this.scope && this.scope.isPage)
					this.scope = this.scope.parent;
			}
			else
				this.scope = w.currentRootDisplayNode; // Not sure this is correct
		}
		else if (this.scope)
			this.mode = 'Embedded';
		else
		{
			this.mode = 'Floating';
			this.scope = w.currentRootDisplayNode;
		}
		
		var options = this.getLeafChild('<Options>') || this.defaultOptions[this.mode];
		var p = options.split(/,/);
		if (p.length != 2)
			throw 'Dialog <Options> must be of the form "[transition],[placement]" - e.g "slide,replace" or "pop,over"';
		this.animationName = p[0];
		/*if (this.animationName == 'slide')
			this.animationName='ter-slide right';*/
		this.placement = p[1];
		
		this.previousNode = this.scope.currentDialog ||  this.scope;
		this.container = this.previousNode.wrapperNode || this.previousNode.viewNode;
		this.replacedNode = null;
		if (this.placement == 'replace')
		{
			this.replacedNode = this.container;
			this.container = this.container.parentNode;
		}
	}
	,float:function()
	{
		var $self = $(this.wrapperNode);
		this.$mask=$('<div>').addClass('ter-dialog-mask').css('zindex',++tersus.topZindex);
		$self.before(this.$mask);
		$self.css('zindex',++tersus.topZindex);
		$self.addClass(this.animationName + ' ter-active in');
		this.onResize = function()
		{
			var th = tersus.availableHeight();
			var dh = $self.outerHeight();
			$self.css('top',(th-dh)/2);
			this.$mask.css('height',th);
			this.onResizeChildren();
		}
		this.onResize();
		$self.animationEnd(function() {
			$self.removeClass("ter-slide slide pop out in reverse right left");
		});
	}
	,unfloat:function()
	{
		var $self = $(this.wrapperNode);
		var self = this;
		$self.addClass(this.animationName + ' out');
		$self.animationEnd(function() {
			$self.remove();
			self.$mask.remove();
			self.$mask = null;
		});
	}
	,cover:function()
	{
		this.onResize = function()
		{
			var $self  = $(this.wrapperNode);
			var $inner = $(this.viewNode);
			$self.css('height',$(this.container).outerHeight());
			this.onResizeChildren();					
		}
		var self=this;
		var $self = $(this.wrapperNode);
		$self.addClass('ter-active '+this.animationName);
		setTimeout(function()
		{
			$self.addClass('in');
			$self.animationEnd(function() {
			self.onResize();
			//Todo: at the end of the animation we probably want to clear these styles 
				//$self.removeClass("slide out in reverse");
			});
		},10);
	}
	,uncover:function()
	{
		var $self = $(this.wrapperNode);
		$self.addClass(this.animationName + ' out');
		$self.animationEnd(function() {
			$self.remove();
		});
	}
	,replace:function()
	{
		var $prev = $(this.previousNode.wrapperNode || this.previousNode.viewNode);
		var $self = $(this.wrapperNode);
		this.scope.currentDialog=this;
		tersus.transition($prev, $self, 'ter-active', this.animationName, null);
	}
	,unreplace:function()
	{
		var $prev = $(this.previousNode.wrapperNode || this.previousNode.viewNode);
		var $self = $(this.wrapperNode);
		this.scope.currentDialog=this.previousNode;
		tersus.transition($self, $prev, 'ter-active', this.animationName + ' reverse', function(){$self.remove();});
	}

	,attachViewNode:function()
	{
		$(this.container).addClass('ter-dialog-container').append($(this.wrapperNode).addClass('ter-dialog'));
	}

	,destroy: function()
	{
		if (! this.isDestroyed)
		{
			this.isDestroyed = true;
			this.supr('destroy');
			this.doClose();
		}
	}
	,closeWindow: function() 
	{
		this.destroyHierarchy();
	}
	
	,doClose: function()
	{
		delete tersus.popupNodes[this.getNodeId()];
		var self=this;
		self['un'+self.placement].call(self);
		this.isOpen = false;
	}
});

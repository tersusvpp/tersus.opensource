(function( $, undefined ) {
	if (!$.mobile)
		return;
	$.mobile.ajaxLinksEnabled = false; // For jQuery Mobile
	 $.mobile.autoInitializePage = false;

	tersus.isIphoneBrowser = navigator.userAgent.match(/Mobile.*Safari/) && !navigator.userAgent.match(/Android/) &&  !navigator.standalone;
	tersus.availableHeight = function(w)
	{
		if (!w) w = window;
		var h =  w.innerHeight ? w.innerHeight : $(w).height();
		return tersus.isIphoneBrowser & !tersus.safari.scrolled ? h+60 : h;

	};
	var setter=tersus.setter;
	$.mobile.ajaxEnabled = false;
tersus.safari={scrolled:false}; //Keep track of whether we've already scrolled away the browser chrome (iPhone) as it changes the behavior of innerHeight;
tersus.Widget('MobileView','HTMLTag',{
	defaultTag:'div'
	,hideNavbar:function()
	{
		if (this.navbar)
		{
			$(this.navbar).css('display','none');
		}
	}
	,showNavbar:function()
	{
		if (this.navbar)
		{
			$(this.navbar).css('display','block');
			$.fixedToolbars.show();
		}
	}
	,'set<Selected Page>':setter(function(p)
		{
			if (p.parent != this)
				throw {message:'Invalid element specified as selected page'};
			this.select(p.getNodeId());
		},true)
	,'get<Selected Page>':function(p)
	{
		return this.selectedPage;
	}
	,onload: function()
	{
		tersus.HTMLTag.prototype.onload.call(this);
	}
	,checkBrowser:function()
	{
	    if ( !$.mobile.gradeA() || !$.support.cssTransitions) {
	    	tersus.error('Your browser does not have all features required to run this application. For best results, please use Safari or Chrome');
	    }
	}
	,create:function(value){
		this.checkBrowser();
		tersus.HTMLTag.prototype.create.call(this);
		if (!$.mobile.gradeA())
			return;
        //set up active page
        var p = $.mobile.startPage = $.mobile.activePage = $('[data-role=page]').first();
        //set page container
        $.mobile.pageContainer = p.parent().addClass('ui-mobile-viewport');
        window.tersusMobileInit();
        //initialize page as active page
        p.page({addBackBtn:false}).addClass( $.mobile.activePageClass);
		$(this.navbar).find('a').each(function(i)
			{
				$(this).find('.ui-icon').css('background-image',$(this).data('iconurl'));
			});
		//remove rendering class
        $('html').removeClass('ui-mobile-rendering');
        $('html').removeClass('ui-loading');
        setTimeout(function(){window.scrollTo(0,1);tersus.safari.scrolled=true;},100);

	}
	,template:'${value}'

	,navSetup:function()
	{
		//Detect mobile safari to know when we can use real fixed positioning
		this.template = $.template(null,'<div data-role="footer" data-position="bottom"><div data-role="navbar"><ul>{{each(i,p) panes}}<li {{if p.tabClass}}class="${p.tabClass}"{{/if}}><a href="javascript:tersus.findNode(${id}).select(${p.paneId})" data-paneid=${p.paneId} {{if icon}}data-icon="${icon}"{{/if}}{{if iconURL}}data-iconurl=url(${iconURL}){{/if}}>{{if p.caption}}${p.caption}{{/if}}</a></li>{{/each}}</ul></div></div>');


		this.contentTag = 'div';
		this.contentClass = 'ter-tp-content';
		this.sharedProperties = $.extend({'data-role':'page'},this.sharedProperties);
		this.attach = function()
		{
			this.navbar=this.viewNode.lastChild;
			this.content=this.viewNode.firstChild;
//			$(this.selectedPage.wrapperNode).addClass('ter-tp-active');
			$(this.content.firstChild).addClass('ter-tp-active');
			$(this.navbar).find('a[data-paneid='+this.selectedPage.getNodeId()+']').addClass('ui-btn-active');
			if (this.selectedPage.includeInNavbar)
				this.showNavbar();
			else
				this.hideNavbar();

		};
		this.onResize = function()
		{
			var height = tersus.availableHeight();
			$('.ui-mobile-viewport').height(height);
			var nh = $(this.navbar).outerHeight();
			var d = this.selectedPage.includeInNavbar ? nh:0;
			$(this.content).css('min-height',height-d);
			$(this.content).css('height',height-d);
			if (tlog) tlog('On Resize: height='+height+' nh='+nh+' d='+d);
			this.eachDisplay(function(i,c){
				var d = c.includeInNavbar ? nh:0;
				 $(c.viewNode).css('min-height',height-d).parent().css('height',height-d);
				 });
			this.onResizeChildren();
			if (this.selectedPage.includeInNavbar)
				this.showNavbar();
			else
				this.hideNavbar();

		};
		this.select = function(id)
		{
			$(this.navbar).find('a').removeClass('ui-btn-active').filter('[data-paneid='+id+']').addClass('ui-btn-active');
			var p =tersus.findNode(id);
			if (!p ||  p == this.selectedPage)
				return;
			window.scrollTo(0,0);
			if (this.selectedPage.includeInNavbar)
				this.showNavbar();
			var reverse = p.element.displayOrder>this.selectedPage.element.displayOrder ? '' : ' reverse';

			var from=$(this.selectedPage.wrapperNode);
			this.selectedPage = p;
			var to=$(this.selectedPage.wrapperNode);
			to.prependTo(to.parent());
			from.addClass('slide out'+reverse);
			to.addClass('ter-tp-active slide in'+reverse);
			this.onResize();
			var v = this;
			to.animationComplete(function() {
				from.removeClass('ter-tp-active');
				from.add( to ).removeClass("slide out in reverse");
				if (p.includeInNavbar)
					v.showNavbar();
				else
					v.hideNavbar()

			});
			tersus.invokeAction('<On Select>',null,null,p);
		};
		this.templateData =  function(displayChildren)
		{
			var panes = [];
			var view = this;
			$.each(displayChildren, function(i,child)
			{
				if (i==0)
					view.selectedPage=child;
				var iconP=child.sprop('icon'), iconJQ=iconP, iconURL=null;
				child.includeInNavbar = child.sprop('includeInNavbar') != false; // Todo: support local property
				if (!child.includeInNavbar)
					return;
				if (iconP && iconP.match(/[.]/))
				{
					iconJQ='custom';
					iconURL=iconP;
				};
				panes.push({caption:child.getElement('<Caption>')?child.getCaption():null,icon:iconJQ,iconURL:iconURL,paneId:child.getNodeId(), tabClass:child.sprop('tabStyleClass')});
			});
			return {'panes':panes};
		}
	}
	,nonavSetup: function()
	{
		var pe = this.displayElements[0];
		pe.properties = $.extend({'data-role':'page'},pe.properties);
  		this.onResize = function()
		{
			var height = tersus.availableHeight();
			$('.ui-mobile-viewport').height(height);
			$(this.viewNode).css('min-height',height);
			this.eachDisplay(function(i,c){
				 $(c.wrapperNode).css('min-height',height);
				 $(c.viewNode).css('min-height',null);
				 });
			this.onResizeChildren();
		};
  	}
	,prepareForHTML:function(e,parent)
	{
		if (this.displayElements && this.displayElements.length >1 )
		{
			this.navSetup();
		}
		else if (this.displayElements)
		{
			this.nonavSetup();
		}

		return tersus.HTMLTag.prototype.prepareForHTML.call(this,e,parent);

	}
	,attachViewNode:function()
	{
		$(this.wrapperNode).appendTo( this.parent ? this.parent.viewNode : '#tersus\\.content');
	}
});
})(jQuery);

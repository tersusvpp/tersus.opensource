(function( $, undefined ) {
	var setter=tersus.setter;
	

tersus.Widget('Page','HTMLTag',{
	defaultTag:'div'
	,defaultWrapperTags:'div'
	,isPage:true
	,prepareForHTML:function(e,parent)
	{
		$(this.displayElements).each(
			function(i) {
				var dataRole = {'<Header>':'header','<Footer>':'footer','<Content>':content}[this.role];
				if (dataRole)
					this.properties = $.extend({'data-role':dataRole},this.properties);
				});
		return tersus.HTMLTag.prototype.prepareForHTML.call(this,e,parent);
	}
	,getScrollContainer:function()
	{
		if (!this.scrollContainer)
		{
			var $v=$(this.viewNode);
			var $s=$v.wrapInner('<div class="tersus-scroll-container"><div class="tersus-scroll-content"/></div>').find('.tersus-scroll-container');
			this.scrollContainer  = $s[0];
		}
		return this.scrollContainer;
	}
	,onBeforeScrollStart:function(e) /*iScroll Hook to detect cases where we don't want to scroll*/
	{
		if (   e.target.tagName == "SELECT"
            || e.target.tagName == "INPUT"
            || e.target.tagName == "BUTTON"
            || e.target.tagName == "A"
            || e.target.tagName == "TEXTAREA") {
            return true;
        }
		e.preventDefault();
		
	}
	,attach:function()
	{
		if (window.iScroll && this.sprop('scroll')!=false)
		{
			var sc=this.getScrollContainer();
			if (sc)
			{
				var self = this;
				$(sc).find('input').bind('vclick',function(){self.onResize();});
	//			this.iscroll = new iScroll(sc,{vScrollbar:false});
				this.iscroll = new iScroll(sc,{vScrollbar:false, onBeforeScrollStart:this.onBeforeScrollStart});
				var id=this.getNodeId();
				$(window).bind('ter-process-end', function(){setTimeout(
				function()
				{
					var n = tersus.findNode(id);
					if (n && n.iscroll)
					{
						n.iscroll.refresh();
					}
				}
				,1000)});
			}
		}
		
	}
	,refresh:function()
	{
	    var wasActive = $(this.viewNode).hasClass('ui-page-active');
	    tersus.HTMLTag.prototype.refresh.call(this);
	    $(this.viewNode).pageUpdate();		
	    if (this == this.parent.selectedPage)
		$(this.wrapperNode).addClass('ter-tp-active');
	    if (wasActive)
		$(this.viewNode).addClass('ui-page-active');
		
		
	}
	,destroy:function()
	{
//		if (this.iscroll)
//			this.iscroll.destroy();
		tersus.HTMLTag.prototype.destroy.call(this);
	}
	,onResize:function()
	{	
		//Set min-height on page content, based on explicit min-height on viewNode or wrapperNode
		var $v = $(this.viewNode);
		
		var c = this.wrapperNode || this.viewNode;
		var h;
/*		while (c.style && ! c.style.height)
			c = c.parentNode;
		if (c.style && c.style.height)
			h = parseInt(c.style.height);*/
		while (c && c.style && ! c.style.height)
			c = c.parentNode;
	        if (c && c.style && c.style.height)
		   h = parseInt(c.style.height);

		if (!h)
			h = $v.outerHeight(); // In certain dialogs the relevant height is on viewNode and not wrapperNode	
		var $c = $v.find('.ui-content');
		var bh = $c.outerHeight(true)-$c.height();
		var fh = 0; $v.find('.ui-footer,.ui-header,.ter-footer,.ter-header').each(function(index){ fh+=$(this).outerHeight();});
		$c.css('min-height',h-bh-fh);
		var s = this.iscroll;
		if (s)
		{
			var sc = this.getScrollContainer();
			$(sc).height(h);
			setTimeout(function(){s.refresh();},100);
		}
		this.onResizeChildren();	
	}
});

})(jQuery);

var setter = tersus.setter;

tersus.Widget('SelectionField','HTMLTag',{
	defaultTag:'div'
	,jQuery:'fieldcontain'
	,templateData: function()
	{
		var n=this;
		var options=$(this.options).map(function(i,o){
													return {val:(o==null ? null:i), text:n.translate(n.getOptionText(o))};
													 });
		var btnIconSide = $(document.body).css('direction')=='rtl' ? 'left':'right';
		return {options:options, text:this.translate(this.getOptionText(this.value)), btnIconSide:btnIconSide};
	}
	,reset: function()
	{
		this.$select = null;
		this.supr('reset');
	}

	,template: '{{if caption}}<label for="${id}">${caption}</label>{{/if}}<div class="ui-select">'+
	'<div class="ui-btn ui-btn-icon-${btnIconSide} ui-btn-corner-all ui-shadow ui-btn-down-c ui-btn-up-c">'+
	'<span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text">${text}</span><span class="ui-icon ui-icon-arrow-d ui-icon-shadow"></span></span>'+
	'<select id="${id}">{{each(i,opt) options}}<option {{if (opt.val != null)}}value="${opt.val}"{{/if}}>${opt.text}</option>{{/each}}</select>'+
	'</div></div>'
	,attach:function()
	{
		 var self = this;
		 this.$select = $(this.viewNode).find('select');
		 this.$select.blur(function(){$.mobile.silentScroll(0);});
		 this.$buttonText = $(this.viewNode).find('.ui-btn-text');
		 this.$select.change(function(){
		 	self.jqRefresh();
		 	});
		 this.setSelectedIndex(this.findIndex(this.value));
		 self.jqRefresh();
	}
	,jqRefresh: function()
	{
		this.$buttonText.html(this.translate(this.getOptionText(this.getValue())));
/*		var s = this.$select.data('selectmenu');
		if (s && s.refresh)
			s.refresh(true);*/
			
	}
	,'set<Value>': setter(function(value)
	{
		this.value = value;
		if (this.$select)
		{
			this.setSelectedIndex(this.findIndex(this.value));
			this.jqRefresh();
		}
	},true)
	,'remove<Value>': function()
	{
		if (this.options && this.options.length>0)
			this.value = this.options[0];
		if (this.$select)
		{
			this.setSelectedIndex(0);
			this.jqRefresh();
		}
	}
	,getValue:function()
	{
		var selectedIndex;
//		if (this.tmpSelectedIndex != null) // In IE, the selected index is updated asynchronously, so we use tmpSelectedIndex if it is set
//			selectedIndex = this.tmpSelectedIndex;
//		else
		var selectedIndex = this.$select[0].selectedIndex;
		if (selectedIndex < 0)
			return null;
		return this.options[selectedIndex];
	}
	,'get<Value>':function()
	{
		return this.getValue();
	}	
	,findIndex: function(value)
	{
		if (this.options)
			for (var i=0; i<this.options.length; i++)
			{
				var option = this.options[i];
				if (compareValues(option, value))
					return i;
			}
		return -1;
	}
	,setSelectedIndex: function(index)
	{
		this.$select[0].selectedIndex = Math.max(index,0);
	}
	,'get<Placeholder Text>': function(){return this.emptyOptionText;}
	,'set<Placeholder Text>': function(text)
	{
		var isNew = this.emptyOptionText == null;
		this.emptyOptionText = text;
		if (isNew)
		{
			if (this.options)
				this.options.splice(0,0,null); // add null to beginning of list
			else
				this.options = [null];
		}
		if (this.$select)
		{
			if (isNew)
				this.$select.prepend($("<option>").text(this.translate(this.getOptionText(null))));
			else
				this.$select.find('option').first().text(this.translate(this.getOptionText(null)));
			if (this.value == null)
				this.setSelectedIndex(0);
			this.jqRefresh();
		}
	}
	,'remove<Placeholder Text>': function()
	{
		if (this.emptyOptionText != null)
		{
			this.emptyOptionText = null;
			this.options.splice(0,1); //remove first
			if (this.$select)
			{
				this.$select.first().remove();
				this.jqRefresh();
			}
		}
		
	}
	,'remove<Options>': function()
	{ 
		this.options = this.emptyOptionText == null ? [] : [null];
		if (this.$select)
		{
			var self = this;
			this.$select.children().filter(function(i){return i>0 || null == self.emptyOptionText;}).remove();
			this.jqRefresh();
		}
	}
	,'add<Options>':setter(function(option)
	 {
	 	if (!this.options)
	 		 this.options = [];
	 	this.options.push(option);
	 	if (this.$select)
	 	{
			this.$select.append($("<option>").text(this.translate(this.getOptionText(option))).val(this.options.length));
			if (this.value != null &&  compareValues(this.value, option) || this.options.length==1)
			{
				this.setSelectedIndex(this.options.length-1);
				this.jqRefresh();
			}
		}
	 },true)
    ,'get<Options>':function()
    {
    	if ( !this.options)
			return [];
			if (this.emptyOptionText != null)
			{
				return this.options.slice(1); // Ignoring the first item in the array, which is the empty option (null)
			}
		return this.options;
	}
	,getOptionText: function(value)
	{
        var outputValue = null;
        if (value && value.elementList)
        {
			var textElementRole = value.__textElementRole;
			if (textElementRole == null)
			{
				for (var i=0; i< value.elementList.length; i++)
				{
					var element = value.elementList[i];
					if (i==0)
					{
				        textElementRole = element.role; // If no text element is found, the first element is used
				        value.constructor.prototype.__textElement = element;
					}
					if (element.modelId == BuiltinModels.TEXT_ID)
					{
				        textElementRole = element.role;
				        value.constructor.prototype.__textElement = element;
				        break;
					}
				}
				value.constructor.prototype.__textElementRole = textElementRole;
			}
			if (textElementRole && value.children)
			        outputValue = value.children[textElementRole];
        }
        else
                outputValue = value;
        var text = '';
        if (outputValue != null)
                text =  outputValue.formatString(this.sharedProperties);
        else if (this.emptyOptionText != null)
                text = this.emptyOptionText;
        return text;
	}
});

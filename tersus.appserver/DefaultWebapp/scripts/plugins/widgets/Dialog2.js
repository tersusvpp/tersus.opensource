(function( $, undefined ) {
	if (!$.mobile)
		return;
tersus.Widget('Dialog2','Page', {
	baseProperties:{'data-role':'page'}
	,isDialog:true
	,attachViewNode:function()
	{
		this.mode = 'View';
		if (this.scope && this.scope.isText)
		{
			this.mode = this.scope.leafValue;
			if (this.mode == 'Page')
			{
				this.scope=this.parent;
				//Find containing page 
				while (this.scope && this.scope.plugin != 'Tersus/Widgets/Page')
					this.scope = this.scope.parent;
			}
		}
		else if (this.scope == window.rootDisplayNode)
			this.mode = 'View';
		else if (this.scope != null)
			this.mode = 'Embedded';

		if (this.mode == 'View')
		{
			$.mobile.pageContainer.add('#tersus\\.content').last().append(this.wrapperNode);
			this.previousPage = $.mobile.activePage;
		}
		else
		{
			var $self=$(this.wrapperNode);
			if (this.mode == 'Floating')
			{
				var theme = null;
				$.mobile.pageContainer.add('#tersus\\.content').last().append($self.addClass('ter-dialog ter-overlay'));
			}
			else
			{
				$(this.scope.wrapperNode).addClass('ter-dialog-container').append($(this.wrapperNode).addClass('ter-dialog'));;
			}
			var $h=$self.find('[data-role=header]');
			if ($h.length>0)
				$h.addClass( "ui-header ui-bar-" + (theme || $self.parent('[data-role=page]').data( "theme" ) || "a") ).find('h1').addClass('ui-title');
			else
				$self.addClass('ter-noheader');
			$self.find('[data-role=content]').addClass( 'ui-content');;
		}
		
	}
	,getScrollContainer:function()
	{
		if (! this.scrollContainer)
		{
			if (this.mode == 'View')
			{
				$(this.viewNode).wrapInner('<div>');
				this.scrollContainer =  this.viewNode;
			}
			else
				this.scrollContainer  = this.wrapperNode;
		}
		return this.scrollContainer;		
	}
	,isPopup:true
	,isPopin:true
	,start: function()
	{
		this.scope = this.getChild('<Scope>');
		if (this.element && ! this.element.prepareForHTML)
			this.element.prepareForHTML = DisplayElement.prototype.prepareForHTML;
		var w = this.currentWindow;
		var doc = w.document;
		clearSelection(w);
		this.create();
		FlowNode.prototype.start.call(this);
		this.isOpen = true;
		w.scrollTo(0,1);
		var transition;
		
		if (this.mode === 'View' )
		{
			$(this.viewNode).page({addBackBtn:false});
			transition = this.replacePage;
		}
		else
		{
			if (this.mode == 'Page')
			{
				$(this.viewNode).find('[data-role=header]').addClass('ui-header ui-bar ui-bar-a').find('h1').addClass('ui-title');
				$(this.viewNode).find('[data-role=content]').addClass('ui-content');
			}
			transition = this.mode == 'Floating' ?this.popDialog : this.slideIn ;
		}	
		var self = this;
		tersus.popupNodes[this.getNodeId()]=this;
		setTimeout(function(){transition.call(self);},200);
		
		// Make scope the DOM Parent
		// Set style on scope and self
	}
	,enhanceForms:function()
	{
		var $self = $(this.wrapperNode);
		$self
			.find( "input, textarea" )
            .not( "[type='radio'], [type='checkbox'], button, [type='button'], [type='submit'], [type='reset'], [type='image']" )
            .textinput();
	    $self
            .find( "input, select" )
            .filter( "[data-role='slider'], [data-type='range']" )
            .slider();

  		/*$self
            .find( "select:not([data-role='slider'])" )
            .selectmenu();*/
        $self
            .find( "[type='radio'], [type='checkbox']" )
            .checkboxradio();
        $self
	        .find( "button, [type='button'], [type='submit'], [type='reset'], [type='image']" ).not("[data-role='none']")
            .button();
	}
	,popDialog:function()
	{
		var $self = $(this.wrapperNode);
		this.enhanceForms();
           // enchance form controls
		$self.wrapInner('<div>');
		$self.addClass('ter-dialog-active pop in');


        
		this.onResize();
		$self.animationEnd(function() {
			$self.removeClass("pop out in reverse");
		});
	}
	,onResize: function()
	{
		var $self = $(this.wrapperNode);
		var $inner =$(this.viewNode);
		var th = tersus.availableHeight();
		if (this.mode == 'Floating')
		{
			var dh = $self.outerHeight();
			$self.css('top',(th-dh)/2);
			this.onResizeChildren();
		}
		else
		{
			if (this.mode=='View')
			{
//				$inner.css('height',th);
			    $inner.css('min-height',th);
			}
			else
			{
				$self.css('height',$(this.scope.wrapperNode).outerHeight());
				if (this.mode != 'Page')
					$self.css('width',$(this.scope.viewNode).innerWidth());
			}
			tersus.Page.prototype.onResize.call(this);
		}		
	}
	,slideIn:function()
	{
		var self=this;
		var $self = $(this.wrapperNode);
		this.enhanceForms();
		$self.addClass('ter-dialog-active ter-slide right');
		setTimeout(function()
		{
			$self.addClass('in');
			$self.animationEnd(function() {
			self.onResize();
			//Todo: at the end of the animation we probably want to clear these styles 
				//$self.removeClass("slide out in reverse");
			});
		},10);
	
	}
	,changePage: function(from, to, transition, back)
	{
		from.addClass( transition + " out " + ( back ? "reverse" : "" ) );
		to.addClass( $.mobile.activePageClass + " " + transition +
			" in " + ( back ? "reverse" : "" ) );
					// callback - remove classes, etc
		to.animationEnd(function() {
			from.add( to ).removeClass("out in reverse " + transition );
			from.removeClass( $.mobile.activePageClass );
			loadComplete();
			removeContainerClasses();
		});
	
	}
	
	,replacePage:function()
	{
		var self = this;
		var $self  = $(this.viewNode).page();
		$.mobile.activePage = $self;
		var $prev = this.previousPage;
		$self.addClass('ter-slide right ui-page-active');
		if ($prev != null)
			$prev.addClass('ter-slide left ui-page-active in');
		setTimeout(function()
		{
			$self.addClass('in');
			self.onResize();
			if ($prev)
			{
				$self.animationEnd(function()
				{
					$prev.removeClass('ui-page-active').add($self).removeClass('ter-slide right left in');
				});
				$prev.removeClass('in');
			}
		},10);
	}
	,destroy: function()
	{
		if (! this.isDestroyed)
		{
			this.isDestroyed = true;
			this.supr('destroy');
			this.doClose();
		}
	}
	,closeWindow: function() 
	{
		this.destroyHierarchy();
	}
	,refresh: function()
	{
		var wasactive = $(this.viewNode).hasClass('ui-page-active');
		tersus.Page.prototype.refresh.call(this);
		if (wasactive)
			$(this.viewNode).addClass('ui-page-active');
		
	}
	,doClose: function()
	{
		delete tersus.popupNodes[this.getNodeId()];
		var self=this;
		var $self = $(this.viewNode);
		var $wrapper = $(this.wrapperNode);
		if (this.isOpen)
		{
			if (this.mode == 'View')
			{
				var doc = this.currentWindow.document;
				var $prev=this.previousPage;
				$.mobile.activePage = $prev;
				
				if ($prev)
					$prev.addClass('ui-page-active ter-slide left');
				$self.addClass('ui-page-active ter-slide right in');
				setTimeout(function()
				{
					$self.animationEnd(function(){
							$wrapper.remove();
							$.fixedToolbars.show(true);
						});
					$self.removeClass('in');
					if ($prev)
						$prev.addClass('in');
					
				},0);
				

			}
			else if (this.mode == 'Floating')
			{
				$(this.wrapperNode).addClass('pop out reverse').animationEnd(function()
				{
					$wrapper.remove();
				});
			
			}
			else
			{
				var $scope = $(this.scope.viewNode);
				$wrapper.addClass('ter-slide right in');
				setTimeout(function()
				{
					$wrapper.removeClass('in').animationEnd(function()
					{
						$wrapper.remove();
						$scope.removeClass('ter-dialog-container');		
					});
				},0);
			}
			this.viewNode = null;
			this.rootNode = null;
		}
		this.isOpen = false;
	}
});
})(jQuery);

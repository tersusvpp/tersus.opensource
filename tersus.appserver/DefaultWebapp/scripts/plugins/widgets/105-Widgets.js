
(function( $, undefined ) {
var setter = tersus.setter = function(f, requiresNodes)
{
	if (requiresNodes)
		f.requiresNodes = true;
	return f;
	
};

$.fn.tersus = function(options)
{
	return this.map(function(ix, e) {return DisplayNode.findNodeForDocumentNode(e);});
};
//animation/transition end callback



var register = function(eventType, domNode)
		{
			if (eventType===Events.ON_CLICK)
			{
				tersus.HTMLTag.prototype.registerEventHandler.call(this,eventType, domNode);
				return;
			}
			if (!eventType.jsName)
			{
				throw {message:('No event dispatcher defined for '+eventType.elementName)};
				return;
			}
			var dispatcher = this.currentWindow.tersus_dispatchers[eventType.jsName];
			if (!dispatcher)
				throw {message:('No event dispatcher defined for '+eventType.jsName)};

			if (!domNode)
			{
				if (this.eventTargets)
					domNode = this.eventTargets[eventType.jsName];
				if (!domNode)
					domNode = this.viewNode;
			 }
			domNode.setAttribute('lid',this.getNodeId());
			$(domNode).bind(eventType.name,dispatcher);
		};
		
tersus.Widget = function(name, base, options)
{
	if (tersus[name])
		throw 'Duplicate definition of \'tersus.\''+name;
		
	var c = tersus[base].__subclass('tersus.'+name);
	tersus[name]=c;
	var p = c.prototype;
	$.extend(p,{
		
		postAttach:function()
		{
			var e = this.viewNode;
			var p = e.getAttribute('data-role');
			if (p=='button') p='buttonMarkup';
			var node = this;
			var pl = $.fn[this.jQuery];
			if (pl)
				pl.apply($(e));
			if (e.tagName=='A')
				$(e).find('span.ui-btn-text').each(function(i,e1){node.textContainer=e1;}); // Should make <Value> work, but not <Disabled>
			if (this.attach)
				this.attach();
			
		}
		,registerEventHandler:register
		
		,eachDisplay: function(callback)
		{
			if (this.elements)
			{
				var i=0;
				for (var elementRole in this.elements)
				{
					var element = this.getElement(elementRole);
					if (element.isDisplayElement)
					{
						if (element.isRepetitive)
						{
							var cc = element.getChildren(this);
							for (var j=0;j<cc.length;j++)
							{
								var c=cc[j];
								if (callback.call( c, i, c ) === false)
									return;
								++i;
							}
						}
						else
						{
							var c = element.getChild(this);
							if (c)
							{
								if (callback.call( c, i, c ) === false)
									return;
								++i;
							}
						}
					}							
				}
			}
		}		
	});
	
	$.extend(p,options);
	if (typeof options.template === 'string')
		p.template=$.template(null,options.template);
	if (p.template)
	{
		p.addHTMLTagContent = function(htmlV, displayChildren, prep)
		{
			var caption = null;
			if (this.getElement('<Caption>'))
				caption = this.getCaption();
			var data = {'caption':caption, value:this.valueStr, id:this.getNodeId(), readOnly:this.currentReadOnly};
			if (typeof this.templateData === 'function')
				$.extend(data, this.templateData.call(this, displayChildren)); // Add widget-specific data
			else if (typeof this.templateData === 'object')
				$.extend(data, this.templateData);
			var t = this.template($,{'data':data});
			$.merge(htmlV,t);
		};
	};
};

tersus.Widget('Toolbar','HTMLTag',{
	defaultTag:'div'
});
tersus.Widget('Label2','HTMLTag', {
	defaultTag:'h2'
	,template:'${caption}'
	,captionChanged:function(){$(this.viewNode).html(this.getCaption());}
});
tersus.Widget('Button2','HTMLTag', {
	defaultTag:'a'
	//,jQuery:'buttonMarkup'
	//,baseProperties:{'data-role':'button'}
	,template:'${caption}'
	,attach: function(){var node=this;$(this.wrapperNode).not('.ui-btn').buttonMarkup().prev().find('span.ui-btn-text').each(function(i,e1){node.textContainer=e1;;});}
	,onClick:function(domNode, callback){
		if (this.getLeafChild('<Disabled>'))
			return;
		DisplayNode.prototype.onClick.call(this, domNode, callback);
	}
	,captionChanged:function(){$(this.viewNode).find('.ui-btn-text').text(this.getCaption());}
	,'jq<Icon>': function(icon) {$(this.viewNode).data('icon',icon).buttonUpdate();}
});

/*

	// A version of Button2 with more explicit HTML Structure.  Doesn't support icons

tersus.Widget('Button2','HTMLTag', {
	defaultTag:'a'
	,defaultClass:'ui-btn ui-btn-corner-all ui-shadow ui-btn-up-c'
	//,jQuery:'buttonMarkup'
	//,baseProperties:{'data-role':'button'}
	,template:'<span class="ui-btn-inner"><span class="ui-btn-text">${caption}</span></span>'
	,attach: function(){
		this.textContainer=$(this.viewNode).find('.ui-btn-text')[0];
	}
	,onClick:function(domNode, callback){
		if (this.getLeafChild('<Disabled>'))
			return;
		DisplayNode.prototype.onClick.call(this, domNode, callback);
	}
	//,'jq<Icon>': function(icon) {$(this.viewNode).data('icon',icon).buttonUpdate();}
});
 */

tersus.Widget('FieldWidget','HTMLTag',{
	template:'{{if readOnly}}{{if caption}}<label for="${id}" class="ui-input-text" >${caption}</label>{{/if}}<span id="${id}" class="ter-field-readonly">${value}</span>'
			+'{{else}}{{if caption}}<label for="${id}" class="ui-input-text">${caption}</label>{{/if}}<input type="${fieldType}" id="${id}" value="${value}"/>{{/if}}'
	,defaultTag:'div'
	,jQuery:'fieldcontain'
	,setTextNode: function()
	{
		var $this = $(this.viewNode);
		this.textContainer = $this.find('.ter-field-readonly')[0];
		this.inputField = $this.find('input,textarea')[0];
	}

	,attach:function(){
		this.setTextNode();
		this.eventTargets={'onfocus':this.inputField};
		$(this.viewNode).find('input[type="text"]').textinput();
	}
	,updateSelfReadOnly: function(ro) {
		this.currentReadOnly=ro;
		if (!this.viewNode)
			return;
		if (ro)
		{
			var $f = $('<span class="ter-field-readonly"/>');
			$f.text(this.valueStr);
			$f.attr('id',this.getNodeId());
			$(this.viewNode).find('input').replaceWith($f);
		}
		else
		{
			var $f = $('<input type="text" class="ui-input-text ui-body-null ui-corner-all ui-shadow-inset ui-body-c"/>');
			$f.val(this.valueStr);
			$f.attr('id',this.getNodeId());
			$(this.viewNode).find('span.ter-field-readonly').replaceWith($f);
		}
		this.setTextNode();
	}
});
tersus.Widget('TextArea2','FieldWidget',{
	template:'{{if caption}}<label for="${id}">${caption}</label>{{/if}}<textarea id="${id}" value="${value}"/>'
});

tersus.Widget('TextField','FieldWidget',{
	templateData:{fieldType:'text'}
});
tersus.Widget('PasswordField2','FieldWidget',{
	templateData:{fieldType:'password'}
});
tersus.Widget('NumberField','FieldWidget',{
	templateData:{fieldType:'number'}

});

tersus.Widget('DateField2','FieldWidget',{
	templateData:{fieldType:'text'}
//	,attach:function(){tersus.FieldWidget.prototype.attach.call(this);var $f=$(this.inputField); setTimeout(function(){$f.datebox();},0);}

});
tersus.Widget('List','HTMLTag',{
	defaultTag:'ul'
	,attach:function(){if(this.pluginVersion<1) $(this.viewNode).listview();}
});
tersus.Widget('Form','HTMLTag',{
	defaultTag:'form'
});
tersus.Widget('ListItem','HTMLTag',{
	defaultTag:'li'
	,attach:function()
	{
		if (this.pluginVersion<1)
		{
			if (this.sprop('data-role')=='none')
				return;
		 	var lv = $(this.viewNode).parent().data('listview'); if (lv) lv.refresh();
		 }
	}
	,prepareForHTML:function(e,parent)
	{
		if (this.pluginVersion<1 && this.hasEventHandler(Events.ON_CLICK))
		{
			$(this.displayElements)
				.filter(function(i){
					return this.getChildConstructor().prototype.plugin=='Tersus/Widgets/Text Display';
				})
				.first()
				.each(function(){
					this.properties = $.extend({wrapperTags:'a'},this.properties);
				});
		}
		return tersus.HTMLTag.prototype.prepareForHTML.call(this);
	}
});
tersus.Widget('ListDivider','HTMLTag',{
	defaultTag:'li'
	,baseProperties:{'data-role':'list-divider'}
	,template:'${caption}'
});

tersus.Widget('Image2','HTMLTag',{
	defaultTag:'img'
});
tersus.Widget('TextDisplay','HTMLTag',{
	defaultTag:'span'
});
tersus.Widget('NumberDisplay','HTMLTag',{
	defaultTag:'span'
});
tersus.Widget('DateDisplay','HTMLTag',{
	defaultTag:'span'
});

tersus.Widget('Slider', 'HTMLTag', {
	templateData: function(){return {min:13, max:77};}
	,template:'{{if caption}}<label for="${id}">${caption}</label>{{/if}}<input type="number" id="${id}" value="${value}" min=${min} max=${max}/>'
	,defaultTag:'div'
	,jQuery:'fieldcontain'
	,attach:function(){this.inputField=this.viewNode.lastChild; $(this.inputField).slider();}
});

tersus.Widget('RadioButton2','Button2',
{
	onClick:function(domNode, callback){
		var $p = $(this.parent.viewNode); $self = $(this.viewNode);
		$p.find('.ui-btn').removeClass('ui-btn-active');
		$self.addClass('ui-btn-active');
		DisplayNode.prototype.onClick.call(this, domNode, callback);
		this.onChange(domNode, callback);
		this.parent.onChange(domNode, callback);
	}
	,getButtonValue:function()
	{
		if (this.value != undefined)
			return this.value;
		else
		{
			var c = tersus.repository.get(BuiltinModels.TEXT_ID);
			var node = new c();
	        node.prototype = c.prototype;
	        node.setLeaf(this.getBaseCaption());
			return node;
		}
	}
});

tersus.Widget('ButtonGroup','HTMLTag',{
	defaultTag:'fieldset',
	attach:function()
	{
		var $self = $(this.viewNode);
		$self.addClass('ui-corner-all ui-controlgroup ui-controlgroup-horizontal');
		setTimeout(function()
		{
			var rtl= $self.css('direction') == 'rtl';
			var $buttons = $self.find('.ui-btn');
			$self.find('.ui-btn-corner-all').removeClass('ui-btn-corner-all ui-shadow');
			$buttons.first().addClass(rtl ? 'ui-corner-right' : 'ui-corner-left');
			$buttons.last().addClass(rtl ? 'ui-corner-left' : 'ui-corner-right');
			
			if ($buttons.filter('.ui-btn-active').length == 0)
				$buttons.first().addClass('ui-btn-active');
		},0);
	}
	,'get<Value>':function()
	{
		var $active = $(this.viewNode).find('.ui-btn-active').tersus();
		if ($active.length>0)
			return $active[0].getButtonValue();
		 
	}	
	
	,'set<Value>':setter(function(value)
	{
		var $self = $(this.viewNode);
		$self.find('.ui-btn-active').removeClass('ui-btn-active');
		$self.find('.ui-btn').each(function(index){
			var node = $(this).tersus()[0];
			if (node && node.getButtonValue && compareValues(value, node.getButtonValue()))
				$(this).addClass('ui-btn-active');
		});
		
	},true)
});
$.fn.pageUpdate = function(options)
{
	var $elem=$(this);
	// If we're refreshing a direct component of a page - update the whole page
	var $dataEls = $elem.find( "[data-role]" ).andSelf().each(function() {
          				var $this = $( this ),
                        role = $this.data( "role" ),
                        theme = $this.data( "theme" );

                        //apply theming and markup modifications to page,header,content,footer
                        if ( role === "header" || role === "footer" ) {
                        	$this.addClass( "ui-bar-" + (theme || $this.parent('[data-role=page]').data( "theme" ) || "a") );

                            // add ARIA role
                            $this.attr( "role", role === "header" ? "banner" : "contentinfo" );

                            //right,left buttons
                            var $headeranchors = $this.children( "a" ),
                            leftbtn = $headeranchors.hasClass( "ui-btn-left" ),
                            rightbtn = $headeranchors.hasClass( "ui-btn-right" );

                            if ( !leftbtn ) {
                                leftbtn = $headeranchors.eq( 0 ).not( ".ui-btn-right" ).addClass( "ui-btn-left" ).length;
                            }

                            if ( !rightbtn ) {
                                rightbtn = $headeranchors.eq( 1 ).addClass( "ui-btn-right" ).length;
                            }
                            //page title
                            $this.children( "h1, h2, h3, h4, h5, h6" )
                                    .addClass( "ui-title" )
                                        //regardless of h element number in src, it becomes h1 for the enhanced page
                                        .attr({ "tabindex": "0", "role": "heading", "aria-level": "1" });
                        } else if ( role === "content" ) {
                            if ( theme ) {
                                    $this.addClass( "ui-body-" + theme );
                            }

                            // add ARIA role
                            $this.attr( "role", "main" );

                        } else if ( role === "page" ) {
                                $this.addClass( "ui-body-" + (theme || "c") );
                    }

					switch(role) {
                        case "header":
                        case "footer":
                        case "page":
                        case "content":
    	                    $this.addClass( "ui-" + role );
	                        break;
                        case "collapsible":
                        case "fieldcontain":
                        case "navbar":
                        case "listview":
                        case "dialog":
                            $this[ role ]();
                            break;
                    }
                });

                //links in bars, or those with data-role become buttons
                $elem.find( "[data-role='button'], .ui-bar > a, .ui-header > a, .ui-footer > a" )
                        .not( ".ui-btn" )
                        .buttonMarkup();

                $elem
                        .find("[data-role='controlgroup']")
                        .controlgroup();
                        
                                 var o = this.options;

             
                // enchance form controls
               $elem
                        .find( "[type='radio'], [type='checkbox']" )
                        .checkboxradio();

			   $elem
                        .find( "button, [type='button'], [type='submit'], [type='reset'], [type='image']" )
                        .button();

                $elem
                        .find( "input, textarea" )
                        .not( "[type='radio'], [type='checkbox'], button, [type='button'], [type='submit'], [type='reset'], [type='image']" )
                        .textinput();

                $elem
                        .find( "input, select" )
                        .filter( "[data-role='slider'], [data-type='range']" )
                        .slider();

           /*     $elem
                        .find( "select:not([data-role='slider'])" )
                        .selectmenu();*/
                        
		

};
$.fn.buttonUpdate = function( options ){
	return this.each( function() {
		var el = $( this ),
		    o = $.extend( {}, $.fn.buttonMarkup.defaults, el.data(), options),

			// Classes Defined
			buttonClass,
			innerClass = "ui-btn-inner",
			iconClass;


		// if not, try to find closest theme container
		if ( !o.theme ) {
			var themedParent = el.closest("[class*='ui-bar-'],[class*='ui-body-']"); 
			o.theme = themedParent.length ?
				/ui-(bar|body)-([a-z])/.exec( themedParent.attr("class") )[2] :
				"c";
		}

		buttonClass = "ui-btn ui-btn-up-" + o.theme;
		if ( o.inline ) {
			buttonClass += " ui-btn-inline";
		}
		
		if ( o.icon ) {
			o.icon = "ui-icon-" + o.icon;
			o.iconpos = o.iconpos || "left";

			iconClass = "ui-icon " + o.icon;

			if ( o.shadow ) {
				iconClass += " ui-icon-shadow"
			}
		}

		if ( o.iconpos ) {
			buttonClass += " ui-btn-icon-" + o.iconpos;
			
			if ( o.iconpos == "notext" && !el.attr("title") ) {
				el.attr( "title", el.text() );
			}
		}
		
		if ( o.corners ) { 
			buttonClass += " ui-btn-corner-all";
			innerClass += " ui-btn-corner-all";
		}
		
		if ( o.shadow ) {
			buttonClass += " ui-shadow";
		}
		
		el
			.attr( "data-theme", o.theme )
			.addClass( buttonClass );

		el.attr('class',buttonClass);
		el.find('.ui-btn-inner').attr('class',innerClass);
		if (o.icon)
			el.find('.ui-icon').attr('class',iconClass);
	});		
};

})( jQuery );
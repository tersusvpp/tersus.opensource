/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.newAction('Extract', function()
{
	var text = this.getLeafChild('<Text>');
    if (text != null)
    {
        var beginIndex = this.getLeafChild('<Position>');
        if (beginIndex <0)
        {
	        modelExecutionError('Negative <Position>',this);
    	    return;
    	}
        var length = this.getLeafChild('<Length>');
        var endIndex = text.length;
        if (length != null)
        {
            if (length < 0)
		        modelExecutionError('Negative <Length>',this);
            endIndex = beginIndex + length;
            if (endIndex > text.length)
            {
               modelExecutionError("<Text> ('" + text
                            + "') too short to extract a segment of length "
                            + length + " at position " + beginIndex + ".");
               return;
            }
        }
        var segment = text.substring(beginIndex, endIndex);
        this.chargeLeafExit('<Segment>', segment);
	}
});


/*********************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
tersus.newAction('Split', function()
{
	var sep = this.getLeafChild('<Separators>');
	var text = this.getLeafChild('<Text>');
	var allowEmpty = this.getLeafChild('<Return Empty Strings>');


	var start = 0;
	var pos = 0;
	while (pos < text.length)
	{
		var c = text.charAt(pos);
		if (sep.indexOf(c) >= 0) // Found a separator
		{
			if (allowEmpty || pos > start)
				this.chargeLeafExit('<Segments>',text.substring(start, pos));
			++pos;
			start = pos;
		}
		else
			++pos;
	}
	if (allowEmpty || pos > start)
		this.chargeLeafExit('<Segments>',text.substring(start, pos));
});

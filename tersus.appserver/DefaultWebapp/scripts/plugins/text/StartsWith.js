/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.newAction('StartsWith', function()
{
	var text = this.getLeafChild('<Text>');
	var prefix = this.getLeafChild('<Prefix>');
	if (text != null && prefix != null)
	{
		if (text.length >= prefix.length && text.substring(0,prefix.length) == prefix)
			this.chargeLeafExit('<Yes>',text);
		else
			this.chargeLeafExit('<No>',text);
	}
});


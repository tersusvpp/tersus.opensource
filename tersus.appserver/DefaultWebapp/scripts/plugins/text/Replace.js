/*********************************************************************************************************
 * Copyright (c) 2003-2011 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
tersus.newAction('Replace', function()
{
	var text = this.getLeafChild('<Text>');
	var replace = this.getLeafChild('<Replace>');
	var by = this.getLeafChild('<By>');
	
	if (by == null)
		by = "";
	
	var i = 0;
	var b = []; //Buffer to collect result fragments
	while (i < text.length)
    {
    	var j = text.indexOf(replace,i);
    	if (j < 0)
    	{
    		b.push(text.substr(i));
    		break;
    	}
    	b.push(text.substr(i,j-i));
    	b.push(by);
    	i = j + replace.length;
    }
 
	this.chargeLeafExit('<Updated Text>',b.join(''));
});
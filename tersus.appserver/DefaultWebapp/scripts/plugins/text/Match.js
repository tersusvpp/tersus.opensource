/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.newAction('Match', function()
{
	var pattern = this.getLeafChild('<Pattern>');
	var text = this.getLeafChild('<Text>');
	var ignoreCase = this.getLeafChild('<Ignore Case>');
	var allowPartial = this.getLeafChild('<Allow Partial Match>');
	var matchGroupsTrigger = this.getElement('<Match Groups>');
	var matchesExit = this.getExit('<Matches>');
	var invalidGroupsExit = this.getElement('<Invalid Groups>');
	
	if (text == null)
		this.chargeEmptyExit('<Missing>');
	else
	{	
		if (allowPartial != true)
		{
			if (pattern.charAt(0) != '^')
				pattern = '^' + pattern;
		
			if (pattern.charAt(pattern.length -1) != '$')
				pattern = pattern + '$';
		}
				
		var modifier = ignoreCase?'i':'';
		
		var pluginVersion = this.pluginVersion ? this.pluginVersion : 0;
		if (pluginVersion > 0)
			modifier += 'g';

		var re = new RegExp(pattern, modifier);
		
		var matches;
		if (pluginVersion == 0)
			matches = text.match(re);
		else
			matches = re.exec(text);
		
		if (matches != null)
		{
			this.chargeLeafExit('<Yes>',text);
			if (matchesExit)
			{
				var l = matches.length;
				var matchGroup = new Array(l);
				var matchGroupsSpecified = false;
				if (matchGroupsTrigger)
				{
					var matchGroups = matchGroupsTrigger.getChildren(this);
					var self=this;
					tersus.foreach(matchGroups, function(g) {
						if (g < l)
						{
							matchGroupsSpecified = matchGroup[g] = true;
						}
						else
							if (invalidGroupsExit)
								self.setLeafChild(invalidGroupsExit.role, g.leafValue, Operation.ADD);
								
					});
				}
				do
				{
					for (var i=0;i<l;i++)
						if (!matchGroupsSpecified || matchGroup[i])
							this.chargeLeafExit(matchesExit, matches[i]);
				} while ((pluginVersion > 0) && ((matches = re.exec(text)) != null));
			}
		}
    	else
    		this.chargeLeafExit('<No>',text);
    		
    }
});
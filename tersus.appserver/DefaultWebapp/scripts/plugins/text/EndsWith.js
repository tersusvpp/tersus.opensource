/*********************************************************************************************************
 * Copyright (c) 2003-2012 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.newAction('EndsWith', function()
{
	var text = this.getLeafChild('<Text>');
	var suffix = this.getLeafChild('<Suffix>');
	if (text != null && suffix != null)
	{
		if (text.length >= suffix.length && text.substring(text.length-suffix.length) == suffix)
			this.chargeLeafExit('<Yes>',text);
		else
			this.chargeLeafExit('<No>',text);
	}
});
 
 
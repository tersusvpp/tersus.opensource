/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.newAction('Trim', function()
{
	var text = this.getLeafChild('<Text>');
    if (text != null)
    {
    	var m = text.replace(/^\s*([\S\s]*?)\s*$/, '$1');
    	if (m && m.length >= 1)
    		this.chargeLeafExit('<Trimmed Text>', m);
	}
});


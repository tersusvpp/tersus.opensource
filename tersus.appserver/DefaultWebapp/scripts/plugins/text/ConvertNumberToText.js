/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.numberFormats = {};
tersus.newAction('ConvertNumberToText', function()
{
	var number = this.getLeafChild('<Number>');
	if (number == null)
		return;
	var formatStr = this.getLeafChild('<Format>');
	if (formatStr == null)
		this.chargeLeafExit ('<Text>',new String(number));
	else
		this.chargeLeafExit ('<Text>',gwtFormatNumber(number,formatStr));
});


/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.newAction('Concatenate', function()
{
	var st = this.triggers.concat([]);
	var values = [];
	st.sort(function(a,b){ if (a.role < b.role) return -1; else if (a.role == b.role) return 0; else return 1;});
	var sep = this.getLeafChild('<Separator>');
	if (sep == null)
		sep = '';
	for (var i=0;i<st.length;i++)
	{
		var t = st[i];
		if (t.role != '<Separator>' && t.modelId != BuiltinModels.NOTHING_ID  )
		{
			if (t.isRepetitive)
			{
				var l =t.getChildren(this);
				if (l)
				{
					for (var j=0;j<l.length; j++)
					{
						var v = l[j];
						if (v!= null && v.leafValue != null)
							values.push(v.leafValue);
					}
				}
						
			}
			else
			{
				var v = t.getChild(this);
				if (v!= null && v.leafValue != null)
					values.push(v.leafValue);
			}
			
		}
	}
	var out = values.join(sep);
	this.chargeLeafExit('<Concatenation>',out);	
});


/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.newAction('GetPosition', function()
{
	var text = this.getLeafChild('<Text>');
	var segment = this.getLeafChild('<Segment>');
	if (text != null && segment != null)
	{
		var position = text.indexOf(segment);
			if (position >= 0)
				this.chargeLeafExit('<Position>',position);
			else
			{
				if (this.getExit('<Not Found>') != null)
					this.chargeLeafExit('<Not Found>', segment);
				else
					modelExecutionError("Segment '" + segment + "' is not contained in '" + 
						text + "' but " + NOT_FOUND + " exit is missing", this);
			}	
	}
});
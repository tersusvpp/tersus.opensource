/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 
//DataNode (of a Flow)
//A DataNode holds a reference to some value, and participates in the flow
// (it can be invoked).
function DataNode() {};
DataNode.prototype = new tersus.Node();
DataNode.prototype.constructor = DataNode;
DataNode.prototype.isDataNode = true;
DataNode.prototype.invoke = function()
{
	if (window.trace)
		window.trace.activated(this);
	this.invokeLinks();
}
DataNode.prototype.toString = function toString()
{
	return this.leafValue + '['+this.modelId +']';
};
DataNode.prototype.copyObject = function copyObject(obj)
{
	this.value.copyObject(obj);
};
DataNode.prototype._getValues = function _getValues(path, index, values)
{
	if (index == path.segments.length)
		values.push(this.value);
	else
		return this.value._getValues(path, index, values);
}
DataNode.prototype.setValue = function setValue(path, value, operation, index)
{
	this.value.setValue(path, value, operation, index);
	//TODO we probably need a special case for leaf data nodes
};
DataNode.prototype.scanChildren = function(filter, alreadyScanned, outputList)
{
// Do nothing - we don't want to scan variables 
//	this.value.scanChildren(filter, alreadyScanned, outputList);
};
DataNode.prototype.getModelId = function()
{
	return this.value.getModelId();
};
/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 
//Path
function Path(pathStr)
{
	if (pathStr !== null && pathStr !== undefined)
	{
		this.str = pathStr;
	 	if (pathStr.length == 0)
			this.segments = [];
		else
			this.segments = pathStr.split('/');
	}
};

Path.prototype.setSegments = function(segments)
{
	this.segments = segments;
	this.str = segments.join('/');
};
Path.prototype.toString = function toString() {return this.str;};
Path.prototype.addSegment = function addSegment(role) 
{
	this.segments.push(role);
	this.str += '/' + role;
};

Path.prototype.getElement = function (parent)
{
	var currentModel = parent;
	var currentElement = null;
	for (var i=0; i<this.segments.length;i++)
	{
		if (currentModel == null)
			return null;
		currentElement = currentModel.getElement(this.segments[i]);
		if (currentElement == null)
			return null;
		if (currentElement.modelId)
			currentModel = tersus.repository.get(currentElement.modelId).prototype;
		else
			currentModel = null;
		
	};
	return currentElement;
};
Path.prototype.clone = function()
{
	var clone = new Path();
	clone.segments = this.segments.concat();
	clone.str = this.str;
	return clone;	
};


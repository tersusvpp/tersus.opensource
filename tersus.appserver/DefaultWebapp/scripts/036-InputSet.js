/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 
function InputSet(parent, subFlow)
{
	this.parent = parent;
	this.subFlow=subFlow;
	this.element=subFlow;
	this.childConstructor = tersus.repository.get(subFlow.modelId);
	this.triggers = this.childConstructor.prototype.triggers;
	this.setStatus(FlowStatus.NOT_READY_TO_START);
	
};
InputSet.prototype = new FlowNode();
InputSet.prototype.constructor = InputSet;
InputSet.prototype.invoke = function ()
{
	this.explode();
};
InputSet.prototype.createInstance = function()
{
	var instance =  this.subFlow.createChild(this.parent);
	instance.setStatus(FlowStatus.NOT_READY_TO_START);
	return instance;
};
InputSet.prototype.explode = function ()
{
	var instances = [];
	instances.push(this.createInstance()); 
	for (var i=0; i<this.triggers.length; i++)
	{
		var trigger = this.triggers[i];
		var values = this.children[trigger.role];
		var nPrototypes = instances.length;
		for (var j=0; j<nPrototypes; j++)
		{
			//TODO handle the case of repetitive triggers (no cartesian product is needed)
			var prototypeInstance = instances[j];
			if (values)
			{
				for (var k=0; k<values.length; k++)
				{
					var value = values[k];
					if (k==0)
					{
						trigger.replaceChild(prototypeInstance, value);
					}
					else
					{
						var newInstance = this.createInstance();
						for (var l=0; l<i; l++)
						{
							// copy values of preceding triggers
							var previousTrigger = this.triggers[l];
							previousTrigger.replaceChild(newInstance, previousTrigger.getChild(prototypeInstance));
						}
						// set current value
						trigger.replaceChild(newInstance, value);
						instances.push(newInstance);
					}
				}
			}	
		}
	}
	for (var i=0; i<instances.length; i++)
		instances[i].checkReady();
};
InputSet.prototype.addValue = function (role, value)
{
	if (! this.children)
		this.children = {};
	if (! this.children[role])
		this.children[role]= [];
	this.children[role].push(value);
};
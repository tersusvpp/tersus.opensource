/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function FlowDataElement(role, modelId)
{
	this.role = role;
	if (modelId)
		this.modelId = modelId;
};
FlowDataElement.prototype = new Element();
FlowDataElement.prototype.isFlowDataElement = true;
FlowDataElement.prototype.constructor = FlowDataElement;
FlowDataElement.prototype.type = 'Data Element';
FlowDataElement.prototype.createChild = function createChild(parent, modelId)
{
	var dataValue = this.createChildInstance(modelId);
	return parent.createReference(this, dataValue);
};
	
FlowDataElement.prototype.replaceChild = function replaceChild(parent, value)
{
	var oldValue;
	if (parent.hasElementChangeHandler)
		oldValue = this.getChild(parent);
	parent.createReference(this, value);
	if (parent.hasElementChangeHandler)
	{
		if (oldValue && oldValue.isDataNode)
			oldValue = oldValue.value;
		if (oldValue != value)
		{
			// handle the case of different leaf instances with same leaf value
			if (oldValue == null || value == null || !oldValue.isLeaf || ! value.isLeaf || (oldValue.leafValue != value.leafValue && ! (oldValue.leafValue != null && oldValue.leafValue.equals && oldValue.leafValue.equals(value.leafValue))))
				parent.handleElementChange(this.role, oldValue, value);
		}
	}
};
FlowDataElement.prototype.addAChild = function addAChild(parent, value)
{
	var reference = parent.createReference(this,value);
    if (parent.hasElementChangeHandler)
    {
        parent.handleElementChange(this.role, null, value);
    }
}

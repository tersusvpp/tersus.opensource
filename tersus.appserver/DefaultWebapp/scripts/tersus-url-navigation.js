/*********************************************************************************************************
 * Copyright (c) 2003-2015 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/

if (window.history.pushState)
{
	tersus.gotoURL = function(url, replace, reload)
	{
		if (url.indexOf('http') < 0 ||  url.indexOf(tersus.rootURL) == 0) // navigation inside current app
		{
			if (replace)
				window.history.replaceState(null,null,url);
			else
				window.history.pushState(null,null,url);
			if (reload)
				tersus.history.locationChanged();
			else
				tersus.loadParams(); // Still need to reload URL parameters
		}
		else
		{
			if (replace)
				window.location.replace(url)
				else
					window.location.href=url;
		}
	}
	tersus.history.locationChanged = function()
	{
		if (tersus.history.fixedURL) /* The 'fixedURL' property is used to disable navigation */
		{
			tersus.gotoURL(tersus.history.fixedURL, false/*replace*/, false/*reload*/);
			return;
		}
		tersus.loadParams(); 
		var o = tersus.getPerspectiveAndViewFromURL();
		if (window.perspective !== o.perspective || window.view !== o.view)
		{
			window.engine.handleEvent(window.rootDisplayNode,Events.ON_LEAVE_VIEW);
			queueEvent(tersus, tersus.reloadView, [], null,null, null, -1);
		}
		else
			tersus.updateView();
	};
	//Disable legacy mechanism for detecting location change
	clearInterval(tersus.history.timer);

	// Register location change on popstate
	$(function() {
		$(window).on('popstate', tersus.history.locationChanged);
	});

	if (tersus.nav != undefined)
		tersus.nav.createURL  = function(perspectiveName, viewName)
		{
			return tersus.rootURL+encodeURIComponent(perspectiveName)+'/'+encodeURIComponent(viewName);
		}

	tersus.setPerspectiveAndView = function()
	{
		var o = tersus.getPerspectiveAndViewFromURL();
		window.view = o.view;
		window.perspective = o.perspective;
	}
	tersus.getPerspectiveAndViewFromURL = function()
	{
		var o = {};
		var parts = location.href.substr(tersus.rootURL.length).split('/'); 
		var perspectiveName = parts[0] ? decodeURIComponent(parts[0]) : null;
		var viewName = parts[1] ? decodeURIComponent(parts[1]) : null;
		o.perspective = getPerspective(perspectiveName);
		if (!o.perspective)
			o.perspective = window.perspectives[0];
		o.view = tersus.getView(o.perspective, viewName);
		if (!o.view)
		{
			var targets = {};
			var views = o.perspective.views;
			targets.defaultView=views[0];
			for (var i=0;i<views.length;i++)
			{
				var v = views[i];
				if (v.name == '<Mobile View>')
					targets.mobileView=v;
				else if (v.name == '<Tablet View>')
					targets.tabletView=v;
				else if (v.name == '<Desktop View>')
					targets.desktopView= v;
			}
			o.view = tersus.selectView(targets);
		}
		return o;
	};

	tersus.getView = function(perspective,name)
	{
		for (var i=0; i< perspective.views.length; i++)
		{
			var v = perspective.views[i];
			if (v.name == name)
			{
				return  v;
			}
		}
		return null;
	};

	tersus.switchView = function(perspective,view,parameters)
	{
		if (!perspective && window.perspectives.length > 1)
			perspective = window.perspective.name;
		var url = tersus.nav.createURL(perspective,view);
		if (parameters)
			url+='?'+parameters;
		tersus.gotoURL(url, false, true);
	};

};

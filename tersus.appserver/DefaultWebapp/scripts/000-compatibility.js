/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 
window.ieLevel = 0;
var userAgent = navigator.userAgent;
window.isWebKit = userAgent.indexOf('WebKit') >= 0;
if (userAgent.indexOf('Safari') >=0)
{
	window.browser = 'Safari';
}
else if (userAgent.indexOf('Gecko') >=0)
{
	window.browser = 'Mozilla';
}
else if (userAgent.indexOf('MSIE') >=0)
{
	window.browser = 'IE';
    if (document.documentMode) 
      ieLevel = document.documentMode;
   else // IE 5-7
   {
      ieLevel = 5; // Assume quirks mode unless proven otherwise
      if (document.compatMode)
      {
         if (document.compatMode == "CSS1Compat") // standards mode
         {
         	if (window.XmlHttpRequest) // Available in IE 7
            	ieLevel = 7; 
            else
            	ieLevel = 6; // IE 6.0
            
         }
      }
   }	
}
else 
{
	window.browser = 'Unknown';
}

function createXMLHTTPRequest()
{
	//TODO: better error reporting when XMLHTTPRequest is not supported
	if (window.XMLHttpRequest)
		return new XMLHttpRequest(); // Works on Mozilla and others
	else
		return new ActiveXObject("Microsoft.XMLHTTP"); // Works on IE
};

function createAnIFrame(parentWindow,name,url)
{
	if (window.browser == 'IE')
	{
		// Creating an iframe in the standard way doesn't work because IE somehow
		// Doesn't recogmize the newly created iframe (submitting a form targeted 
		// To the new iframe causes a new window to be created.
		var styleClause = '';
		if (! window.showServiceInvocations)
		{
			styleClause = 'class="hidden"';
		}
		var srcClause = url ? ' src="'+url+'"' :'';
		var html = '<iframe name="' + name + '" id="'+name+'"'+styleClause+srcClause+'></iframe>';
    	parentWindow.document.body.insertAdjacentHTML( "beforeEnd", html);
    	var iframe = parentWindow.frames[ name ];
    	return iframe;
    }
    else
    {
    	// Standard DOM implementation
    	var iframe = parentWindow.document.createElement('iframe');
		iframe.name = name;
		iframe.id = name;
		iframe.src=url?url:"blank.html";
		if (! window.showServiceInvocations)
			iframe.className = 'hidden';
		var body = parentWindow.document.body;
		body.appendChild(iframe);
		return iframe;
    }
}

function getIFrameWindow(iframe)
{
	if (iframe.contentWindow)
		return iframe.contentWindow;
	else
		return iframe;
};
function getIFrameDocument(iframe)
{
	if (iframe.contentWindow)
		return iframe.contentWindow.document;
	else
		return iframe.document;
};

function setCssFloat(node,value)
{
	if (window.browser == 'IE')
	{
		node.style.styleFloat=value;
	}
	else
	{
		node.style.cssFloat=value;
	};
	
};

function setEncType(form, enctype)
{
	// Althogh 'enctype' and 'encoding' are supposed to be synonyms, 
	// Setting 'enctype' (the DOM standard) doesn't work in IE
	if (window.browser == 'IE')
	{
		form.encoding = enctype;
	}
	else
	{
		form.enctype = enctype;
	};
	
}
function getStackTrace()
{
	var stack = '';
	var e;
	try
	{
		x.x;
	}
	catch(ex)
	{
		e = ex;
	}
	if (e.stack)
	{
		var tmp =  e.stack;
		var frames = tmp.split('\n');
		frames.splice(0,2);
		stack = frames.join('\n');
	}
	else
	{
		for (var func = arguments.caller; func != null; func = func.caller)
		{
			stack += '\n' + getFunctionName(func.callee) + '\n';
		}
	}
	return stack;
  };
  
  // Returns the position of the current event, relative to the window
  // On IE, the event is taken from the window
  // On other browsers, it is supplied as an argument
  function getEventPosition(win,e)
  {
  	var pos = new Position();
	if (window.browser != 'IE')
	{
  		pos.x = e.clientX;
  		pos.y = e.clientY;
  	}
	else
	{
  	 	pos.x = win.event.clientX;
  	 	pos.y = win.event.clientY;
  	 }
  	 return pos;
  };
  function Position() {};
  Position.prototype.equals = function (other)
  {
  	return other.x == this.x && other.y == this.y;
  };
  
  
function setScrollableTable(bodyNode, width, height)
{
	if (window.browser=='Mozilla')
	{
		bodyNode.style.overflow='auto';
		if (width)
			bodyNode.style.width = width;
		if (height)
			bodyNode.style.height = height;
	};
};
  
function isScrollable(node)
{
	return node.scrollHeight != node.offsetHeight || node == node.ownerDocument.body;
};  
function getOffsets(node)
{
	var x = 0;
	var y = 0;
	var currentNode = node;
	do
	{
		x+=currentNode.offsetLeft;
		y+=currentNode.offsetTop;
		currentNode = currentNode.offsetParent;
	}
	while (currentNode);
	return {'left':x, 'top':y};
};
// Returns the position of a node relative to the screen (taking scrolling into account)
function getScreenPosition(node)
{
	var x = 0;
	var y = 0;
	var currentNode = node;
/*	
	//Old calculation of offset relative to the document (doesn't always work correctly).  Currently using jQuery (see below)
	do
	{
		x+=currentNode.offsetLeft;
		y+=currentNode.offsetTop;
		currentNode = currentNode.offsetParent;
	}
	while (currentNode);
	*/
	var offset = $(node).offset();
	var $w = $(getParentWindow(node));
       /* Using jQuery's cross-browser solution to get global scrolling */
	offset.top -= $w.scrollTop();
	offset.left -= $w.scrollLeft();
	
	if (tersus.isIPhoneSafari)
	{
		offset.left -= window.pageXOffset;
		offset.top -= window.pageYOffset;
	}
	return offset;
};

function getScroll(node)
{
	var p = getScreenPosition(node);
	p.top -= node.offsetTop;
	p.left -= node.offsetLeft;
	return p;
};
function contains(node,position)
{
	var offsets = getScreenPosition(node);
	var size = getVisibleSize(node);
	return position.x >= offsets.left && position.y >= offsets.top && position.x <=offsets.left + size.width &&
	position.y <= offsets.top + size.height;
};

function getViewportSize(d)
{
	var n = d.compatMode == 'BackCompat' ? d.body : d.documentElement;
		
	return {'height':n.clientHeight, 'width':n.clientWidth};
}
function getWindowScroll(w)
{
	var scroll = {};
	if (w.pageYOffset != undefined) // For standards - compatible browsers
	{
		scroll.left = w.pageXOffset;
		scroll.top = w.pageYOffset;
	}
	else if (w.document.documentElement && w.document.documentElement.scrollTop) // IE 8 in standards mode
	{
		scroll.left = w.document.documentElement.scrollLeft;
		scroll.top = w.document.documentElement.scrollTop;
	}
	else if (w.document.body) //  IE in quirks mode
	{
		scroll.left = w.document.body.scrollLeft;
		scroll.top = w.document.body.scrollTop;
	}
	return scroll;		
}

function getVisibleSize(node)
{
	var width = node.offsetWidth;
	var height = node.offsetHeight;
	if (node == node.ownerDocument.body)
	{
		if ( node.clientHeight &&  ieLevel<7) // On IE 7 & 8 body.clientHeight is not the visible part of the body
		{
			height = node.clientHeight;
		}
	};
	return {'height':height,'width':width};
};
function getHeight(node)
{
	return node.offsetHeight;
};
function html2Node(t, document)
{
	var p;
	var pref3 = t.substring(0,3);
	if (pref3 == '<tr')
	{
		p = document.createElement('DIV');
		p.innerHTML='<table><tbody>'+t+'</tbody></table>';
		return p.firstChild.tBodies[0].firstChild;
	}
	if (pref3 == '<td' || pref3 == '<th')
	{
		p = document.createElement('DIV');
		p.innerHTML='<table><tbody><tr>'+t+'</tr></tbody></table>';
		return p.firstChild.tBodies[0].firstChild.firstChild;
	}
	else
	{
		p = document.createElement('DIV');
		p.innerHTML=t;
		return p.firstChild;
	}
}
function importNode(document, node)
{
	if (document.importNode && window.browser != 'IE')
		return document.importNode(node,true);
	else if (node.outerHTML)
	{
		var tmpNode = document.createElement('DIV');
		tmpNode.innerHTML = node.outerHTML;
		return tmpNode.firstChild;
	}
	else
	{
		tersus.error('importNode not implemented for this browser');
		return null;
	}
	
};
function getActualBackground(node)
{
	var current = node;
	while (current)
	{
		var background = getActualStyle(current,'background-color');
		if (background != 'transparent' && background != 'rgba(0, 0, 0, 0)') //Chrome has rgba(0, 0, 0, 0)
		{
			 return background;
		}
		current = current.parentNode;
	}
	return null;
};
tersus.getJsAttributeName = function(name)
{
	var parts = name.split('-');
	if (parts.length == 1)
		return parts[0];
	else
	{
		var t = parts[1];
		return parts[0]+t.charAt(0).toUpperCase()+t.substr(1);
	}
}; 

function getActualStyle(node,styleProp)
{
	var y=null;
	if (node.currentStyle)
	{
		var prop = tersus.getJsAttributeName(styleProp);
		if (prop == undefined)
			prop = styleProp;
		y = node.currentStyle[prop];
	}
	else if (window.getComputedStyle)
		y = node.ownerDocument.defaultView.getComputedStyle(node,null).getPropertyValue(styleProp);
	return y;
};
function getMeasure(node, property)
{
	var s = getActualStyle(node,property);
	var m = parseInt(s);
	return isNaN(m) ? 0 : m;
};
getActualStyle.propertyMap = {};
getActualStyle.propertyMap['background-color']='backgroundColor';
getActualStyle.propertyMap['margin-top']='marginTop';
getActualStyle.propertyMap['margin-bottom']='marginBottom';
function isVisible(node)
{
	if (node.tagName && node.tagName == 'HTML')
		return true;
	if (getActualStyle(node,'visibility')=='hidden')
		return false;
	else if (getActualStyle(node,'display') == 'none')
		return false;
	else if (node.parentNode)
		return isVisible(node.parentNode);
	else 
		return true;
};
function getPageSize(w)
{
	var size = {};
	if (w.innerHeight) 
	{
		size.width = w.innerWidth;
		size.height = w.innerHeight;
	}
	else if (w.document.documentElement && w.document.documentElement.clientHeight)
	{
		size.width = w.document.documentElement.clientWidth;
		size.height = w.document.documentElement.clientHeight;
	}
	else if (w.document.body) 
	{
		size.width = w.document.body.clientWidth;
		size.height = w.document.body.clientHeight;
	}
	return size;
};

function getPageScroll(w)
{
	var scroll = {};
	if (w.pageYOffset) 
	{
		scroll.x = w.pageXOffset;
		scroll.y = w.pageYOffset;
	}
	else if (w.document.documentElement && w.document.documentElement.scrollTop)
	{
		scroll.x = w.document.documentElement.scrollLeft;
		scroll.y = w.document.documentElement.scrollTop;
	}
	else if (w.document.body) // all other Explorers
	{
		scroll.x = w.document.body.scrollLeft;
		scroll.y = w.document.body.scrollTop;
	}
	return scroll;
};

function getParentWindow(node)
{
	if (node.parentWindow)
		return node.parentWindow;
	else if (node.defaultView)
		return node.defaultView;
	else if (node.ownerDocument)
		return getParentWindow(node.ownerDocument);
};

function clearSelection(w)
{
	if (w.document.selection && w.document.selection.empty)
		w.document.selection.empty();
	else if (w.getSelection && w.getSelection().removeAllRanges)
		w.getSelection().removeAllRanges();
};

function getEventAttributes(e)
{
	var atts = {};
	atts.type = e.type;
	atts.files = e.originalEvent && e.originalEvent.dataTransfer && e.originalEvent.dataTransfer.files;
	if (e.type.substring(0,3)=='key')
	{
		if (e.keyCode)
			atts.keyCode = e.keyCode;
		else 
			atts.keyCode = e.which;
	}
	atts.ctrlKey = e.ctrlKey;
	atts.shiftKey = e.shiftKey;
    atts.altKey = e.altKey;
    atts.metaKey = e.metaKey;
	var buttonCode = e.button;
	if (this.browser=='Mozilla' || this.browser=='Safari')
	{
		if (buttonCode == 0)
			atts.button = 'Left';
		else if (buttonCode == 1)
			atts.button = 'Middle';
		else if (buttonCode == 2)
			atts.button = 'Right';
	}
	else
	{
		if (buttonCode == 1)
			atts.button = 'Left';
		else if (buttonCode == 4)
			atts.button = 'Middle';
		else if (buttonCode == 2)
			atts.button = 'Right';
	}
	atts.clientX  = e.clientX;
	atts.clientY = e.clientY;
	atts.pageX  = e.pageX;
	atts.pageY = e.pageY;
	
	atts.targetElement = e.target ? e.target : e.srcElement;
	return atts;
};
// Javascript escaping/quoting: based on http://www.json.org/json2.js (public domain)
js_escapeable = /[\\\'\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
	js_meta = {    // table of character substitutions
                '\b': '\\b',
                '\t': '\\t',
                '\n': '\\n',
                '\f': '\\f',
                '\r': '\\r',
                "'" : "\\'",
                '\\': '\\\\'
            };
function js_quote(string) {
js_escapeable.lastIndex = 0;
return js_escapeable.test(string) ?
    '"' + string.replace(js_escapeable, function (a) {
        var c = js_meta[a];
        if (typeof c === 'string') {
            return c;
        }
        return '\\u' + ('0000' +
                (+(a.charCodeAt(0))).toString(16)).slice(-4);
    }) + '"' :
    '"' + string + '"';
}
function is_touch_device() {
  return !!('ontouchstart' in window) ? 1 : 0;
}

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 
function ConstructorRepository()
{
	this.constructors = {};
	this.htmlTemplates = {};
	this.preloaded = {};
	this.loadedPackages = [];
	this.packageTexts = {};
	this.parsedPackages = {};
};
ConstructorRepository.prototype.add = function add_prototype(c)
{
	this.constructors[c.prototype.modelId]=c;
};

ConstructorRepository.prototype.get = function get(id)
{
	if (!this.constructors[id])
	{
		this.parsePackage(tersus.getPackageId(id));
	}
	if (! this.constructors[id])
	{
		if (tlog) tlog('Not preloaded:'+id);
		this.loadPackages([tersus.getPackageId(id)]);
		this.parsePackage(tersus.getPackageId(id));
	}
	var c =  this.constructors[id];
	return c;
};
ConstructorRepository.prototype.isLoaded = function(modelId)
{
	this.parsePackage(tersus.getPackageId(modelId));
	// Check again - maybe the package was parsed and now the constructor is there
	return (this.constructors[modelId]!=undefined);
};
ConstructorRepository.prototype.parsePackage = function(packageId)
{
	if (this.packageTexts[packageId] && !this.parsedPackages[packageId])
	{
		var t0 = new Date();
		this.parsedPackages[packageId] = true;
		var text = this.packageTexts[packageId];
		var models = eval(text);
		var t1 = new Date();
		for (var i=0;i<models.length;i++)
		{
			this.loadModel(packageId, models[i]);
		}
		var t2 = new Date();
		if (tlog)
			tlog('LOADER: ' + packageId+ ': '+models.length+' models loaded in '+(t2-t0) + ' ms ( eval time='+(t1-t0) + ' ms)');
	}
	
};

ConstructorRepository.prototype.loadModel = function (packageId, m)
{
	var c = function(){this.init();};
	var p = c.prototype = eval ('new '+m.c + '()');
	p.constructor = c;
	p.modelId = packageId + '/'+m.n;
	p.modelName = m.n;
	p.plugin = m.p;
	if (p.initModel)
		p.initModel();
	if (m.sp != null)
		p.sharedProperties = m.sp;
	if (m.pv != null)
		p.pluginVersion = m.pv;
	if (m.cc != null)
		p.cancellable = m.cc;
	if (m.sc != null)
		p.secure = m.sc;
	if (m.runImmediately != null)
		p.runImmediately=m.runImmediately;
	if (m.eventHandling != null)
		 p.eventHandling=m.eventHandling;
	if (m.serviceTimeout != null)
		 p.serviceTimeout=m.serviceTimeout;
	if (m.pm != null) p.progressMessage = m.pm;
	if (m.xmlNS != null)
		p.xmlNamespace = m.xmlNS;
	if (m.format != null)
		p.format = m.format;
	if (m.formatAsGMT != null)
		p.formatAsGMT = m.formatAsGMT;
	if (m.progressMessage != null) p.progressMessage = m.progressMessage;
	if (m.v != null)
	{
		try
		{
			p.parseString(m.v);
		}
		catch (e)
		{
			throw {message:'Invalid value/format for '+p.modelId + ' details='+e.message};
		}
	}
	if (m.tableName)
		p.tableName=m.tableName;
	if (m.e)
	{
		for (var i=0;i<m.e.length;i++)
		{
			var me = m.e[i];
			var e = null;
			switch (me.t)
			{
				case 'sf':
				  e = p.addSubFlow(me.r,me.ref);
				  break;
				case 'l':
			      e = p.addLink(me.r, me.src, me.tgt);
			      break;
			    case 'ctx':
			      e = p.addAncestorReference(me.r,me.ref);
			      break;
			    case 'const':
			      e = p.addConstant(me.r,me.ref);
			      break;
			    case 'var':
			      e = p.addIntermediateDataElement(me.r,me.ref);
			      break;
			    case 'da':
			      e = p.addDataElement(me.r,me.ref);
			      break;
			    case 'di':
			      e = p.addDisplayElement(me.r,me.ref,me.ep);
			      break;
			    case 'tr':
			      e = p.addTrigger(me.r, me.ref);
			      break;
			    case 'ex':
			      e = p.addExit(me.r,me.ref);
			      break;
			    case 'er':
			      e = p.addErrorExit(me.r,me.ref);
			      break;
			    default:
			      throw new tersus.Exception('Invalid Model ('+p.modelId+'): unknown element type "'+me.t+'"');
			      
			}
			if (e)
			{
				if (me.rep != null)	e.isRepetitive = me.rep;
				if (me.rk != null)	e.dependencyRank = me.rk;
				if (me.m != null)	e.mandatory = me.m;
				if (me.ac != null)	e.alwaysCreate = me.ac;
				if (me.v != null)	e.valueStr = me.v;
				if (me.ct != null)	e.isConstantTrigger = me.ct;
				if (me.op != null)	e.operation = me.op;
				if (me.sr != null)	e.isSelfReady = me.sr;
				if (me.primaryKey != null) e.isPrimaryKey = me.primaryKey;
				if (me.columnName != null) e.columnName = me.columnName;
				if (me.xmlNS != null) e.xmlNamespace = me.xmlNS;
				if (me.format != null) e.format = me.format;
				if (me.formatAsGMT != null) e.formatAsGMT = me.formatAsGMT;
			}
		}
	}
	this.add(c);
	if (p.onload)
		p.onload();
	
	
};
ConstructorRepository.prototype.checkLoaded = function(modelId)
{
	if (this.isLoaded(modelId))
		return true;
	for (var i=0;i<this.loadedPackages.length;i++)
	{
		var packageId = this.loadedPackages[i];
		if (modelId.substring(0,packageId.length) == packageId
			&& modelId.charAt(packageId.length) == '/')
		{
			modelError("Missing model "+modelId + "\n(pakcage "+packageId + " already loaded)");
			break;
		}
	}
	return false;
		
}

var MAX_URL_LENGTH=1024;
tersus.packageLoader = function(){};
tersus.packageLoader.prototype.onload = function(packages)
{
	tersus[this.scriptCallback] = null; //Drop reference to allow garbage collection
	for (var id in packages)
	{
		tersus.repository.packageTexts[id] = packages[id];
	}
	this.runAsync();
};
tersus.packageLoader.error = function(msg)
{
	tersus.error('An error has occurred while loading the application:\n'+msg);
};
tersus.nextPackageLoaderId = 1;
tersus.packageLoader.prototype.runAsync = function runAsync()
{
	var userName = tersus.getUserName();
	var loader=this;
	if (loader.currentIndex == loader.packageIds.length)
	{
		setTimeout(loader.callback,0);
		return;
	}
	if (window.localStorage && tersus.useLocalStorage) // new mechanism - packages stored in local storage 
	{
		var url = tersus.rootURL+'Code';
        var paramString='packages=';
        while (this.currentIndex<this.packageIds.length)
        {
        	paramString += encodeURIComponent(':'+loader.packageIds[this.currentIndex]);
        	++ this.currentIndex;
        }
        var tsKey = tersus.rootURL+':LAST_TIMESTAMP';
        var storedTimestamp = localStorage.getItem(tsKey);
        var timestamp = Math.max(tersus.lastTimestamp, storedTimestamp);
        var storageKey=tersus.rootURL+':Code?timestamp='+timestamp+'&user='+encodeURIComponent(userName)+'&'+paramString;
        if (timestamp == storedTimestamp)
        {
        	var text = localStorage.getItem(storageKey);
        	if (text != null)
        	{
        		var packages = eval(text);
        		loader.onload(packages);
        		if (tlog) tlog('Loaded from local Storage');
       			return; 	
        	}
        }
        else
        {
        	for (var i=localStorage.length -1; i>=0 ;i--)
        	{
        		var key=localStorage.key(i);
        		if (key.search(/^(Code|File)\?/) >= 0)
        		{
	       			localStorage.removeItem(key);
	       			if (tlog) tlog('Removing '+key + ' from local storage');
	       		}
        	}
        	localStorage.removeItem(tsKey);
        	
        }
 		var req = createXMLHTTPRequest();
        req.onreadystatechange= function(){
        	if (req.readyState ==4)
        	{
        		if (req.status==200)
        		{
        			var text = req.responseText;
	        		var packages=eval(text);
	        		try
	        		{
	        			localStorage.setItem(storageKey, text);
	        			localStorage.setItem(tsKey,timestamp);
	        		}
	        		catch (e)
	        		{
	        			if (localStorage.length > 0)
	        			{
	        				localStorage.clear();
	        				tersus.reloadApplication();
	        				return;
	        			}
	        			else 
	        			{
	        				tersus.error("Can't load application - local storage error:\n"+e);
	        				return;
	        			}
	        			
	        		}
	        		loader.onload(packages);
	        		if (tlog) tlog('Loaded from server');
	        	}
	        	else
	        	{
	        		tersus.error("Failed to load models from server");
	        	}
	        }
	        	
        };
        req.open('POST', url, true);
        req.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
        req.send(paramString);
		return;
	}
	else // old mode
	{
		loader.scriptCallback = 'loadPackages'+(tersus.nextPackageLoaderId ++);
		tersus[loader.scriptCallback] = function(constructors) { loader.onload(constructors);};
		var url = tersus.rootURL+'Code?timestamp='+tersus.lastTimestamp+'&_js_callback='+'tersus.'+loader.scriptCallback+'&user='+encodeURIComponent(userName)+'&packages=';
			
		// We may need to split the request into multiple requests if the url ends up too long
		for (var nextIndex=loader.currentIndex;nextIndex<loader.packageIds.length;nextIndex++)
		{
			var nextURL = url+encodeURIComponent(':'+loader.packageIds[nextIndex]);
			if (nextURL.length > MAX_URL_LENGTH)
				break;
			else
				url = nextURL;
		}		
		if (nextIndex==loader.currentIndex)
		{
			modelError("Package id "+loader.packageIds[nextIndex] + " is too long to load");
			return;
		}
		if (tersus.progress.active)
			tersus.progress.set("Loading application components");
		loader.currentIndex = nextIndex;
		tersus.loadScript(url);
	}
};
tersus.packageLoader.prototype.runSync = function runSync()
{
	var loader = this;
	var url = tersus.rootURL+'Code?timestamp='+tersus.lastTimestamp+'&user='+encodeURIComponent(tersus.getUserName())+'&packages=';
	
	// We work hard to retrieve packages using 'GET', as this will enable caching
	// This means we sometimes have to split the list into multiple requests
	if (tersus.progress.active)
		tersus.progress.set("Loading application components");
	while (loader.currentIndex < loader.packageIds.length)
	{
		for (var nextIndex=loader.currentIndex;nextIndex<loader.packageIds.length;nextIndex++)
		{
			var nextURL = url+encodeURIComponent(':'+loader.packageIds[nextIndex]);
			if (nextURL.length > MAX_URL_LENGTH)
				break;
			else
				url = nextURL;
		}		
		if (nextIndex==loader.currentIndex)
		{
			modelError("Package id "+loader.packageIds[nextIndex] + " is too long to load");
			return;
		}
		loader.currentIndex = nextIndex;
		var jsText = tersus.loadURL(url);
		if (jsText)
		{
			var packages = eval(jsText);	
			for (var id in packages)
			{
				tersus.repository.packageTexts[id] = packages[id];
			}
		}
	}
};
ConstructorRepository.prototype.loadPackages = function loadPackages(packageIds, callback) 
{
	if (tlog) tlog('Loading packages: '+packageIds.join(';'));
	for (var i=0;i<packageIds.length;i++)
	{
		this.loadedPackages.push(packageIds[i]);
	}

	var loader = new tersus.packageLoader();
	
	loader.packageIds = packageIds;
	loader.callback = callback;
	loader.currentIndex = 0;
	if (loader.callback)
		loader.runAsync();
	else
		loader.runSync();
};


ConstructorRepository.prototype.content = function content()
{
	var content = null;
	for (var id in this.constructors)
	{
		if (! content)
			content = id;
		else
			content = content + '\n'+id;
	}
	return content;
};
ConstructorRepository.prototype.templateNumber = 0;

//TODO - change loading mechanism so that html snippets are loaded with model
tersus.preloadedTemplates = {};
tersus.preloadTemplate = function(name,text)
{
	tersus.preloadedTemplates[name]=text;
};
ConstructorRepository.prototype.extractTemplate = function(path,html)
{
	var $t0=$(html);
	var $t = $t0.filter('#content').add($t0.find('#content')); // content cound be top level or lower level
	if (! $t[0]) // Create a container for all content nodes of the template
	{
		var $t = $('<div>');
		$t.append($t0.filter('*').not('title,script,meta'));//filter('*') excludes text nodes
	}
	this.saveTemplate(path, $t[0]);
	return $t[0];
};
ConstructorRepository.prototype.loadHTMLTemplate = function (path)
{
	if (this.htmlTemplates[path])
		return; //Already loaded
	if (tersus.preloadedTemplates[path] != null)
	{
		var text = tersus.preloadedTemplates[path];
		var template = document.createElement('DIV');
		template.innerHTML = text;
		this.saveTemplate(path, template);
		return;
	}
	if (tersus.progress.active)
	{
		var message;
		if (tersus.progressMessage)
			message = tersus.progressMessage;
		else
			message = 'Loading template '+path + '...';
		tersus.progress.set(message);
	}
	var html = tersus.loadFile(path);
	
	this.extractTemplate(path,html);
	
	/*
	
	// Old mechanism (no jQuery)
	if (tersus.progress.active)
		tersus.progress.set('Template loaded.');
	var name = 'templateFrame_'+ (++this.templateNumber);
	var iframe = window.createAnIFrame(window, name);
	var iframeNode = window.document.getElementById(name);
	var repository=this;
	var count=0;
	var cont1 = function()
	{
		++count;
		var d = iframeNode.contentDocument;
		var w = iframeNode.contentWindow;
		if ( d == null && w == null)
		{
			if (count < 50)
				setTimeout(cont1,50);
			else
				tersus.error('Failed to load template '+path);
			return;
		}
		if (d == null)
		{
			d = iframeNode.contentWindow.document;
		}
		d.write(html);
		d.close();
		var cont2 = function() 
		{
			var templateNode = d.getElementById('content');
			if (!templateNode)
				templateNode = d.body;
			templateNode.id='';
			var template;
			if (templateNode.tagName == 'BODY')
			{
				template = document.createElement('DIV');
				template.innerHTML = templateNode.innerHTML;
			}
			else
			{
			  template =  importNode(window.document,templateNode);
			}
			repository.processTemplate(template);
			iframeNode.parentNode.removeChild(iframeNode);
			repository.htmlTemplates[path]=template;
		};
		setTimeout(cont2,50);
	}
	setTimeout(cont1,50);
	*/
};
ConstructorRepository.prototype.saveTemplate = function saveTemplate(path,template)
{
	this.processTemplate(template);
	this.htmlTemplates[path]=template;
};
//TODO move this mechanism to "Embedded HTML" so that it isn't loaded unnecessarity
ConstructorRepository.prototype.getHTMLTemplate = function (path)
{
	var template = this.htmlTemplates[path];
	if (!template)
	{
		var wasInProgress = tersus.progress.active;
		var progressMessage;
		if (tersus.progressMessage)
			message = tersus.progressMessage;
		else
			message = 'Loading template '+path + '...';
		
		tersus.progress.set(message);
		var html = tersus.loadFile(path);
		if (wasInProgress)
			tersus.progress.set('Template loaded.');
		else
			tersus.progress.clear();
			
		/*
		// Old mechanism
		
		var name = 'templateFrame';
		var iframe = window.createAnIFrame(window, name);
		var iframeNode = window.document.getElementById(name);
		var d = iframeNode.contentWindow.document;
		d.write(html);
		d.close();
		var templateNode = d.getElementById('content');
		if (!templateNode)
			templateNode = d.body;
		var template;
		if (templateNode.tagName == 'BODY')
		{
			template = document.createElement('DIV');
			template.innerHTML = templateNode.innerHTML;
		}
		else
		{
		  template =  importNode(window.document,templateNode);
		}
		this.processTemplate(template);
		iframeNode.parentNode.removeChild(iframeNode);
		this.htmlTemplates[path]=template;
		*/
		
		var template = this.extractTemplate(path,html);
	}
	return template;
};
/**
 * Perform the following processing for a template:
 *
 * 1) Remove sample nodes
 * 2) Hide repetitive template nodes
*/

ConstructorRepository.prototype.processTemplate = function(template)
{
	var $t=$(template);
	$t.attr('id','');
	$t.find("[repetitive=sample]").remove();
	$t.find("[repetitive=true]").css('display','none');

/*
	var elements = template.getElementsByTagName('*');
	for (var i=0; i<elements.length; i++)
	{
		var e = elements[i];
		var repetitive  = e.getAttribute('repetitive');
		if (repetitive == 'sample')
		{
			if (e.parentNode)
				e.parentNode.removeChild(e);
		}
		else if (repetitive == true)
			e.style.display='none';
	}
	*/
};

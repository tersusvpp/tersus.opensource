(function($) {
    function isResizeLeft(e, $th) {
        return e.pageX <= $th.offset().left + 5;
    };
    function isResizeRight(e, $th) {
        return e.pageX >= $th.offset().left + $th.outerWidth() - 5;
    };
    function isResize(e, $th) {
        return isResizeLeft(e, $th) || isResizeRight(e, $th);
    };
    function scrollbarWidth() {
        var outer = $(
            '<div style="position:absolute;top:-200px;left:-200px;width:100px;height:100px;overflow:hidden;border:1px solid black;"/>')
            .appendTo(document.body);
        var inner = $('<div style="height:300px;border:0px;padding:0px;"/>')
            .appendTo(outer);
        var fullWidth = inner.width();
        outer.css('overflow-y', 'scroll');
        var w = fullWidth - inner.width();
        if (w == 0)
            w = fullWidth - outer[0].clientWidth;
        outer.remove();
        return w;
    };
    function move($list, from, to, nchildren) {
        $list.each(function() {
            var parent = this;
            var children = parent.children;
	    if (children.length != nchildren)
		return true;
            var childFrom = children[from];
            var childTo = null;
            if (to < children.length) {
                childTo = children[to];
            }
            parent.insertBefore(childFrom, childTo);
        });
    };
    function permute(p, from, to)
    {
	var e = p.splice(from,1)[0];
	if (from < to)
	    --to;
	p.splice(to,0,e);
    };


    $.widget(
        'tersus.datagrid',
        {
	    
            options : {
                scrollable : false,
                resizableColumns : true,
                movableColumns : true,
                sortable : null
            }

            ,
            _create : function() {
                this.setWindow();
                this.setHeaderRow();
                if (this.options.scrollable)
                    this.initScrollable();
                else
                    this.initNonScrollable();
// Commented out to enable dynamic column order in the non-movable case, which might affect performance
//		if (this.options.movableColumns)
		    this.trackContent();
            }

            ,
            destroy : function() {
                $.Widget.prototype.destroy.call(this);
            }

            ,
            _setOption : function(key, value) {
                switch (key) {
                case "myoption":
                    // handle changes to option
                    break;
                }
                $.Widget.prototype._setOption
                    .apply(this, arguments);
            }

            ,
            initScrollable : function() {
                var self = this;
//                this.element.css('width', 'auto');
                if (this.element.is('table')) {
                    this.bodyTable = this.element;
                    this.body = this.bodyTable.wrap('<div class="body">').parent();
                    this.body.wrap('<div class="datagrid">');
                    this.top = this.body.parent();
		    this.top.css('height','100%');
                } 
		else
		{
                    this.top = this.element;
		    this.bodyTable = this.top.children('table'); 
		    if (this.bodyTable.length != 1)
			throw ('Data Grid container should contain exactly one table');
                    this.body = this.bodyTable.wrap('<div class="body">').parent();
	        }
		this.top.addClass('scrollable');
		/* create a header DIV */
                this.head = $('<div class="head"/>').prependTo(this.top);
		/* create a header TABLE with same class as original table (this may copy styling attributes, but will not always work ..)*/
                this.headTable = $('<table>').appendTo(this.head).addClass(this.bodyTable.attr('class'));
		this.top.addClass('datagrid');
                this.body.width(this.top.innerWidth());
		self.top[0].offsetTop;
               this.measureDefaultWidths();
                this.bodyTable.children('caption,thead').appendTo(
                    this.headTable);
		this.tables = this.headTable.add(this.bodyTable);
		/* Create a footer DIV and TABLE if TFOOT elements are present */
		var $tfoot = this.bodyTable.children('tfoot');
                var sw = scrollbarWidth();
		if ($tfoot.length > 0)
		{
                    this.footer = $('<div class="footer"/>').css('bottom',sw).appendTo(this.top);
                    this.footerTable = $('<table>').appendTo(this.footer).addClass(this.bodyTable.attr('class'));
		    $tfoot.appendTo(this.footerTable);
		    this.tables=this.tables.add(this.footerTable);
		}
                this.initCols();
                // If needed - move thead to 'head' div
                this.head.width(this.top.innerWidth() - sw);
                if(this.footer)
		    this.footer.width(this.top.innerWidth() - sw);
                this.body.width(this.top.innerWidth());
		self.top[0].offsetTop;
                this.setColWidths(true);
                this.body.on('scroll', function(e) {
                    self.head.scrollLeft(self.body.scrollLeft());
                    if (self.footer) 
			self.footer.scrollLeft(self.body.scrollLeft());
                });
                this.applyLayout();
            },
            applyLayout : function() {
		var self = this;
		if (self.layoutRequested)
		    return;
		self.layoutRequested=true;
                setTimeout(function() {
		    self.layoutRequested=false;
                    self.tables.show();
		    if (self.head && self.body) /*scrollable*/
		    {
			self.top[0].offsetTop;
			self.body.css('top',self.head.outerHeight());
			if (self.footer)
			{
			    self.body.css('padding-bottom',self.footer.outerHeight()-parseInt(self.bodyTable.css('border-bottom-width')));
			}
		    }
                }, 2);
		
            },
	    trackContent: function()
	    {
		var self = this;
		this.top.find('tr').data('tracked',true);
		this.top.on('DOMSubtreeModified.datagrid',function() {
//		    console.log('DOMSubtreeModified');
		    self.top.find('tr').each(function(index,row) {
			setTimeout(function() {
			    if (! $(row).data('tracked'))
			    {
				if (self.applyPermutationAndVisibility(row))
				{
				    $(row).data('tracked',true);
//				    console.log('New row tracked');
				}
			    }
			},0);
			self.applyLayout();
		    });
			
		});
			       
	    },
            setHeaderRow : function() {
                var self = this;

                this.headerRow = this.element.find('thead>tr')
                    .first();

                if (this.options.resizableColumns
                    || this.options.movableColumns) {
                    this.headerRow
                        .children('th')
                        .on(
                            {
                                'mouseover mousemove' : function(
                                    e) {
                                    if (self.options.resizableColumns
                                        && isResize(e,
                                                    $(this))) {
                                        $(this).addClass('resize');
                                    } else
                                        $(this).removeClass('resize');
                                },
                                'mousedown' : function(e) {
                                    self.mouseDown(e,$(this));
                                },
                                'mouseout' : function(e) {
                                    $(this).removeClass('resize');
                                }
                            });
                }

                var sortable = this.options.sortable;
                if (sortable) {
                    this.headerRow
                        .children('th')
                        .on(
                            'click',
                            function(e) {
                                self.clearSelection();
                                var c = $(this).hasClass('up') ? 'down'
                                    : $(this).hasClass('down') ? null : 'up';
//                                console.log('sort:' + c);
                                // 'Default' - reset all cells
                                // 'multi' - reset just this cell
                                (sortable != 'multi' ? self.headerRow.children('th') : $(this)).removeClass('up down');
                                if (c)
                                    $(this).addClass(c);
				self._trigger('sort');
                            });
                }

            },
            measureDefaultWidths : function() {
		this.defaultTableWidth = this.bodyTable.outerWidth();
                this.headerRow.children('th').each(
                    function() {
                        $(this).data('default-width',
                                     $(this).outerWidth());
                    });
            }

            ,
            initCols : function() {
                this.tables.prepend('<colgroup>');
                this.colgroups = this.tables.children('colgroup');
                var self = this;
                this.headerRow.children('th').each(
                    function() {
                        var $th = $(this);
                        self.colgroups.append('<col>');
                        $th.data('col', self.colgroups
                                 .children('col:last-child'));
                    });
            },
            initPerm : function() {
                this.perm=[];
                for (var i=0; i< this.headerRow.children('th').length;i++)
		{
		    this.perm.push(i);
		}
            },
            initNonScrollable : function() {
                // Non-scrollable currently requires not having a
                // wrapper div
		this.top = this.element;
		this.tables = this.element.is('table') ? this.element : this.element.children('table');
		this.top.addClass('datagrid');

                if (this.options.resizableColumns
                    || this.options.movableColumns) {
                    this.initCols();
                    this.setColWidths();
                    this.applyLayout();
                }
            },
            setWindow : function() {
                var d = this.element[0].ownerDocument;
                this.window = d.defaultView || d.parentWindow;
            },
            clearSelection : function() {
                var w = this.window;
                if (w.document.selection
                    && w.document.selection.empty)
                    w.document.selection.empty();
                else if (w.getSelection
                         && w.getSelection().removeAllRanges)
                    w.getSelection().removeAllRanges();
            },
            mouseOver : function(e, $th) {
                if (isResize(e, $th)) {
                    $th.addClass('resize');
                }
            },
            mouseOut : function() {
                $th.removeClass('resize');
            },
            mouseDown : function(e, $th) {
                this.clearSelection();
                var o = $th.offset();
                // console.log('pageX="+e.pageX+ ' pageY='+e.pageY +
                // ' offsetLeft='+o.left+ ' offsetTop='+o.top+ '
                // width='+$th.outerWidth());

                if (this.options.resizableColumns
                    && isResizeLeft(e, $th)) {
                    var $prev = $th.prev();
			    while ($prev.css('display')=='none')
				$prev=$prev.prev();
                if ($prev.length) {
//                        console.log('Resize left');
                        this.beginResize(e, $prev);
                    }
                } else if (this.options.resizableColumns
                           && isResizeRight(e, $th)) {
//                    console.log('Resize right');
                    this.beginResize(e, $th);
                } else if (this.options.movableColumns) {
//                    console.log('move');
                    this.beginMove(e, $th);
                }
            },
            beginMove : function(e, $th) {
                var self = this;
                var doc = $th[0].ownerDocument;

                this.move = {
                    $feedback : $('<div class="drag-feedback">'),
                    $th : $th,
                    $col : $th.data('col'),
                    startX : e.pageX,
                    cancel : function() {
                        $(doc).off('.datagrid');
                        self.move.$feedback.remove();
                        delete self.move;
                    }
                }
                var offset = $th.offset();
                this.move.$feedback.html($th.html()).css({
                    position : 'absolute',
                    top : offset.top,
                    left : offset.left,
                    boxSizing : 'border-box',
                    width : $th.outerWidth(),
                    height : $th.outerHeight()
                });

                $(doc).on('mousemove.datagrid', function(e) {
                    self.moveFeedback(e);
                }).one('mouseup.datagrid', function(e) {
                    self.finishMove(e);
                });
            },
            moveFeedback : function(e) {
                this.clearSelection();
                var $feedback = this.move.$feedback;
                if ($feedback.parent().length == 0)
                    $feedback.appendTo(this.window.document.body);

                var offset = this.top.offset();
                var y = e.pageY, top = offset.top, left = offset.left, w = this.element
                    .outerWidth(), h = this.element
                    .outerHeight(), x = e.pageX
                    - this.move.startX
                    + this.move.$th.offset().left;
/*                console.log('y=' + y + ' top=' + top + ' left='
                            + left + ' w=' + w + ' h=' + h + ' x=' + x);*/
                if (y < top || y > top + h) {
                    this.move.cancel();
                    return;
                }
                $feedback.css('left', x);
            },
	    applyPermutationAndVisibility: function(e)
	    {
		var $e=$(e);
		if (!this.perm && !this.visibilityV)
		    return true; /* Nothing to do*/
		if (this.perm.length != $e.children().length)
		    return false;
		// TODO // Validate number of elements
		var reordered = [];
//		console.log('Permuting row');
		var list = $e.children();
		var p = this.perm;
		var v = this.visibilityV;
		if (this.perm)
		    list.each(function(i) {$e.append(list[p[i]]);});
		if (v)
		{
		    list.each(function(i) { 
			if (!v[i]) // If we need special logic for "nohide" it can be added here
			    $(this).hide();
		    });
		}
		return true;
		
	    },
            finishMove : function(e) {
                if (!this.move)
                    return;

                var $th = this.move.$th, startX = this.move.startX, currentX = e.pageX, x = currentX
                    - startX + $th.offset().left;

                this.move.cancel();

                var fromIndex = -1, toIndex = -1;
                if (currentX != startX) {
                    this.headerRow
                        .children('th')
                        .each(
                            function(i) {
                                if (this === $th[0]) {
                                    fromIndex = i;
                                    return;
                                }
                                var $cell = $(this);
                                if (!$cell.is(':visible'))
                                    return;
                                var left = $cell.offset().left, width = $cell
                                    .outerWidth();
                                if (x >= left && x < left + width) {
                                    if (x > left + width/2) {
                                        toIndex = i + 1;
                                    } else
                                        toIndex = i;
                                }
                            });
                    if (toIndex != -1 && toIndex != fromIndex
                        && toIndex != fromIndex + 1) {
                        // Check original column order and sort
                        // order
                        this.moveCells(fromIndex, toIndex);
			this.updateColGroups();
			this._trigger('movecolumn');
//			console.log('New permutation:'+JSON.stringify(this.perm));
                    }
                }

            },
            moveCells : function(fromIndex, toIndex) {
                // Todo exclude rows with a wrong number of elements
		if (!this.perm)
		    this.initPerm();
		this.reorderColGroups(fromIndex,toIndex);
                move(this.tables.children(
                    'thead,tbody,tfoot').children('tr'), fromIndex,
                     toIndex, this.perm.length);
		permute(this.perm,fromIndex,toIndex);
            },
	    reorderColGroups:function(fromIndex, toIndex)
	    {
		var from = fromIndex;
		var to = toIndex;
		// Since there are no 'col' elements for hidden columns, we need to correct the indices
		var cells=this.headerRow.children();
		for (var i=0; i<cells.length;i++)
		{
		    if (cells[i].style.display == 'none')
		    {
		    if (i<=fromIndex)
			--from;
			if (i<=toIndex)
			    --to;
		    }
		}
		move(this.colgroups,from,to);
	    },
            beginResize : function(e, $th) {
                this.setColWidths();
                this.element.addClass('ter-resize');
                this.resize = {
                    $col : $th.data('col'),
                    startX : e.pageX,
                    initialColWidth : $th.outerWidth(),
                    initialTableWidth : $th.closest('table')
                        .outerWidth()
                };
//		console.log('resizing:'+$th.text());
                var doc = $th[0].ownerDocument;
                var self = this;
                $(doc).on('mousemove.datagrid', function(e) {
                    self.doResize(e);
                });
                $(doc).one('mouseup.datagrid', function(e) {
                    $(doc).off('mousemove.datagrid');
                    self.resize = {};
                    self.element.removeClass('ter-resize');
                });

            },
            setColWidths : function(useDefaultWidths) {
                this.tables.width(useDefaultWidths ? this.defaultTableWidth : this.tables.eq(0).outerWidth());
                this.headerRow.children().each(
                    function() {
                        var $th = $(this);
                        var $col = $th.data('col');
                        if ($th.is(':visible')) {
                            $col.width(useDefaultWidths ? $th
                                       .data('default-width')
                                       : $th.outerWidth());
                        }
                    });

		this.tables.css('tableLayout','fixed');
            },
            doResize : function(e) {
                this.clearSelection();
                var $col = this.resize.$col;
                var deltaX = e.pageX - this.resize.startX;
                var colWidth = this.resize.initialColWidth + deltaX;
                var tableWidth = this.resize.initialTableWidth
                    + deltaX;
                var self = this;
                setTimeout(function() {
                    $col.width(colWidth);
                    self.tables.css({
                        'box-sizing' : 'border-box',
                        'width' : tableWidth
                    });
		    self.applyLayout();
                }, 0);
            },
	    appendRow : function(target,row)
	    {
		this.tables.children(target).eq(0).append(row);
	    },
	    permutation : function (newPerm) /* vector of currently visible columns */
	    {
		if (!this.perm)
		    this.initPerm();
		if (!newPerm)
		{
		    return this.perm.slice(0); // Create a copy of the array
		}
		var n = this.perm.length;
		if (!newPerm || ! (newPerm.length==n))
		    throw 'Invalid permutation (wrong number of elements)';
		var c=[];
		$.each(newPerm, function() {
		    if (this %1 != 0)
			throw 'Invalid permutation (the value '+this+ ' is not an integer)';
		    if (this<0 || this>=n)
			throw 'Invalid permutation (the value '+this+' out of range (0 .. '+(n-1)+')';
		    if (c[this])
			throw 'Invalid permutation (the value '+this+ ' is not unique';
		    c[this]=true;
		});

		for (var i=0;i<n;i++)
		{
		    // Iteratively reorder columns until the this.perm is identical to newPerm
		    if (this.perm[i] == newPerm[i])
			continue;
		    var required = newPerm[i];
		    for (var j=i+1; j<n; j++)
		    {
			if (this.perm[j]==newPerm[i])
			{
			    this.moveCells(j,i);
			    break;
			}
		    }
		}
		this.updateColGroups();
	    },
	    visibility : function (v) /* vector of 0/1 according to original column order */
	    {
		if (!this.visibilityV)
		{
		    this.visibilityV= $.map(this.headerRow.children('th'),function(){return 1;});
		}
		if (!v)
		{
		    // Return current visibility vector
		    return this.visibilityV.slice(0);
		}
		else
		{
		    // Set visibility
		    var n =  this.headerRow.children('th').length;
		    if (!v.length || v.length != n)
			throw 'Invalid argument (visibility should be a vector with length='+n+')';
		    var c = this.visibilityV;
		    for (var i=0;i<v.length;i++)
			this.updateColumnVisibility(i,v[i]);
		}

	    },

	    sort: function(v)
	    {
		var $h =  this.headerRow.children('th');
		var currentSort =  $h.map(function(i,th){
			if ($(th).hasClass('up'))
			    return 'up';
			if ($(th).hasClass('down'))
			    return 'down';
			return '';
		    });

		if (!v)
		{
		    return currentSort;
		}
		else
		{
		    if (!v.length || v.length != $h.length)
			throw 'Invalid argument (sort vector should have length='+$h.length+')';	    
		    $h.each(function(i,th)
		    {
			if ((v[i]||'') != currentSort[i])
			{
			    $(th).removeClass('up down').addClass((v[i]||''));
			}
		    });
		}
	    },
	    invPerm:function(i)
	    {
		if (!this.perm)
		    return i;
		for (var j=0;j<this.perm.length;j++)
		{
		    if (this.perm[j]==i)
		    {
			return j;
		    }
		}
		return -1;
		
	    },
	    updateColumnVisibility:function(colIndex, visible)
	    {
		if (this.visibilityV[colIndex] == visible)
		    return;
		this.visibilityV[colIndex] = visible;
		var n = this.visibilityV.length;
		var curIndex = this.invPerm(colIndex);
		/* increase width of table */
		var cssWidth = this.tables[0].style.width;
		if (cssWidth) /* non-jquery API checks if width is set explicitly */
		{
		    var headerCell = this.headerRow.children().eq(curIndex);
		    var cw;
		    cw = parseInt(headerCell.data('col')[0].style.width);
		    if (cw)
		    {
			var originalWidth = parseInt(cssWidth);
			this.tables.css('width',(visible ? originalWidth+cw : originalWidth-cw)+'px');
		    }
		    else
		    {
			/* clear explicit width on col elements */
			headerCell.data('col').each(function(){
			    this.style.width='';
			});
		    }
		}
		
		this.tables.find('tr').each(function(i){
/*
// Special logic to suppress hiding columns probably not needed
		    if ($(this).data('datagrid.nohide'))
			return;
*/
		    var $c = $(this).children();
		    if ($c.length == n)
		    {
			if (visible)
			    $c.eq(curIndex).show();
			else
			    $c.eq(curIndex).hide();
		    }
		});
		this.updateColGroups();
	    },
	    updateColGroups:function()
	    {
		var self = this;
		this.headerRow.children().each(function() {
		    var visible = $(this).css('display')!='none';
		    $(this).data('col').each(function(i){ /* when scrollable, there are two or three col elements per column */
			$(this).remove();
			if (visible)
			{
			    $(this).appendTo(self.colgroups.eq(i));
			}
		    });
		});
	    },
	    setTableClass:function(c)
	    {
		this.tables.attr('class',c);
	    },
	    adjustSize:function(callback)
	    {
		if (this.head) /* scrollable */
		{
	            var sw = scrollbarWidth();
                    this.head.width(this.top.innerWidth() - sw);
                    if(this.footer)
			this.footer.width(this.top.innerWidth() - sw);
                    this.body.width(this.top.innerWidth());
		}
		callback.call();
	    }
        });
})(jQuery);

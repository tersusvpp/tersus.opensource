/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 

function BackgroundProcess (parent, subflow, params)
{
	if (parent && subflow)
	{
		this.parent = parent;
		this.process = subflow.createChildInstance();
		this.context = new BackgroundProcessContext(parent);
		this.process.mainWindow = parent.mainWindow;
		this.process.currentWindow = parent.currentWindow;
		this.process.parent = this.context;
		this.process.element = subflow;
		if (params)
		{
			for (var key in params)
			{
			    var trigger = this.process.getElement(key);
			    if (trigger)
			    {
				var value  = params[key];
				if (value!=null && !value.isNode)
				    value = trigger.getValueNode(value);
				trigger.setChild(this.process, value, Operation.REPLACE);

			    }
			}
		}
	}
};

BackgroundProcess.prototype.setTriggerValue = function setTriggerValue(role, value)
{
	this.process.setChild(role,value, Operation.REPLACE);
};
BackgroundProcess.prototype.setTriggerLeafValue = function setTriggerValue(role, value)
{
	this.process.setLeafChild(role,value, Operation.REPLACE);
};


BackgroundProcess.prototype.getExitValues = function getExitValues(role)
{
	return this.process.getChildren(role);
};

BackgroundProcess.prototype.start = function()
{
	if (this.process.isReady())
		this.process.doStart();
	if (window.trace)
		window.trace.flush();
	
};
BackgroundProcess.prototype.whenDone = function whenDone(callback)
{
	this.process.whenDone = callback;
};
function BackgroundProcessContext(context)
{
	this.context = context;
	this.parent = context;
	this.setStatus(FlowStatus.WAITING_FOR_INPUT);
};
BackgroundProcessContext.prototype = new FlowNode();
BackgroundProcessContext.prototype.constructor = BackgroundProcessContext;
BackgroundProcessContext.prototype.getPath = function getPath()
{
	return this.context.getPath();
};
BackgroundProcessContext.prototype.getParentContext = function getParentContext()
{
	return null;
};
BackgroundProcessContext.prototype.isBackground = true;

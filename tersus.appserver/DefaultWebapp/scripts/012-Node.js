/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 //Generic Runtime Node
tersus.Node = function tersus_Node() {};
tersus.Node.prototype.type='Node';
tersus.Node.prototype.textTranslation = true;
tersus.Node.prototype.pluginVersion = 0;
tersus.Node.prototype.getPrototype = function()
{
	if (this.modelId)
		return tersus.repository.get(this.modelId).prototype;
}
tersus.Node.prototype.getNodeId = function()
{
	if (!this.nodeId)
	{
		this.nodeId = window.nextNodeId ? window.nextNodeId : 1;
		window.nextNodeId = this.nodeId + 1;
	}
	return this.nodeId;
};
tersus.Node.prototype.getModelId = function()
{
	return this.modelId;
};
tersus.Node.prototype.isInstance = function(value)
{
	return value && value.isNode && value.modelId == this.modelId;
};
tersus.Node.prototype.init = function() {}; // Called when a concrete instance is created
tersus.findNodeOld = function findNodeOld(id)
{
	if (tlog) tlog("Trying to find node "+id);

	var node = window.rootDisplayNode.findById(id);
	if (node)
	{
		if (tlog) tlog("Node "+id + " found.");
		return node;
	}
	tersus.unregisterPopupNodes();
	for (var popupId in tersus.popupNodes)
	{
		node = tersus.popupNodes[popupId].findById(id);
		if (node)
		{
			if (tlog) tlog("Node "+id + " found.");
			return node;
		}
	}
	return null;
		
};
tersus.Node.prototype.findById = function(id, alreadyScanned, depth)
{
	if (!depth)
	   depth=1;
		
	if (this.nodeId == id)
		return this;
	if (! this.elementList)
		return null;
	if (!alreadyScanned)
		alreadyScanned = {};
	var thisNodeId = this.getNodeId();
	if (alreadyScanned[thisNodeId])
		return null;
	alreadyScanned[thisNodeId]=true;
	for (var i=0; i<this.elementList.length; i++)
	{
		var e = this.elementList[i];
		if (e.isRepetitive)
		{
			var children = e.getChildren(this);
			for (var j=0; j<children.length;j++)
			{
				var child = children[j];
				if (child)
				{
					var node = child.findById(id, alreadyScanned, depth+1);
					if (node)
						return node;
				}
			}
		}
		else
		{
			var child = e.getChild(this);
			if (child)
			{
				var node = child.findById(id, alreadyScanned, depth+1);
				if (node)
					return node;
			}
		}
	}
	return null;
};

/* returns true if we the maximum number of results has been reached */

tersus.Node.prototype.scanChildren = function(filter, alreadyScanned, outputList, maxResults)
{
	var id = this.getNodeId();
	if (alreadyScanned[id])
		return false;
        if(maxResults!=null && maxResults <= outputList.length)
	    return true;
	alreadyScanned[id]=true;
	if (this.currentWindow && this.currentWindow.closed)
		return false;
	if (filter.match(this))
		outputList.push(this);
	if (! this.elementList)
		return false;
	for (var i=0; i<this.elementList.length; i++)
	{
		var e = this.elementList[i];
		if (e.isDisplayElement || e.isDataElement || e.isMethodElement)
		{
			if (e.isRepetitive)
			{
				var children = e.getChildren(this);
				for (var j=0; j<children.length;j++)
				{
					var child = children[j];
					if (child)
					{
						var rc = child.scanChildren(filter, alreadyScanned, outputList, maxResults);
					        if (rc)
						    return true;
					}
				}
			}
			else
			{
				var child = e.getChild(this);
				if (child)
				{
					var rc = child.scanChildren(filter, alreadyScanned, outputList, maxResults);
				        if (rc)
					    return true;
				}
			}
		}
	}
	return false;
};
tersus.Node.prototype.countHierarchy = function()
{
	var count = 1
	if (this.elementList)
	{
		for (var i=0; i<this.elementList.length; i++)
		{
			var e = this.elementList[i];
			if (e.isRepetitive)
			{
				var children = e.getChildren(this);
				for (var j=0; j<children.length;j++)
				{
					var child = children[j];
					if (child)
					{
						count += child.countHierarchy();
					}
				}
			}
		
			else
			{
				var child = e.getChild(this);
				if (child)
				{
					count += child.countHierarchy();
				}
			}
		}
	}
	return count;
};

tersus.Node.prototype.invokeLinks = function invokeLinks()
{
	var links = this.element.outgoingLinks;
	if (links)
	{
		for (var i=0; i<links.length; i++)
		{
			var link = links[i];
			link.invoke(this);		
		}
	}
};
tersus.Node.prototype.isNode = true;
tersus.Node.prototype.isReady = function isReady()
{
	return true;
};

tersus.Node.prototype.addElement = function addElement(element)
{
	if (!this.elements)
		this.elements = {};
	this.elements[element.role]=element;
	if (!this.elementList)
		this.elementList = [];
	this.elementList.push(element);
	element.owner = this;
	return element;
};

tersus.Node.prototype.getElement = function getElement(role)
{
	var element = null;
	if (this.elements)
		element = this.elements[role];
	if (element)
		return element;
	else
		return null;
};
tersus.Node.prototype.addGetter = function(role, getter)
{
	this['getter_'+role] = getter;
};
tersus.Node.prototype.addSetter = function(role, setter)
{
	this['setter_'+role] = setter;
};
tersus.Node.prototype.addRemover = function(role, remover)
{
	this['remover_'+role] = remover;
};

tersus.Node.prototype.getGetter = function getGetter(role)
{
	if (this['getter_'+role])
		return this['getter_'+role];
	else if (role.charAt(0)=='<')
		return this['get'+role];// Obsolete mode (still widely used as of June 2009)
	else return null;
};
tersus.Node.prototype.getSetter = function getSetter(role)
{
	if (this['setter_'+role])
		return this['setter_'+role];
	else if (role.charAt(0)=='<') 
		return this['set'+role]; // Obsolete mode (still widely used as of June 2009)
	else
		return null;
};
tersus.Node.prototype.getAdder = function getAdder(role)
{
	return this['add'+role];
};
tersus.Node.prototype.getRemover = function getRemover(role)
{
	if ( this['remover_'+role])
		return this['remover_'+role];
	else if (role.charAt(0)=='<') 
		return this['remove'+role]; // Obsolete mode (still widely used as of June 2009)
	else
		return null;
};

tersus.Node.prototype.getChild = function getChild(role)
{
	var element = this.getElement(role);
	if (element)
		return element.getChild(this);
	else
		return null;
};

tersus.Node.prototype.getChildren = function getChildren(role)
{
	var element = this.getElement(role);
	if (element)
		return element.getChildren(this);
	else
		return [];
};
/*
	Returns an array of all values at given path.
	path - a Path containing the required path
	index - an optional number indicating which segment in the path should be deemed first 
	         (the first 'index' segments are ignored.)
*/
tersus.Node.prototype.getValues = function getValues(path, index)
{
	if (!index)
		var index = 0;
	var values = [];
	this._getValues(path, index, values);
	return values;
};


/*
	Adds all values at a given path to a given array of values
	This method is used internally to implement 'getValues'/
	path - a Path containing the required path
	index - an optional number indicating which segment in the path should be deemed first 
	         (the first 'index' segments are ignored.)
	values - an array to which all values will be added.
	 
*/
tersus.Node.prototype._getValues = function _getValues(path, index, values)
{
	if (path.segments.length < index)
	{
		internalError('Bad path in _getValues');
		return;
	}
	var role = path.segments[index];
	if (path.segments.length == index )
	{ 
		values.push(this);
	}
	else
	{
		var element = this.getElement(role);
		if (!element)
			return; // The path links to a non-existant element (as of Feb 26, this can happen because of the way the permission mechanism works)
		if (element.isRepetitive)
		{
			var children = element.getChildren(this);
			for (var i=0; i<children.length; i++)
			{
				var child = children[i];
				if (child)
					child._getValues(path, index+1, values);
			}
		}
		else
		{
			var child = element.getChild(this);
			if (child)
				child._getValues(path, index+1, values);
			
		}
	}
}
/**
	Sets the value of a descendant, specified by path
	The index specifies a number of initial segments of the path that should be ignored
	(index = 0 means using the whole path, index = 1 means starting from the second segment).
*/
tersus.Node.prototype.setValue = function setValue(path, value, operation, index)
{
	if (! operation)
	{
		internalError('Operation not specified');
	}
	if (! index)
		var index = 0;
	if (path.segments.length <= index)
	{
		internalError('Bad path in setValue');
	}
	var role = path.segments[index];
	if (path.segments.length-1 == index)
	{
		var element = this.getElement(role);
		if (!element)
			return;// The path links to a non-existant element (as of 0.9.49, this can happen because of the way the permission mechanism works)
		element.setChild(this, value, operation);
	}
	else if (path.segments.length > index)
	{
		var element = this.getElement(role);
		if (! element)
			return; // The path links to a non-existant element (as of 0.9.49, this can happen because of the way the permission mechanism works)
		var child = element.getChild(this);
		if (! child)
			child = element.createChild(this); // Creating an intermediate level
		child.setValue(path, value, operation, index+1);
	}
};


tersus.Node.prototype.getPath = function getPath()
{
	if (this.parent)
	{
		var path = this.parent.getPath();
		path.addSegment(this.element.role);
		return path;
	}
	else
		return new Path(tersus.viewPath);
};
tersus.Node.prototype.setAlwaysCreate = function setAlwaysCreate(role, alwaysCreate)
{
	this.elements[role].alwaysCreate = alwaysCreate;
};
// Performs any operation needed when this node is deleted
tersus.Node.prototype.onDelete = function onDelete() 
{
};

// Performs a 'set' operation with a leafValue
// Returns true if the operation is sucessful, false if there is no element with the given role
tersus.Node.prototype.setLeafChild = function setLeafChild(role, leafValue, operation)
{
	if (leafValue == null)
		return;
	var element = this.getElement(role);
	if (!element)
		return false;
	var value = element.createChildInstance();
	value.setLeaf(leafValue);
	element.setChild(this, value, operation);
	return true;
};

tersus.Node.prototype.getLeafChild = function getLeafChild(role)
{
	var child = this.getChild(role);
	if (child)
		return child.leafValue;
	else
		return null;
};
tersus.Node.prototype.setChild = function setChild(role, value, operation)
{
	var element = this.getElement(role);
	if (!element)
		return false;
	element.setChild(this, value, operation);
	return true;
};
tersus.Node.prototype.setLeaf = function setLeaf(value)
{
	this.leafValue = value;
};

tersus.Node.prototype.parseString = function(str)
{
	this.leafValue = str;
};
tersus.Node.prototype.copyObject = function copyObject(value, strict)
{
	if (this.isLeaf)
		this.setLeaf(value);
	else if (this.elements)
	{
	    for (var elementRole in this.elements)
	    {
		var element = this.getElement(elementRole);
		if (element.isRepetitive)
		{
		    var values = value[elementRole];
		    if (values)
		    {
			for (var i=0; i< values.length; i++)
			{
			    var child = element.createChild(this);
			    child.copyObject(values[i], strict);
			}
		    }
		}
		else
		{
		    var childValue = value[elementRole];
		    if (childValue !== null && childValue !== undefined)
		    {
			var child = element.createChild(this);
			child.copyObject(childValue, strict);
		    }
		}
	    }
	    if (strict)
	    {
		var e = this.elements;
		$.each(value, function(key,value) {
		    if (!e[key])
			throw "Unknown element "+key;
		});
	    }
	}
};
tersus.Node.prototype.getTranslation = function(key)
{
	if (tersus.labelMap)
	{
		var v = tersus.labelMap[key];
		if (v && v.leafValue)
			return v.leafValue;
		else
			return v;
	}
};

tersus.Node.prototype.translate = function (s)
{
	if (! this.textTranslation)
		return s;
	return this.forceTranslate(s);
};
tersus.Node.prototype.forceTranslate = function (s)
{
	var t = this.getTranslation(s);
	if (t != undefined)
		return t;
	else
		return s;
};
tersus.Node.prototype.preloadOnStart = true;
tersus.Node.prototype.preload = function(callback)
{
	if (tersus.repository.preloaded[this.modelId])
		return true;
	var packageSet = new tersus.Set();
	if (tlog) var startTime = new Date();
	var	alreadyChecked = new tersus.Set();
	this.addPreloadPackages(packageSet,alreadyChecked);
	this.addMissingLinkTargets(packageSet,alreadyChecked);
	if (tlog)
	{
		var endTime = new Date();
		if (tlog) tlog("Package scan took "+(endTime-startTime) +" milliseconds");
	}
	var packageList = packageSet.toList();
	if (packageList.length == 0)
	{
		tersus.repository.preloaded[this.modelId] = true;
		return true;
	}
	else
	{
		if (tlog) tlog('Preloading '+this.modelId);
		packageList.sort();
		var mainPackages = [];
		var last = null;
		for (var i=0;i<packageList.length;i++)
		{
			var current = packageList[i];
			if (last && last == current.substring(0,last.length))
				continue;
			mainPackages.push(current);
			last = current;
		}
		this.pause();
		tersus.repository.loadPackages(mainPackages, callback);
		return false;
	}
};
tersus.Node.prototype.addPreloadPackages = function (packageSet, alreadyChecked)
{
	if (tersus.repository.preloaded[this.modelId])
		return;
	if (alreadyChecked.contains(this.modelId))
		return;
	alreadyChecked.add(this.modelId);
	if (this.elementList)
	{
		for (var i=0;i<this.elementList.length;i++)
		{
			var e = this.elementList[i];
			if (e.modelId)
			{
				if (!tersus.repository.checkLoaded(e.modelId))
				{
					var packageId = tersus.getPackageId(e.modelId);
					packageSet.add(packageId);
				}
				else 
				{
					if (e.isDisplayElement || e.isFlowDataElement || ! this.isDisplayNode )
					{
						var childConstructor = tersus.repository.get(e.modelId);
						var childModel = childConstructor.prototype;
//						if (childModel.preloadOnStart)
//							childModel.addPreloadPackages(packageSet, alreadyChecked);
					}
				}
			}
		}
	}
};
tersus.Node.prototype.addMissingLinkTargets = function (packageSet, alreadyChecked)
{
	if (!this.links)
		return;
	for (var i=0;i<this.links.length; i++)
	{
		var link = this.links[i];
		var currentModel = this;
		var segments = link.target.segments;
		for (var j=0; j< segments.length; j++)
		{
			var element = currentModel.getElement(segments[j]);
			if (!element)
				continue; // This can happen if there is no permission
			var id = element.modelId;
			if (!id)
				continue; 
			if (! tersus.repository.checkLoaded(id))
			{
				if (tlog) tlog('Need to preload '+id);
				var packageId = tersus.getPackageId(id);
				packageSet.add(packageId);
				break;
			}
			else
			{
				currentModel = tersus.repository.get(id).prototype;	
				if (currentModel.preloadOnStart)
					currentModel.addPreloadPackages(packageSet,alreadyChecked);
			}
		}
	}
	return true;
};	
tersus.Node.prototype.destroyHierarchy = function()
{
	this.destroyChildren();
        if (this.destroy)
                this.destroy();
};
tersus.Node.prototype.destroyChildren = function()
{
        tersus.Node.subFlowIterator(this,function(node) {node.destroyHierarchy()});
};

tersus.Node.childIterator = function (node,iteration)
{
	if (node && node.elements)
	{
		for (var i=0; i<node.elementList.length; i++)
		{
			var e = node.elementList[i];
			if ( e.isRepetitive)
			{
				var children  = e.getChildren(node);
				for (var j=0;j<children.length;j++)
				{
					var stop = iteration(children[j]);
					if (stop == true)
						return;
				}
			}
			else
			{
				var child = e.getChild(node);
				if (child)
				{
					var stop = iteration(child);
					if (stop == true)
						return;
				}
			}
		}
	}
	
};
tersus.Node.subFlowIterator = function (node,iteration)
{
	if (node && node.subFlowList)
	{
		for (var i=0; i<node.subFlowList.length; i++)
		{
			var e = node.subFlowList[i];
			if ( e.isRepetitive)
			{
				var children  = e.getChildren(node);
				for (var j=0;j<children.length;j++)
				{
					var stop = iteration(children[j]);
					if (stop == true)
						return;
				}
			}
			else
			{
				var child = e.getChild(node);
				if (child)
				{
					var stop = iteration(child);
					if (stop == true)
						return;
				}
			}
		}
	}
	
};

tersus.compare = function (obj1, obj2)
{
	if (obj1 == null)
		return obj2 == null ? 0 : -1;
	return obj1.compare(obj2);
};
tersus.Node.prototype.compare = function (obj2)
{
    /* Note: r3431 contains a generalized implementation of comparison, which has been reverted out of backward and client-server compatibility concerns.  The new implementation remains a good idea, but will significant effort to complete (client+server), review and test */
	if (obj2 == null)
		return 1;
	var obj1 = this;
    if (!this.isInstance(obj2))
        throw "compare: Can't compare "+obj1.modelId + " and "+ obj2.modelId;
        
    if (this.isLeaf)
    	return obj1.leafValue > obj2.leafValue ? 1 : obj1.leafValue == obj2.leafValue ? 0 : -1;

    for (var i = 0; i < this.elementList.length; i++)
    {
        var e = this.elementList[i];
        if (e.isRepetitive)
        {
            var list1 = e.getChildren(obj1);
            var list2 = e.getChildren(obj2);
            var commonSize = Math.min(list1.length, list2.length);
            for (var j=0; j<commonSize;j++)
            {
                var item1 = list1[j]
                var item2 = list2[j];
                
               	var comparison = tersus.compare(item1, item2);
                if (comparison != 0)
                    return comparison;
            }
            return list1.length - list2.length;
        }
        else
        {
            var v1 = e.getChild(obj1);
            var v2 = e.getChild(obj2);
            var comparison = tersus.compare(v1, v2);
            if (comparison != 0)
                return comparison;
        }
    }
    return 0;
};

tersus.Node.prototype.isRecursive = function()
{
    /* Checks if the model represents a potentially recursive structure - used to optimize recursions*/
    var model = this.constructor.prototype;
    if (model.__isRecursive === undefined)
    {
        if (model.__isRecursive || model.__in_recursion_check ||model.modelId == tersus.ANYTHING || model.isMap ||model.constructor === tersus.DynamicDisplay)
        {
            model.__isRecursive = true; 
            return true;
        }
        else
        {
            var recursive = false;
            model.__in_recursion_check=true;
            try
            {
                if (model.elementList)
                {
                    for (var i=0;i<model.elementList.length;i++)
                    {
                        var e = model.elementList[i];
                        var childModel = e.getChildConstructor().prototype;
                        if (childModel.isRecursive())
                        {
                            recursive= true;
                            break;
                        }
                        
                    }
                }
                model.__isRecursive=recursive;
            }
            finally
            {
                delete model.__in_recursion_check;
            }
        }
    }
    return model.__isRecursive;
};


/*********************************************************************************************************
 * Copyright (c) 2003-2023 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/

var SPECIAL_HTML_ATTRIBUTES='marquee.direction,marquee.scrollamount,marquee.loop';
var TRANSLATABLE_HTML_ATTRIBUTES='placeholder';
var HTML_ATTRIBUTES = TRANSLATABLE_HTML_ATTRIBUTES + ',abbr,accept,accept-charset,accesskey,action,align,alink,alt,archive,autoplay,axis,background,bgcolor,border,cellpadding,cellspacing,char,charoff,charset,checked,cite,class,classid,clear,code,codebase,codetype,color,cols,colspan,compact,content,contenteditable,controls,coords,data,datetime,declare,defer,dir,disabled,enctype,face,for,frame,frameborder,headers,height,href,hreflang,hspace,http-equiv,id,ismap,label,lang,language,link,longdesc,marginheight,marginwidth,maxlength,media,method,multiple,name,nohref,noresize,noshade,nowrap,object,onblur,onchange,onclick,ondblclick,onfocus,onkeydown,onkeypress,onkeyup,onload,onmousedown,onmousemove,onmouseout,onmouseover,onmouseup,onreset,onselect,onsubmit,onunload,profile,prompt,readonly,rel,rev,rows,rowspan,rules,scheme,scope,scrolling,selected,shape,size,span,src,standby,start,style,summary,tabindex,target,text,title,type,usemap,valign,value,valuetype,version,vlink,vspace,width,pattern,required,autocorrect,autocapitalize,min,max,step,list,autocomplete';
//var DISTINGUISHED_HTML_ATTRIBUTES="value"
//var DistinguishedRegex=/<(.*)>/gm;
var STYLE_ATTRIBUTES = 'azimuth,background,background-attachment,background-color,background-image,background-position,background-repeat,border,border-collapse,border-color,border-spacing,border-style,border-top,border-right,border-bottom,border-left,border-top-color,border-right-color,border-bottom-color,border-left-color,border-top-style,border-right-style,border-bottom-style,border-left-style,border-top-width,border-right-width,border-bottom-width,border-left-width,border-width,bottom,caption-side,clear,clip,color,content,counter-increment,counter-reset,cue,cue-after,cue-before,cursor,direction,display,elevation,empty-cells,float,font,font-family,font-size,font-size-adjust,font-stretch,font-style,font-variant,font-weight,height,left,letter-spacing,line-height,list-style,list-style-image,list-style-position,list-style-type,margin,margin-top,margin-right,margin-bottom,margin-left,marker-offset,marks,max-height,max-width,min-height,min-width,orphans,outline,outline-color,outline-style,outline-width,overflow,padding,padding-top,padding-right,padding-bottom,padding-left,page,page-break-after,page-break-before,page-break-inside,pause,pause-after,pause-before,pitch,pitch-range,play-during,position,quotes,richness,right,size,speak,speak-header,speak-numeral,speak-punctuation,speech-rate,stress,table-layout,text-align,text-decoration,text-indent,text-shadow,text-transform,top,unicode-bidi,vertical-align,visibility,voice-family,volume,white-space,widows,width,word-spacing,z-index';
tersus.TRANSLATABLE_HTML_ATTRIBUTES = new tersus.Set(TRANSLATABLE_HTML_ATTRIBUTES.split(','));
tersus.HTML_ATTRIBUTES = new tersus.Set(HTML_ATTRIBUTES.split(','));
//tersus.DISTINGUISHED_HTML_ATTRIBUTES = new tersus.Set(DISTINGUISHED_HTML_ATTRIBUTES.split(','));
tersus.SPECIAL_HTML_ATTRIBUTES = new tersus.Set(SPECIAL_HTML_ATTRIBUTES.split(','));
var BOOLEAN_HTML_ATTRIBUTES = 'readonly,disabled,checked,autoplay,controls,required,selected';
tersus.BOOLEAN_HTML_ATTRIBUTES = new tersus.Set(BOOLEAN_HTML_ATTRIBUTES.split(','));
tersus.STYLE_ATTRIBUTES = new tersus.Set(STYLE_ATTRIBUTES.split(','));
tersus.HTMLTag = DisplayNode.__subclass('tersus.HTMLTag', function(){});
tersus.HTMLTag.prototype.isHTMLTag = true;
tersus.propertyConversionMap = { 
	styleclass:'class',
	'style class':'class',
	wrappertag:'wrapperTags',
	wrappertags:'wrapperTags',
	wrapper_tags:'wrapperTags',
	wrapperstyleclass:'wrapperStyleClasses',
	wrapperstyleclasses:'wrapperStyleClasses',
	wrapper_classes:'wrapperStyleClasses',
	path:'src',
	icon:'data-icon',
	theme:'data-theme',
	inset:'data-inset'
};


tersus.HTMLTag.prototype.isHTMLTag = true;
tersus.convertProperty = function(p)
{
	var l = p.toLowerCase(); 
	var c = tersus.propertyConversionMap[l];
	return c ? c: l;
};
DisplayElement.prototype.isHTMLPrepared = false;
DisplayElement.prototype.prepareForHTML = function(parent)
{
	if (this.isHTMLPrepared)
		return;
	this.isHTMLPrepared = true;
	var childPrototype = this.getChildConstructor().prototype;
	if (!childPrototype || ! (childPrototype.isHTMLTag || childPrototype.isGroup ))
		return;
	this.prepHTML = childPrototype.prepareForHTML(this,parent);
	childPrototype.prepareChildrenForHTML();
};
DisplayNode.prototype.prepareChildrenForHTML = function()
{
	if (this.displayElements)
	{
		for (var i=0;i<this.displayElements.length;i++)
		{
			var e = this.displayElements[i];
			if (e.prepareForHTML)
				e.prepareForHTML(this);
		}
	}
};
tersus.HTMLTag.prototype.updateVisible = function()
{
	this.visible = !(this.getLeafChild('<Visible>') == false);
};
tersus.template = function(template, data)
{
	if (!tersus.templates)
		tersus.templates = {};
	if (!tersus.templates[template])
		tersus.templates[template]=$.template(template);
	return tersus.templates[template]($,{'data':data}).join('');
};
DisplayNode.prototype.prepareForHTML = function(e,parent)
{
	var props = {};
	var prep = {};
	if (!this.isHTMLTag)
		return prep;
	prep.updateVisible = parent && parent.isGroup || this.getElement('<Visible>'); //No need to update if parent is not a group and no <Visible>
	props.wrapperTags = this.defaultWrapperTags;
	props.wrapperStyleClasses = this.defaultWrapperStyleClasses;
	$.extend(props,this.baseProperties);
	props['class'] = this.defaultClass;
	props['type']=this.inputType; // For input fields
	props.tag = this.defaultTag;
	if (this.contentTag)
	{
		prep.contentPrefix = tersus.template('<${tag}{{if cls}} class="${cls}"{{/if}}>',{tag:this.contentTag,cls:this.contentClass});
		prep.contentSuffix = '</'+this.contentTag+'>';
	}
	for (var p in this.sharedProperties)
	{
		var v = this.sharedProperties[p];
		if (v != null && v != ' ')
			props[tersus.convertProperty(p)]=v;
	} 
	if (e)
	{
		for (var p in e.properties)
		{
			var v = e.properties[p];
			if (v != null && v != ' ')
				props[tersus.convertProperty(p)]=v;
		}
	}
	if (props.wrapperTags == '-')
		props.wrapperTags = null;
	if (props.wrapperStyleClasses == '-')
		props.wrapperStyleClasses = null; 
	prep.staticStyle = '';
	var html = '';
	for (var p in props)
	{
		var v = props[p];
		var dynamicElement =  this.dynamicAttributeElements[p];
		if (dynamicElement)
		{
			//Prepare default value
			if (prep.defaultValues == null)
				prep.defaultValues = {};
			prep.defaultValues[dynamicElement.role]=v;
		}
		else
		{
			//Parepare static value
			if (tersus.SPECIAL_HTML_ATTRIBUTES.contains(props.tag+'.'+p))
				html += ' ' + p + '="' +v+'"';
			else if (tersus.STYLE_ATTRIBUTES.contains(p))
				prep.staticStyle += p + ':' + v+';';
			else if (tersus.BOOLEAN_HTML_ATTRIBUTES.contains(p))
			{
				if (v)
					html+= " " + p;	
			}
			else if (p.match(/^data-/) || tersus.HTML_ATTRIBUTES.contains(p) && v != undefined)
				html += " " + p +'="'+tersus.escapeHTML(v)+'"'; 	
		}
	}
	if (e && e.displayOrder)
	{
		html+= ' display_order="'+e.displayOrder+'"';
	}
	prep.tag = props.tag.toUpperCase();
	prep.isInput = prep.tag == 'INPUT' || prep.tag == 'TEXTAREA';
	prep.HTMLPrefix = '';
	prep.HTMLSuffix = '';
	prep.wrapperTags = null;
	if (props.wrapperTags)
	{
		var tags = prep.wrapperTags = props.wrapperTags.split(',');
		if (tags.length > 0)
			prep.wrapper_tag = tags[0];
		var classes = [];
		if (props.wrapperStyleClasses)
		{
			classes = props.wrapperStyleClasses.split(',');
		}
		
		for (var i=0;i<tags.length;i++)
		{
			var t = tags[i];
			var c = null;
			if (i< classes.length)
				c = classes[i];
			if (c)
				prep.HTMLPrefix += '<'+t+' class="'+c+'">';
			else
				prep.HTMLPrefix += '<'+t+'>';
			prep.HTMLSuffix = '</'+t+'>'+prep.HTMLSuffix;
		}
	}
	prep.HTMLPrefix += '<'+prep.tag + html;
	if (prep.staticStyle.length>0 && this.styleAttributeElements.length ==0)
		prep.HTMLPrefix += ' style="'+prep.staticStyle+'"';
	prep.HTMLPrefix += ' lid="';
	prep.HTMLSuffix = '</'+prep.tag+'>'+prep.HTMLSuffix;
	prep.simpleHTML = this.HTMLAttributeElements.length == 0 && this.styleAttributeElements.length ==0 && !prep.updateVisible;
	return prep;
};
 
tersus.HTMLTag.prototype.reset = function()
{
	this.removeAll();
	this.create();
};

tersus.HTMLTag.prototype.createDomNodeFromHTML = function(html)
{
    return $(html,  this.currentWindow.document)[0];
};
tersus.HTMLTag.prototype.create = tersus.HTMLTag.prototype.copyValue = function(value)
{

	var oldViewNode = this.viewNode; // Shouldn't we care about "old wrapper node"
	this.viewNode = null; // Clear view node so that attribute values are set logically during the copy phase (and not placed on DOM nodes)
	if (this.element && !this.element.isHTMLPrepared)
		this.element.prepareForHTML(this.parent);
	var nodeMap = {};
	var htmlV = [];
	var legacyNodes = []; // nodes that are not instances of HTMLTag and need to be copied the old way
	this.copyValueOpt(value, nodeMap, htmlV, legacyNodes);
	var html = htmlV.join('');
	var domNode = this.createDomNodeFromHTML(html);
	var prev = this.wrapperNode ? this.wrapperNode: oldViewNode; // TODO (x) use placeHolderNode;
	this.viewNode = domNode;
	var wt = this.prep.wrapperTags;
	this.wrapperNode = this.viewNode;
	if (wt)
	{
		for (var i=0;i<wt.length;i++)
			this.viewNode = this.viewNode.firstChild;
	}
	this.textContainer = this.viewNode;
	if (prev)
		this.replaceDomNode(domNode,prev);
	else
		this.attachViewNode();
	this.setEventHandlers();
	this.postAttach();
	this.attachHierarchy(nodeMap);
	for (var i=0;i<legacyNodes.length;i++)
	{
		var n = legacyNodes[i];
		n.create(n.valueToCopy);
		n.valueToCopy = null;
	}
	var t = this;
	tersus.async.exec(function(){t.onResize();},false);
};
tersus.HTMLTag.prototype.replaceDomNode = function(newNode, prevNode)
{
	prevNode.parentNode.replaceChild(newNode, prevNode);
};
tersus.HTMLTag.prototype.initModel = function()
{
	this.dynamicAttributeElements = {};
	this.HTMLAttributeElements = [];
	this.styleAttributeElements = [];
}
tersus.HTMLTag.prototype.onload = function()
{
	DisplayNode.prototype.onload.call(this);
	this.createEventHandlerList();
};
DisplayNode.prototype.copyValueOpt = function(value, nodeMap, htmlV, legacyNodes)
{
	var prep;
	if (this.element && this.element.prepHTML)
	{
		prep = this.prep = this.element.prepHTML;
	}
	else
	{
		if (!this.prepHTML)
		{
			{
				var prototype = this.getPrototype();
				prototype.prepHTML = prototype.prepareForHTML(null);
				prototype.prepareChildrenForHTML();
			}
		}
		prep = this.prep = this.prepHTML;
	}
	// Copy default values into instance
	var def = prep.defaultValues;
	if (def != null)
	{
		for (var role in def)
		{
			this.setLeafChild(role, def[role], Operation.REPLACE);
		}
	}
	if (value != null)
		this.leafValue = value.leafValue;
	else 
		this.leafValue = null;
	if (!this.isGroup)
	{
		htmlV.push(null); // placeHolder for tag opening; we can't create the tag before we copy values
		var openIndex = htmlV.length-1;
	}
	if (prep.contentPrefix)
		htmlV.push(prep.contentPrefix);
	var displayChildren=[];
	if (this.elements)
	{
		for (var elementRole in this.elements)
		{
			var element = this.getElement(elementRole);
			if (element)
			{
				if (element.isRepetitive)
				{
					if (value)
					{
						var childValues = value.getChildren(elementRole);
						for (var i=0;i<childValues.length;i++)
						{
							var oldChild = childValues[i];
							if (element.isDisplayElement)
							{
								var newChild =  element.createChildWithoutVisuals(this, oldChild.modelId);
								if (newChild.isHTMLTag || newChild.isDisplayGroup)
									newChild.copyValueOpt(oldChild, nodeMap,htmlV, legacyNodes);
								else
								{
									newChild.valueToCopy = oldChild;
									legacyNodes.push(newChild);
								}
							}
							else
							{
								var newChild = oldChild; //No need to clone non-display elements
								element.addAChild(this, newChild);
							}
							displayChildren.push(newChild);
						}
					}
				}
				else
				{
					var oldChild = value ? value.getChild(elementRole) : null;
					if (element.isDisplayElement && (element.alwaysCreate && ! this.initElement  || oldChild != null))
					{
						var newChild =  element.createChildWithoutVisuals(this, oldChild? oldChild.modelId:null);
						if (newChild.isHTMLTag || newChild.isDisplayGroup)
							newChild.copyValueOpt(oldChild, nodeMap,htmlV,legacyNodes);
						else
						{
							newChild.valueToCopy = oldChild;
							legacyNodes.push(newChild);
						}
						displayChildren.push(newChild);
						Element.prototype.replaceChild.call(element,this, newChild);
					}
					else if (oldChild != null)
						element.replaceChild(this, oldChild);
					
				}
			}
		}	
	}
	if (prep.contentSuffix)
		htmlV.push(prep.contentSuffix);
	
	if (! this.isGroup)
	{
		var nodeIdS = '' + this.getNodeId();
		nodeMap[nodeIdS] = this;
		var e = this.element;
		if (prep.simpleHTML)
		{
			htmlV[openIndex] = prep.HTMLPrefix + nodeIdS+'">';
		}
		else
		{
			var hv = [];
			hv.push(prep.HTMLPrefix);
			hv.push(nodeIdS);
			hv.push('" ');
			for (var i=0;i<this.HTMLAttributeElements.length; i++)
			{
				var e = this.HTMLAttributeElements[i];
				var v = e.getChild(this);
				if ( v!=null)
				{
					if (tersus.BOOLEAN_HTML_ATTRIBUTES.contains(e.role))
					{
						if (v)
						{
							hv.push(e.attributeName);
							hv.push(' ');
						}
						
					}
					else
					{
						hv.push(e.attributeName);
						hv.push('="');
						hv.push(tersus.escapeHTMLAttribute(v.leafValue));
						hv.push('" ');
					}
				}
			}
			var invisible = false;
			if (prep.updateVisible)
			{
				this.updateVisible();
				invisible = this.isInvisible();
			}
			if (this.styleAttributeElements.length > 0 || e.staticStyle || invisible)
			{
				hv.push(' style="');
				hv.push(prep.staticStyle);
				for (var i=0;i<this.styleAttributeElements.length; i++)
				{
					var e = this.styleAttributeElements[i];
					var v = e.getChild(this);
					if ( v!=null && v.leafValue != null)
					{
						hv.push(e.attributeName);
						hv.push(':');
						hv.push(v.leafValue);
						hv.push(';');
					}
				};
				if (invisible)
					hv.push('display:none;');
				hv.push('"');
			}
			hv.push('>');
			htmlV[openIndex] = hv.join('');
		}
		this.addHTMLTagContent(htmlV, displayChildren, prep);
		htmlV.push(prep.HTMLSuffix);
	}
};
tersus.HTMLTag.prototype.addHTMLTagContent = function(htmlV, displayChildren, prep)
{
	if (!this.displayElements && !prep.isInput)
		htmlV.push(this.escapeHTML(this.translate(this.valueStr)));
};

tersus.HTMLTag.prototype.attachHTMLNode = function(e)
{
	this.viewNode = this.textContainer=e;
	var wt = this.prep.wrapperTags;
	this.wrapperNode = this.viewNode;
	if (wt)
	{
		for (var i=0; i< wt.length;i++)
			this.wrapperNode = this.wrapperNode.parentNode;
	}
	if (this.hasEventHandlers)
		this.setEventHandlers();
	this.postAttach();
}
tersus.HTMLTag.prototype.postAttach = function()
{
	var e = this.viewNode;
	if (e.tagName=='INPUT' || e.tagName=='TEXTAREA')
		this.inputField = e;
	
	var p = e.getAttribute('data-role');
	if (p=='button') p='buttonMarkup';
	var node = this;
	if (p && $.fn[p])
		$.fn[p].apply($(e));
	if (e.tagName=='A')
		$(e).find('span.ui-btn-text').each(function(i,e1){node.textContainer=e1;}); // Should make <Value> work, but not <Disabled>
	
};
tersus.HTMLTag.prototype.attachHierarchy = function (nodeMap) 
{
	var elements = this.viewNode.getElementsByTagName('*');
	for (var i=0;i<elements.length; i++)
	{
		var e = elements[i];
		var lid = e.getAttribute('lid');
		if (lid)
		{
			var n = nodeMap[lid];
			if (n)
				n.attachHTMLNode(e);
		}
	}
};
//Todo (?) - merge HTMLTag and DisplayNode so that optimization is the default
tersus.HTMLTag.prototype.createEventHandlerList = function createEventHandlerList()
{
	this.eventHandlerList = [];
	for (var key in Events)
	{
		var eventType = Events[key];
		if (this.hasEventHandler(eventType) && eventType.jsName)
			this.eventHandlerList.push(eventType);
	}
	this.isLocationChangeListener = this.hasEventHandler(Events.ON_LOCATION_CHANGE);
	
	this.hasEventHandlers = this.eventHandlerList.length > 0 || this.isLocationChangeListener;
};
tersus.HTMLTag.prototype.setEventHandlers = function DisplayNode_setEventHandlers()
{
	for (var i=0; i<this.eventHandlerList.length; i++)
	{
		var eventType = this.eventHandlerList[i];
		this.registerEventHandler(eventType);
	}
	if (this.isLocationChangeListener)
		tersus.locationChangeListeners.push(this.getNodeId());
};

tersus.HTMLTag.prototype.addIntermediateDataElement = function addIntermediateDataElement(role, modelId)
{
	//Interpret "distingushed" elements as CSS/HTML attributes (and throw error if there is no such HTML/CSS attribute)
	var element = null;
	if (this.getGetter(role) && role != '<Style Class>' && role != '<Style>')
	{
		element = new MethodElement(this, role, modelId);
	}
	else if (role == '<Valid Values>')
	{
		//<Valid Values> is used by the Table Wizard mechanism and is actaully a regular data element
		element = new FlowDataElement(role, modelId);
	}
	else if (role.search(/^<.*>$/)>=0) 
	{
		var p = tersus.convertProperty(role.substring(1,role.length-1));
//		var pd = p.replace(DistinguishedRegex,'$1');
		
		if (tersus.STYLE_ATTRIBUTES.contains(p))
		{
			element = new tersus.StyleAttributeElement(this,role,modelId);
			this.styleAttributeElements.push(element);
			this.dynamicAttributeElements[p]=element;
		}
		else if (p.match(/^data-/) || tersus.HTML_ATTRIBUTES.contains(p))
		{
			element = new tersus.HTMLAttributeElement(this,role,modelId);
			this.HTMLAttributeElements.push(element);
			this.dynamicAttributeElements[p]=element;
		}
//		else if (tersus.DISTINGUISHED_HTML_ATTRIBUTES.contains(pd))
//		{
//			element = new tersus.HTMLAttributeElement(this,role,modelId);
//			this.HTMLAttributeElements.push(element);
//			this.dynamicAttributeElements[pd]=element;
//		}
		else
		 	modelError("Unknown CSS/HTML property \""+p+"\" for data element \""+role+"\"",this);
	}
	else
	{
		element = new FlowDataElement(role, modelId);
	}
	return this.addElement(element);
};

tersus.HTMLAttributeElement = DataElement.__subclass('HTMLAttributeElement', function(owner, role, modelId)
{
	this.owner = owner;
	this.role = role;
	this.modelId = modelId;
	this.attributeName = tersus.convertProperty(role.substring(1,role.length-1));
//	this.attributeName = tersus.convertProperty(role.substring(1,role.length-1)).replace(DistinguishedRegex,'$1');
	this.changeHandler=owner['jq'+role];
});
tersus.HTMLAttributeElement.prototype.replaceChild = function replaceChild(parent, value)
{
	DataElement.prototype.replaceChild.call(this, parent,value);
	var v = value != null ? value.leafValue : null;
	if (parent.viewNode)
	{
		if (this.attributeName == 'class')
		{
			if (v == null) v = '';
			parent.viewNode.className = v;
		}
		else if (tersus.BOOLEAN_HTML_ATTRIBUTES.contains(this.attributeName))
		{
			$(parent.viewNode).prop(this.attributeName, v);
		}
		else
		{
			if (tersus.TRANSLATABLE_HTML_ATTRIBUTES.contains(this.attributeName))
				v = parent.translate(v);
			parent.viewNode.setAttribute(this.attributeName, v);
		}
			
		if (this.changeHandler)
			this.changeHandler.call(parent,value);
	}
};
tersus.HTMLAttributeElement.prototype.removeChild = function removeChild(parent)
{
	DataElement.prototype.removeChild.call(this, parent);
	if (parent.viewNode)
	{
		if (this.attributeName == 'class')
			parent.viewNode.className = '';
		else if (tersus.BOOLEAN_HTML_ATTRIBUTES.contains(this.attributeName))
			$(parent.viewNode).prop(this.attributeName, null);
		else
			parent.viewNode.setAttribute(this.attributeName, null);
	}
};

tersus.HTMLAttributeElement.prototype.getChild = function getChild(parent)
{
	if (parent.viewNode)
	{
		var leafValue;
		if (this.attributeName == 'class')
		{
			leafValue = parent.viewNode.className;
			if (leafValue == '') 
				leafValue = null;
		}
		else if (tersus.BOOLEAN_HTML_ATTRIBUTES.contains(this.attributeName))
			leafValue = $(parent.viewNode).prop(this.attributeName);
		else
			leafValue = parent.viewNode.getAttribute(this.attributeName);
		var dataValue = null;
		if (leafValue != null)
		{
			dataValue = this.createChildInstance();
			dataValue.setLeaf(leafValue);
		}
		return dataValue;

	}
	else
		return DataElement.prototype.getChild.call(this, parent);
};
tersus.StyleAttributeElement = DataElement.__subclass('StyleAttributeElement', function(owner, role, modelId)
{
	this.owner = owner;
	this.role = role;
	this.modelId = modelId;
	this.attributeName = tersus.convertProperty(role.substring(1,role.length-1));
	this.jsAttributeName = tersus.getJsAttributeName(this.attributeName);
});
tersus.StyleAttributeElement.prototype.replaceChild = function replaceChild(parent, value)
{
	DataElement.prototype.replaceChild.call(this, parent,value);
	if (parent.viewNode)
	{
		var v = value != null ? value.leafValue : null;
		if (v != null)
			parent.viewNode.style[this.jsAttributeName]= v;
	}
};
tersus.StyleAttributeElement.prototype.removeChild = function removeChild(parent)
{
	DataElement.prototype.removeChild.call(this, parent);
	if (parent.viewNode) // parent is a real display node
		parent.viewNode.style[this.jsAttributeName] = null;
};

tersus.StyleAttributeElement.prototype.getChild = function getChild(parent)
{
	if (parent.viewNode)
	{
		var leafValue = parent.viewNode.style[this.attributeName];
		var dataValue = this.createChildInstance();
		dataValue.setLeaf(leafValue);
		return dataValue;
	}
	else
		return DataElement.prototype.getChild.call(this, parent);
};
tersus.HTMLTag.prototype.formatProperties = tersus.GenericField.prototype.formatProperties;
tersus.HTMLTag.prototype.createFormatProps = tersus.GenericField.prototype.createFormatProps;
tersus.HTMLTag.prototype.reformat = tersus.GenericField.prototype.reformat;
for (var p in tersus.GenericField.prototype.formatProperties)
{
        tersus.GenericField.addFormatAccessor(tersus.HTMLTag.prototype, p,tersus.GenericField.prototype.formatProperties[p] );
};

tersus.HTMLTag.prototype.captionChanged = function()
{
	if(this.textTranslation && this.viewNode && this.textContainer && this.elements && this.elements['<Value>'])
	{
		this.setValueString();
	}
};
tersus.HTMLTag.prototype.setValueString = function()
{
	this.valueStr = '';
	if (this.value != null)
	{
		if (this.optimizeFormatString)
			this.valueStr = this.value.leafValue;
		else if (this.value.formatString)
			this.valueStr = this.value.formatString(this.formatProps ? this.formatProps : this.sharedProperties);
		else
			this.valueStr = this.value.toString();
	}
	if (this.viewNode)
	{
		if (this.inputField)
			this.inputField.value=this.valueStr;
		else
			this.textContainer.innerHTML = this.escapeHTML(this.translate(this.valueStr));
	}
};
tersus.HTMLTag.prototype.getFieldValue = tersus.HTMLTag.prototype['get<Value>'] = function getFieldValue()
{
    if (this.inputField)
    {       
    	var valueElement = this.getElement('<Value>');
        var valuePrototype = valueElement.getChildConstructor().prototype;
        this.value = valuePrototype.parseInput(this.inputField.value, this.formatProps ? this.formatProps : this.sharedProperties);
    }
    else if (this.viewNode && $(this.viewNode).prop('contenteditable') == 'true')
    {
	this.value = this.viewNode.innerHTML;
    }
    return this.value;
};
tersus.HTMLTag.prototype.setFieldValue = tersus.HTMLTag.prototype['set<Value>'] = tersus.HTMLTag.prototype['remove<Value>'] = function setFieldValue(value) 
{
        this.value = value;
        if (this.inputField && this.inputField.tagName =='INPUT')
        {
	        // Truncate to maxLength if needed (only if this is a text field)
	        if (this.maxLength && value && value.isText && value.leafValue && value.leafValue.length > this.maxLength)
	        {
	                this.value = new value.constructor();
	                this.value.leafValue =  value.leafValue.substring(0,this.maxLength); 
	        }
	    }
        this.setValueString();
};
tersus.HTMLTag.prototype.refresh = function()
{
		DisplayNode.prototype.refresh.call(this);
		var $this  = $(this.viewNode);
		if ($this.parents().hasClass('ui-page'))
			$this.pageUpdate();
};
tersus.HTMLTag.prototype.onSubmit = function(domNode, callback)
{
	if (window.trace)
	{
		window.trace.event(this, Events.ON_SUBMIT);
	}
	this.queueEvent(Events.ON_SUBMIT, null, null, callback);
	tersus.currentEvent.preventDefault();
};


tersus.HTMLTag.prototype.setFieldValue.requiresNodes = true;

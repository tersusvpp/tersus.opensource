/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 
// DisplayNode (a FlowNode that displays soemthing)
function DisplayNode()
{
};


DisplayNode.prototype = new FlowNode();
DisplayNode.prototype.visible = true;
DisplayNode.prototype.constructor = DisplayNode;
DisplayNode.prototype.type='Display';
DisplayNode.prototype.isDisplayNode=true;
  
DisplayNode.prototype.explicitReadOnly = false;
DisplayNode.prototype.VALIDATION_MESSAGE = '<Validation Message>';

DisplayNode.prototype.setViewNode = function DisplayNode_setViewNode(viewNode)
{
	this.viewNode = viewNode;
};
DisplayNode.prototype.getValidationContext = function DisplayNode_getValidationContext()
{
	if (this.parent && this.parent.getValidationContext)
		return this.parent.getValidationContext();
	else
		return this;
};
DisplayNode.findNodeForDocumentNode = function(viewNode)
{
	var nodeId = null;
	while (viewNode && viewNode.getAttribute && !nodeId)
	{
		nodeId = viewNode.getAttribute("lid"); // logic node id
		if (! nodeId)
			nodeId = viewNode.id;
		if (nodeId)
			return tersus.findNode(nodeId);
		viewNode = viewNode.parentNode;
	}
	return null; 
};
DisplayNode.prototype.escapeHTML = function(s)
{
	if (this.sharedProperties.allowHTMLTags != true && ! this.allowHTMLTags)
		return tersus.escapeHTML(s);
	else
		return s;
};
DisplayNode.prototype._onFocus = function()
{
	var parent = this.parent;
	while (parent)
	{
		if (parent.onChildFocus)
			parent.onChildFocus();
		parent = parent.parent;
	}
};

tersus.createEventHandler = function(eventType)
{
	// Default event handler method on DisplayNode
	if (eventType.methodName)
	{
		DisplayNode.prototype[eventType.methodName] = function(domNode, callback)
		{
			if (window.trace)
			{
				window.trace.event(this, eventType);
			}
			var baseMethodName = '_'+eventType.methodName;
			if (this[baseMethodName])
				this[baseMethodName]();
			this.queueEvent(eventType, null, null, callback);
		
		}
	}
};

for (var key in Events)
{
	tersus.createEventHandler(Events[key]);
};

tersus.createEventDispatcher = function(eventType, w)
{
	// Event dispatching global function
	if (eventType.jsName && eventType.methodName)
	{
		w.tersus_dispatchers[eventType.jsName] = function(e)
		{
			var eName = eventType.name;
			var $this = $(this);
			if ($this.hasClass(eName))
			{
				if (tlog) tlog ('Duplicate ' + eName + ' ignored  on ' + this.tagName);
				return;
			}
			$this.addClass(eName);
			var clear = function()
			{
				$this.removeClass(eName);
			};
			var ev = e;
			if (ev == null)
			{
				var w = getParentWindow(this);
				ev = w.event;
			}
			if (tersus.disabled == true || tersus.disabled && tersus.disabled[eventType.jsName] == true)
				return true;
			if (ev)
				tersus.currentEventAttributes = getEventAttributes(ev);
			tersus.currentEvent = ev;
			try
			{
				var node = DisplayNode.findNodeForDocumentNode(this);
				if (tlog)
				{
					var tagDesc = this.tagName;
					if (this.className)
						tagDesc+='.'+this.className;
					tlog('Captured event: '+ eventType.methodName + ' on '+tagDesc+' ['+ node.getPath().str+']');
				}
				
				if (node)
				{
					return node[eventType.methodName].call(node, this, clear);
				}
				else
					return true;
			}
			finally
			{
				tersus.currentEvent = null;
			}
		};
	}
};


tersus.createEventDispatchers = function t_addEvents(w)
{
	w.tersus_dispatchers = {};
	for (var key in Events)
	{
		tersus.createEventDispatcher(Events[key], w);
	}
};

tersus.createEventDispatchers(window);
// New implementation based on jQuery and jQuery Mobile
DisplayNode.prototype.registerEventHandler = function(eventType,domNode)
{
  	if (!domNode)
    {
            if (this.eventTargets)
                    domNode = this.eventTargets[eventType.jsName];
            if (!domNode)
                    domNode = this.viewNode;
    }
    domNode.setAttribute('lid',this.getNodeId());
    if (!eventType.jsName)
    {
            internalError('No event dispatcher defined for '+eventType.elementName);
            return;
    }
    
    var dispatcher = this.currentWindow.tersus_dispatchers[eventType.jsName];
    if (dispatcher)
    {
    	var jqName = eventType.jsName.substr(2);
    	
    	var doc = domNode.ownerDocument;
		var w = doc.defaultView || doc.parentWindow;
    	if (w == window && jqName == 'click' && $.event.special.vclick) // Virtual events don't work in pop-up
    		jqName = 'vclick';
        $(domNode).unbind(jqName,dispatcher);
        $(domNode).bind(jqName,dispatcher);
    }
    else
            internalError('No event dispatcher defined for '+eventType.jsName);
    
};

/* Old implementation */
/*
DisplayNode.prototype.registerEventHandler = function(eventType, domNode)
{
	if (!domNode)
	{
		if (this.eventTargets)
			domNode = this.eventTargets[eventType.jsName];
		if (!domNode)
			domNode = this.viewNode;
	 }
	domNode.setAttribute('lid',this.getNodeId());
	if (!eventType.jsName)
	{
		internalError('No event dispatcher defined for '+eventType.elementName);
		return;
	}
	
	var dispatcher = this.currentWindow.tersus_dispatchers[eventType.jsName];
	if (dispatcher)
		domNode[eventType.jsName] = dispatcher;
	else
		internalError('No event dispatcher defined for '+eventType.jsName);
	
};
*/
DisplayNode.prototype.checkBooleanProperty = function (name)
{
	return this.sprop(name) == true;
}
DisplayNode.prototype.sprop = function (name) // Can be made redundant if we prevent blanks on loading
{
	if (!this.sharedProperties) 
		return null;
	var p = this.sharedProperties[name];
	return (p !== ' ' ? p :null);
}
DisplayNode.prototype.copyValue = function(value)
{
	this.deepCopy(value);
};
DisplayNode.prototype.create = function DisplayNode_create(value)
{
	if (! (tersus.preferHTMLCreation && this.checkBooleanProperty('optimizeHTML')) )
	{
		this.setTextTranslation();
		this.createViewNode();
		this.attachViewNode();
		this.initViewNode();
		if (!this.initElement)
			this.createChildren();
	}
	if (value != null)
		this.copyValue(value);
	
};
DisplayNode.prototype.createChildren = function DisplayNode_createChildren()
{
	if (this.displayElements)
	{
		for (var i=0; i<this.displayElements.length; i++)
		{
			var e = this.displayElements[i];
			if ( !e.isRepetitive && ! e.isMethodElement && e.alwaysCreate)
			{
				if (!e.getChild(this))
					e.createChild(this);
			}
		}
	}
	
};
DisplayNode.prototype.setDisplayOrder = function setDisplayOrder()
{
	var displayOrder = this.element? this.element.displayOrder:1;
	this.viewNode.setAttribute('display_order',displayOrder);
};
DisplayNode.prototype.attachViewNode = function DisplayNode_attachViewNode()
{
	if (! this.viewNode)
		return;
	if (this.parent)
	{
		this.setDisplayOrder();
		this.parent.appendChildViewNode(this, this.element);
	}
};
DisplayNode.prototype.initViewNode = function DisplayNode_initViewNode()
{
	if (this.defaultStyleClass)
		this.setStyleClass(this.defaultStyleClass);
	this.setProperties();
	this.setEventHandlers();

}
tersus.removeEventHandlers = function(domNode)
{
	var doc = domNode.ownerDocument;
	var w = doc.defaultView || doc.parentWindow;
	for (key in Events)
	{
		var eventType = Events[key];
		if (eventType.jsName)
		{
			var dispatcher = w.tersus_dispatchers[eventType.jsName];
    		if (dispatcher)
		    {
		    	var jqName = eventType.jsName.substr(2);
		    	if (w == window && jqName == 'click' && $.event.special.vclick) // Virtual events don't work in pop-up
		    		jqName = 'vclick';
		        $(domNode).unbind(jqName,dispatcher);
		    }
		}
	}
	
};
DisplayNode.prototype.setEventHandlers = function DisplayNode_setEventHandlers()
{
	for (var key in Events)
	{
		var eventType = Events[key];
		if (this.hasEventHandler(eventType) && eventType.jsName || this['_'+eventType.methodName])
		{
			if (this.element)
				this.registerEventHandler(eventType);
			else
				this.registerEventHandler(eventType,document.body);
		}
	}
	if (this.hasEventHandler(Events.ON_LOCATION_CHANGE))
		tersus.locationChangeListeners.push(this.getNodeId());
}
DisplayNode.prototype.removeChildViewNode = function(child)
{
	if (child)
	{
		var top = child.wrapperNode ? child.wrapperNode : child.viewNode;
		if (top && top.parentNode)
			top.parentNode.removeChild(top);
	}
};
DisplayNode.prototype.appendChildViewNode = function appendChildViewNode(child, childElement)
{
	var childNode;
	if (child.wrapperNode)
	{
		childNode = child.wrapperNode; // Child wrapper already created - no need to wrap again 
	}
	else if (child.sharedProperties && (child.sharedProperties.wrapperTag != null) && (child.sharedProperties.wrapperTag != ' ') )
	{
		childNode = this.createHTMLElement(child.sharedProperties.wrapperTag, child.sharedProperties.wrapperStyleClass);
		childNode.setAttribute('display_order', child.viewNode.getAttribute('display_order'));
		childNode.appendChild(child.viewNode);
		child.wrapperNode = childNode;
	}
	else
		childNode = child.viewNode;
	if (child.placeHolderNode)
	{
		this.viewNode.replaceChild(childNode, child.placeHolderNode);
		child.placeHolderNode = null;
	}
	else	
	{
		DisplayNode.appendChildNode(this.viewNode, childNode);
	}
		
};

DisplayNode.appendChildNode = function(parent, newChild)
{
	var displayOrder = parseFloat(newChild.getAttribute('display_order'));
	var children = parent.childNodes;
	for( var i = 0; i<children.length; i++)
	{
		var child = children[i];
		if (child.getAttribute && parseFloat(child.getAttribute('display_order')) > displayOrder)
		{
			parent.insertBefore(newChild, child);
			return;
		}
	}
	parent.appendChild(newChild);
};
DisplayNode.prototype.forDisplayChildren = function (f)
{
	if (this.displayElements)
	{
		for (var i=0;i<this.displayElements.length;i++)
		{
			var e = this.displayElements[i];
			if (e.isRepetitive)
			{
				var list = e.getChildren(this);
				for (var j=0;j<list.length;j++)
				{
					var child = list[j];
					if (child)
						if (f.call(null,child))
							reutrn;
				}
			}
			else
			{
				child = e.getChild(this);
				if (child)
					if (f.call(null,child))
						return;
			}
		}
	}
}	

DisplayNode.prototype.deleteMe = function()
{
	var element = this.element;
	var parent= this.parent;
	if (! parent)
		return;
	if (element.isRepetitive)
	{
		var siblings = element.getChildren(parent);
		for (var i=0;i<siblings.length;i++)
		{
			if (siblings[i] == this)
			{
				siblings.splice(i,1); // removes element at index i
				this.onDelete();
				return;
			}
		}
	}
	else
		element.removeChild(parent);
};
DisplayNode.prototype.destroy = function()
{
	FlowNode.prototype.destroy.call(this);
	// Remove event listener
	if (this.hasEventHandler(Events.ON_LOCATION_CHANGE))
	{
		// Remove this node's id from the tersus.locationChangeListeners
		var listeners = [];
		for (var i=0;i<tersus.locationChangeListeners.length;i++)
		{
			if (tersus.locationChangeListeners[i] != this.getNodeId())
				listeners.push(tersus.locationChangeListeners[i]);
		}
		tersus.locationChangeListeners = listeners;
	};
	
	if (this.hasEventHandler(Events.ON_DELETE))
	{
		var process = new BackgroundProcess(this, this.getEventHandler(Events.ON_DELETE));
		tersus.async.exec(function(){process.start()}, false /* don't wait */); 
	};
};
DisplayNode.prototype.onDelete = function onDelete()
{
	this.destroyHierarchy();
	if (this.parent)
		this.parent.removeChildViewNode(this);
};

DisplayNode.prototype.createHTMLElement = function createHTMLElement(tag, styleClass)
{
	var element = this.currentWindow.document.createElement(tag);
	if (styleClass && styleClass != ' ')
		element.className=styleClass;
	return element;
};
DisplayNode.prototype.createHTMLChildElement = function createHTMLChildElement(parentNode,tag, styleClass)
{
	var childNode = this.createHTMLElement(tag, styleClass);
	parentNode.appendChild(childNode);
	return childNode;
};
DisplayNode.prototype.getTag = function getTag()
{
	var t = this.sprop('tag');
	if (!t)
		t = this.defaultTag;
	return t;
};
DisplayNode.prototype.createViewNode = function DisplayNode_createViewNode()
{
	var tag = this.getTag();
	this.viewNode = this.createHTMLElement(tag);
	this.viewNode.setAttribute('lid',this.getNodeId());
};
DisplayNode.prototype.setProperties = function setProperties()
{
	var props = this.sharedProperties;
	if (!props)
		return;
	if (props.styleClass && props.styleClass != ' ')
		this.setStyleClass(props.styleClass);
	else if (this.defaultStyleClass)
		this.setStyleClass(this.defaultStyleClass);
	this.setStyle(props.style);
	if (props.label)
		this.viewNode.innerHTML = props.label;
/*	if (props.textTranslation !== null || props.textTranslaton !== undefined)
	{
		if (props.textTranslation == false)
			this.textTranslation = false;
		else
			this.textTranslation = true;
	}
*/	var tag = this.viewNode.tagName;
	for (var p in props)
	{
		var value = props[p];
		if ( value != ' ')
		{
			if ((p == 'width'|| p=='height') && value.search(/^[0-9]+$/)>=0)
				value = value+'px';
				
			if (HTMLProperties[tag] && HTMLProperties[tag][p])
				this.viewNode[p]=value;
			else if (STYLE_PROPERTIES[p])
				this.viewNode.style[STYLE_PROPERTIES[p]]=value;
		}
	}
};
DisplayNode.prototype.setTextTranslation = function setTextTranslation()
{
	var textTranslation = this.sharedProperties.textTranslation;
	if (textTranslation !== null && textTranslation !== undefined)
	{
		if (textTranslation == false)
			this.textTranslation = false;
		else
			this.textTranslation = true;
	}
};
DisplayNode.prototype.removeAll = function DisplayNode_removeAll()
{
	if (this.elements)
	{
		for (var i=0; i<this.elementList.length; i++)
		{
			var e = this.elementList[i];
			if (e.isRemovable)
			{
				if (e.isRepetitive)
				{
					e.removeChildren(this);
				}
				else
				{
					e.removeChild(this);
				}
			}
		}
	}
}
DisplayNode.prototype.sharedProperties = DisplayNode.prototype.NO_PROPERTIES = {};
DisplayNode.prototype.addDisplayElement = function addElement(role, modelId, properties)
{
	if (this.getGetter(role))
	{
		var element = new MethodElement(this, role, modelId);
		element.modelId = modelId;
	}
	else
		var element = new DisplayElement(role, modelId);
	if (! this.displayElements)
		this.displayElements = [element];
	else
		this.displayElements.push(element);
	element.displayOrder = this.displayElements.length;
	this.registerSubFlow(element);
	element.properties = properties ? properties : this.NO_PROPERTIES;
	return this.addElement(element);
};
DisplayNode.prototype.optimizeFormatString = false;
DisplayNode.prototype.onload = function()
{
	this.optimizeFormatString = 
		(this.sprop('decimalPlaces') == null || this.sprop('decimalPlaces') == -1)
		&&
	 	this.sprop('useThousandsSeparator') == null &&
	 	this.sprop('currencySymbol') == null;
	 this.htmlTagSuffix = '</'+this.getTag()+'>';
	 this.hasElementChangeHandler =  this.hasEventHandler(Events.ON_ELEMENT_CHANGE);
	 this.initElement = this.getElement('<Init>');
	 this.errorHandler = this.getElement('<On Error>');
}
DisplayNode.prototype.handleElementChange = function (role, oldValue, newValue)
{
	var parameters = {'<Element Name>':role, '<Previous Value>':oldValue, '<New Value>':newValue};
	this.queueEvent(Events.ON_ELEMENT_CHANGE,parameters,null /* No GUI Attributes */);	
}

DisplayNode.prototype['set<Value>'] = function setValue(value)
{
	this.value = value;
	this.setValueString();
};
DisplayNode.prototype['set<Value>'].requiresNodes = true;
DisplayNode.prototype.setValueString = function()
{
	this.valueStr = '';
	if (this.value != null)
	{
		if (this.optimizeFormatString)
			this.valueStr = this.value.leafValue;
		else if (this.value.formatString)
			this.valueStr = this.value.formatString(this.sharedProperties);
		else
			this.valueStr = this.value.toString();
	}
	if (this.viewNode)
		this.viewNode.innerHTML = this.escapeHTML(this.valueStr);
};
DisplayNode.prototype['remove<Value>'] = function removeVaule()
{
	this.value = null;
	if (this.viewNode)
		this.viewNode.innerHTML='';
};
DisplayNode.prototype['set<Labels>'] = function setLabels(map)
{
	tersus.labelMap = map;
	var op = function(node)
	{
		if (node.captionChanged && node.viewNode)
		{
	        try
	        {
	            node.captionChanged();
	        }
	        catch (e)
	        {
	            if (console && console.log)
	                console.log('Error updating caption: path='+node.getPath().str + '\nError='+(e.message? e.message:e));
	            throw 'Error updating caption';
	        }
		}
		
	};
	tersus.traverseHierarchy(window.rootDisplayNode, tersus.Node.childIterator, op);
};
DisplayNode.prototype['get<Labels>'] = function getLabels()
{
	return tersus.labelMap;
};
DisplayNode.prototype['set<Resources>'] = function setLabels(map)
{
	tersus.resourceMap = map;
};
DisplayNode.prototype['get<Resources>'] = function getLabels()
{
	return tersus.resourceMap;
};
DisplayNode.prototype['set<Direction>'] = function DisplayNode_setDirection(direction)
{
	tersus.setDirection(direction);
};
DisplayNode.prototype['get<Direction>'] = function getDirection()
{
	return tersus.getDirection();
};

DisplayNode.prototype['get<Visible>'] = function ()
{
	return this.visible;
};

DisplayNode.prototype['set<Visible>'] = function (visible)
{
	this.visible = visible;
	this.refreshVisibility();
};
DisplayNode.prototype.isInvisible = function()
{
	var visible = this.visible;
	var parent = this.parent;
	while (visible && parent && parent.isGroup)
	{
		visible = parent.visible;
		parent = parent.parent;
	}
	return !visible;	
}
DisplayNode.prototype.refreshVisibility = function()
{
	var domNode = this.wrapperNode ? this.wrapperNode : this.viewNode;
	if (domNode)
	{
		if (this.isInvisible())
			domNode.style.display='none';
		else
		{
			domNode.style.display='';
			this.onResize();
		}
	}
};

DisplayNode.prototype.styleClass = '';
// baseClassName - the class name defined by the application, not taking into account special conditions like selection
DisplayNode.prototype.baseClassName = ''; 
DisplayNode.prototype.extraClassName = ''; 
DisplayNode.prototype.setStyleClass = function setStyleClass(value)
{
	if (this.viewNode)
	{
		var className = value;
		
		//rtl logic should be moved to a legacy script
		var rtlClassName = value+'_rtl';
		if (window.textDirection == 'rtl' && tersus.styleClassNames && tersus.styleClassNames.contains(rtlClassName))
			className = rtlClassName;
		$(this.viewNode).removeClass(this.baseClassName).addClass(className);
		this.baseClassName = className;
	}
	this.styleClass = value;
};
DisplayNode.prototype.addStyleClass = function addStyleClass(value)
{
	var elementClass = this.getLeafChild('<Style Class>');
	if (!elementClass)
		elementClass = this.viewNode ? this.styleClass : this.sharedProperties.styleClass;
	if (!elementClass)
		elementClass = value;
	else
	{
		var orig = ' ' + elementClass + ' ';
		var classNames = value.split(/\s+/);
		for (var i=0;i<classNames.length;i++)
		{
			var c=classNames[i];
			if (orig.indexOf(' '+c+' ') < 0)
				elementClass += ' '+c;
		}
	}
	this.setLeafChild('<Style Class>',$.trim(elementClass), Operation.REPLACE);
	if (!this.getLeafChild('<Style Class>'))
		this.setStyleClass($.trim(elementClass));

};
DisplayNode.prototype.removeStyleClass = function removeStyleClass(value)
{
	var elementClass = this.getLeafChild('<Style Class>');
	if (!elementClass)
	{
		elementClass = this.viewNode ? this.styleClass : this.sharedProperties.styleClass;
		if (!elementClass)
			return;
	}

	elementClass = ' '+elementClass+' ';
	var classNames = value.split(/\s+/);
	for (var i=0;i<classNames.length;i++)
	{
		elementClass = elementClass.replace(' '+classNames[i]+' ',' ');
	}
	this.setLeafChild('<Style Class>',$.trim(elementClass), Operation.REPLACE);
	if (!this.getLeafChild('<Style Class>'))
		this.setStyleClass($.trim(elementClass));
};
DisplayNode.prototype['set<Style Class>'] = function(value)
{
	this.setStyleClass(value);
};
 DisplayNode.prototype['get<Style Class>'] = function getStyleClass()
{
	return this.styleClass;
};
DisplayNode.prototype.setStyle =  DisplayNode.prototype['set<Style>'] = function(value)
{
	if (this.viewNode && value != null)
	{
		try
		{
			return this.viewNode.style.cssText = value;
		}
		catch (e)
		{
			modelExecutionError('Failed to set style "'+value+'"  (HTML tag='+this.viewNode.tagName+')',this);
		}
	}
};
DisplayNode.prototype['get<Style>'] = function getStyleClass()
{
	if (this.viewNode)
		return this.viewNode.style.cssText;
};
DisplayNode.prototype['get<Value>'] = function getValue()
{
	return this.value;
}
DisplayNode.prototype.getBaseCaption = DisplayNode.prototype['get<Caption>'] = function()
{
	if (this.caption !== null &&  this.caption !== undefined)
		return this.caption;
	var c = this.sprop('caption');
	if (c != null)
		return c;
	else
		return this.element.role;
	
};
DisplayNode.prototype.getCaption = function()
{
	var idTranslation = this.getTranslation(this.modelId);
	if(idTranslation !== null && idTranslation !== undefined)
		return idTranslation; 
	else
		return this.translate(this.getBaseCaption());
};
DisplayNode.prototype.captionChanged = function() {};
DisplayNode.prototype['set<Caption>'] = function(caption)
{
	this.caption = caption;
	this.captionChanged();
};
DisplayNode.prototype['get<Read Only>'] = function ()
{
	return this.explicitReadOnly;
};
DisplayNode.prototype['set<Read Only>'] = function (readOnly)
{
	if (this.explicitReadOnly != readOnly)
	{
		var originalState = this.computedReadOnly();
		this.explicitReadOnly = readOnly;
		var newState = this.computedReadOnly();
		if (this.newState != originalState) 
		{
			this.updateSelfReadOnly(newState);
			this.updateChildrenReadOnly(newState);
		}
		
	}
};
DisplayNode.prototype.updateSelfReadOnly = function ()
{
};
DisplayNode.prototype.updateChildrenReadOnly = function DisplayNode_updateChildrenReadOnly(readOnly)
{
	if (! this.displayElements)
		return;
	for (var i=0; i<this.displayElements.length; i++)
	{
		var element = this.displayElements[i];
		if (element.isRepetitive)
		{
			var children = element.getChildren(this);
			for (var j=0; j<children.length;j++)
			{
				var child = children[j];
				if (! child.explicitReadOnly) // If the child is explicitly read only, its state won't change
					child.parentReadOnlyChanged(readOnly);
			}
		}
		else
		{
			var child = element.getChild(this);
			if (child && ! child.explicitReadOnly) // If the child is explicitly read only, its state won't change
				child.parentReadOnlyChanged(readOnly);
		}
	}
};
DisplayNode.prototype.parentReadOnlyChanged = function(readOnly)
{
	this.updateSelfReadOnly(readOnly);
	this.updateChildrenReadOnly(readOnly)
};
/**
 * Computes the read only state of this node.  A node is in read only mode if it's explicitly
 * marked as read only, or if its ancestor is read only
 */	
DisplayNode.prototype.computedReadOnly = function DisplayNode_computedReadOnly()
{
	if (this.explicitReadOnly)
		return true;
	else if (this.parent && this.parent.computedReadOnly)
		return this.parent.computedReadOnly();
	else
		return false;
};
DisplayNode.prototype.onResizeChildren = function onResizeChildren()
{
	if (this.displayElements)
	{
		for (var i=0; i<this.displayElements.length; i++)
		{
			var element = this.displayElements[i];
			if (element.isRepetitive)
			{
				var children = element.getChildren(this);
				for (var j=0; j<children.length;j++)
				{
					var child = children[j];
					child.onResize();
				}
			}
			else
			{
				var child = element.getChild(this);
				if (child)
					child.onResize();
			}
		}
	}
};
DisplayNode.prototype.updateLayout = function(domNode)
{
	var $this=$(domNode);
	if ($this.hasClass('ter-max-height'))
	{
		if (window.console) window.console.log(this.modelId+ 'before resize -  height:'+$this.height()+' innerHeight:'+$this.innerHeight());
		/* For the top level element (no this.parent), we need to use min-height as the height be too large 
		  (a separate method updates the container's min-height according to the available dimensions) */
		  
		var p = domNode.parentNode;
		var $p = $(p);
		var ps = p.style;
/*		if (window.console)
		    console.log('p.id='+$p.attr('id')+' p.class='+$p.attr('class')+' $p.height()' +$p.height(), ' ps.height:'+ps.height +'  ps.minHeight:'+ps.minHeight +'  $p.css(height):'+ $p.css('height') +' $p.css(minHeight):'+ $p.css('minHeight'));
*/
		var h  = parseInt(ps.height || ps.minHeight || $p.css('height') || $p.css('minHeight')); // Need to give preference to explicitly set height
		if ($p.css('box-sizing')=='border-box')
		    h = h - ($p.outerHeight()-$p.height()); // total height of padding
		var s= 0;
		$this.siblings().each(function(){s+=$(this).outerHeight();});
		var m = 0, $prev=$(), $all=$this.parent().children(), n = $all.size();
		$all.each(function(i){
			var $cur =  $(this);
			m+=Math.max(parseInt($prev.css('margin-bottom'))||0, parseInt($cur.css('margin-top'))||0);
			if (i == n-1)
				m+=parseInt($cur.css('margin-bottom'))||0;
			$prev=$cur;
		});
		if (window.console) console.log(this.modelId+': h='+h+ ' s='+s+' m='+m);
		h = h - s - m;
		$this.css({'height':h+'px','box-sizing':'border-box'});
	}
	else if ($this.hasClass('ter-parent-height'))
	{
//		if (window.console) console.log(this.modelId+ 'before resize -  height:'+$this.height()+' innerHeight:'+$this.innerHeight());
//		if (window.console) console.log(this.modelId+ ' parentHeight:'+$this.parent().height()+' parentInnerHeight:'+$this.parent().innerHeight());
		$this.css({'height':$this.parent().height()+'px','box-sizing':'border-box'});
	}
	
};
DisplayNode.prototype.onResize = function()
{
	if(this.wrapperNode && this.wrapperNode != this.viewNode)
	{
		this.updateLayout(this.wrapperNode);
	}
	else if (this.viewNode)
	{
		this.updateLayout(this.viewNode);
	}
	this.onResizeChildren();
};

DisplayNode.prototype.isDisplayNode = true;
/******************************************************************
 *  reset() is called before replacing a display element, and does not trigger initialization processes
 *  refresh() is called when refreshing a display element, and includes initiailization processes (createSubFlows).
 *
 *  If reset triggered initialization processes, we might end up in strange situations, like double initialization
 *   (e.g. If a chooser initializes itself, and then a different chooser is copied onto it)
 *
 *
 ****************************************************************** */
DisplayNode.prototype.reset = function DisplayNode_reset()
{
	this.removeAll();
	this.initViewNode();
	this.createChildren(); // TODO(x) eliminate redundant creation of children (not sure whether this really is redundant as reset() is overridden in HTMLTag)
};
DisplayNode.prototype.refresh = function DisplayNode_refresh()
{
	this.reset();
	this.createSubFlows();
};


DisplayNode.prototype.deepCopy = function deepCopy(value)
{
	if (tlog) tlog("start deepCopy:"+value.modelId);
	for (var elementRole in value.elements)
	{
		var element = this.getElement(elementRole);
		if (element)
		{
			if (element.isRepetitive)
			{
				var childValues = value.getChildren(elementRole);
				element.setChildren(this, childValues, Operation.REPLACE);
			}
			else
			{
				var childValue = value.getChild(elementRole);
				if (childValue != null)
				{
					element.replaceChild(this, childValue);
				}
			}
		}
	}
	if (tlog) tlog("end deepCopy:"+value.modelId);
};
DisplayNode.prototype.tooltip = null;
DisplayNode.prototype.setTooltip = DisplayNode.prototype['set<Tooltip>'] = function (tooltip)
{
	var tooltipClassName = 'tooltip';
	this.tooltip = tooltip;
	if (this.viewNode && tooltip !== null && tooltip !== undefined)
	{
		this.viewNode.title = this.translate(tooltip);
		if (tooltip != '')
			this.addStyleClass(tooltipClassName);
		else
			this.removeStyleClass(tooltipClassName);
	};
};
DisplayNode.prototype.getTooltip = DisplayNode.prototype['get<Tooltip>'] = function ()
{
	return this.tooltip;
};
DisplayNode.prototype.setValidationMessage = DisplayNode.prototype['set<Validation Message>']= function (message)
{
	var invalidClassName = 'invalid';
	this.validationMessage = message;
	if (this.currentWindow && this.viewNode)
	{
		if (message != null)
		{
			this.addStyleClass(invalidClassName);
			this.viewNode.title = this.forceTranslate(message);
		}
		else
		{
			this.removeStyleClass(invalidClassName);
			this.viewNode.title='';
		}
	}
};
DisplayNode.prototype['remove<Validation Message>']= function ()
{
	this.setValidationMessage(null);
};
DisplayNode.prototype.getValidationMessage = DisplayNode.prototype['get<Validation Message>']= function (message)
{
	return this.validationMessage;
};

DisplayNode.prototype.showValidationMessage = function (message)
{
	if (this.currentWindow.alert) this.currentWindow.alert(message);
};
DisplayNode.prototype.validate = function validate()
{
	var message = this.getValidationMessage();
	if (message != null)
	{
		this.showValidationMessage(this.forceTranslate(message));
		return false;
	}
	if (! this.elementList)
		return true;
	for (var j=0; j<this.elementList.length; j++)
	{
		var element = this.elementList[j];
		if (element.isRepetitive)
		{
			var values = element.getChildren(this);
			for (var i=0; i< values.length; i++)
			{
				var value = values[i];
				if (value && value.validate && ! value.validate())
					return false;
				
					
			}
		}
		else
		{
			var value = element.getChild(this);
			if (value && value.validate && ! value.validate())
				return false;
		}
	}
	return true;
};
DisplayNode.prototype.findDisplayElementIndex = function (role)
{
	for (var i=0; i<this.displayElements.length;i++)
	{
		var e = this.displayElements[i];
		if (e.role ==role)
			return i;
	}
	return null;
};
DisplayNode.prototype.findPreviousSibling = function (child)
{
	var role = child.element.role;
	
	// Find the last child among the preceding display elements
	var index = this.findDisplayElementIndex(role);
	if (index == null)
		return null;
	for (var i=index;i>=0;i--)
	{
		var e = this.displayElements[i];
		if (e.isRepetitive)
		{
			var children = e.getChildren(this);
			for (var j=children.length-1; j>=0; j--)
			{
				var sibling = children[j];
				if (sibling && sibling != child)
					return sibling;
			}
		}
		else
		{
			var sibling = e.getChild(this);
			if (sibling && sibling != child)
				return sibling;
		}
	}
	return null;
};
DisplayNode.prototype.focusOnFirstField = function()
{
	var elements = this.viewNode.getElementsByTagName('*');
	for (var i=0; i<elements.length; i++)
	{
		var e = elements[i];
		if (e.tagName == 'INPUT' || e.tagName == 'TEXTAREA')// || e.tagName == 'SELECT')
		{
			if (isVisible(e) && !e.disabled)
			{
				this.currentWindow.setTimeout(function(){if (isVisible(e) && !e.disabled) e.focus();},0);
				break;
			}
		}
	}
};


DisplayNode.prototype.addGetter('<Screen X>', function()
{
	if (!this.viewNode)
		return null;
	return getScreenPosition(this.viewNode).left;
});
DisplayNode.prototype.addSetter('<Screen X>', function(x)
{
	this.viewNode.style.position='absolute';
	this.viewNode.style.left = (x - getScroll(this.viewNode).left)+'px';
});

DisplayNode.prototype.addGetter('<Screen Y>', function()
{
	if (!this.viewNode)
		return null;
	if (this.viewNode.tagName == 'SPAN')
	{
		if (this.inputField) // This happens in input fields
			return getScreenPosition(this.inputField).top;
		else
			internalError("Getting the vertical position of 'SPAN' elements not supported yet");
	}
	else
	return getScreenPosition(this.viewNode).top;
});
DisplayNode.prototype.addSetter('<Screen Y>', function(y)
{
	this.viewNode.style.position='absolute';
	this.viewNode.style.top = (y - getScroll(this.viewNode).top)+'px';
});

DisplayNode.prototype.addGetter('<URL>', function()
{
	if (this.viewNode)
		return this.viewNode.href;
});
DisplayNode.prototype.addSetter('<URL>', function(url)
{
	if (this.viewNode)
		this.viewNode.href = url;
});
DisplayNode.prototype.addGetter('<Target Window>', function()
{
	if (this.viewNode)
		return this.viewNode.target;
});
DisplayNode.prototype.addSetter('<Target Window>', function(target)
{
	if (this.viewNode)
		this.viewNode.target = target;
});
DisplayNode.prototype.addGetter('<Width>', function()
{
	if (this.viewNode)
		return getVisibleSize(this.viewNode).width;
	else
		return this.width;
});
DisplayNode.prototype.addSetter('<Width>', function(width)
{
	this.width = width;
	if (this.viewNode)
		this.viewNode.style.width = width  + 'px';
});
DisplayNode.prototype.addGetter('<Height>', function()
{
	if (! this.viewNode)
		return this.height;
	if (this.viewNode.tagName == 'SPAN')
	{
		if (this.inputField) // This happens in input fields
			return getVisibleSize(this.inputField).height;
		else
			internalError("Getting the height of 'SPAN' elements not supported yet");
	}
	else
		return getVisibleSize(this.viewNode).height;
});
DisplayNode.prototype.addSetter('<Height>', function(height)
{
	this.height = height;
	if (this.viewNode)
		this.viewNode.style.height = height+'px';
});
DisplayNode.prototype.addGetter('<Col Span>', function()
{
	if (this.viewNode && this.viewNode.parentNode && this.viewNode.parentNode.colSpan)
		return this.viewNode.parentNode.colSpan;
	else
		return this.colSpan;
});
DisplayNode.prototype.addSetter('<Col Span>', function(span)
{
	this.colSpan = span;
	if (this.viewNode && this.viewNode.parentNode && this.viewNode.parentNode.colSpan)
		this.viewNode.parentNode.colSpan = span;
});
DisplayNode.prototype.addGetter('<Row Span>', function()
{
	if (this.viewNode && this.viewNode.parentNode && this.viewNode.parentNode.rowSpan)
		return this.viewNode.parentNode.rowSpan;
	else
		return this.rowSpan;
});
DisplayNode.prototype.addSetter('<Row Span>', function(span)
{
	this.rowSpan = span;
	if (this.viewNode && this.viewNode.parentNode && this.viewNode.parentNode.rowSpan)
		this.viewNode.parentNode.rowSpan = span;
});

DisplayNode.displayChildrenIterator = function (node,iteration)
{
	if (node && node.displayElements)
	{
		for (var i=0; i<node.displayElements.length; i++)
		{
			var e = node.displayElements[i];
			if ( e.isRepetitive)
			{
				var children  = e.getChildren(node);
				for (var j=0;j<children.length;j++)
				{
					var stop = iteration(children[j]);
					if (stop == true)
						return;
				}
			}
			else
			{
				var child = e.getChild(node);
				if (child)
				{
					var stop = iteration(child);
					if (stop == true)
						return;
				}
			}
		}
	}
	
};

DisplayNode.prototype.prop = function(name)
{
    var e = this.element;
    var p = (e && e.props) || this.props;
    if (!p) // Initialize combined element+model property map on first time
    {
        p = {};
        $.extend(p,this.baseProperties);
        p['class'] = this.defaultClass || this.defaultStyleClass;
        p.tag = this.defaultTag;
        for (var n in this.sharedProperties)
        {
            var v = this.sharedProperties[n];
            if (v != null && v !== ' ')
                p[tersus.convertProperty(n)]=v;
        } 
        if (e)
        {
            for (var n in e.properties)
            {
                var v = e.properties[n];
                if (v != null && v !== ' ')
                    p[tersus.convertProperty(n)]=v;
            }
            e.props = p;
        }
        else
            this.constructor.prototype.props = e;
    }
    return p[name];
}

/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 
function MethodElement(owner, role, modelId)
{
	this.owner = owner;
	this.role = role;
	debugAssertion(modelId != null, "Expecting MethodElements to always have a model Id");
	if (modelId)
		this.modelId = modelId;
	else
		this.modelId = BuiltinModels.TEXT_ID; //TODO - remove this once we're sure that modelId is always given
	if (owner.getAdder(role)) // TODO - this should be a validation
		this.isRepetitive = true;
	else
		this.isRepetitive = false;
	
	if (! owner.getRemover(this.role))
		this.isRemovable = false;
};
MethodElement.prototype = new Element();
MethodElement.prototype.constructor = MethodElement;
MethodElement.prototype.type = 'Data Element';
MethodElement.prototype.isMethodElement = true;
MethodElement.prototype.replaceChild = function replaceChild(parent, value)
{
	if (parent.currentWindow) // parent is a real display node
	{
		var setter = parent.getSetter(this.role);
		if (setter == null)
			return;
		if (setter.requiresNodes)
			setter.call(parent, value);
		else
		{
			var  leafValue = value.leafValue;
			setter.call(parent, leafValue);
		}
	}
	else
		Element.prototype.replaceChild.call(this, parent,value);
	
};
MethodElement.prototype.addAChild = function addAChild(parent, value)
{
	if (parent.currentWindow) // parent is a real display node
	{
		var adder = parent.getAdder(this.role);
		if (adder.requiresNodes)
			adder.call(parent, value);
		else
		{
			var  leafValue = value.leafValue;
			adder.call(parent, leafValue);
		}
	}
	else
		Element.prototype.addAChild.call(this, parent,value);
	
};
MethodElement.prototype.removeChild = function removeChild(parent)
{
	if (parent.currentWindow) // parent is a real display node
	{
		var remover = parent.getRemover(this.role);
		if (remover)
			remover.call(parent);
	}
	else
		Element.prototype.removeChild.call(this, parent);
};

MethodElement.prototype.getChild = function getChild(parent)
{
	if (parent.currentWindow || parent.isDataValue) // parent is a real display node or a DataValue
	{
		var getter = parent.getGetter(this.role);
		var value = getter.call(parent);
	        if (value !== null && value !== undefined)
		{
		    if (value.isNode)
				return value;
			else
			{
				//This is a primitive value - wrap it as a DataValue
				var dataValue = this.createChildInstance();
				dataValue.setLeaf(value);
				return dataValue;
			}
		}
		else
		return null;
	}
	else // parent is a 'temporary' display-data node
	{
		return Element.prototype.getChild.call(this, parent);
	}
};

MethodElement.prototype.getChildren = function getChildren(parent)
{
	if (parent.currentWindow || parent.isDataValue) // parent is a real display node
	{
		var getter = parent.getGetter(this.role);
		var values = getter.call(parent);
		if (values.length > 0 && ! values[0].isNode) // The getter returned raw(leaf) values, not nodes
		{
			var leafValues = values
			values = [];
			if (leafValues.length)
			{
				for (var i=0; i<leafValues.length; i++)
				{
					var leafValue = leafValues[i];
					if (leafValue !== null && leafValue !== undefined)
					{
						var dataValue = this.createChildInstance();
						dataValue.setLeaf(leafValue);
						values.push(dataValue);
					}
				}
			}
		}
		return values;
	}
	else // parent is a 'temporary' display-data node
	{
		return Element.prototype.getChildren.call(this, parent);
	}
	
};
MethodElement.prototype.removeChildren = function removeChildren(parent)
{
	if (parent.currentWindow) // parent is a real display node
	{
		var remover = parent.getRemover(this.role);
		remover.call(parent);
	}
	else // parent is a 'temporary' display-data node
	{
		Element.prototype.removeChildren.call(this, parent);
	}
		
};


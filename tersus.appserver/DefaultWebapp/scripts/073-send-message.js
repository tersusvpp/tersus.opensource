/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function sendMessage(adapter, message, traceFileName)
{
	var url=tersus.rootURL+'File/'+encodeURIComponent(adapter);
	var req = createXMLHTTPRequest();
	var serviceNode = this;
	req.open('POST', url, true);
	var b = []
	Serializer.prototype._quote(message, b);
	req.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	var requestBody='Message='+window.encodeURIComponent(b.join(''));
	if (traceFileName)
		requestBody+='&_traceFileName='+window.encodeURIComponent(traceFileName);
	req.send(requestBody);
	
}
/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
     if (!window.tersus) window.tersus  = {};
if (!tersus.settings) tersus.settings = {};
window.tlog = null;
tersus.isLocalHost = function(host)
{
    return host == 'localhost' || host =='127.0.0.1' || host == '0:0:0:0:0:0:0:1%0';
};
tersus.isNative=false;//Overridden in native context

tersus.getTraceBaseURL = function() /* Can be overridden in specific cases to direct client-side trace to another server */
{
    var url = tersus.serverURL || tersus.rootURL;
    if (url.lastIndexOf('/') != url.length -1)
	url = url + '/';
    url = url+'Trace/';
    return url;
};
tersus.useIFrames=true;
tersus.ANYTHING = 'Common/Data Types/Anything';
tersus.loadScript = function loadScript(url,w)
{
    if (!w)
	w = window;
    var loadScript = w.document.createElement("script");

    // Add script object attributes
    loadScript.setAttribute("type", "text/javascript");
    loadScript.setAttribute("charset", "utf-8");
    loadScript.setAttribute("src", url);
    var head = w.document.getElementsByTagName("head").item(0);
    head.appendChild(loadScript);

};
tersus.topZindex = 10;
tersus.SERVICE_MAX_RETRIES = 2;
tersus.SERVICE_RETRY_DELAY = 2000;
tersus.alertMode = {interactive:true};
tersus.getEvent = function(elementName)
{
    var eventSpec = null;
    for (var eventKey in Events)
    {
        if (Events[eventKey].elementName == elementName)
            eventSpec = Events[eventKey];
    }
    if (!eventSpec)
        eventSpec = new tersus.Event(elementName); // Ad-hoc event
    return eventSpec;

};
tersus.invokeAction=function(eventName,parameters, domEvent, targetNode, wait)
{
    /* wait - indicates that the event should silently wait until previous events finished processing
     *  - (rather than timeout with "Busy" ).  */
    if (tlog) tlog('invokeAction called ['+eventName+']');
    if (!targetNode)
	targetNode = window.currentRootDisplayNode;
    if (targetNode)
    {
	var eventAttributes = null;
	if (domEvent)
	    eventAttributes = getEventAttributes(domEvent);

	if (tlog)
	    tlog('Queuing Event ..');
	//window.engine.handleEvent(targetNode,eventSpec,parameters,eventAttributes);
	var eventSpec = tersus.getEvent(eventName);
	targetNode.queueEvent(eventSpec,  parameters, domEvent, null /*callback*/, wait);
    }
};
window.tersus.clearLog = function()
{
    window.tersus._log = [];
    window.tlog  = window.tersus.log;
};
tersus.formatTime = function(time)
{
    var p2 = tersus.DateAndTime.pad2;
    return  p2(time.getHours())+':'+p2(time.getMinutes())+':'+p2(time.getSeconds());
};

tersus.useXMLHTTPRequest = true;
tersus.autoLogout = false;
window.tersus.log = function(msg1, msg2, msg3)
{
    var now=new Date();
    var time = now.getTime();
    if (l = window.tersus._log)
    {
	l.push(time);
	l.push('\t');
	l.push(tersus.escapeHTML(msg1));
	if (msg2 != null)
	{
	    l.push('\t');
	    l.push(msg2);
	}
	if (msg3 != null)
	{
	    l.push('\t');
	    l.push(msg3);
	}
	l.push('\n');
    }
};
window.tersus.showLog = function()
{
    if (window.tersus._log)
    {
	var w = window.open();
	w.document.write('<pre>');
	w.document.write(window.tersus._log.join(''));
	w.document.close();
    }
    window.tersus.clearLog();
};
window.tersus.escapeHTML = function(s)
{
    if (s == null)
	return '';
    if (typeof(s) != 'string')
	s = s.toString();
    s = s.replace(/&/g, '&amp;');
    s = s.replace(/</g, '&lt;');
    s = s.replace(/>/g, '&gt;');
    s = s.replace(/\n/g, '<br/>');
    s = s.replace(/["]/g, '&quot;');
    return s;
};
window.tersus.escapeHTMLAttribute = function(s)
{
    if (s == null)
        return '';
    if (typeof(s) != 'string')
        s = s.toString();
    s = s.replace(/&/g, '&amp;');
    s = s.replace(/</g, '&lt;');
    s = s.replace(/>/g, '&gt;');
    s = s.replace(/\n/g, '&#013;');
    s = s.replace(/["]/g, '&quot;');
    return s;
};
tersus.onUnload = function()
{
    if (tersus.Subscribe && tersus.Subscribe.xhr)
    {
	tersus.Subscribe.xhr.abort();
    }
    if (! tersus.reloading) // the onunload event should not be fired when we're reloading
    {
	tersus.isUnloading = true;
	if (window.rootDisplayNode)
	    window.engine.handleEvent(window.rootDisplayNode,Events.ON_UNLOAD);
	// autoLogout default has changed to 'false', since 'true' may create an inconsistent
	// state where user & permissions can still be retrieved, although requiredPermission
	// no longer abides by them.
	// The 'true' option should be deprecated and removed, or moved to a 'backwards
	// compatibility module' we might want to offer in the future
	if (tersus.autoLogout)
	    tersus.logout();
    }
};
tersus._disabled = false;
tersus.createClassNameList = function()
{
    var list = [];
    var sheets = document.styleSheets;
    var count = 0;
    for (var i=0;i<sheets.length;i++)
    {
	var sheet = sheets[i];
	var rules = sheet.cssRules;
	if (rules == undefined)
	    rules = sheet.rules; //IE
	if (rules == undefined)
	    break;
	for (var j=0;j<rules.length;j++)
	{
	    var rule = rules[j];
	    var selectorText = rule.selectorText;
	    /* Adding ' ' to make sure we have somthing before the .,
	       so we can always ignore the first part after splitting */
	    var classNames = (' '+selectorText).split('.');
	    classNames.shift();
	    list = list.concat(classNames);
	}
    }
    tersus.styleClassNames  = new tersus.Set(list);
};
tersus.logout = function logout()
{
    var req = createXMLHTTPRequest();

    var url=tersus.rootURL+'Logout';
    req.open('GET', url, false);
    req.send(null);
};
tersus.KEEP_ALIVE_INTERVAL=600000; //10 minutes
tersus.DEV_UPDATE_CHECK_INTERVAL = 2000; // 2 seconds
tersus.PROD_UPDATE_CHECK_INTERVAL = 60000; // 1 minute
tersus.init = function()
{
    tersus.repository = new ConstructorRepository();
    window.onfocus = tersus.checkUpdate;
    tersus.async.timer = setInterval(tersus.async.worker, tersus.async.interval);
    if (tersus.settings.dev_mode)
        tersus.updateCheckInterval = tersus.DEV_UPDATE_CHECK_INTERVAL;
    else
        tersus.updateCheckInterval = tersus.PROD_UPDATE_CHECK_INTERVAL;

    // The following are also candidates for removal
    tersus.keepAliveTimer = setInterval(tersus.keepAlive, tersus.KEEP_ALIVE_INTERVAL);
    window.onresize = tersus.onResize;
};
tersus.noop = function()
{
};
tersus.keepAliveReq = null;
tersus.keepAlive = function()
{
    var req = tersus.keepAliveReq;
    if (!req)
	req = tersus.keepAliveReq = createXMLHTTPRequest();
    var url = tersus.rootURL+'blank.html?'+(new Date()).getTime();
    req.open('GET', url, true);
    req.onreadystatechange = tersus.noop;
    req.send(null);
};
tersus.getTimestampReq = null;
tersus.lastCheckUpdateTime = 0;
tersus.checkUpdate = function()
{
    var time = (new Date()).getTime();
    if (time < tersus.lastCheckUpdateTime + tersus.updateCheckInterval)
	return; // We don't want to check too often
    tersus.getTimestamp(tersus.handleCheckUpdateResponse);
}
tersus.getTimestamp = function (callback)
{
    var time = (new Date()).getTime();
    tersus.lastCheckUpdateTime = time;
    var path = 'Timestamp';
    var url = tersus.rootURL+path;
    var req = tersus.getTimestampReq;
    if (!req)
	req = tersus.getTimestampReq = createXMLHTTPRequest();
    req.open('POST',url, true);
    req.onreadystatechange = callback;
    req.send(null);

}
tersus.parseTimestampResponse = function (req)
{
    return eval ('x='+req.responseText+';x;');
}
if (tersus.lastTimestamp == undefined)
    tersus.lastTimestamp = 0;
tersus.setLastTimestamp = function(timestamp)
{
    if (timestamp > tersus.lastTimestamp)
	tersus.lastTimestamp = timestamp;
};
tersus.handleCheckUpdateResponse = function(responseText)
{
    if (!window.tersus)
	return; // This can happen, at least in Firefox, when navigating away from the application
    var req = tersus.getTimestampReq;
    if (req.readyState != 4)
	return;
    if (req.status == 200 || req.responseText)
    {
	var r = tersus.parseTimestampResponse(req);
	var currentTimestamp = r.timestamp;
	window.userName = r.userName;
	if (tersus.lastTimestamp && currentTimestamp && currentTimestamp > tersus.lastTimestamp)
	{
	    if (tlog) tlog("Application Updated -> Reloading");
	    tersus.progress.set('Reloading Application ...');
	    //			tersus.progress.show();
		window.onbeforeunload = null;
	    setTimeout(tersus.reloadApplication,1000);
	}
	else
	    if (tlog) tlog("No update");
	tersus.setLastTimestamp(currentTimestamp);
    }
};
tersus.reloadApplication = function()
{
    tersus.reloading = true;
    var windows = tersus.getWindows();
    for (var i=0;i<windows.length;i++)
    {
	if (window != windows[i])
	    windows[i].close();
    }
    location.reload();
};
tersus.initServiceResponseFrames = function ()
{
    tersus.serviceResponseFrames = [];
    for (var i=0; i<10; i++)
    {
	var f = tersus.serviceResponseFrames[i] = createAnIFrame(window,'_service_'+i);
	f.src  = 'blank.html';
    };
    tersus.nextResponseFrameIndex = 0;
};

tersus.nextServiceResponseFrame = function ()
{
    var iframe = tersus.serviceResponseFrames[tersus.nextResponseFrameIndex++];
    if (tersus.nextResponseFrameIndex >= tersus.serviceResponseFrames.length)
	tersus.nextResponseFrameIndex = 0;
    return iframe;
};
tersus.windows = [self];
tersus.getWindows = function()
{
    if (tersus.windows.length > 1)
	tersus.unregisterClosedWindows();
    return tersus.windows;
};
tersus.registerWindow = function (w)
{
    tersus.windows.push(w);
    if (tlog) tlog("registerWindow:"+w.name+" current:"+tersus.windows.length);
};
tersus.unregisterWindow = function (w)
{
    for (var i=0; i <tersus.windows.length; i++)
    {
	if (tersus.windows[i] == w)
	{
	    tersus.windows.splice(i,1); // remove
	    if (tlog) tlog("unregisterWindow:"+w.name+" index:"+i);
	    return;
	}
	if (tlog) tlog("unregisterWindow:"+w.name+" ** not found **");
    }

}
tersus.popupNodes = {};
tersus.checkWindow = function (w)
{
    try
    {
	return w && !w.closed;
    }
    catch (e)
    {
	return false;
    }
}
tersus.unregisterPopupNodes = function()
{
    for (var id in tersus.popupNodes)
    {
	var node = tersus.popupNodes[id];
	var alive = false;
	try
	{
	    alive = node.currentWindow && ! node.currentWindow.closed && (node.isPopIn || node.isDialog || (id == node.currentWindow.popupId));
	}
	catch (e)
	{
	}
	if (! alive)
	{
	    delete tersus.popupNodes[id];
	}
    };
};
tersus.unregisterClosedWindows = function ()
{
    if (tlog) tlog("unregisterClosedWindows - before:"+tersus.windows.length);
    var temp = [];
    for (var i=0;i<tersus.windows.length;i++)
    {
	var w = tersus.windows[i];
	try
	{
	    if ((! w.closed) && w.document && w.document.body)
		temp.push(w);
	    else
		if (tlog) tlog(w.name + ' closed:'+w.closed + ' body:'+w.document.body);
	}
	catch (e)
	{
	    if (tlog) tlog(e);
	};
    }
    tersus.windows = temp;
    if (tlog) tlog("unregisterClosedWindows - after:"+tersus.windows.length);
};
tersus.progress = {};
tersus.progress.setStatus = function (msg)
{
    var windows = tersus.getWindows();
    for (var i=0;i<windows.length;i++)
	windows[i].status = msg;
};
/** Threshold for forcing the progress bar to open when handling a service response */
tersus.progress.responseLengthThreshold = 10000;
tersus.progress.set = function(msg, cancellationFunc)
{
    tersus.progress.active=true;
    $('body').addClass('ter-progress');
}
tersus.progress.clear = function()
{
    tersus.progress.active=false;
    $('body').removeClass('ter-progress');
    document.body.offsetWidth;//force reflow (makes class change apply immediately, clears "working" indication from cursor)
}
tersus.progress.delay = 1000; // 1000 milliseconds until progress bar appears
tersus.gotoURL = function(url, replace, reload)
{
    if (tlog) tlog("gotoURL: '"+url+"' reload="+reload);
    if (replace)
	window.location.replace(url)
    else
	window.location.href=url;
    if (!reload)
    {
	tersus.history.location = url;
	tersus.loadParams();
    }
    if (tersus.useModernHistory)
	return;
    if (window.browser=='IE' && url.indexOf('mailto:')!=0) // In IE, we add the new URL to the history stack using a special IFRAME
    {
	var historyFrame = document.getElementById('_history');
	if (historyFrame)
	{
	    var src = 'history.html?'+encodeURIComponent(url);
	    if (replace)
		getIFrameWindow(historyFrame).location.replace(src);
	    else
		historyFrame.src =src ;
	}
    }
    else
    {
	// The following code loads a dummy page - this makes firefox "understand" that there is nothing more to wait for
	// (the behavior looks like a Firefox bug, but hasn't been fully analyzed)
	var f= tersus.nextServiceResponseFrame()
	if (f)
	{
	    getIFrameWindow(f).location.replace('blank.html?u='+encodeURIComponent(url));
	}
    }
};


tersus.history = {};
tersus.history.fixedURL = null;
tersus.history._checkLocation = function()
{
    var current = window.location.href;
    if (current != tersus.history.location)
    {
	if (tlog) tlog("New location:"+current);
	tersus.history.location = current;
	tersus.loadParams();
	tersus.history.locationChanged();
    };
};
tersus.history.checkLocation = function()
{
    if (window.location.href != tersus.history.location)
 	tersus.async.exec(tersus.history._checkLocation, false /* don't wait */);
};


tersus.history.locationChanged = function()
{
    if (tersus.history.fixedURL) /* The 'fixedURL' property is used to disable navigation */
    {
	tersus.gotoURL(tersus.history.fixedURL, false/*replace*/, false/*reload*/);
	return;
    }
    var newPerspectiveName = tersus.searchParams[tersus.PERSPECTIVE];
    if (newPerspectiveName == null && window.perspectives && window.perspectives.length > 0)
	newPerspectiveName = window.perspectives[0].name;
    var newViewName = tersus.searchParams[tersus.VIEW];
    if (newViewName == null && newPerspectiveName)
    {
	var p = getPerspective(newPerspectiveName);
	if (p && p.views && p.views.length>0)
	    newViewName = p.views[0].name;
    }

    var perspectiveChanged = window.perspectives && window.perspectives.length > 1 && window.perspective.name != newPerspectiveName;
    var viewChanged = window.view == null || window.view.name != newViewName;
    if (perspectiveChanged || viewChanged)
    {
	window.engine.handleEvent(window.rootDisplayNode,Events.ON_LEAVE_VIEW);
	queueEvent(tersus, tersus.reloadView, [], null,null, null, -1);
    }
    else
	tersus.updateView();
};

tersus.locationChangeListeners = [];
tersus.updateView = function()
{
    if (tersus.locationChangeListeners.length > 0)
    {
	for (var i =0 ; i< tersus.locationChangeListeners.length; i++)
	{
	    var listenerId = tersus.locationChangeListeners[i];
	    var listenerNode = tersus.findNode(listenerId);
	    if (listenerNode)
		window.engine.handleEvent(listenerNode,Events.ON_LOCATION_CHANGE);
	}
    }
    else
    {
	window.rootDisplayNode.refresh();
	resumeRoot();
    }

}
// Extract search parameters
tersus.loadParams = function()
{
    tersus.searchParams = {};
    var terms = [];
    var search = window.location.search;
    if (search && search.length > 1)
	terms = terms.concat(search.substring(1).split('&'));

    // Extract the part that follows the hash ('#').
    // We can't use window.location.hash because some bad decoding happens there
    var hashIndex = location.href.indexOf('#');
    if (hashIndex >=0)
    {
	var hash = location.href.substring(hashIndex+1);
	terms = terms.concat(hash.split('&'));
    }
    for (var i=0; i< terms.length; i++)
    {
	var term = terms[i];
	var keyValuePair = term.split('=');
	var key = decodeURIComponent(keyValuePair[0]);
	var value = decodeURIComponent(keyValuePair[1]);
	tersus.searchParams[key]=value;
    }
}

tersus.history.init = function ()
{
    tersus.history.location = window.location.href;
    tersus.loadParams();
    tersus.history.timer = setInterval(tersus.history.checkLocation,100);
}

tersus.getInternalParameters = function()
{
    var params = {};
    for (var p in tersus.searchParams)
	params[p] = tersus.searchParams[p];
    return params;
};

tersus.history.init();

// tersus.async - a mechanism for preventing concurrency
tersus.async = {};
tersus.async.queue = [];
tersus.async.interval=100;
tersus.async.isBusy = function () { return tersus.async.queue.length > 0;};
tersus.async.nowait = function()
{
    // Returns true if any of the items on queue requires waiting (i.e. is a blocking operation)
    for (var i=0;i<tersus.async.queue.length;i++)
	if (tersus.async.queue[i].wait)
	    return false;
    return true;
};
tersus.async.exec = function(f, waitIfBusy)
{
    var item = {func:f, wait:waitIfBusy?true:false};
    if (item.wait && ! tersus.progress.active)
	tersus.progress.set('..');

    tersus.async.queue.push(item);
};

tersus.async.worker = function()
{
    while (tersus.async.queue.length > 0 || window.rootDisplayNode && window.rootDisplayNode.status == FlowStatus.READY_TO_RESUME)
    {
	if (window.rootDisplayNode && window.rootDisplayNode.status == FlowStatus.READY_TO_RESUME)
	    resumeRoot();
	else if (tersus.async.queue.length > 0)
	{
	    var busy = isBusy(); // Root system busy
	    if (! busy)
	    {
		var item = tersus.async.queue.shift();
		try
		{
		    if (tlog) tlog("tersus.async.worker: start");
		    item.func();
		}
		catch (e)
		{
		    if (tlog)
			tlog(e);
		}
		finally
		{
		    if (tlog) tlog("tersus.async.worker: end");
		}
	    }
	    else // if the root system is busy, we search for items on the queue that don't need to wait
	    {
		var found = false;
		for (var i=0;i<tersus.async.queue.length;i++)
		{
		    var item = tersus.async.queue[i];
		    if (!item.wait)
		    {
			tersus.async.queue.splice(i,1);
			try
			{
			    if (tlog) tlog("tersus.async.worker: start");
			    item.func();
			}
			catch (e)
			{
			    if (tlog)
				tlog(e)
			}
			finally
			{
			    if (tlog) tlog("tersus.async.worker: end");
			}
			found = true;
		    }
		}
		if (!found)
		    break; // Try again later
	    }
	}
    }
    //Since we're here - any queue are waiting for the root system to complete. So if the root system is not busy it means that the queue is empty and we can clear progress
    if (tersus.progress.active)
    {
	if ( ! isBusy())
	{
	    debugAssertion(tersus.async.queue.length == 0, 'tersus.async.queue expected to be empty when root system not busy');
	    tersus.progress.clear();
	}
    }

};
tersus.unescapeString = function unescape_string(s)
{
    return s.replace(/\\n/g, '\n').replace(/\\t/g,'\t').replace(/\\\\/g,'\\');
};
tersus.setDirection = function tersus_setDirection(direction)
{
    var windows = tersus.getWindows();
    window.textDirection = direction;
    for (var i=0; i< windows.length;i++)
    {
	windows[i].document.body.className=direction;
    }

};
tersus.getDirection = function tersus_getDirection()
{
    return window.textDirection == 'rtl'? 'rtl':'ltr';
};
tersus.translateResource = function (s)
{
    if (tersus.resourceMap && tersus.resourceMap[s])
	return tersus.resourceMap[s];
    else
	return s;
};
tersus.Conventions = {};
tersus.Conventions.isEventHandler = function (subflow)
{
    return subflow && (subflow.role.match(/^<On .*>$/) || subflow.role == '<Open>') ;
};
window.engine = {};
FlowStatus = {};
FlowStatus.NOT_READY_TO_START = 'Not Ready to Start';
FlowStatus.READY_TO_START = 'Ready to Start';
FlowStatus.READY_TO_RESUME = 'Ready to Resume';
FlowStatus.WAITING_FOR_INPUT = 'Waiting for Input';
FlowStatus.STARTED = 'Started';
FlowStatus.RESUMED = 'Resumed';
FlowStatus.PAUSED = 'Paused';
FlowStatus.DONE = 'Done';

BuiltinNames = {};
BuiltinNames.DONE_EXIT_NAME = '<Done>';

tersus.Event = function(elementName, jsName, methodName, name)
{
    this.elementName = elementName;
    this.jsName = jsName;
    this.methodName = methodName;
    this.name=name;
};
Events = {};
Events.ON_CLICK = new tersus.Event('<On Click>', 'onclick', 'onClick', 'click');
Events.ON_CONTEXT_MENU = new tersus.Event('<On Context Menu>', 'oncontextmenu' , 'onContextMenu', 'contextmenu');
Events.ON_FOCUS = new tersus.Event('<On Focus>', 'onfocus', 'onFocus', 'focus');
Events.ON_BLUR = new tersus.Event('<On Blur>', 'onblur','onBlur', 'blur');
Events.ON_MOUSEOVER = new tersus.Event('<On Mouse Over>','onmouseover','onMouseOver', 'mouseover');
Events.ON_MOUSEOUT = new tersus.Event('<On Mouse Out>','onmouseout','onMouseOut', 'mouseout');
Events.ON_MOUSEENTER = new tersus.Event('<On Mouse Enter>','onmouseenter','onMouseEnter', 'mouseenter');
Events.ON_MOUSELEAVE = new tersus.Event('<On Mouse Leave>','onmouseleave','onMouseLeave', 'mouseleave');
Events.ON_MOUSEDOWN = new tersus.Event('<On Mouse Down>','onmousedown','onMouseDown', 'mousedown');
Events.ON_MOUSEUP = new tersus.Event('<On Mouse Up>','onmouseup','onMouseUp', 'mouseup');
Events.ON_MOUSEMOVE = new tersus.Event('<On Mouse Move>','onmousemove','onMouseMove', 'mousemove');
Events.ON_KEYPRESS = new tersus.Event('<On Key Press>','onkeypress','onKeyPress', 'keypress');
Events.ON_KEYDOWN = new tersus.Event('<On Key Down>','onkeydown','onKeyDown', 'keydown');
Events.ON_KEYUP = new tersus.Event('<On Key Up>','onkeyup','onKeyUp', 'keyup');
Events.ON_DOUBLE_CLICK = new tersus.Event('<On Double Click>','ondblclick','onDoubleClick', 'doubleclick');
Events.ON_TOUCHSTART = new tersus.Event('<On Touch Start>','ontouchstart','onTouchStart', 'touchstart');
Events.ON_TOUCHMOVE = new tersus.Event('<On Touch Move>','ontouchmove','onTouchMove', 'touchmove');
Events.ON_TOUCHEND = new tersus.Event('<On Touch End>','ontouchend','onTouchEnd', 'touchend');
Events.ON_BROWSE = new tersus.Event('<On Browse>',null,null);
Events.ON_TAP_HOLD = new tersus.Event('<On Tap Hold>','ontaphold','onTouchHold', 'taphold');
Events.ON_SWIPE_LEFT = new tersus.Event('<On Swipe Left>','onswipeleft','onSwipeLeft', 'swipeleft');
Events.ON_SWIPE_RIGHT = new tersus.Event('<On Swipe Right>','onswiperight','onSwipeRight', 'swiperight');
Events.ON_SWIPE_UP = new tersus.Event('<On Swipe Up>','onswipeup','onSwipeUp', 'swipeup');
Events.ON_SWIPE_DOWN = new tersus.Event('<On Swipe Down>','onswipedown','onSwipeDown', 'swipedown');
Events.ON_SWIPE = new tersus.Event('<On Swipe>','onswipe','onSwipe', 'swipe');
Events.ON_CHANGE = new tersus.Event('<On Change>','onchange','onChange', 'change');
Events.ON_EXPAND = new tersus.Event('<On Expand>');
Events.ON_POPSTATE = new tersus.Event('<On Pop State>','onpopstate','onPopState', 'popstate');
Events.ON_COLLAPSE = new tersus.Event('<On Collapse>');
Events.ON_SELECT = new tersus.Event('<On Select>');
Events.ON_LOCATION_CHANGE = new tersus.Event('<On Location Change>');
Events.ON_UNLOAD = new tersus.Event('<On Unload>');
Events.ON_LEAVE_VIEW = new tersus.Event('<On Leave View>');
Events.ON_SORT = new tersus.Event('<On Sort>');
Events.OPEN = new tersus.Event('<Open>');
Events.ON_DELETE = new tersus.Event('<On Delete>');
Events.ON_ELEMENT_CHANGE = new tersus.Event('<On Element Change>');
Events.ON_ORIENTATION_CHANGE = new tersus.Event('<On Orientation Change>');
Events.ON_TAP = new tersus.Event('<On Tap>', 'ontap', 'onTap', 'tap');
Events.ON_SUBMIT = new tersus.Event('<On Submit>', 'onsubmit', 'onSubmit', 'submit');
Events.ON_DRAG = new tersus.Event('<On Drag>', 'ondrag', 'onDrag', 'drag');
Events.ON_DROP = new tersus.Event('<On Drop>', 'ondrop', 'onDrop', 'drop');
Events.ON_ERROR = new tersus.Event('<On Error>', 'onerror', 'onError', 'error');

BuiltinModels = {};
BuiltinModels.TEXT_ID='Common/Data Types/Text';
BuiltinModels.NUMBER_ID='Common/Data Types/Number';
BuiltinModels.NOTHING_ID='Common/Data Types/Nothing';

function compareValues(value1, value2, caseInsensitive, recursionMap)
{
	//recursionMap holds a list all comparisons already made (or in progress) - we don't want to comapre the same objects twice
	if (value1 && value1.isDataNode)
		value1 = value1.value;
	if (value2 && value2.isDataNode)
		value2 = value2.value;

	if( value1==value2)
		return true;
	if (! value1 || ! value2)
		return false;

	if (! value1.isNode || !value2.isNode)
		return false;
	if (value1.constructor != value2.constructor && ! (value1.isLeaf && value2.isLeaf && value1.pluginFunc == value2.pluginFunc))
		return false;
	if (! value1.elementList)
	{
		var l1 = value1.leafValue;
		var l2 = value2.leafValue;
		var c1 = 0;
		var c2 = 0;
		if (value1.isMap && value2.isMap)
		{
			var keys2 = new tersus.Set();
			for (var key in l2)
			{
				++c2;
				keys2.add(key);
			}
			for (var key in l1)
			{
				if (! keys2.contains(key))
					return false;
				if (! compareValues(l1[key],l2[key]), caseInsensitive, recursionMap)
					return false;
				++c1;
			}
			if (c1 != c2)
				return false; //Different number of memebers
			return true;
		}
		if (value1.isText && caseInsensitive)
		{
			l1 = l1.toUpperCase();
			l2 = l2.toUpperCase();
		}
		return l1 == l2 || l1 && l1.equals && l1.equals(l2);
	}

	if (!recursionMap && value1.isRecursive())
	{
		recursionMap = {};
	}
	if (recursionMap)
	{
		var id1  = value1.getNodeId();
		var id2 = value2.getNodeId();
		var key = id1+':'+id2;
		if (recursionMap[key])
			return true; // already compared (or comparison in progress) - no need to comapre again
		recursionMap[key]=true;
	}

	for (var i=0; i<value1.elementList.length; i++)
	{
		var e = value1.elementList[i];
		//		if (e.isDisplayElement || e.isDataElement || e.isMethodElement)
		{
			var children1;
			var children2;
			if (e.isRepetitive)
			{
				children1 = e.getChildren(value1);
				children2 = e.getChildren(value2);
			}
			else
			{
				children1 = [e.getChild(value1)];
				children2 = [e.getChild(value2)];
			}
			if (children1.length != children2.length)
				return false;
			for (var j=0; j<children1.length; j++)
			{
				if (! compareValues(children1[j], children2[j], caseInsensitive, recursionMap))
					return false;
			}
		}
	}
	return true;
};
function escapeHTML(value)
{
    return value.replace(/&/g, '&amp;').replace(/>/g,'&gt;').replace(/</g,'&lt;').replace(/\n/g, '<br>');
};
HTMLProperties = {};


// Creates a set

function loadBooleanOption(name)
{
    var value = getCookie(name);
    window[name] = (value == 'true');
};
function loadOptions()
{
    loadBooleanOption('autoReload');
    loadBooleanOption('serverTracing');
    loadBooleanOption('showServiceInvocations');
    loadBooleanOption('clientTracing');
};

tersus.loadFile = function(path, callback, ignoreErrors)
{
    var storageKey = null;
    if (window.localStorage && tersus.useLocalStorage)
    {
		storageKey = "File?"+path;
		var text = localStorage.getItem(storageKey);
		if (text != null)
		{
		    if (callback)
		    {
			setTimeout(function() {callback(text);},0);
			return;
		    }
		    else
			return text;
		}
    }
    return tersus.loadURL(tersus.rootURL+path, callback, ignoreErrors, null/*error callback*/, storageKey);
};
tersus.loadURL = function(url, callback, ignoreErrors, errorCallback, storageKey, timeout)
{
    var req = createXMLHTTPRequest();

    if (callback)
    {
		req.open('GET',url, true);
		if (timeout && timeout > 0)
		{
			req.timeout = timeout;
			req.ontimeout = function()
			{
				errorCallback(-1, 'Timeout occurred after ' + timeout + 'ms', null)
			};
		}
		req.onreadystatechange = function()
		{
		    var responseText = null;
		    var status = null;
		    var statusText = null;
		    try
		    {
				if (req.readyState != 4)
				    return;
				var status = req.status;
				var statusText = req.statusText;
				if (status == 200)
				{
				    responseText = req.responseText;
				    if (storageKey && window.localStorage)
				    {
						if (tlog)
						    tlog('Storing '+storageKey+ ' in local storage.');
						localStorage.setItem(storageKey, responseText);
				    }
				}
		    }
		    catch (e)
		    {
				if (ignoreErrors)
				    callback(null);
				else if (errorCallback)
				    errorCallback(status, statusText, e);
				else
				    tersus.error('Failed to load "'+url+'"');
				return;
	
		    }
		    if (status == 200)
		    	callback(responseText);
		    else if (ignoreErrors)
		    	callback(null);
		    else if (errorCallback)
		    	errorCallback(status, statusText, null);
		    else
		    	tersus.error('Failed to load "'+url+'"');
		};
		req.send(null);
		return req;
    }
    else
    {
		var method = 'GET';
		if (tersus.useLocalStorage && isWebKit && navigator.onLine == true)
		    method = 'POST'; // For some reason 'GET' doesn't work in Safari cached mode (and so offline-enabled applications can't load templates like 'popin.html' even when online)
		req.open(method, url, false);
		req.send(null);
		if (req.status != 200)
		{
		    tersus.error('Missing resource '+url+ '\nerror code='+req.status+' error message='+req.statusText);
		    if (parent && parent.notLoaded)
			parent.notLoaded();
	
		    return;
		}
		if (storageKey && window.localStorage)
		{
		    if (tlog)
			tlog('Storing '+storageKey+ ' in local storage.');
		    localStorage.setItem(storageKey, req.responseText);
		}
		return req.responseText;
    }
};
tersus.getUserName = function()
{
    return window.userName;
};
tersus.setUserName = function(name)
{
    window.userName = name;
};
tersus.getCurrentViewURL = function()
{
    if (tersus.firstURL.match('\\?minify=false#?$'))
	return tersus.firstURL;
    else
	return tersus.rootURL;
};
function addHiddenField(form, name, value)
{

    var field = form.ownerDocument.createElement('input');
    field.type = 'hidden';
    field.value =value;
    field.name = name;
    form.appendChild(field);
    return field;
}

loadOptions();
function set(list)
{
    var out = {};
    for (var i=0; i< list.length; i++)
    {
	out[list[i]] = list[i];
    }
    return out;
}
tersus.Set = function Set(list)
{
    this.members = {};
    if (list)
    {
	for (var i=0; i< list.length; i++)
	{
	    this.members[list[i]] = true;
	}
    }
};
tersus.Set.prototype.contains = function (member)
{
    return this.members[member];
};
tersus.Set.prototype.add = function(obj)
{
    this.members[obj]=true;
};
tersus.Set.prototype.toList = function()
{
    var list = [];
    for (var memeber in this.members)
	list.push(memeber);
    return list;
};

tersus.Set.prototype.remove = function(member)
{
    delete this.members[member];
};
HTMLProperties['table']=set(['width']);
HTMLProperties['TABLE']=set(['width']);
STYLE_PROPERTIES = set(['width', 'height', 'overflow']);

Operation = {};
Operation.REPLACE = 'Replace';
Operation.ADD = 'Add';
Operation.REMOVE = 'Remove';
function internalError(message)
{
    tersus.error('Error:\n'+message);
    this.undefined();
};
function modelError(message, model)
{
    if (model && model.modelId)
	message = 'Invalid Model: '+model.modelId+'\n'+message;
    if (tlog)
	tlog(message);
    throw new tersus.Exception(message);
};
function debugAssertion(expression, message)
{
    //Default implemenation is a no-op. Should be overridden in test settings
}
function modelExecutionError(message, node)
{
    if (node)
	message += '\nLocation:'+new tersus.RuntimeLocation(node);
    tersus.error('Model Execution Error:\n'+message);
};

function makeAssertion(flag, description)
{
    if (! description)
	var description = '';
    if ( !flag)
    {
	tersus.error('Assertion Failed ' + description);
	this.undefined();
    }
};

tersus.RuntimeLocation = function(node,element, suffix)
{
    var segments = [];
    // Collecting path segments in reverse order
    if (suffix)
	segments.push(suffix);
    if (element)
	segments.push(element.role);
    var current = node;
    while (current)
    {
	if (!current.isBackground)
	{
	    var currentModelId = current.getModelId();
	    if (! current.element  || current.element.modelId != currentModelId)
	    {
		this.baseModelId = currentModelId;
		if (!currentModelId)
		    error("No currentModelId id");
		break;
	    }
	    else
		segments.push(current.element.role);
	}
	current = current.parent;
    }
    segments.reverse();
    this.path = new Path();
    this.path.setSegments(segments);
};
tersus.RuntimeLocation.prototype.toString = function()
{
    return this.baseModelId + '['+this.path.str+']';
};
/*
  Handles a user event (mouse click etc.)
  displayNode: the displayNode on which the event was registered
  eventType: the type of the event (see Events.ON_XXX)
  parameters: parameters that will be passed to the event handler action
*/
window.engine.handleEvent = function handleEvent(displayNode, eventType , parameters, domEventAttributes)
{
    if (tlog) tlog ('handleEvent '+eventType.elementName + ' on '+displayNode.modelName + ' ( '+displayNode.getPath().str+')');
    tersus.currentEventAttributes = domEventAttributes;

    var subFlow = displayNode.getEventHandler(eventType);
    if (!subFlow)
	return;
    var child = subFlow.createChild(displayNode);
    if (parameters)
    {
	for (var parameterName in parameters)
	{
	    var trigger = child.getElement(parameterName);
	    if (trigger)
	    {
		var value = parameters[parameterName];
		if (value !== undefined)
		{
		    if (trigger.isRepetitive)
		    {
			var v = value.length? value : [value];
			for (var i=0;i<v.length;i++)
			{
			    trigger.addAChild(child,trigger.getValueNode(v[i]));
			}
		    }
		    else
			trigger.replaceChild(child, trigger.getValueNode(value));
		}
	    }
	}
    }
    if (subFlow.isRepetitive)
	subFlow.addAChild(displayNode,child);
    else
	subFlow.replaceChild(displayNode,child);
    child.readyToStart();

    var ctx = child.getExecutionContext()
    if (ctx === rootDisplayNode)
	resumeRoot();
    else
    {
	ctx.doResume();
        if (window.trace)
            window.trace.flush();
        if (this == window.rootDisplayNode)
            tersus.progress.clear();
        $(window).trigger('ter-process-end');
    }
};

function isBusy()
{
    return(window.rootDisplayNode && window.rootDisplayNode.status != FlowStatus.WAITING_FOR_INPUT)
};
function checkBusy()
{
    if (isBusy())
    {
	if (window.rootDisplayNode.errorHandler)
	{
	    var $pausedNodes = $(tersus.scanHierarchy(window.rootDisplayNode, tersus.Node.subFlowIterator, function(node){return node.status == FlowStatus.PAUSED;}));
            // To get to active nodes, we exclude from paused nodes the parents of paused nodes
	    var $activeNodes = $pausedNodes.not($pausedNodes.map(function(i,node){return node.parent;}));
	    // Then concatenate all with new line spearators as the details
	    var details = $activeNodes.map(function(i,node){return node.getPath().str;}).toArray().join('\n');
	    window.rootDisplayNode.handleException({message:'Application is busy',details:details});
	}
	else if (tersus.alertMode.interactive)
	    tersus.error('The application is busy, please wait (or refresh)');
	return true;
    }
    else
	return false;
};
function resumeRoot()
{
    var start = new Date();
    if (tlog) tlog('resumeRoot');
    if (! tersus.progress.active)
	tersus.progress.set('Busy...');
    if (tlog) tlog ('resumeRoot: resuming root node');
    window.rootDisplayNode.doResume();
    if (window.trace)
    {
	if (tlog) tlog("resumeRoot: flushing trace");
	window.trace.flush();
    }
    var end = new Date();
    $(window).trigger('ter-process-end');
    if (tlog) tlog("resumeRoot: duration: " + (end - start) + " ms.");
    window.duration = "Action duration: " + (end - start) + " ms.";

};

tersus.queueEventDelay = 5000;
function queueEvent(object, method, args, startTime,description, callback, maxDelay)
{
    if (!maxDelay)
	maxDelay=tersus.queueEventDelay;
    if (isBusy())
    {
	var now = new Date();
	var delay = now - startTime;
	if (tlog)
	{
	    if (!description)
		description = method.name;
	    tlog('QE: Busy ('+delay+' ms) - '+description);
	}
	if (delay < maxDelay || maxDelay<0)
	{
	    tersus.progress.set('Busy ...');
	    setTimeout(function() {queueEvent(object,method, args, startTime, description, callback, maxDelay);},100);
	    return;
	}
    }
    //TODO - provide a nice visual indication that the system is waiting for something, and
    // a way for the user to abort the running operation (or refresh the screen).
    // In the meantime, we use 'checkBusy()', that shows a warning message.
    if (checkBusy())
    {
	if (tlog)
	    tlog('QE: Giving up - '+description);
	if (callback)
	    tersus.async.exec(callback,false);
	return;
    }
    if (tlog)
	tlog('QE: Invoking - '+description);
    /* TODO prevent concurrent operation:

       -- If we have a single user gesture triggering multiple events, we want them all to run, even if the first one takes a long time
       -- However, we don't want them to run "in parallel" (in simulated parallel)
       -- Therefore, putting them in the async queue is not good enough (they will run "in parallel")
       -- We may need to add a pre-queue (maybe a variant of queueEvent that runs for ever)

    */
    tersus.async.exec (function () {method.apply(object,args);if (callback)
	tersus.async.exec(callback,true);}, true /* wait if busy */);
};

tersus.traverseHierarchy = function traverseHierarchy(root, iterator, operation)
{
    var alreadyScanned = {};
    var iteration = function(node)
    {
	var id = node.getNodeId();
	if (alreadyScanned[id])
	    return;
	alreadyScanned[id] = true;
	var stop = operation(node);
	if (stop == true)
	    return true;
	iterator(node,iteration);
    }
    iteration(root);
}
tersus.scanHierarchy = function scanHierarchy(root, iterator, filter)
{
    var alreadyScanned = {};
    var out = [];
    var operation = function(node)
    {
	if (filter(node))
	    out.push(node);
    };
    tersus.traverseHierarchy(root,iterator, operation);
    return out;
}
tersus.sqlize = function(str)
{
    return str.replace(/[^a-zA-Z0-9]/g, "_");
};



function stats()
{
    var packages = {};
    var nModels = 0;
    var nPackages = 0;
    for (var modelId in tersus.repository.constructors)
    {
	++nModels;
	var packageId = tersus.getPackageId(modelId);
	if (packages[packageId])
	    ++packages[packageId];
	else
	{
	    packages[packageId]=1;
	    ++nPackages;
	}
    }
    var message = 'Total models:'+nModels;
    message += 'Total packages:'+nPackages;
    for (var packageId in packages)
    {
	message += '\n'+packageId+':'+packages[packageId];
    }
    var w = window.open();
    w.document.write('<pre>');
    w.document.write(message);
    w.document.write('</pre>');
    w.document.close();

}
tersus.getPackageId = function(modelId)
{
    var i = modelId.lastIndexOf('/');
    return modelId.substring(0,i);
};
Function.prototype.__subclass = function(name, c)
{
    if (!c)
	c = function(){};
    c.name=name;
    c.prototype = new this();
    c.prototype.constructor = c;
    c.prototype.parentPrototype = this.prototype;
    c.prototype.supr = function(name, args)
    {
	if (args === undefined)
	    args = [];
	var p = this.parentPrototype;
	var f = this[name];
	while (p && p[name]===f)
	    p = p.parentPrototype;
	return p[name].apply(this,args);
    };
    return c;
};

tersus.newAction = function(name, startFunc)
{
    var c = ActionNode.__subclass('tersus.'+name);
    c.prototype.start = startFunc;
    tersus[name]=c;
};

tersus.findChildNodesByClassName = function(node, className, out)
{
    if (!out) out = [];
    if (node.className && node.className==className)
	out.push(node);
    var children = node.childNodes;
    for( var i = 0; i<children.length; i++)
    {
	tersus.findChildNodesByClassName(children[i], className, out);
    }
    return out;
};
tersus.adjustHeight = function (domNode, delta)
{
    var currentHeight =  getVisibleSize(domNode).height ;
    if (window.browser != 'IE')
    {
	var paddingBottom = parseInt(getActualStyle(domNode,'padding-bottom'));
	var paddingTop = parseInt(getActualStyle(domNode,'padding-top'));
	currentHeight -= (paddingBottom + paddingTop);
    }
    domNode.style.height = (currentHeight + delta) + 'px';
}

tersus.foreach = function(array, f)
{
    if (array && array.length)
	for (var i=0;i<array.length; i++)
    {
	f(array[i]);
    }
}

tersus.preventDefaultEventHandling = function()
{
    var ev = tersus.currentEvent;
    if (ev)
    {
	if (ev.preventDefault)
	    ev.preventDefault();
	else
	    ev.returnValue=false;

	if (ev.stopPropagation)
            ev.stopPropagation();
	else
            ev.cancelBubble = true;
    }
}
tersus.isIPhone =  navigator.userAgent.toLowerCase().indexOf('iphone') >=0 || tersus.searchParams.UserAgent == 'iPhone';
tersus.isIPhoneSafari =  navigator.userAgent.toLowerCase().indexOf('iphone') >=0 && navigator.userAgent.toLowerCase().indexOf('applewebkit') >=0 ;
if (tersus.isPhone)
    tersus.useIFrames = false;

tersus.foreach = function(a, func, contextObj)
{
    if (a == null)
	return;
    for (var i=0;i<a.length;i++)
    {
	func.call(contextObj, a[i], i);
    };
};
tersus.addAll = function (a1, a2)
{
    tersus.foreach(a2, function(x) {a1.push(x);});
};

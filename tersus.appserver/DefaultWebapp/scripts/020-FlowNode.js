/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 
//FlowNode - an instance of FlowModel (implements composite behavior)

function FlowNode() {};
FlowNode.prototype = new tersus.Node();
FlowNode.prototype.constructor = FlowNode;
FlowNode.prototype.errorExit = null;
FlowNode.prototype.errorHandler = null;
FlowNode.prototype.addSubFlow = function addSubFlow(role, modelId)
{
	// Adds a representation of the sub-flow both to a list of sub-flows
	// and to a map of sub-flows
	var subFlow = new SubFlow(role, modelId);
	this.registerSubFlow(subFlow);
	return this.addElement(subFlow);
}
FlowNode.prototype.registerSubFlow = function registerSubFlow(subFlow)
{
	if (! this.subFlowMap)
		this.subFlowMap = {};
	this.subFlowMap[subFlow.role] = subFlow;
	if (! this.subFlowList)
		this.subFlowList = [];
	this.subFlowList.push(subFlow);
};
/**
	Returns the sole DisplayElement of this Flow.
	Returns null if there are 0 or multiple DisplayElements.
*/
FlowNode.prototype.addAncestorReference = function addAncestorReference(role, modelId)
{
	var reference = new FlowDataElement(role, modelId);
	reference.isAncestorReference = true;
	if (! this.ancestorReferenceList)
		this.ancestorReferenceList = [];
	
	this.ancestorReferenceList.push(reference);
	return this.addElement(reference);
};

FlowNode.prototype.addIntermediateDataElement = function addIntermediateDataElement(role, modelId)
{
	if (this.getGetter(role))
	{
		var element = new MethodElement(this, role, modelId);
		element.modelId = modelId;
	}
	else
		var element = new FlowDataElement(role, modelId);
	return this.addElement(element);
};

FlowNode.prototype.addConstant = function addConstant(role, modelId)
{
	var constantReference = new FlowDataElement(role, modelId);
	constantReference.type = 'Constant';
	if (!this.constantList)
		this.constantList = [];
	this.constantList.push(constantReference);
	return this.addElement(constantReference);
};

FlowNode.prototype.addLink = function addLink(role, source, target)
{
	if (!this.elements)
		return null; // If there are no elements, there is nothing to link. This can happen because of permission issues  
		
	var link = new Link(role, source,target);
	var sourceElement;
	if (!this.links)
		this.links = [];
	this.links.push(link);
	var sourceElement = this.elements[link.source.segments[0]];
	if (! sourceElement)
		return;
	if (! sourceElement.outgoingLinks)
		sourceElement.outgoingLinks = [];
	sourceElement.outgoingLinks.push(link);
	return link;
};

FlowNode.prototype.getLink = function getLink(role)
{
	if (! this.links)
		return null;
	for (var i=0; i<this.links.length; i++)
	{
		var link = this.links[i];
		if (link.role == role)
			return link;
	}
	return null;
};

/*
	Called before a FlowNode is started
*/
FlowNode.prototype.started = function started()
{
	this.setStatus(FlowStatus.STARTED);
	if (window.trace)
		window.trace.started(this);
};
FlowNode.prototype.resumed = function ()
{
	this.setStatus(FlowStatus.RESUMED);
};

FlowNode.prototype.finished = function ()
{
	/* Because actions can contain pop-up windows, finish() may be called more than once,
	 * but we don't want the <Done> exit to be charged again, or the trace to contain an additional event*/
	
	if (! this.alreadyFinished)
	{
		if (!this.error)
		{
			var doneExit = this.getElement(BuiltinNames.DONE_EXIT_NAME);
			if (doneExit)
				this.chargeEmptyExit(doneExit);
		}
		if (window.trace)
			window.trace.finished(this);
		if (this.whenDone)
			this.whenDone();
	}
	this.setStatus(FlowStatus.DONE);
	this.alreadyFinished = true;
}
FlowNode.prototype.waiting = function ()
{
	this.setStatus(FlowStatus.WAITING_FOR_INPUT);
	if (window.trace)
		window.trace.waiting(this);
}
FlowNode.prototype.continueExecution = function()
{
		var executionContext = this.getExecutionContext();
		this.resumeAfterPause();
		executionContext.doResume();
		if (window.trace)
			window.trace.flush();
		if (this == window.rootDisplayNode)
			tersus.progress.clear();
		$(window).trigger('ter-process-end');
};
FlowNode.prototype.doStart = function()
{
	var node = this;
	var	resume = function()
	{
		if (tlog) tlog(node.modelName,'resumed after preload', node.modelId);
		node.resumeAfterPause();
		node.setStatus(FlowStatus.READY_TO_START);
		var executionContext = node.getExecutionContext();
		executionContext.doResume();
		if (window.trace)
			window.trace.flush();
	};
	if (! this.preloadOnStart || this.preload(resume))
	{
		if (tlog) tlog(node.modelName,'started (already loaded)', node.modelId);
	
		var isAtomic = this.isAction && !this.subFlowMap;
		var isDisplay = this.isDisplayNode;
		var isRoot = !this.getParentContext();
		if (isAtomic || this.errorExit  || this.onerror|| (isRoot&&!tersus.failOnScriptErrors))
		{
			try
			{
				this.started();
				this.start();
				if (tlog) tlog(node.modelName,'started done', node.modelId);
			}
			catch (e)
			{
				if (this.errorExit || this.errorHandler || isRoot || isDisplay)
					this.handleException(e);
				else
					throw this.convertException(e);
			}
		}
		else
		{
			this.started();
			this.start();
			if (tlog) tlog(node.modelName,'started done', node.modelId);
		}
		if (this.status != FlowStatus.PAUSED)
		{
			if (this.isAction)
				this.finished();
			else
				this.waiting();
		}
	}
	else
		if (tlog) tlog(node.modelName,'paused(waiting for models to load)', node.modelId);
};
FlowNode.prototype.convertException = function(e)
{
	if (typeof e === 'object' && 'getMessage' in e) // Java exception
	{
		e = {message: e.getClass().getName() + ' - ' + e.getMessage()};
	}
	else if (typeof e != 'object')
		e = {message:''+e};
	if (!e.errorPath)
		e.errorPath = this.getPath().str;
	return e;
}
FlowNode.prototype.handleException = function (e)
{
	e = this.convertException(e);
	var msg = e.message?e.message:''+e;
	var details = e.details;
	
	if (this.errorHandler)
	{
		var ERROR='<Error>';
		var cc = this.errorHandler.getChildConstructor();
		var errorTrigger = cc.prototype.getElement(ERROR);
		var params = null; 
		if (errorTrigger)
		{
			params = {};
			var eObj = params[ERROR] = new (errorTrigger.getChildConstructor());
			eObj.setLeafChild('Message', msg, Operation.REPLACE);
			eObj.setLeafChild('Location', e.errorPath, Operation.REPLACE);
			if (details)
				eObj.setLeafChild('Details', e.details, Operation.REPLACE);
		}
		var process = new BackgroundProcess(this, this.errorHandler, params);
		//process.whenDone(callback);
		setTimeout(function(){process.start();},0); 
	}
	else if (this.errorExit)
	{
		var error = this.errorExit.createChildInstance();
		error.setLeafChild('Message', msg, Operation.REPLACE);
		error.setLeafChild('Location', e.errorPath, Operation.REPLACE);
		this.chargeExit(this.errorExit,error);
		this.error = error;
	}
	else
	{
		//TODO  - Throw if below event level, Show message box if at event level
	    tersus.lastError=e;
		var m ="An error has occurred: "+e.message;
		if (e.details)
			m+='\n\nDetails: '+e.details;
		if (e.errorPath)
			m+='\n\nPath: '+e.errorPath;
		
		if (this.currentWindow && !tersus.checkWindow(this.currentWindow))
		{
			m += "\n\nThe problem may have been caused by closing a pop-up window while it was being populated."
		}
		tersus.error(m);
	}
};
FlowNode.prototype.doResume = function doResume(message)
{
	this.resumed();
	var isAtomic = this.isAction && !this.subFlowMap;
	var isDisplay = this.isDisplayNode;
	var isRoot = !this.getParentContext();
	try
	{
		if (isAtomic || this.errorExit || this.errorHandler || (isRoot&&!tersus.failOnScriptErrors))
		{
			try
			{
				this.resume();
			}
			catch (e)
			{
				if (this.errorExit || this.errorHandler || isRoot)
					this.handleException(e);
				else
					throw this.convertException(e);
			}
		}
		else
		{
			this.resume();
		}
	}
	finally
	{
		if (this.status != FlowStatus.PAUSED)
		{
			if (this.isAction)
				this.finished();
			else
				this.waiting();
		}
	}
};

FlowNode.prototype.start = function()
{
	var element; //represents a model element
	var child; // represents a child instance (sub-flow instance)
	var role;
	// add trigger values to the ready queue
	if (this.triggers)
		for (var i = 0; i < this.triggers.length; i++)
		{
			var trigger = this.triggers[i];
			
			if (trigger.isRepetitive)
			{
				var values = trigger.getChildren(this);
				trigger.removeChildren(this);
				for (var j=0; j< values.length; j++)
				{
					var value = values[j];
					this.createReference(trigger, value);
				}
			}		
			else
			{
				var value = this.getChild(trigger.role);
				if (value)
					this.createReference(trigger, value);
			}
		}
			
			
		//create instances for constants
		if (this.constantList)
		{
			for (var i=0; i< this.constantList.length; i++)
			{
				element = this.constantList[i];
				if (element.isSelfReady)
					child = element.createChild(this);
			}
		}
		
		if (this.ancestorReferenceList)
		{
			var ancestor;
			var ancestorData;
			var referenceNode;
			for (var i=0; i< this.ancestorReferenceList.length; i++)
			{
				element = this.ancestorReferenceList[i];
				role = element.role;
				ancestor = this.parent;
				var child = null;
				while (ancestor)
				{
					var parentElement = ancestor.getElement(role);
					if ( parentElement && parentElement.modelId == element.modelId) // In the direct ancestor we will find the referring element
					{
						var parentObj = parentElement.getChild(ancestor);
						if (parentObj)
							child = this.createReference(element, parentObj);
						ancestor = null;
					}
					else if (ancestor.modelId == element.modelId)
					{
						child = this.createReference(element, ancestor);
						ancestor = null; //No need to continue searching
					}
					else
					{
						ancestor = ancestor.parent;
					}
				}
				if (! child)
					if (tlog)
						tlog("Missing ancestor reference for "+role + " in "+ this.modelId);

			}
		}
		this.createSubFlows();
		this.runLoop();
	};
FlowNode.prototype.createSubFlows = function()
{
	//initializes sub flows
	if (this.subFlowList)
	{
		for (var i=0;i<this.subFlowList.length; i++)
		{
			var subFlow = this.subFlowList[i];
			subFlow.init(this);
		}
	}
};


FlowNode.prototype.createReference = function createReference(element, value)
{
	var child = new DataNode();
	child.type = element.type;
	child.mainWindow = this.mainWindow;
	child.currentWindow = this.currentWindow;
	child.parent = this;
	child.element = element;
	if (value.isDataNode)
		child.value = value.value;
	else
		child.value = value;
	element.addChild(this, child);
	if (child.isReady())
		this.addReadyElement(child);
	return child;
};




FlowNode.prototype.readyToResume = function readyToResume()
{
		this.setStatus(FlowStatus.READY_TO_RESUME);
		var parent = this.getParentContext();
		if (parent == null)
			return;
		parent.addReadyElement(this);
		if (parent.status == FlowStatus.WAITING_FOR_INPUT || parent.status == FlowStatus.DONE)
			parent.readyToResume();
};

/*
	Marks this FlowNode and its ancestors as ready to resume from the point of pause:
	(The paused processes are added to the beginning of the run-queue)
	
*/
FlowNode.prototype.resumeAfterPause = function resumeAfterPause()
{
	this.setStatus(FlowStatus.READY_TO_RESUME);
	var parent = this.getParentContext();
	if (parent == null)
		return;
	if ( ! parent.readyElements)
		parent.readyElements = [];
	parent.readyElements.splice(0,0, this); // insert at the beginning
	parent.resumeAfterPause();
}	

FlowNode.prototype.pause = function pause()
{
	this.setStatus(FlowStatus.PAUSED);
	if (window.trace)
		window.trace.paused(this);
	var parent = this.getParentContext();
	if (parent)
		parent.pause();
};

FlowNode.prototype.readyToStart = function readyToStart()
{
		this.setStatus(FlowStatus.READY_TO_START);
		var parent = this.getParentContext();
		if (parent == null)
			return;
		parent.addReadyElement(this);
		if (parent.status == FlowStatus.WAITING_FOR_INPUT || parent.status == FlowStatus.DONE)
			parent.readyToResume();
};
FlowNode.prototype.setInitialStatus = function setInitialStatus()
{
	this.setStatus(FlowStatus.NOT_READY_TO_START);
	this.checkReady();
};

FlowNode.prototype.checkReady = function checkReady()
{
	if (this.status == FlowStatus.NOT_READY_TO_START && this.isReady())
	{
		this.readyToStart();
	}
		
};

/*
	Adds 'child' to the run-queue, while preserving the dependency rank order
	('child' is added at the largest index that will result in a non-decreasing 
	dependency rank order)
*/
FlowNode.prototype.addReadyElement = function addReadyElement(child)
{
	
	
	
	var childRank = child.element.dependencyRank;
	
	
	if (!this.readyElements)
		this.readyElements = [];
	var targetIndex = this.readyElements.length;
	if( child.element === this.initElement) // <Init>
	{
		var parent = this;
		// After <Init> is done, we need to create child elements
		child.whenDone = function()
		{
			parent.createChildren();
		};
		// <Init> must be executed first
		targetIndex = 0;
	}
	else
	{
		while (targetIndex > 0)
		{
			var node = this.readyElements[targetIndex-1];
			var nodeRank = node.element.dependencyRank;
			if (nodeRank <= childRank)
			{
				// child should be inserted at targetIndex
				break;
			}
			--targetIndex;
		}
	}
	// Calling splice adds 'child' at targetIndex
	this.readyElements.splice(targetIndex, 0, child);
	
};

FlowNode.prototype.setStatus = function setStatus(status)
{
	if (tlog)
		tlog('STATUS ['+this.getNodeId()+' - '+this.modelName+']: '+this.status+' => '+status);
	this.status = status;
};


FlowNode.prototype.resume = function resume(message)
{

	this.runLoop();

	if (this == window.rootDisplayNode && 	this.status == FlowStatus.WAITING_FOR_INPUT)
		if (tersus.async.nowait())
			tersus.progress.clear();
};
FlowNode.prototype.isReady = function isReady()
{
	if (this.triggers)
	{
		for (var i=0; i<this.triggers.length; i++)
		{
			var trigger = this.triggers[i];
			if (trigger.mandatory)
			{
				if (trigger.isEmpty(this))
				{
					return false;
				}
			}
		}
	}
	return true;
};
FlowNode.prototype.runLoop = function run()
{
	var child;
//		createParentElementReferences(context, flow);
	while (this.status != FlowStatus.PAUSED && this.hasReadyElements())
	{
		child = this.readyElements.shift();
		child.invoke(); //Start or resume
	}

};

FlowNode.prototype.hasReadyElements = function hasReadyElements()
{
	return (this.readyElements && this.readyElements.length > 0);
};

FlowNode.prototype.invoke = function invoke ()
{
	switch(this.status)
	{
		case FlowStatus.READY_TO_START:
			this.doStart();
			break;
		case FlowStatus.READY_TO_RESUME:
			this.doResume();
			break;
		default:
			internalError('Child with invalid status invoked '+ this.status);
	}
	if (this.status != FlowStatus.PAUSED)
		this.invokeLinks();
	//TODO - remove exit values / input values
};

FlowNode.prototype.chargeEmptyExit = function chargeEmptyExit(exit)
{
	if (! exit.isExit)
		exit = this.getExit(exit); // exit given by name
	if (exit)
	{
		exit.createChild(this);
		if (window.trace)
		{
			window.trace.charged(this, exit);
		};
	}
};

FlowNode.prototype.getExit = function (name)
{
	if (!this.exits)
		return null;
	var exit = this.getElement(name);
	if (exit && exit.isExit)
		return exit;
	else if (name.search(/^<.*>$/)>=0) // Distinguished name - allow perfixes
	{
		var l = name.length;
		for (var i=0; i<this.exits.length; i++)
		{
			var current = this.exits[i];
			var r=current.role;
			if (r.substr(-l) == name && r.indexOf('<')==r.length-l)
			{
				if (exit)
					return null; //ambigous
				exit = current;
			}
		}
	}
	return exit;
};

FlowNode.prototype.getTrigger = function (name)
{
	if (!this.triggers)
		return null;
	var trigger = this.getElement(name);
	if (trigger)
		return trigger;
	else if (name.search(/^<.*>$/)>=0) // Distinguished name - allow perfixes
	{
		var l = name.length;
		for (var i=0; i<this.triggers.length; i++)
		{
			var current = this.triggers[i];
			var r=current.role;
			if (r.substr(-l) == name && r.indexOf('<')==r.length-l)
			{
				if (trigger)
					return null; //ambigous
				trigger = current;
			}
		}
	}
	return trigger;
};
FlowNode.prototype.getTriggerLeafValue = function(role)
{
	var trigger = this.getTrigger(role);
	var value = null;
	if (trigger)
	{
		value = trigger.getChild(this);
		if (value != null)
			value = value.leafValue;
	}
	return value;
};
FlowNode.prototype.chargeExit = function (exit, value)
{
	if (! exit.isExit)
		exit = this.getExit(exit);
	if (exit)
	{
		exit.replaceChild(this,value);
		if (window.trace)
			window.trace.charged(this, exit, value);
	}
};
FlowNode.prototype.accumulateExitValue = function(exit, value)
{
	if (! exit.isExit)
		exit = this.getExit(exit);
	if (exit)
	{
		exit.addAChild(this,value);
		if (window.trace)
			window.trace.charged(this, exit, value);
	}
};

FlowNode.prototype.chargeLeafExit = function chargeLeafExit(exit, leafValue)
{

	if (! exit.isExit)
		exit = this.getExit(exit); // exit given by name
	if (exit && (leafValue != null) )
	{
		var child = exit.createChild(this);
		child.setLeaf(leafValue);
		if (window.trace)
			window.trace.charged(this, exit, child);
		
	}
};

FlowNode.prototype.getExecutionContext = function getExecutionContext()
{
	if (this.getParentContext())
		return this.getParentContext().getExecutionContext();
	else
		return this;
};

FlowNode.prototype.getParentContext = function getParentContext()
{
	return this.parent;
};

FlowNode.prototype.addTrigger = function addTrigger(role, modelId)
{
	if (! this.triggers)
		this.triggers = [];
	var trigger = new Trigger(role, modelId);
	this.triggers.push( trigger);
	return this.addElement(trigger);
};

FlowNode.prototype.addExit = function addExit(role, modelId)
{
	if (! this.exits)
		this.exits = [];
	var exit = new Exit(role, modelId);
	this.exits.push( exit);
	return this.addElement(exit);
};
FlowNode.prototype.addErrorExit = function addErrorExit(role, modelId)
{
	var e = this.addExit(role,modelId);
	if (this.errorExit)
		modelError('Multiple Error exits',this);
	this.errorExit = e;
	e.isError=true;
	return e;
};

FlowNode.prototype.getTriggerValues = function getTriggerValues(excludedTriggerName)
{
	var values = [];
	for (var i=0; i<this.triggers.length; i++)
	{
		var trigger = this.triggers[i];
		if (trigger.modelId != BuiltinModels.NOTHING_ID && trigger.role != excludedTriggerName )
		{
			if (trigger.isRepetitive)
			{
				values = values.concat(trigger.getChildren(this));
			}
			else
			{
				var value = trigger.getChild(this);
				if (value !== null && value !== undefined)
					values.push(value);	
			}
			
		}
	}
	return values;
};

FlowNode.prototype.getEventHandler = function(eventType)
{
	if (! this.subFlowMap)
		return null;
	var handler = this.subFlowMap[eventType.elementName];
	if (handler == undefined)
		return null;
	return handler;
};
FlowNode.prototype.hasEventHandler = function(eventType)
{
	return (this.getEventHandler(eventType) != null);
};

FlowNode.prototype.queueEvent = function (eventType, params, domEvent, callback, wait)
{
	var handler = this.getEventHandler(eventType);
	if (handler)
	{
		var eventAttrbutes = domEvent ? getEventAttributes(domEvent) : tersus.currentEventAttributes;
		var description = null;
		if (tlog)
		{
			description = eventType.methodName + ' ['+ this.getPath().str+']';
			tlog('queuing event:'+eventType.jsName ? eventType.jsName: eventType.elementName);
			if (eventAttrbutes)
				tlog('keyCode='+eventAttrbutes.keyCode+' type='+eventAttrbutes.type);
		}
		var c = handler.getChildConstructor();
		var p = c && c.prototype; // prototype or null
		
		var e = domEvent || tersus.currentEvent;
		if ( e &&  p.eventHandling)
		{
			if (p.eventHandling.match('stop_propagation'))
				e.stopPropagation();
			if (p.eventHandling.match('prevent_default'))
				e.preventDefault();
		    if (console) console.log(e.type+' ' + p.eventHandling);
		}
		
		if (p && p.runImmediately)
		{
			// Run a "background process" in current thread
			var process = new BackgroundProcess(this, handler, params);
			process.whenDone(callback);
			process.start(); 
		}
		else if (wait)
		{
		    var self=this;
		    tersus.async.exec(function()
		            {
		                window.engine.handleEvent(self, eventType, params, eventAttrbutes);
		            }, true /* wait */);
        }
		else
			queueEvent(window.engine, window.engine.handleEvent, [this,eventType,  params, eventAttrbutes], new Date(), description, callback);
	}
	else if (callback)
   		tersus.async.exec(callback,true);

};

FlowNode.map = {};
FlowNode.prototype.getNodeId = function()
{
        if (!this.nodeId)
        {
                this.nodeId = window.nextNodeId ? window.nextNodeId : 1;
                FlowNode.map[this.nodeId]=this;
                window.nextNodeId = this.nodeId + 1;
        }
        return this.nodeId;
};
FlowNode.prototype.destroy = function()
{
	if (this.nodeId)
		delete FlowNode.map[this.nodeId]
}
tersus.findNode = function findNode(id)
{
	return FlowNode.map[id];
}

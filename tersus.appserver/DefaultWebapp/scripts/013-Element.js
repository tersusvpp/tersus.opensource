/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function Element() {};
/**
	createChildInstance - creates an instance of the model refered by this element.
	 
	Note: the instance is not added to the parent node (which is not specified).

	
*/
Element.prototype.dependencyRank = -1;
Element.prototype.isRemovable = true;
Element.prototype.alwaysCreate = true;
Element.prototype.getChildConstructor = function (explicitModelId)
{
	var modelId = explicitModelId ? explicitModelId : this.modelId;
	var childConstructor;
	if (modelId)
		childConstructor = tersus.repository.get(modelId);
	else
		childConstructor = DataValue;
	if (! childConstructor)
	{
		var message ="'"+ this.modelId + "' not found in constructor repository.\n";
		message += "Existing consructors:\n";
		for (var modelId in tersus.repository.constructors)
		{
			message += "'" + modelId + "'\n";
		}
		tersus.error(message);
	}
	return childConstructor;
};
Element.prototype.createChildInstance = function createChildInstance(modelId)
{
	var childConstructor = this.getChildConstructor(modelId);
	var childInstance = new childConstructor();
	childInstance.prototype = childConstructor.prototype;
	return childInstance;
}

/*
	Creates a child instance, and stores it as a child.
	(This implementation is used by DataElements and Exits. Display Elements have a different implementation).
*/
Element.prototype.createChild = function createChild(parent)
{
	var childInstance = this.createChildInstance();
	this.addChild(parent, childInstance);
	return childInstance;
};	
Element.prototype.addChild = function addChild(parent, child)
{
	if (! parent.children)
		parent.children = {};
	if (this.isRepetitive)
	{
		if (! parent.children[this.role])
			parent.children[this.role] = [];
		parent.children[this.role].push(child);
	}
	else
	{
		parent.children[this.role] = child;
	}
};
Element.prototype.isEmpty = function isEmpty(parent)
{
	if (this.isRepetitive)
	{
		var children = this.getChildren(parent);
		return (! children || children.length ==0);
	}
	else
	{
		var child = this.getChild(parent);
		return (! child);
	}
};
Element.prototype.getChild = function getChild(parent)
{
	if ( this.isRepetitive)
		internalError("getChild(parent) called on repetitive element");
	var child = null;
	if (parent.children)
		child = parent.children[this.role];
	return child;
};

/*
	returns a list of children (child instances) of a given parent.
	If there are no such children, an empty list is returned.
	IMPORTANT:  The caller must not modify the returned list 
*/
Element.prototype.getChildren = function getChildren(parent)
{
	if ( ! this.isRepetitive)
		internalError("getChildren(parent) called on non repetitive element");
	var children = null;
	if (parent.children)
		children = parent.children[this.role];
	if (! children)
		children = [];
	return children;
};

Element.prototype.removeChildren = function removeChildren(parent)
{
    if (!this.isRepetitive)
        makeAssertion( this.isRepetitive, "removeChildren called on non-repetitive element "+this.role+" of "+ parent.modelId);
	var children = this.getChildren(parent);
	for (var i=0; i< children.length; i++)
	{
		var child = children[i];
		if (child && child.parent == parent)
			child.onDelete();
	}
	if (parent.children)
		parent.children[this.role] = [];
};

Element.prototype.removeNumberedChild = function removeNumberedChild(parent, index)
{
    if (!this.isRepetitive)
        makeAssertion( this.isRepetitive, "removeNumberedChild called on non-repetitive element "+this.role+" of "+ parent.modelId);
	var children = this.getChildren(parent);
	children.splice(index - 1, 1);
};

Element.prototype.setChildren = function setChildren(parent, values, operation)
{
	if ( ! this.isRepetitive)
		internalError("setChildren(parent, values, operation) called on non repetitive element "+this.role + " of "+parent.modelId);
	if (operation ==  Operation.REPLACE)
		this.removeChildren(parent);
	for (var i=0; i<values.length; i++)
	{
		var value = values[i];
		this.setChild(parent, value, Operation.ADD);
	}
};


Element.prototype.addAChild = function addAChild(parent,value)
{
	if (! parent.children)
		parent.children = {};
	if (!this.isRepetitive)
	    makeAssertion( this.isRepetitive,"addAChild called on non-repetitive element "+this.role+" of "+parent.modelId);
	if (! parent.children[this.role])
		parent.children[this.role] = [];
	parent.children[this.role].push(value);
};
Element.prototype.replaceChild = function replaceChild(parent, value)
{
    if (this.isRepetitive)
        makeAssertion( ! this.isRepetitive, "replaceChild called on repetitive element "+this.role+" of "+parent.modelId);
	if (! parent.children)
		parent.children = {};
	if (this.modelId && tersus.repository.get(this.modelId).prototype.isNothing && ! value.isNothing)
		this.createChild(parent); // Creates a new instance of Nothing
	else
		parent.children[this.role]=value;	
}
Element.prototype.removeChild = function removeChildren(parent)
{
    if (this.isRepetitive)
        makeAssertion( ! this.isRepetitive, "removeChild called on repetitive element "+this.role+" of "+parent.modelId);
	if (! parent.children)
		return;
	var child = this.getChild(parent);
	if (child != null)
	{
		if (parent.hasElementChangeHandler)
		{
			var oldValue = child.isDataNode ? child.value : child;
			parent.handleElementChange(this.role, oldValue, null);
		}
		delete parent.children[this.role];
		if (child && child.parent == parent)
			child.onDelete();
	}
};



Element.prototype.setChild = function setChild(parent, value, operation)
{
	if (! parent.children)
		parent.children = {};
	if (this.isRepetitive)
	{
		if (operation == Operation.ADD)
			this.addAChild(parent, value);
		else if (operation == Operation.REMOVE)
			this.removeChildren(parent);
		else
			internalError('Operation '+operation+' not supported for repetitive elements');
		
	}
	else
	{
		if (operation == Operation.REPLACE)
			this.replaceChild(parent, value,true);
		else if (operation == Operation.REMOVE)
			this.removeChild(parent);
		else
			
			internalError('Operation '+operation+' not supported for non-repetitive elements');
	}
};
Element.prototype.moveChild = function (parent, fromIndex, toIndex)
{
	var children = this.getChildren(parent);
	var child = children.splice(fromIndex, 1)[0];
	children.splice(toIndex,0,child);
	return child;
};
Element.prototype.getValueNode = function(value)
{
	if (value == null)
		return null;
	var valueNode;
	if (value.isNode)
		valueNode=value;
	else
	{
		valueNode=this.createChildInstance();
		valueNode.setLeaf(value);
	}
	return valueNode;
};
/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 
function Exit(role, modelId)
{
	this.role = role;
	if (modelId)
		this.modelId = modelId;
};
Exit.prototype = new Element();
Exit.prototype.constructor = Exit;
Exit.prototype.isExit=true;
Exit.prototype.isError=false;
Exit.prototype.type='Exit';
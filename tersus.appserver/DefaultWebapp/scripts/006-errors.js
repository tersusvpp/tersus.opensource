/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 tersus.Exception = function(message,cause)
{
	this.message = message;
	this.cause = cause;
};
tersus.Exception.prototype = new tersus.Exception(null,null);
tersus.Exception.prototype.toString = function()
{
	return this.message;
}
tersus.errors = null;

tersus.notify = function(title,msg)
{
	if (window.alert)
		window.alert(msg);
	else if (navigator.notification)
    {
	    navigator.notification.alert(msg, function(){}, title, 'OK');                 
    }

};

tersus.error = function(msg)
{
	if (window.tlog) 
		tlog(msg);
	else if (window.console)
		console.log(msg);
	if (tersus.errors)
		tersus.errors.push(msg);
	else 
		tersus.notify('Error',msg);
};

tersus.getErrors = function()
{	
	if (tersus.errors == null || tersus.errors.length == 0)
		return null;
	else
		return tersus.errors.join('\n');
};

tersus.clearErrors = function()
{
	tersus.errors=[];
};

tersus.serverError = function(msg)
{
	tersus.error(msg);
	if (tersus.pendingServiceCalls)
	{
		var list = tersus.pendingServiceCalls.toList();
		if (list.length == 1)
		{
			var node = tersus.findNode(list[0]);
			if (node && node.handleResponse)
			{
				node.handleResponse({});
			}
		}
		else if (length > 1)
		{
			tersus.error("Can't identify caller.  Please refresh.");
		}
	}
};
/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 function DataElement(role, modelId)
{
	this.role = role;
	if (modelId)
		this.modelId = modelId;
};
DataElement.prototype = new Element();
DataElement.prototype.constructor = DataElement;
DataElement.prototype.type = 'Data Element';
DataElement.prototype.isDataElement = true;
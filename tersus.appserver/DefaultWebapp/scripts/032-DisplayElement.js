/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 
function DisplayElement(role, modelId)
{
	this.role = role;
	this.modelId = modelId;
}
DisplayElement.prototype = new SubFlow();
DisplayElement.prototype.constructor = DisplayElement;
DisplayElement.prototype.isDisplayElement = true;
DisplayElement.prototype.type = 'Display Element';
DisplayElement.prototype.addAChild = function addChild(parent, value)
{
	if (parent.currentWindow)
	{
		this.createChild(parent, value.modelId, value);
	}
	else
	{
		Element.prototype.addAChild.call(this, parent, value);
	}
};

DisplayElement.prototype.replaceChild = function replaceChild(parent, value, reset)
{
	if (parent.currentWindow) // parent is a real display node
	{
		var oldChild = this.getChild(parent);
		if (oldChild)
		{
			if (oldChild.modelId == value.modelId)
			{
				if (reset)
					oldChild.reset();
				child = oldChild;
				child.copyValue(value);
			}
			else
			{
				var oldViewNode = null;
				if (oldChild != null)
				{
					var placeHolder = oldChild.wrapperNode ? oldChild.wrapperNode: oldChild.viewNode;
					oldChild.viewNode = oldChild.wrapperNode = null;
					placeHolder.style.display = 'none';
					this.removeChild(parent);
				}
				child = this.createChild(parent,value.modelId, value, placeHolder);
			}
		}
		else
			child = this.createChild(parent,value.modelId, value);
	}
	else
	{
		Element.prototype.replaceChild.call(this, parent, value);
	}
}
DisplayElement.prototype.moveChild = function (parent, fromIndex, toIndex)
{
	var children = this.getChildren(parent);
	var currentChild = null;
	if (children.length > 0)
		currentChild = children[toIndex];
		
	var child = Element.prototype.moveChild.call(this,parent,fromIndex,toIndex);
	if (child.viewNode && currentChild)
	{
		// Move the new child before the current child
		child.viewNode.parentNode.insertBefore(child.viewNode, currentChild.viewNode);
	}
	return child;
};

DisplayElement.prototype.removeNumberedChild = function removeNumberedChild(parent, index)
{
	makeAssertion( this.isRepetitive);
	var children = this.getChildren(parent);
	children[index - 1].deleteMe();
};


// As opposed to regular SubFlows, DisplayElements are added to the parent node
DisplayElement.prototype.createChildWithoutVisuals = function createChildWithoutVisuals(parent, modelId)
{
	var child = SubFlow.prototype.createChild.call(this, parent, modelId);
	this.addChild(parent, child);
	if (parent.currentWindow && ! child.isPopup)
	{
		child.setInitialStatus();
	}
	return child;
};
DisplayElement.prototype.createChild = function createChild(parent, modelId, value, placeHolderNode)
{
	var child = this.createChildWithoutVisuals(parent,modelId);
	if (parent.currentWindow && ! child.isPopup)
	{
		child.placeHolderNode = placeHolderNode;
		child.create(value);
	}
	return child;
};
DisplayElement.prototype.init = function DisplayElement_init(parent)
{
	// There is no need to initialize display elements, as the whole  hierarchy is created in advance
	// Except for popups (which are more like regular subflows)
	// TODO clean this up (the generic DisplayElement should not know about popups)
	if (tersus.repository.get(this.modelId).prototype.isPopup)
	{
		SubFlow.prototype.init.call(this,parent);
	}
	
};

DisplayElement.prototype.prop = function(name)
{
	
	 var local = this.properties[name];
	 if (local != null)
	 	return local;
	 var childConstructor = this.getChildConstructor();
	 if (!childConstructor)
	 	return null;
	 return childConstructor.prototype.sharedProperties[name];
};

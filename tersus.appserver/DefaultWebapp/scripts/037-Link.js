/*********************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. , All rights reserved.
 *
 * This program is made available under the terms of the GNU Lesser General Public License, version 2.1,
 * which is part of this distribution and is available at hhttp://www.gnu.org/licenses/lgpl-2.1.txt
 **********************************************************************************************************/
 
function Link(role, source, target)
{	
	this.role = role;
	this.source = new Path(source);
	this.target = new Path(target);
};
Link.prototype.type= 'Flow';
Link.prototype.invoke = function invoke(sourceNode)
{
	var values;
	var parent = sourceNode.parent;
	// get source value
	values = sourceNode.getValues(this.source, 1);
	var targetElement = parent.getElement(this.target.segments[0]);
	if (! targetElement)
		return; // The target element may be null if access control excluded it from the user's application
	if (window.trace && values.length > 0)
			window.trace.flow(this, parent, values.length + " values");
	for (var i=0; i<values.length; i++)
	{
		var value = values[i];
		
		if (this.isConstantTrigger)
		{
			var constantElement = targetElement;
			constantElement.createChild(parent);
		}
		else
		{
			if (window.trace)
			{
				window.trace.flowValue(this, parent, value);
			}
			if (targetElement.isSubFlow && targetElement.isRepetitive)
			{
				var inputSet = parent.children[targetElement.role];
				if (!inputSet)
				{
					inputSet = new InputSet(parent, targetElement);
					parent.children[targetElement.role] = inputSet;
				}
				makeAssertion(this.target.segments.length == 2);
				var triggerRole = this.target.segments[1];
				inputSet.addValue(triggerRole, value);
				inputSet.checkReady();
			}
			else
			{
				// set target value
				sourceNode.parent.setValue(this.target, value, this.operation);
			
				if (! targetElement.isRepetitive)
				{
					var targetNode = targetElement.getChild(parent);
					if (targetNode && targetNode.checkReady)
						targetNode.checkReady();
				}
			}
		}
		
	}
};

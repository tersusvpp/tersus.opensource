tersus.weinre = function(promptForPort, userid, host)
{
	userid = userid || 'anonymous';

	if (!host)
	{
	    var host = document.domain;
    	var serverHost = tersus.serverURL ? tersus.serverURL.match(/\/\/([^:/]+)[:/]/)[1] : null;
    	if (serverHost != null)
            host=serverHost;
    }
    var port = 8999;
    if (promptForPort)
    	port = prompt("Weinre port number",port);
    // tersus.notify('Info','Remote debugging with host:'+host + ' port:'+port+ ' user:'+userid);
    tersus.loadScript("http://"+host+":"+port+"/target/target-script-min.js#"+userid);
};

/**
 * $Id: editor_plugin_src.js 201 2007-02-12 15:56:56Z spocke $
 *
 * @author Moxiecode
 * @copyright Copyright  2008, Tersus Software Ltd., All rights reserved.
 */

(function() {
	// Load plugin specific language pack
	tinymce.PluginManager.requireLangPack('attach');

	tinymce.create('tinymce.plugins.Attach', {
		/**
		 * Initializes the plugin, this will be executed after the plugin has been created.
		 * This call is done before the editor instance has finished it's initialization so use the onInit event
		 * of the editor instance to intercept that event.
		 *
		 * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
		 * @param {string} url Absolute URL to where the plugin is located.
		 */
		init : function(ed, url) {
			// Register the command so that it can be invoked by using tinyMCE.activeEditor.execCommand('mceExample');
			ed.addCommand('mceAttach', function() {
				alert('Attach');
			});

			// Register example button
			ed.addButton('attach', {
				title : 'attach.desc',
				cmd : 'mceAttach',
				image : url + '/img/attach.gif'
			});

		},

		createControl : function(n, cm) {
			return null;
		},

		getInfo : function() {
			return {
				longname : 'Attachment Button',
				author : 'Tersus Software Ltd.',
				authorurl : 'http://www.tersus.com',
				infourl : 'http://www.tersus.com',
				version : '1.0'
			};
		}
	});

	// Register plugin
	tinymce.PluginManager.add('attach', tinymce.plugins.Attach);

	// Register tersus command
	tersus.HTMLEditor.commandMap.mceAttach = '<Add Attachment>';
	
})();
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.util.List;

import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.runtime.DataHandler;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.trace.EventType;

/**
 * An atomic flow handler that checks if it has received a value through its trigger
 * 
 * @deprecated Use tersus.plugins.collections.Exists
 * 
 * @author Youval Bronicki
 *
 */
public class ExistsHandler extends FlowHandler
{
	SlotHandler trigger, yesExit, noExit;

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);
		Object input = trigger.get(context, flow);
		if (trigger.isRepetitive())
		{
			if (((List) input).size() == 0)
				input = null;
			else
				input = Boolean.TRUE;
		}
		if (yesExit != null && input != null)
		{

			yesExit.set(context, flow, input);
		}
		else if (noExit != null && input == null)
		{
			noExit.set(context, flow, Boolean.TRUE);
		}
		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		trigger = getSoleTrigger();
		if (trigger == null)
			throw new InvalidModelException("Missing trigger in " + model.getId());
		yesExit = (SlotHandler) getElementHandler("Yes");
		noExit = (SlotHandler) getElementHandler("No");

	}
}

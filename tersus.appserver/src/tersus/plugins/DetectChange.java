/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.NotImplementedException;

/**
 * An atomic flow handler that detect changes in composite date value The plugin
 * performs a deep comparison of 2 data structures (with some tolerance for
 * numeric values), If differences are found, a 'Change Record' is created,
 * which contains only the new values for elements that have been modified.
 * 
 * Note: Comparison of repetitive stuctures is not implement. Currently, if a
 * repetitive structure is found, a NotImplementedException is thrown (it is
 * currently not clear what should be thw behavior in such a case).
 * 
 * @see instanceDeepCompare
 * 
 * Note: We need a way to determine the case where an element is missing in the
 * new data structure. We currenly put a StringConstant ' <Empty>', but that may
 * not be a good solution.
 * 
 * @author Youval Bronicki
 *  
 */

// BUG3 Assuming the implementation of composite data types as arrays.
public class DetectChange extends Plugin
{
    SlotHandler newTrigger, oldTrigger, changeExit, noChangeExit;

    private static final String EMPTY = "<Empty>";

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {

        Object oldValue = oldTrigger.get(context, flow);
        Object newValue = newTrigger.get(context, flow);
        Object change = detectChange(context, newTrigger
                .getChildInstanceHandler(), newValue, oldValue);

        if (change == null)
            setExitValue(noChangeExit, context, flow, Boolean.TRUE);
        else
            setExitValue(changeExit, context, flow, change);
    }

    /**
     * @param context
     * @param newValue
     * @param childInstanceHandler
     * @param oldValue
     * @param childInstanceHandler2
     * @return
     */
private Object detectChange(RuntimeContext context, InstanceHandler handler, Object newValue, Object oldValue)
    {
		if (newValue == null && oldValue == null)
		        return null;
		else if (newValue == null && oldValue != null)
		 		return EMPTY;
		else if (newValue != null && oldValue == null)
		    return newValue;
		else // both values are not null
		{
		    if (handler instanceof LeafDataHandler )
		    {
		        if (((LeafDataHandler)handler).equal(newValue, oldValue))
		            return null;
		        else
		            return newValue;
		    }
		    else
		    {
		        Object change = null;
		        for (int i=0; i<handler.elementHandlers.length; i++)
		        {
		            ElementHandler e = handler.elementHandlers[i];
		            if (e.isRepetitive())
		                throw new NotImplementedException("Comparing repetitive structures not implemented yet");
		            Object elementChange = detectChange(context, e.getChildInstanceHandler(), e.get(context, newValue), e.get(context, oldValue));
		            if (elementChange != null)
		            {
		                if (change == null)
		                    change = handler.newInstance(context);
		                e.set(context, change, elementChange);
		            }
		        }
		        return change;    
		    }
		}
    }
    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
     */
    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);

        if (triggers.length < 1)
            throw new InvalidModelException(model.getId() + " has "
                    + triggers.length + " triggers (must have at least 1");

        changeExit = (SlotHandler) getElementHandler("<Change>");
        noChangeExit = (SlotHandler) getElementHandler("<No Change>");
        newTrigger = (SlotHandler) getElementHandler("<New>");
        oldTrigger = (SlotHandler) getElementHandler("<Old>");
    }
}
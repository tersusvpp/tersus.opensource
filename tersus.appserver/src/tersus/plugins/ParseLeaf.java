/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.runtime.EngineException;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.InstanceHandler;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.Misc;

/**
 * An atomic flow handler that converts a String to another Leaf type (specified by the model's exit)
 * 
 * @author Youval Bronicki
 *
 */

public class ParseLeaf extends FlowHandler
{
	SlotHandler trigger;
	SlotHandler exit;

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);
		InstanceHandler outputHandler = exit.getChildInstanceHandler();
		String text = (String) trigger.get(context, flow);
		if (text != null)
		{
			if (outputHandler == null)
			{
				throw new InvalidModelException("Missing type for exit of " + getModelId());
			}
			Object out = null;

			try
			{
				out = outputHandler.newInstance(context, text, null, false);
				if (out == null)
					throw new EngineException("Failed to convert text", "Target type="+outputHandler.getModelId()+ " text='"+text+"'", null);
				setExitValue(exit, context, flow, out);
			}
			catch (EngineException e)
			{
				fireError(context, flow, e);
			}
			catch (RuntimeException e)
			{
				fireError(context, flow, new EngineException("Failed to convert text", "Target type="+outputHandler.getModelId()+ " text='"+text+"'", e));
			}
		}
		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		trigger = getSoleTypedTrigger();
		exit = getSoleExit();
		if (Misc.ASSERTIONS)
			Misc.assertion(trigger != null);
	}
}

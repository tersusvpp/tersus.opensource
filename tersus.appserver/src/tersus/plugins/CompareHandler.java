/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.DataHandler;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.Number;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.trace.EventType;

/**
 * An atomic flow handler that compares two numbers.
 * 
 * @deprecated Use tersus.plugins.math.Compare
 * 
 * @author Ofer Brandes
 *
 */
public class CompareHandler extends FlowHandler
{
	SlotHandler inputX, inputY;
	SlotHandler firstSmallerExit, equalExit, firstBiggerExit;

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);
		Number x = (Number) inputX.get(context, flow);
		Number y = (Number) inputY.get(context, flow);
		SlotHandler exit =
			(x.value == y.value ? equalExit : (x.value < y.value ? firstSmallerExit : firstBiggerExit));
		Object exitValue = Boolean.TRUE;
		exit.set(context, flow, exitValue);
		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		inputX = (SlotHandler) getElementHandler(Role.get("X"));
		inputY = (SlotHandler) getElementHandler(Role.get("Y"));

		firstSmallerExit = (SlotHandler) getElementHandler(Role.get("X<Y"));
		equalExit = (SlotHandler) getElementHandler(Role.get("X=Y"));
		firstBiggerExit = (SlotHandler) getElementHandler(Role.get("X>Y"));
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

/**
 * Thrown to indicate an error in a file format.
 * @author Youval Bronicki
 *
 */
public class InvalidFileFormatException extends RuntimeException
{
	/**
	 * 
	 */
	public InvalidFileFormatException()
	{
		super();
	}
	/**
	 * @param message
	 */
	public InvalidFileFormatException(String message)
	{
		super(message);
	}
	/**
	 * @param cause
	 */
	public InvalidFileFormatException(Throwable cause)
	{
		super(cause);
	}
	/**
	 * @param message
	 * @param cause
	 */
	public InvalidFileFormatException(String message, Throwable cause)
	{
		super(message, cause);
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.util.ArrayList;
import java.util.List;

import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that removes from a list of objects any occurence of a specific object (or objects from another list)
 * 
 * @deprecated Use tersus.plugins.collections.ExcludeFromList
 * 
 * @author Ofer Brandes
 *
 */

public class ExcludeFromList extends FlowHandler
{
	private static final Role LIST_ROLE = Role.get("<List>");
	private static final Role EXCLUDE_ROLE = Role.get("<Exclude>");

	SlotHandler excludeTrigger, listTrigger;
	SlotHandler exit;

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);

		List list = (List) listTrigger.get(context,flow);

		Object excluded = excludeTrigger.get(context,flow);
		
		List left = new ArrayList(list);
		if (excludeTrigger.isRepetitive())
			left.removeAll((List) excluded);
		else
			left.remove(excluded);

		exit.set(context, flow, left);

		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		listTrigger = (SlotHandler) getElementHandler(LIST_ROLE);
		if (listTrigger == null)
			throw new InvalidModelException("Missing '"+LIST_ROLE+"' trigger in " + model.getId());

		excludeTrigger = (SlotHandler) getElementHandler(EXCLUDE_ROLE);
		if (excludeTrigger == null)
			throw new InvalidModelException("Missing '"+EXCLUDE_ROLE+"' trigger in " + model.getId());

		exit = (SlotHandler) getSoleRepetitiveExit();
		if (exit == null)
			throw new InvalidModelException("Missing repetitive exit in " + model.getId());
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that returns the set of unique items in its repetitive trigger
 * 
 * @deprecated Use tersus.plugins.collections.UniqueItems
 * 
 * @author Youval Bronicki
 *
 */
public class UniqueItems extends FlowHandler
{
	SlotHandler trigger, exit;

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);
		HashSet outputs = new HashSet();
		List inputs = (List)trigger.get(context, flow);
		for (Iterator i = inputs.iterator(); i.hasNext();)
		{
			
			Object input = i.next();
			if (! outputs.contains(input))
			{
				accumulateExitValue(exit, context, flow, input);
				outputs.add(input);
			}
		}
		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}


	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		trigger = getSoleRepetitiveTrigger();
		if (trigger == null)
			throw new InvalidModelException("Missing trigger in " + model.getId());

		exit = getSoleExit();
		if (exit == null)
			throw new InvalidModelException("Missing exit in " + model.getId());

		if (!exit.isRepetitive())
			throw new InvalidModelException("Non-repetitive exit in " + model.getId());
	}
}

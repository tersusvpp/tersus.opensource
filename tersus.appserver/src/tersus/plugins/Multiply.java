/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.runtime.DataHandler;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.Number;
import tersus.runtime.NumberHandler;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.trace.EventType;
import tersus.util.NotImplementedException;

/**
 * An atomic flow handler that multiplies all input values
 * 
 * @deprecated Use tersus.plugins.math.Multiply
 * 
 * @author Youval Bronicki
 *
 */
public class Multiply extends FlowHandler
{
	SlotHandler exit;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);
		double result = 1.0;
		for (int i=0; i<triggers.length; i++)
		{
			SlotHandler trigger = triggers[i];
			if (trigger.isRepetitive())
			{
				throw new NotImplementedException("Multiplying repetitive values not implemented yet. Path="+ context.currentFlowPath +  " modelId="+getModelId());
			}
			else
			{
				Number input = (Number) trigger.get(context, flow);
				result *= input.value;
			}
		}
		
		Number out = new Number(result);
		exit.set(context, flow, out);

		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		exit = getSoleExit();
		for (int i=0; i<triggers.length; i++)
		{
			if (!(triggers[i].getChildInstanceHandler() instanceof NumberHandler))
				throw new InvalidModelException("Trigger "+triggers[i].getRole()+ " of  " + getModelId() + " is not a number");
		}
	}
}

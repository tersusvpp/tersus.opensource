/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.util.List;

import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.runtime.DataHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.Number;
import tersus.runtime.NumberHandler;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.trace.EventType;

/**
 * An atomic flow handler that calculates the average of all input values
 * 
 * @deprecated Use tersus.plugins.math.Average
 * 
 * @author Ofer Brandes
 *
 */
public class Average extends FlowHandler
{
	SlotHandler exit;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);
		double sum = 0.0;
		int numberOfNumbers = 0;
		for (int i=0; i<triggers.length; i++)
		{
			SlotHandler trigger = triggers[i];
			if (trigger.isRepetitive())
			{
				List values = (List)trigger.get(context, flow);
				for (int j=0; j<values.size();j++)
				{
					Number input = (Number)values.get(j);
					sum+= input.value;
					numberOfNumbers++;
				}
			}
			else
			{
				Number input = (Number) trigger.get(context, flow);
				sum += input.value;
				numberOfNumbers++;
			}
		}

		if (numberOfNumbers == 0)	
			fireError(context, flow, new EngineException("Cannot compute an average of no numbers",null,null));

		Number out = new Number(sum/numberOfNumbers);
		exit.set(context, flow, out);

		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		exit = getSoleExit();
		for (int i=0; i<triggers.length; i++)
		{
			if (!(triggers[i].getChildInstanceHandler() instanceof NumberHandler))
				throw new InvalidModelException("Trigger "+triggers[i].getRole()+ " of  " + getModelId() + " is not a number");
		}
	}
}

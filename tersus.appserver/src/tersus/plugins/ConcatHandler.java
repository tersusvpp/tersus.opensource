/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import tersus.model.Model;
import tersus.runtime.DataHandler;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.trace.EventType;

/**
 * An atomic flow handler that concatenates a number of input strings
 * 
 * @deprecated Use tersus.plugins.text.Concatenate
 * 
 * @author Youval Bronicki
 *
 */
public class ConcatHandler extends FlowHandler
{
	private ElementHandler separatorTrigger;
	ArrayList inputs;
	SlotHandler exit;
	public static final String SEPARATOR = "Separator";

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);
		StringBuffer out = new StringBuffer();
		String separatorString = null;
		if (separatorTrigger != null)
			separatorString = (String) separatorTrigger.get(context, flow);
		int nValues = 0;
		for (int i = 0; i < inputs.size(); i++)
		{
			SlotHandler trigger = (SlotHandler) inputs.get(i);
			Object value = trigger.get(context, flow);
			//FUNC3 ignore non-atomic inputs (what about validation?)
			if (value != null)
			{
				if (!trigger.isRepetitive())
				{
					if (nValues > 0 && separatorString != null)
						out.append(separatorString);
					nValues++;
					out.append(value);
				}
				else
				{
					List values = (List) value;
					for (int j = 0; j < values.size(); j++)
					{
						if (nValues > 0 && separatorString != null)
							out.append(separatorString);
						nValues++;
						out.append(values.get(j));
					}
				}
			}
		}
		String exitValue = out.toString();
		exit.set(context, flow, exitValue);

		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{ // Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 * 
	 * 
	 * 
	 * Create a triggers sorted by lexicographically (by role)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		inputs = new ArrayList();
		//FUNC3 Ignore empty triggers
		for (int i = 0; i < triggers.length; i++)
		{
			if (!triggers[i].getRole().toString().equals(SEPARATOR))
				inputs.add(triggers[i]);
		}
		separatorTrigger = getElementHandler(SEPARATOR);

		Comparator c = new Comparator()
		{

			public int compare(Object o1, Object o2)
			{
				SlotHandler trigger1 = (SlotHandler) o1;
				SlotHandler trigger2 = (SlotHandler) o2;
				return (trigger1.getRole().toString().compareTo(trigger2.getRole().toString()));
			}
		};
		Collections.sort(inputs, c);
		exit = getSoleExit();
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.runtime.BinaryValue;
import tersus.runtime.CompositeDataHandler;
import tersus.runtime.DataHandler;
import tersus.runtime.DateHandler;
import tersus.runtime.ElementHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.NumberHandler;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.TextHandler;
import tersus.util.Misc;
/**
 * An atomic flow handler that loads a simple table from an Excel file.
 * 
 * @param inputFile	The Excel file (1st row - titles, rest - data)
 * @param rows		Multiple collections of leaves (or just one collection if not repetitive),
 *					where the role of each leaf corresponds to the title of one columns to load
 * @param error		Optional error message
 *
 * @deprecated Use tersus.plugins.variuos.LoadExcelTable
 * 
 * @author Ofer Brandes
 *
 */
public class LoadExcelTable extends Plugin
{
	private static final boolean DEBUG = false;
	SlotHandler fileContentTrigger, sheetNameTrigger, exitHandler;
	private static String FILE_CONTENT_ROLE = "<File Content>";
	private static String SHEET_NAME_ROLE = "<Sheet Name>";
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		BinaryValue data = (BinaryValue) fileContentTrigger.get(context, flow);
		try
		{
			// Create a new org.apache.poi.poifs.filesystem.Filesystem
			POIFSFileSystem poifs;
			try
			{
				poifs = new POIFSFileSystem(new ByteArrayInputStream(data.toByteArray()));
			}
			catch (IOException e)
			{
				throw new EngineException(
					"Failed to read data (possibly not a valid excel file)'",
					null,
					e);
			}
			// Create the workbook
			HSSFWorkbook wb;
			try
			{
				wb = new HSSFWorkbook(poifs);
			}
			catch (IOException e)
			{
				throw new EngineException(
					"Failed to read data (possibly not a valid excel file)'",
					null,
					e);
			}
			// Get the first sheet
			HSSFSheet sheet = null;
			if (sheetNameTrigger == null)
				sheet = wb.getSheetAt(0);
			else
			{
				String sheetName = (String) sheetNameTrigger.get(context, flow);
				if (sheetName == null)
					throw new ModelExecutionException("Missing value for '" + SHEET_NAME_ROLE + "'");
				sheet = wb.getSheet(sheetName);
				if (sheet == null)
					throw new ModelExecutionException("Missing sheet '" + sheetName + "' in Excel file");
			}
			// Convert each row to an output collection (or just the first if the output is not repetitive)
			HSSFRow titleRow = sheet.getRow(0);
			if (titleRow == null)
			{
				throw new EngineException("Missing title row in excel file", null, null);
			}
			CompositeDataHandler outputDataHandler =
				(CompositeDataHandler) exitHandler.getChildInstanceHandler();
			checkTitleRow(titleRow, outputDataHandler, context);
			for (int r = 1; r <= sheet.getLastRowNum(); r++)
			{
				HSSFRow row = sheet.getRow(r);
				if (row != null)
				{
					Object rowObj = null;
					rowObj = loadExcelRow(titleRow, row, outputDataHandler, context);
					if (rowObj == null)
					{
						throw new EngineException(
							"Missing data in row " + r + 1 + " in excel file",
							null,
							null);
					}
					else if (exitHandler.isRepetitive())
					{
						accumulateExitValue(exitHandler, context, flow, rowObj);
					}
					else
					{
						setExitValue(exitHandler, context, flow, rowObj);
						break;
					}
				}
			}
		}
		catch (RuntimeException e)
		{
			fireError(context, flow, e);
		}
	}
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}
	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		fileContentTrigger = (SlotHandler) getElementHandler(FILE_CONTENT_ROLE);
		if (fileContentTrigger == null)
			fileContentTrigger = getSoleTrigger();
		if (fileContentTrigger == null)
			throw new InvalidModelException(
				"Missing trigger '" + FILE_CONTENT_ROLE + "' in '" + getModelId() + "'");
		sheetNameTrigger = (SlotHandler) getElementHandler(SHEET_NAME_ROLE);
		exitHandler = getSoleExit();
	}
	/**
	 * Checks if all (leaf) elements in the output data model appear in the title row
	 * @throws InvalidInputException if any of the elements are missing in the title row 
	 */
	private void checkTitleRow(HSSFRow titleRow, CompositeDataHandler handler, RuntimeContext context)
	{
		ElementHandler elementHandler;
		DataHandler leafHandler;
		List missingColumns = new ArrayList();
		for (int i = 0; i < handler.getNumberOfElements(); i++)
		{
			elementHandler = handler.getElementHandler(i);
			String role = elementHandler.getRole().toString();
			leafHandler = (DataHandler) elementHandler.getChildInstanceHandler();
			if (leafHandler instanceof LeafDataHandler)
			{
				boolean found = false;
				for (short c = 0; c <= titleRow.getLastCellNum(); c++)
				{
					HSSFCell titleCell = titleRow.getCell(c);
					if (compareCellContent(titleCell, role))
					{
						found = true;
						break;
					}
				}
				if (!found)
					missingColumns.add(role);
			}
			else
			{
				// CORE2 Handle composite structures
			}
		}
		if (!missingColumns.isEmpty())
		{
			throw new InvalidFileFormatException(
				"The following columns are missing in the title row (the title row is the first row in the file): "
					+ Misc.concatenateList(missingColumns, ","));
		}
	}
	/**
	 * Checks whether a cell contains a given String as its content.
	 * When checking, whitespace is removed as a workaround for a suspected HSSF bug.
	 * @param cell - a cell whose content is to be compared.
	 * @param content - a String to be compared to the cell's content.
	 * @return
	 */
	private boolean compareCellContent(HSSFCell cell, String content)
	{
		if (cell == null)
			return false;
		if (cell.getCellType() != HSSFCell.CELL_TYPE_STRING)
			return false;
		return cell.getStringCellValue().equals(content) || cell.getStringCellValue().trim().equals(content);
	}
	/**
	 * @param row
	 * @param handler
	 * @return
	 */
	private Object loadExcelRow(
		HSSFRow titleRow,
		HSSFRow row,
		CompositeDataHandler handler,
		RuntimeContext context)
	{
		Object rowObj = handler.newInstance(context);
		ElementHandler elementHandler;
		DataHandler leafHandler;
		for (int i = 0; i < handler.getNumberOfElements(); i++)
		{
			elementHandler = handler.getElementHandler(i);
			String role = elementHandler.getRole().toString();
			leafHandler = (DataHandler) elementHandler.getChildInstanceHandler();
			if (leafHandler instanceof LeafDataHandler)
			{
				for (short c = 0; c <= titleRow.getLastCellNum(); c++)
				{
					HSSFCell titleCell = titleRow.getCell(c);
					if (compareCellContent(titleCell, role))
					{
						HSSFCell cell = row.getCell(c);
						Object leafObj = null;
						if ((cell != null) && (cell.getCellType() != HSSFCell.CELL_TYPE_BLANK))
						{
							if (leafHandler instanceof DateHandler)
							{
								Date date = cell.getDateCellValue();
								leafObj = ((LeafDataHandler) leafHandler).newInstance(context, date);
							}
							else if (leafHandler instanceof NumberHandler)
							{
								double number = Double.NaN;
								if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING)
								{
									String stringValue = cell.getStringCellValue();
									if (stringValue != null && stringValue.trim().length() != 0)
									{
										try
										{
											number = Double.parseDouble(stringValue);
										}
										catch (NumberFormatException e)
										{
											throw new CellFormatException(
												"The cell contains a string value ('"
													+ stringValue
													+ "') and not numeric value. ",
												row.getRowNum() + 1,
												cell.getCellNum() + 1);
										}
									}
								}
								else
								{
									try
									{
										number = cell.getNumericCellValue();
									}
									catch (NumberFormatException e)
									{
										throw new CellFormatException(
											"The cell does not contain a numeric value",
											row.getRowNum() + 1,
											cell.getCellNum() + 1);
									}
								}
								if (Double.isNaN(number))
									leafObj = null;
								else
									leafObj = ((NumberHandler) leafHandler).newInstance(context, number);
							}
							else if (leafHandler instanceof TextHandler)
							{
								String text = getCellContentAsString(cell);
								leafObj = ((TextHandler) leafHandler).newInstance(context, text, false);
							}
							if (leafObj != null)
							{
								elementHandler.set(context, rowObj, leafObj);
								break;
							}
						}
					}
				}
			}
			else
			{
				// CORE2 Handle composite structures
			}
		}
		return rowObj;
	}
	/**
	 * @param cell
	 * @return
	 */
	private String getCellContentAsString(HSSFCell cell)
	{
		String stringValue = null;
		switch (cell.getCellType())
		{
			case HSSFCell.CELL_TYPE_BLANK :
				break;
			case HSSFCell.CELL_TYPE_BOOLEAN :
				stringValue = String.valueOf(cell.getBooleanCellValue());
				break;
			case HSSFCell.CELL_TYPE_ERROR :
				stringValue = "Error(code=" + cell.getErrorCellValue() + ")";
				break;
			case HSSFCell.CELL_TYPE_FORMULA :
				{
					if (!Double.isNaN(cell.getNumericCellValue()))
						stringValue = numberAsString(cell.getNumericCellValue());
					else
						stringValue = cell.getStringCellValue();
					break;
				}
			case HSSFCell.CELL_TYPE_NUMERIC :
				{
					stringValue = numberAsString(cell.getNumericCellValue());
					break;
				}
			case HSSFCell.CELL_TYPE_STRING :
				stringValue = cell.getStringCellValue();
				break;
		}
		if (stringValue != null && stringValue.length() == 0)
			stringValue = null;
		return stringValue;
	}
	private String numberAsString(double number)
	{
		if (Double.isNaN(number))
			return null;
		else if (number == ((long) number))
			return String.valueOf((long) number);
		else
			return String.valueOf(number);
	}
}

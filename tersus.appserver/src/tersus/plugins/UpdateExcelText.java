/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.BinaryValue;
import tersus.runtime.DataHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.InstanceHandler;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.trace.EventType;
import tersus.util.Template;

/**
 * An atomic flow handler that replaces specific sub-strings in an Excel sheet.
 * 
 * @param <File>		The Excel file
 * @param ...			The file contains place-holders of the form "$(Role)",
 *						which are substituted by the content of the slot with role 'Role'.
 *						E.g. if the files contains a cell with the text "Week ${Week Number}",
 *						and the action has input trigger 'Week Number',
 *						then assuming 'Week Number' is 5,
 *						the cell content will be updated to "Week 5".
 * @param outputFile	The updated Excel file (with the rows replacing the first rows of the sub-table)
 * @param error			Optional error message
 * 
 * @deprecated Use tersus.plugins.variuos.UpdateExcelText
 * 
 * @author Ofer Brandes
 *
 */

public class UpdateExcelText extends FlowHandler
{
	private static final String INPUT_FILE_ROLE = "<File>";
		
	private static final boolean DEBUG = false;
	SlotHandler inputHandler, outputHandler;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);

		BinaryValue data = (BinaryValue) inputHandler.get(context, flow);

		try
		{
			// Create a new org.apache.poi.poifs.filesystem.Filesystem
			POIFSFileSystem poifs;
			
			try
			{
				poifs = new POIFSFileSystem(new ByteArrayInputStream(data.toByteArray()));
			}
			catch (IOException e)
			{
				throw new EngineException("Failed to read data (possibly not a valid excel file)'", null, e);
			}
	
			// Create the workbook
			HSSFWorkbook wb;
	
			try
			{
				wb = new HSSFWorkbook(poifs);
			}
			catch (IOException e)
			{
				throw new EngineException("Failed to read data (possibly not a valid excel file)'", null, e);
			}
			
			// Get the first sheet
			HSSFSheet sheet = wb.getSheetAt(0);
			String sheetName = wb.getSheetName(0);

			// Get the input to be inserted into the sheet
			Role fileTriggerRole = inputHandler.getRole();
			
			Map properties = new HashMap();
			for (int i = 0; i < triggers.length; i++)
			{
				SlotHandler trigger = triggers[i];
				Role role = trigger.getRole();
				if (!fileTriggerRole.equals(role))
				{
					Object value = trigger.get(context,flow);
					if (value == null)
						value = "";
					InstanceHandler childInstanceHandler = trigger.getChildInstanceHandler();
					if (childInstanceHandler instanceof LeafDataHandler)
						properties.put(role.toString(), ((LeafDataHandler)childInstanceHandler).toString(context,value));
					else
						properties.put(role.toString(), value.toString());
				}
			}

			// Insert the input into the proper places in the sheet
			for ( int r = 0 ; r <= sheet.getLastRowNum() ; r++ ) {
				HSSFRow row = sheet.getRow(r);
				if (row != null) {
					for ( short c = 0 ; c <= row.getLastCellNum() ; c++ ) {
						HSSFCell cell = row.getCell(c);
						if ((cell != null) && (cell.getCellType() == HSSFCell.CELL_TYPE_STRING)) {
							String cellContent = cell.getStringCellValue();
							if (cellContent.indexOf("${") >= 0) {
								Template template = new Template(cellContent);
								StringWriter output = new StringWriter();
								try
								{
									template.write(output,properties);
								}
								catch (IOException e3)
								{
									throw new EngineException("faild to update '"+cellContent+"'", null, e3);
								}
								String updatedContent = output.toString();
								cell.setCellValue (updatedContent);
							}
						}
					}
				}
			}

			// Output the updated file
			BinaryValue updatedData = null;
			
			try
			{
				ByteArrayOutputStream output = new ByteArrayOutputStream();
				wb.write(output);
				updatedData = new BinaryValue(output.toByteArray()); 
				output.close();
			}
			catch (IOException e2)
			{
				throw new EngineException("Failed to output the updated Excel file", null, e2);
			}

			outputHandler.set(context, flow, updatedData);			

//			// temporary patch - write out the modofied file
//			String outputFileName = "C:/temp/Modified Excel.xls";
//			FileOutputStream fout;
//			try
//			{
//				fout = new FileOutputStream(outputFileName);
//				wb.write(fout);
//				fout.close();
//			}
//			catch (FileNotFoundException e1)
//			{
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			} catch (IOException e)
//			{
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		}
		catch (EngineException e)
		{
			fireError(context, flow, e);
		}
		
		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}


	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		inputHandler = (SlotHandler) getElementHandler(INPUT_FILE_ROLE);
		outputHandler = getSoleExit();
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import tersus.model.Model;
import tersus.model.ModelId;
import tersus.runtime.ColumnDescriptor;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.InstanceHandler;
import tersus.runtime.PersistenceHandler;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * 
 * An atomic flow handler that returns the display names of the columns of its input type
 * 
 * @deprecated Was used in the SAP solutions - probably not needed any more
 * @author Youval Bronicki
 *
 */
public class GetColumns extends FlowHandler
{
	private SlotHandler trigger;
	private List names;
	private SlotHandler exit;

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);

		String typeId = (String) trigger.get(context, flow);
		InstanceHandler type = getLoader().getHandler(new ModelId(typeId));
		names = new ArrayList();
		if (type.isPersistent())
		{
			PersistenceHandler ph = type.getPeristenceHandler();
			for (Iterator i = ph.getColumnDescriptors().iterator(); i.hasNext();)
			{
				ColumnDescriptor column = (ColumnDescriptor) i.next();
				names.add(column.getDisplayName());
			}
			for (int i = 0; i < names.size(); i++)
				accumulateExitValue(exit, context, flow, names.get(i));
		}

		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		trigger = getSoleTypedTrigger();
		exit = getSoleExit();
	}

}

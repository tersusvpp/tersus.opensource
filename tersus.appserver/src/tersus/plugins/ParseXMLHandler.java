/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.io.IOException;
import java.io.StringReader;
import java.util.Stack;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.CompositeDataHandler;
import tersus.runtime.DataHandler;
import tersus.runtime.ElementHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.trace.EventType;
import tersus.util.Misc;
import tersus.util.SpecialRoles;

/**
 * An atomic flow handler that parses an XML document
 * (for which a data model has already been created via 'CreateModelByXmlExample').
 * 
 * @deprecated Use tersus.plugins.variuos.ParseXML
 * 
 * @author Ofer Brandes
 *
 */

public class ParseXMLHandler extends FlowHandler
{
	private static final boolean DEBUG = false;
	SlotHandler inputHandler, exitHandler;
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);

		String xmlDoc = (String) inputHandler.get(context, flow);

		//		System.setProperty ( "org.xml.sax.driver" , "org.apache.xerces.parsers.SAXParser" );
		//		XMLReader xr = XMLReaderFactory.createXMLReader();
		//		FUNC3 Restore the usage of 'XMLReaderFactory'?

		try
		{
			XMLReader xr = XMLReaderFactory.createXMLReader();
			XmlDocumentHandler parser = new XmlDocumentHandler(context, flow, exitHandler);
			xr.setContentHandler(parser);
			xr.setErrorHandler(parser);
			xr.parse(new InputSource(new StringReader(xmlDoc)));
		}
		catch (IOException e)
		{
			throw new EngineException("XML Parsing Failed", null, e);
		}
		catch (SAXException e)
		{
			throw new EngineException("XML Parsing Failed", null, e);
		}

		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		inputHandler = getSoleTrigger();
		exitHandler = getSoleExit();
	}

	/**
	 *
	 * A SAX handler to parse an XML document.
	 *
	 */

	private class XmlDocumentHandler extends DefaultHandler
	{
		private RuntimeContext context = null;
		private FlowInstance flow;
		private SlotHandler output = null;
		private Stack nodes = null;
		private StringBuffer content = new StringBuffer();

		private XmlDocumentHandler(RuntimeContext context, FlowInstance flow, SlotHandler output)
		{
			super();
			this.context = context;
			this.flow = flow;
			this.output = output;
		}

		private class Node
		{
			private ElementHandler handler;
			private Object value;

			private Node(ElementHandler handler)
			{
				this.handler = handler;
				this.value = null;
			}
		}

		/**
		 * Handles a notification of the start of an XML element.
		 * 
		 * @param uri Namespace URI (currently ignored).
		 * @param name Element's local name (currently ignored).
		 * @param qName Element's raw XML 1.0 name (the name of the corresponding model).
		 * @param atts The element's attributes.
		 */
		public void startElement(String uri, String name, String qName, Attributes atts)
		{
			if (DEBUG)
			{
				if ("".equals(uri))
					System.out.println("Start element: " + qName);
				else
					System.out.println("Start element: {" + uri + "}" + name);
			}
			content.setLength(0); // Clear any existing content 
			ElementHandler handler;
			if (nodes == null)
			{
				nodes = new Stack();
				handler = output;
			}
			else
			{
				handler = ((Node) nodes.peek()).handler;
				handler = handler.getChildInstanceHandler().getElementHandler(Role.get(qName));
			}
			nodes.push(new Node(handler));

			DataHandler dataHandler = (DataHandler) handler.getChildInstanceHandler();
			if (DEBUG)
				System.out.println("Handler(" + qName + "): " + dataHandler.getModelId());

			if (dataHandler instanceof CompositeDataHandler)
			{
				Object value = dataHandler.newInstance(context);

				for (int i = 0; i < atts.getLength(); i++)
				{
					if (DEBUG)
						System.out.println("\t" + atts.getQName(i) + " = " + atts.getValue(i));

					String role = SpecialRoles.DEPENDENT_MODEL_ROLE_PREFIX + atts.getQName(i);
					ElementHandler attHandler = dataHandler.getElementHandler(Role.get(role));
					LeafDataHandler attDataHandler = (LeafDataHandler) attHandler.getChildInstanceHandler();
					if (DEBUG)
						System.out.println("\tHandler(" + role + "): " + attDataHandler.getModelId());
					if (Misc.ASSERTIONS)
						Misc.assertion(!attHandler.isRepetitive());
					Object attValue = attDataHandler.newInstance(context, atts.getValue(i), false);
					attHandler.set(context, value, attValue);
					if (DEBUG)
						System.out.println("\tValue: '" + attHandler.get(context, value) + "'");
				}

				((Node) nodes.peek()).value = value;
			}
		}

		/**
		 * Handles a notification of character data inside an XML element.
		 * 
		 * @param ch A characters array containing the element's data.
		 * @param start The start position in the character array.
		 * @param length The number of characters to use from the characters array.
		 */
		public void characters(char ch[], int start, int length)
		{
			content.append(ch, start, length);

			if (DEBUG)
				System.out.print("\tCharacters: \"");
			for (int i = start; i < start + length; i++)
			{
				switch (ch[i])
				{
					case '\\' :
						if (DEBUG)
							System.out.print("\\\\");
						break;
					case '"' :
						if (DEBUG)
							System.out.print("\\\"");
						break;
					case '\n' :
						if (DEBUG)
							System.out.print("\\n");
						break;
					case '\r' :
						if (DEBUG)
							System.out.print("\\r");
						break;
					case '\t' :
						if (DEBUG)
							System.out.print("\\t");
						break;
					default :
						if (DEBUG)
							System.out.print(ch[i]);
						break;
				}
			}
			if (DEBUG)
				System.out.print("\"\n");

		}

		private void addContent()
		{
			String contentStr = content.toString().trim();
			if (contentStr.length() == 0)
				return;
			ElementHandler handler = ((Node) nodes.peek()).handler;
			DataHandler dataHandler = (DataHandler) handler.getChildInstanceHandler();
			Object parentValue = ((Node) nodes.peek()).value;
			if (dataHandler instanceof CompositeDataHandler)
			{
				String role = SpecialRoles.XML_CONTENT_LEAF_ROLE;
				handler = dataHandler.getElementHandler(Role.get(role));
				dataHandler = (LeafDataHandler) handler.getChildInstanceHandler();
				if (DEBUG)
					System.out.println("\tHandler(" + role + "): " + dataHandler.getModelId());
			}
			else // OPT replace pop() & push() by Vector.get()
				{
				if (Misc.ASSERTIONS)
					Misc.assertion(dataHandler instanceof LeafDataHandler);
				Node node = (Node) nodes.pop();
				parentValue = ((Node) nodes.peek()).value;
				nodes.push(node);
			}
			
			Object value = ((LeafDataHandler) dataHandler).newInstance(context, contentStr, false);
			
			if (handler.isRepetitive())
				handler.accumulate(context, parentValue, value);
			else
				handler.set(context, parentValue, value);
			
			if (DEBUG)
				System.out.println("\tValue: '" + handler.get(context, parentValue) + "'");
		}

		/**
		 * Handles a notification of the end of an XML element.
		 * 
		 * @param uri Namespace URI (currently ignored).
		 * @param name Element's local name (currently ignored).
		 * @param qName Element's raw XML 1.0 name (the name of the corresponding model).
		 */
		public void endElement(String uri, String name, String qName)
		{
			if (DEBUG)
			{
				if ("".equals(uri))
					System.out.println("End element: " + qName);
				else
					System.out.println("End element:   {" + uri + "}" + name);
			}

			addContent(); // Add accumulated character content (if any)
			content.setLength(0);
			Node node = (Node) nodes.pop();

			if (nodes.size() > 0)
			{
				Node parentNode = (Node) nodes.peek();
				if (node.handler.getChildInstanceHandler() instanceof CompositeDataHandler)
					if (node.handler.isRepetitive())
						node.handler.accumulate(context, parentNode.value, node.value);
					else
						node.handler.set(context, parentNode.value, node.value);
				if (DEBUG)
				{
					System.out.println(
						"Handler: " + ((DataHandler) parentNode.handler.getChildInstanceHandler()).getModelId());
					System.out.println("Value: '" + node.handler.get(context, parentNode.value) + "'");
				}
			}
			else
			{
				node.handler.set(context, flow, node.value);
				if (DEBUG)
					System.out.println("Value: '" + node.handler.get(context, flow) + "'");

			}
		}
	}
}

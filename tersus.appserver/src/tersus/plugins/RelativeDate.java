/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.util.Date;
import java.util.GregorianCalendar;

import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.DataHandler;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.Number;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.trace.EventType;

/**
 * An atomic flow handler that calculates a date relative to a given base date.
 * 
 * @param Optional <Base Date>: A base date (if none, current date is used)
 * @param Optional <Years>: Number of years to add to the base date
 * @param Optional <Months>: Number of months to add to the base date
 * @param Optional <Days>: Number of days to add to the base date
 * 
 * @deprecated Use tersus.plugins.dates.RelativeDate
 * 
 * @author Ofer Brandes
 *
 */
public class RelativeDate extends FlowHandler
{
	static final String baseTriggerName = "<Base Date>";
	static final String yearsTriggerName = "<Years>";
	static final String monthsTriggerName = "<Months>";
	static final String daysTriggerName = "<Days>";
		
	SlotHandler baseTrigger, yearsTrigger, monthsTrigger, daysTrigger;
	SlotHandler exit;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtimle.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus ( flow , FlowStatus.STARTED );

		GregorianCalendar calendar;

		calendar = new GregorianCalendar();
		if (baseTrigger != null) {
			Date date = (Date) baseTrigger.get(context,flow);
			calendar.setTime(date);
		}
	
//		System.out.println("Initial Date: "+calendar.getTime());

		calendar.setLenient(true);
		
		// BUG2 Some pathological cases may occur (adding a month and a day to Februray 29th will get to Match 29th instead of March 30th)
		
		if (yearsTrigger != null) {
			int years = (int) ((Number) yearsTrigger.get(context,flow)).value;
			calendar.add(GregorianCalendar.YEAR,years);
		}
		
		if (monthsTrigger != null) {
			int months = (int) ((Number) monthsTrigger.get(context,flow)).value;
			calendar.add(GregorianCalendar.MONTH,months);
		}
		
		if (daysTrigger != null) {
			int days = (int) ((Number) daysTrigger.get(context,flow)).value;
			calendar.add(GregorianCalendar.DAY_OF_MONTH,days);
		}

		Date relativeDate = calendar.getTime();
//		System.out.println("Relative Date: "+relativeDate);
		
		exit.set(context,flow,relativeDate);
	
		setStatus ( flow , FlowStatus.FINISHED );
		traceFinish(context, flow);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		baseTrigger = (SlotHandler) getElementHandler(Role.get(baseTriggerName));
		yearsTrigger = (SlotHandler) getElementHandler(Role.get(yearsTriggerName));
		monthsTrigger = (SlotHandler) getElementHandler(Role.get(monthsTriggerName));
		daysTrigger = (SlotHandler) getElementHandler(Role.get(daysTriggerName));
		
		exit = getSoleExit();
		if (exit == null)
			throw new InvalidModelException("Missing exit in " + model.getId());
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.util.List;

import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.runtime.CompositeDataHandler;
import tersus.runtime.DataHandler;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.trace.EventType;

/**
 * An atomic flow handler that checks whether a specific object is one of its repetitive inputs
 * 
 * @deprecated Use tersus.plugins.collections.Appears
 * 
 * @author Ofer Brandes
 *
 */
public class Appears extends FlowHandler
{
	SlotHandler one, repetition;
	SlotHandler appears, missing;

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);

		Object searched = one.get(context, flow);
		List inputs = (List) repetition.get(context, flow);
		SlotHandler exit = missing;
		// FUNC2 Replace loop by a proper call to contains()
		CompositeDataHandler dataHandler = (CompositeDataHandler) one.getChildInstanceHandler();
		String searchedKey = dataHandler.getReferenceString(context, searched);
		if (searchedKey != null)
		{
			dataHandler = (CompositeDataHandler) repetition.getChildInstanceHandler();
			for (int i = 0; i < inputs.size(); i++)
			{
				Object current = inputs.get(i);
				if (searchedKey.equals(dataHandler.getReferenceString(context, current)))
				{
					exit = appears;
					break;
				}
			}
		}
		//		if (inputs.contains(searched))
		//			exit = appears;

		exit.set(context, flow, searched);

		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		one = getSoleNonRepetitiveTrigger();
		repetition = getSoleRepetitiveTrigger();
		if (one == null)
			throw new InvalidModelException("Missing non-repetitive trigger in " + model.getId());
		if (repetition == null)
			throw new InvalidModelException("Missing repetitive trigger in " + model.getId());

		appears = (SlotHandler) getElementHandler("Appears");
		missing = (SlotHandler) getElementHandler("Missing");
		if (appears == null)
			throw new InvalidModelException("Missing exit 'Appears' in " + model.getId());
		if (missing == null)
			throw new InvalidModelException("Missing exit 'Missing' in " + model.getId());
	}
}

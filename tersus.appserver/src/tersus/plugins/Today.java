/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.util.Date;
import java.util.GregorianCalendar;

import tersus.model.Model;
import tersus.runtime.DataHandler;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.trace.EventType;

/**
 * An atomic flow handler that returns the current date (the hour is 00:00:00)
 * 
 * @deprecated Use tersus.plugins.dates.Today
 * 
 * @author Youval Bronicki
 *
 */
public class Today extends FlowHandler
{
	SlotHandler exit;
	
	
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus ( flow , FlowStatus.STARTED );
		GregorianCalendar calendar = new GregorianCalendar(); //Current Date and Time
		//Override system time (used for regression tests)
		if (context.getDebugTime() != null)
			calendar.setTime(context.getDebugTime());
		calendar.set(GregorianCalendar.HOUR_OF_DAY,0);
		calendar.set(GregorianCalendar.MINUTE,0);
		calendar.set(GregorianCalendar.SECOND,0);
		calendar.set(GregorianCalendar.MILLISECOND,0);
		
		Date exitValue = calendar.getTime();
		exit.set(context, flow, exitValue);
		setStatus ( flow , FlowStatus.FINISHED );
		traceFinish(context, flow);
	}


	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		
		
		exit = (SlotHandler) getSoleExit();
	}
}

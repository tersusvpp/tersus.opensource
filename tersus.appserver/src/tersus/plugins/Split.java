/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.util.List;
import java.util.StringTokenizer;

import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.runtime.DataHandler;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.InstanceHandler;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.TextHandler;
import tersus.runtime.trace.EventType;
import tersus.util.Misc;

/**
 * /**
 * An atomic flow handler that splits a String into a sequence of sub-strings.
 * 
 * @deprecated Use tersus.plugins.text.Split
 * 
 * @author Youval Bronicki
 *
 */
public class Split extends FlowHandler
{
	private InstanceHandler exitType;
	private SlotHandler trigger;
	private SlotHandler exit;
	private String separators;

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);

		if (trigger.isRepetitive())
		{
			List values = (List) trigger.get(context, flow);
			for (int i = 0; i < values.size(); i++)
			{
				handleValue(context, flow, (String) values.get(i));
			}
		}
		else
			handleValue(context, flow, (String) trigger.get(context, flow));

		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}

	/**
	 * @param string
	 */
	private void handleValue(RuntimeContext context, Object flow, String value)
	{
		StringTokenizer tokenizer = new StringTokenizer(value, separators);

		while (tokenizer.hasMoreTokens())
		{
			Object exitValue = exitType.newInstance(context, tokenizer.nextToken(), false);
			exit.accumulate(context, flow, exitValue);
		}

	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		trigger = getSoleTrigger();
		if (!(trigger.getChildInstanceHandler() instanceof TextHandler))
			throw new InvalidModelException("Trigger of " + model.getId() + " must be of type text");
		if (Misc.ASSERTIONS)
			Misc.assertion(trigger != null);

		exit = getSoleExit();
		exitType = exit.getChildInstanceHandler();
		if (!(exitType instanceof TextHandler))
			throw new InvalidModelException("Trigger of " + model.getId() + " must be of type text");
		if (!exit.isRepetitive())
			throw new InvalidModelException("Trigger of " + model.getId() + " must be of repetitive");
		separators = (String) model.getProperty("separators");
		if (separators == null)
			throw new InvalidModelException("Missing property 'separators' in " + model.getId());

	}

}

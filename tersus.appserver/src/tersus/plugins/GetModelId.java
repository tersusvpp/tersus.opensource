/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that returns the id of its trigger's type (for reflective operations)
 * 
 * @deprecated Was used in the SAP solutions - probably not needed any more
 * @author Youval Bronicki
 *
 */
public class GetModelId extends FlowHandler
{

	private SlotHandler exit;
	String modelId;
	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);
		setExitValue(exit, context, flow, modelId);
		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);

	}

	/* (non-Javadoc)
	 * @see tersus.runtime.InstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		try
		{
			modelId = getSoleTypedTrigger().getChildModelId().toString();
		}
		catch (NullPointerException e)
		{
			throw new InvalidModelException("Missing trigger or missing trigger type in " + getModelId());
		}
		exit = getSoleExit();
		if (exit == null)
			throw new InvalidModelException("Missing exit in " + getModelId());
	}

}

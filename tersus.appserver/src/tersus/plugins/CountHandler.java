/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.util.List;

import tersus.model.Model;
import tersus.runtime.DataHandler;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.Number;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.trace.EventType;
import tersus.util.Misc;

/**
 * An atomic flow handler that counts its inputs (trigger instances).
 * 
 * @deprecated Use tersus.plugins.collections.Count
 * 
 * @author Ofer Brandes
 *
 */
public class CountHandler extends FlowHandler
{
	SlotHandler trigger, exit;

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);
		List inputs = (List) trigger.get(context, flow);
		Number count = new Number(inputs.size());
		exit.set(context, flow, count);
		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		trigger = getSoleTrigger();
		if (Misc.ASSERTIONS)
			Misc.assertion(trigger != null);
		exit = getSoleExit();
		if (Misc.ASSERTIONS)
			Misc.assertion(exit != null);
	}
}

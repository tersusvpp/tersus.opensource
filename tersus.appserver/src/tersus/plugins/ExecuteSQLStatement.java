/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.Misc;
import tersus.util.Template;
/**
 * @author Youval Bronicki
 *
 */
public class ExecuteSQLStatement extends Plugin
{
	private static Role SQL_TRIGGER_ROLE = Role.get("<SQL>");
	SlotHandler sqlTrigger;
	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		String sqlTemplate = (String) sqlTrigger.get(context, flow);
		Template template = new Template(sqlTemplate);
		StringBuffer sql = new StringBuffer();
		List tokens = template.getTokens();
		List values = new ArrayList();
		for (int i = 0; i < tokens.size(); i++)
		{
			//FUNC3 handle null values - requires a big refactoring (including analysis of the query)
			Template.Token token = (Template.Token) tokens.get(i);
			if (token.getType() == Template.TEXT)
				sql.append(token.getValue());
			else
			{
				String roleStr = token.getValue();
				SlotHandler trigger = (SlotHandler) getElementHandler(roleStr);
				if (trigger == null)
					throw new ModelExecutionException(
						"SQL statement '"
							+ sqlTemplate
							+ "' contains a reference to a non existing trigger '"
							+ roleStr
							+ "' in "
							+ getModelId());
				Object value = trigger.get(context, flow);
				value = ((LeafDataHandler)trigger.getChildInstanceHandler()).toSQL( value);
				if (value == null)
					throw new ModelExecutionException(
						"Null value '"
							+ roleStr
							+ "' for SQL statement '"
							+ sqlTemplate
							+ "' in "
							+ getModelId());
				sql.append('?');
				values.add(value);
			}
		}
		PreparedStatement stmt = null;
		try
		{
			stmt = context.getDefaultConnection().prepareStatement(sql.toString());
			for (int i = 0; i < values.size(); i++)
			{
				Object value = values.get(i);
				stmt.setObject(i + 1, value);
			}
			stmt.execute();
			stmt.close();
			stmt = null;
		}
		catch (SQLException e)
		{
			throw new EngineException(
				"Error in SQL Query",
				"SQL: \"" + sqlTemplate + "\" values:" + Misc.toString(values),
				e);
		}
		finally
		{
			if (stmt != null)
			{
				try
				{
					stmt.close();
				}
				catch (SQLException e)
				{
					//Secondary exception ignored (EngineException thrown anyway)
				}
			}
		}
	}
	/* (non-Javadoc)
	 * @see tersus.runtime.InstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		sqlTrigger = (SlotHandler) getElementHandler(SQL_TRIGGER_ROLE);
		if (sqlTrigger == null)
			throw new InvalidModelException("Missing trigger '" + SQL_TRIGGER_ROLE + "' in " + getModelId());
		if (!sqlTrigger.isMandatory())
			throw new InvalidModelException(
				"Trigger '" + SQL_TRIGGER_ROLE + "' is not mandatory in " + getModelId());
	}
}

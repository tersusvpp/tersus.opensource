/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

/**
 * Thrown when a cell in an excel file has an invalid format
 * @author Youval Bronicki
 *
 */
public class CellFormatException extends RuntimeException
{
	/**
	 * 
	 */
	private int row;
	private int column;
	private String errorMessage;
	public CellFormatException()
	{
		super();
	}
	/**
	 * @param message
	 */
	public CellFormatException(String message, int row, int column)
	{
		super();
		setErrorMessage(message);
		setRow(row);
		setColumn(column);
	}
	/**
	 * @return
	 */
	public int getColumn()
	{
		return column;
	}

	/**
	 * @return
	 */
	public String getErrorMessage()
	{
		return errorMessage;
	}

	/**
	 * @return
	 */
	public int getRow()
	{
		return row;
	}

	/**
	 * @param i
	 */
	public void setColumn(int i)
	{
		column = i;
	}

	/**
	 * @param string
	 */
	public void setErrorMessage(String string)
	{
		errorMessage = string;
	}

	/**
	 * @param i
	 */
	public void setRow(int i)
	{
		row = i;
	}

	/* (non-Javadoc)
	 * @see java.lang.Throwable#getMessage()
	 */
	public String getMessage()
	{
		return getErrorMessage()+" (row="+getRow()+ " column="+getColumn()+")";
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.io.IOException;

import tersus.model.Model;
import tersus.runtime.BinaryValue;
import tersus.runtime.DataHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.trace.EventType;
import tersus.util.Misc;

/**
 * Read the binary content of a resource file (file's path is relative to "models/").
 * 
 * @deprecated Use tersus.plugins.variuos.ReadBinaryFile
 * 
 * @author Ofer Brandes
 *
 */
public class ReadBinaryFile extends FlowHandler
{
	SlotHandler trigger, exit;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus ( flow , FlowStatus.STARTED );
		
		String fileName = (String) trigger.get(context, flow);
		
		byte[] fileContent;
		try
		{
			fileContent = context.getModelLoader().getRepository().getResource(fileName);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			throw new EngineException("Failed to read file '" + fileName+ "'", null, e);
		}
		if (fileContent == null)
			throw new EngineException("Failed to read file '" + fileName+ "'", null, null);

		BinaryValue content = new BinaryValue(fileContent);
		exit.set(context, flow, content);
		
		setStatus ( flow , FlowStatus.FINISHED );
		traceFinish(context, flow);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		
		trigger = getSoleTrigger();  // File Name
		if (Misc.ASSERTIONS)
			Misc.assertion(trigger != null);
		if (Misc.ASSERTIONS)
			Misc.assertion( ! trigger.isRepetitive());
		
		exit = getSoleExit();  // File Content
		if (Misc.ASSERTIONS)
			Misc.assertion(exit != null);
		if (Misc.ASSERTIONS)
			Misc.assertion(! exit.isRepetitive());
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import tersus.model.Model;
import tersus.runtime.BinaryValue;
import tersus.runtime.CompositeDataHandler;
import tersus.runtime.DataHandler;
import tersus.runtime.DateHandler;
import tersus.runtime.ElementHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.Number;
import tersus.runtime.NumberHandler;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.TextHandler;
import tersus.runtime.trace.EventType;

/**
 * An atomic flow handler that inserts data into a simple sub-table of an Excel sheet.
 * 
 * @param <File>		The Excel file
 * @param <Title>		An optional title of the sub-table to be updated in the file
 * 						(if exists, the table is searched in the file starting at the location where the title is found)
 * @param <Rows>		Multiple collections of leaves (or just one collection if not repetitive),
 *						where the role of each leaf corresponds to the title of one columns to update,
 *						not necessarily consecutive columns in the Excel sheet, but in the same row
 *						(the role of the first leaf is ignored, because the first item in each row is the row's title,
 *						which is placed one column to the left of the next item)
 * @param outputFile	The updated Excel file (with the rows replacing the first rows of the sub-table)
 * @param error			Optional error message
 * 
 * @deprecated Use tersus.plugins.variuos.UpdateExcelTable
 * 
 * @author Ofer Brandes
 *
 */

public class UpdateExcelTable extends FlowHandler
{
	private static final String INPUT_FILE_ROLE = "<File>";
	private static final String TITLE_ROLE = "<Title>";
	private static final String ROWS_ROLE = "<Rows>";
		
	private static final boolean DEBUG = false;
	SlotHandler inputHandler, titleHandler, rowsHandler, outputHandler;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);

		BinaryValue data = (BinaryValue) inputHandler.get(context, flow);

		try
		{
			// Create a new org.apache.poi.poifs.filesystem.Filesystem
			POIFSFileSystem poifs;
			
			try
			{
				poifs = new POIFSFileSystem(new ByteArrayInputStream(data.toByteArray()));
			}
			catch (IOException e)
			{
				throw new EngineException("Failed to read data (possibly not a valid excel file)'", null, e);
			}
	
			// Create the workbook
			HSSFWorkbook wb;
	
			try
			{
				wb = new HSSFWorkbook(poifs);
			}
			catch (IOException e)
			{
				throw new EngineException("Failed to read data (possibly not a valid excel file)'", null, e);
			}
			
			// Get the first sheet
			HSSFSheet sheet = wb.getSheetAt(0);
			String sheetName = wb.getSheetName(0);

			// Locate the title
			int startRow = 0;
			if (titleHandler != null) {
				String title = (String) titleHandler.get(context,flow);
				if (title != null) {
					for ( int r = 0 ; r <= sheet.getLastRowNum() ; r++ ) {
						HSSFRow row = sheet.getRow(r);
						if (row != null) {
							for ( short c = 0 ; c <= row.getLastCellNum() ; c++ ) {
								HSSFCell cell = row.getCell(c);
								if ((cell != null) && (cell.getCellType() == HSSFCell.CELL_TYPE_STRING)) {
									String cellContent = cell.getStringCellValue();
									if (title.equals(cellContent)) {
										startRow = r + 1;
										break;
									}
								}
							}
						if (startRow > 0)
							break;
						}
					}
					if (startRow == 0)
					throw new EngineException("Title '"+title+"' not found in Excel", null, null);
				}
				else {
					if (titleHandler.isMandatory())
						throw new EngineException("No title specified", null, null);
				}
			}
			
			// Locate the sub-table titles
			int subTableTitleRow = -1;
			CompositeDataHandler rowHandler = (CompositeDataHandler) rowsHandler.getChildInstanceHandler();
			if (rowHandler.getNumberOfElements() < 2)
				throw new EngineException("Only "+rowHandler.getNumberOfElements()+" items in each row - need at least 2 (1 title and at least 1 content)", null, null);
			
			short[] columns = new short[rowHandler.getNumberOfElements()];
			for ( int r = startRow ; r <= sheet.getLastRowNum() ; r++ ) {
				HSSFRow row = sheet.getRow(r);
				int numberOfColumnTitlesFound = 0;
				if (row != null) {
					for (int col = 1; col < columns.length; col++) {
						ElementHandler elementHandler = rowHandler.getElementHandler(col);
						String role = elementHandler.getRole().toString();
						columns[col] = -1;
						for ( short c = 0 ; c <= row.getLastCellNum() ; c++ ) {
							HSSFCell cell = row.getCell(c);
							if ((cell != null) && (cell.getCellType() == HSSFCell.CELL_TYPE_STRING)) {
								String cellContent = cell.getStringCellValue();
								if (role.equals(cellContent)) {
									columns[col] = c;
									numberOfColumnTitlesFound++;
									break;							
								}
							}
						}
						if (columns[col] < 0)
							break;
					}
					if (numberOfColumnTitlesFound == columns.length - 1) {
						subTableTitleRow = r;
						break;
					}
				}
			}

			if (subTableTitleRow < 0)
				throw new EngineException("faild to find in Excel "+(columns.length-1)+" column titles", null, null);

			columns[0] = (short) (columns[1] - 1);
			for (int col = 2; col < columns.length; col++) {
				if (columns[col] - 1 < columns[0])
					columns[0] = (short) (columns[col] - 1);
			}
			
			if (columns[0] < 0)
				throw new EngineException("First column title found in Excel in row "+(subTableTitleRow+1)+", column "+(columns[0]+2)+": Cannot place row titles in column "+(columns[0]+1), null, null);

			// Get input rows
			List inputs = new ArrayList();
			if (rowsHandler.isRepetitive())
				inputs.addAll((List) rowsHandler.get(context,flow));
			else
				inputs.add(rowsHandler.get(context,flow));
			
			// Place the input rows in the Excel sheet (replacing existing values)
			if (subTableTitleRow + inputs.size() > sheet.getLastRowNum())
				throw new EngineException("Excel contains only "+(sheet.getLastRowNum()+1)+" rows - cannot replace rows "+(subTableTitleRow+1+1)+" to "+(subTableTitleRow+inputs.size()+1), null, null);

			for (int r = 1 ; r <= inputs.size() ; r++) {
				HSSFRow excelRow = sheet.getRow(subTableTitleRow+r);
				if (!(inputs.get(r-1) instanceof Object[]))
					throw new EngineException("Input Row "+r+" is not compoite", null, null);
				Object[] dataRow = (Object []) inputs.get(r-1);
				
				for (int col = 0; col < columns.length; col++) {
					HSSFCell cell = excelRow.getCell(columns[col]);
					ElementHandler elementHandler = rowHandler.getElementHandler(col);
					Object leafValue = elementHandler.get(context,dataRow);
					if (leafValue != null) {
						DataHandler leafHandler = (DataHandler) elementHandler.getChildInstanceHandler();
						if (leafHandler instanceof LeafDataHandler) {
							if (leafHandler instanceof DateHandler) {
								throw new EngineException("Setting cells to date values not implemented yet", null, null);
							}
							else if (leafHandler instanceof NumberHandler) {
								cell.setCellType (HSSFCell.CELL_TYPE_NUMERIC);
								cell.setCellValue (((Number) leafValue).value);
							}
							else if (leafHandler instanceof TextHandler) {
								cell.setCellType (HSSFCell.CELL_TYPE_STRING);
								cell.setCellValue ((String) leafValue);
							}
							else
								throw new EngineException("Input Row "+r+": item "+(col+1)+" is not a date, a number or text", null, null);
						}
						else
							throw new EngineException("Input Row "+r+": item "+(col+1)+" is not a leaf", null, null);
					}
				}
			}

			// Output the updated file
			BinaryValue updatedData = null;
			
			try
			{
				ByteArrayOutputStream output = new ByteArrayOutputStream();
				wb.write(output);
				updatedData = new BinaryValue(output.toByteArray()); 
				output.close();
			}
			catch (IOException e2)
			{
				throw new EngineException("Failed to output the updated Excel file", null, e2);
			}

			outputHandler.set(context, flow, updatedData);			

//			// temporary patch - write out the modofied file
//			String outputFileName = "C:/temp/Modified Excel.xls";
//			FileOutputStream fout;
//			try
//			{
//				fout = new FileOutputStream(outputFileName);
//				wb.write(fout);
//				fout.close();
//			}
//			catch (FileNotFoundException e1)
//			{
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			} catch (IOException e)
//			{
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		}
		catch (EngineException e)
		{
			fireError(context, flow, e);
		}
		
		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}


	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		inputHandler = (SlotHandler) getElementHandler(INPUT_FILE_ROLE);
		titleHandler = (SlotHandler) getElementHandler(TITLE_ROLE);
		rowsHandler = (SlotHandler) getElementHandler(ROWS_ROLE);
		
		outputHandler = getSoleExit();
	}
}

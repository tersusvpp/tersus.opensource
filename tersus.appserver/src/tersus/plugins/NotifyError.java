/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import tersus.model.BuiltinModels;
import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.runtime.ElementHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * @author Youval Bronicki
 * 
 * @deprecated Use tersus.plugins.variuos.NotifyError
 *
 */
public class NotifyError extends Plugin
{
	SlotHandler trigger;
	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Object error = trigger.get(context, flow);
		if (error == null )
			return;
		ElementHandler messageElement = trigger.getChildInstanceHandler().getElementHandler(BuiltinModels.ERROR_MESSAGE);
		String message = (String)messageElement.get(context, error);
		ElementHandler detailsElement = trigger.getChildInstanceHandler().getElementHandler(BuiltinModels.ERROR_DETAILS);
		if (detailsElement == null) // Backwards compatibility (Error/Cause element)
			detailsElement = trigger.getChildInstanceHandler().getElementHandler(BuiltinModels.ERROR_CAUSE);
		String cause = (String)detailsElement.get(context, error);
		throw new EngineException(message, cause, null);
		 
	}
	
	/* (non-Javadoc)
	 * @see tersus.runtime.InstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		trigger = getSoleTypedTrigger(BuiltinModels.ERROR_ID);
		if (trigger == null)
		{
			throw new InvalidModelException(model.getId() + " has no trigger with type "+ BuiltinModels.ERROR_ID);
		}
		
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.util.Date;
import java.util.List;

import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.runtime.DataHandler;
import tersus.runtime.DateHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.trace.EventType;

/**
 * An atomic flow handler that returns the earliest of all date input values
 * 
 * @deprecated Use tersus.plugins.dates.Earliest
 * 
 * @author Youval Bronicki
 *
 */
public class EarliestDate extends FlowHandler
{
	SlotHandler exit;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);
		Date earliest = null;
		for (int i=0; i<triggers.length; i++)
		{
			SlotHandler trigger = triggers[i];
			Object triggerValue = trigger.get(context, flow);
			if (triggerValue == null) {
				if (trigger.isMandatory())
					throw new EngineException("Missing mandatory input", null, null);
			}
			else {
				if (trigger.isRepetitive())
				{
					List values = (List)triggerValue;
					for (int j=0; j<values.size();j++)
					{
						Date input = (Date)values.get(j);
						if (earliest == null || earliest.after(input))
							earliest = input;
					}
				}
				else
				{
					Date input = (Date) triggerValue;
					if (earliest == null || earliest.after(input))
						earliest = input;
				}
			}
		}
		
		exit.set(context, flow, earliest);

		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		exit = getSoleExit();
		for (int i=0; i<triggers.length; i++)
		{
			if (!(triggers[i].getChildInstanceHandler() instanceof DateHandler))
				throw new InvalidModelException("Trigger "+triggers[i].getRole()+ " of  " + getModelId() + " is not a date");
		}
	}
}

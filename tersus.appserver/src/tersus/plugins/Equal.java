/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.util.ArrayList;
import java.util.List;

import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.DataHandler;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.trace.EventType;

/**
 * An atomic flow handler that checks whether all its inputs are equal
 * (deep comparison for composite data types, with some tolerance for numbers).
 * 
 * @see instanceDeepCompare
 * 
 * @deprecated Use tersus.plugins.variuos.Equal
 * 
 * @author Ofer Brandes
 *
 */

// BUG3 Assuming the implementation of composite data types as arrays.

public class Equal extends FlowHandler
{
	SlotHandler equalExit, differentExit;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus ( flow , FlowStatus.STARTED );

		// Accumulate inputs
		List inputs = new ArrayList();
		for (int t = 0; t < triggers.length; t++) {
			if (triggers[t].isRepetitive())
				inputs.addAll((List) triggers[t].get(context,flow));
			else
				inputs.add(triggers[t].get(context,flow));
		}
		
		// Compare inputs (deep comparison)
		boolean allEqual = true;
		for (int i = 1; i < inputs.size(); i++)
			if (!(allEqual = DataHandler.instanceDeepCompare(inputs.get(i),inputs.get(i-1))))
				break;
		
		SlotHandler exit = (allEqual ? equalExit : differentExit);
		Object exitValue = Boolean.TRUE;
		exit.set(context, flow, exitValue);
		setStatus ( flow , FlowStatus.FINISHED );
		traceFinish(context, flow);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		
		if ( triggers.length < 1)
			throw new InvalidModelException(model.getId() + " has " + triggers.length + " triggers (must have at least 1");
		
		equalExit = (SlotHandler) getElementHandler(Role.get("Equal"));
		differentExit = (SlotHandler) getElementHandler(Role.get("Different"));
	}
}

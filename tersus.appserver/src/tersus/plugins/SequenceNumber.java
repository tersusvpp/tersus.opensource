/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.runtime.DataHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.Number;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SQLUtils;
import tersus.runtime.SlotHandler;
import tersus.runtime.trace.EventType;
import tersus.util.Misc;

/**
 * An atomic flow handler that finds the next sequence number for a given key 
 * 
 * @deprecated Use tersus.plugins.database.SequenceNumber
 * 
 * @author Youval Bronicki
 *
 */

public class SequenceNumber extends FlowHandler
{
	private String key;
	private String tableName;
	private String selectStr;
	private String updateStr;
	SlotHandler exitHandler;
	private static String KEY_PROPERTY = "Key";
	private static String TABLE_NAME_PROPERTY = "sequenceNumberTable";
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);
		Number out = new Number(nextSequenceNumber(context));
		exitHandler.set(context, flow, out);
	setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}
	private int nextSequenceNumber(RuntimeContext context)
	{
		Statement insertStatement = null;
		PreparedStatement selectStatement = null;
		PreparedStatement updateStatement = null;
		ResultSet rs = null;
		try
		{
			selectStatement = context.getDefaultConnection().prepareStatement(selectStr);
			rs = selectStatement.executeQuery();
			if (!rs.next())
			{
				rs.close();
				selectStatement.close();
				selectStatement = null;
				insertStatement = context.getDefaultConnection().createStatement();
				insertStatement.executeUpdate(
					"INSERT INTO " + tableName + " (Name, Value) VALUES ('" + key + "',1)");
				insertStatement.close();
				insertStatement = null;
				return 1;

			}
			int nextSequenceNumber = rs.getInt(1) + 1;
			rs.close();
			rs = null;
			selectStatement.close();
			selectStatement = null;
			updateStatement = context.getDefaultConnection().prepareStatement(updateStr);
			updateStatement.setInt(1, nextSequenceNumber);
			int updatedRows = updateStatement.executeUpdate();
			if (Misc.ASSERTIONS)
				Misc.assertion(updatedRows == 1);
			updateStatement.close();
			updateStatement = null;
			return nextSequenceNumber;
		}
		catch (SQLException e)
		{

			e.printStackTrace();
			throw new EngineException(
				"Failed to generate sequence number",
				"Table= " + tableName + " Key=" + key,
				e);
		}
		finally
		{
			if (rs != null)
			{
				try
				{
					rs.close();
				}
				catch (SQLException e1)
				{
					// Another exception already thrown
				}
			}
			if (selectStatement != null)
			{
				try
				{
					selectStatement.close();
				}
				catch (SQLException e1)
				{
					// Another exception already thrown
				}
			}
			if (updateStatement != null)
			{
				try
				{
					updateStatement.close();
				}
				catch (SQLException e1)
				{
					// Another exception already thrown
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		exitHandler = getSoleExit();
		key = (String) model.getProperty(KEY_PROPERTY);
		if (key == null)
			throw new InvalidModelException("Missing property " + KEY_PROPERTY + " in " + model);
		tableName = (String) model.getProperty(TABLE_NAME_PROPERTY);
		if (tableName == null)
			throw new InvalidModelException("Missing property " + TABLE_NAME_PROPERTY + " in " + model);
		selectStr = "select Value from " + tableName + " where Name='" + key + "'";
		updateStr = "update " + tableName + " set Value=? where Name='" + key + "'";
	}

	public void initDB(RuntimeContext context)
	{
		Statement statement = null;
		try
		{

			if (! SQLUtils.tableExists(context, tableName))
			{
				statement = context.getDefaultConnection().createStatement();
				String sql =
					"CREATE TABLE "
						+ tableName
						+ " (Name "
						+ context.getDefaultDBAdapter().getVarchar(0)
						+ ", Value "
						+ context.getDefaultDBAdapter().getDouble()
						+ " )";
				statement.executeUpdate(sql);
				statement.close();
				statement = null;
			}
			return;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			throw new EngineException("Failed to create table " + tableName, null, e);
		}
		finally
		{
		    SQLUtils.forceClose(statement);
		}

	}
}
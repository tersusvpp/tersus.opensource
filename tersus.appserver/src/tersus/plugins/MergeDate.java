/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;
import java.util.Date;
import java.util.GregorianCalendar;

import tersus.model.Model;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.Number;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
/**
 * An atomic flow handler that creates a date from year, month and day numbers
 * Year numbers are full years (e.g. 1999, 2004)
 * Month numbers are 1-12
 * Day numbers are 1-31
 * 
 * @deprecated Use tersus.plugins.dates.ConstructDate
 * 
 * @author Youval Bronicki
 *
 */
public class MergeDate extends FlowHandler
{
	SlotHandler dateExit;
	SlotHandler yearTrigger, monthTrigger, dayTrigger;
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);
		Number yearObj = (Number) yearTrigger.get(context, flow);
		Number monthObj = (Number) monthTrigger.get(context, flow);
		Number dayObj = (Number) dayTrigger.get(context, flow);
		if (yearObj!= null && monthObj!= null && dayObj != null)
		{
			int year = (int) yearObj.value;
			int month = (int) (monthObj.value - 1);
			int day = (int) dayObj.value;
			GregorianCalendar calendar = new GregorianCalendar(year, month, day);
			Date date = calendar.getTime();
			setExitValue(dateExit, context, flow, date);
		}
		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}
	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		dateExit = getSoleExit();
		yearTrigger = (SlotHandler) getElementHandler("Year");
		monthTrigger = (SlotHandler) getElementHandler("Month");
		dayTrigger = (SlotHandler) getElementHandler("Day");
	}
}

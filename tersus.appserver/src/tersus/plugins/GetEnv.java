/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.runtime.DataHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.InstanceHandler;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.TextHandler;
import tersus.runtime.trace.EventType;

/**
 * 
 * An atomic flow handler that returns the value of an environment variable
 * 
 * (Currently Supports only J2EE Environment)
 * 
 * @deprecated temporarily
 * 
 * @author Youval Bronicki
 *
 */
public class GetEnv extends FlowHandler
{
	private InstanceHandler exitType;
	private SlotHandler keyTrigger;
	private SlotHandler exit;

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);
		String key = (String) keyTrigger.get(context, flow);

		if (key != null)
		{
			Object value;
			try
			{
				InitialContext initCtx = new InitialContext();
				Context envCtx = (Context) initCtx.lookup("java:comp/env");
				value = envCtx.lookup(key);
				if (value != null)
				{
					Object exitValue = exitType.newInstance(context, value.toString(), null, false);

					exit.set(context, flow, exitValue);
				}
			}
			catch (NamingException e)
			{
				fireError(
					context,
					flow,
					new EngineException("Failed to lookup environment entry " + key, "", null));
			}

		}

		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}


	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		keyTrigger = (SlotHandler) getElementHandler("Key");
		if (!(keyTrigger.getChildInstanceHandler() instanceof TextHandler))
			throw new InvalidModelException("Trigger of " + model.getId() + " must be of type text");
		if (keyTrigger.isRepetitive())
			throw new InvalidModelException("Trigger of " + model.getId() + " must not be repetitive");

		exit = getSoleExit();
		exitType = exit.getChildInstanceHandler();
		if (!(exitType instanceof LeafDataHandler))
			throw new InvalidModelException("Exit of " + model.getId() + " must be a atomic");

	}

}

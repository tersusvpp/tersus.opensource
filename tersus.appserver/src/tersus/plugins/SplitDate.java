/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import tersus.model.Model;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.Number;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
/**
 * An atomic flow handler that splits a date into year, month and day numbers
 * Year numbers are full years (e.g. 1999, 2004)
 * Month numbers are 1-12
 * Day numbers are 1-31
 * 
 * @deprecated Use tersus.plugins.dates.SplitDate
 * 
 * @author Youval Bronicki
 *
 */
public class SplitDate extends FlowHandler
{
	SlotHandler dateTrigger;
	SlotHandler yearExit, monthExit, dayExit;
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);
		Date date = (Date) dateTrigger.get(context, flow);
		if (date != null)
		{
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(date);
			int year = calendar.get(Calendar.YEAR);
			int month = calendar.get(Calendar.MONTH) + 1;
			int day = calendar.get(Calendar.DAY_OF_MONTH);
			setExitValue(yearExit, context, flow, new Number(year));
			setExitValue(monthExit, context, flow, new Number(month));
			setExitValue(dayExit, context, flow, new Number(day));
		}
		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}
	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		dateTrigger = (SlotHandler) getElementHandler("Date");
		yearExit = (SlotHandler) getElementHandler("Year");
		monthExit = (SlotHandler) getElementHandler("Month");
		dayExit = (SlotHandler) getElementHandler("Day");
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import tersus.model.Model;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.Misc;

/**
 * 
 * An atomic flow handler that replaces a string within another string:
 * 
 * Within the input 'Text', any occurances of 'Replace' are replaced by 'By'
 * 
 * @deprecated Use tersus.plugins.text.Replace
 * 
 * @author Youval Bronicki
 *
 */
public class Replace extends FlowHandler
{
	private SlotHandler exit;
	private SlotHandler textTrigger;
	private SlotHandler replaceTrigger;
	private SlotHandler byTrigger;
	
	private static final String TEXT="Text";
	private static final String REPLACE="Replace";
	private static final String BY="By";

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);

		String text = (String)textTrigger.get(context, flow);
		String replace = (String)replaceTrigger.get(context, flow);
		String by = (String)byTrigger.get(context, flow);


		String out  = Misc.replaceAll(text, replace, by);
		setExitValue(exit, context, flow, out);
		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}


	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		exit = getSoleExit();
		textTrigger = (SlotHandler) getElementHandler(TEXT);
		replaceTrigger = (SlotHandler) getElementHandler(REPLACE);
		byTrigger = (SlotHandler)getElementHandler(BY);
	}

}

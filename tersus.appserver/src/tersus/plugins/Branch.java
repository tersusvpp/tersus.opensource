/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.util.List;

import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.runtime.CompositeFlowHandler;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.NotImplementedException;

/**
 * 
 * An atomic flow handler that branches according to the type and content of its input.
 * 
 * If the input is composite, its type is compared to the types of the exits, and only 
 * the exit with a matching type (if exists) is fired.
 * 
 * If the input is non-composite, its value is compared to the 'value' attribute of the exits (or to the exit's role if 'value' is missing). 
 * 
 * @deprecated Use tersus.plugins.flow.Branch (need a separate plugin for branching by type)
 * 
 * @author Youval Bronicki
 *
 */
public class Branch extends FlowHandler
{
	private SlotHandler defaultExit;
	SlotHandler trigger;
	private static final String VALUE = "value";

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);

		if (trigger.isRepetitive())
		{
			List values = (List) trigger.get(context, flow);
			for (int i = 0; i < values.size(); i++)
				handleValue(context, flow, values.get(i));
		}
		else
			handleValue(context, flow, trigger.get(context, flow));

		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}

	/**
	 * Handles a single input value
	 * @param context
	 * @param flow
	 * @param value
	 */
	private void handleValue(RuntimeContext context, FlowInstance flow, Object value)
	{
		if (value == null)
		{
			if (defaultExit != null)
				setExitValue(defaultExit, context, flow, Boolean.TRUE);
			return;
		}
			
		if (trigger.getChildInstanceHandler() instanceof CompositeFlowHandler)
			throw new NotImplementedException("Branching by composite type not yet implemented");
		for (int i = 0; i < exits.length; i++)
		{
			SlotHandler exit = exits[i];
			String valueStr = (String) exit.getProperty(VALUE);
			if (valueStr == null)
				valueStr = exit.getRole().toString();
			if (value.toString().equals(valueStr))
			{
				setExitValue(exit, context, flow, value);
				return;
			}
		}
		if (defaultExit != null)
			setExitValue(defaultExit, context, flow, value);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		trigger = getSoleTrigger();
		if (trigger == null)
			throw new InvalidModelException("Missing trigger in " + model.getId());

		for (int i = 0; i < exits.length; i++)
		{
			if (exits[i].getProperty(VALUE) == null)
			{
				defaultExit = exits[i];
			}
		}
	}

}

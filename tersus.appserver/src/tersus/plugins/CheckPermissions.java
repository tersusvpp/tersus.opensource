/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import tersus.model.BuiltinProperties;
import tersus.model.FlowModel;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.SlotType;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * @deprecated Use tersus.plugins.security.CheckPermissions
 * 
 * @author Youval Bronicki
 *
 */
public class CheckPermissions extends Plugin
{
	private SlotHandler noneExit;
	private static final String NONE = "<None>";
	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		boolean found = false;
		for (int i=0; i< exits.length; i++)
		{
			SlotHandler exit = exits[i];
			if (exit != noneExit)
			{
				String permission = exit.getRole().toString();
				if (context.isPermitted(permission))
				{
					setExitValue(exit, context, flow, Boolean.TRUE);
					found = true;
				}
			}
		}
		if (! found)
			setExitValue(noneExit, context, flow, Boolean.TRUE);
	}
	
	/* (non-Javadoc)
	 * @see tersus.runtime.InstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		noneExit = (SlotHandler)getElementHandler(NONE);
	}

	/**
	 * @param model
	 * @return The list of permissions checked by a model
	 */
	public static List getPermissionList(FlowModel model)
	{
		List permissions = new ArrayList();
		for (Iterator i = model.getElements().iterator(); i.hasNext(); )
		{
			ModelElement element = (ModelElement) i.next();
			if (element.getProperty(BuiltinProperties.TYPE) == SlotType.EXIT)
			{
				String roleStr = element.getRole().toString();
				if (! roleStr.equals(NONE))
					permissions.add(roleStr);
			}
			
		}
		return permissions;
	}

}

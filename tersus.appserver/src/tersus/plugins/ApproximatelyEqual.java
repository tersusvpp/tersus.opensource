/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.DataHandler;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.Number;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.trace.EventType;

/**
 * An atomic flow handler that checks whether two numbers are equal with tolerance.
 * 
 * @see NumberUtil.essentiallyEqual
 * 
 * @deprecated Use tersus.plugins.math.ApproximatelyEqual
 * 
 * @author Ofer Brandes
 *
 */
public class ApproximatelyEqual extends FlowHandler
{
	SlotHandler input1, input2, tolerance;
	SlotHandler equalExit, differentExit;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus ( flow , FlowStatus.STARTED );

		double in1 = ((Number) input1.get(context,flow)).value;
		double in2 = ((Number) input2.get(context,flow)).value;
		double delta = ((Number) tolerance.get(context,flow)).value;

		boolean approximatelyEqual = (in1 >= in2-delta) && (in1 <= in2+delta);
		
		SlotHandler exit = (approximatelyEqual ? equalExit : differentExit);
		Object exitValue = Boolean.TRUE;
		exit.set(context, flow, exitValue);
		setStatus ( flow , FlowStatus.FINISHED );
		traceFinish(context, flow);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		
		if ( triggers.length != 3)
			throw new InvalidModelException(model.getId() + " Must have 3 triggers");
		input1 = triggers[0];
		input2 = triggers[1];
		tolerance = triggers[2];
		
		equalExit = (SlotHandler) getElementHandler(Role.get("Equal"));
		differentExit = (SlotHandler) getElementHandler(Role.get("Different"));
	}
}

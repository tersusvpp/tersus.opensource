/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.util.Date;

import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that checks whether date X is later than (or equal to) date Y
 * 
 * @deprecated Use tersus.plugins.dates.LaterOrEqual
 * 
 * @author Youval Bronicki
 *
 */
public class DateLaterOrEquals extends FlowHandler
{
	SlotHandler triggerX, triggerY;
	SlotHandler yesExit, noExit;

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);
		Date x = (Date) triggerX.get(context, flow);
		Date y = (Date) triggerY.get(context, flow);
		if (x != null && y != null)
		{
			SlotHandler exit = (x.getTime() >= y.getTime()) ? yesExit : noExit;
			setExitValue(exit, context, flow, Boolean.TRUE);
		}
		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		triggerX = (SlotHandler) getElementHandler("X");
		triggerY = (SlotHandler) getElementHandler("Y");

		yesExit = (SlotHandler) getElementHandler(Role.get("Yes"));
		noExit = (SlotHandler) getElementHandler(Role.get("No"));
	}
}

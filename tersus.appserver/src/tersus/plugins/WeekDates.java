/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.util.Date;
import java.util.GregorianCalendar;

import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.DataHandler;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.Number;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.trace.EventType;

/**
 * An atomic flow handler that calculates the first and last days of a week.
 * 
 * @param "Year": The year (e.g.2004)
 * @param "Week Number": The number of the week in the year (usually 1-52 or 1-53, but "lenient" values are also acceptable)
 * @param "First Day": "Sunday" if weeks start on Sundays (optional - the default is Mondays)
 * 
 * @deprecated Use tersus.plugins.dates.WeekDates
 * 
 * @author Ofer Brandes
 *
 */
public class WeekDates extends FlowHandler
{
	static final String yearTriggerName = "<Year>";
	static final String weekNumberTriggerName = "<Week Number>";
	static final String firstDateExitName = "First Date";
	static final String lastDateExitName = "Last Date";
	static final String yearExitName = "Year";
	static final String weekNumberExitName = "Week Number";
		
	SlotHandler yearTrigger, weekNumberTrigger, firstDayTrigger;
	SlotHandler firstDateExit, lastDateExit;
	SlotHandler yearExit, weekNumberExit;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtimle.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus ( flow , FlowStatus.STARTED );

		int year = (int) ((Number) yearTrigger.get(context,flow)).value;
		int weekNumber = (int) ((Number) weekNumberTrigger.get(context,flow)).value;

		int firstDayOfWeek = GregorianCalendar.MONDAY;
		int lastDayOfWeek = GregorianCalendar.SUNDAY;
		if (firstDayTrigger != null) {
			String firstDay = (String) firstDayTrigger.get(context,flow);
			if ("Sunday".equals(firstDay)) {
				firstDayOfWeek = GregorianCalendar.SUNDAY;
				lastDayOfWeek = GregorianCalendar.SATURDAY;
			}
			else
				throw new InvalidModelException("First Day of Week is Monday unless Sunday specified");
		}
		
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setLenient(true);
		calendar.setFirstDayOfWeek(firstDayOfWeek);
		calendar.clear();
		calendar.set(GregorianCalendar.YEAR,year);
		calendar.set(GregorianCalendar.WEEK_OF_YEAR,weekNumber);
		calendar.set(GregorianCalendar.DAY_OF_WEEK,firstDayOfWeek);
		Date firstDate = calendar.getTime();

		calendar.set(GregorianCalendar.YEAR,year);
		calendar.set(GregorianCalendar.WEEK_OF_YEAR,weekNumber);		
		calendar.set(GregorianCalendar.DAY_OF_WEEK,lastDayOfWeek);
		Date lastDate = calendar.getTime();
		
		if (firstDateExit != null) {
			firstDateExit.set(context,flow,firstDate);
		}

		if (lastDateExit != null) {
			lastDateExit.set(context,flow,lastDate);
		}
		
		if (yearExit != null) {
			year = calendar.get(GregorianCalendar.YEAR);
			Object out = new Number(year);
			yearExit.set(context,flow,out);
		}

		if (weekNumberExit != null) {
			weekNumber = calendar.get(GregorianCalendar.WEEK_OF_YEAR);
			Object out = new Number(weekNumber);
			weekNumberExit.set(context,flow,out);
		}
		
		setStatus ( flow , FlowStatus.FINISHED );
		traceFinish(context, flow);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		
		if ( (triggers.length < 2) || (triggers.length >3))
			throw new InvalidModelException(model.getId() + " has " + triggers.length + " triggers (must have 2 or 3");
		
		yearTrigger = (SlotHandler) getElementHandler(Role.get(yearTriggerName));
		if (yearTrigger == null)
			throw new InvalidModelException("Missing trigger '"+yearTriggerName+"' in " + model.getId());
	
		weekNumberTrigger = (SlotHandler) getElementHandler(Role.get(weekNumberTriggerName));
		if (weekNumberTrigger == null)
			throw new InvalidModelException("Missing trigger '"+weekNumberTriggerName+"' in " + model.getId());
			
		firstDayTrigger = getSoleOptionalTrigger(); // NICE2 Better identify by role?
		
		firstDateExit = (SlotHandler) getElementHandler(Role.get(firstDateExitName));
		lastDateExit = (SlotHandler) getElementHandler(Role.get(lastDateExitName));
		yearExit = (SlotHandler) getElementHandler(Role.get(yearExitName));
		weekNumberExit = (SlotHandler) getElementHandler(Role.get(weekNumberExitName));
	}
}

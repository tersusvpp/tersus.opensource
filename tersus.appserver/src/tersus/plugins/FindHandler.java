/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Role;
import tersus.runtime.ColumnDescriptor;
import tersus.runtime.ElementHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.InstanceHandler;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.PersistenceHandler;
import tersus.runtime.Query;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.Misc;
import tersus.util.NotImplementedException;
import tersus.util.Template;
import tersus.util.Template.Token;

/**
 * An atomic flow handler that finds instances of its specified data type
 * (Initially, the handler returns all instances, conditions will be implemented in the future)
 * 
 * @deprecated Use tersus.plugins.database.Find
 * 
 * @author Youval Bronicki
 *
 */

public class FindHandler extends FlowHandler
{
	private SlotHandler filterTrigger;
	private SlotHandler typeTrigger;
	private SlotHandler orderByTrigger;
	private static final Role FILTER_ROLE = Role.get("<Filter>");
	private static final Role TYPE_ROLE = Role.get("<Type>");
	private static final Role ORDER_BY_ROLE = Role.get("<Order By>");
	private static final String QUERY_TYPE="queryType";
	private static final String LDAP_ROOT_NAME="rootName";
	private String[] ldapAttributes;
	private String ldapFilter;
	private int queryType;
	private static final int STATIC_SQL = 1;
	private static final int DYNAMIC_SQL = 2;
	private static final int LDAP = 3;
	/** 
	 * The name of the model property that holds the LDAP url.
	 */
	private ArrayList queryColumns;
	private ArrayList queryTriggers;
	private Query query;
	private PersistenceHandler persistenceHandler;
	SlotHandler outputExitHandler, notFoundExitHandler;
	private String ldapRootName;
	private ElementHandler[] outputAttributeHandlers;
	private boolean useQuotedIdentifiers;
	//FUNC3 refactor to handle the case of multi-column references.
	//FUNC3 (?)implement dynamic output type for static queries
	//NICE2 Refactor and use a separate plugin for LDAP queries
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);

		List instances;
		try
		{
			switch (queryType)
			{
				case LDAP :
					instances = executeLDAPQuery(context, flow);
					break;
				case DYNAMIC_SQL :
					instances = executeDynamicSQLQuery(context, flow);
					break;
				case STATIC_SQL :
					instances = executeStaticSQLQuery(context, flow);
					break;
				default :
					throw new EngineException("Unexpected Query Type " + queryType, null, null);
			}
			if (instances.size() == 0)
			{
				if (notFoundExitHandler != null)
					setExitValue(notFoundExitHandler, context, flow, Boolean.TRUE);
			}
			else
			{
				if (outputExitHandler != null)
					for (int i = 0; i < instances.size(); i++)
						accumulateExitValue(outputExitHandler, context, flow, instances.get(i));
			}
		}
		catch (EngineException e)
		{
			if (e.getCause() instanceof SQLException)
			{
				fireError(context, flow, e);
			}
			else
				throw e;
		}

		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}

	private List executeStaticSQLQuery(RuntimeContext context, FlowInstance flow)
	{
		List instances;
		ArrayList values = new ArrayList(queryTriggers.size());
		boolean hasNullValues = false;
		for (int i = 0; i < queryTriggers.size(); i++)
		{
			SlotHandler trigger = (SlotHandler) queryTriggers.get(i);
			Object value = trigger.get(context, flow);
			hasNullValues = hasNullValues || value == null;
			values.add(value);
		}
		if (hasNullValues)
		{
			instances = executeQueryWithNulls(context, values);
		}
		else
			instances = query.execute(context, values, context.getDefaultConnection());
		return instances;
	}

	/**
	 * @param context
	 * @param values
	 * @return
	 */
	private List executeLDAPQuery(RuntimeContext context, FlowInstance flow)
	{
		DirContext ctx = null;
		ArrayList values = new ArrayList(queryTriggers.size());
		boolean hasNullValues = false;
		for (int i = 0; i < queryTriggers.size(); i++)
		{
			SlotHandler trigger = (SlotHandler) queryTriggers.get(i);
			Object value = trigger.get(context, flow);
			hasNullValues = hasNullValues || value == null;
			values.add(value);
		}
		try
		{
			List outputList = new ArrayList();
			InitialContext initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			ctx = (DirContext) envCtx.lookup("ldap/Main");

			SearchControls ctls = new SearchControls();
			ctls.setReturningAttributes(ldapAttributes);
			ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			synchronized (ctx)
			{
				NamingEnumeration results = ctx.search(ldapRootName, ldapFilter, values.toArray(), ctls);
				InstanceHandler outputHandler = outputExitHandler.getChildInstanceHandler();
				while (results.hasMore())
				{
					SearchResult result = (SearchResult) results.next();
					Object instance = outputHandler.newInstance(context);
					for (int i = 0; i < outputAttributeHandlers.length; i++)
					{
						ElementHandler attribute = outputAttributeHandlers[i];
						String valueStr =
							Misc.getString(result.getAttributes().get(attribute.getRole().toString()).get());
						attribute.create(context, instance, valueStr, false);
					}
					outputList.add(instance);

				}
				ctx.close();
				ctx = null;
			}
			return outputList;

		}
		catch (Exception e)
		{
			throw new EngineException(
				"Failed to execute LDAP query",
				"rootName: '" + ldapRootName + "'\nFilter:'" + ldapFilter + "'\nValues:" + Misc.toString(values),
				e);
		}
		finally
		{
			if (ctx != null)
				try
				{
					ctx.close();
				}
				catch (NamingException e1)
				{
					//Ignore - "Main" exception already reported 
				}
		}
	}

	/**
	 * Create a specific query for the case of null values;
	 * @param context
	 * @param values
	 * @return
	 */
	private List executeQueryWithNulls(RuntimeContext context, ArrayList values)
	{
		StringBuffer condition = new StringBuffer();
		ArrayList nonNullValues = new ArrayList();
		ArrayList nonNullTypes = new ArrayList();
		for (int i = 0; i < queryTriggers.size(); i++)
		{
			SlotHandler trigger = (SlotHandler) queryTriggers.get(i);
			ColumnDescriptor column = (ColumnDescriptor) queryColumns.get(i);
			Object value = values.get(i);
			if (i > 0)
				condition.append(" AND ");
			condition.append(column.getColumnName(useQuotedIdentifiers));
			if (value == null)
				condition.append(" is null");
			else
			{
				condition.append(" = ?");
				nonNullValues.add(value);
				nonNullTypes.add(trigger.getChildInstanceHandler());
			}

		}
		Query adHocQuery = new Query(persistenceHandler, condition.toString(), nonNullTypes);
		return adHocQuery.execute(context, nonNullValues, context.getDefaultConnection());
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		useQuotedIdentifiers = getLoader().useQuotedIdentifiers();
		outputExitHandler = getSoleRepetitiveExit();
		notFoundExitHandler = getSoleNonRepetitiveExit();
		if ("LDAP".equals(model.getProperty(QUERY_TYPE)))
		{
			initLDAPQuery(model);
		}
		else
		{
			initSQLQuery();
		}
	}

	private void initLDAPQuery(Model model)
	{
		queryType = LDAP;
		queryTriggers = new ArrayList();
		ldapRootName = (String)model.getProperty(LDAP_ROOT_NAME);
		if (ldapRootName == null)
			ldapRootName = "";
		StringBuffer filterBuf = new StringBuffer();
		filterBuf.append("(&");
		for (int i = 0; i < triggers.length; i++)
		{
			queryTriggers.add(triggers[i]);
			filterBuf.append("(" + triggers[i].getRole() + "={" + i + "})");
		}
		filterBuf.append(")");
		ldapFilter = filterBuf.toString();
		outputAttributeHandlers = outputExitHandler.getChildInstanceHandler().elementHandlers;
		ldapAttributes = new String[outputAttributeHandlers.length];
		for (int i = 0; i < outputAttributeHandlers.length; i++)
		{
			ElementHandler attribute = outputAttributeHandlers[i];
			String attributeName = attribute.getRole().toString();
			ldapAttributes[i] = attributeName;
		}
	}

	private void initSQLQuery()
	{
		filterTrigger = (SlotHandler) getElementHandler(FILTER_ROLE);
		typeTrigger = (SlotHandler) getElementHandler(TYPE_ROLE);
		orderByTrigger = (SlotHandler) getElementHandler(ORDER_BY_ROLE);
		persistenceHandler = outputExitHandler.getChildInstanceHandler().getPeristenceHandler();
		if (filterTrigger == null)
			initDefaultSQLQuery();
		else
			queryType = DYNAMIC_SQL;

	}

	/**
	 * Initializes an explicit SQL query (sepcified by a trigger with the role "<Filter>")
	 */
	private List executeDynamicSQLQuery(RuntimeContext context, FlowInstance flow)
	{

		if (typeTrigger != null)
		{
			String typeId = (String) typeTrigger.get(context, flow);
			InstanceHandler type = getLoader().getHandler(new ModelId(typeId));
			persistenceHandler = type.getPeristenceHandler();
		}
		String filter = (String) filterTrigger.get(context, flow);
		StringBuffer condition = new StringBuffer();
		ArrayList values = new ArrayList();
		ArrayList types = new ArrayList();
		if (filter != null && filter.length() > 0)
		{
			Template template = new Template(filter);
			List tokens = template.getTokens();
			for (int i = 0; i < tokens.size(); i++)
			{
				//FUNC3 handle null values - requires a big refactoring (including analysis of the query)
				Template.Token token = (Token) tokens.get(i);
				if (token.getType() == Template.TEXT)
					condition.append(token.getValue());
				else
				{
					String roleStr = token.getValue();
					SlotHandler trigger = (SlotHandler) getElementHandler(roleStr);
					if (trigger == null)
						throw new EngineException(
							"Error in SQL Query",
							"Filter contains a reference to an unknown parameter '" + roleStr + "'",
							null);
					Object value = trigger.get(context, flow);
					if (value == null)
						throw new EngineException(
							"Error in SQL Query",
							"Filter contains a reference to a null parameter '" + roleStr + "'",
							null);
					condition.append('?');
					values.add(value);
					types.add(trigger.getChildInstanceHandler());
				}

			}
		}
		String orderBy = null;
		if (orderByTrigger!= null)
		    orderBy = (String)orderByTrigger.get(context, flow);
		
		Query adHocQuery = new Query(persistenceHandler, condition.toString(), orderBy, types);
		return adHocQuery.execute(context, values, context.getDefaultConnection());
	}

	/**
	 * Initializes a default SQL query (that consists of an 'AND' of all triggers)
	 *
	 */
	private void initDefaultSQLQuery()
	{
		if (persistenceHandler == null)
			throw new InvalidModelException("Non-persistent output type for static query in " + getModelId());
		if (typeTrigger != null)
			throw new NotImplementedException(
				"Static query with dynamic type in " + getModelId() + " not implemented.");
		queryType = STATIC_SQL;
		StringBuffer sqlBuffer = new StringBuffer();
		ArrayList parameterHandlers = new ArrayList();
		queryTriggers = new ArrayList();
		queryColumns = new ArrayList();
		for (int i = 0; i < triggers.length; i++)
		{
			SlotHandler trigger = triggers[i];
			ColumnDescriptor column = persistenceHandler.getColumnDescriptor(trigger.getRole().toString());
			if (column == null)
			{
				System.out.println(
					"Trigger "
						+ trigger
						+ " ignored in query - no such column in "
						+ persistenceHandler.getDataHandler());
				continue;
			}
			queryTriggers.add(trigger);
			queryColumns.add(column);
			if (parameterHandlers.size() > 0)
				sqlBuffer.append(" AND ");
			sqlBuffer.append(column.getColumnName(useQuotedIdentifiers));
			sqlBuffer.append("=?");
			parameterHandlers.add(column.getDataHandler());
			if (column.getDataHandler() != trigger.getChildInstanceHandler())
			{
				if (!(column.getDataHandler() instanceof LeafDataHandler
					&& column.getDataHandler().getClass().equals(trigger.getChildInstanceHandler().getClass())))
					throw new InvalidModelException(
						"Trigger type is incomaptible with column type trigger=" + trigger);
			}
		}
		query = new Query(persistenceHandler, sqlBuffer.toString(), parameterHandlers);
	}
	//FUNC2 add model validation (when mechanism for plugin validation is there)
}

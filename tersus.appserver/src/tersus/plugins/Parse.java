/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins;

import java.util.StringTokenizer;

import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.runtime.CompositeDataHandler;
import tersus.runtime.DataHandler;
import tersus.runtime.ElementHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.InstanceHandler;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.TextHandler;
import tersus.runtime.trace.EventType;
import tersus.util.Misc;

/**
 * 
 * An atomic flow handler parses a text
 * 
 * (The initial Implementation only supports simple separated concatenations)
 * 
 * @deprecated temporarily
 * 
 * @author Youval Bronicki
 *
 */
public class Parse extends FlowHandler
{
	private InstanceHandler exitType;
	private SlotHandler text;
	private SlotHandler exit;
	private String separators;

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus(flow, FlowStatus.STARTED);
		String inputText = (String) text.get(context, flow);
		StringTokenizer tokenizer = new StringTokenizer(inputText, separators);
		
		Object exitValue = exitType.newInstance(context); 
		int elementNumber = 0;
		ElementHandler[] outputElements = exitType.elementHandlers;
		int numberOfElements = outputElements.length;
		while (tokenizer.hasMoreTokens())
		{
			if (elementNumber >= numberOfElements)
				fireError(context, flow, new EngineException("Parse Failed", "Text=" + inputText+ " too many tokens ("+(elementNumber+1)+")" , null));
			String token = tokenizer.nextToken();
			outputElements[elementNumber].create(context, exitValue, token, false);
			elementNumber++;
			
		}
		while (elementNumber < numberOfElements)
		{
			if (outputElements[elementNumber].isMandatory())
			{
				fireError(context, flow, new EngineException("Parse Failed", "Text=" + inputText+ " no value for mandatory element '"+outputElements[elementNumber].getRole()+"'" , null));
			}
			
		}
		
		exit.set(context, flow, exitValue);

		setStatus(flow, FlowStatus.FINISHED);
		traceFinish(context, flow);
	}


	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		text = (SlotHandler)getElementHandler("Text");
		if (!(text.getChildInstanceHandler() instanceof TextHandler))
			throw new InvalidModelException("Trigger of " + model.getId() + " must be of type text");
		if (text.isRepetitive())
			throw new InvalidModelException("Trigger of " + model.getId() + " must not be repetitive");
		if (Misc.ASSERTIONS)
			Misc.assertion(text != null);

		exit = getSoleExit();
		exitType = exit.getChildInstanceHandler();
		Model exitModel = model.getElement(exit.getRole()).getReferredModel();
		separators = (String)exitModel.getProperty("separator");
		if (!(exitType instanceof CompositeDataHandler))
			throw new InvalidModelException("Exit of " + model.getId() + " must be a concatenation");
		
		if (separators == null)
			throw new InvalidModelException("Missing property 'separators' in " + exitModel.getId());

	}

}

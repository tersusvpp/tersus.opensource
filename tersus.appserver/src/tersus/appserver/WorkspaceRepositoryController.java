/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.appserver;

import java.io.File;

import org.eclipse.core.resources.IProject;

import tersus.editor.RepositoryManager;
import tersus.model.Repository;
import tersus.webapp.IRepositoryController;
import tersus.workbench.TersusWorkbench;

public class WorkspaceRepositoryController implements IRepositoryController
{

    public Repository createRepository(String repositoryRoot, String libraryPaths)
    {
        File file = new File(repositoryRoot);
        String projectName = file.getParentFile().getName();
        
        IProject project = TersusWorkbench.getWorkspace().getRoot().getProject(projectName);
        
        return RepositoryManager.getRepositoryManager(project).getRepository();
    }

    public void reload(Repository repository)
    {
        // No need to reload
    }

}

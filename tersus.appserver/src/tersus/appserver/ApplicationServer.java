/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.appserver;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import javax.servlet.Filter;
import javax.servlet.ServletContextListener;

import org.apache.catalina.Container;
import org.apache.catalina.Context;
import org.apache.catalina.Engine;
import org.apache.catalina.Host;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.Loader;
import org.apache.catalina.Service;
import org.apache.catalina.authenticator.BasicAuthenticator;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.deploy.ApplicationParameter;
import org.apache.catalina.deploy.ContextResource;
import org.apache.catalina.deploy.FilterDef;
import org.apache.catalina.deploy.FilterMap;
import org.apache.catalina.deploy.LoginConfig;
import org.apache.catalina.deploy.SecurityCollection;
import org.apache.catalina.deploy.SecurityConstraint;
import org.apache.catalina.loader.WebappLoader;
import org.apache.catalina.startup.ContextConfig;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.startup.Tomcat.FixContextListener;
import org.eclipse.core.runtime.FileLocator;

import tersus.ProjectStructure;
import tersus.eclipse.Extensions;
import tersus.runtime.DatabaseAdapter;
import tersus.util.DataSourceProperties;
import tersus.util.FileUtils;
import tersus.util.NetUtil;
import tersus.util.PKUtil;
import tersus.util.XMLReaderConfigurator;
import tersus.util.hsqldbupgrade.Upgrader;
import tersus.webapp.Config;
import tersus.webapp.ContextPool;
import tersus.workbench.Configuration;
import tersus.workbench.Parameter;

/**
 * @author Youval Bronicki
 * 
 */
public class ApplicationServer
{
	private static final String MAIN_DATA_SOURCE = tersus.webapp.Engine.MAIN_DATA_SOURCE;
	public static final String NOT_STARTED = "NOT_STARTED";
	public static final String AVAILABLE = "AVAILABLE";
	public static final String NOT_AVAILABLE = "NOT_AVAILABLE";
	private Tomcat tomcat;
	private int preferredPort = 8080;
	private int preferredHttpPort = 8443;

	private Host host;

	private String tomcatHome;

	private File pluginHome;

	private ArrayList<ApplicationServerListener> listeners = new ArrayList<ApplicationServerListener>();

	private int port;
	private int securePort = -1;

	private String docBase;

	public static void main(String[] args)
	{
		ApplicationServer server = new ApplicationServer();
		server.startup();
	}

	public ApplicationServer()
	{
		try
		{
			URL installURL = AppserverPlugin.getDefault().getBundle().getEntry("/tomcat");
			URL resolvedURL = FileLocator.resolve(installURL);
			tomcatHome = FileLocator.toFileURL(resolvedURL).getFile();
			pluginHome = (new File(tomcatHome)).getParentFile();
			File defaultWebappDir = new File(pluginHome, "DefaultWebapp");
			docBase = defaultWebappDir.getPath();
			if (!defaultWebappDir.exists())
				throw new ApplicationServerException("The web application is missing in " + docBase);
		}
		catch (IOException e)
		{
			throw new ApplicationServerException("Failed to resolve home directory", e);
		}

	}

	public void shutdown()
	{
		if (tomcat != null)
		{
			try
			{
				for (Connector c:tomcat.getService().findConnectors())
				{
					c.stop();
					c.destroy();
				}
			}
			catch (Exception e)
			{

			}
			try
			{
				for (Container c : host.findChildren())
				{
					if (c instanceof Context)
						host.removeChild((Context) c);
				}
				tomcat.stop();
			}
			catch (LifecycleException e)
			{
				e.printStackTrace();
			}
			finally
			{
				tomcat = null;
			}
			notifyStatusChanged(null);
		}
	}

	public void startup()
	{
		if (tomcat != null)
			return;
		try
		{
			XMLReaderConfigurator.configureXMLReader();
			System.setProperty("catalina.home", tomcatHome);
			System.setProperty("catalina.base", tomcatHome);
			tomcat = new Tomcat();
			tomcat.enableNaming();

			Engine engine = tomcat.getEngine();
			engine.setName("Tersus");
			engine.setDefaultHost("localhost");
			// engine.setLogger(log);
			host = tomcat.getHost();
			this.port = findPort();
			this.securePort = findHttpPort();
			Service service = tomcat.getService();
			service.removeConnector(tomcat.getConnector()); // Remove default connector
			Connector httpConnector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
			httpConnector.setPort(port);
			httpConnector.setUseBodyEncodingForURI(true);
			httpConnector.setRedirectPort(securePort);
			service.addConnector(httpConnector);

			File keystoreFile = new File(System.getProperty("user.home"), ".tersus/tomcat.keystore");
			String keystoreAlias = "tomcat";
			String keystorePassword = "123456";
			if (!PKUtil.checkCertificate(keystoreFile, keystoreAlias, keystorePassword))
			{
				keystoreFile.getParentFile().mkdirs();
				PKUtil.createSelfSignedCertificate(keystoreFile, keystoreAlias, keystorePassword,
						10 * 365 * 24 * 3600, "localhost");
			}

			Connector httpsConnector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
			httpsConnector.setPort(securePort);
			httpsConnector.setSecure(true);
			httpsConnector.setScheme("https");
			httpsConnector.setProperty("SSLEnabled", "true");
			httpsConnector.setProperty("keystoreFile", keystoreFile.getPath());
			httpsConnector.setProperty("keystorePass", keystorePassword);
			httpsConnector.setProperty("keyAlias", keystoreAlias);
			service.addConnector(httpsConnector);
			tomcat.start();
			// log("Application server started. Port = "+port+" Log
			// directory="+logDirectory);
		}
		catch (Exception ex)
		{
			log("An error has occurred when starting the application server ", ex);
			try
			{
				tomcat.stop();
			}
			catch (Exception e)
			{
				log("Failed to stop application server (following an exception)", e);
			}
			tomcat = null;
		}
	}

	private Loader configureClassLoader()
	{
		return new WebappLoader(new ApplicationServerClassLoader());
	}

	/**
	 * @param string
	 */
	private void error(String message)
	{
		AppserverPlugin.error(message, null);
	}

	private void log(String message, Throwable e)
	{
		AppserverPlugin.log(message, e);
	}

	public boolean isStarted()
	{
		return tomcat != null;
	}

	public boolean isInError(Configuration application)
	{
		return NOT_AVAILABLE == getStatus(application.getContextPath());
	}

	public boolean isAvaiable(final Configuration application)
	{
		return AVAILABLE == getStatus(application.getContextPath());
	}

	/**
	 * @param application
	 */
	public void stop(final Configuration application, boolean notify)
	{
		if (tomcat == null)
			return;
		Context ctx = (Context) host.findChild(application.getContextPath());
		stop(ctx, notify);
	}

	private void stop(Context ctx, boolean notify)
	{
		if (ctx != null)
		{
			try
			{
				tomcat.getHost().removeChild(ctx);
				if (notify)
					notifyStatusChanged(ctx.getPath());
			}
			catch (Exception e)
			{
				log("Error stopping application", e);
			}
		}
	}

	private void notifyStatusChanged(String contextPath)
	{
		for (ApplicationServerListener l : listeners)
		{
			l.statusChanged(contextPath);
		}
	}

	synchronized public String getStatus(String name)
	{
		if (host == null)
			return NOT_STARTED;
		Context context = (Context) host.findChild(name);
		if (context == null)
			return NOT_STARTED;
		else if (context.getState().isAvailable())
			return AVAILABLE;
		else
			return NOT_AVAILABLE;
	}

	public void start(final Configuration application, boolean notify,
			boolean addRepositoryController)
	{
		if (tomcat == null)
		{
			error("failed to start application " + application
					+ " - application server is not started");
			return;
		}
		StandardContext ctx;
		ctx = new StandardContext();
		ctx.setDocBase(docBase);
		ctx.setPath(application.getContextPath());
		ctx.setName(application.getContextPath());
		ctx.setPrivileged(true);
		BasicAuthenticator valve = new BasicAuthenticator();
		valve.setDisableProxyCaching(false);
		ctx.addValve(valve);
		ContextConfig config = new ContextConfig();
		config.setCustomAuthenticators(null);
		ctx.addLifecycleListener(config);

		ctx.setLoader(configureClassLoader());
		String repositoryRootPath = null;
		String projectRootPath = null;
		String timestampFilePath = null;
		String dbTimestampPath = null;
		try
		{
			repositoryRootPath = application.getRepositoryRoot().getCanonicalPath();
			projectRootPath = application.getRepositoryRoot().getParentFile().getCanonicalPath();
			timestampFilePath = new File(application.getRepositoryRoot().getParentFile(),
					".timestamp").getCanonicalPath();
			dbTimestampPath = new File(application.getRepositoryRoot().getParentFile(),
					".timestamp.db").getCanonicalPath();

		}
		catch (IOException e)
		{
			throw new ApplicationServerException("Failed to locate repository root", e);
		}
		addApplicationParameter(ctx, ContextPool.TIMESTAMP_PATHS, timestampFilePath);
		addApplicationParameter(ctx, ContextPool.DB_TIMESTAMP_PATH, dbTimestampPath);
		addApplicationParameter(ctx, tersus.webapp.Engine.REPOSITORY_ROOT_KEY, repositoryRootPath);
		addApplicationParameter(ctx, tersus.webapp.Engine.PROJECT_ROOT_KEY, projectRootPath);
		addApplicationParameter(ctx, tersus.webapp.Engine.ROOT_SYSTEM_KEY, application
				.getRootSystemId().getPath());
		addApplicationParameter(ctx, ContextPool.CLASS_LOADER_KEY,
				ApplicationServerClassLoader.class.getName());
		addApplicationParameter(ctx, "app-export-template-folder", pluginHome.getAbsolutePath()
				+ "/app-export-templates");

		if (addRepositoryController)
			addApplicationParameter(ctx, ContextPool.REPOSITORY_CONTROLLER,
					getRepositoryControllerClass().getName());

		setDatabaseAdapters(ctx);
		List<Class<ServletContextListener>> listeners = Extensions
				.getExtensionClasses(ServletContextListener.class);
		if (listeners.isEmpty())
			ctx.addApplicationListener(ContextPool.class.getName());
		else
		{
			for (Class<ServletContextListener> listener : listeners)
				ctx.addApplicationListener(listener.getName());
		}
		addFilters(ctx);

		String traceDirPath;
		String workDirPath;
		String localPath;
		String dbPath = null;
		try
		{
			File projectDir = application.getRepositoryRoot().getParentFile();
			File workDir = new File(projectDir, ProjectStructure.WORK);
			File traceDir = new File(workDir, ProjectStructure.TRACE);
			File logDir = new File(workDir, ProjectStructure.LOGS);
			File localDir = new File(projectDir, ProjectStructure.WEB);
			if (application.getDatabaseFolder() != null)
			{
				File dbFile = new File(application.getDatabaseFolder(), application.getName());
				dbPath = dbFile.getCanonicalPath();
			}
			traceDirPath = traceDir.getCanonicalPath();
			workDirPath = workDir.getCanonicalPath();
			localPath = localDir.getCanonicalPath() + ","
					+ application.getRepositoryRoot().getCanonicalPath();
			FileUtils.empty(workDir);
			logDir.mkdirs();
		}
		catch (IOException e)
		{
			throw new ApplicationServerException("Failed to initialize application parameters", e);
		}

		addApplicationParameter(ctx, tersus.webapp.Engine.TRACE_DIR_KEY, traceDirPath);
		StringBuffer libraries = new StringBuffer();
		File[] libraryFiles = Extensions.getModelLibraries();
		for (int i = 0; i < libraryFiles.length; i++)
		{
			if (i > 0)
				libraries.append(';');
			libraries.append(libraryFiles[i].getPath());
		}
		addApplicationParameter(ctx, tersus.webapp.Engine.MODEL_LIBRARIES_KEY, libraries.toString());
		addApplicationParameter(ctx, Config.LOCAL_DIRS_KEY, localPath);
		addApplicationParameter(ctx, Config.DEV_MODE, "true");
		List<Parameter> parameters = application.getParameters();
		for (int i = 0; i < parameters.size(); i++)
		{
			Parameter p = (Parameter) parameters.get(i);
			addApplicationParameter(ctx, p.getName(), p.getValue());
		}
		ctx.setWorkDir(workDirPath);
		addDataSources(application, ctx);
		addResources(application, ctx);
		if (application.findDataSourceProperties(MAIN_DATA_SOURCE) == null)
		{
			addDefaultDataSource(ctx, dbPath);
		}

		if ("JDBC".equals(application.getAuthenticationMethod()))
		{
			ctx.addSecurityRole("Super");
			ctx.addSecurityRole("User");

			SecurityConstraint constraint = new SecurityConstraint();
			constraint.setAuthConstraint(true);
			constraint.addAuthRole("*");
			SecurityCollection collection = new SecurityCollection();
			collection.addPattern("/*");
			collection.setName("All");
			constraint.addCollection(collection);
			ctx.addConstraint(constraint);

			// Add exception for _Activate URL
			constraint = new SecurityConstraint();
			constraint.setAuthConstraint(false);
			collection = new SecurityCollection();
			collection.addPattern("/_Activate");
			collection.setName("Activate");
			constraint.addCollection(collection);
			ctx.addConstraint(constraint);

			// Add exception for NoAuth URL
			constraint = new SecurityConstraint();
			constraint.setAuthConstraint(false);
			collection = new SecurityCollection();
			collection.addPattern("/NoAuth/*");
			collection.setName("NoAuth");
			constraint.addCollection(collection);
			ctx.addConstraint(constraint);

			LoginConfig loginConfig = new LoginConfig("BASIC", application.getName(), null, null);
			ctx.setLoginConfig(loginConfig);
			boolean quoted = false;
			String permissionsDataSource = tersus.webapp.Engine.MAIN_DATA_SOURCE;
			for (Parameter p : application.getParameters())
			{
				if (p.getName().equals(ContextPool.USE_SQL_QUOTED_IDENTIFIERS))
					quoted = Boolean.parseBoolean(p.getValue());
				if (p.getName().equals(ContextPool.PERMISSIONS_DATA_SOURCE_KEY))
					permissionsDataSource = p.getValue();
			}
			DefaultRealm realm = new DefaultRealm(permissionsDataSource,quoted);
			ctx.setRealm(realm);
		}

		ctx.addLifecycleListener(new FixContextListener());
		host.addChild(ctx);
		if (notify)
			notifyStatusChanged(ctx.getPath());

	}

	protected Class<?> getRepositoryControllerClass()
	{
		Class<?> repositoryControllerClass = WorkspaceRepositoryController.class;
		return repositoryControllerClass;
	}

	private ContextResource createDataSource(StandardContext ctx, String resourceName)
	{
		ContextResource jdbcResource = new ContextResource();
		jdbcResource.setName(resourceName);
		jdbcResource.setType("javax.sql.DataSource");
		jdbcResource.setScope("Shareable");
		return jdbcResource;

	}

	private ContextResource createResource(String resourceName, String type)
	{
		ContextResource resource = new ContextResource();
		resource.setName(resourceName);
		resource.setType(type);
		resource.setScope("Shareable");
		return resource;
	}

	private void addDefaultDataSource(StandardContext ctx, String dbPath)
	{
		ContextResource r = createDataSource(ctx, MAIN_DATA_SOURCE);
		r.setName(MAIN_DATA_SOURCE);
		// p.setProperty(DataSourceProperties.FACTORY,BasicDataSourceFactory.class.getName());
		r.setProperty(DataSourceProperties.DRIVER_CLASS_NAME, "org.hsqldb.jdbcDriver");
		r.setProperty(DataSourceProperties.USERNAME, "sa");
		r.setProperty(DataSourceProperties.PASSWORD, "");
		r.setProperty(DataSourceProperties.URL, "jdbc:hsqldb:file:" + dbPath);
		r.setProperty(DataSourceProperties.VALIDATION_QUERY, "CALL Now()");
		addResource(ctx, r);
	}

	private void addResource(StandardContext ctx, ContextResource resource)
	{
		ctx.getNamingResources().addResource(resource);
		if ("org.hsqldb.jdbcDriver".equals(resource
				.getProperty(DataSourceProperties.DRIVER_CLASS_NAME)))
		{
			upgradeHSQLDB((String) resource.getProperty(DataSourceProperties.URL));
		}
	}

	private void upgradeHSQLDB(String url)
	{
		if (url.indexOf("file:") >= 0)
		{
			String dbPath = url.substring(url.lastIndexOf(':') + 1);
			if ((new File(dbPath + ".properties")).exists())
			{
				Upgrader upgrader = new Upgrader(dbPath);
				upgrader.upgrade();
			}
		}
	}

	/**
	 * @param application
	 * @param ctx
	 */
	private void addDataSources(Configuration application, StandardContext ctx)
	{
		List<Properties> dataSources = application.getDataSources();
		for (int i = 0; i < dataSources.size(); i++)
		{
			Properties props = (Properties) dataSources.get(i);
			String resourceName = props.getProperty(Configuration.DATA_SOURCE_NAME);
			ContextResource resource = createDataSource(ctx, resourceName);
			// if (props.getProperty(DataSourceProperties.FACTORY) == null)
			// props.setProperty(DataSourceProperties.FACTORY,
			// BasicDataSourceFactory.class.getName());
			Enumeration<?> en = props.propertyNames();
			while (en.hasMoreElements())
			{
				String name = (String) en.nextElement();
				if (!Configuration.DATA_SOURCE_NAME.equals(name))
					resource.setProperty(name, (String) props.getProperty(name));
			}
			addResource(ctx, resource);
		}

	}

	private void addResources(Configuration application, StandardContext ctx)
	{
		List<Properties> resourceDefs = application.getResources();
		for (int i = 0; i < resourceDefs.size(); i++)
		{
			Properties props = (Properties) ((Properties) resourceDefs.get(i)).clone();
			String resourceName = props.getProperty(Configuration.DATA_SOURCE_NAME);
			ContextResource resource = createResource(resourceName, props.getProperty("type"));
			props.remove("type");
			Enumeration<?> en = props.propertyNames();
			while (en.hasMoreElements())
			{
				String name = (String) en.nextElement();
				resource.setProperty(name, (String) props.getProperty(name));
			}
			addResource(ctx, resource);
		}

	}

	/**
	 * @param ctx
	 */
	private void setDatabaseAdapters(StandardContext ctx)
	{
		StringBuffer databaseAdapters = new StringBuffer();
		for (Class<DatabaseAdapter> databaseAdapterClass : Extensions
				.getExtensionClasses(DatabaseAdapter.class))
		{
			databaseAdapters.append(databaseAdapterClass.getName());
			databaseAdapters.append(',');
		}
		if (databaseAdapters.length() > 0)
			addApplicationParameter(ctx, ContextPool.DATABASE_ADAPTERS_KEY,
					databaseAdapters.toString());
	}

	private void addFilters(StandardContext ctx)
	{
		for (Class<Filter> filterClass : Extensions.getExtensionClasses(Filter.class))
		{
			FilterDef filterDef = new FilterDef();
			filterDef.setFilterClass(filterClass.getName());
			filterDef.setFilterName(filterClass.getName());
			ctx.addFilterDef(filterDef);
			FilterMap filterMap = new FilterMap();
			filterMap.setFilterName(filterDef.getFilterName());
			filterMap.addURLPattern("/*");
			ctx.addFilterMap(filterMap);
		}
	}

	private static void addApplicationParameter(Context ctx, String name, String value)
	{
		ApplicationParameter parameter = new ApplicationParameter();
		parameter.setValue(value);
		parameter.setName(name);
		ctx.addApplicationParameter(parameter);
	}

	public void addListener(ApplicationServerListener listener)
	{
		if (!listeners.contains(listener))
			listeners.add(listener);
	}

	public void removeListener(ApplicationServerListener listener)
	{
		listeners.remove(listener);
	}

	private int findPort()
	{
		return findFreePort(getPreferredPort());
	}

	private int findHttpPort()
	{
		return findFreePort(getPreferredHttpPort());

	}

	protected int findFreePort(int p)
	{
		int port = NetUtil.findFreePort(p, p + 1000);
		if (port < 0)
			throw new ApplicationServerException("Failed to find free port");
		return port;
	}

	public int getPort()
	{
		return port;
	}

	public int getPreferredPort()
	{
		return preferredPort;
	}

	public void setPreferredPort(int preferredPort)
	{
		this.preferredPort = preferredPort;
	}

	public int getPreferredHttpPort()
	{
		return preferredHttpPort;
	}

	public void setPreferredHttpPort(int preferredHttpPort)
	{
		this.preferredHttpPort = preferredHttpPort;
	}

	public int getSecurePort()
	{
		return securePort;
	}

	protected void setSecurePort(int securePort)
	{
		this.securePort = securePort;
	}

}
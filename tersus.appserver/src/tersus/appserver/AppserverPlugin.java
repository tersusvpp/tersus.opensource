/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.appserver;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.osgi.framework.BundleContext;

import tersus.webapp.appexport.ExportException;

public class AppserverPlugin extends Plugin {
	//The shared instance.
	private static AppserverPlugin plugin;
	//Resource bundle.
	private ResourceBundle resourceBundle;
    private ApplicationServer server;
	private File webAppFolder;
	
	/**
	 * The constructor.
	 */
	public AppserverPlugin() {
		super();
		plugin = this;
		try {
			resourceBundle = ResourceBundle.getBundle("tersus.appserver.AppserverPluginResources");
		} catch (MissingResourceException x) {
			resourceBundle = null;
		}
	}

	/**
	 * This method is called upon plug-in activation
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
	}

	/**
	 * This method is called when the plug-in is stopped
	 */
	public void stop(BundleContext context) throws Exception {
	    if (getAppServer().isStarted())
	        getAppServer().shutdown();
		super.stop(context);
	}

	/**
	 * Returns the shared instance.
	 */
	public static AppserverPlugin getDefault() {
		return plugin;
	}

	/**
	 * Returns the string from the plugin's resource bundle,
	 * or 'key' if not found.
	 */
	public static String getResourceString(String key) {
		ResourceBundle bundle = AppserverPlugin.getDefault().getResourceBundle();
		try {
			return (bundle != null) ? bundle.getString(key) : key;
		} catch (MissingResourceException e) {
			return key;
		}
	}

	/**
	 * Returns the plugin's resource bundle,
	 */
	public ResourceBundle getResourceBundle() {
		return resourceBundle;
	}

    /**
     * @return
     */
    public ApplicationServer getAppServer()
    {
        if (server == null)
            server = new ApplicationServer();
        return server;
    }

    /**
     * @param e
     */
	public static void log(IStatus status)
	{
		getDefault().getLog().log(status);
	}
	public static void log(Throwable e)
	{
		if (e instanceof CoreException)
		{
			log(((CoreException )e).getStatus());
		}
		log(new Status(IStatus.INFO, "tersus.appserver", 1, e.getMessage(), e)); //$NON-NLS-1$
	}
	public static void log(String message, Throwable e)
	{
		log(new Status(IStatus.INFO, getDefault().getBundle().getSymbolicName(), 0, message,e)); //$NON-NLS-1$
	}
	public static void error(String message, Throwable e)
	{
		log(new Status(IStatus.ERROR, getDefault().getBundle().getSymbolicName(), 0, message,e)); //$NON-NLS-1$
	}
	
	public static File getAppTempalteFolder()
	{
		URL url = getDefault().getBundle().getEntry("app-export-templates/index.html");
		try
		{
			File templateFolder = new File(FileLocator.resolve(url).getFile()).getParentFile();
			return templateFolder;
		}
		catch (IOException e)
		{
			throw new ExportException("Failed to locate template folder", e);
		}
	}	
	public File getWebAppFolder()
	{
		if (webAppFolder == null)
		{
			try
			{
	           URL installURL = getBundle().getEntry("/DefaultWebapp");
	            URL resolvedURL = FileLocator.resolve(installURL);
	            webAppFolder = new File(FileLocator.toFileURL(resolvedURL).getFile());
			}
			catch (IOException e)
			{
                throw new ApplicationServerException("Can't find web application folder",e);
				
			}
	            if (!webAppFolder.exists())
	                throw new ApplicationServerException("Can't find web application folder");
		}
		return webAppFolder;
	}
	
	
}

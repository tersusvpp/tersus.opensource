/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.appserver;

import java.security.Principal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.realm.GenericPrincipal;
import org.apache.catalina.realm.RealmBase;

import tersus.runtime.SQLUtils;
import tersus.webapp.Engine;

/**
 * @author Youval Bronicki
 *  
 */
public class DefaultRealm extends RealmBase
{

    private Connection dbConnection;

    private PreparedStatement selectPasswordStmt;

    private PreparedStatement selectRolesStmt;

	private String passwordSQL;

	private String rolesSQL;

	private String permissionsDataSource;

    public DefaultRealm(String permissionsDataSource, boolean useSQLQoutedIdentifiers)
    {
		
		if (useSQLQoutedIdentifiers)
		{
			passwordSQL = "SELECT \"Password\" FROM \"Users\" WHERE \"User ID\" = ?";
			rolesSQL = "SELECT \"Role\" FROM \"User_Roles\" WHERE \"User ID\" = ?";
		}
		else
		{
			passwordSQL = "SELECT Password FROM Users WHERE User_ID = ?";
			rolesSQL = "SELECT Role FROM User_Roles WHERE User_ID = ?";
		}
		
		this.permissionsDataSource = permissionsDataSource;
    }


    /**
     *  
     */
    protected void connect()
    {
        if (dbConnection != null)
            return;
        dbConnection =         
         SQLUtils.connectToDataSource(permissionsDataSource);

        try
        {
            dbConnection.setAutoCommit(true); //We don't need transactions here
            selectPasswordStmt = dbConnection
                    .prepareStatement(passwordSQL);
            selectRolesStmt = dbConnection.prepareStatement(rolesSQL);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    /**
     *  
     */
    protected void disconnect()
    {
        SQLUtils.forceClose(selectPasswordStmt);
        selectPasswordStmt = null;
        SQLUtils.forceClose(selectRolesStmt);
        selectRolesStmt = null;
        SQLUtils.forceClose(dbConnection);
        dbConnection = null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.catalina.realm.RealmBase#getName()
     */
    protected String getName()
    {
        return "TersusDefaultRealm";
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.catalina.realm.RealmBase#getPassword(java.lang.String)
     */
    protected String getPassword(String username)
    {
        ResultSet rs = null;
        String password = null;
        try
        {
            selectPasswordStmt.setString(1, username);
            rs = selectPasswordStmt.executeQuery();
            if (rs.next())
                password = rs.getString(1);
            rs.close();
            rs = null;

        }
        catch (SQLException e)
        {
            SQLUtils.forceClose(rs);
            rs = null;
            e.printStackTrace();
            disconnect();
        }
        finally
        {
            SQLUtils.forceClose(rs);
        }
        return password;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.catalina.realm.RealmBase#getPrincipal(java.lang.String)
     */
    protected Principal getPrincipal(String username)
    {
        connect();
        ResultSet rs = null;
        ArrayList roles = new ArrayList();
        String password = getPassword(username);
        if (password == null)
            return null;
        try
        {
            selectRolesStmt.setString(1, username);
            rs = selectRolesStmt.executeQuery();
            while (rs.next())
            {
                roles.add(rs.getString(1).trim());
            }
            rs.close();
            rs = null;
        }
        catch(SQLException e)
        {
            return null;
        }
        finally
        {
            SQLUtils.forceClose(rs);
            disconnect();
        }
        return new GenericPrincipal(username, password, roles);
    }
    public synchronized Principal authenticate(String username, String credentials)
    {

        GenericPrincipal principal = (GenericPrincipal)getPrincipal(username);
        if (principal == null)
            return null;
        String serverCredentials = principal.getPassword();
        if ( (serverCredentials == null)
             || (!serverCredentials.equals(credentials)) )
            return null;
        return principal;
    }
    public void stopInternal() throws LifecycleException
    {
        super.stopInternal();
        disconnect();
    }
}
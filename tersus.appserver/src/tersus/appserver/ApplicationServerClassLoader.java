/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.appserver;

import tersus.eclipse.ExtensionClassLoader;

/**
 * A class loader that loads all classes visible to the application server class
 * and all extension classes
 * @author Youval Bronicki
 *
 */
public class ApplicationServerClassLoader extends ExtensionClassLoader
{
    public ApplicationServerClassLoader()
    {
        super(ApplicationServer.class.getClassLoader());
    	System.out.println("ApplicationServerClassLoader created");
    }

	@Override
	protected void finalize() throws Throwable
	{
	
	   	System.out.println("ApplicationServerClassLoader finalized");
	}
    

}

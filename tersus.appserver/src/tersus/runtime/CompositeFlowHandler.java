/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.runtime;

import java.util.ArrayList;
import java.util.ListIterator;

import tersus.model.FlowModel;
import tersus.model.FlowType;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.runtime.trace.EventType;

/**
 * A Handler for composite flows. This InstanceHandler, together with the
 * associated element handlers (LinkHandler, SlotHandler) implements the real
 * semantics of the language. <br>
 * The 'core' of the runtime algorithm is implemented in the method
 * <code>run()</code>, which activates any ready elements as long as there
 * are such elements.
 * 
 * 
 * @author Youval Bronicki
 * 
 */
public class CompositeFlowHandler extends FlowHandler
{

    /**
     * Holds the subset of sub-flows who are ready as soon as the process starts
     * (i.e. those who have no mandatory triggers)
     */
    private ElementHandler[] selfReadyElements;

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
        setStatus(flow, FlowStatus.STARTED);
        traceStarted(context, flow);
        // add trigger values to the ready queue
        for (int i = 0; i < triggers.length; i++)
        {
            SlotHandler handler = triggers[i];
            Object value = handler.get(null, flow);
            // FUNC2 handle repetitive case
            FlowDataInstance element = new FlowDataInstance();
            element.init(flow, handler, value);
            addReadyElement(context, flow, element);
        }
        // Create the main data (it has to exist if sub-flows are to find it)

        // create self-ready elements and add them to the ready queue
        for (int i = 0; i < selfReadyElements.length; i++)
        {
            ElementHandler elementHandler = selfReadyElements[i];
//            if (context.trace.traceCreation)
//            {
//                InstanceHandler childInstanceHandler = elementHandler
//                        .getChildInstanceHandler();
//                Enum type = childInstanceHandler.getType();
//                if (childInstanceHandler instanceof DataHandler
//                        && ((DataHandler) childInstanceHandler).isConstant())
//                    type = ObjectType.CONSTANT;
//                 context.trace.add(EventType.CREATED, type,
//                 elementHandler.getRole(), null);
//            }
            elementHandler.create(context, flow);
        }

        try
        {
            run(context, flow);
            if (type == FlowType.ACTION || type == FlowType.SERVICE)
                chargeDoneExit(context, flow);
        }

        catch (RuntimeException e)
        {
            fireError(context, flow, e);
        }
        if (type == FlowType.ACTION || type == FlowType.SERVICE)
        {
            setStatus(flow, FlowStatus.FINISHED);
            traceFinish(context, flow);
        }
        else
        {
            setStatus(flow, FlowStatus.WAITING_FOR_INPUT);
            traceWaiting(context, flow);
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.FlowHandler#resume(tersus.runtime.RuntimeContext,
     *      tersus.runtime.FlowState)
     */
    public void resume(RuntimeContext context, FlowInstance flow)
    {
        setStatus(flow, FlowStatus.RESUMED);
        traceResumed(context, flow);
        for (int i = 0; i < inputSlots.length; i++)
        {
            SlotHandler handler = inputSlots[i];
            Object value = handler.get(context, flow);
            FlowDataInstance element = new FlowDataInstance();
            handler.remove(context, flow);
            element.init(flow, handler, value);
            addReadyElement(context, flow, element);
        }
        // Handle waiting elements
        run(context, flow);

        setStatus(flow, FlowStatus.WAITING_FOR_INPUT);
        traceWaiting(context, flow);

    }

    /**
     * Performs the 'core' flow execution algorithm by activating all elements
     * in the current flow's ready queue.
     * 
     * @param context
     *            The <code>RuntimeContext</code> in which this flow is
     *            executed
     * @param flow
     *            The <code>FlowState</code> object that represents the state
     *            of the flow instance being activated.
     */
    public void run(RuntimeContext context, FlowInstance flow)
    {
        createParentElementReferences(context, flow);
        while (flow.hasReadyElements())
        {
            IFlowElement element = flow.nextReadyElement();
            FlowElementHandler handler = element.getElementHandler();
            handler.invoke(context, element); // Start or resume
        }

    }

    /**
     * Adds an element to the stack of ready elements.
     * 
     * The element is added in a location that conforms to the execution order
     * of elements in the model
     * 
     * @param element
     */
    void addReadyElement(RuntimeContext context, FlowInstance flow,
            IFlowElement element)
    {
        int rank = getDependencyRank(element);
        if (flow.readyElements.isEmpty())
        {
            flow.readyElements.add(element);
            if (context.trace.traceReady)
                context.trace.add(EventType.READY_TO_START, element.getType(),
                        element.getElementHandler().getRole(), "rank=" + rank
                                + "; Added as sole ready element.");
        }
        else
        {
            ListIterator i = flow.readyElements.listIterator();
            while (i.hasNext())
            {
                IFlowElement readyElement = (IFlowElement) i.next();
                int currentElementRank = getDependencyRank(readyElement);
                if (currentElementRank <= rank)
                {
                    // Add the new element before the current one
                    if (context.trace.traceReady)
                        context.trace.add(EventType.READY_TO_START, element
                                .getType(), element.getElementHandler()
                                .getRole(), "rank=" + rank
                                + "; Added after element '"
                                + readyElement.getElementHandler().getRole()
                                + "' whose rank is" + currentElementRank);
                    i.previous();
                    i.add(element);
                    return;
                }
            }
            i.add(element); // add the element as last in the list (i.e. first
            // to be executed)
            if (context.trace.traceReady)
                context.trace.add(EventType.READY_TO_START, element.getType(),
                        element.getElementHandler().getRole(), "rank=" + rank
                                + "; Added as first ready element.");
        }
    }

    /**
     * @param readyElement
     */
    private int getDependencyRank(IFlowElement readyElement)
    {
        return readyElement.getElementHandler().getDependencyRank();
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
     */
    public void initializeFromModel(Model model)
    {
        ((FlowModel) model).updateRanks();
        super.initializeFromModel(model);

        /*
         * After initializing all the elements, the LinkHandlers need to
         * 'prepared' for invocation
         */

        for (int i = 0; i < nElements; i++)
        {
            if (elementHandlers[i] instanceof LinkHandler)
            {
                ((LinkHandler) elementHandlers[i]).prepare();
            }
        }

        /*
         * Initialize the array of 'self-ready' sub-flows (those that have no
         * mandatory triggers), and the mainDataElement
         */

        ArrayList selfReadyElementList = new ArrayList();
        for (int i = 0; i < nElements; i++)
        {
            FlowElementHandler elementHandler = (FlowElementHandler) elementHandlers[i];

            if (elementHandler.isSelfReady())
            {
                ModelId childId = elementHandler.getChildModelId();
                Model childModel = getModel().getRepository().getModel(childId,
                        true);
                if (childModel.getPluginDescriptor() != null
                        && childModel.getPluginDescriptor()
                                .isAutomaticallyInvoked())
                    selfReadyElementList.add(elementHandler);
            }
        }

        selfReadyElements = (ElementHandler[]) selfReadyElementList
                .toArray(new ElementHandler[0]);
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.FlowHandler#traceResumed(tersus.runtime.RuntimeContext,
     *      tersus.runtime.FlowState)
     */
    protected void traceResumed(RuntimeContext context, FlowInstance flow)
    {
        if (context.trace.traceStarted)
        {
            for (int i = 0; i < inputSlots.length; i++)
            {
                SlotHandler handler = inputSlots[i];
                Object value = handler.get(null, flow);
                context.trace.addDetailValue(handler, value);
            }
            super.traceResumed(context, flow);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.FlowHandler#doStart(tersus.runtime.RuntimeContext,
     *      tersus.runtime.FlowInstance)
     */
    public void doStart(RuntimeContext context, FlowInstance flow)
    {
        if (getType() == FlowType.DISPLAY)
            return; // We don't want to invoke display models on the server
        super.doStart(context, flow);
    }

}
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import tersus.util.Enum;

/**
 * An enumeration the possible statuses of flow instances
 * 
 * @author Youval Bronicki
 *
 */
public class FlowStatus extends Enum
{

	/**
	 * A flow is not ready to start if it has been created and has not yet received
	 * values for all of its mandatory triggers
	 */
	public static FlowStatus NOT_READY_TO_START= new FlowStatus("Not Ready To Start");
	/**
	 * A flow is ready to start if it has received values for all of its mandatory triggers,
	 * but has not yet started.
	 */ 
	public static FlowStatus READY_TO_START= new FlowStatus("Ready To Start");
	/**
	 * A flow 'is started' if it has received all of its mandatory triggers and its execution has
	 * started and is 'currently executing'.  This status is transient in the following sense:
	 * (1) In most cases the flow has this status for a very short time (2) The flow will usually
	 * reach another status before it is written to the database.
	 */
	public static FlowStatus STARTED = new FlowStatus("Started");
	/**
	 * A flow is waiting for input if (1) it is not an Action. (2) It has started (3) It has not yet finished
	 * (4) It is not currently executing (i.e. it's status is not 'started' or 'resumed')
	 */
	public static FlowStatus WAITING_FOR_INPUT= new FlowStatus("Waiting For Input");
	/**
	 * A flow is ready to resume if it received input (through an input slot
	 * or externally) and has not yet been resumed by its parent flow.
	 */ 
	public static FlowStatus READY_TO_RESUME= new FlowStatus("Ready To Resume");
	/** 
	 * A flow is 'resumed' if it has been resumed by its parent flow and is still executing. This 
	 * status is transient in the same sense that 'started' is transient.
	 */
	public static FlowStatus RESUMED = new FlowStatus("Resumed");
	/**
	 * A flow is 'finished' if (1) It is an Action which has been started and its execution has ended or (2)
	 * It is a flow that 'has reached' a terminating exit
	 * 
	 */
	public static FlowStatus FINISHED = new FlowStatus("Finished");
	
	
	public static FlowStatus[] values = {NOT_READY_TO_START, READY_TO_START, STARTED, WAITING_FOR_INPUT, READY_TO_RESUME, RESUMED, FINISHED};
	 
	/**
	 * Creates a new FlowStatus with the given value
	 * @param value
	 */
	private FlowStatus(String value)
	{
		super(value);
	}
	/* (non-Javadoc)
	 * @see tersus.util.Enum#getValues()
	 */
	protected Enum[] getValues()
	{
		return values;
	}
}

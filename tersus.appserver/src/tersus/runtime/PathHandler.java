/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tersus.model.ElementPath;
import tersus.model.InvalidModelException;
import tersus.model.Path;
import tersus.model.Role;
import tersus.runtime.trace.EventType;
import tersus.util.Misc;

/**
 * NICE2 [Description]
 * 
 * @author Youval Bronicki
 *
 */
/**
 * Handles path related operations - retrieving elements and/or creating
 * intermediate levels
 * 
 * @author Youval Bronicki
 *
 */
public class PathHandler
{
	private boolean repetitive;
	ElementHandler segments[];
	InstanceHandler valueHandler;
	ElementPath path;

	/**
	 * Creates a PathHandler for a given path starting at a given InstanceHandler
	 * 
	 * @param rootHandler The source InstanceHandler
	 * @param path The path 
	 * @throws InvalidModelException if the class traverses a non-composite node, or if
	 * the path is otherwise invalid
	 */
	public PathHandler(InstanceHandler rootHandler, ElementPath path)
	{
		this.path = path;
		segments = new ElementHandler[path.getNumberOfSegments()];
		InstanceHandler current = rootHandler;
		for (int i = 0; i < segments.length; i++)
		{
			ElementHandler elementHandler = current.getElementHandler(path.getSegment(i));
			segments[i] = elementHandler;
			if (elementHandler == null)
				throw new InvalidPathException(
					"Path '" + path + "' is invalid for model '" + rootHandler.getModelId() + "'");
			current = elementHandler.getChildInstanceHandler();
		}
		valueHandler = current;
		calcRepetitive();
	}

	/**
	 * Creates a PathHandler for a given path starting at a given ElementHandler.
	 * 
	 * Using this constructor is equivalent to using <code>PathHandler(InstanceHandler rootHandler, Path path)</code> 
	 * with the child instance handler, with the exception that if the path is empty, there is no need to retrieve the 
	 * child (the child may be null for slots).
	 * 
	 * @param elementHandler The source ElementHandler
	 * @param path The path 
	 * @throws InvalidModelException if the class traverses a non-composite node, or if
	 * the path is otherwise invalid
	 */
	public PathHandler(ElementHandler elementHandler, Path path)
	{
		ElementHandler current = elementHandler;
		segments = new ElementHandler[path.getNumberOfSegments()];
		for (int i = 0; i < segments.length; i++)
		{
			InstanceHandler child = current.getChildInstanceHandler();
			current = child.getElementHandler(path.getSegment(i));
			if (current == null)
				throw new InvalidPathException(
						"Path '" + path + "' is invalid for model '" + elementHandler.getChildModelId() + "'");
			segments[i] = current;
		}
		valueHandler = current.getChildInstanceHandler();
		calcRepetitive();
	}
	/**
	 * Gets the object at the end of the path.
	 * The path must not contain repetitive segments for this method to work.
	 * @return the object at the end of the path
	 * @throws ClassCastException if any of the path's segments is repetitive 
	 */
	public Object get(RuntimeContext context, Object root)
	{
		if (isRepetitive())
			return getList(context, root);
		Object current = root;
		for (int i = 0; i < segments.length; i++)
		{
			if (current == null)
				return null;
			ElementHandler segment = segments[i];
			current = segment.get(context, current);
		}
		return current;
	}
	/**
	 * Sets the given value at the end of the path
	 * If the path points to a slot of a repetitive sub-process, automatically create the instance (InputSet) for that process
	 * @param context A <code>RuntimeContext</code> for the operation
	 * @param root The root instance at which path traversal should start
	 */
	public void set(RuntimeContext context, Object root, Object value)
	{
		Object parent = root;
		int nSegments = segments.length;
		for (int i = 0; i < nSegments - 1; i++)
		{
			ElementHandler segment = segments[i];
			Object child = segment.get(null, parent);
			if (child == null ||  segment instanceof SubFlowHandler && segment.repetitive && child == Collections.EMPTY_LIST)
			{
				child = segment.create(context, parent);
				if (context.trace.traceCreation)
				{
					context.trace.add(
						EventType.CREATED,
						segment.getChildInstanceHandler().getType(),
						path.getPrefix(i + 1), null);
				}
			}
			parent = child;
		}
		ElementHandler lastSegment = segments[nSegments - 1];
		lastSegment.set(context, parent, value);
	}
	/**
	 * Removes the given value at the end of the path
	 * @param context A <code>RuntimeContext</code> for the operation
	 * @param root The root instance at which path traversal should start
	 */
	void remove(RuntimeContext context, Object root)
	{
		Object parent = root;
		int nSegments = segments.length;
		for (int i = 0; i < nSegments - 1; i++)
		{
			ElementHandler segment = segments[i];
			Object child = segment.get(null, parent);
			if (child == null)
			{
				return;
			}
			parent = child;
		}
		ElementHandler lastSegment = segments[nSegments - 1];
		lastSegment.remove(context, parent);
	}
	/**
	 * Retrieves all elements identified by this path, starting from a specified
	 * root node. 
	 * 
	 * <b>Note</b> This method creates a new ArrayList.  To eliminate excessive
	 *   creation, use addToList()
	 * @param context A <code>RuntimeContext</code> for the operation
	 * @param root The root object (to which the path referes)
	 * @return A new list with all the elements.  If there are no elements, the list is
	 * empty.
	 */

	public List getList(RuntimeContext context, Object root)
	{
		List list = new ArrayList();
		addToList(context, root, list, 0);
		return list;
	}
	/**
	 * Retrieves elements from an object hierarchy, adding them to a target list.
	 * The elements are retrieved by applying a suffix of the path, to a specified root node.
	 * @param context A <code>RuntimeContext</code> for the operation
	 * @param root The root node
	 * @param target The target list (to which element are appended)
	 * @param offset The number of head segments of the path that should be ignored  
	 */
	public void addToList(RuntimeContext context, Object root, List target, int offset)
	{
		if (root == null)
			return;
		if (offset == segments.length)
		{
			target.add(root);
			return;
		}
		ElementHandler segment = segments[offset];
		Object value = segment.get(context, root);

		if (value instanceof List) //NICE3 validate conformance to segment.isRepetitive()
		{
			List values = (List) value;
			int nValues = values.size();
			for (int i = 0; i < nValues; i++)
				addToList(context, values.get(i), target, offset + 1);
		}
		else
		{
			addToList(context, value, target, offset + 1);
		}

	}

	/**
	 * @return The InstanceHandler for the element at the 'end' the path
	 */
	public InstanceHandler getValueHandler()
	{
		return valueHandler;
	}
	/**
	 * @return
	 */
	public ElementPath getPath()
	{
		return path;
	}

	/**
	 * 
	 */
	public ElementHandler getLastSegment()
	{
		return segments[segments.length - 1];
	}

	/**
	 * @param context
	 * @param root
	 * @param value
	 * @param trace indicates whether the a trace event should be kept for the operation
	 */
	public void accumulate(RuntimeContext context, Object root, Object value)
	{
		int nSegments = segments.length;
		ElementHandler lastSegment = segments[nSegments - 1];
		if (Misc.ASSERTIONS)
			Misc.assertion(lastSegment.isRepetitive());
		Object parent = root;
		for (int i = 0; i < nSegments - 1; i++)
		{
			ElementHandler segment = segments[i];
			Object child = segment.get(null, parent);
			if (child == null)
			{
				child = segment.create(context, parent);
				if (context.trace.traceCreation)
				{
					context.trace.add(
							EventType.CREATED,
							segment.getChildInstanceHandler().getType(),
						segment.getRole(),
						path.getPrefix(i + 1));
				}
			}
			parent = child;
		}
		lastSegment.accumulate(context, parent, value);
	}

	/**
	 * @param i
	 * @return
	 */
	public ElementHandler getSegment(int i)
	{
		return segments[i];
	}

	/**
	 * @return true if any of the elements along the path is repetitive
	 */
	private void calcRepetitive()
	{
		for (int i = 0; i < segments.length; i++)
			if (segments[i].isRepetitive())
			{
				repetitive = true;
				return;
			}
		repetitive = false;
	}

	/**
	 * @return
	 */
	public boolean isRepetitive()
	{
		return repetitive;
	}
/**
 * @deprecated use the more general FieldList mechanism 
 */
	public List<PathHandler> allLeafHandlers (InstanceHandler rootHandler) // For data handlers only
	{
		List<PathHandler> handlers = new ArrayList<PathHandler>();

		if (valueHandler instanceof CompositeDataHandler)
		{
			for (int i = 0; i < valueHandler.nElements; i++)
			{
				ElementHandler elementHandler = valueHandler.elementHandlers[i];
				Path childPath = (path instanceof Path ? ((Path) path).getCopy() : new Path((Role) path));
				childPath.addSegment(elementHandler.getRole());
				PathHandler childhandler = new PathHandler(rootHandler,childPath);
	
				handlers.addAll(childhandler.allLeafHandlers(rootHandler));
			}
		}
		else
			handlers.add(this);

		return handlers;
	}

    /**
     * @return
     */
    public int getNumberOfSegments()
    {
        return segments.length;
    }
}

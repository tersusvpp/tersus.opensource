package tersus.runtime;

import java.util.Collection;
import java.util.TimerTask;

public class ContextLeakCleanupTask extends TimerTask
{

	ContextPool pool;
	public ContextLeakCleanupTask(ContextPool contextPool)
	{
		this.pool = contextPool;
	}

	@Override
	public void run()
	{
		Collection<RuntimeContext> used = pool.getUsedContextsCopy();
		
		for (RuntimeContext c : used)
		{
			if (c.getRemainingTimeMilliseconds() < 0)
			{
				c.abort(c+" indetified as leaked");
				c.disposeAndCloseBaseConnections();
			}
		}
	}

}

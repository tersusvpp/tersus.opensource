/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
package tersus.runtime;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class PostgreSQLAdapter implements DatabaseAdapter
{
	public boolean test(RuntimeContext context)
	{
		return true;
	}
    public String getIdentity()
	{
		return "SERIAL";
	}

	public boolean useColumnNamesForDBGenereatedColumns()
	{
		return false;
	}
	public String getSelectForUpdate(String column, String table, String condition)
    {
        return "SELECT "+column+" FROM "+table + " WHERE "+condition + " FOR UPDATE";
    }

    public boolean supportsQueryPaging()
    {
        return true;
    }
    private static final String DUPLICATE_KEY_ERROR_MESSAGE_PREFIX = "ERROR: duplicate key";

    public boolean alterTableSupportsMultipleColumns()
    {
        return false;
    }

    public String convertTableName(String tableName)
    {
        return tableName.toLowerCase();
    }


    public String getBoolean()
    {
        return "CHAR";
    }

    public String getChar(int maxLength)
    {
        return "CHAR("+maxLength+")";
    }

    public String getDatabaseProductName()
    {
        return "PostgreSQL";
    }

    public String getDate()
    {
        return "date";
    }

    public String getDateTime()
    {
        return "timestamp";
    }

    public String getDouble()
    {
        return "double precision";
    }

    public String getExceptionType(SQLException e)
    {
        if (e.getMessage().startsWith(DUPLICATE_KEY_ERROR_MESSAGE_PREFIX))
            return DUPLICATE_KEY;
        else
            return e.getMessage();
    }

    public String getInt()
    {
        return "int";
    }

    public String getLongVarbinary()
    {
        return "bytea";
    }

    public String getTableTypeDDL(String tableType)
    {
        return "";
    }

    public String getVarchar(int maxLength)
    {
        if (maxLength == 0)
            return "text";
        else
            return "varchar("+maxLength+")";
    }

    public void shutdown(RuntimeContext context)
    {
    }

    public String translateMessage(SQLException e)
    {
        return e.getMessage();
    }

    public boolean canResumeAfterExceptions()
    {
        return false;
    }
    public String getStoredProcedureClause(String parameterName)
    {
        return parameterName + "=?";
    }
    public String prepareSelectStatement(StringBuffer columns, StringBuffer table, String primaryKey, StringBuffer condition, String orderBy, int numberOfRecords, int offset, Set<String> optimizations)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT ");
        sql.append(columns);
        sql.append(" FROM ");
        sql.append(table);
        if (condition.length() > 0)
        {
            sql.append(" WHERE ");
            sql.append(condition);
        }
        if (orderBy != null && orderBy.length() > 0)
        {
            sql.append(" ORDER BY ");
            sql.append(orderBy);
        }
        if (numberOfRecords > 0)
        {
            sql.append(" LIMIT ");
            sql.append(numberOfRecords);
        }
        if (offset > 0)
        {
            sql.append(" OFFSET ");
            sql.append(offset);
        }
        return sql.toString();
    }
    public boolean requiresPadding()
    {
        return false;
    }

    public boolean supportsQueryTimeout()
	{
		return false;
	}
    static int id=0;
	synchronized static int nextId()
	{
		return ++id;
	}
	public String getConectionId(Connection c, RuntimeContext context)
	{
		return "PostgreSQL Connection "+nextId();
	}
	
	public String prepareCountStatement(StringBuffer table, StringBuffer condition,
			HashSet<String> optimizations)
	{
		String sql = "SELECT count(1) FROM " + table;
		if (condition.length() > 0)
			sql += " WHERE " + condition;
		return sql;
	}

	public boolean commit(Connection c, RuntimeContext context) throws SQLException
	{
		if (c.getAutoCommit())
			return false;
		c.commit();
		return true;
	}
	public boolean rollback(Connection c, RuntimeContext context) throws SQLException
	{
		if (c.getAutoCommit())
			return false;
		c.rollback();
		return true;
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2007 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. Initial API and implementation
 *************************************************************************************************/
package tersus.runtime;

public interface IResourceHandler
{
    void connect(RuntimeContext context, String dataSourceName);
    void beginTransaction(RuntimeContext context);
    void rollbackTransaction(RuntimeContext context);
    void commitTransaction(RuntimeContext context);
}

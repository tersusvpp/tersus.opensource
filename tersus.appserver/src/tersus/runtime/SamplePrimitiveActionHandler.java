/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.trace.EventType;

/**
 * A Sample flow handler that performs addition of <code>Number<conde>s from slots 'x' and 'y', putting
 * the result on slot 'sum'.
 * 
 * 
 * This class is used for test purposes.
 * 
 * @author Youval Bronicki
 *
 */
public class SamplePrimitiveActionHandler extends FlowHandler
{
	SlotHandler xHandler, yHandler, sumHandler;
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		setStatus ( flow , FlowStatus.STARTED );
		Number x = (Number) xHandler.get(null, flow);
		Number y = (Number) yHandler.get(null, flow);
		Number sum = new Number(x.value + y.value);
		sumHandler.set(context, flow, sum);
		if (context.trace.traceSet)
		{
			context.trace.addDetailValue((DataHandler)sumHandler.getChildInstanceHandler(),sum);
			context.trace.add(EventType.SET, sumHandler.getType(), sumHandler.getRole(), null);
		}
		setStatus ( flow , FlowStatus.FINISHED );
		traceFinish(context, flow);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		xHandler = (SlotHandler) getElementHandler(Role.get("x"));
		yHandler = (SlotHandler) getElementHandler(Role.get("y"));
		sumHandler = (SlotHandler) getElementHandler(Role.get("sum"));

	}

}

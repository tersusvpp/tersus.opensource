/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.runtime;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Timer;

import org.apache.log4j.Hierarchy;
import org.apache.log4j.Logger;

import tersus.model.BuiltinProperties;
import tersus.model.FlowModel;
import tersus.model.FlowType;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.PackageId;
import tersus.model.PluginDescriptor;
import tersus.model.Repository;
import tersus.model.Role;
import tersus.model.indexer.ElementEntry;
import tersus.model.indexer.ModelEntry;
import tersus.model.indexer.RepositoryIndex;
import tersus.util.FileUtils;
import tersus.util.Misc;
import tersus.webapp.Engine;
import tersus.webapp.LoggingHelper;

/**
 * @author Youval Bronicki
 * 
 */
public class ContextPool
{

	protected static final int DEFAULT_TIMEOUT_MILLISECONDS = 600000;
	protected static final int DEFAULT_CONTEXT_LEAK_CLEANUP_INTERVAL = 600000;

	public static final String CACHE_LOGGER_NAME = "tersus_engine.cache";

	public static final String PERMISSION_LOGGER_NAME = "tersus_engine.permissions";

	public static final String SQL_LOGGER_NAME = "tersus_engine.sql";

	public static final String ENGINE_LOGGER_NAME = "tersus_engine";

	public static final String POOL_LOGGER_NAME = "tersus_context_pool";

	public static final String ACTIVITY_LOGGER_NAME = "tersus_engine.activity";

	public static final String CONNECTION_LOGGER_NAME = "tersus_connections";
	
	public static final String AUTO_COMMIT_DATA_SOURCES = "auto_commit_data_sources";

	protected boolean debug;

	private String rootId;
	
	/* Identifies which data sources should be left out of transactions:
	 * NULL (default) - all except the default data source
	 * Empty string - none
	 * "*" - all
	 * other - comma separated list of data source names
	 */
	private Set<String> autoCommitDataSourceSet = null;

	private boolean initDB = true;

	private boolean checkDB = false;
	private boolean completeTracing = false;
	private boolean checkRequestSignatures = false;

	private boolean useQuotedIdentifiers = false;
	private LowPrecisionClock clock;
	private ConnectionIdHelper connectionIdHelper = new ConnectionIdHelper();

	public boolean getCheckRequestSignatures()
	{
		return checkRequestSignatures;
	}

	public void setCheckRequestSigsnatures(boolean checkRequestSignature)
	{
		this.checkRequestSignatures = checkRequestSignature;
	}

	private Repository repository;

	private long deafultTimeoutMilliseconds = DEFAULT_TIMEOUT_MILLISECONDS;

	private String traceDirPath = null;

	private ArrayList<RuntimeContext> pool = new ArrayList<RuntimeContext>();
	private HashSet<RuntimeContext> usedContexts = new HashSet<RuntimeContext>();
	private Map<PermissionSet, Set<ModelId>> permittedModelCache = new HashMap<PermissionSet, Set<ModelId>>();
	protected File[] timestampFiles;
	protected File dbTimestampFile;
	protected long lastTimestamp = 0;
	protected long lastDBTimestamp = 0;
	
	protected int contextLeakCleanupInterval = DEFAULT_CONTEXT_LEAK_CLEANUP_INTERVAL;

	private ClassLoader classLoader = getClass().getClassLoader();

	protected ArrayList<DatabaseAdapter> databaseAdapters = new ArrayList<DatabaseAdapter>();

	protected boolean debugMode;

	protected List<Runnable> shutdownActions  = new ArrayList<Runnable>();


	protected HashMap<String, Object> properties = new HashMap<String, Object>();

	private ModelLoader modelLoader;

	private Map<Role, List<ModelId>> eventHandlers;

	private boolean preloadHandlers = true;

	private long applicationStartTime;

	private Date fixedTimestamp;

	private String appName = "TersusApp";

	private String permissionsDataSource = Engine.MAIN_DATA_SOURCE;
	private static final String DEFAULT_PERMISSIONS_QUERY="select Role_Permissions.Permission from User_Roles, Role_Permissions where User_Roles.User_Id=? and User_Roles.Role = Role_Permissions.Role";;
	private static final String DEFAULT_PERMISSIONS_QUERY_QUOTED="select \"Role_Permissions\".\"Permission\" from \"User_Roles\", \"Role_Permissions\" where \"User_Roles\".\"User ID\"=? and \"User_Roles\".\"Role\" = \"Role_Permissions\".\"Role\"";;
	
	private String permissionsQuery = null;
	private String permissionsLastUpdateQuery = null;

	public Logger engineLog = Logger.getLogger(ENGINE_LOGGER_NAME);
	public Logger poolLog = Logger.getLogger(POOL_LOGGER_NAME);
	public Logger sqlLog = Logger.getLogger(SQL_LOGGER_NAME);
	public Logger permissionLog = Logger.getLogger(PERMISSION_LOGGER_NAME);
	public Logger cacheLog = Logger.getLogger(CACHE_LOGGER_NAME);
	public Logger activityLog = Logger.getLogger(ACTIVITY_LOGGER_NAME);
	public Logger connectionLog = Logger.getLogger(CONNECTION_LOGGER_NAME);
	private Hierarchy logHierarchy = null;

	public void setLogHierarchy(Hierarchy h)
	{
		logHierarchy = h;
	}

	public Logger getLogger(String name)
	{
		if (logHierarchy != null)
			return logHierarchy.getLogger(name);
		else
			return Logger.getLogger(name);
	}

	private int traceMaxStringLength = 0;

    public int getTraceMaxStringLength()
	{
		return traceMaxStringLength;
	}

	public void setTraceMaxStringLength(int maxStringLength)
	{
		this.traceMaxStringLength = maxStringLength;
	}

	public void init(Repository repository, String rootId, String traceDirPath, boolean debug)
	{
		initClock();
		this.repository = repository;
		this.setRootId(rootId);
		this.debug = debug;
		setTraceDirPath(traceDirPath);

		if (getRootModel() == null)
			throw new RuntimeException("Root model '" + getRootId() + "' not found in '"
					+ repository);
		String checkSig = String.valueOf(getRootModel().getProperty(
				BuiltinProperties.CHECK_REQUEST_SIGNATURE));

		if ("true".equals(checkSig))
			setCheckRequestSigsnatures(true);
		setAppName(getRootModel().getName());
		if (permissionsQuery == null)
			permissionsQuery = useQuotedIdentifiers() ? DEFAULT_PERMISSIONS_QUERY_QUOTED : DEFAULT_PERMISSIONS_QUERY;
		initEventHandlers();
	}

	public void initClock()
	{
		if (clock == null)
			clock = new LowPrecisionClock();
	}

	public void initDBAdapters(String databaseAdaptersStr)
	{
		if (databaseAdaptersStr != null)
		{
			StringTokenizer tokenizer = new StringTokenizer(databaseAdaptersStr, ", ");
			while (tokenizer.hasMoreTokens())
			{
				String databaseAdapterClassName = tokenizer.nextToken().trim();
				DatabaseAdapter databaseAdapter = null;
				try
				{
					databaseAdapter = (DatabaseAdapter) getClassLoader().loadClass(
							databaseAdapterClassName).newInstance();
				}
				catch (Exception e)
				{
					throw new RuntimeException("Failed to instantiate database adapter "
							+ databaseAdapterClassName);
				}
				engineLog.info("registering database adapter for "
						+ databaseAdapter.getDatabaseProductName());
				databaseAdapters.add(databaseAdapter);
			}
		}
	}

	private static final InvocationAdapter startupInvocationAdapter = new InvocationAdapter()
	{

		public String getLogType()
		{
			return "Startup";
		}

		public boolean needsAccessValidation()
		{
			return false;
		}
		
		public void setTriggers(RuntimeContext context, FlowInstance serviceInstance,
				SlotHandler[] triggers)
		{
			// Nothing to do - no triggers on startup
		}

	};

	synchronized public void startUp()
	{
		initUIModelMaps();
		initSecureObjectSet();
		initModelLoader();
		initContextLeakCleanupTimer();
		RuntimeContext context = null;
		LoggingHelper.get().startRequest(activityLog, engineLog, null);
		try
		{
			context = getContext();
			String traceFileName = null;
			applicationStartTime = System.currentTimeMillis();
			long loadTime = 0;

			if (preloadHandlers)
			{
				loadTime += loadModels();
				loadTime += loadHandlers();
			}
			if (initDB || checkDB)
			{
				initOrCheckDB(context);
			}
			context.setRoot(true);
			context.setRequestType("Startup");
			if (debugMode)
			{
				DateFormat dateAndTimeFormat = new SimpleDateFormat("yyyy-MM-dd-HHmmss");
				String dateTimeStr = dateAndTimeFormat.format(new Date());
				traceFileName = "Startup-" + dateTimeStr;
				createTraceFile(traceFileName);
			}
			context.runProcess(null, "", traceFileName, startupInvocationAdapter);
			poolContext(context);
			context = null;
			LoggingHelper.get().endRequest();
		}
		catch (IOException e)
		{
			engineLog.error("Startup failed", e);
			LoggingHelper.get().endRequest(e);
			throw new RuntimeException("Startup failed", e);

		}
		catch (RuntimeException e)
		{
			LoggingHelper.get().endRequest(e);
			engineLog.error("Startup failed", e);
			throw e;
		}
		catch (Error e)
		{
			LoggingHelper.get().endRequest(e);
			throw e;
		}
		finally
		{
			if (context != null)
			{
				context.dispose();
				context = null;
			}
			LoggingHelper.dispose();
		}

	}

	private void initContextLeakCleanupTimer()
	{
		if (getContextLeakCleanupInterval() > 0)
		{
			final Timer timer = new Timer(true);
			timer.schedule(new ContextLeakCleanupTask(this), getContextLeakCleanupInterval(), getContextLeakCleanupInterval());
			addShutdownAction(new Runnable(){public void run(){timer.cancel();}});
		}
		
		
	}

	private void initOrCheckDB(RuntimeContext context)
	{
		FlowHandler rootHandler = (FlowHandler) context.getModelLoader().getHandler(
				new ModelId(getRootId()));
		context.connect(null);
		long startTime = System.currentTimeMillis();
		if (initDB)
		{
			context.getPersistenceManager().initDB(rootHandler);
			engineLog.info("DB initialization took " + (System.currentTimeMillis() - startTime)
					+ "ms.", null);
		}
		else if (checkDB)
		{
			context.getPersistenceManager().checkDB(rootHandler);
			engineLog.info("DB check took " + (System.currentTimeMillis() - startTime) + "ms.",
					null);
		}
		context.disconnect();
	}

	private void initUIModelMaps()
	{
		uiModelIds = repository.getUpdatedRepositoryIndex().calculateUIModelIds(getRootModel(),
				null);
		uiPackageIds = new HashSet<PackageId>();
		for (ModelId modelId : getUIModelIds())
		{
			uiPackageIds.add(modelId.getPackageId());
		}
	}

	/**
     * 
     */
	public void initModelLoader()
	{
		modelLoader = new ModelLoader(repository, getClassLoader());
		modelLoader.setUseQuotedIdentifiers(useQuotedIdentifiers());
		modelLoader.setContextPool(this);
	}

	public FlowModel getRootModel()
	{
		return (FlowModel) repository.getModel(new ModelId(getRootId()), true);
	}

	/**
     * 
     */
	private long loadModels()
	{
		System.gc();
		long start = System.currentTimeMillis();
		long startMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		Model root = repository.getModel(new ModelId(rootId), true);
		HashSet<Model> loaded = new HashSet<Model>();
		loadModelHierarchy(root, loaded);
		long duration = (System.currentTimeMillis() - start);
		engineLog.info("Models loaded in " + duration + " milliseconds");
		long memDelta = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()
				- startMemory;
		engineLog.info("Memory delta:" + memDelta);
		return duration;

	}

	private long loadHandlers()
	{
		System.gc();
		long start = System.currentTimeMillis();
		long startMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		InstanceHandler root = modelLoader.getHandler(new ModelId(rootId));
		HashSet<InstanceHandler> loaded = new HashSet<InstanceHandler>();
		loadHandlerHierarchy(root, loaded);
		long duration = (System.currentTimeMillis() - start);
		if (engineLog.isInfoEnabled())
			engineLog.info("Handlers loaded in " + duration + " milliseconds");
		long memDelta = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()
				- startMemory;
		if (engineLog.isInfoEnabled())
			engineLog.info("Memory delta:" + memDelta);
		return duration;

	}

	/**
	 * @param root
	 * @param loaded
	 */
	private void loadModelHierarchy(Model model, HashSet<Model> loaded)
	{
		if (loaded.contains(model))
			return;
		loaded.add(model);
		List<?> elements = model.getElements();
		for (int i = 0; i < elements.size(); i++)
		{
			ModelElement element = (ModelElement) elements.get(i);
			Model child = element.getReferredModel();
			if (child != null)
				loadModelHierarchy(child, loaded);
		}
	}

	private void loadHandlerHierarchy(InstanceHandler handler, HashSet<InstanceHandler> loaded)
	{
		if (loaded.contains(handler))
			return;
		loaded.add(handler);
		for (int i = 0; i < handler.elementHandlers.length; i++)
		{
			ElementHandler element = handler.elementHandlers[i];
			InstanceHandler child = element.getChildInstanceHandler();
			if (child != null)
				loadHandlerHierarchy(child, loaded);
		}
	}

	/**
	 * Dispose all contexts in the pool.
	 */
	public void disposeAll()
	{
		synchronized (pool)
		{
			while (!pool.isEmpty())
			{
				RuntimeContext context = pop();
				context.dispose();
			}
			pool.clear();
		}
	}

	public RuntimeContext getContext()
	{
		RuntimeContext context;
		boolean needsInit = false;
		synchronized (pool)
		{
			if (pool.isEmpty())
			{
				context = createContext();
				needsInit = true;
			}
			else
				context = pop();
			context.setStartTime(System.currentTimeMillis());
			context.setRequestId(LoggingHelper.get().getRequestId());
			context.setEndTime(-1);
			context.setRequestType(null);
			context.setServiceModelId(null);
			context.setRootModelId(null);
			context.setRootUser(null);
			usedContexts.add(context);
		}
		if (needsInit)
			initContext(context);
		context.setTimeout(getDefaultTimeoutMilliseconds()); // Will be reset later when the process starts, but need for SQL Query in getConnection


		// else if (context.getConnection() == null)
		// connectContext(context);
		context.clearPermissionCacheIfNeeded();

		if (poolLog.isInfoEnabled())
			poolLog.info(context + " obtained");
		return context;
	}

	private RuntimeContext pop()
	{
		RuntimeContext context;
		context = pool.remove(pool.size() - 1);
		return context;
	}

	public void poolContext(RuntimeContext context)
	{
		context.disconnect();
		synchronized (pool)
		{
			if (context.isDisposed())
				throw new EngineException(context  + " can't be pooled. It has already been disposed",null,null);
			context.setRoot(false);
			context.setEndTime(System.currentTimeMillis());
			pool.add(context);
			usedContexts.remove(context);
			// if (engineLog.isDebugEnabled())
			if (poolLog.isInfoEnabled())
				poolLog.info(context + " pooled", null);
		}
	}

	protected RuntimeContext createContext()
	{
		RuntimeContext context = new RuntimeContext();
		return context;
	}

	protected void initContext(RuntimeContext context)
	{
		long startTime = System.currentTimeMillis();
		context.setContextPool(this);
		context.setClock(clock);
		if (engineLog.isInfoEnabled())
			engineLog.info("Initializing " + context);
		context.setModelLoader(modelLoader);
		context.setConnectionIdHelper(getConnectionIdHelper());
		// connectContext(context);
		// Connection moved to the responsibility of Engine
		long initializationTime = System.currentTimeMillis() - startTime;
		if (engineLog.isInfoEnabled())
		{
			engineLog.info(context + " initialized in " + initializationTime + " ms.", null);
		}

		for (Iterator<DatabaseAdapter> i = databaseAdapters.iterator(); i.hasNext();)
		{
			context.getDatabaseAdapterManager().addDatabaseAdapter(i.next());
		}
	}

	/**
	 * @return
	 */
	public boolean isDebug()
	{
		return debug;
	}

	/**
     * 
     */
	public Repository getRepository()
	{
		return repository;
	}

	/**
	 * @return
	 */
	public String getTraceDirPath()
	{
		return traceDirPath;
	}

	/**
	 * @param string
	 */
	public void setTraceDirPath(String string)
	{
		traceDirPath = string;
		if (!getTraceDir().exists())
			getTraceDir().mkdirs();

	}

	/**
	 * @return
	 */
	public long getDefaultTimeoutMilliseconds()
	{
		return deafultTimeoutMilliseconds;
	}

	/**
	 * @param l
	 */
	public void setDefaultTimeoutMilliseconds(long l)
	{
		deafultTimeoutMilliseconds = l;
	}

	/**
	 * @param b
	 */
	public void setDebug(boolean b)
	{
		this.debug = b;
	}

	public int getPoolSize()
	{
		return pool.size();
	}

	/**
	 * @return
	 */
	public boolean reloadNeeded()
	{
		return getTimestamp() > lastTimestamp;
	}

	public long getTimestamp()
	{
		long timestamp = 0;
		if (timestampFiles != null)
		{
			for (int i = 0; i < timestampFiles.length; i++)
			{
				timestamp = Math.max(timestampFiles[i].lastModified(), timestamp);
			}
		}
		else if (this.fixedTimestamp != null)
			timestamp = this.fixedTimestamp.getTime();
		else
			timestamp = applicationStartTime;
		return timestamp;
	}

	public boolean hasExplicitTimestamp()
	{
		return timestampFiles != null;
	}

	public void reload()
	{
		lastTimestamp = getTimestamp();
		if (engineLog.isInfoEnabled())
			engineLog
					.info("Automatic reloading is on. Clearing repository cache and disposing contexts.");
		reloadRepository();
		clearPermittedModelCache();
		initUIModelMaps();
		initSecureObjectSet();
		initModelLoader(); // We create a new model loader in order not to
		// interfere with processes that are already running
		disposeAll(); // Because existing contexts are linked to the old model
		if (initDB || checkDB)
		{
			long dbTimestamp = getDbTimestamp();
			if (dbTimestamp > lastDBTimestamp)
			{
				lastDBTimestamp = dbTimestamp;
				RuntimeContext context = getContext();
				try
				{
					initOrCheckDB(context);
					poolContext(context);
					context = null;
				}
				finally
				{
					if (context != null)
					{
						context.dispose();
						context = null;
					}
				}

			}
		}

	}

	protected HashSet<ModelId> secureModels = new HashSet<ModelId>();

	protected void initSecureObjectSet()
	{
		secureModels.clear();
		RepositoryIndex index = repository.getUpdatedRepositoryIndex();
		HashSet<ModelId> current = new HashSet<ModelId>();
		HashSet<ModelId> next = new HashSet<ModelId>();
		for (ModelEntry modelEntry: index.allModelEntries())
		{
			boolean isSecret = isSecret(modelEntry);
			if (isSecret)
			{
				current.add(modelEntry.getId());
				secureModels.add(modelEntry.getId());
			}
		}
		while (current.size() > 0)
		{
			next.clear();
			for (ModelId id : current)
			{
				for (ElementEntry ref : index.getReferenceEntries(id))
				{
					ModelId parentId = ref.getParentId();
					ModelEntry parent = index.getModelEntry(parentId);
					if (parent != null)
					{
						String parentType = null;
						if (parent.getDescriptor() != null)
							parentType = parent.getDescriptor().getType();
						if (secureModels.contains(parentId))
							continue; // Already handled
						if ("Action".equals(parentType))
						{
							if (ref.isSlot())
							{
								secureModels.add(parentId);
							}
							continue;
						}
						secureModels.add(parentId);
						next.add(parentId);
					}
				}
			}
			HashSet<ModelId> t = current;
			current = next;
			next = t;
			next.clear();
		}

	}

	private boolean isSecret(ModelEntry modelEntry)
	{
		if (modelEntry == null)
			return false;
		PluginDescriptor descriptor = modelEntry.getDescriptor();
		if (descriptor == null)
			return false;
		return descriptor.isSecret();
	}

	private void clearPermittedModelCache()
	{
		synchronized (permittedModelCache)
		{
			permittedModelCache.clear();
		}

	}

	/** Reloads models in the repository */
	protected void reloadRepository()
	{
		getRepository().clearCache();
	}

	/**
	 * @return
	 */
	public File getTraceDir()
	{
		File traceDir = new File(getTraceDirPath());
		return traceDir;

	}

	/**
	 * @param rootId
	 *            The rootId to set.
	 */
	public void setRootId(String rootId)
	{
		this.rootId = rootId;
	}

	/**
	 * @return Returns the rootId.
	 */
	public String getRootId()
	{
		return rootId;
	}

	String traceFileSemaphore = "TRACE";

	public File createTraceFile() throws IOException
	{
		File file = null;
		synchronized (traceFileSemaphore)
		{
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

			DecimalFormat numberFormat = new DecimalFormat("0000");
			String dateStr = dateFormat.format(new Date());
			int fileNumber = 1;
			do
			{
				file = new File(getTraceDir(), dateStr + "-" + numberFormat.format(fileNumber));
				++fileNumber;
			}
			while (!file.createNewFile());
		}
		return file;
	}

	public File createTraceFile(String name) throws IOException
	{
		File file = null;
		file = new File(getTraceDir(), name);
		if (file.exists())
			file.delete();
		file.createNewFile();
		return file;
	}

	/**
	 * @param traceFile
	 */

	/**
     * 
     */
	synchronized public void reloadIfNeeded()
	{
		if (reloadNeeded())
			reload();
	}

	public void addShutdownAction(Runnable listener)
	{
		synchronized (shutdownActions)
		{
			if (!shutdownActions.contains(listener))
				shutdownActions.add(listener);
		}
	}
	public void removeShutdownAction(Runnable listener)
	{
		synchronized (shutdownActions)
		{
			shutdownActions.remove(listener);
		}
	}

	protected void shutdown()
	{
		/* duplicating the list to prevent ConcurentModificationException when shutdown actions remove themselves from the list */
		ArrayList<Runnable> list = new ArrayList<Runnable>();
		list.addAll(shutdownActions);
		for (Runnable action: list)
		{
			try
			{
				action.run();
			}
			catch (Throwable t)
			{
				error("Failed to invoke shutdown action", t);
			}
		}
		RuntimeContext context = null;
		try
		{
			context = getContext();
			context.getDefaultDBAdapter().shutdown(context);
			poolContext(context);
			context = null;
		}
		finally
		{
			if (context != null)
			{
				context.dispose();
				context = null;
			}
		}
		disposeAll();
		if (clock != null)
		{
			clock.stop();
			clock = null;
		}

		if (engineLog.isInfoEnabled())
			engineLog.info("Context destroyed");
	}

	public Object getProperty(String key)
	{
		synchronized (properties)
		{
			return properties.get(key);
		}
	}

	/*
	 * Set a property of this context pool. @returns the previous value of the property.
	 */
	public Object setProperty(String key, Object value)
	{
		synchronized (properties)
		{
			Object oldValue = properties.get(key);
			properties.put(key, value);
			return oldValue;
		}
	}

	protected void initEventHandlers()
	{
		Model rootModel = getRepository().getModel(new ModelId(getRootId()), true);
		HashSet<ModelId> scanned = new HashSet<ModelId>();
		eventHandlers = new HashMap<Role, List<ModelId>>();
		scanEventHandlers(rootModel, scanned);
	}

	private void scanEventHandlers(Model rootModel, HashSet<ModelId> scanned)
	{
		if (scanned.contains(rootModel.getId()))
			return;
		scanned.add(rootModel.getId());

		for (ModelElement element : rootModel.getElements())
		{
			if (element.getReferredModel() instanceof FlowModel)
			{
				FlowModel refModel = (FlowModel) element.getReferredModel();
				if (refModel.getType() == FlowType.SYSTEM)
					scanEventHandlers(refModel, scanned);
				else if (refModel.getType() == FlowType.ACTION
						|| refModel.getType() == FlowType.SERVICE
						&& element.getRole().isDistinguished())
				{
					List<ModelId> handlers = eventHandlers.get(element.getRole());
					if (handlers == null)
					{
						handlers = new ArrayList<ModelId>();
						eventHandlers.put(element.getRole(), handlers);
					}
					handlers.add(element.getRefId());
				}

			}

		}

	}

	public List<ModelId> getEventHandlers(Role event)
	{
		List<ModelId> handlers = eventHandlers.get(event);
		if (handlers == null)
			return Collections.emptyList();
		else
			return handlers;
	}

	public void disconnectAll()
	{
		synchronized (pool)
		{
			for (RuntimeContext context : pool)
			{
				context.disconnect();
			}
		}

	}

	private HashSet<ModelId> uiModelIds = null;

	public HashSet<ModelId> getUIModelIds()
	{
		return uiModelIds;
	}

	private HashSet<PackageId> uiPackageIds;

	public HashSet<PackageId> getUIPackageIds()
	{
		return uiPackageIds;
	}

	public boolean isInitDB()
	{
		return initDB;
	}

	public void setInitDB(boolean initDB)
	{
		this.initDB = initDB;
	}

	public boolean isPreloadHandlers()
	{
		return preloadHandlers;
	}

	public void setPreloadHandlers(boolean preLoadHandlers)
	{
		this.preloadHandlers = preLoadHandlers;
	}

	protected void error(String message, Throwable e)
	{
		engineLog.error(message, e);
		String underlyingErrorMessage = e.getMessage();
		if (underlyingErrorMessage == null)
			underlyingErrorMessage = "";
		throw new RuntimeException(this.getClass().getName() + ":" + message + " : "
				+ e.getClass().getName() + " : " + underlyingErrorMessage);
	}

	public void setFixedTimestamp(Date timestamp)
	{
		if (engineLog.isInfoEnabled())
			engineLog.info("Application timestamp set to " + timestamp);
		this.fixedTimestamp = timestamp;
	}

	public void setCheckDB(boolean checkDB)
	{
		this.checkDB = checkDB;
	}

	public byte[] readResource(PackageId refPackageId, String urlString) throws IOException
	{
		byte[] fileContent;
		if (urlString.indexOf(':') < 0)
		{
			fileContent = readApplicationResource(refPackageId, urlString);
		}
		else
		{
			fileContent = FileUtils.readURL(urlString);
		}
		return fileContent;
	}

	public String readResourceAsString(PackageId refPackageId, String urlString) throws IOException
	{
		if (urlString.indexOf(':') < 0)
		{
			return getString(readApplicationResource(refPackageId, urlString));
		}
		else
		{
			return FileUtils.readURLAsString(urlString);
		}
	}

	private String getString(byte[] data)
	{
		return data == null ? null : new String(data);
	}

	/**
	 * @param path
	 * @return
	 */
	public static String condense(String path)
	{
		StringTokenizer tokenizer = new StringTokenizer(path, "/");
		ArrayList<String> list = new ArrayList<String>();
		while (tokenizer.hasMoreElements())
		{
			String segment = tokenizer.nextToken();
			if ("..".equals(segment))
			{
				if (list.isEmpty())
					throw new ModelExecutionException("Illeagal path:" + path);
				else
					list.remove(list.size() - 1);
			}
			else
				list.add(segment);

		}
		return Misc.concatenateList(list, "/");
	}

	/**
	 * @param context
	 * @param urlString
	 * @return
	 */
	public byte[] readApplicationResource(PackageId refPackageId, String path) throws IOException
	{
		if (!path.startsWith("/"))
		{
			String packagePath = refPackageId.getPath();
			path = packagePath + "/" + path;
			path = condense(path);
		}
		return getRepository().getResource(path);
	}

	public void setRepository(Repository repository)
	{
		this.repository = repository;
	}

	public Hierarchy getLogHierarchy()
	{
		return logHierarchy;
	}

	public void remove(RuntimeContext context)
	{
		synchronized (pool)
		{
			if (pool.contains(context))
				pool.remove(context);
			if (usedContexts.contains(context))
				usedContexts.remove(context);
		}

	}

	public Collection<RuntimeContext> getUsedContextsCopy()
	{
		HashSet<RuntimeContext> copy = new HashSet<RuntimeContext>();
		synchronized(pool)
		{
			copy.addAll(usedContexts);
		}
		return copy;
	}
	public Collection<RuntimeContext> getPooledContextsCopy()
	{
		HashSet<RuntimeContext> copy = new HashSet<RuntimeContext>();
		synchronized(pool)
		{
			copy.addAll(pool);
		}
		return copy;
	}

	public ArrayList<RuntimeContext> getPool()
	{
		return pool;
	}

	public void setUsedContexts(HashSet<RuntimeContext> usedContexts)
	{
		this.usedContexts = usedContexts;
	}

	public boolean getCompleteTracing()
	{
		return completeTracing;
	}

	public void setCompleteTracing(boolean traceAll)
	{
		this.completeTracing = traceAll;
	}

	protected void setClassLoader(ClassLoader classLoader)
	{
		this.classLoader = classLoader;
	}

	public ClassLoader getClassLoader()
	{
		return classLoader;
	}

	public Set<ModelId> getPermittedUIModels(PermissionSet permissions)
	{
		synchronized (permittedModelCache)
		{
			Set<ModelId> set = permittedModelCache.get(permissions);
			if (set == null)
			{
				set = getRepository().getUpdatedRepositoryIndex().calculateUIModelIds(
						getRootModel(), permissions.getSet());
				permittedModelCache.put(permissions, set);
			}
			return set;
		}

	}

	public boolean isDebugMode()
	{
		return debugMode;
	}

	public String getAppName()
	{
		return appName;
	}

	public void setAppName(String appName)
	{
		this.appName = appName;
	}

	public File getTraceFile(String filename) throws IOException, FileNotFoundException
	{
		File traceDir = getTraceDir();
		File traceFile = new File(traceDir, filename);
		// Make sure that the specified trace file exists and appears under the
		// trade directory
		// (We musn't allow writing content to arbitrary locations on the file
		// system)
		if (traceFile.getCanonicalFile().getParent().equals(traceDir.getCanonicalFile()))
			throw new IOException("Invalid file " + filename);
		if (!traceFile.exists())
			throw new FileNotFoundException("Missing trace file '" + filename + "'");
		return traceFile;
	}

	public File getDbTimestampFile()
	{
		return dbTimestampFile;
	}

	public void setDbTimestampFile(File dbTimestampFile)
	{
		this.dbTimestampFile = dbTimestampFile;
		lastDBTimestamp = getDbTimestamp();
	}

	private long getDbTimestamp()
	{
		long lastModified = 0;
		if (dbTimestampFile != null && dbTimestampFile.exists())
		{
			lastModified = dbTimestampFile.lastModified();
		}
		return lastModified;
	}

	public String getPermissionsDataSource()
	{
		return permissionsDataSource;
	}

	public void setPermissionsDataSource(String permissionsDataSource)
	{
		this.permissionsDataSource = permissionsDataSource;
	}

	public String getPermissionsQuery()
	{
		return permissionsQuery;
	}

	public void setPermissionsQuery(String permissionsQuery)
	{
		this.permissionsQuery = permissionsQuery;
	}

	public String getPermissionsLastUpdateQuery()
	{
		return permissionsLastUpdateQuery;
	}

	public void setPermissionsLastUpdateQuery(String permissionsLastUpdateQuery)
	{
		this.permissionsLastUpdateQuery = permissionsLastUpdateQuery;
	}

	public Set<String> getAutoCommitDataSources()
	{
		return autoCommitDataSourceSet;
	}

	public void setAutoCommitDataSources(String autoCommitDataSources)
	{
		if (autoCommitDataSources == null)
			autoCommitDataSourceSet = null;
		else
		{
			autoCommitDataSourceSet = new HashSet<String>();
			autoCommitDataSources = autoCommitDataSources.trim();
			for (String ds: autoCommitDataSources.split(" *, *"))
			{
				autoCommitDataSourceSet.add(ds);
			}
		}
	}

	public HashSet<ModelId> getSecureModels()
	{
		return secureModels;
	}

	public boolean useQuotedIdentifiers()
	{
		return useQuotedIdentifiers;
	}
	
	public void useQuotedIdentifiers(boolean b)
	{
		this.useQuotedIdentifiers = b;
	}

	public ConnectionIdHelper getConnectionIdHelper()
	{
		return connectionIdHelper;
	}

	public int getContextLeakCleanupInterval()
	{
		return contextLeakCleanupInterval;
	}

	public void setContextLeakCleanupInterval(int contextLeakCleanupInterval)
	{
		this.contextLeakCleanupInterval = contextLeakCleanupInterval;
	}
	
}
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.sql.SQLException;
import java.util.List;

import tersus.util.Misc;

/**
 * @author Ofer Brandes
 *
 */
public class DatabaseException extends EngineException
{
	private List values;
	private String exceptionType;

    public DatabaseException(RuntimeContext context, String sqlStatement, String exceptionType, SQLException cause)
	{
        
		super("Failed to execute SQL statement '"+sqlStatement+"': ", exceptionType, cause);
		setExceptionType(exceptionType);
	}

    /**
     * @param context
     * @param sql
     * @param values
     * @param exceptionType
     * @param e
     */
    public DatabaseException(RuntimeContext context, String sql, List values, String exceptionType, SQLException e)
    {
        this(context, sql, exceptionType, e);
        this.values = values;
		if (values!=null)
		    setDetails("Exception Type:"+exceptionType+"\nValues:"+Misc.concatenateList(values,","));
    }

 
    /**
     * @return Returns the exceptionType.
     */
    public String getExceptionType()
    {
        return exceptionType;
    }
    /**
     * @param exceptionType The exceptionType to set.
     */
    public void setExceptionType(String exceptionType)
    {
        this.exceptionType = exceptionType;
    }
}

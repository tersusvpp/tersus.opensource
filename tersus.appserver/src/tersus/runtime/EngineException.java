/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;



/**
 * An exception thrown by the runtime engine
 * 
 * @author Youval Bronicki
 *
 */
public class EngineException extends RuntimeException
{

    private static final long serialVersionUID = 1L;
	private String details;
    public String getDetails()
    {
        return details;
    }
    protected void setDetails(String details)
    {
        this.details = details;
    }
	public EngineException(String message, String details, Throwable cause)
	{
		super(message, cause);
		setDetails(details);
	}

    public String toString()
    {
        if (details == null)
            return getMessage();
        else
            return getMessage() + "\nDetails:\n" + details;
    }

	/* (non-Javadoc)
	 * @see java.lang.Throwable#printStackTrace(java.io.PrintStream)
	 */

}

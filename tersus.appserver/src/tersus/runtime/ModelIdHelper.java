package tersus.runtime;

import java.util.Date;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;

public class ModelIdHelper
{
	/**
	 * 
	 * @param value
	 * @return the ModelId of 'value' if 'value' is an instance of a composite data model,
	 *         null otherwise
	 */
	public static ModelId getModelId(Object value)
	{
	    if (value instanceof FlowInstance)
	        return ((FlowInstance)value).getHandler().getModelId();
		ModelId modelId = ModelIdHelper.getCompositeModelId(value);
		if (modelId != null)
				return modelId;
		else
			return getPrimitiveModelId(value);
	}

	
	private static ModelId getPrimitiveModelId(Object value)
	{
		if (value instanceof String)
			return BuiltinModels.TEXT_ID;
		else if (value instanceof Number)
			return BuiltinModels.NUMBER_ID;
		else if (value instanceof Boolean)
			return BuiltinModels.BOOLEAN_ID;
		else if (value instanceof Boolean)
			return BuiltinModels.BOOLEAN_ID;
		else if (value instanceof Date)
			return BuiltinModels.DATE_ID;
		else if (value instanceof BinaryValue)
			return BuiltinModels.BINARY_ID;
		else if (value instanceof Model)
			return BuiltinModels.MODEL_ID;
		else if (value instanceof ModelElement)
			return BuiltinModels.MODEL_ELEMENT_ID;
		else
			return null;
	}


	/**
	 * Returns the ModelId of a composite value, or null if the value is not a composite value;
	 * @param value
	 * @return
	 */
	public static ModelId getCompositeModelId(Object value)
	{
		ModelId modelId = null;
		if (value instanceof Object[])
		{
			Object[] array = (Object[]) value;
			if (array.length > 0)
			{
				if (array[0] instanceof ModelId)
					modelId =  ((ModelId) array[0]);
				
			}
		}
		return modelId;
	}



}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import tersus.model.BuiltinProperties;
import tersus.model.Path;

/**
 * NICE2 [Description]
 * 
 * @author Youval Bronicki
 *
 */
public class SimpleColumnDescriptor extends ColumnDescriptor
{

	/**
	 * @param handler
	 * @param path
	 */
	public SimpleColumnDescriptor(PersistenceHandler handler, Path path)
	{
		super(handler, path);
		setNames();
	}


}

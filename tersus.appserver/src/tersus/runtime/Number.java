/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

/**
 * A Mutuable wrapper around <code>Double</code>
 * 
 * @author Youval Bronicki
 *
 */
public class Number
{

    /**
	 * @param value
	 */
	public Number(double value)
	{
		this.value = value;
	}

	/**
	 * The <code>Double</code> value of this <code>Number</code>
	 */
	public double value;

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj)
	{
		if (obj instanceof Number)
		{
			return value == ((Number) obj).value;
		}
		else
			return false;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		if (value == (int) value)
			return String.valueOf((int) value);
		return String.valueOf(value);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode()
	{
		long bits = Double.doubleToLongBits(value);
		return (int) (bits ^ (bits >>> 32));
	}

    public java.lang.Number getNumber()
    {
        return Double.valueOf(value);
    }
}

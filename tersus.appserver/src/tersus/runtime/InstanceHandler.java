/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/

package tersus.runtime;

import java.util.Date;
import java.util.List;
import java.util.Map;

import tersus.model.BuiltinModels;
import tersus.model.BuiltinProperties;
import tersus.model.ElementPath;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.Role;
import tersus.util.Enum;
import tersus.util.Misc;
import tersus.util.NotImplementedException;

/**
 * An object that handles instances according to the logic defined in the model.
 * <br/>InstanceHandlers consistute the skeleton of the runtime engine.
 * Handlers control the complete lifecycle of runtime instances, from creation
 * to destruction.
 * 
 * In addition to their responsiblity to manage instances, InstanceHandlers also
 * have the responsibility to initialize themselves from the associated models
 * (see <code>initializeFromModel()</code>
 * 
 * 
 * <br/>InstanceHandlers, together with the <code>ElementHandlers</code> they
 * contain, fully encapsulate the structure of the instances they manage. (e.g.
 * The initial design decision of representing data values by simple arrays is
 * the 'internal business' of DataHandlers and DataElements.
 * 
 * <br/>FUNC3 Interface for DataHandlers and DataElements? It is yet to be
 * defined if and how plug-ins are to access composite data structures,
 * eventually, plug-ins may need access to the data handler hierarchy, in which
 * case we will probably need to extract a public inrterface for data handlers
 * and data elements.
 * 
 * 
 * InstanceHandler is an abstract base class for all instance handlers, and
 * implements the basic capability of representing a composite structure
 * (although leaf data models are not composite structures, they still inherit
 * the composite characteristics of InstanceHandlers). Note that atomic actions
 * are not really atomic, as they usually contain slots.
 * 
 * @author Youval Bronicki
 *  
 */
public abstract class InstanceHandler
{

    private static final float DEFAULT_CACHE_MAX_AGE = 3600;

	protected ModelId modelId;

    protected ModelLoader loader;

    protected int nElements;

    private Model model;

    public ElementHandler[] elementHandlers;

    private String plugin;

    public abstract Object newInstance(RuntimeContext context);

    /**
     * @return The id of this handler's model
     */
    public ModelId getModelId()
    {
        return modelId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IInstanceHandler#setLoader(tersus.runtime.ModelLoader)
     */
    public void setLoader(ModelLoader loader)
    {
        this.loader = loader;
    }

    /**
     * Retrieves an ElmentHandler by role
     * 
     * @param role
     *            The <code>Role</code> of the required elements
     * @return An ElementHandler with the specified role, or null if there is no
     *         such element handler
     */
    public ElementHandler getElementHandler(Role role)
    {
        //OPT Keep and use a hash table (e.g. to make xml parsing more
        // efficient) ?
        for (int i = 0; i < elementHandlers.length; i++)
        {
            ElementHandler elementHandler = elementHandlers[i];
            if (elementHandler.getRole() == role)
                return elementHandler;
        }
        return null;

    }

    /**
     * Retrieves an ElmentHandler by role or role suffix
     * 
     * @param roleSuffix
     *            The <code>Role</code> of the required element (or, if "
     *            <...>", the suffix of the Role)
     * @return An ElementHandler with the specified role (or role suffix), or
     *         null if there is no such element handler (or more than one)
     */
    public ElementHandler getDistinguishedElementHandler(Role roleSuffix)
    {
        String roleSuffixStr = roleSuffix.toString();
        if (!roleSuffixStr.startsWith("<") || !roleSuffixStr.endsWith(">"))
            return getElementHandler(roleSuffix); // Exact match is expected

        ElementHandler distinguishedHandler = null;
        for (int i = 0; i < elementHandlers.length; i++)
        {
            ElementHandler elementHandler = elementHandlers[i];
            String elementRole = elementHandler.getRole().toString();
            if (elementRole.endsWith(roleSuffixStr))
                if (elementRole.indexOf('<') == elementRole.length()
                        - roleSuffixStr.length()) // Else "<X<Y>" is considered
                                                  // a match for "<Y>"
                    if (distinguishedHandler == null)
                        distinguishedHandler = elementHandler;
                    else
                        return null;
        }
        return distinguishedHandler;
    }

    /**
     * Retrieves an ElmentHandler by role
     * 
     * @param roleStr
     *            A <code>String</code> that contains the role of the required
     *            element ((or its suffix if " <...>")
     * @return An ElementHandler with the specified role (or role suffix), or
     *         null if there is no such element handler (or there are more than
     *         one)
     */
    public ElementHandler getElementHandler(String roleStr)
    {
        //OPT Keep and use a hash table (e.g. to make xml parsing more
        // efficient) ?
        for (int i = 0; i < elementHandlers.length; i++)
        {
            ElementHandler elementHandler = elementHandlers[i];
            if (elementHandler.getRole().toString().equals(roleStr))
                return elementHandler;
        }
        return null;
    }

    /**
     * Retrieves an ElmentHandler by role or role suffix
     * 
     * @param roleSuffixStr
     *            A <code>String</code> that contains the role of the required
     *            element (or, if " <...>", the suffix of the Role)
     * @return An ElementHandler with the specified role (or role suffix), or
     *         null if there is no such element handler (or more than one)
     */
    public ElementHandler getDistinguishedElementHandler(String roleSuffixStr)
    {
        if (!roleSuffixStr.startsWith("<") || !roleSuffixStr.endsWith(">"))
            return getElementHandler(roleSuffixStr); // Exact match is expected

        ElementHandler distinguishedHandler = null;
        for (int i = 0; i < elementHandlers.length; i++)
        {
            ElementHandler elementHandler = elementHandlers[i];
            if (elementHandler.getRole().toString().endsWith(roleSuffixStr))
                if (distinguishedHandler == null)
                    distinguishedHandler = elementHandler;
                else
                    return null;
        }
        return distinguishedHandler;
    }

    /**
     * Initializes this <code>InstanceHandler</code> form its model.
     * 
     * The base implementation stores the id of the model, and creates an array
     * of element handlers. Sub-classes typically extend this method (calling
     * the base implementation and then adding additional logic).
     * 
     * Sub-classes control the creation of element handlers by implementubg
     * <code>createElementHandler(MdoelElement)</code>
     * 
     * @param model
     *            The <code>Model</code> that is 'implemented' by this
     *            instance handler.
     */
    public void initializeFromModel(Model model)
    {
        modelId = model.getId();
        setPlugin(model.getPlugin());
        if ("true".equals(model.getProperty(BuiltinProperties.CACHED)))
        {
        	setCacheMaxAgeSeconds(DEFAULT_CACHE_MAX_AGE);
        }
        String maxAgeStr = (String)model.getProperty(BuiltinProperties.CACHE_MAX_AGE_SECONDS);
        if (maxAgeStr != null)
        	setCacheMaxAgeSeconds(Float.parseFloat(maxAgeStr));
        setModel(model);
        List<?> modelElements = model.getElements();
        nElements = modelElements.size();
        elementHandlers = new ElementHandler[nElements];
        int count = 0;
        for (int i = 0; i < nElements; i++)
        {
            ModelElement element = (ModelElement) modelElements.get(i);
            ElementHandler handler = createElementHandler(element);
            if (handler != null)
            {
                handler.setIndex(count);
                handler.setLoader(loader);
                elementHandlers[count] = handler;
                handler.initializeFromElement(element);
                ++count;
            }
        }
        if (count < nElements)
        {
            ElementHandler[] tmp = elementHandlers;
            elementHandlers = new ElementHandler[count];
            System.arraycopy(tmp, 0, elementHandlers, 0, count);
            nElements = count;
        }
        Object pluginVersionStr = model.getProperty(BuiltinProperties.PLUGIN_VERSION);
        if (pluginVersionStr != null)
        {
            pluginVersion = Integer.parseInt(pluginVersionStr.toString());
        }

    }

    /**
     * Creates an ElementHandler of the type appropriate for a given
     * ModelElement
     * 
     * @param element
     *            The ModelElement which
     * @return a new ElementHandler
     */
    protected ElementHandler createElementHandler(ModelElement element)
    {
        //NICE2 make this method and ElementHandler abstract. Implement in
        // DataHandler
        return new ElementHandler(this);
    }

    /**
     * @return the type of object represented by this handler. The type can be a
     *         FlowType, a Composition, or a SlotType
     */
    public abstract Enum getType();

    /**
     * @return the element handler obtain by following a given path (starting at
     *         a given offset)
     */
    public ElementHandler getElementHandler(ElementPath path, int offset)
    {
        ElementHandler elementHandler = getElementHandler(path
                .getSegment(offset));
        if (offset == path.getNumberOfSegments() - 1)// TODO Review this
                                                     // auto-generated method
                                                     // stub
            return elementHandler;
        else
            return elementHandler.getChildInstanceHandler().getElementHandler(
                    path, offset + 1);
    }

    public ElementHandler getElementHandler(ElementPath path)
    {
        return getElementHandler(path, 0);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    public String toString()
    {
        String className = Misc.getShortName(getClass());
        return className + "[" + modelId + "]";
    }

    /**
     * @return
     */
    public String getName()
    {
        return modelId.getName();
    }

    /**
     * @return
     */
    public boolean isPersistent()
    {
        return false;
    }

    public String getReferenceString(RuntimeContext context, Object instance)
    {
        throw new IllegalArgumentException(
                "Invalid call to getReferenceString(): " + getModelId()
                        + " is not persistent");
    }

    /**
     * @return
     */
    public PersistenceHandler getPeristenceHandler()
    {
        return null;
    }

    /**
     * @return true if this is a constant data type
     */
    public boolean isConstant()
    {
        return false;
    }

    /**
     * @return
     */
    public ModelLoader getLoader()
    {
        return loader;
    }

    public void initDB(RuntimeContext context)
    {
    }

    /**
     * @param context
     * @param valueStr
     * @param forceDefaultFormat indicates that a specific format should be ignored
     * @return
     */
    public Object newInstance(RuntimeContext context, String valueStr, boolean forceDefaultFormat)
    {
        throw new IllegalArgumentException("Can't create an instance of "
                + this + " from a string");
    }

    /**
     * @return
     */
    public Model getModel()
    {
        return model;
    }

    /**
     * @param model
     */
    public void setModel(Model model)
    {
        this.model = model;
    }

    public void setPlugin(String plugin)
    {
        this.plugin = plugin;
    }

    public String getPlugin()
    {
        return plugin;
    }

    protected float cacheMaxAgeSeconds = 0;

	private int pluginVersion = 0;

    /**
     * @return
     */
    public boolean isCached()
    {
        return cacheMaxAgeSeconds > 0;
    }
 

    protected int compareNull(Object obj1, Object obj2)
    {
        if (obj1 == null && obj2 == null)
            return 0;
        else if (obj1 == null)
            return -1;
        else if (obj2 == null)
            return 1;
        else
            throw new IllegalArgumentException(
                    "One of the arguments must be null");

    }

    public abstract int compare(RuntimeContext context, Object obj1, Object obj2);

    /**
     * @return true if obj is an instance of this handler's data type
     */
    public abstract boolean isInstance(Object obj);

    protected int compareComposite(RuntimeContext context, Object obj1,
            Object obj2)
    {
        if (obj1 == null || obj2 == null)
            return compareNull(obj1, obj2);
        if (!isInstance(obj1))
            throw new IllegalArgumentException("Argumet 1 is not of type '"
                    + getModelId() + "'");
        if (!isInstance(obj2))
            throw new IllegalArgumentException("Argumet 2 is not of type '"
                    + getModelId() + "'");
        for (int i = 0; i < elementHandlers.length; i++)
        {
            ElementHandler e = elementHandlers[i];
            if (e.isRepetitive())
            {
                List<?> list1 = (List<?>)e.get(context, obj1);
                List<?> list2 = (List<?>)e.get(context, obj2);
                int commonSize = (int)Math.min(list1.size(), list2.size());
                for (int j=0; j<commonSize;j++)
                {
                    Object item1 = list1.get(j);
                    Object item2 = list2.get(j);
                    int comparison = e.getChildInstanceHandler().compare(context, item1, item2);
                    if (comparison != 0)
                        return comparison;
                }
                return list1.size() - list2.size();
            }
            else
            {
                Object v1 = e.get(context, obj1);
                Object v2 = e.get(context, obj2);
                int comparison = e.getChildInstanceHandler().compare(context,
                        v1, v2);
                if (comparison != 0)
                    return comparison;
            }
        }
        return 0;
    }
    public Object deepCopy(RuntimeContext context, Object original)
    {
        Object copy = newInstance(context);
        for (int i=0; i<elementHandlers.length; i++)
        {
            ElementHandler e = elementHandlers[i];
            Object v = e.get(context, original);
            if (v == null)
            	continue;
            if (e.isRepetitive())
                throw new NotImplementedException("Deep copy of repetitive element not implemented yet");
            Object c = e.getChildInstanceHandler().deepCopy(context, v);
            e.set(context, copy, c);
        }
        return copy;
    }

    protected void notifyInvalidModel(ElementPath path, String problemType, String problemDetails)
    {
    	Model.notifyInvalidModel (this.getModelId(), path, problemType, problemDetails);
    }

    public static String getObjectTypeStr(Object value)
    {
        if (value == null)
            return "null";
        ModelId modelId = ModelIdHelper.getCompositeModelId(value);
        if (modelId != null)
            return modelId.toString();
        else
            return value.getClass().getName();
    }

    public String checkDB(RuntimeContext context)
    {
        return null;
    }

	public int getPluginVersion()
	{
	   return pluginVersion;
	}
	/**
	 * 
	 * @param value
	 * @return the ModelId of 'value' 
	 * @note if value is a leaf data value, the model id is inferred from the Java object class
	 */
	public static ModelId getModelIdEx(Object value)
	{
		if (value instanceof FlowInstance)
			return ((FlowInstance)value).getHandler().getModelId();
		ModelId modelId  = ModelIdHelper.getCompositeModelId(value);
		if (modelId != null)
				return modelId;
		else if (value instanceof String)
			return BuiltinModels.TEXT_ID;
		else if (value instanceof Number)
			return BuiltinModels.NUMBER_ID;
		else if (value instanceof Boolean)
			return BuiltinModels.BOOLEAN_ID;
		else if (value instanceof Boolean)
			return BuiltinModels.BOOLEAN_ID;
		else if (value instanceof Date)
			return BuiltinModels.DATE_ID;
		else if (value instanceof BinaryValue)
			return BuiltinModels.BINARY_ID;
		else if (value instanceof Model)
			return BuiltinModels.MODEL_ID;
		else if (value instanceof ModelElement)
			return BuiltinModels.MODEL_ELEMENT_ID;
		else
			return null;
	}

    public Object newInstance(RuntimeContext context, String valueStr, Map<String, Object> properties, boolean forceDefaultFormat)
    {
        return newInstance(context, valueStr, forceDefaultFormat);
    }

    public Object padValue(Object fieldValue, int columnSize)
    {
        return fieldValue;
    }

	public boolean isSecret()
	{
		return false;
	}

	protected float getCacheMaxAgeSeconds()
	{
		return cacheMaxAgeSeconds;
	}

	protected void setCacheMaxAgeSeconds(float cacheMaxAgeSeconds)
	{
		this.cacheMaxAgeSeconds = cacheMaxAgeSeconds;
	}


}
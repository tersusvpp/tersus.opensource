/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.util.ArrayList;

import tersus.model.ModelElement;

/**
 * An ElementHandler for elements of a flow.
 * <br>
 * FlowElementHandlers enhance ElementHandler in the following ways:
 * 
 * - Adding the methods invoke() invokeLinks() that are part of flow execution.
 * - Implementing access/modification of the elements of FlowState instances
 * 
 * 
 * @author Youval Bronicki
 *
 */
public abstract class FlowElementHandler extends ElementHandler
{

	public FlowElementHandler(FlowHandler parentHandler)
	{
		super(parentHandler);
	}
	/**
	 * Indicates whether this element is the direct or indirect target of any of its parent's links 
	 */
	public boolean isLinkTarget = false;
	protected FlowHandler parentFlowHandler;
	ArrayList outgoingLinks = new ArrayList();
	private int dependencyRank;
	/**
	 * 'Invokes' this element starting/resuming the element if the element is a sub-flow
	 * and invoking outgoing links as necessary. 
	 * @param element
	 */
	public abstract void invoke(RuntimeContext context, IFlowElement element);

	/**
	 * Invoke the links originating in this element
	 * @param element
	 */
	public void invokeLinks(RuntimeContext context, IFlowElement element)
	{
		for (int i = 0; i < outgoingLinks.size(); i++)
		{
			LinkHandler handler = (LinkHandler) outgoingLinks.get(i);
			handler.invoke(context, element);
		}

	}

	public Object getValue(RuntimeContext context, Object parent)
	{
		if (parent == null)
			return null;
		Object[] parentArray = ((FlowInstance) parent).elements;
		return parentArray[getIndex()];
	}

	public void setValue(RuntimeContext context, Object parent, Object value)
	{
		Object[] parentArray = ((FlowInstance) parent).elements;
		parentArray[getIndex()] = value;
	}

	/**
	 * @return
	 */
	public int getDependencyRank()
	{
		return dependencyRank;
	}

	/**
	 * @param i
	 */
	public void setDependencyRank(int i)
	{
		dependencyRank = i;
	}
    public void initializeFromElement(ModelElement element)
    {
        super.initializeFromElement(element);
        setDependencyRank(element.getDependencyRank());
		setSelfReady(element.isSelfReady());

    }
    private boolean selfReady;
    protected boolean isSelfReady()
    {
        return selfReady;
    }

    private void setSelfReady(boolean selfReady)
    {
        this.selfReady = selfReady;
    }
}

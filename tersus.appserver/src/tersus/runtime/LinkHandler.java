/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.util.List;

import tersus.model.InvalidModelException;
import tersus.model.Link;
import tersus.model.LinkOperation;
import tersus.model.ModelElement;
import tersus.model.Path;
import tersus.model.Role;
import tersus.model.validation.Problems;
import tersus.runtime.trace.EventType;
import tersus.runtime.trace.ObjectType;

/**
 * Handles a link
 * 
 * @author Youval Bronicki
 *
 */
public class LinkHandler extends FlowElementHandler
{
	private FlowElementHandler targetElementHandler;
	private boolean constantTrigger;
	private LinkOperation operation;
	private Path traceValueRelativePath;
	PathHandler sourcePath, targetPath;
	/**
	 * A reference to the link is kept initializeFromModel()
	 * and cleared in attachToElement().
	 */
	private Link link;
	/**
	 * The SubFlowHandler that contains the links target slot (or null if the target is not a slot of a sub-flow)
	 */
	private FlowElementHandler sourceElementHandler;
    private InstanceHandler targetType;
	/**
	 * @param parentHandler
	 */
	public LinkHandler(FlowHandler parentHandler)
	{
		super(parentHandler);
	}

	/**
	 * 'Invoke' the link, retrieving the source object/s and performing the specified 
	 * operation (set/accumulate/enrich/update/...) on the target
	 * @param context A RuntimeContext for the operation
	 * @param element the instance of the element at the source of the link
	 *    (If the source of the link is a slot of the flow, the source instance
	 * is the slot's value.  If the source of the link is an exit of a sub-flow, the source
	 * instance is the sub-flow instance.  If the source of the link is a data element,
	 * the source instance is the instance of the flow data element)
	 */
	public void invoke(RuntimeContext context, IFlowElement sourceInstance)
	{
		//FUNC2 Handle update/enrich/accumulate
		//FUNC2 Duplicate the value if required
		//FUNC3 Translate (cast) the value if required 
		context.throwIfAborted();
		Object value = sourcePath.get(context, sourceInstance.getValueObject());
		if (value == null)
			return;
		if (value instanceof List) //NICE3 Find a better way to know that the source is repetitive
		{
			List values = (List) value;
			if (context.trace.traceLinks && values.size() > 0)
			{
				context.trace.add(EventType.ACTIVATED, ObjectType.FLOW, getRole(), values.size() + (values.size() ==1?" value":" values"));
			}
			for (int i = 0; i < values.size(); i++)
			{
				Object v = values.get(i);
				handleValue(context, sourceInstance, v);
			}
		}
		else
		{
			if (context.trace.traceLinks)
			    context.trace.add(EventType.ACTIVATED, ObjectType.FLOW, getRole(), null);
		    handleValue(context, sourceInstance, value);
		}
	}
	private void handleValue(RuntimeContext context, IFlowElement sourceInstance, Object value)
	{
		FlowInstance parent = sourceInstance.getParent();
		if (! constantTrigger && !(operation == LinkOperation.REMOVE))
		{
		    if (targetType instanceof NothingHandler)
		        value = Nothing.NOTHING;
		    else if (targetType != null && !targetType.isInstance(value))
		    {
		        Object type = ModelIdHelper.getCompositeModelId(value);
		        if (type == null)
		        	type = value.getClass().getName();
		        throw new ModelExecutionException("Incompatible type on \""+this.getRole()+"\" in \""+this.parentFlowHandler.getName()+"\". Expected "+targetType.getModelId()+" but found "+type);
		    }
		}
		if (constantTrigger)
		{
			value = targetElementHandler.getChildInstanceHandler().newInstance(context);
		}
		if (operation == LinkOperation.REMOVE)
		{
			targetPath.remove(context, parent);
			if (context.trace.traceSet)
			{
				ElementHandler lastSegment = targetPath.getLastSegment();
				context.trace.add(EventType.REMOVED, lastSegment.getType(), traceValueRelativePath, null, targetPath.path);
			}

		}
			
		else if (targetPath.getLastSegment().isRepetitive())
		{
			targetPath.accumulate(context, parent, value);
			if (context.trace.traceSet)
			{
				ElementHandler lastSegment = targetPath.getLastSegment();
				context.trace.addDetailValue(sourcePath.getValueHandler(), value);
				context.trace.add(EventType.ACCUMULATED, lastSegment.getType(), traceValueRelativePath, null, targetPath.path);
			}
		}
		else
		{
			targetPath.set(context, parent, value);
			if (context.trace.traceSet)
			{
				ElementHandler lastSegment = targetPath.getLastSegment();
				context.trace.addDetailValue(lastSegment.getChildInstanceHandler(), value);
				context.trace.add(EventType.SET, lastSegment.getType(), traceValueRelativePath, null, targetPath.path);
			}
		}

	}

	/* (non-Javadoc)
	 * @see tersus.runtime.ElementHandler#initializeFromElement(tersus.model.ModelElement)
	 */
	public void initializeFromElement(ModelElement element)
	{
		super.initializeFromElement(element);
		link = (Link) element;
		operation = link.getOperation();
		constantTrigger = link.isConstantTrigger();
		traceValueRelativePath = new Path();
		traceValueRelativePath.addSegment(element.getRole());
		traceValueRelativePath.addSegment(Role.get("value"));
	}

	/**
	 * Prepares this LinkHanler for invocation, by attaching it to its source element handler,
	 * and initializing source and target <code>PathHandler</code>s.
	 */
	public void prepare()
	{
		Role sourceRole = link.getSource().getSegment(0);
		sourceElementHandler = (FlowElementHandler) parentFlowHandler.getElementHandler(sourceRole);

		if (sourceElementHandler == null)
		{
			parentHandler.notifyInvalidModel(this.role, Problems.INVALID_FLOW, "The source is not valid");
			return;
		}
		Role targetRole = link.getTarget().getSegment(0);
		targetElementHandler = (FlowElementHandler) parentFlowHandler.getElementHandler(targetRole);
		if (targetElementHandler == null)
		{
			parentHandler.notifyInvalidModel(this.role, Problems.INVALID_FLOW, "The target is not valid");
			return;
		}
		
		targetElementHandler.isLinkTarget = true;

		try
		{
			sourcePath =
				new PathHandler(sourceElementHandler, link.getSource().getTail(link.getSource().getNumberOfSegments() - 1));
			targetPath = new PathHandler(parentFlowHandler, link.getTarget());
		}
		catch (InvalidPathException e)
		{
			throw new InvalidModelException(parentFlowHandler.getModelId(), this.getRole(), Problems.INVALID_FLOW, e.getMessage());
		}
		link = null; // Drop the reference to allow garbage collection of models
		sourceElementHandler.outgoingLinks.add(this);
		targetType = targetPath.getLastSegment().getChildInstanceHandler();


	}
	/**
	 * @return
	 */
	public FlowElementHandler getSourceElementHandler()
	{
		return sourceElementHandler;
	}

	/**
	 * @return
	 */
	public FlowElementHandler getTargetElementHandler()
	{
		return targetElementHandler;
	}


    /**
     * @return Returns the targetSubFlow.
     */
    private SubFlowHandler getTargetSubFlow()
    {
        if (targetElementHandler instanceof SubFlowHandler)
            return (SubFlowHandler)targetElementHandler;
        else 
            return null;
    }

    public PathHandler getTargetPath()
    {
        return targetPath;
    }
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import tersus.runtime.trace.ObjectType;
import tersus.util.Enum;

/**
 * Represents a data value held by a Flow (occupying a slot or 
 * or FlowDataElement)
 * 
 * FlowDataInstances encapsulate a value, enhacing it with context
 * information (the elementHandler and the parent flow instance)
 * 
 * @author Youval Bronicki
 *
 */
public class FlowDataInstance implements IFlowElement
{

	FlowElementHandler elementHandler;
	IFlowElement nextReadySibling;
	Object value;
	FlowInstance parent;
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowElement#getElementHandler()
	 */
	public FlowElementHandler getElementHandler()
	{
		return elementHandler;
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowElement#getValueObject()
	 */
	public Object getValueObject()
	{
		return value;
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowElement#getParent()
	 */
	public FlowInstance getParent()
	{
		return parent;
	}

	/**
	 * Initializes this FlowDataInstance
	 * @param parent - the parent FlowState
	 * @param elementHandler - the element's handler
	 * @param value - the encapsulated value 
	 */
	public void init(FlowInstance parent, FlowElementHandler elementHandler, Object value)
	{
		this.parent = parent;
		this.elementHandler = elementHandler;
		this.value = value;
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowElement#getNextReadySibling()
	 */
	public IFlowElement getNextReadySibling()
	{
		return nextReadySibling;
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowElement#setNextReadySibling(tersus.runtime.IFlowElement)
	 */
	public void setNextReadySibling(IFlowElement next)
	{
		nextReadySibling = next;
		
	}

    /* (non-Javadoc)
     * @see tersus.runtime.IFlowElement#getType()
     */
    public Enum getType()
    {
        return elementHandler.getType();
    }
}

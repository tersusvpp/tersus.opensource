/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.util.Map;

import org.apache.commons.codec.binary.Base64;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Role;
import tersus.model.SlotType;
import tersus.model.validation.Problems;
import tersus.runtime.sapi.SAPIManager;

/**
 * @author Youval Bronicki
 * 
 * A base class for plugins (handles tracing of start/finish
 *
 */
public abstract class Plugin extends FlowHandler
{
    public void doStart(RuntimeContext context, FlowInstance flow)
	{
		traceStarted(context, flow);
		try
		{
			super.doStart(context, flow);
			chargeDoneExit(context, flow);
		}
		catch (RuntimeException e)
		{
			fireError(context, flow, e);
		}
		traceFinish(context, flow);
	}
    
    public Object getTriggerValue(RuntimeContext context, FlowInstance flow, SlotHandler trigger)
    {
        if (trigger == null)
            return null;
        else
            return trigger.get(context,flow);
    }
    protected boolean getTriggerValue(SlotHandler trigger, RuntimeContext context, FlowInstance flow, boolean defaultValue)
	{
		boolean res = defaultValue;
		Boolean v = (Boolean)getTriggerValue(context, flow, trigger);
		if (v != null)
			res = v.booleanValue();
		return res;
	}
    public int getTriggerValue(SlotHandler trigger, RuntimeContext context, FlowInstance flow, int defaultValue)
    {
        int res = defaultValue;
        
        tersus.runtime.Number n = (tersus.runtime.Number)getTriggerValue(context, flow, trigger);
        if (n != null)
            res = (int)n.value;        
        return res;
    }
    public String getTriggerValue(SlotHandler trigger, RuntimeContext context, FlowInstance flow, String defaultValue)
    {
        String res = defaultValue;
        
        String s = (String)getTriggerValue(context, flow, trigger);
        if (s != null)
            res = s;        
        return res;
    }
    public String handlePluginRequest(ContextPool pool, Map<String,String> request)
    {
        return null;
    }
    
    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
    }
    
    public void setExitValue(RuntimeContext context, FlowInstance flow, String elementName, Object value)
    {
        SlotHandler exit = getExit(Role.get(elementName));
        if (exit == null)
            throw new ModelExecutionException("Missing exit '"+elementName+"' in "+getModelId());
        Object internalValue = SAPIManager.toInstance(exit.getChildInstanceHandler(), value);
        if (exit.isRepetitive())
            accumulateExitValue(exit, context, flow, internalValue);
        else
            setExitValue(exit, context, flow, internalValue);
    }

	public void start(RuntimeContext context, FlowInstance flow)
    {
    }

	protected void validateType(SlotHandler slotHandler, ModelId id1, ModelId id2)
	{
		if (slotHandler == null)
			return;
		ModelId refId = slotHandler.getChildModelId();
		if (id1.equals(refId) || id2.equals(refId))
			return; // OK
		notifyInvalidModel(slotHandler.getRole(), slotHandler.getType() == SlotType.EXIT ? Problems.INVALID_EXIT : Problems.INVALID_TRIGGER, "Type must be "+id1.getName() + " or "+ id2.getName());
	}

	protected void setExitValueWithBase64Conversion(SlotHandler exit, RuntimeContext context, FlowInstance flow,
			byte[] value)
	{
		if (exit == null)
			return;
		if (exit.getChildInstanceHandler() instanceof TextHandler)
		{
			setExitValue(exit, context, flow, new String(Base64.encodeBase64(value)));
		}
		else
		{
			setExitValue(exit, context, flow, value);
		}
	
	}

	protected SlotHandler getMandatoryTextOrSecretTextTrigger(Role role)
	{
		SlotHandler trigger =  getRequiredMandatoryTrigger(role, Boolean.FALSE, null);
		return getTextOrSecretTextTrigger(role, trigger);
	}

	protected SlotHandler getNonRequiredTextOrSecretTextTrigger(Role role)
	{
		SlotHandler trigger =  getNonRequiredTrigger(role, Boolean.FALSE, null);
		return getTextOrSecretTextTrigger(role, trigger);
	}

	private SlotHandler getTextOrSecretTextTrigger(Role role, SlotHandler trigger)
	{
		if (trigger != null)
		{
			ModelId typeId = trigger.getChildModelId();
			if (!BuiltinModels.TEXT_ID.equals(typeId) && !BuiltinModels.SECRET_TEXT_ID.equals(typeId))
				notifyInvalidModel(role, Problems.INVALID_TRIGGER, "Data type should be textual, but is "+typeId);
		}
		return trigger;
	}
}

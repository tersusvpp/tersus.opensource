/************************************************************************************************
 * Copyright (c) 2003-2007 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
package tersus.runtime;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import tersus.webapp.Engine;

public class DefaultResourceHandler implements IResourceHandler
{

	public void connect(RuntimeContext context, String dataSourceName)
	{
		if (dataSourceName == null)
			dataSourceName = Engine.MAIN_DATA_SOURCE;
		context.setDefaultDataSourceName(dataSourceName);
		context.setPersistenceManager(new PersistenceManager(context));
	}

	/**
	 * @param connection
	 */
	public void beginTransaction(RuntimeContext context)
	{
		/**
		 * Nothing to do: any connection that are supposed to be part of the transaction will
		 * already have autoCommit=false;
		 */

	}

	public void commitTransaction(RuntimeContext context)
	{
		Logger log = context.getContextPool().connectionLog;
		for (Connection connection : context.getAllConnections())
		{
			try
			{
				if (context.getDBAdapter(connection).commit(connection, context))
					if (log.isInfoEnabled())
						log.info("Changes comitted : "
								+ context.getConnectionIdHelper().toString(connection));
			}
			catch (SQLException e)
			{
				throw new EngineException("Failed to commit changes to "
						+ context.getConnectionIdHelper().toString(connection), e.getMessage(), e);
			}
		}

	}

	public void rollbackTransaction(RuntimeContext context)
	{
		Logger log = context.getContextPool().connectionLog;
		for (Connection connection : context.getAllConnections())
		{
			try
			{

				if (context.getDBAdapter(connection).rollback(connection, context))
					if (log.isInfoEnabled())
						log.info("Changes rolled back : "
								+ context.getConnectionIdHelper().toString(connection));
			}
			catch (SQLException e)
			{
				throw new EngineException("Failed to rollback changes", e.getMessage(), e);
			}
		}
	}

}

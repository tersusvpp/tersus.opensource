package tersus.runtime;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

public class ObjectHandler extends LeafDataHandler
{

	@Override
	public Object fromSQL(ResultSet rs, int index) throws SQLException
	{
		int type = rs.getMetaData().getColumnType(index);
		if (type == Types.DATE)
		{
			java.sql.Date sqlDate = rs.getDate(index);
			if (sqlDate == null)
				return null;
			return new java.util.Date(sqlDate.getTime());
		}
		if (type == Types.TIMESTAMP)
		{
			java.sql.Timestamp sqlTimestamp = rs.getTimestamp(index);
			if (sqlTimestamp == null)
				return null;
			return new java.util.Date(sqlTimestamp.getTime());
		
		}
        Object value = rs.getObject(index);
		return fromSQL(value);
	}

	@Override
	protected Object fromSQL(Object obj)
	{
		if (obj instanceof java.lang.Number)
			return new tersus.runtime.Number(((java.lang.Number)obj).doubleValue());
		if (obj instanceof java.sql.Timestamp)
			return new java.util.Date(((java.sql.Timestamp)obj).getTime());
		if (obj instanceof java.sql.Date)
			return new java.util.Date(((java.sql.Date)obj).getTime());
		return obj;
	}

	@Override
	public int getSQLType()
	{
		return Types.OTHER;
	}

	@Override
	public Object toSQL(Object value)
	{
		if (value instanceof tersus.runtime.Number)
			return Double.valueOf(((tersus.runtime.Number)value).value);
		if (value instanceof java.util.Date)
	        return new java.sql.Date(((java.util.Date) value).getTime());     
		return value;
	}

	@Override
	public boolean isInstance(Object obj)
	{
		// TODO Auto-generated method stub
		return false;
	}

}

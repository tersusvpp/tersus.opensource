/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import tersus.model.ModelId;
import tersus.util.Misc;

/**
 * An object that manages the persistence of flow instances.
 * 
 * @author Youval Bronicki
 *
 */
public class PersistenceManager
{

	/**
	 * Default size for String columns
	 */
	private RuntimeContext context;
	/** Creates a new PersistenceManager for the given context
	 * 
	 * @param context The <code>RuntimeContext</code> of the new <code>PersistenceManager</code>
	 */
	public PersistenceManager(RuntimeContext context)
	{
		this.context = context;
	}

	static StringBuffer getColumnName(String name)
	{
		StringBuffer translated = new StringBuffer();
		for (int i = 0; i < name.length(); i++)
		{
			char c = name.charAt(i);
			if (Character.isLetterOrDigit(c))
			{
				translated.append(c);
				if (Character.isLowerCase(c)
					&& i < name.length() - 1
					&& Character.isUpperCase(name.charAt(i + 1)))
					translated.append('_');
			}
			//				name.setCharAt(i, Character.toUpperCase(c));
			else
				translated.append('_');
		}

		return translated;
	}


	/**
	 * Drops the tables in the database  
	 * NICE2 - remove and user dropTables(root)
	 * @param rootHandler
	 * @param statement
	 */
	public List dropAllTables()
	{
		List tables = new ArrayList();

		ResultSet tablesResultSet = null;
		Statement statement = null;
		try
		{
			DatabaseMetaData metadata = context.getDefaultConnection().getMetaData();
			statement = context.getDefaultConnection().createStatement();
			tablesResultSet = metadata.getTables(null, null, null, null);
			while (tablesResultSet.next())
			{
				String tableType = tablesResultSet.getString("TABLE_TYPE");
				if ("TABLE".equals(tableType))
				{
					String tableName = tablesResultSet.getString("TABLE_NAME");
					tables.add(tableName);
					context.getEngineLog().info("Dropping table " + tableName);
					try
					{
						statement.executeUpdate("DROP TABLE " + tableName);
					}
					catch (SQLException e)
					{
						throw new EngineException("Failed to drop table " + tableName, null, e);
					}
				}
			}
			statement.close();
			statement = null;
			tablesResultSet.close();
			tablesResultSet = null;
			return tables;
		}
		catch (SQLException e)
		{
			throw new EngineException("Failed to drop tables", null, e);
		}
		finally
		{
			if (statement != null)
			{
				try
				{
					statement.close();
				}
				catch (SQLException e)
				{
					// Ignore, exception thrown in main catch clause
				}
			}
			if (tablesResultSet != null)
			{
				try
				{
					tablesResultSet.close();
				}
				catch (SQLException e)
				{
					// Ignore, exception thrown in main catch clause
				}
			}
		}

	}
	public void initDB(ModelId rootId)
	{
		InstanceHandler root = context.getModelLoader().getHandler(rootId);
		initDB(root);
	}
	public void initDB(InstanceHandler root)
	{
		initDB(root, new HashSet());
	}
	/**
	 * @param rootHandler
	 */
	private void initDB(InstanceHandler root, Set checkedHandlers)
	{
		if (root == null)
			return;
		if (checkedHandlers.contains(root))
			return;
		checkedHandlers.add(root);
		root.initDB(context);
		for (int i = 0; i < root.elementHandlers.length; i++)
		{
			ElementHandler e = root.elementHandlers[i];
			initDB(e.getChildInstanceHandler(), checkedHandlers);
		}
	}

    public void checkDB(InstanceHandler root)
    {
        ArrayList errors = new ArrayList();
        checkDB(root, new HashSet(), errors);
        if (!errors.isEmpty())
            throw new EngineException("Incompatible database", Misc.concatenateList(errors, "\n"),null);
    }
    private void checkDB(InstanceHandler root, Set checkedHandlers, Collection errors)
    {
        if (root == null)
            return;
        if (checkedHandlers.contains(root))
            return;
        checkedHandlers.add(root);
        String error = root.checkDB(context);
        if (error != null)
            errors.add(error);
        for (int i = 0; i < root.elementHandlers.length; i++)
        {
            ElementHandler e = root.elementHandlers[i];
            checkDB(e.getChildInstanceHandler(), checkedHandlers, errors);
        }
    }
}

//FUNC2 implement dispose (disposing updaters)

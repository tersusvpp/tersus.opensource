package tersus.runtime;

public class IllegalServerAccessException extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IllegalServerAccessException()
	{
		// TODO Auto-generated constructor stub
	}

	public IllegalServerAccessException(String message)
	{
		super(message);
		// TODO Auto-generated constructor stub
	}

	public IllegalServerAccessException(Throwable cause)
	{
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public IllegalServerAccessException(String message, Throwable cause)
	{
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}

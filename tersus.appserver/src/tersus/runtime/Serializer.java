/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import tersus.model.ModelId;
import tersus.util.InvalidInputException;
import tersus.util.Misc;

/**
 * NICE2 [Description]
 * 
 * @author Youval Bronicki
 *
 */
public class Serializer
{
	private static final boolean DEBUG = false;

	/**
	 * Create a compact textual serialization of a data value
	 * @param value The data value
	 * @param handler A <code>DataHandler</code> that represents the data type
	 */	
	public static String serialize(
		Object value,
		InstanceHandler handler,
		RuntimeContext context)
	{
		StringBuffer serialization = new StringBuffer();
		int maxStringLength = context.getContextPool().getTraceMaxStringLength();
		if (value == null)
			serialization.append("null");
		else
			if ((value instanceof String) && (maxStringLength > 0) && (((String) value).length() > maxStringLength))
				Serializer.serialize("** Long String ("+((String) value).length()+") **", handler, serialization, context);
			else
				Serializer.serialize(value, handler, serialization, context);

		return serialization.toString();
	}
	
	/**
	 * Create a compact textual serialization of a data value
	 * @param value The data value
	 * @param handler A <code>DataHandler</code> that represents the data type
	 * @param buffer A <code>StringBuffer</code> to which the serialization will be appended
	 */
	public static void serialize(
		Object value,
		InstanceHandler handler,
		StringBuffer buffer,
		RuntimeContext context)
	{
	    HashSet stack = new HashSet();
	    
	    _serialize(value, handler, buffer, context, stack);
	}
	private static void _serialize(Object value, InstanceHandler handler, StringBuffer buffer, RuntimeContext context, HashSet stack)
    {
        if (isComposite(value))
	    {
	        if (stack.contains(value))
	        {
	            buffer.append("*** Recursive structure ***");
	            return;
	        }
	        stack.add(value);
	        
	    }
	    try
	    {
		if (value instanceof List)
		{
			List values = (List) value;
			int nValues = values.size();
			buffer.append('[');
			for (int i = 0; i < nValues; i++)
			{
				if (i > 0)
					buffer.append(',');
				_serialize(values.get(i), handler, buffer, context, stack);
			}
			buffer.append(']');
			return;
		}
		ModelId actualModelId = ModelIdHelper.getCompositeModelId(value);
		if (handler == null)
		{
			if (value instanceof FlowInstance)
				handler = ((FlowInstance)value).getHandler();
			if (actualModelId != null)
				handler = context.getModelLoader().getHandler(actualModelId);
		}
		if (handler == null || handler instanceof LeafDataHandler)
		{
			appendEscaped(buffer, String.valueOf(value));
			return;
		}
//		if (handler.isPersistent() && value != null)
//		{
//			//FUNC3 use full serialization for non-reference values.
//			String referenceString = handler.getReferenceString(context, value);
//			if (referenceString != null)
//			{
//				appendEscaped(buffer, referenceString);
//				return;
//			}
//		}
		if (Misc.ASSERTIONS)
			Misc.assertion(handler instanceof CompositeDataHandler || handler instanceof FlowHandler);
		int elementValueCount = 0;
		buffer.append('(');
		if (actualModelId != null && !handler.getModelId().equals(actualModelId))
		{
			handler = context.getModelLoader().getHandler(actualModelId);
		}
		for (int i = 0; i < handler.nElements; i++)
		{
			ElementHandler elementHandler = handler.elementHandlers[i];
			Object elementValue = elementHandler.get(context, value);
			if (elementValue == null)
				continue;
			if (elementHandler.isRepetitive())
			{
				List values = (List) elementValue;
				if (values.size() > 0)
				{
					elementValueCount++;
					if (elementValueCount > 1)
						buffer.append(',');
					appendEscaped(buffer, elementHandler.getRole().toString());
					buffer.append('=');
					_serialize(values, elementHandler.getChildInstanceHandler(), buffer, context, stack);
				}
			}
			else
			{
				elementValueCount++;
				if (elementValueCount > 1)
					buffer.append(',');
				appendEscaped(buffer, elementHandler.getRole().toString());
				buffer.append('=');
				_serialize(elementValue, elementHandler.getChildInstanceHandler(), buffer, context, stack);
			}
		}
		buffer.append(')');
	    }
	    finally
	    {
	        if (isComposite(value))
	            stack.remove(value);
	    }
    }

	/**
     * @param value
     * @return
     */
    private static boolean isComposite(Object value)
    {
        return value instanceof FlowInstance || ModelIdHelper.getCompositeModelId(value) != null;
    }

    /**
	 * Appends a String to a StringBuffer replacing special characters with escape sequences
	 * @param buffer The target StringBuffer
	 * @param s The String to be appended
	 */
	private static void appendEscaped(StringBuffer buffer, String s)
	{
		for (int i = 0; i < s.length(); i++)
		{
			char c = s.charAt(i);
			switch (c)
			{
				case '=' :
					buffer.append("\\=");
					break;
				case ',' :
					buffer.append("\\,");
					break;
				case '(' :
					buffer.append("\\(");
					break;
				case ')' :
					buffer.append("\\)");
					break;
				case '[' :
					buffer.append("\\[");
					break;
				case ']' :
					buffer.append("\\]");
					break;
				case '\n' :
					buffer.append("\n");
					break;
				case '\t' :
					buffer.append("\t");
					break;
				case '\r' :
					buffer.append("\r");
					break;
				case '\\' :
					buffer.append("\\");
					break;
				default :
					buffer.append(c);
			}
		}
	}
	private String serialization;
	private int start;
	private int end;
	private int offset;
	private boolean inList;
	private InstanceHandler rootHandler;
	private RuntimeContext context;
	private StringBuffer textBuffer = new StringBuffer();
	private PathHandler pathHandler;
	private ArrayList instanceStack = new ArrayList();
	private ArrayList elementStack = new ArrayList();
	private Object currentInstance;
	private ElementHandler currentElement;
	public Object parseList(RuntimeContext context, InstanceHandler handler, String buffer)
	{
		return parseList(context, handler, buffer, 0, buffer.length());
	}
	public Object parseList(RuntimeContext context, InstanceHandler handler, String buffer, int start, int end)
	{
		this.start = start;
		this.end = end;
		this.serialization = buffer;
		this.rootHandler = handler;
		this.context = context;
		textBuffer.setLength(0);
		currentInstance = new ArrayList();

		while (offset < end - 1)
		{
			char c = nextChar();
			switch (c)
			{
				case '[' :
					startList();
					break;
				case ']' :
					endList();
					break;
				case '(' :
					startComposite();
					break;
				case ')' :
					endComposite();
					break;
				case '\\' :
					getEsacapedChar();
					break;
				case '=' :
					startElement();
					break;
				case ',' :
					endElement();
					break;
				default :
					getChar(c);
			}
		}
		end();
		return currentInstance;
	}

	/**
	 * 
	 */
	private void end()
	{
		endText();
	}
	/**
	 * @param c
	 */
	private void getChar(char c)
	{
		textBuffer.append(c);
	}
	/**
	 * 
	 */
	private void endElement()
	{
		endText();
		if (!inList) // This is the end of an element of a composite value
		{
			popElement();
		}
	}
	/**
	 * 
	 */
	private void popElement()
	{

		if (DEBUG)
			debug("End element " + currentElement.getRole());
		currentElement = (ElementHandler) elementStack.remove(elementStack.size() - 1);

	}
	/**
	 * 
	 */
	private void startElement()
	{
		InstanceHandler handler = currentElement != null ? currentElement.getChildInstanceHandler() : rootHandler;
		ElementHandler childElement = handler.getElementHandler(textBuffer.toString());
		if (childElement == null)
			error("Unknown element " + textBuffer + " in " + handler);
		pushElement(childElement);
		textBuffer.setLength(0);
	}
	private void pushElement(ElementHandler childElement)
	{
		if (DEBUG)
			debug("Start Element " + childElement.getRole());
		elementStack.add(currentElement);
		currentElement = childElement;
	}
	/**
	 * 
	 */
	private void getEsacapedChar()
	{
		char escaped = nextChar();
		switch (escaped)
		{
			case 'n' :
				getChar('\n');
				break;
			case 't' :
				getChar('\t');
				break;
			case 'r' :
				getChar('\r');
				break;
			default :
				getChar(escaped);

		}

	}
	/**
	 * 
	 */
	private void startComposite()
	{
		Object childInstance;
		if (currentElement == null)
		{
			childInstance = rootHandler.newInstance(context);
			((ArrayList) currentInstance).add(childInstance);
		}

		else
			childInstance = currentElement.create(context, currentInstance);
		pushInstance(childInstance);
		inList = false;
	}
	/**
	 * @param childInstance
	 */
	private void pushInstance(Object childInstance)
	{
		if (DEBUG)
		{

			debug("Push current instance was " + getString(currentInstance));
			debug(" Current instance= " + getString(childInstance));
		}
		instanceStack.add(currentInstance);
		currentInstance = childInstance;
	}
	/**
	 * 
	 */
	private void endComposite()
	{
		endElement();
		popInstance();
		inList = currentElement == null || currentElement.isRepetitive();
	}
	/**
	 * 
	 */
	private void endText()
	{
		if (textBuffer.length() > 0)
		{
			if (currentElement != null)
				currentElement.create(context, currentInstance, textBuffer.toString(), true);
			else // The root list
				 ((ArrayList) currentInstance).add(rootHandler.newInstance(context, textBuffer.toString(), true));

			textBuffer.setLength(0);
		}
	}
	/**
	 * 
	 */
	private void popInstance()
	{
		if (DEBUG)
		{
			debug("Pop current instance was " + getString(currentInstance));

			debug(
				"  Last char was '"
					+ serialization.charAt(offset - 1)
					+ "' offset="
					+ offset
					+ " text = "
					+ serialization.substring(0, offset));
		}
		currentInstance = instanceStack.remove(instanceStack.size() - 1);
		if (DEBUG)
			debug("  Current instance= " + getString(currentInstance));

	}
	/**
	 * 
	 */
	private void endList()
	{
		inList = false;
	}
	/**
	 * 
	 */
	private void startList()
	{
		if (currentElement != null && !currentElement.isRepetitive())
			error("Found a list where a non-repetitive element was expected");
		inList = true;
	}
	/**
	 * @param string
	 */
	private void error(String string)
	{
		throw new InvalidInputException(string); // FUNC3 add error details

	}
	private char nextChar()
	{
		return serialization.charAt(offset++);
	}

	private String getString(Object obj)
	{
		if (obj instanceof ArrayList)
			return "List";
		else if (obj instanceof Object[])
			return ModelIdHelper.getCompositeModelId(obj).toString();
		else
			return String.valueOf(obj);
	}

	private void debug(String s)
	{
		System.out.println(s);
	}
}

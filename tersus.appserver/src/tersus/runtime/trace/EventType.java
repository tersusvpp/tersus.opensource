/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime.trace;

import tersus.util.Enum;

/**
 * An Enumeration for the types of trace events
 * 
 * @author Youval Bronicki
 *
 */
public class EventType extends Enum
{

	public static final EventType STARTED = new EventType("Started");
	public static final EventType WAITING = new EventType("Waiting");
	public static final EventType RESUMED = new EventType("Resumed");
	public static final EventType ACTIVATED = new EventType("Activated");
	public static final EventType CREATED = new EventType("Created");
	public static final EventType SET = new EventType("Set");
	public static final EventType REMOVED = new EventType("Removed");
	public static final EventType ACCUMULATED = new EventType("Accumulated");
	public static final EventType READY_TO_START = new EventType("ReadyToStart");
	public static final EventType FINISHED = new EventType("Finished");
	public static final EventType TRACE = new EventType("Trace");
	public static final EventType SQL = new EventType("SQL");
    public static final EventType VALUE = new EventType("Value");

	private static final EventType[] values = {STARTED,WAITING, RESUMED, ACTIVATED, CREATED, SET, REMOVED, ACCUMULATED, READY_TO_START, FINISHED, TRACE, SQL, VALUE};
	  
	
	/**
	 * @param value
	 */
	private EventType(String value)
	{
		super(value);
	}

	/* (non-Javadoc)
	 * @see tersus.util.Enum#getValues()
	 */
	protected Enum[] getValues()
	{
		return values;
	}
	
	public static EventType parse(String s)
	{
		return (EventType) Enum.get(s, values);
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime.trace;

import tersus.model.Composition;
import tersus.model.FlowDataElementType;
import tersus.model.SlotType;
import tersus.util.Enum;

/**
 * NICE2 [Desctiption]
 * 
 * @author Youval Bronicki
 *
 */
public class ObjectType extends Enum
{

	public static final ObjectType SYSTEM = new ObjectType("System");
	public static final ObjectType SERVICE = new ObjectType("Service");
	public static final ObjectType OPERATION = new ObjectType("Operation");
	public static final ObjectType ACTION = new ObjectType("Action");
	public static final ObjectType FLOW = new ObjectType("Flow");
	public static final ObjectType EXIT = new ObjectType(SlotType.EXIT.toString());
	public static final ObjectType ERROR_EXIT = new ObjectType(SlotType.ERROR.toString());
	public static final ObjectType TRIGGER = new ObjectType(SlotType.TRIGGER.toString());
	public static final ObjectType IO = new ObjectType(SlotType.IO.toString());
	public static final ObjectType INPUT = new ObjectType(SlotType.INPUT.toString());
	public static final ObjectType ELEMENT = new ObjectType("Element");
	public static final ObjectType SELECTION = new ObjectType(Composition.SELECTION.toString());
	public static final ObjectType ATOMIC = new ObjectType(Composition.ATOMIC.toString());
	public static final ObjectType COLLECTION = new ObjectType(Composition.COLLECTION.toString());
	public static final ObjectType CONCATENATION = new ObjectType(Composition.CONCATENATION.toString());
	public static final ObjectType CONSTANT = new ObjectType("Constant");
	public static final ObjectType INTERMEDIATE = new ObjectType(FlowDataElementType.INTERMEDIATE.toString());
	public static final ObjectType MAIN = new ObjectType(FlowDataElementType.MAIN.toString());
	public static final ObjectType PARENT = new ObjectType(FlowDataElementType.PARENT.toString());

	private static final ObjectType[] values =
		{
			SYSTEM,
			SERVICE,
			OPERATION,
			ACTION,
			FLOW,
			EXIT,
			ERROR_EXIT,
			TRIGGER,
			INPUT,
			IO,
			ELEMENT,
			COLLECTION,
			CONCATENATION,
			SELECTION,
			ATOMIC,
			CONSTANT,
			INTERMEDIATE,
			MAIN,
			PARENT };

	private ObjectType(String value)
	{
		super(value);
	}

	/* (non-Javadoc)
	 * @see tersus.util.Enum#getValues()
	 */
	protected Enum[] getValues()
	{
		return values;
	}
	public static ObjectType parse(String s)
	{
		if (s == null)
			return null;
		return (ObjectType) Enum.get(s, values);
	}

}

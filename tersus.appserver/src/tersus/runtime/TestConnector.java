/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.runtime;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * NICE2 [Description]
 * 
 * @author Youval Bronicki
 * 
 */
public class TestConnector
{
    public static Connection getConnection() throws SQLException
    {
        String db = System.getProperty("DATABASE");
        if ("MS_SQL".equals(db))
            return getSQLServerConnecstion();
        else if ("HSQLDB".equals(db))
            return getHSQLDBConnection();
        else if ("POSTGRESQL".equals(db))
            return getPostgreSQLConnecstion();
        else
            return getMySQLConnection();

    }

    public static Connection getSQLServerConnecstion() throws SQLException
    {
        System.out.println("Connecting to Microsoft SQL Server");
        try
        {
            Class.forName("com.microsoft.jdbc.sqlserver.SQLServerDriver")
                    .newInstance();
        }
        catch (InstantiationException e)
        {
            e.printStackTrace();
            throw new SQLException(
                    "Failed to create driver insatance (InstantiationException)",
                    e.getMessage());
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();
            throw new SQLException(
                    "Failed to create driver insatance (IllegalAccessException)",
                    e.getMessage());
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
            throw new SQLException(
                    "Failed to create driver insatance (ClassNotFoundException)",
                    e.getMessage());
        }

        Properties props = new Properties();
        props.setProperty("User", System.getProperty("DB_USER"));
        props.setProperty("Password", System.getProperty("DB_PASSWORD"));
        props.setProperty("DatabaseName", System.getProperty("DB_NAME"));
        String host = System.getProperty("DB_HOST", "localhost");

        String url = "jdbc:microsoft:sqlserver://" + host;
        Connection connection = DriverManager.getConnection(url, props);
        return connection;

    }

    public static Connection getPostgreSQLConnecstion() throws SQLException
    {
        System.out.println("Connecting to PostgreSQL");
        try
        {
            Class.forName("org.postgresql.Driver").newInstance();
        }
        catch (InstantiationException e)
        {
            e.printStackTrace();
            throw new SQLException(
                    "Failed to create driver insatance (InstantiationException)",
                    e.getMessage());
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();
            throw new SQLException(
                    "Failed to create driver insatance (IllegalAccessException)",
                    e.getMessage());
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
            throw new SQLException(
                    "Failed to create driver insatance (ClassNotFoundException)",
                    e.getMessage());
        }

        Properties props = new Properties();
        String user = System.getProperty("DB_USER");
        String password = System.getProperty("DB_PASSWORD");
        String databaseName = System.getProperty("DB_NAME");
        String host = System.getProperty("DB_HOST", "localhost");

        String url = "jdbc:postgresql://" + host + "/" + databaseName;
        Connection connection = DriverManager.getConnection(url,user,password);
        return connection;

    }

    public static Connection getMySQLConnection() throws SQLException
    {
        System.out.println("Connecting to MySQL");
        try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        }
        catch (InstantiationException e)
        {
            e.printStackTrace();
            throw new SQLException(
                    "Failed to create driver insatance (InstantiationException)",
                    e.getMessage());
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();
            throw new SQLException(
                    "Failed to create driver insatance (IllegalAccessException)",
                    e.getMessage());
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
            throw new SQLException(
                    "Failed to create driver insatance (ClassNotFoundException)",
                    e.getMessage());
        }

        String url = "jdbc:mysql://localhost/" + System.getProperty("DB_NAME")
                + "?user=" + System.getProperty("DB_USER") + "&password="
                + System.getProperty("DB_PASSWORD");

        System.out.println("Connection url:" + url);
        java.sql.Connection connection = DriverManager.getConnection(url);
        return connection;

    }

    public static Connection getHSQLDBConnection() throws SQLException
    {
        System.out.println("Connecting to HSQLDB");
        try
        {
            Class.forName("org.hsqldb.jdbcDriver").newInstance();
        }
        catch (InstantiationException e)
        {
            e.printStackTrace();
            throw new SQLException(
                    "Failed to create driver insatance (InstantiationException)",
                    e.getMessage());
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();
            throw new SQLException(
                    "Failed to create driver insatance (IllegalAccessException)",
                    e.getMessage());
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
            throw new SQLException(
                    "Failed to create driver insatance (ClassNotFoundException)",
                    e.getMessage());
        }

        java.sql.Connection connection = DriverManager.getConnection(
                "jdbc:hsqldb:file:db/testdb", "sa", "");
        return connection;

    }

}

package tersus.runtime;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

public class MapField extends Field
{

	private static final LeafDataHandler valueHandler = new ObjectHandler();

	public MapField(String columnName)
	{
		this.name = columnName;
	}

	@Override
	public int getSQLType()
	{
		// TODO Auto-generated method stub
		return super.getSQLType();
	}

	@Override
	public LeafDataHandler getValueHandler()
	{
		return valueHandler;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object getValue(RuntimeContext context, Object value)
	{
		return ((Map<String, Object>)value).get(name);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setValue(RuntimeContext context, Object obj, Object value)
	{
		((Map<String, Object>)obj).put(name, value);
	}

	@Override
	public void setValueInStatement(PreparedStatement stmt, int i, Object value)
			throws SQLException
	{
		if (value instanceof tersus.runtime.Number)
			value = ((tersus.runtime.Number)value).getNumber();
		else if (value instanceof java.util.Date)
			value =  new java.sql.Date(((java.util.Date)value).getTime());
		super.setValueInStatement(stmt, i, value);
	}

}

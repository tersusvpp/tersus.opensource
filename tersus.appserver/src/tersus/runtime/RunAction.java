/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.io.File;
import java.io.FileOutputStream;

import javax.naming.InitialContext;

import tersus.ProjectStructure;
import tersus.model.ModelId;

/**
 * @author Youval Bronicki
 *
 */
public class RunAction
{

    public static void main(String[] args) throws Exception
    {
		System.setProperty(InitialContext.INITIAL_CONTEXT_FACTORY, "tersus.runtime.tests.InitialContextFactory");
		//Set up and start the root system
		String modelFolder = System.getProperty("REPOSITORY_ROOT", ProjectStructure.MODELS);
		ModelId modelId = new ModelId(System.getProperty("ACTION_MODEL_ID"));
		ModelLoader loader = new ModelLoader(new File(modelFolder));
		RuntimeContext context = new RuntimeContext();
		context.setModelLoader(loader);
//		Connection connection = TestConnector.getConnection();
//		context.setDatabaseProductName(connection.getMetaData().getDatabaseProductName());
//		Statement statement = connection.createStatement();
//		context.setConnection(connection);
		context.trace.enableAll();
		context.trace.timing = false;
		context.trace.setOutputStream(new FileOutputStream(modelId.getName()+".trace.xml"));
		context.trace.open();
        FlowHandler handler = (FlowHandler) loader.getHandler(modelId);
    	FlowInstance flow = (FlowInstance) handler.newInstance(context);
    	handler.start(context, flow);
		context.dispose();

    }
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;


import tersus.eclipse.ExtensionClassLoader;
import tersus.model.ElementPath;
import tersus.model.InvalidModelException;
import tersus.model.Model;
import tersus.model.ModelException;
import tersus.model.Path;
import tersus.model.Repository;
import tersus.model.validation.IValidationContext;
import tersus.model.validation.ValidatorBase;
import tersus.util.Misc;

/**
 * @author Youval Bronicki
 *  
 */
public class DefaultValidator extends ValidatorBase
{

    private ModelLoader loader = null;

    /*
     * (non-Javadoc)
     * 
     * @see tersus.model.validation.IValidator#validate(tersus.model.validation.IValidationContext,
     *      tersus.model.Model)
     */
    public void validate(IValidationContext context, Model model)
    {
        super.validate(context, model);
        if (loader == null)
        {
            ExtensionClassLoader platformLoader = new ExtensionClassLoader(getClass().getClassLoader());
            ClassLoader pluginLoader = tersus.webapp.ContextPool.getPluginLoader(getProjectRoot(), platformLoader);
			loader = new ModelLoader((Repository)model.getRepository(), pluginLoader);
        }
        try
        {
            loader.getHandler(model.getId());
        }
        catch (ModelException e)
        {
            ElementPath path = Path.EMPTY;
            String problem = Misc.getShortName(e.getClass());
            String details = e.getMessage();

            Throwable cause = e.getCause();
            if (cause != null)
            {
                problem = Misc.getShortName(cause.getClass());
                details = cause.getMessage();
                boolean reported = false;
                if (cause instanceof InvalidModelException)
                {
                    InvalidModelException invalidModelException = (InvalidModelException) e
                            .getCause();
                    if (model.getId().equals(invalidModelException.getId()))
                    {
                        // If the invalid model exception refers to the current
                        // model, extract the "structured" information

                        problem = invalidModelException.getProblem();
                        details = invalidModelException.getDetails();
                        path = invalidModelException.getPath();
                        reported = true;
                    }
                }
                if (! reported) 
                {
                    Throwable current = e;
                    while (current instanceof ModelException)
                    {
                        current = current.getCause();
                        if (current instanceof InvalidModelException)
                        {
                            InvalidModelException invalidModelException = (InvalidModelException) current;
                            String _problem = invalidModelException.getProblem();
                            String _details = invalidModelException.getDetails();
                            ElementPath _path = invalidModelException.getPath();
//                            context.addRootException(invalidModelException.getId(),_problem, _details, _path);
                            break;
                        }
                    }
                }
                context.addProblem(problem, details, path);
            }
        }
    }
}
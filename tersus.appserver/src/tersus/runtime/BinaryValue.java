/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import tersus.util.Misc;

/**
 * A binary data value
 * 
 * @author Youval Bronicki
 *
 */
public class BinaryValue
{

	private byte[] data;
	/**
	 * 
	 */
	public BinaryValue(byte[] data)
	{
		if (data == null)
			throw new IllegalArgumentException("Null data");
		this.data = data;
	}

	/**
	 * @return a byte[] that contauns the data of the BinaryBuffer.
	 * 
	 * The returned array <b>may or may not</b> be the actual underlying buffer.
	 */
	public byte[] toByteArray()
	{
		return data;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		return "Binary: "+data.length + " bytes";
	}

    /**
     * @return
     */
    public boolean isEmpty()
    {
        return data == null && data.length == 0;
    }

    public InputStream toInputStream()
    {
        return new ByteArrayInputStream(data);
    }


    public boolean equals(Object obj)
    {
        return (obj instanceof BinaryValue  &&
                Misc.compare(toByteArray(), ((BinaryValue)obj).toByteArray()));
    }
}

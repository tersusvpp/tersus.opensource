/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.commons.lang3.StringEscapeUtils;

import tersus.InternalErrorException;
import tersus.model.BuiltinProperties;
import tersus.model.Composition;
import tersus.model.DataType;
import tersus.model.Model;
import tersus.model.Path;
import tersus.model.validation.Problems;
import tersus.util.Enum;
import tersus.util.EscapeUtil;
import tersus.util.NumberUtil;

/**
 * An abstract InstanceHandler for data types. <br>
 * 
 * @author Youval Bronicki
 *  
 */
public abstract class DataHandler extends InstanceHandler
{
    private boolean constant;

    Composition type;

    String valueStr;

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.InstanceHandler#getType()
     */
    public Enum getType()
    {
        return type;
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.InstanceHandler#intializeFromModel(tersus.model.Model)
     */
    public void initializeFromModel(Model model)
    {
        /*
         * Extend InstanceHandler#intializeFromModel by extracting the
         * (Composition) type.
         */

        super.initializeFromModel(model);
        if ( ! (model instanceof DataType))
        {
            notifyInvalidModel(Path.EMPTY, Problems.INVALID_MODEL_TYPE, "The plugin expects "+DataType.class.getName()+" but found "+model.getClass().getName());
            return;
        }
        type = ((DataType) model).getComposition();
        setConstant(((DataType) model).isConstant());
        valueStr = EscapeUtil.unquote((String) model.getProperty(BuiltinProperties.VALUE));
        if (getPluginVersion() >= 1)
        	valueStr = StringEscapeUtils.unescapeEcmaScript(valueStr);
    }


    /**
     * Creates an instance from a serialized representation.
     * 
     * 
     * For persistent types, the serialization is interpreted as a serialization
     * of primary key values and the returned instance is a
     * <code>PersistentValueReference</code>
     * 
     * @param context
     *            A RuntimeContext for the operation
     * @param valueStr
     *            The serialization
     * @return An instance represented by the serialized representaion.
     */
    public abstract Object newInstance(RuntimeContext context, String valueStr, boolean forceDefaultFormat);

    public void setConstant(boolean constant)
    {
        this.constant = constant;
    }

    public boolean isConstant()
    {
        return constant;
    }

    /**
     * Comparing two Tersus runtime data instances
     * 
     * @param obj1
     *            Tersus data instance (leaf or composite)
     * @param obj2
     *            Tersus data instance (leaf or composite)
     * 
     * @return true iff both are identical content-wise (models don't have to
     *         match, numbers may slightly differ, as allowed by
     *         NumberUtil.essentiallyEqual())
     */
    public static boolean instanceDeepCompare(Object obj1, Object obj2)
    {
        if ((obj1 == null) || (obj2 == null))
            return (obj1 == obj2);
        else if ((obj1 instanceof Object[]) && (obj2 instanceof Object[]))
        { // Both composites
            Object[] composite1 = (Object[]) obj1;
            Object[] composite2 = (Object[]) obj2;
            if (composite1.length != composite2.length)
                return false;
            for (int i = 1; i < composite1.length; i++)
                // Skipping first item (ModelId)
                if (!instanceDeepCompare(composite1[i], composite2[i]))
                    return false;
            return true;
        }
        else if ((obj1 instanceof Number) && (obj2 instanceof Number))
        { // Both numbers
            double n1 = ((Number) obj1).value;
            double n2 = ((Number) obj2).value;
            return NumberUtil.essentiallyEqual(n1, n2);
        }
        else if (obj1 instanceof ArrayList && obj2 instanceof ArrayList)
        {
            ArrayList list1 = (ArrayList) obj1;
            ArrayList list2 = (ArrayList) obj2;
            int size = list1.size();
            if (list2.size() != size)
                return false;
            for (int i = 0; i < size; i++)
                if (!instanceDeepCompare(list1.get(i), list2.get(i)))
                    return false;
            return true;
        }
        else
            return obj1.equals(obj2); // At least one leaf - string, date, etc.
    }
}
/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/

package tersus.runtime;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tersus.model.BuiltinProperties;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.runtime.ElementHandler;
import tersus.runtime.InstanceHandler;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.RuntimeContext;
import tersus.util.SpecialRoles;
import tersus.util.XMLWriter;

/**
 * @author oferb
 * 
 *         Creating a well formed XML document.
 * 
 *         This class includes the common parts of SerializeXML' and 'CallWebService'.
 * 
 */

public class XMLSerializer

{
	public static final String NS_XSI = "http://www.w3.org/2001/XMLSchema-instance"; // Based on
																						// http://www.w3.org/TR/wsdl#_notational

	String implicitNameSpaceToUse = null;
	HashMap<String, String> nameSpaceAliases = null;
	String bindingUse = null; // WSDL_LITERAL or WSDL_ENCODED (null is treated as WSDL_LITERAL)
	String schemaInstanceAlias = null;
	RuntimeContext context;
	StringWriter serialization = null;
	private XMLWriter w = null;
	private StringBuffer tmpBuffer = new StringBuffer(); // Temporary buffer for String
	private boolean includeEmptyElements; 

	public XMLSerializer(String implicitNameSpaceToUse, HashMap<String, String> nameSpaceAliases,
			String bindingUse, RuntimeContext context) throws IOException
	{
		this(implicitNameSpaceToUse, nameSpaceAliases, false, bindingUse, context);
	}

	public XMLSerializer(String implicitNameSpaceToUse, HashMap<String, String> nameSpaceAliases,
			boolean includeEmptyElements, String bindingUse, RuntimeContext context) throws IOException
	{
		this.bindingUse = bindingUse;
		this.implicitNameSpaceToUse = implicitNameSpaceToUse;
		this.nameSpaceAliases = nameSpaceAliases;
		if (SpecialRoles.WSDL_ENCODED.equals(bindingUse))
		{
			oneMoreNameSpace(NS_XSI, "xsi");
			schemaInstanceAlias = (String) nameSpaceAliases.get(NS_XSI); // Might be different from
																			// "xsi" if previously
																			// existed
		}
		this.context = context;
		this.includeEmptyElements = includeEmptyElements;		
		serialization = new StringWriter();
		this.w = new XMLWriter(serialization);
	}

	public void alsoListUsedNameSpaces(ElementHandler elementHandler)
	{
		String nameSpace = elementNameSpace(elementHandler, null);
		if (nameSpace != null)
			oneMoreNameSpace(nameSpace, null);

		InstanceHandler handler = elementHandler.getChildInstanceHandler();
		if (handler instanceof CompositeDataHandler)
		{
			for (int i = 0; i < handler.elementHandlers.length; i++)
			{
				String role = handler.elementHandlers[i].getRole().toString();
				role = netRole(role);
				if (!role.startsWith(SpecialRoles.DEPENDENT_MODEL_ROLE_PREFIX))
					alsoListUsedNameSpaces(handler.elementHandlers[i]);
			}
		}
	}

	private void oneMoreNameSpace(String nameSpace, String alias)
	{
		if (nameSpaceAliases == null)
			nameSpaceAliases = new HashMap<String, String>();

		if (nameSpaceAliases.get(nameSpace) == null)
		{
			if (alias == null)
			{
				int i = 1;
				while (nameSpaceAliases.containsValue(alias = "ns" + i))
					i++;
			}
			nameSpaceAliases.put(nameSpace, alias);
		}
	}

	@SuppressWarnings("unchecked")
	public void addXmlSerialization(String parentNameSpace, String elementName, boolean useAliases,
			ElementHandler elementHandler, Object parent) throws IOException
	{
		String nameSpace = elementNameSpace(elementHandler, implicitNameSpaceToUse);
		Object value = elementHandler.get(context, parent);

		if ((value != null) || includeEmptyElements) // Null possible for non-mandatory elements
		{
			if (elementHandler.isRepetitive())
			{
				String itemTagName = (String) elementHandler.getProperty("xmlItemTagName");
				List<Object> childValues = (List<Object>) value;
				if (childValues.size() > 0)
				{
					if (itemTagName != null)
					{
						w.openTag(elementName);
						w.closeTag();
						elementName = itemTagName;
					}

					for (int i = 0; i < childValues.size(); i++)
					{
						Object childValue = childValues.get(i);
						addXmlElement(parentNameSpace, nameSpace, false, elementName, useAliases,
								childValue, elementHandler);
					}
					if (itemTagName != null)
						w.endElement();
				}
				else
					if (includeEmptyElements)
						addXmlElement(parentNameSpace, nameSpace, false, elementName, useAliases, null,
								elementHandler);
			}
			else
			// Non-repetitive
			{
				addXmlElement(parentNameSpace, nameSpace, false, elementName, useAliases, value,
						elementHandler);
			}
		}
	}

	public void addXmlElement(String parentNameSpace, String nameSpace, boolean listAliases,
			String elementName, boolean useAliases, Object value, ElementHandler elementHandler)
			throws IOException
	{
		InstanceHandler instanceHandler = elementHandler.getChildInstanceHandler();
		if (instanceHandler instanceof AnythingHandler)
		{
		    ModelId valueModelId = ModelIdHelper.getCompositeModelId(value);
		    instanceHandler = context.getModelLoader().getHandler(valueModelId);
		}
		boolean isLeaf = instanceHandler instanceof LeafDataHandler;
		boolean isSelfContentLeaf = isLeaf
				&& (SpecialRoles.XML_CONTENT_LEAF_ROLE.equals(elementName));

		if (value != null)
			if (isSelfContentLeaf)
				addXmlContent(elementName, value, elementHandler);
			else
			{
				// [1] Start Element Tag
				addXmlStartElementTag(parentNameSpace, nameSpace, listAliases, elementName, useAliases,
					value, instanceHandler);
		
				// [2] Content
				if (!isLeaf)	// CompositeDataHandler
					addXmlChildElements(nameSpace, useAliases, value, instanceHandler);
				else			// LeafDataHandler
					addXmlContent(elementName, value, elementHandler);
				
				// [3] End Element Tag
				addXmlEndElementTag();
			}
		else if (includeEmptyElements && !isSelfContentLeaf)
		{
				// [1] Start Element Tag
				openXmlElementTag(parentNameSpace, nameSpace, listAliases, elementName, null,
						instanceHandler);
		
				// [2] Content
				if (!isLeaf)	// CompositeDataHandler
					addXmlChildElements(nameSpace, useAliases, value, instanceHandler);
				
				// [3] End Element Tag
				addXmlEndElementTag();
			}
	}

	private void addXmlContent(String elementName, Object value, ElementHandler elementHandler)
			throws IOException
	{
		tmpBuffer.setLength(0);
		elementHandler.format(context, value, tmpBuffer);
		w.writeContent(tmpBuffer, "true".equals(elementHandler.getProperty(BuiltinProperties.XML_USE_CDATA)));
	}


	public void addXmlStartElementTag(String parentNameSpace, String nameSpace,
			boolean listAliases, String elementName, boolean useAliases, Object value,
			InstanceHandler instanceHandler) throws IOException
	{
		openXmlElementTag(parentNameSpace, nameSpace, listAliases, elementName, value,
				instanceHandler);

		// [5] Close tag
		w.closeTag();
	}

	private void openXmlElementTag(String parentNameSpace, String nameSpace, boolean listAliases,
			String elementName, Object value, InstanceHandler instanceHandler) throws IOException
	{
		boolean useAliases;
		// [1] Open tag
		String alias = null;
		if (useAliases = nameSpaceAliases != null)
			useAliases = (alias = (String) nameSpaceAliases.get(nameSpace)) != null;

		if (useAliases)
			w.openTag(alias + ":" + elementName);
		else
			w.openTag(elementName);

		// [2] Global Namespace
		boolean isLeaf = (instanceHandler != null) && (instanceHandler instanceof LeafDataHandler);
		if (isLeaf && (value != null) && SpecialRoles.WSDL_ENCODED.equals(bindingUse)) // "encoded"
																						// (WSDL) -
																						// see
																						// http://hitenmpatel.blogspot.com/2006/05/web-service-description-language-wsdl.html
		{
			String type = compactNameSpace(modelNameSpace(instanceHandler.getModel(), null)) + ":"
					+ instanceHandler.getName();
			// BUG2 If compactNameSpace() returns the namespace itself (rather than its alias),
			// we might create something like
			// xsi:type="urn:ebay:apis:eBLBaseComponents:BalanceCodeType",
			// which is probably illegal
			w.writeAttribute(schemaInstanceAlias + ":type", type);
		}
		else
		// Default (for "regular" XML serialization) = "literal" (WSDL)
		{
			if (!useAliases)
				if ((nameSpace != null) && !nameSpace.equals(parentNameSpace))
					w.writeAttribute("xmlns", nameSpace); // Is it always true for leaves?
		}

		// [3] Additional Namespace Aliases
		if (listAliases && nameSpaceAliases != null)
		{
			for (Map.Entry<String, String> e : nameSpaceAliases.entrySet())
			{
				String ns = e.getKey();
				alias = e.getValue();
				w.writeAttribute("xmlns:" + alias, ns);
			}
		}

		// [4] Additional Attributes
		if (!isLeaf && value != null) // CompositeDataHandler
		{
			// Attributes
			for (int i = 0; i < instanceHandler.elementHandlers.length; i++)
			{
				String role = instanceHandler.elementHandlers[i].getRole().toString();
				if (role.startsWith(SpecialRoles.DEPENDENT_MODEL_ROLE_PREFIX))
				{
					String attributeName = role.substring(SpecialRoles.DEPENDENT_MODEL_ROLE_PREFIX
							.length());
					addXmlAttribute(attributeName, instanceHandler.elementHandlers[i], value);
				}
			}
		}
	}

	public void addXmlEndElementTag() throws IOException
	{
		w.endElement();
	}

	public void addXmlChildElements(String nameSpace, boolean useAliases, Object parentValue,
			InstanceHandler parentHandler) throws IOException
	{
		for (int i = 0; i < parentHandler.elementHandlers.length; i++)
		{
			String role = parentHandler.elementHandlers[i].getRole().toString();
			role = netRole(role);
			if (!role.startsWith(SpecialRoles.DEPENDENT_MODEL_ROLE_PREFIX))
				addXmlSerialization(nameSpace, role, useAliases, parentHandler.elementHandlers[i],
						parentValue);
		}
	}

	private void addXmlAttribute(String attributeName, ElementHandler elementHandler, Object parent)
			throws IOException
	{
		Object value = elementHandler.get(context, parent);

		if (value != null) // Null possible for non-mandatory attributes
		{
			tmpBuffer.setLength(0);
			elementHandler.format(context, value, tmpBuffer);
			w.writeAttribute(attributeName, tmpBuffer);
		}
	}

	public String done() throws IOException
	{
		w.close();
		context = null;
		w = null;
		return serialization.toString();
	}

	public static String netRole(String role) // Reverses the action of
												// tersus.editor.wizards.ImportWsdlCommand.WsdlHandler.uniqueRelativeRole()
	{
		int p = role.lastIndexOf(':');
		return (p >= 0 ? role.substring(p + 1) : role);
	}

	public static String elementNameSpace(ElementHandler elementHandler, String implicitNameSpace)
	{
		String nameSpace = (String) elementHandler.getProperty(BuiltinProperties.XML_NAME_SPACE);
		if (nameSpace == null) // No namespace for the element
		{
			// Use the model's namespace (good for slots and top-level data elements, maybe in some
			// cases parent's namespace is better for data elements)
			Model dataModel = elementHandler.getChildInstanceHandler().getModel();
			nameSpace = modelNameSpace(dataModel, null);
		}

		return (nameSpace == null ? implicitNameSpace : nameSpace);
	}

	public static String modelNameSpace(Model dataModel, String implicitNameSpace)
	{
		String nameSpace = (String) dataModel.getProperty(BuiltinProperties.XML_NAME_SPACE);
		return (nameSpace == null ? implicitNameSpace : nameSpace);
	}

	private String compactNameSpace(String nameSpace)
	{
		String alias = (String) nameSpaceAliases.get(nameSpace);
		if (alias == null)
		{
			// Tolerate a missing or extra '/' at the end of the name space
			String modifiedNameSpace = (nameSpace.charAt(nameSpace.length() - 1) == '/' ? nameSpace
					.substring(0, nameSpace.length() - 1) : nameSpace + "/");
			alias = (String) nameSpaceAliases.get(modifiedNameSpace);
		}
		return (alias != null ? alias : nameSpace);
	}
}
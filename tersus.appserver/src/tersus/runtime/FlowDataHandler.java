/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import tersus.model.BuiltinProperties;
import tersus.model.DataElement;
import tersus.model.FlowDataElementType;
import tersus.model.ModelElement;
import tersus.runtime.trace.EventType;

/**
 * An ElementHandler for flow data elements
 * 
 * @author Youval Bronicki
 *
 */
public class FlowDataHandler extends FlowElementHandler
{

	FlowDataElementType type;
    /**
	 * @param parentHandler
	 */
	public FlowDataHandler(FlowHandler parentHandler)
	{
		super(parentHandler);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowElementHandler#invoke(tersus.runtime.FlowElement)
	 */
	public void invoke(RuntimeContext context, IFlowElement element)
	{
		if (context.trace.traceData)
		{
			Object value = element.getValueObject();
			context.trace.addDetailValue(getChildInstanceHandler(), value);
			context.trace.add(EventType.ACTIVATED, type, getRole(), null);
		}
		invokeLinks(context, element);

	}

	/* (non-Javadoc)
	 * @see tersus.runtime.ElementHandler#set(java.lang.Object, java.lang.Object)
	 */
	public void set(RuntimeContext context, Object parent, Object value)
	{
		super.set(context, parent, value);
		FlowDataInstance element = new FlowDataInstance();
		element.init((FlowInstance) parent, this, value);

		((CompositeFlowHandler) parentFlowHandler).addReadyElement(context, (FlowInstance) parent, element);
	}
	
	/**
	 * Set the value without adding the element to the queue
	 * @param parent
	 * @param value
	 */
	public void primSet(RuntimeContext context, Object parent, Object value)
	{
		super.set(context, parent, value);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.ElementHandler#initializeFromElement(tersus.model.ModelElement)
	 */
	public void initializeFromElement(ModelElement element)
	{
		super.initializeFromElement(element);
		type = (FlowDataElementType) ((DataElement) element).getProperty(BuiltinProperties.TYPE);
		if (type == null)
			type = FlowDataElementType.INTERMEDIATE;
	}

    public void accumulate(RuntimeContext context, Object parent, Object value)
    {
        super.accumulate(context, parent, value);
		FlowDataInstance element = new FlowDataInstance();
		element.init((FlowInstance) parent, this, value);

		((CompositeFlowHandler) parentFlowHandler).addReadyElement(context, (FlowInstance) parent, element);
    }
}

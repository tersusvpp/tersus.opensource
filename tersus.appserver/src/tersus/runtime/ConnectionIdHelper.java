package tersus.runtime;

import java.sql.Connection;
import java.util.WeakHashMap;

/**
 * This class helps obtain identifiers for JDBC connection, where we're trying to identify the base connection
 * to the database (which is stored in a connection pool), as opposed to wrappers or proxies around it (which are created every time we obtain a connection from the pool)
 * @author youval
 *
 */
public class ConnectionIdHelper
{
	ConnectionUnwrapper unwrapper = new ConnectionUnwrapper();
	WeakHashMap<Object, String> connectionIdMap = new WeakHashMap<Object, String>();

	public String getConnectionId(Connection c)
	{
		synchronized (connectionIdMap)
		{
			String id = connectionIdMap.get(c);
			return id;
		}
	}

	public Object getBaseConnectionKey(Connection c)
	{
		// Uses the underlying connection as a key for storing the id of the connection
		return unwrapper.unwrap(c);
	}

	public String saveConnectionId(Connection c, RuntimeContext context)
	{
		synchronized (connectionIdMap)
		{
			Object k = getBaseConnectionKey(c);
			String id = connectionIdMap.get(k);
			if (id == null)
			{
				id = context.getDatabaseAdapterManager()
						.getDatabaseAdapter(SQLUtils.getDatabaseProductName(c))
						.getConectionId(c, context);
				connectionIdMap.put(k, id);
			}
			connectionIdMap.put(c, id);
			return id;
		}
	}

	public String toString(Connection c)
	{
		try
		{
		return c.getClass().getName() + "{" + System.identityHashCode(c) + "} ["
				+ getConnectionId(c) + "]";
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return "Unidentified connection (error getting connection id string)";
		}
	}
}

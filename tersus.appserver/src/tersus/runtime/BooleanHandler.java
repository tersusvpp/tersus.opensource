/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import tersus.util.Misc;

/**
 * A handler for a boolean value.
 * 
 * @author Youval Bronicki
 * 
 * Boolean values are represented in the database by the characters 'Y' and 'N'
 * The Textual representation is 'Yes' and 'No'.
 *
 */
public class BooleanHandler extends LeafDataHandler
{
    /* (non-Javadoc)
     * @see tersus.runtime.DataHandler#toSQL(tersus.runtime.RuntimeContext, java.lang.Object)
     */
    private static final String TRUE_LITTERAL = "true";
    private static final String YES_LITTERAL = "Yes";
    private static final String DB_FALSE_LITTERAL = "N";
    private static final String DB_TRUE_LITTERAL = "Y";
    public BooleanHandler()
    {
        super();
        setMaxLength(1);
    }
    public Object toSQL(Object value)
    {
        if (value == null)
            return null;
        else
            return ((Boolean)value).booleanValue() ? DB_TRUE_LITTERAL : DB_FALSE_LITTERAL;
    }
    /* (non-Javadoc)
     * @see tersus.runtime.DataHandler#fromSQL(java.lang.Object)
     */
    public Object fromSQL(ResultSet rs, int i) throws SQLException
    {
        Object value = rs.getObject(i);
        return fromSQL(value);
    }
    protected Object fromSQL(Object value)
    {
        if (value == null)
            return null;
        if (DB_TRUE_LITTERAL.equals(value))
            return Boolean.TRUE;
        else
            return Boolean.FALSE;
    }
	public Object newInstance(RuntimeContext context, boolean b)
	{
		return Boolean.valueOf(b);
	}

	/**
	 * 
	 * @param context
	 * @param s A string representation of the numeric value of the created instance (e.g. "10.7").
	 * @return
	 */
	public Object newInstance(RuntimeContext context, String s, boolean forceDefaultFormat)
	{
		if (s == null || s.trim().length() == 0) 
		    
			return null;
		if (TRUE_LITTERAL.equals(s) || YES_LITTERAL.equals(s))
		    return Boolean.TRUE;
		else
		    return Boolean.FALSE;
		    
	}

	public int getSQLType()
	{
		return Types.CHAR;
	}

    public boolean equal(Object value1, Object value2)
    {
        return Misc.equal(value1, value2);
    }
    // R
    public int compare(RuntimeContext context, Object obj1, Object obj2)
    {
        
        if (obj1 == null || obj2 == null)
            return compareNull(obj1, obj2);
		if (obj1.equals(obj2))
		    return 0;
		else return ((Boolean) obj1).booleanValue() ? 1:-1; // true is considered greater than false 
		        
    }
    public boolean isInstance(Object obj)
    {
        return obj instanceof Boolean;
    }
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.sql.Clob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import tersus.util.FileUtils;
import tersus.util.Misc;

/**
 * A handler for a textual leaf data types.
 * 
 * @author Youval Bronicki
 *
 */
public class TextHandler extends LeafDataHandler
{

    /* (non-Javadoc)
	 * @see tersus.runtime.InstanceHandler#newInstance(tersus.runtime.RuntimeContext, java.lang.String)
	 */
	public Object newInstance(RuntimeContext context, String s, boolean forceDefaultFormat)
	{
		return truncate(s);

	}
	/* (non-Javadoc)
	 * @see tersus.runtime.DataHandler#toSQL(tersus.runtime.RuntimeContext, java.lang.Object)
	 */
	public Object toSQL(Object value)
	{
		String s = (String)value;
		return truncate(s);
	}
	private Object truncate(String s)
	{
		if (s == null)
			return null;
		else if (getMaxLength() <= 0 || s.length() <= getMaxLength())
			return s;
		else
		{
			synchronized (System.err)
			{
				System.err.println("WARNING: String truncated (max Length is " + getMaxLength() + ")");
				System.err.println("Original String:" + s);
			}
			return s.substring(0, getMaxLength());
		}
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.InstanceHandler#newInstance(tersus.runtime.RuntimeContext)
	 */
	public Object newInstance(RuntimeContext context)
	{
		if (Misc.ASSERTIONS)
			Misc.assertion(isConstant());
		if (valueStr == null)
			return "";
		else
			return valueStr;
	}

    public int compare(RuntimeContext context, Object obj1, Object obj2)
    {
        if (obj1 == null || obj2 == null)
            return compareNull(obj1, obj2);
        return ((String)obj1).compareTo((String)obj2);
    }
    public boolean isInstance(Object obj)
    {
        return obj instanceof String;
    }
    public Object fromSQL(ResultSet rs, int index) throws SQLException
    {
        return fromSQL(rs.getObject(index));
    }
    protected Object fromSQL(Object object)
    {
        if (object instanceof Clob)
        {
             Clob clob = (Clob)object;
             try
             {
                 StringBuffer s = FileUtils.readString(clob.getCharacterStream(), true);
                 return s.toString();
             }
             catch (SQLException e)
             {
                 throw new ModelExecutionException("Failed to text from clob",e.getMessage(), e);
             }
        }
        else     
            return object!= null? object.toString() : null;
    }
    public int getSQLType()
    {
        return Types.VARCHAR;
    }
    
    public Object padValue(Object fieldValue, int columnSize)
    {
        if (fieldValue == null)
            return null;
       String s = String.valueOf(fieldValue);
       if (s.length() >= columnSize)
           return s;
       StringBuffer sb = new StringBuffer(columnSize);
       sb.append(s);
       for (int i=s.length(); i< columnSize; i++)
           sb.append(' ');
       return sb.toString();
    }
    
}
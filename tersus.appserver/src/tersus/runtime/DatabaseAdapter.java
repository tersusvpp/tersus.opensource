/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

/**
 * Implements DB-specific behavior.
 * 
 * @author Youval Bronicki
 *
 */
public interface DatabaseAdapter
{
    String getDatabaseProductName();
	/**
	 * @return A string containing the DDL for creating boolean fields.
	 */
    String getBoolean();
	/**
	 * @return A string containing the DDL for creating double fields.
	 */
	String getDouble();

	/**
	 * @return A string containing the DDL for creating varchar fields.
	 */
	String getVarchar(int maxLength);

	/**
	 * @return A string containing the DDL for creating date fields.
	 */
	String getDate();

	/**
	 * @return A string containing the DDL for creating long binary.
	 */
	String getLongVarbinary();


	/**
	 * Translates the error message returned from the JDBC driver to a more friendly message
	 */
	String translateMessage(SQLException e);
	

	/**
	 * @param tableType
	 * @return
	 */
	String getTableTypeDDL(String tableType);

	/**
	 * @param e An SQLException
	 * @return The exception's type (e.g. DUPLICATE_KEY)
	 */
	String DUPLICATE_KEY = "Duplicate Key";
	String getExceptionType (SQLException e);

    /**
     * Performs database shutdown (used by in-memory version of HSQLDB)
     */
    void shutdown(RuntimeContext context);

    /**
     * Perform any table name conversions required by the database
     * 
     * @param tableName
     * @return
     */
    String convertTableName(String tableName);

    /**
     * @return
     */
    boolean alterTableSupportsMultipleColumns();
    /**
     * @return
     */
    
    String getSelectForUpdate(String column, String table, String condition);
    String getInt();
    /**
     * @return
     */
    String getDateTime();
    String getChar(int maxLength);
    boolean canResumeAfterExceptions();
    
    
    String getStoredProcedureClause(String parameterName);
    /**
     * 
     * @param columns - list of columns to select
     * @param table - table, view, or function call to select from
     * @param primaryKey - name of single primary key in result set (optional - for optimization)
     * @param condition - WHERE clause expression
     * @param orderBy - ORDER BY expression
     * @param numberOfRecords - maximum number of records to retrieve
     * @param offset - 0 - based offset (for paging)
     * @param optimization TODO
     * @return
     */
    String prepareSelectStatement(StringBuffer columns, StringBuffer table, String primaryKey, StringBuffer condition, String orderBy, int numberOfRecords, int offset, Set<String> optimizations);
    boolean supportsQueryPaging();
    boolean requiresPadding();
	String getIdentity();
	boolean useColumnNamesForDBGenereatedColumns();
	
	boolean supportsQueryTimeout();
	boolean test(RuntimeContext context);
	String getConectionId(Connection c, RuntimeContext context);
	String prepareCountStatement(StringBuffer table, StringBuffer condition,
			HashSet<String> optimizations);
	
	/**
	 * 
	 * @param context TODO
	 * @return true if a commit operation was performed, false if otherwise (auto-commit connection)
	 */
	boolean commit(Connection c, RuntimeContext context) throws SQLException;
	/**
	 * 
	 * @param context TODO
	 * @return true if a rollback operation was performed, false if otherwise (auto-commit connection)
	 */
	boolean rollback(Connection c, RuntimeContext context) throws SQLException;
}

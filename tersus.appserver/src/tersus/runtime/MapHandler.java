package tersus.runtime;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import tersus.model.ModelException;

public class MapHandler extends LeafDataHandler
{

    @Override
    public Object fromSQL(ResultSet rs, int index) throws SQLException
    {
        throw new ModelException("Maps cannot be directly read from SQL or written to SQL");
    }

    @Override
    protected Object fromSQL(Object obj)
    {
        throw new ModelException("Maps cannot be directly read from SQL or written to SQL");
    }

    @Override
    public int getSQLType()
    {
        throw new ModelException("Maps cannot be directly read from SQL or written to SQL");
    }

    @Override
    public Object toSQL(Object value)
    {
        throw new ModelException("Maps cannot be directly read from SQL or written to SQL");
    }

    @Override
    public boolean isInstance(Object obj)
    {
        return (obj instanceof Map<?, ?>) || obj instanceof JSONObject;
    }

	@Override
	public Object newInstance(RuntimeContext context)
	{
		return new HashMap<Object,Object>();
	}
    

}

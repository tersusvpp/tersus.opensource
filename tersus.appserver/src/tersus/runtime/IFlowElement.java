/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import tersus.util.Enum;

/**
 * Represents an element of a flow instance - either a sub-flow
 * , a data element, or a data value received or sent through a slot
 * 
 * @author Youval Bronicki
 *
 */
public interface IFlowElement
{

	/**
	 * @return Tje FlowElementHandler associated with this instance
	 */
	FlowElementHandler getElementHandler();

	/**
	 * @return the value object referred by this element
	 */
	Object getValueObject();

	/**
	 * @return the parent flow of this FlowElement
	 */
	FlowInstance getParent();

	IFlowElement getNextReadySibling();

	void setNextReadySibling(IFlowElement state);

    /**
     * @return
     */
    Enum getType();

}

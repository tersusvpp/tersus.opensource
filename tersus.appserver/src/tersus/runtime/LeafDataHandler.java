/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.FieldPosition;
import java.util.Map;

import tersus.model.BuiltinProperties;
import tersus.model.Model;
import tersus.model.Path;
import tersus.model.validation.Problems;
import tersus.util.Misc;

/**
 * A handler for leaf types that have no special behaviour.  This handler
 * is used initially to represent all leaf types, and may continue to be used until
 * some "interesting" behaviour, such as constraints and constants is added to data types.
 * 
 * @author Youval Bronicki
 *
 */
public abstract class LeafDataHandler extends DataHandler
{
	protected static final FieldPosition DUMMY_FIELD_POSITION = new FieldPosition(0);
	private int maxLength=0;
	protected Integer sqlType = null;
	//NICE2 make this class abstract
	public Object newInstance(RuntimeContext context)
	{
		if (Misc.ASSERTIONS)
			Misc.assertion(isConstant());
		return newInstance(context, valueStr, false);
	}


    public abstract Object toSQL(Object value);
    public abstract Object fromSQL(ResultSet rs, int index) throws SQLException;
    protected abstract Object fromSQL(Object obj);
    public Object fromSQL(CallableStatement cs, int index) throws SQLException
    {
        Object obj = cs.getObject(index);
        if (obj == null)
            return obj;
        else
            return fromSQL(obj);
    }
    public abstract int getSQLType();

    /**
     * @return
     */

	/* (non-Javadoc)
	 * @see tersus.runtime.InstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		String s = (String) model.getProperty(BuiltinProperties.MAX_LENGTH);
		if (s != null)
		{
			setMaxLength(Integer.parseInt(s));
		}
        String sqlTypeName = (String)model.getProperty(BuiltinProperties.COLUMN_TYPE);
        if (sqlTypeName != null)
        {
        	sqlType = tersus.util.SQLUtils.getSQLTypeNumber(sqlTypeName);
        	if (sqlType == null)
                notifyInvalidModel(Path.EMPTY, Problems.UNKNOWN_TYPE,"Unknown column type "+sqlTypeName);
        }

	}

	/**
	 * @return
	 */
	public int getMaxLength()
	{
		return maxLength;
	}

	/**
	 * @param i
	 */
	public void setMaxLength(int i)
	{
		maxLength = i;
	}


	/* (non-Javadoc)
	 * @see tersus.runtime.DataHandler#newInstance(tersus.runtime.RuntimeContext, java.lang.String)
	 */
	public Object newInstance(RuntimeContext context, String valueStr, boolean forceDefaultFormat)
	{
		throw new EngineException("Invalid instantiation", "String='"+valueStr+"'", null);
	}


	public StringBuffer format(RuntimeContext context, Object value, Map<String, Object> properties, StringBuffer buffer, boolean forceDefaultFormat)
	{
		return buffer.append(value);
	}

	public boolean equal(Object value1, Object value2)
	{
	    return Misc.equal(value1, value2);
	}
    public int compare(RuntimeContext context, Object obj1, Object obj2)
    {
        return 0;
    }
    public Object deepCopy(RuntimeContext context, Object original)
    {
        return original; // Assuming all leaf objects are immutable
    }


    public Object newInstance(RuntimeContext context, Object obj)
    {
        return obj;
    }


	public String toString(RuntimeContext context, Object o) {
		StringBuffer s = new StringBuffer();
		format(context, o, null,s, false);
		return s.toString();
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import tersus.model.ModelId;
import tersus.util.Misc;

/**
 * An object that represents a tuple of input values used as key to search for cached outputs.
 * 
 * @author Youval Bronicki
 *
 */
public class TriggerSet
{

	ModelId type;
	Object[] triggers;
	int hashValue;
	public TriggerSet(RuntimeContext context, FlowHandler flowHandler, FlowInstance flowInstance)
	{
		type = flowHandler.getModelId();
		hashValue = type.hashCode();
		triggers = new Object[flowHandler.triggers.length];
		for (int i = 0; i < flowHandler.triggers.length; i++)
		{
			triggers[i] = flowHandler.triggers[i].get(context, flowInstance);
			if (triggers[i] != null)
				hashValue += triggers[i].hashCode();
		}

	}

	public boolean equals(Object obj)
	{
		if (obj == null)
			return false;
		if (obj == this)
			return false;
		if (obj.getClass() != getClass())
			return false;
		TriggerSet other = (TriggerSet) obj;
		if (!type.equals(other.type))
			return false;
		return Misc.equal(triggers, other.triggers);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode()
	{
		return hashValue;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		StringBuffer buffer = new StringBuffer();
		buffer.append(type.getName());
		buffer.append('[');
		for (int i=0; i< triggers.length; i++)
		{
			if (i>0)
				buffer.append(',');
			buffer.append(triggers[i]);
		}
		buffer.append(']');
		return buffer.toString();
	}

}

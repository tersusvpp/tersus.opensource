/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

/**
 * Thrown when an error is encountered during execution of a model.
 * 
 * @author Youval Bronicki
 *  
 */
public class ModelExecutionException extends RuntimeException
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String details;

    public String getDetails()
    {
    	return details;
    }
    public ModelExecutionException(String message)
    {
        super(message);
    }

    public ModelExecutionException(String message, String details)
    {
        super(message);
        this.details = details;
    }
    public ModelExecutionException(String message, String details, Throwable e)
    {
        super(message,e);
        this.details = details;
    }
    public ModelExecutionException(String message, Throwable e)
    {
        super(message,e);
    }

    public String toString()
    {
        if (details == null)
            return getMessage();
        else
            return getMessage() + "\nDetails:\n" + details;
    }
}
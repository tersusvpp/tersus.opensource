/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.security.SecureRandom;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.Name;
import javax.naming.RefAddr;
import javax.naming.Reference;
import javax.naming.StringRefAddr;
import javax.naming.spi.ObjectFactory;

import tersus.util.InvalidInputException;

/**
 * An "proxy" ObjectFactory that knows how to decrypt a password.
 * 
 * Notes: (1)The encryption algoritm is not secure
 *        (2)The class was only tested in Tomcat. Not sure whether it would work
 *           in other J2EE containers. 
 * 
 * @author Youval Bronicki
 *
 */
public class HiddenPasswordFactory implements ObjectFactory
{

	/* (non-Javadoc)
	 * @see javax.naming.spi.ObjectFactory#getObjectInstance(java.lang.Object, javax.naming.Name, javax.naming.Context, java.util.Hashtable)
	 */
	public Object getObjectInstance(Object obj, Name name, Context nameCtx, Hashtable environment)
		throws Exception
	{
		Reference ref = (Reference) obj;
		Reference newRef = new Reference(ref.getClassName());

		String factoryClassName = null;
		//FUNC3 - perform a similar translation on the given environment
		for (Enumeration e = ref.getAll(); e.hasMoreElements();)
		{
			RefAddr ra = (RefAddr) e.nextElement();
			if ("realFactory".equals(ra.getType()))
				factoryClassName = (String) ra.getContent();
			else if ("java.naming.security.credentials".compareToIgnoreCase(ra.getType()) == 0 || "password".compareToIgnoreCase(ra.getType()) == 0)
			{
				String password = decrypt((String) ra.getContent());
				RefAddr newRA = new StringRefAddr(ra.getType(), password);
				newRef.add(newRA);
			}
			else
				newRef.add(ra);
		}
		if (factoryClassName == null)
			throw new InvalidInputException("No realFactory specified");
		ObjectFactory realFactory = (ObjectFactory) Class.forName(factoryClassName).newInstance();
		return realFactory.getObjectInstance(newRef, name, nameCtx, environment);

	}

	public static void main(String[] args) throws Exception
	{
		// quick way to do input from the keyboard, now deprecated...
		System.out.print("Input clear password : ");
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String clear = in.readLine().trim();
		String secret = new String(encrypt(clear));
		System.out.println("Clear password : " + clear);
		System.out.println("Hidden password : " + secret);
		if (! clear.equals(decrypt(secret)))
			System.out.println("ERROR: The result can't be decrypted");
	}

	public static String encrypt(String x) throws Exception
	{
		SecureRandom r = getRandom();
		byte[] bytes = x.getBytes("UTF8");
		StringBuffer sb = new StringBuffer(bytes.length * 2);
		for (int i=0; i<bytes.length; i++)
		{
			int v = ((bytes[i]&0xFF) + r.nextInt(256))&0xFF;
			if (v < 16)
			{
				sb.append('0');
			}
			sb.append(Integer.toHexString(v));
			
		}
		return sb.toString().toUpperCase();
	}

	private static String decrypt(String s) throws Exception
	{
		byte[] bytes = hexStringToByteArray(s);
		SecureRandom r = getRandom();
		for (int i=0; i<bytes.length; i++)
		{
			int v = ((bytes[i]&0xFF) - r.nextInt(256))&0xFF;
			bytes[i] = (byte)v;
		}
		return new String(bytes, "UTF8");
	}
	private static SecureRandom getRandom()
	{
		return new SecureRandom(new byte[] { 12,35,44,-56,88});
	}


	private static byte[] hexStringToByteArray(String s)
	{
		byte[] b = new byte[s.length() / 2];
		for (int i = 0; i < b.length; i++)
		{
			int index = i * 2;
			int v = Integer.parseInt(s.substring(index, index + 2), 16);
			b[i] = (byte) v;
		}
		return b;
	}
}

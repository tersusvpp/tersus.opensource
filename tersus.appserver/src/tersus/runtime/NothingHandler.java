/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.runtime;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

/**
 * @author Youval Bronicki
 *
 */
public class NothingHandler extends LeafDataHandler
{

    private static final String DB_NOTHING_LITTERAL = "*";

	/* (non-Javadoc)
     * @see tersus.runtime.InstanceHandler#newInstance(tersus.runtime.RuntimeContext, java.lang.String)
     */
    public NothingHandler()
    {
        super();
        setMaxLength(1);
    }
     public Object newInstance(RuntimeContext context, String valueStr, boolean forceDefaultFormat)
    {
        if (! Nothing.LABEL.equals(valueStr))
            throw new ModelExecutionException("Can't create an instance of 'Nothing' from String '"+valueStr+"'");
        return Nothing.NOTHING;
    }

    /* (non-Javadoc)
     * @see tersus.runtime.InstanceHandler#newInstance(tersus.runtime.RuntimeContext)
     */
    public Object newInstance(RuntimeContext context)
    {
        return Nothing.NOTHING;
    }

    /* (non-Javadoc)
     * @see tersus.runtime.InstanceHandler#compare(tersus.runtime.RuntimeContext, java.lang.Object, java.lang.Object)
     */
    public int compare(RuntimeContext context, Object obj1, Object obj2)
    {
        return 0;
    }

    /* (non-Javadoc)
     * @see tersus.runtime.InstanceHandler#isInstance(java.lang.Object)
     */
    public boolean isInstance(Object obj)
    {
        return obj instanceof Nothing;
    }

	@Override
	public Object fromSQL(ResultSet rs, int index) throws SQLException
	{
		 Object value = rs.getObject(index);
	     return fromSQL(value);
	}

	@Override
	protected Object fromSQL(Object obj)
	{
		
		return obj == null ? null : Nothing.NOTHING;
	}

	@Override
	public int getSQLType()
	{
		return Types.CHAR;
	}

	@Override
	public Object toSQL(Object value)
	{
		return value == null ? null : DB_NOTHING_LITTERAL;
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.runtime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Youval Bronicki
 *
 */
public class InputSet extends FlowInstance
{

    /**
     * @param handler
     */
    public InputSet(FlowHandler handler)
    {
        super(handler);
    }

    /**
     * Override "set" with "accumulate" (with input sets, we are always accumulating values)
     */
    public void set(RuntimeContext context, SlotHandler slot, Object value)
    {
        super.accumulate(context, slot, value);
    }

    
    /**
     * Create "concrete" instances by performing a cartesian product, and add the instances to the parent
     * run-queue
     * @param context
     */
    public void explode(RuntimeContext context)
    {
        List instances = new ArrayList();
        instances.add(new FlowInstance(handler));
        for (int i=0; i<handler.triggers.length; i++)
        {
            SlotHandler trigger = handler.triggers[i];
            List values = (List)trigger.get(context, this);
            if (values == null)
                values = Collections.EMPTY_LIST;
            int nPrototypes = instances.size();
            for (int j=0; j<nPrototypes; j++)
            {
                //TODO handle the case of repetitive triggers (no cartesian product is needed)
                FlowInstance prototype = (FlowInstance)instances.get(j);
                for (int k=0; k<values.size(); k++)
                {
                    Object value = values.get(k);
                    if (k==0)
                    {
                        trigger.set(context, prototype, value);
                    }
                    else
                    {
                        FlowInstance newInstance = new FlowInstance(handler);
                        newInstance.elements = new Object[this.elements.length];
                        System.arraycopy(prototype.elements, 0, newInstance.elements, 0, this.elements.length);                 
                        trigger.set(context, newInstance, value);
                        instances.add(newInstance);
                    }
                }
            }
        }
        for (int i=0; i<instances.size(); i++)
        {
            FlowInstance child = (FlowInstance) instances.get(i);
            child.parent = this.parent;
            child.subFlowHandler = this.subFlowHandler;
            handler.setInitialStatus(context, child);
        }
    }
}

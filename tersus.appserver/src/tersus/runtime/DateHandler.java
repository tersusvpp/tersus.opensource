/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.runtime;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DateFormat;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;

import tersus.InternalErrorException;
import tersus.model.BuiltinProperties;
import tersus.model.Model;

/**
 * A handler for a date leaf.
 * 
 * @author Ofer Brandes
 * 
 */
public class DateHandler extends LeafDataHandler
{
    private String formatString = null;
    /**
     * 
     * @param context
     * @param date
     *            The date value of the created instance (e.g. 29/07/2003).
     * @return
     */
    public Object newInstance(RuntimeContext context, Object obj)
    {
        if (obj instanceof Date)
            return obj;
        else
            throw new IllegalArgumentException(obj.getClass().getName()  + " is an illegal value for "+this.modelId);
    }

    /**
     * 
     * @param context
     * @param s
     *            A string representation of the date value of the created
     *            instance (e.g. "29/07/2003").
     * @return
     */
    public Object newInstance(RuntimeContext context, String s, Map<String, Object> elementProperties, boolean forceDefaultFormat)
    {
        if (s == null || s.length() == 0 || s.trim().length() == 0)
            return null;
        DateFormat format;
        String fmt = null;
        if (elementProperties != null)
            fmt = (String)elementProperties.get(BuiltinProperties.FORMAT);
        if (fmt == null)
            fmt = formatString;
        if (fmt == null)
            format = context.getDefaultDateFormat();
        else
            format = context.getDateFormat(fmt);
        try
        {
            Date date = format.parse(s);
            return date;
        }
        catch (ParseException e)
        {
            throw new IllegalArgumentException("Bad date format '" + s + "'");
        }
    }
    
    public Object newInstance(RuntimeContext context, String s, boolean forceDefaultFormat)
    {
        return newInstance(context, s, null, forceDefaultFormat);
    }

    public Object fromSQL(ResultSet rs, int index) throws SQLException
    {
        java.sql.Date sqlDate = rs.getDate(index);
        if (sqlDate == null)
            return null;
        return new java.util.Date(sqlDate.getTime());
    }
    public Object fromSQL(CallableStatement cs, int index) throws SQLException
    {
        java.sql.Date sqlDate = cs.getDate(index);
        if (sqlDate == null)
            return null;
        return new java.util.Date(sqlDate.getTime());
    }
    protected Object fromSQL(Object value)
    {
        throw new InternalErrorException("This method should not have been called");
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.DataHandler#getSQLType()
     */
    public int getSQLType()
    {
        return Types.DATE;
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.DataHandler#toSQL(java.lang.Object)
     */
    public Object toSQL(Object value)
    {
        if (value == null)
            return null;
        java.util.Date date = (java.util.Date) value;
        return new java.sql.Date(date.getTime());
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.LeafDataHandler#toString(tersus.runtime.RuntimeContext,
     *      java.lang.Object)
     */
    private DateFormat getFormat(RuntimeContext context, boolean forceDefaultFormat)
    {
        DateFormat format;
        if (formatString == null|| forceDefaultFormat)
            format = context.getDefaultDateFormat();
        else
            format = context.getDateFormat(formatString);
        return format;
    }
    
	public StringBuffer format(RuntimeContext context, Object value, Map<String, Object> properties, StringBuffer buffer, boolean forceDefaultFormat)
    {
         return getFormat(context, forceDefaultFormat).format((Date) value, buffer,DUMMY_FIELD_POSITION);
    }

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        formatString = (String) model.getProperty(BuiltinProperties.FORMAT);
    }

    public int compare(RuntimeContext context, Object obj1, Object obj2)
    {
        if (obj1 == null || obj2 == null)
            return compareNull(obj1, obj2);
        return ((Date) obj1).compareTo((Date) obj2);
    }

    public boolean isInstance(Object obj)
    {
        return obj instanceof Date;
    }
}

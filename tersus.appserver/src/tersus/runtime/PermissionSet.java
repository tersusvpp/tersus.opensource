package tersus.runtime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

public class PermissionSet
{
    private static PermissionSet empty = new PermissionSet(null);

    Set<String> permissions;

    int hashCode = 0;

    public PermissionSet(Set<String> permissions)
    {
        if (permissions == null )
            this.permissions = Collections.emptySet();
        else
            this.permissions = permissions;

        calculateHashCode();
    }

    private void calculateHashCode()
    {
        ArrayList<String> list = new ArrayList<String>();
        list.addAll(permissions);
        Collections.sort(list);
        hashCode = 0;
        for (String permission: list)
        {
            hashCode = 31 * hashCode + permission.hashCode();
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj instanceof PermissionSet)
        {
            PermissionSet other = (PermissionSet) obj;
            return other.hashCode() == hashCode()
                    && other.permissions.equals(permissions);
        }
        return false;
    }

    @Override
    public int hashCode()
    {
        return hashCode;
    }

    public Set<String> getSet()
    {
        return permissions;
    }

    public static PermissionSet empty()
    {
        return empty ;
    }

}

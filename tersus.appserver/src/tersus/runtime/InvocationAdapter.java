package tersus.runtime;


public interface InvocationAdapter
{
    void setTriggers(RuntimeContext context, FlowInstance serviceInstance, SlotHandler[] triggers);
    public String getLogType();
    boolean needsAccessValidation();
}

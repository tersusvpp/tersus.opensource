/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import tersus.model.BuiltinModels;
import tersus.model.BuiltinProperties;
import tersus.model.DataType;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.util.Misc;
import tersus.util.NotImplementedException;

/**
 * An InstanceHandler for composite data types.
 * 
 * 
 * Composite data values are represented as simple arrays of objects,
 * where each position in the array corresponds to an element of the data type.
 * 
 * This representation is very compact (a single object), and 
 * allows very efficient lookup by role/path (each element is mapped to 
 * a fixed integer which is the index to the table.  This index is 
 * calculated when handlers are loaded, and is kept on the corresponding 
 * element handler.
 * 
 * 
 * @author Youval Bronicki
 *
 */
public class CompositeDataHandler extends DataHandler
{

	public String tableName;
	public String tableType;
	public String viewDefinition;
	private PersistenceHandler persistenceHandler;
    private FieldList fields;

	/* (non-Javadoc)
	 * @see tersus.runtime.IDataHandler#newInstance()
	 */
	public Object newInstance(RuntimeContext context)
	{
		Object[] instance = new Object[nElements + 1];
		instance[0] = getModelId();
		return instance;
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.InstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		tableName = (String) ((DataType) model).getProperty(BuiltinProperties.TABLE_NAME);
        viewDefinition = (String) ((DataType) model).getProperty(BuiltinProperties.VIEW_DEFINITION);
		tableType = (String) ((DataType) model).getProperty(BuiltinProperties.TABLE_TYPE);
		if (isPersistent())
		{
			persistenceHandler = new PersistenceHandler();
			persistenceHandler.initialize(this, getLoader().useQuotedIdentifiers());
		}
	}

	/**
	 * @return The number of elements in this handlers data models
	 */
	public int getNumberOfElements()
	{
		return nElements;
	}

	/**
	 * Returns the i'th elementHandler of this data handler
	 * @param i the index ( 0, 1, ...)
	 */
	public ElementHandler getElementHandler(int i)
	{
		return elementHandlers[i];
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.InstanceHandler#isPersistent()
	 */
	public boolean isPersistent()
	{
		return tableName != null;
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.InstanceHandler#getPeristenceHandler()
	 */
	public PersistenceHandler getPeristenceHandler()
	{
		return persistenceHandler;
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.DataHandler#newInstance(tersus.runtime.RuntimeContext, java.lang.String)
	 */
	public Object newInstance(RuntimeContext context, String valueStr, boolean forceDefaultFormat)
	{
		throw new UnsupportedOperationException("Deserialization not yet implemented");
	}


	public boolean isHandlerOf(Object instance)
	{
		return getModelId().equals(ModelIdHelper.getCompositeModelId(instance));
	}
	/* (non-Javadoc)
	 * @see tersus.runtime.InstanceHandler#initDB(tersus.runtime.RuntimeContext)
	 */
	public void initDB(RuntimeContext context)
	{
		
		if (isPersistent())
		{
			persistenceHandler.ensureTableExists(context);
		}
	}
    public String checkDB(RuntimeContext context)
    {
        
        if (isPersistent())
        {
            if (! SQLUtils.tableExists(context, tableName))
                return "Missing table " +tableName;

            List missing = persistenceHandler.getMissingColumns(context);
            if (missing.isEmpty())
                return null;
            else
            {
                boolean useQuotedIdentifiers = getLoader().useQuotedIdentifiers();
                List missingColumnNames = new ArrayList();
                for (Iterator i= missing.iterator(); i.hasNext();)
                {
					missingColumnNames.add(((ColumnDescriptor)i.next()).getColumnName(useQuotedIdentifiers));
                }
                return "Missing columns in table "+tableName+ " : "+Misc.concatenateList(missingColumnNames, " , ");
            }
            
        }
        else
            return null;
    }

	/* (non-Javadoc)
	 * @see tersus.runtime.DataHandler#toSQL(java.lang.Object)
	 */
	public Object toSQL(RuntimeContext context, Object value)
	{
		throw new UnsupportedOperationException("Non-persistent composite value used as query parameter");
	}

    public int compare(RuntimeContext context, Object obj1, Object obj2)
	{
	    return compareComposite(context, obj1, obj2);

}

    /**
     * @param obj1
     * @return
     */
    public boolean isInstance(Object obj)
    {
        return getModelId().equals(ModelIdHelper.getCompositeModelId(obj));
    }
    
    synchronized public FieldList getFields()
    {
        if (fields == null)
            fields = new FieldList(this);
        return fields;
    }
}

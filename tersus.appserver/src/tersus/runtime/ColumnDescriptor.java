/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.sql.Types;

import tersus.model.ElementPath;
import tersus.model.Path;
import tersus.util.Misc;
import tersus.util.NotImplementedException;
import tersus.util.SpecialRoles;

/**
 * NICE2 [Description]
 * 
 * @author Youval Bronicki
 *
 */
public abstract class ColumnDescriptor
{
	/**
	 * @param handler
	 * @param path
	 */
	
	/** A classification of column types for query support */

	public static final String TEXT="Text";
	public static final String NUMBER="Number";
	public static final String DATE="Date";
	public static final String DATE_AND_TIME="Date and Time";
	public static final String OTHER="Other";
		
	private String type = OTHER;
	public ColumnDescriptor(PersistenceHandler owner, Path path)
	{
		this.owner = owner;
		setElementHandler(owner.getDataHandler().getElementHandler(path));
		dataHandler = (LeafDataHandler) getElementHandler().getChildInstanceHandler();
		if (dataHandler instanceof TextHandler)
			setType(TEXT);
		else if (dataHandler instanceof NumberHandler)
			setType(NUMBER);
		else if (dataHandler instanceof DateHandler)
			setType(DATE);
		else if (dataHandler instanceof DateAndTimeHandler)
			setType(DATE_AND_TIME);
		this.path = path;
		this.pathHandler = new PathHandler(owner.getDataHandler(), path);
	}
	
	private PersistenceHandler owner;
	private String sqlizedColumnsName;
	private String quotedColumnName;
	private String displayName;
	private LeafDataHandler dataHandler;
	private ElementPath path;
	protected PathHandler pathHandler;

	private ElementHandler elementHandler;
	/**
	 * Retruns the SQL DDL clause for creating this column (e.g. DATE, VARCHAR(254), etc.)
	 * @param context The runtime context for this operation (the runtime context is required in order to determined the database type) 
	 * @return A String containing the DDL portion
	 */
	public String getDDL(RuntimeContext context)
	{
		//FUNC2 use DB specific behavior
		DatabaseAdapter dbAdapter = context.getDefaultDBAdapter();
		if (dataHandler instanceof NumberHandler && elementHandler.isDatabaseGenerated())
			return dbAdapter.getIdentity();
		switch (dataHandler.getSQLType())
		{
			case Types.DOUBLE :
				return dbAdapter.getDouble();
			case Types.VARCHAR :
				return dbAdapter.getVarchar(((LeafDataHandler)dataHandler).getMaxLength());
            case Types.CHAR :
                return dbAdapter.getChar(((LeafDataHandler)dataHandler).getMaxLength());
			case Types.DATE :
				return dbAdapter.getDate();
			case Types.TIMESTAMP :
				return dbAdapter.getDateTime();
			case Types.LONGVARBINARY :
				return dbAdapter.getLongVarbinary();
			case Types.BOOLEAN:
			    return dbAdapter.getBoolean();
			case Types.INTEGER:
				return dbAdapter.getInt();
			default :
				throw new NotImplementedException(
					"SQL Type " + dataHandler.getSQLType() + " Not supported yet (see java.SQL.Types for values)");
		}
	}

	public ElementPath getPath()
	{
		return path;
	}
	/**
	 * Sets the display name column name (currently based on the path) 
	 */
	protected void setNames()
	{
//		displayName = getDisplayName(columnDisplayName(pathHandler));
		displayName = columnDisplayName(pathHandler);
		sqlizedColumnsName = Misc.sqlize(displayName);
		quotedColumnName = Misc.quote(displayName);
	}

	public static String columnDisplayName (PathHandler topHandler)
	{
		int includedSegments = 0;
		StringBuffer name = new StringBuffer();

		for (int i = 0; i < topHandler.segments.length; i++)
		{
			ElementHandler handler = topHandler.getSegment(i);
			String segment = handler.getRole().toString();
			if (handler.isExcludedFromFieldName())
				continue;

			if (includedSegments > 0)
				name.append(' ');

			if (handler.explicitColumnName != null)
			{
				name.append(handler.explicitColumnName);
			}
			else if (segment.startsWith(SpecialRoles.DEPENDENT_MODEL_ROLE_PREFIX))
				name.append(segment.substring(1));
			else
				name.append(segment);

			++ includedSegments;
		}

		return name.toString();
	}

	/**
	 * @param name
	 * @return
	 * @deprecated Was used in the SAP solutions - causes problems ("ColumnName" is changed to "Column Name")
	 */
	private String getDisplayName(String name)
	{
		StringBuffer translated = new StringBuffer();
		for (int i = 0; i < name.length(); i++)
		{
			char c = name.charAt(i);
			if (Character.isLetterOrDigit(c))
			{
				translated.append(c);
				if (Character.isLowerCase(c)
					&& i < name.length() - 1
					&& Character.isUpperCase(name.charAt(i + 1)))
					translated.append(' ');
			}
			else
				translated.append(' ');
		}

		return translated.toString();
	}

	/**
	 * @return
	 */
	public String getColumnName(boolean useQuotedIdentifiers)
	{
		return useQuotedIdentifiers ? quotedColumnName : sqlizedColumnsName;
	}

	public String getQuotelessColumnName(boolean useQuotedIdentifiers)
	{
		return useQuotedIdentifiers ? displayName : sqlizedColumnsName;
	}
	/**
	 * @return
	 */
	public String getDisplayName()
	{
		return displayName;
	}

	/**
	 * @return
	 */
	public PersistenceHandler getOwner()
	{
		return owner;
	}

	/**
	 * 
	 */
	public DataHandler getDataType()
	{
		return dataHandler;

	}

	/**
	 * Assignes a given value to the relevant field in an instnace
	 * @param context The runtime context of the operation
	 * @param instance The instance to be populated (an instance of the owner DataHandler)
	 * @param value The value of the column 
	 */
	public void setRawValue(RuntimeContext context, Object instance, Object value)
	{

		pathHandler.set(context, instance, value);
	}

	/**
	 * Retrieves the value of a column in a persistent value. 
	 * @param context The RuntimeContext of the operation 
	 * @param input The object from which the value should be retrieved.  The input is an instance
	 * of the owner data type of this column
	 * @return an Object containing the value (converted if necessary to the appropriate
	 * SQL value
	 */
	public Object getSQLValue(RuntimeContext context, Object input)
	{
		//FUNC2 handle type conversions (date, number, etc.)
		Object value = getRawValue(context, input);
		return dataHandler.toSQL(value);
	}

	public Object getRawValue(RuntimeContext context, Object input)
	{
		return pathHandler.get(context, input);
	}

	public boolean isPrimaryKey()
	{
		return getElementHandler().isPrimaryKey();
	}

	/**
	 * @return
	 */
	public LeafDataHandler getDataHandler()
	{
		return dataHandler;
	}

	/**
	 * @return
	 */
	public int getSQLType()
	{
		return getDataHandler().getSQLType();
	}

	private void setElementHandler(ElementHandler elementHandler)
	{
		this.elementHandler = elementHandler;
	}

	public ElementHandler getElementHandler()
	{
		return elementHandler;
	}


	/**
	 * @return
	 */
	public String getType(RuntimeContext context)
	{
			return type;
	}

	/**
	 * @param string
	 */
	public void setType(String string)
	{
		type = string;
	}


}

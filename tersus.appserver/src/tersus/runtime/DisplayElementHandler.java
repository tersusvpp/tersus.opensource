/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

/**
 * This class represents DisplayElements - which may be 'present' on the server side
 * as data elements (representing display data that it being sent between server and client).
 * 
 * A separate class was needed here in order to overrride the 'stateless' behavior of repetitive SubFlows.
 *  
 * @author Youval Bronicki
 *
 */
public class DisplayElementHandler extends SubFlowHandler
{

    /**
     * @param parentHandler
     */
    public DisplayElementHandler(FlowHandler parentHandler)
    {
        super(parentHandler);
    }

    public Object create(RuntimeContext context, Object parent)
    {
		Object value = getChildInstanceHandler().newInstance(context);
		if (isRepetitive())
			accumulate(context, parent, value);
		else
			set(context, parent, value);
		return value;
    }
}

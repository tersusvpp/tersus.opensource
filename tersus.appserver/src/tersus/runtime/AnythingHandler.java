/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.runtime;

import tersus.model.ModelId;

/**
 * @author Youval Bronicki
 *
 */
public class AnythingHandler extends CompositeDataHandler
{

    /* (non-Javadoc)
     * @see tersus.runtime.InstanceHandler#newInstance(tersus.runtime.RuntimeContext, java.lang.String)
     */
    public Object newInstance(RuntimeContext context, String valueStr, boolean forceDefaultFormat)
    {
        throw new ModelExecutionException("Can't create an instance of Anything ("+getModelId()+")");
    }

    /* (non-Javadoc)
     * @see tersus.runtime.InstanceHandler#newInstance(tersus.runtime.RuntimeContext)
     */
    public Object newInstance(RuntimeContext context)
    {
        throw new ModelExecutionException("Can't create an instance of Anything ("+getModelId()+")");
    }

    /* (non-Javadoc)
     * @see tersus.runtime.InstanceHandler#compare(tersus.runtime.RuntimeContext, java.lang.Object, java.lang.Object)
     */
    public int compare(RuntimeContext context, Object obj1, Object obj2)
    {
    	if (obj1 == null && obj2 == null)
    		return 0;
    	if (obj1 == null)
    		return 1;
    	if (obj2 == null)
    		return -1;
    	ModelId id1 = getModelIdEx(obj1);
    	ModelId id2 = getModelIdEx(obj2);
    	
    	if (id1 == null)
    		throw new ModelExecutionException("Can't compare instanceof of "+obj1.getClass().getName());
    	if (!id1.equals(id2))
    		throw new ModelExecutionException("Can't compare elements of different types  (types='"+id1+"' type2='"+id2+"')");
    	InstanceHandler h = context.getModelLoader().getHandler(id1);
        return h.compare(context, obj1, obj2);
    }

    /* (non-Javadoc)
     * @see tersus.runtime.InstanceHandler#isInstance(java.lang.Object)
     */
    public boolean isInstance(Object obj)
    {
        return obj != null && ! (obj instanceof Nothing);
    }

}

/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/

package tersus.runtime;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

/**
 * Contains information about Microsoft SQL Server database column types
 * 
 * @author Youval Bronicki
 * 
 */
public class HSQLDBAdapter implements DatabaseAdapter
{
	static int id=0;
	synchronized static int nextId()
	{
		return ++id;
	}
	public boolean test(RuntimeContext context)
	{
		return true;
	}

	private static final String DATABASE_PRODUCT_NAME = "HSQL Database Engine";

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.DatabaseTypeAdapter#getDouble()
	 */
	public String getDouble()
	{
		return "DOUBLE";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.DatabaseTypeAdapter#getVarchar()
	 */
	public String getVarchar(int maxLength)
	{
		if (maxLength == 0)
			maxLength = 32768;
		return "VARCHAR(" + maxLength + ")";
	}

	public String getChar(int maxLength)
	{
		return "CHAR";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.DatabaseTypeAdapter#getDate()
	 */
	public String getDate()
	{
		return "DATETIME";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.DatabaseTypeAdapter#getLongVarbinary()
	 */
	public String getLongVarbinary()
	{
		return "LONGVARBINARY";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.DatabaseAdapter#translateMessage(java.lang.Throwable)
	 */
	public String translateMessage(SQLException e)
	{
		return e.getMessage();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.DatabaseAdapter#getTableTypeDDL(java.lang.String)
	 */
	public String getTableTypeDDL(String tableType)
	{
		return "";
	}

	public String getIdentity()
	{
		return "INTEGER GENERATED ALWAYS AS IDENTITY(START WITH 1)";
	}

	public boolean useColumnNamesForDBGenereatedColumns()
	{
		return false;
	}

	public String getExceptionType(SQLException e)
	{
		switch (e.getErrorCode())
		// See http://dev.mysql.com/doc/connector/j/en/apb.html
		{
			case -104:
				return (DUPLICATE_KEY); // ER_DUP_ENTRY
			default:
				return (e.getMessage());
		}
	}

	public void shutdown(RuntimeContext context)
	{
		Statement stmt = null;
		try
		{
			stmt = context.getDefaultConnection().createStatement();
			stmt.execute("SHUTDOWN");
			stmt.close();
			stmt = null;
		}
		catch (SQLException e)
		{
			throw new EngineException("Failed to shutdown database", "", e);
		}
		finally
		{
			if (stmt != null)
			{
				try
				{
					stmt.close();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					// Secondary Exception Ignored
				}
			}
		}
	}

	public String convertTableName(String tableName)
	{
		return tableName.toUpperCase();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.DatabaseAdapter#alterTableSupportsMultipleColumns()
	 */
	public boolean alterTableSupportsMultipleColumns()
	{
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.DatabaseAdapter#getBoolean()
	 */
	public String getBoolean()
	{
		return "CHAR";
	}

	public String getDatabaseProductName()
	{
		return DATABASE_PRODUCT_NAME;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.DatabaseAdapter#getInt()
	 */
	public String getInt()
	{
		return "INTEGER";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.DatabaseAdapter#getDateTime()
	 */
	public String getDateTime()
	{
		return getDate();
	}

	public boolean canResumeAfterExceptions()
	{
		return true;
	}

	public String getStoredProcedureClause(String parameterName)
	{
		return parameterName + "=?";
	}

	public String prepareSelectStatement(StringBuffer columns, StringBuffer table,
			String primaryKey, StringBuffer condition, String orderBy, int numberOfRecords,
			int offset, Set<String> optimizations)
	{
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT ");
		if (numberOfRecords > 0 || offset > 0)
		{
			sql.append("LIMIT ");
			sql.append(offset);
			sql.append(" ");
			sql.append(numberOfRecords);
			sql.append(" ");
		}
		sql.append(columns);
		sql.append(" FROM ");
		sql.append(table);
		if (condition.length() > 0)
		{
			sql.append(" WHERE ");
			sql.append(condition);
		}
		if (orderBy != null && orderBy.length() > 0)
		{
			sql.append(" ORDER BY ");
			sql.append(orderBy);
		}
		return sql.toString();
	}

	public boolean supportsQueryPaging()
	{
		return true;
	}

	public String getSelectForUpdate(String column, String table, String condition)
	{
		return "SELECT " + column + " FROM " + table + " WHERE " + condition; // "For update" not
																				// supported by
																				// HSQLDB
	}

	public boolean requiresPadding()
	{
		return false;
	}

	public boolean supportsQueryTimeout()
	{
		return true;
	}

	public String getConectionId(Connection c, RuntimeContext context)
	{
		return "HSQLDB Connection #"+nextId();
	}

	public String prepareCountStatement(StringBuffer table, StringBuffer condition,
			HashSet<String> optimizations)
	{
		String sql = "SELECT count(1) FROM " + table;
		if (condition.length() > 0)
			sql += " WHERE " + condition;
		return sql;
	}
	public boolean commit(Connection c, RuntimeContext context) throws SQLException
	{
		if (c.getAutoCommit())
			return false;
		c.commit();
		return true;
	}
	public boolean rollback(Connection c, RuntimeContext context) throws SQLException
	{
		if (c.getAutoCommit())
			return false;
		c.rollback();
		return true;
	}
}
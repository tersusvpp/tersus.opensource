/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

/**
 * @author Youval Bronicki
 *
 */
public class PermissionDeniedException extends EngineException
{

	private String permission;
	/**
	 * @param permission
	 */
	public PermissionDeniedException(RuntimeContext context, String permission)
	{
		super("User lacks required permission.", "The missing permission is '"+permission+"'", null);
		setPermission(permission);
	}
	/**
	 * @return
	 */
	public String getPermission()
	{
		return permission;
	}

	/**
	 * @param string
	 */
	public void setPermission(String string)
	{
		permission = string;
	}

}

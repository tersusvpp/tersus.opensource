/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.runtime;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.zip.CRC32;

import tersus.model.ModelException;
import tersus.model.Path;
import tersus.util.Misc;

/**
 * Handles persistence of data values
 * 
 * 
 * This class should be retired sometime - use the leaner FieldList on one hand,
 * and cache SQL strings on the plugins (Insert, Update).
 * 
 * @author Youval Bronicki
 * @deprecated Transitioning to FieldList
 */
public class PersistenceHandler
{
    private String tableName;

    private String tableType;

    private static final int PK = 1;

    private static final int NON_PK = 2;

    private static final boolean DEBUG = false;

    private List<ColumnDescriptor> columns = new ArrayList<ColumnDescriptor>();

    private HashMap<String, ColumnDescriptor> columnMap = new HashMap<String, ColumnDescriptor>();

    private List<ColumnDescriptor> primaryKeyColumns = new ArrayList<ColumnDescriptor>();


    private CompositeDataHandler dataHandler;

    private String primaryKeyFilterSQL;

	private boolean useQuotedIdentifiers = false;

	private String ddlTableName;

  
    public void initialize(CompositeDataHandler dataHandler, boolean useQuotedIdentifiers)
    {
        this.dataHandler = dataHandler;
        this.useQuotedIdentifiers  = useQuotedIdentifiers;
        tableName = useQuotedIdentifiers ? dataHandler.tableName : Misc.sqlize(dataHandler.tableName);
        ddlTableName = useQuotedIdentifiers ? Misc.quote(tableName) : Misc.sqlize(tableName);
        tableType = dataHandler.tableType;

        columns.clear();
        extractPrimaryKeyColumns(dataHandler);
        extractNonPrimaryKeyColumns(dataHandler);
        if (primaryKeyColumns.size() == 0)
            primaryKeyColumns.add(columns.get(0));
        initPrimaryKeyFilterSQL();
        initColumnMap();
    }

    /**
     * 
     */
    private void initColumnMap()
    {
        for (int i = 0; i < columns.size(); i++)
        {
            ColumnDescriptor column = columns.get(i);
            columnMap.put(column.getColumnName(useQuotedIdentifiers), column);
        }
    }

    public ColumnDescriptor getColumnDescriptorByColumnName(String columnName)
    {
        return columnMap.get(columnName);
    }

    /**
     * @param dataHandler
     */
    private void extractNonPrimaryKeyColumns(CompositeDataHandler dataHandler)
    {
        extractColumns(new Path(), dataHandler, NON_PK);

    }

    private void extractPrimaryKeyColumns(CompositeDataHandler dataHandler)
    {
        extractColumns(new Path(), dataHandler, PK);
        primaryKeyColumns.addAll(columns);
    }

    /**
     * Initilizes the sql string used to load instances from the database
     */
    private void initPrimaryKeyFilterSQL()
    {
        StringBuffer sql = new StringBuffer();
        ArrayList<LeafDataHandler> parameterHandlers = new ArrayList<LeafDataHandler>(primaryKeyColumns.size());
        for (int i = 0; i < primaryKeyColumns.size(); i++)
        {
            ColumnDescriptor keyColumn = primaryKeyColumns.get(i);
            if (i > 0)
                sql.append(" AND ");
            sql.append(keyColumn.getColumnName(useQuotedIdentifiers));
            sql.append("= ?");
            parameterHandlers.add(keyColumn.getDataHandler());
        }
        primaryKeyFilterSQL = sql.toString();
    }

    /**
     * @return A new StringBuffer that contains an SQL SelectStatement for
     *         reading the columns of this PersistenceHandler's data type. The
     *         returned statement does not contain a where clause;
     */
    protected StringBuffer getSelectStatement()
    {
        StringBuffer sql = new StringBuffer("SELECT ");

        for (int i = 0; i < columns.size(); i++)
        {
            ColumnDescriptor column = columns.get(i);
            if (i > 0)
                sql.append(", ");
            sql.append(column.getColumnName(useQuotedIdentifiers));
        }
        sql.append(" FROM " + ddlTableName);
        return sql;
    }


    private void extractColumns(Path path, InstanceHandler handler, int mode)
    {
        if (Misc.ASSERTIONS)
            Misc.assertion(handler != null);
        if (handler instanceof LeafDataHandler)
        {
            ColumnDescriptor column = new SimpleColumnDescriptor(this, path);
            addColumn(column, mode);
            return;
        }
        else
        {

            for (int i = 0; i < handler.nElements; i++)
            {
                ElementHandler childElement = handler.elementHandlers[i];
                InstanceHandler childHandler = childElement.getChildInstanceHandler();
                Path p = path.getCopy();
                p.addSegment(childElement.getRole());
                extractColumns(p, childHandler, mode);
            }
        }
    }

    /**
     * Adds a column to the list of columns, depending on a PK/NON_PK mode
     * 
     * In PK mode, primary key columns are added and non-primary key columns are
     * ignored In NON_PK mode, non primary key columns are added and primary key
     * columns are ignored
     * 
     * @param column
     *            the column to be added
     * @param mode
     *            the mode - eithet PK or NON_PK
     */
    private void addColumn(ColumnDescriptor column, int mode)
    {
        if (mode == PK)
        {
            if (column.isPrimaryKey())
            {
                if (DEBUG)
                    System.out.println("Adding PK column " + column.getColumnName(useQuotedIdentifiers) + " to " + tableName);
                columns.add(column);
            }
        }
        else
        {
            if (Misc.ASSERTIONS)
                Misc.assertion(mode == NON_PK);
            if (!column.isPrimaryKey())
            {
                if (DEBUG)
                    System.out.println("Adding NON_PK column " + column.getColumnName(useQuotedIdentifiers) + " to " + tableName);
                columns.add(column);
            }
        }
    }

    /**
     * @return
     */
    public List<ColumnDescriptor> getColumnDescriptors()
    {
        return columns;
    }

    /**
     * @return
     */
    public List<ColumnDescriptor> getPrimaryKeyColumns()
    {
        return primaryKeyColumns;
    }

    /**
     * @return
     */
    public String getTableName()
    {
        return tableName;
    }

    /**
     * @param rs
     * @return
     */

    protected Object loadInstance(RuntimeContext context, ResultSet rs) throws SQLException
    {
        Object instance = dataHandler.newInstance(context);
        Object[] keys = new Object[primaryKeyColumns.size()];
        for (int i = 0; i < columns.size(); i++)
        {
            ColumnDescriptor column = columns.get(i);
            Object value = column.getDataHandler().fromSQL(rs, i + 1);
            if (i < primaryKeyColumns.size())
            {
                keys[i] = value;
            }
            if (value != null)
                column.setRawValue(context, instance, value);
        }

        return instance;
    }

    /**
     * @return
     */
    public DataHandler getDataHandler()
    {
        return dataHandler;
    }

    /**
     * Ensures that a table exits for a given CompositeFlowHandler, creating one
     * if necessary.
     * 
     * FUNC3 If some fields are missing in the table, add missing field.
     * 
     * @param handler
     *            A CompositeFlowHandler
     */
    public void ensureTableExists(RuntimeContext context)
    {

        try
        {
            if (SQLUtils.tableExists(context, tableName))
            {
                updateTable(context);
            }
            else
            {
                String viewDef = dataHandler.viewDefinition;
                if (viewDef == null)
                    createTable(context);
                else
                    createView(context, viewDef);
            }
            return;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            throw new EngineException("Failed to create table " + tableName, null, e);
        }

    }

    private void createView(RuntimeContext context, String viewDef)
    {
        StringBuffer sql = new StringBuffer("CREATE VIEW " + ddlTableName + " AS " + viewDef);
        context.executeUpdate(sql.toString(),true);

    }

    private void alterView(RuntimeContext context, String viewDef)
    {
        context.executeUpdate("DROP VIEW "+ddlTableName, true);
        createView(context, viewDef);

    }

    private void createTable(RuntimeContext context)
    {

        StringBuffer sql = new StringBuffer("CREATE TABLE " + ddlTableName + " (");
        for (int i = 0; i < columns.size(); i++)
        {
            ColumnDescriptor column = columns.get(i);
            if (i > 0)
                sql.append(",");
            String type = column.getDDL(context);
            sql.append(column.getColumnName(useQuotedIdentifiers) + " " + type);
            if (column.isPrimaryKey())
                sql.append(" NOT NULL");
        }
        if (primaryKeyColumns.size() > 0)
        {
            sql.append(", CONSTRAINT " + getPkConstraintName() + " PRIMARY KEY (");
            for (int i = 0; i < primaryKeyColumns.size(); i++)
            {
                ColumnDescriptor column = columns.get(i);
                if (i > 0)
                    sql.append(",");
                sql.append(column.getColumnName(useQuotedIdentifiers));
            }
            sql.append(")");
        }
        sql.append(")");
        DatabaseAdapter dbAdapater = context.getDefaultDBAdapter();
        sql.append(" ");
        sql.append(dbAdapater.getTableTypeDDL(tableType));
        context.executeUpdate(sql.toString(), true);
    }

    /**
     * @return
     */
    Random r = new Random(System.nanoTime());
    private String getPkConstraintName()
    {
        return "DEFAULT_PK_"+r.nextInt(1000000);
    }

    private void updateTable(RuntimeContext context) throws SQLException
    {
        List<ColumnDescriptor> missingColumns = getMissingColumns(context);

        if (missingColumns.size() > 0)
        {
            if (dataHandler.viewDefinition == null)
                addColumns(context, missingColumns);
            else
                alterView(context, dataHandler.viewDefinition);
        }

    }

    private void addColumns(RuntimeContext context, List<ColumnDescriptor> columns)
    {
        if (context.getDefaultDBAdapter().alterTableSupportsMultipleColumns())
        {
            StringBuffer sql = createAddColumnsStatement(context, columns);
            context.executeUpdate(sql.toString(), true);
        }
        else
        {
            for (int i = 0; i < columns.size(); i++)
            {
                StringBuffer sql = createAddColumnsStatement(context, columns.subList(i, i + 1));
                context.executeUpdate(sql.toString(), true);

            }
        }
    }

    public List<ColumnDescriptor> getMissingColumns(RuntimeContext context)
    {
        List<String> tableColumns = SQLUtils.getColumns(context, tableName);
        List<ColumnDescriptor> missingColumns = new ArrayList<ColumnDescriptor>();
        for (int i = 0; i < columns.size(); i++)
        {
            ColumnDescriptor column = columns.get(i);
            if (!tableColumns.contains(column.getQuotelessColumnName(useQuotedIdentifiers).toUpperCase()))
            {
                missingColumns.add(column);
            }
        }
        return missingColumns;
    }

    private StringBuffer createAddColumnsStatement(RuntimeContext context, List<ColumnDescriptor> missingColumns)
    {
        StringBuffer sql = new StringBuffer("ALTER TABLE " + ddlTableName + " ");
        for (int i = 0; i < missingColumns.size(); i++)
        {

            ColumnDescriptor column = (ColumnDescriptor) missingColumns.get(i);
            if (i > 0)
                sql.append(",");
            String type = column.getDDL(context);
            sql.append(" ADD " + column.getColumnName(useQuotedIdentifiers) + " " + type);
        }
        return sql;
    }

    public ColumnDescriptor getColumnDescriptor(String name)
    {
        // Full path Match
        for (int i = 0; i < columns.size(); i++)
        {
            ColumnDescriptor column = columns.get(i);
            if (column.getPath().toString().equals(name))
                return column;
        }
        String sqlName = useQuotedIdentifiers ? name : Misc.sqlize(name);
        // If no full path matches, match by column Name (Youval, 8/1/06)
        ColumnDescriptor matching = null;
        for (int i = 0; i < columns.size(); i++)
        {
            ColumnDescriptor column = columns.get(i);
            if (column.getQuotelessColumnName(useQuotedIdentifiers).equals(sqlName))
            {
                if (matching == null)
                    matching = column;
                else
                    throw new ModelException("Ambigous columns name " + name + " in " + getDataHandler().getModelId());
            }
        }
        return matching;
    }

    public String getPrimaryKeyFilterSQL()
    {
        return primaryKeyFilterSQL;
    }
}
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.runtime;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Map;

import tersus.model.BuiltinProperties;
import tersus.util.Misc;
import tersus.util.NumberUtil;
import tersus.util.SQLUtils;

/**
 * A handler for a numeric leaf.
 * 
 * @author Ofer Brandes
 * 
 */
public class NumberHandler extends LeafDataHandler
{
	/**
	 * 
	 * @param context
	 * @param n
	 *            The numeric value of the created instance (e.g. 10.7).
	 * @return
	 */
	public Object newInstance(RuntimeContext context, double n)
	{
		return new Number(n);
	}

	public Object newInstance(RuntimeContext context, String valueStr,
			Map<String, Object> properties, boolean forceDefaultFormat)
	{
		String formatString = properties == null || forceDefaultFormat ? null : (String)properties.get(BuiltinProperties.FORMAT);
		if (formatString == null)
			return newInstance(context, valueStr, forceDefaultFormat);
		 NumberFormat format = context.getNumberFormat(formatString);
		 try
		 {
		 return new Number(format.parse(valueStr).doubleValue());
		 }
		 catch (ParseException e)
		 {
			 throw new NumberFormatException("Invalid number "+valueStr+ " for format:"+formatString);
		 }
		
	}

	/**
	 * 
	 * @param context
	 * @param s
	 *            A string representation of the numeric value of the created instance (e.g.
	 *            "10.7").
	 * @return
	 */
	public Object newInstance(RuntimeContext context, String s, boolean forceDefaultFormat)
	{

		if (s == null || s.trim().length() == 0)
			return null;
		Number n = new Number(Double.parseDouble(s));
		return n;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.DataHandler#toSQL(java.lang.Object)
	 */
	public Object toSQL(Object value)
	{
		if (value == null)
			return null;
		if (Misc.ASSERTIONS)
			Misc.assertion(value instanceof Number);
		if (SQLUtils.isIntegralType(getSQLType()))
			return Integer.valueOf((int)Math.round(((Number) value).value));
		return new Double(((Number) value).value);
	}

	public Object fromSQL(ResultSet rs, int index) throws SQLException
	{
		Object value = rs.getObject(index);
		return fromSQL(value);
	}

	protected Object fromSQL(Object value)
	{
		if (value == null)
			return null;
		if (value instanceof Boolean) // bit 
		{
			return new Number(((Boolean)value).booleanValue() ? 1 : 0);
		}
		java.lang.Number valueNumber = (java.lang.Number) value;
		return new Number(valueNumber.doubleValue());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.DataHandler#getSQLType()
	 */
	public int getSQLType()
	{
		return sqlType != null ? sqlType : Types.DOUBLE;
	}

	public boolean equal(Object value1, Object value2)
	{
		if (value1 == null)
			return (value2 == null);
		else if (value2 == null)
			return value1 == null;
		else
		{
			double number1 = ((Number) value1).value;
			double number2 = ((Number) value2).value;
			return NumberUtil.essentiallyEqual(number1, number2);
		}
	}

	public int compare(RuntimeContext context, Object obj1, Object obj2)
	{
		if (obj1 == null || obj2 == null)
			return compareNull(obj1, obj2);
		double n1 = ((Number) obj1).value;
		double n2 = ((Number) obj2).value;
		if (NumberUtil.essentiallyEqual(n1, n2))
			return 0;
		else
			return Double.compare(n1, n2);

	}

	public boolean isInstance(Object obj)
	{
		return obj instanceof Number;
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tersus.model.Path;
import tersus.runtime.trace.EventType;
import tersus.util.Misc;

/**
 * NICE2 [Description]
 * 
 * @author Youval Bronicki
 * @deprecated We should use the newer mechanisms (Fields etc) - See AdvancedFind 
 */
 public class Query
{

    //FUNC3 refactor to handle the case of multi-column references.
    private String sqlStatement;

    private List parameterHandlers;

    private String condition;

    private PersistenceHandler persistenceHandler;

    private String orderBy;

    private int timeout;



    /**
     *  
     */
    public Query(PersistenceHandler handler, String condition,
            List parameterHandlers)
    {
        init(handler, condition, null, parameterHandlers);
    }

    /**
     * @param persistenceHandler2
     * @param string
     * @param orderBy
     * @param types
     */
    public Query(PersistenceHandler handler, String condition, String orderBy,
            List parameterHandlers)
    {
        init(handler, condition, orderBy, parameterHandlers);
    }

    private void init(PersistenceHandler handler, String condition,
            String orderBy, List parameterHandlers)
    {
        this.persistenceHandler = handler;
        this.condition = condition;
        this.parameterHandlers = parameterHandlers;
        this.orderBy = orderBy;
        createSQLQuery();
    }

    /**
     *  
     */
    private void createSQLQuery()
    {
        StringBuffer sql = persistenceHandler.getSelectStatement();
        if (condition != null && condition.trim().length() > 0)
        {
            sql.append(" WHERE ");
            sql.append(condition);
        }
        if (orderBy != null && orderBy.length() > 0)
        {
            sql.append(" ORDER BY ");
            sql.append((orderBy));
        }
        sqlStatement = sql.toString();
    }

    public List execute(RuntimeContext context, List values, Connection connection)
    {
        PreparedStatement statement = null;
        ResultSet rs = null;
        ArrayList<Object> sqlValues = new ArrayList<Object>();
        String sql = sqlStatement;
        try
        {
            statement = connection.prepareStatement(sql);
            for (int i = 0; i < values.size(); i++)
            {
                Object parameter = values.get(i);
                LeafDataHandler parameterHandler = (LeafDataHandler) parameterHandlers
                        .get(i);
                Object sqlValue = parameterHandler.toSQL(parameter);
                sqlValues.add(sqlValue);
                if (sqlValue != null)
                    statement.setObject(i + 1, sqlValue, parameterHandler.getSQLType());
                else
                    statement.setNull(i + 1, parameterHandler.getSQLType());
            }
            rs = context.executeQuery(statement, sql, sqlValues, getTimeout());
            List output = new ArrayList();
            while (rs.next())
            {
                output.add(persistenceHandler.loadInstance(context, rs));
            }
            rs.close();
            rs = null;
            statement.close();
            statement = null;
            return output;
        }
        catch (SQLException e)
        {
            throw new EngineException("Error in SQL Query", "SQL: \""
                    + sql + "\" values:" + Misc.toString(sqlValues), e);
        }
        finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                }
                catch (SQLException e)
                {
                    //Secondary exception ignored (EngineException thrown
                    // anyway)
                }
            }
            if (statement != null)
            {

                try
                {
                    statement.close();
                }
                catch (SQLException e)
                {
                    //Secondary exception ignored (EngineException thrown
                    // anyway)
                }
            }
        }

    }

    /**
     * @return
     */
    private int getTimeout()
    {
        return timeout;
    }

    public void setTimeout(int timeout)
    {
        this.timeout = timeout;
    }

    /**
     * @param n
     */
}
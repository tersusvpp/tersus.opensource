package tersus.runtime;

import java.lang.reflect.Method;

import tersus.model.BuiltinModels;

public class ObjectAccessElementHandler extends ElementHandler
{

	public ObjectAccessElementHandler(InstanceHandler parentHandler)
	{
		super(parentHandler);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected Object getValue(RuntimeContext context, Object parent)
	{
		String prefix = BuiltinModels.BOOLEAN_ID.equals(getChildModelId()) ? "is"
				: "get";
		String accessorName = prefix + getRole().toString().replaceAll(" ", "");
		try
		{
			Method accessor = parent.getClass().getMethod(accessorName, null);
			Object value = accessor.invoke(parent, null);
			if (BuiltinModels.TEXT_ID.equals(getChildModelId())
					&& value != null && !(value instanceof String))
				value = value.toString();
			return value;
		} catch (NoSuchMethodException e)
		{
		    return null;
		}
		catch (Exception e)
		{
			throw new ModelExecutionException("Can't access property "
					+ getRole() + " of class " + parent.getClass().getName());
		}
	}

	@Override
	public void set(RuntimeContext context, Object parent, Object value,
			int elementIndex)
	{
		throw new UnsupportedOperationException("Instances of "
				+ parentHandler.getModelId() + " are immutable");
	}

	@Override
	public void set(RuntimeContext context, Object parent, Object value)
	{
		throw new UnsupportedOperationException("Instances of "
				+ parentHandler.getModelId() + " are immutable");
	}

	@Override
	protected void setValue(RuntimeContext context, Object parent, Object value)
	{
		throw new UnsupportedOperationException("Instances of "
				+ parentHandler.getModelId() + " are immutable");
	}

}

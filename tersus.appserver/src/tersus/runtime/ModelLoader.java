/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import tersus.model.Composition;
import tersus.model.DataType;
import tersus.model.FileRepository;
import tersus.model.FlowModel;
import tersus.model.InvalidModelException;
import tersus.model.MissingModelException;
import tersus.model.Model;
import tersus.model.ModelException;
import tersus.model.ModelId;
import tersus.model.Repository;
import tersus.util.Misc;

/**
 * An object resposible for loading and caching InstanceHandlers
 * @author Youval Bronicki
 *
 */

public class ModelLoader
{

	/**
	 * @param repository
	 */
	public ModelLoader(Repository repository, ClassLoader pluginLoader)
	{
	    if (pluginLoader != null)
	    this.pluginLoader = pluginLoader;
		setRepository(repository);
	}

	/**
	 * A cache of already-loaded instance handlers. The key to the cache is the model id.
	 */
	private Map cache = new HashMap();
	private ClassLoader pluginLoader = ModelLoader.class.getClassLoader();

	private Repository repository;
    private ContextPool contextPool;
    private boolean useQuotedIdentifiers;
	/**
	 * Creates a new ModelLoader with the given model repository root
	 * @param repositoryRoot a <code>java.io.File</code> pointing to the root of
	 * the model repository 
	 */
	public ModelLoader(File repositoryRoot)
	{
		setRepository(new FileRepository());
		((FileRepository) getRepository()).setRoot(repositoryRoot);
	}

	/**
	 * Returns the handler for instances of a given model
	 * @param modelId A String containing the fully qualified id of the model (package '/' name)
	 * @return an InstanceHandler for the given modelId
	 * @throws InvalidModelException if there is no such model or if the model is invalid
	 */
	public synchronized InstanceHandler getHandler(ModelId modelId)
	{
		InstanceHandler handler = (InstanceHandler) cache.get(modelId);
		if (handler == null)
		{
			Model model = getRepository().getModel(modelId, true);
			if (model == null)
			{
				throw new MissingModelException("Missing model '" + modelId + "'");
			}
			try
			{
				handler = createHandler(model);
				cache.put(modelId, handler);
				handler.setLoader(this);
				handler.initializeFromModel(model);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw new ModelException("Failed to initialize model handler for "+model.getId(),e);
			}

		}
		return handler;
	}

	/**
	 * Creates a new InstanceHandler for the specified model
	 * @param model the model
	 * @return a new (uninitialized) instance handler.
	 */
	private InstanceHandler createHandler(Model model)
	{
		String pluginClassName = model.getPluginJavaClass();
		if (pluginClassName != null)
		{
			try
			{
				Class pluginClass = pluginLoader.loadClass(pluginClassName);
                InstanceHandler handler = (InstanceHandler)pluginClass.newInstance();
				return handler;
			}
			catch (NoClassDefFoundError e)
			{
			    throw new EngineException("Missing class "+e.getMessage()+ " (required by "+ pluginClassName+ ")", null,e);
			}
			catch (Exception e)
			{
				throw new EngineException("Failed to load plugin '"+pluginClassName+ "' for " + model.getId(), null, e);			}
		}
		else
		{
			if (model instanceof DataType)
			{
				if (((DataType) model).getComposition() == Composition.ATOMIC)
					return new TextHandler(); // FUNC3 throw an exception (requires modifying existing models)
				else
					return new CompositeDataHandler();
			}
			else
			{
				if (Misc.ASSERTIONS)
					Misc.assertion(model instanceof FlowModel);
				return new CompositeFlowHandler();
			}
		}
	}

	void setRepository(Repository repository)
	{
		this.repository = repository;
	}

	public Repository getRepository()
	{
		return repository;
	}
	
	public void clearCache()
	{
		cache.clear();
	}

    public void setContextPool(ContextPool pool)
    {
        this.contextPool = pool;
    }

    public ContextPool getContextPool()
    {
        return contextPool;
    }

	public boolean useQuotedIdentifiers()
	{
		return useQuotedIdentifiers;
	}

	public void setUseQuotedIdentifiers(boolean useQuotedIdentifiers)
	{
		this.useQuotedIdentifiers = useQuotedIdentifiers;
	}
}
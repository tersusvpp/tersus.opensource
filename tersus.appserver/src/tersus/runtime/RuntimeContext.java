/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.��� Initial API and implementation
 *************************************************************************************************/

package tersus.runtime;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import tersus.model.BuiltinPlugins;
import tersus.model.BuiltinProperties;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelException;
import tersus.model.ModelId;
import tersus.model.Path;
import tersus.model.PluginDescriptor;
import tersus.model.Role;
import tersus.runtime.sapi.SAPIManager;
import tersus.runtime.trace.EventType;
import tersus.util.EscapeUtil;
import tersus.util.InvalidInputException;
import tersus.util.Misc;
import tersus.webapp.Engine;
import tersus.webapp.LoggingHelper;

/**
 * Provides runtime context services (such as tracing, logging , caching and database conenctivity)
 * 
 * @author Youval Bronicki
 * 
 */
public class RuntimeContext
{
	private static final String CACHE_UPDATES_TABLE_NAME = "Cache_Updates";

	private ContextPool contextPool;

	private LowPrecisionClock clock;

	volatile private long startTime, endTime;
	private DatabaseAdapterManager databaseAdapterManager = new DatabaseAdapterManager();

	public DatabaseAdapterManager getDatabaseAdapterManager()
	{
		return databaseAdapterManager;
	}

	static final boolean DEBUG_SQL = false;

	private static Date debugTime;

	private boolean aborted = false;

	private boolean root = false;

	private String rootUser = null;

	private String abortReason = null;

	volatile private String requestId = null; // Volatile because we need read access from another
												// thread

	public HashMap<TriggerSet, Map<Role, Object>> longTermCache = new HashMap<TriggerSet, Map<Role, Object>>();
	
	private HashMap<String, Object> pluginHelperMap = new HashMap<String, Object>();

	private IResourceHandler resourceHandler = new DefaultResourceHandler();

	private ConnectionUnwrapper unwrapper = new ConnectionUnwrapper();

	private ConcurrentMap<String, Connection> connections = new ConcurrentHashMap<String, Connection>(); // Using
																											// Concurrent
																											// map
																											// because
																											// we
																											// may
																											// access
																											// the
																											// map
																											// from
																											// another
																											// thread
																											// ("Diagnostics")
	private ConnectionIdHelper connectionIdHelper = new ConnectionIdHelper();

	/**
	 * A cache for holding information lazily caluclated based on the model. This cache should not
	 * hold actual data, only metadata.
	 */
	private HashMap<String, Object> derivedMetadataCache = new HashMap<String, Object>();

	/**
	 * A ModelLoader associated with this context. Note that the same ModelLoader may be associated
	 * with multiple RuntimeContexts.
	 */
	private ModelLoader modelLoader;

	/**
	 * A PersistenceManager for this context.
	 */
	private PersistenceManager persistenceManager;

	public DatabaseAdapter dbAdapter;

	/**
	 * The <code>TraceWriter</code> of this <code>RuntimeContext</code>
	 */
	public TraceWriter trace = new TraceWriter(this);

	/** Indicates whether this RuntimeContext is 'open' and needs disposing */
	private boolean open = true;

	public Path currentFlowPath = new Path();

	private boolean disposed = false;

	public void dispose()
	{
		dispose(false);
	}

	public void disposeAndCloseBaseConnections()
	{
		dispose(true);
	}

	/**
	 * Performs any cleanup operations (such as closing the trace file) required before this
	 * <code>RuntimeContext</code> can be disposed
	 * 
	 * @param closeBaseConnections
	 *            In the rare and dangerous case where we dispose a context from a different thread,
	 *            we want to close the underlying connection to prevent them from being recycled by
	 *            the connection pool in the middle of some work. It is not obvious that this will
	 *            always work (no documentation regarding what happens when a connection is closed
	 *            from another thread) but it seems better than to leave the underlying connection
	 *            open (and get weird results)
	 */

	private void dispose(boolean closeBaseConnections)
	{
		Logger log = getContextPool().poolLog;
		if (log.isTraceEnabled())
			trace(log, "Disposing " + this);
		try
		{
			if (disposed)
			{
				if (log.isTraceEnabled())
					trace(log, this + " has been previously disposed");
			}
			else
			{
				if (log.isTraceEnabled())
				{
					trace(log, this + ": Disconnecting");
				}
				disconnect(closeBaseConnections);
				if (log.isTraceEnabled())
					trace(log, this + ": Disconnected");

				if (open)
				{
					trace.close();
					open = false;
				}
			}
		}
		finally
		{
			if (getContextPool() != null)
			{
				if (log.isTraceEnabled())
					trace(log, this + ": Removing");

				getContextPool().remove(this);
				if (log.isTraceEnabled())
					trace(log, this + ": Removed");
			}
			disposed = true;
			info(log, this + " disposed");
		}
	}

	public void info(Logger log, String message)
	{
		log(log, Level.INFO, message);
	}

	public void trace(Logger log, String message)
	{
		log(log, Level.TRACE, message);
	}

	public void log(Logger log, Level level, String message)
	{
		try
		{
			log.log(level, message);
		}
		catch (Throwable t)
		{
			try
			{
				t.printStackTrace();
			}
			catch (Throwable t1)
			{

			}
		}
	}

	/**
	 * @return an Adapter for the database type associated with this context
	 */
	public DatabaseAdapter getDefaultDBAdapter()
	{

		return getDBAdapter(getDefaultConnection());
	}

	public DatabaseAdapter getDBAdapter(Connection conn)
	{

		dbAdapter = getDatabaseAdapterManager().getDatabaseAdapter(
				SQLUtils.getDatabaseProductName(conn));
		return dbAdapter;

	}

	/**
	 * @return
	 */
	public ModelLoader getModelLoader()
	{
		return modelLoader;
	}

	/**
	 * @param loader
	 */
	public void setModelLoader(ModelLoader loader)
	{
		modelLoader = loader;
	}

	public void setPersistenceManager(PersistenceManager persistenceManager)
	{
		this.persistenceManager = persistenceManager;
	}

	public PersistenceManager getPersistenceManager()
	{
		return persistenceManager;
	}

	/**
	 * @return
	 */
	public Date getDebugTime()
	{
		return debugTime;
	}

	/**
	 * @param date
	 */
	public void setDebugTime(Date date)
	{
		debugTime = date;
	}

	public void executeUpdate(String sql, boolean autocommit)
	{
		Statement statement = null;
		long start = System.currentTimeMillis();
		try
		{
			Connection c = getConnection(getDefaultDataSourceName(), autocommit);
			statement = c.createStatement();
			if (trace.traceSQL)
				trace.add(EventType.SQL, null, TraceWriter.RUN, "Executing statement: '" + sql
						+ "'", Path.EMPTY);
			statement.executeUpdate(sql);
			logSQL(sql, null, start, null);
			statement.close();
			statement = null;
		}
		catch (SQLException e)
		{
			logSQL(sql, null, start, e);
			throw new EngineException("Failed to execute SQL statement", "Statement:" + sql, e);
		}
		finally
		{
			if (statement != null)
				try
				{
					statement.close();
				}
				catch (SQLException e)
				{
					// Ignore - exception thrown anyway
				}
		}
	}
	public void executeUpdate(Connection c, String sql)
	{
		Statement statement = null;
		long start = System.currentTimeMillis();
		try
		{
			statement = c.createStatement();
			if (trace.traceSQL)
				trace.add(EventType.SQL, null, TraceWriter.RUN, "Executing statement: '" + sql
						+ "'", Path.EMPTY);
			statement.executeUpdate(sql);
			logSQL(sql, null, start, null);
			statement.close();
			statement = null;
		}
		catch (SQLException e)
		{
			logSQL(sql, null, start, e);
			throw new EngineException("Failed to execute SQL statement", "Statement:" + sql, e);
		}
		finally
		{
			if (statement != null)
				try
				{
					statement.close();
				}
				catch (SQLException e)
				{
					// Ignore - exception thrown anyway
				}
		}
	}
	/**
	 * @param string
	 * @param string2
	 * @param object
	 */
	public void warn(String message, String details, Exception cause)
	{
		synchronized (System.err)
		{
			System.err.println("Warning:" + message);
			if (details != null)
				;
			System.err.println(details);
			if (cause != null)
				cause.printStackTrace();
		}
	}

	public String getLoggedInUser()
	{
		return null;
	}

	private HashMap<String, PermissionSet> userPermissions = new HashMap<String, PermissionSet>();
	private HashMap<String, Set<ModelId>> userUIModelCache = new HashMap<String, Set<ModelId>>();

	volatile private long lastTimeChecked;

	private static final long PERIODIC_CHECK_INTERVAL = 1000; // The time in

	// milliseconds
	// between
	// periodic checks

	public void checkPermission(String permissionName)
	{
		boolean permitted = isPermitted(permissionName);
		if (!permitted)
			throw new PermissionDeniedException(this, permissionName);
	}

	public long lastPermissionUpdateCheck = 0; // The last time we checked

	// wether permissions were
	// updated

	/** The last time we know permissions were updated */
	public long lastPermissionsUpdate = 0;

	private static final long PERMISSION_UPDATE_CHECK_INTERVAL = 10000; // The

	/**
	 * Absolute maximum duration (in seconds) for a database operation (we could use MAX_INT, but
	 * using a week instead)
	 */
	private static final int MAX_QUERY_TIMEOUT = 7 * 24 * 3600;

	// duration
	// we
	// wait
	// between
	// permission
	// update
	// checks

	public boolean isPermitted(String permissionName)
	{
		if (isRoot())
			return true;
		String userID = getLoggedInUser();
		if (userID == null)
			return false;
		PermissionSet permissions = getUserPermissions(userID);
		boolean permitted = permissions.getSet().contains(permissionName);
		if (getContextPool().permissionLog.isDebugEnabled())
		{
			getContextPool().permissionLog.debug("User:" + userID + " Permission(" + permissionName
					+ "):" + permitted);
		}
		return permitted;
	}

	public PermissionSet getUserPermissions(String userID)
	{
		if (userID == null)
			return PermissionSet.empty();

		PermissionSet permissions = userPermissions.get(userID);
		if (permissions == null)
		{
			Set<String> set = loadUserPermissions(userID);
			permissions = new PermissionSet(set);
			userPermissions.put(userID, permissions);
		}
		return permissions;
	}

	public PermissionSet getUserPermissions()
	{
		String userID = getLoggedInUser();
		if (userID != null)
			return getUserPermissions(userID);
		else
			return PermissionSet.empty();
	}

	public void clearPermissionCacheIfNeeded()
	{
		if (permissionCacheExists() && ! (userPermissions.isEmpty() && userUIModelCache.isEmpty()))
		{
			long previousLastPermissionUpdate = lastPermissionsUpdate;
			if (getLastPermissionUpdate() > previousLastPermissionUpdate)
			{
				userPermissions.clear();
				userUIModelCache.clear();
			}
		}
	}

	private boolean permissionCacheExists()
	{
		return getContextPool().getPermissionsLastUpdateQuery() != null || cacheUpdateTableExits();
	}

	private Boolean _cacheUpdateTableExits = null;

	private boolean cacheUpdateTableExits()
	{
		if (_cacheUpdateTableExits == null)
		{
			String dataSourceName = getContextPool().getPermissionsDataSource();
			_cacheUpdateTableExits = Boolean.valueOf(SQLUtils.tableExists(this, dataSourceName,
					CACHE_UPDATES_TABLE_NAME, getContextPool().useQuotedIdentifiers()));
		}
		return _cacheUpdateTableExits.booleanValue();
	}

	public long getLastPermissionUpdate()
	{
		updateLastPermissionUpdate();
		return lastPermissionsUpdate;
	}

	private void updateLastPermissionUpdate()
	{
		if (permissionCacheExists())
		{
			long currentTime = clock.getTime();
			if (currentTime - lastPermissionUpdateCheck > PERMISSION_UPDATE_CHECK_INTERVAL)
			{
				lastPermissionUpdateCheck = currentTime;
				retrieveLastPermissionUpdate();
			}
		}
	}

	private void retrieveLastPermissionUpdate()
	{
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String sql = getContextPool().getPermissionsLastUpdateQuery();
		if (sql == null)
		{
			if (getContextPool().useQuotedIdentifiers())
				sql = "SELECT \"Last Update\" FROM \"" + CACHE_UPDATES_TABLE_NAME
					+ "\" WHERE \"Cache Name\"='Permissions'";
			else
				sql = "SELECT Last_Update FROM " + CACHE_UPDATES_TABLE_NAME
					+ " WHERE Cache_Name='Permissions'";
		}
		try
		{
			con = getConnection(getContextPool().getPermissionsDataSource(), false);
			stmt = con.prepareStatement(sql);
			rs = stmt.executeQuery();
			if (rs.next())
			{
				Timestamp lastUpdate = rs.getTimestamp(1);
				rs.close();
				rs = null;
				stmt.close();
				stmt = null;
				lastPermissionsUpdate = lastUpdate.getTime();
			}
		}
		catch (SQLException e)
		{
			throw new EngineException("Failed to retrieve last permission update time",
					"Failed to execute query " + sql, e);
		}
		finally
		{
			SQLUtils.forceClose(rs);
			SQLUtils.forceClose(stmt);
		}
	}

	public void checkPermission(FlowHandler handler)
	{
		String permission = handler.getRequiredPermission();
		if (permission == null)
			return;
		else
			checkPermission(permission);
	}

	public void checkPermission(Model model)
	{
		String permission = (String) model.getProperty(BuiltinProperties.REQUIRED_PERMISSION);
		if (permission == null)
			return;
		else
			checkPermission(permission);
	}

	public boolean isPermitted(ModelId modelId)
	{
		if (modelId == null)
			return true; // This case is used by Constructors.vsl
		Model model = getModelLoader().getRepository().getModel(modelId, true);
		return model != null && isPermitted(model);
	}

	public boolean isPermitted(Model model)
	{
		String permission = (String) model.getProperty(BuiltinProperties.REQUIRED_PERMISSION);
		if (permission == null)
			return true;
		else
			return isPermitted(permission);
	}

	public void checkPermission(ModelId rootId, Path path)
	{
		Model model = modelLoader.getRepository().getModel(rootId, true);
		checkPermission(model, path);
	}

	public void checkPermission(Model rootModel, Path path)
	{
		Model currentModel = rootModel;
		for (int index = 0; index < path.getNumberOfSegments(); index++)
		{
			checkPermission(currentModel);
			Role segment = path.getSegment(index);
			ModelElement element = currentModel.getElement(segment);
			if (element == null)
				throw new InvalidPathException("Path " + path + " is invalid for "
						+ rootModel.getId());
			currentModel = element.getReferredModel();
		}
		checkPermission(currentModel);

	}

	/**
	 * @param user
	 * @return
	 */
	private Set<String> loadUserPermissions(String userID)
	{
		PreparedStatement stmt = null;
		ResultSet rs = null;
		HashSet<String> permissions = new HashSet<String>();
		Connection connection = null;
		try
		{

			connection = getConnection(getContextPool().getPermissionsDataSource(), false);
			stmt = connection.prepareStatement(getContextPool().getPermissionsQuery());
			stmt.setString(1, userID);
			rs = stmt.executeQuery();
			while (rs.next())
			{
				String permission = rs.getString(1);
				if (permission != null)
				{
					permissions.add(permission);
					if (getContextPool().permissionLog.isDebugEnabled())
						getContextPool().permissionLog.debug("Loading permission " + permission
								+ " for user " + userID + ".");
				}
			}
			rs.close();
			rs = null;
			stmt.close();
			stmt = null;
			return permissions;
		}
		catch (SQLException e)
		{
			throw new EngineException("Failed to load permissions ", "User='" + userID + "'", e);
		}
		finally
		{
			SQLUtils.forceClose(rs);
			SQLUtils.forceClose(stmt);
		}
	}

	/**
	 * @return
	 */
	public synchronized boolean isAborted()
	{
		return aborted;
	}

	/**
	 * @param b
	 */
	private synchronized void setAborted(boolean b)
	{
		aborted = b;
	}

	public void throwIfAborted()
	{
		if (clock.getTime() >= lastTimeChecked + PERIODIC_CHECK_INTERVAL)
		{
			checkAborted();
		}
		if (!isAborted())
			return;
		if (getContextPool().engineLog.isInfoEnabled())
			getContextPool().engineLog.info("Execution Aborted:" + getAbortReason());
		throw new ExecutionAbortedException(this + " " + getAbortReason());
	}

	public void checkAborted()
	{
		checkTimeout();
		lastTimeChecked = clock.getTime();
	}

	public long getLastActivityTime()
	{
		return lastTimeChecked;
	}

	private void checkTimeout()
	{
		if (getRemainingTimeMilliseconds() < 0)
		{
			abort("Execution Timed Out.");
		}
	}

	/**
	 * @return
	 */
	public String getAbortReason()
	{
		return abortReason;
	}

	/**
	 * @param string
	 */
	// Should use abort()
	private void setAbortReason(String string)
	{
		abortReason = string;
	}

	synchronized public void abort(String reason)
	{
		if (!isAborted())
		{
			getContextPool().engineLog.warn(this + " aborted : " + reason);
			setAbortReason(reason);
			setAborted(true);
		}
	}

	public void connect(String dataSourceName)
	{
		getResourceHandler().connect(this, dataSourceName);
	}

	public void commit()
	{
		long start = System.currentTimeMillis();
		getResourceHandler().commitTransaction(this);
		logSQL("<Commit>", null, start, null);
	}

	public void rollback()
	{
		long start = System.currentTimeMillis();
		getResourceHandler().rollbackTransaction(this);
		logSQL("<Rollback>", null, start, null);
	}

	public void beginTransaction()
	{
		long start = System.currentTimeMillis();
		getResourceHandler().beginTransaction(this);
		logSQL("<Begin Transaction>", null, start, null);
	}

	/**
     * 
     */
	public Connection getDefaultConnection()
	{
		return getConnection(null, false);
	}

	public String describeConnections()
	{
		StringBuilder s = new StringBuilder();
		boolean first = true;
		for (String key : connections.keySet())
		{
			if (first)
				first = false;
			else
				s.append(" , ");
			Connection c = connections.get(key);
			s.append(key);
			s.append(':');
			s.append(getConnectionIdHelper().getConnectionId(c));
		}
		return s.toString();
	}

	HashMap<String, Object> properties = new HashMap<String, Object>();

	HashMap<String, DateFormat> dateFormats = new HashMap<String, DateFormat>();
	HashMap<String, NumberFormat> numberFormats = new HashMap<String, NumberFormat>();

	public Object getProperty(String key)
	{
		return properties.get(key);
	}

	public void setProperty(String key, Object value)
	{
		properties.put(key, value);
	}

	/**
     * 
     */
	public void openTrace(OutputStream traceOutputStream)
	{
		trace.setOutputStream(traceOutputStream);
		trace.disableAll();
		trace.traceSet = true;
		trace.traceSlots = true;
		trace.traceLinks = true;
		trace.traceStarted = true;
		trace.traceWaiting = true;
		trace.traceData = true;
		trace.traceSQL = true;
		trace.timing = false;
		trace.open();
	}

	/**
     * 
     */
	public void closeTrace()
	{
		this.trace.close();
	}

	/**
     * 
     */
	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	private DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss:SSS");

	private DateFormat dateAndTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");

	volatile private long timeoutAbortTime;

	private SAPIManager sapiManager;
	private String defaultDataSourceName = Engine.MAIN_DATA_SOURCE;

	public Path errorPath;

	private String requestType;

	private ModelId serviceModelId;

	private String rootModelId;

	public DateFormat getDefaultDateFormat()
	{
		return dateFormat;
	}

	/**
	 * @return
	 */
	public DateFormat getTimeFormat()
	{
		return timeFormat;
	}

	public DateFormat getDefaultDateAndTimeFormat()
	{
		dateAndTimeFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		return dateAndTimeFormat;
	}

	/**
	 * @param formatString
	 * @return
	 */
	public DateFormat getDateFormat(String formatString)
	{
		DateFormat format = (DateFormat) dateFormats.get(formatString);
		if (format == null)
		{
			format = new SimpleDateFormat(formatString);
			dateFormats.put(formatString, format);
		}
		format.setTimeZone(TimeZone.getDefault());
		return format;
	}

	/**
	 * Checks whether a given path is valid for external acceess. A path is valid for external
	 * access if (A) It refers to a top-level "explicit" service or (B) It refers to a process
	 * within a display context has no Javascript implementation (and that is not part of another
	 * service)
	 * 
	 * A top-level service is a service that is not contained in another service.
	 * 
	 * 
	 * Motivation:
	 * 
	 * Without this sort of validation, a client can call any action within any service, with any
	 * arguments. For example, if a user uses "database update" within a service, any user
	 * authorized to use the service can call "database update" with arbitrary SQL statement.
	 * 
	 * With this validation, the service is encapsulated: the service can perform input validation
	 * prior to calling low-level actions, and users can't circumvent the validation by calling the
	 * lower level actions directly.
	 * 
	 * @param rootModelId
	 * @param path
	 */
	public void validateExternalAccessToProcess(ModelId rootModelId, Path path)
	{
		Model rootModel = modelLoader.getRepository().getModel(rootModelId, true);
		Model currentModel = rootModel;
		boolean foundService = false;
		boolean inClient = getContextPool().getUIModelIds().contains(rootModelId);
		boolean secureContext = this.isSecure();
		for (int index = 0; index <= path.getNumberOfSegments(); index++)
		{
			if (foundService)
				throw new EngineException("Illegal Service Path", path.toString(), null);
			PluginDescriptor pluginDescriptor = currentModel.getPluginDescriptor();
			if (pluginDescriptor != null)
			{
				if (!pluginDescriptor.allowedOnServer())
					inClient = true;
				if (inClient && pluginDescriptor.isServiceClient()
						|| pluginDescriptor.isWebService())
				{
					if (!secureContext
							&& this.getContextPool().secureModels.contains(currentModel.getId()))
						throw new IllegalServerAccessException(
								"This service requires a secure connection.");
					foundService = true;
				}
			}
			if (index < path.getNumberOfSegments())
			{
				Role segment = path.getSegment(index);
				ModelElement element = currentModel.getElement(segment);
				if (element == null)
					throw new InvalidPathException("Path " + path + " is invalid for "
							+ rootModel.getId());
				currentModel = element.getReferredModel();
			}
		}
		if (!foundService)
			throw new EngineException("Illegal Service Path", path.toString(), null);
	}

	public boolean isSecure()
	{
		return true; // In the base case - no security issues
	}

	public ContextPool getContextPool()
	{
		return contextPool;
	}

	void setContextPool(ContextPool contextPool)
	{
		this.contextPool = contextPool;
	}

	/**
	 * @param DEFAULT_TIMEOUT
	 */
	public void setTimeout(long timeoutMilliseconds)
	{
		long time = clock.getTime();
		this.lastTimeChecked = time;
		this.timeoutAbortTime = time + timeoutMilliseconds;
	}

	

	public boolean isRoot()
	{
		return root;
	}

	public void setRoot(boolean root)
	{
		this.root = root;
	}

	public SAPIManager getSapiManager()
	{
		if (sapiManager == null)
			sapiManager = new SAPIManager(this);
		return sapiManager;
	}

	public NumberFormat getNumberFormat(String formatString)
	{
		NumberFormat format = (NumberFormat) numberFormats.get(formatString);
		if (format == null)
		{
			format = new DecimalFormat(formatString);
			numberFormats.put(formatString, format);
		}
		return format;
	}

	public Object getDerivedMetadataFromCache(String key)
	{
		return derivedMetadataCache.get(key);
	}

	public void caceDerivedMetaData(String key, Object value)
	{
		derivedMetadataCache.put(key, value);
	}

	public IResourceHandler getResourceHandler()
	{
		return resourceHandler;
	}

	public void setResourceHandler(IResourceHandler resourceHandler)
	{
		this.resourceHandler = resourceHandler;
	}

	public Connection getConnection(String dataSourceName, boolean autoCommit)
	{
		Logger log = getContextPool().connectionLog;

		if (dataSourceName == null)
			dataSourceName = getDefaultDataSourceName();
		autoCommit = autoCommit || forceAutoCommit(dataSourceName);
		String dataSourceKey = dataSourceName;
		if (autoCommit)
			dataSourceKey += "__autocommit";
		Connection c = (Connection) connections.get(dataSourceKey);
		if (c == null)
		{
			if (log.isTraceEnabled())
				log.trace("Getting new connection to datasource \"" + dataSourceKey + "\"");
			c = SQLUtils.connectToDataSource(dataSourceName);
			try
			{
				c.setAutoCommit(autoCommit);
			}
			catch (SQLException e)
			{
				
				throw new EngineException("Failed to obtain data source " + dataSourceName,
						"Failed to set autoCommit("+autoCommit+") on "+getConnectionIdHelper().toString(c), e);
			}
			/*
			 *  Logging has to happen after we set auto-commit because getting the connection id can enroll the connection in a distributed transaction and prevent setting auto-commit.
			 * This happens with JOTM/xapool but could happen with other distributed transaction implementations
			 */
			
			if (log.isInfoEnabled())
			{
				getConnectionIdHelper().saveConnectionId(c, this);
				log.info("Received new connection to datasource \"" + dataSourceKey + "\" : "
						+ getConnectionIdHelper().toString(c));
			}
			connections.put(dataSourceKey, c);
		}
		else
		{
			if (log.isTraceEnabled())
				log.trace("Got existing connection to datasource \"" + dataSourceKey + "\""
						+ getConnectionIdHelper().toString(c));

		}
		return c;
	}

	private boolean forceAutoCommit(String dataSourceName)
	{
		 Set<String> acDataSources = getContextPool().getAutoCommitDataSources();
		if (acDataSources == null)
			return ! getDefaultDataSourceName().equals(dataSourceName);
		else
			return acDataSources.contains(dataSourceName);
	}

	public void disconnect()
	{
		disconnect(false);
	}

	/**
	 * Close this context's database connections so they can be recycled
	 * 
	 * @param closeBaseConnections
	 *            in the rare and dangerous case where we're doing this from another thread
	 *            (cleaning up leaked/stale contexts), we don't want to recycle the underlying
	 *            connections, so we close them. In "SQLUtils.connectToDataSource()" we test whether
	 *            the underlying connection is close, and if so, retry. Rudimentary tests show this
	 *            mechanism seems to work OK, but there is probably still a risk of having a race
	 *            condition.
	 */
	synchronized public void disconnect(boolean closeBaseConnections)
	{
		persistenceManager = null;
		Logger log = getContextPool().connectionLog;
		ConnectionIdHelper h = getConnectionIdHelper();
		if (log.isTraceEnabled())
		{
			log.trace("Closing " +  connections.size() + " connections");
		}
		for (Connection c : getAllConnections())
		{
			try
			{
				if (closeBaseConnections)
				{
					Connection c0 = unwrapper.unwrap(c);
					if (c0 != c)
					{
						c0.close();
						c.close();
						log.warn("Closed connection " + h.toString(c) + " and base connection: "
								+ h.toString(c0));
					}
					else
					{
						c.close();
						log.warn("Closed connection " + h.toString(c)
								+ " - Could not obtain base connection");
					}
				}
				else
				{
					c.close();
					if (log.isTraceEnabled())
					{
						log.trace("Closed connection " + h.toString(c));
					}
				}
			}
			catch (Throwable e)
			{
				getContextPool().engineLog.error("Failed to close "+h.toString(c), e);
			}
		}
		connections.clear();
	}

	public Collection<Connection> getAllConnections()
	{
		return connections.values();
	}

	public String getDefaultDataSourceName()
	{
		return defaultDataSourceName;
	}

	public void setDefaultDataSourceName(String defautlDataSource)
	{
		this.defaultDataSourceName = defautlDataSource;
		if (defaultDataSourceName == null)
			defaultDataSourceName = Engine.MAIN_DATA_SOURCE;
	}

	public Logger getEngineLog()
	{
		return getContextPool().engineLog;
	}

	/**
	 * 
	 * @return The time in milliseconds remaining until timeout should occur
	 */
	public long getRemainingTimeMilliseconds()
	{
		return (timeoutAbortTime - clock.getTime());
	}

	public long getStartTime()
	{
		return startTime;
	}

	public void setStartTime(long startTime)
	{
		this.startTime = startTime;
	}

	public long getEndTime()
	{
		return endTime;
	}

	public void setEndTime(long endTime)
	{
		this.endTime = endTime;
	}

	public String getRequestType()
	{
		return requestType;
	}

	public void setRequestType(String requestType)
	{
		this.requestType = requestType;
	}

	public void setServiceModelId(ModelId serviceModelId)
	{
		this.serviceModelId = serviceModelId;
	}

	public ModelId getServiceModelId()
	{
		return serviceModelId;
	}

	public Set<ModelId> getUserPermittedUIModels()
	{
		Set<ModelId> set = userUIModelCache.get(getLoggedInUser());
		if (set == null)
		{
			set = getContextPool().getPermittedUIModels(getUserPermissions(getLoggedInUser()));
			userUIModelCache.put(getLoggedInUser(), set);
		}
		return set;
	}

	private void setQueryTimeout(Statement statement, int maxTimeoutSeconds) throws SQLException
	{
		if (!getDBAdapter(statement.getConnection()).supportsQueryTimeout())
			return;
		int remainingSeconds = (int) (getRemainingTimeMilliseconds() / 1000);
		if (remainingSeconds <= 0)
			throw new ExecutionAbortedException("Execution timed out");
		int maxTimeout = maxTimeoutSeconds > 0 ? maxTimeoutSeconds : MAX_QUERY_TIMEOUT;
		statement.setQueryTimeout(Math.min(maxTimeout, remainingSeconds));

	}

	public void logSQL(String sql, List<Object> values, long startTime, SQLException e)
	{
		if (getContextPool().sqlLog.isInfoEnabled())
		{
			long now = System.currentTimeMillis();
			LoggingHelper loggingHelper = LoggingHelper.get();
			String requestId = loggingHelper.getRequestId();
			long requestStartTime = loggingHelper.getStartTime();
			long sequence = loggingHelper.getSQLSequenceNumber();
			long duration = now - startTime;
			long timeOffset = now - requestStartTime;
			StringBuffer valuesString = (values == null || values.isEmpty()) ? new StringBuffer("-")
					: Misc.toString(values);
			String errorMessage = "";
			if (e != null)
			{
				errorMessage = "\t" + EscapeUtil.escapeControlCharacters(e.getMessage()) + "\t"
						+ e.getErrorCode();
			}
			getContextPool().sqlLog.info(requestId + "\t" + sequence + "\t" + timeOffset + "\t"
					+ duration + "\t" + EscapeUtil.escapeControlCharacters(sql) + "\t"
					+ valuesString + "\t/" + currentFlowPath + "\t" + getRootModelId()
					+ errorMessage);
		}
	}

	private String getRootModelId()
	{
		return rootModelId;
	}

	public ResultSet executeQuery(PreparedStatement stmt, String sql, List<Object> values,
			int timeout) throws SQLException
	{
		setQueryTimeout(stmt, timeout);
		if (trace.traceSQL)
		{
			trace.add(EventType.SQL, null, TraceWriter.RUN, "Executing statement: '" + sql
					+ "' values: " + Misc.toString(values), Path.EMPTY);
		}
		long start = System.currentTimeMillis();
		try
		{
			ResultSet rs = stmt.executeQuery();
			logSQL(sql, values, start, null);
			return rs;
		}
		catch (SQLException e)
		{
			logSQL(sql, values, start, e);
			throw e;
		}
	}

	public boolean execute(CallableStatement stmt, String sql, List<Object> values, int timeout)
			throws SQLException
	{

		setQueryTimeout(stmt, timeout);
		if (trace.traceSQL)
		{
			trace.add(EventType.SQL, null, TraceWriter.RUN, "Executing statement: '" + sql
					+ "' values: " + Misc.toString(values), Path.EMPTY);
		}
		long start = System.currentTimeMillis();
		try
		{
			boolean rs = stmt.execute();
			logSQL(sql, values, start, null);
			return rs;
		}
		catch (SQLException e)
		{
			logSQL(sql, values, start, e);
			throw e;
		}
	}
	public boolean executeBatch(Statement stmt, String sql, int timeout)
			throws SQLException
	{

		setQueryTimeout(stmt, timeout);
		if (trace.traceSQL)
		{
			trace.add(EventType.SQL, null, TraceWriter.RUN, "Executing statement: '" + sql
					, Path.EMPTY);
		}
		long start = System.currentTimeMillis();
		try
		{
			boolean rs = stmt.execute(sql);
			logSQL(sql, null, start, null);
			return rs;
		}
		catch (SQLException e)
		{
			logSQL(sql, null, start, e);
			throw e;
		}
	}

	public int executeUpdate(PreparedStatement stmt, String sql, List<Object> values, int timeout)
			throws SQLException
	{
		setQueryTimeout(stmt, timeout);
		if (trace.traceSQL)
		{
			trace.add(EventType.SQL, null, TraceWriter.RUN, "Executing statement: '" + sql
					+ "' values: " + Misc.toString(values), Path.EMPTY);
		}
		long start = System.currentTimeMillis();
		try
		{
			int count = stmt.executeUpdate();
			logSQL(sql, values, start, null);
			if (trace.traceSQL)
			{
				trace.add(EventType.SQL, null, TraceWriter.RUN, "row count= " + count, Path.EMPTY);
			}
			return count;
		}
		catch (SQLException e)
		{
			logSQL(sql, values, start, e);
			throw e;
		}

	}

	public void setRootModelId(String rootId)
	{
		this.rootModelId = rootId;

	}


	public FlowInstance runProcess(String baseModelIdStr, String pathStr, String traceFileName,
			InvocationAdapter invocationAdapter)
	{
		ModelId baseModelId = baseModelIdStr == null ? null : new ModelId(baseModelIdStr);
		Path path = new Path(pathStr);

		ContextPool pool = getContextPool();
		if (baseModelId == null)
		{
			baseModelId = pool.getRootModel().getId();
		}
		File traceFile = null;
		try
		{
			if (traceFileName != null)
				traceFile = pool.getTraceFile(traceFileName);
			else if (pool.getCompleteTracing())
			{
				traceFile = pool.createTraceFile(Thread.currentThread().getName() + " "
						+ System.currentTimeMillis());
			}
		}
		catch (IOException e1)
		{
			e1.printStackTrace();
			traceFile = null;
		}
		Model rootModel = getModelLoader().getRepository().getModel(baseModelId, true);

		ModelId processModelId = null;
		if (path.getNumberOfSegments() == 0)
			processModelId = rootModel.getId();
		else
		{
			ModelElement element = rootModel.getElement(path);
			if (element == null)
				throw new InvalidInputException("Cannot find element\nPath: " + path + "\n in "
						+ rootModel.getId());

			processModelId = element.getRefId();
		}

		FlowHandler serviceHandler = (FlowHandler) getModelLoader().getHandler(processModelId);

		Role adapterRole = null;
		if (path.getNumberOfSegments() > 0
				&& BuiltinPlugins.INPUT_ADAPTER.equals(serviceHandler.getPlugin()))
		{
			adapterRole = path.getLastSegment();
			path.removeLastSegment();
			if (path.getNumberOfSegments() == 0)
				processModelId = rootModel.getId();
			else
			{
				ModelElement element = rootModel.getElement(path);
				if (element == null)
					throw new ModelException("Cannot find element\nPath: " + path);

				processModelId = element.getRefId();
			}
			serviceHandler = (FlowHandler) getModelLoader().getHandler(processModelId);
		}

		LoggingHelper loggingHelper = LoggingHelper.get();
		loggingHelper.startProcess(invocationAdapter.getLogType(), baseModelId, path,
				processModelId);
		try
		{
			if (traceFile != null)
			{
				try
				{
					FileOutputStream traceFileStream = new FileOutputStream(traceFile, true);

					openTrace(traceFileStream);
					trace.comment("Server side trace");
					trace.setBaseModelId(baseModelId);
					trace.timing = true;
				}
				catch (IOException e)
				{
					throw new EngineException("Can't write trace file", "Error opening "
							+ traceFileName, e);
				}
			}
			if (adapterRole == null && invocationAdapter.needsAccessValidation())
				validateExternalAccessToProcess(baseModelId, path);
			checkPermission(baseModelId, path);
			currentFlowPath = path.getCopy();
			errorPath = null;
			String defaultDataSource = (String) serviceHandler.getModel().getProperty(
					BuiltinProperties.DATA_SOURCE);
			connect(defaultDataSource);

			long timeoutMillis = serviceHandler.getServiceTimeout() * 1000;
			if (timeoutMillis <= 0)
				timeoutMillis = pool.getDefaultTimeoutMilliseconds();
			setTimeout(timeoutMillis);
			setServiceModelId(processModelId);
			setRootModelId(pool.getRootId());
			beginTransaction();
			FlowInstance serviceInstance = (FlowInstance) serviceHandler.newInstance(this);
			if (adapterRole == null)
			{
				// Normal service invocation
				SlotHandler[] triggers = serviceHandler.getTriggers();
				invocationAdapter.setTriggers(this, serviceInstance, triggers);
				serviceHandler.doStart(this, serviceInstance);
			}
			else
			{
				// TODO - refactor or remove adapter invocation
				FlowInstance adapterInstance = (FlowInstance) serviceHandler.getElementHandler(
						adapterRole).create(this, serviceInstance);
				serviceInstance.status = FlowStatus.WAITING_FOR_INPUT;
				FlowHandler adapterHandler = adapterInstance.getFlowHandler();
				invocationAdapter.setTriggers(this, adapterInstance, adapterHandler.exits);
				serviceInstance.readyElements.clear(); // Our adapter instance is already in the
														// list of ready elements - we don't want to
														// add it again
				adapterHandler.readyToResume(this, adapterInstance);
				serviceHandler.resume(this, serviceInstance);
			}
			checkAborted();
			throwIfAborted();
			commit();
			loggingHelper.endProcess();
			return serviceInstance;
		}
		catch (Throwable e)
		{
			loggingHelper.endProcess(e, errorPath);
			getContextPool().engineLog.error("Error while executing process (type="
					+ getRequestType() + ")\nError Location:" + errorPath, e);
			try
			{
				rollback();
			}
			catch (Throwable t)
			{
				getContextPool().engineLog.error("Error during rollback", t);
			}
			dispose();
			if (e instanceof RuntimeException)
				throw (RuntimeException) e;
			else
				throw (Error) e;

		}
		finally
		{
			closeTrace();
		}
	}

	public LowPrecisionClock getClock()
	{
		return clock;
	}

	public void setClock(LowPrecisionClock clock)
	{
		this.clock = clock;
	}

	HashSet<String> tableList = null;

	public synchronized Set<String> getTableList()
	{
		if (tableList == null)
		{
			Connection conn = null;
			try
			{
				conn = SQLUtils.connectToDataSource(getDefaultDataSourceName());
				tableList = tersus.util.SQLUtils.getTableNameList(conn, null, null);
				conn.close();
				conn = null;

			}
			catch (SQLException e)
			{
				throw new EngineException("Failed to obtain list of database tables", null, e);
			}
			finally
			{
				SQLUtils.forceClose(conn);
			}
		}
		return tableList;
	}

	public Object getPluginHelper(String key)
	{
		return pluginHelperMap.get(key);
	}
	
	public void putPluginHelper(String key, Object helper)
	{
		pluginHelperMap.put(key, helper);
	}
	
	public boolean isDisposed()
	{
		return disposed;
	}

	public ConnectionIdHelper getConnectionIdHelper()
	{
		return connectionIdHelper;
	}

	public void setConnectionIdHelper(ConnectionIdHelper connectionIdHelper)
	{
		this.connectionIdHelper = connectionIdHelper;
	}

	public void setRequestId(String requestId)
	{
		this.requestId = requestId;
	}

	public String getRequestId()
	{
		return requestId;
	}

	public void setRootUser(String user)
	{
		rootUser = user;
	}

	public String getRootUser()
	{
		return rootUser;
	}

}
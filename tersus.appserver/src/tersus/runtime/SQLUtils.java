/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.runtime;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringEscapeUtils;

import tersus.model.BuiltinProperties;
import tersus.util.EscapeUtil;

/**
 * @author Youval Bronicki
 * 
 */
public class SQLUtils {
	
	
	private static ConnectionUnwrapper unwraper = new ConnectionUnwrapper();
	public static boolean tableExists(RuntimeContext context, String tableName) {
        boolean useQuotedIndetifiers = context.getContextPool().useQuotedIdentifiers();
        String dataSourceName = context.getDefaultDataSourceName();
//        if (!useQuotedIndetifiers)
        	return tableExists(context, dataSourceName, tableName, useQuotedIndetifiers);
/*        else
        {
        	return context.getTableList().contains(tableName);
        }*/

	}

    public static boolean tableExists(RuntimeContext context, String dataSourceName, String tableName, boolean useQuotedIdentifiers)
    {
        Connection connection = SQLUtils.connectToDataSource(dataSourceName);
        if (!useQuotedIdentifiers)
        	tableName = context.getDBAdapter(connection).convertTableName(tableName);
		ResultSet rs = null;
		try {
			DatabaseMetaData metadata = connection.getMetaData();
			rs = metadata.getTables(null, null, tableName, null);
			boolean found = rs.next();
			rs.close();
			rs = null;
            connection.close();
            connection = null;
			return found;
		} catch (SQLException e) {
			throw new EngineException("Failed to determine whether table '" + tableName
            		+ "' exists in the database",
					null, e);
		} finally {
			forceClose(rs);
            forceClose(connection);
		}
    }

	public static void forceClose(ResultSet rs) {
		if (rs != null) {

			try {
				rs.close();
				rs = null;
			} catch (SQLException e) {
				// Exception ignored (EngineException thrown in catch
				// clause)
			}
		}
	}

	public static void forceClose(Statement stmt) {
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
			}
		}
	}

	/**
	 * 
	 * @param context
	 * @param tableName
	 * @return A List of Strings, containing the names of of the table's
	 *         columns.
	 */
	public static List<String> getColumns(RuntimeContext context, String tableName) {
        if (!context.getContextPool().useQuotedIdentifiers())
        	tableName = context.getDefaultDBAdapter().convertTableName(tableName);
		ArrayList<String> tableColumns = new ArrayList<String>();
		ResultSet rs = null;
		try {
			rs = context.getDefaultConnection().getMetaData().getColumns(null, null,
					tableName, null);

			while (rs.next()) {
				String columnName = rs.getString("COLUMN_NAME").toUpperCase();
				tableColumns.add(columnName);
			}
			rs.close();
			rs = null;
			return tableColumns;
		} catch (SQLException e) {
			throw new EngineException("Failed to obtain list of columns for table '" + tableName
            		+ "'",
					null, e);
		} finally {
			forceClose(rs);
		}
	}

	/**
	 * @param dbConnection
	 */
	public static void forceClose(Connection conn) {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
			}
		}
	}

	public static Connection connectToDataSource(String dataSourceName) {
		try {
			DataSource ds = lookupDataSource(dataSourceName);
			Connection con = ds.getConnection();
			try
			{
				//Testing for a case where the underlying connection was closed, as we're doing when disposing stale RuntimeContexts
				Connection c0 = unwraper.unwrap(con);
				if(c0.isClosed())
				{
					con = null;
				}
			}
			catch (SQLException e1)
			{
				e1.printStackTrace();
				con = null;
			}
			
			if (con == null)
				con = ds.getConnection();
			
			return con;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(
					"Failed to create connection to DataSource "
							+ dataSourceName, e);
		}
	}

	public static DataSource lookupDataSource(String dataSourceName) {
		DataSource ds = null;
		try {
			InitialContext initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			ds = (DataSource) envCtx.lookup(dataSourceName);
		} catch (NamingException e) {
			e.printStackTrace();
			throw new RuntimeException("Failed to locate DataSource "
					+ dataSourceName, e);
		}
		return ds;
	}

    public static String getDatabaseProductName(Connection connection)
    {
        String databaseProductName;
        try
        {
            DatabaseMetaData metaData = connection.getMetaData();
            databaseProductName = metaData.getDatabaseProductName();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            throw new RuntimeException(
                    "Failed to obtain database product name", e);
        }
        return databaseProductName;
    }

    /**
     * Handles nulls and padding in a value (based on element properties)
     * @param context
     * @param dbAdapter
     * @param modelElement
     * @param fieldValue
     * @return
     */
    public static Object handleNullsAndPadding(RuntimeContext context, DatabaseAdapter dbAdapter, ElementHandler modelElement,
            Object fieldValue)
    {
        if (fieldValue == null)
        {
            if (modelElement != null )
            {
                if ("false".equals(modelElement.getProperty(BuiltinProperties.NULLABLE)))
                {
                    String defaultValueStr = (String)modelElement.getProperty(BuiltinProperties.DEFAULT_VALUE);
                    if (defaultValueStr != null)
                   {
                    	defaultValueStr = EscapeUtil.unquote(defaultValueStr);
                    	defaultValueStr = StringEscapeUtils.unescapeEcmaScript(defaultValueStr);
                    	fieldValue = modelElement.getChildInstanceHandler().newInstance(context,defaultValueStr, modelElement.getProperties(), false);
                    }
                    else 
                        throw new ModelExecutionException("Null value supplied for non-nullable element "+modelElement.getRole()+ " with no default value");
                }
            }
    
        }
        if (modelElement != null && modelElement.columnSize > 0 && dbAdapter.requiresPadding())
        {
             Integer columnType = tersus.util.SQLUtils.getSQLTypeNumber((String)modelElement.getProperty(BuiltinProperties.COLUMN_TYPE));
             if (columnType  == Types.CHAR )
                 fieldValue = modelElement.getChildInstanceHandler().padValue(fieldValue, modelElement.columnSize);
        }
        return fieldValue;
    }

	public static String quote(String s)
	{
		return '"'+s+'"';
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import tersus.util.FileUtils;
import tersus.util.NotImplementedException;

/**
 * A handler for a binary data.
 * 
 * @author Youval Bronicki
 *
 */
public class BinaryDataHandler extends LeafDataHandler
{
	/**
	 * 
	 * @param context
	 * @param s A string representation of the numeric value of the created instance (e.g. "10.7").
	 * @return
	 */
	public Object newInstance(RuntimeContext context, String s, boolean forceDefaultFormat)
	{
		throw new UnsupportedOperationException("Attempt to create binary data from a string");
	}
	/**
	 * Creates a new instance 
	 * @param context
	 * @param data
	 * @return
	 */
	public Object newInstance(RuntimeContext context, byte[] data)
	{
		return new BinaryValue(data);
	}
	

	/* (non-Javadoc)
	 * @see tersus.runtime.DataHandler#toSQL(java.lang.Object)
	 */
	public Object toSQL(Object value)
	{
		if (value == null)
			return null;
		BinaryValue buffer = (BinaryValue)value;
		return buffer.toByteArray();
	}
	
	public Object fromSQL(ResultSet rs, int index) throws SQLException
	{
        Object value = rs.getObject(index);
		return fromSQL(value);
	}
    protected Object fromSQL(Object value)
    {
        if (value == null)
			return null;
        byte[] data;
        if (value instanceof Blob)
        {
            
            Blob blob = (Blob)value;
            
            try
            {
                data = FileUtils.readBytes(blob.getBinaryStream(),true);
            }
            catch (Exception e)
            {
                throw new ModelExecutionException("Failed to read bytes from blob",e.getMessage(), e);
            }
        }
        else
        {
            data = (byte[])value;
        }
		return new BinaryValue(data);
    }

	/* (non-Javadoc)
	 * @see tersus.runtime.DataHandler#getSQLType()
	 */
	public int getSQLType()
	{
		return Types.LONGVARBINARY;
	}

    public int compare(RuntimeContext context, Object obj1, Object obj2)
    {
        throw new NotImplementedException("Comparison of binary values is not implemented");
    }
    public boolean isInstance(Object obj)
    {
        return obj instanceof BinaryValue;
    }
}

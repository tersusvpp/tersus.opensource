/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.DTDHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.AttributesImpl;

import tersus.util.Bidi;

/**
 * 
 *  An XMLReader that converts a Tersus data value structure to a stream od SAX events
 * 
 *  
 * @author Youval Bronicki
 *
 */ 
class DataValueXMLReader implements XMLReader
{

	private ContentHandler contentHandler;
	private static final Attributes EMPTY_ATTS = new AttributesImpl();
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");

	/* (non-Javadoc)
	 * @see org.xml.sax.XMLReader#getFeature(java.lang.String)
	 */
	public boolean getFeature(String name) throws SAXNotRecognizedException, SAXNotSupportedException
	{
		return false;
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.XMLReader#setFeature(java.lang.String, boolean)
	 */
	public void setFeature(String name, boolean value) throws SAXNotRecognizedException, SAXNotSupportedException
	{
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.XMLReader#getProperty(java.lang.String)
	 */
	public Object getProperty(String name) throws SAXNotRecognizedException, SAXNotSupportedException
	{
		return null;
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.XMLReader#setProperty(java.lang.String, java.lang.Object)
	 */
	public void setProperty(String name, Object value) throws SAXNotRecognizedException, SAXNotSupportedException
	{
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.XMLReader#setEntityResolver(org.xml.sax.EntityResolver)
	 */
	public void setEntityResolver(EntityResolver resolver)
	{
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.XMLReader#getEntityResolver()
	 */
	public EntityResolver getEntityResolver()
	{
		return null;
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.XMLReader#setDTDHandler(org.xml.sax.DTDHandler)
	 */
	public void setDTDHandler(DTDHandler handler)
	{
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.XMLReader#getDTDHandler()
	 */
	public DTDHandler getDTDHandler()
	{
		return null;
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.XMLReader#setContentHandler(org.xml.sax.ContentHandler)
	 */
	public void setContentHandler(ContentHandler handler)
	{
		this.contentHandler  = handler;
		
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.XMLReader#getContentHandler()
	 */
	public ContentHandler getContentHandler()
	{
		return contentHandler;
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.XMLReader#setErrorHandler(org.xml.sax.ErrorHandler)
	 */
	public void setErrorHandler(ErrorHandler handler)
	{
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.XMLReader#getErrorHandler()
	 */
	public ErrorHandler getErrorHandler()
	{
		return null;
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.XMLReader#parse(org.xml.sax.InputSource)
	 */
	public void parse(InputSource input) throws IOException, SAXException
	{
		DataValueInputSource inputSource = (DataValueInputSource)input;
		Object data = inputSource.getDataValue();
		ElementHandler handler = inputSource.getHandler();
		RuntimeContext context = inputSource.getContext();
		
		contentHandler.startDocument();
		generateEventsFor(context, handler, data);
		contentHandler.endDocument();
	}

	/**
	 * @param context
	 * @param handler
	 * @param data
	 */
	private void generateEventsFor(RuntimeContext context, ElementHandler handler, Object data) throws SAXException
	{
		if (data == null)
			return;
		String tagName = handler.getRole().toString();
		contentHandler.startElement(null, tagName, tagName, EMPTY_ATTS);
		InstanceHandler childHandler = handler.getChildInstanceHandler();
		if (childHandler instanceof DateHandler)
		{
			String text = dateFormat.format((Date) data);
			char[] characters = text.toCharArray();
			contentHandler.characters(characters, 0, characters.length);
		}
		else if (childHandler instanceof LeafDataHandler)
		{
			//FUNC3 BIDI reordering should be optional (it is needed for FOP.  Can be moved to a "Filter".
			String visualText = Bidi.reorderVisually(data.toString());
			char[] characters = visualText.toCharArray();
			contentHandler.characters(characters, 0, characters.length);
		}
		else
		{
			generateEventsForChildren(context, data, childHandler);
		}
		
		contentHandler.endElement(null, tagName, tagName);
	}
	private void generateEventsForChildren(RuntimeContext context, Object data, InstanceHandler dataHandler)
		throws SAXException
	{
		for (int i=0; i<dataHandler.elementHandlers.length;  i++)
		{
			ElementHandler childElement = dataHandler.elementHandlers[i];
			if (childElement.isRepetitive())
			{
				List values = (List)childElement.get(context, data);
				for (int j=0; j<values.size(); j++)
				{
					Object value = values.get(j);
					generateEventsFor(context, childElement, value);
				}
			}
			else
			{
				Object value = childElement.get(context, data);
				generateEventsFor(context, childElement, value);
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.XMLReader#parse(java.lang.String)
	 */
	public void parse(String systemId) throws IOException, SAXException
	{
		throw new SAXException(
			this.getClass().getName()
				+ " cannot be used with system identifiers (URIs)");
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.XMLReader#getFeature(java.lang.String)
	 */

}
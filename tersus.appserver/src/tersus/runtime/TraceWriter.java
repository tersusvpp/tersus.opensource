/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.  Initial API and implementation
 *************************************************************************************************/

package tersus.runtime;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;

import tersus.model.BuiltinPlugins;
import tersus.model.ElementPath;
import tersus.model.ModelId;
import tersus.model.Path;
import tersus.model.Role;
import tersus.runtime.trace.EventType;
import tersus.util.Enum;
import tersus.webapp.JavaScriptFormat;

/**
 * A helper object for writing test information. <br>
 * TraceWriter holds flags that control the tracing of different event types (it is the
 * repsonsibility of the caller to check
 * 
 * @author Youval Bronicki
 * 
 */
public class TraceWriter
{
	public boolean traceFinished = false;
	public static Role RUN = Role.get("_run");

	private RuntimeContext context;

	public boolean traceData = false;

	private OutputStream out;

	private StringWriter detailBuffer = new StringWriter();

	private Writer writer;

	private ModelId baseModelId;

	/**
	 * Indicates whether SQL operations should be traced
	 */
	public boolean traceSQL = false;

	/**
	 * Indicates whether a trace file is open for writing
	 */
	private boolean open = false;

	/**
	 * Indicates whether 'Set' events (setting a value to a slot or data element) should be tracesd.
	 */
	public boolean traceSet = false;

	/**
	 * Indicates whether 'Created' events should be traced.
	 */
	public boolean traceCreation = false;

	/**
	 * Indicates whether 'Activated' events should be traced for slots
	 */
	public boolean traceSlots = false;

	/**
	 * Indicates whether 'Activated' events should be traced for links
	 */
	public boolean traceLinks = false;

	/**
	 * Indicates whether 'Started'/'Resumed' events should be traced
	 */
	public boolean traceStarted = false;

	/**
	 * Indicates whether 'Waiting' events should be traced
	 */
	public boolean traceWaiting = false;

	/**
	 * Indicates whether the addition of 'Ready Elements' should be traced
	 */
	public boolean traceReady = false;

	/**
	 * Indicates whether a time attribute should be added to trace events
	 */
	public boolean timing = true;

	/**
	 * The path from the root flow to the currently active flow (the innermost flow whose start() or
	 * resume() method is being activated.
	 */

	/**
     * 
     */
	public TraceWriter(RuntimeContext context)
	{
		this.context = context;
	}

	/**
	 * Turns off all tracing flags (disables tracing)
	 * 
	 */
	public void disableAll()
	{
		traceSet = false;
		traceCreation = false;
		traceSlots = false;
		traceLinks = false;
		traceStarted = false;
		traceFinished = false;
		traceWaiting = false;
		traceSQL = false;
		traceData = false;
		traceReady = false;
	}

	/**
	 * Turns on all tracing flags (full tracing)
	 */
	public void enableAll()
	{
		traceSet = true;
		traceCreation = true;
		traceSlots = true;
		traceLinks = true;
		traceStarted = true;
		traceFinished = true;
		traceWaiting = true;
		traceSQL = true;
		traceData = true;
		traceReady = true;
	}

	/**
	 * Create creates and opens the trace file
	 */
	public void open()
	{
		try
		{
			writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
			open = true;
			baseModelId = null;

		}
		catch (Exception e)
		{
			throw new EngineException("Failed to open trace file", null, e);
		}
	}

	/**
	 * Closes the trace (closes the document element and closes the file)
	 */
	public void close()
	{
		try
		{
			disableAll();
			if (!open)
				return;
			open = false;
			writer.close();
		}
		catch (Throwable e)
		{
			context.getEngineLog().error("Failed to close trace", e);
		}
	}

	public void add(EventType eventType, Enum objectType, ElementPath relativePath, Object details)
	{
		add(eventType, objectType, relativePath, details, null);
	}

	/**
	 * @param details
	 */
	private void writeObject(Object obj) throws IOException
	{
		if (obj == null)
			writer.write('-');
		else if (obj instanceof StringBuffer)
			write((StringBuffer) obj);
		else
			write(obj.toString());

	}

	/**
	 * @param buffer
	 */
	private void write(StringBuffer buffer) throws IOException
	{
		for (int i = 0; i < buffer.length(); i++)
		{
			writeChar(buffer.charAt(i));
		}
	}

	private void write(String str) throws IOException
	{
		for (int i = 0; i < str.length(); i++)
		{
			writeChar(str.charAt(i));
		}
	}

	/**
	 * @param c
	 */
	private void writeChar(char c) throws IOException
	{
		switch (c)
		{
			case '\n':
				writer.write("\\n");
				break;
			case '\t':
				writer.write("\\t");
				break;
			case '\r':
				writer.write("\\r");
				break;
			default:
				writer.write(c);
		}
	}

	public void setOutputStream(OutputStream trace)
	{
		this.out = trace;

	}

	public void addDetailValue(ElementHandler elementHandler, Object value)
	{
		if (value == null)
			return;
		if (detailBuffer.getBuffer().length() > 0)
			detailBuffer.append(",'");
		else
			detailBuffer.append("{'");
		detailBuffer.append(elementHandler.getRole().toString());
		detailBuffer.append("':");
		writaDetailValue(elementHandler.getChildInstanceHandler(), value);

	}

	/**
	 * Adds a simple detail value
	 * 
	 * @param value
	 *            the value
	 */
	public void addDetailValue(InstanceHandler handler, Object value)
	{
		if (!open)
			return;
		if (value == null)
			return;
		if (detailBuffer.getBuffer().length() != 0)
			throw new IllegalStateException(
					"Can't add multiple detail values using addDetailValue(Object)");
		writaDetailValue(handler, value);
	}

	private void writaDetailValue(InstanceHandler instanceHandler, Object value)
	{
		instanceHandler = getActualHandler(instanceHandler, value);
		JavaScriptFormat.serialize(value, instanceHandler, detailBuffer, context, 
				true, false/* don't add whitespace */, true /* hide secret data */);
	}

	private InstanceHandler getActualHandler(InstanceHandler handler, Object value)
	{
		if (handler != null
				&& (handler instanceof AnythingHandler || BuiltinPlugins.DYNAMIC_DISPLAY
						.equals(handler.getPlugin())))
		{
			// Get actual model id
			ModelId modelId = ModelIdHelper.getModelId(value);
			if (modelId != null)
				handler = handler.getLoader().getHandler(modelId);
		}
		return handler;
	}

	/**
	 * @param accumulated
	 * @param type
	 * @param role
	 * @param object
	 * @param path
	 */
	public void add(EventType eventType, Enum objectType, ElementPath execRelativePath,
			Object details, ElementPath displayRelativePath)
	{
		if (!open)
			return;
		String execPathStr;
		if (context.currentFlowPath == null || context.currentFlowPath.getNumberOfSegments() == 0)
		{
			if (execRelativePath == null || execRelativePath.getNumberOfSegments() == 0)
				execPathStr = "-";
			else
				execPathStr = execRelativePath.toString();
		}
		else
		{
			if (execRelativePath == null || execRelativePath.getNumberOfSegments() == 0)
				execPathStr = context.currentFlowPath.toString();
			else
				execPathStr = context.currentFlowPath + "/" + execRelativePath;
		}
		if (eventType == null)
			throw new IllegalArgumentException("eventType must not be null");

		try
		{
			writeObject(objectType);
			writer.write('\t');
			writeObject(eventType);
			writer.write('\t');
			writeObject(execPathStr);
			writer.write('\t');
			if (details != null)
				writeObject(details);
			else
			{
				if (detailBuffer.getBuffer().length() == 0)
					writer.write('-');
				else if (detailBuffer.getBuffer().charAt(0) == '{')
					detailBuffer.append('}');
				writer.write(this.detailBuffer.toString());
			}
			writer.write('\t');
			writer.write('-'); // comment placeholder
			writer.write('\t');
			if (displayRelativePath != null)
			{
				writeObject(context.currentFlowPath);
				if (context.currentFlowPath.getNumberOfSegments()> 0 && displayRelativePath.getNumberOfSegments() > 0)
				{
					writer.write(Path.SEPARATOR);
					writeObject(displayRelativePath);
				}
			}
			else
				write("-");
			if (timing)
			{
				writer.write('\t');
				writer.write(String.valueOf(System.currentTimeMillis()));
			}
			writer.write('\n');
			writer.flush();
		}
		catch (IOException e)
		{
			throw new EngineException("Failed to write to trace file", null, e);
		}
		this.detailBuffer.getBuffer().setLength(0);

	}

	public ModelId getBaseModelId()
	{
		return baseModelId;
	}

	public void setBaseModelId(ModelId lastModelId)
	{
		if (lastModelId != null && (!lastModelId.equals(this.baseModelId)) && open)
		{
			try
			{
				writer.write('@');
				writer.write(lastModelId.toString());
				writer.write('\n');
			}
			catch (IOException e)
			{
				throw new EngineException("Failed to write to trace file", null, e);
			}
		}
		this.baseModelId = lastModelId;
	}

	public void comment(String message)
	{
		try
		{
			writer.write("// ");
			writer.write(message);
			writer.write('\n');
			writer.flush();
		}
		catch (IOException e)
		{
			throw new EngineException("Failed to write to trace file", null, e);
		}
	}
}
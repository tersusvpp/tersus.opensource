/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.runtime;

/**
 * 
 * A clock with low precision and low reource usage, used for checking timeouts
 * and performing other periodic tests.
 * @author Youval Bronicki
 *  
 */
public class LowPrecisionClock
{
    private static final int DEFAULT_PRECISION = 100;
    private int precision = DEFAULT_PRECISION;
    private boolean stopped = false;
    volatile private long time;
	private Thread timerThread;
    
    public LowPrecisionClock() {
        start();
    }
    public void stop()
    {
    	setStopped(true);
    	timerThread.interrupt();
    	try
		{
    		//Allow some time for clock thread to stop
			Thread.sleep(50);
		}
		catch (InterruptedException e)
		{
		}
    }
	private void start()
	{
		this.timerThread = new Thread(new Runnable() {
            public void run()
            {
                while (!isStopped())
                {
                    try
                    {
                        time = System.currentTimeMillis();
                        Thread.sleep(precision);
                    }
                    catch (InterruptedException e)
                    {
                    }
                }
            }
        });
        timerThread.setDaemon(true);
        timerThread.setName(this.getClass().getName());
        timerThread.start();
	}
    public long getTime()
    {
        return time;
    }
	synchronized private void setStopped(boolean stopped)
	{
		this.stopped = stopped;
	}
	synchronized private boolean isStopped()
	{
		return stopped;
	}
}

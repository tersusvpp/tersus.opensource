/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import javax.xml.transform.sax.SAXSource;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

/**
 * 
 * An InputSource that wraps a Tersus data value.
 * 
 * @author Youval Bronicki
 *
 */

public class DataValueInputSource extends InputSource
{
	private Object dataValue;
	private ElementHandler handler;
	private RuntimeContext context;
	public DataValueInputSource(RuntimeContext context, ElementHandler handler, Object data)
	{
		setDataValue(data);
		setContext(context);
		setHandler(handler);
	}
	/**
	 * @return
	 */
	public Object getDataValue()
	{
		return dataValue;
	}

	/**
	 * @param object
	 */
	public void setDataValue(Object object)
	{
		dataValue = object;
	}
	

	/**
	 * @return
	 */
	public ElementHandler getHandler()
	{
		return handler;
	}

	/**
	 * @param handler
	 */
	public void setHandler(ElementHandler handler)
	{
		this.handler = handler;
	}

	/**
	 * @return
	 */
	public RuntimeContext getContext()
	{
		return context;
	}

	/**
	 * @param context
	 */
	public void setContext(RuntimeContext context)
	{
		this.context = context;
	}
	
	public static SAXSource getSAXSource(RuntimeContext context, ElementHandler handler, Object data)
	{
		XMLReader reader = new DataValueXMLReader();
		InputSource inputSource = new DataValueInputSource(context, handler, data);
		return new SAXSource(reader, inputSource);
	}

}
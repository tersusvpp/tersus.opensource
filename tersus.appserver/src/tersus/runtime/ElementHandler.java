/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tersus.model.BuiltinProperties;
import tersus.model.InvalidModelException;
import tersus.model.MetaProperty;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.Role;
import tersus.runtime.trace.ObjectType;
import tersus.util.Enum;
import tersus.util.Misc;

/**
 * Base class for handling elements. This class declares abstract methods for
 * retrieving, setting, and creating elements, and implements the common
 * behavior of initilizing from a model and loading child handlers
 * 
 * NICE2 move concreate implementaions to DataElementHandler
 * 
 * @author Youval Bronicki
 *  
 */
public class ElementHandler
{

    public String explicitColumnName;

    private boolean primaryKey;

	private boolean databaseGenerated;

    protected boolean repetitive;

    private boolean mandatory;

    private boolean excludedFromFieldName;

    protected Role role;

    protected ModelId childModelId;

    private InstanceHandler childHandler;

    protected ModelLoader loader;

    protected InstanceHandler parentHandler;
    
    public int columnSize = -1;

    private Map<String, Object> properties;

    /**
     * The index of this element handler in the parent. This index is used to
     * access child instances in value arrays.
     */
    private int index;

    public ElementHandler(InstanceHandler parentHandler)
    {
        this.parentHandler = parentHandler;
    }

    /**
     * @return the InstanceHandler for this element's children
     */
    public synchronized InstanceHandler getChildInstanceHandler() 
    {
        if (childModelId == null)
            return null;
        if (childHandler == null)
        {
            childHandler = loader.getHandler(childModelId);
            if (childHandler == null)
                throw new InvalidModelException(parentHandler.getModelId(),
                        getRole(), "Missing Model", "Element " + getRole()
                                + " referers to non-existent model "
                                + childModelId);
        }
        return childHandler;
    }

    /**
     * @return true if the corresponding ModelElement is repetitive
     */
    public boolean isRepetitive()
    {
        return repetitive;
    }

    /**
     * Sets the value of this element
     * 
     * @param parent
     *            The parent instance (in which the value should be set)
     * @param value
     *            The new value for the element
     */
    public void set(RuntimeContext context, Object parent, Object value)
    {
        setValue(context, parent, value);
    }

    protected void setValue(RuntimeContext context, Object parent, Object value)
    {
        Object[] parentArray = getArray(context, parent);
        if (parentArray != null)
            parentArray[index] = value;
    }

    public void remove(RuntimeContext context, Object parent)
    {
        set(context, parent, null);
    }

    /**
     * Return the value of this element in a given parent
     * 
     * @param context
     *            the <code>RuntimeContext</code> for the operation
     * @param parent
     *            the parent instance
     * @return the value of this element in the parent instance. If the element
     *         is repetitive, returs a non-null (possibly empty) List of values
     */
    public Object get(RuntimeContext context, Object parent)
    {
        Object value = getValue(context, parent);

        InstanceHandler childInstanceHandler = getChildInstanceHandler();
        boolean constant = hasDefaultValue();
        if (value == null && constant)
            return childInstanceHandler.newInstance(context);
        if (repetitive && value == null)
            value = Collections.EMPTY_LIST;
        return value;
    }

    protected boolean hasDefaultValue()
    {
        return getChildInstanceHandler() != null
                && getChildInstanceHandler().isConstant();
    }

    @SuppressWarnings("unchecked")
    public Object get(RuntimeContext context, Object parent, int index)
    {
        if (Misc.ASSERTIONS)
            Misc.assertion(isRepetitive());
        List<Object> values = (List<Object>) getValue(context, parent);
        if (values == null)
            return null;
        if (index < values.size())
            return values.get(index);
        else
            return null;
    }

    protected Object getValue(RuntimeContext context, Object parent)
    {
        if (parent == null)
            return null;
        Object[] parentArray = getArray(context, parent);
        if (parentArray == null)
            return null;
        Object value = parentArray[index];
        return value;
    }

    private Object[] getArray(RuntimeContext context, Object instance)
    {
        Object[] array;
        try
        {
            array = (Object[]) instance;
        }
        catch (ClassCastException e)
        {
            throw new ModelExecutionException("When trying to extract element '" + getRole()
                    + "' of '" + parentHandler.getModelId()
                    + "' parent object expected to be composite (array) but encountered '"
                    + instance + "' (" + instance.getClass().getName() + ")");
        }
        return array;
    }

    /**
     * Creates a defualt/empty value for this element. FUNC3 Implement defualt
     * values
     * 
     * @param context
     *            A RuntimeContext for the operation
     * @param parent
     *            The parent instance (whose element should be created)
     * @return The newly created value.
     */
    public Object create(RuntimeContext context, Object parent)
    {
        Object value = getChildInstanceHandler().newInstance(context);
        if (isRepetitive())
            accumulate(context, parent, value);
        else
            set(context, parent, value);
        return value;
    }

    /**
     * @return the model id of the child model referred by this element handler
     */
    public ModelId getChildModelId()
    {
        return childModelId;
    }

    /**
     * @return the role of the ModelElement referred by this element handler
     */
    public Role getRole()
    {
        return role;
    }

    /**
     * Inititalizes this ElementHandler from the corresponding ModelElement
     * 
     * @param element
     */
    public void initializeFromElement(ModelElement element)
    {
        role = element.getRole();
        childModelId = element.getRefId();
        repetitive = element.isRepetitive();
        Boolean tmpPrimaryKey = (Boolean) element
                .getProperty(BuiltinProperties.PRIMARY_KEY);
        primaryKey = Boolean.TRUE.equals(tmpPrimaryKey);
        
        databaseGenerated = Boolean.TRUE.equals(element.getProperty(BuiltinProperties.DATABASE_GENERATED));
        Boolean excludedFromFieldName = (Boolean) element
                .getProperty(BuiltinProperties.EXCLUDE_FROM_FIELD_NAME);
        setExcludedFromFieldName(Boolean.TRUE.equals(excludedFromFieldName));
        setMandatory(Boolean.TRUE.equals(element
                .getProperty(BuiltinProperties.MANDATORY)));
        properties = new HashMap<String, Object>();
        explicitColumnName = (String) element
                .getProperty(BuiltinProperties.COLUMN_NAME);
        for (MetaProperty metaProperty: element.getMetaProperties())
        {
            properties.put(metaProperty.name, element
                    .getProperty(metaProperty.name));
        }
        String columnSizeStr = (String)properties.get(BuiltinProperties.COLUMN_SIZE);
        if (columnSizeStr != null)
            columnSize = Integer.parseInt(columnSizeStr);
    }

    /**
     * Sets the ModelLoader for this element
     * 
     * @param loader
     */
    public void setLoader(ModelLoader loader)
    {
        this.loader = loader;
    }

    /**
     * @return the index of this ElementHandler in the parent model's list of
     *         elements (this index is used to access elements of instances
     *         represented by arrays)
     */
    public int getIndex()
    {
        return index;
    }

    /**
     * Sets the index of this elementHandler within its parent. This method is
     * only called during the initialization of the element.
     * 
     * @param i
     *            The index of this element
     */
    public void setIndex(int i)
    {
        index = i + 1;
        // Internally, indexes start with 1 as the first element in the array is
        // reseved for the model id
    }

    /**
     * Adds a new value to the list of values of a repetitive element
     * 
     * @param parent
     *            The parent instance (to which a value should be added)
     * @param value
     *            The new value.
     */
    @SuppressWarnings("unchecked")
    public void accumulate(RuntimeContext context, Object parent, Object value)
    {
        if (Misc.ASSERTIONS)
            Misc.assertion(repetitive);
        List<Object> list = (List<Object>) getValue(context, parent);
        if (list == null)
        {
            list = new ArrayList<Object>();
            setValue(context, parent, list);
        }
        list.add(value);

    }

    /**
     * Returns the type of this elment. <br>
     * This method is meany to be used by the trace mechanism.
     * 
     * @return The type of this element (ObjectType.ELEMENT or a specific value
     *         such as a slot type)
     */
    public Enum getType()
    {
        return ObjectType.ELEMENT;
    }

    /**
     * Adds a value of a repetitive element, at a given index
     * 
     * @param parent
     *            the parent object
     * @param value
     *            the new element value
     * @param elementIndex
     *            the index of the child
     */
    @SuppressWarnings("unchecked")
    public void set(RuntimeContext context, Object parent, Object value,
            int elementIndex)
    {
        if (Misc.ASSERTIONS)
            Misc.assertion(repetitive);
        
        ArrayList<Object> list = (ArrayList<Object>) getValue(context, parent);
        if (list == null)
        {
            list = new ArrayList<Object>(elementIndex + 1);
            setValue(context, parent, list);
        }
        
        list.ensureCapacity(elementIndex + 1);       
        for (int i = list.size()+1; i <= elementIndex; i++)
             list.add(null);
        
        ArrayList<Object> tempList = list;
        list = new ArrayList<Object>();
        int listIndex = 0;
        
        for (int i = 0; i < tempList.size()+1; i++)
        {
        	if (i == elementIndex)
        		list.add(value);
        	else
        	{
        		list.add(tempList.get(listIndex));
        		listIndex++;
        	}
        }
        setValue(context, parent, list);
    }

    public void remove(RuntimeContext context, Object parent, int elementIndex)
    {
        if (Misc.ASSERTIONS)
            Misc.assertion(repetitive);
        ArrayList<Object> list = (ArrayList<Object>) getValue(context, parent);
        
        if (list != null)
        	list.remove(elementIndex - 1);
    }
    /**
     * @return
     */
    public boolean isPrimaryKey()
    {
        return primaryKey;
    }
    public boolean isDatabaseGenerated()
	{
		return databaseGenerated;
	}

    public String toString()
    {
        return parentHandler.toString() + "[" + role + "] - " + childModelId;
    }

    /**
     * @return true if the role of this element should be included in the names
     *         of fields whose path contains this element
     */
    public boolean isExcludedFromFieldName()
    {
        return excludedFromFieldName;
    }

    /**
     * @param b
     */
    public void setExcludedFromFieldName(boolean b)
    {
        excludedFromFieldName = b;
    }

    /**
     * @return
     */
    public boolean isMandatory()
    {
        return mandatory;
    }

    /**
     * @param b
     */
    public void setMandatory(boolean b)
    {
        mandatory = b;
    }

    /**
     * @param context
     * @param currentInstance
     * @param string
     */
    public void create(RuntimeContext context, Object parent, String valueStr, boolean forceDefaultFormat)
    {
    	create(context, parent, valueStr, properties, forceDefaultFormat);

    }

	public void create(RuntimeContext context, Object parent, String valueStr,
			Map<String, Object> properties, boolean forceDefaultFormat) {
		Object child = getChildInstanceHandler().newInstance(context, valueStr ,properties, forceDefaultFormat );
        if (isRepetitive())
            accumulate(context, parent, child);
        else
            set(context, parent, child);
	}

    public Object getProperty(String name)
    {
        return properties.get(name);
    }

    public Map<String, Object> getProperties()
    {
        return properties;
    }

	public void format(RuntimeContext context, Object value, StringBuffer buffer) {
		 ((LeafDataHandler)getChildInstanceHandler()).format(context, value, properties, buffer, false);
	}

}
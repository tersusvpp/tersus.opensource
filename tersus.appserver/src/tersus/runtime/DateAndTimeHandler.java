/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.tz.DateTimeZoneBuilder;

import tersus.InternalErrorException;
import tersus.model.BuiltinProperties;
import tersus.model.Model;

/**
 * A handler for a point in time
 * @author Ofer Brandes
 * @author Youval Bronicki
 *
 */
public class DateAndTimeHandler extends LeafDataHandler
{
	private String formatString = null;
	private String DTFFormat = null;
	private List<DateTimeFormatter> DTFFormatters = null;

    /**
	 * 
	 * @param context
	 * @param dateAndTime The point in time value of the created instance (e.g. 8/12/2003 13:49:00).
	 * @return
	 */
	public Object newInstance(RuntimeContext context, Object obj)
	{
        if (obj instanceof Date)
            return obj;
        else
            throw new IllegalArgumentException(obj.getClass().getName()  + " is an illegal value for "+this.modelId);
	}

	/**
	 * 
	 * @param context
	 * @param s A string representation of the point in time value of the created instance (e.g. "08/12/2003 13:49:00").
	 * @return
	 */
	public Object newInstance(RuntimeContext context, String s, boolean forceDefaultFormat)
	{
	    return newInstance(context, s, null, forceDefaultFormat);
	}
    public Object newInstance(RuntimeContext context, String s, Map<String, Object> elementProperties, boolean forceDefaultFormat)
	{
		if (s == null || s.length() == 0 || s.trim().length() == 0)
			return null;

		// Special treatment for W3C DFT formats (see http://www.hackcraft.net/web/datetime/#w3cdtf)
		if (DTFFormat.equals(formatString) && !forceDefaultFormat)
		{
			for (int i = 0; i < DTFFormatters.size(); i++)
			{
				try
				{
					DateTimeFormatter jodaFormatter = DTFFormatters.get(i);
					DateTime dateAndTime = jodaFormatter.parseDateTime(s);
					return dateAndTime.toDate();
				}
				catch (UnsupportedOperationException e) { continue; }
				catch (IllegalArgumentException e) { continue; }		
			}
		}

		// In all other cases, using SimpleDateFormat
		DateFormat format = forceDefaultFormat ? context.getDefaultDateAndTimeFormat(): getFormat(context,elementProperties);

		try
		{
			Date dateAndTime = format.parse(s);
			return dateAndTime;
		}
		catch (ParseException e)
		{
			throw new IllegalArgumentException("Bad date and time format '"+s+"'");
		}
	}

	protected DateFormat getFormat(RuntimeContext context, Map<String,Object> elementProperties)
    {
	    String fmt = null;
	    String useGMTs = (String)this.getModel().getProperty(BuiltinProperties.FORMAT_USE_GMT);
		boolean useGMT = useGMTs == null ? false : Boolean.valueOf(useGMTs).booleanValue();
	    
	    
	    if (elementProperties != null)
	    {
	        fmt = (String)elementProperties.get(BuiltinProperties.FORMAT);
	        useGMT = Boolean.valueOf((String)elementProperties.get(BuiltinProperties.FORMAT_USE_GMT)).booleanValue();
	    }
	    if (fmt == null)
	        fmt = formatString;
        DateFormat format;
		if (fmt == null)
		    format = context.getDefaultDateAndTimeFormat();
		else
		    format = context.getDateFormat(fmt);
		if (useGMT)
		    format.setTimeZone(TimeZone.getTimeZone("GMT"));
		else
			format.setTimeZone(TimeZone.getDefault());
        return format;
    }

    public Object fromSQL(ResultSet rs, int index) throws SQLException
    {
        Timestamp sqlTimestamp = rs.getTimestamp(index);
        if (sqlTimestamp == null)
            return null;
        return new java.util.Date(sqlTimestamp.getTime());
    }
    protected Object fromSQL(Object value)
    {
        throw new InternalErrorException("This method should not have been called");
    }
    public Object fromSQL(CallableStatement cs, int index) throws SQLException
    {
        Timestamp sqlTimestamp = cs.getTimestamp(index);
        if (sqlTimestamp == null)
            return null;
        return new java.util.Date(sqlTimestamp.getTime());
    }

	/* (non-Javadoc)
	 * @see tersus.runtime.DataHandler#getSQLType()
	 */
	public int getSQLType()
	{
		return Types.TIMESTAMP;
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.DataHandler#toSQL(java.lang.Object)
	 */
	public Object toSQL(Object value)
	{
		if (value == null)
			return null;
		java.util.Date date = (java.util.Date)value;
		return new java.sql.Timestamp(date.getTime());
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.LeafDataHandler#toString(tersus.runtime.RuntimeContext, java.lang.Object)
	 */
	public StringBuffer format(RuntimeContext context, Object value, Map<String, Object> properties, StringBuffer buffer, boolean forceDefaultFormat)
	{
		// Special treatment for W3C DFT formats (see http://www.hackcraft.net/web/datetime/#w3cdtf)
		if (DTFFormat.equals(formatString) && !forceDefaultFormat)
		{
			DateTimeFormatter jodaFormatter = (DateTimeFormatter) DTFFormatters.get(0);
			jodaFormatter.printTo(buffer,((Date)value).getTime());
		}
		else
		{
			DateFormat format = forceDefaultFormat ? context.getDefaultDateAndTimeFormat(): getFormat(context,properties);
			return format.format((Date)value, buffer,DUMMY_FIELD_POSITION);
		}
		
		return buffer;
	}
	

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        formatString = (String) model.getProperty(BuiltinProperties.FORMAT);

		// Special treatment for W3C DFT formats (see http://www.hackcraft.net/web/datetime/#w3cdtf):
		// - We support 3 levels of details (no seconds, with seconds, with seconds and fraction of second)
		// - We support 5 methods to indicate time zone (Joda's "z", "Z", "ZZ", or "ZZZ", or the constant 'Z')
		String DTF[] = { "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS", "yyyy-MM-dd'T'HH:mm:ss", "yyyy-MM-dd'T'HH:mm" };
		String TZD[] = { "z", "Z", "ZZ", "ZZZ", "'Z'" };
		DTFFormatters = new ArrayList<DateTimeFormatter>(DTF.length*TZD.length);
        for (int i = 0; i < DTF.length; i++)
			for (int j = 0; j < TZD.length; j++)
				DTFFormatters.add(DateTimeFormat.forPattern(DTF[i]+TZD[j]).withZone(DateTimeZone.UTC));
        DTFFormat = DTF[0] + "TZD"; // this is what we expect as the format string
    }
    public int compare(RuntimeContext context, Object obj1, Object obj2)
    {
        if (obj1 == null || obj2 == null)
            return compareNull(obj1, obj2);
        return ((Date)obj1).compareTo((Date)obj2);
    }
    public boolean isInstance(Object obj)
    {
        return obj instanceof Date;
    }
}

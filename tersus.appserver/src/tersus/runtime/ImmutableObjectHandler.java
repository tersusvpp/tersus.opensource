package tersus.runtime;

import tersus.model.ModelElement;

/**
 * A class that provides read-only access to properties of Java Objects
 * Added to support Reflection (implements types 'Model' and 'Model Element'
 * @author youval
 *
 */
public class ImmutableObjectHandler extends CompositeDataHandler
{

	@Override
	public Object newInstance(RuntimeContext context, String valueStr, boolean forceDefaultFormat)
	{
		throw new UnsupportedOperationException("Can't create instances of "+getModelId());
	}

	@Override
	public Object newInstance(RuntimeContext context)
	{
		throw new UnsupportedOperationException("Can't create instances of "+getModelId());
	}

	@Override
	public boolean isInstance(Object obj)
	{
		if (obj == null)
			return false;
		
		Class<?> declaredClass = getModel().getPluginDescriptor().getObjectClass();
		if (declaredClass == null)
			return false;
		return declaredClass.isAssignableFrom(obj.getClass());
	}

	@Override
	public Object deepCopy(RuntimeContext context, Object original)
	{
		return original;
	}

	@Override
	protected ElementHandler createElementHandler(ModelElement element)
	{
		return  new ObjectAccessElementHandler(this);
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.runtime;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import tersus.util.Enum;
import tersus.util.Misc;

/**
 * Holds the state of a flow instance.
 * 
 * The state of a flow consists of a status, a reference to the parent instance
 * and an array of elements values of slots and data elements, and instances of
 * sub-flows.
 * 
 * In addition, a flow that is not the root flow contains a reference to its
 * sub-flow-handler, and a composite flow has a stack of ready elements.
 * 
 * @author Youval Bronicki
 * 
 */
public class FlowInstance implements IFlowElement
{
    FlowHandler handler;

    // The next 2 fields define a linked list of ready elements.
    LinkedList readyElements = new LinkedList(); // The "first" child element
                                                    // that is ready to
                                                    // start/resume

    IFlowElement nextReadySibling; // The "next" sibling that is ready to
                                    // start/resume

    FlowStatus status;

    FlowInstance parent;

    SubFlowHandler subFlowHandler;

    Object[] elements;

    public FlowInstance(FlowHandler handler)
    {
        this.handler = handler;
        this.elements = new Object[handler.nElements + 1];
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowElement#getElementHandler()
     */
    public FlowElementHandler getElementHandler()
    {
        return subFlowHandler;
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowElement#getValueObject()
     */
    public Object getValueObject()
    {
        return this;
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowElement#getParent()
     */
    public FlowInstance getParent()
    {
        return parent;
    }

    /**
     * @return The handler of this flow instance
     */
    public FlowHandler getHandler()
    {
        return handler;
    }

    /**
     * A convience method that eliminates the need for an explicit cast in this
     * very common case
     * 
     * @return The handler of this flow instance (assuming that the handler is
     *         composite)
     * @throws ClassCastException
     *             if the handler is not composite
     */
    public CompositeFlowHandler getCompositeFlowHandler()
    {
        if (Misc.ASSERTIONS)
            Misc.assertion(handler instanceof CompositeFlowHandler);
        return (CompositeFlowHandler) handler;
    }

    /**
     * @return
     */
    public IFlowElement getNextReadySibling()
    {
        return nextReadySibling;
    }

    /**
     * @param state
     */
    public void setNextReadySibling(IFlowElement state)
    {
        nextReadySibling = state;
    }

    /**
     * Retrieves the next ready element (and removes it from the list of ready
     * elements)
     * 
     * @return
     */
    public IFlowElement nextReadyElement()
    {
        IFlowElement readyElement = (IFlowElement) readyElements.removeLast();
        return readyElement;
    }

    public boolean hasReadyElements()
    {
        return !readyElements.isEmpty();
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowElement#getType()
     */
    public Enum getType()
    {
        return handler.getType();
    }

    /**
     * @param context
     * @param handler2
     * @param value
     */
    public void set(RuntimeContext context, SlotHandler slot, Object value)
    {
        elements[slot.getIndex()] = value;
    }

    public void accumulate(RuntimeContext context, SlotHandler slot,
            Object value)
    {
        int index = slot.getIndex();
        List list = (List) elements[index];
        if (list == null)
        {
            list = new ArrayList();
            elements[index] = list;
        }
        list.add(value);

    }

    public void setParent(FlowInstance parent)
    {
        this.parent = parent;
    }

    public FlowHandler getFlowHandler()
    {
        return handler;
    }
}

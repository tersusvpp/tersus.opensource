/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.runtime;

public class ContextPoolException extends RuntimeException
{

    private static final long serialVersionUID = 1;

    public ContextPoolException(String message)
    {
        super(message);
    }
    
    public ContextPoolException(String message, Throwable cause)
    {
        super(message, cause);
    }

}

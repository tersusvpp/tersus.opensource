/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.runtime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tersus.model.Path;
import tersus.util.InvalidInputException;

/**
 * A flat representation of a data structure.
 * 
 * This is a leaner version of 'PersistenceHandler', which does not depend on a specific table or connection 
 * 
 * 
 * @author Youval Bronicki
 *
 */
public class FieldList
{
	private HashMap<String, Field> fieldMap;
	public boolean hasDatabaseGeneratedFields = false;
	public FieldList(MapHandler handler)
	{
		allFields = new Field[20];
		fieldMap = new HashMap<String, Field>();
		nFields = 0;
	}
	public void addMapFields(Map<String,Object> map)
	{
		if (map == null)
			return;
		for (String name: map.keySet())
			addMapField(name);
	}
	public void prepare()
	{
		if (nFields < allFields.length)
		{
			Field[] oldFields = allFields;
			allFields = new Field[nFields];
			System.arraycopy(oldFields, 0, allFields, 0, nFields);
		}
	}
    private void addField(MapField mapField)
	{
		if (nFields >= allFields.length)
		{
			Field[] oldFields= allFields;
			allFields = new Field[nFields*2];
			System.arraycopy(oldFields, 0, allFields, 0, allFields.length);
		}
		allFields[nFields++]=mapField;
		fieldMap.put(mapField.name, mapField);
	}
	public FieldList (CompositeDataHandler dataHandler)
    {
        List allFieldPaths = new PathHandler(dataHandler, Path.EMPTY).allLeafHandlers(dataHandler);
        allFields = new Field[allFieldPaths.size()];
        nFields = allFields.length;
        ArrayList<String> sqlizedDbGenList = new ArrayList<String>();
        ArrayList<String> quotedDbGenList = new ArrayList<String>();
        for (int i=0;i<allFields.length; i++)
        {
            Field f = new Field();
            f.path = (PathHandler)allFieldPaths.get(i);
            f.name = ColumnDescriptor.columnDisplayName(f.path);
            f.primaryKey = f.path.getLastSegment().isPrimaryKey();
            f.databaseGenerated = f.path.getLastSegment().isDatabaseGenerated();
            if (f.databaseGenerated)
            {
            	hasDatabaseGeneratedFields  = true;
            	sqlizedDbGenList.add(f.getSqlizedName());
            	quotedDbGenList.add(f.getQuotedName());
            }
            f.element = dataHandler.getElementHandler(f.path.getPath());
            allFields[i]=f;
        }
        sqlizedDbGeneratedColumns = sqlizedDbGenList.toArray(new String[sqlizedDbGenList.size()]);
        quotedDbGeneratedColumns = quotedDbGenList.toArray(new String[quotedDbGenList.size()]);
    }
    public Field[] getFields()
    {
        return allFields;
    }
    public synchronized Field[] getPrimaryKeyFields()
    {
        if (pkFields == null)
        {
            ArrayList l = new ArrayList();
            for (int i=0;i<allFields.length; i++)
                if (allFields[i].primaryKey)
                    l.add(allFields[i]);
            if (l.isEmpty() && allFields.length >0)
                l.add(allFields[0]);
            pkFields = (Field[])l.toArray(new Field[l.size()]);
        }
        return pkFields;
    }
    
    public Field getField(String fieldName, boolean allowVariants)
    {
        for (int i=0;i<nFields;i++)
        {
            Field f = allFields[i];
            if (f.name.equals(fieldName))
                return f;
            if (allowVariants)
            {
            	if (f.name.equalsIgnoreCase(fieldName))
            		return f;
            	if (f.getSqlizedName().equalsIgnoreCase(fieldName))
            		return f;
            	if (f.getQuotedName().equals(fieldName))
            		return f;
            }
        }
        return null;
    }
    
    private Field[] allFields;
    private int nFields;
    private Field[] pkFields;
	public String[] sqlizedDbGeneratedColumns;
	public String[] quotedDbGeneratedColumns;
	public void addMapField(String name)
	{
		if (getField(name, false) == null)
		{
			MapField field = new MapField(name);
			if (fieldMap.containsKey(field.name))
				throw new InvalidInputException("Ambiguous column "+name);
			addField(field);
		}
	}
    
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.runtime;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import tersus.model.BuiltinModels;
import tersus.model.BuiltinProperties;
import tersus.model.DataElement;
import tersus.model.FlowDataElementType;
import tersus.model.FlowModel;
import tersus.model.FlowType;
import tersus.model.InvalidModelException;
import tersus.model.Link;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelException;
import tersus.model.ModelId;
import tersus.model.Path;
import tersus.model.Role;
import tersus.model.Slot;
import tersus.model.SlotType;
import tersus.model.SubFlow;
import tersus.model.validation.Problems;
import tersus.runtime.trace.EventType;
import tersus.util.DateUtil;
import tersus.util.Enum;
import tersus.util.Misc;

/**
 * An InstanceHandler for flow. <br>
 * FlowHandler is an abstract class, that implements common behavior for all flows (composite and
 * atomic)
 * 
 * @author Youval Bronicki
 * 
 */
public abstract class FlowHandler extends InstanceHandler
{
	public static final Role DONE_EXIT_ROLE = Role.get("<Done>");

	private SlotHandler errorExit;

	private SlotHandler doneExit;

	private int serviceTimeout = -1;

	/**
	 * Handlers for the exits of the flow model
	 */
	protected SlotHandler[] exits;

	/**
	 * Handlers for the triggers of the flow model
	 */
	protected SlotHandler[] triggers;

	/**
	 * * Handlers for the mandatory triggers of the flow model
	 */
	protected SlotHandler[] mandatoryTriggers;

	/**
	 * Handlers for the input slots of the flow model
	 */
	protected SlotHandler[] inputSlots;

	/**
	 * The type of flow model
	 */
	FlowType type;

	private String requiredPermission = null;

	/**
	 * Set the status of a flow instance.
	 * 
	 * @param flow
	 *            A flow instance.
	 * @param status
	 *            The new status to set.
	 */
	public void setStatus(FlowInstance flow, FlowStatus status)
	{
		flow.status = status;
	}

	/**
	 * @return The status of a flow instance
	 */
	public FlowStatus getStatus(FlowInstance flow)
	{
		return flow.status;
	}

	private void checkPermission(RuntimeContext context, FlowInstance flow)
	{
		context.checkPermission(this);
	}

	public void doStart(RuntimeContext context, FlowInstance flow)
	{
		context.throwIfAborted();
		checkPermission(context, flow);
		start(context, flow);
	}

	public void doResume(RuntimeContext context, FlowInstance flow)
	{
		context.throwIfAborted();
		checkPermission(context, flow);
		resume(context, flow);
	}

	/**
	 * Starts a flow instance (performing all operations that "can be executed" until the flow
	 * finishes or waits for more input
	 * 
	 * @param context
	 *            The RuntimeContext for this operation
	 * @param flow
	 *            The flow instance
	 */
	public abstract void start(RuntimeContext context, FlowInstance flow);

	/**
	 * @return true of the flow instance is ready (ready to or ready to resume)
	 */
	public boolean isReady(FlowInstance flow)
	{
		return flow.status == FlowStatus.READY_TO_START;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.InstanceHandler#newInstance(tersus.runtime.RuntimeContext)
	 */
	public Object newInstance(RuntimeContext context)
	{
		FlowInstance flow = new FlowInstance(this);
		setInitialStatus(context, flow);
		return flow;
	}

	/*
	 * Sets the initial status to a flow instance
	 */
	void setInitialStatus(RuntimeContext context, FlowInstance flow)
	{
		// OPT Initial status can be determined statically (when model is
		// loaded)
		setStatus(flow, FlowStatus.NOT_READY_TO_START);
		checkMandatoryTriggers(context, flow);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.InstanceHandler#createElementHandler(tersus.model.ModelElement)
	 */
	protected ElementHandler createElementHandler(ModelElement element)
	{
		FlowElementHandler handler;
		if (element instanceof Link)
			handler = new LinkHandler(this);
		else if (element instanceof SubFlow)
		{
			try
			{
				if (getType() == FlowType.DISPLAY
						&& element.getReferredModel().getProperty(BuiltinProperties.TYPE) == FlowType.DISPLAY)
					handler = new DisplayElementHandler(this);
				else
					handler = new SubFlowHandler(this);
			}
			catch (Exception e)
			{
				throw new ModelException("Error creating handler for element " + element.getRole()
						+ " in " + element.getParentModel().getModelId() + " refid="
						+ element.getRefId(), e);
			}
		}
		else if (element instanceof DataElement)
			handler = new FlowDataHandler(this);
		else if (element instanceof Slot)
			handler = new SlotHandler(this);
		else
			return null;
		handler.parentFlowHandler = this;
		return handler;
	}

	/**
	 * Checks whether all manadatory triggers have been received. If so, updates the status, and
	 * updates the ready stack of the parent flow instance.
	 * 
	 * @param flow
	 *            - A <code>FlowState</code> representing the flow instance to be checke FUNC2 add
	 *            tracing NICE2 Add parameter for RuntimeContext
	 */
	public void checkMandatoryTriggers(RuntimeContext context, FlowInstance flow)
	{
		if (flow.status != FlowStatus.NOT_READY_TO_START)
		{
			// No need to check triggers
			return;
		}
		for (int i = 0; i < mandatoryTriggers.length; i++)
		{
			SlotHandler slotHandler = mandatoryTriggers[i];
			if (slotHandler.isRepetitive())
			{
				if (((List<?>) slotHandler.get(null, flow)).size() == 0)
					return; // Mandatory trigger value is missing
			}
			else
			{
				if (slotHandler.get(null, flow) == null)
					return; // Mandatory trigger is missing

			}
		}
		setStatus(flow, FlowStatus.READY_TO_START);

		// Add flow to the parent's ready queue
		SubFlowHandler subFlowHandler = flow.subFlowHandler;
		if (subFlowHandler != null)
		{
			CompositeFlowHandler parentHandler = (CompositeFlowHandler) subFlowHandler.parentFlowHandler;
			parentHandler.addReadyElement(context, flow.parent, flow);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		type = ((FlowModel) model).getType();
		super.initializeFromModel(model);

		setRequiredPermission((String) model.getProperty(BuiltinProperties.REQUIRED_PERMISSION));
		setServiceTimeout((String) model.getProperty(BuiltinProperties.SERVICE_TIMEOUT));
		// Initialize specific lists for exits, triggers and mandatory triggers
		ArrayList<SlotHandler> triggerList = new ArrayList<SlotHandler>();
		ArrayList<SlotHandler> inputSlotList = new ArrayList<SlotHandler>();
		ArrayList<SlotHandler> mandatoryTriggerList = new ArrayList<SlotHandler>();
		ArrayList<SlotHandler> exitList = new ArrayList<SlotHandler>();
		for (int i = 0; i < nElements; i++)
		{
			ElementHandler elementHandler = elementHandlers[i];

			if (elementHandler instanceof SlotHandler)
			{
				SlotHandler slotHandler = (SlotHandler) elementHandler;
				if (((SlotType) slotHandler.getType()).isTrigger())
				{
					triggerList.add(slotHandler);
					if (slotHandler.isMandatory())
						mandatoryTriggerList.add(slotHandler);
				}
				else if (slotHandler.type.isOutput())
				{
					exitList.add(slotHandler);
				}
				else if (slotHandler.type == SlotType.INPUT)
				{
					inputSlotList.add(slotHandler);
				}
				if (slotHandler.type == SlotType.ERROR)
					errorExit = slotHandler;
			}
		}
		triggers = (SlotHandler[]) triggerList.toArray(new SlotHandler[0]);
		mandatoryTriggers = (SlotHandler[]) mandatoryTriggerList.toArray(new SlotHandler[0]);
		exits = (SlotHandler[]) exitList.toArray(new SlotHandler[0]);
		inputSlots = (SlotHandler[]) inputSlotList.toArray(new SlotHandler[0]);
		doneExit = getNonRequiredExit(DONE_EXIT_ROLE, Boolean.FALSE, BuiltinModels.NOTHING_ID);
	}

	/**
	 * @param string
	 */
	private void setServiceTimeout(String s)
	{
		if (s == null)
			this.serviceTimeout = 0;
		else
			this.serviceTimeout = Integer.parseInt(s);
	}

	public int getServiceTimeout()
	{
		return serviceTimeout;
	}

	/**
	 * 
	 * @return The handler of the sole trigger of the flow (or null if no trigger or more than one
	 *         trigger).
	 */
	public SlotHandler getSoleTrigger()
	{
		if (triggers.length == 1)
			return triggers[0];
		else
			return null;
	}

	/**
	 * @deprecated Not needed and not recommended (we have distinguished slots)
	 */
	public SlotHandler getSolePersistentTrigger()
	{
		SlotHandler trigger = null;
		for (int i = 0; i < triggers.length; i++)
		{
			if (triggers[i].getChildInstanceHandler() != null
					&& triggers[i].getChildInstanceHandler().isPersistent())
			{
				if (trigger != null)
					throw new InvalidModelException(getModelId()
							+ " has multiple triggers with persistent data types");
				trigger = triggers[i];
			}
		}
		return trigger;
	}

	/**
	 * 
	 * @return the handler of the sole repetitive trigger of the flow
	 * @throws InvalidModelException
	 *             if there is no repetitive trigger or if there is more than one repetitive trigger
	 * 
	 * @deprecated Not needed and not recommended (we have distinguished slots)
	 */
	public SlotHandler getSoleRepetitiveTrigger()
	{
		SlotHandler repetitiveTrigger = null;
		for (int i = 0; i < triggers.length; i++)
			if (triggers[i].isRepetitive())
			{
				if (repetitiveTrigger != null)
					throw new InvalidModelException(getModelId()
							+ " has more than one repetitive trigger");
				repetitiveTrigger = triggers[i];
			}
		if (repetitiveTrigger == null)
			throw new InvalidModelException(getModelId() + " has no repetitive trigger");
		return repetitiveTrigger;
	}

	/**
	 * 
	 * @return the handler of the sole trigger of the flow with a given type
	 * @throws InvalidModelException
	 *             if there is no trigger with the given type or if there is more than one trigger
	 *             with the given type
	 * 
	 * @deprecated Not needed and not recommended (we have distinguished slots)
	 */
	public SlotHandler getSoleTypedTrigger(ModelId typeId)
	{
		SlotHandler trigger = null;
		for (int i = 0; i < triggers.length; i++)
			if (typeId.equals(triggers[i].getChildModelId()))
			{
				if (trigger != null)
					throw new InvalidModelException(getModelId()
							+ " has more than one trigger with type " + typeId);
				trigger = triggers[i];
			}
		if (trigger == null)
			throw new InvalidModelException(getModelId() + " has no trigger with type " + typeId);
		return trigger;
	}

	/**
	 * 
	 * @return The handler of the sole non-repetitive trigger of the flow (or null if no
	 *         non-repetitive trigger or more than one non-repetitive exit).
	 */
	public SlotHandler getSoleNonRepetitiveTrigger()
	{
		SlotHandler trigger = null;

		for (int i = 0; i < triggers.length; i++)
		{
			if (!triggers[i].isRepetitive())
				if (trigger == null)
					trigger = triggers[i];
				else
					return null; // More than one
		}

		return (trigger);
	}

	/**
	 * 
	 * @return The handler of the sole mandatory trigger of the flow (or null if no mandatory
	 *         trigger or more than one mandatory trigger).
	 */
	public SlotHandler getSoleMandatoryTrigger()
	{
		if (mandatoryTriggers.length == 1)
			return mandatoryTriggers[0];
		else
			return null;
	}

	/**
	 * 
	 * @return The handler of the sole optional trigger of the flow (or null if no optional trigger
	 *         or more than one optional trigger).
	 */
	public SlotHandler getSoleOptionalTrigger()
	{
		SlotHandler trigger = null;

		for (int i = 0; i < triggers.length; i++)
		{
			if (!triggers[i].isMandatory())
				if (trigger == null)
					trigger = triggers[i];
				else
					return null; // More than one
		}

		return (trigger);
	}

	/**
	 * 
	 * @return The handler of the sole trigger of the flow that has a data type (or null if no typed
	 *         trigger or more than one typed trigger).
	 */
	public SlotHandler getSoleTypedTrigger()
	{
		SlotHandler trigger = null;

		for (int i = 0; i < triggers.length; i++)
		{
			// FUNC3 use explicit "empty trigger"/"empty exit" mechanism
			if (!("true".equals(triggers[i].getProperty(BuiltinProperties.EMPTY))))
				if (trigger == null)
					trigger = triggers[i];
				else
					return null; // More than one
		}

		return (trigger);
	}

	/**
	 * 
	 * @return The handler of the sole trigger of the flow that is an input-output slot (or null if
	 *         no input-output trigger or more than one input-output trigger).
	 */
	public SlotHandler getSoleIoTrigger()
	{
		SlotHandler trigger = null;

		for (int i = 0; i < triggers.length; i++)
		{
			if (SlotType.IO.equals(triggers[i].getProperty(BuiltinProperties.TYPE)))
				if (trigger == null)
					trigger = triggers[i];
				else
					return null; // More than one
		}

		return (trigger);
	}

	/**
	 * 
	 * @return The handler of the sole exit of the flow (or null if no exit or more than one exit).
	 */
	public SlotHandler getSoleExit()
	{
		if (exits.length == 1 && getErrorExit() == null)
			return exits[0];
		if (exits.length == 2 && getErrorExit() != null)
		{
			for (int i = 0; i < exits.length; i++)
			{
				if (exits[i].type == SlotType.EXIT)
					return exits[i];
			}

		}
		return null;
	}

	/**
	 * 
	 * @return The handler of the sole repetitive exit of the flow (or null if no repetitive exit or
	 *         more than one repetitive exit).
	 */
	public SlotHandler getSoleRepetitiveExit()
	{
		SlotHandler exit = null;

		for (int i = 0; i < exits.length; i++)
		{
			if (exits[i].isRepetitive())
				if (exit == null)
					exit = exits[i];
				else
					return null; // More than one
		}

		return (exit);
	}

	/**
	 * 
	 * @return The handler of the sole non-repetitive exit of the flow (or null if no non-repetitive
	 *         exit or more than one non-repetitive exit).
	 */
	public SlotHandler getSoleNonRepetitiveExit()
	{
		SlotHandler exit = null;

		for (int i = 0; i < exits.length; i++)
		{
			if (!exits[i].isRepetitive() && exits[i].type == SlotType.EXIT)
				if (exit == null)
					exit = exits[i];
				else
					return null; // More than one
		}

		return (exit);
	}

	/**
	 * 
	 * @return The handler of the sole input slot of the flow (or null if no input slot or more than
	 *         one input slot).
	 */
	public SlotHandler getSoleInputSlot()
	{
		if (inputSlots.length == 1)
			return inputSlots[0];
		else
			return null;
	}

	/**
	 * Adds a 'Finished' trace event for a flow
	 * 
	 * @param context
	 *            The <code>RuntimeContext</code> of the operation
	 * @param flow
	 *            The flow that finished
	 */
	protected void traceFinish(RuntimeContext context, FlowInstance flow)
	{
		if (context.trace.traceFinished)
		{
			for (int i = 0; i < exits.length; i++)
			{
				SlotHandler handler = exits[i];
				Object value = handler.get(null, flow);
				context.trace.addDetailValue(handler, value);
			}
			Role role = null;
			if (flow.getElementHandler() != null)
				role = flow.getElementHandler().getRole();
			context.trace.add(EventType.FINISHED, type, role, null);
		}

	}

	/**
	 * Adds a 'Started' trace event for a flow
	 * 
	 * @param context
	 *            The <code>RuntimeContext</code> of the operation
	 * @param flow
	 *            The flow that started
	 */
	protected void traceStarted(RuntimeContext context, FlowInstance flow)
	{
		if (context.trace.traceStarted)
		{
			for (int i = 0; i < triggers.length; i++)
			{
				SlotHandler handler = triggers[i];
				Object value = handler.get(null, flow);
				context.trace.addDetailValue(handler, value);
			}
			context.trace.add(EventType.STARTED, type, null, null);
		}
	}

	/**
	 * Adds a 'Resumed' trace event for a flow
	 * 
	 * @param context
	 *            The <code>RuntimeContext</code> of the operation
	 * @param flow
	 *            The flow that started
	 */
	protected void traceResumed(RuntimeContext context, FlowInstance flow)
	{
		if (context.trace.traceStarted)
		{
			Role role = null;
			if (flow.getElementHandler() != null)
				role = flow.getElementHandler().getRole();
			context.trace.add(EventType.RESUMED, type, role, null);
		}
	}

	/**
	 * Adds a 'Waiting' trace event for a flow
	 * 
	 * @param context
	 *            The <code>RuntimeContext</code> of the operation
	 * @param flow
	 *            The flow that's waiting
	 */
	protected void traceWaiting(RuntimeContext context, FlowInstance flow)
	{
		if (context.trace.traceWaiting)
		{
			Role role = null;
			if (flow.getElementHandler() != null)
				role = flow.getElementHandler().getRole();
			context.trace.add(EventType.WAITING, type, role, null);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.InstanceHandler#getType()
	 */
	public Enum getType()
	{
		return type;
	}

	/**
	 * Returns the path from the root instance to a given instance
	 * 
	 * @param instance
	 * @return
	 */
	public Path getPath(FlowInstance instance)
	{
		if (instance.parent == null)
			return new Path();
		Path path = getPath(instance.parent);
		path.addSegment(instance.getElementHandler().getRole());
		return path;
	}

	public void readyToResume(RuntimeContext context, FlowInstance flow)
	{
		setStatus(flow, FlowStatus.READY_TO_RESUME);
		FlowInstance parent = flow.parent;
		if (parent == null)
			return;
		CompositeFlowHandler parentHandler = (CompositeFlowHandler) flow.getElementHandler().parentFlowHandler;
		// FUNC1 Add Trace
		parentHandler.addReadyElement(context, parent, flow);
		if (parent.status == FlowStatus.WAITING_FOR_INPUT)
			parentHandler.readyToResume(context, parent);
	}

	/**
	 * Resumes execution of a flow (after some input was received in some level)
	 * 
	 * @param context
	 * @param root
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// base implementation is empty
	}

	/**
	 * @return
	 */
	public SlotHandler[] getExits()
	{
		return exits;
	}

	/**
	 * Clears the context of all exists of an instance
	 * 
	 * @param context
	 * @param flow
	 */
	public void clearExits(RuntimeContext context, FlowInstance flow)
	{
		for (int i = 0; i < exits.length; i++)
		{
			SlotHandler exit = exits[i];
			exit.remove(context, flow);
		}

	}

	protected void fireError(RuntimeContext context, FlowInstance flow, RuntimeException e)
	{
		if (getErrorExit() == null)
			throw e;
		if (flow.getParent() == null) // For a top level process (Service), there's no sense in
										// catching the exception)
			throw e;
		clearExits(context, flow);
		InstanceHandler errorType = getErrorExit().getChildInstanceHandler();
		Object error = errorType.newInstance(context);
		setElement(context, errorType, BuiltinModels.ERROR_MESSAGE, e.getMessage(), error);
		String details = null;

		if (e.getCause() != null)
		{
			details = e.getCause().getMessage();
			if (e.getCause() instanceof SQLException)
				try
				{
					details = context.getDefaultDBAdapter().translateMessage(
							(SQLException) e.getCause());
				}
				catch (Exception e1)
				{
					context.getEngineLog().error("Error in translating exception", e1);
					context.getEngineLog().error("[Original Exception]", e);
				}
		}
		if (e instanceof EngineException)
		{
			details = ((EngineException) e).getDetails();
		}
		if (details != null)
			try
			{
				setElement(context, errorType, BuiltinModels.ERROR_DETAILS, details, error);
			}
			catch (Exception e1)
			{
				setElement(context, errorType, BuiltinModels.ERROR_CAUSE, details, error); // Backwards compatibility (Error/Cause element)
			}
		if (context.errorPath != null)
			setElement(context, errorType, BuiltinModels.ERROR_LOCATION, context.errorPath.toString(), error);
		context.errorPath = null;
		setExitValue(getErrorExit(), context, flow, error);

	}

	/**
	 * Sets a value on an exit (adding trace events if necessary)
	 * 
	 * @param exit
	 * @param context
	 * @param flow
	 * @param value
	 */
	protected void setExitValue(SlotHandler exit, RuntimeContext context, FlowInstance flow,
			Object value)
	{
		if (exit == null || value == null)
			return;
		exit.set(context, flow, value);
		if (context.trace.traceSet)
		{
			context.trace.addDetailValue(exit.getChildInstanceHandler(), value);
			context.trace.add(EventType.SET, exit.getType(), exit.getRole(), null);
		}
	}

	protected void setExitValue(SlotHandler exit, RuntimeContext context, FlowInstance flow,
			byte[] value)
	{
		if (value != null && exit != null)
			setExitValue(exit, context, flow, new BinaryValue(value));
	}

	protected void setExitValue(SlotHandler exit, RuntimeContext context, FlowInstance flow,
			double value)
	{
		if (exit == null)
			return;

		setExitValue(exit, context, flow, new tersus.runtime.Number(value));
	}

	/**
	 * Sets a time value on an exit (adding trace events if necessary) if the exit is of type
	 * "Date", the timeof day is first reset to the default.
	 * 
	 * @param exit
	 *            - Either a "Date and Time" or "Date" exit
	 * @param context
	 * @param flow
	 * @param value
	 */
	protected void setTimeExitValue(SlotHandler exit, RuntimeContext context, FlowInstance flow,
			Object value)
	{
		Date time = (Date) value;
		if (Misc.equal(BuiltinModels.DATE_ID, exit.getChildModelId()))
		{
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(time);
			time = DateUtil.getDate(calendar);
		}
		setExitValue(exit, context, flow, time);
	}

	/**
	 * Sets a value on an exit (adding trace events if necessary)
	 * 
	 * @param handler
	 * @param context
	 * @param flow
	 * @param error
	 */
	public void accumulateExitValue(SlotHandler exit, RuntimeContext context, FlowInstance flow,
			Object value)
	{
		if (exit == null || value == null)
			return;
		exit.accumulate(context, flow, value);
		if (context.trace.traceSet)
		{
			context.trace.addDetailValue(exit.getChildInstanceHandler(), value);
			context.trace.add(EventType.ACCUMULATED, exit.getType(), exit.getRole(), null);
		}
	}

	/**
	 * Sets a named element of a composite data value
	 * 
	 * @param context
	 * @param type
	 *            the InstanceHandler of the parent data value
	 * @param elementRole
	 *            the Role of the element to be set
	 * @param elementContent
	 *            the content of the element to be set
	 * @param parent
	 */
	public void setElement(RuntimeContext context, InstanceHandler type, String elementRole,
			Object elementContent, Object parent)
	{
		ElementHandler element = type.getElementHandler(elementRole);
		element.set(context, parent, elementContent);
	}

	/**
     *  
     */
	protected SlotHandler getErrorExit()
	{
		return errorExit;
	}

	/**
	 * @return
	 */
	public SlotHandler[] getTriggers()
	{
		return triggers;
	}

	public void chargeSoleExit(RuntimeContext context, FlowInstance flow)
	{
		SlotHandler exit = getSoleExit();

		if (exit != null)
		{
			Object exitValue = Boolean.TRUE;
			exit.set(context, flow, exitValue);
			if (context.trace.traceSet)
			{
				context.trace.addDetailValue((DataHandler) exit.getChildInstanceHandler(),
						exitValue);
				context.trace.add(EventType.SET, exit.getType(), exit.getRole(), null);
			}
		}
	}

	public void chargeDoneExit(RuntimeContext context, FlowInstance flow)
	{
		SlotHandler exit = doneExit;
		chargeEmptyExit(context, flow, exit);
	}

	protected void chargeEmptyExit(RuntimeContext context, FlowInstance flow, SlotHandler exit)
	{
		if (exit != null)
		{
			Object exitValue = Nothing.NOTHING;
			if (!BuiltinModels.NOTHING_ID.equals(exit.getChildModelId()))
				throw new ModelExecutionException("Incompatible type. The type of exit '"
						+ exit.getRole() + "' in " + getModelId() + " should be "
						+ BuiltinModels.NOTHING_ID + " but is " + exit.getChildModelId());
			exit.set(context, flow, exitValue);
			if (context.trace.traceSet)
			{
				context.trace.addDetailValue((DataHandler) exit.getChildInstanceHandler(),
						exitValue);
				context.trace.add(EventType.SET, exit.getType(), exit.getRole(), null);
			}
		}
	}

	/**
	 * @return
	 */
	public String getRequiredPermission()
	{
		return requiredPermission;
	}

	/**
	 * @param string
	 */
	public void setRequiredPermission(String string)
	{
		requiredPermission = string;
	}

	public SlotHandler getTrigger(Role role)
	{
		ElementHandler element = getElementHandler(role);
		if (element instanceof SlotHandler)
		{
			SlotHandler slot = (SlotHandler) element;
			if (slot.getType() == SlotType.TRIGGER)
				return slot;

		}
		return null;
	}

	public SlotHandler getDistinguishedTrigger(Role role)
	{
		ElementHandler element = getDistinguishedElementHandler(role);
		if (element instanceof SlotHandler)
		{
			SlotHandler slot = (SlotHandler) element;
			if (slot.getType() == SlotType.TRIGGER)
				return slot;

		}
		return null;
	}

	public SlotHandler getExit(Role role)
	{
		ElementHandler element = getElementHandler(role);
		if (element instanceof SlotHandler)
		{
			SlotHandler slot = (SlotHandler) element;
			if (slot.getType() == SlotType.EXIT)
				return slot;

		}
		return null;
	}

	public SlotHandler getDistinguishedExit(Role role)
	{
		ElementHandler element = getDistinguishedElementHandler(role);
		if (element instanceof SlotHandler)
		{
			SlotHandler slot = (SlotHandler) element;
			if (slot.getType() == SlotType.EXIT)
				return slot;

		}
		return null;
	}

	/**
	 * Created on Dec 6, 2004
	 * 
	 * An iterator to iterate over the inputs received through the flow's triggers.
	 * 
	 * @author Ofer Brandes
	 * 
	 */
	protected class TriggerIterator implements Iterator<Object>
	{
		RuntimeContext context;

		FlowInstance flow;

		Role exclude;

		int t = 0;

		int i = 0;

		Object next = null;

		InstanceHandler nextHandler = null;

		InstanceHandler currentHandler = null;

		public TriggerIterator(RuntimeContext context, FlowInstance flow, Role exclude)
		{
			this.context = context;
			this.flow = flow;
			this.exclude = exclude;
			moveToNext();
		}

		private void moveToNext()
		{
			next = null;
			nextHandler = null;

			while (t < triggers.length)
			{
				if (false /* triggers[t].isEmpty() */|| // FUNC1 Implement empty
				// slots
				triggers[t].getRole().equals(exclude))
				{
					t++;
					i = 0;
				}
				else
				{ // Interesting trigger
					if (triggers[t].isRepetitive())
					{
						@SuppressWarnings("unchecked")
						List<Object> values = (List<Object>) triggers[t].get(context, flow);
						if (values != null)
							while (i < values.size())
							{
								next = values.get(i);
								nextHandler = triggers[t].getChildInstanceHandler();
								i++;
								if (next != null)
									break;
							}

						if (next == null)
						{
							t++;
							i = 0;
						}
					}
					else
					{ // Non-repetitive trigger
						next = triggers[t].get(context, flow);
						nextHandler = triggers[t].getChildInstanceHandler();
						t++;
						i = 0;
					}

					if (next != null)
						break;
				}
			}
		}

		public boolean hasNext()
		{
			return (next != null);
		}

		public Object next()
		{
			Object current = next;
			currentHandler = nextHandler;
			moveToNext();
			return current;
		}

		public InstanceHandler handler()
		{ // Get the handler of the last object returned by next()
			return currentHandler;
		}

		// Not supported
		public void remove()
		{
		}
	}

	public boolean isInstance(Object obj)
	{
		return obj instanceof FlowInstance && ((FlowInstance) obj).getHandler() == this;
	}

	public int compare(RuntimeContext context, Object obj1, Object obj2)
	{
		return compareComposite(context, obj1, obj2);
	}

	public SlotHandler getRequiredMandatoryTrigger(Role role, Boolean repetitive, ModelId typeId)
	{
		return getRequiredTrigger(role, repetitive, typeId, true);
	}

	public SlotHandler getRequiredTrigger(Role role, Boolean repetitive, ModelId typeId)
	{
		return getRequiredTrigger(role, repetitive, typeId, false);
	}

	private SlotHandler getRequiredTrigger(Role role, Boolean repetitive, ModelId typeId,
			boolean mandatory)
	{
		SlotHandler trigger = getDistinguishedTrigger(role);

		if (trigger == null)
		{
			notifyInvalidModel(Path.EMPTY, Problems.MISSING_TRIGGER, "Missing "
					+ triggerDescription(role, repetitive, typeId));
			return null;
		}

		if (mandatory && !trigger.isMandatory())
			notifyInvalidModel(role, Problems.INVALID_TRIGGER, "Trigger should be mandatory");

		if (repetitive != null)
			if (trigger.isRepetitive() != repetitive.booleanValue())
				notifyInvalidModel(role, Problems.INVALID_TRIGGER, "Trigger should be "
						+ repetitiveness(repetitive));

		checkTriggerType(trigger, typeId);

		return trigger;
	}

	public SlotHandler getNonRequiredTrigger(Role role, Boolean repetitive, ModelId typeId)
	{
		SlotHandler trigger = getDistinguishedTrigger(role);

		if (trigger != null)
		{
			if (repetitive != null)
				if (trigger.isRepetitive() != repetitive.booleanValue())
					notifyInvalidModel(role, Problems.INVALID_TRIGGER, "Trigger should be "
							+ repetitiveness(repetitive));

			checkTriggerType(trigger, typeId);
		}
		return trigger;
	}

	public SlotHandler getRequiredTimeTrigger(Role role, Boolean repetitive)
	{
		SlotHandler trigger = getNonRequiredTrigger(role, repetitive, null);
		if (trigger == null)
		{
			notifyInvalidModel(Path.EMPTY, Problems.MISSING_TRIGGER, "Missing "
					+ triggerDescription(role, repetitive, BuiltinModels.DATE_AND_TIME_ID));
			return null;
		}
		else
			checkTimeTriggerType(trigger);
		return trigger;
	}

	public SlotHandler getNonRequiredMandatoryTrigger(Role role, Boolean repetitive, ModelId typeId)
	{
		SlotHandler trigger = getDistinguishedTrigger(role);

		if (trigger != null)
		{
			if (!trigger.isMandatory())
				notifyInvalidModel(role, Problems.INVALID_TRIGGER, "Trigger should be mandatory");

			if (repetitive != null)
				if (trigger.isRepetitive() != repetitive.booleanValue())
					notifyInvalidModel(role, Problems.INVALID_TRIGGER, "Trigger should be "
							+ repetitiveness(repetitive));

			checkTriggerType(trigger, typeId);
		}
		return trigger;
	}

	public SlotHandler getNonRequiredTimeTrigger(Role role, Boolean repetitive)
	{
		SlotHandler trigger = getNonRequiredTrigger(role, repetitive, null);
		if (trigger != null)
			checkTimeTriggerType(trigger);
		return trigger;
	}

	public SlotHandler getRequiredMandatoryTimeTrigger(Role role, Boolean repetitive)
	{
		SlotHandler trigger = getNonRequiredMandatoryTrigger(role, repetitive, null);
		if (trigger == null)
		{
			notifyInvalidModel(Path.EMPTY, Problems.MISSING_TRIGGER, "Missing "
					+ triggerDescription(role, repetitive, BuiltinModels.DATE_AND_TIME_ID));
			return null;
		}
		else
			checkTimeTriggerType(trigger);
		return trigger;
	}

	public SlotHandler getNonRequiredExit(Role role, Boolean repetitive, ModelId typeId)
	{
		SlotHandler exit = getDistinguishedExit(role);

		if (exit != null)
		{
			checkExitType(exit, typeId);
			if (repetitive != null)
				if (exit.isRepetitive() != repetitive.booleanValue())
					notifyInvalidModel(role, Problems.INVALID_EXIT, "Exit should be "
							+ repetitiveness(repetitive));
		}
		return exit;
	}

	public SlotHandler getRequiredExit(Role role, Boolean repetitive, ModelId typeId)
	{
		SlotHandler exit = getDistinguishedExit(role);
		if (exit == null)
		{
			notifyInvalidModel(Path.EMPTY, "Missing Exit",
					"Missing " + exitDescription(role, repetitive, typeId));
			return null;
		}

		if (repetitive != null)
			if (exit.isRepetitive() != repetitive.booleanValue())
				notifyInvalidModel(role, Problems.INVALID_EXIT, "Exit should be "
						+ repetitiveness(repetitive));

		checkExitType(exit, typeId);

		return exit;
	}

	public SlotHandler getNonRequiredTimeExit(Role role, Boolean repetitive)
	{
		SlotHandler exit = getNonRequiredExit(role, repetitive, null);
		if (exit != null)
			checkTimeExitType(exit);
		return exit;
	}

	public SlotHandler getRequiredTimeExit(Role role, Boolean repetitive)
	{
		SlotHandler exit = getNonRequiredExit(role, repetitive, null);
		if (exit == null)
		{
			notifyInvalidModel(Path.EMPTY, "Missing Exit",
					"Missing " + exitDescription(role, repetitive, BuiltinModels.DATE_AND_TIME_ID));
			return null;
		}
		else
			checkTimeExitType(exit);
		return exit;
	}

	private void checkTriggerType(SlotHandler trigger, ModelId typeId)
	{
		ModelId actualTypeId = trigger.getChildModelId();

		if (typeId == null) // Meaning we don't know what type we expect (but we
							// do expect something!)
		{
			if ((actualTypeId == null) || Misc.equal(BuiltinModels.NOTHING_ID, actualTypeId))
				notifyInvalidModel(trigger.getRole(), Problems.INVALID_TRIGGER,
						"Trigger data type should not be '" + actualTypeId + "'");
		}
		else
		// Meaning we know what type we expect
		{
			if (!Misc.equal(typeId, actualTypeId))
				notifyInvalidModel(trigger.getRole(), Problems.INVALID_TRIGGER,
						"Trigger data type should be '" + typeId + "', but is '" + actualTypeId
								+ "'");
		}
	}

	private void checkTimeTriggerType(SlotHandler trigger)
	{
		ModelId actualTypeId = trigger.getChildModelId();

		if (!Misc.equal(BuiltinModels.DATE_AND_TIME_ID, actualTypeId)
				&& !Misc.equal(BuiltinModels.DATE_ID, actualTypeId))
			notifyInvalidModel(trigger.getRole(), Problems.INVALID_TRIGGER,
					"Trigger data type should be '" + BuiltinModels.DATE_AND_TIME_ID + "' or '"
							+ BuiltinModels.DATE_ID + "', but is '" + actualTypeId + "'");
	}

	private void checkExitType(SlotHandler exit, ModelId typeId)
	{
		ModelId actualTypeId = exit.getChildModelId();

		if (typeId == null) // Meaning we don't know what type we expect (but we
							// do expect something!)
		{
			if ((actualTypeId == null) || Misc.equal(BuiltinModels.NOTHING_ID, actualTypeId))
				notifyInvalidModel(exit.getRole(), Problems.INVALID_EXIT,
						"Exit data type should not be '" + actualTypeId + "'");
		}
		else
		// Meaning we know what type we expect
		{
			if (!Misc.equal(typeId, actualTypeId))
				notifyInvalidModel(exit.getRole(), Problems.INVALID_EXIT,
						"Exit data type should be '" + typeId + "', but is '" + actualTypeId + "'");
		}
	}

	private void checkTimeExitType(SlotHandler exit)
	{
		ModelId actualTypeId = exit.getChildModelId();

		if (!Misc.equal(BuiltinModels.DATE_AND_TIME_ID, actualTypeId)
				&& !Misc.equal(BuiltinModels.DATE_ID, actualTypeId))
			notifyInvalidModel(exit.getRole(), "Invalid Exit", "Exit data type should be '"
					+ BuiltinModels.DATE_AND_TIME_ID + "' or '" + BuiltinModels.DATE_ID
					+ "', but is '" + actualTypeId + "'");
	}

	/**
	 * Ensures the slot has a data type, and optionally ('isLeaf != null') ensure it is
	 * atomic/composite
	 * 
	 */
	protected DataHandler checkDataType(SlotHandler slot, Boolean isLeaf, boolean ignoreEmpty)
	{
		InstanceHandler handler = slot.getChildInstanceHandler();

		if (handler == null) // Can be null only during validations, not at
								// runtime
		{
			notifyInvalidModel(slot.getRole(), "Invalid Slot", "Slot has no data type");
		}
		else if (!(ignoreEmpty && Misc.equal(BuiltinModels.NOTHING_ID, slot.getChildModelId())))
		{
			if (!(handler instanceof DataHandler))
				notifyInvalidModel(slot.getRole(), "Invalid Data Type",
						"'" + slot.getChildModelId() + "' is not a data type");

			if (isLeaf != null)
				if (isLeaf.booleanValue()) // Should be a leaf data type
				{
					if (!(handler instanceof LeafDataHandler))
						notifyInvalidModel(slot.getRole(), "Invalid Data Type",
								"'" + slot.getChildModelId() + "' is not an atomic data type");
				}
				else
				// Should be a composite data type
				{
					if (!(handler instanceof CompositeDataHandler))
						notifyInvalidModel(slot.getRole(), "Invalid Data Type",
								"'" + slot.getChildModelId() + "' is not a composite data type");
				}
		}

		return ((DataHandler) handler);
	}

	/**
	 * Ensures the slot's data type is composite with only atomic children
	 * 
	 */
	protected CompositeDataHandler checkFlatDataType(SlotHandler slot)
	{
		CompositeDataHandler dataHandler = (CompositeDataHandler) checkDataType(slot,
				Boolean.FALSE, false);
		if (dataHandler != null) // Can be null only during validations, not at
									// runtime
		{
			for (int i = 0; i < dataHandler.getNumberOfElements(); i++)
			{
				ElementHandler elementHandler = dataHandler.getElementHandler(i);
				if (!(elementHandler.getChildInstanceHandler() instanceof LeafDataHandler))
					notifyInvalidModel(slot.getRole(), "Invalid Data Type",
							"'" + slot.getChildModelId() + "' contains a non-atomic element '"
									+ elementHandler.getRole() + "'");
			}
		}

		return dataHandler;
	}

	/**
	 * Ensures the slot's data type is composite and persistent
	 * 
	 */
	protected PersistenceHandler checkPersistenceType(SlotHandler slot)
	{
		PersistenceHandler persistenceHandler = null;

		CompositeDataHandler dataHandler = (CompositeDataHandler) checkDataType(slot,
				Boolean.FALSE, false);
		if (dataHandler != null) // Can be null only during validations, not at
									// runtime
		{
			persistenceHandler = dataHandler.getPeristenceHandler();
			if (persistenceHandler == null)
				notifyInvalidModel(
						slot.getRole(),
						"Invalid Data Type",
						"'"
								+ slot.getChildModelId()
								+ "' is not a valid persistent data type (expecting a data structure with a 'tabelName' property)");
		}

		return persistenceHandler;
	}

	/**
	 * Ensures the data types of the two slots are identical (or one is Anything)
	 * 
	 */
	protected void checkTypeMismatch(SlotHandler slot1, SlotHandler slot2)
	{
		if ((slot1 != null) && (slot2 != null))
		{
			ModelId typeId1 = slot1.getChildModelId();
			ModelId typeId2 = slot2.getChildModelId();

			if (!Misc.equal(typeId1, typeId2))
				if (!Misc.equal(typeId1, BuiltinModels.ANYTHING_ID)
						&& !Misc.equal(typeId2, BuiltinModels.ANYTHING_ID))
					notifyInvalidModel(Path.EMPTY, Problems.INCONSISTENT_TYPES,
							"'" + slot1.getRole() + "' and '" + slot2.getRole()
									+ "' have different data types");
		}
	}

	private String triggerDescription(Role role, Boolean repetitive, ModelId typeId)
	{
		String descripion = (repetitive == null ? "trigger" : repetitiveness(repetitive)
				+ " trigger");
		descripion += " '" + role + "'";
		if (typeId != null)
			descripion += " of type '" + typeId + "'";
		return descripion;
	}

	private String exitDescription(Role role, Boolean repetitive, ModelId typeId)
	{
		String descripion = (repetitive == null ? "exit" : repetitiveness(repetitive) + " exit");
		descripion += " '" + role + "'";
		if (typeId != null)
			descripion += " of type '" + typeId + "'";
		return descripion;
	}

	private String repetitiveness(Boolean repetitive)
	{
		return (repetitive.booleanValue() ? "repetitive" : "non-repetitive");
	}

	protected String getTriggerText(RuntimeContext context, FlowInstance flow, SlotHandler trigger)
	{
		if (trigger == null)
			return null;
		else
			return (String) trigger.get(context, flow);
	}

	/**
	 * @param context
	 * @param flow
	 */
	protected void createParentElementReferences(RuntimeContext context, FlowInstance flow)
	{
		FlowInstance parent = flow.parent;
		if (parent == null)
			return;
		FlowHandler parentHandler = parent.getFlowHandler();
		for (int i = 0; i < elementHandlers.length; i++)
		{
			if (elementHandlers[i] instanceof FlowDataHandler)
			{
				FlowDataHandler elementHandler = (FlowDataHandler) elementHandlers[i];
				if (elementHandler.type == FlowDataElementType.PARENT)
				{
					if (elementHandler.get(context, flow) != null)
						continue; // reference already exists
					Object value = parentHandler.getContextValue(context, elementHandler.getRole(),
							parent);
					if (value != null)
					{
						InstanceHandler type = elementHandler.getChildInstanceHandler();
						if (type != null && !type.isInstance(value))
						{
							Object typeDesc = ModelIdHelper.getCompositeModelId(value);
							if (typeDesc == null)
								typeDesc = value.getClass().getName();
							throw new ModelExecutionException(
									"Incompatible type on parent reference \""
											+ elementHandler.getRole() + "\" in \""
											+ elementHandler.parentFlowHandler.getName()
											+ "\". Expected " + type.getModelId() + " but found "
											+ typeDesc);
						}
						if (flow.status == FlowStatus.RESUMED)
							elementHandler.primSet(context, flow, value);
						else
						{
							if (Misc.ASSERTIONS)
								Misc.assertion(flow.status == FlowStatus.STARTED);
							elementHandler.set(context, flow, value);
						}
					}
				}
			}
		}

	}

	protected Object getContextValue(RuntimeContext context, Role role, FlowInstance flow)
	{
		ElementHandler handler = getElementHandler(role);
		if (!(handler instanceof SubFlowHandler))
		{
			Object value = null;
			if (handler != null)
				value = handler.get(context, flow);
			if (value != null)
				return value;
		}
		if (flow.parent != null)
		{
			if (flow.parent.getFlowHandler() != null) // NICE2 remove
				// this test
				return flow.parent.getFlowHandler().getContextValue(context, role, flow.parent);
		}
		return null;
	}

}
/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. -  Initial API and implementation
 *************************************************************************************************/
package tersus.runtime.sapi;

import java.util.HashMap;

import tersus.model.ModelId;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.ModelIdHelper;
import tersus.runtime.Number;
import tersus.runtime.RuntimeContext;

/**
 * @author Youval Bronicki
 *
 */
public class SAPIManager
{

    public SAPIManager(RuntimeContext context)
    {
        this.context = context;
    }
    private HashMap<ModelId, Model> models = new HashMap<ModelId, Model>();
    public RuntimeContext context;
    
    public Model getModel(InstanceHandler handler)
    {
        Model model;
        if (models.containsKey(handler.getModelId()))
                model = (Model)models.get(handler.getModelId());
        else
        {
            model = new Model(handler);
            models.put(handler.getModelId(),model);
        }
        return model;
    }

    public Object fromInstance(InstanceHandler declaredHandler, Object value)
    {
        if (value instanceof FlowInstance)
            return getCompositeValue(value,((FlowInstance)value).getHandler());
        else
        {
            ModelId modelId = ModelIdHelper.getCompositeModelId(value);
            if (modelId == null)
                return getLeafValue(value);
            else 
            {
                if (modelId.equals(declaredHandler.getModelId()))
                    return getCompositeValue(value, declaredHandler);
                else
                    return getCompositeValue(value, context.getModelLoader().getHandler(modelId));
                        
            }
        }
        
    }

    private CompositeValue getCompositeValue(Object value, InstanceHandler handler)
    {
        Model model = getModel(handler);
        return new CompositeValue(this,model, value);
    }

    private Object getLeafValue(Object value)
    {
        if (value instanceof Number)
        {
            return new Double(((Number)value).value);
        }
        else
            return value;
    }

    public static Object toInstance(InstanceHandler childInstanceHandler, Object value)
    {
        //Todo - support conversion of maps to instances. See parseJSON
        if (value instanceof java.lang.Number)
        {
            return new Number(((java.lang.Number)value).doubleValue());
        }
        else if (value instanceof CharSequence)
        	return ((CharSequence)value).toString();
        else
            return value;
    }
}

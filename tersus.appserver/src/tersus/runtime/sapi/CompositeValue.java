/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.runtime.sapi;

import java.lang.reflect.Array;
import java.util.List;

import tersus.runtime.ElementHandler;
import tersus.runtime.InstanceHandler;
import tersus.runtime.ModelExecutionException;

/**
 * @author Youval Bronicki
 *  
 */
public class CompositeValue
{

    Model model;

    Object instance;

    SAPIManager manager;

    public CompositeValue(SAPIManager manager, Model model, Object instanceObj)
    {
        this.manager = manager;
        this.model = model;
        this.instance = instanceObj;
    }

    @SuppressWarnings("unchecked")
    public Object get(String elementName)
    {
        ElementHandler e = (ElementHandler) model.elementMap.get(elementName);
        if (e == null)
            return null;
        Object childObj = e.get(manager.context, instance);
        if (childObj == null)
            return null;
        if (e.isRepetitive())
        {
            List<Object> instances = (List<Object>) childObj;
            if (instances.size() == 0)
                return null;
            return prepareArray(e.getChildInstanceHandler(),instances);
        }
        else
        {
            return manager.fromInstance(e.getChildInstanceHandler(), childObj);
        }
    }

    private Object prepareArray(InstanceHandler declaredHandler, List<Object> instances)
    {
        Class<? extends Object> commonClass = null;
        boolean uniform = true;
        Object[] array = new Object[instances.size()];
        for (int i = 0; i < array.length; i++)
        {
            Object o = instances.get(i);
            array[i] = manager.fromInstance(declaredHandler, o);
            if (uniform)
            {
                if (commonClass == null)
                    commonClass = array[i].getClass();
                else
                    if (! commonClass.equals(array[i].getClass()))
                        uniform = false;
            }
        }
        if (uniform)
        {
            Object[] array1 = (Object[]) Array.newInstance(commonClass, array.length);
            System.arraycopy(array, 0, array1, 0, array.length);
            array = array1;
        }
        return array;
    }

    public double getDouble(String elementName, double defaultValue)
    {
        Number n = (Number)get(elementName);
        if (n != null)
            return n.doubleValue();
        else
            return defaultValue;
    }
    
    public String getString(String elementName, String defaultValue)
    {
    	String s = (String)get(elementName);
    	return s != null  ? s : defaultValue;
    }
    public Object create(String elementName)
    {
    	ElementHandler e = (ElementHandler) model.elementMap.get(elementName);
    	if (e == null)
    		throw new ModelExecutionException("No element "+elementName + " in " + model);
    	Object newChild = e.create(manager.context, instance); 
    	return manager.fromInstance(e.getChildInstanceHandler(), newChild);
    }

    public void set(String elementName, Object value)
    {
//    	if (true) throw new RuntimeException("x");
    	ElementHandler e = (ElementHandler) model.elementMap.get(elementName);
    	if (e == null)
    		throw new ModelExecutionException("No element "+elementName + " in " + model);
    	if (e.isRepetitive())
    	{
    		throw new ModelExecutionException("Can't use 'set' with repetitive element ("+elementName + ")");
    		
    	}
    	if (value instanceof Double)
    	{
    		value  = new tersus.runtime.Number((Double)value);
    	}
    	if (value instanceof CompositeValue)
    		{
    			value = ((CompositeValue)value).instance;
    		}
    	e.set(manager.context, instance,  value);
    }
}
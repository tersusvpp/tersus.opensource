/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.runtime.sapi;

import java.util.HashMap;

import tersus.runtime.ElementHandler;
import tersus.runtime.InstanceHandler;

/**
 * @author Youval Bronicki
 *
 */
public class Model
{
    private InstanceHandler handler;
    HashMap elementMap;

    public String toString()
    {
    	return handler.getModelId().toString();
    }
    public Model(InstanceHandler handler)
    {
        this.handler = handler;
        this.elementMap = new HashMap();
        for (int i=0;i<handler.elementHandlers.length;i++)
        {
            ElementHandler e = handler.elementHandlers[i];
            elementMap.put(e.getRole().toString(), e);
        }
    }
    

}

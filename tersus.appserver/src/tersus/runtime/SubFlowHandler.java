/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/

package tersus.runtime;

import java.util.HashMap;
import java.util.Map;

import tersus.model.Path;
import tersus.model.Role;

/**
 * An ElementHandler for SubFlows
 * 
 * @author Youval Bronicki
 * 
 */
public class SubFlowHandler extends FlowElementHandler
{
    
	public static final Role CACHE_TIME_KEY = Role.get("________CACHE_TIME"); /* Using role because our cache items are Map<Role,Object> - Ideally the cache should be more structured */

    /**
     * @param parentHandler
     */
    public SubFlowHandler(FlowHandler parentHandler)
    {
        super(parentHandler);
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.FlowElementHandler#invoke(tersus.runtime.RuntimeContext,
     *      tersus.runtime.IFlowElement) starts/resumes the sub-flow, then
     *      invokes outgoing links
     */
    public void invoke(RuntimeContext context, IFlowElement element)
    {
        if (element instanceof InputSet)
        {
            ((InputSet) element).explode(context);
        }
        else
        {
            FlowInstance flow = (FlowInstance) element;
            FlowHandler child = (FlowHandler) getChildInstanceHandler();
            boolean foundInCache = false;
            TriggerSet triggers = null;
            if (child.isCached()) // Only actions should be cached
            {
                triggers = new TriggerSet(context, child, flow);
                Map<Role,Object> outputSet = context.longTermCache.get(triggers);
                if (outputSet != null)
                {
                	Long cache_time = (Long)outputSet.get(CACHE_TIME_KEY);
                	float age = (System.currentTimeMillis() - (cache_time == null ? 0  : cache_time.longValue()))/1000.0f;
                	if (age <= child.getCacheMaxAgeSeconds())
                	{
                		if (context.getContextPool().cacheLog.isInfoEnabled())
                			context.getContextPool().cacheLog.info("Reusing cached results of " + triggers);
                		for (int i = 0; i < child.exits.length; i++)
                		{
                			SlotHandler exit = child.exits[i];
                			Object value = outputSet.get(exit.getRole());
                			exit.set(context, flow, value);
                		}
                        foundInCache = true;
                	}
                    // FUNC2 add relevant tracing
                }
            }
            context.currentFlowPath.addSegment(getRole());
            boolean error = true; 
            try
            {
                if (flow.status == FlowStatus.READY_TO_START)
                {
                    if (!foundInCache)
                    {
                        child.doStart(context, flow);
                        if (child.isCached())
                        {
                            if (context.getContextPool().cacheLog.isInfoEnabled())
                                context.getContextPool().cacheLog.info("Caching results for " + triggers);
                            Map outputSet = new HashMap();
                            for (int i = 0; i < child.exits.length; i++)
                            {
                                SlotHandler exit = child.exits[i];
                                Object value = exit.get(context, flow);
                                outputSet.put(exit.getRole(), value);
                            }
                            outputSet.put(CACHE_TIME_KEY, System.currentTimeMillis());
                            context.longTermCache.put(triggers, outputSet);
                        }
                    }
                }
                else
                {
                	// 2009-02-19 - relaxed the assertion because in the current implementation of messaging topology, statuses are not 100% accurate.
                    /* if (Misc.ASSERTIONS)
                        Misc
                                .assertion(flow.status == FlowStatus.READY_TO_RESUME); */
                    child.doResume(context, flow); 
                }
                error = false; // Indication that no exception was thrown
            }
            finally
            {
                if (error && context.errorPath == null)
                {
                    context.errorPath = new Path();
                    context.errorPath.copy(context.currentFlowPath);
                }
                context.currentFlowPath.removeLastSegment();
            }
            invokeLinks(context, flow);
            if (!outgoingLinks.isEmpty())
                child.clearExits(context, flow);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IElementHandler#set(java.lang.Object,
     *      java.lang.Object)
     */
    public void set(RuntimeContext context, Object parent, Object value)
    {
        super.set(context, parent, value);
        FlowInstance child = (FlowInstance) value;
        FlowInstance parentInstance = (FlowInstance) parent;
        child.parent = parentInstance;
        child.subFlowHandler = this;
        if (child.status == FlowStatus.READY_TO_START
                || child.status == FlowStatus.READY_TO_RESUME)
            parentInstance.getCompositeFlowHandler().addReadyElement(context,
                    parentInstance, child);
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.ElementHandler#create(tersus.runtime.RuntimeContext,
     *      java.lang.Object)
     */
    public Object create(RuntimeContext context, Object parent)
    {
        Object value = null;
        if (repetitive)
        {
            value = new InputSet((FlowHandler) getChildInstanceHandler());
            ((FlowHandler) getChildInstanceHandler()).setInitialStatus(context,
                    (InputSet) value);
        }
        else
            value = getChildInstanceHandler().newInstance(context);
        set(context, parent, value);
        return value;
    }
}
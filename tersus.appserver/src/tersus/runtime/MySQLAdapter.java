/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import tersus.InternalErrorException;
import tersus.util.SQLUtils;

/**
 * Contains information about Microsoft SQL Server database column types 
 * 
 * @author Youval Bronicki
 *
 */
public class MySQLAdapter extends MySQLAdapterBase implements DatabaseAdapter
{

	public boolean commit(Connection c, RuntimeContext context) throws SQLException
	{
		/* A mysql connection can start a transaction while in auto-commit mode, and will be silent about a redundant commit */
		if (c.getAutoCommit())
			context.executeUpdate(c, "commit");
		else
			c.commit();
		return true;
	}
	public boolean rollback(Connection c, RuntimeContext context) throws SQLException
	{
		/* A mysql connection can start a transaction while in auto-commit mode, and will be silent about a redundant rollback */
		if (c.getAutoCommit())
			context.executeUpdate(c, "rollback");
		else
			c.rollback();
		return true;
	}

}

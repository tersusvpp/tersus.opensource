package tersus.runtime;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import tersus.InternalErrorException;
import tersus.util.SQLUtils;

public abstract class MySQLAdapterBase implements DatabaseAdapter
{
	public boolean test(RuntimeContext context)
	{
		return true;
	}
    private static final String DATABASE_PRODUCT_NAME = "MySQL";
	/* (non-Javadoc)
	 * @see tersus.runtime.DatabaseTypeAdapter#getDouble()
	 */
    public String getSelectForUpdate(String column, String table, String condition)
    {
        return "SELECT "+column+" FROM "+table + " WHERE "+condition + " FOR UPDATE";
    }

    public String getDouble()
	{
		return "DOUBLE";
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.DatabaseTypeAdapter#getVarchar()
	 */
	public String getVarchar(int maxLength)
	{
		if (maxLength <= 0)
			return "VARCHAR(254)";
		else if (maxLength <= 254)
			return "VARCHAR(" + maxLength + ")";
		else if (maxLength < (1<<16))
			return "TEXT";
		else if (maxLength < (1<<24))
			return "MEDIUMTEXT";
		return "LONGTEXT";
	}
    public String getChar(int maxLength)
    {
        if (maxLength <= 0)
            return "CHAR(254)";
        else
            return "CHAR(" + maxLength + ")";
    }

	/* (non-Javadoc)
	 * @see tersus.runtime.DatabaseTypeAdapter#getDate()
	 */
	public String getDate()
	{
		return "DATE";
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.DatabaseTypeAdapter#getLongVarbinary()
	 */
	public String getLongVarbinary()
	{
		return "LONGBLOB";
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.DatabaseAdapter#translateMessage(java.lang.Throwable)
	 */
	public String translateMessage(SQLException e)
	{
		return e.getMessage();
	}
	
	/* (non-Javadoc)
	 * @see tersus.runtime.DatabaseAdapter#getTableTypeDDL(java.lang.String)
	 */
	public String getTableTypeDDL(String tableType)
	{
		if (tableType == null)
			return "ENGINE=InnoDB CHARACTER SET utf8";
		return "ENGINE="+tableType;
	}

	public String getExceptionType (SQLException e)
	{
		switch (e.getErrorCode()) // See http://dev.mysql.com/doc/connector/j/en/apb.html
		{
			case 1062:	return (DUPLICATE_KEY);	// ER_DUP_ENTRY
			default:	return (e.getMessage());
		}
	}
    public void shutdown(RuntimeContext context)
    {
        // This operation is not relevant for MySQL
    }
    public String convertTableName(String tableName)
    {
        return tableName;
    }

    /* (non-Javadoc)
     * @see tersus.runtime.DatabaseAdapter#alterTableSupportsMultipleColumns()
     */
    public boolean alterTableSupportsMultipleColumns()
    {
        return true;
    }

    /* (non-Javadoc)
     * @see tersus.runtime.DatabaseAdapter#getBoolean()
     */
    public String getBoolean()
    {
        return "CHAR";
    }
    public String getDatabaseProductName()
    {
        return DATABASE_PRODUCT_NAME;
    }

    /* (non-Javadoc)
     * @see tersus.runtime.DatabaseAdapter#getInt()
     */
    public String getInt()
    {
        return "INTEGER";
    }

    public String getIdentity()
	{	
    	return "INTEGER NOT NULL AUTO_INCREMENT";
	}

	public boolean useColumnNamesForDBGenereatedColumns()
	{
		return false;
	}
	/* (non-Javadoc)
     * @see tersus.runtime.DatabaseAdapter#getDateTime()
     */
    public String getDateTime()
    {
        return "DATETIME";
    }

    public boolean canResumeAfterExceptions()
    {
        return true;
    }
    public String getStoredProcedureClause(String parameterName)
    {
        return "?";
    }
    
    public String prepareSelectStatement(StringBuffer columns, StringBuffer table, String primaryKey, StringBuffer condition, String orderBy, int numberOfRecords, int offset, Set<String> optimizations)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT ");
        sql.append(columns);
        sql.append(" FROM ");
        sql.append(table);
        if (condition.length() > 0)
        {
            sql.append(" WHERE ");
            sql.append(condition);
        }
        if (orderBy != null && orderBy.length() > 0)
        {
            sql.append(" ORDER BY ");
            sql.append(orderBy);
        }
        if (numberOfRecords > 0 )
        {
            sql.append(" LIMIT ");
            sql.append(offset);
            sql.append(" , ");
            sql.append(numberOfRecords);
        }
        return sql.toString();
    }

    public boolean supportsQueryPaging()
    {

        return true;
    }
    public boolean requiresPadding()
    {
        return false;
    }
    
    public boolean supportsQueryTimeout()
	{
		return true;
	}
    static int id=0;
	synchronized static int nextId()
	{
		return ++id;
	}
	
	public String getConectionId(Connection c, RuntimeContext context)
	{
		PreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			String sql = "SELECT CONNECTION_ID()";
			st = c.prepareStatement(sql);
			rs = context.executeQuery(st, sql, Collections.emptyList(), 0);
			if (!rs.next())
			{
				throw new InternalErrorException("Query '" + sql + "' should return a result");
			}
			int id = rs.getInt(1);
			rs.close();
			rs = null;
			st.close();
			st = null;
			return "MySQL#" + id ;
		}
		catch (SQLException e)
		{
			throw new EngineException("Failed to obtain SQL Server Connection SPID", null, e);
		}
		finally
		{
			SQLUtils.forceClose(rs);
			SQLUtils.forceClose(st);
		}
	}
	
	public String prepareCountStatement(StringBuffer table, StringBuffer condition,
			HashSet<String> optimizations)
	{
		String sql = "SELECT count(1) FROM " + table;
		if (condition.length() > 0)
			sql += " WHERE " + condition;
		return sql;
	}

	public boolean commit(Connection c, RuntimeContext context) throws SQLException
	{
		if (c.getAutoCommit())
			return false;
		c.commit();
		return true;
	}
	public boolean rollback(Connection c, RuntimeContext context) throws SQLException
	{
		if (c.getAutoCommit())
			return false;
		c.rollback();
		return true;
	}
}

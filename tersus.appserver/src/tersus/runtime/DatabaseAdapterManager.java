/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.util.HashMap;

/**
 * @author Youval Bronicki
 *
 */
public class DatabaseAdapterManager
{
    HashMap adapters = new HashMap();
    public DatabaseAdapterManager()
    {
        addDatabaseAdapter(new HSQLDBAdapter());
        addDatabaseAdapter(new MySQLAdapter());
        addDatabaseAdapter(new PostgreSQLAdapter());
    }
    public void addDatabaseAdapter(DatabaseAdapter adapter)
    {
        adapters.put(adapter.getDatabaseProductName(), adapter);
    }
    public DatabaseAdapter getDatabaseAdapter(String databaseProductName)
    {
        DatabaseAdapter dbAdapter = (DatabaseAdapter)adapters.get(databaseProductName);
        if (dbAdapter == null)
            throw new EngineException("Database " + databaseProductName
                + " not supported", null, null);
        return dbAdapter;
    }


}

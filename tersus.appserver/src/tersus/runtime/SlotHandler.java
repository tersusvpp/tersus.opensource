/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import tersus.model.BuiltinModels;
import tersus.model.ModelElement;
import tersus.model.Slot;
import tersus.model.SlotType;
import tersus.runtime.trace.EventType;
import tersus.util.Enum;
import tersus.util.Misc;

/**
 * An ElementHandler for slots
 * 
 * @author Youval Bronicki
 *
 */
public class SlotHandler extends FlowElementHandler
{

	SlotType type;

	/**
	 * @param parentHandler
	 */
	public SlotHandler(FlowHandler parentHandler)
	{
		super(parentHandler);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowElementHandler#invoke(tersus.runtime.FlowElement)
	 */
	public void invoke(RuntimeContext context, IFlowElement element)
	{
		if (context.trace.traceSlots)
			context.trace.add(EventType.ACTIVATED, type, getRole(), null);
		invokeLinks(context, element);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.ElementHandler#initializeFromElement(tersus.model.ModelElement)
	 */
	public void initializeFromElement(ModelElement element)
	{
		super.initializeFromElement(element);
		type = ((Slot) element).getType();
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IElementHandler#set(java.lang.Object, java.lang.Object)
	 */
	public void set(RuntimeContext context, Object parent, Object value)
	{
		// Update the flow's status if necessary
		FlowInstance flow = (FlowInstance) parent;
		flow.set(context, this, value);
		if (type.isTrigger() && isMandatory())
			parentFlowHandler.checkMandatoryTriggers(context, flow);
		else if (type == SlotType.INPUT && value != null)
		{
/*			if (Misc.ASSERTIONS)
				Misc.assertion(
					flow.status == FlowStatus.WAITING_FOR_INPUT || flow.status == FlowStatus.READY_TO_RESUME);*/
			// 2009-02-17 Removed assertion to enable new InputAdapter
			setReadyToResume(context, flow);

		}
	}

	public void accumulate(RuntimeContext context, Object parent, Object value)
	{
		// Update the flow's status if necessary
		FlowInstance flow = (FlowInstance) parent;
		flow.accumulate(context, this, value);
		if (type == SlotType.TRIGGER && isMandatory())
			parentFlowHandler.checkMandatoryTriggers(context, flow);
		else if (type == SlotType.INPUT && value != null)
		{
			if (Misc.ASSERTIONS)
				Misc.assertion(
					flow.status == FlowStatus.WAITING_FOR_INPUT || flow.status == FlowStatus.READY_TO_RESUME);
			setReadyToResume(context, flow);

		}
	}
	private void setReadyToResume(RuntimeContext context, FlowInstance flow)
	{
		if (flow.status != FlowStatus.READY_TO_RESUME)
		{
			flow.status = FlowStatus.READY_TO_RESUME;
			FlowInstance parentFlow = flow.parent;
			if (Misc.ASSERTIONS)
				Misc.assertion(parentFlow != null);
			parentFlow.getCompositeFlowHandler().addReadyElement(context, parentFlow, flow);
		}
	}
	/* (non-Javadoc)
	 * @see tersus.runtime.ElementHandler#getType()
	 */
	public Enum getType()
	{
		return type;
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.ElementHandler#isConstant()
	 */
	protected boolean hasDefaultValue()
	{
		//Since we are using constants for branching, we don't want exits to have a default value. 
		//FUNC3 distinguish between exits and triggers.  Triggers may have a default value.
		// If we decide we want exits to hace default values, we'll need a way to distinguish the exits of the branch action.
		return false;
	}

    /**
     * @return
     */
    public boolean isEmpty()
    {
        return BuiltinModels.NOTHING_ID.equals(childModelId);
    }

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;

import java.util.Enumeration;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.Name;
import javax.naming.RefAddr;
import javax.naming.Reference;
import javax.naming.spi.InitialContextFactory;
import javax.naming.spi.ObjectFactory;

/**
 * An Object Factory that creates an InitialDirContext with the supplied reference values
 * 
 * @author Youval Bronicki
 *
 */
public class InitialDirContextFactory implements ObjectFactory
{

	/* (non-Javadoc)
	 * @see javax.naming.spi.ObjectFactory#getObjectInstance(java.lang.Object, javax.naming.Name, javax.naming.Context, java.util.Hashtable)
	 */
	public Object getObjectInstance(Object obj, Name name, Context nameCtx, Hashtable environment) throws Exception
	{
		Hashtable env = new Hashtable();
		if (environment != null)
			env.putAll(environment);
		Reference ref = (Reference) obj;
		Reference newRef = new Reference(ref.getClassName());

		String factoryClassName = null;
		//FUNC3 - perform a similar translation on the given environment
		for (Enumeration e = ref.getAll(); e.hasMoreElements();)
		{
			RefAddr ra = (RefAddr) e.nextElement();
			env.put(ra.getType(), ra.getContent());
		}
		String factoryName = (String) env.get(Context.INITIAL_CONTEXT_FACTORY);
		InitialContextFactory factory = (InitialContextFactory) Class.forName(factoryName).newInstance();
		return factory.getInitialContext(env);
	}

}

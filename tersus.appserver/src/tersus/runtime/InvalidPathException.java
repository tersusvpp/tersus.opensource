/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.runtime;


/**
 * An exception thrown when an attempt is made to use an invalid path
 * 
 * @author Youval Bronicki
 *
 */
public class InvalidPathException extends RuntimeException
{

	/**
	 * 
	 */
	public InvalidPathException()
	{
		super();
	}

	/**
	 * @param message
	 */
	public InvalidPathException(String message)
	{
		super(message);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public InvalidPathException(String message, Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * @param cause
	 */
	public InvalidPathException(Throwable cause)
	{
		super(cause);
	}

}

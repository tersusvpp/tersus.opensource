/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.runtime;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import tersus.util.Misc;

public class Field
{
    
    public Field(){};
    public String name, quotedName;

    private String sqlName;

    synchronized public String getSqlizedName()
    {
        if (sqlName == null)
            sqlName = Misc.sqlize(name);
        return sqlName;
    }
    
    public String getSQLName(boolean useQuotedIdentifiers)
    {
    	return useQuotedIdentifiers ? getQuotedName() : getSqlizedName();
    }
    synchronized public String getQuotedName()
    {
        if (quotedName == null)
        	quotedName = Misc.quote(name);
        return quotedName;
    }
    synchronized public String getUnQuotedName()
    {
        return name;
    }

    public boolean primaryKey;
    public boolean databaseGenerated;

    public PathHandler path;

    public ElementHandler element;

    public Object getValue(RuntimeContext context, Object value)
    {
        Object rawValue = path.get(context, value);
        return rawValue;
    }
    public LeafDataHandler getValueHandler()
    {
        return (LeafDataHandler)path.getValueHandler();
    }
    
    public int getSQLType()
    {
        return getValueHandler().getSQLType();
    }

    public void setValueInStatement(PreparedStatement stmt, int i,
            Object value) throws SQLException
    {
        if (value != null)
        {
            LeafDataHandler type = (LeafDataHandler) path.getValueHandler();
			Object sqlValue = type.toSQL(
                    value);
            stmt.setObject(i, sqlValue, getSQLType());
        }
        else
            stmt.setNull(i, getSQLType());
    }
    
    public void setValue(RuntimeContext context, Object obj, Object value)
    {
        path.set(context, obj, value);
    }

}

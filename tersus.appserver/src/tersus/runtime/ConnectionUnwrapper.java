package tersus.runtime;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.ConcurrentHashMap;

import tersus.InternalErrorException;

public class ConnectionUnwrapper
{
	String dummy;
	java.lang.reflect.Field dummyField; // Used to represent NULL in the fieldMap

	ConcurrentHashMap<Class<?>, java.lang.reflect.Field> fieldMap = new ConcurrentHashMap<Class<?>, java.lang.reflect.Field>();

	public ConnectionUnwrapper()
	{
		try
		{
			dummyField = getClass().getDeclaredField("dummy");
		}
		catch (Exception e)
		{
			throw new InternalErrorException("Failed to initialize " + getClass().getName(), e);
		}
	}

	public java.lang.reflect.Field getConnectionField(Class<?> cl)
	{
		java.lang.reflect.Field f = fieldMap.get(cl);
		if (f == null)
		{
			f = getField(cl, "_conn");
			if (f == null)
				f = getField(cl, "delegate");
			if (f == null)
				f = dummyField;
			else
				f.setAccessible(true);
			fieldMap.putIfAbsent(cl, f);
		}
		return f != dummyField ? f : null;
	}

	private java.lang.reflect.Field getField(Class<?> cl, String name)
	{
		java.lang.reflect.Field f = null;
		try
		{
			f = cl.getDeclaredField(name);
		}
		catch (SecurityException e)
		{
			f = null;
		}
		catch (NoSuchFieldException e)
		{
			f = null;
		}
		if (f != null && Connection.class.equals(f.getGenericType()))
			return f;
		else
			return null;
	}

	public Connection unwrap(Connection conn)
	{
		fieldMap.clear();
		Class<?> cl = conn.getClass();
		if (Proxy.isProxyClass(cl))
		{
			Object handler = Proxy.getInvocationHandler(conn);
			java.lang.reflect.Field f = getConnectionField(handler.getClass());
			if ( f!= null)
			{
				try
				{
					Connection c0 = (Connection)f.get(handler);
					return c0;
				}
				catch (IllegalArgumentException e)
				{
				}
				catch (IllegalAccessException e)
				{
				}
			}
			
		}
		java.lang.reflect.Field f = getConnectionField(cl);
		if (f == null)
		{
			try
			{
				return conn.getMetaData().getConnection(); // This might work in some cases
			}
			catch (SQLException e)
			{
				return conn;
			}
		}
		else
			try
			{
				return (Connection) f.get(conn);
			}
			catch (IllegalArgumentException e)
			{
				return conn;
			}
			catch (IllegalAccessException e)
			{
				return conn;
			}
	}
}

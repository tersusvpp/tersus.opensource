package tersus.webapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import tersus.model.BuiltinProperties;
import tersus.model.FlowModel;
import tersus.model.FlowType;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Path;
import tersus.model.SubFlow;
import tersus.runtime.PermissionSet;

public class ApplicationSettings
{
    private List<Perspective> perspectiveList = new ArrayList<Perspective>();
    private Map<String,Object> properties = new HashMap<String,Object>();
    private PermissionSet permissions;
    private List<Perspective> devicePespectives;
    
    private boolean standardsMode = false;
    public static final String TITLE = "title";
    public static final String TEXT_DIRECTION = "textDirection";
    public static final String COMPACT_NAVIGATION = "compactNavigationTabs";
	public static final String IE_LEGACY_DOCUMENT_MODE = "ieLegacyDocumentMode";
    public static final String DEV_MODE = Config.DEV_MODE;
    public static final String NOTIFICATION_SERVER ="notificationServer";
    public static final String NOTIFICATION_SERVER_APP_ID ="notificationServerAppId";
    public static final String OPTIMIZED_FOR_MOBILE = "optimizedForMobile";
    public static final Object IPHONE_VIEW = "<iPhone View>";
    public static final Object MOBILE_VIEW = "<Mobile View>";
    public static final Object DESKTOP_VIEW = "<Desktop View>";
    public static final Object TABLET_VIEW = "<Tablet View>";
	private static final String USE_STANDARDS_MODE = "USE_STANDARDS_MODE";
    public ApplicationSettings(ContextPool pool, PermissionSet permissions)
    {
        this.permissions = permissions;
        FlowModel rootModel = pool.getRootModel();
        properties.put(TITLE, getTitle(rootModel));
        properties.put(TEXT_DIRECTION, (String) rootModel
                .getProperty(BuiltinProperties.TEXT_DIRECTION));
        properties.put(DEV_MODE, (String)pool.getProperty(Config.DEV_MODE));
        properties.put(COMPACT_NAVIGATION, (String)rootModel.getProperty(BuiltinProperties.COMPACT_NAVIGATION));
        properties.put(IE_LEGACY_DOCUMENT_MODE, (String)rootModel.getProperty(BuiltinProperties.IE_LEGACY_DOCUMENT_MODE));
        properties.put(OPTIMIZED_FOR_MOBILE, (String)rootModel.getProperty(BuiltinProperties.OPTIMIZED_FOR_MOBILE));
        properties.put(NOTIFICATION_SERVER, NotificationServer.getExternalServer(pool.getServletContext()));
        properties.put(NOTIFICATION_SERVER_APP_ID, NotificationServer.getExternalServerAppId(pool.getServletContext()));
        if ("true".equals(rootModel.getProperty(BuiltinProperties.USE_STANDARDS_MODE)))
        		setStandardsMode(true);
        scanModelForViews(rootModel, new Stack<Model>(), new Path());
    }
    
    public ApplicationSettings(FlowModel rootModel)
    {
        this.permissions = new PermissionSet(new HashSet<String>()); // Assuming no permissions set on mobile view
        properties.put(TITLE, getTitle(rootModel));
        properties.put(TEXT_DIRECTION, "ltr");
        properties.put(DEV_MODE, "false");
        properties.put(COMPACT_NAVIGATION, "true");
        scanModelForViews(rootModel, new Stack<Model>(), new Path());
    }   
    private static String getTitle(FlowModel rootModel)
    {
        String title = (String) rootModel
                .getProperty(BuiltinProperties.APPLICATION_TITLE);
        if (title == null)
            title = rootModel.getName();
        return title;
    }
    /**
     * Retrieves an perspective from a List, by name.
     */
    private Perspective getPerspective(String name)
    {
        for ( Perspective p : perspectiveList)
        {
            if (p.getName().equals(name))
                return p;
        }
        return null;
    }

    private void scanModelForViews( FlowModel model,
            Stack<Model> parentModels, Path path)
    {
        if (parentModels.contains(model))
            return;
        if (! isPermitted(model))
           return;
        parentModels.add(model);
        if (model.getType() == FlowType.DISPLAY)
        {
            String perspectiveName;
            if (path.getNumberOfSegments() >= 2)
            {
                // The perspective is usually the role of the parent model
                perspectiveName = path.getSegment(
                        path.getNumberOfSegments() - 2).toString();
            }
            else
            {
                // If the parent model (or the view) is the root, the
                // perspective name is the name of the root model
                perspectiveName = ((FlowModel) parentModels.get(0)).getName();
            }
            Perspective p = getPerspective(perspectiveName);
            if (p == null)
            {
                p = new Perspective(perspectiveName);
                perspectiveList.add(p);
            }
            View view = new View(path.getLastSegment().toString(), path, model.getId());
            String name = view.getName();
            if (IPHONE_VIEW.equals(name) || MOBILE_VIEW.equals(name) || TABLET_VIEW.equals(name))
                addDeviceView(view);
            p.addView(view);
        }
        else if (model.getType() == FlowType.SYSTEM)
        {
            List<SubFlow> elements = model.getOrderedSubFlows();
            for (SubFlow e :elements)
            {
                path.addSegment(e.getRole());
                FlowModel childModel = (FlowModel) e.getReferredModel();
                scanModelForViews(childModel, parentModels, path);
                path.removeLastSegment();
            }
        }
        parentModels.pop();
    }
    private boolean isPermitted(FlowModel model)
    {
        String modelPermission = (String)model.getProperty(BuiltinProperties.REQUIRED_PERMISSION);
        return (modelPermission == null  || permissions.getSet().contains(modelPermission));
    }
    public static class Perspective
    {
        String name;

        List<View> views;

        /**
         * @return
         */
        public String getName()
        {
            return name;
        }

        /**
         * @param view
         */
        public void addView(View view)
        {
            if (views == null)
                views = new ArrayList<View>();
            views.add(view);
        }

        /**
         * @param string
         */
        public Perspective(String name)
        {
            setName(name);
        }

        public String getEscapedName()
        {
            return JavaScriptFormat.escape(name).toString();
        }

        /**
         * @return
         */
        public List<View> getViews()
        {
            return views;
        }

        /**
         * @param string
         */
        public void setName(String string)
        {
            name = string;
        }

        /**
         * @param list
         */
        public void setViews(List<View> list)
        {
            views = list;
        }
    }

    public static class View
    {
        String name;

        String pathStr;

        private ModelId id;

        public View(String name, Path path, ModelId id)
        {
            setName(name);
            this.pathStr = path.toString();
            this.id = id;
        }

        /**
         * @return
         */
        public String getName()
        {
            return name;
        }

        public String getEscapedName()
        {
            return JavaScriptFormat.escape(name).toString();
        }

        public String getEscapedPath()
        {
            return JavaScriptFormat.escape(pathStr).toString();
        }
        public String getEscapedId()
        {
            return JavaScriptFormat.escape(id.toString()).toString();
        }

        /**
         * @param string
         */
        public void setName(String string)
        {
            name = string;
        }

        public String getPath()
        {
            return pathStr;
        }

        public String getId()
        {
            return id.toString();
        }
    }

    public Map<String, Object> getProperties()
    {
        return properties;
    }
    
    
    public List<Perspective> getPerspectives()
    {
        return perspectiveList;
    }
    private void addDeviceView(View view)
    {
    	if (devicePespectives == null)
    		devicePespectives = new ArrayList<Perspective>();
    	Perspective p;
    	if (devicePespectives.isEmpty())
    	{
    		p = new Perspective("Devices");
    		devicePespectives.add(p);
    	}
    	else
    		p = devicePespectives.get(0);
    	p.addView(view);
    }
    /**
     * @return a list containing a single perspective with a single view &lt;iPhone View&gt;,
     *  or null if there is no such view
     */
    
    public List<Perspective> getDevicePerspectives()
    {
        return devicePespectives;
    }
    /**
     * @return a list containing a single perspective with a single view &lt;Desktop View&gt;,
     *  or null if there is no such view
     */

	public void setStandardsMode(boolean standardsMode)
	{
		this.standardsMode = standardsMode;
	}

	public boolean isStandardsMode()
	{
		return standardsMode;
	}
}

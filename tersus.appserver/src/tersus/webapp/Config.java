/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.webapp;


/**
 * @author Youval Bronicki
 *
 */
public interface Config
{
    String LOCAL_DIRS_KEY = "local_web_directories";
    /** 
     * When DEV_MODE is set to 'true', the server prevents client side caching of models generated files,
     * so that modifications in the model are reflected immediately. 
     */
    String DEV_MODE = "dev_mode";
	String PRODUCT = "product";
    long YEAR_SECONDS = 3600l * 24 * 365;
    long YEAR_MILLISECONDS = 1000 * Config.YEAR_SECONDS;

}

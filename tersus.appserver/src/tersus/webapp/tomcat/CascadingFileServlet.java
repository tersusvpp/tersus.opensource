/************************************************************************************************
 * Copyright (c) 2003-2007 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.webapp.tomcat;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.naming.directory.DirContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.naming.resources.FileDirContext;

import tersus.model.BuiltinProperties;
import tersus.model.FlowModel;
import tersus.model.FlowType;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.Path;
import tersus.model.SubFlow;
import tersus.util.Misc;
import tersus.webapp.Config;
import tersus.webapp.ContextPool;
import tersus.webapp.Engine;

/**
 * A Servlet that serves files according to a cascade of folders
 * 
 * @author Youval Bronicki
 * 
 */
public class CascadingFileServlet extends DefaultServlet
{

    private static final String REDIRECT_FILE = "redirect.txt";
    private static final long serialVersionUID = 1L;
    private DirContext[] directories = null;

    public void init() throws ServletException
    {
        super.init();

        
    }

    protected synchronized DirContext[] getDirectories()
    {
        if (directories == null)
        {
            ArrayList<DirContext> dirList = new ArrayList<DirContext>();
            for (File dirFile: ((ContextPool)getServletContext().getAttribute(ContextPool.POOL_KEY)).getWebDirectories())
            {
                FileDirContext dirContext = new FileDirContext();
                dirContext.setDocBase(dirFile.getPath());
                dirList.add(dirContext);
            }
            directories = dirList.toArray(new DirContext[dirList
                    .size()]);
        }
        return directories;
    }

    protected ResourceInfo findResource(String path)
    {

        DirContext[] directories = getDirectories();
        for (int i = 0; i < directories.length; i++)
        {
            ResourceInfo resourceInfo = new ResourceInfo(path, directories[i]);
            if (resourceInfo.exists())
                return resourceInfo;
        }
        return null;
    }
}
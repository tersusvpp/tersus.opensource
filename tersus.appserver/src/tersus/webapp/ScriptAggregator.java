package tersus.webapp;

import java.io.File;
import java.util.List;

import tersus.ProjectStructure;

public class ScriptAggregator extends FileAggregator
{
	public static final String SUFFIX = ".js";
	public static final String CONTENT_TYPE = "text/javascript";
	static
	{
		defaultExcludeList.addPattern("scripts/jqueryui");
		defaultExcludeList.addPattern("scripts/plugins/ui/jquery-mobile");
	}

	public ScriptAggregator(List<File> baseDirectories, File excludeFile)
	{
		setSuffix(SUFFIX);
		addDirectories(baseDirectories);
		setExcludeFile(excludeFile);
	}

	protected String getFileHeader(File file)
	{
		return "\n// file: " + file.getName() + "\n";
	}

    protected void addDirectories(List<File> baseDirectories)
    {
        for (File dir : baseDirectories)
        {
        	addDirectory(dir, dir);
        	addDirectoryTree(dir, new File(dir,ProjectStructure.SCRIPTS));
        }
    }
    
}

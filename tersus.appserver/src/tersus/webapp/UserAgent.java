package tersus.webapp;

import java.util.HashMap;
import java.util.Map;

public class UserAgent
{
	private static final String SIMULATOR_FOR = "simulator for";
	private static final String SIMULATOR = "simulator";
	private static final String APPLE_IPHONE_SIMULATOR = "iphone_simulator";
	private static final String MSIE = "msie";
	private static final String MSIE8 = "msie8";
	private static final String GECKO = "gecko";
	private static final String MOBILE = "mobile";
	private static String IPHONE = "iphone";
	private static String WEBKIT = "webkit";
	static HashMap<String, UserAgent> cache = new HashMap<String, UserAgent>();
	public static String[] allKeywords =
	{ GECKO, MSIE, IPHONE, "safari", WEBKIT, "firefox", "opera", "chrome", "android", MOBILE,
			SIMULATOR, APPLE_IPHONE_SIMULATOR, "symbian" };
	Map<String, Boolean> keywords = new HashMap<String, Boolean>();
	private String description;

	private UserAgent(String description)
	{
		if (description == null)
			description = "Unknown";
		setDescription(description);
		String lcdescription = description.toLowerCase();
		for (String keyword : allKeywords)
		{
			String searchWord;
			if (MSIE8.equals(keyword))
				searchWord = "MSIE 8";
			else
				searchWord = SIMULATOR.equals(keyword) ? SIMULATOR_FOR : keyword.replaceAll("_",
						" ");
			keywords.put(keyword, lcdescription.contains(searchWord) ? Boolean.TRUE : Boolean.FALSE);
		}
		
		if (check(MOBILE))
			keywords.put(IPHONE, Boolean.TRUE);
		if (check("android"))
			keywords.put(MOBILE, Boolean.TRUE);

	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	synchronized public static UserAgent get(String userAgentStr)
	{
		cache.clear();
		UserAgent userAgent = cache.get(userAgentStr);
		if (userAgent == null)
		{
			userAgent = new UserAgent(userAgentStr);
			cache.put(userAgentStr, userAgent);
		}
		return userAgent;
	}

	boolean check(String keyword)
	{
		return keywords.get(keyword) == Boolean.TRUE;
	}

	public boolean isIE()
	{
		return check(MSIE);
	}

	public boolean isWebKit()
	{
		return check(WEBKIT);
	}

	public boolean isIPhone()
	{
		return check(IPHONE);
	}

	public boolean isGecko()
	{
		return check(GECKO);
	}

	public boolean match(String fileName)
	{
		fileName = fileName.toLowerCase();
		for (String keyword : allKeywords)
		{
			if (check(keyword))
			{
				if (fileName.contains(".non-" + keyword + "."))
					return false;
			}
			else
			{
				if (fileName.contains("." + keyword + "."))
					return false;
			}
		}
		return true;
	}

	public boolean isMobile()
	{
		return isIPhone() || check(MOBILE);
	}
}

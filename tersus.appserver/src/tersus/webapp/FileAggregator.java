package tersus.webapp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import tersus.util.FileUtils;

abstract public class FileAggregator
{
	private class DirEntry
	{
		public DirEntry(File baseDir, File dir)
		{
			this.directory=dir;
			this.relativePath = this.directory.getPath().substring(baseDir.getPath().length()).replaceAll("\\\\", "/");
		}
		String relativePath;
		File directory;
	}
    private String suffix;
    private List<DirEntry> directories = new ArrayList<DirEntry>();
    private Set<File> allDirectories = new HashSet<File>();
    private File excludeFile = null;
    protected PatternList excludeList = null;
    protected long excludeFileLastModified = 0;
	protected static final PatternList defaultExcludeList = new PatternList();
    protected PatternList getExcludeList()
    {
    	// Not synchronized because it's OK if we get the old version in a boundary condition
    	if (excludeFile == null || !excludeFile.isFile())
    		return defaultExcludeList;
    	if (excludeFile.lastModified() > excludeFileLastModified)
    	{
    		excludeFileLastModified = excludeFile.lastModified();
    		excludeList = new PatternList(excludeFile);
    	}
    	return excludeList;
    }
    
    public long getLastModified()
    {
        return calculateLastModified(getDirectories());
    }
    private long calculateLastModified(List<DirEntry> dirList)
    {
        long timestamp = 0;
        if (excludeFile != null && excludeFile.exists())
        	timestamp = excludeFile.lastModified();
        for (DirEntry dir : dirList)
        {
            timestamp = Math.max(timestamp, dir.directory.lastModified());
            for (File file : getFiles(dir, null))
                timestamp = Math.max(timestamp, file.lastModified());
        }
        return timestamp;
    }
    public File[] getFiles(final DirEntry dir, final FilenameFilter filter)
    {
        FilenameFilter f = new FilenameFilter()
        {
            public boolean accept(File d, String name)
            {
            	String path=dir.relativePath+"/"+name;
                return (suffix == null || name.endsWith(suffix)) && (filter == null || filter.accept(d, name)) && (!getExcludeList().match(path));
            }
        };

        File[] files = dir.directory.listFiles(f);
        return files;
    }
    public void setSuffix(String suffix)
    {
        this.suffix = suffix;
    }
    public String getSuffix()
    {
        return suffix;
    }
    public void addDirectory(File baseDir, File dir)
    {
        if (dir != null && dir.isDirectory())
        {
        	File abs = dir.getAbsoluteFile();
        	if (!allDirectories.contains(abs))
        	{
        		directories.add(new DirEntry(baseDir, dir));
        		allDirectories.add(abs);
        	}
        }
    }
    
    public void addDirectoryTree(File baseDir, File dir)
    {
    	if (dir.isDirectory())
    	{
    		addDirectory(baseDir, dir);
    		for (File sub : dir.listFiles())
			{
				if (sub.isDirectory() && !sub.getName().startsWith("."))
					addDirectoryTree(baseDir, sub);
			}
    	}
    }
    public List<DirEntry> getDirectories()
    {
        return directories;
    }

    public StringBuffer getAggregatedFile(FilenameFilter filter) throws IOException
    {
        List<File> files = getAllFiles(filter);
        StringBuffer out = getAggregatedFile(files);
        return out;
    }
	protected StringBuffer getAggregatedFile(List<File> files) throws FileNotFoundException
	{
		StringBuffer out = new StringBuffer(1000000);
		for (File file : files)
        {
            out.append(getFileHeader(file));
            out.append(FileUtils.readString(new FileInputStream(file), Engine.ENCODING, true));
        }
		return out;
	}
	public ArrayList<File> getAllFiles(FilenameFilter filter)
	{
		HashMap<String, File> fileMap = new HashMap<String, File>();
        for (DirEntry dir : getDirectories())
        {
            File[] files = getFiles(dir, filter);
            for (File file : files)
            {
                if (!fileMap.containsKey(file.getName()))
                {
                    fileMap.put(file.getName(), file);
                }
            }

        }
        ArrayList<File> files = new ArrayList<File>(fileMap.size());
        files.addAll(fileMap.values());
        Collections.sort(files, new Comparator<File>()
        {

            public int compare(File o1, File o2)
            {
                return o1.getName().compareTo(o2.getName());
            }
        });
		return files;
	}
    protected abstract String getFileHeader(File file);
	public void setExcludeFile(File excludeFile)
	{
		this.excludeFile = excludeFile;
	}
}

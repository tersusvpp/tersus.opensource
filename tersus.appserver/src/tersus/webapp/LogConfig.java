package tersus.webapp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletContext;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.Hierarchy;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.spi.RootLogger;

import tersus.ProjectStructure;
import tersus.runtime.EngineException;
import tersus.util.FileUtils;

public class LogConfig
{
	ContextPool pool;
	private String logFileName;
	private String repositoryRootStr;
	private String activityLogFileName;
	private String sqlLogFileName;
	private File projectRoot;

	public LogConfig(ContextPool pool)
	{
		this.pool = pool;
		ServletContext servletContext = pool.getServletContext();
		logFileName = servletContext.getInitParameter(ContextPool.LOG_FILE_KEY);
		repositoryRootStr = servletContext.getInitParameter(Engine.REPOSITORY_ROOT_KEY);
		activityLogFileName = servletContext.getInitParameter(ContextPool.ACTIVITY_LOG_FILE_KEY);
		sqlLogFileName = servletContext.getInitParameter(ContextPool.SQL_LOG_FILE_KEY);
		projectRoot = null;
		if (repositoryRootStr != null)
		{
			projectRoot = new File(repositoryRootStr).getParentFile();
		}
	}

	public void initLogging()
	{
		System.out.println("Initializing logging");

		File configFile = getConfigFile();

		if (configFile != null)
		{
			configureByFile(configFile);
		}
		else
		{
			configureByParameters();
		}
		System.out.println("Finished initializing logging");

	}

	public void configureByParameters()
	{
		setLogFiles();
		try
		{

			RootLogger rootLogger = new RootLogger((Level) Level.INFO);
			pool.setLogHierarchy(new Hierarchy(rootLogger));
			setLoggers();
			configure(rootLogger, "%-5p %d{ISO8601} [%t] %m%n%n", logFileName);
			rootLogger.addAppender(getConsoleAppender());
			configure(pool.activityLog, "%m%n", activityLogFileName);
			configure(pool.sqlLog, "%m%n", sqlLogFileName);

			pool.getLogHierarchy().getLogger(ContextPool.POOL_LOGGER_NAME).setLevel(Level.WARN);
			// Pool log can only be turned on using log4j properties file
		}
		catch (IOException e)
		{
			System.err.println("Failed to initialize logging");
			e.printStackTrace();
		}
	}

	public ConsoleAppender getConsoleAppender()
	{
		ConsoleAppender appender2 = new ConsoleAppender(new PatternLayout(
				"%-5p %d{ISO8601} [%t] %m%n%n"));
		appender2.setThreshold(Level.WARN);
		return appender2;
	}

	public void configure(Logger logger, String pattern, String fileName) throws IOException
	{
		if (fileName == null)
		{
			logger.setLevel(Level.OFF);
			return;
		}
		PatternLayout activityLogLayout = new PatternLayout(pattern);
		DailyRollingFileAppender activityLogAppender = createAppender(fileName, activityLogLayout);
		logger.addAppender(activityLogAppender);
		logger.setLevel(Level.INFO);
		logger.setAdditivity(false);
	}

	public void setLogFiles()
	{
		if (logFileName == null)
		{
			File logFolder = getDefaultLogFolder();
			File logFile = new File(logFolder, ContextPool.DEFAULT_LOG_FILE);
			logFileName = logFile.getPath();
		}
		createParentFolders(logFileName);
		if (activityLogFileName != null)
		{
			if (!(new File(activityLogFileName)).isAbsolute())
				activityLogFileName = (new File(getDefaultLogFolder(), activityLogFileName))
						.getAbsolutePath();
			createParentFolders(activityLogFileName);
		}

		if (sqlLogFileName != null)
		{
			if (!(new File(sqlLogFileName)).isAbsolute())
				sqlLogFileName = (new File(getDefaultLogFolder(), sqlLogFileName))
						.getAbsolutePath();
			createParentFolders(sqlLogFileName);
		}
	}

	class LogConfigWatchdog extends Thread
	{
		PropertyConfigurator configurator;
		File file;
		private boolean stopped;
		private boolean running;
		long delay = 60000;
		long lastModified;
		private Properties baseProperties;

		protected LogConfigWatchdog(File file, PropertyConfigurator configurator, Properties baseProperties)
		{
			this.file = file;
			this.configurator = configurator;
			this.baseProperties = baseProperties;
			setDaemon(true);
		}
		public void configure()
		{
			Properties p = new Properties(baseProperties);
			FileInputStream in = null;
			try
			{
				in = new FileInputStream(file);
				p.load(in);
				in.close();
				in = null;
			}
			catch (IOException e)
			{
				throw new EngineException("Failed to configure logging", "config_file="+file.getPath(), e);
			}
			finally
			{
				FileUtils.forceClose(in);
			}
			if (projectRoot != null)
			{
				p.setProperty("project.root", projectRoot.getAbsolutePath());
			}
			configurator.doConfigure(p, pool.getLogHierarchy());
			setLoggers();
		}
		public void run()
		{
			setRunning(true);
			configure();
			this.setName(this.getName() + " (LogConfigWatchdog)");
			this.setStopped(false);
			this.lastModified = file.lastModified();
			while (!isStopped())
			{
				try
				{
					Thread.sleep(delay);
					long lm = file.lastModified();
					if (lm > this.lastModified)
					{
						lastModified = lm;
						configure();
					}
				}
				catch (InterruptedException e)
				{
					// Could be during cancel() and in this case we don't want to begin checking for
					// changes
				}
			}
			pool.engineLog.info("Log config file watchdog stopped");
			setRunning(false);
		}

		public void cancel()
		{
			this.setStopped(true);
			this.interrupt();
			while (isRunning())
			{
				try
				{
					Thread.sleep(50);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		}

		synchronized void setStopped(boolean stopped)
		{
			this.stopped = stopped;
		}

		synchronized boolean isStopped()
		{
			return stopped;
		}

		public void setDelay(long delay)
		{
			this.delay = delay;

		}

		synchronized public void setRunning(boolean running)
		{
			this.running = running;
		}

		synchronized public boolean isRunning()
		{
			return running;
		}

	}

	public void configureByFile(File configFile)
	{
		pool.setLogHierarchy(new Hierarchy(new RootLogger((Level) Level.INFO)));
		System.out.println("using log configuration file:" + configFile.getPath());
		PropertyConfigurator configurator = new PropertyConfigurator();
		Properties baseProperties = new Properties();
		baseProperties.setProperty("project_root", projectRoot.getPath());
		final LogConfigWatchdog watchdog = new LogConfigWatchdog(configFile, configurator, baseProperties);
		pool.addShutdownAction(new Runnable()
		{

			public void run()
			{
				watchdog.cancel();

			}
		});
		watchdog.setDelay(60000);
		watchdog.start();
	}

	public File getConfigFile()
	{
		File configFile = null;
		if (projectRoot != null)
		{
			configFile = new File(projectRoot.getParentFile(), "log4j.properties");
			if (!configFile.exists())
			{
				configFile = new File(projectRoot, "log4j.properties");
			}
		}
		if (logFileName != null)
			configFile = null;
		if (configFile != null && !configFile.exists())
			configFile = null;
		return configFile;
	}

	public void setLoggers()
	{
		pool.engineLog = pool.getLogger(ContextPool.ENGINE_LOGGER_NAME);
		pool.activityLog = pool.getLogger(ContextPool.ACTIVITY_LOGGER_NAME);
		pool.sqlLog = pool.getLogger(ContextPool.SQL_LOGGER_NAME);
		pool.poolLog = pool.getLogger(ContextPool.POOL_LOGGER_NAME);
		pool.permissionLog = pool.getLogger(ContextPool.PERMISSION_LOGGER_NAME);
		pool.cacheLog = pool.getLogger(ContextPool.CACHE_LOGGER_NAME);
		pool.connectionLog = pool.getLogger(ContextPool.CONNECTION_LOGGER_NAME);
	}

	private DailyRollingFileAppender createAppender(String logFileName, PatternLayout layout)
			throws IOException
	{
		DailyRollingFileAppender ap = new DailyRollingFileAppender();
		ap.setEncoding(Engine.ENCODING);
		ap.setLayout(layout);
		ap.setFile(logFileName, true, false, ap.getBufferSize());
		ap.setDatePattern(".yyyy-MM-dd");
		ap.activateOptions();
		return ap;
	}

	private void createParentFolders(String filename)
	{
		File file = new File(filename);
		File folder = file.getParentFile();
		if (folder != null)
			folder.mkdirs();
	}

	private File getDefaultLogFolder()
	{
		File logFolder = null;
		if (projectRoot != null)
		{
			logFolder = new File(new File(projectRoot, ProjectStructure.WORK), ProjectStructure.LOGS);
			if (!logFolder.exists())
				logFolder = new File(projectRoot, ProjectStructure.LOGS);
		}
		else
			logFolder = new File(ProjectStructure.LOGS);
		if (!logFolder.exists())
		{
			System.out.println("Creating log folder " + logFolder.getAbsolutePath());
			logFolder.mkdir();
		}
		return logFolder;
	}

}

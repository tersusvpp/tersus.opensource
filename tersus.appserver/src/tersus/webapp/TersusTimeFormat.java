package tersus.webapp;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TersusTimeFormat
{
    private static final String MINUTE_PATTERN = "yyyyMMdd-HH:mm";
    private SimpleDateFormat baseFormat = new SimpleDateFormat(MINUTE_PATTERN);
    private long lastTime;
    private long lastWholeSecond;
    private long lastWholeMinute;
    private String lastStr;
    StringBuffer buffer = new StringBuffer();
    
    public String format(long time)
    {
        if (time == lastTime)
            return lastStr;
        long millis = time%1000;
        long wholeSecond = time - millis;
        if (wholeSecond != lastWholeSecond)
        {
            long deltaFromWholeMinute = wholeSecond%60000;
            long wholeMinute = wholeSecond - deltaFromWholeMinute;
            if (wholeMinute != lastWholeMinute)
            {
                buffer.setLength(0);
                buffer.append(baseFormat.format(new Date(wholeMinute)));
                buffer.append(':');
                lastWholeMinute = wholeMinute;
            }
            buffer.setLength(MINUTE_PATTERN.length()+1);
            pad2(buffer,deltaFromWholeMinute/1000);
            buffer.append(':');
            lastWholeSecond = wholeSecond;
        }
        buffer.setLength(MINUTE_PATTERN.length()+4);
        pad3(buffer, millis);
        lastStr = buffer.toString();
        lastTime = time;
        return lastStr;
    }
    private void pad2(StringBuffer buffer, long l)
    {
        if (l<10)
            buffer.append('0');
        buffer.append(l);
    }
    private void pad3(StringBuffer buffer, long l)
    {
        if (l<10)
            buffer.append('0');
        if (l<100)
            buffer.append('0');
        buffer.append(l);
    }
    

}

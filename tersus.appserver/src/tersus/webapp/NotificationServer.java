package tersus.webapp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import tersus.util.FileUtils;

public class NotificationServer
{
    protected Map<String, HttpServletResponse> clientConnections = new HashMap<String, HttpServletResponse>();
    protected Map<String, Set<String>> clientChannels = new HashMap<String, Set<String>>();
    protected Map<String, Set<String>> channelClients = new HashMap<String, Set<String>>();
    protected Map<String, String> clients = new HashMap<String, String>();
    protected ArrayList<Message> messages = new ArrayList<Message>();

    protected MessageSender sender = new MessageSender();

    protected SimpleDateFormat dateTimeFormat;
    private SecureRandom random;
    private boolean allowRemotePublishing;
    private String externalServer;
    private Logger logger;
    private String externalServerAppId;
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Timer keepAliveTimer;

    public NotificationServer(final tersus.runtime.ContextPool pool)
    {
        logger = pool.getLogger(getClass().getName());
        logger.setLevel(Level.DEBUG); // TODO change later
        sender = new MessageSender();
        random = new SecureRandom();
        dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        dateTimeFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        ServletContext servletContext = ((ContextPool) pool).getServletContext();
        allowRemotePublishing = "true".equals(servletContext
                .getInitParameter("notification_server_allow_http_publishing"));
        externalServer = getExternalServer(servletContext);
        if (externalServer != null)
            externalServerAppId = getExternalServerAppId(servletContext);
        pool.setProperty(NotificationServer.class.getName(), this);
        pool.addShutdownAction(new Runnable()
        {

            public void run()
            {
                pool.setProperty(NotificationServer.class.getName(), null);
                destroy();
            }
        });

        Thread messageSenderThread = new Thread(sender, "NotificationSender[" + pool.getAppName() + "]");
        messageSenderThread.setDaemon(true);
        messageSenderThread.start();
        keepAliveTimer = new Timer(true);
        keepAliveTimer.schedule(new TimerTask()
        {
            public void run()
            {
                keepAlive();
            }
        }, 30000, 50000); // sending keep-alive every 50 seconds because Safari times out after a minute
    }

    public static String getExternalServerAppId(ServletContext servletContext)
    {
        String p = (String) servletContext.getInitParameter("external_notification_server_app_id");
        if (p == null)
            p = "default";
        return p;
    }

    public static String getExternalServer(ServletContext servletContext)
    {
        String extserver = (String) servletContext.getInitParameter("external_notification_server_url");
        return extserver;
    }

    public void destroy()
    {
        clientConnections.clear();
        sender.stop();
        synchronized (messages)
        {
            messages.clear();
            messages.notify();
        }
        sender = null;
        keepAliveTimer.cancel();
    }

    public void send(Date timestamp, String channel, String source, String messageType, String message, String appId)
    {
        Message msg = new Message(timestamp, channel, source, messageType, message, appId);
        if (logger.isDebugEnabled())
            logger.debug("Adding message to queue");
        synchronized (messages)
        {
            messages.add(msg);
            messages.notify();
        }
        if (logger.isDebugEnabled())
            logger.debug("Message added to queue");
    }

    protected void postToExternalServer(Message msg)
    {

        boolean debug = logger.isDebugEnabled();
        if (debug)
            logger.debug("Posting message to external server " + externalServer);
        HttpURLConnection huc = null;
        try
        {
            msg.setAppId(externalServerAppId);
            byte[] bytes = (msg.serialize()).getBytes(Engine.ENCODING);
            if (debug)
                logger.debug("Openning connection");
            huc = (HttpURLConnection) (new URL(externalServer).openConnection());
            huc.setRequestMethod("POST");
            huc.setRequestProperty("Content-Type", "text/plain;charset=UTF-8");
            huc.setRequestProperty("Content-Length", "" + bytes.length);
            huc.setDoOutput(true);
            if (debug)
                logger.debug("Openning output stream");
            OutputStream os = huc.getOutputStream();
            if (debug)
                logger.debug("Writing data");
            os.write(bytes);
            if (debug)
                logger.debug("Closing output stream");
            os.close();
            if (debug)
                logger.debug("Opening input stream");
            InputStream is = huc.getInputStream();
            if (debug)
                logger.debug("Reading response");
            byte[] response = FileUtils.readBytes(is, true);
            if (debug)
                logger.debug("Disconnecting");
            huc.disconnect();
            huc = null;
            String responseStr = new String(response, Engine.ENCODING).trim();
            if (!"OK".equals(responseStr))
                throw new NotificationServerException(
                        "Failed to post message to external server (remote server responded \"" + responseStr + "\"");
            if (debug)
                logger.debug("Message posted to external server " + externalServer);
        }
        catch (Exception e)
        {
            logger.error("Failed to post message to external notification server", e);
        }
        finally
        {
            if (huc != null)
                huc.disconnect();
        }
    }

    public void connect(HttpServletRequest request, HttpServletResponse response)
    {
        String clientId = getClientId(request);
        logger.info("connect: ip=" + request.getRemoteAddr() + " sessionId=" + request.getSession().getId()
                + " clientId=" + clientId);
        synchronized (clientConnections)
        {
            if (clientId != null)
            {
                if (request.getRemoteAddr().equals(clients.get(clientId)))
                    clientConnections.put(clientId, response);
                else
                    respond(response, "RECONNECT");
            }
            else
            {
                byte[] bytes = new byte[48];
                do
                {
                    random.nextBytes(bytes);
                    clientId = new String(Hex.encodeHex(bytes));
                } while (clients.containsKey(clientId));

                clients.put(clientId, request.getRemoteAddr());
                respond(response, "CONNECTED\t" + clientId);
            }
        }
    }

    void respond(HttpServletResponse response, String text)
    {
        try
        {
            response.getWriter().write('\n');
            response.getWriter().write(text);
            response.getWriter().write('\n');
            response.getWriter().close();
        }
        catch (Exception e)
        {
            logger.info("Failed to respond to client", e);
        }
    }

    public void disconnect(HttpServletRequest request, HttpServletResponse response)
    {
        String clientId = getClientId(request);
        logger.info("disconnect: sessionId=" + request.getSession().getId() + " clientId=" + clientId);
        if (clientId != null)
        {
            synchronized (clientConnections)
            {
                clientConnections.remove(clientId);
                clients.remove(clientId);
            }
            synchronized (clientChannels)
            {
                for (String channel : getClientChannels(clientId))
                {
                    getChannelClients(channel).remove(clientId);
                }
                clientChannels.remove(clientId);
            }
        }
        ok(response);
    }

    public void subscribe(HttpServletRequest request, HttpServletResponse response)
    {
        String channelListStr = request.getParameter("channels");
        String appId = request.getParameter("appId");
        if (appId == null)
            appId = "";
        String clientId = getClientId(request);
        logger.info("subscribe: sessionId=" + request.getSession().getId() + " clientId=" + clientId);
        if (channelListStr != null)
        {
            synchronized (clientChannels)
            {
                for (String command : channelListStr.split(","))
                {
                    char c = command.charAt(0);
                    String channelKey = appId + ":" + command.substring(1).trim();
                    if (c == '+')
                        addSubscription(clientId, channelKey);
                    else
                        removeSubscription(clientId, channelKey);
                }

            }
        }
        ok(response);
    }

    private String getClientId(HttpServletRequest request)
    {
        String clientId = request.getParameter("clientId");
        return clientId;
    }

    private Set<String> getClientChannels(String clientId)
    {
        Set<String> channels = clientChannels.get(clientId);
        if (channels == null)
        {
            channels = new HashSet<String>();
            clientChannels.put(clientId, channels);
        }
        return channels;
    }

    private Set<String> getChannelClients(String channel)
    {
        Set<String> clients = channelClients.get(channel);
        if (clients == null)
        {
            clients = new HashSet<String>();
            channelClients.put(channel, clients);
        }
        return clients;
    }

    private void addSubscription(String clientId, String channel)
    {
        getClientChannels(clientId).add(channel);
        getChannelClients(channel).add(clientId);
    }

    private void removeSubscription(String clientId, String channel)
    {
        getClientChannels(clientId).remove(channel);
        getChannelClients(channel).remove(clientId);
    }

    private class Message
    {
        public Date timestamp;
        public String channel;
        public String source;
        public String type;
        public String body;
        String appId;

        public Message(Date timestamp, String channel, String source, String type, String body, String appId)
        {
            super();
            this.channel = channel;
            this.source = source;
            this.type = type;
            this.timestamp = timestamp != null ? timestamp : new Date();
            this.body = body;
            this.appId = appId;
        }

        public void setAppId(String appId)
        {
            this.appId = appId;

        }

        public String serialize()
        {
            StringBuffer buffer = new StringBuffer();
            buffer.append("MESSAGE\t ");
            buffer.append(dateTimeFormat.format(timestamp));
            buffer.append('\t');
            if (channel != null)
                buffer.append(StringEscapeUtils.escapeJava(channel));
            buffer.append('\t');
            if (source != null)
                buffer.append(StringEscapeUtils.escapeJava(source));
            buffer.append('\t');
            if (type != null)
                buffer.append(StringEscapeUtils.escapeJava(type));
            buffer.append('\t');
            if (body != null)
                buffer.append(StringEscapeUtils.escapeJava(body));
            buffer.append('\t');
            if (appId != null)
                buffer.append(StringEscapeUtils.escapeJava(appId));
            return buffer.toString();
        }

        public String getChannelKey()
        {
            return (appId == null ? "" : appId) + ":" + channel;
        }
    }

    private class MessageSender implements Runnable
    {

        protected boolean running = true;

        public MessageSender()
        {
        }

        public void stop()
        {
            running = false;
        }

        public void run()
        {

            while (running)
            {

                try // catching all exceptions to prevent thread from dying
                {
                    if (messages.size() == 0)
                    {
                        try
                        {
                            synchronized (messages)
                            {
                                messages.wait();
                            }
                        }
                        catch (InterruptedException e)
                        {
                            // Ignore
                        }
                    }
                    if (logger.isDebugEnabled())
                        logger.debug("Processing messages");

                    Message[] pendingMessages = null;
                    synchronized (messages)
                    {
                        pendingMessages = messages.toArray(new Message[0]);
                        messages.clear();
                    }
                    for (Message message : pendingMessages)
                    {
                        if (externalServer != null)
                        {
                            postToExternalServer(message);
                        }
                        else
                        {
                            if (logger.isDebugEnabled())
                                logger.debug("Processing message");
                            String[] clients;
                            String serialization = message.serialize();
                            synchronized (clientChannels)
                            {
                                clients = getChannelClients(message.getChannelKey()).toArray(new String[0]);
                            }
                            for (String clientId : clients)
                            {
                                HttpServletResponse clientResponse;
                                if (logger.isDebugEnabled())
                                    logger.debug("Looking up client " + clientId);
                                synchronized (clientConnections)
                                {
                                    clientResponse = clientConnections.get(clientId);
                                }
                                if (clientResponse != null)
                                {
                                    synchronized (clientResponse)
                                    {
                                        if (logger.isDebugEnabled())
                                            logger.debug("Delivering to client " + clientId);
                                        respond(clientResponse, serialization);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.error("Error in notification server:", e);
                }
            }

        }

    }

    public String getClientDescription(HttpServletRequest request)
    {
        return "RemoteAddress:" + request.getRemoteAddr() + " ClientId:" + request.getParameter("clientID")
                + " RemoteUser:" + request.getRemoteUser();
    }

    public void ok(HttpServletResponse response)
    {
        respond(response, "OK");
    }

    public void removeConnection(HttpServletRequest request, HttpServletResponse response)
    {
        synchronized (clientConnections)
        {
            if (clientConnections.get(getClientId(request)) == response)
            {
                clientConnections.remove(getClientId(request));
            }
        }

    }

    public void error(HttpServletRequest request, HttpServletResponse response)
    {
    	String sessionId="Unknown";
    	String clientId="Unknown";
    	try
    	{
    		sessionId=request.getSession().getId();
    	}
    	catch (Exception e)
    	{};
    	try
    	{
    		clientId = getClientId(request);
    	}
    	catch (Exception e)
    	{
    		
    	}
        logger.error("error: sessionId=" + request.getSession().getId() + " clientId=" + getClientId(request));
        removeConnection(request, response);
    }

    public void end(HttpServletRequest request, HttpServletResponse response)
    {
        logger.info("end: clientId=" + getClientId(request));
        removeConnection(request, response);
    }

    public static NotificationServer getInstance(final tersus.runtime.ContextPool pool)
    {
        NotificationServer notificationServer = (NotificationServer) pool.getProperty(NotificationServer.class
                .getName());
        if (notificationServer == null)
        {
            notificationServer = new NotificationServer(pool);
        }
        return notificationServer;
    }

    public void handlePostedMessages(String request)
    {
        if (logger.isDebugEnabled())
            logger.debug("Processing posted message/s");
        if (!allowRemotePublishing)
            throw new NotificationServerException("Remote publishing disabled");
        for (String line : request.split("\n"))
        {
            String[] parts = line.split("\t");
            if (parts.length < 5)
            {
                throw new NotificationServerException("Invalid message format");
            }
            Date timestamp = null;
            try
            {
                timestamp = dateTimeFormat.parse(parts[1]);
            }
            catch (ParseException e)
            {
                throw new NotificationServerException("Invalid message format (bad timestamp)", e);
            }
            String channel = StringEscapeUtils.unescapeJava(parts[2]);
            String source = StringEscapeUtils.unescapeJava(parts[3]);
            String messageType = StringEscapeUtils.unescapeJava(parts[4]);
            String messageBody = StringEscapeUtils.unescapeJava(parts[5]);
            String appId = null;
            if (parts.length > 6)
                appId = StringEscapeUtils.unescapeJava(parts[6]);

            send(timestamp, channel, source, messageType, messageBody, appId);
        }

        if (logger.isDebugEnabled())
            logger.debug("Posted message/s processed");
    }

    protected void keepAlive()
    {
        HttpServletResponse[] clientResponses;
        synchronized (clientConnections)
        {
            clientResponses = clientConnections.values().toArray(new HttpServletResponse[clientConnections.size()]);
        }
        for (HttpServletResponse response : clientResponses)
        {
            try
            {
                synchronized (response)
                {
                    Writer w = response.getWriter();
                    w.write('.');
                    w.flush();
                }
            }
            catch (Exception e)
            {
                //This is best effort only
            }
        }

    }
}

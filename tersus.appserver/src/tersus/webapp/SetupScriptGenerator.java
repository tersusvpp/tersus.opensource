package tersus.webapp;

import java.util.List;
import java.util.Map;

import tersus.webapp.ApplicationSettings.Perspective;
import tersus.webapp.ApplicationSettings.View;

public class SetupScriptGenerator {

	public static StringBuffer prepareSetupScript(List<Perspective> perspectives, Map<String, Object> properties, String userName, long timestamp,
	        long cssLastModified)
	{
	    StringBuffer setupScript = new StringBuffer();
	    setupScript.append("tersus = {};tersus.settings={};\n");
	   appendProperty(setupScript, "window.userName", userName);
	    appendProperty(setupScript, "tersus.lastTimestamp", timestamp);
	    appendProperty(setupScript, "tersus.lastStyleModification", String.valueOf(cssLastModified));
	    appendProperty(setupScript, "window.textDirection", (String) properties.get(
	            ApplicationSettings.TEXT_DIRECTION));
	    appendPropertyRaw(setupScript, "tersus.compactNavigationTabs", (String) properties.get(
	            ApplicationSettings.COMPACT_NAVIGATION));
	    appendPropertyRaw(setupScript, "tersus.settings.dev_mode", (String) properties.get(
	            ApplicationSettings.DEV_MODE));
	    appendProperty(setupScript, "tersus.urlPathSuffix",(String)properties.get(Engine.URL_PATH_SUFFIX));
	    appendPropertyRaw(setupScript, "tersus.useSQLQuotedIdentifiers", String.valueOf(properties.get(ContextPool.USE_SQL_QUOTED_IDENTIFIERS)));
	    String notificationServer = (String) properties.get(ApplicationSettings.NOTIFICATION_SERVER);
	    if (notificationServer != null)
	    {
	        appendProperty(setupScript, "tersus.notificationServer", notificationServer);
	        appendProperty(setupScript, "tersus.notificationServerAppId", (String) properties.get(
	                ApplicationSettings.NOTIFICATION_SERVER_APP_ID));
	    }
	    setupScript.append("window.perspectives=[\n");
	    boolean firstP = true;
	    for (Perspective p : perspectives)
	    {
	        if (!firstP)
	            setupScript.append(",");
	        firstP = false;
	
	        setupScript.append("{");
	        appendProperty(setupScript, "name", p.getName(), ":", ",");
	        setupScript.append("views:[");
	        boolean firstV = true;
	        for (View v : p.getViews())
	        {
	            if (!firstV)
	                setupScript.append(",");
	            firstV = false;
	            setupScript.append("{");
	            appendProperty(setupScript, "name", v.getName(), ":", ",");
	            appendProperty(setupScript, "path", v.getPath(), ":", ",");
	            appendProperty(setupScript, "id", v.getId(), ":", "");
	            setupScript.append("} ");
	
	        }
	        setupScript.append("]}\n");
	    }
	    setupScript.append("];\n");
	    return setupScript;
	}

    static void appendProperty(StringBuffer buffer, String name, String value)
    {
        appendProperty(buffer, name, value, "=", ";\n");
    }

    static void appendProperty(StringBuffer buffer, String name, String value, String separator, String suffix)
    {
        buffer.append(name);
        buffer.append(separator);
        buffer.append("'");
        buffer.append(JavaScriptFormat.escape(value));
        buffer.append("'");
        buffer.append(suffix);
    }

    static void appendPropertyRaw(StringBuffer buffer, String name, String value)
    {
        buffer.append(name);
        buffer.append("=");
        buffer.append(value);
        buffer.append(";\n");
    }

    static void appendProperty(StringBuffer setupScript, String name, long value)
    {
        setupScript.append(name);
        setupScript.append("=");
        setupScript.append(String.valueOf(value));
        setupScript.append(";\n");
    }

}

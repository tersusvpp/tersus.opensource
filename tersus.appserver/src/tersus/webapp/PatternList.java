package tersus.webapp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import tersus.runtime.EngineException;

public class PatternList
{
	private List<Pattern> patterns = new ArrayList<Pattern>();

	public PatternList()
	{

	}

	public static final PatternList EMPTY = new PatternList();

	public PatternList(File file)
	{
		load(file);
	}

	public void load(File file)
	{
		if (!file.exists())
			return;
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(file));
			while (true)
			{
				String line = reader.readLine();
				if (line == null)
					break;
				addPattern(line);
			}
		}
		catch (IOException e)
		{
			throw new EngineException("Error in file " + file.getAbsolutePath(), null, e);
		}
	}

	public void addPattern(String pattern)
	{
		if (pattern == null)
			return;
		pattern = pattern.trim();
		if (pattern.length() > 0)
			patterns.add(Pattern.compile(pattern));
	}

	public boolean match(String path)
	{
		for (Pattern p : patterns)
			if (p.matcher(path).find())
				return true;
		return false;
	}

}

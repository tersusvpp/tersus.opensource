package tersus.webapp;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import tersus.util.FileUtils;

/**
 * Filter for dumping HTTP Requests (header and content).
 * To use the filter, web.xml has to be modified
 * @author youval
 *
 */
/*
 * 
 * <filter>
 *   <filter-name>RequestDumpFilter</filter-name>
 *   <filter-class>tersus.webapp.RequestDumpFilter</filter-class>
 * </filter>

 * <filter-mapping>
 *   <filter-name>RequestDumpFilter</filter-name>
 *   <url-pattern>/*</url-pattern>
 * </filter-mapping>
 * There is a default output folder for the dump files (/tmp/request_dumps), which can be overridden using init paramter output_folder
 * */

 
public class RequestDumpFilter implements Filter
{

	private static final String DEFAULT_OUTPUT_FOLDER = "/tmp/request_dumps";
	File outputFolder;
	public void init(FilterConfig filterConfig) throws ServletException
	{
		String folder = filterConfig.getInitParameter("output_folder");
		if (folder==null)
			folder = DEFAULT_OUTPUT_FOLDER;
		outputFolder = new File(folder);
		outputFolder.mkdirs();
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException
	{
		HttpServletRequest req = (HttpServletRequest)request;
		String prefix = String.valueOf(System.currentTimeMillis());
		FileWriter hw = new FileWriter(new File(outputFolder, prefix+".header"));
		@SuppressWarnings("unchecked")
		Enumeration<String> names = req.getHeaderNames();
		while (names.hasMoreElements())
		{
			String name = names.nextElement();
			@SuppressWarnings("unchecked")
			Enumeration<String> values = req.getHeaders(name);
			while(values.hasMoreElements())
			{
				hw.write(name +":"+values.nextElement()+"\n");
			}
		}
		hw.close();
		RequestWrapper rw = new RequestWrapper(req);
		File dumpFile = new File(outputFolder, prefix + ".request");
		FileUtils.write(rw.data, dumpFile);
		chain.doFilter(rw, response);
	}

	public void destroy()
	{
		// TODO Auto-generated method stub

	}

	private class RequestWrapper extends HttpServletRequestWrapper
	{

		byte[] data;
		ServletInputStream is;

		public RequestWrapper(HttpServletRequest request)
		{
			super(request);
			try
			{
				data = FileUtils.readBytes(request.getInputStream(), true);
				final ByteArrayInputStream bis = new ByteArrayInputStream(data);
				is = new ServletInputStream()
				{
					public int read() throws IOException
					{
						return bis.read();
					}

					public int read(byte[] b, int off, int len) throws IOException
					{
						return bis.read(b, off, len);
					}

					public int available() throws IOException
					{
						return bis.available();
					}
				};
			}
			catch (IOException e)
			{
				throw new RuntimeException(e);
			}
		}

		public ServletInputStream getInputStream() throws IOException
		{
			return is;
		}

	}

}

/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.webapp;

import java.io.File;

import tersus.model.FileRepository;
import tersus.model.Repository;

/**
 * Default IRepositoryController implementation: Creates a FileRepository, Completely clears repository cache on reload 
 * @author Youval Bronicki
 *
 */
public class DefaultRepositoryController implements IRepositoryController
{

    public Repository createRepository(String repositoryRoot, String libraryPaths)
    {
         Repository   repository = new FileRepository();
            ((FileRepository) repository).setRoot(new File(repositoryRoot));
        if (libraryPaths != null)
        {
            repository.addLibraries(libraryPaths);
        }
        return repository;
    }

    public void reload(Repository repository)
    {
        repository.clearCache();
    }

}

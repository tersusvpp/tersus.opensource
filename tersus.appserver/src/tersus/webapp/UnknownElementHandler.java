package tersus.webapp;

import tersus.model.BuiltinModels;
import tersus.runtime.ElementHandler;
import tersus.runtime.InstanceHandler;
import tersus.runtime.RuntimeContext;

public class UnknownElementHandler extends ElementHandler
{
	
	private static final String DUMMY="";

	public UnknownElementHandler(InstanceHandler parentHandler)
	{
		super(parentHandler);
		this.loader = parentHandler.getLoader();
		childModelId = BuiltinModels.ANYTHING_ID;
	}

	

	@Override
	public Object create(RuntimeContext context, Object parent)
	{
		return DUMMY;
	}

}

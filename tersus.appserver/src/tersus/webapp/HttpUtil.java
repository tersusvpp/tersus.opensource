/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.webapp;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import tersus.util.InvalidInputException;

/**
 * @author Youval Bronicki
 *  
 */
public class HttpUtil
{
    public static String getURIEncodedParameter(HttpServletRequest request,
            String name)
    {

        return decode(getRawParameter(request, name));
    }

    /**
     * @param encodedPath
     * @return
     */
    public static String decode(String encodedString)
    {
        if (encodedString == null)
            return null;
        int len = encodedString.length();
        StringBuffer out = new StringBuffer();
        byte[] bytes = new byte[encodedString.length()];
        int nBytes = 0;
        int i = 0;
        while (i < len)
        {
            char c = encodedString.charAt(i);
            if (c != '%')
            {
                out.append(c);
                ++i;
                continue;
            }
            nBytes = 0;
            while ( i < len && encodedString.charAt(i) == '%')
            {
                if (i + 2 >= len)
                    throw new InvalidInputException(encodedString
                            + " is not a valid URI encoded string");
                char h1 = encodedString.charAt(i + 1);
                char h2 = encodedString.charAt(i + 2);
                bytes[nBytes++] = getByteFromHex(h1, h2);
                i += 3;
            }
            if (nBytes > 0)
            {
                byte[] bytes1 = new byte[nBytes];
                System.arraycopy(bytes, 0, bytes1, 0, nBytes);
                try
                {
                    out.append(new String(bytes1, "UTF8"));
                }
                catch (UnsupportedEncodingException e)
                {
                    throw new RuntimeException(e);
                }
            }
        }
        return out.toString();
    }

    /**
     * @param h1
     * @param h2
     * @return
     */
    public static byte getByteFromHex(char h1, char h2)
    {
        return (byte) (getHex(h1) * 16 + getHex(h2));
    }

    /**
     * @param h1
     * @return
     */
    public static int getHex(char c)
    {
        if ((c >= '0') && (c <= '9'))
            return c - '0';
        if ((c >= 'a') && (c <= 'f'))
            return c - 'a' + 10;
        if ((c >= 'A') && (c <= 'F'))
            return c - 'A' + 10;
        throw new IllegalArgumentException(c
                + " is not a hexadecimal character");
    }

    /**
     * @param request
     * @param string
     * @return
     */
    public static String getRawParameter(HttpServletRequest request, String name)
    {
        String queryString = request.getQueryString();
        if (queryString == null)
            return null;
        String prefix = name + "=";
        int parameterStart = queryString.indexOf(prefix);
        if (parameterStart < 0)
            return null;
        parameterStart += prefix.length();
        int end = queryString.indexOf('?', parameterStart);
        if (end < 0)
            end = queryString.length();
        String encodedParameter = queryString.substring(parameterStart, end);
        return encodedParameter;
    }

}
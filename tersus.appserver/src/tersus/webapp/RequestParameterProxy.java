package tersus.webapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.AsyncContext;
import javax.servlet.DispatcherType;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.catalina.util.RequestUtil;

import tersus.InternalErrorException;

public class RequestParameterProxy implements HttpServletRequest
{
	private HttpServletRequest baseRequest;
	private Map parameters;

	public RequestParameterProxy(HttpServletRequest baseRequest)
	{
		this.baseRequest = baseRequest;
		this.parameters = new HashMap();
		RequestUtil.parseParameters(this.parameters, baseRequest.getQueryString(), Engine.ENCODING);
	}

	public String getParameter(String name)
	{
		String[] v =  (String[])parameters.get(name);
		if (v!= null && v.length > 0)
			return v[0];
		else
			return null;
	}

	public Map getParameterMap()
	{
		return parameters;
	}

	public Enumeration getParameterNames()
	{
		return Collections.enumeration(parameters.keySet());
	}

	public String[] getParameterValues(String name)
	{
		return (String[]) parameters.get(name);
	}

	public Object getAttribute(String name)
	{
		return baseRequest.getAttribute(name);
	}

	public Enumeration getAttributeNames()
	{
		return baseRequest.getAttributeNames();
	}

	public String getAuthType()
	{
		return baseRequest.getAuthType();
	}

	public String getCharacterEncoding()
	{
		return baseRequest.getCharacterEncoding();
	}

	public int getContentLength()
	{
		return baseRequest.getContentLength();
	}

	public String getContentType()
	{
		return baseRequest.getContentType();
	}

	public String getContextPath()
	{
		return baseRequest.getContextPath();
	}

	public Cookie[] getCookies()
	{
		return baseRequest.getCookies();
	}

	public long getDateHeader(String name)
	{
		return baseRequest.getDateHeader(name);
	}

	public String getHeader(String name)
	{
		return baseRequest.getHeader(name);
	}

	public Enumeration getHeaderNames()
	{
		return baseRequest.getHeaderNames();
	}

	public Enumeration getHeaders(String name)
	{
		return baseRequest.getHeaders(name);
	}

	public ServletInputStream getInputStream() throws IOException
	{
		return baseRequest.getInputStream();
	}

	public int getIntHeader(String name)
	{
		return baseRequest.getIntHeader(name);
	}

	public String getLocalAddr()
	{
		return baseRequest.getLocalAddr();
	}

	public Locale getLocale()
	{
		return baseRequest.getLocale();
	}

	public Enumeration getLocales()
	{
		return baseRequest.getLocales();
	}

	public String getLocalName()
	{
		return baseRequest.getLocalName();
	}

	public int getLocalPort()
	{
		return baseRequest.getLocalPort();
	}

	public String getMethod()
	{
		return baseRequest.getMethod();
	}

	public String getPathInfo()
	{
		return baseRequest.getPathInfo();
	}

	public String getPathTranslated()
	{
		return baseRequest.getPathTranslated();
	}

	public String getProtocol()
	{
		return baseRequest.getProtocol();
	}

	public String getQueryString()
	{
		return baseRequest.getQueryString();
	}

	public BufferedReader getReader() throws IOException
	{
		return baseRequest.getReader();
	}

	public String getRealPath(String path)
	{
		return baseRequest.getRealPath(path);
	}

	public String getRemoteAddr()
	{
		return baseRequest.getRemoteAddr();
	}

	public String getRemoteHost()
	{
		return baseRequest.getRemoteHost();
	}

	public int getRemotePort()
	{
		return baseRequest.getRemotePort();
	}

	public String getRemoteUser()
	{
		return baseRequest.getRemoteUser();
	}

	public RequestDispatcher getRequestDispatcher(String path)
	{
		return baseRequest.getRequestDispatcher(path);
	}

	public String getRequestedSessionId()
	{
		return baseRequest.getRequestedSessionId();
	}

	public String getRequestURI()
	{
		return baseRequest.getRequestURI();
	}

	public StringBuffer getRequestURL()
	{
		return baseRequest.getRequestURL();
	}

	public String getScheme()
	{
		return baseRequest.getScheme();
	}

	public String getServerName()
	{
		return baseRequest.getServerName();
	}

	public int getServerPort()
	{
		return baseRequest.getServerPort();
	}

	public String getServletPath()
	{
		return baseRequest.getServletPath();
	}

	public HttpSession getSession()
	{
		return baseRequest.getSession();
	}

	public HttpSession getSession(boolean create)
	{
		return baseRequest.getSession(create);
	}

	public Principal getUserPrincipal()
	{
		return baseRequest.getUserPrincipal();
	}

	public boolean isRequestedSessionIdFromCookie()
	{
		return baseRequest.isRequestedSessionIdFromCookie();
	}

	public boolean isRequestedSessionIdFromUrl()
	{
		return baseRequest.isRequestedSessionIdFromUrl();
	}

	public boolean isRequestedSessionIdFromURL()
	{
		return baseRequest.isRequestedSessionIdFromURL();
	}

	public boolean isRequestedSessionIdValid()
	{
		return baseRequest.isRequestedSessionIdValid();
	}

	public boolean isSecure()
	{
		return baseRequest.isSecure();
	}

	public boolean isUserInRole(String role)
	{
		return baseRequest.isUserInRole(role);
	}

	public void removeAttribute(String name)
	{
		baseRequest.removeAttribute(name);
	}

	public void setAttribute(String name, Object o)
	{
		baseRequest.setAttribute(name, o);
	}

	public void setCharacterEncoding(String env)
			throws UnsupportedEncodingException
	{
		baseRequest.setCharacterEncoding(env);
	}

	public AsyncContext getAsyncContext()
	{
		return baseRequest.getAsyncContext();
	}

	public boolean isAsyncStarted()
	{
		return baseRequest.isAsyncStarted();
	}

	public boolean isAsyncSupported()
	{
		return baseRequest.isAsyncSupported();
	}

	public AsyncContext startAsync()
	{
		return baseRequest.startAsync();
	}

	public AsyncContext startAsync(ServletRequest arg0, ServletResponse arg1)
	{
		return baseRequest.startAsync(arg0, arg1);
	}

	public boolean authenticate(HttpServletResponse arg0) throws IOException, ServletException
	{
		return baseRequest.authenticate(arg0);
	}

	public DispatcherType getDispatcherType()
	{
		return baseRequest.getDispatcherType();
	}

	public Part getPart(String arg0) throws IOException, IllegalStateException, ServletException
	{
		return baseRequest.getPart(arg0);
	}

	public Collection<Part> getParts() throws IOException, IllegalStateException, ServletException
	{
		return baseRequest.getParts();
	}

	public ServletContext getServletContext()
	{
		return baseRequest.getServletContext();
	}

	public void login(String arg0, String arg1) throws ServletException
	{
		baseRequest.login(arg0, arg1);
	}

	public void logout() throws ServletException
	{
		baseRequest.logout();
	}

}

package tersus.webapp.appexport;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

import tersus.util.FileUtils;

public class ExportUtils
{
	public static final String DEVICE_NAME_PROPERTY_SUBKEY = "device.name";
	public static final String PACKAGE_NAME_PROPERTY_SUBKEY = "package.name";
	public static final String CLASS_NAME_PROPERTY_SUBKEY = "class.name";
	public static final String EXPORT_LOCATION_PROPERTY_SUBKEY = "export.location";
	public static final String LAST_TARGET_PLATFORM_PROPERTY_KEY = "last.target.platform";
	
	public static String convertName(String projectName)
	{
		boolean uc = true;
		boolean first = true;
		StringBuffer out = new StringBuffer();
		for (int i = 0; i < projectName.length(); i++)
		{
			char ch = projectName.charAt(i);
			if (first && Character.isJavaIdentifierStart(ch))
			{
				out.append(Character.toUpperCase(ch));
				uc = false;
				first = false;
			}
			else if (!first && Character.isJavaIdentifierPart(ch))
			{
				out.append(uc ? Character.toUpperCase(ch) : ch);
				uc = false;
				first = false;
			}
			else if (Character.isSpaceChar(ch))
			{
				uc = true;
				first = false;
			}
			else
			{
				out.append("_");
				uc = false;
				first = false;
			}
		}
		return out.toString();
	}
	
	public static Properties getExportProperties (IExportAdapter adapter)
	{
		Properties properties = new Properties();
		File file = new File(adapter.getProjectFolder(), "mobile.export.properties");
		if (file.exists())
		{
			FileInputStream in = null;
			try
			{
				in = new FileInputStream(file);
				properties.load(in);
			}
			catch (Exception e)
			{
				throw new ExportException("Failed to read export.properties", e);
			}
			finally
			{
				FileUtils.forceClose(in);
			}

		}
		return properties;
	}

	public static void saveProperties (IExportAdapter adapter, Properties properties)
	{
		File file = new File(adapter.getProjectFolder(), "mobile.export.properties");
		FileOutputStream out = null;
			try
			{
				out = new FileOutputStream(file);
				properties.store(out, "Saved by export wizard");
			}
			catch (Exception e)
			{
				throw new ExportException("Failed to save mobile.export.properties", e);
			}
			finally
			{
				FileUtils.forceClose(out);
			}

	}

	public static String getPlatformKey(IExportAdapter adapter)
	{
		return adapter.getTargetPlatform().toLowerCase().replaceAll(" ", "-");
	}
}

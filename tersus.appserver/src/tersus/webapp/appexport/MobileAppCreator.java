package tersus.webapp.appexport;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.imageio.ImageIO;

import org.htmlparser.Parser;
import org.htmlparser.filters.TagNameFilter;
import org.htmlparser.tags.BodyTag;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;

import tersus.ProjectStructure;
import tersus.model.BuiltinProperties;
import tersus.model.FlowModel;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Repository;
import tersus.util.EscapeUtil;
import tersus.util.FileUtils;
import tersus.util.Misc;
import tersus.util.Template;
import tersus.webapp.ApplicationSettings;
import tersus.webapp.CSSAggregator;
import tersus.webapp.Engine;
import tersus.webapp.PatternList;
import tersus.webapp.ScriptAggregator;
import tersus.webapp.SetupScriptGenerator;
import tersus.webapp.UserAgent;
import tersus.webapp.jsloader.JavascriptGenerator;

public abstract class MobileAppCreator
{
	protected IExportAdapter adapter;
	private String serverURL;
	protected Properties properties = null;
	protected Collection<File> webFiles;
	private String lineSeparator = null;
	private  boolean addBOMToTextFiles = false;
	
	protected PatternList webExcludes = new PatternList();

	public abstract void export();

	public MobileAppCreator(IExportAdapter adapter)
	{
		this.adapter = adapter;
		properties = ExportUtils.getExportProperties(adapter);
	}

	public void exportMobileApplication(File wwwFolder, File exportFolder)
	{
		if (!wwwFolder.exists())
			wwwFolder.mkdirs();
		Model rootModel = adapter.getRootModel();
		exportScripts(wwwFolder);
		exportWelcomePage(rootModel, wwwFolder);
		exportModels(rootModel, wwwFolder);
		exportStyles(rootModel, wwwFolder);
		exportWebFolders(wwwFolder);
		exportImages(adapter.getApplicationIdentifier(), exportFolder);
		exportTemplates(wwwFolder);
	}

	protected void exportWelcomePage(Model rootModel, File wwwFolder)
	{
		File templateFile = getWelcomePageTemplate();
		ApplicationSettings settings = new ApplicationSettings((FlowModel) rootModel);
		if (settings.getDevicePerspectives() == null)
			throw new ExportException("A mobile application must contain either "
					+ ApplicationSettings.MOBILE_VIEW + " or " + ApplicationSettings.IPHONE_VIEW + ", or "+ ApplicationSettings.TABLET_VIEW);
		StringBuffer setupScript = SetupScriptGenerator.prepareSetupScript(
				settings.getDevicePerspectives(), new HashMap<String, Object>(), "", 0, 0);
		if (serverURL != null)
			setupScript.append("tersus.serverURL='" + serverURL + "';");

		File indexFile = new File(wwwFolder, "index.html");
		Writer out = null;
		try
		{
			File file = new File(this.getProjectFolder(), "web/loading.txt");
			String loadingText = "Loading ...";
			if (file.exists())
				loadingText = FileUtils.readString(new FileReader(file), true).toString();
			String content = FileUtils.readString(new FileInputStream(templateFile),
					FileUtils.UTF8, true);
			String scriptFragment = getScriptFragment();
			content = Misc.replaceAll(content, "SCRIPTS_FILE_LIST", scriptFragment);

			Template template = new Template(content);
			out = new OutputStreamWriter(new FileOutputStream(indexFile), FileUtils.UTF8);
			if (addBOMToTextFiles)
				out.write('\ufeff');
			Map<String, Object> values = new HashMap<String, Object>();
			values.put("setup", setupScript);
			values.put("loading", loadingText);
			template.write(out, values);
			out.close();
			out = null;
		}
		catch (IOException e1)
		{
			throw new ExportException("Failed to export " + indexFile.getName(), e1);
		}
		finally
		{
			if (out != null)
			{
				try
				{
					out.close();
				}
				catch (Exception e2)
				{
					e2.printStackTrace();// Ignoring secondary exception
				}
			}
		}
	}

	protected File getWelcomePageTemplate()
	{
		File templateFile = new File(adapter.getTemplateFolder(), "index.html");
		return templateFile;
	}

	public String getScriptFragment()
	{
		String scriptFragment = "<script language=\"Javascript\" src=\"scripts.js\"></script>";
		return scriptFragment;
	}

	protected void exportModels(Model rootModel, File wwwFolder)
	{
		File modelsFile = new File(wwwFolder, "models.js");

		HashSet<ModelId> uiModels = ((Repository) rootModel.getRepository())
				.getUpdatedRepositoryIndex().calculateUIModelIds(rootModel, null);
		try
		{
			String code = JavascriptGenerator.createScript((Repository) rootModel.getRepository(),
					uiModels, new HashSet<ModelId>(), null /* package list */, "tersus.preload",
					true);
			writeTextFile(modelsFile,code);
		}
		catch (IOException e)
		{
			throw new ExportException("Failed to export " + modelsFile.getName());
		}
	}

	protected void exportScripts(File wwwFolder)
	{
		File scriptsFile = new File(wwwFolder, "scripts.js");
		try
		{
			final UserAgent userAgent = UserAgent.get(getUserAgentStr());
			ScriptAggregator aggregator = new ScriptAggregator(getWebDirectories(), new File(adapter.getProjectFolder(),ProjectStructure.SCRIPT_EXCLUDE));
			StringBuffer scripts = aggregator.getAggregatedFile(new FilenameFilter()
			{

				public boolean accept(File dir, String name)
				{
					return userAgent.match(name) && !name.equals(ProjectStructure.DEFAULT_NAV_SCRIPT_NAME) && !name.equals(ProjectStructure.URL_NAV_SCRIPT_NAME);

				}
			});

			writeTextFile(scriptsFile, scripts.toString());

		}
		catch (IOException e)
		{
			throw new ExportException("Failed to export " + scriptsFile.getName());
		}
	}

	protected void writeTextFile(File file, String content) throws IOException,
			UnsupportedEncodingException
	{
		FileOutputStream os = null;
		try
		{
			if (lineSeparator != null)
				content=content.replaceAll("[\\n]", lineSeparator);
			os = new FileOutputStream(file);
			Writer writer = new BufferedWriter(new OutputStreamWriter(os, FileUtils.UTF8));
			if (addBOMToTextFiles)
				writer.write('\ufeff');
			writer.write(content);
			writer.close();
			os.close();
			os = null;
		}
		finally
		{
			FileUtils.forceClose(os);
		}
	}

	protected abstract String getUserAgentStr();

	protected void exportStyles(Model rootModel, File wwwFolder)
	{
		File cssFile = new File(wwwFolder, "styles.css");
		try
		{
			CSSAggregator aggregator = new CSSAggregator(getWebDirectories(), new File(adapter.getProjectFolder(),ProjectStructure.STYLE_EXCLUDE));

			String cssExclude = (String) rootModel.getProperty(BuiltinProperties.OLD_CSS_EXCLUDE);
			final boolean excludeOldCSS = "all".equals(cssExclude) || "mobile".equals(cssExclude);

			final UserAgent userAgent = UserAgent.get(getUserAgentStr());
			StringBuffer styles = aggregator.getAggregatedFile(new FilenameFilter()
			{

				public boolean accept(File dir, String name)
				{
					return userAgent.match(name) && !(excludeOldCSS && dir.getName().equals("old"));
				}
			});
			writeTextFile(cssFile, styles.toString());

		}
		catch (IOException e)
		{
			throw new ExportException("Failed to export " + cssFile.getName());
		}
	}

	/**
	 * Copies web resources to the target folder.
	 */
	protected void exportWebFolders(File wwwFolder)
	{
		// We first create the list of files, based on precedence (project
		// specific takes
		// precedence)
		// Then we copy all recently modified files
		HashMap<File, File> files = new HashMap<File, File>();
		for (File folder : getWebDirectories())
			scanWebFolder(folder, folder, wwwFolder, files);
		for (Entry<File, File> entry : files.entrySet())
		{
			File target = entry.getKey();
			File source = entry.getValue();
			try
			{
				if (target.isDirectory())
					FileUtils.delete(target);
				if (target.exists() && target.lastModified() >= source.lastModified())
					continue;
				target.getParentFile().mkdirs();
				FileInputStream is = new FileInputStream(source);
				FileOutputStream os = new FileOutputStream(target);
				FileUtils.copy(is, os, true);
				target.setLastModified(source.lastModified());
			}
			catch (Exception e)
			{
				throw new ExportException("Failed to export " + source.getPath(), e);
			}

		}
		webFiles = new ArrayList<File>(files.size());
		webFiles.addAll(files.keySet());
	}

	protected void addWebExclude(String pattern)
	{
		webExcludes.addPattern(pattern);
	}
	protected void exportImages(String appName, File exportFolder)
	{

	}

	protected void exportTemplates(File wwwFolder)
	{
		File outputFile = new File(wwwFolder, "templates.js");
		String js = getPreloadTempalteJS("popin.html") + getPreloadTempalteJS("actiondialog.html")
				+ getPreloadTempalteJS("blank.html");
		try
		{
			writeTextFile(outputFile, js);
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			throw new ExportException("Filed to generate " + outputFile.getName());
		}
	}

	protected List<File> getWebDirectories()
	{
		try
		{
			List<File> webDirectories = new ArrayList<File>();
			File webFolder = getWebFolder();
			String baseName = ExportUtils.getPlatformKey(adapter);
			File platformWebFolder = getPlatformWebFolder();
			if (platformWebFolder.isDirectory())
				webDirectories.add(platformWebFolder);
			String framework = getFrameworkName();
			if (framework != null)
			{
				File framweworkWebFolder = new File(getTemplateFolder(), baseName + "." + framework
						+ "-web");
				webDirectories.add(framweworkWebFolder);
			}

			webDirectories.add(adapter.getServerWebDirectory());
			webDirectories.add(webFolder);
			return webDirectories;
		}
		catch (Exception e)
		{
			throw new ExportException("Failed to initialize web folders", e);
		}
	}

	protected File getPlatformWebFolder()
	{
		String baseName = ExportUtils.getPlatformKey(adapter);
		File platformWebFolder = new File(getTemplateFolder(), baseName + "-web");
		return platformWebFolder;
	}

	private void scanWebFolder(File root, File source, File target, Map<File, File> files)
	{
		String name = source.getName();
		if (name.equals(ProjectStructure.SCRIPTS) || name.equals(ProjectStructure.STYLES) || name.endsWith(".js")
				|| name.endsWith(".css") || name.equals(ProjectStructure.TINY_MCE) || name.equals(".svn")
				|| name.equals("WEB-INF"))
			return;
		String path = source.getPath().substring(root.getPath().length());
		if (webExcludes.match(path))
			return;
		if (source.isFile())
		{
			if (!files.containsKey(target))
				files.put(target, source);
		}
		else if (source.isDirectory())
		{
			for (String child : source.list())
			{
				scanWebFolder(root, new File(source, child), new File(target, child), files);
			}
		}
	}

	protected void saveImageText(String text, File target, int width, int height, float fontSize)
	{
		Font font = new Font("Helvetica", Font.PLAIN, 1);
		font = font.deriveFont(fontSize);
		BufferedImage buffer = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics2D g2 = buffer.createGraphics();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		FontRenderContext fc = g2.getFontRenderContext();
		Rectangle2D bounds = font.getStringBounds(text, fc);

		// calculate the size of the text
		int w = (int) bounds.getWidth();
		int h = (int) bounds.getHeight();

		buffer = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		g2 = buffer.createGraphics();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setFont(font);

		// actually do the drawing
		g2.setColor(Color.BLACK);
		g2.fillRect(0, 0, width, height);
		g2.setColor(Color.WHITE);
		g2.drawString(text, (width - w) / 2, (int) -bounds.getY() + (height - h) / 2);

		try
		{
			FileOutputStream os = new FileOutputStream(target);

			ImageIO.write(buffer, "png", os);
			os.close();
		}
		catch (IOException e)
		{
			throw new ExportException("Failed to export " + target.getName(),e);
		}
	}

	private String getPreloadTempalteJS(String name)
	{
		String html = getTemplateHTML(name);
		String js = "tersus.preloadTemplate('" + name + "','" + EscapeUtil.escape(html, true)
				+ "');\n";
		return js;
	}

	private String getTemplateHTML(String name)
	{
		File file = null;
		for (File dir : getWebDirectories())
		{
			file = new File(dir, name);
			if (file.exists())
				break;
		}
		if (!file.exists())
			return null;
		Parser p = new Parser();
		try
		{
			p.setInputHTML(FileUtils.readString(new FileInputStream(file), "UTF-8", true));
			NodeList list = p.extractAllNodesThatMatch(new TagNameFilter("body"));
			BodyTag body;
			if (list.size() == 0)
				throw new ExportException("Missing <body> in HTML Template " + name);
			body = (BodyTag) list.elementAt(0);
			return body.getChildrenHTML();
		}
		catch (IOException e)
		{
			throw new ExportException("Filed to read HTML Template " + name, e);
		}
		catch (ParserException e)
		{
			throw new ExportException("Filed to parse HTML Template " + name, e);
		}

	}

	protected File getTemplateFolder()
	{
		return adapter.getTemplateFolder();
	}

	protected File getProjectFolder()
	{
		return adapter.getProjectFolder();
	}

	protected File getWebFolder()
	{
		return new File(getProjectFolder(), ProjectStructure.WEB);
	}

	protected File createFolder()
	{
		File exportFolder = new File(adapter.getExportLocation());
		exportFolder.mkdirs();
		return exportFolder;
	}

	public String getServerURL()
	{
		return serverURL;
	}

	public void setServerURL(String serverURL)
	{
		this.serverURL = serverURL;
	}

	protected int exportSplitScripts(File wwwFolder, ArrayList<File> scriptsFileList)
	{
		int scriptFileNumber = 1;
		StringBuffer out = new StringBuffer(500000);

		try
		{
			for (File file : scriptsFileList)
			{
				String fileHeader = "// file: " + file.getName() + "\n";
				String fileContent = FileUtils.readString(new FileInputStream(file),
						Engine.ENCODING, true);
				if (out.length() + fileContent.length() + fileHeader.length() > 500000)
				{
					File scriptsFile = new File(wwwFolder, "scripts" + scriptFileNumber + ".js");
					writeTextFile(scriptsFile, out.toString() );

					out = new StringBuffer(500000);
					scriptFileNumber++;
				}

				out.append(fileHeader);
				out.append(fileContent);
			}

			File scriptsFile = new File(wwwFolder, "scripts" + scriptFileNumber + ".js");
			writeTextFile(scriptsFile, out.toString());

			return scriptFileNumber;
		}
		catch (IOException e)
		{
			throw new ExportException("Failed to export scrupt file");
		}
	}

	public String getSplitScriptsFragment(int numberOfSplits)
	{
		StringBuffer scripts = new StringBuffer();
		for (int i = 1; i <= numberOfSplits; i++)
		{
			scripts.append("<script language=\"Javascript\" src=\"scripts" + i + ".js\"></script>");
			scripts.append("\n");
		}
		String scriptFragment = scripts.toString();
		return scriptFragment;
	}

	protected String getFrameworkName()
	{
		String framework = (String) adapter.getRootModel().getProperty(
				BuiltinProperties.APPLICATION_FRAMEWORK);
		return framework;
	}
	
	protected void lineSeparator(String value)
	{
		this.lineSeparator=value;
	}
	protected void addBOMToTextFiles(boolean value)
	{
		this.addBOMToTextFiles=value;
	}

}

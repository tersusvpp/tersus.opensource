/************************************************************************************************
 * Copyright (c) 2003-2021 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
package tersus.webapp.appexport;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tersus.model.Model;
import tersus.util.NotImplementedException;
import tersus.util.ZipUtil;
import tersus.webapp.ContextPool;

public class IPhoneExporter
{

	private ContextPool pool;
	private File exportFolder;


	public IPhoneExporter(ContextPool pool)
	{
		this.pool = pool;
		File parentFolder = new File(pool.getProjectRoot(), "export");
		if (!parentFolder.exists())
			parentFolder.mkdir();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		exportFolder =  new File(parentFolder, format.format(new java.util.Date()));
	}

	public void export(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			Adapter adapter = new Adapter();
			MobileAppCreator creator = new IPhoneAppCreator(adapter);
			StringBuffer requestURL = request.getRequestURL();
			int index = requestURL.indexOf("/_Export");
			String appURL = requestURL.substring(0,index);
			creator.setServerURL(appURL);
			creator.export();
			
			byte[] zipBytes = ZipUtil.prepareZipFile(exportFolder);
			response.setContentType("application/zip");
			response.setHeader("Content-Disposition","attachment; filename=\""
					+ adapter.getApplicationIdentifier() + ".zip\"");
			response.setContentLength(zipBytes.length);
			ServletOutputStream os = response.getOutputStream();
			os.write(zipBytes);
			os.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	
	private class Adapter implements IExportAdapter
	{

		public Model getRootModel()
		{
			return pool.getRootModel();
		}

		public String getKeyStorePath()
		{
			throw new NotImplementedException();
		}

		public String getKeyAlias()
		{
			throw new NotImplementedException();
		}

		public String getAppName()
		{
			return getRootModel().getName();
		}

		public String getTargetPlatform()
		{
			return "iPhone";
		}

		public File getProjectFolder()
		{
			return pool.getProjectRoot();
		}

		public String getExportLocation()
		{
			return exportFolder.getAbsolutePath();
		}

		public String getApplicationIdentifier()
		{
			return getClassName();
		}

		public String getDeviceName()
		{
			throw new NotImplementedException();
		}

		public IProcessDialog getProcessDialog()
		{
			throw new NotImplementedException();
		}

		public String getPackageName()
		{
			throw new NotImplementedException();
		}

		public String getClassName()
		{
			return ExportUtils.convertName(getAppName());
		}

		public void writeExportLog(String message)
		{
			throw new NotImplementedException();
		}

		public void reportError(int rc, String message, String output, String error)
		{
			throw new NotImplementedException();
		}

		public void setDeviceName(String string)
		{
			throw new NotImplementedException();
		}

		public String[] getAntCommand(String target)
		{
			throw new NotImplementedException();
		}

		public void error(String title, String details)
		{
			throw new NotImplementedException();
		}

		public void message(String title, String details)
		{
			throw new NotImplementedException();
		}

		public IAndroidCommandHelper getAndroidCommandHelper()
		{
			throw new NotImplementedException();
		}

		public File getTemplateFolder()
		{
			String s =  pool.getServletContext().getInitParameter("app-export-template-folder");
			if (s != null)
				return new File(s);
			else
			{
				File serverBase = new File(pool.getServletContext().getRealPath("version.txt")).getParentFile();
				return new File(serverBase, "WEB-INF/app-export-templates");
			}
		}

		public File getServerWebDirectory()
		{
			return pool.getRootDir();
		}

		public void saveKeyStorePath(String keyStorePath)
		{
			throw new NotImplementedException();
			
		}

		public void savePackagePrefix(String prefix)
		{
			throw new NotImplementedException();
		}
		
	}

}

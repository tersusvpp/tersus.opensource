package tersus.webapp.appexport;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import tersus.util.FileUtils;
import tersus.util.Misc;

public class IPhoneAppCreator extends MobileAppCreator 
{
	
	public IPhoneAppCreator(IExportAdapter adapter) 
	{
		super(adapter);
	}

	public void export() 
	{
		File exportFolder = createFolder();
		exportIPhoneProject(exportFolder);
		File wwwFolder = new File(exportFolder, "www");
		exportMobileApplication(wwwFolder, exportFolder);
		updateBundleIndentifier(exportFolder, adapter.getApplicationIdentifier());
	}

	private void updateBundleIndentifier(File exportFolder, String name)
	{
		File xcodeFolder = new File(exportFolder, name);
		File propertyFile = new File(xcodeFolder, name+"-Info.plist");
		try
		{
			String bundleIdentifier = adapter.getApplicationIdentifier();
			String content = FileUtils.readString(new FileInputStream(propertyFile), null, true);
			content = Misc.replaceAll(content, "BUNDLE_IDENTIFIER", bundleIdentifier);
			FileUtils.write(content, new FileOutputStream(propertyFile), true);
		}
		catch (Exception e)
		{
			throw new ExportException("Failed to update bundle identifier in "
					+ propertyFile.getPath(), e);
		}
	}
	
	private void exportIPhoneProject(File exportFolder)
	{
		try
		{
			File templateFolder = getTemplateFolder();
			String baseName = "iphone";
			String framework = getFrameworkName();
			if (framework != null)
				baseName = baseName+"."+framework;
			
			String templateName = baseName;
			File projectTemplate = new File(templateFolder, templateName);
			FileUtils.delete(new File(exportFolder, "build"));
			copyAndRename(projectTemplate, exportFolder, adapter.getApplicationIdentifier());
			// renamePhonegapFiles(exportFolder, name);

		}
		catch (Exception e)
		{
			throw new ExportException("Failed to create target project from template", e);
		}
	}

	private void copyAndRename(File source, File target, String name) throws IOException
	{
		String fileName = source.getName();
		if (".svn".equals(fileName) || "build".equals(fileName) || fileName.endsWith(".mode1v3")
				|| fileName.endsWith(".pbxuser") || fileName.equals("xcuserdata"))
			return;
		if (source.isFile())
		{
			if (target.isDirectory())
				FileUtils.delete(target);
			if (target.exists() && target.lastModified() >= source.lastModified())
				return;
			if (isTextFile(source) && !source.getPath().contains("/Plugins/org.apache")) /* Hack to prevent messing up some files that don't need editing*/
			{
				String content = FileUtils.readString(new FileInputStream(source), "UTF-8", true);
				content = Misc.replaceAll(content, "XXX", name);
				FileUtils.write(content, "UTF-8", new FileOutputStream(target), true);
			}
			else
			{
				FileInputStream is = new FileInputStream(source);
				FileOutputStream os = new FileOutputStream(target);
				FileUtils.copy(is, os, true);
			}
			target.setLastModified(source.lastModified());
	        target.setExecutable(source.canExecute() || source.getName().endsWith(".sh"));			
		}
		else if (source.isDirectory())
		{
			if (target.isFile())
				target.delete();
			if (!target.exists())
				target.mkdir();
			for (String child : source.list())
			{
				String newChild = child.replaceAll("XXX", name);
				copyAndRename(new File(source, child), new File(target, newChild), name);
			}
		}
	}

	protected void exportImages(String appName, File exportFolder)
	{
		File images = new File(getProjectFolder(),"images");
		File icon57 = new File(images,"icon57.png");
		File iconTarget = new File(exportFolder, appName+"/Resources/icons/icon.png");
		if (icon57.exists())
		{
			try
			{
				FileUtils.copy(icon57, iconTarget, null, false);
			}
			catch (IOException e)
			{
				throw new ExportException("Failed to export " + icon57.getName(), e);
			}
		}
		else
		{
			saveImageText(appName, iconTarget, 57, 57, 12);
		}
		File icon72 = new File(images,"icon72.png");
		File icon72Target = new File(exportFolder, appName+"/Resources/icons/icon-72.png");
		if (icon72.exists())
		{
			try
			{
				FileUtils.copy(icon72, icon72Target, null, false);
			}
			catch (IOException e)
			{
				throw new ExportException("Failed to export " + icon72.getName(), e);
			}
		}
		else
		{
			saveImageText(appName, icon72Target, 72, 72, 12);
		}
		
		File icon114 = new File(images,"icon114.png");
		File icon114Target = new File(exportFolder, appName+"/Resources/icons/icon@2x.png");
		if (icon114.exists())
		{
			try
			{
				FileUtils.copy(icon114, icon114Target, null, false);
			}
			catch (IOException e)
			{
				throw new ExportException("Failed to export " + icon114.getName(), e);
			}
		}
		else
		{
			saveImageText(appName, icon114Target, 114, 114, 24);
		}
		
		saveSplashImage(appName, exportFolder, images, 640, 1136, "Default-568h@2x~iphone.png");
		saveSplashImage(appName, exportFolder, images, 320, 480, "Default~iphone.png");
		saveSplashImage(appName, exportFolder, images, 640, 960, "Default@2x~iphone.png");
		saveSplashImage(appName, exportFolder, images, 1024, 768, "Default-Landscape~ipad.png");
		saveSplashImage(appName, exportFolder, images, 2048, 1536, "Default-Landscape@2x~ipad.png");
		saveSplashImage(appName, exportFolder, images, 768, 1024, "Default-Portrait~ipad.png");
		saveSplashImage(appName, exportFolder, images,  1536, 2048, "Default-Portrait@2x~ipad.png");
	
	}

	protected void saveSplashImage(String appName, File exportFolder, File images, int w, int h,
			String path)
	{
		File splashLandscapeIpad2 = new File(images,"splash"+w+"x"+h+".png");
		File splashLandscapeIpadTarget2 = new File(exportFolder, appName+"/Resources/splash/"+path);
		if (splashLandscapeIpad2.exists())
		{
			try
			{
				FileUtils.copy(splashLandscapeIpad2,  splashLandscapeIpadTarget2, null, false);
			}
			catch (IOException e)
			{
				throw new ExportException("Failed to export " + splashLandscapeIpad2.getName(), e);
			}
		}
		else if (!splashLandscapeIpadTarget2.exists())
		{
			saveImageText(appName, splashLandscapeIpadTarget2, w, h, 80);
		}
	}
	public static boolean isTextFile(File source)
	{
		String[] suffixes =
		{ ".h", ".m", ".plist", ".xib", ".pch", ".pbxproj",".xcworkspacedata" };
		String name = source.getName();
		for (String suffix : suffixes)
			if (name.endsWith(suffix))
				return true;
		return false;
	}
	protected String getUserAgentStr()
	{
		return "iphone webkit mobile safari";
	}
}

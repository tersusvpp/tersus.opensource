/************************************************************************************************
 * Copyright (c) 2003-2021 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
package tersus.webapp.appexport;

public class ExportException extends RuntimeException {

	public ExportException() {
		super("Failed to create android project");
	}

	public ExportException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ExportException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ExportException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}

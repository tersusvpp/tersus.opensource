/************************************************************************************************
 * Copyright (c) 2003-2021 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
package tersus.webapp.appexport;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.ArrayUtils;

import tersus.runtime.DateAndTimeHandler;
import tersus.util.Misc;
import tersus.util.ProcessRunner;

public class AndroidSDKHelper
{
	public static final String COMMAND_ANDROID = "android";
	
	public static final String COMMAND_ADB = "adb";
	
	private static final long TIMEOUT = 120000;
	
	public static final String LAUNCH_NEW_AVD = "Create and launch default AVD, install and run application";
	
	public static final String LAUNCH_AVD = "Launch AVD, install and run application in AVD:";
	
	public static final String INSTALL_IN_DEVICE = "Install and run application on existing AVD:";
	
	public static final String CREATE_DEBUG_PACKAGE = "Create debug package";

	public static final String CREATE_RELEASE_PACKAGE = "Create release package";

	public static final String NONE = "None (export only)";
	
	public static String runAndroidSDKCommand(IAndroidCommandHelper helper, String commandName, String[] commandParameters, long timeout, String errorMessage)
	//			throws IOException, InterruptedException
		{
			return runAndroidSDKCommand(helper, commandName, commandParameters, timeout, errorMessage, null);
		}

	public static String runAndroidSDKCommand(IAndroidCommandHelper helper, String commandName, String[] commandParameters, long timeout, String errorMessage, IExportAdapter adapter)
	//			throws IOException, InterruptedException
		{
			return runAndroidSDKCommand(helper, commandName, commandParameters, timeout, errorMessage, null, adapter);
		}

	public static String runAndroidSDKCommand(IAndroidCommandHelper helper, String commandName, String[] commandParameters, long timeout, String errorMessage, String processInput, IExportAdapter adapter)
//			throws IOException, InterruptedException
	{
		String[] commandArray = ArrayUtils.addAll(new String[]{helper.getAndroidCommand(commandName)}, commandParameters);
		return runCommand(commandArray, timeout, errorMessage, processInput, adapter);
	}

	public static String runCommand(String[] commandArray, long timeout, String errorMessage,
			String processInput, IExportAdapter adapter)
//			throws IOException, InterruptedException
	{
		ProcessRunner runner = new ProcessRunner();
		if (processInput != null)
			runner.setProcessInput(processInput);
		if (adapter != null)
			runner.setWorkingDirectory(new File(adapter.getExportLocation()));
		Date beginTimestamp = Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
		int rc;
		try {
			rc = runner.run(commandArray, timeout);
		}
		catch (Exception e)
		{
			throw new ExportException("Failed to export android project", e);
		}

		String commandOutput = new String(runner.getOutputStream().toByteArray());
		String logMessage = String.join("\n",
				"", "begin: " + dateFormat.format(beginTimestamp),
				"", "cmd: " + String.join(" ", commandArray), "rc: " + rc,
				"", "Output: " + commandOutput);
		String commandError = new String(runner.getErrorStream().toByteArray());
		if ((commandError != null) && !commandError.isEmpty()) {
			logMessage = logMessage + String.join("\n",
					"", "error: " + commandError, "");
		}
		logMessage = logMessage + String.join("\n",
				"", "end: " + dateFormat.format(Calendar.getInstance().getTime()));

		System.out.println(logMessage + "\n--------------------");
		
		if (adapter != null)
			adapter.writeExportLog(logMessage);
		if (rc != 0)
		{
			if (adapter != null) {
//				adapter.message(errorMessage, commandError);
				adapter.reportError(rc, errorMessage, commandOutput, commandError);
			}
			throw new ExportException(errorMessage);
		}
		else
			return commandOutput;
	}
		
	public static void addAndroidDevices(IAndroidCommandHelper helper, ArrayList<String> list, String commandErrorSpecific) throws IOException, InterruptedException
	{

		String[] commandArray = new String[]
		{ "devices" };
		String commandError = "Failed to obtain list of Android devices\n" + commandErrorSpecific;

		try
		{
			String commandOutput = runAndroidSDKCommand(helper, COMMAND_ADB, commandArray, TIMEOUT, commandError);

			String[] lines = commandOutput.split("\r?\n");
			for (String l : lines)
			{
				if (l.matches("(\\S+)\\s+device"))
					list.add(INSTALL_IN_DEVICE + " " + l.split("\\s")[0]);
			}
		}
		catch (Exception e)
		{
			throw new ExportException(commandError);
		}
	}
	
	public static String[] getAvds(IAndroidCommandHelper helper)
	{
		String[] commandArray = new String[]
		{ "list", "avds" };
		String commandError = "Failed to obtain list of AVDs";

		try
		{
			String commandOutput = runAndroidSDKCommand(helper, COMMAND_ANDROID, commandArray, 30000, commandError);
			
			ArrayList<String> list = new ArrayList<String>();
			String[] lines = commandOutput.split("[\r\n]");
			for (String l : lines)
			{
				if (l.matches("\\s*Name:\\s*\\S+"))
					list.add(Misc.lastToken(l, "\\s|:"));
			}
			return list.toArray(new String[list.size()]);
		}
		catch (Exception e)
		{
			throw new ExportException(commandError);
		}
	}

	public static String[] getDeviceSelectionList(IAndroidCommandHelper helper)
	{
		try
		{
			ArrayList<String> list = new ArrayList<String>();
			addAndroidDevices(helper, list, "");
			if (list.size() == 0)
			{
				String[] avds = getAvds(helper);
				if (avds.length > 0)
					for (String avd : avds)
					{
						list.add(LAUNCH_AVD + " " + avd);
					}
// Create default AVD when none is available has been disabled since there are too many exceptions to handle
// e.g.
// 		1. there are more then one ABIs (image) installed for a given SDK version
//		2. Intel based images are not supported in certain configurations
// therefore, the user must create a working AVD by himself 
//
//
//				else
//					list.add(LAUNCH_NEW_AVD);
			}
			list.add(CREATE_DEBUG_PACKAGE);
			list.add(CREATE_RELEASE_PACKAGE);
			list.add(NONE);
			return list.toArray(new String[list.size()]);
		}
		catch (Exception e)
		{
			throw new ExportException("Failed to obtain list of Android devices", e);
		}

	}
}

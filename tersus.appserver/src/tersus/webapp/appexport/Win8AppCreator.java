package tersus.webapp.appexport;

import static org.joox.JOOX.$;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.joox.Match;
import org.w3c.dom.Document;

import tersus.util.FileUtils;

public class Win8AppCreator extends MobileAppCreator 
{
	
	private File exportFolder;
	private File projectFile;
	private static final String XMLDECL="<?xml version=\"1.0\" encoding=\"utf-8\"?>";
	public Win8AppCreator(IExportAdapter adapter) 
	{
		super(adapter);
		addBOMToTextFiles(true);
		lineSeparator("\r\n");
		addWebExclude("[.]html$");
		addWebExclude("logo[.]gif");
		addWebExclude("(base-)?offline-files");
		addWebExclude("loading[.]txt");
		addWebExclude("favicon[.]ico");
		addWebExclude("themes");
		addWebExclude("images/bodyBGI[.]jpg");
		addWebExclude("images/bodyBGI_rtl[.]jpg");
		addWebExclude("images/browse.gif");
		addWebExclude("images/button1");
		addWebExclude("images/button2");
		addWebExclude("images/buttons");
		addWebExclude("images/calendar");
		addWebExclude("images/dataTable");
		addWebExclude("images/dialog");
		addWebExclude("images/dialog_mask.gif");
		addWebExclude("images/dialog_mask.png");
		addWebExclude("images/error.gif");
		addWebExclude("images/folder.gif");
		addWebExclude("images/frames");
		addWebExclude("images/header");
		addWebExclude("images/i[.]gif");
		addWebExclude("images/innerNav");
		addWebExclude("images/iphone/");
		addWebExclude("images/iphone-horizontal[.]png");
		addWebExclude("images/iphone-vertical[.]png");
		addWebExclude("images/iphone_selected_row_bg[.]gif");
		addWebExclude("images/item[.]gif");
		addWebExclude("images/leftNav");
		addWebExclude("images/login/");
		addWebExclude("images/menu_button[.]gif");
		addWebExclude("images/ok[.]gif");
		addWebExclude("images/progress[.]gif");
		addWebExclude("images/running[.]gif");
		addWebExclude("images/sort/");
		addWebExclude("images/spacer[.]gif");
		addWebExclude("images/start[.]gif");
		addWebExclude("images/tabbedPane/");
		addWebExclude("images/trace/");
		addWebExclude("images/tree/");
		addWebExclude("/[.]DS_Store");
		addWebExclude(".*~");
	}

	public void export() 
	{
		checkFolder();
		exportMobileApplication(exportFolder, exportFolder);
		File welcomeFile=new File(exportFolder,"index.html");
		welcomeFile.renameTo(new File(exportFolder,"default.html"));
		updateProjectFile();
	}

	public void checkFolder()
	{
		this.exportFolder = new File(adapter.getExportLocation());
		if (!exportFolder.exists())
			throw new ExportException(exportFolder.getAbsolutePath()+ " does not exist");
		if (!exportFolder.isDirectory())
			throw new ExportException(exportFolder.getAbsolutePath()+ " is not a directory");

		File packageManifest = new File(exportFolder, "package.appxmanifest");
		if (!packageManifest.isFile())
			throw new ExportException("Missing file "+packageManifest.getName()+".  Export folder should be a Windows 8 Javascript project folder (created with Visual Studio)");
		findProjectFile();	
	}
		

	protected void findProjectFile()
	{
		File[] projFiles = exportFolder.listFiles(new FilenameFilter()
		{
			public boolean accept(File dir, String name)
			{
				return name.endsWith(".jsproj");
			}
		});
		if (projFiles == null || projFiles.length == 0)
			throw new ExportException("Project file \"*.jsproj\" not found in "+exportFolder.getPath());
		if (projFiles.length>1)
			throw new ExportException("Found mutliple project files \"*.jsproj\"  in "+exportFolder.getPath());
		this.projectFile  = projFiles[0];
			
	}
	
	protected void updateProjectFile()
	{
		try
		{
			Document document = $(projectFile).document();
			$(document).find("Content").remove();
			Match itemGroup = $(document).find("ItemGroup").first();
			addPath(itemGroup, "default.html");
			addPath(itemGroup, "scripts.js");
			addPath(itemGroup, "models.js");
			addPath(itemGroup, "templates.js");
			addPath(itemGroup, "styles.css");
			addPath(itemGroup, "images\\logo.png");
			addPath(itemGroup, "images\\smalllogo.png");
			addPath(itemGroup, "images\\splashscreen.png");
			addPath(itemGroup, "images\\storelogo.png");
			for (String path: getWebFilePaths())
				addPath(itemGroup, path);

			String xml = XMLDECL+"\n"+$(document).content();
				
			FileUtils.write(xml.getBytes("UTF-8"),projectFile);
		}
		catch(Exception e)
		{
			throw new ExportException("Error updating project file "+projectFile.getName(), e);
		}
	}

	protected List<String> getWebFilePaths()
	{
		ArrayList<String> list = new ArrayList<String>(webFiles.size());
		for (File f: webFiles)
			list.add(f.getPath().substring(exportFolder.getPath().length()+1));
		Collections.sort(list);
		return list;
	}
	protected void addPath(Match itemGroup, String path)
	{
		itemGroup.append($("Content").attr("Include", path.replaceAll("[/]","\\\\")));
	}
	
	protected void exportImages(String appName, File exportFolder)
	{

	
	}
	public static boolean isTextFile(File source)
	{
		String[] suffixes =
		{ ".h", ".m", ".plist", ".xib", ".pch", ".pbxproj",".xcworkspacedata" };
		String name = source.getName();
		for (String suffix : suffixes)
			if (name.endsWith(suffix))
				return true;
		return false;
	}
	protected String getUserAgentStr()
	{
		return "MSIE mobile";
	}
}

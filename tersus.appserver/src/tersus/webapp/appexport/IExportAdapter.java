/************************************************************************************************
 * Copyright (c) 2003-2021 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
package tersus.webapp.appexport;

import java.io.File;

import tersus.model.Model;

public interface IExportAdapter
{

	Model getRootModel();

	String getKeyStorePath();
	String getKeyAlias();

	String getTargetPlatform();

	File getProjectFolder();

	String getExportLocation();

	String getApplicationIdentifier();

	String getDeviceName();

	IProcessDialog getProcessDialog();

	String getPackageName();

	String getClassName();
	public void writeExportLog(String message);
	void reportError(int rc, String message, String output, String error);

	void setDeviceName(String string);
	
	String[] getAntCommand(String target);
	
	void error(String title, String details);
	void message(String title, String details);

	IAndroidCommandHelper getAndroidCommandHelper();

	File getTemplateFolder();

	File getServerWebDirectory();

	void saveKeyStorePath(String keyStorePath);

	void savePackagePrefix(String prefix);

}

package tersus.webapp.appexport;

import java.io.File;

import tersus.model.Model;

public class CordovaAppCreator extends MobileAppCreator 
{
	
	private File exportFolder;
	private String userAgentStr = "mobile";

	public CordovaAppCreator(IExportAdapter adapter) 
	{
		super(adapter);
		addBOMToTextFiles(true);
		addWebExclude("[.]html$");
		addWebExclude("logo[.]gif");
		addWebExclude("(base-)?offline-files");
		addWebExclude("loading[.]txt");
		addWebExclude("favicon[.]ico");
		addWebExclude("themes");
		addWebExclude("images/bodyBGI[.]jpg");
		addWebExclude("images/bodyBGI_rtl[.]jpg");
		addWebExclude("images/browse.gif");
		addWebExclude("images/button1");
		addWebExclude("images/button2");
		addWebExclude("images/buttons");
		addWebExclude("images/calendar");
		addWebExclude("images/dataTable");
		addWebExclude("images/dialog");
		addWebExclude("images/dialog_mask.gif");
		addWebExclude("images/dialog_mask.png");
		addWebExclude("images/error.gif");
		addWebExclude("images/folder.gif");
		addWebExclude("images/frames");
		addWebExclude("images/header");
		addWebExclude("images/i[.]gif");
		addWebExclude("images/innerNav");
		addWebExclude("images/iphone/");
		addWebExclude("images/iphone-horizontal[.]png");
		addWebExclude("images/iphone-vertical[.]png");
		addWebExclude("images/iphone_selected_row_bg[.]gif");
		addWebExclude("images/item[.]gif");
		addWebExclude("images/leftNav");
		addWebExclude("images/login/");
		addWebExclude("images/menu_button[.]gif");
		addWebExclude("images/ok[.]gif");
		addWebExclude("images/progress[.]gif");
		addWebExclude("images/running[.]gif");
		addWebExclude("images/sort/");
		addWebExclude("images/spacer[.]gif");
		addWebExclude("images/start[.]gif");
		addWebExclude("images/tabbedPane/");
		addWebExclude("images/trace/");
		addWebExclude("images/tree/");
		addWebExclude("/[.]DS_Store");
		addWebExclude(".*~");
	}

	public void export() 
	{
		checkFolder();
		exportMobileApplication(new File(exportFolder,"www"), exportFolder);
		exportMerges();
	}

	private void exportMerges()
	{
		userAgentStr = "mobile native";
		File mergesFolder = new File(exportFolder, "merges");
		File platformsFolder = new File(exportFolder, "platforms");
		if (!mergesFolder.exists())
			mergesFolder.mkdir();
		lineSeparator(null);
		for (String p:platformsFolder.list())
		{
			if ("ios".equals(p))
				userAgentStr="iphone webkit mobile safari native";
			else if ("android".equals(p))
				userAgentStr="iphone webkit android native";
			else if ("windows".equals(p))
			{
				lineSeparator("\r\n");
				userAgentStr="MSIE mobile native";
			}
			else
				continue;
			Model rootModel = adapter.getRootModel();
			File platformMergeFolder = new File(mergesFolder, p);
			if (! platformMergeFolder.exists())
				platformMergeFolder.mkdir();
			if (p.equals("windows"))
				exportWelcomePage(rootModel, platformMergeFolder);
			exportScripts(platformMergeFolder);
			exportStyles(rootModel, platformMergeFolder);
		}
		
	}

	public void checkFolder()
	{
		this.exportFolder = new File(adapter.getExportLocation());
		if (!exportFolder.exists())
			throw new ExportException(exportFolder.getAbsolutePath()+ " does not exist");
		if (!exportFolder.isDirectory())
			throw new ExportException(exportFolder.getAbsolutePath()+ " is not a directory");

		File configFile = new File(exportFolder, "config.xml");
		if (!configFile.isFile())
			throw new ExportException("Missing file "+configFile.getName()+".  Export folder should be the top level of a Cordova project");
	}
		
	protected void exportImages(String appName, File exportFolder)
	{
	}
	public static boolean isTextFile(File source)
	{
		String[] suffixes =
		{ ".h", ".m", ".plist", ".xib", ".pch", ".pbxproj",".xcworkspacedata" };
		String name = source.getName();
		for (String suffix : suffixes)
			if (name.endsWith(suffix))
				return true;
		return false;
	}
	protected String getUserAgentStr()
	{
		return userAgentStr;
	}

	protected File getWelcomePageTemplate()
	{
		return new File(getPlatformWebFolder(),"index.html");
	}
}

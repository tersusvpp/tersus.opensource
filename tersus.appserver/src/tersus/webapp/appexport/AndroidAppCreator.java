/************************************************************************************************
 * Copyright (c) 2003-2021 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
package tersus.webapp.appexport;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

import tersus.ProjectStructure;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Repository;
import tersus.model.indexer.RepositoryIndex;
import tersus.util.FileUtils;
import tersus.util.Misc;
import tersus.webapp.ScriptAggregator;
import tersus.webapp.UserAgent;

public class AndroidAppCreator extends MobileAppCreator
{
	private static final String COMMAND_EMULATOR = "emulator";

	private static final String ADB_COMMAND_SHELL = "shell";
	
	private static final long TIMEOUT = 120000;

	private static final int NOT_READY = 7777;

	private static final Object SUCCESS = "Success";

	private int numberOfScriptFiles;

	public AndroidAppCreator(IExportAdapter adapter)
	{
		super(adapter);
	}

	public void export()
	{
		File exportFolder = createFolder();
		exportAndroidProject(exportFolder);
		updateManifest(exportFolder);
		File wwwFolder = new File(exportFolder, "assets/www");
		exportMobileApplication(wwwFolder, exportFolder);

		if (AndroidSDKHelper.NONE.equals(adapter.getDeviceName()))
			return; // No build or install
		if (AndroidSDKHelper.CREATE_RELEASE_PACKAGE.equals(adapter.getDeviceName()))
			androidAntRelease();
		else if (adapter.getDeviceName() != null)
			androidBuildAndInstall();
	}

	private void updateManifest(File exportFolder)
	{

		AndroidOptions options = getOptions();
		File manifest = getManifestFile(exportFolder);
		StringBuffer content;
		int pos;
		try
		{
			content = FileUtils.readString(new InputStreamReader(new FileInputStream(manifest),
					"UTF-8"), true);
		}
		catch (IOException e)
		{
			throw new ExportException("Failed to read " + manifest.getPath(), e);
		}
		if (content.indexOf("android:icon") < 0)
		{
			String s = "<application ";
			pos = content.indexOf(s) + s.length();
			content.insert(pos, "android:icon=\"@drawable/icon\" ");
		}
		pos = content.indexOf("<uses-permission");
		if (pos == -1)
			pos = content.indexOf("</manifest>");
		content.setLength(pos);
		for (String p : options.permissions)
		{
			content.append("<uses-permission android:name=\"");
			content.append(p);
			content.append("\"/>\n");
		}
		for (String f : options.features)
		{
			content.append("<uses-feature android:name=\"");
			content.append(f);
			content.append("\"/>\n");
		}
        content.append("<uses-sdk android:minSdkVersion=\"10\" />\n");
		content.append("</manifest>");

		
		String s="<activity ";
		pos = content.indexOf(s);
		if (pos >=0)
		{
			pos+=s.length();
			
			if (content.indexOf("android:configChanges", pos) < 0)
			{
				int next = content.indexOf("android:name", pos);
				content.replace( pos, next, "android:configChanges=\"screenLayout|keyboard|uiMode|orientation|keyboardHidden\" android:screenOrientation=\"portrait\" ");
			}
		}
		


		try
		{
			FileUtils.write(content.toString().getBytes("UTF-8"), manifest);
		}
		catch (IOException e)
		{
			throw new ExportException("Failed to write " + manifest.getPath(), e);
		}
	}

	protected File getManifestFile(File exportFolder)
	{
		File manifest = new File(exportFolder, "AndroidManifest.xml");
		return manifest;
	}

	public AndroidOptions getOptions()
	{
		AndroidOptions options = new AndroidOptions();
		Model rootModel = adapter.getRootModel();
		RepositoryIndex index = ((Repository) rootModel.getRepository())
				.getUpdatedRepositoryIndex();
		HashSet<String> plugins = new HashSet<String>();
		for (ModelId id : index.calculateUIModelIds(rootModel, null))
		{
			plugins.add(index.getModelEntry(id).getDescriptor().getName());
		}
		Collection<String> permissions = options.permissions;
		Collection<String> features = options.features;
		if (properties!=null)
		{
			String permissionsStr = properties.getProperty("android.permissions");
			if (permissionsStr != null)
				for (String s: permissionsStr.split(","))
				{
					String s1=s.trim();
					if (s1.length()>0)
						permissions.add(s1);
				}
			String featuresStr = properties.getProperty("android.features");
			if (featuresStr != null)
				for (String s: featuresStr.split(","))
				{
					String s1=s.trim();
					if (s1.length()>0)
						features.add(s1);
				}
		}
		permissions.add("android.permission.INTERNET");
		permissions.add("android.permission.ACCESS_NETWORK_STATE");
		if (plugins.contains("Tersus/Display Actions/Get Geolocation"))
		{
			permissions.add("android.permission.ACCESS_COARSE_LOCATION");
			permissions.add("android.permission.ACCESS_FINE_LOCATION");
			permissions.add("android.permission.ACCESS_LOCATION_EXTRA_COMMANDS");
		}
		if (plugins.contains("Tersus/Widgets/Take Picture")
				|| plugins.contains("Tersus/Mobile/Take Picture"))
		{
			permissions.add("android.permission.CAMERA");
			//Not sure both VIBRATE and WRITE_EXTERNAL_STORAGE, but without them , "Take Picture" doesn't seem to work on 2.3.3
			permissions.add("android.permission.WRITE_EXTERNAL_STORAGE");
			permissions.add("android.permission.VIBRATE");
			features.add("android.hardware.camera");
			features.add("android.hardware.camera.autofocus");
		}
		return options;
	}

	private void androidAntRelease()
	{
		String keyStorePath = adapter.getKeyStorePath();
		String keyAlias = adapter.getKeyAlias();
		
		boolean savePathInPrefrences = false;
		File keyStoreFile;
		if (keyStorePath == null || keyStorePath.length() == 0)
		{
			File homeDir = new File(System.getProperty("user.home"));
			keyStoreFile = new File(homeDir, ".tersus/android.keystore");
			try
			{
				keyStorePath = keyStoreFile.getCanonicalPath();
			}
			catch (IOException e)
			{
				keyStorePath = keyStoreFile.getPath();
			}
			savePathInPrefrences = true;
		}
		else
			keyStoreFile = new File(keyStorePath);
		if (!keyStoreFile.exists())
		{
			ProcessBuilder builder = new ProcessBuilder("keytool", "-genkey", "-validity", "10000",
					"-keystore", keyStorePath, "-alias", keyAlias, "-keyalg", "RSA");
			builder.directory(new File(System.getProperty("user.home")));
			IProcessDialog dialog = adapter.getProcessDialog();
			try
			{
				dialog.setProcess(builder.start());
			}
			catch (IOException e)
			{
				e.printStackTrace();
				throw new ExportException("Failed to lauch keytool", e);
			}
			dialog.setCloseOnSuccess(true);
			dialog.setTitle("Creating application signing key");
			dialog.open();
			if (dialog.getReturnCode() != 0)
				return;

		}
		if (savePathInPrefrences) 
			adapter.saveKeyStorePath(keyStorePath);

		try
		{
			FileUtils.write("key.store=" + keyStorePath.replaceAll("[\\\\]", "\\\\\\\\")
					+ "\nkey.alias=" + keyAlias + "\n",
					new FileOutputStream(new File(adapter.getExportLocation(), "ant.properties")),
					true);
		}
		catch (IOException e)
		{
			throw new ExportException("Failed to write build.prorperties", e);
		}

		String antCommand = "release";
		IProcessDialog dialog = adapter.getProcessDialog();
		ProcessBuilder builder = new ProcessBuilder(adapter.getAntCommand(antCommand));
		builder.directory(new File(adapter.getExportLocation()));
		try
		{
			dialog.setProcess(builder.start());
		}
		catch (IOException e)
		{
			throw new ExportException("Error launching \"ant " + antCommand + "\"", e);
		}
		dialog.setCloseOnSuccess(false);
		dialog.setTitle("Building release package");

		dialog.open();
		System.out.println("Done");
		return;

	}

	private void exportAndroidProject(File exportFolder)
	{
		try
		{
			String packageName = adapter.getPackageName();
			if (packageName.toLowerCase().endsWith("." + adapter.getClassName().toLowerCase()))
			{
				String parentPackage = packageName.substring(0, packageName.lastIndexOf('.'));
				adapter.savePackagePrefix(parentPackage);
			}

			if ( ! getManifestFile(exportFolder).exists() )
				createAndroidProject(exportFolder);
			else
				updateAndroidProject(exportFolder);
				
			File templateFolder = getTemplateFolder();
			copyFolder(templateFolder, "android/libs", exportFolder, "libs");
			copyFolder(templateFolder, "android/res", exportFolder, "res");
			File templateClassFile = new File(templateFolder, "android/java/Activity.java");
			String classContent = FileUtils.readString(new FileReader(templateClassFile), true)
					.toString();
			classContent = classContent.replaceAll("PackageName", packageName).replaceAll(
					"ClassName", adapter.getClassName());

			File targetClassFile = new File(exportFolder, "src/"
					+ packageName.replaceAll("\\.", "/") + "/" + adapter.getClassName() + ".java");
			targetClassFile.getParentFile().mkdirs();
			FileUtils.write(classContent, new FileOutputStream(targetClassFile), true);

			// URL url = Activator.getDefault().getBundle().getResource("template/index.html");
			// File templateFolder = new File(FileLocator.resolve(url).getFile()).getParentFile();
			// File projectTemplate = new File(templateFolder, "iphone");
			// removeBuildFolder(exportFolder);
			// copyAndRename(projectTemplate, exportFolder, name);
			// renamePhonegapFiles(exportFolder, name);

		}
		catch (Exception e)
		{
			throw new ExportException("Failed to create target project from template", e);
		}
	}

	private String runAndroidSDKCommand(String commandName, String[] commandParameters, long timeout, String errorMessage)
	//			throws IOException, InterruptedException
		{
			return AndroidSDKHelper.runAndroidSDKCommand(adapter.getAndroidCommandHelper(), commandName, commandParameters, timeout, errorMessage, adapter);
		}

	private String runAndroidSDKCommand(String commandName, String[] commandParameters, long timeout, String errorMessage, String processInput)
	//			throws IOException, InterruptedException
		{
			return AndroidSDKHelper.runAndroidSDKCommand(adapter.getAndroidCommandHelper(), commandName, commandParameters, timeout, errorMessage, processInput, adapter);
		}

	private void createAndroidProject(File exportFolder)
	{
//		ProcessRunner runner = new ProcessRunner();
//		String[] commandArray = new String[]
//		{ adapter.getAndroidCommandHelper().getAndroidCommand(AndroidSDKHelper.COMMAND_ANDROID), "create",
//				"project", "-t", "1", "-a", adapter.getClassName(), "-k", adapter.getPackageName(),
//				"-p", exportFolder.getAbsolutePath() };

		String[] commandParameters = new String[]
		{	"create",
			"project", "-t", "1", "-a", adapter.getClassName(), "-k", adapter.getPackageName(),
			"-p", exportFolder.getAbsolutePath() };
		String commandError = "Failed to create android source project";
		
		runAndroidSDKCommand(AndroidSDKHelper.COMMAND_ANDROID, commandParameters, 30000, commandError);

//		try
//		{
//			int rc = runner.run(commandArray, 30000);
//			String output = new String(runner.getOutputStream().toByteArray());
//			String error = new String(runner.getErrorStream().toByteArray());
//			adapter.writeExportLog(rc, output, error);
//			if (rc != 0)
//			{
//				adapter.reportError(rc, "Problem creating project", output, error);
//				throw new ExportException("Failed to create android project");
//			}
//		}
//		catch (Exception e)
//		{
//			throw new ExportException("Failed to create android project", e);
//		}
	}
	private void updateAndroidProject(File exportFolder)
	{
//		ProcessRunner runner = new ProcessRunner();
//		String[] commandArray = new String[]
//		{ adapter.getAndroidCommandHelper().getAndroidCommand(AndroidSDKHelper.COMMAND_ANDROID), "update",
//				"project", "-t", "1", "-p", exportFolder.getAbsolutePath() };

		String[] commandParameters = new String[]
		{	"update",
			"project", "-t", "1", "-p", exportFolder.getAbsolutePath() };
		String commandError = "Failed to update android source project";

		runAndroidSDKCommand(AndroidSDKHelper.COMMAND_ANDROID, commandParameters, 30000, commandError);

//		try
//		{
//			int rc = runner.run(commandArray, 30000);
//			String output = new String(runner.getOutputStream().toByteArray());
//			String error = new String(runner.getErrorStream().toByteArray());
//			adapter.writeExportLog(rc, output, error);
//			if (rc != 0)
//			{
//				adapter.reportError(rc, "Problem creating project", output, error);
//				throw new ExportException("Failed to create android project");
//			}
//		}
//		catch (Exception e)
//		{
//			throw new ExportException("Failed to create android project", e);
//		}
	}
	
	public void copyFolder(File srcRoot, String srcPath, File targetRoot, String targetPath)
	{
		try
		{
			File srcFolder = new File(srcRoot, srcPath);
			FileUtils.copy(srcFolder, new File(targetRoot, targetPath), new FilenameFilter()
			{

				public boolean accept(File dir, String name)
				{
					return !".svn".equals(name);
				}
			}, false);
		}
		catch (Exception e)
		{
			throw new ExportException("Export failed", e);
		}
	}

	private void androidBuildAndInstall()
	{
		int rc = androidAntDebug();
		if (rc != 0)
			return;
		if (AndroidSDKHelper.CREATE_DEBUG_PACKAGE.equals(adapter.getDeviceName()))
			adapter.message("Success", "Debug APK built");
		else
		{
// The following condition is always false since creating default AVD tends to fail
//
			if (adapter.getDeviceName().startsWith(AndroidSDKHelper.LAUNCH_NEW_AVD))
			{
				String avdName = androidCreateAvd();
				if (avdName == null)
					return;
				rc = androidLaunchEmulator(avdName);
				if (rc != 0)
					return;
				try
				{
					// Give AVD enough time for first-time initialization
					Thread.sleep(20000);
				}
				catch (InterruptedException e)
				{
	
				}
			}
			if (adapter.getDeviceName().startsWith(AndroidSDKHelper.LAUNCH_AVD))
			{
				rc = androidLaunchEmulator(getDeviceName());
				if (rc != 0)
					return;
			}
			int retry = 0;
			do
			{
				if (retry > 0)
				{
					try
					{
						System.out.println("Waiting for device");
	
						Thread.sleep(10000);
					}
					catch (InterruptedException e)
					{
	
					}
				}
				rc = androidInstall();
				++retry;
			}
			while (retry < 10 && rc == NOT_READY);
			if (rc == NOT_READY)
			{
				adapter.error("Error",
						"Android device not ready.");
			}
			if (rc != 0)
				return;
			
			rc = androidLaunchApplication();
			if (rc == 0)
				adapter.message("Success",
						"Application launched successfully");
		}
	}

	private int androidInstall()
	{
//		ProcessRunner runner = new ProcessRunner();
//		runner.setWorkingDirectory(new File(adapter.getExportLocation()));
		int rc = 0;

		String[] commandParameters = new String[]
		{	"-s", getDeviceName(),
			"install", "-r", getDebugAPKPath() };
		String commandError = "Failed to install APK on " + getDeviceName();
		String commandOutput = runAndroidSDKCommand(AndroidSDKHelper.COMMAND_ADB, commandParameters, TIMEOUT, commandError);

//		try
//		{
//			System.out.println("Trying to install application");
//			rc = runner.run(
//					new String[]
//					{ adapter.getAndroidCommandHelper().getAndroidCommand(AndroidSDKHelper.COMMAND_ADB), "-s",
//							getDeviceName(), "install", "-r", getDebugAPKPath() }, TIMEOUT);
//			String output = new String(runner.getOutputStream().toByteArray());
//			String error = new String(runner.getErrorStream().toByteArray());

		if (commandOutput.indexOf("Is the system running") >= 0)
			rc = NOT_READY;
		if (!SUCCESS.equals(Misc.lastLine(commandOutput)))
			rc = -1;
//
//			adapter.reportError(rc, "Install failed", output, error);
//
//		}
//		catch (Exception e)
//		{
//			throw new ExportException("Install failed", e);
//		}
		return rc;
	}

	private String getDebugAPKPath()
	{
		String path = adapter.getExportLocation() + "/bin/" + adapter.getClassName() + "-debug.apk";
		File file = new File(path);
		try
		{
			return file.getCanonicalPath();
		}
		catch (IOException e)
		{
			throw new ExportException("Failed to locate apk file", e);
		}
	}

	private String getDeviceName()
	{
		return Misc.lastToken(adapter.getDeviceName(), "[: ]");
	}

// This method is never called since creating default AVD tends to fail
//
	private String androidCreateAvd()
	{
//		ProcessRunner runner = new ProcessRunner();
//		runner.setProcessInput("no\n");
//		runner.setWorkingDirectory(new File(adapter.getExportLocation()));
//		int rc;
//		try
//		{
			String target = getAndroidTarget();
			if (target == null)
			{
				return null;
			}
			String avdName = "default-" + target;

			String[] commandParameters = new String[]
			{	"create", "avd",
				"-n", avdName,
				"-t", target };
			String commandError = "Failed to create default AVD " + avdName;
			String processInput = "no\n";
			runAndroidSDKCommand(AndroidSDKHelper.COMMAND_ANDROID, commandParameters, TIMEOUT, commandError, processInput);

//			rc = runner.run(new String[]
//			{ adapter.getAndroidCommandHelper().getAndroidCommand( AndroidSDKHelper.COMMAND_ANDROID), "create",
//					"avd", "-n", avdName, "-t", target }, TIMEOUT);
//
//			String output = new String(runner.getOutputStream().toByteArray());
//			String error = new String(runner.getErrorStream().toByteArray());
//			adapter.writeExportLog(rc, output, error);
//			if (rc != 0)
//			{
//				adapter.reportError(rc, "Problem launching AVD", output, error);
//				return null;
//			}
			return avdName;
//		}
//		catch (Exception e)
//		{
//			throw new ExportException(commandError, e);
//		}
	}

	private String getAndroidTarget()
	{
		ArrayList<String> list = new ArrayList<String>();
//		ProcessRunner runner = new ProcessRunner();
//		runner.setWorkingDirectory(new File(adapter.getExportLocation()));
//		int rc;

		String target = null;
		String[] commandParameters = new String[]
		{	"list",
			"targets" };
		String commandError = "Failed to obtain list of installed targets";
		String commandOutput = runAndroidSDKCommand(AndroidSDKHelper.COMMAND_ANDROID, commandParameters, TIMEOUT, commandError);

//		try
//		{
//			rc = runner.run(new String[]
//			{ adapter.getAndroidCommandHelper().getAndroidCommand( AndroidSDKHelper.COMMAND_ANDROID), "list",
//					"targets" }, TIMEOUT);
//			String output = new String(runner.getOutputStream().toByteArray());
//			String error = new String(runner.getErrorStream().toByteArray());
//			if (rc == 0)
//			{
				String potentialTarget = null;
				String abi = null;
				for (String l : commandOutput.split("[\r\n]"))
				{
					if (l.startsWith("id:"))
					{
						potentialTarget = Misc.lastToken(l, "[\"]");
					}
					if(l.contains("Tag/ABIs")) {
						if (l.contains("arm"))
								target=potentialTarget;
					}
				}
				if (target == null)
					target=potentialTarget;
				if (target == null)
					adapter.error("Error",
							"No Android Targets Installed");
//			}
//			else
//			{
//				adapter.writeExportLog(rc, output, error);
//				adapter.reportError(rc, "Problem getting AVD List", output, error);
//			}
			return target;
//		}
//		catch (Exception e)
//		{
//			throw new ExportException("AVD Launch failed", e);
//		}
	}

	private String runAntCommand(String[] antCommand, String commandError)
	{
		String commandOutput = AndroidSDKHelper.runCommand(antCommand, TIMEOUT, commandError, null, adapter);
		return commandOutput;
	}

	private int androidAntDebug()
	{
//		ProcessRunner runner = new ProcessRunner();
//		runner.setWorkingDirectory(new File(adapter.getExportLocation()));
//		int rc;

		String[] antCommand = adapter.getAntCommand("debug");
		String commandError = "Failed to build a debug APK";
		runAntCommand(antCommand, commandError);

//		try
//		{
//			String target = "debug";
//			String[] command = adapter.getAntCommand(target);
//			rc = runner.run(command, TIMEOUT);
//			String output = new String(runner.getOutputStream().toByteArray());
//			String error = new String(runner.getErrorStream().toByteArray());
//			adapter.writeExportLog(rc, output, error);
//			if (rc != 0)
//			{
//				adapter.reportError(rc, "Build failed", output, error);
//			}
//		}
//		catch (Exception e)
//		{
//			throw new ExportException("Build failed", e);
//		}

		return 0;
	}

	private int androidLaunchEmulator(String avdName)
	{
//		ProcessRunner runner = new ProcessRunner();
//		runner.setWorkingDirectory(new File(adapter.getExportLocation()));
//		int rc;

		String[] launchCommandParameters =  new String[] { "-avd", avdName };
		String launchCommandError = "Failed to launch emulator " + avdName;
		runAndroidSDKCommand(COMMAND_EMULATOR, launchCommandParameters, -1, launchCommandError);

		String[] waitCommandParameters = new String[]
		{	"wait-for-device",
			ADB_COMMAND_SHELL,
			"getprop",
			"sys.boot_completed" };
		String waitCommandError = "Failed to connect to launched emulator " + avdName;

		long timeout = 5000;
		long aggregate = 0;
		boolean success = false;
		do
		{
            try
			{
				Thread.sleep(timeout);
			}
			catch (InterruptedException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			success = runAndroidSDKCommand(AndroidSDKHelper.COMMAND_ADB, waitCommandParameters, timeout, waitCommandError).startsWith("1");
		}
		while (!success && ((aggregate += timeout) < TIMEOUT));
		if (!success)
			return -1;
		
		try
		{
//			rc = runner.run(new String[]
//			{ adapter.getAndroidCommandHelper().getAndroidCommand(COMMAND_EMULATOR), "-avd", avdName }, -1);
//			rc = runner.run(
//					new String[]
//					{ adapter.getAndroidCommandHelper().getAndroidCommand(AndroidSDKHelper.COMMAND_ADB),
//							"wait-for-device" }, TIMEOUT);

			ArrayList<String> runningDevices = new ArrayList<String>();
//			if (rc == 0)
//			{
				AndroidSDKHelper.addAndroidDevices(adapter.getAndroidCommandHelper(), runningDevices, "No Running AVD");
//			}
			if (runningDevices.isEmpty())
//			{
//				String output = new String(runner.getOutputStream().toByteArray());
//				String error = new String(runner.getErrorStream().toByteArray());
//				adapter.writeExportLog(rc, output, error);
//				adapter.reportError(rc, "No Running AVD", output, error);
				return -1;
//			}

			adapter.setDeviceName(runningDevices.get(0));
		}
		catch (Exception e)
		{
			throw new ExportException("AVD Launch failed", e);
		}
		return 0;
	}

	private void androidUnlockEmulator()
	{
//		ProcessRunner runner = new ProcessRunner();
//		runner.setWorkingDirectory(new File(adapter.getExportLocation()));
//		int rc;

		String[] commandParameters = new String[]
		{	"wait-for-device",
			ADB_COMMAND_SHELL,
			"input", "keyevent", "82"  };
		String commandError = "Failed to unlock " + getDeviceName();

		runAndroidSDKCommand(AndroidSDKHelper.COMMAND_ADB, commandParameters, TIMEOUT, commandError);

//		try
//		{
//			rc = runner.run(
//					new String[]
//					{ adapter.getAndroidCommandHelper().getAndroidCommand(AndroidSDKHelper.COMMAND_ADB),
//							ADB_COMMAND_SHELL, "input", "keyevent", "82" }, TIMEOUT);
//			if (rc != 0)
//			{
//				String output = new String(runner.getOutputStream().toByteArray());
//				String error = new String(runner.getErrorStream().toByteArray());
//				adapter.writeExportLog(rc, output, error);
//				adapter.reportError(rc, "AVD could not be unlocked", output, error);
//				rc=0;
//			}
//		}
//		catch (Exception e)
//		{
//			// TODO Auto-generated catch block
//		}
	}

	private int androidLaunchApplication()
	{
//		ProcessRunner runner = new ProcessRunner();
//		runner.setWorkingDirectory(new File(adapter.getExportLocation()));
//		int rc;

		String[] commandParameters = new String[]
		{	"-s", getDeviceName(),
			ADB_COMMAND_SHELL,"am",
			"start",
			"-a", "android.intent.action.MAIN",
			"-c", "android.intent.category.LAUNCHER",
			"-n", adapter.getPackageName() + "/" + adapter.getPackageName() + "." + adapter.getClassName() };
		
		String commandError = "Failed to launch the application in " + getDeviceName();

		runAndroidSDKCommand(AndroidSDKHelper.COMMAND_ADB, commandParameters, TIMEOUT, commandError);

//		try
//		{
//			rc = runner.run(
//					new String[]
//					{
//							adapter.getAndroidCommandHelper().getAndroidCommand(AndroidSDKHelper.COMMAND_ADB),
//							"-s",
//							getDeviceName(),
//							ADB_COMMAND_SHELL,
//							"am",
//							"start",
//							"-a",
//							"android.intent.action.MAIN",
//							"-c",
//							"android.intent.category.LAUNCHER",
//							"-n",
//							adapter.getPackageName() + "/" + adapter.getPackageName() + "."
//									+ adapter.getClassName() }, TIMEOUT);
//			String output = new String(runner.getOutputStream().toByteArray());
//			String error = new String(runner.getErrorStream().toByteArray());
//			adapter.writeExportLog(rc, output, error);
//			if (error.indexOf("Error") >= 0)
//				rc = -1;
//
//			if (rc != 0)
//			{
//				adapter.reportError(rc, "Launch Failed", output, error);
//			}

			androidUnlockEmulator();
//		}
//		catch (Exception e)
//		{
//			throw new ExportException("Launch failed", e);
//		}
		return 0;
	}

	protected boolean copyfromAlternativeSources(File target, File... sources)
	{
		File source = null;
		for (File s : sources)
		{
			if (s.exists())
			{
				source = s;
				break;
			}
		}
		if (source != null)
		{
			try
			{
				FileUtils.copy(source, target, null, true);
			}
			catch (IOException e)
			{
				throw new ExportException("Failed to export icon", e);
			}
			return true;
		}
		else
			return false;
	}

	protected void exportImages(String appName, File exportFolder)
	{
		File images = new File(adapter.getProjectFolder(), "images");
		File icon36 = new File(images, "icon36.png");
		File ldpiIconTarget = new File(exportFolder, "res/drawable-ldpi/icon.png");
		File icon48 = new File(images, "icon48.png");
		File mdpiIconTarget = new File(exportFolder, "res/drawable-mdpi/icon.png");
		File icon72 = new File(images, "icon72.png");
		File hdpiIconTarget = new File(exportFolder, "res/drawable-hdpi/icon.png");

		File icon96 = new File(images, "icon96.png");
		File xhdpiIconTarget = new File(exportFolder, "res/drawable-xhdpi/icon.png");
		File defaultIconTarget = new File(exportFolder, "res/drawable/icon.png");

		File icon57 = new File(images, "icon57.png");

		copyfromAlternativeSources(ldpiIconTarget, icon36, icon48, icon57);
		copyfromAlternativeSources(mdpiIconTarget, icon48, icon57);
		copyfromAlternativeSources(hdpiIconTarget, icon72, icon48, icon57);
		copyfromAlternativeSources(xhdpiIconTarget, icon96, icon48, icon57);
		copyfromAlternativeSources(defaultIconTarget, icon96, icon48, icon57);

	}

	@Override

	protected void exportScripts(File wwwFolder)
	{
		final UserAgent userAgent = UserAgent.get(getUserAgentStr());
		ScriptAggregator aggregator = new ScriptAggregator(getWebDirectories(), new File(adapter.getProjectFolder(),ProjectStructure.SCRIPT_EXCLUDE));
		
		ArrayList<File> scriptsFileList = aggregator.getAllFiles(new FilenameFilter()
		{

			public boolean accept(File dir, String name)
			{
				
				return userAgent.match(name)
					&& !name.equals(tersus.ProjectStructure.DEFAULT_NAV_SCRIPT_NAME)
					&& !name.equals(tersus.ProjectStructure.URL_NAV_SCRIPT_NAME);
			}
		});
		
		numberOfScriptFiles = exportSplitScripts(wwwFolder , scriptsFileList);
	}

	@Override
	public String getScriptFragment()
	{
		return getSplitScriptsFragment(numberOfScriptFiles);
	}

	protected String getUserAgentStr()
	{
		return "iphone webkit android";
	}
}

package tersus.webapp.appexport;

public interface IProcessDialog
{

	void setProcess(Process start);

	void setCloseOnSuccess(boolean b);

	void setTitle(String string);

	int open();

	int getReturnCode();

}

/*******************************************************************************
 * Copyright (c) 2003-2011 Tersus Software Ltd. and others. All rights reserved.
 * 
 * This program is made available under the terms of the GNU General Public
 * License v2, which is part of this distribution and is available at
 * http://www.gnu.org/licenses/gpl.txt.
 * 
 * Contributors: Tersus Software Ltd. - Initial API and implementation
 ******************************************************************************/
package tersus.webapp;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;

import tersus.model.FlowModel;
import tersus.model.FlowType;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.Path;
import tersus.model.SubFlow;
import tersus.util.FileUtils;
import tersus.webapp.appexport.IPhoneExporter;

public class ServiceFilter implements Filter
{
	private static final String REDIRECT_FILE = "/redirect.txt";
	private Engine engine = null;
	private HashMap<String, String> redirections = new HashMap<String, String>();
	private HashSet<String> services = new HashSet<String>();
	private ServletContext servletContext;

	private boolean initialized = false;
	private boolean urlNavigation;

	private void addWebServices(Path path, FlowModel systemModel)
	{
		for (ModelElement e : systemModel.getElements())
		{
			Model refModel = e.getReferredModel();
			if (e instanceof SubFlow)
			{
				Path newPath = path.getCopy();
				newPath.addSegment(e.getRole());
				if (refModel.getPluginDescriptor() != null
						&& refModel.getPluginDescriptor().isWebService())
				{
					services.add("/" + newPath.toString());
					if (newPath.getNumberOfSegments() == 1
							&& Engine.WELCOME_SERVICE.equals(e.getRole().toString()))
					{
						services.add("/");
						services.add("/index.html");
					}
				}
				else if (refModel instanceof FlowModel
						&& ((FlowModel) refModel).getType() == FlowType.SYSTEM)
					addWebServices(newPath, (FlowModel) refModel);
			}
		}

	}

	public void destroy()
	{
		if (engine != null)
			engine.destroy();
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException
	{
		if (!initialized) // Very small risk of race condition
			init();
		HttpServletRequest req = (HttpServletRequest) request;
		String path = getPath(req);
		if ("/_Export/iPhone".equals(path))
			generateiPhoneApp(request, response);
		else if (redirections.containsKey(path))
			((HttpServletResponse) response).sendRedirect((String) redirections.get(path));
		else if (services.contains(path))
			engine.service(request, response);
		else if (path.equals("/"))
			engine.service(request, response);
		else if (urlNavigation && FilenameUtils.getExtension(path).isEmpty() && !path.equals("/Diagnostics"))
			engine.service(request, response);
		else
			chain.doFilter(request, response);

	}

	private void generateiPhoneApp(ServletRequest request, ServletResponse response)
	{
		IPhoneExporter exporter = new IPhoneExporter((ContextPool) servletContext.getAttribute(ContextPool.POOL_KEY));
		exporter.export((HttpServletRequest)request, (HttpServletResponse)response);
	}

	private String getPath(HttpServletRequest request)
	{
		String result = request.getPathInfo();
		if (result == null)
		{
			result = request.getServletPath();
		}
		if ((result == null) || (result.equals("")))
		{
			result = "/";
		}
		return (result);
	}

	public void init(FilterConfig filterConfig) throws ServletException
	{
		servletContext = filterConfig.getServletContext();
		init();
	}

	synchronized public void init()
	{
		if (initialized)
			return;

		ContextPool pool = (ContextPool) servletContext.getAttribute(ContextPool.POOL_KEY);
		if (pool == null)
			return;
		engine = new Engine();

		engine.init(pool);
		
		urlNavigation = pool.urlNavigation;

		try
		{
			if (servletContext.getResource(REDIRECT_FILE) != null)
			{
				String redirectionsStr = FileUtils.readString(
						servletContext.getResource(REDIRECT_FILE).openStream(), Engine.ENCODING,
						true);
				StringTokenizer tok = new StringTokenizer(redirectionsStr, "\n\r");
				while (tok.hasMoreElements())
				{
					String item = tok.nextToken().trim();
					int index = item.indexOf(":");
					if (index < 0)
						engine.getPool().engineLog.error("Invalid redirect entry '" + item + "'");
					String path = item.substring(0, index).trim();
					String location = item.substring(index + 1);
					redirections.put(path, location);
				}
			}
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		FlowModel rootModel = engine.getPool().getRootModel();
		addWebServices(Path.EMPTY, rootModel);
		initialized = true;

	}

}

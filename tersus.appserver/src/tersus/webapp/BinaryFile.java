/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.webapp;

/**
 * A structure that holds the content of a file (as posted using a multipart HTTP request)
 * 
 * @author Youval Bronicki
 *
 */
public class BinaryFile
{
	private String fileName;
	private byte[] data;
	private String contentType;

	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}

	public String getFileName()
	{
		return fileName;
	}

	public void setData(byte[] data)
	{
		this.data = data;
	}

	public byte[] getData()
	{
		return data;
	}

	public void setContentType(String contentType)
	{
		this.contentType = contentType;
	}

	public String getContentType()
	{
		return contentType;
	}

}

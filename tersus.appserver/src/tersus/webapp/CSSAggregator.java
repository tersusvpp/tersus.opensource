package tersus.webapp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.lesscss.LessCompiler;
import org.lesscss.LessException;

import tersus.ProjectStructure;
import tersus.util.FileUtils;

public class CSSAggregator extends FileAggregator
{
    private static final String CSS_SUFFIX = ".css";
	private static final String LESS_SUFFIX = ".less"; 


    public CSSAggregator(List<File> baseDirectories, File excludeFile)
    {
        addDirectories(baseDirectories);
        setExcludeFile(excludeFile);
    }
    
    protected String getFileHeader(File file)
    {
        return "\n/* file: "+file.getName() + " */\n";
    }
    
    protected void addDirectories(List<File> baseDirectories)
    {
        for (File dir : baseDirectories)
        {
        	addDirectory(dir, dir);
        	addDirectoryTree(dir, new File(dir, ProjectStructure.STYLES));
        }
    }
	@Override
	public StringBuffer getAggregatedFile(FilenameFilter filter) throws IOException
	{
		List<File> all = getAllFiles(filter);
		List<File> cssFiles = new ArrayList<File>(all.size());
		List<File> lessFiles = new ArrayList<File>(all.size());
		for (File file:all)
		{
			if (file.getName().endsWith(CSS_SUFFIX))
				cssFiles.add(file);
			else if(file.getName().endsWith(LESS_SUFFIX))
				lessFiles.add(file);
		}
		
		StringBuffer css = getAggregatedFile(cssFiles);
		if (lessFiles.size()> 0)
		{
			StringBuffer less = getAggregatedFile(lessFiles);
//			long time = System.currentTimeMillis();
			LessCompiler lessCompiler = new LessCompiler();
			String compiled = null;
			try
			{
				compiled = lessCompiler.compile(less.toString());
			}
			catch (LessException e)
			{
				String message = "";
				try
				{
					File f = File.createTempFile("Aggregated", ".less");
					FileUtils.write(less.toString(), new FileOutputStream(f), true);
					message = " Aggregated LESS file saved in "+f.getAbsolutePath();
				}
				catch (Exception e1)
				{
					
				}
				throw new RuntimeException("Failed to compile LESS/CSS."+message, e);
			}
			// System.out.println("LESS Compilation time: "+ (System.currentTimeMillis()-time) + " milliseconds");
			css.append(compiled);
		}
		return css;
	}

}

package tersus.webapp;


public class ClassLoaderWrapper extends ClassLoader
{

	public ClassLoaderWrapper(ClassLoader base)
	{
		super(base);
	}

	@Override
	public synchronized Class<?> loadClass(String name, boolean resolve)
			throws ClassNotFoundException
	{
		Class<?> clazz = getParent().loadClass(name);
		if (clazz != null && resolve)
			super.resolveClass(clazz);
		return clazz;
	}
	

}

package tersus.webapp;

public class NotificationServerException extends RuntimeException
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public NotificationServerException(String message)
    {
        super(message);
    }
    public NotificationServerException(String message, Throwable e)
    {
        super(message,e);
    }

}

package tersus.webapp.jsloader;

import java.io.IOException;
import java.io.Writer;
import java.util.HashSet;
import java.util.regex.Pattern;

import tersus.util.EscapingWriter;

public class JSONWriter
{
    private Pattern jsIdentifierPattern;
    private HashSet<String> reservedNames;
    
    public JSONWriter(Writer w)
    {
    	reservedNames = new HashSet<String>();
        this.w = w;
        this.ew = new EscapingWriter(w);
        needSeparator = false;
        jsIdentifierPattern = Pattern.compile("[a-zA-Z][a-zA-Z0-9_]*");
        reservedNames.add("for");
    }

    public void flush() throws IOException
    {
        w.flush();
    }

    private int indentation = 0;
    private Writer w;
    private EscapingWriter ew;
    private boolean needSeparator;
    private boolean compact = false;

    public void attribute(String name, Object value) throws IOException
    {
        if (value != null)
        {
            separator();
            newline();
            indent();
            if (reservedNames.contains(name) && !jsIdentifierPattern.matcher(name).matches())
                w.write(name);
            else
                ew.quote(name);
            w.write(':');
            if (value instanceof Number || value instanceof Boolean || value instanceof tersus.runtime.Number || "true".equals(value) || "false".equals(value))
                w.write(String.valueOf(value));
            else
                ew.quote(value);
        }
    }

    public void obj() throws IOException
    {
        obj(null);
    }
    public void obj(String name) throws IOException
    {
        separator();
        newline();
        indent();
        if (name != null)
        {
            w.write(name);
            w.write(':');
        }
        w.write('{');
        ++indentation;
        needSeparator = false;
    }

    private void newline() throws IOException
    {
        if (!compact)
            w.write('\n');
    }

    private void indent() throws IOException
    {
        if (!compact)
            for (int i = 0; i < indentation; i++)
                w.write(' ');
    }

    public void end_obj() throws IOException
    {
        newline();
        --indentation;
        indent();
        w.write('}');
        needSeparator = true;
    }

    public void array() throws IOException
    {
        array(null);
    }
    public void array(String name) throws IOException
    {
        separator();
        newline();
        indent();
        if (name != null)
        {
            w.write(name);
            w.write(':');
        }
        w.write('[');
        ++indentation;
        needSeparator = false;
    }

    protected void separator() throws IOException
    {
        if (needSeparator)
            w.write(",");
        needSeparator = true;
    }

    public void end_array() throws IOException
    {
        --indentation;
        indent();
        w.write(']');
    }

    public boolean isCompact()
    {
        return compact;
    }

    public void setCompact(boolean compact)
    {
        this.compact = compact;
    }

    public void reset()
    {
        indentation = 0;
        needSeparator = false;
    }
    
}

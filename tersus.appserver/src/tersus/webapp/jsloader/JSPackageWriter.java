package tersus.webapp.jsloader;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;
import java.util.Set;

import tersus.model.BuiltinProperties;
import tersus.model.DataElement;
import tersus.model.DataType;
import tersus.model.DisplayModelWrapper;
import tersus.model.FileRepository;
import tersus.model.FlowDataElementType;
import tersus.model.FlowModel;
import tersus.model.FlowType;
import tersus.model.Link;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelException;
import tersus.model.ModelId;
import tersus.model.ModelUtils;
import tersus.model.Package;
import tersus.model.PackageId;
import tersus.model.Slot;
import tersus.model.SubFlow;
import tersus.util.EscapeUtil;

public class JSPackageWriter extends JSONWriter
{
	private static final String PLUGIN = "p";
	private static final String CONSTRUCTOR = "c";
	private static final String NAME = "n";
	private static final String CONTSTANT_TRIGGER = "ct";
	private static final String OPERATION = "op";
	private static final String TARGET = "tgt";
	private static final String SOURCE = "src";
	private static final String LINK = "l";
	private static final String MANDATORY = "m";
	private static final String INTERMEDIATE_DATA_ELEMENT = "var";
	private static final String IS_SELF_READY = "sr";
	private static final String CONSTANT_DATA_ELEMENT = "const";
	private static final String CONTEXT_DATA_ELEMENT = "ctx";
	private static final String ALWAYS_CREATE = "ac";
	private static final String DISPLAY_ELEMENT = "di";
	private static final String DATA_ELEMENT = "da";
	private static final String DEPENDENCY_RANK = "rk";
	private static final String SUB_FLOW = "sf";
	private static final String TYPE = "t";
	private static final String REPETITIVE = "rep";
	private static final String REF_ID = "ref";
	private static final String ROLE = "r";
	private static final String VALUE = "v";
	private static final String CANCELLABLE = "cc";
	private static final String SECURE = "sc";
	private static final String PROGRESS_MESSAGE = "pm";
	private static final String SHARED_PROPERTIES = "sp";
	private static final String ELEMENT_PROPERTIES = "ep";
	private static final String START_FUNCTION_CODE = "startCode";
	private static final String RUN_IMMEDIATELY = "runImmediately";
	private static final String EVENT_HANDLING = "eventHandling";
	private static final String SERVICE_TIMEOUT = "serviceTimeout";
	private static final String PRIMARY_KEY = "primaryKey";
	private static final String COLUMN_NAME = "columnName";
	private static final String TABLE_NAME = "tableName";
	private static final String ELEMENTS = "e";
	private static final String PLUGIN_VERSION = "pv";
	private static final String XML_NAMESPACE = "xmlNS";
	private static final String FORMAT = "format";
	private static final String FORMAT_GMT = "formatAsGMT";

	private Set<ModelId> permitted;
	private Set<ModelId> secure;

	public static void main(String[] args) throws Exception
	{
		FileRepository repository = new FileRepository();
		repository.setRoot(new File(args[0]));
		Package p = repository.getPackage(new PackageId(args[1]));
		JSPackageWriter w = new JSPackageWriter(new OutputStreamWriter(System.out));
		w.write(p);
		w.flush();

	}

	boolean checkPermitted(ModelId id)
	{
		return permitted == null || permitted.contains(id);
	}

	public JSPackageWriter(Writer w)
	{
		super(w);
	}

	public void write(tersus.model.Package p) throws IOException
	{
		array();
		for (Model m : p.getModels())
		{
			if (checkPermitted(m.getId()))
				writeModel(m);
		}
		end_array();
	}

	public void writeModel(Model model) throws IOException
	{
		try
		{
		obj();
		attribute(NAME, model.getName());
		attribute(CONSTRUCTOR, model.getPluginJavascript());
		attribute(PLUGIN, model.getPlugin());
		attribute(PLUGIN_VERSION, model.getProperty(BuiltinProperties.PLUGIN_VERSION));

		if (model instanceof DataType)
		{
			writeDataType((DataType) model);
		}
		else
		{
			writeFlowModel((FlowModel) model);
		}
		end_obj();
		}
		catch (RuntimeException e)
		{
			throw new ModelException("Failed to serialize "+model.getId(), e);
		}
	}

	private void writeFlowModel(FlowModel model) throws IOException
	{
		model.updateRanks();
		if (isServiceClient(model))
			writeServiceModel(model);
		else if (model.getType() == FlowType.DISPLAY)
			writeDisplayModel(DisplayModelWrapper.wrap(model));
		else
			writeProcessModel(model);

	}

	private void writeProcessModel(FlowModel model) throws IOException
	{
		attribute(START_FUNCTION_CODE, model.getProperty(BuiltinProperties.JAVASCRIPT_CODE)); // legacy
		attribute(RUN_IMMEDIATELY,
				"true".equals(model.getProperty(BuiltinProperties.RUN_IMMEDIATELY)), false);
		attribute(EVENT_HANDLING, model.getProperty(BuiltinProperties.EVENT_HANDLING));
		if (!model.getElements().isEmpty())
		{
			array(ELEMENTS);
			writeSlots(model);
			writeDataElements(model);
			writeSubFlows(model);
			writeLinks(model);
			end_array();
		}
	}

	private void writeDisplayModel(DisplayModelWrapper model) throws IOException
	{

		writeSharedProperties(model);
		if (!model.getElements().isEmpty())
		{
			array(ELEMENTS);
			writeSlots(model);
			writeDataElements(model);
			writeOrderedSubFlows(model);
			writeLinks(model);
			end_array();
		}
	}

	private void writeSharedProperties(DisplayModelWrapper model) throws IOException
	{
		List<String> sharedProperties = model.getSharedProperties();
		if (!sharedProperties.isEmpty())
		{
			obj(SHARED_PROPERTIES);
			for (String prop : sharedProperties) // TODO (optimization - read directly from flow
													// model, without creating a separate List)
			{
				Object v = model.getDisplayProperty(prop);
				if (!" ".equals(v))
					attribute(prop, v);
			}
			end_obj();
		}
	}

	private void writeServiceModel(FlowModel model) throws IOException
	{
		attribute(PROGRESS_MESSAGE, model.getProperty(BuiltinProperties.PROGRESS_MESSAGE));
		attribute(CANCELLABLE, model.getProperty(BuiltinProperties.CANCELLABLE));
		attribute(SERVICE_TIMEOUT, model.getProperty(BuiltinProperties.SERVICE_TIMEOUT));
		if (secure.contains(model.getModelId()))
			attribute(SECURE, true);
		array(ELEMENTS);
		writeSlots(model);
		end_array();
	}

	private void writeDataType(DataType model) throws IOException
	{
		String valueStr = EscapeUtil.unquote(model.getValue());
		if (valueStr != null)
		{
			if (model.checkPluginVersion(1))
				valueStr = EscapeUtil.unescape(valueStr);
			attribute(VALUE, valueStr);
		}
		if (!model.getName().equals(model.getTableName()))
			attribute(TABLE_NAME, model.getTableName());
		attribute(XML_NAMESPACE, model.getProperty(BuiltinProperties.XML_NAME_SPACE));
		attribute(FORMAT, model.getProperty(BuiltinProperties.FORMAT));
		attribute(FORMAT_GMT, model.getProperty(BuiltinProperties.FORMAT_USE_GMT), false);
		if (!model.getElements().isEmpty())
		{
			array(ELEMENTS);
			writeDataElements(model);
			end_array();
		}
	}

	private void writeElement(ModelElement element) throws IOException
	{
		ModelId refId = element.getRefId();
		if (!(refId == null || checkPermitted(refId)))
			return; // Not permitted
		obj();
		attribute(ROLE, element.getRole());
		attribute(REF_ID, refId);
		attribute(REPETITIVE, element.isRepetitive(), false);
		if (element instanceof DataElement)
		{
			attribute(PRIMARY_KEY, ((DataElement) element).isPrimaryKey(), false);
			attribute(COLUMN_NAME, element.getProperty(BuiltinProperties.COLUMN_NAME));
			attribute(XML_NAMESPACE, element.getProperty(BuiltinProperties.XML_NAME_SPACE));
			attribute(FORMAT, element.getProperty(BuiltinProperties.FORMAT));
			attribute(FORMAT_GMT, element.getProperty(BuiltinProperties.FORMAT_USE_GMT), false);

		}
		if (element instanceof Link)
			writeLink(element);
		else if (element instanceof Slot)
			writeSlot((Slot) element);
		else if (element instanceof DataElement)
		{
			writeDataElement((DataElement) element);
		}
		else if (element instanceof SubFlow)
		{
			if (ModelUtils.isDisplayModel(element.getParentModel())
					&& ModelUtils.isDisplayModel(element.getReferredModel()))
				writeDisplayElement((SubFlow) element);
			else
				writeSubFlow((SubFlow) element);
		}

		end_obj();
	}

	private void writeSubFlow(SubFlow element) throws IOException
	{
		attribute(TYPE, SUB_FLOW);
		attribute(DEPENDENCY_RANK, element.getDependencyRank());
	}

	private void writeDisplayElement(SubFlow element) throws IOException
	{
		attribute(TYPE, DISPLAY_ELEMENT);
		attribute(DEPENDENCY_RANK, element.getDependencyRank());
		attribute(ALWAYS_CREATE, element.checkAlwaysCreate(), true);
		List<String> elementProperties = DisplayModelWrapper.getElementProperties(element);
		if (!elementProperties.isEmpty())
		{
			obj(ELEMENT_PROPERTIES);
			for (String p : elementProperties)
				attribute(p, DisplayModelWrapper.getElementProperty(element, p));
			end_obj();

		}
	}

	private void writeSlots(Model model) throws IOException
	{
		for (ModelElement element : model.getElements())
		{
			if (element instanceof Slot)
				writeElement(element);
		}
	}

	private void writeDataElements(Model model) throws IOException
	{
		for (ModelElement element : model.getElements())
		{
			if (element instanceof DataElement)
				writeElement(element);
		}
	}

	private void writeLinks(Model model) throws IOException
	{
		for (ModelElement element : model.getElements())
		{
			if (element instanceof Link)
				writeElement(element);
		}
	}

	private void writeSubFlows(Model model) throws IOException
	{
		for (ModelElement element : model.getElements())
		{
			if (element instanceof SubFlow)
				writeElement(element);
		}
	}

	private void writeOrderedSubFlows(DisplayModelWrapper model) throws IOException
	{
		for (SubFlow subFlow : model.getOrderedSubFlows())
		{
			if (subFlow instanceof SubFlow)
				writeElement(subFlow);
		}
	}

	private void writeDataElement(DataElement element) throws IOException
	{
		if (element.getParentModel() instanceof DataType)
		{
			attribute(TYPE, DATA_ELEMENT);
		}
		else if (element.getType() == FlowDataElementType.PARENT)
			attribute(TYPE, CONTEXT_DATA_ELEMENT);
		else if (Boolean.TRUE.equals(element.getReferredModel().getProperty(
				BuiltinProperties.CONSTANT)))
		{
			attribute(TYPE, CONSTANT_DATA_ELEMENT);
			attribute(IS_SELF_READY, element.isSelfReady(), false);
		}
		else
		{
			attribute(TYPE, INTERMEDIATE_DATA_ELEMENT);
		}

		if (element.getParentModel() instanceof FlowModel)
			attribute(DEPENDENCY_RANK, element.getDependencyRank());

	}

	private void writeSlot(Slot slot) throws IOException
	{
		attribute(TYPE, slot.getType().toString().substring(0, 2).toLowerCase());
		attribute(MANDATORY, slot.isMandatory(), false);
		attribute(VALUE, slot.getProperty(BuiltinProperties.VALUE));
		attribute(DEPENDENCY_RANK, slot.getDependencyRank());
	}

	private void writeLink(ModelElement element) throws IOException
	{
		Link link = (Link) element;
		attribute(TYPE, LINK);
		attribute(SOURCE, link.getSource());
		attribute(TARGET, link.getTarget());
		attribute(OPERATION, link.getOperation());
		attribute(CONTSTANT_TRIGGER, link.isConstantTrigger(), false);
	}

	private void attribute(String name, boolean value, boolean defaultValue) throws IOException
	{
		if (value != defaultValue)
			attribute(name, value);
	}

	private void attribute(String name, Object rawValue, boolean defaultValue) throws IOException
	{
		boolean value = Boolean.TRUE.equals(rawValue) || "true".equals(rawValue);
		attribute(name, value, defaultValue);
	}

	private static boolean isServiceClient(Model model)
	{
		return model.getProperty(BuiltinProperties.TYPE) == FlowType.SERVICE
				|| model.getPluginDescriptor() != null
				&& model.getPluginDescriptor().isServiceClient();
	}

	public Set<ModelId> getPermitted()
	{
		return permitted;
	}

	public void setPermitted(Set<ModelId> permitted)
	{
		this.permitted = permitted;
	}

	public Set<ModelId> getSecure()
	{
		return secure;
	}

	public void setSecure(Set<ModelId> secure)
	{
		this.secure = secure;
	}

}

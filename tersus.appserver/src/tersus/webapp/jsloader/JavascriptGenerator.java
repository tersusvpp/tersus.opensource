/************************************************************************************************
 * Copyright (c) 2003-2011 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.webapp.jsloader;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tersus.model.ModelException;
import tersus.model.ModelId;
import tersus.model.Package;
import tersus.model.PackageId;
import tersus.model.Repository;
import tersus.util.EscapingWriter;
import tersus.util.Misc;
import tersus.webapp.Config;
import tersus.webapp.Engine;
import tersus.webapp.WebContext;

/**
 * 
 * Generates JavaScript representation of models (for use on the client side).
 * 
 * The generated code contains constructor functions for a hierachy of models
 * (JavaScript constructors (with their prototypes) are the nearest equivalent
 * to Java classes and so we generate a constructor for each model in the chosen
 * hierarchy)
 * 
 * It is still TBD how deep a hierarchy we should generate. Here are some
 * preliminary rules: - When we reach a server-side model, we don't need to
 * generate code for sub-models.
 * 
 * We may also decide that there is no need to generate code for UI that is
 * opened in a new window (NOT SURE ABOUT THAT).
 * 
 * @author Youval Bronicki
 * 
 */
public class JavascriptGenerator
{
    private static final String PACKAGES = "packages";
    private static final String PRETTY = "pretty";

    WebContext engineContext;

    public void init(WebContext engineContext)
    {
        this.engineContext = engineContext;
    }

    /**
     * packageList - a list of base packages to include, separated by colons
     * @param uiModelIds
     * @param packagesStr
     * @return
     */
	private static List<PackageId> getPackageList(Set<ModelId> uiModelIds,
			String packagesStr) {
		if (packagesStr == null)
        	packagesStr = ":";
        String[] packages = packagesStr.split(":");
        HashSet<PackageId> userPackages = new HashSet<PackageId>();
        for (ModelId id : uiModelIds)
        {
            userPackages.add(id.getPackageId());

        }
        HashSet<PackageId> packageSet;
        if (packages == null || packages.length == 0)
        {
            // No package ids specified - loading the whole application
            packageSet = userPackages;

        }
        else
        {
            packageSet = new HashSet<PackageId>();
            for (String packageStr: packages)
            {
                PackageId basePackageId = new PackageId(packageStr);
                packageSet
                        .addAll(getSubPackageIds(basePackageId, userPackages));
            }
        }

        ArrayList<PackageId> packageList = new ArrayList<PackageId>(packageSet
                .size());
        packageList.addAll(packageSet);
        sort(packageList);
        return packageList;
	}

	@SuppressWarnings("unchecked")
	private static void sort(ArrayList<PackageId> packageIdList)
	{
		Collections.sort(packageIdList);
	}

    private static Collection<PackageId> getSubPackageIds(PackageId basePackageId,
            Collection<PackageId> allPackageIds)
    {
        HashSet<PackageId> subPackageIds = new HashSet<PackageId>();
        for (PackageId id : allPackageIds)
        {
            if (basePackageId.isAncestorOf(id))
                subPackageIds.add(id);

        }
        return subPackageIds;
    }

    private Repository getRepository()
    {
        return engineContext.getModelLoader().getRepository();
    }

    protected void setContentType(HttpServletRequest request,
            HttpServletResponse response)
    {
        if (getCallbackName(request) != null)
            response.setContentType("text/javascript; charset=UTF-8");
        else
            response.setContentType("text/plain;charset=UTF-8");
    }

    private String getCallbackName(HttpServletRequest request)
    {
        return request.getParameter(Engine._JS_CALLBACK);
    }

    public void doRequest(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            String reponseBody = createResponseBody(request);
            response.setCharacterEncoding(Engine.ENCODING);
            setContentType(request, response);
            response.setDateHeader("Last-Modified", engineContext
                    .getLastModified());
            if (engineContext.getContextPool().hasExplicitTimestamp())
            {
                response.setDateHeader("Expires", System.currentTimeMillis()
                        + Config.YEAR_MILLISECONDS);
                response.addHeader("Cache-control", "public,max-age:" + Config.YEAR_SECONDS);
            }
            response.getWriter().write(reponseBody);
        }
        catch (Throwable e)
        {
            engineContext.getContextPool().engineLog.error("Error when generating models for client",e);
            response.setCharacterEncoding(Engine.ENCODING);
            setContentType(request, response);
            response.addHeader("Pragma", "no-cache");
            response.addHeader("Cache-control", "no-cache");
            StringWriter out = new  StringWriter();
            EscapingWriter ew = new EscapingWriter(out);
            if (getCallbackName(request) != null)
            {
                ew.write("tersus.packageLoader.error(");
                ew.quote(Misc.getShortName(e.getClass())+ " : " + e.getMessage());
                ew.write(");");
            }
            else
            {
                out.write(new Date()+ "\n");
                e.printStackTrace(new PrintWriter(out));
            }
            response.getWriter().write(out.toString());
        }
    }

    private String createResponseBody(HttpServletRequest request)
            throws IOException
    {
        String callbackName = getCallbackName(request);
        Repository repository = getRepository();
       Set<ModelId> models = engineContext.getUserPermittedUIModels();
       Set<ModelId> secureModels = engineContext.getContextPool().getSecureModels();
		String packagesStr = request.getParameter(PACKAGES);
        boolean compact = request.getParameter(PRETTY) == null;
        return createScript(repository, models, secureModels, packagesStr, callbackName,
				compact);
    }

	public static String createScript(Repository repository, Set<ModelId> models, Set<ModelId> secureModels,
			String packagesStr, String callbackName, boolean compact)
			throws IOException {
		List<PackageId> packageList = getPackageList(models, packagesStr);
        StringWriter sw = new StringWriter();
        EscapingWriter ew = new EscapingWriter(sw);
		JSPackageWriter pw = new JSPackageWriter(compact ? ew : sw); // The package serializations are escaped unless we're in 'pretty' mode
		pw.setPermitted(models);
		pw.setSecure(secureModels);
        pw.setCompact(compact);
        if (callbackName != null)
            ew.writeRaw("var _js_load = function() {\n");
        ew.writeRaw("var packages = {\n");

        boolean first = true;
        for (PackageId pkgId : packageList)
        {
            if (!first)
                ew.writeRaw(",\n");
            else
                first = false;

            ew.quote(pkgId);
            ew.writeRaw(":'");
            pw.reset();
 			Package pkg = repository.getPackage(pkgId);
 			if (pkg == null)
 				throw new ModelException("Missing package "+pkgId);
			pw.write(pkg);
            ew.writeRaw("'");
        }
        if (callbackName == null)
            ew.writeRaw("};\n;packages;");
        else
        {
            ew.writeRaw("};\n");
            ew.writeRaw(callbackName);
            ew.writeRaw("(packages);\n};\n");
            ew.writeRaw("_js_load();\n");
        }
        String reponseBody = sw.toString();
        return reponseBody;
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/

package tersus.webapp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.StringTokenizer;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.Appender;
import org.xeustechnologies.jcl.JarClassLoader;
import org.xeustechnologies.jcl.ProxyClassLoader;

import tersus.ProjectStructure;
import tersus.model.BuiltinProperties;
import tersus.model.ModelId;
import tersus.model.Repository;
import tersus.model.Role;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.IResourceHandler;
import tersus.runtime.InvocationAdapter;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.EntryFilter;
import tersus.util.FileUtils;
import tersus.webapp.jsloader.JavascriptGenerator;

/**
 * @author Youval Bronicki
 * 
 */
public class ContextPool extends tersus.runtime.ContextPool implements ServletContextListener,
		HttpSessionListener, HttpSessionAttributeListener
{
	public static final String DEFAULT_LOG_FILE = "tersus.log";

	private static final String TIMEOUT_KEY = "request_timeout";

	public static final String POOL_KEY = "Context Pool";

	public static final String CLASS_LOADER_KEY = "class_loader";

	public static final String DATABASE_ADAPTERS_KEY = "database_adapters";
	
//	public static final String URL_NAVIGATION_KEY = "url_navigation";
	
	public static final String TRUSTED_HOSTS_KEY = "trusted_hosts";

	public static final String TRACE_MAX_STRING_LENGTH_KEY = "trace_max_string_length";

	public static final String MAX_UPLOAD_FILE_SIZE_KEY = "max_upload_file_size";

	public static final String PERMISSIONS_DATA_SOURCE_KEY = "permissions_data_source";

	public static final String PERMISSIONS_QUERY_KEY = "permissions_query";

	public static final String USE_SQL_QUOTED_IDENTIFIERS = "use_sql_quoted_identifiers";

	public static final String CONTEXT_LEAK_CLEANUP_INTERVAL = "context_leak_cleanup_interval";

	public static final String PERMISSIONS_LAST_UPDATE_QUERY_KEY = "permissions_last_update_query";

	public static final String DISABLE_SSL_CERTIFICATE_VALIDATION = "disable_ssl_certificate_validation";
	
	public static final String BUFFER_ALL_HTTPS_MULTIPART_REQUESTS = "buffer_all_https_multipart_requests";

	private int numberOfContexts = 0;

	public static final String REMOTE_USER = "REMOTE_USER";

	public static final String REMOTE_ADDR = "REMOTE_ADDR";

	private static final Role ON_NEW_SESSION = Role.get("<On New Session>");

	private static final Role ON_LOGOUT = Role.get("<On Logout>");

	private static final Role ON_TIMEOUT = Role.get("<On Session Timeout>");

	private static final Role ON_LOGIN = Role.get("<On Login>");

	private static final String LOGGED_OUT = "LOGGED_OUT";

	private File projectRoot = null;

	private ServletContext servletContext;

	private IRepositoryController repositoryController;

	private static final String INIT_DB = "init_db";

	private static final String CHECK_DB = "check_db";

	private static final String PRELOAD_HANDLERS = "preload_handlers";

	public static final String REPOSITORY_CONTROLLER = "repository_controller";

	public static final String TIMESTAMP_PATHS = "timestamp_files";

	public static final String TIMESTAMP_KEY = "timestamp";

	public static final String LOG_FILE_KEY = "log_file";

	public static final String ACTIVITY_LOG_FILE_KEY = "activity_log_file";

	public static final String SQL_LOG_FILE_KEY = "sql_log_file";

	private static final String RESOURCE_HANDLER_CLASS_NAME = "resource_handler_class_name";

	public static final String DB_TIMESTAMP_PATH = "db_timestamp_file";

	private String resourceHandlerClassName;

	private String theme = null;

	private ScriptAggregator scriptAggregator;

	public boolean customNavigation;

	private List<File> webDirectories;

	private CSSAggregator cssAggregator;

	private String loadingText;

	private HashSet<String> trustedHosts;
	
	public boolean urlNavigation = false;

	private long maxUploadFileSize = 50000000; // 50MB default for maximum upload file size

	public long getMaxUploadFileSize()
	{
		return maxUploadFileSize;
	}

	public void setMaxUploadFileSize(long maxUploadFileSize)
	{
		this.maxUploadFileSize = maxUploadFileSize;
	}

	protected RuntimeContext createContext()
	{
		RuntimeContext context = new WebContext();
		return context;
	}

	@SuppressWarnings("unchecked")
	protected void initContext(RuntimeContext context)
	{
		WebContext webContext = (WebContext) context;
		int contextNumber = ++numberOfContexts;
		webContext.setId("WebContext #" + contextNumber);
		super.initContext(webContext);
		webContext.setProperty(Config.DEV_MODE, debugMode);
		webContext.setDebug(isDebug());
		webContext.setTraceDirPath(getTraceDirPath());
		JavascriptGenerator codeGenerator = new JavascriptGenerator();
		codeGenerator.init(webContext);
		webContext.setCodeGenerator(codeGenerator);
		if (resourceHandlerClassName != null)
		{
			try
			{
				Class<IResourceHandler> c = (Class<IResourceHandler>) getClassLoader().loadClass(
						resourceHandlerClassName);
				webContext.setResourceHandler(c.newInstance());
			}
			catch (Exception e)
			{
				throw new EngineException("Could not instantiate resource handler "
						+ resourceHandlerClassName, e.getMessage(), e);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent event)
	{
		final ServletContext servletContext = event.getServletContext();
		initAndStartup(servletContext);

	}

	public void initAndStartup(final ServletContext servletContext)
	{
		servletContext.setAttribute(POOL_KEY, this);
		try
		{
			init(servletContext);
			if (debug)
				servletContext.log("Tersus Context Pool Initialized");
			startUp();
		}
		catch (Throwable e)
		{
			if (engineLog != null)
				engineLog.error("Startup failed", e);
			throw new EngineException("Startup failed", e.getMessage(), e);
		}
	}

	private void init(ServletContext servletContext)
	{

		if ("true".equalsIgnoreCase(servletContext
				.getInitParameter(DISABLE_SSL_CERTIFICATE_VALIDATION)))
		{
			disableSSLCertificateValidation();
		}

		this.setServletContext(servletContext);

		String repositoryRoot = servletContext.getInitParameter(Engine.REPOSITORY_ROOT_KEY);
		String projectRootStr = servletContext.getInitParameter(Engine.PROJECT_ROOT_KEY);
		setProjectRoot(projectRootStr == null ? null : new File(projectRootStr));

		(new LogConfig(this)).initLogging();
		String repositoryControllerClassName = servletContext
				.getInitParameter(REPOSITORY_CONTROLLER);
		String timestampFilePaths = servletContext.getInitParameter(TIMESTAMP_PATHS);
		String dbTimestampPath = servletContext.getInitParameter(DB_TIMESTAMP_PATH);
		resourceHandlerClassName = servletContext.getInitParameter(RESOURCE_HANDLER_CLASS_NAME);
		if (timestampFilePaths == null)
			timestampFilePaths = servletContext.getInitParameter(Engine.TIMESTAMP_FILE_KEY);
		String timestampStr = servletContext.getInitParameter(TIMESTAMP_KEY);

		setAutoCommitDataSources(servletContext.getInitParameter(AUTO_COMMIT_DATA_SOURCES));

		useQuotedIdentifiers("true".equals(servletContext
				.getInitParameter(USE_SQL_QUOTED_IDENTIFIERS)));
		if (timestampStr != null)
		{
			SimpleDateFormat dateAndTimeFormat = new SimpleDateFormat("yyyyMMddHHmm");
			try
			{
				Date timestamp = dateAndTimeFormat.parse(timestampStr);
				setFixedTimestamp(timestamp);
			}
			catch (ParseException e)
			{
				engineLog.error("Invalid time format for timestamp '" + timestampStr
						+ "'. The expected format is 'yyyyMMddHHmm'");
			}

		}
		if (dbTimestampPath != null)
			setDbTimestampFile(new File(dbTimestampPath));
		// backward
		// compatability
		String libraryPaths = servletContext.getInitParameter(Engine.MODEL_LIBRARIES_KEY);
		String classLoaderName = servletContext.getInitParameter(CLASS_LOADER_KEY);

		setInitDB(!("false".equals(servletContext.getInitParameter(INIT_DB))));
		setCheckDB(!isInitDB() && "true".equals(servletContext.getInitParameter(CHECK_DB)));
		setPreloadHandlers(!("false".equals(servletContext.getInitParameter(PRELOAD_HANDLERS))));

		Repository repository = null;
		if (repositoryControllerClassName == null)
			repositoryController = new DefaultRepositoryController();
		else
		{
			try
			{
				repositoryController = (IRepositoryController) Class.forName(
						repositoryControllerClassName).newInstance();
			}
			catch (Exception e)
			{
				throw new RuntimeException("Failed to instantiate repository controller", e);
			}
		}

		if (projectRoot != null)// If project root exists, there is no need to set repository root
		// in XML file.
		{
			if (repositoryRoot == null)
				repositoryRoot = projectRoot + "/models";

			String librariesPath = projectRoot + "/libraries";
			File libariesFile = new File(librariesPath);

			if (libariesFile.exists())
			{
				for (File file : libariesFile.listFiles())
				{
					String fileString = file.getPath().toString();
					if (fileString.endsWith(".tersus") || fileString.endsWith(".zip")
							|| fileString.endsWith(".trl"))
						libraryPaths = libraryPaths.concat(Repository.LIBARAY_PATH_SEPARATOR
								+ fileString);
				}
			}
		}

		if (repositoryRoot != null)
		{
			repository = repositoryController.createRepository(repositoryRoot, libraryPaths);
		}
		else
			repository = new WebRepository(servletContext);
		if (timestampFilePaths != null)
		{
			StringTokenizer tok = new StringTokenizer(timestampFilePaths, ";");
			timestampFiles = new File[tok.countTokens()];
			for (int i = 0; i < timestampFiles.length; i++)
			{
				timestampFiles[i] = new File(tok.nextToken());
			}
		}

		lastTimestamp = getTimestamp();
		boolean debug = Boolean.valueOf(servletContext.getInitParameter(Engine.DEBUG_KEY))
				.booleanValue();
		debugMode = Boolean.valueOf(servletContext.getInitParameter(Config.DEV_MODE));
		setDefaultTimeoutMilliseconds(1000 * getIntParameter(servletContext, TIMEOUT_KEY,
				DEFAULT_TIMEOUT_MILLISECONDS / 1000));
		String rootId = servletContext.getInitParameter(Engine.ROOT_SYSTEM_KEY);
		String traceDirPath = servletContext.getInitParameter(Engine.TRACE_DIR_KEY);
		if (traceDirPath == null)
		{
			traceDirPath = servletContext.getServletContextName() + "_Trace";
		}
		Enumeration<?> names = servletContext.getInitParameterNames();
		while (names.hasMoreElements())
		{
			String name = (String) names.nextElement();
			String value = servletContext.getInitParameter(name);
			setProperty(name, value);
		}
		setContextLeakCleanupInterval(getIntParameter(servletContext,
				CONTEXT_LEAK_CLEANUP_INTERVAL, getContextLeakCleanupInterval()));
		initPermissions(servletContext);
		init(repository, rootId, traceDirPath, debug);

		boolean allowAppSpecificPlugins = isAllowAppSpecificPlugins(projectRoot);
		validateApplicationSpecificPlugins(allowAppSpecificPlugins, projectRoot);
		initClassLoader(projectRoot, classLoaderName, allowAppSpecificPlugins);

		String databaseAdaptersStr = servletContext.getInitParameter(DATABASE_ADAPTERS_KEY);
		initDBAdapters(databaseAdaptersStr);
		String trustedHostsStr = servletContext.getInitParameter(TRUSTED_HOSTS_KEY);
		if (trustedHostsStr == null)
			trustedHostsStr = "127.0.0.1,0:0:0:0:0:0:0:1%0,0:0:0:0:0:0:0:1,fe80:0:0:0:0:0:0:1%1";

		initTrustedHosts(trustedHostsStr);
//		urlNavigation = Boolean.valueOf(servletContext.getInitParameter(URL_NAVIGATION_KEY));
		String traceMaxStringLengthStr = servletContext
				.getInitParameter(TRACE_MAX_STRING_LENGTH_KEY);
		setTraceMaxStringLength(traceMaxStringLengthStr == null ? 0 : Integer
				.parseInt(traceMaxStringLengthStr));

		String maxUploadFileSizeStr = servletContext.getInitParameter(MAX_UPLOAD_FILE_SIZE_KEY);
		if (maxUploadFileSizeStr != null)
			setMaxUploadFileSize(Long.parseLong(maxUploadFileSizeStr));
		initTheme();
		customNavigation = "true".equals(getRootModel().getProperty(
				BuiltinProperties.CUSTOM_NAVIGATION));
		urlNavigation = "true".equals(getRootModel().getProperty(
				BuiltinProperties.URL_NAVIGATION));
		initWebDirectories();
		initAggregators();
	}

	protected void disableSSLCertificateValidation()
	{
		TrustManager[] nullTrustManager = new TrustManager[]
		{ new X509TrustManager()
		{
			public java.security.cert.X509Certificate[] getAcceptedIssuers()
			{
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String authType)
			{
			}

			public void checkServerTrusted(X509Certificate[] certs, String authType)
			{
			}
		} };

		HostnameVerifier nullVerifier = new HostnameVerifier()
		{
			public boolean verify(String hostname, SSLSession session)
			{
				return true;
			}
		};
		try
		{
			HttpsURLConnection.setDefaultHostnameVerifier(nullVerifier);
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, nullTrustManager, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		}
		catch (NoSuchAlgorithmException e)
		{
			engineLog.error("Error disabling SSL certificate validation", e);
		}
		catch (KeyManagementException e)
		{
			engineLog.error("Error disabling SSL certificate validation", e);
		}
	}

	private boolean isAllowAppSpecificPlugins(File projectRoot)
	{
		String allowAppSpecificPluginsString = servletContext
				.getInitParameter(Engine.ALLOW_APPLICATION_SPECIFIC_PLUGINS);
		return allowAppSpecificPluginsString != null ? Boolean
				.parseBoolean(allowAppSpecificPluginsString) : true;
	}

	private void validateApplicationSpecificPlugins(boolean allowAppSpecificPlugins,
			File projectRoot)
	{
		if (!allowAppSpecificPlugins && projectRoot != null)
		{
			String binPath = projectRoot + "/bin";
			File binFolder = new File(binPath);

			if (binFolder.exists())
				engineLog
						.warn("Application specific plugins are exist in bin folder but they are not allowed to use.");

			String jarsPath = projectRoot + "/bin";
			File jarsFolder = new File(jarsPath);

			if (jarsFolder.exists())
				engineLog
						.warn("Application specific plugins are exist in jars folder but they are not allowed to use.");

			String librariesPath = projectRoot + "/libraries";
			File librariesFolder = new File(librariesPath);

			if (librariesFolder.exists())
			{
				for (File file : librariesFolder.listFiles())
				{
					String fileString = file.getPath().toString();
					if (fileString.endsWith(".tersus") || fileString.endsWith(".zip"))
					{
						ZipFile zipFile = null;
						try
						{
							zipFile = new ZipFile(file);
						}
						catch (Exception e)
						{
							engineLog.warn("Fialed to create zip file: " + file.getPath());
						}

						String fullFileName = file.getName();
						int pointIndex = fullFileName.indexOf('.');

						if (pointIndex != -1)
						{
							String binPrefixFileName = fullFileName.substring(0, pointIndex)
									+ "/bin";
							String jarsPrefixFileName = fullFileName.substring(0, pointIndex)
									+ "/jars";
							Enumeration<?> entries = zipFile.entries();

							while (entries.hasMoreElements())
							{
								ZipEntry entry = (ZipEntry) entries.nextElement();
								String fullPathName = entry.getName();
								if (fullPathName.startsWith(binPrefixFileName)
										|| fullPathName.startsWith(jarsPrefixFileName))
									engineLog
											.warn("Application specific plugins are exist in libraries folder but they are not allowed to use."
													+ "\nPath: " + zipFile.getName());
							}
						}
					}
				}
			}
		}
	}

	private void initClassLoader(File projectRoot, String classLoaderName,
			boolean allowApplicationSpecificPlugins)
	{
		ClassLoader platformClassLoader = null;

		if (classLoaderName != null)
		{
			try
			{
				platformClassLoader = (ClassLoader) Class.forName(classLoaderName).newInstance();
			}
			catch (Exception e)
			{
				throw new RuntimeException("Failed to initialize plugin loader '" + classLoaderName
						+ "'", e);
			}
		}
		if (platformClassLoader == null)
			platformClassLoader = this.getClassLoader();

		ClassLoader pluginLoader;

		if (allowApplicationSpecificPlugins)
			pluginLoader = getPluginLoader(projectRoot, platformClassLoader);
		else
			pluginLoader = platformClassLoader;

		if (pluginLoader != null)
			setClassLoader(pluginLoader);
	}

	public static ClassLoader getPluginLoader(File projectRoot, ClassLoader platformClassLoader)
	{
		ClassLoader pluginLoader = null;
		if (projectRoot == null)
			pluginLoader = platformClassLoader;
		else
		{

			System.setProperty("jcl.isolateLogging", "false");
			JarClassLoader jcl = new JarClassLoader();
			pluginLoader = jcl;
			final ClassLoaderWrapper baseLoader = new ClassLoaderWrapper(platformClassLoader);
			jcl.addLoader(new ProxyClassLoader()
			{

				@Override
				public Class<?> loadClass(String className, boolean resolveIt)
				{
					try
					{
						return baseLoader.loadClass(className, resolveIt);
					}
					catch (ClassNotFoundException e)
					{
						return null;
					}
				}

				@Override
				public InputStream loadResource(String name)
				{
					return baseLoader.getResourceAsStream(name);
				}

				@Override
				public URL findResource(String name)
				{
					// TODO Auto-generated method stub
					return baseLoader.getResource(name);
				}
			});
			addProjectFolder(jcl, projectRoot);
			File classloaderFolder = new File(projectRoot, "classloader");

			FileUtils.delete(classloaderFolder);
			File librariesFolder = new File(projectRoot, "libraries");
			if (librariesFolder.isDirectory())
				for (File libraryFile : librariesFolder.listFiles())
				{
					if (libraryFile.getName().endsWith(".tersus"))
						addLibrary(jcl, libraryFile, classloaderFolder);
				}

		}
		return pluginLoader;
	}

	private static void addProjectFolder(JarClassLoader jcl, File rootFolder)
	{
		File binFolder = new File(rootFolder, "bin");
		File pluginXML = new File(rootFolder, "plugin.xml");
		if (binFolder.isDirectory() && !pluginXML.exists()) // If project contains plugin.xml we
															// assume that relevant code is loaded
															// through Eclipse mechanisms
			jcl.add(binFolder.getAbsolutePath());
		File jarsFolder = new File(rootFolder, "jars");
		if (jarsFolder.isDirectory())
		{
			for (File jarFile : jarsFolder.listFiles())
			{
				if (jarFile.getName().endsWith(".jar"))
					jcl.add(jarFile.getAbsolutePath());
			}
		}
	}

	private static void addLibrary(JarClassLoader loader, File libraryFile, File classloaderFolder)
	{
		try
		{
			JarFile jarFile;
			jarFile = new JarFile(libraryFile);
			JarEntry firstEntry = jarFile.entries().nextElement();

			FileUtils.unzip(jarFile, classloaderFolder, new EntryFilter()
			{

				public boolean accept(String entryName)
				{
					return entryName.indexOf("/classes/") > 0 || entryName.indexOf("/jars/") > 0
							|| entryName.indexOf("/bin/") > 0 || entryName.indexOf("/lib/") > 0;
				}
			});
			String baseName = firstEntry.getName().split("/")[0];
			addProjectFolder(loader, new File(classloaderFolder, baseName));
		}
		catch (Exception e)
		{
			throw new RuntimeException("Failed to load library " + libraryFile.getAbsolutePath(), e);
		}

	}

	private void initPermissions(ServletContext servletContext)
	{
		String ds = servletContext.getInitParameter(PERMISSIONS_DATA_SOURCE_KEY);
		if (ds != null)
			setPermissionsDataSource(ds);
		String query = servletContext.getInitParameter(PERMISSIONS_QUERY_KEY);
		if (query != null)
			setPermissionsQuery(query);
		setPermissionsLastUpdateQuery(servletContext
				.getInitParameter(PERMISSIONS_LAST_UPDATE_QUERY_KEY));

	}

	private void initTrustedHosts(String trustedHostsStr)
	{
		trustedHosts = new HashSet<String>();
		for (String hostname : trustedHostsStr.split(","))
		{
			trustedHosts.add(hostname);
		}

	}

	private void initTheme()
	{
		setTheme((String) getRootModel().getProperty(BuiltinProperties.THEME));
	}

	private int getIntParameter(ServletContext servletContext, String parameterName,
			int defaultValue)
	{
		int intValue = defaultValue;
		String s = servletContext.getInitParameter(parameterName);
		if (s != null)
		{
			try
			{
				intValue = Integer.parseInt(s);
			}
			catch (NumberFormatException e)
			{
				throw new RuntimeException("The value '" + s
						+ "' is not valid for context parameter '" + parameterName + "'");
			}
		}
		return intValue;
	}

	private void initAggregators()
	{
		scriptAggregator = new ScriptAggregator(getWebDirectories(),new File(getProjectRoot(), ProjectStructure.SCRIPT_EXCLUDE));
		cssAggregator = new CSSAggregator(getWebDirectories(), new File(getProjectRoot(), ProjectStructure.STYLE_EXCLUDE));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent event)
	{
		try
		{
			shutdown();
		}
		catch (Exception e)
		{
			error("Shutdown failed", e);
		}
		if (engineLog.isInfoEnabled())
			engineLog.info("Context destroyed");
		@SuppressWarnings("rawtypes")
		/** Not sure why we would need to close appenders explicitly */
		Enumeration appenders = getLogHierarchy().getRootLogger().getAllAppenders();
		while (appenders.hasMoreElements())
		{
			Appender appender = (Appender) appenders.nextElement();
			try
			{
				appender.close();
			}
			catch (Throwable e)
			{
				e.printStackTrace();
			}
		}
	}

	public RuntimeContext getContext()
	{
		WebContext context = (WebContext) super.getContext();
		context.setRequest(null);
		context.setResponse(null);
		context.setSession(null);
		return context;

	}

	public void sessionCreated(HttpSessionEvent se)
	{
		/*
		 * We ignore this event because we still don't have the IP address - using newSession
		 * instead.
		 */

	}

	static final InvocationAdapter sessionInvoker = new InvocationAdapter()
	{

		public String getLogType()
		{
			return "SessionEvent";
		}

		public void setTriggers(RuntimeContext context, FlowInstance serviceInstance,
				SlotHandler[] triggers)
		{
			// Nothing to do (no trigger values)
		}

		public boolean needsAccessValidation()
		{
			return false;
		}
	};

	/*
	 * protected void sessionEvent(Role event, HttpSession session) { WebContext context = null; try
	 * { List<ModelId> handlerIds = getEventHandlers(event); if (handlerIds.isEmpty()) return; for
	 * (ModelId handlerId : handlerIds) { context = (WebContext) getContext();
	 * context.connect(null); context.setRoot(true); context.setSession(session);
	 * context.runProcess(handlerId.toString(), "", null, sessionInvoker); poolContext(context);
	 * context = null; } } catch (Exception e) { //Nothing to do - error already reported }
	 * 
	 * }
	 */

	protected void sessionEvent(Role event, HttpSession session)
	{
		WebContext context = null;
		List<ModelId> handlerIds = getEventHandlers(event);
		if (handlerIds.isEmpty())
			return;
		for (ModelId handlerId : handlerIds)
		{
			LoggingHelper.get().startRequest(activityLog, engineLog, null);
			try
			{
				context = (WebContext) getContext();
				context.connect(null);
				context.setRoot(true);
				context.setSession(session);
				context.runProcess(handlerId.toString(), "", null, sessionInvoker);
				poolContext(context);
				context = null;
				LoggingHelper.get().endRequest();
			}
			catch (Throwable e)
			{
				LoggingHelper.get().endRequest(e);
				if (e instanceof Error)
					throw (Error) e;
			}
			finally
			{
				if (context != null)
				{
					context.dispose();
					context = null;
				}
				LoggingHelper.dispose();
			}
		}

	}

	private void newSession(HttpSessionEvent se)
	{
		sessionEvent(ON_NEW_SESSION, se.getSession());
	}

	public void sessionDestroyed(HttpSessionEvent se)
	{
		if (Boolean.TRUE.equals(se.getSession().getAttribute(LOGGED_OUT)))
			sessionEvent(ON_LOGOUT, se.getSession());
		else
			sessionEvent(ON_TIMEOUT, se.getSession());
	}

	public void attributeAdded(HttpSessionBindingEvent e)
	{
		checkSessionAttributes(e);
	}

	private void checkSessionAttributes(HttpSessionBindingEvent e)
	{
		if (REMOTE_ADDR.equals(e.getName()))
			newSession(e);
		if (REMOTE_USER.equals(e.getName()))
		{
			sessionEvent(ON_LOGIN, e.getSession());
		}
	}

	public void attributeRemoved(HttpSessionBindingEvent e)
	{
	}

	public void attributeReplaced(HttpSessionBindingEvent e)
	{
		checkSessionAttributes(e);
	}

	protected void setServletContext(ServletContext servletContext)
	{
		this.servletContext = servletContext;
	}

	public ServletContext getServletContext()
	{
		return servletContext;
	}

	protected void reloadRepository()
	{
		repositoryController.reload(getRepository());
	}

	@Override
	public void reload()
	{
		super.reload();
		loadingText = null;
		initTheme();
		initWebDirectories();
		initAggregators();
	}

	protected void initWebDirectories()
	{
		String pathList = getServletContext().getInitParameter(Config.LOCAL_DIRS_KEY);
		ArrayList<File> baseDirectories = new ArrayList<File>();
		if (pathList == null)
		{
			engineLog.warn("Missing parameter " + Config.LOCAL_DIRS_KEY);
		}
		else
		{
			for (String path : pathList.split("[;,]"))
			{
				File dir = new File(path);
				if (dir.isDirectory())
					baseDirectories.add(dir);
			}
		}
		File rootDir = getRootDir();
		baseDirectories.add(rootDir);

		String theme = getTheme();
		webDirectories = new ArrayList<File>();
		if (theme != null)
		{
			String themePath = "themes/" + theme;
			for (File dir : baseDirectories)
			{
				File themeDir = new File(dir, themePath);
				if (themeDir.isDirectory())
					webDirectories.add(themeDir);
			}
		}
		webDirectories.addAll(baseDirectories);
	}

	public File getRootDir()
	{
		File rootDir = (new File(getServletContext().getRealPath("logo.gif"))).getParentFile();
		return rootDir;
	}

	public List<File> getWebDirectories()
	{
		return webDirectories;
	}

	public File getWebFile(String path)
	{
		for (File dir : getWebDirectories())
		{
			File file = new File(dir, path);
			if (file.isFile())
				return file;
		}
		return null;
	}

	public ScriptAggregator getScriptAggregator()
	{
		return scriptAggregator;
	}

	public CSSAggregator getCSSAggregator()
	{
		return cssAggregator;
	}

	public String getLoadingText() throws FileNotFoundException
	{
		if (loadingText == null || isDebugMode())
		{
			File loadingTextFile = getWebFile("loading.txt");
			if (loadingTextFile != null)
				loadingText = FileUtils.readString(new FileInputStream(loadingTextFile),
						Engine.ENCODING, true);
			else
				loadingText = "Loading ...";
		}
		return loadingText;
	}

	public String getTheme()
	{
		return theme;
	}

	public void setTheme(String theme)
	{
		this.theme = theme;
	}

	public boolean isTrustedAddress(String remoteAddr)
	{
		if (trustedHosts.contains(remoteAddr) || trustedHosts.contains("*"))
			return true;
		else
		{
			engineLog.info("Address not trusted:" + remoteAddr);
			return false;
		}
	}

	public File getProjectRoot()
	{
		return projectRoot;
	}

	public void setProjectRoot(File projectRoot)
	{
		this.projectRoot = projectRoot;
	}

}
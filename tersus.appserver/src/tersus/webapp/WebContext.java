/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.webapp;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import tersus.runtime.RuntimeContext;
import tersus.webapp.jsloader.JavascriptGenerator;

/**
 * NICE2 Remove this class, replacing it by a Map of named objects on the
 * RuntimeContext
 * 
 * @author Youval Bronicki
 * 
 */
public class WebContext extends RuntimeContext
{
    private JavascriptGenerator codeGenerator;

    private HttpServletRequest request;

    private HttpServletResponse response;

    private HttpSession session;

    private String traceDirPath;

    // per request
    private String id;

    // processed;

    private String userRequestID; // The id of the request as assigned by the

    // client

    private boolean debug;

    /**
     * Indicates whether an output page was generated by the engine. (If no
     * output is generated, WebHandler creates a default output page which
     * closes the popup window
     */
    private boolean isOutputGenerated;

	/**
     * @return
     */
    public HttpServletRequest getRequest()
    {
        return request;
    }

    /**
     * @return
     */
    public HttpServletResponse getResponse()
    {
        return response;
    }

    /**
     * @param request
     */
    public void setRequest(HttpServletRequest request)
    {
        this.request = request;
        if (request != null)
            setUserRequestID(request.getParameter("_userRequestId"));
    }

    /**
     * @param response
     */
    public void setResponse(HttpServletResponse response)
    {
        this.response = response;
    }

    /**
     * @return
     */
    public boolean isOutputGenerated()
    {
        return isOutputGenerated;
    }

    /**
     * @param b
     */
    public void setOutputGenerated(boolean b)
    {
        isOutputGenerated = b;
    }

    public String getLoggedInUser()
    {
        String user = null;
        if (session != null)
        {
        	try
        	{
        		user = (String) session.getAttribute(ContextPool.REMOTE_USER);
        	}
        	catch (IllegalStateException e)
        	{
        		user = null;
        	}
        }
        else if (isRoot())
        {
        	user = getRootUser();
        	if (user == null)
        		user = "System";
        }
        return user;
    }

    /**
     * @param codeGenerator
     */
    public void setCodeGenerator(JavascriptGenerator codeGenerator)
    {
        this.codeGenerator = codeGenerator;
    }

    /**
     * @return
     */
    public JavascriptGenerator getCodeGenerator()
    {
        return codeGenerator;
    }

    /**
     * @return
     */
    public String getId()
    {
        return id;
    }

    /**
     * @param i
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    public String toString()
    {
        return id;
    }


    /**
     * @return
     */
    public boolean isDebug()
    {
        return debug;
    }

    /**
     * @param b
     */
    public void setDebug(boolean b)
    {
        debug = b;
    }

    /**
     * @return
     */
    public String getTraceDirPath()
    {
        return traceDirPath;
    }

    /**
     * @param string
     */
    public void setTraceDirPath(String string)
    {
        traceDirPath = string;
    }

    

    /**
     * @return the time when the models were last modified.
     */
    public long getLastModified()
    {
        return Math.max(getContextPool().getTimestamp(), getLastPermissionUpdate());
    }


    public void checkAborted()
    {
        super.checkAborted();
        checkAbortRequest();
    }

    protected void checkAbortRequest()
    {
        if (getRequest() != null)
        {
            HttpSession session = getRequest().getSession();
            synchronized (session)
            {
                if (Boolean.TRUE.equals(session.getAttribute("ABORT_"
                        + getUserRequestID())))
                    abort("Operation aborted by user");
            }
        }
    }

    public String getUserRequestID()
    {
        return userRequestID;
    }

    public void setUserRequestID(String userRequestID)
    {
        this.userRequestID = userRequestID;
    }

    public HttpSession getSession()
    {
        return session;
    }

    public void setSession(HttpSession session)
    {
        this.session = session;
    }
    
    public boolean isSecure()
    {
    	if (request != null)
    		return request.isSecure();
    	else
    		return true; // No request = no client - server = no need for encryption
    }
}

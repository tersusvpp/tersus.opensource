/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.??? Initial API and implementation
 *************************************************************************************************/

package tersus.webapp;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;

import tersus.InternalErrorException;
import tersus.model.ModelId;
import tersus.runtime.CompositeDataHandler;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.ModelIdHelper;
import tersus.runtime.RuntimeContext;
import tersus.util.InvalidInputException;
import tersus.util.Misc;

/**
 * A Utility Class used to parse and serialize data values using a JavaScript
 * format
 * 
 * @author Youval Bronicki
 *  
 */
public class JavaScriptFormat
{
    private static final boolean DEBUG = false;

    private static class Tokens
    {
        static final char START_OBJECT = '{';

        static final char END_OBJECT = '}';

        static final char START_ARRAY = '[';

        static final char END_ARRAY = ']';

        static final char SEPARATOR = ',';

        static final char OBJECT_SEPARATOR = ',';

        static final char ARRAY_SEPARATOR = ',';

        static final char ASSIGN = ':';

        static final char QUOTE = '"';

        static final char BACKSLASH = '\\';
    };

    /**
     * Create a compact textual serialization of a data value
     * 
     * @param value
     *            The data value
     * @param handler
     *            A <code>DataHandler</code> that represents the data type
     * @param buffer
     *            A <code>StringBuffer</code> to which the serialization will
     *            be appended
     */
    @SuppressWarnings("unchecked")
	public static void serialize(Object value, InstanceHandler handler,
            Writer buffer, RuntimeContext context, boolean forceDefaultFormats)
    {
    	try
    	{
    		serialize0(value, handler, buffer, context, forceDefaultFormats, true, new ArrayList<Object>(),0, false);
    	}	
		catch (IOException e)
		{
			throw new InternalErrorException("Error escaping string",e);
		}
    }
	public static void serialize(Object value, InstanceHandler handler,
            Writer buffer, RuntimeContext context, boolean forceDefaultFormats, boolean addWhitespace, boolean hideSecretData)
    {
    	try
    	{
    		serialize0(value, handler, buffer, context, forceDefaultFormats, addWhitespace, new ArrayList<Object>(), context.getContextPool().getTraceMaxStringLength(), hideSecretData);
    	}	
		catch (IOException e)
		{
			throw new InternalErrorException("Error escaping string",e);
		}
    }
	private static void serialize0(Object value, InstanceHandler handler,
            Writer buffer, RuntimeContext context, boolean forceDefaultFormats, boolean addWhitespace, ArrayList<Object> stack, int maxStringLength, boolean hideSecretData) throws IOException
    {
        if (value instanceof List)
        {
            List<Object> values = (List<Object>) value;
            int nValues = values.size();
            buffer.append(Tokens.START_ARRAY);
            for (int i = 0; i < nValues; i++)
            {
                if (i > 0)
                {
                    buffer.append(Tokens.ARRAY_SEPARATOR);
                    if (addWhitespace)
                    	buffer.append('\n');
                }
                serialize0(values.get(i), handler, buffer, context, forceDefaultFormats, addWhitespace, stack, maxStringLength, hideSecretData);
            }
            buffer.append(Tokens.END_ARRAY);
            return;
        }
/* preparation for marshalling Anything
        if (handler instanceof AnythingHandler)
        {
        	ModelId actualId = ModelIdHelper.getModelId(value);
        	InstanceHandler actualHandler = context.getModelLoader().getHandler(actualId);
        	buffer.append(Tokens.START_OBJECT);
        	buffer.append(Tokens.MODEL_ID);
        	buffer.append(Tokens.ASSIGN);
            buffer.append(Tokens.QUOTE);
            appendEscaped(buffer, actualId.toString());
            buffer.append(Tokens.QUOTE);
            buffer.append(Tokens.SEPARATOR);
            buffer.append(Tokens.VALUE);
            buffer.append(Tokens.ASSIGN);
            serialize(value, actualHandler, buffer, context, forceDefaultFormats);
        	buffer.append(Tokens.END_OBJECT);
        	return;
        	
        }*/
        if (handler == null)
        {
            ModelId modelId = ModelIdHelper.getCompositeModelId(value);
            if (modelId != null)
                handler = context.getModelLoader().getHandler(modelId);
        }

        if (handler == null)
        {
            if (value instanceof FlowInstance)
                handler = ((FlowInstance) value).getHandler();
            
        }

        if (hideSecretData && handler.isSecret())
        {
        	buffer.append("'*****'");
        	return;
        }
        if (handler instanceof LeafDataHandler || handler == null)
        {

        	if ((value instanceof String) && (maxStringLength > 0) && (((String) value).length() > maxStringLength))
        	{
        		buffer.append("'** Long String ("+((String) value).length()+") **'");
        		return;
        	}
            StringBuffer valueStr = new StringBuffer();
            if (handler != null)
                 ((LeafDataHandler) handler).format(context, value, null, valueStr, forceDefaultFormats);
            else
                valueStr.append(value);
            if (value instanceof tersus.runtime.Number
                    || value instanceof Boolean)
                buffer.append(valueStr);
            else
            {
                buffer.append(Tokens.QUOTE);
                appendEscaped(buffer, valueStr);
                buffer.append(Tokens.QUOTE);
            }
            return;
        }
        //		if (handler.isPersistent() && value != null)
        //		{
        //			//FUNC3 use full serialization for non-reference values.
        //			String referenceString = handler.getReferenceString(context, value);
        //			if (referenceString != null)
        //			{
        //				appendQuoted(buffer, referenceString);
        //				return;
        //			}
        //		}
        if (Misc.ASSERTIONS)
            Misc.assertion(handler instanceof CompositeDataHandler
                    || handler instanceof FlowHandler);
        int elementValueCount = 0;
        for (int i = stack.size()-1;i>=0; i--)
        {
        	if (value == stack.get(i))
        	{
        		buffer.append("'RECURSIVE STUCTURE ["+(i-stack.size())+"]'");
        		return;
        	}
        }
        stack.add(value);
        buffer.append(Tokens.START_OBJECT);
        for (int i = 0; i < handler.elementHandlers.length; i++)
        {
            ElementHandler elementHandler = handler.elementHandlers[i];
            Object elementValue = elementHandler.get(context, value);
            if (elementValue == null)
                continue;
            if (elementHandler.isRepetitive())
            {
                List values = (List) elementValue;
                if (values.size() > 0)
                {
                    elementValueCount++;
                    if (elementValueCount > 1)
                        buffer.append(Tokens.OBJECT_SEPARATOR);
                    appendQuoted(buffer, elementHandler.getRole().toString());
                    buffer.append(Tokens.ASSIGN);
                    serialize0(values, elementHandler.getChildInstanceHandler(),
                            buffer, context, forceDefaultFormats, addWhitespace, stack, maxStringLength, hideSecretData);
                }
            }
            else
            {
                elementValueCount++;
                if (elementValueCount > 1)
                    buffer.append(Tokens.OBJECT_SEPARATOR);
                appendQuoted(buffer, elementHandler.getRole().toString());
                buffer.append(Tokens.ASSIGN);
                serialize0(elementValue, elementHandler
                        .getChildInstanceHandler(), buffer, context, forceDefaultFormats,addWhitespace, stack, maxStringLength, hideSecretData);
            }
        }
        buffer.append(Tokens.END_OBJECT);
        stack.remove(stack.size()-1);
    }

    private static void appendQuoted(Writer out, String string) throws IOException
    {
        out.write(Tokens.QUOTE);
        appendEscaped(out, string);
        out.write(Tokens.QUOTE);
    }

    /**
     * Appends a String to a StringBuffer replacing special characters with
     * escape sequences
     * 
     * @param buffer
     *            The target StringBuffer
     * @param s
     *            The String to be appended
     */
    private static void appendEscaped(Writer out, CharSequence s) throws IOException
    {
    		StringEscapeUtils.ESCAPE_JAVA.translate(s,out);
    }
    public static StringBuffer escape(CharSequence s)
    {
    	try
    	{
    		StringWriter out = new StringWriter();
    		StringEscapeUtils.ESCAPE_JAVA.translate(s,out);
    		return out.getBuffer();
    	}
    	catch (IOException e)
    	{
			throw new InternalErrorException("Error escaping string",e);
    	}
    }


    private String serialization;

    private int end;

    private int offset;

    private boolean inList;

    private InstanceHandler rootHandler;

    private RuntimeContext context;

    private StringBuffer textBuffer = new StringBuffer();

    private ArrayList<Object> instanceStack = new ArrayList<Object>();

    private ArrayList<Object> elementStack = new ArrayList<Object>();

    private Object currentInstance;
    private boolean readingContent = false;

    private ElementHandler currentElement;

	private boolean ignoreUnknownElements;

	private boolean forceDefaultFormats;

    /**
     * @param context
     * @param handler
     * @param valueStr
     * @param ignoreUnknownElements TODO
     * @return
     */
    public static Object parseValue(RuntimeContext context,
            InstanceHandler handler, String valueStr, boolean ignoreUnknownElements, boolean forceDefaultFormats)
    {
        JavaScriptFormat parser = new JavaScriptFormat();
        parser.end = valueStr.length();
        parser.offset = 0;
        parser.serialization = valueStr;
        parser.rootHandler = handler;
        parser.context = context;
        parser.currentInstance = null;
        parser.ignoreUnknownElements = ignoreUnknownElements;
        parser.forceDefaultFormats = forceDefaultFormats;
        parser.parse();
        return parser.currentInstance;
    }

    @SuppressWarnings("unchecked")
	public static List<Object> parseList(RuntimeContext context,
            InstanceHandler handler, String buffer, boolean ignoreUnknownElements, boolean forceDefaultFormat)
    {
        JavaScriptFormat parser = new JavaScriptFormat();
        parser.end = buffer.length();
        parser.offset = 0;
        parser.serialization = buffer;
        parser.rootHandler = handler;
        parser.context = context;
        parser.currentInstance = new ArrayList<Object>();
        parser.ignoreUnknownElements = ignoreUnknownElements;
        parser.forceDefaultFormats = forceDefaultFormat;
        parser.parse();
        return (List<Object>) parser.currentInstance;
    }

    private void parse()
    {
    	if (rootHandler instanceof LeafDataHandler)
    		readingContent = true;
        textBuffer.setLength(0);
        boolean quote = false;
        StringWriter unescapedString;
        StringBuffer escapedString = new StringBuffer();
        char c = 0, p;
        while (offset < this.end)
        {
        	p = c;
        	c = nextChar();
            if (quote)
            {
                if (c == Tokens.QUOTE && p != Tokens.BACKSLASH)
                {
            		quote = false;
            		try
            		{
                		unescapedString = new StringWriter();
            			StringEscapeUtils.UNESCAPE_JAVA.translate(escapedString, unescapedString);
                	}
                	catch (IOException e)
                	{
            			throw new InternalErrorException("Error unescaping string",e);
                	}
            		textBuffer.append(unescapedString);
                }
                else
                {
                	escapedString.append(c);
                	if (c == Tokens.BACKSLASH && p == Tokens.BACKSLASH)
                		c = 0;
                }
            }
            else
            {
            	if (Character.isWhitespace(c))
            		continue;
                switch (c)
                {
                    case Tokens.START_ARRAY:
                        startList();
                        break;
                    case Tokens.END_ARRAY:
                       	if (readingContent)
                    		endElement();
                        endList();
                        readingContent=false;
                        break;
                    case Tokens.START_OBJECT:
                        startComposite();
                        readingContent=false;
                        break;
                    case Tokens.END_OBJECT:
                    	if (readingContent)
                    		endElement();
                        endComposite();
                        readingContent = false;
                        break;
                    case Tokens.QUOTE:
                        textBuffer.setLength(0); // Ignore whitespace text that comes before the quotes ** Can cause strange result in case of malformed JSON
                        escapedString.setLength(0);
                        quote = true;
                        break;
                    case Tokens.ASSIGN:
                        startElement();
                        readingContent= true;
                        break;
                    case Tokens.SEPARATOR:
                    	if (readingContent)
                    		endElement(); 
                        break;
                    default:
                        getChar(c);
                }
            }
        }
        end();
    }

    /**
     *  
     */
    private void end()
    {
        if (readingContent)
        	endText();
    }

    /**
     * @param c
     */
    private void getChar(char c)
    {
        textBuffer.append(c);
    }

    /**
     *  
     */
    private void endElement()
    {
        endText();
        if (!inList) // This is the end of an element of a composite value
        {
            popElement();
        }
    }

    /**
     *  
     */
    private void popElement()
    {
        if (DEBUG)
            debug("End element " + (currentElement!= null ? currentElement.getRole() : "null"));
        if (elementStack.isEmpty())
            currentElement = null;
        else
            currentElement = (ElementHandler) elementStack.remove(elementStack
                    .size() - 1);
    }

    /**
     *  
     */
    
    private void startElement()
    {
        InstanceHandler handler = currentElement != null ? currentElement
                .getChildInstanceHandler() : rootHandler;
        ElementHandler childElement = handler.getElementHandler(textBuffer
                .toString());
        if (childElement == null)
        {
        	if (! ignoreUnknownElements)
        		error("Unknown element '" + textBuffer + "' in " + handler);
        	else
        	{
        		childElement = new UnknownElementHandler(handler);
        	}
        	
        }
        pushElement(childElement);
        textBuffer.setLength(0);
        inList=false;
    }

    private void pushElement(ElementHandler childElement)
    {
        if (DEBUG)
            debug("Start Element " + childElement.getRole());
        elementStack.add(currentElement);
        currentElement = childElement;
    }

    @SuppressWarnings("unchecked")
	private void startComposite()
    {
        Object newInstance;
        if (currentElement == null)
        {
            newInstance = rootHandler.newInstance(context);
            if (currentInstance instanceof ArrayList)
                ((ArrayList<Object>) currentInstance).add(newInstance);
            else
                currentInstance = newInstance;
        }
        else
            newInstance = currentElement.create(context, currentInstance);
        pushInstance(newInstance);
        inList = false;
    }

    /**
     * @param childInstance
     */
    private void pushInstance(Object childInstance)
    {
        if (DEBUG)
        {
            debug("Push current instance was " + getString(currentInstance));
            debug(" Current instance= " + getString(childInstance));
        }
        instanceStack.add(currentInstance);
        currentInstance = childInstance;
    }

    /**
     *  
     */
    private void endComposite()
    {
    	/** We're at the end of an object:
    	 * 	We need to distinguish between the case where we're ending an item in a list (popInstance only),
    	 *  and the case where we're not in a list (popInstance and popElement)
    	 *  
    	 */
    	if (currentElement != null && currentElement.getChildInstanceHandler().isInstance(currentInstance) && currentElement.isRepetitive())
    	{
    		popInstance();
    	}
    	else
    	{
    		popElement();
    		popInstance();
    	}
   		inList = currentElement == null ? currentInstance instanceof List :  currentElement.isRepetitive();
    }

    /**
     *  
     */
    @SuppressWarnings("unchecked")
	private void endText()
    {
        if (currentElement != null
                && currentElement.getChildInstanceHandler() instanceof LeafDataHandler)
            currentElement.create(context, currentInstance, textBuffer
                    .toString(), forceDefaultFormats); // Not using element properties becasue we want to always use defualt formats in JSON
        else if (rootHandler instanceof LeafDataHandler)
        // The root instance
        {
            Object newInstance = rootHandler.newInstance(context, textBuffer
                    .toString(), null, forceDefaultFormats);
            if (currentInstance instanceof ArrayList)
            {
                ((ArrayList<Object>) currentInstance).add(newInstance);
            }
            else
            {
                currentInstance = newInstance;
            }
        }
        textBuffer.setLength(0);
    }

    /**
     *  
     */
    private void popInstance()
    {
        if (DEBUG)
        {
            debug("Pop current instance was " + getString(currentInstance));
            debug("  Last char was '" + serialization.charAt(offset - 1)
                    + "' offset=" + offset + " text = "
                    + serialization.substring(0, offset));
        }
        currentInstance = instanceStack.remove(instanceStack.size() - 1);
        if (DEBUG)
            debug("  Current instance= " + getString(currentInstance));
    }

    /**
     *  
     */
    private void endList()
    {
        inList = false;
        popElement();
    }

    /**
     *  
     */
    private void startList()
    {
        if (currentElement != null && !currentElement.isRepetitive())
            error("Found a list where a non-repetitive element was expected");
        inList = true;
    }

    /**
     * @param string
     */
    private void error(String string)
    {
        throw new InvalidInputException(string); // FUNC3 add error details
    }

    private char nextChar()
    {
        return serialization.charAt(offset++);
    }

    private String getString(Object obj)
    {
        if (obj instanceof ArrayList)
            return "List";
        else if (obj instanceof Object[])
            return ModelIdHelper.getCompositeModelId(obj).toString();
        else
            return String.valueOf(obj);
    }

    private void debug(String s)
    {
        System.out.println(s);
    }
}
/*******************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others. All rights reserved.
 * 
 * This program is made available under the terms of the GNU General Public
 * License v2, which is part of this distribution and is available at
 * http://www.gnu.org/licenses/gpl.txt.
 * 
 * Contributors: Tersus Software Ltd. - Initial API and implementation
 ******************************************************************************/

package tersus.webapp;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileUploadBase.FileSizeLimitExceededException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang3.StringEscapeUtils;
import org.inconspicuous.jsmin.JSMin;

import tersus.model.BuiltinModels;
import tersus.model.BuiltinProperties;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelException;
import tersus.model.ModelId;
import tersus.model.Path;
import tersus.model.PluginDescriptor;
import tersus.model.Repository;
import tersus.model.Role;
import tersus.runtime.BinaryDataHandler;
import tersus.runtime.BinaryValue;
import tersus.runtime.ElementHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.IllegalServerAccessException;
import tersus.runtime.InstanceHandler;
import tersus.runtime.InvalidPathException;
import tersus.runtime.InvocationAdapter;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.FileUtils;
import tersus.util.Misc;
import tersus.util.Template;
import tersus.webapp.ApplicationSettings.Perspective;

public class Engine extends HttpServlet
{
	private static final String TRACE_FILE_NAME_HEADER = "X-TRACE-FILE-NAME";

	private static final String WELCOME_SERVLET_PATH = "/__welcome";

	private static final long serialVersionUID = 1L;

	public static final String _JS_CALLBACK = "_js_callback";

	private static final String TRACE_FILE_NAME_PARAMETER = "_traceFileName";

	ContextPool pool;

	public static final String WELCOME_SERVICE = "<Welcome>";

	public static final String SERVICE_SERVLET_PATH = "/Service";

	public static final String FILE_SERVLET_PATH = "/File";

	public static final String CACHE_MANIFEST_SERVLET_PATH = "/CacheManifest";

	public static final String CODE_SERVLET_PATH = "/Code";

	public static final String PLUGIN_SERVLET_PATH = "/Plugin";

	public static final String TIMESTAMP_SERVLET_PATH = "/Timestamp";

	public static final String ABORT_SERVLET_PATH = "/Abort";

	public static final String LOGOUT_SERVLET_PATH = "/Logout";

	public static final String SCRIPTS_SERVLET_PATH = "/Scripts";

	public static final String STYLES_SERVLET_PATH = "/Styles";

	public static final String TRACE_SERVLET_PATH = "/Trace";

	public static final String ENCODING = "UTF-8";

	public static final String MAIN_DATA_SOURCE = "jdbc/Main";

	public static final String ROOT_SYSTEM_KEY = "root_system";

	public static final String REPOSITORY_ROOT_KEY = "repository_root";

	public static final String PROJECT_ROOT_KEY = "project_root";

	public static final String ALLOW_APPLICATION_SPECIFIC_PLUGINS = "allow_application_spacific_plugins";

	static final String DEBUG_KEY = "debug";

	static final String REPOSITORY_KEY = "tersus.engine.repository";

	public static final String TRACE_DIR_KEY = "trace";

	public static final String MODEL_LIBRARIES_KEY = "model_libraries";

	public static final String TIMESTAMP_FILE_KEY = "timestamp_file";

	private static final String NEW_FILE = "NewFile";
	private static final String TRACE_ALL = "All";
	private static final String TRACE_RESET = "Reset";


	private static final String USER = "user";

	public static final String GUEST = "Guest";

	private static final String SETUP = "setup";

	private static final String USER_AGENT = "UserAgent";

	private static final String USER_AGENT_CLAUSE = "UserAgentClause";
	private static final String MINIFY_CLAUSE = "MinifyClause";
	private static final String USER_AGENT_CLAUSE0 = "UserAgentClause0";
	public static final String URL_PATH_SUFFIX="UrlPathSuffix";
	public static final String CONTEXT_PATH="ContextPath";

	private static final String DOCTYPE = "doctype";

	private static final String STANDARDS_DOCTYPE = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\">";

	private static final String QUIRKSMODE_DOCTYPE = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">";

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,
			ServletException
	{
		request.setCharacterEncoding(Engine.ENCODING);
		handleRequest(new RequestParameterProxy(request), response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		request.setCharacterEncoding(Engine.ENCODING);
		handleRequest(request, response);
	}

	private void handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException
	{

		String servletPath = request.getServletPath();
		String contentType = request.getContentType();
		boolean isBinaryRequest = "application/octet-stream".equals(contentType);
		if (ServletFileUpload.isMultipartContent(request))
		{
			try
			{
				boolean bufferAll = request.isSecure() && "true".equals(getPool().getServletContext().getInitParameter(ContextPool.BUFFER_ALL_HTTPS_MULTIPART_REQUESTS));
				request = new MultipartRequest(request, getPool().getMaxUploadFileSize(), bufferAll);
			}
			catch (Throwable e)
			{
				String message = "Failed to process multipart request";
				if (e instanceof FileSizeLimitExceededException)
					message += " - file size limit exceeded";
				else
					message += "\n This is probably a temporary network problem - please retry";
				EngineException e1 = new EngineException(message, null, e);
				try
				{
					if (SERVICE_SERVLET_PATH.equals(servletPath)
							|| FILE_SERVLET_PATH.equals(servletPath))
					{
						sendServiceError(response, e1);
						LoggingHelper.get().setError(e);
						return;
					}
				}
				catch (Throwable e2)
				{
				}
				throw e1;
			}

		}
		if (isBinaryRequest)
			handleFileRequest(request, response);
		else if (servletPath.equals(SERVICE_SERVLET_PATH)
				|| "json".equals(request.getParameter("_output")))
			handleJavascriptRequest(request, response);
		else if (servletPath.equals(FILE_SERVLET_PATH))
			handleFileRequest(request, response);
		else if (servletPath.equals(CACHE_MANIFEST_SERVLET_PATH))
			serveCacheManifest(request, response);
		else if (servletPath.equals(CODE_SERVLET_PATH))
			handleCodeRequest(request, response);
		else if (servletPath.equals(SCRIPTS_SERVLET_PATH))
			handleScriptsRequest(request, response);
		else if (servletPath.equals(STYLES_SERVLET_PATH))
			handleStylesRequest(request, response);
		else if (servletPath.equals(TRACE_SERVLET_PATH))
			handleTraceRequest(request, response);
		else if (servletPath.equals(ABORT_SERVLET_PATH))
			handleAbortRequest(request, response);
		else if (servletPath.equals(LOGOUT_SERVLET_PATH))
			handleLogoutRequest(request, response);
		else if (servletPath.equals(TIMESTAMP_SERVLET_PATH))
			handleTimestampRequest(request, response);
		else if (servletPath.equals(PLUGIN_SERVLET_PATH))
			handlePluginRequest(request, response);
		else if (("/".equals(request.getServletPath())
					|| WELCOME_SERVLET_PATH.equals(request.getServletPath()))
				|| getPool().urlNavigation && !checkService(request))
		{
			if (getPool().getRootModel().getElement(Role.get(WELCOME_SERVICE)) != null)
				runWelcomeService(request, response);
			else
			serveDefaultWelcomePage(request, response);
		}
		else
			handleFileRequest(request, response);
	}

	private void runWelcomeService(HttpServletRequest request, HttpServletResponse response)
	{
		HTTPRequestHandler handler = new HTTPRequestHandler(this);
		handler.setPath(WELCOME_SERVICE);
		handler.run(request,response);		
	}

	private void sendServiceError(HttpServletResponse response, Exception e1)
	{
		response.setCharacterEncoding(ENCODING);
		response.setContentType("text/html");
		response.addHeader("Pragma", "no-cache");
		response.addHeader("Cache-control", "no-cache");
		String responseContent = "<html><head><title>Error</title></head><body><p>"
				+ StringEscapeUtils.escapeHtml4("Error:" + e1.getMessage())
				+ "</p><script type=\"text/javascript\">if (parent && parent.tersus && parent.tersus.serverError) parent.tersus.serverError('"
				+ StringEscapeUtils.escapeEcmaScript(e1.getMessage())
				+ "');</script></body>>/html>";
		response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		try
		{
			response.getWriter().write(responseContent);
			response.getWriter().flush();
		}
		catch (IOException e)
		{

		}

	}

	private void serveDefaultWelcomePage(HttpServletRequest request, HttpServletResponse response)
	{

		RuntimeContext context = null;
		try
		{
			context = initContext(request, response);
			String welcomePageContent = getWelcomePageContent(context);
			response.setCharacterEncoding(ENCODING);
			response.setContentType("text/html");
			// response.addHeader("Pragma", "no-cache");
			// response.addHeader("Cache-control", "no-cache");
			response.getWriter().write(welcomePageContent);
			getPool().poolContext(context);

			context = null;
		}
		catch (Exception e)
		{
			throw new EngineException("Failed to create welcome page", null, e);
		}
		finally
		{
			if (context != null)
			{
				context.dispose();
				context = null;
			}

		}

	}

	private void serveCacheManifest(HttpServletRequest request, HttpServletResponse response)
	{
		RuntimeContext context = null;
		try
		{
			String userAgentStr = request.getParameter(USER_AGENT);
			context = initContext(request, response);
			StringBuffer content = new StringBuffer();
			content.append("CACHE MANIFEST\n");
			long scriptsLastModified = getPool().getScriptAggregator().getLastModified();
			long cssLastModified = getPool().getCSSAggregator().getLastModified();

			if (userAgentStr != null)
			{
				content.append("Styles?timestamp=" + cssLastModified + "&UserAgent=" + userAgentStr
						+ "\n");
				content.append("Scripts?timestamp=" + scriptsLastModified + "&UserAgent="
						+ userAgentStr + "\n");
			}
			else
			{
				content.append("Styles?timestamp=" + cssLastModified + "\n");
				content.append("Scripts?timestamp=" + scriptsLastModified + "\n");
			}
			content.append(readWebFile("base-offline-files"));
			content.append(readWebFile("offline-files"));
			response.setContentType("text/cache-manifest");
			long[] timestamps =
			{ cssLastModified, scriptsLastModified, getPool().getTimestamp(),
					getPool().getWebFile("base-offline-files").lastModified(),
					getPool().getWebFile("offline-files").lastModified() };
			long lastModified = timestamps[0];
			for (int i = 1; i < timestamps.length; i++)
				lastModified = Math.max(lastModified, timestamps[i]);
			response.setDateHeader("Last-Modified", lastModified);
			response.setDateHeader("Expires", System.currentTimeMillis());

			response.addHeader("Pragma", "no-cache");
			response.addHeader("Cache-control", "no-cache");
			response.getWriter().write(content.toString());
			getPool().poolContext(context);
			context = null;
		}
		catch (Exception e)
		{
			throw new EngineException("Failed to create cache manifest", null, e);
		}
		finally
		{
			if (context != null)
			{
				context.dispose();
				context = null;
			}

		}

	}

	private String readWebFile(String filename) throws FileNotFoundException
	{
		String list = FileUtils.readString(new FileInputStream(getPool().getWebFile(filename)),
				ENCODING, true);
		return list;
	}

	static UserAgent getUserAgent(HttpServletRequest request)
	{
		String userAgentStr = request.getHeader("USER-AGENT");
		String simulatorAgentStr = request.getParameter(USER_AGENT);
		if (simulatorAgentStr != null)
			userAgentStr += "; simulator for " + simulatorAgentStr;
		UserAgent agent = UserAgent.get(userAgentStr);
		return agent;
	}

	public static String getWelcomePageContent(RuntimeContext context)
	{
		try
		{
			String userAgentStr = null;
			UserAgent userAgent = null;
			String minify = "true";
			HttpServletRequest request = ((WebContext)context).getRequest();
			if (context instanceof WebContext)
			{
				if (request != null)
				{
					userAgentStr = request.getParameter(USER_AGENT);
					userAgent = getUserAgent(request);
					minify=request.getParameter("minify");
				}
			}
			if (userAgent == null)
				userAgent = getUserAgent(null);
			ContextPool contextPool = ((ContextPool) context.getContextPool());
			String templateName = contextPool.customNavigation ? "welcome-nonav-template.html"
					: "welcome-nav-template.html";
			File templateFile = contextPool.getWebFile(templateName);
			String loadingText = contextPool.getLoadingText();

			// TODO: refactor to improve reuse with ScriptEnv.getSetupScript();
			ApplicationSettings settings = new ApplicationSettings(contextPool,
					context.getUserPermissions());// TODO cache settings
			Map<String, Object> properties = new HashMap<String, Object>();
			properties.putAll(settings.getProperties());
			String loggedInUser = context.getLoggedInUser();
			String userName = loggedInUser == null ? GUEST : loggedInUser;
			String pathSuffix = request.getServletPath();
			String uri = request.getRequestURI();
			String contextPath = request.getContextPath();
			pathSuffix = uri.substring(contextPath.length());
			properties.put(URL_PATH_SUFFIX, pathSuffix);
			properties.put(CONTEXT_PATH, request.getContextPath());
			properties.put(USER, userName);
			if (userAgentStr != null)
			{
				properties.put(USER_AGENT_CLAUSE, "&" + USER_AGENT + "=" + userAgentStr);
				properties.put(USER_AGENT_CLAUSE0, "?" + USER_AGENT + "=" + userAgentStr);
			}
			properties.put(MINIFY_CLAUSE, minify!=null ? "&minify="+minify : "");
			properties.put(DOCTYPE, settings.isStandardsMode() ? STANDARDS_DOCTYPE
					: QUIRKSMODE_DOCTYPE);
			properties.put("rtlSuffix", "rtl".equals(settings.getProperties().get(
					ApplicationSettings.TEXT_DIRECTION)) ? "_rtl" : "");
			List<Perspective> perspectives = settings.getPerspectives();
			if (userAgent.isIPhone())
			{
				properties
						.put("metaTags",
								"<meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;\"/>\n<meta name=\"apple-mobile-web-app-capable\" content=\"yes\" />\n<meta names=\"apple-mobile-web-app-status-bar-style\" content=\"black-translucent\" />");
			}

			if (userAgent.isIE())
			{
				String ieLegacyDocumentMode = (String)settings.getProperties().get(ApplicationSettings.IE_LEGACY_DOCUMENT_MODE);
				if (ieLegacyDocumentMode == null)
					ieLegacyDocumentMode = "IE=Edge";

				properties
					.put("metaTags",
							"<meta http-equiv=\"X-UA-Compatible\" content=\"" + ieLegacyDocumentMode + "\">");
			}

			properties.put("loading", loadingText);
			long timestamp = Math
					.max(contextPool.getTimestamp(), context.getLastPermissionUpdate());
			properties.put("timestamp", String.valueOf(timestamp));
			properties.put("scriptsLastModified",
					String.valueOf(contextPool.getScriptAggregator().getLastModified()));
			long cssLastModified = contextPool.getCSSAggregator().getLastModified();
			properties.put("cssLastModified", String.valueOf(cssLastModified));
			properties.put(ContextPool.USE_SQL_QUOTED_IDENTIFIERS,contextPool.useQuotedIdentifiers());
			StringBuffer setupScript = SetupScriptGenerator.prepareSetupScript(perspectives,
					properties, userName, timestamp, cssLastModified);
			properties.put(SETUP, setupScript);
			String traceImageStyle = "";
			if (!"true".equals(properties.get(ApplicationSettings.DEV_MODE)))
				traceImageStyle = "display:none;";

			properties.put("traceImageStyle", traceImageStyle);
			StringWriter out = new StringWriter();
			String templateText = FileUtils.readString(new FileInputStream(templateFile), ENCODING,
					true);
			Template template = new Template(templateText);
			template.write(out, properties);
			String welcomePageContent = out.toString();
			return welcomePageContent;
		}
		catch (Exception e)
		{
			throw new RuntimeException("Failed to generate welcome page", e);
		}
	}

	public long getScriptsLastModified()
	{
		return getPool().getScriptAggregator().getLastModified();
	}

	public long getCSSLastModified()
	{
		return getPool().getCSSAggregator().getLastModified();
	}

	private void handleTimestampRequest(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			String callBack = request.getParameter("callback");
			Misc.assertion(callBack == null);// TODO (eliminate redundant use of callback)
			String withPermissions = request.getParameter("withPermissions");
			String jsVariableName = request.getParameter("jsVariableName");
			long timestamp = getPool().getTimestamp();
			if ("true".equals(withPermissions))
			{
				RuntimeContext context = null;
				try
				{
					context = getPool().getContext();
					timestamp = Math.max(timestamp, context.getLastPermissionUpdate());
					getPool().poolContext(context);
					context = null;
				}
				finally
				{
					if (context != null)
					{
						context.dispose();
						context = null;
					}
				}
			}
			String responseStr;
			if (callBack != null)
				responseStr = callBack + "(" + timestamp + ");";
			else if (jsVariableName != null)
				responseStr = jsVariableName + "=" + timestamp + ";\n";
			else
			{
				String loggedInUser = (String) request.getSession().getAttribute(
						ContextPool.REMOTE_USER);
				String userName = loggedInUser == null ? GUEST : loggedInUser;

				responseStr = "{\n timestamp:" + timestamp + ",\n userName:'"
						+ StringEscapeUtils.escapeEcmaScript(userName) + "'\n}\n";
			}
			if (jsVariableName != null || callBack != null)
				response.setContentType("text/javascript");
			else
				response.setContentType("text/plain");
			response.addHeader("Pragma", "no-cache");
			response.addHeader("Cache-control", "no-cache");
			response.getWriter().write(responseStr);
			response.getWriter().flush();
		}
		catch (IOException e)
		{
			throw new RuntimeException("Failed to write response");
		}
	}

	private void handlePluginRequest(HttpServletRequest request, HttpServletResponse response)
	{
		String responseStr = null;
		String errorMessage = null;
		try
		{
			Map<String, String> parameters = new HashMap<String, String>();
			@SuppressWarnings("rawtypes")
			Enumeration parameterNames = request.getParameterNames();
			while (parameterNames.hasMoreElements())
			{
				String key = (String) parameterNames.nextElement();
				String value = request.getParameter(key);
				parameters.put(key, value);
			}
			String plugin = request.getPathInfo().substring(1);
			if (plugin.endsWith("/"))
				plugin = plugin.substring(0, plugin.length() - 1);
			Repository repository = getPool().getRepository();
			PluginDescriptor descriptor = repository.getPluginDescriptor(plugin);
			ClassLoader pluginLoader = getPool().getClassLoader();
			Class<?> pluginClass = pluginLoader.loadClass(descriptor.getJavaClass());
			Plugin instance = (Plugin) pluginClass.newInstance();
			responseStr = instance.handlePluginRequest(getPool(), parameters);
		}
		catch (Exception e)
		{
			logException(request, e, null, null);
			StringWriter w = new StringWriter();
			e.printStackTrace(new PrintWriter(w));
			errorMessage = w.toString();
		}
		try
		{
			response.setCharacterEncoding(ENCODING);
			response.setContentType("text/plain");
			response.addHeader("Pragma", "no-cache");
			response.addHeader("Cache-control", "no-cache");
			if (errorMessage == null)
			{
				response.getWriter().write(responseStr);
				response.getWriter().flush();
			}
			else
			{
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				response.getWriter().write(errorMessage);
				response.getWriter().flush();
			}
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, errorMessage);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private void updateSessionAttributes(HttpServletRequest request)
	{
		HttpSession s = request.getSession();
		if (s.getAttribute(ContextPool.REMOTE_ADDR) == null && request.getRemoteAddr() != null)
			s.setAttribute(ContextPool.REMOTE_ADDR, request.getRemoteAddr());
		String remoteUser = request.getRemoteUser();
		if (remoteUser != null && s.getAttribute(ContextPool.REMOTE_USER) == null)
		{
			int indexOfBackslash = remoteUser.indexOf('\\');
			if (indexOfBackslash >= 0)
				remoteUser = remoteUser.substring(indexOfBackslash + 1);
			s.setAttribute(ContextPool.REMOTE_USER, remoteUser);
		}
	}

	private void handleAbortRequest(HttpServletRequest request, HttpServletResponse response)
	{
		HttpSession session = request.getSession();
		String userRequestId = request.getParameter("_userRequestId");
		synchronized (session)
		{
			session.setAttribute("ABORT_" + userRequestId, Boolean.TRUE);
		}
		response.setContentType("text/plain");
		response.addHeader("Pragma", "no-cache");
		response.addHeader("Cache-control", "no-cache");
		PrintWriter writer;
		try
		{
			writer = response.getWriter();
		}
		catch (IOException e)
		{
			throw new RuntimeException("Failed to generate response", e);
		}
		writer.write("Abort request registered");
		writer.flush();

	}

	private void handleLogoutRequest(HttpServletRequest request, HttpServletResponse response)
	{
		HttpSession session = request.getSession();
		response.setContentType("text/plain");
		response.addHeader("Pragma", "no-cache");
		response.addHeader("Cache-control", "no-cache");
		PrintWriter writer;
		try
		{
			writer = response.getWriter();
		}
		catch (IOException e)
		{
			throw new RuntimeException("Failed to generate response", e);
		}
		writer.write("Logged out.");
		writer.flush();
		session.setAttribute("LOGGED_OUT", Boolean.TRUE);
		session.invalidate();
	}

	/**
	 * @param request
	 * @param response
	 */
	private void handleTraceRequest(HttpServletRequest request, HttpServletResponse response)
	{
		if (!getPool().isTrustedAddress(request.getRemoteAddr()))
			return;
		String path = getPathStr(request);
		if (path.startsWith("/"))
			path = path.substring(1);
		if (NEW_FILE.equals(path))
			newTraceFile(response);
		else if (TRACE_ALL.equals(path))
			traceAll(response);
		else if (TRACE_RESET.equals(path))
			traceReset(response);
		else
			appendToTraceFile(path, request, response);

	}

	/**
	 * @param response
	 */
	private void appendToTraceFile(String filename, HttpServletRequest request,
			HttpServletResponse response)
	{
		try
		{
			File traceFile = getPool().getTraceFile(filename);
			byte[] content = FileUtils.readBytes(request.getInputStream(), true);
			FileOutputStream os = new FileOutputStream(traceFile, true);
			os.write(content);
			os.close();
			response.setContentType("text/plain");
			PrintWriter out = response.getWriter();
			out.println("OK\n");
			out.flush();
		}
		catch (IOException e)
		{
			throw new RuntimeException("Append to trace file failed", e);
		}
	}

	/**
	 * @param response
	 */
	private void newTraceFile(HttpServletResponse response)
	{
		File traceFile;
		try
		{
			traceFile = getPool().createTraceFile();

		}
		catch (IOException e)
		{
			throw new RuntimeException("Failed to generate trace file", e);
		}
		response.setContentType("text/plain");
		response.addHeader("Pragma", "no-cache");
		response.addHeader("Cache-control", "no-cache");
		PrintWriter writer;
		try
		{
			writer = response.getWriter();
		}
		catch (IOException e)
		{
			throw new RuntimeException("Failed to generate response", e);
		}
		writer.write(traceFile.getName());
		writer.flush();
	}

	private void traceAll(HttpServletResponse response)
	{
		getPool().setCompleteTracing(true);
		response.setContentType("text/plain");
		response.addHeader("Pragma", "no-cache");
		response.addHeader("Cache-control", "no-cache");
		PrintWriter writer;
		try
		{
			writer = response.getWriter();
		}
		catch (IOException e)
		{
			throw new RuntimeException("Failed to generate response", e);
		}
		writer.write("Complete server tracing turned on");
		writer.flush();
	}

	private void traceReset(HttpServletResponse response)
	{
		getPool().setCompleteTracing(false);
		response.setContentType("text/plain");
		response.addHeader("Pragma", "no-cache");
		response.addHeader("Cache-control", "no-cache");
		PrintWriter writer;
		try
		{
			writer = response.getWriter();
		}
		catch (IOException e)
		{
			throw new RuntimeException("Failed to generate response", e);
		}
		writer.write("Complete server tracing turned off");
		writer.flush();
	}

	private void handleScriptsRequest(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{

			long lastModified = getScriptsLastModified();
			if (checkIfModified(request, lastModified))
			{
				response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
				return;
			}
			final UserAgent userAgent = getUserAgent(request);

			StringBuffer out = getPool().getScriptAggregator().getAggregatedFile(
					new FilenameFilter()
					{

						public boolean accept(File dir, String name)
						{
							return userAgent.match(name)
									// If custom navigation requested - exclude default navigation mechanism
									&& (!getPool().customNavigation ||
											!name.equals(tersus.ProjectStructure.DEFAULT_NAV_SCRIPT_NAME))
									// If url navigation not requested - exclude default navigation mechanism
									&& (getPool().urlNavigation ||
											!name.equals(tersus.ProjectStructure.URL_NAV_SCRIPT_NAME));

						}
					});

			response.setCharacterEncoding(ENCODING);
			response.setContentType("text/javascript");
			response.setDateHeader("Last-Modified", lastModified);
			response.setDateHeader("Expires", System.currentTimeMillis() + Config.YEAR_MILLISECONDS);
            response.addHeader("Cache-control", "public,max-age:" + Config.YEAR_SECONDS);

			try
			{
				byte[] bytes = out.toString().getBytes(ENCODING);
				ServletOutputStream os = response.getOutputStream();
				String minifyParam = request.getParameter("minify");
				if (minifyParam == null)
					minifyParam = pool.isDebugMode()?"false":"true";
			
				boolean minify = !"false".equals(minifyParam);
				if (minify)
				{
					JSMin jsmin = new JSMin(new ByteArrayInputStream(bytes), os);
					jsmin.jsmin();
				}
				else
					os.write(bytes);
			}
			catch (IOException e)
			{
				logException(request, e, "failed to generate scripts file", null);
			}
		}
		catch (Exception e)
		{
			String message = "Failed to load scripts. See server error log for details.";
			logException(request, e, null, null);
			try
			{
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message);
			}
			catch (IOException e1)
			{
				logException(request, e1, null, null);
			}
		}

	}

	/**
	 * Check the client's cached version using the request header If-Modified-Since
	 * 
	 * @param request
	 * @param response
	 * @return true if the cached version is OK, false if a new version needs to be served
	 */
	private boolean checkCachedVersion(RuntimeContext context, HttpServletRequest request,
			HttpServletResponse response)
	{
		long lastModified = Math.max(getPool().getTimestamp(), context.getLastPermissionUpdate());
		return checkIfModified(request, lastModified);
		// nearest second
	}

	private boolean checkIfModified(HttpServletRequest request, long lastModified)
	{
		long headerValue = request.getDateHeader("If-Modified-Since");
		if (headerValue == -1)
			return false;
		return (headerValue > lastModified - 1000); // Allow for rounding to the
	}

	/**
	 * Handles a request to generate code
	 * 
	 * @param request
	 * @param response
	 */
	private void handleCodeRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		getPool().reloadIfNeeded();
		WebContext context = null;
		try
		{
			context = initContext(request, response);
			if ("GET".equals(request.getMethod()) && checkCachedVersion(context, request, response))
				response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
			else
				context.getCodeGenerator().doRequest(request, response);
			getPool().poolContext(context);
			context = null;
		}
		finally
		{
			if (context != null)
			{
				context.dispose();
				context = null;
			}
		}
	}

	/**
	 * @param request
	 * @param response
	 */
	private void handleFileRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException
	{
		HTTPRequestHandler handler = new HTTPRequestHandler(this);
		handler.setPath(getServicePath(request));
		handler.run(request,response);

	}


	private boolean checkService(HttpServletRequest request)
	{
		String pathStr = getPathStr(request);
		if (pathStr == null || pathStr.isEmpty())
			return false;
		Path path = new Path(pathStr);
		ContextPool pool = getPool();
		Model rootModel =  pool.getRootModel();
		Model currentModel = rootModel;
		boolean inClient = getPool().getUIModelIds().contains(rootModel.getId());
		for (int index = 0; index <= path.getNumberOfSegments(); index++)
		{
			PluginDescriptor pluginDescriptor = currentModel.getPluginDescriptor();
			if (pluginDescriptor != null)
			{
				if (!pluginDescriptor.allowedOnServer())
					inClient = true;
				if (inClient && pluginDescriptor.isServiceClient()
						|| pluginDescriptor.isWebService())
				{
					return true; // Path refers to a service or an inner part of a servie
				}
			}
			if (index < path.getNumberOfSegments())
			{
				Role segment = path.getSegment(index);
				ModelElement element = currentModel.getElement(segment);
				if (element == null)
					return false;
				currentModel = element.getReferredModel();
			}
		}
		return false;

	}
	
	String getServicePath(HttpServletRequest request)
	{
		String path = request.getParameter("_path");
		if (path != null)
			return path;
		path = request.getPathInfo();
		if (path == null)
			path = request.getServletPath();
		if (path == null || path.length() <= 1 || path.equals("/index.html")
				|| path.equals(WELCOME_SERVLET_PATH))
			return WELCOME_SERVICE;
		else
			return path.substring(1);

	}

	/**
	 * @param request
	 * @return
	 */
	public static String getPathStr(HttpServletRequest request)
	{
		String uri = HttpUtil.decode(request.getRequestURI());
		String contextPath = request.getContextPath();
		String servletPath = request.getServletPath();
		int pathStart = contextPath.length() + servletPath.length() + 1;
		if (pathStart>=uri.length())
			return servletPath.substring(1); // Welcome page / callable service paths
		else
			return uri.substring(pathStart);
	}



	private void handleJavascriptRequest(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException
	{
		if (getPool().getCheckRequestSignatures()
				&& request.getServletPath().equals(SERVICE_SERVLET_PATH)
				&& request.getParameter("_sig") == null)
			throw new BadRequestException("Incomplete request - missing signature");
		getPool().reloadIfNeeded();
		String callback = request.getParameter(_JS_CALLBACK);
		WebContext context = null;
		Throwable error = null;
		try
		{
			context = initContext(request, response);
			if (callback != null)
				response.setContentType("text/html; charset=" + Engine.ENCODING);
			else
				response.setContentType("text/javascript; charset=" + Engine.ENCODING);
			PrintWriter out = response.getWriter();
			String pathStr = getServicePath(request);
			String baseModelId = request.getParameter("_baseModelId");
			writeHeader(callback, out, pathStr);
			StringBuffer serviceResponse = null;
			try
			{
				FlowInstance serviceInstance = callService(request, baseModelId, pathStr, context,
						serviceInvocationAdapter);
				serviceResponse = serializeExitValues(context, serviceInstance, out);
				writeResponse(out, serviceResponse, callback);
				out.flush();
				getPool().poolContext(context);
				context = null;
			}
			catch (Throwable e)
			{
				error = e;
				serviceResponse = new StringBuffer();
				serviceResponse.append("{_ERROR:");
				appendError(serviceResponse, e,pathStr,context);
				serviceResponse.append("}");
				serviceResponse.append("\n");
				writeResponse(out, serviceResponse, callback);
			}
			writeFooter(callback, out);
			out.flush();
			out.close();
		}
		finally
		{
			if (context != null)
			{
				context.dispose();
				context = null;
			}
		}
		if (error instanceof RuntimeException)
			throw (RuntimeException) error;
		else if (error instanceof Error)
			throw (Error) error;
		else if (error != null)
			throw new RuntimeException(error);
	}

	private void writeFooter(String callback, PrintWriter out)
	{
		if (callback != null)
		{
			out.println("}");
			out.println("-->");
			out.println("</script>");
			out.println("</body");
			out.println("</html>");
		}
	}


	private void writeHeader(String callback, PrintWriter out, String pathStr)
	{
		if (callback != null)
		{
			out.println("<html>");
			out.println("<body onLoad=\"done()\">");
			out.println("Invoking service with path <b>'" + pathStr + "</b>'<br/>");
			out.println("<script language=\"javascript\">");
			out.println("<!--");
			out.println("function done(){");
		}
	}

	private void writeResponse(PrintWriter out, StringBuffer response, String callback)
			throws IOException
	{
		if (callback == null)
		{
			out.write(response.toString());
		}
		else
		{
			String escaped = response.toString().replaceAll("[<]", "\\\\x3C")
					.replaceAll("[>]", "\\\\x3E"); // Since

			// we're
			// wrapping
			// in
			// HTML, we need to escape
			// '<' and '>'
			out.write(callback + "(" + escaped + ");");
		}
		out.write("\n");
	}

	WebContext initContext(HttpServletRequest request, HttpServletResponse response)
	{
		WebContext context = null;
		try
		{
			context = (WebContext) getPool().getContext();
			context.setRequest(request);
			context.setResponse(response);
			context.setRequestType(request.getServletPath());
			context.setServiceModelId(null);
			context.setSession(request.getSession());
			return context;
		}
		catch (RuntimeException e)
		{
			if (context != null)
				context.dispose();
			throw e;
		}
		catch (Error e)
		{
			if (context != null)
				context.dispose();
			throw e;
		}
	}

	private void logException(HttpServletRequest request, Throwable e, String pathPrefix,
			RuntimeContext context)
	{
		StringBuffer message = new StringBuffer();
		message.append("An error has occurred while processing a request");
		String separator = System.getProperty("line.separator");
		if (context != null)
		{
			message.append(separator);
			message.append("Context: " + context);
			message.append(separator);
			message.append("Error Location: " + context.errorPath);
		}
		if (e instanceof EngineException)
		{
			String details = ((EngineException) e).getDetails();
			if (details != null)
			{
				message.append(separator);
				message.append("Details:");
				message.append(details);
			}
		}
		getPool().engineLog.error(message.toString(), e);
	}

	StringBuffer appendError(StringBuffer buf, Throwable e, String pathPrefix, RuntimeContext context)
	{
		String message;
		String details;
		if (e instanceof EngineException)
		{
			message = e.getMessage();
			details = ((EngineException) e).getDetails();
		}
		else if (e instanceof ModelExecutionException)
		{
			message = e.getMessage();
			details = null;
		}
		else
		{
			message  = e.getClass().getName();
			details  = e.getMessage();
		}
		buf.append("{message:\""+JavaScriptFormat.escape(message));
		if (details != null)
			buf.append("\",details:\""+JavaScriptFormat.escape(details));
		if (context != null && context.errorPath != null)
			buf.append("\",errorPath:\""+JavaScriptFormat.escape(context.errorPath.toString()));
		buf.append(("\"}"));
		return buf;
	}

	static FlowInstance callService(HttpServletRequest request, String baseModelIdStr,
			String pathStr, RuntimeContext context, InvocationAdapter invoker)
	{
		String traceFileName = request.getParameter(TRACE_FILE_NAME_PARAMETER);
		if (traceFileName == null)
			traceFileName = request.getHeader(TRACE_FILE_NAME_HEADER);
		return context.runProcess(baseModelIdStr, pathStr, traceFileName, invoker);
	}

	/**
	 * @param context
	 * @param serviceHandler
	 * @param serviceInstance
	 * @param out
	 */
	private StringBuffer serializeExitValues(RuntimeContext context, FlowInstance serviceInstance,
			PrintWriter out)
	{
		SlotHandler[] exits = serviceInstance.getHandler().getExits();
		StringWriter response = new StringWriter();
		response.append("{");
		int nValues = 0;
		for (int i = 0; i < exits.length; i++)
		{
			SlotHandler exit = exits[i];
			Object value = exit.get(context, serviceInstance);

			if (value != null)
			{
				if (nValues > 0)
					response.append(",");
				++nValues;
				response.append('"');
				response.append(JavaScriptFormat.escape(exit.getRole().toString()));
				response.append("\":\n");
				JavaScriptFormat.serialize(value, exit.getChildInstanceHandler(), response,
						context, true);
				response.append("\n");
			}
		}
		response.append("}");
		return response.getBuffer();
	}

	static final InvocationAdapter serviceInvocationAdapter = new InvocationAdapter()
	{

		public void setTriggers(RuntimeContext context, FlowInstance serviceInstance,
				SlotHandler[] triggers)
		{
			HttpServletRequest request = ((WebContext) context).getRequest();
			if (triggers.length == 1
					&& BuiltinModels.BINARY_ID.equals(triggers[0].getChildModelId()))
			{
				try
				{
					byte[] bytes = FileUtils.readBytes(request.getInputStream(), true);
					BinaryValue binary = new BinaryValue(bytes);
					triggers[0].set(context, serviceInstance, binary);
				}
				catch (IOException e)
				{
					throw new EngineException("Failed to read request", e.getMessage(), e);
				}
			}
			else
				for (int i = 0; i < triggers.length; i++)
				{
					SlotHandler trigger = triggers[i];
					String name = trigger.getRole().toString();
					if (request instanceof MultipartRequest && !name.equals(name.trim()))
					{
						throw new ModelException(
								"Invalid trigger name '"
										+ name
										+ "' - leading and trailing blanks in trigger names not supported when uploading files");
					}
					InstanceHandler childInstanceHandler = trigger.getChildInstanceHandler();
					if (request instanceof MultipartRequest
							&& BuiltinModels.FILE_ID.equals(trigger.getChildModelId()))
					{
						BinaryFile file = ((MultipartRequest) request).getBinaryFile(name);
						if (file != null && file.getData() != null)
						{
							InstanceHandler fileHandler = childInstanceHandler;
							Object value = fileHandler.newInstance(context);
							ElementHandler contentElement = fileHandler
									.getElementHandler(BuiltinModels.CONTENT_ROLE);
							ElementHandler contentTypeElement = fileHandler
									.getElementHandler(BuiltinModels.CONTENT_TYPE_ROLE);
							ElementHandler filenameElement = fileHandler
									.getElementHandler(BuiltinModels.FILENAME_ROLE);
							Object content = ((BinaryDataHandler) contentElement
									.getChildInstanceHandler())
									.newInstance(context, file.getData());
							contentElement.set(context, value, content);
							contentTypeElement.create(context, value, file.getContentType(), true);
							filenameElement.create(context, value, getFileName(file.getFileName()),
									true);
							trigger.set(context, serviceInstance, value);
						}
					}
					else
					{
						String valueStr = request.getParameter(name);
						if (valueStr != null)
						{
							if (trigger.isRepetitive())
							{
								for (Object item : JavaScriptFormat.parseList(context,
										childInstanceHandler, valueStr, false, true))
								{
									trigger.accumulate(context, serviceInstance, item);
								}
							}
							else
							{
								Object value;
								if (childInstanceHandler instanceof LeafDataHandler)
									value = childInstanceHandler.newInstance(context, valueStr,
											true);
								else
									value = JavaScriptFormat.parseValue(context,
											childInstanceHandler, valueStr, false, true);
								trigger.set(context, serviceInstance, value);
							}
						}
					}
				}
		}

		public String getLogType()
		{
			return "Service";
		}

		public boolean needsAccessValidation()
		{
			return true;
		}

	};

	/**
	 * Extracts the filename from a path
	 * 
	 * @param fileName
	 * @return
	 */
	private static String getFileName(String path)
	{
		int lastSeparator = Math.max(path.lastIndexOf('/'), path.lastIndexOf('\\'));
		if (lastSeparator < 0)
			return path;
		else
			return path.substring(lastSeparator + 1);
	}

	public ContextPool getPool()
	{
		if (pool == null)
		{
			init((ContextPool) getServletContext().getAttribute(ContextPool.POOL_KEY));
		}
		return pool;
	};

	public void init() throws ServletException
	{
	}

	public void init(ContextPool pool)
	{
		this.pool = pool;
	}

	private void handleStylesRequest(HttpServletRequest request, HttpServletResponse response)
	{
		getPool().reloadIfNeeded();
		try
		{

			long lastModified = getCSSLastModified();
			if (checkIfModified(request, lastModified))
			{
				response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
				return;
			}

			final UserAgent userAgent = getUserAgent(request);
			final String exclude = request.getParameter("exclude");//Used by theme editor
			String cssExclude = (String)getPool().getRootModel().getProperty(BuiltinProperties.OLD_CSS_EXCLUDE);
			final boolean excludeOldCSS = "all".equals(cssExclude) || "mobile".equals(cssExclude) && userAgent.isMobile();
			StringBuffer out = getPool().getCSSAggregator().getAggregatedFile(new FilenameFilter()
			{
				public boolean accept(File dir, String name)
				{
					return userAgent.match(name) && !(exclude != null && name.equals(exclude)) && !(excludeOldCSS && dir.getName().equals("old"));
				}
			});

			response.setCharacterEncoding(ENCODING);
			response.setContentType("text/css");
			response.setDateHeader("Last-Modified", lastModified);
			response.setDateHeader("Expires", System.currentTimeMillis() + Config.YEAR_MILLISECONDS);
            response.addHeader("Cache-control", "public,max-age:" + Config.YEAR_SECONDS);
			try
			{
				byte[] bytes = out.toString().getBytes(ENCODING);
				ServletOutputStream os = response.getOutputStream();
				os.write(bytes);
			}
			catch (IOException e)
			{
				logException(request, e, "failed to generate scripts file", null);
			}
		}
		catch (Exception e)
		{
			String message = "Failed to load scripts. See server error log for details.";
			logException(request, e, null, null);
			try
			{
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message);
			}
			catch (IOException e1)
			{
				logException(request, e1, null, null);
			}
		}

	}

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException
	{
		updateSessionAttributes(req);

		LoggingHelper loggingHelper = LoggingHelper.get();
		loggingHelper.startRequest(getPool().activityLog, getPool().engineLog, req);
		try
		{
			super.service(req, resp);
			loggingHelper.endRequest();
		}
		catch (BadRequestException e)
		{
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
			loggingHelper.endRequest(e);

		}
		catch (ServletException e)
		{
			loggingHelper.endRequest(e);
			throw e;
		}
		catch (IOException e)
		{
			loggingHelper.endRequest(e);
			throw e;
		}
		catch (Error e)
		{
			loggingHelper.endRequest(e);
			throw e;
		}
		catch (Throwable e)
		{
			loggingHelper.endRequest(e);
			e.printStackTrace();
			throw new ServletException("Unexpected error ", e);
		}
		finally
		{
			LoggingHelper.dispose();
		}
	}

}
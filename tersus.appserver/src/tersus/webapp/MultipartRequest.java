package tersus.webapp;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.LimitedInputStream;

import tersus.runtime.EngineException;
import tersus.util.FileUtils;
import tersus.util.NotImplementedException;

/*******************************************************************************
 * Copyright (c) 2003-2007 Tersus Software Ltd. and others. All rights reserved.
 * 
 * This program is made available under the terms of the GNU General Public License v2, which is
 * part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 * 
 * Contributors: Tersus Software Ltd.� Initial API and implementation
 ******************************************************************************/

public class MultipartRequest extends HttpServletRequestWrapper
{
	private Map<String, Object> parameterMap = new HashMap<String, Object>();
	private Vector<String> parameterNames = new Vector<String>();
	ServletInputStream is;

	public MultipartRequest(HttpServletRequest base, long maxRequestSize, boolean bufferAll)
			throws FileUploadException
	{

		super(base);
		if (!ServletFileUpload.isMultipartContent(base))
			throw new IllegalArgumentException("Conent type is not multi-part");
		try
		{
			if (bufferAll)
			{
				// This is a work-around for a very strange problem where under HTTPS MultipartRequest is not parsed correctly - unless its input is pre-fetched into a ByteArrayInputStream

				InputStream in = base.getInputStream();
				if (maxRequestSize > 0)
					in = new LimitedInputStream(in, maxRequestSize)
					{
						protected void raiseError(long pSizeMax, long pCount) throws IOException
						{
							throw new IOException("MultipartRequest size exceeds limit");
						}
					};

				final InputStream bis = new ByteArrayInputStream(FileUtils.readBytes(in, true));
				is = new ServletInputStream()
				{
					public int read() throws IOException
					{
						return bis.read();
					}

					public int read(byte[] b, int off, int len) throws IOException
					{
						return bis.read(b, off, len);
					}

					public int available() throws IOException
					{
						return bis.available();
					}

				};
			}

			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload();

			// Set overall request size constraint
			if (maxRequestSize > 0)
				upload.setFileSizeMax(maxRequestSize);

			// Parse the request

			FileItemIterator iter = upload.getItemIterator(this);
			while (iter.hasNext())
			{
				FileItemStream item = iter.next();
				String name = item.getFieldName();
				InputStream stream = item.openStream();
				if (item.isFormField())
				{
					String value = FileUtils.readString(stream, Engine.ENCODING, false);

					parameterMap.put(name, value);
				}
				else
				{
					BinaryFile file = new BinaryFile();
					file.setFileName(item.getName());
					file.setContentType(item.getContentType());
					file.setData(FileUtils.readBytes(stream, false));
					parameterMap.put(name, file);
				}
				parameterNames.add(name);
			}
			this.getInputStream().close();
		}
		catch (Exception e)
		{
			throw new EngineException("Failed to process multipart request", null, e);
		}

	}

	/** Returns the binary content of a request parameter */
	public BinaryFile getBinaryFile(String name)
	{
		Object item = parameterMap.get(name);
		if (item instanceof BinaryFile)
			return (BinaryFile) item;
		else
			return null;
	}

	public String getParameter(String name)
	{
		Object item = parameterMap.get(name);
		if (item instanceof String)
			return (String) item;
		else
			return null;
	}

	@SuppressWarnings("rawtypes")
	public Map getParameterMap()
	{
		throw new NotImplementedException(
				"method getParameterMap() not implemented for MultiPartRequest");
	}

	@SuppressWarnings("rawtypes")
	public Enumeration getParameterNames()
	{
		return parameterNames.elements();
	}

	public String[] getParameterValues(String arg0)
	{
		throw new NotImplementedException(
				"method getParameterValues() not implemented for MultiPartRequest");
	}

	public ServletInputStream getInputStream() throws IOException
	{
		if (is != null)
			return is;
		else
			return super.getInputStream();
	}
}

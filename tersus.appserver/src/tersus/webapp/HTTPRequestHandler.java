package tersus.webapp;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tersus.InternalErrorException;
import tersus.model.BuiltinModels;
import tersus.model.BuiltinProperties;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.BinaryValue;
import tersus.runtime.ElementHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.IllegalServerAccessException;
import tersus.runtime.InstanceHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.EscapeUtil;
import tersus.util.Misc;

public class HTTPRequestHandler
{
	private static final Role FILE = Role.get("<File>");
	private static final Role ERROR_CODE = Role.get("<Error Code>");
	private static final Role ERROR_MESSAGE = Role.get("<Error Message>");
	private static final Role STATUS_CODE = Role.get("<Status Code>");
	private static final Role REDIRECT = Role.get("<Redirect>");

	private Engine engine;
	private tersus.runtime.Number statusCode;
	private tersus.runtime.Number errorCode;
	private String httpErrorMessage;
	private String redirect;
	private String contentType;
	private BinaryValue content;
	private String filename;
	private String pathStr;
	private RuntimeException error;
	private boolean showFile;
	private long maxAge = 0;
	

	public HTTPRequestHandler(Engine engine)
	{
		this.engine = engine;
	}

	public void run(HttpServletRequest request, HttpServletResponse response)
	{
		showFile = request.getServletPath().equals(Engine.FILE_SERVLET_PATH);
		if (engine.getPool().getCheckRequestSignatures() && showFile
				&& request.getParameter("_sig") == null)
			throw new BadRequestException("Incomplete request - missing signature"); // Handled by
																						// Engine.service()
		engine.getPool().reloadIfNeeded();

		// TODO - a problem here: runService is responsible for recycling / disposing the context.
		// This is not clean. Looks like this needs further refactoring
		WebContext context = null;
		context = engine.initContext(request, response);
		runService(context, request);
		createResponse(context, request, response);
	}

	private void createResponse(WebContext context, HttpServletRequest request,
			HttpServletResponse response)
	{
		response.setCharacterEncoding(Engine.ENCODING);
		if (maxAge>0)
		{
			response.addHeader("Cache-control", "max-age="+maxAge);
		}
		else
		{
			response.addHeader("Pragma", "no-cache");
			response.addHeader("Cache-control", "no-cache");
		}

		if (error == null)
			handleOutput(request, response);
		else
			handleError(context, request, response);
	}

	private void handleError(WebContext context, HttpServletRequest request,
			HttpServletResponse response)
	{
		LoggingHelper.get().setError(error);
		if ("TEXT".equals(request.getHeader("X-ERROR-REPORT-FORMAT")))
		{
			StringBuffer errorBuffer = new StringBuffer();
			engine.appendError(errorBuffer, error, pathStr, context);
			reportErrorAsText(response, errorBuffer.toString());
			return;
		}
		else if (showFile)
		{
			writeErrorMessage(request, response, getErrorMessage(error, pathStr, context)
					.toString());
			return;
		}
		else
		{
			try
			{
				int code = error instanceof IllegalServerAccessException ? HttpServletResponse.SC_FORBIDDEN
						: HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
				response.sendError(code, error.getMessage());
			}
			catch (IOException e1)
			{
				throw new RuntimeException("Error writing response", error);
			}
		}
	}

	private void handleOutput(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			if (errorCode != null)
			{
				response.sendError((int) errorCode.value, httpErrorMessage);
			}
			else if (redirect != null)
			{
				response.sendRedirect(redirect);
			}
			else if (content != null)
			{
				if (contentType != null)
					response.setContentType(contentType);
				if (statusCode != null)
					response.setStatus((int) statusCode.value);

				OutputStream out = new BufferedOutputStream(response.getOutputStream());

				if (filename != null)
				{
					boolean interactive = (request.getParameter("_path") != null);
					String disposition = interactive ? "attachment" : "inline";
					/*
					 * Setting the filename: solution taken from
					 * http://forum.java.sun.com/thread.jspa?threadID=696263 (with an additional
					 * kludge for handling spaces ['+' -> '%20']).
					 */

					UserAgent agent = Engine.getUserAgent(request);
					if (agent.isGecko() && !agent.isIE() && (agent.check("chrome") || !agent.check("safari")))
					{
						String encodedName = MimeUtility.encodeText(filename, "UTF8", "B");
						response.setHeader("Content-Disposition", disposition + ";filename=\""
								+ encodedName + "\"");
					}
					else
					{
						// Other browsers nost known to support UTF8 encoding (as of 2009-09-02)
						String encodedName = filename;
						if (agent.isIE())
							encodedName = Misc.replaceAll(
									URLEncoder.encode(filename, Engine.ENCODING), "+", "%20");// IE

						response.setHeader("Content-Disposition", disposition + ";filename=\""
								+ encodedName + "\"");
					}
				}
				else
					response.setHeader("Content-disposition", "inline;");

				byte[] contentBytes = content.toByteArray();
				response.setContentLength(contentBytes.length);
				out.write(contentBytes);

				out.close();
			}
		}
		catch (IOException e)
		{
			throw new EngineException("Failed to create HTTP response", null, e);
		}
	}

	private void runService(WebContext context, HttpServletRequest request)
	{
		try
		{
			engine.getPool().reloadIfNeeded();
			String baseModelId = request.getParameter("_baseModelId");
			FlowInstance serviceInstance = Engine.callService(request, baseModelId, pathStr,
					context, Engine.serviceInvocationAdapter);
			Model model = serviceInstance.getFlowHandler().getModel();
			String maxAgeStr = (String)model.getProperty(BuiltinProperties.MAX_AGE);
			if (maxAgeStr != null)
				this.maxAge = Integer.parseInt(maxAgeStr);
			
			Object serviceOutput = null;
			SlotHandler fileExit = serviceInstance.getHandler().getSoleExit();
			SlotHandler statusCodeExit = null;
			SlotHandler errorCodeExit = null;
			SlotHandler errorMessageExit = null;
			SlotHandler redirectExit = null;

			if (fileExit == null)
			{
				fileExit = serviceInstance.getHandler().getNonRequiredExit(FILE, false,
						BuiltinModels.FILE_ID);
				statusCodeExit = serviceInstance.getHandler().getNonRequiredExit(STATUS_CODE,
						false, BuiltinModels.NUMBER_ID);
				errorCodeExit = serviceInstance.getHandler().getNonRequiredExit(ERROR_CODE, false,
						BuiltinModels.NUMBER_ID);
				errorMessageExit = serviceInstance.getHandler().getNonRequiredExit(ERROR_MESSAGE,
						false, BuiltinModels.TEXT_ID);
				redirectExit = serviceInstance.getHandler().getNonRequiredExit(REDIRECT, false,
						BuiltinModels.TEXT_ID);
			}
			if (fileExit != null && BuiltinModels.FILE_ID.equals(fileExit.getChildModelId()))
			{
				serviceOutput = getExitValue(context, fileExit, serviceInstance);
				statusCode = (tersus.runtime.Number) getExitValue(context, statusCodeExit,
						serviceInstance);
				errorCode = (tersus.runtime.Number) getExitValue(context, errorCodeExit,
						serviceInstance);
				httpErrorMessage = (String) getExitValue(context, errorMessageExit, serviceInstance);
				redirect = (String) getExitValue(context, redirectExit, serviceInstance);
				InstanceHandler fileHandler = context.getModelLoader().getHandler(
						BuiltinModels.FILE_ID);
				ElementHandler contentElement = fileHandler
						.getElementHandler(BuiltinModels.CONTENT_ROLE);
				ElementHandler contentTypeElement = fileHandler
						.getElementHandler(BuiltinModels.CONTENT_TYPE_ROLE);
				ElementHandler filenameElement = fileHandler
						.getElementHandler(BuiltinModels.FILENAME_ROLE);
				contentType = (String) contentTypeElement.get(context, serviceOutput);
				content = (BinaryValue) contentElement.get(context, serviceOutput);

				filename = (String) filenameElement.get(context, serviceOutput);

				if (showFile && content == null)
				{
					throw new ModelExecutionException("No Output");
				}
			}
			else if (fileExit != null && BuiltinModels.BINARY_ID.equals(fileExit.getChildModelId()))
			{
				serviceOutput = getExitValue(context, fileExit, serviceInstance);
				contentType = "application/octet-stream";
				content = (BinaryValue) serviceOutput;
			}
			else
			{
				contentType = "text/plain";
				serviceOutput = getExitValue(context, fileExit, serviceInstance);
				String contentStr = String.valueOf(serviceOutput);
				try
				{
					content = new BinaryValue(contentStr.getBytes(Engine.ENCODING));
				}
				catch (UnsupportedEncodingException e)
				{
					throw new InternalErrorException(e);
				}

			}
			engine.getPool().poolContext(context);
			context = null;
		}
		catch (RuntimeException e)
		{
			error = e;
		}
		finally
		{
			if (context != null)
			{
				context.dispose();
				context = null;
			}
		}
	}

	private StringBuffer getErrorMessage(Throwable e, String pathPrefix, RuntimeContext context)
	{
		StringBuffer errorMessage = new StringBuffer();
		errorMessage.append("A processing error has occurred:\n");
		errorMessage.append(e.toString());
		if (context != null && context.errorPath != null)
			errorMessage.append("\nError Location: " + context.errorPath);
		return errorMessage;
	}

	private void reportErrorAsText(HttpServletResponse response, String errorMessage)
	{
		response.setHeader("X-ERROR-REPORT-FORMAT", "TEXT");
		response.setContentType("text/plain");
		try
		{
			response.getWriter().write(errorMessage);
			response.getWriter().close();
		}
		catch (IOException e1)
		{
			throw new RuntimeException("Failed write response", e1);
		}
	}

	private Object getExitValue(RuntimeContext context, SlotHandler exit, FlowInstance instace)
	{
		if (exit != null)
			return exit.get(context, instace);
		else
			return null;
	}

	/**
	 * @param request
	 * @param response
	 * @param string
	 */
	private void writeErrorMessage(HttpServletRequest request, HttpServletResponse response,
			String message)
	{
		PrintWriter out;
		try
		{
			out = response.getWriter();
		}
		catch (IOException e)
		{
			throw new RuntimeException("Failed to open response output stream", e);
		}
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Server Error</title>");
		out.println("</head>");
		out.println("</body>");
		out.println("<pre>");
		out.println(message);
		out.println("</pre>");
		out.println("<script type=\"text/javascript\">");
		out.println("if (parent && parent.tersus && parent.tersus.error)");
		out.println("parent.tersus.error('" + EscapeUtil.escape(message, false) + "');");
		out.println("</script>");
		out.println("</body>");
		out.println("</html>");
	}

	public void setPath(String servicePath)
	{
		this.pathStr = servicePath;
	}
}

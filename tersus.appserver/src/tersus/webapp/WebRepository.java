/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.webapp;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.ServletContext;

import tersus.model.ModelException;
import tersus.model.ModelId;
import tersus.model.PackageId;
import tersus.model.Repository;
import tersus.model.SimplePackageCache;
import tersus.util.FileUtils;

/**
 * NICE2 [Description]
 * 
 * @author Youval Bronicki
 * 
 */
public class WebRepository extends Repository
{
	public WebRepository()
	{
		cachedPackages = new SimplePackageCache();
	}

	protected void delete(String path) throws IOException
	{
		throw new RuntimeException(
				"Can't delete resources from web repository (WebRepository is read only)");
	}

	private ServletContext context;

	public WebRepository(ServletContext context)
	{
		this.context = context;
		cachedPackages = new SimplePackageCache();
	}

	protected byte[] read(String p) throws IOException
	{
		try
		{
			String path = "/models/" + p;
			URL url = context.getResource(path);
			if (url == null)
				return null;
			InputStream in = url.openStream();

			byte[] serialization = FileUtils.readBytes(in, true);
			return serialization;
		}
		catch (Exception e)
		{
			throw new ModelException("Failed to read resource " + p, e);
		}
	}

	protected boolean exists(String p) throws IOException
	{
		String path = "/models/" + p;
		URL url = context.getResource(path);
		return url != null;
	}

	protected void save(ModelId modelId, byte[] bytes)
	{
		throw new RuntimeException(
				"Can't write models to web repository (WebRepository is read only)");
	}

	protected void writeResource(String path, byte[] bytes) throws IOException
	{
		throw new RuntimeException(
				"Can't write resources to web repository (WebRepository is read only)");
	}

	protected List getSavedSubPackageIds(PackageId parent)
	{
		String parentPath = "/models/" + (parent == null ? "" : parent.getPath() + "/");
		// System.out.println("Listing children for "+parentPath + " ("+parent+")");
		Set childPathSet = context.getResourcePaths(parentPath);
		// System.out.println(childPathSet == null ? "Null Children":
		// childPathSet.size()+" children");
		if (childPathSet == null || childPathSet.isEmpty())
			return Collections.EMPTY_LIST;
		ArrayList childPathList = new ArrayList();
		childPathList.addAll(childPathSet);
		Collections.sort(childPathList);
		ArrayList subPackageIds = new ArrayList();
		for (int i = 0; i < childPathList.size(); i++)
		{
			String childPath = (String) childPathList.get(i);
			StringTokenizer tok = new StringTokenizer(childPath, "/");
			String childName = null;
			while (tok.hasMoreTokens())
				childName = tok.nextToken();
			if (ignoredFiles.contains(childName))
				continue;
			PackageId subPackageId = null;
			if (childPath.endsWith("/"))
			{
				subPackageId = new PackageId(parent, childName);
			}
			else if (childName.endsWith(PACKAGE_SUFFIX))
			{
				subPackageId = new PackageId(parent, childName.substring(0, childName
						.lastIndexOf(PACKAGE_SUFFIX)));
			}
			if (subPackageId != null && !subPackageIds.contains(subPackageId))
				subPackageIds.add(subPackageId);
		}
		return subPackageIds;
	}

	public boolean packageFolderExists(PackageId packageId)
	{
		return false;
	}

	public boolean isFileReadOnly(PackageId packageId) throws IOException
	{
		return true;
	}

	public boolean isPackageFolderReadOnly(PackageId packageId) throws IOException
	{
		return true;
	}
}

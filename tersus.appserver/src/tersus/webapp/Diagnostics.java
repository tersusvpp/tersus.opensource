/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.webapp;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tersus.runtime.RuntimeContext;

/**
 * @author Youval Bronicki
 * 
 */
public class Diagnostics extends HttpServlet
{

	/**
     * 
     */
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		ContextPool pool = (ContextPool) getServletContext().getAttribute(ContextPool.POOL_KEY);
		response.setContentType("text/plain");
		response.addHeader("Pragma", "no-cache");
		response.addHeader("Cache-control", "no-cache");

		if (!pool.isTrustedAddress(request.getRemoteAddr()))
			return;
		PrintWriter out = response.getWriter();
		if ("cleanup".equals(request.getParameter("command")))
		{
			String givenPassword = request.getParameter("password");
			String password = getServletContext().getInitParameter("control_password");
			password="1";
			if (givenPassword != null && givenPassword.equals(password))
			{
				out.println("Cleanup ...");
				long maxIdleTime = 10 * 60 * 1000; // 10 Minutes
				String s = request.getParameter("timeout");
				if (s != null)
					maxIdleTime = Integer.parseInt(s);
				synchronized (pool)
				{
					for (RuntimeContext c : pool.getUsedContextsCopy())
					{
						long idleTime = System.currentTimeMillis() - c.getLastActivityTime();
						if (idleTime > maxIdleTime)
						{
							String message = getIdleTimeStr(idleTime);

							out.println("Aborting " + c + message);
							c.abort("Cleaned up " + message);
							
							c.disposeAndCloseBaseConnections();
						}
					}
				}
				out.println();
			}
		}
		out.println("Max Memory: " + Runtime.getRuntime().maxMemory());
		out.println("Free Memory: " + Runtime.getRuntime().freeMemory());
		out.println("Total Memory: " + Runtime.getRuntime().totalMemory());
		out.println("\nUsed contexts:");
		long now = System.currentTimeMillis();
		for (RuntimeContext c : pool.getUsedContextsCopy())
		{

			String loggedInUser = null;
			try
			{
				loggedInUser = c.getLoggedInUser();
			}
			catch (Exception e)
			{
				loggedInUser = "Unknown";
			}
			out.println(c.toString() + " " + "Request Id:"+c.getRequestId()+ " requestType:"+c.getRequestType() + " User:" + loggedInUser + " Connections:"
					+ c.describeConnections() + " Start Time:"
					+ (new Date(c.getStartTime())) + getIdleTimeStr(now-c.getLastActivityTime())
					+ " currentPath:"+c.currentFlowPath + " baseModelId:" + c.getServiceModelId());
		}
		out.println("\nPooled contexts:");
		for (RuntimeContext c : pool.getPooledContextsCopy())
		{
			out.println(c.toString() + " " + c.getRequestType() + " Last Start Time:"
					+ (new Date(c.getStartTime())) + " Last Duration:" + (c.getEndTime() - c.getStartTime())
					+ "ms " + c.getServiceModelId());
		}

		out.println("\nComplete server tracing: " + (pool.getCompleteTracing() ? "on" : "off"));
	}

	public String getIdleTimeStr(long idleTime)
	{
		String message;
		if (idleTime < 10000)
			message = " (idle for " + idleTime + " milliseconds)";
		else if (idleTime < 120000)
			message = " (idle for " + (idleTime / 100) / 10.0 + " seconds)";
		else if (idleTime < 3600 * 2 * 1000)
			message = " (idle for " + (idleTime / 6000) / 10.0 + " minutes)";
		else
			message = " (idle for " + (idleTime / 360000) / 10.0 + " hours)";
		return message;
	}
}

package tersus.webapp;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import tersus.model.ModelId;
import tersus.model.Path;
import tersus.util.EscapeUtil;

public class LoggingHelper
{
    public static final String PROCESS_MODEL_ID = "ProcessModelId";
	private static final String PROCESS_TYPE = "ProcessType";
	public static final String PROCESS_PATH = "ProcessPath";
	private static final String BASE_MODEL_ID = "BaseModelId";
	public static final String REQUEST_URL = "RequestURL";
	public static final String SERVLET_PATH = "ServletPath";
	public static final String USER_ID = "UserId";
	public static final String REQUEST_ID = "RequestId";
	private static final String LINE_SEPARATOR = System.getProperty("line.separator");
    private static final String OK = "OK";
    private static final String ERROR = "Error";
    private int sqlSequence;
    
    
    private static ThreadLocal<LoggingHelper> threadLocal = new ThreadLocal<LoggingHelper>()
    {

        @Override
        protected LoggingHelper initialValue()
        {
            return new LoggingHelper();
        }

    };

    public static LoggingHelper get()
    {
        return threadLocal.get();
    }
    
    public static void dispose()
    {
    	threadLocal.remove();
    }

    private long startTime;
    private int tsSequence = 0;
    private long processStartTime;
    private long processEndTime;
    private String requestId;
    private String userId;
    private ModelId baseModelId;
    private String requestURL;
    private Path processPath;
    private ModelId processModelId;
    private Logger activityLog;
    private String status;
    private Throwable error;
    private String processType;
    private TersusTimeFormat format = new TersusTimeFormat();
    private String servletPath;
    private Logger errorLog;
    private Path errorLocation;
	public static final String LOCATION = "Location";
	public static final String ERROR_DETAILS = "ErrorDetails";

    public void startRequest(Logger activityLogger,Logger errorLogger, HttpServletRequest request)
    {
        error = null;
        processPath = null;
        processModelId = null;
        processType = null;
        requestId = null;
        processStartTime = 0;
        processEndTime = 0;
        servletPath = null;
        status = null;
        
        start(activityLogger, errorLogger);
        if (request != null)
        {
            requestURL = request.getRequestURL().toString();
            String queryString = request.getQueryString();
            if (queryString !=null)
                requestURL+='?'+queryString;
            userId = request.getRemoteUser();
            if (userId == null)
            {
            	try
            	{
            		HttpSession session = request.getSession();
            		userId = (String) session.getAttribute(ContextPool.REMOTE_USER);
            	}
            	catch (IllegalStateException e)
            	{
            		userId = null;
            	}
            }
            servletPath = request.getServletPath();
        }
        if (activityLogger.isInfoEnabled() && request!=null)
        {
            log("Request",startTime);
        }
        MDC.clear();
        if (requestId!=null)
        	MDC.put(REQUEST_ID, requestId);
        if (userId != null)
        	MDC.put(USER_ID, userId);
        if (servletPath != null)
        	MDC.put(SERVLET_PATH, servletPath);
        if (requestURL != null)
        	MDC.put(REQUEST_URL, requestURL);
    }

    public void log (String event, long time)
    {
        StringBuffer message = new StringBuffer();
        append(message,event);
        append(message,time-startTime);
        append(message,requestId);
        append(message,status);
        append(message,userId);
        append(message,servletPath);
        append(message,requestURL);
        append(message,processType);
        append(message,baseModelId);
        append(message,processPath);
        append(message,processModelId);
        append(message,processEndTime - processStartTime);
        if (error != null)
        {
        	append(message, error.getClass().getName());
        	append(message, error.getMessage());
        	append(message, errorLocation);
        	StringWriter st  = new StringWriter();
        	error.printStackTrace(new PrintWriter(st));
        	append(message, st.toString());
        }
        
        activityLog.info(message);
    }
    public void error()
    {
        StringBuffer message = new StringBuffer();
        append(message,"Request ID",requestId);
        append(message,"User ID", userId);
        append(message,"Error Location",errorLocation);
        append(message,"Servlet Path", servletPath);
        append(message,"Request URL", requestURL);
        append(message,"Process Type", processType);
        append(message,"Base Model Id", baseModelId);
        append(message,"Process Path", processPath);
        append(message,"Process Model Id",processModelId);
        
        errorLog.error(message,error);
    }
    private static void append(StringBuffer b, Path path)
    {
        if (path == null)
            b.append("-");
        else
        {
            b.append('/');
            b.append(path);
        }
        b.append('\t');
    }
    private static void append(StringBuffer b, Object obj)
    {
        if (obj != null)
            b.append(EscapeUtil.escape(obj.toString(), true));
        else
            b.append("-");
        b.append("\t");
    }
    private static void append(StringBuffer b, String message, Object obj)
    {
        if (obj != null)
        {   b.append(message);
            b.append(':');
            b.append(obj);
            b.append(LINE_SEPARATOR);
        }
    }
    public void endProcess()
    {
        endProcess(OK, null);
    }

    public void endProcess(Throwable e, Path errorLocation)
    {
        this.errorLocation = errorLocation;
        endProcess(ERROR, e);
    }

    public void endProcess(String status, Throwable e)
    {
        this.error = e;
        this.userId = (String)MDC.get(USER_ID);
        if (this.error != null)
            error();
        this.status = status;
        long now = System.currentTimeMillis();
        this.processEndTime = now;
        if (activityLog.isInfoEnabled())
        {
            log("FINISH", now);
        }

    }

    public void endRequest()
    {
        endRequest((Throwable) null);
    }

    public void endRequest(Throwable e)
    {
        long now = System.currentTimeMillis();
        if (this.error==null && e != null)
        {
        	setError(e);
            error();
        }
        if (activityLog.isInfoEnabled())
        {
            log("END",now);
        }
    }

    private void start(Logger activityLog, Logger errorLog)
    {
        long now = System.currentTimeMillis();
        if (now == startTime)
            ++tsSequence;
        else
        {
            startTime = now;
            tsSequence = 0;
        }
        sqlSequence = 1;
        requestId = formatTime(startTime) + " [" + Thread.currentThread().getName() + ']';
        if (tsSequence > 0)
            requestId += '-' + tsSequence;
        this.activityLog = activityLog;
        this.errorLog = errorLog;
    }

    public long getStartTime()
    {
        return startTime;
    }
    public int getSQLSequenceNumber()
    {
        return (sqlSequence++);
    }
    private String formatTime(long time)
    {
        return format.format(time);
    }

    public void startProcess(String processType, ModelId baseModelId, Path path, ModelId processModelId)
    {
        long now = System.currentTimeMillis();
        processStartTime = now;
        processEndTime = now;
        this.baseModelId = baseModelId;
        this.processPath = path;
        this.processType = processType;
        this.processModelId = processModelId;
        if (activityLog.isInfoEnabled())
            log("START", now);
        if (this.baseModelId != null)
        	MDC.put(BASE_MODEL_ID, this.baseModelId);
        if (this.processPath != null)
        	MDC.put(PROCESS_PATH, this.processPath);
        if (this.processType != null)
        	MDC.put(PROCESS_TYPE, this.processType);
        if (this.processModelId != null)
        	MDC.put(PROCESS_MODEL_ID, this.processModelId);
    }

    public String getRequestId()
    {
        return requestId;
    }

	public void setError(Throwable e)
	{
		error = e;
		status = ERROR;
		
	}

}

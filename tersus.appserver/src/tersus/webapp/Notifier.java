package tersus.webapp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.catalina.comet.CometEvent;
import org.apache.catalina.comet.CometProcessor;
import org.apache.log4j.Logger;


public class Notifier extends HttpServlet implements CometProcessor
{

    protected HashMap<String, HttpServletResponse> connections = new HashMap<String, HttpServletResponse>();
    protected HashMap<String, HashSet<String>> subscriptions = new HashMap<String, HashSet<String>>();

    protected ArrayList<String> messages = new ArrayList<String>();

    protected NotificationServer server;

    
    protected HashMap<HttpServletRequest, ByteArrayOutputStream> requestBuffers;
    private Logger logger;
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
	public static final String KEY = "NOTIFIER";

    public void init()
    {
    	getServletContext().setAttribute(KEY, this);
        final ContextPool pool = (ContextPool) getServletContext().getAttribute(ContextPool.POOL_KEY);
        if (pool == null)
        	return;
        server = NotificationServer.getInstance(pool);
        requestBuffers = new HashMap<HttpServletRequest, ByteArrayOutputStream>();
        logger = pool.getLogger(getClass().getName());
    }

    public void event(CometEvent event) throws IOException, ServletException
    {
        HttpServletRequest request = event.getHttpServletRequest();
        HttpServletResponse response = event.getHttpServletResponse();
        if (logger.isDebugEnabled())
        {
            logger.debug("Processing event:"+event.getEventType()+"/"+event.getEventSubType()+" method="+request.getMethod()+ " remoteHost="+request.getRemoteHost());
        }
        try
        {
        response.setContentType("text/plain");
        response.setCharacterEncoding(Engine.ENCODING);
        response.addHeader("Pragma", "no-cache");
        response.addHeader("Cache-control", "no-cache");
        if ("GET".equals(request.getMethod()))
        {
            if (event.getEventType() == CometEvent.EventType.BEGIN)
            {
                event.setTimeout(60 * 1000 * 10);
                String action = request.getParameter("action");
                if ("connect".equals(action))
                    server.connect(request, response);
                else if ("subscribe".equals(action))
                    server.subscribe(request, response);
                else if ("disconnect".equals(action))
                    server.disconnect(request, response);

            }
            else if (event.getEventType() == CometEvent.EventType.ERROR)
            {
                server.error(request, response);
                event.close();
            }
            else if (event.getEventType() == CometEvent.EventType.END)
            {
                server.end(request, response);
                event.close();
            }
            else if (event.getEventType() == CometEvent.EventType.READ)
            {
                // We don't read anything ...
            }
        }
        else if ("POST".equals(request.getMethod()))
        {
            ByteArrayOutputStream buffer;
            synchronized (requestBuffers)
            {
                buffer = requestBuffers.get(request);
                if (buffer == null)
                {
                    buffer = new ByteArrayOutputStream();
                    requestBuffers.put(request, buffer);
                }
            }
            if (event.getEventType() == CometEvent.EventType.READ)
            {
                ServletInputStream is = request.getInputStream();
                byte[] buf = new byte[512];
                do {
                    int n = is.read(buf); //can throw an IOException
                    if (n > 0) {
                        buffer.write(buf, 0, n);
                        System.out.println("Read "+n +" bytes for request:"+request + " response:"+response);
                    } else if (n < 0) {
                        throw new NotificationServerException("Error reading request",null);
                    }
                } while (is.available() > 0) ;      
                System.out.println("buffer size:"+ buffer.size() + " content length:"+request.getContentLength());
                if (buffer.size() == request.getContentLength())
                {
                    server.handlePostedMessages(buffer.toString(Engine.ENCODING));
                    server.respond(response, "OK");
                    buffer.reset();
                }
            }
            else if (event.getEventType() == CometEvent.EventType.END  || event.getEventType() == CometEvent.EventType.ERROR)
            {
                requestBuffers.remove(request);
            }
            
        }
        }
        finally
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Finished processing event:"+event.getEventType()+"/"+event.getEventSubType()+" method="+request.getMethod()+ " remoteHost="+request.getRemoteHost());
            }

        }
    }
}

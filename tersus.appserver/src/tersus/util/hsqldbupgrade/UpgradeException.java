package tersus.util.hsqldbupgrade;

public class UpgradeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UpgradeException() {
	}

	public UpgradeException(String message) {
		super(message);
	}

	public UpgradeException(Throwable cause) {
		super(cause);
	}

	public UpgradeException(String message, Throwable cause) {
		super(message, cause);
	}

}

package tersus.util.hsqldbupgrade;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.xeustechnologies.jcl.JarClassLoader;

import tersus.util.FileUtils;
import tersus.util.SQLUtils;

public class Upgrader
{

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception
	{
		if (args.length == 0 || args[0].startsWith("-h"))
		{
			System.out.println("Usage:  Upgrader <path to db>");
			return;
		}
		Upgrader upgrader = new Upgrader(args[0]);
		upgrader.verbose = true;
		upgrader.upgrade();
	}

	private String dbPath;
	private Connection conn;
	private Statement stmt;
	private boolean verbose = false;

	public Upgrader(String dbPath)
	{
		this.dbPath = dbPath;
	}

	public void upgrade()
	{
		conn = null;
		stmt = null;
		FileInputStream is = null;
		Properties p = new Properties();
		String version;
		try
		{
			is = new FileInputStream(dbPath + ".properties");
			p.load(is);
			is.close();
			version = p.getProperty("version");
		}
		catch (Exception e)
		{
			throw new UpgradeException("Failed to load database properties", e);
		}
		if (verbose)
			System.out.println("Current version: " + version);

		try
		{
			if (version.compareTo("1.7.3") <= 0)
			{
				if (verbose)
					System.out.println("Upgrading from " + version);
				open("hsqldb-1.7.3.jar");
				stmt.execute("SET SCRIPTFORMAT TEXT");
				stmt.execute("SHUTDOWN SCRIPT");
				close();
			}
			if (version.compareTo("2.0.0") < 0)
			{
				if (verbose)
					System.out.println("Upgrading from 1.8.1");
				open("hsqldb-1.8.1.jar");
				stmt.execute("SET SCRIPTFORMAT TEXT");
				stmt.execute("SHUTDOWN SCRIPT");
				close();
			}
			if (verbose)
				System.out.println("Database is ready for 2.0.0");
		}
		catch (SQLException e)
		{
			throw new UpgradeException(e);
		}
		finally
		{
			FileUtils.forceClose(is);
			forceClose();
		}

	}

	private void forceClose()
	{
		SQLUtils.forceClose(stmt);
		SQLUtils.forceClose(conn);
	}

	private void close() throws SQLException
	{
		stmt.close();
		stmt = null;
		conn.close();
		conn = null;
	}

	public void open(String driverName)
	{
		Driver driver;
		try
		{
			if (driverName != null)
			{
				JarClassLoader loader = new JarClassLoader();
				loader.add(getClass().getResource(driverName));
				driver = (Driver) loader.loadClass("org.hsqldb.jdbcDriver").newInstance();
			}
			else
				driver = (Driver) Class.forName("org.hsqldb.jdbcDriver").newInstance();
		}
		catch (Exception e)
		{
			throw new UpgradeException("Failed to load driver", e);
		}
		Properties p = new Properties();
		try
		{
			conn = driver.connect("jdbc:hsqldb:file:" + dbPath, p);
			if (verbose) 
				System.out.println("Using " + conn.getMetaData().getDatabaseProductName() + " "
					+ conn.getMetaData().getDatabaseProductVersion());
			stmt = conn.createStatement();
		}
		catch (SQLException e)
		{
			throw new UpgradeException("Can't get connection", e);
		}
	}

}

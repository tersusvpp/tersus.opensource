@echo off

rem Get full path
rem (Running command.com before retrieving %~sf1 overcomes a well known bug in conversion of long to short name)
command /c rem
set DBPATH=%~sf1

rem Change to the batch file directory so we can find the jar files
cd "%~dp0"

java -classpath ..\lib\log4j-1.2.16.jar;..\lib\jcl-core-2.1.2.jar;..\lib\tersus.jar tersus.util.hsqldbupgrade.Upgrader %DBPATH%

    var apiVersion = "v23.0";
    var forcetkClient;
    var debugMode = true;

    jQuery(document).ready(function() {
        //Add event listeners and so forth here
        SFHybridApp.logToConsole("onLoad: jquery ready");
		document.addEventListener("deviceready", onDeviceReady,false);

    });

    // When this function is called, PhoneGap has been initialized and is ready to roll 
    function onDeviceReady() {
        SFHybridApp.logToConsole("onDeviceReady: PhoneGap ready");
		//Call getAuthCredentials to get the initial session credentials
        SalesforceOAuthPlugin.getAuthCredentials(salesforceSessionRefreshed, getAuthCredentialsError);

        //register to receive notifications when autoRefreshOnForeground refreshes the sfdc session
        document.addEventListener("salesforceSessionRefresh",salesforceSessionRefreshed,false);

        //enable buttons
        regLinkClickHandlers();
    }
        

    function salesforceSessionRefreshed(creds) {
        SFHybridApp.logToConsole("salesforceSessionRefreshed");
		
        forcetkClient = new forcetk.Client(creds.clientId, creds.loginUrl);
        forcetkClient.setSessionToken(creds.accessToken, apiVersion, creds.instanceUrl);
        forcetkClient.setRefreshToken(creds.refreshToken);
        forcetkClient.setUserAgentString(creds.userAgent);
    }


    function getAuthCredentialsError(error) {
        SFHybridApp.logToConsole("getAuthCredentialsError: " + error);
    }


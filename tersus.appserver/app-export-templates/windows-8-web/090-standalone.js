tersus.isNative=true;
tersus.preload = function (packages)
{
	tersus.repository = new ConstructorRepository();
    tersus.repository.packageTexts = packages;
};
tersus.init = function()
{
	tersus.async.timer = setInterval(tersus.async.worker, tersus.async.interval);
	tersus.createClassNameList();
	window.onresize = tersus.onResize;
};

tersus.checkUpdate = function()
{
};

(function()
{
    var load = tersus.loader.load;
    tersus.loader.load = function()
    {
        MSApp.execUnsafeLocalFunction(load);
    };
    tersus.loadView = function loadView(targetElement, view)
    {
        tersus.progress.set('Loading view - '+view.name);
        tersus.async.exec(function(){ MSApp.execUnsafeLocalFunction(function(){tersus._loadView(targetElement, view);});},false);
    }
})();
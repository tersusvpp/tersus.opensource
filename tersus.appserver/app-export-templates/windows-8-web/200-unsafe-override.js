(function()
{
    // A messy way to overcome Microsoft 1701 errors - wrap relevant functions with MSApp.execUnsafeLocalFunction
    //TBD - find the underlying code
    var a = DisplayNode.prototype.attachViewNode;
    DisplayNode.prototype.attachViewNode = function()
    {
        var self=this;
        MSApp.execUnsafeLocalFunction(function() { a.call(self);});
    };
    
    if (tersus.UIDialog)
    {
        var b = tersus.UIDialog.prototype.attachViewNode;
        tersus.UIDialog.prototype.attachViewNode = function()
        {
            var self=this;
            MSApp.execUnsafeLocalFunction(function() { b.call(self);});
        };
    }
    
    var c = DisplayElement.prototype.replaceChild;
    DisplayElement.prototype.replaceChild = function(parent, value, reset)
    {
        var self = this;
        MSApp.execUnsafeLocalFunction(function() { c.call(self, parent, value, reset);});
    };
    
    var d = tersus.HTMLTag.prototype.replaceDomNode;
    tersus.HTMLTag.prototype.replaceDomNode = function (newNode,prevNode)
    {
        var self = this;
        MSApp.execUnsafeLocalFunction(function() { d.call(self, newNode, prevNode);});        
    };
    tersus.HTMLTag.prototype.createDomNodeFromHTML = function(html)
    {
	var doc = this.currentWindow.document;
	return  MSApp.execUnsafeLocalFunction(function() { return $(html,doc)[0];}); 
    };
    tersus.UIHTMLDisplay.prototype.setValueString = function()
    {
        var self = this;
        MSApp.execUnsafeLocalFunction(function() {
            self.valueStr = '';
            if (self.value != null)
            {
                 self.valueStr = self.value.toString();
            }
            if (self.viewNode)
            {
                if (self.textContainer)
                    self.textContainer.innerHTML = toStaticHTML(self.valueStr);
            }            
        });               
    }
    
})();

tersus.isNative=true;
tersus.preload = function (packages)
{
	tersus.repository = new ConstructorRepository();
    tersus.repository.packageTexts = packages;
};
tersus.init = function()
{
	if (! tersus.isIPhoneSafari)
		tersus.async.exec(tersus.initServiceResponseFrames);
	if (window.browser == 'IE')
		window.document.body.onunload = tersus.onUnload;
	else
		window.onunload = tersus.onUnload;
	
	tersus.async.timer = setInterval(tersus.async.worker, tersus.async.interval);
	tersus.keepAliveTimer = setInterval(tersus.keepAlive, tersus.KEEP_ALIVE_INTERVAL);
			
	tersus.createClassNameList();
	window.onresize = tersus.onResize;
	if (tersus.settings.dev_mode)
		tersus.updateCheckInterval = tersus.DEV_UPDATE_CHECK_INTERVAL;
	else
		tersus.updateCheckInterval = tersus.PROD_UPDATE_CHECK_INTERVAL;
};

tersus.checkUpdate = function()
{
};

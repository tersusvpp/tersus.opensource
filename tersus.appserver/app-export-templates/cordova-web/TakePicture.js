tersus.SAMPLE_IMAGE='data:image/png;base64,\niVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAIAAAD/gAIDAAAAAXNSR0IArs4c6QAAANNJREFUeNrt\n3NEJgDAMQEErruGgOoAO6iB1hUoJsfTevx8eERpBS611UVsrAliwYMGCBQsBLFiwYMGChQAWLFiw\nYMFC0N7Wc/G5HyPe8/XcJstjCAsWLMGCBQvWjCf4oINyZ3F7hcmCBQsWLFiCBQsWLOtO8FKStUiZ\nLFiwYMGCJViwYMGCBUuwYMGCBQuWYH0o7R181nt0kwULFixYggUL1lSH0kG/czVZsGDBggVLsGDB\n+nPF/+BNFixYsGDBQgALFixYsGAhgAULFixYsBDACukFZWESy0+VwUsAAAAASUVORK5CYII=\n';
tersus.newAction('TakePicture', function()
{
	var dataURLExit = this.getExit('<Data URL>');
	var fileURIExit = this.getExit('<File URI>');
	var imgQuality=70;
	var q=this.getLeafChild('<Quality>');
	if (q)
	{
		q = Math.round(q);
		
		if (q>0 && q<=100)
			imgQuality = q;
	}
	
	var w = this.getLeafChild('<Width>');
	var h = this.getLeafChild('<Height>');
	
	var dest = dataURLExit ?  Camera.DestinationType.DATA_URL : Camera.DestinationType.FILE_URI;
	
	/*if (tersus.simulateCamera != false)
	{
		if(window.confirm('No camera available.  Would you like to use a sample image?'))
			this.chargeLeafExit('<Data URL>',tersus.SAMPLE_IMAGE);
	}
	else */if (navigator.camera && navigator.camera.getPicture)
	{
		var node = this;
		this.pause();
		var success = function(data)
		{
			console.log('Got Image '+data);			
			if (dataURLExit)
				node.chargeLeafExit(dataURLExit,"data:image/jpeg;base64," + data.replace(/\n/g,''));
			else
			{
				node.chargeLeafExit(fileURIExit, data);
			}
			node.continueExecution();
			console.log('Got Image (done)');			
			
		}
		var error = function(message)
		{
			if (message == 'no image selected' || message == 'Cancelled' || message == 'Camera cancelled.' || message == 'has no access to assets' || message == "User didn't capture a photo.")
			{
				node.chargeEmptyExit('<None>');
			}
			else
			{
				console.log('Get Picture: Error: '+message);			
				modelExecutionError("Failed to take picture: "+message,node);
			}
			node.continueExecution();
		};
		
		var p = {quality:imgQuality, destinationType:dest};
		
		if (w)
			p.targetWidth=w+0;
		if (h)
			p.targetHeight=h+0;

		console.log('Calling getPicture w='+p.targetWidth+' h='+p.targetHeight +" quality="+p.quality);			
		navigator.camera.getPicture(success, error, p); 
		
	}
	else
		modelExecutionError('No camera available.');		
});

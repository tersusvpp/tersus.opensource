(function()
{
    var load = tersus.loader.load;
    tersus.loader.load = function()
    {
        MSApp.execUnsafeLocalFunction(load);
    };
    tersus.loadView = function loadView(targetElement, view)
    {
        tersus.progress.set('Loading view - '+view.name);
        tersus.async.exec(function(){ MSApp.execUnsafeLocalFunction(function(){tersus._loadView(targetElement, view);});},false);
    }
})();

tersus.isNative=true;
tersus.preload = function (packages)
{
	tersus.repository = new ConstructorRepository();
    tersus.repository.packageTexts = packages;
};
tersus.init = function()
{
	tersus.async.timer = setInterval(tersus.async.worker, tersus.async.interval);
	tersus.createClassNameList();
	window.onresize = tersus.onResize;
};

tersus.checkUpdate = function()
{
};


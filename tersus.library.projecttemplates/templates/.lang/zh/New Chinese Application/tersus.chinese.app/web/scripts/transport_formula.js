function transport_gongshi(district,puhuo,weihuo,fzc) {
	var puhuo_gongshi = "无普货公式"; //普货公式
	var weihuo_gongshi = "无危货公式"; //危货公式
	var puhuo_total = 0; //普货合计
	var weihuo_total = 0; //危货合计
	var total = 0; //总合计

	switch(district) {
     case "上海":
		//普货部分
		if (puhuo==0) {		
			puhuo_gongshi ="0元";
		}
        else if (puhuo*fzc<=2) {		
			puhuo_total=900; //900元(x * 1.09未超过2吨)
			puhuo_gongshi ="900元("+puhuo+"*"+fzc+"未超过2吨)";
		}
		else if (puhuo<=5) {
			puhuo_total=puhuo*350*fzc; //普货吨数小于等于5吨=x * 350元/吨 * 1.09
			puhuo_gongshi =puhuo+"*350元/吨*"+fzc;
		}
		else if (puhuo<=10) {
			puhuo_total=puhuo*250*fzc; //普货吨数小于等于10吨=x * 250元/吨 * 1.09
			puhuo_gongshi =puhuo+"*250元/吨*"+fzc;
		}
		else {
			puhuo_total=puhuo*200*fzc; //x * 200元/吨 * 1.09
			puhuo_gongshi =puhuo+"*200元/吨*"+fzc;
		}
		// 危货部分
		if (weihuo==0) {		
			weihuo_gongshi ="0元";
		}	
		else if (weihuo*fzc<=5) {	
			weihuo_total=3100;  	//3100元(X * 1.09未超过5吨)
			weihuo_gongshi ="3100元("+weihuo+"*"+fzc+"未超过5吨)";
		}
		else if (weihuo<=5) {
			weihuo_total=3100+(5-weihuo)*fzc*620;	//危货吨数小于等于5吨=3100元 + (5-X*1.09) * 620元/吨
			weihuo_gongshi ="3100元+"+(5-weihuo).toFixed(2)+"*"+fzc+"*620元/吨";
		}
		else if (weihuo<=10) {
			weihuo_total=(3100+(weihuo-5)*620)*fzc;	//危货吨数小于等于10吨=(3100元 + (x-5)*620元/吨)*1.09
			weihuo_gongshi ="(3100元+"+(weihuo-5).toFixed(2)+"*620元/吨)*"+fzc;
		}
		else {
			weihuo_total=(3100+(weihuo-5)*600)*fzc ;	//(3100元 + （X-5） * 600元/吨) * 1.09
			weihuo_gongshi ="(3100元+"+(weihuo-5).toFixed(2)+"*600元/吨)*"+fzc;
		}
        break;
		
		case "福州":
		//普货部分
		if (puhuo==0) {		
			puhuo_gongshi ="0元";
		}
        else if (puhuo*fzc<=2) {		
			puhuo_total=900; //900元(x * 1.09未超过2吨)
			puhuo_gongshi ="900元("+puhuo+"*"+fzc+"未超过2吨)";
		}
		else if (puhuo<=5) {
			puhuo_total=puhuo*350*fzc; //普货吨数小于等于5吨=x * 350元/吨 * 1.09
			puhuo_gongshi =puhuo+"*350元/吨*"+fzc;
		}
		else if (puhuo<=10) {
			puhuo_total=puhuo*250*fzc; //普货吨数小于等于10吨=x * 250元/吨 * 1.09
			puhuo_gongshi =puhuo+"*250元/吨*"+fzc;
		}
		else {
			puhuo_total=puhuo*200*fzc; //x * 200元/吨 * 1.09
			puhuo_gongshi =puhuo+"*200元/吨*"+fzc;
		}
		// 危货部分
		if (weihuo==0) {		
			weihuo_gongshi ="0元";
		}	
		else if (weihuo*fzc<=2) {	
			weihuo_total=4600;  	//4600元
			weihuo_gongshi ="4600元";
		}
		else if (weihuo<=5) {
			weihuo_total=weihuo*fzc*2300;	//危货吨数小于等于5吨=5.00 * 2300元/吨 * 1.09
			weihuo_gongshi =weihuo+"*2300元/吨*"+fzc;
		}
		else if (weihuo<=10) {
			weihuo_total=weihuo*fzc*1900;	//危货吨数小于等于10吨=(10.00 * 1900元/吨) * 1.09
			weihuo_gongshi =weihuo+"*1900元/吨*"+fzc;
		}
		else {
			weihuo_total=weihuo*fzc*1700; 	//((200.00 * 1700元/吨) * 1.09
			weihuo_gongshi =weihuo+"*1700元/吨*"+fzc;
		}
        break;
    default:
        break;
	}
	total=(puhuo_total+weihuo_total);
	puhuo_total =parseFloat(puhuo_total.toFixed(4));	//toFixed(4)取4位小数,parseFloat取小数点后面无用的0
	weihuo_total = parseFloat(weihuo_total.toFixed(4));
	total =parseFloat(total.toFixed(4));
	var wuliu ={"普货公式":puhuo_gongshi,"危货公式":weihuo_gongshi,"普货合计":puhuo_total,"危货合计":weihuo_total,"总合计":total}; //JSON对象
	return JSON.stringify(wuliu); //返回JSON字符串
}
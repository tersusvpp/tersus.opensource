/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.htmltemplates;

import tersus.editor.actions.AbstractSelectionAction;
import tersus.model.Composition;
import tersus.model.DataType;
import tersus.model.FlowModel;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.PackageId;
import tersus.model.Slot;
import tersus.model.commands.MultiStepCommand;
import tersus.model.commands.SynchronizeDisplayCommand;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Ofer Brandes
 * 
 */
public class SynchronizeDisplayAndDataAction extends AbstractSelectionAction
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.editor.actions.AbstractSelectionAction#calculateEnabled()
	 */
	protected boolean calculateEnabled()
	{
		Model model = getModel();
		if (model == null)
			return false;
		return isAutomaticallyGeneratedData(model) || isAutomaticallyGeneratedExtract(model)
				|| isAutomaticallyGeneratedPopulate(model);
	}

	private boolean isAutomaticallyGeneratedData(Model model)
	{
		return ((model instanceof DataType)
				&& (((DataType) model).getComposition() != Composition.ATOMIC) && (Constants.DATA
				.equals(model.getPackage().getPackageId().getName())));
	}

	private boolean isAutomaticallyGeneratedExtract(Model model)
	{
		return ((model instanceof FlowModel)
				&& (model.getElement(SynchronizeDisplayCommand.DISPLAY_ROLE) != null)
				&& (model.getElement(SynchronizeDisplayCommand.DATA_ROLE) != null) && (SynchronizeDisplayCommand.EXTRACT
				.equals(model.getPackage().getPackageId().getName())));
	}

	private boolean isAutomaticallyGeneratedPopulate(Model model)
	{
		return ((model instanceof FlowModel)
				&& (model.getElement(SynchronizeDisplayCommand.DATA_ROLE) != null)
				&& (model.getElement(SynchronizeDisplayCommand.DISPLAY_ROLE) != null) && (SynchronizeDisplayCommand.POPULATE
				.equals(model.getPackage().getPackageId().getName())));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.editor.actions.AbstractSelectionAction#run()
	 */
	public void run()
	{
		Model model = getModel();
		PackageId parentPackage = model.getPackage().getPackageId().getParent();
		String parentPackagePath = parentPackage.getPath();

		String dataModelName = null;
		ModelId displayModelId = null;
		ModelId dataModelId = null;
		ModelId extractModelId = null;
		ModelId populateModelId = null;

		if (isAutomaticallyGeneratedData(model))
		{
			dataModelId = model.getId();
			dataModelName = model.getName();
		}
		else if (isAutomaticallyGeneratedExtract(model))
		{
			extractModelId = model.getId();
			dataModelName = model.getName().substring(
					SynchronizeDisplayCommand.EXTRACT_PREFIX.length());
		}
		else if (isAutomaticallyGeneratedPopulate(model))
		{
			populateModelId = model.getId();
			dataModelName = model.getName().substring(
					SynchronizeDisplayCommand.POPULATE_PREFIX.length());
		}

		if (dataModelId == null)
		{
			String dataPath = parentPackagePath + "/" + Constants.DATA + "/" + dataModelName;
			dataModelId = new ModelId(dataPath);
		}

		if (extractModelId == null)
		{
			String extractPath = parentPackagePath + "/" + SynchronizeDisplayCommand.EXTRACT + "/"
					+ SynchronizeDisplayCommand.EXTRACT_PREFIX + dataModelName;
			extractModelId = new ModelId(extractPath);
		}

		if (populateModelId == null)
		{
			String populatePath = parentPackagePath + "/" + SynchronizeDisplayCommand.POPULATE
					+ "/" + SynchronizeDisplayCommand.POPULATE_PREFIX + dataModelName;
			populateModelId = new ModelId(populatePath);
		}

		WorkspaceRepository repository = getRepositoryManager().getRepository();
		FlowModel extractModel = (FlowModel) repository.getModel(extractModelId, true);
		Slot extractTrigger = (Slot) extractModel
				.getElement(SynchronizeDisplayCommand.DISPLAY_ROLE);
		displayModelId = extractTrigger.getDataTypeId();

		MultiStepCommand command = new SynchronizeDisplayAndDataCommand(repository, displayModelId,
				dataModelId, extractModelId, populateModelId);
		command.execute();
		if (command.modelChanged())
			getRepositoryManager().getCommandStack().execute(command); // This just adds the command
																		// to the stack (it's
																		// already been executed)
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.htmltemplates;

import java.util.List;

import tersus.model.BuiltinProperties;
import tersus.model.Composition;
import tersus.model.DataElement;
import tersus.model.DataType;
import tersus.model.DisplayModelWrapper;
import tersus.model.FlowModel;
import tersus.model.FlowType;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.Path;
import tersus.model.SubFlow;
import tersus.model.commands.SynchronizeDisplayCommand;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Ofer Brandes
 */
public class SynchronizeDisplayAndDataCommand extends SynchronizeDisplayCommand
{
	private ModelId displayModelId, dataModelId, extractModelId, populateModelId;

	public SynchronizeDisplayAndDataCommand(WorkspaceRepository repository, ModelId displayModelId,
			ModelId dataModelId, ModelId extractModelId, ModelId populateModelId)
	{
		super(repository);
		this.displayModelId = displayModelId;
		this.dataModelId = dataModelId;
		this.extractModelId = extractModelId;
		this.populateModelId = populateModelId;
	}

	public void run()
	{
		Model displayModel = repository.getModel(displayModelId, true);
		Model dataModel = repository.getModel(dataModelId, true);
		FlowModel extractModel = (FlowModel) repository.getModel(extractModelId, true);
		FlowModel populateModel = (FlowModel) repository.getModel(populateModelId, true);

		syncCompositeData(displayModel, dataModel);
		syncCompositeExtract(displayModel, DISPLAY_ROLE, INPUT_STRUCTURE_ROLE, null, dataModel, null,
				DATA_ROLE, OUTPUT_STRUCTURE_ROLE, extractModel, false, false, false);
		syncCompositePopulate(dataModel, null, DATA_ROLE, INPUT_STRUCTURE_ROLE, displayModel,
				DISPLAY_ROLE, new Path(OUTPUT_STRUCTURE_ROLE), null, populateModel, false, false, false);
	}

	private void syncCompositeData(Model displayModel, Model dataModel)
	{
		List<ModelElement> displayElements = displayModel.getElements();
		for (int i = 0; i < displayElements.size(); i++)
		{
			ModelElement displayElement = (ModelElement) displayElements.get(i);
			DataElement dataElement = (DataElement) dataModel.getElement(displayElement.getRole());

			Model displaySubModel = displayElement.getReferredModel();
			DataType dataSubModel = (dataElement == null ? null : dataElement.getModel());

			int prevHistorySize = history.size();

			// Ensure there is a corresponding data model
			if (dataSubModel == null)
			{
				// BUG1 Handle recursion!!!

				// The display element refers to a data type
				if (displayElement instanceof DataElement)
					dataSubModel = (DataType) displaySubModel;
				// The display element refers to a "real" display model
				else if ((displayElement instanceof SubFlow)
						&& (displaySubModel.getProperty(BuiltinProperties.TYPE) == FlowType.DISPLAY))
				{
					// The display element corresponds to a composite data type
					if (DisplayModelWrapper
							.correspondsToCompositeDataType((FlowModel) displaySubModel))
					{
						ModelId dataSubModelId = new ModelId(dataModel.getPackage().getPackageId(),
								displaySubModel.getName());
						dataSubModel = (DataType) createModelFromTemplateOrPrototype(Constants.DATA_STRUCTURE_TEMPLATE_ID,	dataSubModelId, false);
					}
					// The display element corresponds to a leaf data type
					else
					{
						ModelId dataSubModelId = DisplayModelWrapper
								.correspondingLeafDataType((FlowModel) displaySubModel);
						if (dataSubModelId == null)
							continue; // No corresponding data type (e.g. 'Button')

						dataSubModel = (DataType) repository.getModel(dataSubModelId, true);
					}
				}
			}

			// If composite, synchronize the corresponding models
			if (dataSubModel.getComposition() != Composition.ATOMIC)
			{
				syncCompositeData(displaySubModel, dataSubModel);
				if (dataSubModel.getElements().size() == 0) // Newly created model is empty
					partialUndo(prevHistorySize);
			}

			// Create the corresponding data element
			if (dataElement == null)
				dataElement = (DataElement) addSubModel(dataModel, dataSubModel, displayElement
						.getRole().toString(), 0, 0);

			// Ensure the corresponding elements are both repetitive or non-repetitive
			if (dataElement.isRepetitive() != displayElement.isRepetitive())
				setProperty(dataElement, BuiltinProperties.REPETITIVE, new Boolean(displayElement
						.isRepetitive()));
		}
	}
}
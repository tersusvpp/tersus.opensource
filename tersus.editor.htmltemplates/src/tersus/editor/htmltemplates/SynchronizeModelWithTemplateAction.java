/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.htmltemplates;

import tersus.editor.actions.AbstractSelectionAction;
import tersus.model.BuiltinProperties;
import tersus.model.Model;

/**
 * @author Youval Bronicki
 *
 */
public class SynchronizeModelWithTemplateAction extends AbstractSelectionAction
{

    /* (non-Javadoc)
     * @see tersus.editor.actions.AbstractSelectionAction#calculateEnabled()
     */
    protected boolean calculateEnabled()
    {
        Model model = getModel();
        if (model == null)
            return false;
        return Constants.EMBEDDED_HTML_PLUGIN.equals(model.getProperty(BuiltinProperties.PLUGIN));
    }

    /* (non-Javadoc)
     * @see tersus.editor.actions.AbstractSelectionAction#run()
     */
    public void run()
    {
        SynchronizeModelWithHTMLTemplateCommand command = new SynchronizeModelWithHTMLTemplateCommand(getRepositoryManager().getRepository(), getModel().getId());
        command.execute();
        if (command.modelChanged())
            getRepositoryManager().getCommandStack().execute(command); // This just adds the command to the stack (it's already been executed) 
    }

}

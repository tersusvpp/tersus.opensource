/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.editor.htmltemplates;

import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.jface.dialogs.MessageDialog;

import tersus.editor.actions.AbstractSelectionAction;
import tersus.model.BuiltinProperties;
import tersus.model.FlowModel;
import tersus.model.FlowType;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.PackageId;
import tersus.model.commands.CreateModelFromPrototypeCommand;
import tersus.model.commands.SynchronizeDisplayCommand;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Ofer Brandes
 */
public class CreateDisplayDataStructureAction extends AbstractSelectionAction
{

	public void run()
	{
		Model displayModel = getModel();
		ModelId displayModelId = displayModel.getId();
		PackageId displayModelPackage = displayModel.getPackage().getPackageId();
		String name = displayModel.getName();

		PackageId parentPackage = displayModelPackage.getParent();
		String pathPrefix = (parentPackage == null ? "" : parentPackage.getPath() + "/");

		// Data Model (a data structure)
		String dataPackagePath = pathPrefix + Constants.DATA;
		// MessageDialog.openInformation(getShell(), "Create Data Structure" ,
		// "Creating a data structure package "+dataPackagePath);
		PackageId dataPackageId = new PackageId(dataPackagePath);
		WorkspaceRepository repository = getRepositoryManager().getRepository();
		if (repository.getPackage(dataPackageId) != null)
		{
			MessageDialog.openError(getShell(), "Error", "There is an existing package '"
					+ dataPackageId + "'");
			return;
		}
		ModelId dataModelId = new ModelId(dataPackageId, name);

		CompoundCommand command = new CompoundCommand();
		command.setLabel("Create Display Data Structure");
		command.add(new CreateModelFromPrototypeCommand(repository.getModel(
				Constants.DATA_STRUCTURE_TEMPLATE_ID, true), dataModelId));

		// Extract Model (transformation from the display to the data structure)
		String extractPackagePath = pathPrefix + SynchronizeDisplayCommand.EXTRACT;
		// MessageDialog.openInformation(getShell(), "Create Extract Transformation" ,
		// "Creating a transformation package "+extractPackagePath);
		PackageId extractPackageId = new PackageId(extractPackagePath);
		if (repository.getPackage(extractPackageId) != null)
		{
			MessageDialog.openError(getShell(), "Error", "There is an existing package '"
					+ extractPackageId + "'");
			return;
		}
		ModelId extractModelId = new ModelId(extractPackageId,
				SynchronizeDisplayCommand.EXTRACT_PREFIX + name);
		command.add(new CreateModelFromPrototypeCommand(repository.getModel(
				SynchronizeDisplayCommand.EXTRACT_TEMPLATE_ID, true), extractModelId));

		// Populate Model (transformation from the data structure to the display)
		String populatePackagePath = pathPrefix + SynchronizeDisplayCommand.POPULATE;
		// MessageDialog.openInformation(getShell(), "Create Populate Transformation" ,
		// "Creating a transformation package "+populatePackagePath);
		PackageId populatePackageId = new PackageId(populatePackagePath);
		if (repository.getPackage(populatePackageId) != null)
		{
			MessageDialog.openError(getShell(), "Error", "There is an existing package '"
					+ populatePackageId + "'");
			return;
		}
		ModelId populateModelId = new ModelId(populatePackageId,
				SynchronizeDisplayCommand.POPULATE_PREFIX + name);
		command.add(new CreateModelFromPrototypeCommand(repository.getModel(
				SynchronizeDisplayCommand.POPULATE_TEMPLATE_ID, true), populateModelId));

		// Synchronizing all 4 models
		command.add(new SynchronizeDisplayAndDataCommand(repository, displayModelId, dataModelId,
				extractModelId, populateModelId));
		getRepositoryManager().getCommandStack().execute(command);
	}

	/**
     * 
     */
	public CreateDisplayDataStructureAction()
	{
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.editor.actions.AbstractSelectionAction#calculateEnabled()
	 */
	protected boolean calculateEnabled()
	{
		Model model = getModel();
		if (model == null)
			return false;
		return ((model instanceof FlowModel)
				&& (model.getProperty(BuiltinProperties.TYPE) == FlowType.DISPLAY) && !((FlowModel) model)
				.isAtomic());
	}
}

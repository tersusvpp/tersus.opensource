/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
package tersus.editor.htmltemplates;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.htmlparser.Node;
import org.htmlparser.NodeFilter;
import org.htmlparser.Parser;
import org.htmlparser.Tag;
import org.htmlparser.filters.TagNameFilter;
import org.htmlparser.lexer.Lexer;
import org.htmlparser.nodes.TagNode;
import org.htmlparser.tags.Div;
import org.htmlparser.tags.InputTag;
import org.htmlparser.tags.Span;
import org.htmlparser.tags.TextareaTag;
import org.htmlparser.util.NodeList;

import tersus.editor.layout.LayoutConstraint;
import tersus.model.BuiltinProperties;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.Role;
import tersus.model.commands.MultiStepCommand;
import tersus.util.Misc;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 */
public class SynchronizeModelWithHTMLTemplateCommand extends MultiStepCommand
{

	private ModelId modelId;

	private boolean alreadyRun;

	public SynchronizeModelWithHTMLTemplateCommand(WorkspaceRepository repository, ModelId modelId)
	{
		super(repository);
		this.modelId = modelId;
	}

	public boolean modelChanged()
	{
		return history.size() > 0;
	}

	public void run()
	{
		if (!alreadyRun)
			runSynchronizeModelWithHTMLTemplateCommand();
	}

	@SuppressWarnings("serial")
	public void runSynchronizeModelWithHTMLTemplateCommand()
	{
		alreadyRun = true;
		history.clear();
		Model model = repository.getModel(modelId, true);
		String templatePath = (String) model.getProperty(Constants.TEMPLATE_PATH_PROPERTY_NAME);
		try
		{
			byte[] templateBytes = repository.read(templatePath);
			Parser parser = new Parser(new Lexer(new String(templateBytes, "UTF-8")));
			NodeList contentNodes = parser.extractAllNodesThatMatch(new NodeFilter()
			{
				public boolean accept(Node node)
				{
					return node instanceof TagNode
							&& !((Tag) node).isEndTag()
							&& (Constants.CONTENT_NODE_ID.equals(((TagNode) node)
									.getAttribute(Constants.ID)));
				}
			});
			if (contentNodes.size() > 1)
				throw new ImportException(
						"The template contains more than one node with Id 'content'");
			else if (contentNodes.size() == 0)
			{
				parser.reset();
				contentNodes = parser
						.extractAllNodesThatMatch(new TagNameFilter(Constants.BODY_TAG));
				if (contentNodes.size() == 0)
					throw new ImportException("Body or content node not found");
				else if (contentNodes.size() > 1)
					throw new ImportException("There is more than one BODY in the template");
			}
			Node contentNode = contentNodes.elementAt(0);
			synchronizeModelWithTemplateNode(model, contentNode);
		}
		catch (Exception e)
		{
			String message;
			undo();
			if (e instanceof ImportException)
			{
				message = e.getMessage();
			}
			else
				message = "An error has occurred while importing an HTML template";
			MessageDialog.openError(TersusWorkbench.getActiveWorkbenchShell(), "Import Failed",
					message);
			TersusWorkbench.log(e);
		}
	}

	private void synchronizeModelWithTemplateNode(Model model, Node node)
	{
		ArrayList<Node> elementNodes = new ArrayList<Node>();
		addElementNodes(elementNodes, node);
		for (int i = 0; i < elementNodes.size(); i++)
		{
			TagNode childNode = (TagNode) elementNodes.get(i);
			String repetitive = childNode.getAttribute(Constants.REPETITIVE_ATTRIBUTE);

			String elementName = getElementName(childNode);
			ModelElement element = model.getElement(Role.get(elementName));
			if (element == null)
			{
				element = createElementForNode(model, childNode, elementName, i, elementNodes
						.size());
			}
			if (Constants.TRUE.equals(repetitive) && !element.isRepetitive())
				setProperty(element, BuiltinProperties.REPETITIVE, Boolean.TRUE);

			synchronizeModelWithTemplateNode(element.getReferredModel(), childNode);
		}
	}

	private String getElementName(TagNode node)
	{
		String elementName = ((TagNode) node).getAttribute(Constants.ELEMENT_NAME_ATTRIBUTE);
		elementName = Misc.replaceAll(elementName, "&amp;", "&");
		elementName = Misc.replaceAll(elementName, "&gt;", ">");
		elementName = Misc.replaceAll(elementName, "&lt;", "<");
		elementName = Misc.replaceAll(elementName, "&quot;", "\"");
		return elementName;
	}

	private void addElementNodes(List<Node> list, Node node)
	{
		NodeList allChildNodes = node.getChildren();
		if (allChildNodes != null)
		{
			for (int i = 0; i < allChildNodes.size(); i++)
			{
				Node childNode = allChildNodes.elementAt(i);
				if (childNode instanceof TagNode)
				{
					boolean sample = Constants.SAMPLE.equals((((TagNode) childNode)
							.getAttribute(Constants.REPETITIVE_ATTRIBUTE)));
					if (sample)
						continue; // Ignoring sample nodes;
					String elementName = ((TagNode) childNode)
							.getAttribute(Constants.ELEMENT_NAME_ATTRIBUTE);
					if (elementName != null)
					{
						list.add(childNode);
					}
					else
						addElementNodes(list, childNode);
				}
			}
		}
	}

	private ModelElement createElementForNode(Model parentModel, TagNode childNode,
			String elementName, int childIndex, int numberOfChildren)
	{
		Model templateModel = repository.getModel(getTemplateIdForNode(childNode), true);
		double p = 0.25 + childIndex * 0.5 / numberOfChildren;
		createElementFromTemplate(templateModel, parentModel,
				new LayoutConstraint(p, p, 0.25, 0.25), elementName, true);

		return parentModel.getElement(Role.get(elementName));

	}

	/**
	 * Returns the appropriate template for a given HTML node
	 * 
	 * @param childNode
	 * @return
	 */
	private ModelId getTemplateIdForNode(TagNode node)
	{
		if (isCompositeNode(node))
			return Constants.EMBEDDED_ELEMENT_TEMPLATE_ID;
		else if (node instanceof Div)
		{
			return Constants.PANE_TEMPLATE_ID;
		}
		else if (node instanceof Span)
		{
			return Constants.TEXT_DISPLAY_TEMPLATE_ID;
		}
		else if (node instanceof TextareaTag)
		{
			return Constants.TEXTAREA_TEMPLATE_ID;
		}
		else if (node instanceof InputTag)
		{
			String type = node.getAttribute(Constants.INPUT_TYPE_ATTRIBUTE);
			if (type == null || type.equals(Constants.TEXT_INPUT_TYPE)
					|| type.equals(Constants.PASSWORD_INPUT_TYPE))
				return Constants.TEXT_INPUT_FIELD_TEMPLATE_ID;
			else if (type.equals(Constants.CHECKBOX_INPUT_TYPE))
				return Constants.CHECKBOX_TEMPLATE_ID;
			else if (type.equals(Constants.SUBMIT_INPUT_TYPE)
					|| type.equals(Constants.BUTTON_INPUT_TYPE))
				return Constants.BUTTON_TEMPLATE_ID;
			else if (type.equals(Constants.FILE_INPUT_TYPE))
				return Constants.FILE_INPUT_FIELD_TEMPLATE_ID;

		}
		else if (Constants.BUTTON_TAG.equals(node.getTagName()))
			return Constants.BUTTON_TEMPLATE_ID;

		return Constants.GENERIC_HTML_TEMPLATE_ID;

	}

	/**
	 * @return true if the given TagNode contains child TagNodes (and hence represents a composite
	 *         HTML element).
	 */
	private boolean isCompositeNode(TagNode node)
	{
		NodeList allChildNodes = node.getChildren();
		if (allChildNodes != null)
		{
			for (int i = 0; i < allChildNodes.size(); i++)
			{
				Node childNode = allChildNodes.elementAt(i);
				if (childNode instanceof TagNode)
				{
					return true;
				}
			}
		}
		return false;
	}

}
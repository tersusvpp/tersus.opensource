/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.htmltemplates;

import java.io.File;
import java.io.FileInputStream;

//import org.eclipse.core.internal.content.XMLContentDescriber;
import org.htmlparser.Node;
import org.htmlparser.Parser;
import org.htmlparser.filters.TagNameFilter;
import org.htmlparser.util.NodeIterator;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import tersus.util.XMLReaderConfigurator;

/**
 * @author Youval Bronicki
 *  
 */
public class Test
{

    public static void main(String[] args) throws Exception
    {
        File file = new File("Template.html");
                Parser parser = new Parser();
                parser.setURL(file.toURL().toString());
                NodeList nodes = parser.extractAllNodesThatMatch(new TagNameFilter("body"));
                Node body = nodes.elementAt(0);
                for (NodeIterator i = body.getChildren().elements();i.hasMoreNodes();)
                {
                    Node node = i.nextNode();
                    NodeList children = node.getChildren();
                    System.out.println(node.getClass() + ":" + (children == null? 0 : children.size()) + " children");
                    System.out.println(node.getText());
                }

    }
}
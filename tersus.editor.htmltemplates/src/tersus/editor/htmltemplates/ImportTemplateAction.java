/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.htmltemplates;


import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.jface.dialogs.MessageDialog;

import tersus.editor.actions.AbstractSelectionAction;
import tersus.model.ModelId;
import tersus.model.ModelObject;
import tersus.model.PackageId;
import tersus.model.commands.CopyModelCommand;
import tersus.model.commands.MultiStepCommand;
import tersus.model.commands.SetPropertyValueCommand;
import tersus.util.Misc;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Youval Bronicki
 *
 */
public class ImportTemplateAction extends AbstractSelectionAction
{

    public void run()
    {
        IFile file = getFile();
        IPath path = file.getParent().getFullPath();
        WorkspaceRepository repository = getRepositoryManager().getRepository();
        String packagePath = path.toString().substring(repository.getRepositoryRoot().getFullPath().toString().length()+1);
        final String filePath = file.getFullPath().toString().substring(repository.getRepositoryRoot().getFullPath().toString().length()+1);
        String name = file.getName();
        String extension = file.getFileExtension();
        if (extension.length() > 0)
            name = name.substring(0, name.length() - extension.length()-1);
        MessageDialog.openInformation(getShell(), "Import Template" , "Importing a Template package="+packagePath + " name="+name);
        PackageId pkgId = new PackageId(packagePath+"/"+name);
        if (repository.getPackage(pkgId) != null)
        {
            MessageDialog.openError(getShell(), "Error", "There is an existing package '"+pkgId+"'");
            return;
        }
        final ModelId modelId = new ModelId(pkgId, name);
        MultiStepCommand firstCommand = new MultiStepCommand(repository, "Create HTML Template Model") // Quick patch, 31/5/09 - part is MultiStepCommand, the rest using CompoundCommand 
		{
			@Override
			protected void run()
			{
				createModelFromTemplate (Constants.EMBEDDED_HTML_TEMPLATE_ID, modelId);
				setProperty(modelId, Constants.TEMPLATE_PATH_PROPERTY_NAME, filePath);
			}
		};
        CompoundCommand command = new CompoundCommand();
        command.setLabel("Import HTML Template");
        command.add(firstCommand);
        command.add(new SynchronizeModelWithHTMLTemplateCommand(repository,modelId));
        getRepositoryManager().getCommandStack().execute(command);
    }
    /**
     * 
     */
    public ImportTemplateAction()
    {
        super();
    }

    /* (non-Javadoc)
     * @see tersus.editor.actions.AbstractSelectionAction#calculateEnabled()
     */
    protected boolean calculateEnabled()
    {
        IFile file = getFile();
        return file != null && "html".equals(file.getFileExtension());
    }
    private IFile getFile()
    {
        IFile file = (IFile)getSelectedObject(IFile.class);
        return file;
    }

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.editor.htmltemplates;

import tersus.model.ModelId;
import tersus.model.Role;

/**
 * @author Youval Bronicki
 *
 */
public interface Constants
{
	// For importing HTML template
    ModelId EMBEDDED_HTML_TEMPLATE_ID = new ModelId("Common/Templates/Display/Embedded HTML/Embedded HTML");
    ModelId EMBEDDED_ELEMENT_TEMPLATE_ID = new ModelId("Common/Templates/Display/Embedded Element/Embedded Element");
    String TEMPLATE_PATH_PROPERTY_NAME = "html.templatePath";
    String CONTENT_NODE_ID = "content";
    String ID = "id";
    String ELEMENT_NAME_ATTRIBUTE = "element_name";
    String REPETITIVE_ATTRIBUTE="repetitive";
    String TRUE = "true";
    String SAMPLE= "sample";
    String EMBEDDED_HTML_PLUGIN = "Tersus/Display/Embedded HTML";
    String INPUT_TYPE_ATTRIBUTE = "type";
    String BUTTON_INPUT_TYPE = "button";
    String SUBMIT_INPUT_TYPE = "submit";
    String FILE_INPUT_TYPE = "file";
    String PASSWORD_INPUT_TYPE = "password";
    String TEXT_INPUT_TYPE = "text";
    String CHECKBOX_INPUT_TYPE = "checkbox";
    ModelId CHECKBOX_TEMPLATE_ID = new ModelId("Common/Templates/Display/Check Box/Check Box");
    ModelId PANE_TEMPLATE_ID = new ModelId("Common/Templates/Display/Pane/Pane");;
    ModelId TEXT_DISPLAY_TEMPLATE_ID = new ModelId("Common/Templates/Display/Text Display/Text Display");
    ModelId TEXTAREA_TEMPLATE_ID = new ModelId("Common/Templates/Display/Text Area/Text Area");;
    ModelId TEXT_INPUT_FIELD_TEMPLATE_ID = new ModelId("Common/Templates/Display/Text Input Field/Text Input Field");
    ModelId BUTTON_TEMPLATE_ID = new ModelId("Common/Templates/Display/Button/Button");
    ModelId FILE_INPUT_FIELD_TEMPLATE_ID = new ModelId("Common/Templates/Display/File Input Field/File Input Field");;
    ModelId GENERIC_HTML_TEMPLATE_ID = new ModelId("Common/Templates/Display/Generic HTML Element/Generic HTML Element");
    String BUTTON_TAG = "BUTTON";
    String BODY_TAG = "BODY";

	// For creating corresponding data structure and bidirectional transformations
    String DATA = "Data";
    ModelId DATA_STRUCTURE_TEMPLATE_ID = new ModelId("Common/Templates/Composite Data Types/Data Structure/Data Structure");

    // More constants are defined in tersus.model.commands.MultiStepCommand
}

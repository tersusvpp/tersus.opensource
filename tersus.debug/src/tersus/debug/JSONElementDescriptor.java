/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.debug;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author Liat Shiff
 */
public class JSONElementDescriptor
{
	private String name;
	private Object value;
	
	public JSONElementDescriptor(String name, Object value)
	{
		this.name = name;
		this.value = value;
	}
	
	public String getName()
	{
		return name;
	}
	
	public Object getValue()
	{
		return value;
	}
	
	public boolean hasChildren()
	{
		return value instanceof JSONObject || value instanceof JSONArray;
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.debug;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;

import tersus.editor.TersusEditor;
import tersus.editor.editparts.TersusEditPart;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.indexer.RepositoryIndex;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * @author Liat shiff
 */
public abstract class ShowMatchRowAction extends Action
{
	protected TraceView traceView;
	
	protected ModelElement selectedElement;
	
	protected WorkspaceRepository repository;

	protected RepositoryIndex index;
	
	protected TraceContentProvider contentProvider;
	
	public ShowMatchRowAction(TraceView traceView)
	{
		this.traceView = traceView;
	}
	
	public void initializeDialogVariables()
	{
		repository = (WorkspaceRepository) selectedElement.getRepository();
		index = repository.getUpdatedRepositoryIndex();
		contentProvider = (TraceContentProvider) traceView.getViewer().getContentProvider();
	}
	
	protected void setSelectedElement()
	{
		IWorkbenchPage page = TersusWorkbench.getActiveWorkbenchWindow().getActivePage();
		IEditorPart activeEditor = page.getActiveEditor();
		
		TersusEditor editor = (TersusEditor)activeEditor;
		StructuredSelection selection = (StructuredSelection)editor.getViewer().getSelection();
		Object selectedObject = selection.getFirstElement();
		
		if (selectedObject != null && selectedObject instanceof TersusEditPart)
		{
			TersusEditPart part = (TersusEditPart)selectedObject;
			
			// If Nothing is selected keep the previous selection
			if (part.getModelElement() != null)
			{
				selectedElement = part.getModelElement();
			}
		}
	}
	
	protected TraceEvent getCurrentEvent()
	{
		TraceEvent currentEvent = traceView.getSelectedTraceEvent();
		
		if (currentEvent == null)
		{
			Object rootObject = traceView.getViewer().getTree().getTopItem().getData();
			if (rootObject instanceof TraceEvent)
				currentEvent = (TraceEvent)rootObject;
		}
		
		return currentEvent;
	}
	
	
	protected boolean isEventMatch(TraceEvent evt)
	{
		ModelId baseModelId = evt.getBaseModelId();
		Model baseModelModel = repository.getModel(baseModelId, true);

		return baseModelModel != null
				&& index.pathContainsElement(baseModelId, evt.getDisplayPath(), selectedElement,
						true);
	}
}

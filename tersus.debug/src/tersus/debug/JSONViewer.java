/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.debug;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * @author Liat Shiff
 */
public class JSONViewer extends TitleAreaDialog
{
	private Composite composite;

	private JSONObject jsonObject;

	private TreeViewer viewer;

	private String defaultMessage;

	private String defaultTitle;

	private Action copyValueAction;

	protected JSONViewer(Shell shell, String jsonText, String defaultTitle, String defaultMessage)
	{
		super(shell);

		setJsonObject(jsonText);
		this.defaultMessage = defaultMessage;
		this.defaultTitle = defaultTitle;
	}

	protected Control createDialogArea(Composite parent)
	{
		composite = (Composite) super.createDialogArea(parent);
		GridLayout layout = new GridLayout();
		layout.marginHeight = 5;
		layout.marginWidth = 5;
		composite.setLayout(layout);
		composite.setLayoutData(new GridData(GridData.FILL_BOTH));
		composite.setFont(parent.getFont());

		initializeDialogUnits(composite);
		setTitle(defaultTitle);
		setMessage(defaultMessage);
		initializeTreeViewer();

		makeActions();
		hookContextMenu();

		return composite;
	}

	private void setJsonObject(String jsonText)
	{
		JSONTokener tokener = new JSONTokener(jsonText);
		try
		{
			jsonObject = new JSONObject(tokener);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private void initializeTreeViewer()
	{
		if (jsonObject != null)
		{
			viewer = new TreeViewer(composite);
			viewer.getTree().setLayout(new GridLayout());
			GridData gridData = new GridData(GridData.FILL_BOTH);
			gridData.heightHint = getShell().getBounds().height - 20;
			gridData.widthHint = getShell().getBounds().width - 20;
			viewer.getTree().setLayoutData(gridData);

			viewer.setContentProvider(new JSONViewerContentProvider());
			viewer.setLabelProvider(new JSONViewerLabelProvider());
			viewer.setInput(jsonObject);

			viewer.addSelectionChangedListener(new ISelectionChangedListener()
			{
				public void selectionChanged(SelectionChangedEvent event)
				{
					hookContextMenu();
				}
			});
		}
	}

	private void hookContextMenu()
	{
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener()
		{
			public void menuAboutToShow(IMenuManager manager)
			{
				JSONViewer.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
	}

	private void fillContextMenu(IMenuManager manager)
	{
		ISelection selection = viewer.getSelection();
		Object obj = ((IStructuredSelection) selection).getFirstElement();

		if (obj instanceof JSONElementDescriptor && !((JSONElementDescriptor) obj).hasChildren())
			manager.add(copyValueAction);
	}

	private void makeActions()
	{
		copyValueAction = new Action()
		{
			public void run()
			{
				ISelection selection = viewer.getSelection();
				Object obj = ((IStructuredSelection) selection).getFirstElement();

				if (obj instanceof JSONElementDescriptor)
				{
					JSONElementDescriptor descriptor = (JSONElementDescriptor) obj;
					if (!descriptor.hasChildren())
					{
						String value = descriptor.getValue().toString();
						Clipboard clipboard = new Clipboard(getShell().getDisplay());
						TextTransfer textTransfer = TextTransfer.getInstance();

						clipboard.setContents(new String[]
						{ value }, new Transfer[]
						{ textTransfer });
						clipboard.dispose();
					}
				}
			}
		};
		copyValueAction.setText("Copy Value");
		copyValueAction.setToolTipText("Copy the value");
	}

	protected void configureShell(Shell shell)
	{
		super.configureShell(shell);

		shell.setText("Details");
		Composite comp = shell.getParent();
		int width = comp.getBounds().width;
		int height = comp.getBounds().height;
		shell.setBounds(width / 2 - 250, height / 2 - 225, 500, 375);
	}

	protected void createButtonsForButtonBar(Composite parent)
	{
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
	}

	protected boolean isResizable()
	{
		return true;
	}
}

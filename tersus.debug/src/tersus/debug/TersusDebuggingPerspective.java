/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.debug;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

import tersus.editor.TersusModelingPerspective;
import tersus.editor.validation.ui.ValidationView;

/**
 * 
 * @author Joel Barel
 *
 */
public class TersusDebuggingPerspective extends TersusModelingPerspective implements IPerspectiveFactory {
	
	/**
	 * Constructs a new Default layout engine.
	 */
	public TersusDebuggingPerspective() {
		super();
	}

	protected void createBottomFolder(IPageLayout layout) {
 		String editorArea = layout.getEditorArea();
		
		IFolderLayout bottomFolder= layout.createFolder("bottom", IPageLayout.BOTTOM, (float)0.60, editorArea); //$NON-NLS-1$

		bottomFolder.addView(TRACE_VIEW);
		bottomFolder.addView(IPageLayout.ID_PROP_SHEET);
		bottomFolder.addPlaceholder(ValidationView.ID);
	}
}

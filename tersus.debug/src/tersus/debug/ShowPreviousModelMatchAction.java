/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.debug;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;

import tersus.editor.actions.TersusActionConstants;

/**
 * @author Liat shiff
 */
public class ShowPreviousModelMatchAction extends ShowMatchRowAction
{
	public ShowPreviousModelMatchAction(TraceView traceView)
	{
		super(traceView);

		setId(TersusActionConstants.SHOW_PREVIOUS_MODEL_MATCH_ACTION_ID);
		setText(TersusActionConstants.SHOW_PREVIOUS_MODEL_MATCH_ACTION_TEXT);
		setImageDescriptor(ImageDescriptor.createFromFile(TraceView.class, "icons/prev.gif"));
	}

	public void run()
	{
		setSelectedElement();
		if (selectedElement != null)
		{
			initializeDialogVariables();

			TraceEvent currentEvent = getCurrentEvent();

			if (currentEvent != null)
			{
				TraceEvent previousEvent = getPreviousTraceEvent(currentEvent, false);
				if (previousEvent != null)
				{
					IStructuredSelection selection = new StructuredSelection(previousEvent);
					traceView.getViewer().setSelection(selection, true);
				}
			}
		}
		else
			MessageDialog.openInformation(traceView.getViewer().getControl().getShell(), "Runtime Trace View",
					"There is no selected element in Tersus editor.");
	}

	protected TraceEvent getPreviousTraceEvent(TraceEvent currentEvent, boolean checkChildren)
	{
		if (checkChildren)
		{
			Object[] children = contentProvider.getChildren(currentEvent);
			if (children != null && children.length > 0)
			{
				for (int i = children.length - 1; i > -1; i--)
				{
					TraceEvent child = (TraceEvent)children[i];
					
					if (contentProvider.hasChildren(child))
						return getPreviousTraceEvent(child, true);
					
					if (isEventMatch(child))
						return child;
				}
			}
			
			if (isEventMatch(currentEvent))
				return currentEvent;
		}
		
		Object parentObject = contentProvider.getParent(currentEvent);
		
		if (parentObject != null && parentObject instanceof TraceEvent)
		{
			TraceEvent parent = (TraceEvent)parentObject;
			Object[] brothers = contentProvider.getChildren(parent);
			if (brothers != null && brothers.length > 1)
			{
				TraceEvent prevBrother = (TraceEvent)brothers[0];
				for(int j=1; j < brothers.length; j++)
				{
					if (prevBrother.equals(currentEvent))
						break;
					
					TraceEvent brother = (TraceEvent)brothers[j];
					if (currentEvent.equals(brother))
					{
						if (contentProvider.hasChildren(prevBrother))
							return getPreviousTraceEvent(prevBrother, true);
						
						if (isEventMatch(prevBrother))
							return prevBrother;
					}
					prevBrother = brother;
				}
			}
			
			if (isEventMatch(parent))
				return parent;
			
			return getPreviousTraceEvent(parent, false);
		}
		return null;
	}
}

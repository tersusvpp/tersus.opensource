/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.debug;

import org.eclipse.jface.viewers.LabelProvider;

/**
 * @author Liat Shiff
 */
public class JSONViewerLabelProvider extends LabelProvider 
{
	public String getText(Object element)
	{
		if (element instanceof JSONElementDescriptor)
		{
			JSONElementDescriptor descriptor = (JSONElementDescriptor)element;
			String name = descriptor.getName();
			
			if (descriptor.hasChildren())
				return name;
			else
			{
				Object value = descriptor.getValue();
				return name + " : " + value.toString();
			}
		}
		return null;
	}
}

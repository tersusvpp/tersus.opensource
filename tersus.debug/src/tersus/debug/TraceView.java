/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.debug;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.Path;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
//import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.ide.IDE.SharedImages;
import org.eclipse.ui.part.ViewPart;

import tersus.editor.ConcreteModelIdentifier;
import tersus.editor.RepositoryManager;
import tersus.editor.TersusEditor;
import tersus.editor.editparts.FlowEditPart;
import tersus.editor.editparts.TersusEditPart;
import tersus.editor.editparts.TopFlowEditPart;
import tersus.model.DataType;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.RelativeSize;
import tersus.runtime.trace.EventType;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.WorkspaceRepository;

/**
 * A <code>ViewPart</code> that displays runtime trace events in a table-tree display.
 * 
 * TraceView 'loads' trace files (only one file at each time), displays the content in a
 * <code>Tree</code> and responds to selection events by highlighting elements in the model diagram.
 * 
 * @author Youval Bronicki
 * 
 */
public class TraceView extends ViewPart
{
	private TersusEditor editor;

	private TreeViewer viewer;

	private Action openFileAction;

	private Action refreshAction;

	private File traceFile;

	private Action copyAction;

	private Action openJasonViewerAction;
	
	private Action openDetailsAction;

	private ShowNextModelMatchAction showNextAction;

	private ShowPreviousModelMatchAction showPreviousAction;

	/**
	 * An <code>ITableLabelProvider</code> for trace events.
	 * 
	 * @author Youval Bronicki
	 * 
	 */
	class TraceLabelProvider extends LabelProvider implements ITableLabelProvider
	{
		private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss:SSS");

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object, int)
		 * 
		 * Returns the following columns: - Event Description - Details - Time - Path (FUNC2 -
		 * remove the path column from the view)
		 */
		public String getColumnText(Object obj, int index)
		{
			TraceEvent evt = (TraceEvent) obj;
			if (index == 0)
				return evt.getDescription();
			else if (index == 1)
				return getText((evt).details);
			else if (index == 2)
			{
				TraceEvent event = evt;
				if (event.time >= 0)
					return timeFormat.format(new Date(event.time));
				else
					return "";
			}
			else if (index == 3)
				return getText((evt).getDisplayPath());
			else
				return "";
		}

		public Image getColumnImage(Object obj, int index)
		{
			return null;
		}

	}

	public void createPartControl(Composite parent)
	{
		/*
		 * Create and configure the Tree
		 */
		Tree tree = new Tree(parent, SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI | SWT.FULL_SELECTION);
		TableLayout layout = new TableLayout();
		tree.setLayout(layout);
		tree.setLinesVisible(true);
		tree.setHeaderVisible(true);
		ColumnWeightData[] columnLayouts =
		{ new ColumnWeightData(100), new ColumnWeightData(100), new ColumnWeightData(25),
				new ColumnWeightData(50) };
		String[] labels = new String[]
		{ "Description", "Details", "Time", "Path" };
		for (int i = 0; i < labels.length; i++)
		{

			TreeColumn tc = new TreeColumn(tree, SWT.NONE, i);
			tc.setText(labels[i]);
			tc.pack();
			columnLayouts[i].minimumWidth = tc.getWidth();
			layout.addColumnData(columnLayouts[i]);
		}

		/*
		 * Create and configure the viewer
		 */
		viewer = new TreeViewer(tree);
		viewer.setContentProvider(new TraceContentProvider());
		viewer.setLabelProvider(new TraceLabelProvider());
		viewer.setInput(new TraceEvent());

		/*
		 * Create a SelectionListener that forwards selection events to
		 * TraceView#handleSelection(TraceEvent)
		 */
		SelectionListener selectionListener = new SelectionListener()
		{

			public void widgetSelected(SelectionEvent e)
			{
				IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
				TraceEvent event = (TraceEvent) selection.getFirstElement();
				if (event == null)
					return;
				openEditor(event);
				if (editor != null)
				{
					TersusEditPart element = editor.getTopFlowEditPart().getChild(
							event.getDisplayPath().getTail(
									event.getDisplayPath().getNumberOfSegments()
											- focusPath.getNumberOfSegments()), true);
					if (element != null)
					{
						TersusEditPart parent = element instanceof TopFlowEditPart ? null
								: (TersusEditPart) element.getParent();
						zoom(parent != null ? parent : element);

						if (EventType.FINISHED.toString().equals(event.eventName))
						{
							if (parent != null)
							{
								((FlowEditPart) element).setOpen(false);
							}
						}
						editor.highlight(event.getDisplayPath().getTail(
								event.getDisplayPath().getNumberOfSegments()
										- focusPath.getNumberOfSegments()));
					}
				}
			}

			public void widgetDefaultSelected(SelectionEvent e)
			{
			}
		};
		tree.addSelectionListener(selectionListener);
		makeActions();
		hookContextMenu();
		addListeners();
		contributeToActionBars();
		getViewSite().getActionBars()
				.setGlobalActionHandler(ActionFactory.COPY.getId(), copyAction);

		String osString = System.getProperty("os.name");

		if ("Linux".equals(osString))
			addKeyListener();
	}

	private void addKeyListener()
	{
		viewer.getTree().addKeyListener(new KeyListener()
		{
			public void keyPressed(KeyEvent e)
			{
				Tree tree = (Tree) e.getSource();
				TreeItem item = tree.getSelection()[0];
				Event event = new Event();

				if (e.keyCode == SWT.ARROW_RIGHT)
				{
					event.item = item;
					item.getParent().notifyListeners(SWT.Expand, event);

					if (!item.getExpanded())
						item.setExpanded(true);
				}
				else if (e.keyCode == SWT.ARROW_LEFT)
				{
					event.item = item;
					item.getParent().notifyListeners(SWT.Collapse, event);

					if (item.getExpanded())
						item.setExpanded(false);
				}
			}

			public void keyReleased(KeyEvent e)
			{
			}
		});
	}

	/**
	 * Creates and hooks the context menu.
	 */
	private void hookContextMenu()
	{
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener()
		{
			public void menuAboutToShow(IMenuManager manager)
			{
				TraceView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	/**
	 * Populate the view's action bars (the view's toolbar and the view's pull-down menu.
	 */
	private void contributeToActionBars()
	{
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	/**
	 * Populates the view's pull-down menu (currently contains only 'Open'
	 * 
	 * @param manager
	 *            the IMenuManager for the pull-down menu
	 */
	private void fillLocalPullDown(IMenuManager manager)
	{
		manager.add(openFileAction);
		manager.add(refreshAction);
	}

	/**
	 * Populates the view's context menu.
	 * 
	 * The context menu currently only contains a dummy 'Action2' (FFU) FUNC3 Create a real context
	 * menu (actions TBD), or remove.
	 * 
	 * @param manager
	 *            the IMenuManager for the context menu
	 */
	private void fillContextMenu(IMenuManager manager)
	{
		manager.add(copyAction);
		if (isValidJSON())
		{
			manager.add(openJasonViewerAction);
		}
	}

	private boolean isValidJSON()
	{
		TraceEvent event = getSelectedTraceEvent();

		if (event != null)
		{
			String details = event.details.trim();
			if (details.startsWith("{") && details.endsWith("}") && details.length() > 2)
				return true;
		}

		return false;
	}

	/**
	 * Populates the view's toolbar.
	 * 
	 * @param manager
	 *            The <code>IToolBarManager</code> for the view's toolbar
	 */
	private void fillLocalToolBar(IToolBarManager manager)
	{
		manager.add(showNextAction);
		manager.add(showPreviousAction);
		manager.add(new Separator());
		manager.add(openFileAction);
		manager.add(refreshAction);
	}

	/**
	 * Creates the actions for this view. FUNC3 Complete the set of actions. Remove 'Action2'
	 */
	private void makeActions()
	{
		openFileAction = new Action()
		{
			public void run()
			{
				loadFile();
			}
		};
		openFileAction.setText("Open");
		openFileAction.setToolTipText("Opens a trace file and displays its content");
		openFileAction.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages()
				.getImageDescriptor(SharedImages.IMG_OPEN_MARKER));

		refreshAction = new Action()
		{
			public void run()
			{
				if (traceFile != null)
					loadFile(traceFile);

			}
		};
		refreshAction.setText("Refresh");
		refreshAction.setToolTipText("Refresh Trace");
		refreshAction.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages()
				.getImageDescriptor(ISharedImages.IMG_TOOL_REDO));

		copyAction = new Action()
		{
			public void run()
			{
				TraceEvent event = getSelectedTraceEvent();
				if (event != null)
				{
					String text = event.details;

					Clipboard clipboard = new Clipboard(getSite().getShell().getDisplay());
					TextTransfer textTransfer = TextTransfer.getInstance();
					clipboard.setContents(new String[]
					{ text }, new Transfer[]
					{ textTransfer });
					clipboard.dispose();
				}
			}

		};
		copyAction.setText("Copy");
		copyAction.setToolTipText("Copy the description field");

		openJasonViewerAction = new Action()
		{
			public void run()
			{
				TraceEvent evt = getSelectedTraceEvent();
				if (evt != null)
				{
					String text = evt.details;
					JSONViewer viewer = new JSONViewer(getSite().getShell(), text, evt
							.getDisplayPath().toString(), "Description: " + evt.getDescription());

					viewer.open();

				}
			}
		};

		openJasonViewerAction.setText("Show Details");
		showNextAction = new ShowNextModelMatchAction(this);
		showPreviousAction = new ShowPreviousModelMatchAction(this);
		
		
		openDetailsAction = new Action()
		{
			public void run()
			{
				ISelection selection = viewer.getSelection();
				Object obj = ((IStructuredSelection) selection)
						.getFirstElement();
				if (obj instanceof TraceEvent)
				{
					showMessage(((TraceEvent) obj).details);
				} else
				{
					showMessage("Double-click detected on " + obj);

				}
			}
		};
	}

	private void showMessage(String message)
	{
		MessageDialog.openInformation(viewer.getControl().getShell(),
				"Runtime Trace View", message);
	}
	
	public TraceEvent getSelectedTraceEvent()
	{
		ISelection selection = viewer.getSelection();
		Object obj = ((IStructuredSelection) selection).getFirstElement();

		if (obj instanceof TraceEvent)
			return (TraceEvent) obj;

		return null;
	}

	private void addListeners()
	{
		viewer.addDoubleClickListener(new IDoubleClickListener()
		{
			public void doubleClick(DoubleClickEvent event)
			{
				if (isValidJSON())
					openJasonViewerAction.run();
				else
					openDetailsAction.run();
					
			}
		});

		viewer.addSelectionChangedListener(new ISelectionChangedListener()
		{
			public void selectionChanged(SelectionChangedEvent event)
			{
				hookContextMenu();
			}
		});
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus()
	{
		viewer.getControl().setFocus();
	}

	/**
	 * Highlights the model element that correspodns to a trace event. This method is called when
	 * selection is changed in the TabelTree.
	 * 
	 * If the trace file's root model is not shown as the active editor. It is shown.
	 * 
	 * Note: In order to display the correct model, we need to know the repository associated with
	 * the trace file. We currently assume that if all open models belong to the same repository,
	 * then this is our repository.
	 * 
	 * If no models are open, or if models are open from different repositories, a message is shown
	 * and no highlighting happens. This may not be the best approach ... FUNC3 Find a better way to
	 * associate a repository with a trace file.
	 * 
	 * @param
	 */
	private void openEditor(TraceEvent event)
	{
		System.out.println("Event selected:  Path='" + event.getDisplayPath() + "'");
		if (event.getDisplayPath() == null)
			return;
		if (getRootModel() == null)
		{
			// NICE3 use showMessage()
			MessageDialog.openInformation(TersusWorkbench.getActiveWorkbenchShell(),
					"No Default Project Selected",
					" Please open any model in the target project to allow visual tracing");
			return;
		}

		checkFocus(event);

		ConcreteModelIdentifier modelIdentifier = new ConcreteModelIdentifier(
				((WorkspaceRepository) getFocusModel().getRepository()).getRepositoryRoot()
						.getProject(), getFocusModel().getId());
		IWorkbenchPage page = TersusWorkbench.getActiveWorkbenchWindow().getActivePage();
		IEditorPart activeEditor = page.getActiveEditor();
		editor = null;
		if (activeEditor instanceof TersusEditor
				&& modelIdentifier.equals(activeEditor.getEditorInput()))
		{
			editor = (TersusEditor) activeEditor;
		}
		if (editor == null)
		{
			try
			{
				editor = (TersusEditor) page.openEditor(modelIdentifier, TersusEditor.ID);
				editor.getTopFlowEditPart().zoomToPart();
			}
			catch (PartInitException e)
			{
				TersusWorkbench.log(e);
				return;
			}
			setFocus();
		}
	}

	tersus.model.Path focusPath = new tersus.model.Path();

	Model focusModel;

	private Model rootModel;

	private static double MIN_SIZE = 1000000.0 / Integer.MAX_VALUE;

	private Model baseModel;

	private void checkFocus(TraceEvent event)
	{
		tersus.model.Path path = event.getDisplayPath();
		setBaseModel(getRootModel().getRepository().getModel(event.getBaseModelId(), true));
		if (!path.beginsWith(focusPath)
				|| (path.equals(focusPath) && path.getNumberOfSegments() > 0))
			unfocus();

		double vSize = 1;
		double hSize = 1;
		for (int i = path.getNumberOfSegments() - 2; i > focusPath.getNumberOfSegments(); i--)
		{
			ModelElement e = baseModel.getElement(path.getPrefix(i));
			if (e != null)
			{
				RelativeSize size = e.getSize();
				if (size != null)
				{
					vSize *= size.getHeight();
					hSize *= size.getWidth();
					if (Math.min(vSize, hSize) < MIN_SIZE)
					{
						focus(path.getPrefix(Math.max(path.getNumberOfSegments() - 1, i + 3)));
						break;
					}
				}
			}
		}
	}

	private void setBaseModel(Model model)
	{
		if (model == baseModel)
			return;
		baseModel = model;
		unfocus();
	}

	private Model getBaseModel()
	{
		return baseModel;
	}

	private void focus(tersus.model.Path focusPath)
	{
		this.focusPath.copy(focusPath);
		ModelElement element = getBaseModel().getElement(this.focusPath);

		if (element != null)
			this.focusModel = element.getReferredModel();
		else
			this.focusModel = null;

		while (focusModel instanceof DataType && focusPath.getNumberOfSegments() > 0)
		{
			this.focusPath.removeLastSegment();
			element = getBaseModel().getElement(this.focusPath);

			if (element != null)
				this.focusModel = element.getReferredModel();
			else
				this.focusModel = null;
		}
	}

	private Model getRootModel()
	{
		if (rootModel == null)
		{
			RepositoryManager repositoryManager = findRepositoryManager(root.getBaseModelId());
			if (repositoryManager == null)
				return null;
			rootModel = repositoryManager.getRepository().getModel(root.getBaseModelId(), true);
		}
		return rootModel;
	}

	private void unfocus()
	{
		focusModel = getBaseModel();
		focusPath.clear();
	}

	private Model getFocusModel()
	{
		return focusModel == null ? getBaseModel() : focusModel;
	}

	private RepositoryManager findRepositoryManager(ModelId rootModelId)
	{
		ArrayList<RepositoryManager> domains = new ArrayList<RepositoryManager>();
		for (RepositoryManager editDomain : RepositoryManager.getActiveManagers())
		{
			if (editDomain.getActiveEditors().size() > 0
					&& editDomain.getRepository().getModel(rootModelId, true) != null)
				domains.add(editDomain);

		}
		if (domains.size() == 1)
			return (RepositoryManager) domains.get(0);
		else
			return null;
	}

	TersusEditPart zoomed = null;

	private TraceEvent root;

	private void zoomGradually(TersusEditPart part)
	{
		Rectangle bounds = part.getFigure().getBounds();
		if ((bounds.height < 5 || bounds.width < 5) && part.getParent() instanceof TersusEditPart)
			zoomGradually((TersusEditPart) part.getParent());
		part.zoomToPart();

	}

	private void zoom(TersusEditPart part)
	{
		if (part != zoomed)
		{
			zoomGradually(part);
			zoomed = part;
		}
		// TODO Review this auto-generated method stub

	}

	private void loadFile(File file)
	{
		try
		{
			traceFile = file;
			rootModel = null;
			root = TraceEvent.load(new FileInputStream(file));
			unfocus();
			viewer.setInput(root);
		}
		catch (IOException e)
		{
			TersusWorkbench.log(e);
			MessageDialog.openError(getSite().getShell(), "Error", "Failed to open trace file");
			viewer.setInput(new TraceEvent());
		}

	}

	private IProject getCurrentProject()
	{
		IEditorPart activeEditor = getSite().getPage().getActiveEditor();
		if (activeEditor == null)
			return null;
		IEditorInput activeEditorInput = activeEditor.getEditorInput();
		IProject currentProject = (IProject) activeEditorInput.getAdapter(IProject.class);
		if (currentProject != null)
			return currentProject;
		IFile currentFile = (IFile) activeEditorInput.getAdapter(IFile.class);
		if (currentFile != null)
			return currentFile.getProject();
		return null;
	}

	private void loadFile()
	{
		FileDialog dialog = new FileDialog(TersusWorkbench.getActiveWorkbenchShell(), SWT.OPEN);
		IDialogSettings settings = DebugPlugin.getDefault().getDialogSettings();
		IProject project = getCurrentProject();
		if (project != null)
		{
			File traceFolder = project.getFolder(new Path("work/trace")).getLocation().toFile();
			if (traceFolder.exists())
			{
				dialog.setFilterPath(traceFolder.getAbsolutePath());
			}
		}
		dialog.setText("Select runtime trace file:");
		// dialog.setFilterExtensions(new String[] {"trace"});
		String filePath = dialog.open();
		if (filePath != null)
		{
			settings.put(DebugPlugin.LAST_TRACE_FILE, filePath);
			loadFile(new File(filePath));
		}
	}

	public  TreeViewer getViewer()
	{
		return viewer;
	}
}
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.debug;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import tersus.model.ModelId;
import tersus.model.Path;
import tersus.runtime.trace.EventType;

/**
 * An entry in the runtime trace (representing a simple or composite runtime event)
 * 
 * @author Youval Bronicki
 *
 */
public class TraceEvent
{
	TraceEvent parent;
	ArrayList<TraceEvent> children = new ArrayList<TraceEvent>();
	Path execPath, displayPath;
	String details;
	String eventName;
	String objectType;
	private ModelId baseModelId;
	long time;
	int indent = 0 ;
    private String comment;

	public static TraceEvent load(InputStream in) throws IOException
	{
		final TraceEvent root = new TraceEvent();
		BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
		ModelId currentModelId = null;
		TraceEvent previous = root;
		for (String line = reader.readLine(); line != null; line = reader.readLine())
		{
		    if (line.startsWith("@"))
		    {
		        currentModelId = new ModelId(line.substring(1));
		        if (root.baseModelId == null)
		            root.baseModelId = currentModelId; 
		        continue;
		    }
		    if (line.startsWith("//"))
		    {
		        continue;
		    }
		    StringTokenizer tokenizer = new StringTokenizer(line.trim(), "\t");
		    String pathStr = null;
		    String displayPathStr = null;
		    String timeStr = null;
			TraceEvent event = new TraceEvent();
			event.baseModelId = currentModelId;
			if (tokenizer.hasMoreTokens())
			    event.objectType = tokenizer.nextToken();
			if (tokenizer.hasMoreTokens())
			    event.eventName = tokenizer.nextToken();
			if (tokenizer.hasMoreTokens())
			    pathStr = tokenizer.nextToken();
			if (tokenizer.hasMoreTokens())
			    event.details = tokenizer.nextToken();
			if (tokenizer.hasMoreTokens())
			    event.comment = tokenizer.nextToken();
			if (tokenizer.hasMoreTokens())
			    displayPathStr = tokenizer.nextToken();
			if (tokenizer.hasMoreTokens())
			    timeStr = tokenizer.nextToken();
			    
			Path execPath = null;
			if (pathStr != null  && !pathStr.equals("-"))
			    event.execPath = new Path(pathStr);
			if (displayPathStr != null&& !displayPathStr.equals("-"))
			    event.displayPath =  new Path(displayPathStr);
			if (timeStr != null && ! timeStr.equals("-"))
			    event.time = Long.parseLong(timeStr);
			// Find the parent and attach the current event to it
			TraceEvent parent;
		    if (previous.eventName == EventType.FINISHED.toString())
		    	parent = previous.parent;
		    else
		    	parent = previous;
			
			if (event.execPath == null)
			    parent = root;
			while (parent != root && (! event.execPath.beginsWith(parent.execPath) || event.execPath.equals(parent.execPath)))
			    parent = parent.parent;
		    if (event.execPath != null && event.execPath.equals(parent.execPath) && parent.parent != null && event.execPath.equals(parent.parent.execPath))
			     	parent = parent.parent;
	        parent.addChild(event);
			previous = event;
		}
		return root;
	}
	/**
	 * @param Adds a child event to this <code>TraceEvent</code>
	 */
	protected void addChild(TraceEvent child)
	{
		children.add(child);
		child.parent = this;
		child.indent = this.indent + 1;

	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		return getDescription();
	}
	/**
	 * @return The list of children of this <code>TraceEvent</code>. If there are no children, an
	 * empty list is returned.
	 */
	public List<TraceEvent> getChildren()
	{
		return children;

	}
	/**
	 * @return A textual description of the <code>TraceEvent</code>.
	 */
	public String getDescription()
	{
	    String elementName = getName();
		if (eventName != null && (eventName.endsWith("ed") || eventName.equals("Set")))
		    return objectType+ " '" + getName() + "' " + eventName;
		else
			return eventName;


	}

    private String getName()
    {
        if (getDisplayPath() == null || getDisplayPath().getNumberOfSegments() == 0)
            return getBaseModelId().getName();
        else
            return getDisplayPath().getLastSegment().toString();
        
    }

    Path getDisplayPath()
    {
        return displayPath != null ? displayPath: execPath != null ? execPath: Path.EMPTY;
    }
    /**
	 * @return The id of the root model associated with this trace event. 
	 * The root model is the model of the top-most Flow that was executed
	 * to create the trace.
	 */
	ModelId getBaseModelId()
	{
		if (baseModelId != null)
			return baseModelId;
		if (parent != null)
			return parent.getBaseModelId();
		return null;
	}
}

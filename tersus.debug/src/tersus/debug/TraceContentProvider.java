/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.debug;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

/**
 * An <code>ITreeContentProvider</code> that wraps a <code>TraceEvent</code> hierarchy, adapting
 * it for display in a <code>TreeViewer</code>
 * 
 * @author Youval Bronicki
 * @author Liat Shiff
 * 
 */

public class TraceContentProvider implements IStructuredContentProvider, ITreeContentProvider
{
	public void inputChanged(Viewer v, Object oldInput, Object newInput)
	{
	}

	public void dispose()
	{
		// FUNC2 make sure the input stream is closed.
	}

	public Object[] getElements(Object parent)
	{
		return getChildren(parent);
	}

	public Object getParent(Object child)
	{
		return ((TraceEvent) child).parent;
	}

	public Object[] getChildren(Object parent)
	{
		return ((TraceEvent) parent).getChildren().toArray();
	}

	public boolean hasChildren(Object parent)
	{
		return (((TraceEvent) parent).getChildren().size() > 0);
	}
}

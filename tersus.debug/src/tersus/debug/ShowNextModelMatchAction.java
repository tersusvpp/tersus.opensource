/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.debug;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;

import tersus.editor.actions.TersusActionConstants;

/**
 * @author Liat shiff
 */
public class ShowNextModelMatchAction extends ShowMatchRowAction
{
	public ShowNextModelMatchAction(TraceView traceView)
	{
		super(traceView);

		setId(TersusActionConstants.SHOW_NEXT_MODEL_MATCH_ACTION_ID);
		setText(TersusActionConstants.SHOW_NEXT_MODEL_MATCH_ACTION_TEXT);
		setImageDescriptor(ImageDescriptor.createFromFile(TraceView.class, "icons/next.gif"));
	}

	public void run()
	{
		setSelectedElement();
		if (selectedElement != null)
		{
			initializeDialogVariables();
			TraceEvent currentEvent = getCurrentEvent();
			
			if (currentEvent != null)
			{
				TraceEvent nextEvent = getNaxtTraceEvent(currentEvent, null);
				if (nextEvent != null)
				{
					IStructuredSelection selection = new StructuredSelection(nextEvent);
					traceView.getViewer().setSelection(selection, true);
				}
			}
		}
		else
			MessageDialog.openInformation(traceView.getViewer().getControl().getShell(),
					"Runtime Trace View", "There is no selected element in Tersus editor.");
	}

	private TraceEvent getNaxtTraceEvent(TraceEvent currentEvent, TraceEvent prevEvent)
	{	
		Object[] children = contentProvider.getChildren(currentEvent);
		if (children != null && children.length > 0)
		{
			boolean prevEventIndexFound = (prevEvent != null ? false : true);
			for (Object cObject : children)
			{
				TraceEvent child = (TraceEvent) cObject;
				if (prevEventIndexFound)
				{
					if (isEventMatch(child))
						return child;
					if (contentProvider.hasChildren(child))
						return getNaxtTraceEvent(child, null);
				}

				else if (child.equals(prevEvent))
					prevEventIndexFound = true;
			}
		}

		Object parent = contentProvider.getParent(currentEvent);

		if (parent != null && parent instanceof TraceEvent)
		{
			Object[] brothers = contentProvider.getChildren(parent);
			if (brothers != null && brothers.length > 1)
			{
				boolean currentEventIndexFound = false;
				for (Object bObject : brothers)
				{
					TraceEvent brother = (TraceEvent) bObject;
					if (currentEventIndexFound)
					{
						if (isEventMatch(brother))
							return brother;
						else if (contentProvider.hasChildren(brother))
							return getNaxtTraceEvent(brother, null);
						
					}

					else if (currentEvent.equals(brother))
						currentEventIndexFound = true;
				}
			}
			return getNaxtTraceEvent((TraceEvent) parent, currentEvent);
		}

		// The next event was not found
		return null;
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.debug;

import java.util.ArrayList;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Liat Shiff
 */
public class JSONViewerContentProvider implements ITreeContentProvider
{

	public JSONViewerContentProvider()
	{
	}

	public void dispose()
	{
	}

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
	{
	}

	public Object[] getElements(Object inputElement)
	{
		ArrayList<Object> objects = null;

		if (inputElement instanceof JSONObject)
		{
			JSONObject jsonObject = (JSONObject) inputElement;
			String[] names = JSONObject.getNames(jsonObject);

			objects = new ArrayList<Object>();

			for (int i = 0; i < names.length; i++)
			{
				try
				{
					JSONElementDescriptor descriptor = new JSONElementDescriptor(names[i],
							jsonObject.get(names[i]));
					objects.add(descriptor);
				}
				catch (JSONException e)
				{
					e.printStackTrace();
				}
			}

		}
		return objects.toArray();
	}

	public Object[] getChildren(Object parentElement)
	{
		ArrayList<Object> objects = null;

		if (parentElement instanceof JSONElementDescriptor)
		{
			JSONElementDescriptor parentDescriptor = (JSONElementDescriptor) parentElement;
			objects = new ArrayList<Object>();
			Object value = parentDescriptor.getValue();

			if (value instanceof JSONObject)
			{
				JSONObject jsonObject = ((JSONObject) ((JSONElementDescriptor) parentElement)
						.getValue());
				String[] names = JSONObject.getNames(jsonObject);

				if (names != null)
				{
					for (String name : names)
					{
						try
						{
							JSONElementDescriptor descriptor = new JSONElementDescriptor(name,
									jsonObject.get(name));
							objects.add(descriptor);
						}
						catch (JSONException e)
						{
							e.printStackTrace();
						}
					}
				}
			}
			else if (value instanceof JSONArray)
			{
				JSONArray array = (JSONArray) value;
				for (int i = 0; i < array.length(); i++)
				{
					try
					{
						Object o = array.get(i);
							JSONElementDescriptor descriptor = new JSONElementDescriptor(
									"[" + Integer.toString(i) + "]", o);
							objects.add(descriptor);							
					}
					catch (JSONException e)
					{
						e.printStackTrace();
					}
				}
				System.out.println();
			}

		}
		return objects.toArray();
	}

	public Object getParent(Object element)
	{
		// TODO Auto-generated method stub
		return null;
	}

	public boolean hasChildren(Object element)
	{
		if (element instanceof JSONElementDescriptor)
			return ((JSONElementDescriptor) element).hasChildren();

		return false;
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model;

import java.io.Serializable;
import java.util.ArrayList;

import tersus.util.Misc;

/**
 * 
 */
public class Path implements ElementPath, Serializable
{
	int hash = 0;
	static final long serialVersionUID = 1;
	ArrayList<Role> segments = new ArrayList<Role>();
	public static final char SEPARATOR = '/';
	public static final Path EMPTY = new Path();

	public Path()
	{
	}

	public void addSegment(Role segment)
	{
		segments.add(segment);
		hash = 0;
	}

	public void addSegment(String segment)
	{
		segments.add(Role.get(segment));
		hash = 0;
	}

	public Path(String s)
	{
		if (s==null)
			throw new IllegalArgumentException("Null path");
		if (s.length() == 0)
			return;
		for (String token : s.split("/"))
		{
			if (token.length() == 0)
				throw new IllegalArgumentException("Illegal path '"+s+"' : empty path segment");
			segments.add(Role.get(token));
		}

	}

	public Path(Role role) // Ofer, 27/10/05
	{
		segments.add(role);
	}

	public String toString()
	{
		StringBuffer buffer = new StringBuffer();
		appendToBuffer(buffer);
		return buffer.toString();
	}

	/**
	 * Appends the textual representation of this Path to an existing <code>StringBuffer</code>
	 * 
	 * @param buffer
	 *            An existing StringBuffer (must not be null)
	 * @return The given StringBuffer (as a convinience)
	 */
	public StringBuffer appendToBuffer(StringBuffer buffer)
	{
		int index = 0;

		for (Object obj : segments)
		{
			if (index != 0)
				buffer.append('/');

			Role segment = (Role) obj;
			buffer.append(segment.toString());
			index++;
		}
		return buffer;
	}

	/**
	 * @return
	 */
	public int getNumberOfSegments()
	{
		return segments.size();
	}

	/**
	 * @param i
	 * @return
	 */
	public Role getSegment(int i)
	{
		return (Role) segments.get(i);
	}

	/**
	 * @return
	 */
	public Path getCopy()
	{
		Path newPath = new Path();
		newPath.segments.addAll(segments);
		return newPath;
	}

	/**
	 * @return The last segment of this <code>Path</code>, or null if the path is empty
	 */
	public Role getLastSegment()
	{
		if (segments.size() == 0)
			return null;
		return (Role) segments.get(segments.size() - 1);
	}

	/**
	 * Returns the tail of this path, with given length
	 * 
	 * @param length
	 * @return a new Path representing the tail, or null if the required length is too long
	 */
	public Path getTail(int length)
	{
		if (length > segments.size())
			return null; // TODO: throw an exception, or ignore (ArrayIndexOutOfBoundsException)
		Path tail = new Path();
		for (int i = segments.size() - length; i < segments.size(); i++)
			tail.addSegment(getSegment(i));
		return tail;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj)
	{
		if (obj instanceof Path)
			return isEquivalent((Path) obj);
		else
			return false;

	}

	public boolean isEquivalent(ElementPath other)
	{
		if (other == null)
			return false;
		if (other.getNumberOfSegments() != getNumberOfSegments())
			return false;
		for (int i = 0; i < getNumberOfSegments(); i++)
		{
			if (other.getSegment(i) != getSegment(i))
				return false;
		}
		return true;
	}

	/**
	 * Returns a new Path that contains a prefix of this Path
	 * 
	 * @param length
	 *            indicates the number of segments to return.
	 * @returns a new Path with the specified prefix
	 */
	public Path getPrefix(int length)
	{
		if (Misc.ASSERTIONS)
		{
			Misc.assertion(length >= 0);
			Misc.assertion(length <= segments.size());
		}
		Path prefix = new Path();
		for (int i = 0; i < length; i++)
			prefix.addSegment(getSegment(i));
		return prefix;
	}

	/**
	 * Checks whether this path begins with a given prefix
	 * 
	 * @param prefix
	 * @return true if this path begins with prefix, false otherwise
	 */
	public boolean beginsWith(Path prefix)
	{
		if (prefix == null)
			return false;
		if (prefix.getNumberOfSegments() > getNumberOfSegments())
			return false;
		for (int i = 0; i < prefix.getNumberOfSegments(); i++)
			if (prefix.getSegment(i) != getSegment(i))
				return false;
		return true;
	}

	public static Path getCommonPrefix(Path path1, Path path2)
	{
		if (Misc.ASSERTIONS)
		{
			Misc.assertion(path1 != null);
			Misc.assertion(path2 != null);
		}
		int maxLength = Math.min(path1.getNumberOfSegments(), path2.getNumberOfSegments());
		int length = 0;
		while (length < maxLength && path1.getSegment(length) == path2.getSegment(length))
		{
			++length;
		}
		return path1.getPrefix(length);
	}

	/**
	 * Clears this path (making it a path with 0 segments)
	 */
	public void clear()
	{
		segments.clear();
		hash = 0;
	}

	/**
	 * Copies another Path (making this equal to the other path)
	 * 
	 * @param path
	 *            - the Path to be copied
	 */
	public void copy(ElementPath path)
	{
		hash = 0;
		segments.clear();
		int size = path.getNumberOfSegments();
		for (int i = 0; i < size; i++)
			segments.add(path.getSegment(i));
	}

	/**
	 * 
	 */
	public void removeLastSegment()
	{
		hash = 0;
		segments.remove(segments.size() - 1);

	}

	/**
	 * @param suffix
	 */
	public void append(ElementPath suffix)
	{
		hash = 0;
		for (int i = 0; i < suffix.getNumberOfSegments(); i++)
			this.addSegment(suffix.getSegment(i));
	}

	public int hashCode()
	{
		if (hash == 0)
			hash = toString().hashCode();
		return hash;
	}

	public void setSegment(int i, Role role)
	{
		segments.set(i, role);
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;

import tersus.util.Enum;

/**
 * @author Youval Bronicki
 *
 */
public class LinkOperation extends Enum
{
	/**
	 * @param value
	 */
	protected LinkOperation(String value)
	{
		super(value);
	}
	public static LinkOperation ADD= new LinkOperation("Add"); 
	public static LinkOperation REPLACE= new LinkOperation("Replace"); 
	public static LinkOperation REMOVE= new LinkOperation("Remove"); 
	public static LinkOperation[] values = {REPLACE, ADD, REMOVE};
	/* (non-Javadoc)
	 * @see tersus.util.Enum#getValues()
	 */
	protected Enum[] getValues()
	{
		return values;
	}
}

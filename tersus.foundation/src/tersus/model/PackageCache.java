/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model;

/**
 * @author Liat Shiff
 */
public interface PackageCache
{
	public Package get(PackageId key);
	
	public void put(PackageId key, Package value);

	public Package remove(PackageId key);

	public void clear();
	
	public void fill(PackageCache cacheToFill);
}

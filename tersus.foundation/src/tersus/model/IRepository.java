/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model;

import java.io.IOException;


public interface IRepository
{
	double CURRENT_VERSION = 0.92;
	void modelObjectChanged(ModelObject modelObject);

	PluginDescriptor getPluginDescriptor(String plugin);

	Model getModel(ModelId refId, boolean b);

	ModelId getModelId(String prototypePath);

	boolean isFileReadOnly(PackageId packageId) throws IOException;

	boolean isPackageFolderReadOnly(PackageId packageId) throws IOException;
	
	void packageModified(Package pkg);
}

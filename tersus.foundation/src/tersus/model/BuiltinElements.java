package tersus.model;



public interface BuiltinElements
{
    public static final Role EARLY_INIT=Role.get("<Init>");
    public static final Role DONT_RUN=Role.get("<Don't Run>"); // Ofer, 31/8/10 (container for necessary elements we don't want to run)
    public static final Role DONE=Role.get("<Done>");
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. -  Initial API and implementation
 *************************************************************************************************/

package tersus.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * @author Youval Bronicki
 */
public class Package
{
	List<Model> models;

	private double repositoryVersion = IRepository.CURRENT_VERSION;

	private String lineSeparator = null;

	private PackageId packageId;

	private boolean deleted;
	
	private boolean isLibrary = false;

	private PackageId savedPackageId;

	private IRepository repository;

	/** the last known read-only status of this package (as opposed to the current status, which may be modified externally */
	private boolean savedReadOnly;

	public Package(IRepository repository)
	{
		setRepository(repository);
	}

	public String toString()
	{
		return String.valueOf(packageId);
	}

	public boolean isReadOnly()
	{
		return ((Repository)repository).isReadOnly(getPackageId());
	}

	public void setSavedReadOnly(boolean readOnly)
	{
		savedReadOnly = readOnly;
	}

	public boolean getSavedReadOnly()
	{
		return savedReadOnly;
	}

	public void addModel(Model model, boolean setModified)
	{
		addModel(model, -1, setModified);
	}
	
	public void addModel(Model model)
	{
		addModel(model, -1, true);
	}

	public void addModel(Model model, int index, boolean setModified)
	{
		if (models == null)
			models = new ArrayList<Model>();

		if (index >= 0)
			models.add(index, model);
		else
			models.add(model);
		
		model.setRepository(repository);
		model.setPackage(this);
		
		if (setModified)
			setModified();
	}

	/**
	 * Returns the list of models contained in this package. Note that the internal list of models
	 * is returned, so the client must not modify this list.
	 */
	public List<Model> getModels()
	{
		if (models == null)
			return Collections.emptyList();
		else
			return models;
	}
	
	public HashSet<Model> getModels(String pluginName)
	{
		HashSet<Model> modelsByPlugin = new HashSet<Model>();

		for (Model model: getModels())
		{
			if (pluginName.equals(model.getPlugin()))
				modelsByPlugin.add(model);
		}

		return modelsByPlugin;
	}

	public Model getModel(String modelName)
	{
		for (Model m: getModels())
		{
			if (modelName.equals(m.getName()))
				return m;
		}
		return null;
	}

	public PackageId getPackageId()
	{
		return packageId;
	}

	public boolean isDeleted()
	{
		return deleted;
	}

	public boolean isEmpty()
	{
		return models == null || models.isEmpty();
	}

	public void removeModel(Model model)
	{
		setModified();
		models.remove(model);
	}

	public void setId(PackageId packageId)
	{
		this.packageId = packageId;
	}

	public void setModified()
	{
		repository.packageModified(this);	
	}

	public void setDeleted(boolean deleted)
	{
		this.deleted = deleted;
		setModified();
	}

	public PackageId getSavedPackageId()
	{
		return savedPackageId;
	}

	public void setSavedPackageId(PackageId id)
	{
		this.savedPackageId = id;
	}

	public boolean isMoved()
	{
		return getSavedPackageId() != null && !getPackageId().equals(getSavedPackageId());
	}

	public String getLineSeparator()
	{
		return lineSeparator;
	}

	public void setLineSeparator(String lineSeparator)
	{
		this.lineSeparator = lineSeparator;
	}

	public double getRepositoryVersion()
	{
		return repositoryVersion;
	}

	public void setRepository(IRepository repository)
	{
		this.repository = repository;
	}

	public IRepository getRepository()
	{
		return repository;
	}

	public void setRepositoryVersion(double repositoryVersion)
	{
		this.repositoryVersion = repositoryVersion;
	}

	public boolean isNew()
	{
		return savedPackageId == null;
	}

	public boolean isLibrary()
	{
		return isLibrary;
	}

	public void setLibrary(boolean isLibrary)
	{
		this.isLibrary = isLibrary;
	}
}
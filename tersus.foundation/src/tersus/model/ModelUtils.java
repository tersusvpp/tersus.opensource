/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.- Initial API and implementation
 *************************************************************************************************/

package tersus.model;

import java.util.HashSet;



/**
 * @author Liat Shiff
 */

public class ModelUtils
{
    public static boolean isStrictDataElement(ModelObject modelObject)
    {
    	return (modelObject instanceof DataElement && ((ModelElement) modelObject).getReferredModel() instanceof DataType);
    }

    public static boolean isDisplayElement(ModelObject modelObject)
    {
    	return (modelObject instanceof SubFlow && isDisplayModel(((SubFlow) modelObject).getReferredModel()));
    }

    public static boolean isDisplayModel(Model model)
    {
        return model instanceof FlowModel && FlowType.DISPLAY == model.getProperty(BuiltinProperties.TYPE);
    }
    
    public static boolean isSystemModel(Model model)
    {
        return model instanceof FlowModel && FlowType.SYSTEM == model.getProperty(BuiltinProperties.TYPE);
    }

    public static boolean isSubProcess(ModelObject modelObject)
    {

        if (!(modelObject instanceof SubFlow))
        {
            return false;
        }
        
        Model referredModel =  ((SubFlow) modelObject).getReferredModel();
       
        return referredModel instanceof FlowModel 
                    && (FlowType.ACTION.equals(((FlowModel) referredModel).getType())
                        || FlowType.SERVICE.equals(((FlowModel) referredModel).getType()) 
                        || FlowType.OPERATION.equals(((FlowModel) referredModel).getType()));
    }

    public static boolean isService(Model model)
    {

        return model instanceof FlowModel && FlowType.SERVICE.equals(((FlowModel) model).getType());
    }
    
    /**
     * Returns true if model is popin, popup or dialog.
     * 
     * @param model
     * @return true if model is popin, popup or dialog.
     */
    public static boolean isDialog(ModelObject modelObject)
    {
        return modelObject instanceof FlowModel && ((Model) modelObject).getPluginDescriptor() != null
                && ((Model) modelObject).getPluginDescriptor().isDisplayAction();
    }

    public static boolean isTriggerModel(ModelObject modmodelObjectel)
    {
        return (modmodelObjectel instanceof Slot)
                && ("Trigger".equals(((ModelElement) modmodelObjectel).getRole().toString()));
    }

    public static boolean isExitModel(ModelObject modelObject)
    {
        return (modelObject instanceof Slot) && ("Exit".equals(((ModelElement) modelObject).getRole().toString()));
    }
    
    public static boolean isAncestorReferanceModel(ModelObject modelObject)
    {
        return (modelObject instanceof DataElement) && FlowDataElementType.PARENT.equals(((DataElement)modelObject).getType());
    }
    
    public static boolean isDisplayData(ModelObject modelObject)
    {
    	return (modelObject instanceof DataElement) && isDisplayModel(((DataElement)modelObject).getReferredModel());
    }

    public static boolean isConstant(ModelObject target)
    {
        if (target instanceof DataElement)
        {
    		Model refModel = ((DataElement)target).getReferredModel();
            return (refModel instanceof DataType && ((DataType) refModel).isConstant());
            
        }
        else
            return false;
    }
    
    public static Model getModel(ModelObject modelObject)
    {
        if (modelObject instanceof Model)
        {
            return (Model)modelObject;
        }
        if (modelObject instanceof ModelElement)
        {
    		return ((ModelElement)modelObject).getReferredModel();
        }
        return null;
    }
    
    public static HashSet<ModelElement> getFieldList(ModelElement element)
    {
        HashSet<ModelElement> fieldList = new HashSet<ModelElement>();
        
        if (isStrictDataElement(element))
        {
            Model model = element.getReferredModel();
            if (model instanceof DataType)
            {
                DataType dataTypeModel = (DataType)model;
                if (Composition.COLLECTION.equals(dataTypeModel.getComposition()))
                {
                    for (ModelElement e: dataTypeModel.getElements())
                        fieldList.addAll(getFieldList(e));
                }
                else if(Composition.ATOMIC.equals(dataTypeModel.getComposition()))
                    fieldList.add(element); 
            }
        }
        return fieldList;
    }
    
    public static boolean checkIfPathContainsElement(Model baseModel, Path path, ModelElement element)
    {   
        Model currentModel = baseModel;
        Model elementModel = null;
        
        elementModel = element.getReferredModel();
        if (elementModel == null)
			return false;
		
        for (int i=0; i< path.getNumberOfSegments(); i++ )
        {
            Role s = path.getSegment(i);
            ModelElement e = currentModel.getElement(s);
            
            if (e == element)
                return true;
            
            if (e == null || e instanceof Slot)
                return false;
            
            currentModel = e.getReferredModel();
            if (currentModel == null)
                return false;
            
            if (currentModel.equals(elementModel))
            	return true;
        }
        return false;
    }

	public static boolean isSecret(Model sourceType)
	{
		PluginDescriptor pluginDescriptor = sourceType.getPluginDescriptor();
		return pluginDescriptor != null && pluginDescriptor.isSecret();
	}

	public static boolean isProcessModel(Model model)
	{
        Object type = model.getProperty(BuiltinProperties.TYPE);
		return model instanceof FlowModel && (type == FlowType.ACTION || type == FlowType.SERVICE);
	}
	public static RelativePosition findNonOverlappingPosition(Model parent, ModelElement newElement)
	{
		RelativePosition p = newElement.getPosition();
		if (p == null)
			return null;
		boolean moved=false;
		do
		{
			moved = false;
			for (ModelElement e: parent.getElements())
			{
				if (p.equals(e.getPosition()))
				{				
					if (newElement instanceof Slot)
					{
						if (p.getX()==0 || p.getX() == 1)
						{
							p = new RelativePosition(p.getX(), p.getY()+Math.min(0.05,(1-p.getY())/2));
						}
						else
							p = new RelativePosition(p.getX()+Math.min(0.05,(1-p.getX())/2), p.getY());
					}
					else
					{
						p = new RelativePosition(p.getX()+Math.min(0.05,(1-p.getX())/2), p.getY()+Math.min(0.05,(1-p.getY())/2));
					}
					moved = true;
					break;
				}
			}
		}
		while (moved);
		return p;
	}
}

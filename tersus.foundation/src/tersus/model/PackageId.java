/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;


/**
 * @author Youval Bronicki
 *  
 */
public class PackageId implements Comparable
{
    private String path;

    private String name;

    private PackageId parent;

    public PackageId(String path)
    {
        if (path == null)
            throw new IllegalArgumentException(
                    "Can't create package id with null path");
        this.path = path;
        int lastSeparator = path.lastIndexOf(Path.SEPARATOR);
        if (lastSeparator < 0)
            init(null, path);
        else
            init(new PackageId(path.substring(0, lastSeparator)), path.substring(lastSeparator + 1));

    }

    /**
     * @param parent
     * @param string
     */
    public PackageId(PackageId parent, String name)
    {
        init(parent, name);
    }

    private void init(PackageId parent, String name)
    {
//        if (!Misc.isValidFileName(name))
//            throw new IllegalArgumentException("'"+name+"' is not a valid package name");
        this.parent = parent;
        this.name = name;
        if (parent == null)
            this.path = name;
        else
            this.path = parent.path + Path.SEPARATOR + name;
    }

    public String getPath()
    {
        return path;
    }

    public String toString()
    {
        return path;
    }
    
    public String getName()
    {
        return name;
    }
    
    public boolean equals(Object obj)
    {
        if (obj instanceof PackageId)
            return path.equals(((PackageId) obj).path);
        else
            return false;
    }

    public int hashCode()
    {
        return path.hashCode();
    }

    /**
     * @return
     */
    public PackageId getParent()
    {
        return parent;
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Object o)
    {
        return path.compareTo(((PackageId) o).path);
    }

    /**
     * @param id
     * @return
     */
    public boolean isAncestorOf(ModelId id)
    {
        return id != null && id.getPath().startsWith(getPath()+"/");
    }
    public boolean isAncestorOf(PackageId id)
    {
        return id != null && (this.equals(id) || id.getPath().startsWith(getPath()+"/"));
    }
}
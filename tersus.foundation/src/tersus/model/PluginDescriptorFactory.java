package tersus.model;

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import tersus.InternalErrorException;

public class PluginDescriptorFactory
{

	public static PluginDescriptor loadXML(InputStream in)
	{
		try
		{
			DocumentBuilder builder = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();
			Document document = builder.parse(in);
			document.normalize();
			PluginDescriptor descriptor = new PluginDescriptor();
			Element pluginDescriptorElement = PluginDescriptorFactory.getElement(document,
					PluginDescriptorFactory.PLUGIN_DESCRIPTOR_TAG, true);
			descriptor.setName(PluginDescriptorFactory.getElementContent(document, PluginDescriptorFactory.PLUGIN_TAG));
			descriptor.setType(pluginDescriptorElement
					.getAttribute(PluginDescriptorFactory.TYPE_ATTRIBUTE));
			String displayActionStr = PluginDescriptorFactory.getElementContent(document,
					PluginDescriptorFactory.DISPLAY_ACTION_TAG);
			if (displayActionStr != null)
				descriptor.setDisplayAction(Boolean.valueOf(displayActionStr)
						.booleanValue());
			descriptor
					.setJavaClass(PluginDescriptorFactory.getElementContent(document, PluginDescriptorFactory.JAVA_CLASS_TAG));
			
			descriptor.setTypePropagationGroup(PluginDescriptorFactory.getElementContent(document, PluginDescriptorFactory.TYPE_PROPAGATION_GROUP));
			descriptor.setDeprecated(PluginDescriptorFactory.getElementContent(document, PluginDescriptorFactory.DEPRECATED_TAG));
			descriptor.setDefaultTag(PluginDescriptorFactory.getElementContent(document, PluginDescriptorFactory.DEFAULT_TAG));
			
			setClassName(document, descriptor);
			descriptor
					.setJavascript(PluginDescriptorFactory.getElementContent(document, PluginDescriptorFactory.JAVASCRIPT_TAG));
			descriptor.setValidatorClass(PluginDescriptorFactory.getElementContent(document,
					PluginDescriptorFactory.VALIDATOR_TAG));
			descriptor.setIconFolder(PluginDescriptorFactory.getElementContent(document,
					PluginDescriptorFactory.ICON_FOLDER_TAG));
			descriptor.setTiers(pluginDescriptorElement.getAttribute("tiers"));
			descriptor.setDocumentation(PluginDescriptorFactory.getElementContent(document,
					PluginDescriptorFactory.DOCUMENTATION_TAG));
			descriptor.setPrototype(PluginDescriptorFactory.getElementContent(document, PluginDescriptorFactory.PROTOTYPE_TAG));
			String serviceClientStr = PluginDescriptorFactory.getElementContent(document,
					PluginDescriptorFactory.SERVICE_CLIENT_TAG);
			descriptor.serviceClient = Boolean.valueOf(serviceClientStr)
					.booleanValue();
			String webServiceStr = PluginDescriptorFactory.getElementContent(document, PluginDescriptorFactory.WEB_SERVICE_TAG);
			descriptor.webService = Boolean.valueOf(webServiceStr)
					.booleanValue();
			String autoInvokeStr = PluginDescriptorFactory.getElementContent(document,
					PluginDescriptorFactory.AUTOMATICALLY_INVOKED_TAG);
			if (autoInvokeStr != null)
				descriptor.automaticallyInvoked = Boolean
						.valueOf(autoInvokeStr).booleanValue();
			return descriptor;
	
		} catch (Exception e)
		{
			throw new ModelException("Failed to load plugin descriptor", e);
		}
	}
	private static void setClassName(Document document, PluginDescriptor descriptor)
	{
		String objectClassName = PluginDescriptorFactory.getElementContent(document,
				PluginDescriptorFactory.OBJECT_CLASS_TAG);
		if (objectClassName == null)
			descriptor.setObjectClass(null);
		else
			try
			{
				descriptor.setObjectClass(Class.forName(objectClassName));
			} catch (ClassNotFoundException e)
			{
				throw new InternalErrorException("Can't find object class "
						+ objectClassName + " for plugin " + descriptor.getName());
			}
	}

	public static final String TYPE_ATTRIBUTE = "type";
	public static final String PLUGIN_DESCRIPTOR_TAG = "PluginDescriptor";
	public static final String PLUGIN_TAG = "Plugin";
	public static final String JAVA_CLASS_TAG = "JavaClass";
	public static final String OBJECT_CLASS_TAG = "ObjectClass";
	public static final String JAVASCRIPT_TAG = "Javascript";
	public static final String VALIDATOR_TAG = "ValidatorClass";
	public static final String ICON_FOLDER_TAG = "IconFolder";
	public static final String DOCUMENTATION_TAG = "Documentation";
	public static final String SERVICE_CLIENT_TAG = "ServiceClient";
	public static final String DISPLAY_ACTION_TAG = "DisplayAction";
	public static final String WEB_SERVICE_TAG = "WebService";
	public static final String AUTOMATICALLY_INVOKED_TAG = "AutomaticallyInvoked";
	public static final String PROTOTYPE_TAG = "Prototype";
	public static final String TYPE_PROPAGATION_GROUP = "TypePropagationGroup";
	public static final String DEFAULT_TAG = "DefaultTag";
	public static final String DEPRECATED_TAG = "Deprecated";
	
	public static String getElementContent(Document document, String tag)
	{
		Element element = PluginDescriptorFactory.getElement(document, tag, false);
		if (element == null)
			return null;
		NodeList childNodes = element.getChildNodes();
		if (childNodes.getLength() > 1)
			throw new ModelException("Element <" + tag
					+ "> unexpectedly contains multiple nodes");
		if (childNodes.getLength() == 0)
			return null;
		String text = ((Text) childNodes.item(0)).getData();
		if (text == null)
			return null;
		text = text.trim();
		if (text.length() == 0)
			return null;
		return text;
	}
	public static Element getElement(Document document, String tag,
			boolean mandatory)
	{
		NodeList list = document.getElementsByTagName(tag);
		if (list.getLength() > 1)
			throw new ModelException("Invalid plugin descriptor: multiple <"
					+ tag + "> elements");
		if (list.getLength() == 0)
		{
			if (mandatory)
				throw new ModelException(
						"Invalid plugin descriptor: missing element <" + tag
								+ ">");
			else
				return null;
		}
		return (Element) list.item(0);
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/

package tersus.model;

import java.util.StringTokenizer;

/**
 * Definition of valid values of atomic data types.
 * 
 * @author Ofer Brandes
 * 
 */

public class ValidValues
{
	static final String FOREGIN_KEY = "Foregin Key";
	static final String VALUE_LIST = "Value List";
	static final String RANGE = "Range";
	static final String REGEX = "Regular Expression";
	
	private String type = null;
	private String tableName = null;	// For FOREGIN_KEY
	private String columnName = null;	// For FOREGIN_KEY
	private String[] values = null;		// For VALUE_LIST
	private String minValue = null;		// For RANGE
	private String maxValue = null;		// For RANGE
	private String pattern = null;		// For REGEX

	public ValidValues (String validValuesProerty)
						// Examples:
						// Foreign Key: Table=Categories, Column=Name
						// Value List: "Bakery", "Dairy" [Text]
						// Value List: -1, 0, 1 [Number]
						// Range: Range=[1-5] [Number, Date, Date and Time]
						// Regular Expression: Pattern=\d{1,8} [Text]
	{
		String validValues = validValuesProerty.trim();

		// Foreign key - reference to a column in a table.view
		if (validValues.startsWith("Table="))
		{
			type = FOREGIN_KEY;
			StringTokenizer tokenizer = new StringTokenizer(validValues, ",");
	        while (tokenizer.hasMoreTokens())
			{
				String token = tokenizer.nextToken();
				token = token.trim();
				if (token.startsWith("Table="))
					tableName = token.substring("Table=".length());
				else if (token.startsWith("Column="))
					columnName = token.substring("Column=".length());
			}
		}
		else if (validValues.startsWith("Range="))
		{
			type = RANGE;
			validValues = validValues.substring("Range=".length());
			if (validValues.charAt(0)=='[' && validValues.charAt(validValues.length()-1)==']')
				validValues = validValues.substring(1,validValues.length()-1);
			StringTokenizer tokenizer = new StringTokenizer(validValues, "-");
			if (tokenizer.hasMoreTokens())
			{
				String token = tokenizer.nextToken();
				minValue = token.trim();
			}
			if (tokenizer.hasMoreTokens())
			{
				String token = tokenizer.nextToken();
				maxValue = token.trim();
			}
		}
		else if (validValues.startsWith("Pattern="))
		{
			type = REGEX;
			pattern = validValues.substring("Pattern=".length());
		}
		else if (validValues.length()>0)
		{
			type = VALUE_LIST;
			StringTokenizer tokenizer = new StringTokenizer(validValues, ",");
			values = new String[tokenizer.countTokens()];
			int n = 0;
	        while (tokenizer.hasMoreTokens())
			{
				String token = tokenizer.nextToken();
				token = token.trim();
				if ((validValues.charAt(0)=='"' && validValues.charAt(validValues.length()-1)=='"') ||
					(validValues.charAt(0)=='\'' && validValues.charAt(validValues.length()-1)=='\''))
					token = token.substring(1,token.length()-1);
				values[n++] = token;
			}
		}
	}


	public boolean isForeignKey()
	{
		return FOREGIN_KEY.equals(type);
	}


	public boolean isValueList()
	{
		return VALUE_LIST.equals(type);
	}

	public boolean isRange()
	{
		return RANGE.equals(type);
	}

	public boolean isRegex()
	{
		return REGEX.equals(type);
	}

	public String getTableName()
	{
		return tableName;
	}

	public String getColumnName()
	{
		return columnName;
	}

	public String[] getValueList()
	{
		return values;
	}

	public String getMinValue()
	{
		return minValue;
	}

	public String getMaxValue()
	{
		return maxValue;
	}
}

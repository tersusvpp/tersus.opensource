/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 * Data Element (either a flow data element or a sub-element of a data models).
 * 
 * @author Ofer Brandes
 *  
 */

public class Note extends ModelElement
{
    public Note()
    {
    }

    private static List defaultProperties;
    static
    {
        defaultProperties = new ArrayList();
        defaultProperties.add(new MetaProperty(BuiltinProperties.ROLE,
                Role.class, false, false, false));
        defaultProperties.add(new MetaProperty(BuiltinProperties.COMMENT,
                String.class, true, false, false));
        defaultProperties.add(new MetaProperty(BuiltinProperties.POSITION,
                RelativePosition.class, true, false, false));
        defaultProperties.add(new MetaProperty(BuiltinProperties.SIZE,
                RelativeSize.class, true, false, false));
    };

 
    /*
     * (non-Javadoc)
     * 
     * @see tersus.model.ModelObject#getMetaProperties()
     */
    public List getDefaultProperties()
    {
        return defaultProperties;
    }


	@Override
	public Object getProperty(String key)
	{
		if (BuiltinProperties.ROLE.equals(key))
			return getRole();
		if (BuiltinProperties.POSITION.equals(key))
			return getPosition();
		if(BuiltinProperties.SIZE.equals(key))
			return getSize();
		return getPropertyBase(key);

	}


	@Override
	public boolean isPropertySet(String key)
	{
		return isPropertySetBase(key);
	}


	@Override
	public void setProperty(String key, Object value)
	{
		if (BuiltinProperties.ROLE.equals(key))
		{
			if (value != null && ! (value instanceof Role))
				illegalValue(key, value);
			setRole((Role)value);
		}
		else if (BuiltinProperties.POSITION.equals(key))
		{
			if (value != null && ! (value instanceof RelativePosition))
				illegalValue(key, value);
			setPosition((RelativePosition)value);
		}
		else if(BuiltinProperties.SIZE.equals(key))
		{
			if (value != null &&  ! (value instanceof RelativeSize))
				illegalValue(key, value);
			setSize((RelativeSize)value);
		}
		else 
			setPropertyBase(key,value);		
	}
}
/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model;

public class SaveFailedException extends RuntimeException
{
    private static final long serialVersionUID = 1L;
    private Exception[] errors;
    public SaveFailedException(String message, Exception[] errors)
    {
        super (message);
        this.errors = errors;
    }
    public Exception[] getErrors()
    {
        return errors;
    }
    
    public SaveFailedException(String message, Throwable cause)
    {
        super(message, cause);
    }
    

}

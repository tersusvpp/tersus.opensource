/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model;

import java.util.ArrayList;
import java.util.List;

import tersus.model.validation.Messages;
import tersus.model.validation.Problems;

public class Link extends ModelElement
{
	/**
	 * @param source
	 * @param target
	 */
	private static List<MetaProperty> defaultProperties;
	private LinkOperation operation = null;

	public List<MetaProperty> getDefaultProperties()
	{
		return defaultProperties;
	}

	static
	{
		defaultProperties = new ArrayList<MetaProperty>();
		defaultProperties.add(new MetaProperty(BuiltinProperties.ROLE, Role.class, false, false,
				false));
		defaultProperties.add(new MetaProperty(BuiltinProperties.SOURCE, Path.class, true, false,
				false));
		defaultProperties.add(new MetaProperty(BuiltinProperties.TARGET, Path.class, true, false,
				false));
		defaultProperties.add(new MetaProperty(BuiltinProperties.OPERATION, LinkOperation.class,
				true, false, false));
	}
	static final long serialVersionUID = 1;

	/**
	 * Default constructor - used for creating links from templates
	 */
	public Link()
	{
	}

	public Link(Path source, Path target)
	{
		setSource(source.getCopy());
		setTarget(target.getCopy());
	}

	public Path getSource()
	{
		return source;
	}

	public void setSource(Path source)
	{
		Path oldSource = this.source;
		this.source = source;
		markModified();
		firePropertyChange(BuiltinProperties.SOURCE, oldSource, source);
	}

	public void setSource(String source)
	{
		if (source != null)
			setSource(new Path(source));
		else
			setSource((Path) null);
	}

	public Path getTarget()
	{
		return target;
	}

	public void setTarget(Path target)
	{
		Path oldTarget = this.target;
		this.target = target;
		markModified();
		firePropertyChange(BuiltinProperties.TARGET, oldTarget, target);
	}

	public void setTarget(String target)
	{
		if (target != null)
			setTarget(new Path(target));
		else
			setTarget((Path) null);
	}

	private Path source;
	private Path target;

	public String toString()
	{
		return getSource() + "->" + getTarget();
	}

	public boolean isValid()
	{
		try
		{
			validate(false);
			return true;
		}
		catch (InvalidModelException e)
		{
			return false;
		}
	}

	public boolean validate(boolean fullValidation)
	{
		FlowModel parent = (FlowModel) getParentModel();
		if (parent == null)
			return true; // The link is not attached to a parent so
							// validation is meaningless
		if (!validateLinkSource() || !validateLinkTarget())
			return false;
		if (source.equals(target))
			Model.notifyInvalidModel(getParentModel().getId(), getRole(), Problems.INVALID_FLOW,
					"The target is the same as the source");
		if (fullValidation)
			checkInconsistentFlow();
		if (!endTypesAreCompatible())
		{
			Model.notifyInvalidModel(getParentModel().getId(), getRole(), Problems.INVALID_FLOW,
					"The source and target datatypes are not compatible");
			return false;
		}

		return true;
	}

	public boolean validateLinkSource()
	{
		boolean valid = true;
		Path path = getSource();
		FlowModel parent = (FlowModel) getParentModel();

		if (!parent.isValidLinkPath(path))
		{
			FlowModel.notifyInvalidModel(parent.getId(), getRole(), Problems.INVALID_FLOW,
					Messages.THE_SOURCE_IS_NOT_VALID);
			valid = false;
		}
		ModelElement element = parent.getElement(path);
		if (element instanceof Slot)
		{
			SlotType type = ((Slot) element).getType();
			if (type == SlotType.EXIT || type == SlotType.ERROR)
			{
				if (path.getNumberOfSegments() != 2)
				{
					FlowModel
							.notifyInvalidModel(
									parent.getId(),
									getRole(),
									Problems.INVALID_FLOW,
									Messages.AN_EXIT_OR_ERROR_EXIT_OF_THE_PROCESS_IS_NOT_VALID_AS_A_LINK_SOURCE);
					valid = false;
				}
			}
			else if (path.getNumberOfSegments() > 1)
			{
				FlowModel
						.notifyInvalidModel(
								parent.getId(),
								getRole(),
								Problems.INVALID_FLOW,
								Messages.A_TRIGGER_OR_INPUT_SLOT_OF_A_SUB_PROCESS_IS_NOT_VALID_AS_A_LINK_SOURCE);
				valid = false;
			}
		}
		return valid;
	}

	public boolean validateLinkTarget()
	{
		boolean valid = true;
		FlowModel parent = (FlowModel) getParentModel();
		if (!parent.isValidLinkPath(getTarget()))
		{
			FlowModel.notifyInvalidModel(parent.getId(), getRole(), Problems.INVALID_FLOW,
					Messages.FLOW_TARGET_IS_NOT_VALID);
			valid = false;
		}
		ModelElement element = parent.getElement(getTarget());
		if (element instanceof Slot)
		{
			SlotType type = ((Slot) element).getType();
			if (type == SlotType.EXIT || type == SlotType.ERROR)
			{
				if (getTarget().getNumberOfSegments() > 1)
				{
					FlowModel
							.notifyInvalidModel(
									parent.getId(),
									getRole(),
									Problems.INVALID_FLOW,
									Messages.AN_EXIT_OR_ERROR_EXIT_OF_A_CHILD_IS_NOT_VALID_AS_A_LINK_TARGET);
					valid = false;
				}
			}
			else if (getTarget().getNumberOfSegments() == 1)
			{
				FlowModel.notifyInvalidModel(parent.getId(), getRole(), Problems.INVALID_FLOW,
						Messages.AN_TRIGGER_OR_INPUT_SLOT_OF_A_PROCESS_IS_NOT_VALID_AS_LINK_TARGET);
				valid = false;
			}
		}
		else if (!(element instanceof DataElement && getTarget().getNumberOfSegments() == 1
				&& !element.isRepetitive() && isSourceRepetitive()))
		{
			Model currentModel = parent;
			for (int i = 0; i < getTarget().getNumberOfSegments() - 1; i++)
			{
				ModelElement e = currentModel.getElement(getTarget().getSegment(i));
				if (e != null && e.isRepetitive())
				{
					FlowModel.notifyInvalidModel(parent.getId(), getRole(), Problems.INVALID_FLOW,
							Messages.FLOW_INTO_A_CHILD_OF_A_REPETITIVE_ELEMENT);
					valid = false;
					break;
				}
				currentModel = e.getReferredModel();
			}
		}
		return valid;
	}

	public boolean checkInconsistentFlow()
	{
		boolean isTargetRepetitive = false;
		boolean isSourceRepetitive = false;

		ModelElement targetElmenet = getParentModel().getElement(getTarget());
		ModelElement sourceElmenet = getParentModel().getElement(getSource());

		if (targetElmenet instanceof Slot)
			isTargetRepetitive = ((Slot) targetElmenet).isRepetitive()
					|| getTargetElement().isRepetitive();
		else
			// Target is a data element
			isTargetRepetitive = isDataElemetRepetitive(getTarget(), getParentModel());

		if (sourceElmenet instanceof Slot)
			isSourceRepetitive = ((Slot) sourceElmenet).isRepetitive()
					|| getSourceElement().isRepetitive();
		else
			// Source is a data element
			isSourceRepetitive = isDataElemetRepetitive(getSource(), getParentModel());

		if (!isTargetRepetitive && isSourceRepetitive)
		{
			FlowModel.notifyInvalidModel(getParentModel().getId(), getRole(),
					Problems.INCONSISTENT_FLOW,
					"The flow source is repetitive while the flow target is non-repetitive");
			return false;
		}
		
		return true;
	}

	private boolean isDataElemetRepetitive(Path path, Model parent)
	{
		for (int i = 0; i < path.getNumberOfSegments(); i++)
		{
			ModelElement e = parent.getElement(path.getSegment(i));

			if (e.isRepetitive())
				return true;

			parent = e.getReferredModel();
		}

		return false;
	}

	private boolean endTypesAreCompatible()
	{
		Model sourceType = getType(source);
		Model targetType = getType(target);
		if (getOperation() == LinkOperation.REMOVE)
		{
			return true;
		}
		if (sourceType == targetType)
			return true;
		if (sourceType == null || targetType == null)
			return true; // FUNC3 Should this be a warning?

		ModelElement targetElement = getParentModel().getElement(target);
		if (targetElement instanceof DataElement
				&& (Boolean.TRUE.equals(targetType.getProperty(BuiltinProperties.CONSTANT))))
			return true;
		if (BuiltinModels.NOTHING_ID.equals(targetType.getId()))
			return true;
		if (BuiltinModels.ANYTHING_ID.equals(sourceType.getId()))
			return true;
		if (BuiltinModels.ANYTHING_ID.equals(targetType.getId())
				&& !BuiltinModels.NOTHING_ID.equals(sourceType.getId()) && !ModelUtils.isSecret(sourceType))
			return true;
		if (BuiltinPlugins.DYNAMIC_DISPLAY.equals(sourceType.getPlugin())
				&& ModelUtils.isDisplayModel(targetType))
			return true;
		if (BuiltinPlugins.DYNAMIC_DISPLAY.equals(targetType.getPlugin())
				&& ModelUtils.isDisplayModel(sourceType))
			return true;
		String sourcePlugin = sourceType.getPluginJavaClass();
		String targetPlugin = targetType.getPluginJavaClass();
		if ("tersus.runtime.DateAndTimeHandler".equals(sourcePlugin)
				&& "tersus.runtime.DateHandler".equals(targetPlugin)
				|| "tersus.runtime.DateAndTimeHandler".equals(targetPlugin)
				&& "tersus.runtime.DateHandler".equals(sourcePlugin))
			return true;
		// NICE3 - remove after all models are updated to contain explicit Text
		// Plugin
		if (targetPlugin == null || targetPlugin.equals(""))
			targetPlugin = getTextPlugin();

		if (sourcePlugin == null || targetPlugin.equals(""))
			sourcePlugin = getTextPlugin();

		return (sourceType.getProperty(BuiltinProperties.COMPOSITION) == Composition.ATOMIC
				&& targetType.getProperty(BuiltinProperties.COMPOSITION) == Composition.ATOMIC && sourcePlugin
				.equals(targetPlugin));
	}

	private String getTextPlugin()
	{
		String plugin = getRepository().getModel(BuiltinModels.TEXT_ID, true).getPluginJavaClass();
		if (plugin == null)
			plugin = "";
		return plugin;
	}

	/**
	 * Returns the type of an element of the link's parent model.
	 * 
	 * @param path
	 *            - A Path that identifies the required element
	 * @return the DataType referred to by the specified element, or null if the specified elemnt
	 *         does not refer a data type.
	 */
	private Model getType(Path path)
	{
		ModelElement element = getParentModel().getElement(path);
		if (element == null)
			return null;
		ModelId id = element.getRefId();
		if (id == null)
			return null;
		Model referredModel = getRepository().getModel(id, true);
		return referredModel;
	}


	public boolean isBidirectional()
	{
		FlowModel parent = (FlowModel) getParentModel();
		if (parent == null)
			return false;
		ModelElement sourceElement = parent.getElement(getSource());
		ModelElement targetElement = parent.getElement(getTarget());

		// FUNC3 add more rules (requires language design decisions)
		if (getTarget().getNumberOfSegments() == 2 && targetElement instanceof Slot
				&& SlotType.IO == ((Slot) targetElement).getType())
			return true;

		if (getSource().getNumberOfSegments() == 1 && sourceElement instanceof Slot
				&& SlotType.IO == ((Slot) sourceElement).getType())
			if (getTarget().getNumberOfSegments() == 1 && targetElement instanceof DataElement
					&& FlowDataElementType.INTERMEDIATE == ((DataElement) targetElement).getType())
				return true;
		return false;
	}

	/**
	 * Checks whether this links 'generates' new sub-flows when it is invoked A link is defined as
	 * 'generating' if its target is a trigger of a repetitive sub-flow
	 * 
	 * @return
	 */
	public boolean isGenerating()
	{

		if (target.getNumberOfSegments() != 2)
			return false;
		ModelElement targetSubFlow = getParentModel().getElement(target.getSegment(0));
		if (!(targetSubFlow instanceof SubFlow))
			return false;
		if (!targetSubFlow.isRepetitive())
			return false;
		ModelElement targetSlot = getParentModel().getElement(target);
		if (!(targetSlot instanceof Slot))
			return false;
		return (SlotType.TRIGGER == ((Slot) targetSlot).getType());
	}

	public void setOperation(LinkOperation operation)
	{
		if (this.operation != operation)
		{
			LinkOperation old = this.operation;
			this.operation = operation;
			firePropertyChange(BuiltinProperties.OPERATION, old, operation);
		}
	}

	public boolean isOperationSet()
	{
		return (operation != null);
	}

	public LinkOperation getOperation()
	{
		if (operation == null)
			return getDefaultOperation();
		else
			return operation;
	}

	public LinkOperation getDefaultOperation()
	{
		if (getParentModel() == null)
			return null;
		ModelElement lastElement = getParentModel().getElement(target);
		if (lastElement == null)
			return null;
		if (lastElement.isRepetitive())
			return LinkOperation.ADD;
		else
			return LinkOperation.REPLACE;
	}

	/**
	 * @return the element of the owning flowmodel that corresponds to the first segment in the
	 *         target path
	 */
	public ModelElement getTargetElement()
	{
		return getParentModel().getElement(target.getSegment(0));
	}

	/**
	 * @return the element of the owning flowmodel that corresponds to the first segment in the
	 *         source path
	 */
	public ModelElement getSourceElement()
	{
		return getParentModel().getElement(source.getSegment(0));
	}

	public boolean isConstantTrigger()
	{
		return getTargetElement() != null
				&& getTargetElement().getReferredModel() != null
				&& Boolean.TRUE.equals(getTargetElement().getReferredModel().getProperty(
						BuiltinProperties.CONSTANT));
	}

	public boolean isSourceRepetitive()
	{
		Model model = getParentModel();

		try
		{
			for (int i = 0; i < getSource().getNumberOfSegments(); i++)
			{
				Role role = getSource().getSegment(i);
				ModelElement e = model.getElement(role);
				if (e.isRepetitive())
					return true;
				model = e.getReferredModel();
			}
		}
		catch (NullPointerException e)
		{
			throw new InvalidLinkException("Invalid source.", e);
		}

		return false;

	}

	public boolean isTargetRepetitive()
	{
		Model model = getParentModel();

		try
		{
			for (int i = 0; i < getTarget().getNumberOfSegments(); i++)
			{
				Role role = getTarget().getSegment(i);
				ModelElement e = model.getElement(role);
				if (e.isRepetitive())
					return true;
				model = e.getReferredModel();
			}
		}
		catch (NullPointerException e)
		{
			throw new InvalidLinkException("Invalid target.", e);
		}

		return false;

	}

	@Override
	public Object getProperty(String key)
	{
		if (BuiltinProperties.ROLE.equals(key))
			return getRole();
		else if (BuiltinProperties.SOURCE.equals(key))
			return getSource();
		else if (BuiltinProperties.TARGET.equals(key))
			return getTarget();
		else if (BuiltinProperties.OPERATION.equals(key))
			return getOperation();
		else
			return getPropertyBase(key);
	}

	@Override
	public boolean isPropertySet(String key)
	{
		if (BuiltinProperties.OPERATION.equals(key))
			return isOperationSet();
		else
			return isPropertySetBase(key);
	}

	@Override
	public void setProperty(String key, Object value)
	{
		if (BuiltinProperties.ROLE.equals(key))
		{
			if (value != null && !(value instanceof Role))
				illegalValue(key, value);
			setRole((Role) value);
		}
		else if (BuiltinProperties.SOURCE.equals(key))
		{
			if (value instanceof String)
				setSource((String) value);
			else if (value == null || value instanceof Path)
				setSource((Path) value);
			else
				illegalValue(key, value);
		}
		else if (BuiltinProperties.TARGET.equals(key))
		{
			if (value instanceof String)
				setTarget((String) value);
			else if (value == null || value instanceof Path)
				setTarget((Path) value);
			else
				illegalValue(key, value);
		}
		else if (BuiltinProperties.OPERATION.equals(key))
		{
			if (value != null && !(value instanceof LinkOperation))
				illegalValue(key, value);
			setOperation((LinkOperation) value);
		}
		else
			setPropertyBase(key, value);

	}
}

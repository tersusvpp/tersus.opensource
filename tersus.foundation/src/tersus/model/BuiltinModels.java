/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;


/**
 * 
 * Id's of built-in common models (e.g. basic data types like Number or Date).
 * 
 * @author Ofer Brandes
 * 
 */

public class BuiltinModels
{
	/** Package of common models (e.g. basic data types like Number or Date) */
	private static final String COMMON_PACKAGE = "Common";
	
	/** Package of common data types (e.g. Number or Date) */
	public static final PackageId DATA_TYPES_PACKAGE = new PackageId(COMMON_PACKAGE + "/" + "Data Types");
	private static final PackageId DATA_STRUCTURES_PACKAGE = new PackageId(COMMON_PACKAGE + "/" + "Data Structures");
	/** Id of "Text" built-in common data type (any string) */
	public static final ModelId TEXT_ID = new ModelId(DATA_TYPES_PACKAGE,"Text");

	public static final ModelId SECRET_TEXT_ID = new ModelId(DATA_TYPES_PACKAGE,"Secret Text");
	
	/** Id of "Number" built-in common data type (any number) */
	public static final ModelId NUMBER_ID = new ModelId (DATA_TYPES_PACKAGE,"Number");
	
	/** Ids of "Date" & "Date and Time" built-in common data types */
	public static final ModelId DATE_ID = new ModelId(DATA_TYPES_PACKAGE,"Date");
	public static final ModelId DATE_AND_TIME_ID = new ModelId(DATA_TYPES_PACKAGE,"Date and Time");

	/** Id of "Boolean" built-in data type */
	public static final ModelId BOOLEAN_ID = new ModelId(DATA_TYPES_PACKAGE,"Boolean");
	
	/** Id of "Binary" built-in common data type */
	public static final ModelId BINARY_ID = new ModelId (DATA_TYPES_PACKAGE,"Binary");
	
	public static final ModelId ANYTHING_ID = new ModelId(DATA_TYPES_PACKAGE, "Anything");
	public static final ModelId NOTHING_ID = new ModelId(DATA_TYPES_PACKAGE, "Nothing");
	/** Id of "File" built-in common data type (any date) */
    public static final ModelId FILE_ID = new ModelId(DATA_STRUCTURES_PACKAGE,"File");
    public static final ModelId[] modelIds = { TEXT_ID, NUMBER_ID, DATE_ID, BINARY_ID, ANYTHING_ID, NOTHING_ID, FILE_ID}; 

	public static final ModelId ERROR_ID =  new ModelId(DATA_STRUCTURES_PACKAGE,"Error");
	public static final String ERROR_MESSAGE = "Message";
	public static final String ERROR_DETAILS = "Details";
	public static final String ERROR_CAUSE = "Cause"; // Backwards compatibility (Error/Cause element)
	public static final String ERROR_LOCATION = "Location";
	public static final ModelId MODEL_ID =  new ModelId(DATA_STRUCTURES_PACKAGE+"/Model/Model");
	public static final ModelId MODEL_ELEMENT_ID =  new ModelId(DATA_STRUCTURES_PACKAGE+"/Model Element/Model Element");
    public static final ModelId MAP_ID =  new ModelId(DATA_TYPES_PACKAGE,"Map");
    public static final ModelId ROW_ID =  new ModelId(DATA_STRUCTURES_PACKAGE,"Row");
    public static final ModelId CELL_ID =  new ModelId(DATA_STRUCTURES_PACKAGE,"Cell");

	public static final ModelId TEXT_CONSTANT_ID = new ModelId("Common/Templates/Constants/Text/Text");
	public static final ModelId NUMBER_CONSTANT_ID = new ModelId("Common/Templates/Constants/Number/Number");
	public static final ModelId DATE_CONSTANT_ID = new ModelId("Common/Templates/Constants/Date/Date");
	public static final ModelId DATE_AND_TIME_CONSTANT_ID = new ModelId("Common/Templates/Constants/Date and Time/Date and Time");
	public static final ModelId YES_CONSTANT_ID = new ModelId("Common/Constants/Yes");
	public static final ModelId NO_CONSTANT_ID = new ModelId("Common/Constants/No");
		
	public static final Role CONTENT_ROLE=Role.get("Content");

	public static final Role CONTENT_TYPE_ROLE=Role.get("Content Type");
	public static final Role FILENAME_ROLE=Role.get("File Name");
    public static final ModelId SYSTEM_TEMPLATE_ID = new ModelId("Common/Templates/Basic/System/System");
    public static final ModelId VIEW_TEMPLATE_ID = new ModelId("Common/Templates/Display/View/View");
    public static final ModelId ACTION_TEMPLATE_ID = new ModelId("Common/Templates/Basic/Action/Action");
    public static final ModelId SERVICE_TEMPLATE_ID = new ModelId("Common/Templates/Basic/Service/Service");
    public static final ModelId CALL_WEB_SERVICE_TEMPLATE_ID = new ModelId("Common/Templates/Miscellaneous/Call Web Service/Call Web Service");
    public static final ModelId DATA_STRUCTURE_TEMPLATE_ID =  new ModelId("Common/Templates/Composite Data Types/Data Structure/Data Structure");
    public static final ModelId XML_DATA_STRUCTURE_TEMPLATE_ID = new ModelId("Common/Templates/Data Structures/XML Data Structure/XML Data Structure");

    public static final ModelId GROUP_TEMPLATE_ID = new ModelId("Common/Templates/Display/Group/Group");	
    public static final ModelId LEGACY_PANE_TEMPLATE_ID = new ModelId("Common/Templates/Display/Pane/Pane");
    public static final ModelId PANE_TEMPLATE_ID = new ModelId("Common/Templates/UI/Pane/Pane");
    
    public static final ModelId DATA_RECORD_TEMPLATE_ID = new ModelId("Common/Templates/Composite Data Types/Database Record/Database Record");

    public static final ModelId CREATE_FROM_TEMPLATE_ID = new ModelId("Common/Templates/Miscellaneous/Create from Template/Create from Template");
    public static final ModelId DATABASE_QUERY_ID = new ModelId("Common/Templates/Database/Database Query/Database Query");
    public static final ModelId RANGE_ID = new ModelId("Common/Templates/Collections/Range/Range");

    // Tersus DB types
    public static final ModelId[] ALL_DATA_TYPES = new ModelId[] { TEXT_ID, NUMBER_ID, DATE_ID, DATE_AND_TIME_ID, BOOLEAN_ID, BINARY_ID };

    public static final ModelId COLUMN_DESCIPTOR = new ModelId(DATA_STRUCTURES_PACKAGE, "Column Descriptor");
    
    public static final ModelId RESULT_SET = new ModelId(DATA_STRUCTURES_PACKAGE, "Result Set");
    public static final ModelId PROPERTY = new ModelId(DATA_STRUCTURES_PACKAGE, "Property");
       
    //added by Danny:
    public static final ModelId TABLE_DESCRIPTOR = new ModelId(DATA_STRUCTURES_PACKAGE,"Table Descriptor");

	public static final ModelId IMAGE_TEMPLATE_ID = new ModelId("Common/Templates/HTML/Image/Image");

	public static final ModelId PLAY_SOUND_ID = new ModelId("Common/Templates/Display Actions/Play Sound/Play Sound");

	public static final ModelId HTTP_HEADER_ID = new ModelId(DATA_STRUCTURES_PACKAGE, "HTTP Header");

}

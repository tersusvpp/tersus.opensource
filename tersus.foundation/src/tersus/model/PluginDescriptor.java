/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * @author Youval Bronicki
 */
public class PluginDescriptor
{
	public PluginDescriptor()
	{
	}

	private String javaClass;

	private String javascript;

	private String validatorClass;

	private String iconFolder;

	private String documentation;

	private String type;
	
	private ArrayList<String> tiers;

	public boolean serviceClient;

	private boolean databasePlugin;

	private static final String SERVICE_JAVASCRIPT = "ServiceNode";

	private boolean allowedOnServer;

	private boolean displayAction = false;

	private String name;

	public boolean webService = false;

	public boolean automaticallyInvoked = true;

	private String prototype;

	private String typePropagationGroup;

	private String defaultTag;
	
	private boolean deprecated;

	private Class<?> objectClass = null;

	private boolean isSecret = false;

	public static final String ALL_EXITS = "ALL_EXITS";

	public static final String ALL_TRIGERS = "ALL_TRIGGERS";

	public void setDisplayAction(boolean displayAction)
	{
		this.displayAction = displayAction;
	}

	public void setName(String name)
	{
		this.name = name;
		if (name.indexOf("Database/") >= 0)
			databasePlugin = true;
	}

	public boolean isDatabasePlugin()
	{
		return databasePlugin;
	}

	public void setDatabasePlugin(boolean databasePlugin)
	{
		this.databasePlugin = databasePlugin;
	}

	public void setTiers(String tiers)
	{
		if (tiers == null)
			tiers = "";

		this.tiers = new ArrayList<String>();
		for (String tier : tiers.split(","))
		{
			this.tiers.add(tier.trim());
		}

		this.allowedOnServer = ("client".compareToIgnoreCase(tiers.trim()) != 0);
	}

	public ArrayList<String> getTiers()
	{
		return this.tiers;
	}
	
	public boolean isClientTierOnly()
	{
		if (tiers.size() != 1)
			return false;
		
		return "client".equals(tiers.get(0).toLowerCase());
	}
	
	public boolean isServerTierOnly()
	{
		if (tiers.size() != 1)
			return false;
		
		return "server".equals(tiers.get(0).toLowerCase());
	}
	
	public String getIconFolder()
	{
		return iconFolder;
	}

	public void setIconFolder(String iconFolder)
	{
		this.iconFolder = iconFolder;
	}

	public String getDocumentation()
	{
		return documentation;
	}

	public void setDocumentation(String documentation)
	{
		this.documentation = documentation;
	}

	public String getJavaClass()
	{
		return javaClass;
	}

	public void setJavaClass(String javaClass)
	{
		this.javaClass = javaClass;
		isSecret = javaClass != null && javaClass.indexOf("Secret") >=0;
	}

	public String getJavascript()
	{
		if (javascript == null || isServiceClient() && isDatabasePlugin()) // Indicates that
			// client-side databases
			// are disabled
			return SERVICE_JAVASCRIPT;
		else
			return javascript;
	}

	public void setJavascript(String javascript)
	{
		this.javascript = javascript;
	}

	public void setTypePropagationGroup(String typePropagationGroup)
	{
		this.typePropagationGroup = typePropagationGroup;
	}

	public void setDefaultTag(String defaultTag)
	{
		this.defaultTag = defaultTag;
	}

	public String getDefaultTag()
	{
		return this.defaultTag;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public String getValidatorClass()
	{
		return validatorClass;
	}

	public void setValidatorClass(String validatorClass)
	{
		this.validatorClass = validatorClass;
	}

	public boolean isService()
	{
		return serviceClient;
	}

	public boolean isServiceClient()
	{
		return javascript == null || serviceClient;
	}

	public void setServiceClient(boolean serviceClient)
	{
		this.serviceClient = serviceClient;
	}

	public boolean allowedOnServer()
	{
		return allowedOnServer;
	}

	public void setAllowedOnServer(boolean allowedOnServer)
	{
		this.allowedOnServer = allowedOnServer;
	}

	public String getName()
	{
		return name;
	}

	public boolean isDisplayAction()
	{
		return displayAction;
	}

	public boolean isWebService()
	{
		return webService;
	}

	public boolean isAutomaticallyInvoked()
	{
		return automaticallyInvoked;
	}

	public String getPrototype()
	{
		return prototype;
	}

	public void setPrototype(String prototype)
	{
		this.prototype = prototype;
	}

	public boolean isDeprecated()
	{
		return deprecated;
	}

	public void setDeprecated(String deprecated)
	{
		if (deprecated != null && "yes".equals(deprecated))
			this.deprecated = true;
		else
			this.deprecated = false;
	}
	
	public Class<?> getObjectClass()
	{
		return objectClass;
	}

	public HashSet<String> getTypePropagationGroup()
	{
		HashSet<String> nameList = new HashSet<String>();

		if (typePropagationGroup != null)
			for (String element : typePropagationGroup.split(","))
			{
				if (element.equals(PluginDescriptor.ALL_TRIGERS)
						|| element.equals(PluginDescriptor.ALL_EXITS) || element.contains("|"))
					nameList.add(element);
				else
					nameList.add("<" + element + ">");
			}

		return nameList;
	}

	public void setObjectClass(Class<?> objectClass)
	{
		this.objectClass = objectClass;
	}

	public boolean isSecret()
	{
		return isSecret ;
	}
}

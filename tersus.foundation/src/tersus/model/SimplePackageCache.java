/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model;

import java.util.HashMap;

/**
 * @author Liat Shiff
 */
public class SimplePackageCache implements PackageCache
{
	private HashMap<PackageId, Package> packageCache;
	
	public SimplePackageCache()
	{
		packageCache = new HashMap<PackageId, Package>();
	}
	
	public Package get(PackageId key)
	{
		return packageCache.get(key);
	}
	
	public void put(PackageId key, Package value)
	{
		packageCache.put(key, value);
	}

	public Package remove(PackageId key)
	{
		return packageCache.remove(key);
	}

	public void clear()
	{
		packageCache.clear();
	}

	public void fill(PackageCache cacheToFill)
	{
		for (Package pkg: packageCache.values())
		{
			if (pkg != null)
				cacheToFill.put(pkg.getPackageId(), pkg);
		}
	}
}

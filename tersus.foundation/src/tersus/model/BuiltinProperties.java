/************************************************************************************************
 * Copyright (c) 2003-2021 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;

/**
 * 
 */
public interface BuiltinProperties
{
	static final String APPLICATION_TITLE = "html.applicationTitle";

	static final String JAVASCRIPT_CONSTRUCTOR = "javascript.constructor"; //legacy - now using plugin descriptions

	static final String REQUIRED_PERMISSION = "requiredPermission";

	static final String OPERATION = "operation";

	static final String EMPTY = "empty";

	static final String FULL_TEXT_INDEX = "fullTextIndex";

	static final String MAX_LENGTH = "maxLength";

    static final String COMMENT = "comment";

	static final String COLUMN_NAME = "columnName";
	
	static final String COLUMN_TYPE = "columnType";
	
	static final String COLUMN_SIZE = "columnSize";

	static final String SORT_COLUMN_NAME = "html.sortColumnName";
	
	static final String DECIGMAL_DIGITS = "decimalDigits";

	static final String ICON_FOLDER = "iconFolder";

	static final String DOCUMENTATION = "documentation";

	static final String CONSTANT = "constant";

	static final String VALUE = "value";

	static final String PRIMARY_KEY = "primaryKey";

	static final String DATABASE_GENERATED = "databaseGenerated";

	static final String TABLE_NAME = "tableName";

	static final String TABLE_TYPE = "tableType";

	static final String DATA_TYPE_ID = "dataTypeId";

	static final String PLUGIN = "plugin";

	static final String MANDATORY = "mandatory";

    static final String ALWAYS_CREATE = "alwaysCreate";

	static final String MODIFIED = "modified";

	static final String ELEMENTS = "elements"; // A pseudo property that represents the list of elements of a model

	static final String WIDTH = "width";

	static final String REPETITIVE = "repetitive";

	static final String SIZE = "size";

	static final String POSITION = "position";

	static final String NAME = "name";

	static final String ID = "id";

	static final String ROLE="role";
	
	static final String REF_ID="refId";

	static final String TYPE="type";
	
	static final String COMPOSITION="composition";
	
	static final String SOURCE="source";	

	static final String TARGET="target";

	static final String EXCLUDE_FROM_FIELD_NAME = "excludeFromFieldName";

    static final String VALID = "valid";

    static final String CREATE_SUB_PACKAGE = "createSubPackage";

    static final String FORMAT = "format";

    static final String TEXT_DIRECTION = "html.textDirection";

    static final String SERVICE_TIMEOUT = "serviceTimeout";

    static final String TEMPLATE = "template";
    
    static final String CUSTOM_NAVIGATION = "html.customNavigation";

    static final String COMPACT_NAVIGATION = "html.compactNavigationTabs";

    static final String URL_NAVIGATION = "html.urlNavigation";

    static final String THEME = "html.theme";

    static final String PROTOTYPE = "prototype";

    static final String PROTOTYPE_ELEMENT = "prototype.element";

    static final String MULTIPLICITY = "prototype.multiplicity";

    public static final String PLUGIN_VERSION = "pluginVersion";

	public static final String XML_NAME_SPACE = "xml.namespace";
    
	public static final String XML_USE_CDATA = "xml.useCDATA";
    
    public static final String DATA_SOURCE="dataSource";

	static final String MODULARITY = "modularity";

    static final String PROGRESS_MESSAGE = "progressMessage";

    static final String CANCELLABLE = "cancellable";

    static final String JAVASCRIPT_CODE = "javascript.code"; // legacy - now using plugin descriptors

    static final String OPTIMIZED_FOR_MOBILE = "html.optimizedForMobile";
    
    static final String PROPERTIES = "properties";
    
    static final String DATA_TYPE_SETTING = "dataTypeSetting";
    
    static final String RUN_IMMEDIATELY = "runImmediately";

    static final String EVENT_HANDLING = "eventHandling";

    static final String FORMAT_USE_GMT = "format.useGMT";

    static final String VIEW_DEFINITION = "viewDefinition";

    static final String NULLABLE = "nullable";
    
    static final String VALID_VALUES = "validValues";

    static final String DEFAULT_VALUE = "defaultValue";
    
    static final String FILTERING = "filtering";

    static final String SCHEMA = "schema";
    
    static final String CATALOG = "catalog";
    
    static final String CLIENT_DATABASE = "clientSideDatabase";

	static final String AUTHENTICATION_METHOD = "authenticationMethod";

	static final String USE_STANDARDS_MODE = "useStandardsMode";
	
	static final String IE_LEGACY_DOCUMENT_MODE = "ieLegacyDocumentMode";

	static final String CACHED = "cached";
	
	static final String HTML_STYLE_CLASS = "html.styleClass";

	static final String HTML_STYLE = "html.style";

	static final String HTML_TAG = "html.tag";
	
	static final String CHECK_REQUEST_SIGNATURE = "checkRequestSignatures";
	
	static final String HTML_WRAPPERTAGS = "html.wrapperTags";
	
	static final String HTML_WRAPPERTAG = "html.wrapperTag";
	
	static final String IGNORED_PROBLEMS = "ignoredProblems";

	static final String APPLICATION_FRAMEWORK = "applicationFramework";

	static final String OLD_CSS_EXCLUDE = "oldCssExclude";
	
	/** On a Callable Service, indicates how long responses may be cached */

	static final String MAX_AGE = "maxAge";

	static final String CACHE_MAX_AGE_SECONDS = "cacheMaxAgeSeconds";

	static final String[] KEY_PROPERTIES = {ID,
	                                          NAME,
	                                          ROLE,
	                                          REQUIRED_PERMISSION,
	                                          DEFAULT_VALUE,
	                                          MANDATORY,
	                                          REPETITIVE,
	                                          PRIMARY_KEY,
	                                          APPLICATION_TITLE,
	                                          HTML_STYLE,
	                                          HTML_STYLE_CLASS,
	                                          HTML_TAG,
	                                          SOURCE,
	                                          TARGET,
	                                          VALUE,
	                                          PLUGIN};
}
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;

import tersus.util.Enum;

/**
 * An enumeration of types of flow data elements.
 *
 * @author Youval Bronicki
 *
 */
public class FlowDataElementType extends Enum
{

	/** A Main data element of a flow */
	public static FlowDataElementType MAIN = new FlowDataElementType("Main");
	/** A reference to a data element of the parent flow*/ 
	public static FlowDataElementType PARENT = new FlowDataElementType("Parent");
	/** A data element that holds intermediate data (not accessible to children) */ 
	public static FlowDataElementType INTERMEDIATE = new FlowDataElementType("Intermediate");
	
	public static FlowDataElementType[] values = {MAIN,PARENT,INTERMEDIATE};
	/**
	 * Creates a FlowDataElmentTyope
	 * @param value the String representation of the type
	 */
	private FlowDataElementType(String value)
	{
		super(value);
	}

	/* (non-Javadoc)
	 * @see tersus.util.Enum#getValues()
	 */
	protected Enum[] getValues()
	{
		return values;
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;

import java.io.Serializable;

/**
 * 
 */
public class RelativeSize implements Serializable
{
	static final long serialVersionUID = 1;
	private double width;
	private double height;
	public RelativeSize(double width, double height)
	{
		setWidth(width);
		setHeight(height);
	}
	public boolean equals(Object obj)
	{
		if (obj instanceof RelativePosition)
			return equals((RelativePosition)obj);
		else
			return false;
	}

	public boolean equals(RelativeSize other)
	{
		return getWidth() == other.getWidth() && getHeight()==other.getHeight();
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		return getWidth()+","+getHeight();
	}

	public static RelativeSize valueOf(String serialization)
	{
		RelativeSize size = new RelativeSize(0,0);

		if (!"".equals(serialization))
		{
		    int commaPlace = serialization.indexOf(",");
	        size.setWidth(Double.parseDouble(serialization.substring(0,commaPlace)));
	        size.setHeight(Double.parseDouble(serialization.substring(commaPlace+1)));
		}
		
		
		return (size);
	}

	public RelativeSize getCopy()
	{
		RelativeSize size = new RelativeSize(getWidth(), getHeight());
		return size;
	}
	/**
	 * @param size
	 */
	public void copy(RelativeSize size)
	{
		setWidth(size.getWidth());
		setHeight(size.getHeight());
		
	}

	public void setHeight(double height)
	{
		this.height = Precision.round(height);
	}

	public double getHeight()
	{
		return height;
	}

	public void setWidth(double width)
	{
		this.width = Precision.round(width);
	}

	public double getWidth()
	{
		return width;
	}

}

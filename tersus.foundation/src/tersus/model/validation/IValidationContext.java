/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.model.validation;

import java.util.HashMap;
import java.util.HashSet;

import tersus.model.ElementPath;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.indexer.RepositoryIndex;

/**
 * @author Youval Bronicki
 */
public interface IValidationContext
{
	public static final int NO_CONTEXT = 0;
	
	public static final int SYSTEM_CONTEXT = 1;

	public static final int SERVER_CONTEXT = 2;

	public static final int CLIENT_CONTEXT = 4;
	
	public static final int UNUSED_CONTEXT = 8;
	
    void addProblem(String problemType, String details, ElementPath path);
	
	public HashMap<String, HashSet<String>> getHtmlTagHierarchy();
	
	public int getModelInnerContext(Model model);
	
	public boolean isCancelled();
	
	public RepositoryIndex getRepositoryIndex();
	
	public Model getRefModel(ModelElement e);
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model.validation;

import java.util.HashSet;

/**
 * @author Youval Bronicki
 */
public class Problems
{
	// Warnings
	public static String INVALID_CONTEXT = "Invalid Context";
	public static String NO_PLUGIN_SET = "No Plugin Set";
	public static String INVALID_ATTRIBUTE = "Invalid Attribute";
	public static String INVALID_PARENT_MODEL = "Invalid Parent Model";
	public static String INVALID_REFERENCE = "Invalid Reference";
	public static String MISSING_FLOW = "Missing Flow";
	public static String MISSING_ELEMENT = "Missing Element";
	public static String INVALID_TAG = "Invalid Tag";
	public static String MUTUALLY_EXCLUSIVE_PROPERTIES = "Mutually Exclusive Properties";
	public static String DEPRECATED_PLUGIN = "Deprecated Plugin";
	public static String INCONSISTENT_FLOW = "Inconsistent Flow";
	public static String UNSUPPORTED_FLOW = "Unsupported Flow";
	public static String SUSPECT_SLOT_NAME = "Suspect Slot Name";
	public static String INVALID_PLUGIN = "Invalid Plugin";
	public static String UNKNOWN_TYPE = "Unknown Type";

	// Errors
	public static String INVALID_CONSTANT = "Invalid Constant";
	public static String INVALID_DATA_ELEMENT = "Invalid Data Element";
	public static String INVALID_FLOW = "Invalid Flow";
	public static String INVALID_CHILD = "Invalid Child";
	public static String INVALID_TRIGGER_DATA_TYPE = "Invalid Trigger Type";
	public static String INVALID_EXIT_DATA_TYPE = "Invalid Exit Type";
	public static String INVALID_EXIT = "Invalid Exit";
	public static String INCOMPATIBLE_TYPE = "The link's source and target data types are not compatible";
	public static String INVALID_TRIGGER = "Invalid Trigger";
	public static String INVALID_TRIGGERS = "Invalid Triggers";
	public static String MISSING_TRIGGER = "Missing Trigger";
	public static String INVALID_SUB_PROCESS = "Invalid Sub-process"; // Indicates that something is wrong about a sub-process
	public static String MISSING_SUB_PROCESS = "Missing Sub-process"; // Indicates that an expected sub-process is missing
	public static String TOO_MANY_SUB_PROCESSES = "Too Many Sub-process"; // Indicates that there are un-expected sub-processes
	public static String INVALID_MODEL_TYPE = "Invalid Model Type";
	public static String INVALID_NUMBER_OF_ELEMENTS = "Invalid Number of Elements";
	public static String INCONSISTENT_TYPES = "Inconsistent Types";
	public static String MISSING_PROPERTY = "Missing Property";
	public static String MISSING_MODEL = "Missing Model";
	
	private static HashSet<String> warnings;
	
	public static boolean isWarning(String problem)
	{
		if (warnings == null)
			initWarnings();
		
		return warnings.contains(problem);
	}
	
	private static void initWarnings()
	{
		warnings = new HashSet<String>();
		
		warnings.add(INVALID_CONTEXT);
		warnings.add(NO_PLUGIN_SET);
		warnings.add(INVALID_ATTRIBUTE);
		warnings.add(INVALID_PARENT_MODEL);
		warnings.add(INVALID_REFERENCE);
		warnings.add(MISSING_FLOW);
		warnings.add(MISSING_ELEMENT);
		warnings.add(INVALID_TAG);
		warnings.add(MUTUALLY_EXCLUSIVE_PROPERTIES);
		warnings.add(DEPRECATED_PLUGIN);
		warnings.add(INCONSISTENT_FLOW);
		warnings.add(UNSUPPORTED_FLOW);
		warnings.add(SUSPECT_SLOT_NAME);
		warnings.add(INVALID_PLUGIN);
	}
}

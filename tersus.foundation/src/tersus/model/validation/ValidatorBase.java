/************************************************************************************************
 * Copyright (c) 2003-2011 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model.validation;

import tersus.model.BuiltinElements;
import tersus.model.BuiltinModels;
import tersus.model.BuiltinPlugins;
import tersus.model.BuiltinProperties;
import tersus.model.Conventions;
import tersus.model.DataElement;
import tersus.model.DataType;
import tersus.model.FlowDataElementType;
import tersus.model.FlowModel;
import tersus.model.FlowType;
import tersus.model.InvalidModelException;
import tersus.model.Link;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.ModelUtils;
import tersus.model.Note;
import tersus.model.Path;
import tersus.model.PluginDescriptor;
import tersus.model.Slot;
import tersus.model.SlotType;
import tersus.model.SubFlow;
import tersus.model.TemplateAndPrototypeSupport;
import tersus.model.indexer.ElementEntry;
import tersus.model.indexer.RepositoryIndex;
import tersus.util.EscapeUtil;
import tersus.util.Misc;
import tersus.util.Multiplicity;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Youval Bronicki
 * @author Liat Shiff
 */
public class ValidatorBase implements IValidator
{
	public static ValidatorBase INSTANCE = new ValidatorBase();

	public static Set<String> HTML5Tags;
	static
	{
		HTML5Tags = new HashSet<String>();
		for (String tag : "article,aside,audio,bdi,canvas,command,data,datalist,details,embed,figcaption,figure,footer,header,keygen,mark,meter,nav,output,progress,rp,rt,ruby,section,source,summary,time,track,video,wbr".split(","))
			HTML5Tags.add(tag);
	}

	private File projectRoot;

	public File getProjectRoot()
	{
		return projectRoot;
	}

	public void setProjectRoot(File projectRoot)
	{
		this.projectRoot = projectRoot;
	}

	public void validate(IValidationContext context, Model model)
	{
		if (context.isCancelled())
			return;

		validateElements(context, model);
		validateForPrototype(context, model);
		validatePlugin(context, model);

		if (ModelUtils.isDisplayModel(model) || ModelUtils.isSystemModel(model))
			validateTagHierarchy(context, model);

	}

	protected void validateElements(IValidationContext context, Model model)
	{
		int modelInnerContext = context.getModelInnerContext(model);
		ArrayList<Link> linksElements = getLinksElements(model);

		HashSet<Path> linkTargets = new HashSet<Path>();
		for (Link l : getLinksElements(model))
		{
			Path target = l.getTarget();
			if (target.getNumberOfSegments() == 2) // We're only interested in links whose target is
													// a trigger of a sub-flow
				linkTargets.add(l.getTarget());
		}
		for (ModelElement element : model.getElements())
		{
			if (element instanceof Link)
				validateLink(context, (Link) element);
			else if (element instanceof Slot)
				validateSlot(context, (Slot) element);
			else if (!(element instanceof Note) && validateRefModel(context, element))
			{
				if (element instanceof SubFlow)
				{
					if (model instanceof FlowModel)
						validateElementContext(context, element, modelInnerContext);

					validateMissingFlow(context, model, element, linkTargets);
				}
				if (element instanceof DataElement)
				{
					if (element.getReferredModel() instanceof DataType)
					{
						DataType dataType = (DataType) element.getReferredModel();

						if (dataType.isConstant() && dataType.checkPluginVersion(1))
						{
							String valueStr = dataType.getValue();
							try
							{
								EscapeUtil.unescape(valueStr);
							}
							catch (IllegalArgumentException e)
							{
								context.addProblem(Problems.INVALID_CONSTANT, e.getMessage()
										+ " in '" + valueStr + "'", element.getRole());
							}
						}
					}
					if (FlowDataElementType.PARENT.equals(((DataElement) element).getType()))
					{
						// Parent references at the module level are assumed to be OK - we can't
						// check the context
						if (!model.isModule())
						{
							validateParentReference(context, model, modelInnerContext, element,
									new HashSet<Model>());
						}
					}

					if (model instanceof FlowModel)
						validateDataElementInit(context, (DataElement) element, (FlowModel) model,
								linksElements);
				}
			}
		}
	}

	private ArrayList<Link> getLinksElements(Model model)
	{
		ArrayList<Link> links = new ArrayList<Link>();

		for (ModelElement element : model.getElements())
		{
			if (element instanceof Link)
			{
				links.add((Link) element);
			}
		}
		return links;
	}

	protected boolean validateRefModel(IValidationContext context, ModelElement element)
	{
		Model refModel = context.getRefModel(element);

		if (refModel == null)
		{
			context.addProblem("Missing Model", "Cannot find element's model",
					new Path(element.getRole()));
			return false;
		}
		return true;
	}

	protected void validateDataElementInit(IValidationContext context, DataElement element,
			FlowModel model, ArrayList<Link> links)
	{
		RepositoryIndex index = context.getRepositoryIndex();

		if (FlowDataElementType.PARENT.equals(element.getType()))
		{
			boolean incomingLinkExists = false;
			for (Link link : links)
			{
				if (index.pathContainsElement(model.getModelId(), link.getTarget(), element, true))
				{
					incomingLinkExists = true;
					break;
				}
			}

			if (incomingLinkExists)
				context.addProblem(Problems.UNSUPPORTED_FLOW,
						"Parent reference's root element cannot be updated",
						new Path(element.getRole()));
		}
		else if (!ModelUtils.isConstant(element)
				&& (FlowType.ACTION.equals(model.getType())
						|| FlowType.SERVICE.equals(model.getType()) || FlowType.OPERATION
							.equals(model.getType())))
		{
			boolean incomingLinkExists = false;
			for (Link link : links)
			{
				if (index.pathContainsElement(model.getModelId(), link.getTarget(), element, false))
				{
					incomingLinkExists = true;
					break;
				}
			}

			if (!incomingLinkExists)
				context.addProblem(Problems.MISSING_FLOW,
						"Missing flow into a data element (element will not be created)", new Path(
								element.getRole()));
		}

	}

	protected boolean validateParentReference(IValidationContext context, Model model,
			int modelInnerContext, ModelElement child, HashSet<Model> checkedModels)
	{
		checkedModels.add(model);
		RepositoryIndex index = context.getRepositoryIndex();

		if (IValidationContext.UNUSED_CONTEXT == modelInnerContext)
			return false;
		for (ElementEntry refEntry : index.getReferenceEntries(model.getModelId()))
		{
			if (context.getRepositoryIndex().getModelElement(refEntry) instanceof SubFlow)
			{
				Model parentModel = model.getRepository().getModel(refEntry.getParentId(), true);

				int parentInnerContext = context.getModelInnerContext(parentModel);

				if ((IValidationContext.SYSTEM_CONTEXT & parentInnerContext) != 0) 
				{
					// Once we reach a System, we can't find the reference
					// Note that we don't expect a model to have SYSTEM context in addition to any other context
					context.addProblem(Problems.INVALID_REFERENCE,
							"A matching element was not found in the containing context",
							child.getRole());
					return true;
				}
				if ((IValidationContext.CLIENT_CONTEXT & parentInnerContext) !=0 && (model.getPluginDescriptor().isServiceClient()))
				{
					// Can't go up the hierarchy when switching from server context to client
					// context.  We identify switching by the fact that the parent model appears in a client context and the model is a "Service Client" (i.e Service, ShowFile)
					context.addProblem(Problems.INVALID_REFERENCE,
							"A matching element was not found in the containing context",
							child.getRole());
					return true;
				}
				// Looking for "regular reference"
				ModelElement referredElement = parentModel.getElement(child.getRole());
				if (referredElement != null
						&& Misc.equal(referredElement.getRefId(), child.getRefId()))
					continue;

				// Looking for reference to the display model 
				// Note that ModelUtils.isDisplayModel(parentModel) implies parentInnerContxt == DISPLAY, but we don't care
				if (ModelUtils.isDisplayModel(parentModel)
						&& parentModel.getId().equals(child.getRefId()))
					continue;


				if (!checkedModels.contains(parentModel)
						&& validateParentReference(context, parentModel, parentInnerContext, child,
								checkedModels))
					return true;

			}
		}

		return false;
	}

	protected void validateElementContext(IValidationContext context, ModelElement element,
			int modelInnerContext)
	{
		Model childRef = context.getRefModel(element);

		if (childRef != null)
		{
			if (!((IValidationContext.UNUSED_CONTEXT & modelInnerContext) == 0))
				return;

			if (ModelUtils.isSystemModel(childRef)
					&& ((IValidationContext.SYSTEM_CONTEXT & modelInnerContext) == 0))
			{
				context.addProblem(Problems.INVALID_CONTEXT, getCharacterizationString(childRef)
						+ " is only allowed in a System context (the current context is "
						+ getContextString(modelInnerContext) + ").", element.getRole());
			}
			else
			{
				PluginDescriptor descriptor = childRef.getPluginDescriptor();

				if (descriptor != null)
				{
					if (descriptor.isClientTierOnly())
					{
						if (!((modelInnerContext & IValidationContext.SERVER_CONTEXT) == 0))
							context.addProblem(Problems.INVALID_CONTEXT,
									getCharacterizationString(childRef)
											+ " is not allowed in a Server context",
									element.getRole());
						if (!ModelUtils.isDisplayElement(element)
								&& !((modelInnerContext & IValidationContext.SYSTEM_CONTEXT) == 0))
							context.addProblem(Problems.INVALID_CONTEXT,
									getCharacterizationString(childRef)
											+ " is not allowed in a System context",
									element.getRole());
					}
					else if (descriptor.isServerTierOnly()
							&& !((modelInnerContext & IValidationContext.CLIENT_CONTEXT) == 0))
						context.addProblem(
								Problems.INVALID_CONTEXT,
								getCharacterizationString(childRef)
										+ " is a server-side plugin and should not be used in a client-side context",
								element.getRole());
				}
			}
		}
	}

	protected void validateMissingFlow(IValidationContext context, Model parent,
			ModelElement element, HashSet<Path> linkTargets)
	{
		Model refModel = context.getRefModel(element);
		PluginDescriptor descriptor = refModel.getPluginDescriptor();

		if (descriptor != null && descriptor.isAutomaticallyInvoked()
				&& !Conventions.isEventHandler(element))
		{
			Path p = new Path();
			p.addSegment(element.getRole());
			for (ModelElement e : refModel.getElements())
			{

				if (e instanceof Slot && SlotType.TRIGGER.equals(((Slot) e).getType())
						&& ((Slot) e).isMandatory())
				{
					p.addSegment(e.getRole());
					if (!linkTargets.contains(p))
					{
						context.addProblem(Problems.MISSING_FLOW,
								Messages.MISSING_FLOW_INTO_MANDATORY_TRIGGER, p);
					}
					p.removeLastSegment();
				}
			}
		}
	}

	private Object getCharacterizationString(Model model)
	{
		if (model.getPluginDescriptor() != null)
			return model.getPluginDescriptor().getName();
		else
			return Misc.getShortName(model.getClass()) + "[type="
					+ model.getProperty(BuiltinProperties.TYPE) + ";plugin=" + model.getPlugin()
					+ "]";

	}

	private String getContextString(int context)
	{
		switch (context)
		{
			case IValidationContext.SYSTEM_CONTEXT:
				return "System";
			case IValidationContext.SERVER_CONTEXT:
				return "Server";
			case IValidationContext.CLIENT_CONTEXT:
				return "Client";
			case 6:
				return "Client-Server";
		}

		return "";
	}

	protected void validateLink(IValidationContext context, Link link)
	{
		try
		{
			link.validate(true);
		}
		catch (InvalidModelException e)
		{
			String problem = e.getProblem();
			if (Problems.INCONSISTENT_FLOW.equals(problem))
			{
				context.addProblem(e.getProblem(), e.getDetails(), e.getPath());
			}
			else
				context.addProblem(e.getProblem(), e.getDetails(), link.getRole());

		}
	}

	protected void validateSlot(IValidationContext context, Slot slot)
	{
		Model refModel = slot.getReferredModel();
		if (refModel instanceof DataType && ((DataType) refModel).isConstant())
		{
			if (SlotType.TRIGGER == slot.getProperty(BuiltinProperties.TYPE))
				context.addProblem(Problems.INVALID_TRIGGER_DATA_TYPE,
						Messages.TRIGGER_DATA_TYPE_CAN_NOT_BE_A_CONSTANT, slot.getRole());
			else if (SlotType.EXIT == slot.getProperty(BuiltinProperties.TYPE))
				context.addProblem(Problems.INVALID_EXIT_DATA_TYPE,
						Messages.EXIT_DATA_TYPE_CAN_NOT_BE_A_CONSTANT, slot.getRole());
		}

		String slotElementName = slot.getElementName();
		if (slotElementName.endsWith(">") && !slotElementName.startsWith("<"))
			context.addProblem(
					Problems.SUSPECT_SLOT_NAME,
					"The slot name is suspect. Distinguished slot names should start with '<' and end with '>'.",
					slot.getRole());

		if (BuiltinElements.DONE == slot.getRole())
		{
			if (slot.getType() != SlotType.EXIT)
				context.addProblem(Problems.INVALID_CHILD, BuiltinElements.DONE
						+ " must be an exit", slot.getRole());
			if (!BuiltinModels.NOTHING_ID.equals(slot.getRefId()))
				context.addProblem(Problems.INVALID_EXIT, BuiltinElements.DONE + " must be of type"
						+ BuiltinModels.NOTHING_ID, slot.getRole());
		}

		if (slot.getDataTypeId() == null && slot.getType().equals(SlotType.EXIT))
			context.addProblem(Problems.INVALID_EXIT, "The exit has no datatype defined",
					slot.getRole());
		else if (SlotType.TRIGGER.equals(slot.getType()) && slot.getDataTypeId() == null)
			context.addProblem(Problems.INVALID_TRIGGER, "The trigger has no datatype defined",
					slot.getRole());
	}

	protected void validateForPrototype(IValidationContext context, Model model)
	{
		Object prototypeIdStr = model.getProperty(BuiltinProperties.PROTOTYPE);
		if (prototypeIdStr == null)
			return;
		Model prototype = null;
		try
		{
			ModelId prototypeId = new ModelId((String) prototypeIdStr);
			prototype = model.getRepository().getModel(prototypeId, true);
		}
		catch (Exception e)
		{
			context.addProblem(Problems.INVALID_ATTRIBUTE, "'" + prototypeIdStr
					+ "' is not a valid prototype - " + e.getMessage() + ")", Path.EMPTY);
			return;
		}

		if (prototype == null)
			return;
		List<ModelElement> prototypeElements = prototype.getElements();

		for (ModelElement pElement : prototypeElements)
		{
			if (pElement instanceof Link || pElement instanceof Note
					&& pElement.getProperty(BuiltinProperties.MULTIPLICITY) == null)
				continue;
			int nInstances = TemplateAndPrototypeSupport.getInstanceElements(model, pElement)
					.size();

			Multiplicity m = TemplateAndPrototypeSupport.getMultiplicity(pElement);
			if (!m.validate(nInstances))
			{
				if (nInstances == 0)
					context.addProblem(Problems.MISSING_ELEMENT,
							"No instances of '" + pElement.getRole()
									+ "' (which appears in the prototype with multiplicity \"" + m
									+ "\"", Path.EMPTY);
				else
					context.addProblem(Problems.INVALID_NUMBER_OF_ELEMENTS,
							"The number of elements of '" + pElement.getRole()
									+ "' doesn't match the multiplicity (\"" + m + "\"", Path.EMPTY);
			}
		}
	}

	protected void validatePlugin(IValidationContext context, Model model)
	{
		PluginDescriptor descriptor = model.getPluginDescriptor();

		if (descriptor == null)
			context.addProblem(Problems.INVALID_PLUGIN,
					"The plugin '" + model.getProperty(BuiltinProperties.PLUGIN)
							+ "' is not defined.", Path.EMPTY);
		else if (descriptor.isDeprecated())
			context.addProblem(Problems.DEPRECATED_PLUGIN, "The plugin " + descriptor.getName()
					+ " is deprecated.", Path.EMPTY);
	}

	protected void validateTagHierarchy(IValidationContext context, Model model)
	{
		for (ModelElement element : model.getElements())
		{
			if (ModelUtils.isDisplayElement(element))
			{
				Model refModel = context.getRefModel(element);
				PluginDescriptor descriptor = refModel.getPluginDescriptor();
				if (descriptor != null && needToPerformInnerTagHierarchyValidation(descriptor))
				{
					String elementTagHierarchy = getElementTagHierarchy(context, element, refModel,
							true);

					if (elementTagHierarchy != null
							&& validateTagName(context, elementTagHierarchy, element))
					{
						for (ModelElement elementChild : refModel.getElements())
						{
							Model childRefModel = context.getRefModel(elementChild);
							if (childRefModel != null)
							{
								PluginDescriptor childDescriptor = childRefModel
										.getPluginDescriptor();
								if (ModelUtils.isDisplayElement(elementChild)
										&& childDescriptor != null
										&& !childDescriptor.getName().equals(BuiltinPlugins.ROW)
										&& !childDescriptor.getName().equals(
												BuiltinPlugins.MENU_BAR))
								{
									String elementChildTagHierarchy = getElementTagHierarchy(
											context, elementChild, childRefModel, false);

									if (elementChildTagHierarchy != null)
									{
										String fullPathHierarchy = elementTagHierarchy + ","
												+ elementChildTagHierarchy;

										validateTagHierarchy(context, fullPathHierarchy, element,
												elementChild);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private boolean needToPerformInnerTagHierarchyValidation(PluginDescriptor descriptor)
	{
		return !(descriptor.getName().equals(BuiltinPlugins.ROW)
				&& !descriptor.getName().equals(BuiltinPlugins.MENU_BAR)
				&& !descriptor.getName().equals(BuiltinPlugins.TABLE)
				&& !descriptor.getName().equals(BuiltinPlugins.GRID)
				&& !descriptor.getName().equals(BuiltinPlugins.SIMPLE_TABLE) && !descriptor
				.getName().equals(BuiltinPlugins.HTML_EDITOR));
	}

	/**
	 * Validate If the input tag is valid HTML Tag.
	 * 
	 * @return true if the tag name is valid otherwise returns false/
	 */
	private boolean validateTagName(IValidationContext context, String elementTagHierarchy,
			ModelElement element)
	{
		String[] tags = elementTagHierarchy.split(",");

		String lastTag = tags[tags.length - 1].trim();

		if (HTML5Tags.contains(lastTag))
			return true;

		HashSet<String> subTagList = context.getHtmlTagHierarchy().get(lastTag.toLowerCase());

		if (subTagList == null)// Means the tag does not exists in the html dtd
		{
			context.addProblem(Problems.INVALID_TAG, "<" + lastTag + "> is not a valid HTML tag name",
					new Path(element.getRole()));

			return false;
		}

		return true;
	}

	private void validateTagHierarchy(IValidationContext context, String fullPathHierarchy,
			ModelElement elementParent, ModelElement elementChild)
	{
		String[] tags = fullPathHierarchy.split(",");

		for (int i = 0; i < tags.length - 1; i++)
		{
			String tag = tags[i].trim().toLowerCase();
			String nextTag = tags[i + 1].trim().toLowerCase();
			if (HTML5Tags.contains(tag) || HTML5Tags.contains(nextTag))
			{
				continue;
			}
			HashSet<String> subTagList = context.getHtmlTagHierarchy().get(tag);

			if (subTagList == null)// Means the tag does not exists in the html dtd
			{
				context.addProblem(Problems.INVALID_TAG, "<" + tag + "> is not a valid HTML tag name",
						new Path(elementParent.getRole() + "/" + elementChild.getRole()));
			}
			else if (!subTagList.contains(nextTag))
				context.addProblem(Problems.INVALID_TAG, "<" + tag + "> cannot contain <" + nextTag
						+ ">", new Path(elementParent.getRole() + "/" + elementChild.getRole()));
		}
	}

	private String getElementTagHierarchy(IValidationContext context, ModelElement element,
			Model refModel, boolean htmlTagMissingValue)
	{
		String elementTag = getElementTag(context, element, refModel, htmlTagMissingValue);

		if (elementTag == null)
			return null; // No need to do the Tag Hierarchy Validation

		String wrapperTags = getElementWrapperTags(context, element, refModel);

		return wrapperTags != null && wrapperTags.trim().length() > 0 ? wrapperTags + ","
				+ elementTag : elementTag;
	}

	private String getElementTag(IValidationContext context, ModelElement element, Model refModel,
			boolean htmlTagMissingValue)
	{
		String localParoperty = (String) element.getProperty(BuiltinProperties.HTML_TAG);

		if (localParoperty != null && localParoperty.trim().length() > 0)
			return localParoperty;

		String shardParoperty = (String) refModel.getProperty(BuiltinProperties.HTML_TAG);
		if (shardParoperty != null && shardParoperty.trim().length() > 0)
			return shardParoperty;

		PluginDescriptor descriptor = refModel.getPluginDescriptor();
		// Todo use a specific validator class for HTML Tag
		String javascript = descriptor.getJavascript();
		if (javascript != null && javascript.matches("tersus.HTMLTag|tersus\\.UI.*") && htmlTagMissingValue)
			context.addProblem(Problems.MISSING_PROPERTY, "html.tag not specified", new Path(
					element.getRole()));

		return descriptor.getDefaultTag();
	}

	private String getElementWrapperTags(IValidationContext context, ModelElement element,
			Model refModel)
	{
		String localWrapperTagsParoperty = (String) element
				.getProperty(BuiltinProperties.HTML_WRAPPERTAGS);
		String localWrapperTagParoperty = (String) element
				.getProperty(BuiltinProperties.HTML_WRAPPERTAG);

		if (isValidPropertyVal(localWrapperTagsParoperty))
		{
			if (isValidPropertyVal(localWrapperTagParoperty))
				context.addProblem(
						Problems.MUTUALLY_EXCLUSIVE_PROPERTIES,
						"The properties html.wrapperTag and html.wrapperTags can not contain both values at Local scope ",
						new Path(element.getRole()));

			return localWrapperTagsParoperty;
		}
		else if (isValidPropertyVal(localWrapperTagParoperty))
			return localWrapperTagsParoperty;

		String sharedWrapperTagsParoperty = (String) refModel
				.getProperty(BuiltinProperties.HTML_WRAPPERTAGS);
		String sharedWrapperTagParoperty = (String) refModel
				.getProperty(BuiltinProperties.HTML_WRAPPERTAG);

		if (isValidPropertyVal(sharedWrapperTagsParoperty))
		{
			if (isValidPropertyVal(sharedWrapperTagParoperty))
				context.addProblem(
						Problems.MUTUALLY_EXCLUSIVE_PROPERTIES,
						"The properties html.wrapperTag and html.wrapperTags can not contain both values at Shared scope ",
						new Path(element.getRole()));

			return sharedWrapperTagsParoperty;
		}
		else if (isValidPropertyVal(sharedWrapperTagParoperty))
			return sharedWrapperTagParoperty;

		return null;
	}

	private boolean isValidPropertyVal(String propertyVal)
	{
		return propertyVal != null && propertyVal.trim().length() > 0;
	}
}
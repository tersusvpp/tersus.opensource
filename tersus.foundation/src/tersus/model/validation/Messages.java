/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.model.validation;

/**
 * @author Youval Bronicki
 *
 */
public interface Messages
{

    public static final String FLOW_INTO_A_CHILD_OF_A_REPETITIVE_ELEMENT = "Flow into a child of a repetitive element";
    public static final String THE_SOURCE_IS_NOT_VALID = "The source is not valid";
    public static final String AN_EXIT_OR_ERROR_EXIT_OF_THE_PROCESS_IS_NOT_VALID_AS_A_LINK_SOURCE = "An exit or error exit of the process is not valid as a link source";
    public static final String A_TRIGGER_OR_INPUT_SLOT_OF_A_SUB_PROCESS_IS_NOT_VALID_AS_A_LINK_SOURCE = "A trigger or input slot of a sub-process is not valid as a link source";
    public static final String AN_TRIGGER_OR_INPUT_SLOT_OF_A_PROCESS_IS_NOT_VALID_AS_LINK_TARGET = "An trigger or input slot of a process is not valid as link target";
    public static final String AN_EXIT_OR_ERROR_EXIT_OF_A_CHILD_IS_NOT_VALID_AS_A_LINK_TARGET = "An exit or error exit of a child is not valid as a link target";
    public static final String FLOW_TARGET_IS_NOT_VALID = "The target is not valid";
    public static final String REPETITIVE_SUBPROCESS_WITH_REPETITIVE_TRIGGERS = "Repetitive sub-processes with repetitive triggers are not supported";
    public static final String MISSING_FLOW_INTO_MANDATORY_TRIGGER = "Missing flow into a mandatory trigger (sub-process can't start)";
    public static final String MISSING_DATA_TYPE = "Missing data type";
	public static final String FLOW_FROM_REPETITIVE_CONTEXT_TO_NON_REPETITIVE_ELEMENT = "Flow from repetitive context to non-repetitive element";
	public static final String TRIGGER_DATA_TYPE_CAN_NOT_BE_A_CONSTANT = "Trigger data type can not be a constant";
	public static final String EXIT_DATA_TYPE_CAN_NOT_BE_A_CONSTANT = "Exit data type can not be a constant";
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import tersus.util.PropertyChangeListener;

/**
 * A Decorator that adds 'DisplayModel' behavior to a flow model.
 * 
 * 
 * 
 * @author Youval Bronicki
 *
 */
public class DisplayModelWrapper extends FlowModel
{
	public static final Role VALUE_RESERVED_ROLE = Role.get("<Value>");
	public static final Role VALUE_RESERVED_ROLE_DATE_AND_TIME = Role.get("Date and Time"); // Special role for 'Date and Time Input Field' only
	public static final Role VALUE_RESERVED_ROLE_CHECKBOX = Role.get("<Checked>"); // Special role for the NEW 'Check Box' only (under UI)
	public static final Role READ_ONLY_RESERVED_ROLE = Role.get("<Read Only>");
	public static final Role VISIBLE_RESERVED_ROLE = Role.get("<Visible>");

	private FlowModel model;
	/*
	* It would be more elegant if DisplayModels were created different from flow models,
	* but implementing that would require a lot of effort.  This is probably a worthy refactoring
	 */
	public static DisplayModelWrapper wrap(FlowModel model)
	{
		if (model.getType() != FlowType.DISPLAY)
		{
			throw new ModelException(
				"Can't create DisplayModelWrapper for "
					+ model.getId()
					+ " it is not of type 'Display'");
		}
		return new DisplayModelWrapper(model);
	}
	/**
	 * @param model
	 */
	private DisplayModelWrapper(FlowModel model)
	{
		this.model = model;
	}
	/**
	 * Returns the tag of the root HTML element that corresponds to this element
	 */
	public String getTag()
	{
		return (String) model.getProperty(BuiltinProperties.HTML_TAG);
	}
	/**
	 * Returns the style of the root HTML element (corresponds to the HTML 'class attributes') 
	 */
	public String getStyle()
	{
		return (String) model.getProperty(BuiltinProperties.HTML_STYLE);
	}
	/**
	 * Returns 
	 * @return A List of (key/value) pairs
	 */
	public List<String> getSharedProperties() /* (non-Javadoc)
	 * @see tersus.model.FlowModel#addElement(tersus.model.FlowModel)
	 */
	{
		List<String> list = new ArrayList<String>();
		for (Iterator i = getMetaProperties().iterator(); i.hasNext();)
		{
			String name = ((MetaProperty) i.next()).name;
			if (name.startsWith(DISPLAY_PROPERTY_PREFIX))
			{
				list.add(name.substring(DISPLAY_PROPERTY_PREFIX.length()));
			}
		}
		return list;
	}
	public static List<String> getElementProperties(ModelElement element) 
	{
		List<String> list = new ArrayList<String>();
		for (MetaProperty mp: element.getMetaProperties())
		{
			String name = mp.name;
			if (name.startsWith(DISPLAY_PROPERTY_PREFIX))
			{
				list.add(name.substring(DISPLAY_PROPERTY_PREFIX.length()));
			}
		}
		return list;
	}
	
    public static Object getElementProperty(ModelElement element, String name)
    {
        return element.getProperty(DISPLAY_PROPERTY_PREFIX + name);
    }

	/* (non-Javadoc)
	 * @see tersus.model.Model#addElement(int, tersus.model.ModelElement)
	 */
	public void addElement(int index, ModelElement element)
	{
		model.addElement(index, element);
	}
	/* (non-Javadoc)
	 * @see tersus.model.Model#addElement(tersus.model.ModelElement)
	 */
	public void addElement(ModelElement element)
	{
		model.addElement(element);
	}
    /* (non-Javadoc)
     * @see tersus.model.FlowModel#addDataElement(tersus.model.Model, java.lang.String, tersus.model.RelativePosition, tersus.model.RelativeSize)
     */
    public DataElement addDataElement(Model childModel, String elementName,
            RelativePosition position, RelativeSize size)
    {
        return model.addDataElement(childModel, elementName, position, size);
    }
    /* (non-Javadoc)
     * @see tersus.model.FlowModel#addElement(tersus.model.ModelId, java.lang.String, tersus.model.RelativePosition, tersus.model.RelativeSize)
     */
    public ModelElement addElement(ModelId childModelId, String elementName,
            RelativePosition position, RelativeSize size)
    {
        return model.addElement(childModelId, elementName, position, size);
    }
	/* (non-Javadoc)
	 * @see tersus.model.FlowModel#addLink(tersus.model.Path, tersus.model.Path)
	 */
	public void addLink(Link link)
	{
		model.addLink(link);
	}
	/* (non-Javadoc)
	 * @see tersus.model.ModelObject#addMetaProperty(tersus.model.MetaProperty)
	 */
	public boolean addMetaProperty(MetaProperty metaProperty)
	{
		return model.addMetaProperty(metaProperty);
	}
	/* (non-Javadoc)
	 * @see tersus.model.ModelObject#addPropertyChangeListener(java.beans.PropertyChangeListener)
	 */
	public void addPropertyChangeListener(PropertyChangeListener l)
	{
		model.addPropertyChangeListener(l);
	}
	/* (non-Javadoc)
	 * @see tersus.model.Model#clearModified()
	 */
	public void clearModified()
	{
		model.clearModified();
	}
	/* (non-Javadoc)
	 * @see tersus.model.FlowModel#createSlot(tersus.model.SlotType, tersus.model.RelativePosition)
	 */
	public Slot createSlot(SlotType slotType, RelativePosition position)
	{
		return model.createSlot(slotType, position, null);
	}
	/* (non-Javadoc)
	 * @see tersus.model.Model#defaultRole(java.lang.String)
	 */
	public Role defaultRole(String suggestedRole)
	{
		return model.defaultRole(suggestedRole);
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj)
	{
		return model.equals(obj);
	}
	/* (non-Javadoc)
	 * @see tersus.model.ModelObject#fireStructureChanged(java.lang.String)
	 */
	public void fireStructureChanged(String typeOfChange)
	{
		model.fireStructureChanged(typeOfChange);
	}
	/* (non-Javadoc)
	 * @see tersus.model.Model#getElement(tersus.model.Path)
	 */
	public ModelElement getElement(Path path)
	{
		return model.getElement(path);
	}
	/* (non-Javadoc)
	 * @see tersus.model.Model#getElement(tersus.model.Role)
	 */
	public ModelElement getElement(Role role)
	{
		return model.getElement(role);
	}
	/* (non-Javadoc)
	 * @see tersus.model.Model#getElements()
	 */
	public List getElements()
	{
		return model.getElements();
	}
	/* (non-Javadoc)
	 * @see tersus.model.Model#getId()
	 */
	public ModelId getId()
	{
		return model.getId();
	}
	/* (non-Javadoc)
	 * @see tersus.model.ModelObject#getMetaProperties()
	 */
	public List getMetaProperties()
	{
		return model.getMetaProperties();
	}
	/* (non-Javadoc)
	 * @see tersus.model.ModelObject#getMetaProperty(java.lang.String)
	 */
	public MetaProperty getMetaProperty(String name)
	{
		return model.getMetaProperty(name);
	}
	/* (non-Javadoc)
	 * @see tersus.model.Model#getName()
	 */
	public String getName()
	{
		return model.getName();
	}
	/* (non-Javadoc)
	 * @see tersus.model.Model#getPlugin()
	 */
	public String getPlugin()
	{
		return model.getPlugin();
	}
	/* (non-Javadoc)
	 * @see tersus.model.ModelObject#getProperty(java.lang.String)
	 */
	public Object getProperty(String key)
	{
		return model.getProperty(key);
	}
	/* (non-Javadoc)
	 * @see tersus.model.Model#getRepository()
	 */
	public IRepository getRepository()
	{
		return model.getRepository();
	}
	/* (non-Javadoc)
	 * @see tersus.model.FlowModel#getSubFlows()
	 */
	public List getSubFlows()
	{
		return model.getSubFlows();
	}
	/* (non-Javadoc)
	 * @see tersus.model.FlowModel#getType()
	 */
	public FlowType getType()
	{
		return model.getType();
	}
	/* (non-Javadoc)
	 * @see tersus.model.FlowModel#getWidth()
	 */
	public double getWidth()
	{
		return model.getWidth();
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode()
	{
		return model.hashCode();
	}
	/* (non-Javadoc)
	 * @see tersus.model.Model#indexOf(tersus.model.ModelElement)
	 */
	public int indexOf(ModelElement child)
	{
		return model.indexOf(child);
	}
	/* (non-Javadoc)
	 * @see tersus.model.FlowModel#isAtomic()
	 */
	public boolean isAtomic()
	{
		return model.isAtomic();
	}
	/* (non-Javadoc)
	 * @see tersus.model.FlowModel#isValidLinkPath(tersus.model.Path)
	 */
	public boolean isValidLinkPath(Path path)
	{
		return model.isValidLinkPath(path);
	}
	/* (non-Javadoc)
	 * @see tersus.model.Model#markModified()
	 */
	public void markModified()
	{
		model.markModified();
	}
	/* (non-Javadoc)
	 * @see tersus.model.Model#removeElement(tersus.model.ModelElement)
	 */
	public void removeElement(ModelElement child)
	{
		model.removeElement(child);
	}
	/* (non-Javadoc)
	 * @see tersus.model.ModelObject#removePropertyChangeListener(java.beans.PropertyChangeListener)
	 */
	public void removePropertyChangeListener(PropertyChangeListener l)
	{
		model.removePropertyChangeListener(l);
	}
	/* (non-Javadoc)
	 * @see tersus.model.Model#resetId(tersus.model.ModelId)
	 */
	public void resetId(ModelId id)
	{
		model.resetId(id);
	}
	/* (non-Javadoc)
	 * @see tersus.model.Model#setId(tersus.model.ModelId)
	 */
	public void setId(ModelId id)
	{
		model.setId(id);
	}
	/* (non-Javadoc)
	 * @see tersus.model.Model#setModified(boolean)
	 */
	public void setPlugin(String plugin)
	{
		model.setPlugin(plugin);
	}
	/* (non-Javadoc)
	 * @see tersus.model.ModelObject#setProperty(java.lang.String, java.lang.Object)
	 */
	public void setProperty(String key, Object value)
		throws IllegalPropertyAccessException
	{
		model.setProperty(key, value);
	}
	/* (non-Javadoc)
	 * @see tersus.model.Model#setRepository(tersus.model.Repository)
	 */
	public void setRepository(IRepository repository)
	{
		model.setRepository(repository);
	}
	/* (non-Javadoc)
	 * @see tersus.model.FlowModel#setType(tersus.model.FlowType)
	 */
	public void setType(FlowType type)
	{
		model.setType(type);
	}
	/* (non-Javadoc)
	 * @see tersus.model.FlowModel#setWidth(double)
	 */
	public void setWidth(double width)
	{
		model.setWidth(width);
	}
	/* (non-Javadoc)
	 * @see tersus.model.FlowModel#toString()
	 */
	public String toString()
	{
		return model.toString();
	}
	
    public String getPluginJavaClass()
    {
        return model.getPluginJavaClass();
    }

    public static boolean correspondsToCompositeDataType (FlowModel model)
    {
    	String plugin = (String) model.getProperty(BuiltinProperties.PLUGIN);

    	return	BuiltinPlugins.VIEW.equals(plugin) ||
    			BuiltinPlugins.MOBILE_VIEW.equals(plugin) ||
    			BuiltinPlugins.GRID.equals(plugin) ||
    			BuiltinPlugins.SIMPLE_TABLE.equals(plugin) ||
    			BuiltinPlugins.TABLE.equals(plugin) ||
    			BuiltinPlugins.ROW.equals(plugin) ||
    			BuiltinPlugins.PANE.equals(plugin) ||
    			BuiltinPlugins.SCROLL_PANE.equals(plugin) ||
    			
    			BuiltinPlugins.IPHONE_VARIABLE_PANE.equals(plugin)||
    			BuiltinPlugins.SCROLLABLE.equals(plugin) ||
    			BuiltinPlugins.FLOATINGTOOLBAR.equals(plugin)||
    			BuiltinPlugins.ACTION_DIALOG.equals(plugin)||
    			BuiltinPlugins.NAVBAR.equals(plugin)||
    			BuiltinPlugins.NAVBAR_ITEM.equals(plugin)||
    			BuiltinPlugins.SPLIT_PANE.equals(plugin) ||
    			BuiltinPlugins.TABBED_PANE.equals(plugin) ||
    			BuiltinPlugins.VARIABLE_PANE.equals(plugin) ||
    			BuiltinPlugins.EMBEDDED_HTML.equals(plugin);
    	// What about trees?
    }

    public static Role correspondingLeafRole (FlowModel model)
    {
        return ("Tersus/Display/Date and Time Input Field".equals(model.getPlugin()) ? VALUE_RESERVED_ROLE_DATE_AND_TIME :
        		"Tersus/UI/Check Box".equals(model.getPlugin()) ? VALUE_RESERVED_ROLE_CHECKBOX : VALUE_RESERVED_ROLE);
    }

    public static ModelId correspondingLeafDataType (FlowModel model)
	{
    	ModelElement valueElement = model.getElement(correspondingLeafRole(model));
    	return (valueElement == null ? null : valueElement.getRefId());
	}
 }

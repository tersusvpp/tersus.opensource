/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;

import java.util.ArrayList;
import java.util.List;

import tersus.util.IllegalValueException;
import tersus.util.Misc;

/**
 * 
 * Data Element (either a flow data element or a sub-element of a data models).
 * 
 * @author Ofer Brandes
 *  
 */

public class DataElement extends ModelElement
{
    public DataElement()
    {
    }

    private static List defaultProperties;
    static
    {
        defaultProperties = new ArrayList();
        defaultProperties.add(new MetaProperty(BuiltinProperties.ROLE,
                Role.class, false, false, false));
        defaultProperties.add(new MetaProperty(BuiltinProperties.TYPE,
                "DataElementType", FlowDataElementType.class, true, false, false));
        defaultProperties.add(new MetaProperty(BuiltinProperties.REF_ID,
                ModelId.class, false, false, false));
        defaultProperties.add(new MetaProperty(BuiltinProperties.REPETITIVE,
                Boolean.class, true, false, false));
        defaultProperties.add(new MetaProperty(BuiltinProperties.POSITION,
                RelativePosition.class, true, false, false));
        defaultProperties.add(new MetaProperty(BuiltinProperties.SIZE,
                RelativeSize.class, true, false, false));
        defaultProperties.add(new MetaProperty(BuiltinProperties.PRIMARY_KEY,
                Boolean.class, true, false, false));
        defaultProperties.add(new MetaProperty(BuiltinProperties.DATABASE_GENERATED,
                Boolean.class, true, false, false));
        defaultProperties.add(new MetaProperty(BuiltinProperties.MANDATORY,
                Boolean.class, true, false, false));
        defaultProperties.add(new MetaProperty(
                BuiltinProperties.EXCLUDE_FROM_FIELD_NAME, Boolean.class, true, false, false));
    };

    private boolean primaryKey = false;
    
    private boolean databaseGenerated = false;

    private FlowDataElementType type = null;

    /*
     * (non-Javadoc)
     * 
     * @see tersus.model.ModelObject#getMetaProperties()
     */
    public List getDefaultProperties()
    {
        return defaultProperties;
    }

    /**
     * Get the model (data type) to which the data element refers.
     */
    public DataType getModel()
    {
        return (DataType) getReferredModel();
    }

    /**
     * Set the data element to refer to a specific model (data type).
     */
    public void setModel(DataType model)
    {
        setReferredModel(model);
    }

    /**
     * @return
     */
    public boolean isPrimaryKey()
    {
        return primaryKey;
    }

    /**
     * @param primaryKey
     */
    public void setPrimaryKey(boolean primaryKey)
    {
        if (this.primaryKey != primaryKey)
        {
            this.primaryKey = primaryKey;
            firePropertyChange(BuiltinProperties.PRIMARY_KEY, Misc
                    .getBoolean(!primaryKey), Misc.getBoolean(primaryKey));
            markModified();
        }
    }

    public void setDatabaseGenerated(boolean databaseGenerated)
	{
		this.databaseGenerated = databaseGenerated;
	}

	public boolean isDatabaseGenerated()
	{
		return databaseGenerated;
	}

	/**
     * @return
     */
    public FlowDataElementType getType()
    {
        return type;
    }

    /**
     * @param type
     */
    public void setType(FlowDataElementType type)
    {
        if (this.type != type)
        {
            FlowDataElementType old = this.type;
            this.type = type;
            markModified();
            firePropertyChange(BuiltinProperties.TYPE, old, type);
        }
    }

    /*
     * A data element is self-ready only if it is a constant and has no incoming links 
     */
    public boolean isSelfReady()
    {
        if (!(getParentModel() instanceof FlowModel))
           return false;
        if (FlowDataElementType.MAIN == getType())
            return true;
        if (!Boolean.TRUE.equals(getReferredModel().getProperty(
                BuiltinProperties.CONSTANT)))
            return false; // This is not a constant
        List parentElements = getParentModel().getElements();
        for (int i = 0; i < parentElements.size(); i++)
        {
            if (parentElements.get(i) instanceof Link)
            {
                Link link = (Link) parentElements.get(i);
                if (this.getRole().isEquivalent(link.getTarget()))
                    return false; // This is triggered by the link
            }
        }
        return true; // This is a constant with no incoming links
    }

	@Override
	public Object getProperty(String key)
	{
		if (BuiltinProperties.ROLE.equals(key))
			return getRole();
		if (BuiltinProperties.REF_ID.equals(key))
			return getRefId();
		if (BuiltinProperties.POSITION.equals(key))
			return getPosition();
		if(BuiltinProperties.SIZE.equals(key))
			return getSize();
		if (BuiltinProperties.TYPE.equals(key))
			return getType();
		if (BuiltinProperties.REPETITIVE.equals(key))
			return isRepetitive();
		else if (BuiltinProperties.PRIMARY_KEY.equals(key))
			return isPrimaryKey();
		return getPropertyBase(key);
	}

	@Override
	public boolean isPropertySet(String key)
	{
		return isPropertySetBase(key);
	}

	@Override
	public void setProperty(String key, Object value)
	{
		if (BuiltinProperties.ROLE.equals(key))
		{
			if (value != null && ! (value instanceof Role))
				illegalValue(key, value);
			setRole((Role)value);
		}
		else if (BuiltinProperties.REF_ID.equals(key))
		{
			if (value != null && ! (value instanceof ModelId))
				illegalValue(key, value);
			setRefId((ModelId)value);
		}
		else if (BuiltinProperties.POSITION.equals(key))
		{
			if (value != null && ! (value instanceof RelativePosition))
				illegalValue(key, value);
			setPosition((RelativePosition)value);
		}
		else if(BuiltinProperties.SIZE.equals(key))
		{
			if (value != null &&  ! (value instanceof RelativeSize))
				illegalValue(key, value);
			setSize((RelativeSize)value);
		}
		else if (BuiltinProperties.TYPE.equals(key))
		{
			if (value != null && ! (value instanceof FlowDataElementType))
				illegalValue(key, value);
			setType((FlowDataElementType) value);
		}
		else if (BuiltinProperties.REPETITIVE.equals(key))
		{
			if (value == null || ! (value instanceof Boolean))
				illegalValue(key, value);
			setRepetitive((Boolean)value);
		}
		else if (BuiltinProperties.PRIMARY_KEY.equals(key))
		{
			if (value == null || ! (value instanceof Boolean))
				illegalValue(key, value);
			setPrimaryKey((Boolean)value);
		}
		else setPropertyBase(key,value);
		
	}
}
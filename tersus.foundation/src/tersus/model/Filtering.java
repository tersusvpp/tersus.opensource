/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/

package tersus.model;

/**
 * Definition of filtering conditions. 
 * @author Ofer Brandes
 */
public class Filtering
{
	public static final int NO_FILTERING = 0;
	public static final int EXACT = 1;
	public static final int STARTS_WITH = 2;
	public static final int CONTAINS = 3;
	public static final int RANGE = 4;
	
	public static String getFilteringName(int type)
	{
		switch (type)
		{
			case EXACT:
				return "Exact";
			case STARTS_WITH:
				return "Starts with";
			case CONTAINS:
				return "Contains";
			case RANGE:
				return "Range";
			default:
				return "";
		}
	}
	
	public static int getFilteringIntValue(String filterName)
	{
		if ("Exact".equals(filterName))
			return EXACT;
		if ("Starts with".equals(filterName))
			return STARTS_WITH;
		if ("Contains".equals(filterName))
			return CONTAINS;
		if ("Range".equals(filterName))
			return RANGE;
		else
			return NO_FILTERING;
	}
}

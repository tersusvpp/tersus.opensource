/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import tersus.util.Misc;

/**
 * @author Youval Bronicki
 */
public class FlowModel extends Model
{
    private double width = 1.3333;

    private static List defaultProperties;

    public List getDefaultProperties()
    {
        return defaultProperties;
    }

    static
    {
        defaultProperties = new ArrayList();
        defaultProperties.add(new MetaProperty(BuiltinProperties.ID,
                ModelId.class, false, false, false));
        defaultProperties.add(new MetaProperty(BuiltinProperties.NAME,
                String.class, false, false, false));
        defaultProperties.add(new MetaProperty(BuiltinProperties.TYPE,
                FlowType.class, true, false, false, false));
        defaultProperties.add(new MetaProperty(BuiltinProperties.WIDTH,
                Double.class, true, false, false));
        defaultProperties.add(new MetaProperty(BuiltinProperties.PLUGIN,
                String.class, true, false, false));
        defaultProperties.add(new MetaProperty(BuiltinProperties.PROTOTYPE,
                String.class, true, false, false));
        defaultProperties.add(new MetaProperty(BuiltinProperties.ICON_FOLDER,
                String.class, true, false, false));
		defaultProperties.add(new MetaProperty(BuiltinProperties.DOCUMENTATION,
				String.class, true,	false, false));
    }

    static final long serialVersionUID = 1;

    private FlowType type;

    /**
     * Adds a process element to this ProcessModel 
     * @param subModel
     */
    public DataElement addDataElement(Model childModel, String elementName,
            RelativePosition position, RelativeSize size)
    {
        DataElement element = new DataElement();
        element.setType(FlowDataElementType.INTERMEDIATE);
        element.setReferredModel(childModel);
        element.setRole(defaultRole(elementName));
        element.setPosition(position);
        element.setSize(size);
        addElement(element);
        return element;
    }

    protected ModelElement createChild(Model childModel, String elementName)
    {
        ModelElement e;
        if (childModel instanceof FlowModel)
            e = new SubFlow();
        else
        {
            if (Misc.ASSERTIONS)
                Misc.assertion(childModel instanceof DataType);
            e = new DataElement();
            ((DataElement) e).setType(FlowDataElementType.INTERMEDIATE);
        }
        e.setReferredModel(childModel);
        e.setRole(defaultRole(elementName));
        return e;
    }

    public ModelElement addElement(ModelId childModelId, String elementName,
            RelativePosition position, RelativeSize size)
    {
        ModelElement child = createChild(childModelId, elementName);
        child.setPosition(position);
        child.setSize(size);
        addElement(child);
        return child;
    }

    public ModelElement createChild(ModelId childModelId, String elementName)
    {
        Model childModel = repository.getModel(childModelId, true);
        return createChild(childModel, elementName);
    }

    public String toString()
    {
        return id.getPath();
    }

    public List<SubFlow> getSubFlows()
    {
        ArrayList<SubFlow> list = new ArrayList<SubFlow>();
        for (Object element: elements)
        {
            if (element instanceof SubFlow)
                list.add((SubFlow)element);
        }
        return list;
    }

    public boolean isAtomic()
    {
        return getSubFlows().size() == 0;
    }

    public void addLink(Link link)
    {
        Role role = defaultRole("Flow");
        link.setRole(role);
        addElement(link);
    }

    public FlowType getType()
    {
        return type;
    }

    public void setType(FlowType type)
    {
        FlowType oldValue = this.type;
        if (type != oldValue)
        {
            this.type = type;
            firePropertyChange(BuiltinProperties.TYPE, oldValue, type);
            markModified();
        }
    }

    public double getWidth()
    {
        return width;
    }

    public void setWidth(double width)
    {
        width = Precision.round(width);
        if (this.width != width)
        {
            Double oldWidth = new Double(this.width);
            this.width = width;
            firePropertyChange(BuiltinProperties.WIDTH, oldWidth, new Double(
                    width));
            markModified();
        }
    }

    // TODO remove when XML Serialization is ready
    Object readResolve()
    {
        if (width == 0)
            width = 1.3333;
        return this;
    }

    
   
    /**
     * Check whether a given path is valid as a link source or target
     * 
     * @param path
     * @return true if path points to a slot of this flow or of a sub-flow, to a
     *         data-element of this flowmodel or to a descendant of a
     *         data-element, false otherwise
     */
    public boolean isValidLinkPath(Path path)
    {
    	if (path == null)
    		return false;
        if (path.getNumberOfSegments() == 0)
            return false;
        ModelElement element = getElement(path);
        if (element == null)
            return false;
        if (element instanceof Slot)
        {
            return path.getNumberOfSegments() <= 2;
        }
        if (path.getNumberOfSegments() >= 1
                && getElement(path.getSegment(0)) instanceof DataElement)
            return true;
        return false;
    }

    public Slot createSlot(SlotType slotType, RelativePosition position, Role role)
    {
        // TODO Implement plugin-specific properties (e.g. switch
        // value/expression)
        Slot slot = new Slot();
        slot.setType(slotType);
        if (role == null)
        	role = defaultRole(slotType.toString());
        slot.setRole(role);
        slot.setPosition(position);
        if (slotType == SlotType.TRIGGER || slotType == SlotType.IO)
            slot.setMandatory(true);
        addElement(slot);
        return slot;
    }
    public void updateRanks()
    {
    	updateRanks(null);
    }

    public void updateRanks(StringBuffer summary)
    {
        HashSet nodes = new HashSet();
        HashSet dependentNodes = new HashSet();
        List<Dependency> dependencies = new ArrayList<Dependency>();
        List links = new ArrayList();
		if (summary!=null)
			summary.append("Dependencies:\n*************\n");
        for (ModelElement element: getElements())
        {
            if (element instanceof Link)
            {
                Dependency dependency = new Dependency((Link) element);
				dependencies.add(dependency);
				if (summary!=null)
					summary.append(dependency.getSourceElement().getRole()  + " -> " + dependency.getTargetElement().getRole()+ "  ["+element.getRole()+"]\n");
                links.add(element);
            }
            else
                nodes.add(element);
        }
        // Find cases where there's the target of one link is an element (or
        // sub-element) of the target of another link
        //OPTIMIZE: use a more efficient algorithm
        for (int i = 0; i < links.size(); i++)
        {
            Link link1 = (Link) links.get(i);
            for (int j = 0; j < links.size(); j++)
            {
                if (j != i)
                {
                    Link link2 = (Link) links.get(j);
                    if (link1.getTarget().beginsWith(link2.getTarget())
                            && !link1.getTarget().isEquivalent(
                                    link2.getTarget()))
                    {
                        Dependency dependency = new Dependency(link2
                                .getSourceElement(), link1.getSourceElement());
						dependencies.add(dependency);
        				if (summary!=null)
        					summary.append(dependency.getSourceElement().getRole()  + " -> " + dependency.getTargetElement().getRole()+ " [Target of "+link1.getRole()+" is ancestor of target of "+link2.getRole()+" ]\n");
                    }
                }
            }

        }
		if (summary!=null)
			summary.append("\nRanks:\n******\n");
        int rank = 0;
        while (!nodes.isEmpty())
        {
            // Move all dependent nodes to a seprate list
            dependentNodes.clear();
            for (int i = 0; i < dependencies.size(); i++)
            {
                Dependency dependency = (Dependency) dependencies.get(i);
                ModelElement target = dependency.getTargetElement();
                ModelElement source = dependency.getSourceElement();
                if (nodes.contains(source) && nodes.contains(target))
                {
                    dependentNodes.add(target);
                }
            }
            nodes.removeAll(dependentNodes);
            if (nodes.isEmpty())
            {
                /*
                 * All nodes are apparently dependent (cyclic dependency)
                 * Find nodes that are 'at the tail' and exclude them from the cycle
                 */
                HashSet head = getHead(dependentNodes, dependencies);
                HashSet tmp = nodes;
                nodes = head;
                dependentNodes.removeAll(head);
            }
            for (Iterator i = nodes.iterator(); i.hasNext();)
            {
                ModelElement node = (ModelElement) i.next();
                node.setDependencyRank(rank);
    			if (summary!=null)
    				summary.append(node.getRole()+ " : rank "+rank+ "\n");
            }
            HashSet tmp = nodes;
            nodes = dependentNodes;
            dependentNodes = tmp;
            ++rank;
        }
    }

    /**
     * @param dependentNodes
     * @param dependencies
     * @return
     */
    private HashSet getHead(HashSet nodes, List dependencies)
    {
        HashSet set = new HashSet();
        set.addAll(nodes);
        HashSet head = new HashSet();
        HashSet sourceNodes;
        do
        {
            sourceNodes = new HashSet<ModelElement>();
            for (int i = 0; i < dependencies.size(); i++)
            {
                Dependency dependency = (Dependency) dependencies.get(i);
                ModelElement target = dependency.getTargetElement();
                ModelElement source = dependency.getSourceElement();
                if (set.contains(source) && set.contains(target))
                {
                    sourceNodes.add(source);
                }
            }
            set.removeAll(sourceNodes);
            head.addAll(sourceNodes);
        }
        while (!sourceNodes.isEmpty());
        return head;
    }

    /**
     * @return The list of sub-flows, ordered by the x/y location
     * 
     * Sub-flow a is anterior to sub-flow b if the whole of a is higher than b
     * or if there is an overlap in the y axis and the whole of a is to the left
     * of b
     */
    public List<SubFlow> getOrderedSubFlows()
    {
        List<SubFlow> subFlows = new ArrayList<SubFlow>();
        for (Object element: getElements())
        {
            if (element instanceof SubFlow)
                subFlows.add((SubFlow)element);
        }
        for (int i = 0; i < subFlows.size(); i++)
            for (int j = i + 1; j < subFlows.size(); j++)
            {
                SubFlow si = (SubFlow) subFlows.get(i);
                SubFlow sj = (SubFlow) subFlows.get(j);
                if (!checkXYOrder(si, sj))
                {
                    subFlows.set(i, sj);
                    subFlows.set(j, si);
                }
            }
        return subFlows;
    }

    /**
     * @return true if s1 is 'prior' to 's2' in the XY order
     */
    private boolean checkXYOrder(SubFlow s1, SubFlow s2)
    {
        double x1 = s1.getPosition().getX();
        double y1 = s1.getPosition().getY();
        RelativeSize size1 = s1.getSize();
        double right1 = x1 + size1.getWidth() / 2;
        double left1 = x1 - size1.getWidth() / 2;
        double top1 = y1 - size1.getHeight() / 2;
        double bottom1 = y1 + size1.getHeight() / 2;

        double x2 = s2.getPosition().getX();
        double y2 = s2.getPosition().getY();
        RelativeSize size2 = s2.getSize();
        double right2 = x2 + size2.getWidth() / 2;
        double left2 = x2 - size2.getWidth() / 2;
        double top2 = y2 - size2.getHeight() / 2;
        double bottom2 = y2 + size2.getHeight() / 2;

        if (bottom1 <= top2)
            return true; // s2 is below s1
        else if (bottom2 <= top1)
            return false; // s1 is below s2
        else if (left1 <= left2)
            return true; // s1 is to the right of s2
        else
            return false;
    }

    public boolean hasMandatoryTriggers()
    {
        List<ModelElement> elements = getElements();
        for (int i = 0; i < elements.size(); i++)
        {
            ModelElement element = (ModelElement) elements.get(i);
            if (SlotType.TRIGGER == element.getProperty(BuiltinProperties.TYPE)
                    && Boolean.TRUE.equals(element
                            .getProperty(BuiltinProperties.MANDATORY)))
                return true;

        }
        return false;
    }
    public boolean hasNonMandatoryTriggers()
    {
        List<ModelElement> elements = getElements();
        for (int i = 0; i < elements.size(); i++)
        {
            ModelElement element = (ModelElement) elements.get(i);
            if (SlotType.TRIGGER == element.getProperty(BuiltinProperties.TYPE)
                    && Boolean.FALSE.equals(element
                            .getProperty(BuiltinProperties.MANDATORY)))
                return true;

        }
        return false;
    }
    
    public boolean hasTriggers()
    {
        for (ModelElement e: getElements())
        {
            
            if (SlotType.TRIGGER == e.getProperty(BuiltinProperties.TYPE))
            {
                return true; 
            }
        }
        return false;
    }

    public static final String DISPLAY_PROPERTY_PREFIX = "html.";

    public Object getDisplayProperty(String name)
    {
    	return getProperty(DISPLAY_PROPERTY_PREFIX + name);
    }

	@Override
	public Object getProperty(String key)
	{
        if (BuiltinProperties.ID.equals(key))
        {
        	return getId();
        }
        else if (BuiltinProperties.NAME.equals(key))
        {
        	return getName();
        }
        else if (BuiltinProperties.TYPE.equals(key))
        {
        	return getType();
        }
        else if (BuiltinProperties.WIDTH.equals(key))
        {
        	return getWidth();
        }
        else if (BuiltinProperties.PLUGIN.equals(key))
        {
        	return getPlugin();
        }
        else if (BuiltinProperties.PROTOTYPE.equals(key))
        {
        	return getPrototype();
        }
        else if (BuiltinProperties.ICON_FOLDER.equals(key))
        {
        	return getIconFolder();
        }
        else if (BuiltinProperties.DOCUMENTATION.equals(key))
        {
        	return getDocumentation();
        }
        else
        	return getPropertyBase(key);

	}

	@Override
	public boolean isPropertySet(String key)
	{
		if(BuiltinProperties.ICON_FOLDER.equals(key))
			return isIconFolderSet();
		else if(BuiltinProperties.DOCUMENTATION.equals(key))
			return isDocumentationSet();
		else if(BuiltinProperties.PROTOTYPE.equals(key))
			return isPrototypeSet();
		else
			return isPropertySetBase(key);

	}

	@Override
	public void setProperty(String key, Object value)
	{
		if (BuiltinProperties.ID.equals(key))
		{
			if (value != null && ! (value instanceof ModelId))
				illegalValue(key, value);
			setId((ModelId)value);
		}
		else if(BuiltinProperties.NAME.equals(key))
		{
			illegalValue(key, value);
		}
        else if (BuiltinProperties.TYPE.equals(key))
        {
        	if (value!= null && ! (value instanceof FlowType))
    			illegalValue(key, value);
        	setType((FlowType)value);
        }
        else if (BuiltinProperties.WIDTH.equals(key))
        {
        	if (!(value instanceof Double) )
    			illegalValue(key, value);
        	setWidth((Double)value);
        }
    	else if(BuiltinProperties.PLUGIN.equals(key))
		{
			if (value != null && ! (value instanceof String))
				illegalValue(key, value);
			setPlugin((String)value);
		}
		else if(BuiltinProperties.PROTOTYPE.equals(key))
		{
			if (value != null && ! (value instanceof String))
				illegalValue(key, value);
			setPrototype((String)value);
		}
		else if(BuiltinProperties.ICON_FOLDER.equals(key))
		{
			if (value != null && ! (value instanceof String))
				illegalValue(key, value);
			setIconFolder((String)value);
		}
		else if(BuiltinProperties.DOCUMENTATION.equals(key))
		{
			if (value != null && ! (value instanceof String))
				illegalValue(key, value);
			setDocumentation((String)value);
		}
	    else
	    {
	    	setPropertyBase(key, value);
	    }
	}

}

class Dependency
{
    public Dependency(ModelElement sourceElement, ModelElement targetElement)
    {
        this.sourceElement = sourceElement;
        this.targetElement = targetElement;
    }

    Dependency(Link link)
    {
        sourceElement = link.getSourceElement();
        targetElement = link.getTargetElement();
    }

    private ModelElement sourceElement;

    private ModelElement targetElement;

    ModelElement getSourceElement()
    {
        return sourceElement;
    }

    ModelElement getTargetElement()
    {
        return targetElement;
    }

    public String toString()
    {
        return sourceElement + " ==> " + targetElement;
    }
}
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model;

import java.lang.ref.WeakReference;
import java.util.List;

import tersus.InternalErrorException;
import tersus.util.Misc;

public abstract class ModelElement extends ModelObject
{

	private int dependencyRank = -1;

	private Model parentModel;

	private WeakReference<Model> referredModelReference;

	static final long serialVersionUID = 1;

	private Role role;

	private RelativePosition position;

	private RelativeSize size;

	private boolean repetitive;

	public void setPosition(double x, double y)
	{
		setPosition(new RelativePosition(x, y));
	}

	public void setPosition(RelativePosition position)
	{
		RelativePosition oldPosition = this.position;
		if (position == null)
			this.position = null;
		else
			this.position = position.getCopy();
		firePropertyChange(BuiltinProperties.POSITION, oldPosition, position);
		markModified();

	}

	public Role getRole()
	{
		return role;
	}

	/*
	 * Used by the reflection mechanism
	 */
	public String getElementName()
	{
		return role.toString();
	}

	/**
	 * Sets the role of this element (notifying listeners about the change)
	 * 
	 * @param role
	 *            The new role for this element
	 */
	public void setRole(Role role)
	{
		Role oldRole = this.role;
		this.role = role;
		firePropertyChange(BuiltinProperties.ROLE, oldRole, role);
		markModified();
	}

	public Model getReferredModel()
	{
		if (referredModelReference != null && referredModelReference.get() != null)
			return referredModelReference.get();
		if (getRepository() == null)
			throw new InternalErrorException("Trying to get referred model of orphan element");
		
		Model refModel = null;
		
		if (refId != null)
		{
			refModel = getRepository().getModel(refId, true);

			if (refModel != null)
				referredModelReference = new WeakReference<Model>(refModel);
		}

		return refModel;
	}

	public RelativePosition getPosition()
	{
		return position;
	}

	/**
	 * @return
	 */
	public RelativeSize getSize()
	{
		return size;
	}

	/**
	 * @param size
	 */
	public void setSize(RelativeSize size)
	{
		if (size != null)
		{
			setSize(size.getWidth(), size.getHeight());
			markModified();
		}

	}

	/**
	 * @param width
	 * @param height
	 */
	public void setSize(double width, double height)
	{
		RelativeSize oldSize = this.size;
		this.size = new RelativeSize(width, height);
		if (oldSize == null && position == null)
			return;
		if (oldSize == null || !oldSize.equals(size))
			firePropertyChange(BuiltinProperties.SIZE, oldSize, size);
		markModified();
	}

	/**
	 * @param position
	 * @param size
	 * @return
	 */
	/**
	 * @return
	 */
	public boolean isRepetitive()
	{
		return repetitive;
	}

	/**
	 * @param b
	 */
	public void setRepetitive(boolean b)
	{
		if (b == repetitive)
			return;
		repetitive = b;
		firePropertyChange(BuiltinProperties.REPETITIVE, Misc.getBoolean(!b), Misc.getBoolean(b));
		markModified();
	}

	/**
	 * @return
	 */
	public Model getParentModel()
	{
		return parentModel;
	}

	/**
	 * @param model
	 */
	public void setParentModel(Model model)
	{
		parentModel = model;
	}

	protected void markModified()
	{
		if (parentModel != null)
			parentModel.markModified();
	}

	protected ModelId refId;

	public ModelId getRefId()
	{
		return refId;
	}

	/*
	 * Synonym for getRefId() - used by the reflection plugin for Data Element
	 * 
	 * @return
	 */
	public ModelId getModelId()
	{
		return refId;
	}

	/**
	 * @param string
	 */
	public void setRefId(ModelId id)
	{
		basicSetRefId(id);
		if (parentModel != null)
			parentModel.fireStructureChanged(BuiltinProperties.ELEMENTS);
	}

	public void basicSetRefId(ModelId id)
	{
		referredModelReference = null;
		ModelId oldRefId = refId;
		refId = id;
		markModified();
		firePropertyChange(BuiltinProperties.REF_ID, oldRefId, id);

	}

	public void setReferredModel(Model model)
	{
		referredModelReference = new WeakReference<Model>(model);
		refId = model.getId();
		markModified();
		if (parentModel != null)
			parentModel.fireStructureChanged(BuiltinProperties.ELEMENTS);

	}

	public IRepository getRepository()
	{
		if (getParentModel() == null)
			return null;
		return getParentModel().getRepository();
	}

	/**
	 * @param rank
	 */
	public void setDependencyRank(int rank)
	{
		this.dependencyRank = rank;
	}

	/**
	 * Returns the dependency rank of this element (applicable to elements of flow models) NOTE:
	 * Before calling getDependencyRank(), you should call updateDependencyRanks() on the parent
	 * flow model
	 * 
	 * @return the dependency rank (0,1,2 where 0 means no dependencies, and rank(E)=max(rank(e))+ 1
	 *         where e are all the nodes that are anterior to E or if updateDependencyRanks() was
	 *         not called on the parent FlowModel @
	 */
	public int getDependencyRank()
	{
		return dependencyRank;
	}

	/**
	 * 
	 * @return true if instances of this element are ready when the parent process starts
	 * 
	 *         For links, slots, and elements of non-FlowModels, this method returns false
	 */
	public boolean isSelfReady()
	{
		return false;
	}

	protected List getDefaultProperties()
	{
		return null;
	}

	public String toString()
	{
		return getRole() + " [" + Misc.getShortName(getClass()) + "]";
	}

	public void clearPosition()
	{
		this.position = null;
	}

	public void clearSize()
	{
		this.size = null;
	}

	public void resetReferredModel()
	{
		referredModelReference = null;
	}
}

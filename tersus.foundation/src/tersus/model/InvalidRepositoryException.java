package tersus.model;

public class InvalidRepositoryException extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public InvalidRepositoryException(String message, Throwable e)
	{
		super(message,e);
	}
	public InvalidRepositoryException(String message)
	{
		super(message);
	}

}

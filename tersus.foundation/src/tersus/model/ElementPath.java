/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;

/**
 * A path that identifies a model element
 * 
 * @author Youval Bronicki
 * 
 */
public interface ElementPath
{

	/**
	 * @return
	 */
	int getNumberOfSegments();

	/**
	 * @param i
	 */
	Role getSegment(int i);

	/**
	 * @param i
	 * @return
	 */
	Path getPrefix(int i);
	//NICE2 use ElementPath instead of Path wherever possible
	
	StringBuffer appendToBuffer(StringBuffer buffer);

	/**
	 * @param path
	 * @return
	 */
	boolean isEquivalent(ElementPath path);
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;

/**
 * @author Youval Bronicki
 *  
 */
public interface BuiltinPlugins
{
    String TEXT = "Tersus/Data Types/Text";
    String NUMBER = "Tersus/Data Types/Number";
    String DATE = "Tersus/Data Types/Date";
    String BOOLEAN = "Tersus/Data Types/Boolean";
    String DATA_STRUCTURE = "Tersus/Composite Data Types/Data Structure";
    String TEXT_CONSTANT = "Tersus/Constants/Text";
    String NUMBER_CONSTANT = "Tersus/Constants/Number";
    String DATE_CONSTANT = "Tersus/Constants/Date";
    String DATABASE_RECORD = "Tersus/Composite Data Types/Database Record";
    String DISPLAY = "Tersus/Display/Generic Display";
    String ACTION = "Tersus/Basic/Action";
    String SYSTEM = "Tersus/Basic/System";
    String SERVICE = "Tersus/Basic/Service";

    String VIEW = "Tersus/Display/View";
    String MOBILE_VIEW = "Tersus/Widgets/Mobile View";
    String NEW_DESKTOP_VIEW = "Tersus/UI/Desktop View";
    String NEW_TABLET_VIEW = "Tersus/UI/Tablet View";
    String NEW_MOBILE_VIEW = "Tersus/UI/Mobile View";
    String GRID = "Tersus/Display/Grid";
    String SIMPLE_TABLE = "Tersus/Display/Simple Table";
    String TABLE = "Tersus/Display/Table";
    String TABLE_WIZARD = "Tersus/Display/Table Wizard";
    String ROW = "Tersus/Display/Row";
    String PANE = "Tersus/Display/Pane";
    String SCROLL_PANE = "Tersus/Display/Scroll Pane";
    String HTML_EDITOR = "Tersus/Display/HTML Editor";
    
    String SPLIT_PANE = "Tersus/Display/Split Pane";
    String TABBED_PANE = "Tersus/Display/Tabbed Pane";
    String VARIABLE_PANE = "Tersus/Display/Variable Pane";
    String EMBEDDED_HTML = "Tersus/Display/Embedded Element";
    
    String BUTTON = "Tersus/Display/Button";

    String POPUP = "Tersus/Display/Popup";
    String DILOG = "Tersus/Display/Dialog";
    String POPIN = "Tersus/Display/Pop-in";

    String TEXT_DISPLAY = "Tersus/Display/Text Display";
    String TEXT_INPUT_FIELD = "Tersus/Display/Text Input Field";
    String TEXT_AREA = "Tersus/Display/Text Area";

    String NUMBER_DISPLAY = "Tersus/Display/Number Display";
    String NUMBER_INPUT_FIELD = "Tersus/Display/Number Input Field";
    String PERCENT_DISPLAY = "Tersus/Display/Percent Display";

    String DATE_DISPLAY = "Tersus/Display/Date Display";
    String DATE_INPUT_FIELD = "Tersus/Display/Date Input Field";

    String CHECK_BOX = "Tersus/Display/Check Box";
    String DYNAMIC_DISPLAY = "Tersus/Display/Dynamic Display";
	String INPUT_ADAPTER = "Tersus/Basic/Input Adapter";
	
	String MENU_BAR = "Tersus/Display/Menu Bar";
	
	String TAG = "Tersus/HTML/Tag";

	//iPhone
	String IPHONE_VARIABLE_PANE = "Tersus/iPhone/Variable Pane";
	String SCROLLABLE = "Tersus/iPhone/Scrollable";
	String FLOATINGTOOLBAR = "Tersus/iPhone/Floating Toolbar";
	String ACTION_DIALOG= "Tersus/iPhone/Action Dialog";
	String LIST = "Tersus/iPhone/List";
	String MENU_ITEM = "Tersus/iPhone/Menu Item";
	String CONTENT_ITEM = "Tersus/iPhone/Content Item";
	String NAVBAR = "Tersus/iPhone/Navigation Bar";
	String NAVBAR_ITEM = "Tersus/iPhone/Navigation Bar Item/";
}
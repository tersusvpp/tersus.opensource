/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;


/**
 * Represents an Error in a model element in a specific context
 * 
 * @author Youval Bronicki
 *
 */
public class ErrorMessage
{
	ElementPath path;
	ModelId modelId;
	String errorMessage;

	/**
	 * @param path
	 * @param modelId
	 * @param errorMessage
	 */
	public ErrorMessage(ElementPath path, ModelId modelId, String errorMessage)
	{
		
		this.path = path;
		this.modelId = modelId;
		this.errorMessage = errorMessage;
	}

}

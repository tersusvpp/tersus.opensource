/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;

import java.util.ArrayList;
import java.util.List;

import tersus.util.Misc;


public class Slot extends ModelElement
{
    public static final String DATA_TYPE_SETTING_EXPLICIT = "explicit";
    public static final String DATA_TYPE_SETTING_INFERRED = "inferred";
    
	static final long serialVersionUID = 1;

	SlotType type;
	private boolean mandatory = false;
	private String dataTypeSetting = null;

	public Slot()
	{ 
	}

	/* (non-Javadoc)
	 * @see tersus.model.ModelObject#getProperties()
	 */
	//FUNC3 make the MANDATORY property conditional (only for triggers)
	private static List defaultProperties;
	public List getDefaultProperties()
	{
		return defaultProperties;
	}
	static 
	{
		defaultProperties = new ArrayList();
		defaultProperties.add(new MetaProperty(BuiltinProperties.ROLE, Role.class, false, false, false));
		defaultProperties.add(new MetaProperty(BuiltinProperties.TYPE, SlotType.class, true, false, false));
		defaultProperties.add(new MetaProperty(BuiltinProperties.DATA_TYPE_ID, ModelId.class, true, false, false));
		defaultProperties.add(new MetaProperty(BuiltinProperties.POSITION, RelativePosition.class, true, false, false));
		defaultProperties.add(new MetaProperty(BuiltinProperties.REPETITIVE, Boolean.class, true, false, false));
		defaultProperties.add(new MetaProperty(BuiltinProperties.MANDATORY, Boolean.class, true, false, false));
		defaultProperties.add(new MetaProperty(BuiltinProperties.DATA_TYPE_SETTING, String.class, false, false, false));
	}

	/**
	 * @return
	 */
	public SlotType getType()
	{
		if (type == null)
			return SlotType.INPUT;
		return type;
	}

	/**
	 * @param type
	 */
	public void setType(SlotType type)
	{
		if (Misc.ASSERTIONS)
			Misc.assertion(type != null);
		SlotType oldType = this.type;
		if (oldType != type)
		{
			this.type = type;
			firePropertyChange(BuiltinProperties.TYPE, oldType, type);
		}
	}

	/**
	 * @return true if this slot is mandatory
	 */
	public boolean isMandatory()
	{
		return mandatory;
	}
	
	/**
	 * Sets the mandatory status of this slot
	 * @param b - The mandatory status 
	 */
	public void setMandatory(boolean b)
	{
		if (mandatory != b)
		{
			Boolean oldValue = Misc.getBoolean(mandatory);
			mandatory = b;
			markModified();
			firePropertyChange(BuiltinProperties.MANDATORY, oldValue, Misc.getBoolean(mandatory));
		}
	}

    public String getDataTypeSetting()
    {
        if(dataTypeSetting == null)
            initDataTypeSetting();
        
        return dataTypeSetting;
    }
    
	public void setDataTypeSetting(String value)
	{
	    String oldValue = dataTypeSetting;
	    
	    if(DATA_TYPE_SETTING_EXPLICIT.equals(value))
	        dataTypeSetting = DATA_TYPE_SETTING_EXPLICIT;
	    else 
	        dataTypeSetting = DATA_TYPE_SETTING_INFERRED;
	    
	    firePropertyChange(BuiltinProperties.DATA_TYPE_SETTING, oldValue, dataTypeSetting);
	}
	
	public void setDataTypeSetting(Boolean value)
    {
        String oldValue = dataTypeSetting;
        
        if(value)
            dataTypeSetting = DATA_TYPE_SETTING_EXPLICIT;
        else 
            dataTypeSetting = DATA_TYPE_SETTING_INFERRED;
        
        firePropertyChange(BuiltinProperties.DATA_TYPE_SETTING, oldValue, dataTypeSetting);
    }

	private void initDataTypeSetting()
	{
	    ModelId temp = getDataTypeId();
        if(getDataTypeId() != null)
            dataTypeSetting = DATA_TYPE_SETTING_EXPLICIT;
        else
            dataTypeSetting = DATA_TYPE_SETTING_INFERRED;
	}
	
	/**
	 * @return the modelId of the model referred by this slot (usually a DataModelId but sometimes a FlowModelId)
	 */
	public ModelId getDataTypeId()
	{
		return getRefId();
	}

	/**
	 * Set the type of this slot
	 * @param dataTypeId - the model id of the slot's data type (usually a DataModelId but sometimes a FlowModelId)
	 */
	public void setDataTypeId(ModelId typeId)
	{
		setRefId(typeId);
	}

	/* (non-Javadoc)
	 * @see tersus.model.ModelElement#setRefId(java.lang.String)
	 */
	public void setRefId(ModelId id)
	{
	    ModelId oldId = getDataTypeId();
		basicSetRefId(id);
		firePropertyChange(BuiltinProperties.DATA_TYPE_ID, oldId, id);
	}

	@Override
	public Object getProperty(String key)
	{
		if (BuiltinProperties.ROLE.equals(key))
			return getRole();
		if (BuiltinProperties.REF_ID.equals(key))
			return getRefId();
		if (BuiltinProperties.DATA_TYPE_ID.equals(key))
			return getDataTypeId();
		if (BuiltinProperties.POSITION.equals(key))
			return getPosition();
		if(BuiltinProperties.DATA_TYPE_SETTING.equals(key))
			return getDataTypeSetting();
		if (BuiltinProperties.TYPE.equals(key))
			return getType();
		if (BuiltinProperties.REPETITIVE.equals(key))
			return isRepetitive();
		if (BuiltinProperties.MANDATORY.equals(key))
			return isMandatory();
		return getPropertyBase(key);
	}

	@Override
	public boolean isPropertySet(String key)
	{
		return isPropertySetBase(key);
	}

	@Override
	public void setProperty(String key, Object value)
	{
		if (BuiltinProperties.ROLE.equals(key))
		{
			if (value != null && ! (value instanceof Role))
				illegalValue(key, value);
			setRole((Role)value);
		}
		else if (BuiltinProperties.REF_ID.equals(key))
		{
			if (value != null && ! (value instanceof ModelId))
				illegalValue(key, value);
			setRefId((ModelId)value);
		}
		else if (BuiltinProperties.DATA_TYPE_ID.equals(key))
		{
			if (value != null && ! (value instanceof ModelId))
				illegalValue(key, value);
			setDataTypeId((ModelId)value);
		}
		else if (BuiltinProperties.POSITION.equals(key))
		{
			if (value != null && ! (value instanceof RelativePosition))
				illegalValue(key, value);
			setPosition((RelativePosition)value);
		}
		else if (BuiltinProperties.DATA_TYPE_SETTING.equals(key))
		{
			if (value != null && ! (value instanceof String))
				illegalValue(key, value);
			setDataTypeSetting((String)value);
		}
		else if (BuiltinProperties.TYPE.equals(key))
		{
			if (value != null && ! (value instanceof SlotType))
				illegalValue(key, value);
			setType((SlotType) value);
		}
		else if (BuiltinProperties.REPETITIVE.equals(key))
		{
			if (value == null || ! (value instanceof Boolean))
				illegalValue(key, value);
			setRepetitive((Boolean)value);
		}
		else if (BuiltinProperties.MANDATORY.equals(key))
		{
			if (value == null || ! (value instanceof Boolean))
				illegalValue(key, value);
			setMandatory((Boolean)value);
		}
		else setPropertyBase(key,value);
		
	}

}

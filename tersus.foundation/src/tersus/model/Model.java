/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tersus.util.Misc;
import tersus.util.NotImplementedException;

/**
 * 
 */
public abstract class Model extends ModelObject
{

	static final long serialVersionUID = 1;
	protected ModelId id;
	private boolean modified;
	private String iconFolder = null;
	private String documentation = null;
	private String prototype = null;

	protected transient IRepository repository;

	public IRepository getRepository()
	{
		return repository;
	}

	public void setRepository(IRepository repository)
	{
		this.repository = repository;
	}

	/**
	 * @return the id of this model
	 */
	public ModelId getId()
	{
		return id;
	}

	/**
	 * Synonym for getId() required for the reflection mechanism TODO - remove getId() and keep
	 * getModelId()
	 * 
	 * @return
	 */
	public ModelId getModelId()
	{
		return id;
	}

	public PackageId getPackageId()
	{
		return id.getPackageId();
	}

	protected ArrayList<ModelElement> elements = new ArrayList<ModelElement>();

	public void addElement(ModelElement element)
	{
		addElement(-1, element);
	}

	public void addElement(int index, ModelElement element)
	{
		if (index < 0)
			elements.add(element);
		else
			elements.add(index, element);
		element.setParentModel(this);
		markModified();
		fireStructureChanged(BuiltinProperties.ELEMENTS);
	}

	/**
	 * @return the list of elements of this model, or null if the model is atomic
	 * 
	 *         <b>Warning:</b> getElements may return the internal list, so the list must not be
	 *         modified (use addElement or removeElement to modify the list of elements)
	 */
	public List<ModelElement> getElements()
	{
		return elements;
	}

	/**
	 * @return the list of elements of this model except for those specified to be skipped, or null
	 *         if the model is atomic
	 * 
	 *         <b>Warning:</b> getElements may return the internal list, so the list must not be
	 *         modified (use addElement or removeElement to modify the list of elements)
	 */
	public List<ModelElement> getElements(List<Role> exceptForRoles)
	{
		if (exceptForRoles == null || exceptForRoles.isEmpty() || elements == null
				|| elements.isEmpty())
			return elements;

		ArrayList<ModelElement> elementList = new ArrayList<ModelElement>();
		for (ModelElement element : elements)
		{
			if (element != null && !exceptForRoles.contains(element.getRole()))
				elementList.add(element);
		}
		return elementList;
	}

	/**
	 * Sets the id of this model (only to be used when creating a new model)
	 * 
	 * @param id
	 */
	public void setId(ModelId id)
	{
		if (Misc.ASSERTIONS)
			Misc.assertion(this.id == null); // Only to be used when creating a new model
		this.id = id;
		markModified();
	}

	/**
	 * Changes the id of this model (only to be used when renaming a model)
	 * 
	 * @param id
	 */
	public void resetId(ModelId id)
	{
		ModelId oldId = this.id;
		if (Misc.ASSERTIONS)
			Misc.assertion(this.id != null); // Only to be used when renaming a model
		this.id = id;
		firePropertyChange(BuiltinProperties.ID, oldId, id);
		firePropertyChange(BuiltinProperties.NAME, oldId.getName(), id.getName());
		markModified();
	}

	public String getName()
	{
		return id.getName();
	}

	public String getModelName()
	{
		return id.getName();
	}

	public ModelElement getElement(Role role)
	{
		for (ModelElement element : elements)
		{
			if (element != null && element.getRole() == role)
				return element;
		}
		return null;
	}

	public List<Path> findSubFlow(Role role, int maxDepth) // Like getElement(), but: [1] only for sub-flows; [2] returns all occurrences, including indirect children
	{
		List<Path> descendants = new ArrayList<Path>();

		// Direct child (maximum 1)
		if (getElement(role) != null)
			descendants.add(new Path(role));

		// Indirect children (breadth first)
		if (maxDepth > 1)
		{
			for (ModelElement element : elements)
			{
				if (element != null && element instanceof SubFlow)
				{
					Model model = element.getReferredModel();
					if (model != null) // Not good enough - how do we prevent an infinite loop?
					{
						List<Path> childDescendants = model.findSubFlow(role, maxDepth - 1);
						for (Path path : childDescendants)
							if (path != null)
							{
								Path fullPath = new Path(element.getRole());
								fullPath.append(path);
								descendants.add(fullPath);
							}
					}
				}
			}
		}

		return (descendants); // Better return null if empty list?
	}

	public ModelElement getElement(Path path)
	{
		if (Misc.ASSERTIONS)
			Misc.assertion(path.getNumberOfSegments() >= 1);
		return getElement(path, 0);
	}

	private ModelElement getElement(Path path, int offset)
	{
		ModelElement element = getElement(path.getSegment(offset));
		if (element == null)
			return null;
		if (path.getNumberOfSegments() == offset + 1)
			return element;
		else
		{
			// TODO: Slots should not be traversed
			Model referredModel = element.getReferredModel();
			if (referredModel == null)
				return null;
			else
				return referredModel.getElement(path, offset + 1);
		}
	}

	public ModelElement getElementByReferredModel(ModelId refId)
	{
		ModelElement found = null;
		for (ModelElement element : elements)
		{
			if (refId.equals(element.getRefId()))
				if (found == null)
					found = element;
				else
					return null; // Not unique
		}
		return found;
	}

	public ModelElement getElementByPlugin(String plugin)
	{
		ModelElement found = null;
		for (ModelElement element : elements)
		{
			Model referredModel = element.getReferredModel();
			if (referredModel != null)
				if (plugin.equals(referredModel.getPlugin()))
					if (found == null)
						found = element;
					else
						return null; // Not unique
		}
		return found;
	}

	public Link getLinkFrom(Path path) // Need to decide what to do if there are several (currently returns the first found)
	{
		for (ModelElement element : elements)
		{
			if (element instanceof Link)
				if (path.isEquivalent(((Link) element).getSource()))
					return (Link) element;
		}
		return null;
	}

	public Link getLinkFrom(Path path, Path targetPrefix, Role targetRole) // Need to decide what to do if there are several (currently returns the first found)
	{
		for (ModelElement element : elements)
		{
			if (element instanceof Link)
				if (path.isEquivalent(((Link) element).getSource()))
				{
					Path linkTargetPath = ((Link) element).getTarget();
					if ((targetPrefix == null || linkTargetPath.beginsWith(targetPrefix)) &&
						(targetRole == null	|| targetRole.equals(linkTargetPath.getLastSegment())))
						return (Link) element;
				}
		}
		return null;
	}

	public Link getLinkTo(Path path) // Need to decide what to do if there are several (currently returns the first found)
	{
		for (ModelElement element : elements)
		{
			if (element instanceof Link)
				if (path.isEquivalent(((Link) element).getTarget()))
					return (Link) element;
		}
		return null;
	}

	public Link getLinkTo(Path targetPrefix, Role targetRole) // Need to decide what to do if there are several (currently returns the first found)
	{
		for (ModelElement element : elements)
		{
			if (element instanceof Link)
			{
				Path linkTargetPath = ((Link) element).getTarget();
				if ((targetPrefix == null || linkTargetPath.beginsWith(targetPrefix)) &&
					(targetRole == null	|| targetRole.equals(linkTargetPath.getLastSegment())))
					return (Link) element;
			}
		}
		return null;
	}

	public Link getLinkBetween(Path fromPath, Path toPath) // Need to decide what to do if there are several (currently returns the first found)
	{
		for (ModelElement element : elements)
		{
			if (element instanceof Link)
				if (fromPath.isEquivalent(((Link) element).getSource())
						&& toPath.isEquivalent(((Link) element).getTarget()))
					return (Link) element;
		}
		return null;
	}

	public List<Link> getLinksBetween(Path sourcePrefix, Path targetPrefix)
	{
		List<Link> links = new ArrayList<Link>();

		for (ModelElement element : elements)
		{
			if (element instanceof Link)
				if (((Link) element).getSource().beginsWith(sourcePrefix)
						&& ((Link) element).getTarget().beginsWith(targetPrefix))
					links.add((Link) element);
		}
		return links; // Better return null if empty list?
	}

	/**
	 * Suggest a role for a model element to be added to the model (based on an input initial
	 * suggestion)
	 * 
	 * @param suggestedRole
	 */
	public Role defaultRole(String suggestedRole)
	{
		if (suggestedRole == null)
			throw new IllegalArgumentException("Suggested role musn't be null");
		Role role = Role.get(suggestedRole);

		int inc = 1;
		while (getElement(role) != null) // Already exists
		{
			role = Role.get(suggestedRole + " " + ++inc);
		}

		return (role);
	}

	public boolean isModified()
	{
		return modified;
	}

	private void setModified(boolean b)
	{
		if (modified != b)
		{
			modified = b;
			if (modified && getRepository() != null)
				getRepository().modelObjectChanged(this);
		}
	}

	public void markModified()
	{
		setModified(true);
		if (modified && getPackage() != null)
			getPackage().setModified();
	}

	public void clearModified()
	{
		setModified(false);
	}

	/**
	 * @param child
	 */
	public void removeElement(ModelElement child)
	{
		elements.remove(child);
		child.setParentModel(null);
		markModified();
		fireStructureChanged(BuiltinProperties.ELEMENTS);
	}

	/**
	 * @param child
	 */
	public int indexOf(ModelElement child)
	{
		return elements.indexOf(child);
	}

	protected String plugin;
	private Package pkg;
	private PluginDescriptor pluginDescriptor;

	public String getPlugin()
	{
		return plugin;
	}

	/**
	 * Associate a plugin class with this model
	 * 
	 * @param plugin
	 *            The fully qualified name of the plugin class
	 */
	public void setPlugin(String plugin)
	{
		String oldValue = this.plugin;
		String oldIconFolder = getIconFolder();
		
		if (oldValue == null && plugin != null || oldValue != null && !oldValue.equals(plugin))
		{
			this.plugin = plugin;
			pluginDescriptor = null;
			markModified();
			firePropertyChange(BuiltinProperties.PLUGIN, oldValue, plugin);
			if (this.iconFolder == null)
				firePropertyChange(BuiltinProperties.ICON_FOLDER, oldIconFolder, getIconFolder());
		}
	}

	public PluginDescriptor getPluginDescriptor()
	{
		if (pluginDescriptor == null && getPlugin() != null)
		{
			if (getRepository() != null)
				pluginDescriptor = getRepository().getPluginDescriptor(getPlugin());
		}
		return pluginDescriptor;

	}

	public String getPluginJavaClass()
	{
		if (plugin == null)
			return null;
		if (plugin.indexOf("/") == -1)
			return getPlugin();

		PluginDescriptor pd = getPluginDescriptor();
		if (pd == null)
			return null;
		return pd.getJavaClass();

	}

	/**
	 * 
	 * @return the name of the javascript class (i.e. constructor) used to create client side
	 *         instances of this model.
	 * 
	 *         DO NOT DELETE THIS METHOD: It is called indirectly by the Javascript generation
	 *         mechanism (Constructors.vsl)
	 */
	public String getPluginJavascript()
	{
		String js = (String) getProperty(BuiltinProperties.JAVASCRIPT_CONSTRUCTOR);
		if (js == null)
		{
			PluginDescriptor pd = getPluginDescriptor();
			if (pd != null)
			{
				String s = pd.getJavascript();
				if (s != null)
				{
					int version = getPluginVersion();
					// If there are multiple options for the constructor we use the last one whose version is not greater than the model's version
					// This allows for matching Dialog2 with no plugin version but Dialog3 for version >= 3
					for (String c: s.split(","))
					{
						if (js == null || getIntSuffix(c)<= version)
							js = c;
					}
				}
			}
		}
		return js;

	}
	/**
	 * @param s
	 * @return The integer suffix of a non-numeric string, or 0 if the string does not end with an integer
	 */
	private static int getIntSuffix(String s)
	{
		Matcher m = Pattern.compile("[^0-9]([0-9]+)").matcher(s);
		if (m.find())
			return Integer.parseInt(m.group(1));
		else
			return 0;
	}
	public Package getPackage()
	{
		return pkg;
	}

	void setPackage(Package pkg)
	{
		this.pkg = pkg;
	}

	public String getIconFolder()
	{
		if (iconFolder != null)
			return iconFolder;
		PluginDescriptor pd = getPluginDescriptor();
		if (pd != null)
			return pd.getIconFolder();
		else
			return null;
	}

	public String getDocumentation()
	{
		if (documentation != null)
			return documentation;
		PluginDescriptor pd = getPluginDescriptor();
		if (pd != null)
			return pd.getDocumentation();
		else
			return null;
	}

	public boolean isIconFolderSet()
	{
		return iconFolder != null;
	}

	public void setIconFolder(String iconFolder)
	{
		String oldValue = getIconFolder();
		this.iconFolder = iconFolder;
		markModified();
		firePropertyChange(BuiltinProperties.ICON_FOLDER, oldValue, getIconFolder());
	}

	public boolean isDocumentationSet()
	{
		return documentation != null;
	}

	public void setDocumentation(String documentation)
	{
		String oldValue = getDocumentation();
		this.documentation = documentation;
		markModified();
		firePropertyChange(BuiltinProperties.DOCUMENTATION, oldValue, getDocumentation());
	}

	public String getPrototype()
	{
		if (prototype != null)
			return prototype;
		PluginDescriptor pd = getPluginDescriptor();
		if (pd != null)
			return pd.getPrototype();
		else
			return null;
	}

	public boolean isPrototypeSet()
	{
		return prototype != null;
	}

	public void setPrototype(String prototype)
	{
		String oldValue = getPrototype();
		this.prototype = prototype;
		markModified();
		firePropertyChange(BuiltinProperties.ICON_FOLDER, oldValue, getIconFolder());
	}
	
	static public void modelWarning(ModelId id, ElementPath path, String problemType,
			String problemDetails)
	{
		throw new NotImplementedException("Model warnings not implemented yet");
	}

	static public void notifyInvalidModel(ModelId id, ElementPath path, String problemType,
			String problemDetails)
	{
		// Currently, always throwing an exception.
		// In the future we'll act differently when used in validation mode
		// (to allow notification of multiple errors).
		throw new InvalidModelException(id, path, problemType, problemDetails);
	}

	public boolean isReadOnly()
	{
		return getPackage().isReadOnly();
	}

	public boolean isModule()
	{
		return Modularity.MODULE.equals(getProperty(BuiltinProperties.MODULARITY));
	}

	public boolean checkPluginVersion(int version)
	{
		return getPluginVersion() >= version;
	}

	protected int getPluginVersion()
	{
		String vs = (String) getProperty(BuiltinProperties.PLUGIN_VERSION);
		int version = 0;
		if (vs != null)
			version = Integer.parseInt(vs);
		return version;
	}

	public HashSet<Slot> getAllNonDistinguishedTriggers()
	{
		HashSet<Slot> nonDistinguishedTriggers = new HashSet<Slot>();

		for (ModelElement element : getElements())
		{
			if (element instanceof Slot && ((Slot) element).getType().equals(SlotType.TRIGGER)
					&& !element.getRole().isDistinguished())
				nonDistinguishedTriggers.add((Slot) element);
		}

		return nonDistinguishedTriggers;
	}

	public HashSet<Slot> getAllNonDistinguishedExits()
	{
		HashSet<Slot> nonDistinguishedExits = new HashSet<Slot>();

		for (ModelElement element : getElements())
		{
			if (element instanceof Slot && ((Slot) element).getType().equals(SlotType.EXIT)
					&& !element.getRole().isDistinguished())
				nonDistinguishedExits.add((Slot) element);
		}

		return nonDistinguishedExits;
	}
}

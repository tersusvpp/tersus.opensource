/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model;

/**
 * @author Youval Bronicki
 */
public class MissingModelException extends ModelException
{
    public MissingModelException(String message)
    {
        super(message);
    }

    public MissingModelException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public MissingModelException(Throwable cause)
    {
        super(cause);
    }
}

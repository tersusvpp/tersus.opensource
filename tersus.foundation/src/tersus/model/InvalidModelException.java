/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;


/**
 * InvalidModelException is thrown when the runtime engine encounters an
 *  error that arises from an invalid model.  This exception is also thrown when 
 * a model or an element is not found (assuming the origin of the error is an invalid 
 * model)
 * 
 * @author Youval Bronicki
 *
 */
public class InvalidModelException extends RuntimeException
{

    /* The id of the model in which a problem was detected*/
	private ModelId id; 
	/* The relative path of the element in which a problem was detected */
    private ElementPath path; 
    /* A textual description of the problem */
    private String problem;
    
    /**
     * @return Returns the problemDetails.
     */
    public String getDetails()
    {
        return details;
    }
    private String details;

    /**
     * @deprecated Invalid Model Exception should be created using the more structured constructor
     * InvalidModelException(ModelId, ElementPath, String)
     * @param message
     */
    public InvalidModelException(String message)
    {
        super(message);
    }

    /**
     * Expected to be used only by Model.notifyInvalidModel()
     */
	public InvalidModelException(ModelId id, ElementPath path, String problemType, String problemDetails)
	{
		super(problemType + " : " + problemDetails + " [Model Id: "+id + " Path:"+path);
		this.id = id;
		this.path = path;
		this.problem = problemType;
		this.details = problemDetails;
	}

    /**
     * @return Returns the id.
     */
    public ModelId getId()
    {
        return id;
    }
    /**
     * @return Returns the path.
     */
    public ElementPath getPath()
    {
        if (path != null)
            return path;
        else
            return Path.EMPTY;
    }
    /**
     * @return Returns the problem.
     */
    public String getProblem()
    {
        if (problem != null)
            return problem;
        else
            return getMessage();
    }
}

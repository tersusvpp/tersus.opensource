/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import tersus.util.IObservableItem;

public class TersusTableFieldDescriptor implements IObservableItem
{
    private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    private ModelElement element;
    
    // Table Fields
    
    private String elementName;

    private String columnName;
    
    private String columnDescription;
    
    private ModelId dataType;
    
    private String columnType;
    
    private Boolean primaryKey;
    
    private Boolean nullable;
    
    private String columnSize;
    
    private String status;

    private String defaultValue;
    
    private int position;
    
    public TersusTableFieldDescriptor(ModelElement element, String elementName, String columnName, ModelId dataType, 
            String columnType, Boolean primaryKey, Boolean nullable, String columnSize, String defaultValue, String columnDescription, String status, int position)
    {
    	this.element = element;
        this.elementName = elementName;
        this.columnName = columnName;
        this.dataType = dataType;
        this.columnType = columnType;
        this.primaryKey = primaryKey;
        this.nullable = nullable;
        this.columnSize = columnSize;
        this.defaultValue = defaultValue;
        this.status = status;
        this.columnDescription = columnDescription;
        this.position = position;
    }
    
    public int getPosition()
	{
		return position;
	}

	public void setPosition(int position)
	{
		this.position = position;
	}

	public void addListener(PropertyChangeListener listener)
    {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removeListener(PropertyChangeListener listener)
    {
        propertyChangeSupport.removePropertyChangeListener(listener);  
    }
    
    public void setElementName(String newValue)
    {
        String oldValue = getElementName();
        this.elementName = newValue;
        propertyChangeSupport.firePropertyChange("Name", oldValue, newValue);
    }

    public String getElementName()
    {
        return elementName;
    }
    
    
    public void setColumnName(String newValue)
    {
        String oldValue = getElementName();
        this.columnName = newValue;
        propertyChangeSupport.firePropertyChange("Column Name", oldValue, newValue);
    }

    public String getColumnName()
    {
        return columnName;
    }
    
    public void setDataType(ModelId dataType)
    {
        ModelId oldValue = this.dataType;
        this.dataType = dataType;
        
        if (oldValue != dataType)
            propertyChangeSupport.firePropertyChange("Data Type", oldValue, dataType);   
    }

    public ModelId getDataType()
    {
        return dataType;
    }
   
    public void setColumnType(String columnType)
    {
    	String oldValue = this.columnType;
        this.columnType = columnType;
        
        if (!oldValue.equals(columnType))
        	propertyChangeSupport.firePropertyChange("Column Type", oldValue, columnType);
    }
    
    public String getColumnType()
    {
        return columnType;
    }
    
    public void setPrimaryKey(Boolean newValue)
    {
        Boolean oldValue = getPrimaryKey();
        if (primaryKey != newValue)
        {
            this.primaryKey = newValue;
            propertyChangeSupport.firePropertyChange("Primary Key", oldValue, newValue);
        }
    }
    
    public Boolean getPrimaryKey()
    {
        return primaryKey;
    }
    
    public void setColumnSize(String columnSize)
    {
    	String oldValue = this.columnSize;
        this.columnSize = columnSize;
        
        if (!oldValue.equals(columnSize))
        	propertyChangeSupport.firePropertyChange("Column Size", oldValue, columnSize);
    }
    
    public String getColumnSize()
    {
        return this.columnSize;
    }
    
    public void setStatus(String status)
    {
    	String oldValue = this.status;
        this.status= status;
        
        if (oldValue != status)
        	propertyChangeSupport.firePropertyChange("Status", oldValue, status);
    }
    
    public String getStatus()
    {
        return status;
    }
    
    public ModelElement getElement()
    {
    	return element;
    }

    public void setNullable(Boolean nullable)
    {
    	Boolean oldValue = this.nullable;
        this.nullable = nullable;
        
        if (oldValue != nullable)
        	propertyChangeSupport.firePropertyChange("Nullable", oldValue, status);
    }

    public Boolean getNullable()
    {
    	if (nullable == null)
    		return false;
    	
        return nullable;
    }

    public String getDefaultValue()
    {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue)
    {
    	String oldValue = this.defaultValue;
        this.defaultValue = defaultValue;
        
        if (oldValue != defaultValue)
        	propertyChangeSupport.firePropertyChange("Default Value", oldValue, status);
    }
    
    public String getColumnDescription()
	{
    	if (columnDescription == null)
    		return "";
    	
		return columnDescription;
	}
    
    public void setColumnDescription(String columnDescription)
    {
    	this.columnDescription = columnDescription;
    }
}

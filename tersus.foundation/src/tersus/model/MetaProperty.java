/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;

import tersus.util.EnumFactory;
import tersus.util.Misc;


/**
 * Represents a property a class of models or model elements (meta-meta-data)
 */
public class MetaProperty
{
	public String name;
	public String displayName;
	public Class<?> valueClass;
	
	private boolean canEdit;
    private boolean canDelete;
    private boolean canDisplay = true;
    private boolean dualProperty;

	public MetaProperty(
	        String name, Class<?> valueClass, boolean canEdit, boolean canDelete, boolean dualProperty)
	{
		this.name = name;
		this.displayName = name;
		this.valueClass = valueClass;
		
		this.canEdit = canEdit;
		this.canDelete = canDelete;
		this.dualProperty = dualProperty;
	}
	
	public MetaProperty(
            String name, String displayName, Class<?> valueClass, boolean canEdit, boolean canDelete, boolean dualProperty)
    {
        this.name = name;
        this.displayName = displayName;
        this.valueClass = valueClass;
        
        this.canEdit = canEdit;
        this.canDelete = canDelete;
        this.dualProperty = dualProperty;
    }
	
	public MetaProperty(
            String name, Class<?> valueClass, boolean canEdit, boolean canDelete, boolean dualProperty, boolean canDisplay)
    {
        this.name = name;
        this.displayName = name;
        this.valueClass = valueClass;
        
        this.canEdit = canEdit;
        this.canDelete = canDelete;
        this.canDisplay = canDisplay;
        this.dualProperty = dualProperty;
    }
	
	/**
	 * Parse a String represantation of a value of the property
	 * @param s The string representation
	 * @return The result of parsing (an instance of the property's value class)
	 */
	public Object parseValue(String s)
	{
		
		if (valueClass.equals(String.class))
			return s;
		EnumFactory factory = EnumFactory.getFactory(valueClass);
		if (factory != null)
			return factory.getValue(s);
		else if (valueClass.equals(ModelId.class))
			return new ModelId(s);
		else if (valueClass.equals(Role.class))
			return Role.get(s);
		else if (valueClass.equals(Boolean.class))
			return Boolean.valueOf(s);
		else if (valueClass.equals(RelativePosition.class))
			return RelativePosition.valueOf(s);
		else if (valueClass.equals(RelativeSize.class))
			return RelativeSize.valueOf(s);
		else if (valueClass.equals(Double.class))
			return new Double(Double.parseDouble(s));
		else if (valueClass.equals(Path.class))
			return new Path(s);
		if (Misc.ASSERTIONS) Misc.assertion(false); // Unsupported property type							
		return null;
	}
	
	public boolean canEdit ()
	{
	    return canEdit;
	}
	
	public boolean canDelete ()
    {
        return canDelete;
    }
	
	public boolean isdualProperty ()
    {
        return dualProperty;
    }
	
	public String getDisplayName()
	{
	    return displayName;
	}
	
	public String getName()
	{
	    return name;
	}
	
	public boolean canDisplay ()
    {
        return canDisplay;
    }
}

/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model.indexer;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import tersus.model.FileRepository;
import tersus.model.Package;
import tersus.model.PackageId;
import tersus.model.Repository;

public class RepostitoryPerformanceTest
{

    public static void main(String[] args)
    {
        FileRepository repository = new FileRepository();
        repository.setRoot(new File(args[0]));
        start = System.currentTimeMillis();
        last = System.currentTimeMillis();
        int numberOfModels = getAllModels(repository).size();
        message("Loaded " + numberOfModels + " models");
        System.gc();
        message("GC (1)");
        System.gc();
        message("GC (2)");
    }

    public static List getAllPackages(Repository repository)
    {
        return repository.getPackageHierarchy(null);
    }

    private static long start = System.currentTimeMillis();

    private static long last = System.currentTimeMillis();

    private static void message(String msg)
    {
        long time = System.currentTimeMillis();

        System.out
                .println((time - start) + " (" + (time - last) + ") : " + msg);
        final Runtime runtime = Runtime.getRuntime();
        System.out
                .println("Memory usage: "
                        + (runtime.totalMemory() - runtime.freeMemory()) / 1024
                        + " KB");
        last = time;
    }

    public static List getAllModels(Repository repository)
    {
        List models = null;
        List allPackages = getAllPackages(repository);
        message("Loading "+allPackages.size()+ " packages");
        int counter = 0;
        for (Iterator iter = allPackages.iterator(); iter.hasNext();)
        {
            ++ counter;
            if (counter > 1000)
            {
                System.out.println();
                return models;
            }
            if (counter%10 == 0)
            {
                    System.out.print('.');
                    System.out.flush();
            }
            PackageId packageId = (PackageId) iter.next();
            Package pkg = repository.getPackage(packageId);
            if (pkg != null)
            {
                List packageModels = pkg.getModels();

                if (models == null)
                    models = new ArrayList();
                models.addAll(packageModels);
            }
        }
        System.out.println("");
        return (models);
    }

}

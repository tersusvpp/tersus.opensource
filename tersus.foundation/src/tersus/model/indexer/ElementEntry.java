/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model.indexer;

import tersus.model.ModelId;
import tersus.model.Path;
import tersus.model.Role;


public class ElementEntry
{
    private static final int IS_DATA_ELEMENT=1;
    private static final int IS_SUB_FLOW=2;
    private static final int IS_REPETITIVE=4;
    private static final int IS_SLOT=8;
    private static final int IS_LINK=16;
    
    private ModelId parentId;
    private ModelId refId;
    private Role role;
    private int flags;
    private Path source, target;
    
    public boolean isRepetitive()
    {
        return getFlag(IS_REPETITIVE);
    }
    void setRepetitive(boolean v)
    {
        setFlag(v, IS_REPETITIVE);
    }
    public boolean isDataElemnt()
    {
        return getFlag(IS_DATA_ELEMENT);
    }
    void setDataElemnt(boolean v)
    {
        setFlag(v, IS_DATA_ELEMENT);
    }
    public boolean isSlot()
    {
        return getFlag(IS_SLOT);
    }
    public void setSlot(boolean v)
    {
        setFlag(v, IS_SLOT);
    }
    public boolean isLink()
    {
        return getFlag(IS_LINK);
    }
    public void setLink(boolean v)
    {
        setFlag(v, IS_LINK);
    }
    
    public boolean isSubflow()
    {
        return getFlag(IS_SUB_FLOW);
    }
    void setSubFlow(boolean v)
    {
        setFlag(v, IS_SUB_FLOW);
    }
    protected boolean getFlag(final int flag)
    {
        return flag == (flag & flags);
    }
    protected void setFlag(boolean v, final int mask)
    {
        if (v)
            flags |= mask;
        else
            flags &= (~mask);
    }
    public ModelId getRefId()
    {
        return refId;
    }
    void setRefId(ModelId refId)
    {
        this.refId = refId;
    }
    public Role getRole()
    {
        return role;
    }
    void setRole(Role role)
    {
        this.role = role;
    }
    public ModelId getParentId()
    {
        return parentId;
    }
    public void setParentId(ModelId parentId)
    {
        this.parentId = parentId;
    }
    public Path getSource()
    {
        return source;
    }
    public void setSource(Path source)
    {
        this.source = source;
    }
    public Path getTarget()
    {
        return target;
    }
    public void setTarget(Path target)
    {
        this.target = target;
    }

}

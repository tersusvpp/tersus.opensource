/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.- Initial API and implementation
 *************************************************************************************************/
package tersus.model.indexer;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.mutable.MutableInt;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import tersus.model.BuiltinProperties;
import tersus.model.DataElement;
import tersus.model.InvalidRepositoryException;
import tersus.model.Link;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelException;
import tersus.model.ModelId;
import tersus.model.Modularity;
import tersus.model.Note;
import tersus.model.Package;
import tersus.model.PackageId;
import tersus.model.Path;
import tersus.model.PluginDescriptor;
import tersus.model.Repository;
import tersus.model.Role;
import tersus.model.Slot;
import tersus.model.SlotType;
import tersus.model.SubFlow;
import tersus.util.IProgressMonitor;
import tersus.util.Misc;

public class RepositoryIndex
{
	/**
	 * A memory-efficient multi-map implementation for element entries Avoids creating lists or
	 * Map.Entry instances
	 */
	private class ElementMap
	{
		private ElementEntry[] elementsTable;

		private int size;

		public void init()
		{
			size = elements.size() * 3;
			elementsTable = new ElementEntry[size];
			for (int i = 0; i < elements.size(); i++)
			{
				addEntry(elements.get(i));
			}
		}

		private void addEntry(ElementEntry entry)
		{
			int idx = Math.abs(entry.getParentId().hashCode() % size);
			while (elementsTable[idx] != null)
			{
				++idx;
				if (idx == size)
					idx = 0;
			}
			elementsTable[idx] = entry;
		}

		public List<ElementEntry> getElementEntries(ModelId parentId)
		{
			List<ElementEntry> elements = Collections.emptyList();
			int idx = Math.abs(parentId.hashCode() % size);
			while (elementsTable[idx] != null)
			{
				if (elementsTable[idx].getParentId().equals(parentId))
				{
					if (elements.isEmpty())
						elements = new ArrayList<ElementEntry>();
					elements.add(elementsTable[idx]);
				}
				++idx;
				if (idx == size)
					idx = 0;
			}
			return elements;
		}

		public ElementEntry getElementEntry(ModelId parentId, Role role)
		{
			int idx = Math.abs(parentId.hashCode() % size);
			while (elementsTable[idx] != null)
			{
				if (elementsTable[idx].getParentId().equals(parentId)
						&& elementsTable[idx].getRole() == role)
					return elementsTable[idx];
				++idx;
				if (idx == size)
					idx = 0;
			}
			return null;

		}
	}

	/**
	 * A memory-efficient multi-map implementation for looking up references to an id Avoids
	 * creating Map.Entry instances but does creates Lists
	 */
	private class ReferenceMap
	{
		private Object[] referncesTable;

		private int size;

		public void init()
		{
			size = getAllModelIds().size() * 2;
			referncesTable = new Object[size];
			for (ElementEntry entry: elements)
			{
				addEntry(entry);
			}
		}

		private void addEntry(ElementEntry entry)
		{
			ModelId refId = entry.getRefId();
			if (refId != null)
			{
				int idx = Math.abs(refId.hashCode() % size);

				while(true)
				{
					if (referncesTable[idx] == null)
					{
						referncesTable[idx] = entry;
						return;
					}	
					else if (referncesTable[idx] instanceof ElementEntry)
					{
						ElementEntry temp = (ElementEntry) referncesTable[idx];
						
						if (temp.getRefId().equals(entry.getRefId()))
						{
							referncesTable[idx] = null;
							referncesTable[idx] = new ArrayList<ElementEntry>();
							ArrayList<ElementEntry> list = ((ArrayList<ElementEntry>) referncesTable[idx]);
							list.add(temp);
							list.add(entry);
							return;
						}
						else
						{
							idx++;
							if (idx == size)
								idx = 0;
						}
						
					}
					else
					{
						ArrayList<ElementEntry> list = ((ArrayList<ElementEntry>) referncesTable[idx]);
						if (list.get(0).getRefId().equals(entry.getRefId()))
						{
							list.add(entry);
							return;
						}
						else
						{
							idx++;
							if (idx == size)
								idx = 0;
						}
					}
				}
			}
		}

		public List<ElementEntry> getReferences(ModelId refId)
		{
			List<ElementEntry> elements = new ArrayList<ElementEntry>();
			int idx = Math.abs(refId.hashCode() % size);

			while(true)
			{
				Object objectEntry = referncesTable[idx];
				
				if (objectEntry == null)
					return Collections.EMPTY_LIST;
				else if (objectEntry instanceof ElementEntry)
				{
					if (((ElementEntry)objectEntry).getRefId().equals(refId))
					{
						elements.add((ElementEntry) objectEntry);
						return elements;
					}
					
					idx++;
					if (idx == size)
						idx = 0;
				}
				else
				{
					ArrayList<ElementEntry> elementsEntries = (ArrayList<ElementEntry>) objectEntry;
					
					if (elementsEntries.get(0).getRefId().equals(refId))
					{
						for (ElementEntry e: elementsEntries)
						{
							elements.add(e);
						}
						return elements;
					}
					else
					{
						idx++;
						if (idx == size)
							idx = 0;
					}
				}
			}
		}

		public ElementEntry getElementEntry(ModelId refId, Role role)
		{
			int idx = Math.abs(refId.hashCode() % size);
			
			while (true)
			{
				Object objectEntry = referncesTable[idx];
				
				if (objectEntry instanceof ElementEntry)
				{
					ElementEntry entry = (ElementEntry)objectEntry;
					if (entry.getRefId().equals(refId) && entry.getRole() == role)
						return entry;
				}
				else if (objectEntry == null)
					return null;
				else
				{
					ArrayList<ElementEntry> elementsEntries = (ArrayList<ElementEntry>) objectEntry;
					
					if (elementsEntries.get(0).getRefId().equals(refId))
					{
						for (ElementEntry entry: (ArrayList<ElementEntry>)objectEntry)
						{
							if (entry.getRole() == role)
								return entry;
						}
					}
					else
					{
						idx++;
						if (idx == size)
							idx = 0;
					}
				}
			}
		}
	}

	private Map<ModelId, ModelEntry> modelEntries = new HashMap<ModelId, ModelEntry>();

	private ElementMap elementMap = new ElementMap();

	private ReferenceMap referenceMap = new ReferenceMap();

	private Map<ModelId, String> permissionMap = new HashMap<ModelId, String>();

	private HashSet<ModelId> dirtyModelIds = new HashSet<ModelId>();

	private ArrayList<ElementEntry> elements = new ArrayList<ElementEntry>();

	private Repository repository;

	protected void addElementEntry(ElementEntry e)
	{
		if (e.getRole() == null)
			throw new ModelException("Element with null role");
		elements.add(e);
	}

	public void buildIndex(Repository repository, IProgressMonitor monitor)
	{
		try
		{
			this.repository = repository;
			List<PackageId> packageIds = repository.getPackageHierarchy(null);
			monitor.beginTask("Building repository index", packageIds.size() + 1);
			long size = 0;
			for (PackageId packageId : packageIds)
			{
				Package pkg;
				if ((pkg = repository.getPackageFromCache(packageId)) != null)
				{
					indexCachedPackage(pkg);
				}
				else
				{
					size = indexSavedPackage(repository, size, packageId);
				}
				monitor.worked(1);

			}
			dirtyModelIds.clear();
			elementMap.init();
			referenceMap.init();
			monitor.done();
		}
		catch (Exception e)
		{
			throw new ModelException("Failed to create repository index", e);
		}
	}

	private void indexCachedPackage(Package pkg)
	{
		final List<Model> models = pkg.getModels();
		boolean isLibrary = pkg.isLibrary();
		for (int i = 0; i < models.size(); i++)
		{
			indexCachedModel((Model) models.get(i), isLibrary);
		}
	}

	private void indexCachedModel(Model model, boolean isLibrary)
	{
		ModelId id = model.getId();
		ModelEntry entry = new ModelEntry();
		entry.setId(id);
		entry.setDescriptor(model.getPluginDescriptor());
		String permission = (String) model.getProperty(BuiltinProperties.REQUIRED_PERMISSION);
		if (permission != null)
			permissionMap.put(id, permission);
		if (model.isModule())
			entry.setIsModule(true);
		if (isLibrary)
			entry.setIsLibrary(true);

		modelEntries.put(id, entry);
		List<ModelElement> elements = model.getElements();
		for (ModelElement e : elements)
		{
			if (e instanceof Link)
				addLink((Link) e);
			else if (e instanceof DataElement)
				addDataElement((DataElement) e);
			else if (e instanceof SubFlow)
				addSubFlow((SubFlow) e);
			else if (e instanceof Slot)
				addSlot((Slot) e);
			else if (e instanceof Note)
			{
				// ignore
			}
			else
				System.out.println("Unexpected Element " + e.getClass().getName());
		}
	}

	private ElementEntry addElement(ModelElement element)
	{
		ElementEntry e = new ElementEntry();
		e.setRole(element.getRole());
		e.setRefId(element.getRefId());
		e.setParentId(element.getParentModel().getId());
		addElementEntry(e);
		return e;
	}

	private void addSubFlow(SubFlow element)
	{
		ElementEntry e = addElement(element);
		e.setSubFlow(true);
	}

	private void addDataElement(DataElement element)
	{
		ElementEntry e = addElement(element);
		e.setDataElemnt(true);
	}

	private void addLink(Link element)
	{
		ElementEntry e = addElement(element);
		e.setLink(true);
		e.setSource(element.getSource());
		e.setTarget(element.getTarget());
	}

	private void addSlot(Slot element)
	{
		ElementEntry e = addElement(element);
		e.setSlot(true);
	}

	private long indexSavedPackage(Repository repository, long size, PackageId element)
			throws IOException, Exception
	{
		String path = repository.getPackageFilePath(element);
		boolean isLibrary = false;
		byte[] packageContent = repository.readLocalResource(path);
		if (packageContent == null)
		{
			packageContent = repository.readLibraryResource(path);
			isLibrary = true;
		}

		if (packageContent != null)
		{
			size += packageContent.length;
			try
			{
				parsePackage(element, packageContent, isLibrary);
			}
			catch (Exception e)
			{
				throw new ModelException("Failed to parse package " + element, e);
			}
		}
		return size;
	}

	private void parsePackage(PackageId element, byte[] packageContent, boolean isLibrary)
			throws Exception
	{
		XMLReader xr = XMLReaderFactory.createXMLReader();
		final SaxHandler saxHandler = new SaxHandler();
		saxHandler.isLibrary = isLibrary;
		xr.setContentHandler(saxHandler);
		xr.setErrorHandler(saxHandler);
		try
		{
			xr.parse(new InputSource(new ByteArrayInputStream(packageContent)));
		}
		catch (SAXException e)
		{
			if (e.getException() != null)
				e.getException().printStackTrace();
			else
				e.printStackTrace();
			throw new InvalidRepositoryException("Malformed XML in pacakge "+element.getPath(),e);
		}

	}

	private class SaxHandler extends DefaultHandler
	{
		ModelEntry currentModelEntry = null;

		String currentModelTag = null;
		boolean isLibrary = false;

		public void startElement(String uri, String localName, String qName, Attributes attributes)
				throws SAXException
		{
			if (localName.endsWith("Model") || localName.endsWith("Type"))
			{
				ModelId id = repository.getModelId(attributes.getValue("id"));
				ModelEntry entry = new ModelEntry();
				entry.setId(id);
				String plugin = attributes.getValue(BuiltinProperties.PLUGIN);
				if (plugin != null)
					entry.setDescriptor(repository.getPluginDescriptor(plugin));
				modelEntries.put(id, entry);
				currentModelEntry = entry;
				currentModelTag = localName;
				String permission = attributes.getValue(BuiltinProperties.REQUIRED_PERMISSION);
				if (permission != null)
					permissionMap.put(id, permission);
				if (Modularity.MODULE.equals(attributes.getValue(BuiltinProperties.MODULARITY)))
					entry.setIsModule(true);
				if (isLibrary)
					entry.setIsLibrary(true);

			}
			else if (localName.equals("Link"))
				addLink(attributes);
			else if (localName.equals("DataElement"))
				addDataElement(attributes);
			else if (localName.equals("SubProcess") || localName.equals("TopProcess")
					|| localName.equals("SubSystem"))
				addSubFlow(attributes);
			else if (localName.equals("Trigger") || localName.equals("Exit")
					|| localName.equals("ErrorExit"))
				addSlot(attributes);
			else if (localName.equals("Package") || localName.equals("Note"))
			{
				// Ignore
			}
			else
				System.out.println("Unknown Element " + localName);
		}

		private void addSlot(Attributes attributes)
		{
			ElementEntry e = addElement(attributes);
			e.setRefId(repository.getModelId(attributes.getValue(BuiltinProperties.DATA_TYPE_ID))); // For
			// slots
			// we have
			// another
			// name to
			// the ref
			// id
			// property

			e.setSlot(true);
		}

		private ElementEntry addElement(Attributes attributes)
		{
			ElementEntry e = new ElementEntry();
			e.setRole(Role.get(attributes.getValue(BuiltinProperties.ROLE)));
			e.setRefId(repository.getModelId(attributes.getValue(BuiltinProperties.REF_ID)));
			e.setParentId(currentModelEntry.getId());
			addElementEntry(e);
			return e;
		}

		private void addSubFlow(Attributes attributes)
		{
			ElementEntry e = addElement(attributes);
			e.setSubFlow(true);
		}

		private void addDataElement(Attributes attributes)
		{
			ElementEntry e = addElement(attributes);
			e.setDataElemnt(true);
		}

		private void addLink(Attributes attributes)
		{
			ElementEntry e = addElement(attributes);
			e.setLink(true);
			e.setSource(repository.getPath(attributes.getValue("source")));
			e.setTarget(repository.getPath(attributes.getValue("target")));
		}

		public void endElement(String uri, String localName, String qName) throws SAXException
		{
			if (localName.equals(currentModelTag))
				currentModelEntry = null;
		}

	}

	long start = 0;

	long last = 0;

	synchronized public void markDirty(ModelId id)
	{
		dirtyModelIds.add(id);
	}

	public boolean contains(ModelId id)
	{
		return modelEntries.containsKey(id);
	}

	synchronized public void update(IProgressMonitor monitor)
	{
		if (isDirty())
		{
			monitor.beginTask("Updating repository index", 3);
			removeDirtyModels();
			monitor.worked(1);
			reindexDirtyModels();
			monitor.worked(1);
			dirtyModelIds.clear();
			elementMap.init();
			referenceMap.init();
			monitor.done();
		}
	}

	public boolean isDirty()
	{
		return !dirtyModelIds.isEmpty();
	}

	public int countDirtyModels()
	{
		return dirtyModelIds.size();
	}

	private void reindexDirtyModels()
	{
		for (Iterator<ModelId> i = dirtyModelIds.iterator(); i.hasNext();)
		{
			ModelId id = i.next();
			Model model = repository.getModel(id, true);
			if (model != null)
				indexCachedModel(model, model.getPackage().isLibrary());
		}

	}

	private void removeDirtyModels()
	{
		ArrayList<ElementEntry> newElementEntries = new ArrayList<ElementEntry>(elements.size());
		for (ModelId id : dirtyModelIds)
		{
			modelEntries.remove(id);
			permissionMap.remove(id);
		}
		for (ElementEntry e : elements)
		{
			if (!dirtyModelIds.contains(e.getParentId()))
				newElementEntries.add(e);
		}
		this.elements = newElementEntries;
	}

	public List<ModelElement> getReferences(ModelId refId)
	{
		List<ModelElement> references = Collections.emptyList();
		for (int i = 0; i < elements.size(); i++)
		{
			ElementEntry e = elements.get(i);
			if (refId.equals(e.getRefId()))
			{
				if (references.isEmpty())
					references = new ArrayList<ModelElement>();
				references.add(getModelElement(e));
			}

		}
		return references;
	}

	public List<ModelId> getAncestorModels(ModelId refId, Set<ModelId> alreadyChecked)
	{
		List<ModelId> ancestors = Collections.emptyList();
		if (alreadyChecked == null)
			alreadyChecked = new HashSet<ModelId>();
		List<ElementEntry> references = getReferenceEntries(refId);
		for (int i = 0; i < references.size(); i++)
		{
			ModelId parent = references.get(i).getParentId();
			if (!alreadyChecked.contains(parent))
			{
				if (ancestors.isEmpty())
					ancestors = new ArrayList<ModelId>();
				ancestors.add(parent);
				alreadyChecked.add(parent);
				ancestors.addAll(getAncestorModels(parent, alreadyChecked));
			}
		}
		return ancestors;
	}

	public ModelElement getModelElement(ElementEntry e)
	{
		Model model = repository.getModel(e.getParentId(), true);
		final ModelElement element = model.getElement(e.getRole());
		return element;
	}

	public List<ModelElement> getReferences(PackageId packageId, boolean includeInternalReferences)
	{
		List<ModelElement> references = Collections.emptyList();
		for (int i = 0; i < elements.size(); i++)
		{
			ElementEntry e = elements.get(i);
			if (packageId.isAncestorOf(e.getRefId())
					&& (includeInternalReferences || !packageId.isAncestorOf(e.getParentId())))
			{
				if (references.isEmpty())
					references = new ArrayList<ModelElement>();
				references.add(getModelElement(e));
			}
		}
		return references;
	}

	public List<Link> getReferringLinks(ModelElement element)
	{
		List<Link> links = Collections.emptyList();

		for (int i = 0; i < elements.size(); i++)
		{
			ElementEntry e = elements.get(i);
			if (e.isLink())
			{
				if (pathContainsElement(e.getParentId(), e.getSource(), element, false)
						|| pathContainsElement(e.getParentId(), e.getTarget(), element, false))
				{
					if (links.isEmpty())
						links = new ArrayList<Link>();
					links.add((Link) getModelElement(e));
				}
			}
		}
		return links;
	}

	public ArrayList<Link> getOutgoingLinks(ModelElement element, boolean checkIsElementSource)
	{
		ArrayList<Link> links = new ArrayList<Link>();

		if (element instanceof Slot)
			links.addAll(getSlotOutgoingLinks((Slot) element));
		else
		{
			for (int i = 0; i < elements.size(); i++)
			{
				ElementEntry e = elements.get(i);
				if (e.isLink())
				{
					if (pathContainsElement(e.getParentId(), e.getSource(), element,
							checkIsElementSource))
						links.add((Link) getModelElement(e));
				}
			}
		}
		return links;
	}

	private ArrayList<Link> getSlotOutgoingLinks(Slot slot)
	{
		ArrayList<Link> links = new ArrayList<Link>();

		if (slot.getType().equals(SlotType.EXIT) || slot.getType().equals(SlotType.ERROR))
		{
			for (ElementEntry refernceEntry : getReferenceEntries(slot.getParentModel()
					.getModelId()))
			{
				ModelId parentId = refernceEntry.getParentId();
				for (ElementEntry elementEntry : getElementEntries(refernceEntry.getParentId()))
				{
					if (elementEntry.isLink()
							&& pathContainsElement(parentId, elementEntry.getSource(), slot, false))
						links.add((Link) getModelElement(elementEntry));
				}
			}
		}
		else if (slot.getType().equals(SlotType.TRIGGER))
		{
			ModelId parentId = slot.getParentModel().getModelId();
			for (ElementEntry elementEntry : getElementEntries(parentId))
			{
				if (elementEntry.isLink()
						&& pathContainsElement(parentId, elementEntry.getSource(), slot, false))
					links.add((Link) getModelElement(elementEntry));
			}
		}

		return links;
	}

	public ArrayList<Link> getIncomingLinks(ModelElement element, boolean checkIsElementTarget)
	{
		ArrayList<Link> links = new ArrayList<Link>();

		if (element instanceof Slot)
			links.addAll(getSlotIncomingLinks((Slot) element));
		else
		{
			for (int i = 0; i < elements.size(); i++)
			{
				ElementEntry e = elements.get(i);
				if (e.isLink())
				{
					if (pathContainsElement(e.getParentId(), e.getTarget(), element,
							checkIsElementTarget))
						links.add((Link) getModelElement(e));
				}
			}
		}
		return links;
	}

	private ArrayList<Link> getSlotIncomingLinks(Slot slot)
	{
		ArrayList<Link> links = new ArrayList<Link>();

		if (slot.getType().equals(SlotType.EXIT) || slot.getType().equals(SlotType.ERROR))
		{
			ModelId parentId = slot.getParentModel().getModelId();
			for (ElementEntry elementEntry : getElementEntries(parentId))
			{
				if (elementEntry.isLink()
						&& pathContainsElement(parentId, elementEntry.getTarget(), slot, false))
					links.add((Link) getModelElement(elementEntry));
			}
		}
		else if (slot.getType().equals(SlotType.TRIGGER))
		{
			for (ElementEntry refernceEntry : getReferenceEntries(slot.getParentModel()
					.getModelId()))
			{
				ModelId parentId = refernceEntry.getParentId();
				for (ElementEntry elementEntry : getElementEntries(refernceEntry.getParentId()))
				{
					if (elementEntry.isLink()
							&& pathContainsElement(parentId, elementEntry.getTarget(), slot, false))
						links.add((Link) getModelElement(elementEntry));
				}
			}
		}
		return links;
	}

	public boolean pathContainsElement(ModelId parentId, Path path, ModelElement element,
			boolean checkLastElement)
	{
		if (path == null)
			return false;
		if (!pathContainsRole(path, element.getRole()))
			return false;
		ModelId parentModelId = parentId;
		for (int i = 0; i < path.getNumberOfSegments(); i++)
		{
			Role segment = path.getSegment(i);
			ElementEntry childElement = getElementEntry(parentModelId, segment);
			if (childElement == null)
				return false; // Invlid path, but we can ignore it in this
			// case
			ModelId childModelId = childElement.getRefId();
			if (segment == element.getRole() && Misc.equal(element.getRefId(), childModelId))
			{
				if (parentModelId.equals(element.getParentModel().getId()))
				{
					if (checkLastElement)
						return i == path.getNumberOfSegments() - 1;
					else
						return true;
				}
			}

			parentModelId = childModelId;
		}
		return false;
	}

	private boolean pathContainsRole(Path path, Role role)
	{
		for (int i = 0; i < path.getNumberOfSegments(); i++)
		{
			if (role == path.getSegment(i))
				return true;
		}
		return false;
	}

	private ElementEntry getElementEntry(ModelId parentModelId, Role role)
	{
		return elementMap.getElementEntry(parentModelId, role);
	}

	public Collection<ModelId> getRootModelIds()
	{

		HashSet<ModelId> rootModelIds = new HashSet<ModelId>();
		for (Iterator<ModelEntry> i = modelEntries.values().iterator(); i.hasNext();)
		{
			rootModelIds.add(i.next().getId());
		}
		for (int i = 0; i < elements.size(); i++)
		{
			ElementEntry e = elements.get(i);
			if (e.getRefId() != null)
				rootModelIds.remove(e.getRefId());
		}
		return rootModelIds;
	}

	public Collection<ModelId> getAllModuleIds()
	{
		ArrayList<ModelId> moduleIds = new ArrayList<ModelId>();

		for (Iterator<ModelEntry> i = modelEntries.values().iterator(); i.hasNext();)
		{
			ModelEntry me = i.next();
			if (me.isModule())
				moduleIds.add(me.getId());
		}
		return moduleIds;

	}

	public Map<ModelId, String> getPermissionMap()
	{
		return permissionMap;
	}

	@SuppressWarnings("unchecked")
	public HashSet<ModelId> calculateUIModelIds(Model rootModel, Set<String> permissions)
	{
		boolean useClientDatabase = "local".equals(rootModel
				.getProperty(BuiltinProperties.CLIENT_DATABASE));
		getRepository().updateClientDatabase(useClientDatabase);
		MutableInt count = new MutableInt();
		HashSet<ElementEntry>[] scannedElements = new HashSet[SERVICE_CLIENT + 1];
		count.setValue(0);
		for (int i = TOP; i <= SERVICE_CLIENT; i++)
			scannedElements[i] = new HashSet<ElementEntry>();
		HashSet<ModelId> uiModels = new HashSet<ModelId>();
		scan(rootModel.getId(), uiModels, TOP, scannedElements, permissions, count);
		for (Iterator<ModelId> i = getAllModuleIds().iterator(); i.hasNext();)
		{
			scan(i.next(), uiModels, TOP, scannedElements, permissions, count);
		}
		return uiModels;
	}

	private static final int SERVICE_CLIENT = 3;

	private static final int SERVICE = 2;

	private static final int CLIENT = 1;

	private static final int TOP = 0;

	private void scan(ModelId modelId, HashSet<ModelId> uiModelIds, int state,
			HashSet<ElementEntry>[] scannedElements, Set<String> permissions, MutableInt count)
	{
		count.add(1);
		ModelEntry me = modelEntries.get(modelId);
		if (me == null)
			return;
		if (permissions != null) // Check that this models is permitted
		{
			String modelPermission = getPermission(modelId);
			if (modelPermission != null && !permissions.contains(modelPermission))
				return; // Model not permitted
		}
		PluginDescriptor pluginDescriptor = me.getDescriptor();
		if (pluginDescriptor != null && !pluginDescriptor.allowedOnServer())
			state = CLIENT;
		if (state == CLIENT)
			uiModelIds.add(modelId);

		if (state == CLIENT && pluginDescriptor != null && pluginDescriptor.isServiceClient())
			state = SERVICE_CLIENT;
		List<ElementEntry> elementEntries = getElementEntries(modelId);
		for (int i = 0; i < elementEntries.size(); i++)
		{
			ElementEntry e = elementEntries.get(i);
			ModelId refId = e.getRefId();
			if (refId == null)
				continue;
			int elementState = state;
			if (state == SERVICE_CLIENT)
				elementState = e.isSlot() ? CLIENT : SERVICE;
			if (scannedElements[elementState].contains(e))
				continue;
			scannedElements[elementState].add(e);
			scan(refId, uiModelIds, elementState, scannedElements, permissions, count);
		}
	}

	private String getPermission(ModelId modelId)
	{
		return permissionMap.get(modelId);
	}

	public List<ElementEntry> getElementEntries(ModelId parentId)
	{
		return elementMap.getElementEntries(parentId);
	}

	public List<ElementEntry> getReferenceEntries(ModelId refId)
	{
		return referenceMap.getReferences(refId);
	}

	public ModelEntry getModelEntry(ModelId modelId)
	{
		return modelEntries.get(modelId);
	}

	public Repository getRepository()
	{
		return repository;
	}

	public int getNumberOfModelElements()
	{
		return elements.size();
	}

	public int getNumberOfModels()
	{
		return modelEntries.size();
	}

	public Collection<ModelEntry> allModelEntries()
	{
		return modelEntries.values();
	}

	public Collection<ModelId> getAllModelIds()
	{
		return modelEntries.keySet();
	}

	public int countLocalModels()
	{
		int count = 0;
		for (ModelEntry entry : modelEntries.values())
		{
			if (!entry.isLibrary())
				++count;
		}
		return count;
	}
}

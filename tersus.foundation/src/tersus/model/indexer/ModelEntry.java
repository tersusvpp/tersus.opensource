/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model.indexer;

import tersus.model.ModelId;
import tersus.model.PluginDescriptor;

public class ModelEntry
{
    private ModelId id;
    private int flags;
    private PluginDescriptor descriptor;
    private static final int IS_MODULE = 1;
    private static final int IS_LIBRARY = 2;
    
    public PluginDescriptor getDescriptor()
    {
        return descriptor;
    }
    public void setDescriptor(PluginDescriptor descriptor)
    {
        this.descriptor = descriptor;
    }
    public ModelId getId()
    {
        return id;
    }
    public void setId(ModelId id)
    {
        this.id = id;
    } 
    protected boolean getFlag(final int flag)
    {
        return flag == (flag & flags);
    }
    protected void setFlag(boolean v, final int mask)
    {
        if (v)
            flags |= mask;
        else
            flags &= (~mask);
    }
    
	public void setIsModule(boolean b)
	{
		setFlag(b, IS_MODULE);
		
	}
	
	public boolean isModule()
	{
		return getFlag(IS_MODULE);
	}

	public void setIsLibrary(boolean b)
	{
		setFlag(b, IS_LIBRARY);
	}

	public boolean isLibrary()
	{
		return getFlag(IS_LIBRARY);
	}
	
}

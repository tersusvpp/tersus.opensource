/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;

import java.util.ArrayList;
import java.util.List;


/**
 * Sub-system, sub-process or display element.
 * @author Youval Bronicki
 *
 */
public class SubFlow extends ModelElement
{
	public SubFlow()
	{
	}

	private static List defaultProperties;
	public List getDefaultProperties()
	{
		return defaultProperties;
	}
	static {
		defaultProperties = new ArrayList();
		defaultProperties.add(new MetaProperty(BuiltinProperties.ROLE, Role.class, false, false, false));
		defaultProperties.add(new MetaProperty(BuiltinProperties.REF_ID, ModelId.class, false, false, false));
		defaultProperties.add(new MetaProperty(BuiltinProperties.POSITION, RelativePosition.class, true, false, false));
		defaultProperties.add(new MetaProperty(BuiltinProperties.SIZE, RelativeSize.class, true, false, false));
		defaultProperties.add(new MetaProperty(BuiltinProperties.REPETITIVE, Boolean.class, true, false, false));
	}
	static final long serialVersionUID = 1;
	protected transient FlowModel model;

	public FlowModel getModel()
	{
		return (FlowModel) getReferredModel();
	}

	/**
	 * @param model
	 */
	public void setModel(FlowModel model)
	{
		setReferredModel(model);
	}


    /* (non-Javadoc)
     * @see tersus.model.ModelElement#isSelfReady()
     */
    public boolean isSelfReady()
    {
        return !isRepetitive() && (getModel() != null && ! getModel().hasMandatoryTriggers());
    }
    public boolean checkAlwaysCreate()
    {
        return (! "false".equals(getProperty(BuiltinProperties.ALWAYS_CREATE)));
    }

	@Override
	public Object getProperty(String key)
	{
		if (BuiltinProperties.ROLE.equals(key))
			return getRole();
		if (BuiltinProperties.REF_ID.equals(key))
			return getRefId();
		if (BuiltinProperties.POSITION.equals(key))
			return getPosition();
		if(BuiltinProperties.SIZE.equals(key))
			return getSize();
		if (BuiltinProperties.REPETITIVE.equals(key))
			return isRepetitive();
		return getPropertyBase(key);
	}
	@Override
	public boolean isPropertySet(String key)
	{
		return isPropertySetBase(key);
	}

	@Override
	public void setProperty(String key, Object value)
	{
		if (BuiltinProperties.ROLE.equals(key))
		{
			if (value != null && ! (value instanceof Role))
				illegalValue(key, value);
			setRole((Role)value);
		}
		else if (BuiltinProperties.REF_ID.equals(key))
		{
			if (value != null && ! (value instanceof ModelId))
				illegalValue(key, value);
			setRefId((ModelId)value);
		}
		else if (BuiltinProperties.POSITION.equals(key))
		{
			if (value != null && ! (value instanceof RelativePosition))
				illegalValue(key, value);
			setPosition((RelativePosition)value);
		}
		else if(BuiltinProperties.SIZE.equals(key))
		{
			if (value != null &&  ! (value instanceof RelativeSize))
				illegalValue(key, value);
			setSize((RelativeSize)value);
		}
		else if (BuiltinProperties.REPETITIVE.equals(key))
		{
			if (value == null || ! (value instanceof Boolean))
				illegalValue(key, value);
			setRepetitive((Boolean)value);
		}
		else setPropertyBase(key,value);
		
	}
	
	public String getType()
	{
		Model refModel = getReferredModel();
		if (ModelUtils.isDisplayModel(refModel))
			return "Display";
		else if (ModelUtils.isSystemModel(getParentModel()))
		{
			if (ModelUtils.isSystemModel(refModel))
				return "SubSystem";
			else
				return "TopProcess";
		}
		else
			return "SubProcess";
		
	}


}

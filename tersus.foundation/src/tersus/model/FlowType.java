/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;

import tersus.util.Enum;

/**
 * 
 */
public class FlowType extends Enum
{
	private static final long serialVersionUID = 1L;
    public static FlowType SYSTEM= new FlowType("System"); 
	public static FlowType OPERATION= new FlowType("Operation"); 
	public static FlowType ACTION= new FlowType("Action");
	public static FlowType DISPLAY= new FlowType("Display");
	public static FlowType SERVICE= new FlowType("Service");
	public static FlowType[] values = {SYSTEM,OPERATION,ACTION, DISPLAY, SERVICE};
	 
	/**
	 * @param value
	 */
	private FlowType(String value)
	{
		super(value);
	}
	/* (non-Javadoc)
	 * @see tersus.util.Enum#getValues()
	 */
	protected Enum[] getValues()
	{
		return values;
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model;

import java.lang.ref.WeakReference;
import java.util.HashMap;

/**
 * @author Liat Shiff
 */
public class WeakPackageCache implements PackageCache
{
	private HashMap<PackageId, WeakReference<Package>> packageCache;

	public WeakPackageCache()
	{
		packageCache = new HashMap<PackageId, WeakReference<Package>>();
	}

	public Package get(PackageId key)
	{
		WeakReference<Package> ref = packageCache.get(key);
		
		if (ref == null)
			return null;
		
		return ref.get();
	}

	public void put(PackageId key, Package value)
	{
		packageCache.put(key, new WeakReference<Package>(value));
	}
	
	public Package remove(PackageId key)
	{
		WeakReference<Package> ref = packageCache.remove(key);
		return ref != null ? ref.get(): null;
	}

	public void clear()
	{
		packageCache.clear();
	}
	
	public void fill(PackageCache cacheToFill)
	{
		for (WeakReference<Package> ref: packageCache.values())
		{
			Package pkg = ref.get();
			if (pkg != null)
				cacheToFill.put(pkg.getPackageId(), pkg);
		}
	}
}

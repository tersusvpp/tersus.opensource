/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/

package tersus.model;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import tersus.ProjectStructure;
import tersus.util.FileUtils;


/**
 * @author Youval Bronicki
 * 
 */
public class Library
{
	File sourceFile;
	HashMap<String, ZipEntry> files;
	String modelsPrefix;
	String projectPath;
	long lastModified = 0;
	
	public Library(File sourceFile)
	{
		try
		{
			this.sourceFile = sourceFile;
			
			load();
		}
		catch (Exception e)
		{
			throw new ModelException("Failed to initialize library from file '"
					+ sourceFile.getPath() + "'", e);
		}
	}
	public void reloadIfNeeded() throws IOException
	{
		if (sourceFile.lastModified() > lastModified)
			load();
	}
	public void load() throws IOException
	{
		if (files == null)
			files = new HashMap<String, ZipEntry>();
		else
			files.clear();
		ZipFile zf = null;
		lastModified = sourceFile.lastModified();
		try
		{
			zf = new ZipFile(sourceFile);
			
			Enumeration<? extends ZipEntry> en = zf.entries();
			while(en.hasMoreElements()) {
				ZipEntry ze = en.nextElement();
				if ( ze.isDirectory())
					continue; // We don't care about directories
				String path = ze.getName();
				if (path.equals(".project"))
					projectPath = "";
				else if (path.endsWith("/.project"))
					projectPath = path.substring(0, path.lastIndexOf("/")+1);
				
				files.put(path, ze);
			}	
			zf.close();
			zf = null;
		}
		finally
		{
			FileUtils.forceClose(zf);
		}
	}


	public byte[] readModelsFile(String path)
	{
		path = getModelsPath(path);
		return readZipFile(path);
	}
	
	public byte[] readProjectFile(String path)
	{
		if (projectPath != null)
			path = projectPath+path;
		return readZipFile(path);
	}
	protected byte[] readZipFile(String path)
	{
		ZipEntry ze = files.get(path);
		if (ze == null)
			return null;
		ZipFile zf = null;
		try
		{
			zf = new ZipFile(sourceFile);
			byte[] bytes =  FileUtils.readBytes(zf.getInputStream(ze), false);
			zf.close();
			zf = null;
			return bytes;
		}
		catch (Exception e)
		{
			throw new ModelException("Failed to read "+path+" from library "+ sourceFile.getPath(), e);
		}
		finally
		{
			FileUtils.forceClose(zf);
		}
	}
	protected String getModelsPath(String path)
	{
		if (projectPath != null)
			path = projectPath+ProjectStructure.MODELS+"/"+path;
		return path;
	}

	@SuppressWarnings("unchecked")
	public List<PackageId> getPackageIds()
	{
		HashSet<PackageId> set = new HashSet<PackageId>(files.size());
		int suffixLength=Repository.PACKAGE_SUFFIX.length();
		String modelsPath = projectPath == null ? "" : projectPath+ProjectStructure.MODELS+"/";
		int prefixLength = modelsPath.length();
		for (String p: files.keySet())
		{
			if (p.endsWith(Repository.PACKAGE_SUFFIX) && p.startsWith(modelsPath))
			{
				PackageId packageId = new PackageId(p.substring(prefixLength, p.length()-suffixLength));
				set.add(packageId);
				PackageId parent = packageId.getParent(); 
				while (parent != null && ! set.contains(parent))
				{
					set.add(parent);
					parent = parent.getParent();
				}
			}
		}
		ArrayList<PackageId> list = new ArrayList<PackageId>(set.size());
		list.addAll(set);
		Collections.sort(list);
		return list;
	}

	public File getSourceFile()
	{
		return sourceFile;
	}
	
	
	public boolean contains(String path)
	{
		return files.containsKey(getModelsPath(path));
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tersus.util.Multiplicity;



/**
 * @author Youval Bronicki
 *
 */
public class TemplateAndPrototypeSupport
{
	public static final String TRUE = "true";
	public static final String PROTOTYPE = "prototype";
    /**
     * @param element
     * @return
     */
    public static Multiplicity getMultiplicity(ModelElement element)
    {
        return new Multiplicity((String)element.getProperty(BuiltinProperties.MULTIPLICITY));
    }
    /**
     * @param model
     * @param element
     * @return
     */
    public static List getInstanceElements(Model model, ModelElement prototypeElement)
    {
        List elements = Collections.EMPTY_LIST;
        List allElements = model.getElements();
        String elementName = prototypeElement.getRole().toString();
        for (int i=0; i<allElements.size(); i++)
        {
            ModelElement e = (ModelElement)allElements.get(i);
            if (elementName.equals(e.getProperty(BuiltinProperties.PROTOTYPE_ELEMENT))|| elementName.equals(e.getRole().toString()))
            {
                if (elements == Collections.EMPTY_LIST)
                    elements = new ArrayList();
                elements.add(e);
            }
           
        }
        return elements;
    }
    /**
     * @param template
     * @return
     */
    public static boolean isPrototype(Model model)
    {
        return model != null && PROTOTYPE.equals(model.getProperty(BuiltinProperties.TEMPLATE));
    }
    /**
     * Checkes whether a model is a template (or prototype)
     * 
     */
    public static boolean isTemplate(Model model) {
    	if (model == null)
    		return false;
    
    	Object template = model.getProperty(BuiltinProperties.TEMPLATE);
    	return (TRUE.equals(template) || PROTOTYPE
    			.equals(template));
    }
    public static Model getPrototypeModel(Model model)
    {
        String prototypePath = model.getPrototype();
        IRepository repository = model.getRepository();
    
        if (prototypePath != null)
        {
            return repository.getModel(repository.getModelId(prototypePath), true);
        }
        return null;
    }
	public static boolean isStrictTemplate(Model model)
	{
		Object template = model.getProperty(BuiltinProperties.TEMPLATE);
    	return TRUE.equals(template); 
	}
}

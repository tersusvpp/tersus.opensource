/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;

import java.io.Serializable;
import java.util.HashMap;

/**
 * 
 */
public class Role implements ElementPath, Serializable {
	String s;
	static HashMap allRoles = new HashMap();
	
	/**
	 * Returns the unique role for a given string.
	 * @param s the given string
	 * @return the unique role whose string is s
	 * @throws ModelException if the string is not valid for a role
	 */
	public static synchronized Role get(String s)
	{
		if (! isValidString(s))
			throw new ModelException("Invalid Role:"+s);
		Role role = (Role)allRoles.get(s);
		if (role == null)
			role = new Role(s);
		return role;
	}
	/**
     * @param s
     * @return
     */
    public static boolean isValidString(String s)
    {
        return s!=null && s.length() > 0 && s.indexOf('/')<0;
    }
    private Role(String s)
	{
		this.s = s;
		allRoles.put(s,this);
	}
	
	public String toString() {
		return s;
	}
	
	Object readResolve()
	{
		return get(s);
	}
	/* (non-Javadoc)
	 * @see tersus.model.ElementPath#getNumberOfSegments()
	 */
	public int getNumberOfSegments()
	{
		return 1;
	}
	/* (non-Javadoc)
	 * @see tersus.model.ElementPath#getSegment(int)
	 */
	public Role getSegment(int i)
	{
		if (i==0)
			return this;
		else
			throw new IndexOutOfBoundsException();
	}
	/* (non-Javadoc)
	 * @see tersus.model.ElementPath#getPrefix(int)
	 */
	public Path getPrefix(int i)
	{
		if (i == 0)
			return new Path();
		if (i==1)
		{
			Path p = new Path();
			p.addSegment(this);
			return p;
		}
		else 
			throw new IllegalArgumentException("getPrefix() called with size greater than the number of segments");
			
	}
	public StringBuffer appendToBuffer(StringBuffer buffer)
	{
		buffer.append(this);	
		return buffer;
	}
	/* (non-Javadoc)
	 * @see tersus.model.ElementPath#isEquivalent(tersus.model.ElementPath)
	 */
	public boolean isEquivalent(ElementPath path)
	{
		if (path == null)
			return false;
		return toString().equals(path.toString());
	}
	
    public boolean isDistinguished()
    {
        return s.endsWith(">") && (s.indexOf('<') >=0);
    }

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model;

import java.util.ArrayList;
import java.util.List;

import tersus.util.Misc;

/**
 * 
 */
public class DataType extends Model
{
	static final long serialVersionUID = 1;
	private String tableName;
	private String value;
	private boolean constant = false;
	private Composition composition;
	private static List defaultProperties;
	static
	{
		defaultProperties = new ArrayList();

		defaultProperties.add(new MetaProperty(BuiltinProperties.ID, ModelId.class, false, false,
				false));
		defaultProperties.add(new MetaProperty(BuiltinProperties.NAME, String.class, false, false,
				false));
		defaultProperties.add(new MetaProperty(BuiltinProperties.COMPOSITION, Composition.class,
				true, false, false));
		defaultProperties.add(new MetaProperty(BuiltinProperties.TABLE_NAME, String.class, true,
				false, false));
		defaultProperties.add(new MetaProperty(BuiltinProperties.CONSTANT, Boolean.class, true,
				false, false));
		defaultProperties.add(new MetaProperty(BuiltinProperties.VALUE, String.class, true, false,
				false));
		defaultProperties.add(new MetaProperty(BuiltinProperties.PLUGIN, String.class, true, false,
				false));
		defaultProperties.add(new MetaProperty(BuiltinProperties.PROTOTYPE, String.class, true,
				false, false));
		defaultProperties.add(new MetaProperty(BuiltinProperties.ICON_FOLDER, String.class, true,
				false, false));
		defaultProperties.add(new MetaProperty(BuiltinProperties.DOCUMENTATION, String.class, true,
				false, false));
	}

	public List getDefaultProperties()
	{
		return defaultProperties;
	}

	/**
	 * @return
	 */
	public Composition getComposition()
	{
		return composition;
	}

	/**
	 * @param composition
	 */
	public void setComposition(Composition composition)
	{
		Composition oldValue = this.composition;
		if (composition != oldValue)
		{
			this.composition = composition;
			firePropertyChange(BuiltinProperties.COMPOSITION, oldValue, composition);
			markModified();
		}
	}

	/**
	 * @return
	 */
	public String getTableName()
	{
		if (tableName != null)
			return tableName;
		else if (BuiltinPlugins.DATABASE_RECORD.equals(plugin))
			return Misc.sqlize(getName());
		else
			return null;
	}

	public boolean isTableNameSet()
	{
		return tableName != null;
	}

	/**
	 * @param string
	 */
	public void setTableName(String name)
	{
		String oldName = tableName;
		tableName = name;
		firePropertyChange(BuiltinProperties.TABLE_NAME, oldName, tableName);
		markModified();
	}

	/**
	 * @param constant
	 */
	public void setConstant(boolean constant)
	{
		if (this.constant != constant)
		{
			this.constant = constant;
			firePropertyChange(BuiltinProperties.CONSTANT, Misc.getBoolean(!constant), Misc
					.getBoolean(constant));
			markModified();
		}
	}

	/**
	 * @return
	 */
	public boolean isConstant()
	{
		return constant;
	}

	/**
	 * @param childModelId
	 * @param index
	 * @return
	 */
	public ModelElement addElement(ModelId childModelId, String elementName, int index)
	{
		DataElement element = new DataElement();
		element.setRefId(childModelId);
		element.setProperty(BuiltinProperties.MANDATORY, Boolean.TRUE);

		element.setRole(defaultRole(elementName));
		addElement(index, element);
		return element;
	}

	public String getValue()
	{
		if (value == null && isConstant())
			return getName();
		else
			return value;
	}

	public boolean isValueSet()
	{
		return value != null;
	}

	/**
	 * @param value
	 *            The value to set.
	 */
	public void setValue(String value)
	{
		String oldValue = this.value;
		this.value = value;
		firePropertyChange(BuiltinProperties.VALUE, oldValue, value);
		markModified();
	}

	public String getType()
	{
		return "Data Type";
	}

	@Override
	public Object getProperty(String key)
	{
		if (BuiltinProperties.ID.equals(key))
				return getId();
		else if(BuiltinProperties.NAME.equals(key))
				return getName();
		else if(BuiltinProperties.COMPOSITION.equals(key))
				return getComposition();
		else if(BuiltinProperties.TABLE_NAME.equals(key))
				return getTableName();
		else if(BuiltinProperties.CONSTANT.equals(key))
				return isConstant();
		else if(BuiltinProperties.VALUE.equals(key))
				return getValue();
		else if(BuiltinProperties.PLUGIN.equals(key))
				return getPlugin();
		else if(BuiltinProperties.PROTOTYPE.equals(key))
				return getPrototype();
		else if(BuiltinProperties.ICON_FOLDER.equals(key))
			return getIconFolder();
		else if(BuiltinProperties.DOCUMENTATION.equals(key))
			return getDocumentation();
		else if(BuiltinProperties.TYPE.equals(key))
			return getType();
		else
			return getPropertyBase(key);
	}

	@Override
	public boolean isPropertySet(String key)
	{
		if(BuiltinProperties.ICON_FOLDER.equals(key))
			return isIconFolderSet();
		else if(BuiltinProperties.DOCUMENTATION.equals(key))
			return isDocumentationSet();
		else if(BuiltinProperties.PROTOTYPE.equals(key))
			return isPrototypeSet();
		else if(BuiltinProperties.TABLE_NAME.equals(key))
			return isTableNameSet();
		else if(BuiltinProperties.VALUE.equals(key))
			return isValueSet();
		else
			return isPropertySetBase(key);
	}

	@Override
	public void setProperty(String key, Object value)
	{
		if (BuiltinProperties.ID.equals(key))
		{
			if (value != null && ! (value instanceof ModelId))
				illegalValue(key, value);
			setId((ModelId)value);
		}
		else if(BuiltinProperties.NAME.equals(key))
		{
			illegalValue(key, value);
		}
		else if(BuiltinProperties.COMPOSITION.equals(key))
		{
			if (!(value instanceof Composition))
				illegalValue(key, value);
			setComposition((Composition)value);
		}
		else if(BuiltinProperties.TABLE_NAME.equals(key))
		{
			 setTableName((String)value);
		}
		else if(BuiltinProperties.CONSTANT.equals(key))
		{
			if (! (value instanceof Boolean))
				illegalValue(key, value);
			setConstant((Boolean)value);
		}
		else if(BuiltinProperties.VALUE.equals(key))
		{
			if (value != null && ! (value instanceof String))
				illegalValue(key, value);
			setValue((String)value);
		}
		else if(BuiltinProperties.PLUGIN.equals(key))
		{
			if (value != null && ! (value instanceof String))
				illegalValue(key, value);
			setPlugin((String)value);
		}
		else if(BuiltinProperties.PROTOTYPE.equals(key))
		{
			if (value != null && ! (value instanceof String))
				illegalValue(key, value);
			setPrototype((String)value);
		}
		else if(BuiltinProperties.ICON_FOLDER.equals(key))
		{
			if (value != null && ! (value instanceof String))
				illegalValue(key, value);
			setIconFolder((String)value);
		}
		else if(BuiltinProperties.DOCUMENTATION.equals(key))
		{
			if (value != null && ! (value instanceof String))
				illegalValue(key, value);
			setDocumentation((String)value);
		}
		else if(BuiltinProperties.TYPE.equals(key))
		{
			illegalValue(key, value);
		}
		else
		{	
			setPropertyBase(key, value);
		}
		
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;

/**
 * Identifier of a model.
 * 
 * @author Ofer Brandes
 *
 */
public class ModelId
{
	
	private PackageId packageId;
	private String name;
	private String suffix;
    private int hashcode;
	
	public ModelId (String path)
	{
	    int lastSlash = path.lastIndexOf('/');
	    if (lastSlash < 0)
	    	throw new IllegalArgumentException("Wrong modelId format '"+path+"' (missing '/')");
	    final String n = path.substring(lastSlash+1);
        final PackageId p = new PackageId(path.substring(0, lastSlash));
        init(p, n, path);
	}
    private void init(final PackageId packageId, final String name, String path)
    {
    	if (name == null)
    		throw new IllegalArgumentException("Missing Name");
    	if (packageId == null)
    		throw new IllegalArgumentException("Missing packageId");
        this.name = name;
        this.packageId = packageId;
        this.hashcode = path.hashCode();
    }
	/**
     * @param packageId
     * @param name2
     */
    public ModelId(PackageId packageId, String modelName)
    {
        String path = packageId + "/" + modelName;
        init(packageId, modelName, path);
    }
    public boolean equals (Object other)
	{
		if (this == other)
			return true;
			
		if ((other == null) || (other.getClass() != this.getClass()))
			return false;
			
        ModelId o = (ModelId)other;
		return hashcode == o.hashcode && packageId.equals(o.packageId) && name.equals(o.name);
	}
	
	public int hashCode()
	{
		return hashcode;
	}
	
	public PackageId getPackageId()
	{
		return packageId; // 'null' if the project's root package
	}
	
	public String getName()
	{
		return name;
	}

	public String getSuffix()
	{
		return suffix;
	}

	public String getPath()
	{
		return getPackageId() + "/"+getName();
	}
	
	public String toString()
	{
		return getPath();
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd - Initial API and implementation
 *************************************************************************************************/

package tersus.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import tersus.model.indexer.RepositoryIndex;
import tersus.util.FileUtils;
import tersus.util.IProgressMonitor;
import tersus.util.InvalidInputException;
import tersus.util.Misc;
import tersus.util.NotImplementedException;
import tersus.util.NullProgressMonitor;
import tersus.util.XMLReaderConfigurator;

/**
 * Manages the creation and persistence of models
 */

public abstract class Repository implements IRepository
{
	private static final String LF = "\n";

	private static final String CRLF = "\r\n";

	public static final String LIBARAY_PATH_SEPARATOR = ";";

	private static final Logger log = Logger.getLogger(Repository.class.getName());

	private List<Library> libraries = new ArrayList<Library>();

	private List<PackageId> libraryPackageIds = null;

	private static final String COMMON_TEMPLATES_PATH = "Common/Templates/";

	private static final String COMMON_PATH = "Common/";

	private RepositoryIndex index;

	protected static Set<String> ignoredFiles;

	public static final String OLD_SUFFIX = ".tersus";

	public static final String PACKAGE_EXTENSION = "trp";

	public static final String PACKAGE_SUFFIX = "." + PACKAGE_EXTENSION;

	private static final String PLUGIN_DESCRIPTOR_SUFFIX = ".tpd";

	private boolean saveInProgress = false;

	private String lineSeparator = null;

	static
	{
		ignoredFiles = new HashSet<String>();
		ignoredFiles.add("CVS");
		ignoredFiles.add(".svn");
		XMLReaderConfigurator.configureXMLReader();
	}

	private static final Set<String> builtinModelIds = new HashSet<String>();

	static
	{
		for (int i = 0; i < BuiltinModels.modelIds.length; i++)
			builtinModelIds.add(BuiltinModels.modelIds[i].getPath());
	}

	@SuppressWarnings("unchecked")
	public synchronized List<PackageId> getLibraryPackageIds()
	{
		if (libraryPackageIds == null)
		{
			libraryPackageIds = new ArrayList<PackageId>();
			for (int i = 0; i < libraries.size(); i++)
			{
				Library library = libraries.get(i);
				for (PackageId packageId : library.getPackageIds())
				{
					if (!libraryPackageIds.contains(packageId))
						libraryPackageIds.add(packageId);
				}
			}
			Collections.sort(libraryPackageIds);
		}
		return libraryPackageIds;
	}

	public void clearLibraryPackageIds()
	{
		libraryPackageIds = null;
	}

	private HashMap<String, PluginDescriptor> cachedDescriptors = new HashMap<String, PluginDescriptor>();

	protected Map<String, ModelId> cachedModelIds = new HashMap<String, ModelId>();

	protected Map<String, Path> cachedPaths = new HashMap<String, Path>();

	protected Map<String, PackageId> cachedPackageIds = new HashMap<String, PackageId>();

	protected Map<String, MetaProperty> cachedMetaProperties = new HashMap<String, MetaProperty>();

	protected PackageCache cachedPackages;

	protected HashMap<PackageId, Package> modifiedPackageMap = new HashMap<PackageId, Package>();

	protected HashSet<Package> newPackageSet = new HashSet<Package>();

	protected void removeModel(Model model)
	{
		Package pkg = model.getPackage();
		markModelDirtyInIndex(model.getModelId());
		pkg.removeModel(model);
	}

	protected void markModelDirtyInIndex(ModelId modelId)
	{
		if (index != null)
			index.markDirty(modelId);
	}

	protected void movePackage(PackageId pkgId, PackageId newId, boolean notifyListeners)
	{
		Package pkg = getPackage(pkgId);
		if (pkg == null)
			return;
		for (Model model : pkg.getModels())
		{
			markModelDirtyInIndex(model.getId());
			model.resetId(new ModelId(newId, model.getName()));// resetId() indirectly causes new
			// model id to be marked ditry in
			// index
		}
		cachedPackages.remove(pkgId);
		pkg.setId(newId);
		cachedPackages.put(newId, pkg);
	}

	public void packageModified(Package pkg)
	{
		if (pkg.getSavedPackageId() != null) // Otherwise - this is a new package (in newPackageSet)
			modifiedPackageMap.put(pkg.getSavedPackageId(), pkg);
	}

	/*
	 * Clears the cache of models. References to existing models must not be used, because these
	 * models are no longer managed.
	 */
	synchronized public void clearCache()
	{
		cachedPackages.clear();
		cachedDescriptors.clear();
		cachedModelIds.clear();
		cachedPackageIds.clear();
		cachedMetaProperties.clear();
		cachedPaths.clear();
		modifiedPackageMap.clear();
		newPackageSet.clear();
		index = null;
		setLineSeparator(null);
		// TODO: invalidate existing models? Adding such a mechanism will help
		// detect errors
	}

	protected void deletePackageFile(PackageId id) throws IOException
	{
		throw new NotImplementedException("deletePackageFile() not implemented for "
				+ getClass().getName());
	}

	protected void deletePackageFolder(PackageId id) throws IOException
	{
		throw new NotImplementedException("deletePackageFile() not implemented for "
				+ getClass().getName());
	}

	public Collection<Package> getModifiedPackages()
	{
		return modifiedPackageMap.values();
	}

	synchronized public Collection<Package> getDeletedPackages()
	{
		ArrayList<Package> deleted = new ArrayList<Package>();

		for (Package pkg : modifiedPackageMap.values())
		{
			if (pkg.isDeleted() || pkg.isMoved())
				deleted.add(pkg);
		}
		return deleted;
	}

	/**
	 * Translates Tersus plugin descriptors (e.g. Tersus/Database/Find) to the appropriate location
	 * under "Common" (e.g. Common/Templatate/Database/Find)
	 * 
	 * @param plugin
	 * @return
	 */
	private String[] getPossibleDescriptorPaths(String plugin)
	{
		String[] paths = new String[5];
		Path path = new Path(plugin);
		String suffix = path.getTail(path.getNumberOfSegments() - 1).toString();
		String name = path.getLastSegment().toString();
		paths[0] = plugin + PLUGIN_DESCRIPTOR_SUFFIX;
		paths[1] = plugin + Path.SEPARATOR + name + PLUGIN_DESCRIPTOR_SUFFIX;
		paths[2] = COMMON_PATH + suffix + Path.SEPARATOR + name + PLUGIN_DESCRIPTOR_SUFFIX;
		paths[3] = COMMON_TEMPLATES_PATH + suffix + PLUGIN_DESCRIPTOR_SUFFIX;
		paths[4] = COMMON_TEMPLATES_PATH + suffix + Path.SEPARATOR + name
				+ PLUGIN_DESCRIPTOR_SUFFIX;
		return paths;
	}

	/**
	 * Retrieves a model from the cache or from the file system. If the model is read from the file
	 * system, the package it belongs too will be cached if the parameter cachePacka is true
	 * 
	 * @param id
	 *            the id of the model to be retrieved
	 * @param indicates
	 *            whether the model should be cached
	 * @return the retreived model, or null if not found
	 */
	public synchronized Model getModel(ModelId modelId, boolean cacheModel)
	{
		Model model = null;
		PackageId packageId = modelId.getPackageId();
		Package pkg = getPackage(packageId);
		
		if (pkg != null)
			model = pkg.getModel(modelId.getName());

		return model;
	}

	public Package getPackage(PackageId packageId)
	{
		Package pkg = null;
		if ((pkg = cachedPackages.get(packageId)) != null)
			return pkg;
		else if ((pkg = modifiedPackageMap.get(packageId)) != null)
		{
			if (Misc.ASSERTIONS)
				Misc.assertion(pkg.isDeleted() || pkg.isMoved());
			// If the package is deleted or modified, we return null
			
			return null;
		}
		else
			pkg = readPackage(packageId);
		
		return pkg;
	}

	public String getPackageFilePath(PackageId packageId)
	{
		String path = packageId + PACKAGE_SUFFIX;
		return path;
	}

	public PluginDescriptor getPluginDescriptor(String plugin)
	{
		try
		{
			PluginDescriptor descriptor = cachedDescriptors.get(plugin);
			if (descriptor != null)
				return descriptor;
			String[] paths = getPossibleDescriptorPaths(plugin);
			for (int i = 0; i < paths.length; i++)
			{
				descriptor = loadPluginDescriptor(paths[i]);
				if (descriptor != null)
				{
					cachedDescriptors.put(plugin, descriptor);
					return descriptor;
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new ModelException("Failed to load plugin descriptor '" + plugin + "'", e);
		}
		return null;
	}

	private PluginDescriptor loadPluginDescriptor(String path) throws IOException
	{
		byte[] buffer = getResource(path);
		if (buffer == null)
			return null;
		ByteArrayInputStream in = new ByteArrayInputStream(buffer);
		PluginDescriptor descriptor = PluginDescriptorFactory.loadXML(in);
		return descriptor;
	}

	/**
	 * @param templateResourcePath
	 * @return null if resource does not exist (and does not throw IOException)
	 */
	public byte[] getResource(String path) throws IOException

	{
		byte[] bytes = read(path);
		if (bytes == null)
			bytes = readLibraryResource(path);
		return bytes;
	}

	protected abstract List<PackageId> getSavedSubPackageIds(PackageId parent);

	@SuppressWarnings("unchecked")
	public List<PackageId> getSubPackageIds(PackageId parent)
	{
		List<PackageId> subPackageIds = new ArrayList<PackageId>(getSavedSubPackageIds(parent));
		List<PackageId> toRemove = new ArrayList<PackageId>();

		for (PackageId savedPackageId : subPackageIds)
		{
			Package cachedPackageForPath = getPackageForSavedPath(savedPackageId);
			if (cachedPackageForPath != null
					&& (cachedPackageForPath.isDeleted() || cachedPackageForPath.isMoved()))
				toRemove.add(savedPackageId);
		}
		subPackageIds.removeAll(toRemove);

		for (Package pkg : newPackageSet)
		{
			PackageId id = pkg.getPackageId();
			if (Misc.equal(id.getParent(), parent))
			{
				if (!subPackageIds.contains(id))
					subPackageIds.add(id);
			}
		}

		for (Package pkg : modifiedPackageMap.values())
		{
			if (!pkg.isDeleted())
			{
				PackageId id = pkg.getPackageId();
				if (Misc.equal(id.getParent(), parent))
				{
					if (!subPackageIds.contains(id))
						subPackageIds.add(id);
				}
			}
		}

		ArrayList<PackageId> librarySubPackageIds = new ArrayList<PackageId>();
		List<PackageId> allLibraryPackageIds = getLibraryPackageIds();
		for (int i = 0; i < allLibraryPackageIds.size(); i++)
		{
			PackageId id = allLibraryPackageIds.get(i);
			if (Misc.equal(parent, id.getParent()))
			{
				if (!subPackageIds.contains(id))
					librarySubPackageIds.add(id);
			}
		}
		subPackageIds.addAll(librarySubPackageIds);
		Collections.sort(subPackageIds);
		return subPackageIds;
	}

	synchronized private Package getPackageForSavedPath(PackageId savedPackageId)
	{
		return modifiedPackageMap.get(savedPackageId);
	}

	synchronized private void removeModifiedPackage(PackageId savedPackageId)
	{
		modifiedPackageMap.remove(savedPackageId);
	}

	public Package getPackageFromCache(PackageId packageId)
	{
		return cachedPackages.get(packageId);
	}

	public void loadAll(ModelId id)

	{
		HashSet<ModelId> loaded = new HashSet<ModelId>();
		loaded.add(id);
		loadOffsprings(id, loaded);
	}

	private void loadOffsprings(ModelId parentId, HashSet<ModelId> loaded)
	{
		loaded.add(parentId);
		Model parent = getModel(parentId, true);
		for (ModelElement element : parent.getElements())
		{
			ModelId childId = element.getRefId();
			if (childId != null && !loaded.contains(childId))
				loadOffsprings(childId, loaded);
		}
	}

	protected void log(String message, Exception e)
	{
		if (log.isInfoEnabled())
			log.info(message, e);
	}

	protected void log(String message)
	{
		if (log.isInfoEnabled())
			log.info(message);
	}

	/**
	 * @param newModelId
	 *            The id of the new model.
	 * @param templateModelId
	 *            The id of a template to create the model from.
	 * @param createElements
	 * @return The newly created model.
	 */
	public Model newModel(ModelId newModelId, ModelId templateModelId, boolean createElements)
	{
		// Get template
		if (getModel(newModelId, true) != null)
			throw new ModelException("Repository already contains a model with id '" + newModelId
					+ "'");

		Model template = getModel(templateModelId, true);
		if (template == null)
			throw new ModelException("No such model: " + templateModelId);

		Model model = baseNewModel(newModelId, template.getClass(), null);

		// Copy attributes and elements from template (shallow copy)
		for (MetaProperty property : template.getMetaProperties())
		{
			if (template.isPropertySet(property.name))
			{
				// TODO copy meta property to new model
				setProperty(model, property.name, template.getProperty(property.name));
			}
		}
		// Copy elements from template (shallow copy)
		if (createElements)
		{
			for (ModelElement element : template.getElements())
			{
				ModelElement elementCopy = copyElement(element);
				model.addElement(elementCopy);
			}
		}
		return (model);
	}

	public Model newModel(ModelId newModelId, Class<? extends Model> modelClass,
			Map<String, Object> properties)
	{
		return baseNewModel(newModelId, modelClass, properties);
	}

	private Model baseNewModel(ModelId newModelId, Class<? extends Model> modelClass,
			Map<String, Object> properties)
	{
		// Get template
		if (getModel(newModelId, true) != null)
			throw new ModelException("Repository already contains a model with id '" + newModelId
					+ "'");

		Model model = null;
		if (FlowModel.class.equals(modelClass))
		{
			model = new FlowModel();
			((FlowModel) model).setType(FlowType.OPERATION);
		}
		else
		{
			if (Misc.ASSERTIONS)
				Misc.assertion(DataType.class.equals(modelClass));
			model = new DataType();
		}

		model.setId(newModelId);

		// Add new model to package
		Package pkg = getPackage(newModelId.getPackageId());
		if (pkg == null)
			pkg = newPackage(newModelId.getPackageId());
		else
			packageModified(pkg);
		pkg.addModel(model);

		markModelDirtyInIndex(model.getModelId());

		if (properties != null)
		{
			// Copy attributes and elements from template (shallow copy)
			for (Map.Entry<String, Object> e : properties.entrySet())
			{
				String propertyName = e.getKey();
				Object propertyValue = e.getValue();
				setProperty(model, propertyName, propertyValue);
			}
		}
		return (model);
	}
	
	private void setProperty(Model model, String propertyName, Object propertyValue)
	{
		if (BuiltinProperties.ID.equals(propertyName))
			return; // That is the one property we do NOT want to copy
		if (BuiltinProperties.NAME.equals(propertyName))
			return; // Name is part of the id (and set when the id is
		// set)
		if (BuiltinProperties.TEMPLATE.equals(propertyName))
			return; // When creating an instance of a template, we don't
		// want the result to be a template
		model.setProperty(propertyName, propertyValue);
	}

	public static ModelElement copyElement(ModelElement element)
	{

		ModelElement elementCopy;
		try
		{
			elementCopy = element.getClass().newInstance();
			for (MetaProperty property : element.getMetaProperties())
			{
				if (element.isPropertySet(property.name))
				{
					Object propertyValue = element.getProperty(property.name);
					elementCopy.setProperty(property.name, propertyValue);
				}
			}
		}
		catch (InstantiationException e)
		{
			throw new ModelException("Failed to instantiate model element", e);
		}
		catch (IllegalAccessException e)
		{
			throw new ModelException("Failed to instantiate model element", e);
		}
		return elementCopy;
	}

	public Package newPackage(PackageId packageId)
	{
		if (getPackage(packageId) != null)
			throw new InvalidInputException("A package with id '" + packageId + "' already exists");
		if (packageId.getParent() != null)
			if (getPackage(packageId.getParent()) == null)
				newPackage(packageId.getParent());
		Package pkg = new Package(this);
		pkg.setId(packageId);
		pkg.setSavedReadOnly(pkg.isReadOnly());
		cachedPackages.put(packageId, pkg);
		newPackageSet.add(pkg);
		return pkg;
	}

	public abstract boolean packageFolderExists(PackageId packageId);

	/**
	 * Reads a serialized representation of a model
	 * 
	 * @param refId
	 * @return a byte array containing the serialized model
	 */
	abstract protected byte[] read(String path) throws IOException;

	abstract protected boolean exists(String path) throws IOException;

	abstract public boolean isFileReadOnly(PackageId packageId) throws IOException;

	abstract public boolean isPackageFolderReadOnly(PackageId packageId) throws IOException;

	private Package readPackage(PackageId packageId)
	{
		ByteArrayInputStream in = null;
		try
		{
			Package pkg = null;
			String path = getPackageFilePath(packageId);
			byte[] serialization = read(path);
			boolean isLibraryPackage = false;
			if (serialization == null)
			{
				serialization = readLibraryResource(path);
				if (serialization != null)
					isLibraryPackage = true;
			}
			if (serialization != null)
			{ // Pakcage file exists
				
				in = new ByteArrayInputStream(serialization);
				PackageFormat format = new PackageFormat(this);
				
				pkg = format.readPackage(in);
				pkg.setLibrary(isLibraryPackage);
				String packageString = new String(serialization, "UTF8");
				if (packageString.indexOf(CRLF) >= 0)
				{
					pkg.setLineSeparator(CRLF);
					if (!isLibraryPackage && getLineSeparator() == null)
						setLineSeparator(CRLF);
				}
				else if (packageString.indexOf(LF) > 0)
				{
					pkg.setLineSeparator(LF);
					if (!isLibraryPackage && getLineSeparator() == null)
						setLineSeparator(LF);
				}
				in.close();
				in = null;
				modifiedPackageMap.remove(pkg.getPackageId());
				pkg.setSavedReadOnly(pkg.isReadOnly());
				cachedPackages.put(packageId, pkg);
				for (Model model : pkg.getModels())
				{
					// Since the model is being modified while read - we need to clear the modified
					// flag here
					model.clearModified();
				}
			}
			else
			{
				// Package file does not exist
				boolean packageFolderExists = packageFolderExists(packageId);
				if (packageFolderExists || getLibraryPackageIds().contains(packageId))
				{
					pkg = new Package(this);
					pkg.setId(packageId);
					modifiedPackageMap.remove(packageId);
					pkg.setSavedReadOnly(pkg.isReadOnly());
					cachedPackages.put(packageId, pkg);
				}
			}
			if (pkg != null)
			{
				pkg.setSavedPackageId(packageId);
			}
			
			return pkg;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new ModelException("Failed to read package " + packageId, e);
		}
		finally
		{
			if (in != null)
				try
				{
					in.close();
				}
				catch (IOException e1)
				{
					// Secondary Exception ignored
				}
		}
	}

	public boolean isReadOnly(PackageId packageId)
	{
		boolean isFileReadOnly;
		boolean isPackageFolderReadOnly;
		boolean librarySourceURLExists;
		
		String path = getPackageFilePath(packageId);
		
		try
		{
			isFileReadOnly = isFileReadOnly(packageId);
			isPackageFolderReadOnly = isPackageFolderReadOnly(packageId);
			librarySourceURLExists = getLibrary(path) != null && !exists(path);
		}
		catch (IOException e)
		{
			throw new ModelException("Failed to check status for package " + packageId);
		}
		
		return isFileReadOnly ||  isPackageFolderReadOnly || librarySourceURLExists;
	}

	public Library getLibrary(String path)
	{
		for (Library lib: libraries)
		{
			if (lib.contains(path))
				return lib;
		}
		return null;
	}
	public byte[] readLibraryResource(String path) throws IOException
	{
		Library lib = getLibrary(path);
		if (lib != null)
			return lib.readModelsFile(path);
		else
			return null;
	}

	public byte[] readLocalResource(String path) throws IOException
	{
		return read(path);
	}

/*	protected URL getResourceURL(String path)
	{
		return getLibraryResourceURL(path);
	}

	private URL getLibraryResourceURL(String path)
	{
		for (int i = 0; i < libraries.size(); i++)
		{
			URL url = libraries.get(i).getResource(path);
			if (url != null)
				return url;
		}
		return null;
	}
*/
	public void save(Package pkg)
	{
		if (pkg == null)
			throw new IllegalArgumentException("Inavlid argument 'null' to save(Package)");
		try
		{
			if (Misc.ASSERTIONS)
				Misc.assertion(!pkg.isDeleted());
		
			ByteArrayOutputStream outputBytes = new ByteArrayOutputStream();
			PackageFormat format = new PackageFormat(this);
			if (pkg.getLineSeparator() == null)
				pkg.setLineSeparator(getLineSeparator()); // If the
			// package doesn't have a set line separator, use the repository's default
			format.write(pkg, outputBytes);
			writeResource(getPackageFilePath(pkg.getPackageId()), outputBytes.toByteArray());
			
			pkg.setSavedPackageId(pkg.getPackageId());

			// Clear modified models.
			for (Model model : pkg.getModels())
			{
				model.clearModified();
			}
		}
		catch (Exception e)
		{
			throw new ModelException("Failed to save package '" + pkg.getPackageId() + "'", e);
		}
	}

	/**
	 * Moves any files belonging to a specifc package (not sub-packages)
	 * 
	 * @param pkg
	 * 
	 */
	protected void move(Package pkg)
	{
		throw new NotImplementedException("move() not implemented in " + getClass().getName());
	}

	private void delete(Package pkg)
	{
		try
		{
			PackageId savedPackageId = pkg.getSavedPackageId();
			deletePackageFile(savedPackageId);
			deletePackageFolder(savedPackageId);
		}
		catch (Exception e)
		{
			throw new ModelException("Failed to delete package '" + pkg.getPackageId() + "'", e);

		}
	}

	synchronized public void saveAllModifiedPackages(ISaveMonitor monitor)
	{
		setSaveInProgress(true);

		ArrayList<SaveFailedException> errors = new ArrayList<SaveFailedException>();

		ArrayList<Package> deleteList = new ArrayList<Package>();
		ArrayList<Package> moveList = new ArrayList<Package>();
		List<Package> saveList = new ArrayList<Package>();

		for (Package pkg : newPackageSet)
		{
			saveList.add(pkg);
		}

		for (Package pkg : modifiedPackageMap.values())
		{
			if (pkg.isDeleted() || pkg.isMoved())
				deleteList.add(pkg);

			if (pkg.isMoved() && !pkg.isDeleted())
			{
				moveList.add(pkg);
			}

			if (!pkg.isDeleted())
			{
				saveList.add(pkg);
			}
		}

		for (Package pkg : deleteList)
		{
			if (monitor != null)
				monitor.saving(pkg.getPackageId());
			try
			{
				delete(pkg);
				removeModifiedPackage(pkg.getSavedPackageId());
			}
			catch (Exception e)
			{
				errors.add(new SaveFailedException(
						"Failed to delete package " + pkg.getPackageId(), e));
			}
		}

		if (monitor != null)
			monitor.begin(moveList.size() + saveList.size() + deleteList.size());

		for (Package pkg : moveList)
		{
			try
			{
				if (monitor != null)
					monitor.saving(pkg.getPackageId());
				move(pkg);
			}
			catch (Exception e)
			{
				errors.add(new SaveFailedException("Failed to move package " + pkg.getPackageId(),
						e));
			}
		}

		for (Package pkg : saveList)
		{
			if (monitor != null)
				monitor.saving(pkg.getPackageId());
			try
			{
				save(pkg);
			}
			catch (Exception e)
			{
				errors.add(new SaveFailedException("Failed to save package " + pkg.getPackageId(),
						e));
			}
		}

		modifiedPackageMap.clear();
		newPackageSet.clear();

		setSaveInProgress(false);
		if (errors.size() > 0)
		{
			throw new SaveFailedException("Failed to save some packages", errors
					.toArray(new Exception[0]));
		}
	}

	/**
	 * Writes a resource to the underlying storgage system
	 * 
	 * @param path
	 *            a String that identifies the resource
	 * @param bytes
	 *            the content of the resource
	 */
	abstract protected void writeResource(String path, byte bytes[]) throws IOException;

	public boolean isSaveInProgress()
	{
		return saveInProgress;
	}

	protected void setSaveInProgress(boolean saveInProgress)
	{
		this.saveInProgress = saveInProgress;
	}

	public void addLibrary(File libraryFile)
	{
		if (libraryFile == null)
			return;
		if (!libraryFile.exists())
			return;
		for (int i = 0; i < libraries.size(); i++)
		{
			if (libraryFile.equals(libraries.get(i).getSourceFile()))
				return;
		}
		libraries.add(new Library(libraryFile));
	}

	public void addLibraries(String libraryPaths)
	{
		StringTokenizer tokenizer = new StringTokenizer(libraryPaths, LIBARAY_PATH_SEPARATOR);
		while (tokenizer.hasMoreTokens())
		{
			String path = tokenizer.nextToken().trim();
			addLibrary(new File(path));
		}
	}

	public List<PackageId> getPackageHierarchy(PackageId rootId)
	{
		ArrayList<PackageId> packageIds = new ArrayList<PackageId>();
		addPackageHierarchy(rootId, packageIds);
		return packageIds;
	}

	private void addPackageHierarchy(PackageId rootId, List<PackageId> packageIds)
	{
		if (rootId != null)
			packageIds.add(rootId);
		List<PackageId> subPackageIds = getSubPackageIds(rootId);
		for (Iterator<PackageId> i = subPackageIds.iterator(); i.hasNext();)
		{
			PackageId subPackageId = i.next();
			addPackageHierarchy(subPackageId, packageIds);
		}

	}

	public String getLineSeparator()
	{
		return lineSeparator;
	}

	public void setLineSeparator(String lineSeparator)
	{
		this.lineSeparator = lineSeparator;
	}

	synchronized public ModelId getModelIdOld(String idstr)
	{
		if (idstr == null)
			return null;
		ModelId id = cachedModelIds.get(idstr);
		if (id == null)
		{
			id = new ModelId(idstr);
			cachedModelIds.put(idstr, id);
		}
		return id;
	}

	synchronized public ModelId getModelId(String path)
	{
		if (path == null)
			return null;
		ModelId id = cachedModelIds.get(path);
		if (id == null)
		{
			int lastSlash = path.lastIndexOf('/');
			if (lastSlash < 0)
				throw new IllegalArgumentException("Wrong modelId format '" + path
						+ "' (missing '/')");
			final String name = path.substring(lastSlash + 1);
			final String packagePath = path.substring(0, lastSlash);
			PackageId packageId = getPackageId(packagePath);
			id = new ModelId(packageId, name);
			cachedModelIds.put(path, id);
		}
		return id;
	}

	synchronized public PackageId getPackageId(String path)
	{
		PackageId id = cachedPackageIds.get(path);
		if (id == null)
		{
			id = new PackageId(path);
			cachedPackageIds.put(path, id);
		}
		return id;
	}

	public MetaProperty getStringMetaPropery(String propertyName)
	{
		MetaProperty p = cachedMetaProperties.get(propertyName);
		if (p == null)
		{
			p = new MetaProperty(propertyName, String.class, true, true, true);
			cachedMetaProperties.put(propertyName, p);
		}
		return p;
	}

	synchronized public Path getPath(String path)
	{
		Path p = cachedPaths.get(path);
		if (p == null)
		{
			p = new Path(path);
			cachedPaths.put(path, p);
		}
		return p;
	}

	public void modelObjectChanged(ModelObject object)
	{
		Model model = null;
		if (object instanceof Model)
			model = (Model) object;
		else
			model = ((ModelElement) object).getParentModel();
		if (model != null && index != null)
		{
			index.markDirty(model.getId());
		}

	}

	public void renameModel(Model model, ModelId newModelId, int indexInPackage)
	{
		if (Misc.ASSERTIONS)
			Misc.assertion(model.getRepository() == this);
		Package oldPackage = model.getPackage();
		Package newPackage = getPackage(newModelId.getPackageId());

		if (newPackage == null)
			newPackage = newPackage(newModelId.getPackageId());
		// else - no need to call packageModified(). It is called by model.resetId(newModelId)

		markModelDirtyInIndex(model.getModelId());
		model.resetId(newModelId);
		if (oldPackage != newPackage)
		{
			oldPackage.removeModel(model);
			newPackage.addModel(model, indexInPackage, true);
			
		}
	}

	protected void removePackageFromCache(Package pkg)
	{
		pkg.setDeleted(true);
		cachedPackages.remove(pkg.getPackageId());

		if (newPackageSet.contains(pkg))
			newPackageSet.remove(pkg);

		for (int i = 0; i < pkg.getModels().size(); i++)
		{
			Model model = (Model) pkg.getModels().get(i);
			markModelDirtyInIndex(model.getModelId());
		}
	}

	public Package getCachedPackage(PackageId packageId)
	{
		return cachedPackages.get(packageId);
	}

	protected boolean modelExists(ModelId id)
	{
		return getModel(id, true) != null;
	}

	protected void addPackageToCache(Package pkg)
	{
		cachedPackages.put(pkg.getPackageId(), pkg);
		pkg.setDeleted(false);
	}

	protected boolean isRepositoryIndexReady()
	{
		return index != null && !index.isDirty();
	}

	protected boolean repositoryIndexExists()
	{
		return index != null;
	}

	synchronized protected RepositoryIndex getUpdatedRepositoryIndex(IProgressMonitor monitor)
	{
		RepositoryIndex updatedIndex = index;
		index = null; // If there's an error, we don't want to keep a corrupt index
		if (updatedIndex == null)
		{
			updatedIndex = new RepositoryIndex();
			updatedIndex.buildIndex(this, monitor);
		}
		else
			updatedIndex.update(monitor);
		index = updatedIndex;
		return index;
	}

	public RepositoryIndex getUpdatedRepositoryIndex()
	{
		return getUpdatedRepositoryIndex(NullProgressMonitor.getInstance());
	}

	protected int getRepositoryIndexDirtyCount()
	{
		return index.countDirtyModels();
	}

	public void updateClientDatabase(boolean useClientDatabase)
	{
		for (PluginDescriptor desc : cachedDescriptors.values())
		{
			if (desc.isDatabasePlugin())
			{
				desc.setServiceClient(!useClientDatabase);
			}
		}
	}

	public List<Library> getLibraries()
	{
		return libraries;
	}
}
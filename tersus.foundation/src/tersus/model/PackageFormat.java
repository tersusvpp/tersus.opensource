/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.model;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import tersus.util.EnumFactory;
import tersus.util.Misc;
import tersus.util.XMLWriter;

// import org.xml.sax.helpers.XMLReaderFactory;

/**
 * 
 * Reads and writes model packages in an XML format.
 * 
 * @author Ofer Brandes and Youval Bronici
 * 
 */

public class PackageFormat
{
    private Repository repository;

    public PackageFormat(Repository repository)
    {
        this.repository = repository;
    }

    private static final String PACKAGE_ID_ATTR = "id";
    private static final String REPOSITORY_VERSION_ATTR = "repositoryVersion";

    public void write(Package pkg, OutputStream out) throws IOException
    {
    	XMLWriter xmlWriter = new XMLWriter(out, pkg.getLineSeparator());
        xmlWriter.openTag(PACKAGE_TAG);
        xmlWriter.writeAttribute(PACKAGE_ID_ATTR, pkg.getPackageId().getPath());
//        xmlWriter.writeAttribute(REPOSITORY_VERSION_ATTR, String.valueOf(pkg.getRepositoryVersion()));
        // xmlWriter.closeTag();
        for (Model model: pkg.getModels())
        {
            write(model, xmlWriter);
        }
        xmlWriter.endElement();
        xmlWriter.close();
    }

    /**
     * Writes a model as an XML document.
     * 
     * @param model
     *            The model to be "XMLized".
     * @param out
     *            A character stream writer to write the XML representation.
     * @throws IOException
     */
    private void write(Model model, XMLWriter xmlWriter) throws IOException
    {

        // Write element tag (according to the model's type)
        String xmlTag = modelTag(model);
        xmlWriter.openTag(xmlTag);

        // Write element attributes (one per each model property)
        writeProperties(model, xmlWriter);
        // xmlWriter.closeTag();

        // Write sub-elements (one per each model element)
        if (model.getElements() != null)
            for (ModelElement element: model.getElements())
                write(model, element, xmlWriter);

        // Write element end tag
        xmlWriter.endElement();
    }

    /**
     * Writes a model element as an XML sub-element.
     * 
     * @param parent
     *            The model to which the model element belongs.
     * @param element
     *            The element to be "XMLized".
     * @param xmlWriter
     *            The XML Writer used to write the model.
     * @throws IOException
     */

    private void write(Model parent, ModelElement element, XMLWriter xmlWriter)
            throws IOException
    {
        // Write element tag (according to the element's type)
        String xmlTag = elementTag(parent, element);
        xmlWriter.openTag(xmlTag);

        // Write element attributes (one per each element property)
        writeProperties(element, xmlWriter);

        // That's all
        xmlWriter.endElement();
    }

    /**
     * Writes the properties of a model object as a series of XML sub-elements.
     * If the property is new and empty (with null value), It won't be written.
     * 
     * @param modelObject
     *            The model or model element whose attributes are to be
     *            "XMLized".
     * @param xmlWriter
     *            The XML Writer used to write the model.
     * @throws IOException
     */
    private void writeProperties(ModelObject modelObject, XMLWriter xmlWriter)
            throws IOException
    {
        List<MetaProperty> properties = modelObject.getMetaProperties();
        for (int i = 0; i < properties.size(); i++)
        {
            MetaProperty property = (MetaProperty) properties.get(i);
            if (property.name.equals(BuiltinProperties.TYPE)
                    && !(modelObject instanceof DataElement))
                continue; // The XML tag (usually) expresses the type
            if (modelObject.isPropertySet(property.name))
            {
                Object propertyValue = modelObject.getProperty(property.name);
                // We are writing all properties (including null values, which may server as placehodlers for future values)
                xmlWriter.writeAttribute(property.name, propertyValue);
            }
        }
    }

    private static final String FLOW_MODEL_SUFFIX = "Model";

    /**
     * Creates an XML element tag according to the type of a model (Different
     * for each type of flow model, one for all data types).
     * 
     * @param model
     *            A model (a flow model or a data type).
     * @return An XML element tag corresponding to the model's type.
     */
    private String modelTag(Model model)
    {
        String type;
        String tag = null;

        if (model instanceof FlowModel)
        {
            type = ((FlowModel) model).getType().toString();
            tag = type + FLOW_MODEL_SUFFIX;
        }
        else if (model instanceof DataType)
        {
            tag = Misc.getShortName(DataType.class);
        }

        if (Misc.ASSERTIONS)
            Misc.assertion(tag != null);
        return (tag);
    }

    private static final String IO_TAG = "IOTrigger";

    private static final String ERROR_TAG = "ErrorExit";

    private static final String SUB_SYSTEM_TAG = "SubSystem";

    private static final String TOP_PROCESS_TAG = "TopProcess";

    private static final String SUB_PROCESS_TAG = "SubProcess";

    public static final String PACKAGE_TAG = "Package";

    /**
     * Creates an XML element tag according to the type of a model element
     * (Different for each type of flow element and each type of slot, one for
     * all data elements, one for all links).
     * 
     * @param parent
     *            The model to which the model element belongs.
     * @param element
     *            The model element.
     * @return An XML element tag corresponding to the model element's type.
     */
    private String elementTag(Model parent, ModelElement element)
    {
        String tag = "?";

        if (element instanceof Slot)
        {
            if (SlotType.ERROR.equals(((Slot) element).getType()))
                tag = ERROR_TAG;
            else if (SlotType.IO.equals(((Slot) element).getType()))
                tag = IO_TAG;
            else
                tag = ((Slot) element).getType().toString();
        }
        else if (element instanceof DataElement)
        {
            tag = Misc.getShortName(DataElement.class);
        }
        else if (element instanceof SubFlow)
        {
        	FlowModel subModel = (FlowModel)((SubFlow) element).getReferredModel();
            if (subModel == null)
            	tag = SUB_PROCESS_TAG;
            else if (FlowType.SYSTEM.equals(subModel.getType()))
                tag = SUB_SYSTEM_TAG;
            else if (FlowType.SYSTEM.equals(((FlowModel) parent).getType()))
                tag = TOP_PROCESS_TAG;
            else
                tag = SUB_PROCESS_TAG;
        }
        else
        {
            if (Misc.ASSERTIONS)
                Misc.assertion(element instanceof Link
                        || element instanceof Note);
            tag = Misc.getShortName(element.getClass());
        }

        return (tag);
    }

    /**
     * Creates a model according to its XML representation.
     * 
     * @param serializedModel
     *            A character stream containing the XML representation of model
     *            (typically created by 'XmlFormat.write()')
     * @return The model created from the XML representation.
     * @throws IOException
     * @throws SAXException
     */

    public Package readPackage(InputStream serializedModel) throws IOException,
            SAXException

    {
    	XMLReader xr = XMLReaderFactory.createXMLReader();
        PackageContentHandler parser = new PackageContentHandler();
        xr.setContentHandler(parser);
        xr.setErrorHandler(parser);

        try
        {
            xr.parse(new InputSource(serializedModel));
        }
        catch (SAXException e)
        {
            Exception e1 = e.getException();
            if (e1 != null)
            {
                e1.printStackTrace();
                if (e1 instanceof RuntimeException)
                    throw (RuntimeException) e1;
            }
            throw e;
        }

        return (parser.pkg);
    }

    /**
     * 
     * A SAX handler to parse an XML document representing a model while
     * creating the model represented by it.
     * 
     */

    // NICE3 Algorithm needs to be better documented (e.g. "memeory" implemented
    // through the 'model', 'modelElement' and 'modelObject' fields).
    private class PackageContentHandler extends DefaultHandler
    {

        protected Package pkg = null;

        protected Model model = null;

        private ModelElement modelElement = null;

        private ModelObject modelObject = null;



        /**
         * 
         * Creates an instance of a SAX handler that parses an XML document
         * while creating the model represented by it.
         * 
         */
        private PackageContentHandler()
        {
            super();
        }

        /**
         * Handles a notification of the start of an XML element.
         * 
         * The start of an XML element triggers the creation of a model object
         * (a model or a model element).
         * 
         * Each XML attribute of the XML element yields a property of the model
         * object.
         * 
         * @param uri
         *            Namespace URI (ignored).
         * @param name
         *            Element's local name (ignored).
         * @param qName
         *            Element's raw XML 1.0 name (@see modelTag(),
         *            elementTag()).
         * @param atts
         *            The element's attributes.
         */
        public void startElement(String uri, String name, String qName,
                Attributes atts)
        {

            if (qName.endsWith(PackageFormat.PACKAGE_TAG))
            {
                pkg = new Package(repository);
                
                PackageId packageId = repository.getPackageId(atts.getValue(PACKAGE_ID_ATTR));
                pkg.setId(packageId);
                String version = atts.getValue(REPOSITORY_VERSION_ATTR);
                if (version != null)
                    pkg.setRepositoryVersion(Double.parseDouble(version));
                else
                    pkg.setRepositoryVersion(0);
                return;
            }
            else if (qName.endsWith(PackageFormat.FLOW_MODEL_SUFFIX)) // FlowModel
            {
                int suffixLength = PackageFormat.FLOW_MODEL_SUFFIX.length();
                String type = qName.substring(0, qName.length() - suffixLength);
                EnumFactory flowTypeFactory = new EnumFactory(FlowType.class);

                FlowType flowType = (FlowType) flowTypeFactory.getValue(type);
                model = new FlowModel();
                ((FlowModel) model).setType(flowType);

                modelObject = model;
            }
            else if (qName.equals(Misc.getShortName(DataType.class))) // DataType
            {
                model = new DataType();
                modelObject = model;
            }
            else
            // Model Element
            {
                if (qName.equals(Misc.getShortName(Link.class))) // Link
                {
                    modelElement = new Link();
                }
                else if (qName.equals(PackageFormat.SUB_SYSTEM_TAG)
                        || qName.equals(PackageFormat.TOP_PROCESS_TAG)
                        || qName.equals(PackageFormat.SUB_PROCESS_TAG)) // Sub-Flow
                {
                    modelElement = new SubFlow();
                }
                else if (qName.equals(Misc.getShortName(DataElement.class))) // Data
                // Element
                {
                    modelElement = new DataElement();
                }
                else if (qName.equals(Misc.getShortName(Note.class)))
                {
                    modelElement = new Note();

                }
                else
                {
                    // Slot
                    SlotType slotType = null;

                    if (qName.equals(PackageFormat.IO_TAG))
                        slotType = SlotType.IO;
                    else if (qName.equals(PackageFormat.ERROR_TAG))
                        slotType = SlotType.ERROR;
                    else
                    {
                        EnumFactory slotTypeFactory = new EnumFactory(
                                SlotType.class);
                        slotType = (SlotType) slotTypeFactory.getValue(qName);
                    }

                    modelElement = new Slot();
                    ((Slot) modelElement).setType(slotType);
                }

                modelObject = modelElement; // Assuming no need to "pop" again
                // to the Model!
            }

            for (int i = 0; i < atts.getLength(); i++)
            {

                String propertyName = atts.getQName(i);
                MetaProperty metaProperty = modelObject
                        .getMetaProperty(propertyName);
                if (metaProperty == null
                        && BuiltinProperties.REF_ID.equals(propertyName))
                    continue; 
                if (metaProperty == null)
                {
                    metaProperty = repository.getStringMetaPropery(propertyName);
                    modelObject.addMetaProperty(metaProperty);
                }
                if (metaProperty.name == BuiltinProperties.NAME)
                    continue; // Name is part of the id
                Object value;
                    
                String attrValue = atts.getValue(i);
                if ("".equals(attrValue))
                    value = null;
                else if (ModelId.class.isAssignableFrom(metaProperty.valueClass))
                    value = repository.getModelId(attrValue);
                else if (Path.class.isAssignableFrom(metaProperty.valueClass))
                    value = repository.getPath(attrValue);
                else
                    value = metaProperty.parseValue(attrValue);
                modelObject.setProperty(metaProperty.name, value);
            }
        }

        /**
         * Handles a notification of the end of an XML element.
         * 
         * The end of an XML element triggers making the current model element
         * an element of the containing model (unless it is the top model, in
         * which case parsing is done).
         * 
         * @param uri
         *            Namespace URI (ignored).
         * @param name
         *            Element's local name (ignored).
         * @param qName
         *            Element's raw XML 1.0 name (@see modelTag(),
         *            elementTag()).
         */
        public void endElement(String uri, String name, String qName)
        {
            if (modelElement != null) // Model Element
            {
                model.addElement(modelElement);
                modelElement = null;
            }
            else if (model != null) // Model
            {
                pkg.addModel(model, false);
                model = null;
            }
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.xml.sax.ErrorHandler#error(org.xml.sax.SAXParseException)
         */
        public void error(SAXParseException exception) throws SAXException
        {
            exception.printStackTrace();
            super.error(exception);
        }

    }
}
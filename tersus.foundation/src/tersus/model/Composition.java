/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;

import tersus.util.Enum;

/**
 * 
 * Composition methods of data types (Atomic, Concatenation, etc.)
 * 
 * @author Youval Bronicki
 * @author Ofer Brandes
 * 
 */

public class Composition extends Enum
{
	public static Composition ATOMIC= new Composition("Atomic"); 
	public static Composition CONCATENATION= new Composition("Concatenation"); 
	public static Composition COLLECTION= new Composition("Collection");
	public static Composition SELECTION= new Composition("Selection");
	
	/** All possible composition methods (Atomic, Concatenation, etc.) */
	public static Composition[] values = {ATOMIC,CONCATENATION,COLLECTION, SELECTION};
	 
	/**
	 * Creates a Composition instance given its string representation.
	 * 
	 * @param value String representation of a composition method (e.g. "Atomic") 
	 */
	private Composition(String value)
	{
		super(value);
	}
	
	/* (non-Javadoc)
	 * @see tersus.util.Enum#getValues()
	 */
	protected Enum[] getValues()
	{
		return values;
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import tersus.util.InvalidInputException;
import tersus.util.PropertyChangeListener;

public abstract class ModelObject implements Serializable
{
	static final long serialVersionUID = 1;

	private HashMap<String,Object> properties = new HashMap<String,Object>();
	private List<MetaProperty> metaProperties;
	private List<PropertyChangeListener> listeners = null;


	synchronized public void addPropertyChangeListener(PropertyChangeListener l)
    {
		if (listeners == null)
			listeners = new ArrayList<PropertyChangeListener>();
		
		if (!listeners.contains(l))
			listeners.add(l);
    }
	
	synchronized public void removePropertyChangeListener(PropertyChangeListener l)
	{
		if (listeners != null)
			listeners.remove(l);
	}

	protected void firePropertyChange(String property, Object oldValue, Object newValue)
	{
		List<PropertyChangeListener> ls = Collections.emptyList();
		synchronized (this) /// Need to clone the list of listeners as it may be modified during the iteration
		{
			if (listeners != null)
			{
				ls = new ArrayList<PropertyChangeListener>(this.listeners.size());
				ls.addAll(this.listeners);
			}
		}
		for (PropertyChangeListener l: ls)
		{
			l.propertyChange(this, property, oldValue, newValue);
		}
	
        IRepository r = getRepository();
        if (r != null)
            r.modelObjectChanged(this);
	}

	public void fireStructureChanged(String typeOfChange)
	{
		firePropertyChange(typeOfChange, null, null);
	}

	public void fireReadOnlyStatusChanged()
	{
		firePropertyChange("ReadOnlyStatus", null, null);
	}

	protected abstract void markModified();

	public abstract void setProperty(String key, Object value);
	protected void setPropertyBase(String key, Object value)
	{
        validatePropertyName(key);
        Object oldValue = getProperty(key);
		MetaProperty metaProperty = getMetaProperty(key);
		/** TODO Here we're creating missing meta properties on the fly.  We should probably ensure in advance that they
		 exist and then change the code here to throw an exception.   See TODO note in in Repository.newModel */
		if (metaProperty == null && value != null)
		{
			metaProperty = new MetaProperty(key, value.getClass(), true, true, true);
			addMetaProperty(metaProperty);
		}

		properties.put(key, value);
		
		firePropertyChange(key, oldValue, value);
		markModified();
		return;

	}


	public abstract Object getProperty(String key);
	protected  Object getPropertyBase(String key)
	{
		return properties.get(key);
	}

	public List<MetaProperty> getMetaProperties()
	{
		if (metaProperties == null)
			return getDefaultProperties();
		else
			return metaProperties;
	}

	protected abstract List getDefaultProperties();

	public static void validatePropertyName(String name)
    {
        if (! checkPropertyName(name))
            throw new InvalidInputException("Invalid attribute name '"+name+"'.  Attribute names must begin with a letter or underscore and may only contain letters, digits, dots, hyphens and underscores");
    }

    public static boolean checkPropertyName(String name)
    {
    	if (name == null || name.length() == 0)
    		return false;
    	if (!Character.isLetter(name.charAt(0)) && !(name.charAt(0) == '_'))
    		return false;
    	for (int i=1;i<name.length(); i++)
    	{
    		char c  = name.charAt(i);
    		if (! (Character.isLetter(c) || Character.isDigit(c) || c == '.' || c == '_' || c == '-'))
    				return false;
    	}
    	return true;
    }

    /**
	 * Returns a MetaProperty for a property with the given name
	 * @param name The name of the property
	 * @return A MetaProperty that describes the given property, or null if there is no such property
	 */
	public MetaProperty getMetaProperty(String name)
	{
		List metaProperties = getMetaProperties();
		for (int i = 0; i < metaProperties.size(); i++)
		{
			MetaProperty property = (MetaProperty) metaProperties.get(i);
			if (property.name.equals(name))
				return property;
		}
		return null;
	}

	public void removeMetaProperty(String name)
	{
	    if (metaProperties == null) 
        {
            metaProperties = new ArrayList();
            metaProperties.addAll(getDefaultProperties());
        }
	    
	    MetaProperty propertyToRemove = null;
	    for (MetaProperty property: metaProperties)
        {
            if (property.name.equals(name) && property.canDelete())
            {
                propertyToRemove = property;
                break;
                
            }
        }
	    if (propertyToRemove != null)
	    {
	        metaProperties.remove(propertyToRemove);
            fireStructureChanged(BuiltinProperties.PROPERTIES);
            markModified();
	    }
	}
	
	public boolean addMetaProperty(MetaProperty metaProperty)
    {
        validatePropertyName(metaProperty.name);
        if (metaProperties == null) 
        {
            metaProperties = new ArrayList();
            metaProperties.addAll(getDefaultProperties());
        }
        
        boolean metaPropertyExists = isMetaPropertyExists(metaProperty.getDisplayName());
        
        if (metaPropertyExists)
            return false;
            
        metaProperties.add(metaProperty);
        fireStructureChanged(BuiltinProperties.PROPERTIES);
        markModified();
        return true;
    }
	
	public boolean isMetaPropertyExists(String newPropertyName)
    {
        for (MetaProperty metaProperty: getMetaProperties())
        {
            if (metaProperty.getDisplayName().equals(newPropertyName))
               return true;
        }
        
        if (properties.containsKey(newPropertyName))
            return true;
        
        return false;
    }
	
	public void removeProperty(String name)
	{
	    properties.remove(name);
	}
	
	public void addProperty(String name, Object value)
    {
        properties.put(name, value);
    }
	
	/**
	 * @param name the name of the property
	 * @return
	 */
	public abstract boolean isPropertySet(String key);
	protected boolean isPropertySetBase(String key)
	{
		return getProperty(key) != null;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#finalize()
	 */

    /**
     * @return
     */
    public abstract IRepository getRepository();
    
    public Map getProperties(boolean includeUnsetProperties)
    {
    	HashMap properties = new HashMap();
    	for (Iterator i = getMetaProperties().iterator(); i.hasNext(); )
    	{
    		MetaProperty p = (MetaProperty)i.next();
    		if (includeUnsetProperties || isPropertySet(p.name))
    			properties.put(p.name, getProperty(p.name));
    	}
    	return properties;
    }
    
    protected void illegalValue(String key, Object value)
    {
    	String classComment = "";
    	if (value != null)
    		classComment=" ["+value.getClass().getName()+"]" ;
		throw new IllegalArgumentException("Illegal value '" + value + "'" + classComment
				+ "  in class '" + getClass().getName());
	}
}

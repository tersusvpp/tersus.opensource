/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;

import tersus.util.Enum;

/**
 * 
 */
public class SlotType extends Enum
{
	public static final SlotType IO = new SlotType("Input Output Trigger");
	public static final SlotType TRIGGER = new SlotType("Trigger");
	public static final SlotType INPUT = new SlotType("Input");
	public static final SlotType EXIT = new SlotType("Exit");
	public static final SlotType ERROR = new SlotType("Error Exit");

	public static final SlotType[] values = { TRIGGER, INPUT, EXIT, IO, ERROR };

	/**
	 * @param value
	 */
	private SlotType(String value)
	{
		super(value);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see tersus.util.Enum#getValues()
	 */
	protected Enum[] getValues()
	{
		// TODO Review this auto-generated method stub
		return values;
	}

	/**
	 * @return
	 */
	public boolean isInput()
	{
		return this == INPUT || this == TRIGGER;
	}
	public boolean isOutput()
	{
		return this == EXIT || this == ERROR;
	}

	/**
	 * @return
	 */
	public boolean isTrigger()
	{
		return this == TRIGGER || this == IO;
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;

import java.io.Serializable;

/**
 * 
 */
public class RelativePosition implements Serializable
{
	static final long serialVersionUID = 1;
	private double x;
	private double y;
	/**
	 * 
	 */
	public RelativePosition(double x, double y)
	{
		this.x = Precision.round(x);
		this.y = Precision.round(y);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj)
	{
		if (obj instanceof RelativePosition)
			return equals((RelativePosition)obj);
		else
			return false;
	}

	public boolean equals(RelativePosition other)
	{
		return other != null && x == other.x && y==other.y;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		return x+","+y;
	}

	public static RelativePosition valueOf(String serialization)
	{
		RelativePosition position = new RelativePosition(0,0);

		// TODO Add validity checks				
		int commaPlace = serialization.indexOf(",");
		position.setX(Double.parseDouble(serialization.substring(0,commaPlace)));
		position.setY(Double.parseDouble(serialization.substring(commaPlace+1)));
		
		return (position);
	}
	

	public void setX(double x)
	{
		this.x = Precision.round(x);
	}

	public double getX()
	{
		return x;
	}

	public void setY(double y)
	{
		this.y = Precision.round(y);
	}

	public double getY()
	{
		return y;
	}

	/**
	 * @return
	 */
	public RelativePosition getCopy()
	{
		RelativePosition p = new RelativePosition(x,y);
		return p;
	}

}

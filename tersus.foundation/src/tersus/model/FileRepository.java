/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tersus.util.FileUtils;

public class FileRepository extends Repository
{
	public FileRepository()
	{
		cachedPackages = new SimplePackageCache();
	}
	
    public String toString()
    {
        return "File Repository ["+root.getPath()+"]";
    }
    /**
     * The root of the repository (i.e. the folder that contains all models)
     */
    private File root;

    public void save(ModelId modelId, byte[] bytes)
    {
        String path = modelId.getPath();
        try
        {
            writeResource(path, bytes);
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            throw new RuntimeException(e);
        }
    }

    public void writeResource(String path, byte[] bytes) throws IOException
    {
        FileOutputStream out;
        out = new FileOutputStream(getFile(path));
        out.write(bytes);
        out.close();

    }

    private File getFile(String path)
    {
        return new File(root, path);
    }
    
    public byte[] read(String path) throws IOException
    {
        File file = getFile(path);
        if (!file.exists())
        {
            return null;
        }
        FileInputStream in = new FileInputStream(file);
        byte[] data = FileUtils.readBytes(in, true);
        return data;
    }
    protected boolean exists(String path) throws IOException
    {
        File file = getFile(path);
        return file.exists();
    }

    /**
     * @return a File pointing to the repository's root folder
     */
    public File getRoot()
    {
        return root;
    }

    /**
     * Sets the root of the repository to a specified folder
     * 
     * @param rootFolder
     *            A java.io.file pointing to the repository root folder
     */
    public void setRoot(File rootFolder)
    {
        root = rootFolder;
    }

    protected void delete(String path) throws IOException
    {
        File file = getFile(path);
        if (file.exists())
            file.delete();

    }

    protected List getSavedSubPackageIds(PackageId parent)
    {
        File packageFolder = getPackageFolder(parent);
        if (! packageFolder.isDirectory())
            return Collections.EMPTY_LIST;
        String[] children = packageFolder.list();
        if (children.length == 0)
            	return Collections.EMPTY_LIST;
        ArrayList subPackageIds = new ArrayList();
        for (int i = 0; i < children.length; i++)
        {
            String childName = children[i];
            if (ignoredFiles.contains(childName))
                continue;
            File child = new File(packageFolder, childName);
            PackageId subPackageId = null;
            if (child.isDirectory())
            {
                subPackageId = new PackageId(parent, childName);
            }
            else if (child.isFile() && childName.endsWith(PACKAGE_SUFFIX))
            {
                subPackageId = new PackageId(parent, childName.substring(0,childName.lastIndexOf(PACKAGE_SUFFIX)));
            }
            if (subPackageId != null && ! subPackageIds.contains(subPackageId))
                subPackageIds.add(subPackageId);
        }
        return subPackageIds;
    }

    private File getPackageFolder(PackageId packageId)
    {
        File packageFolder = packageId == null? getRoot(): getFile(packageId.getPath());
        return packageFolder;
    }
    
    public boolean packageFolderExists(PackageId packageId)
    {
        return getPackageFolder(packageId).isDirectory();
    }
    protected void deletePackageFile(PackageId id) throws IOException
    {
        File file = getFile(getPackageFilePath(id));
        if (file.exists())
            file.delete();
    }

	public boolean isFileReadOnly(PackageId packageId) throws IOException 
	{
		File file = getFile(getPackageFilePath(packageId));
		
		if (file != null)
			return !file.canWrite() && file.canRead();
		
		return false;
	}
	
	public boolean isPackageFolderReadOnly(PackageId packageId) throws IOException 
	{
		File folder = getPackageFolder(packageId);
		
		if (folder != null)
			return !folder.canWrite() && folder.canRead();
		
		return false;
	}
}
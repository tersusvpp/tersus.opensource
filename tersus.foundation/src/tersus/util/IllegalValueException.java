/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.util;

/**
 * 
 */
public class IllegalValueException extends RuntimeException
{
	public IllegalValueException()
	{
		super();
	}

	public IllegalValueException(String message)
	{
		super(message);
	}

	public IllegalValueException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public IllegalValueException(Throwable cause)
	{
		super(cause);
	}
}

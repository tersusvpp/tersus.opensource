/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.util;

import java.io.*;
import java.util.Stack;
/**
 * A simple XML writer (adapted from org.eclipse.core.internal.resources.XMLWriter)
 */
public class XMLWriter
{
	private static final String DEFAULT_LINE_SEPARATOR = System.getProperty("line.separator");
    protected int tab;
	Stack<String> tagStack;
	boolean tagOpen = false;
	private boolean addWhiteSpace = true;
	private boolean wroteContent = false;
	private String lineSeparator = DEFAULT_LINE_SEPARATOR;
	/* constants */
	protected static final String XML_VERSION = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"; //$NON-NLS-1$
	private static final String CDATA_START = "<![CDATA[";
	private static final String CDATA_END = "]]>";
	
	Writer writer;
    public XMLWriter(OutputStream output) throws IOException
    {
        this(output, DEFAULT_LINE_SEPARATOR, true);
    }
	public XMLWriter(OutputStream output, String lineSeparator) throws IOException
	{
		this(output, lineSeparator, true);
	}
	public XMLWriter(OutputStream output, String lineSeparator, boolean addXMLDeclaration) throws IOException
	{
		this(new BufferedWriter(new OutputStreamWriter(output, "UTF8")), lineSeparator, addXMLDeclaration); //$NON-NLS-1$
	}
	public XMLWriter(Writer out) throws IOException
	{
		this (out, DEFAULT_LINE_SEPARATOR, true);
	}
	public XMLWriter(Writer out, String lineSeparator, boolean addXMLDeclaration) throws IOException
	{
		this.writer = out;
		setLineSeparator(lineSeparator);
		tab = 0;
		tagStack = new Stack<String>();
		if (addXMLDeclaration)
		{
			write(XML_VERSION);
		}
	}
	
	public void printTabulation() throws IOException
	{
		for (int i = 0; i < tab; i++)
			write('\t');
	}
	public void openTag(String name) throws IOException
	{
		if (tagOpen)
			closeTag();
		if ((addWhiteSpace && !wroteContent))
		{
			newLine();
			if (addWhiteSpace)
				printTabulation();
		}
		wroteContent = false;
		tagOpen = true;
		write('<'); //$NON-NLS-1$
		write(name);
		tab++;
		tagStack.push(name);
	}

	public void writeAttribute(String name, Object value) throws IOException
	{
		if (!tagOpen)
			throw new IllegalStateException("Can't write attribute " + name + " (no tag is open)");
		if (value == null)
			return;
		write(' ');
		write(name);
		write("=\"");
		writeEscaped(value);
		write('"');
	}
	StringBuffer tmp = new StringBuffer();
	public void writeAttribute(String name, long value) throws IOException
	{
		if (!tagOpen)
			throw new IllegalStateException("Can't write attribute " + name + " (no tag is open)");
		write(' ');
		write(name);
		write("=\"");
		tmp.setLength(0);
		tmp.append(value);
		write(tmp);
		write('"');

	}

	/**
	 * @param tmp
	 */
	private void write(StringBuffer buffer) throws IOException
	{
		for (int i = 0; i < buffer.length(); i++)
			write(buffer.charAt(i));

	}
	/**
	 * Closes an open tag without ending the element
	 */
	public void closeTag() throws IOException
	{
		write('>');
		tagOpen = false;
		wroteContent = false;
	}
	/**
	 * Ends an element by either closing a tag with "/>" (if a tag is open) or writing an end tag (if no tag is open)
	 */
	public void endElement() throws IOException
	{
		String tagName = tagStack.pop();
		tab--;
		if (tagOpen)
		{
			write("/>");
			tagOpen = false;
		}
		else
		{
			if (addWhiteSpace && !wroteContent)
			{
				newLine();
				printTabulation();
			}
			write("</");
			write(tagName);
			write('>');
		}
		wroteContent = false;
	}

	private static void writeEscapedChar(Writer writer,char c) throws IOException
	{
		String replacement = getReplacement(c);
		if (replacement != null)
		{
			writer.write('&');
			writer.write(replacement);
			writer.write(';');
		}
		else
		{
			writer.write(c);
		}
	}
	public void writeEscaped(Object value) throws IOException
	{
		if (value instanceof StringBuffer)
			writeEscaped(writer, (StringBuffer) value);
		else if (value != null)
			writeEscaped(writer, value.toString());
	}
	public static void writeEscaped(Writer writer, String s) throws IOException
	{
		for (int i = 0; i < s.length(); ++i)
			writeEscapedChar(writer, s.charAt(i));
	}

	public static void writeEscaped(Writer writer, StringBuffer s) throws IOException
	{
		for (int i = 0; i < s.length(); ++i)
			writeEscapedChar(writer, s.charAt(i));
	}
	private static String getReplacement(char c) throws IOException
	{
		// Encode special XML characters into the equivalent character references.
		// These five are defined by default for all XML documents.
		switch (c)
		{
			case '<' :
				return "lt"; //$NON-NLS-1$
			case '>' :
				return "gt"; //$NON-NLS-1$
			case '"' :
				return "quot"; //$NON-NLS-1$
			case '\'' :
				return "apos"; //$NON-NLS-1$
			case '&' :
				return "amp"; //$NON-NLS-1$
			case '\n':
			    return "#10";
		}
		return null;
	}
	/**
	 * @throws java.io.IOException
	 */
	private void newLine() throws IOException
	{
		writer.write(getLineSeparator());
		writer.flush();
	}

	/**
	 * @param c
	 * @throws java.io.IOException
	 */
	private void write(int c) throws IOException
	{
		writer.write(c);
	}

	/**
	 * @param str
	 * @throws java.io.IOException
	 */
	private void write(String str) throws IOException
	{
		writer.write(str);
	}

	/**
	 * @throws java.io.IOException
	 */
	public void close() throws IOException
	{
		newLine();
		writer.close();
	}

	/**
	 * @throws java.io.IOException
	 */
	public void flush() throws IOException
	{
		writer.flush();
	}

	/**
	 * @return
	 */
	public boolean isAddWhiteSpace()
	{
		return addWhiteSpace;
	}

	/**
	 * @param b
	 */
	public void setAddWhiteSpace(boolean b)
	{
		addWhiteSpace = b;
	}
    public String getLineSeparator()
    {
        return lineSeparator;
    }
    private void setLineSeparator(String lineSeparator)
    {
        if (lineSeparator != null)
            this.lineSeparator = lineSeparator;
        else
            lineSeparator = DEFAULT_LINE_SEPARATOR;
    }
	public void writeContent(StringBuffer content, boolean cdata) throws IOException
	{
		if (cdata)
		{
			write(CDATA_START);
			write(content);
			write(CDATA_END);
		}
		else
			writeEscaped(content);
		wroteContent = true;
	}

	/**
	 * @return
	 */

}
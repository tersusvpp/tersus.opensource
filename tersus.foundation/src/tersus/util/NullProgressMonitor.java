/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.util;

public class NullProgressMonitor implements IProgressMonitor
{

    public void beginTask(String name, int totalWork)
    {
    }

    public void done()
    {
    }

    public boolean isCancelled()
    {
        return false;
    }

    public void worked(int work)
    {
    }

    private static NullProgressMonitor instance = new NullProgressMonitor();
    public static IProgressMonitor getInstance()
    {
        return instance;
    }

}

/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;

import tersus.model.BuiltinModels;
import tersus.model.ModelId;
import tersus.model.TersusTableFieldDescriptor;

/**
 * @author Liat Shiff
 */
public class SQLUtils
{
	private static final String MICROSOFT_SQL_SERVER = "Microsoft SQL Server";
	public static String PASSWORD_PROPERTY = "password";
	public static String USER_PROPERTY = "username";
	public static String URL_PROPERTY = "url";
	public static String DRIVER_CLASS_NAME = "driverClassName";

	public static Connection connectToDataSource(String dataSourceName, Properties properties)
			throws SQLException
	{
		Connection con = null;

		String user = properties.getProperty(USER_PROPERTY);
		String password = properties.getProperty(PASSWORD_PROPERTY);
		String url = properties.getProperty(URL_PROPERTY);
		String className = properties.getProperty(DRIVER_CLASS_NAME);

		try
		{
			Class.forName(className).newInstance();
			DriverManager.setLoginTimeout(10);
			con = DriverManager.getConnection(url, user, password);
		}
		catch (InstantiationException e)
		{
			e.printStackTrace();
			throw new SQLException("Failed to create driver insatance (InstantiationException)",
					e.getMessage());
		}
		catch (IllegalAccessException e)
		{
			e.printStackTrace();
			throw new SQLException("Failed to create driver insatance (IllegalAccessException)",
					e.getMessage());
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
			throw new SQLException("Failed to create driver insatance (ClassNotFoundException)",
					e.getMessage());
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			throw new RuntimeException("Failed to create connection to DataSource "
					+ dataSourceName, e);
		}

		return con;
	}

	public static HashSet<String> getCatalogNameList(Connection con) throws SQLException
	{
		PreparedStatement ps = null;
		ResultSet rs = null;

		try
		{
			HashSet<String> catalogNameList = new HashSet<String>();
			
			String sql = "SELECT * FROM information_schema.INFORMATION_SCHEMA_CATALOG_NAME";
			String databaseProductName = con.getMetaData().getDatabaseProductName();
			if (MYSQL.equals(databaseProductName))
				return catalogNameList;
			if (MICROSOFT_SQL_SERVER.equals(databaseProductName))
				sql = "SELECT DISTINCT CATALOG_NAME FROM INFORMATION_SCHEMA.SCHEMATA";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			if (rs != null)
			{
				while (rs.next())
				{
					String catalogName = rs.getString("CATALOG_NAME");

					if (!catalogNameList.contains(catalogName))
						catalogNameList.add(catalogName);
				}
			}
			close(rs, ps);
			return catalogNameList;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			throw new SQLException("Failed to create Catalog Name List", e.getMessage());
		}
		finally
		{
			forceClose(rs, ps);
		}
	}

	/**
	 * @return All Schemas in specific database catalog.
	 */
	public static HashSet<String> getSchemaNameList(Connection con, String catalog)
			throws SQLException
	{
		ResultSet rs = null;

		try
		{
			HashSet<String> schemaNameList = new HashSet<String>();

			Statement statement;
			if (MICROSOFT_SQL_SERVER.equals(con.getMetaData().getDatabaseProductName()))
			{
				statement = con.createStatement();
				rs = statement.executeQuery("select CATALOG_NAME, "
						+ "SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA");
				while (rs.next())
				{
					String schemaName = rs.getString("SCHEMA_NAME");
					String catalogName = rs.getString("CATALOG_NAME");

					if (catalogName.equals(catalog))
						schemaNameList.add(schemaName);
				}

				statement.close();
			}
			else
			{
				rs = con.getMetaData().getSchemas();
				while (rs.next())
				{
					String tableSchem = rs.getString("TABLE_SCHEM");
					String tableCatalog = rs.getString("TABLE_CATALOG");
					if (catalog == null || catalog.equals(tableCatalog))
						schemaNameList.add(tableSchem);
				}
			}

			rs.close();
			rs = null;

			return schemaNameList;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			throw new SQLException("Failed to create Schema Name List", e.getMessage());
		}
		finally
		{
			forceClose(rs);
		}
	}

	// TODO Need to add the views options
	public static HashSet<String> getTableNameList(Connection con, String catalog, String schema)
			throws SQLException
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		String schemaCol = getSchemaColumn(con);

		try
		{
			HashSet<String> tableNameList = new HashSet<String>();

			if (catalog != null && schema != null)
			{
				String sql = "SELECT TABLE_NAME FROM " + getTablesViewName(con) + " WHERE "
						+ schemaCol + " = ? AND TABLE_CATALOG = ?"
						+ " UNION SELECT TABLE_NAME FROM " + getViewsViewName(con) + " WHERE "
						+ schemaCol + " = ? AND TABLE_CATALOG = ?";
				ps = con.prepareStatement(sql);
				ps.setString(1, schema);
				ps.setString(2, catalog);
				ps.setString(3, schema);
				ps.setString(4, catalog);
			}
			else if (schema != null)
			{
				String sql = "SELECT DISTINCT TABLE_NAME FROM " + getTablesViewName(con)
						+ " WHERE " + schemaCol + " = ?" + "UNION SELECT DISTINCT TABLE_NAME FROM "
						+ getViewsViewName(con) + " WHERE " + schemaCol + "= ?";
				ps = con.prepareStatement(sql);
				ps.setString(1, schema);
				ps.setString(2, schema);
			}
			else
			{
				String sql = "SELECT DISTINCT TABLE_NAME FROM " + getTablesViewName(con) + " "
						+ "UNION SELECT DISTINCT TABLE_NAME FROM " + getViewsViewName(con);
				ps = con.prepareStatement(sql);
			}

			rs = ps.executeQuery();

			if (rs != null)
			{
				while (rs.next())
				{
					String tableName = rs.getString("TABLE_NAME");

					if (!tableNameList.contains(tableName))
						tableNameList.add(tableName);
				}
			}

			close(rs, ps);

			return tableNameList;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			throw new SQLException("Failed to create Schema Name List", e.getMessage());
		}
		finally
		{
			forceClose(rs, ps);
		}
	}

	private static String getSchemaColumn(Connection con) throws SQLException
	{
		DatabaseMetaData metaData = con.getMetaData();
		if (HSQLDB.equals(metaData.getDatabaseProductName())
				&& metaData.getDatabaseMajorVersion() < 2)
			return "TABLE_SCHEM";
		else
			return "TABLE_SCHEMA";
	}

	private static String getViewsViewName(Connection con) throws SQLException
	{
		DatabaseMetaData metaData = con.getMetaData();

		if (HSQLDB.equals(metaData.getDatabaseProductName())
				&& metaData.getDatabaseMajorVersion() < 2)
			return "information_schema.system_views";
		else
			return "information_schema.views";
	}

	private static String getTablesViewName(Connection con) throws SQLException
	{
		DatabaseMetaData metaData = con.getMetaData();

		if (HSQLDB.equals(metaData.getDatabaseProductName())
				&& metaData.getDatabaseMajorVersion() < 2)
			return "information_schema.system_tables";
		else
			return "information_schema.tables";
	}

	public static HashSet<TersusTableFieldDescriptor> getColumnsParameters(Connection con,
			String catalog, String schemaName, String tableName) throws SQLException
	{
		HashSet<TersusTableFieldDescriptor> fields = new HashSet<TersusTableFieldDescriptor>();
		HashSet<String> primaryKeyColumns = getPrimaryKeyColumns(con, catalog, schemaName,
				tableName);

		for (String str : primaryKeyColumns)
		{
			System.out.println(str + ", ");
		}

		ResultSet tableColumnResultSet = null;
		try
		{
			tableColumnResultSet = con.getMetaData().getColumns(catalog, schemaName, tableName,
					null);

			if (tableColumnResultSet != null)
			{
				while (tableColumnResultSet.next())
				{
					String columnName = tableColumnResultSet.getString("COLUMN_NAME");
					String str = tableColumnResultSet.getString("DATA_TYPE");
					Integer sqlTypeNumber = Integer.valueOf(str);
					ModelId type = convertFromSQLTypeToTersusType(sqlTypeNumber.intValue());
					String columnType = SQLUtils.getSQLTypeName(sqlTypeNumber.intValue());
					String columnSize = tableColumnResultSet.getString("COLUMN_SIZE");
					Boolean nullable = tableColumnResultSet.getBoolean("NULLABLE");
					String defaultValue = getDefaultValue(con, tableColumnResultSet, nullable, type);
					Boolean isPrimaryKey = new Boolean(primaryKeyColumns.contains(columnName));
					String description = tableColumnResultSet.getString("REMARKS");
					int position = tableColumnResultSet.getInt("ORDINAL_POSITION");

					TersusTableFieldDescriptor descriptor = new TersusTableFieldDescriptor(null,
							columnName, columnName, type, columnType, isPrimaryKey, nullable,
							columnSize, defaultValue, description, "V", position);
					fields.add(descriptor);
				}

				tableColumnResultSet.close();
				tableColumnResultSet = null;

				return fields;
			}
		}
		catch (SQLException e)
		{
			throw new SQLException(
					"Failed to obtain list of columns for table '" + tableName + "'",
					e.getMessage());
		}
		finally
		{
			forceClose(tableColumnResultSet);
		}

		return fields;
	}

	public static TersusTableFieldDescriptor getColumnParameters(Connection con,
			String catalogName, String schemaName, String tableName, String columnName)
	{
		ResultSet tableColumnResultSet = null;

		try
		{
			HashSet<String> primaryKeyColumns = getPrimaryKeyColumns(con, catalogName, schemaName,
					tableName);
			tableColumnResultSet = con.getMetaData().getColumns(null, null, tableName, columnName);

			if (tableColumnResultSet != null)
			{
				tableColumnResultSet.next();

				String str = tableColumnResultSet.getString("DATA_TYPE");
				Integer sqlTypeNumber = Integer.valueOf(str);
				String columnType = SQLUtils.getSQLTypeName(sqlTypeNumber.intValue());
				String columnSize = tableColumnResultSet.getString("COLUMN_SIZE");
				Boolean isPrimaryKey = new Boolean(primaryKeyColumns.contains(columnName));
				ModelId type = convertFromSQLTypeToTersusType(sqlTypeNumber.intValue());
				Boolean nullable = tableColumnResultSet.getBoolean("NULLABLE");
				String defaultValue = getDefaultValue(con, tableColumnResultSet, nullable, type);
				String description = tableColumnResultSet.getString("REMARKS");
				int position = tableColumnResultSet.getShort("ORDINAL_POSITION");

				TersusTableFieldDescriptor field = new TersusTableFieldDescriptor(null, columnName,
						columnName, type, columnType, isPrimaryKey, nullable, columnSize,
						defaultValue, description, "V", 0);

				tableColumnResultSet.close();
				tableColumnResultSet = null;

				return field;
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return null;
	}

	private static String getDefaultValue(Connection con, ResultSet tableColumnResultSet,
			Boolean nullable, ModelId type) throws SQLException
	{
		String defaultValue = tableColumnResultSet.getString("COLUMN_DEF");
		if ((defaultValue == null || defaultValue.length() == 0)
				&& "DB2 UDB for AS/400".equals(getDatabaseProductName(con)) && !nullable)
		{
			if (BuiltinModels.NUMBER_ID.equals(type))
				defaultValue = "0";
			else if (BuiltinModels.TEXT_ID.equals(type))
				defaultValue = " ";
		}
		else if (defaultValue != null && "blank".equals(defaultValue.toLowerCase()))
			defaultValue = " ";
		else if (defaultValue != null
				&& (defaultValue.startsWith("'") && defaultValue.endsWith("'")))
		{
			defaultValue = defaultValue.substring(1, defaultValue.length() - 1);
			defaultValue = defaultValue.replaceAll("\"", "\\\"");
			defaultValue = defaultValue.replaceAll("''", "'");
			defaultValue = "\"" + defaultValue + "\"";
		}

		return defaultValue;
	}

	public static HashSet<String> getPrimaryKeyColumns(Connection con, String catalogName,
			String schemaName, String tableName) throws SQLException
	{
		System.out.println(con.isClosed());
		HashSet<String> primaryKeyColumns = new HashSet<String>();
		ResultSet rs = null;

		try
		{
			if ("DB2 UDB for AS/400".equals(getDatabaseProductName(con)))
				rs = con.getMetaData().getIndexInfo(catalogName, schemaName, tableName, true, true);
			else
				rs = con.getMetaData().getPrimaryKeys(catalogName, schemaName, tableName);

			if (rs != null)
			{
				while (rs.next())
				{
					String columnName = rs.getString("COLUMN_NAME");

					if (columnName != null)
						primaryKeyColumns.add(columnName);
				}

				rs.close();
				rs = null;
			}

		}
		catch (SQLException e)
		{
			throw new SQLException(
					"Failed to obtain list of columns for table '" + tableName + "'",
					e.getMessage());
		}
		finally
		{
			forceClose(rs);
		}

		return primaryKeyColumns;
	}

	public static String getDatabaseProductName(Connection connection)
	{
		String databaseProductName;
		try
		{
			DatabaseMetaData metaData = connection.getMetaData();
			databaseProductName = metaData.getDatabaseProductName();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			throw new RuntimeException("Failed to obtain database product name", e);
		}
		return databaseProductName;
	}

	// Force close methods:
	public static void close(ResultSet rs, PreparedStatement ps) throws SQLException
	{
		ps.close();
		ps = null;
		rs.close();
		rs = null;
	}

	public static void forceClose(ResultSet rs, Statement st)
	{
		forceClose(st);
		forceClose(rs);
	}

	public static void forceClose(ResultSet rs)
	{
		if (rs != null)
		{
			try
			{
				rs.close();
				rs = null;
			}
			catch (SQLException e)
			{
				// Exception ignored (EngineException thrown in catch
				// clause)
			}
		}
	}

	public static void forceClose(Connection con)
	{
		if (con != null)
		{
			try
			{
				con.close();
			}
			catch (SQLException e)
			{
				// Exception ignored (EngineException thrown in catch clause)
			}
		}
	}

	public static void forceClose(Statement st)
	{
		if (st != null)
		{
			try
			{
				st.close();
				st = null;
			}
			catch (SQLException e)
			{
				// Exception ignored (EngineException thrown in catch clause)
			}
		}
	}

	private static Map<Integer, String> sqlTypeNameMap;
	private static Map<String, Integer> sqlTypeNumberMap;
	private static final String[] sqlTypeList;
	static
	{
		sqlTypeNameMap = new HashMap<Integer, String>();
		sqlTypeNumberMap = new HashMap<String, Integer>();
		Field[] fields = Types.class.getFields();
		for (Field field : fields)
		{
			if ((field.getModifiers() & Modifier.STATIC) != 0)
			{
				try
				{
					Object value = field.get(null);
					if (value instanceof Integer)
					{
						sqlTypeNameMap.put((Integer) value, field.getName());
						sqlTypeNumberMap.put(field.getName(), (Integer) value);
					}
				}
				catch (IllegalAccessException e)
				{
					// Not expected
				}

			}
		}
		ArrayList<String> list = new ArrayList<String>();
		list.addAll(sqlTypeNumberMap.keySet());
		Collections.sort(list);
		sqlTypeList = list.toArray(new String[]
		{});
	}

	public static String getSQLTypeName(int sqlTypeNumber)
	{
		return sqlTypeNameMap.get(sqlTypeNumber);
	}

	public static Integer getSQLTypeNumber(String sqlTypeName)
	{
		return sqlTypeNumberMap.get(sqlTypeName);
	}

	public static String[] getSQLTypeList()
	{
		return sqlTypeList;
	}

	public static ModelId convertFromSQLTypeToTersusType(int sqlTypeNumber)
	{
		switch (sqlTypeNumber)
		{
			case Types.BIT:
			case Types.TINYINT:
			case Types.SMALLINT:
			case Types.INTEGER:
			case Types.BIGINT:
			case Types.FLOAT:
			case Types.REAL:
			case Types.DOUBLE:
			case Types.NUMERIC:
			case Types.DECIMAL:
			case Types.ROWID:
				return BuiltinModels.NUMBER_ID;
			case Types.CHAR:
			case Types.LONGVARCHAR:
			case Types.VARCHAR:
			case Types.NVARCHAR:
			case Types.NCHAR:
			case Types.LONGNVARCHAR:
			case Types.CLOB:
			case Types.DATALINK:
			case Types.NCLOB:
			case Types.SQLXML:
				return BuiltinModels.TEXT_ID;
			case Types.DATE:
				return BuiltinModels.DATE_ID;
			case Types.TIMESTAMP:
			case Types.TIME:
				return BuiltinModels.DATE_AND_TIME_ID;
			case Types.NULL:
				return BuiltinModels.NOTHING_ID;
			case Types.BOOLEAN:
				return BuiltinModels.BOOLEAN_ID;
			case Types.BINARY:
			case Types.LONGVARBINARY:
			case Types.VARBINARY:
			case Types.BLOB:
				return BuiltinModels.BINARY_ID;
			default:
				return null;
		}

	}
	public static boolean isIntegralType(int sqlTypeNumber)
	{
		switch (sqlTypeNumber)
		{
			case Types.BIT:
			case Types.TINYINT:
			case Types.SMALLINT:
			case Types.INTEGER:
			case Types.BIGINT:
				return true;
			default:
				return false;
		}

	}
	public static Integer convertFromTersusTypeToSQLType(ModelId tersusTypeId)
	{
		if (tersusTypeId.equals(BuiltinModels.BINARY_ID))
			return Types.LONGVARBINARY;
		if (tersusTypeId.equals(BuiltinModels.TEXT_ID))
			return Types.VARCHAR;
		if (tersusTypeId.equals(BuiltinModels.BOOLEAN_ID))
			return Types.CHAR;
		if (tersusTypeId.equals(BuiltinModels.NUMBER_ID))
			return Types.DOUBLE;
		if (tersusTypeId.equals(BuiltinModels.DATE_ID))
			return Types.DATE;
		if (tersusTypeId.equals(BuiltinModels.DATE_AND_TIME_ID))
			return Types.TIMESTAMP;

		return null;
	}

	public static final String HSQLDB = "HSQL Database Engine";
	public static final String MYSQL="MySQL";

}

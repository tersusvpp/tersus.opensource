/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.util;

import java.io.FileInputStream;
import java.io.IOException;


/**
 * @author Youval Bronicki
 *
 */
public class XMLFormatter
{

    public static void main(String[] args) throws Exception
    {
        String in = FileUtils.readString(new FileInputStream(args[0]),null, true);
        System.out.println(format(in));
    }

    /**
     * @param in
     * @param out
     */
    private static StringBuffer format(String in) throws IOException
    {
        StringBuffer out = new StringBuffer((int)(in.length()*1.2));
        int depth = 0;
        boolean endTag = false;
        boolean inTag = true;
        boolean cdata = false;
        for (int i=0; i<in.length(); i++)
        {
            char c = in.charAt(i);
            switch (c)
            {
                case '<':
                    inTag = true;
                    endTag = in.charAt(i+1) == '/' ;
                    if ( !endTag || !cdata)
                    {
                        out.append('\n');
                        for (int j=0; j<depth;j++)
                            out.append(' ');
                    }
                    cdata = false;
                    if (endTag)
                        --depth;
                    else
                        ++depth;
                    out.append(c);
                    break;
                case '>':
                    inTag = false;
                    out.append(c);
                    if (in.charAt(i-1) == '/')
                    {
                        endTag = true;
                        --depth;
                    }
                    if (endTag)
                    {
                        while (i+1<in.length() && Character.isWhitespace(in.charAt(i+1)) )
                            ++i; // consume whitespace
                    }
                break;
                default:
                    if ( ! inTag)
                        cdata = true;
                    out.append(c);

            }    
        }
        return out;
    }
}

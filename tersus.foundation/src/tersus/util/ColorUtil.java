package tersus.util;

import java.awt.Color;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ColorUtil
{

	public static Color getColor(String colorStr)
	{
	    if (colorStr == null)
	        return null;
	    Pattern p = Pattern.compile("#([a-fA-F0-9]{6,8})");
	    Color color = null;
	    if (colorStr != null)
	    {
	        Matcher m = p.matcher(colorStr);
	        if (!m.find())
	            throw new InvalidInputException("Invalid color specification '"
	                    + colorStr + "'");
	        String hex = m.group(1);
	        long colorCode = Long.parseLong(hex, 16);
	        color = new Color((int) (colorCode & 0xFFFFFFFF),
	                colorCode != (colorCode & 0xFFFFFF));
	    }
	    return color;
	}

}

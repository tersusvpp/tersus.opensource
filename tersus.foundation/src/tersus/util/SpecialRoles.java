/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.util;

import tersus.model.Role;

/**
 * Special roles used for XML and Web Services Processing.
 * 
 * @author Ofer Brandes
 *
 */
public class SpecialRoles
{
	public static final String XML_PACKAGE = "XML";

	public static final String XML_ELEMENT_COLLECTION_NAME = "(Elements)";
	public static final String XML_ATTRIBUTE_COLLECTION_NAME = "(Attributes)";
	public static final String WSDL_MESSAGE_PACKAGE_NAME = "(Messages)";
	
	public static final String XML_GENERIC_ELEMENT_ROLE = "Element";
	public static final String XML_GENERIC_NAME_ROLE = "(Name)";
	public static final String XML_GENERIC_NAMESPACE_ROLE = "(Namespace)";
	public static final String XML_CONTENT_LEAF_ROLE = "(Content)";
	public static final String DEPENDENT_MODEL_ROLE_PREFIX = "#";
	public static final String XML_GENERIC_ATTRIBUTE_ROLE = "#Attribute";
	public static final String XML_GENERIC_VALUE_ROLE = "(Value)";

	public static final Role ACTOR_ATTRIBUTE_ROLE = Role.get(DEPENDENT_MODEL_ROLE_PREFIX+"actor");
	public static final Role MUST_UNDERSTAND_ATTRIBUTE_ROLE = Role.get(DEPENDENT_MODEL_ROLE_PREFIX+"mustUnderstand");
	public static final Role ENCODING_STYLE_ATTRIBUTE_ROLE = Role.get(DEPENDENT_MODEL_ROLE_PREFIX+"encodingStyle");

	public static final Role PROTOCOL_TRIGGER_ROLE = Role.get("<HTTP Protocol>");
	public static final Role BINDING_TRIGGER_ROLE = Role.get("<Binding>");
	public static final Role ALIASES_TRIGGER_ROLE = Role.get("<Aliases>");
	public static final Role URL_TRIGGER_ROLE = Role.get("<URL>");
	public static final Role SOAP_ACTION_TRIGGER_ROLE = Role.get("<SOAP Action>");
	public static final Role METHOD_TRIGGER_ROLE = Role.get("<Method>");
	public static final Role HEADER_TRIGGER_ROLE = Role.get("<Header>");
	public static final Role HEADER_EXIT_ROLE = Role.get("<<Header>>");

	public static final String WSDL_LITERAL = "literal";
	public static final String WSDL_ENCODED = "encoded";
	public static final String WSDL_DOCUMENT = "document";
	public static final String WSDL_RPC = "rpc";
}

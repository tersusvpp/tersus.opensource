package tersus.util;

import java.io.IOException;
import java.io.Writer;

public class EscapingWriter extends Writer
{

	private Writer _writer;
	private boolean escapeUnicode = true;
	private boolean escapeQuote = true;
	private boolean escapeDoubleQuote = true;

	public boolean isEscapeQuote()
	{
		return escapeQuote;
	}

	public void setEscapeQuote(boolean escapeQuote)
	{
		this.escapeQuote = escapeQuote;
	}

	public boolean getEscapeDoubleQuote()
	{
		return escapeDoubleQuote;
	}

	public void setEscapeDoubleQuote(boolean escapeDoubleQuote)
	{
		this.escapeDoubleQuote = escapeDoubleQuote;
	}

	public boolean isEscapeUnicode()
	{
		return escapeUnicode;
	}

	public void setEscapeUnicode(boolean escapeUnicode)
	{
		this.escapeUnicode = escapeUnicode;
	}

	public EscapingWriter(Writer _writer)
	{
		this._writer = _writer;
	}

	@Override
	public void close() throws IOException
	{
		_writer.close();
	}

	public void writeRaw(char c) throws IOException
	{
		_writer.write(c);
	}

	public void writeRaw(String s) throws IOException
	{
		_writer.write(s);
	}

	@Override
	public void flush() throws IOException
	{
		_writer.flush();
	}

	@Override
	public void write(char[] cbuf, int off, int len) throws IOException
	{
		for (int i = off; i < off + len; i++)
		{
			char c = cbuf[i];
			if (c < 32) // Special characters
			{
				switch (c)
				{
					case '\n':
						_writer.write('\\');
						_writer.write('n');
						break;
					case '\t':
						_writer.write('\\');
						_writer.write('t');
						break;
					case '\r':
						_writer.write('\\');
						_writer.write('r');
						break;
					case '\f':
						_writer.write('\\');
						_writer.write('f');
						break;
					case '\b':
						_writer.write('\\');
						_writer.write('b');
						break;
					default:
						if (c <= 0xF)
						{
							_writer.write("\\u000"); // unprintable - 3 leading zero
							writehex(c);
						}
						else
						{
							_writer.write("\\u00"); // unprintable 1 leading zero
							writehex(c);
						}
				}

			}
			else if (c <= 0x7F || !escapeUnicode) // Printable ASCII range
			{
				switch (c)
				{
					case '"':
						if (escapeDoubleQuote)
						{
							_writer.write('\\');
							_writer.write('"');
						}
						else
							_writer.write(c);

						break;

					case '\\':
						_writer.write('\\');
						_writer.write('\\');
						break;
					case '\'':
						if (escapeQuote)
						{
							_writer.write('\\');
							_writer.write('\'');
							break;
						}
					default:
						_writer.write(c);
				}

			}
			else if (c <= 0xFF) // Unicode escaping - 2 leading zeroes
			{
				_writer.write("\\u00");
				writehex(c);
			}
			else if (c <= 0xFFF) // Unicode escaping - 1 leading zero
			{
				_writer.write("\\u0");
				writehex(c);
			}
			else
			// Unicode escaping - no leading zeros
			{
				_writer.write("\\u");
				writehex(c);
			}
		}

	}

	private void writehex(char c) throws IOException
	{
		_writer.write(Integer.toHexString(c).toUpperCase());
	}

	public void quote(Object obj) throws IOException
	{
		if (obj == null)
			write("null");
		else
		{
			writeRaw('\'');
			write(obj.toString());
			writeRaw('\'');
		}
	}

}

package tersus.util;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;


public class EscapeUtil
{
    public static String unquote(String s)
    {
        if (s != null && s.startsWith("\"") && s.endsWith("\"") && s.length() >= 2)
            return s.substring(1, s.length()-1);
        return s;
    }

    public static void unescape(Writer out, Reader in) throws IOException
    {
        char[] hex = new char[4];
        while (true)
        {
            int c = in.read();
            if (c == -1)
                return;
            if (c != '\\')
                out.write(c);
            else
            {
                c = in.read();
                switch (c)
                {
                case -1:
                    throw new IllegalArgumentException(
                            "Unterminated escape sequence");
                case 'n':
                    out.write('\n');
                    break;
                case 'r':
                    out.write('\r');
                    break;
                case 't':
                    out.write('\t');
                    break;
                case 'f':
                    out.write('\f');
                    break;
                case 'b':
                    out.write('\b');
                    break;
                case 'u':
                {
                    for (int i = 0; i < 4; i++)
                    {
                        int u = in.read();
                        if (u == -1)
                            throw new IllegalArgumentException(
                                    "Unterminated escape sequence");
                        hex[i] = (char) u;
                    }
                    out.write(Integer.parseInt(new String(hex), 16));
                    break;
                }
                default:
                    out.write(c);

                }
            }
        }
    }

    public static void writeEscaped(Writer out, String in) throws IOException
    {
        EscapingWriter w = new EscapingWriter(out);
        w.write(in);
    }
    public static String escapeControlCharacters(String in)
    {
        StringWriter out = new StringWriter();
        EscapingWriter w = new EscapingWriter(out);
        w.setEscapeUnicode(false);
        w.setEscapeDoubleQuote(false);
        w.setEscapeQuote(false);
        try
        {
            w.write(in);
            w.flush();
        }
        catch (IOException e)
        {
            throw new RuntimeException("Escaping failed unexpectedly", e);
        }
        return out.toString();
    	
    }
    public static String escape(String in, boolean escapeUnicode)
    {
        StringWriter out = new StringWriter();
        EscapingWriter w = new EscapingWriter(out);
        w.setEscapeUnicode(escapeUnicode);
        try
        {
            w.write(in);
            w.flush();
        }
        catch (IOException e)
        {
            throw new RuntimeException("Escaping failed unexpectedly", e);
        }
        return out.toString();
    }
    
    public static String unescape(String in)
    {
        if (in == null)
            return null;
        StringWriter w = new StringWriter();
        try
        {
            unescape(w, new StringReader(in));
        }
        catch (IOException e)
        {
            throw new RuntimeException("Unescaping failed unexpectedly", e);
        }
        return w.toString();
    }
}

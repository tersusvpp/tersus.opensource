/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.util;

/**
 * NICE2 [Description]
 * 
 * @author Youval Bronicki
 *
 */
public class Indenter
{

	/* (non-Javadoc)
	 * @see java.io.Writer#write(char[], int, int)
	 */
	public static StringBuffer cannonize(StringBuffer inBuf, StringBuffer out)
	{
		String in = inBuf.toString();
		boolean atEOL = false;
		char indent = '\t';
		int len = in.length();
		int indentation = 0;
		for (int i = 0; i < len; i++)
		{
			char c = in.charAt(i);
			switch (c)
			{
				case ' ' :
				case '\t' :
					if (atEOL)
						continue;
					else
						out.append(c);
					break;
				case '\n' :
					atEOL = true;
					out.append(c);
					break;
				case '<' :
					int tagEnd = in.indexOf(">", i + 1);
					if (tagEnd < 0)
					{
						if (atEOL)
						{
							for (int j = 0; j < indentation; j++)
								out.append(indent);
							atEOL = false;
						}
						out.append(c);
						continue; // ignore aborted tag
					}
					if (in.charAt(tagEnd - 1) == '/' || in.charAt(i+1) =='!') // ignore closed tags and comments
					{
						if (atEOL)
						{
							for (int j = 0; j < indentation; j++)
								out.append(indent);
							atEOL = false;
						}
						out.append(c);
						continue; 
					}
					String tag = in.substring(i + 1, tagEnd);
					if (tag.startsWith("img"))
					{
						if (atEOL)
						{
							for (int j = 0; j < indentation; j++)
								out.append(indent);
							atEOL = false;
						}
						out.append(c);
						continue; 
					}
					if (tag.charAt(0) == '/')
					{
						indentation--;
						if (atEOL)
						{
							for (int j = 0; j < indentation; j++)
								out.append(indent);
							atEOL = false;
						}
						out.append(c);
					}
					else
					{
						if (atEOL)
						{
							for (int j = 0; j < indentation; j++)
								out.append(indent);
							atEOL = false;
						}
						out.append(c);
						indentation++;
					}
					break;

				default :
					if (atEOL)
					{
						for (int j = 0; j < indentation; j++)
							out.append(indent);
						atEOL = false;
					}
					out.append(c);

			}

		}
		return out;
	}
}
package tersus.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Date;
import sun.security.tools.keytool.CertAndKeyGen;
import sun.security.x509.X500Name;

public class PKUtil
{

	public static boolean checkCertificate(File keystoreFile, String alias, String password)
	{
		try
		{
			validateCertificate(keystoreFile, alias, password);
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}

	public static Date validateCertificate(File keystoreFile, String alias, String password)
			throws Exception
	{

		FileInputStream fis = null;
		try
		{
			KeyStore ks = KeyStore.getInstance("JKS");
			fis = new FileInputStream(keystoreFile);
			ks.load(fis, password.toCharArray());
			fis.close();
			fis = null;
			Certificate cert = ks.getCertificate(alias);
			if (cert == null)
				throw new NoSuchAliasException("No aliase " + alias + " in keystore "
						+ keystoreFile.getAbsolutePath());
			if (cert instanceof X509Certificate)
			{
				((X509Certificate) cert).checkValidity();
				return ((X509Certificate) cert).getNotAfter();
			}
			else
				throw new PKException("Unexpected certificate type " + cert.getClass().getName());

		}
		finally
		{
			FileUtils.forceClose(fis);
		}
	}

	public static void createSelfSignedCertificate(File keystoreFile, String alias,
			String password, long validity, String... details)
	{
		try
		{
			CertAndKeyGen cakg = new CertAndKeyGen("RSA", "MD5WithRSA");
			cakg.generate(1024);
			String[] c = new String[6];
			for (int i = 0; i < 6; i++)
			{
				if (details.length > i)
					c[i] = details[i];
				else
					c[i] = "--";
			}

			X500Name name = new X500Name(c[0], c[1], c[2], c[3], c[4],c[5]);
			java.security.cert.X509Certificate certificate = cakg
					.getSelfCertificate(name, validity);
			KeyStore ks = KeyStore.getInstance("JKS");

			FileInputStream fis = null;// NULL input stream = empty key store
			ks.load(fis, password.toCharArray());

			ks.setKeyEntry(alias, cakg.getPrivateKey(), password.toCharArray(), new Certificate[]
			{ certificate });
			FileOutputStream fos = null;
			try
			{
				fos = new java.io.FileOutputStream(keystoreFile);
				ks.store(fos, password.toCharArray());
			}
			finally
			{
				if (fos != null)
				{
					fos.close();
				}
			}
		}
		catch (Exception e)
		{
			throw new PKException("Failed to generate certificate", e);
		}
	}

	static class PKException extends RuntimeException
	{
		private static final long serialVersionUID = 1L;

		public PKException(String message, Throwable e)
		{
			super(message, e);
		}

		public PKException(String message)
		{
			super(message);
		}
	}

	static class NoSuchAliasException extends RuntimeException
	{
		private static final long serialVersionUID = 1L;

		public NoSuchAliasException(String message)
		{
			super(message);
		}
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.util;


import tersus.InternalErrorException;
import tersus.model.Composition;
import tersus.model.FlowDataElementType;
import tersus.model.FlowType;
import tersus.model.LinkOperation;
import tersus.model.SlotType;

/**
 * An abstract implmenetation of IValueFactory (for Enum types)
 * Concrete sub-classes (for specific types) must implement getValues() 
 */
public class EnumFactory
{

	Class<?> type;
	Enum[] values;
	String[] stringValues;
	public static EnumFactory getFactory(Class<?> type)
	{
		Enum[] values = getValues(type);
		if (values == null)
			return null;
		return new EnumFactory(type, values);
	}
	public EnumFactory(Class<?> type)
	{
		Enum[] values = getValues(type);
		if (values == null)
			throw new InternalErrorException("No factory for "+type.getName());
		this.values = values;
		// type must be a subclass of Enum
		this.type = type;
		
	}
	private EnumFactory(Class<?> type, Enum[] values)
	{
		this.type = type;
		this.values = values;
	}
	private static Enum[] getValues(Class<?> type)
	{
		Enum[] values = null;
		if ( Composition.class.equals(type))
			values = Composition.values;
		else if (FlowDataElementType.class.equals(type))
			values = FlowDataElementType.values;
		else if (FlowType.class.equals(type))
			values = FlowType.values;
		else if (LinkOperation.class.equals(type))
			values = LinkOperation.values;
		else if (SlotType.class.equals(type))
			values = SlotType.values;
		return values;
	}
	public Enum getValue(String value)
	{
		return Enum.get(value, values);
	}
	/* (non-Javadoc)
	 * @see tersus.util.IValueFactory#getValues()
	 */
	public Enum[] getValues()
	{
		return values;
	}
	/**
	 * 
	 */
	public String[] getStringValues()
	{
		if (stringValues == null)
		{
			stringValues = new String[values.length];
			for (int i = 0; i < values.length; i++)
			{
				stringValues[i] = values[i].value;
			}
		}
		return stringValues;

	}
	/**
	 * @param i
	 * @return
	 */
	public Enum getValue(int i)
	{
		return values[i];
	}
}

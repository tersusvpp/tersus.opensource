/************************************************************************************************
 * Copyright (c) 2003-2011 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/

package tersus.util;

import java.util.Iterator;
import java.util.List;

public class Misc
{
	public static final String INVALID_FILENAME_CHARACTERS = "/\\?:*#<>|\"";

	public static final boolean ASSERTIONS = true;

	public static void assertion(boolean test)
	{
		if (!test)
		{

			Exception e = new RuntimeException("Assertion Failed");
			e.printStackTrace();
		}
	}

	public static String getShortName(Class<?> c)
	{
		String fullName = c.getName();
		String name = fullName.substring(fullName.lastIndexOf('.') + 1);
		return name;
	}

	public static boolean compare(byte[] array1, byte[] array2)
	{
		if (array1.length != array2.length)
			return false;
		for (int i = 0; i < array1.length; i++)
			if (array1[i] != array2[i])
				return false;
		return true;
	}

	public static boolean compareStrings(String s1, String s2)
	{
		if (s1.equals(s2))
			return true;
		System.out.println("Strings are different:");
		System.out.println("String1=");
		System.out.println(s1);
		System.out.println("String2=");
		System.out.println(s2);
		return false;

	}

	/**
	 * Returns a String representation for an object
	 * 
	 * @param object
	 *            any object
	 * @return the result of calling the object's toString(), or null if the object is null
	 */
	public static String getString(Object object)
	{
		if (object == null)
			return null;
		else
			return object.toString();
	}

	/**
	 * @param b
	 * @return
	 */
	public static Boolean getBoolean(boolean b)
	{
		return b ? Boolean.TRUE : Boolean.FALSE;
	}

	/**
	 * Replaces all occurances of a string
	 * 
	 * @param input
	 *            The input string (to which replacement will be applied)
	 * @param search
	 *            The sub-string to find (old)
	 * @param replace
	 *            The sub-string to replace (new)
	 * @return A new String, the result of replacing 'search' by 'replace' in 'input' (or null if
	 *         the input string is null)
	 */
	public static String replaceAll(String input, String search, String replace)
	{
		if (input == null)
			return null;
		int index = 0;
		StringBuffer output = new StringBuffer();
		while (index < input.length())
		{
			int location = input.indexOf(search, index);
			if (location < 0)
			{
				output.append(input.substring(index));
				break;
			}
			output.append(input.substring(index, location));
			output.append(replace);
			index = location + search.length();
		}
		return output.toString();
	}

	/**
	 * @param values
	 * @return
	 */
	public static StringBuffer toString(List<Object> values)
	{
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < values.size(); i++)
		{
			if (i > 0)
				buf.append(',');
			Object value = values.get(i);
			if (value instanceof String)
			{
				buf.append('\'');
				buf.append(EscapeUtil.escape((String) value, false));
				buf.append('\'');
			}
			else
				buf.append(value);
		}
		return buf;
	}

	public static StringBuffer toString(Object[] values)
	{
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < values.length; i++)
		{
			if (i > 0)
				buf.append(',');
			Object value = values[i];
			if (value instanceof String)
			{
				buf.append('\'');
				buf.append(value);
				buf.append('\'');
			}
			else
				buf.append(value);
		}
		return buf;
	}

	/**
	 * @param object1
	 * @param object2
	 * @return
	 */
	public static boolean equal(Object object1, Object object2)
	{
		if (object1 == null)
			return object2 == null;
		else
			return object1.equals(object2);
	}
	
	public static boolean equal(Object object1, Object object2, boolean ignoreCase)
	{
		if (object1 == null)
			return object2 == null;
		if (ignoreCase && (object1 instanceof String) && (object2 instanceof String))
			return ((String)object1).equalsIgnoreCase((String)object2);
		else
			return object1.equals(object2);	
	}

	public static boolean equal(Object[] array1, Object[] array2)
	{
		if (array1 == null)
			return (array2 == null);
		if (array1.length != array2.length)
			return false;
		for (int i = 0; i < array1.length; i++)
		{
			if (!equal(array1[i], array2[i]))
				return false;
		}
		return true;
	}

	/**
	 * Concatenates the string representations of a list of objects, with a separator
	 * 
	 * @param list
	 *            - the list objects to concatenate
	 * @param separator
	 *            - a string to be inserted between adjacent values
	 * @return a new String contaning the representation.
	 */
	public static String concatenateList(List<?> list, String separator)
	{
		StringBuffer out = new StringBuffer();
		boolean first = true;
		for (Iterator<?> i = list.iterator(); i.hasNext();)
		{
			if (!first)
				out.append(separator);
			out.append(i.next());
			first = false;

		}
		return out.toString();
	}

	public static boolean isValidFileName(String name)
	{
		for (int i = 0; i < INVALID_FILENAME_CHARACTERS.length(); i++)
			if (name.indexOf(INVALID_FILENAME_CHARACTERS.charAt(i)) >= 0)
				return false;
		// if (name.endsWith(" "))
		// return false;
		return true;
	}

	private static final String LETTERS_AND_DIGITS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	public static String sqlize(String name)
	{
		StringBuffer translated = new StringBuffer();
		for (int i = 0; i < name.length(); i++)
		{
			char c = name.charAt(i);
			if (LETTERS_AND_DIGITS.indexOf(c) >= 0)
				translated.append(c);
			else
				translated.append('_');
		}

		return translated.toString();
	}

	/**
	 * Trimming trailing whitespaces (like String.trim(), but leaving leading whitespaces)
	 */
	public static String trimTrailing(String string)
	{
		int len = string.length();
		while (len > 0)
			if (string.charAt(len - 1) <= ' ')
				len--;
			else
				break;

		return (len < string.length() ? string.substring(0, len) : string);
	}


	public static String ucFirst(String s)
	{
		return Character.toUpperCase(s.charAt(0)) + s.substring(1);
	}

	public static boolean isWindows()
	{
		return System.getProperty("os.name").toLowerCase().indexOf("windows") >= 0;
	}

	public static String lastToken(String text, String separatorPattern)
	{
		String[] lines = text.split(separatorPattern);
		return lines[lines.length - 1];
	}

	public static String lastLine(String text)
	{
		String separatorPattern = "[\r\n]";
		return lastToken(text, separatorPattern);
	}
	

	public static String concatenate(String[] strings, String separator)
	{
		StringBuffer b = new StringBuffer();
		int l = strings.length;
		for (int i=0;i<l;i++)
		{
			if (i>0)
				b.append(separator);
			b.append(strings[i]);
		}
		return b.toString();
	}

	public static String quote(String name)
	{
		return '"'+name+'"';
	}
}
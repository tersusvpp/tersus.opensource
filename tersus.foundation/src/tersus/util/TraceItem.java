/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.util;

import java.util.ArrayList;
import java.util.Iterator;




/**
 * 
 */
public class TraceItem
{
	/**
	 * @param e
	 */
	public TraceItem(Throwable e)
	{
		
		text = e.getMessage();
		status = Trace.ERROR;
		cause = e;
	}
	/**
	 * @param string
	 * @param string2
	 */
	public TraceItem()
	{
	}
	public TraceItem(String type, String text)
	{
		this.type = type;
		this.text = text;
	}
	public static final String USER_ACTION = "User Action";
	private ArrayList myListeners;
	private String text;
	private TraceItem parent;
	private String status = null;
	private String type = null;
	private Throwable cause = null;
	private long timestamp = System.currentTimeMillis();

	public void addListener(ItemListener listener)
	{
		if (myListeners == null)
			myListeners = new ArrayList();
		myListeners.add(listener);
	}
	public void removeListener(ItemListener listener)
	{
		myListeners.remove(listener);
	}
	public void fireChanged()
	{
		ArrayList listeners = getListeners();
		if (listeners != null)
		{
			for (Iterator iter = listeners.iterator(); iter.hasNext();)
			{
				ItemListener listener = (ItemListener) iter.next();
				listener.itemChanged(this);
			}
		}
	}

	/**
	 * @return
	 */
	protected ArrayList getListeners()
	{
		// TODO Review this auto-generated method stub
		if (myListeners != null)
			return myListeners;
		if (getParent() == null)
			return null;
		return getParent().getListeners();
	}
	public TraceItem(String text)
	{
		this.text = text;
	}
	public String getText()
	{
		return text;
	}
	public void removeParent()
	{
		this.parent = null;
	}
	public void setParent(TraceItem newParent)
	{
		if (this.parent != null)
			throw new RuntimeException("Item already has a parent");
		this.parent = newParent;
	}
	public TraceItem getParent()
	{
		return parent;
	}
	public String toString()
	{
		if (cause != null)
		    {
		    	if (cause.getMessage() != null)
					return getText() + " ["+Misc.getShortName(cause.getClass())+ ": "+cause.getMessage()+ "]";
		    	else
		    	    return getText() + " ["+Misc.getShortName(cause.getClass())+"]";
		    }
		else
			return getText();
	}
	protected ArrayList children = new ArrayList();
	public void addChild(TraceItem child)
	{
		children.add(child);
		child.setParent(this);
		fireChanged();
	}
	public void removeChild(TraceItem child)
	{
		children.remove(child);
		child.removeParent();
		fireChanged();
	}
	public TraceItem[] getChildren()
	{
		return (TraceItem[]) children.toArray(new TraceItem[children.size()]);
	}
	public boolean hasChildren()
	{
		return children.size() > 0;
	}
	/**
		 * 
		 */
	public void removeAll()
	{
		for (Iterator iter = children.iterator(); iter.hasNext();)
		{
			TraceItem child = (TraceItem) iter.next();
			child.removeParent();
		}
		children.clear();
		fireChanged();
	}
	/**
	 * @return
	 */
	public String getStatus()
	{
		return status;
	}

	/**
	 * @param string
	 */
	public void setStatus(String string)
	{
		status = string;
		fireChanged();
	}

	/**
	 * @return
	 */
	public Throwable getCause()
	{
		return cause;
	}

	/**
	 * @param throwable
	 */
	public void setCause(Throwable throwable)
	{
		cause = throwable;
		fireChanged();
	}
	
	public void setError(Throwable cause)
	{
		this.cause = cause;
		this.status = Trace.ERROR;
		fireChanged();
	}

	/**
	 * @return
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * @param string
	 */
	public void setType(String string)
	{
		type = string;
		fireChanged();
	}

	/**
	 * @param string
	 */
	public void setText(String string)
	{
		text = string;
		fireChanged();
	}

	/**
	 * @return
	 */
	public long getTimestamp()
	{
		return timestamp;
	}
    /**
     * 
     */
 

}


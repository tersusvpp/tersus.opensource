/************************************************************************************************
 * Copyright (c) 2003-2021 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
package tersus.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

/**
 * Utility process that runs a process synchronously
 */
public class ProcessRunner
{
	ByteArrayOutputStream outputStream;
	File workingDirectory = null;
	String processInput;
	public String getProcessInput()
	{
		return processInput;
	}

	public void setProcessInput(String processInput)
	{
		this.processInput = processInput;
	}

	public ByteArrayOutputStream getOutputStream() {
		return outputStream;
	}

	public ByteArrayOutputStream getErrorStream() {
		return errorStream;
	}

	ByteArrayOutputStream errorStream;
	private Repeater outputRepeater;
	private Repeater errorRepeater;

	public int run(String[] cmdArray, long timeoutMilliseconds)
		throws IOException, InterruptedException
	{
		int status = 0;
		try
		{
			Process process = Runtime.getRuntime().exec(cmdArray, null, workingDirectory);
			outputStream = new ByteArrayOutputStream();
			errorStream = new ByteArrayOutputStream();
			if (processInput != null)
				(new Repeater(new ByteArrayInputStream(processInput.getBytes()), process.getOutputStream())).start();
			outputRepeater = new Repeater(process.getInputStream(), outputStream);
			outputRepeater.start();
			errorRepeater = new Repeater(process.getErrorStream(), errorStream);
			errorRepeater.start();
			
			if (timeoutMilliseconds != -1)
				status = waitFor(process, timeoutMilliseconds);
		}
		catch (Exception e)
		{
			// TODO: handle exception
		}
		return status;
	}
	
	public int waitFor(final Process p,final long timeout) throws InterruptedException
	{
	    final StringBuffer timeoutMessage = new StringBuffer();
	    final long startTime = System.currentTimeMillis();
	    Thread killer = new Thread() {
	        public void run()
	        {
	            try
	            {
	                Thread.sleep(timeout);
	                long duration = System.currentTimeMillis() - startTime;
	                timeoutMessage.append("Process timed out after "+(duration/10)/100.0 + " seconds");
	                p.destroy();
	            }
	            catch (Exception e)
	            {
	            }
	        }
	    };
	    killer.start();
	    int rc  = p.waitFor();
	    while (outputRepeater.isAlive() || errorRepeater.isAlive())
	    	Thread.sleep(100);
	    killer.interrupt();
	    if (timeoutMessage.length() > 0)
	        throw new RuntimeException(timeoutMessage.toString());
	    return rc;
	}

	public void setWorkingDirectory(File workingDirectory)
	{
		this.workingDirectory = workingDirectory;	
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.util;

/**
 * 
 * Number-related utilities (e.g. formatting).
 * 
 * @author Ofer Brandes
 *
 */

public class NumberUtil
{
	/**
	 * Checks whether a string represents a number.
	 * 
	 * Tries to interpret the input string as a number (as defined by 'Double.parseDouble'),
	 * and returns whether a number has been found or not.
	 *
	 * @param s A string representation of some number (e.g. "10.7").
	 * @return 'true' if the input string represents a number, 'false' if not.
	 */		
	public static boolean isNumber (String s)
	{
		try
		{
			Double.parseDouble (s);
		}
		catch (NumberFormatException e)
		{
			return (false);
		}
			
		return (true);
	}

	/**
	 * Checks whether two numbers are close enough to be considered "essentially equal".
	 * 
	 * @return 'true' if the two numbers have the same sign and their ratio is ~1, 'false' if their signs are different (specifically if only one of them is 0) or their ratio is not ~1.
	 */
	
	static final double EPSILON = 1.e-9;
				
	public static boolean essentiallyEqual (double n1, double n2)
	{
		if (n1 == n2)
			return true;
			
		int sign1 = (n1 > 0 ? 1 : (n1 < 0 ? -1 :0));
		int sign2 = (n2 > 0 ? 1 : (n2 < 0 ? -1 :0));
		
		if (sign1 != sign2)
			return false;
		else if (sign1 == 0)
			return true;
		
		double ratio = Math.max(n1/n2,n2/n1);  // Both of same sign, so ratio > 1

		return (ratio - 1 < EPSILON);
	}
}

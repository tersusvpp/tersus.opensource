package tersus.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A utility class that provides a simple unified interface to Java & Javascript regular expression matching.
 * (This should be part of GWT, but is apparently missing as of July 2009)
 */
public class Regex
{

	public static String[] exec(String regex, String str)
	{
		Pattern p = Pattern.compile(regex);
	    Matcher m = p.matcher(str);
		if (! m.matches())
			return null;
		String[] out = new String[m.groupCount()+1];
		for (int i=0;i<out.length; i++)
			out[i]=m.group(i);
		return out;
	}

}

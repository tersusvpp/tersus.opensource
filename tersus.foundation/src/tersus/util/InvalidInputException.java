/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.util;

/**
 * Thrown to indicate that some input is not valid
 * 
 * @author Youval Bronicki
 * 
 */
public class InvalidInputException extends RuntimeException
{
	public InvalidInputException(String message)
	{
		super(message);
	}

	public InvalidInputException(String message, Throwable cause)
	{
		super(message, cause);
	}
}

package tersus.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipUtil
{
	public static byte[] prepareZipFile(File folder)
	{
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		ZipOutputStream zo = new ZipOutputStream(bo);
		try
		{
			writeToZip(folder, folder, zo);
			zo.close();
		}
		catch (Exception e)
		{
			throw new ZipException("Failed to create zip file", e);
		}
		return bo.toByteArray();
	}

	public static void writeToZip(File root, File f, ZipOutputStream zo) throws IOException	{
		if (f.isFile())
		{
			int l = root.getPath().length();
			String relativePath = f.getPath().substring(l+1);
			ZipEntry entry = new ZipEntry(relativePath);
			zo.putNextEntry(entry);
			byte[] readBuffer = new byte[8192];
			FileInputStream contentStream = new FileInputStream(f);
			try
			{
				int n;
				while ((n = contentStream.read(readBuffer)) > 0)
				{
					zo.write(readBuffer, 0, n);
				}
			}
			finally
			{
				if (contentStream != null)
				{
					contentStream.close();
				}
			}
			zo.closeEntry();
		}
		else if (f.isDirectory())
		{
			for (File member :f.listFiles())
			{
				writeToZip(root, member, zo);
			}
		}
	}
}

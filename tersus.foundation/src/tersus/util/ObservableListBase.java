/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.util;

import javax.swing.event.EventListenerList;

/**
 * 
 * @author Liat Shiff
 */
public abstract class ObservableListBase<T> implements IObservableList<T>
{
    protected EventListenerList listeners = new EventListenerList();
    
    protected void fireItemChanged(T source, String propertyName, Object oldValue, Object newValue)
    {
        for (ListListener<T> l : listeners.getListeners(ListListener.class))
        {
            l.itemChanged(source, propertyName, oldValue, newValue);
        }
    }
    
    protected void fireContentChanged()
    {
        for (ListListener<T> l : listeners.getListeners(ListListener.class))
        {
            l.contentChanged(this);
        }
    }
    
    protected void fireItemAdded(T item)
    {
        for (ListListener<T> l : listeners.getListeners(ListListener.class))
        {
            l.itemAdded(item);
        }
    }
    
    public void addListener(ListListener<T> l)
    {
        listeners.add(ListListener.class, l);  
    }
    
    public void removeListener(ListListener<T> l)
    {
        listeners.remove(ListListener.class, l);
    }
}

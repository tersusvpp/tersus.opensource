package tersus.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
/**
 * A thread that copies information from a source InputStream to a target OutputStream
 * 
 * Used by ProcessRunner  
 * @author yobronicki
 *
 */
class Repeater extends Thread
{
    private InputStream source;
    private OutputStream target;

    public Repeater(InputStream source, OutputStream target)
    {
        this.source = new BufferedInputStream(source);
        this.target = new BufferedOutputStream(target);
    }

    public void run()
    {
        try
        {
            while (true)
            {
                int b = source.read();
                if (b == -1)
                {
                    target.flush();
                    return;
                }
                target.write(b);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

    }
}


/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.util;

import tersus.InternalErrorException;

public class Trace
{

	private static TraceItem root = new TraceItem("");
	private static TraceItem currentTraceItem = root;
	private static String status;
	public static final String RUNNING = "Running";
	public static final String ERROR = "Error";
	public static final String FAILED = "Failed";
	public static final String OK = "O.K.";

//	private static ArrayList listeners = new ArrayList();
//
//	public static void addListener(TraceListener listener)
//	{
//		listeners.add(listener);
//	}
//	public static void removeListener(TraceListener listener)
//	{
//		listeners.remove(listener);
//	}
	public static void push(String text)
	{
		TraceItem newTraceItem = new TraceItem(text);
		push(newTraceItem);
	}

	public static void push(TraceItem item)
	{
		System.out.println("push:"+item);
		currentTraceItem.addChild(item);
		currentTraceItem = item;
	}
	public static void pop()
	{
		currentTraceItem = currentTraceItem.getParent();
		if (currentTraceItem == null)
		{
			currentTraceItem = root;
			addError(new InternalErrorException("Excessive pop()"));
		}
	}

	public static void add(String text)
	{
		add(new TraceItem(text));
	}

	public static void add(TraceItem item)
	{
		currentTraceItem.addChild(item);

	}

	public static TraceItem getRoot()
	{
		return root;
	}

	public static void clear()
	{
		root.removeAll();
		currentTraceItem = root;
	}

//	public static void setStatus(String newStatus)
//	{
//		String oldStatus = status;
//		status = newStatus;
//		for (Iterator iter = listeners.iterator(); iter.hasNext();)
//		{
//			TraceListener listener = (TraceListener) iter.next();
//			listener.statusChanged(oldStatus, newStatus);
//		}
//
//	}
	/**
	 * @param string
	 * @param e
	 */
	public static void addError(Throwable e)
	{
		add(new TraceItem(e));
		

	}

}

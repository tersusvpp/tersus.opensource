package tersus.util;

public class ZipException extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ZipException()
	{
		// TODO Auto-generated constructor stub
	}

	public ZipException(String message)
	{
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ZipException(Throwable cause)
	{
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ZipException(String message, Throwable cause)
	{
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}

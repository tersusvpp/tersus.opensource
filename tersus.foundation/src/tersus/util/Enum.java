/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.util;

import java.io.Serializable;



/**
 * Abstract base class for type-safe enumeration
 * The class provides support for uniqueness of values
 * (even through serialization/deserialization)
 */
public abstract class Enum implements Serializable
{
	protected String value;
	protected Integer index;
	abstract protected Enum[] getValues();
	public static Enum get(String value, Enum[] values) throws IllegalValueException
	{
		for (int i = 0; i < values.length; i++)
		{
			Enum element = values[i];
			if (element.value.equals(value))
				return element;
		}
		throw new IllegalValueException(
			"Illegal value " + value + " for " + Misc.getShortName(values[0].getClass()));
	}
	protected Enum(String value)
	{
		this.value = value;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		return value;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj)
	{
		if (obj == null)
			return false;
		if (!getClass().equals(obj.getClass()))
			return false;
		return (value.equals(((Enum) obj).value));
	}
	protected Object readResolve()
	{
		return get(value, getValues());
	}
	/**
	 * @return
	 */
	public Integer getIndexObject()
	{
		Enum[] values = getValues();
		if (index == null)
		{
			for (int i = 0; i < values.length; i++)
			{
				if (values[i] == this)
				{
					index = new Integer(i);
					break;
				}
			}
		}
		if (Misc.ASSERTIONS)
			Misc.assertion(index != null);
		return index;
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.util;

import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 * @author Youval Bronicki
 *  
 */
public class XMLReaderConfigurator
{
    static boolean configured = false;

    public static void main(String[] args)
    {
        configureXMLReader();
    }

    synchronized public static void configureXMLReader()
    {
        if (configured)
            return;
        try
        {
            XMLReader reader = XMLReaderFactory.createXMLReader();
            configured = true;
            return;
        }
        catch (SAXException e)
        {
        }
        
        try
        {
            Class.forName("org.apache.crimson.parser.XMLReaderImpl");
            System.setProperty("org.xml.sax.driver","org.apache.crimson.parser.XMLReaderImpl");
            XMLReaderFactory.createXMLReader();
            configured = true;
            return;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Failed to configure XML Reader", e);
        }
    }
}
package tersus.util;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.swing.event.EventListenerList;

public class ObservableList<T extends IObservableItem> extends ObservableListBase<T>
{
	private ArrayList<T> content;
	private PropertyChangeListener itemListener;
	protected EventListenerList listeners;

	public ObservableList(List<T> initialContent)
	{
		init();
		addAll(initialContent);
	}

	public ObservableList()
	{
		init();
	}

	public List<T> getContent()
	{
		return (List<T>) Collections.unmodifiableList(content);
	}

	private void init()
	{
		content = new ArrayList<T>();
		listeners = new EventListenerList();
		itemListener = new PropertyChangeListener()
		{
			@SuppressWarnings("unchecked")
			public void propertyChange(PropertyChangeEvent evt)
			{
				fireItemChanged((T) evt.getSource(), evt.getPropertyName(), evt.getOldValue(),
						evt.getNewValue());
			}

		};
	}

	public void add(T item)
	{
		content.add(item);
		item.addListener(itemListener);
		fireItemAdded(item);
	}

	public void add(T item, int index)
	{
		content.add(index, item);
		item.addListener(itemListener);
		fireItemAdded(item);
	}

	public void addAll(Collection<T> col)
	{
		content.addAll(col);
		for (T item : col)
			item.addListener(itemListener);
		fireContentChanged();
	}

	public void remove(T item)
	{
		content.remove(item);
		item.removeListener(itemListener);
		fireContentChanged();
	}

	public void remove(int index)
	{
		T item = content.remove(index);
		if (item != null)
			item.removeListener(itemListener);
		fireContentChanged();
	}
	
	public void clear()
	{
		for (T item: content)
		{
			item.removeListener(itemListener);
		}
		
		content.clear();
		fireContentChanged();
	}

	public void dispose()
	{
		for (T item : content)
			item.removeListener(itemListener);
		content.clear();
	}

}

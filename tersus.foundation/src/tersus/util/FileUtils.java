package tersus.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class FileUtils
{
	public static final String UTF8 = "UTF-8";
	
	public static void copy(InputStream is, OutputStream os, boolean closeStreams) throws IOException
	{
	    try
	    {
	        int n;
	        byte[] readBuffer = new byte[8192];
	        while ((n = is.read(readBuffer)) > 0)
	        {
	            os.write(readBuffer, 0, n);
	        }
	        if (closeStreams)
	        {
	            is.close();
	            os.close();
	        }
	        else
	        {
	            is = null;
	            os = null;
	        }
	    }
	    finally
	    {
	        if (closeStreams)
	        {
	        	forceClose(is);
	        	forceClose(os);
	        }
	    }
	}

	public static void forceClose(InputStream is)
	{
	    if (is != null)
	    {
	        try
	        {
	            is.close();
	        }
	        catch (Exception e)
	        {
	        }
	    }
	}
	public static void forceClose(Reader in)
	{
	    if (in != null)
	    {
	        try
	        {
	            in.close();
	        }
	        catch (Exception e)
	        {
	        }
	    }
	}

	public static void forceClose(OutputStream os)
	{
	    if (os != null)
	    {
	        try
	        {
	            os.close();
	        }
	        catch (Exception e)
	        {
	        }
	    }
	}

	/*
	 * Copies a file or directory to a target location. Existing files in the
	 * target location are not deleted
	 */
	public static void copy(File source, File target, FilenameFilter filter, boolean forceOverwrite) throws IOException
	{
		if (filter != null && ! filter.accept(source.getParentFile(), source.getName()))
			return;
	    if (source.isFile())
	    {
	        if (target.isDirectory())
	            target.delete();
	        if (forceOverwrite || !target.exists() || target.lastModified() < source.lastModified())
	        {
		        FileInputStream is = new FileInputStream(source);
		        FileOutputStream os = new FileOutputStream(target);
		        copy(is, os, true);
		        target.setLastModified(source.lastModified());
		        target.setExecutable(source.canExecute());
	        }
	    }
	    else if (source.isDirectory())
	    {
	        if (target.isFile())
	            target.delete();
	        if (!target.exists())
	            target.mkdir();
	        for (String child : source.list())
	        {
	            copy(new File(source, child), new File(target, child), filter, forceOverwrite);
	        }
	    }
	}
	
	
	public static void write(String s, OutputStream out, boolean close) throws IOException
	{
		try
		{
			out.write(s.getBytes());
	
		}
		finally
		{
			if (close)
				out.close();
		}
	}
	public static void write(String s, String encoding,  OutputStream out, boolean close) throws IOException
	{
		try
		{
			out.write(s.getBytes(encoding));
	
		}
		finally
		{
			if (close)
				out.close();
		}
	}
	
	public static void write(byte[] content, File file) throws IOException
	{
		FileOutputStream os = null;
		try
		{
			os = new FileOutputStream(file);
			os.write(content);
			os.close();
			os = null;
		}
		finally
		{
			if (os != null)
				try
				{
					os.close();

				}
				catch (Exception e)
				{
					// Ignoring secondary exception
					e.printStackTrace();
				}
		}
	}
	
	/**
	 * Deletes a file or directory 
	 */
	public static void delete(File target)
	{
		if (!target.exists())
			return;
		if (target.isDirectory())
			FileUtils.empty(target);
		target.delete();
		
		
	}

	/**
	 * @param dir
	 */
	public static void empty(File dir)
	{
	    if (dir == null || !dir.isDirectory())
	        return;
	    File[] files = dir.listFiles();
	    for (int i = 0; i < files.length; i++)
	    {
	        File file = files[i];
	        if (file.isDirectory())
	            empty(file);
	        file.delete();
	    }
	}

	/**
	 * Creates a String from an InputStream. This method closes the input stream
	 * 
	 * @param in
	 *            the input stream
	 * @param encoding the encoding to use (null for the system's default encoding)           
	 * @param closeStream
	 *            indicates whether the stream should be closed before the
	 *            method returns
	 * @return A String that contains the whole content of the InputStream
	 */
	public static String readString(InputStream in, String encoding, boolean closeStream)
	{
	    String s = new String();
	    try
	    {
	        byte[] bytes = FileUtils.readBytes(in, closeStream);
	        if (encoding != null)
	            s = new String(bytes, encoding);
	        else
	            s = new String(bytes);
	    }
	    catch (IOException e)
	    {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    }
	    return s;
	}

	/**
	 * Creates a StringBuffer from a Reader
	 * 
	 * @param input
	 *            the reader
	 * @param closeReader
	 *            indicates whether the reader should be closed before the
	 *            method returns
	 * @return A StringBuffer that contains the whole content of the Reader
	 */
	public static StringBuffer readString(Reader in, boolean closeReader)
	{
	    StringBuffer s = new StringBuffer();
	    try
	    {
	        BufferedReader buffer = new BufferedReader(in);
	        while (true)
	        {
	            int b = buffer.read();
	            if (b == -1)
	            {
	                return s;
	            }
	            s.append((char)b);
	        }
	    }
	    catch (IOException e)
	    {
	        throw new RuntimeException("Failed to read characters from stream",e);
	    }
	}

	public static byte[] readBytes(InputStream in, boolean closeStream)
	        throws IOException
	{
	    try
	    {
	        ByteArrayOutputStream out = new ByteArrayOutputStream();
	        copy(in, out, closeStream);
	        return out.toByteArray();
	    }
	    finally
	    {
	        if (closeStream)
	            in.close();
	    }
	}

	public static File URLtoFile(URL url)
	{
		if (url.getHost() == null || url.getHost().length() == 0)
		{
			try
			{
				return new File(URLDecoder.decode(url.getFile(), "UTF-8"));
			}
			catch (UnsupportedEncodingException e)
			{
				e.printStackTrace();
			}
		}
		else
	    {
	        String path = "\\\\" + url.getHost() + "\\" + url.getPath();
	        return new File(path);
	    }
		
		return null;
	}

	public static byte[] readURL(String urlString) throws IOException
	{
	    byte[] fileContent;
	    URL url = new URL(urlString);
	    fileContent = readBytes(url.openStream(), true);
	    return fileContent;
	}
	
	public static String readURLAsString(String urlString) throws IOException
	{
		URL url = new URL(urlString);
		URLConnection c = url.openConnection();
		String encoding = c.getContentEncoding();
		if (encoding == null)
		{
			String contentType = c.getContentType();
			if (contentType != null)
			{
				Pattern p = Pattern.compile("charset=(\\S+)");
				Matcher m = p.matcher(contentType);
				if (m.find())
					encoding=m.group(1);
			}

		}
		byte[] bytes = readBytes(c.getInputStream(), true);
		if (encoding == null)
			return new String(bytes);
		else
			return new String(bytes, encoding);
	}

	/**
	 * @param referenceTraceFile
	 * @param actualTraceFile
	 */
	public static boolean compareFiles(File file1, File file2)
	        throws IOException
	{
	    byte[] array1 = readBytes(new FileInputStream(file1), true);
	    byte[] array2 = readBytes(new FileInputStream(file2), true);
	    return Misc.compare(array1, array2);
	}

	/**
	 * @param String
	 *            filePath - The full path of the file
	 * @param String
	 *            stringToReplace - The String to be replaced
	 * @param String
	 *            newString - The new String Note - To modify this method to
	 *            include other File manipulations - change
	 *            'content.repalceFirst' to other methods of 'String content'
	 */
	public static void replaceStringInFile(String filePath, String encoding,
	        String stringToReplace, String newString) throws IOException
	{
	
	    FileInputStream filein = new FileInputStream(filePath);
	    InputStream in = filein;
	    String content = readString(in, encoding, true);
	    String newContent = Misc.replaceAll(content, stringToReplace, newString);
	    FileOutputStream fileout = new FileOutputStream(filePath);
	    fileout.write(newContent.getBytes());
	    fileout.close();
	
	}
	
	public static void unzip(ZipFile zipFile, File targetFolder, EntryFilter filter) throws IOException
	{
		for (Enumeration<?> e = zipFile.entries(); e.hasMoreElements();)
		{
			ZipEntry entry = (ZipEntry) e.nextElement();
			String entryName = entry.getName();
			if (entry.isDirectory())
				continue;

			if ('/' == entryName.charAt(0))
				entryName = entryName.substring(1);
			
			if (filter == null || filter.accept(entryName))
			{
				File file = new File(targetFolder, entryName);
				File folder = file.getParentFile();
				folder.mkdirs();
				copy( zipFile.getInputStream(entry), new FileOutputStream(file), true);
			}
		}
		zipFile.close();
	}

	public static boolean contains(File folder, File file) throws IOException
	{
		String parent = folder.getCanonicalPath();
		String child = file.getCanonicalPath();
		return child.startsWith(parent) && isPathSeparator(child.charAt(parent.length()));
	}

	private static boolean isPathSeparator(char c)
	{
		return c == '/';
	}

	public static boolean isTextFile(File source)
	{
		String[] suffixes =
		{ ".h", ".m", ".plist", ".xib", ".pch", ".pbxproj" };
		String name = source.getName();
		for (String suffix : suffixes)
			if (name.endsWith(suffix))
				return true;
		return false;
	}

	public static void forceClose(ZipFile zf)
	{
		if (zf != null)
		{
			try
			{
				zf.close();
			}
			catch(Exception e)
			{
			}
		}
	}
}

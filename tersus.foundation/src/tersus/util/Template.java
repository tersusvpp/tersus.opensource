/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.util;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * NICE2 [Description]
 * 
 * @author Youval Bronicki
 *
 */
public class Template
{

	public interface TokenHandler
	{
		void write(String key, Writer out) throws IOException;
	}
	public static final int TEXT = 1;
	public static final int DATA = 2;
	public class Token
	{
		Token(int type, String key)
		{
			this.value = key;
			this.type = type;
		}
		String value;
		int type;
		public void write(Writer out, Map<String,Object> values) throws IOException
		{
			if (type == TEXT)
				out.write(value);
			else
			{
				Object data = values.get(value);
				if (data != null)
					out.write(String.valueOf(data));
			}
		}
		public void write(Writer out, TokenHandler handler) throws IOException
		{
			if (type == TEXT)
				out.write(value);
			else
			{
				handler.write(value, out);
			}
		}
		/**
		 * @return the Type of this token (either Template.TEXT or Template.DATA)
		 */
		public int getType()
		{
			return type;
		}
		/**
		 * @return the value of this token (the token's text for text tokens, and the token's 'key' for data tokens) 
		 */
		public String getValue()
		{
			return value;
		}
	}
	private List<Token> tokens;
	/**
	 * 
	 */
	public Template(String templateText)
	{
		parse(templateText);
	}
	/**
	 * @param templateText
	 */
	private void parse(String templateText)
	{
		int currentIndex = 0;
		tokens = new ArrayList<Token>();
		while (currentIndex < templateText.length() && currentIndex >= 0)
		{
			int nextIndex = templateText.indexOf("${", currentIndex);
			if (nextIndex == -1)
			{
				tokens.add(new Token(TEXT, templateText.substring(currentIndex)));
				currentIndex = nextIndex;
			}
			else
			{
				tokens.add(new Token(TEXT, templateText.substring(currentIndex, nextIndex)));
				int tokenStart = nextIndex + 2;
				int tokenEnd = templateText.indexOf('}', tokenStart);
				if (tokenEnd < 0)
					throw new InvalidInputException(
						"Invalid template: missing '}' for '${'.  Position="
							+ nextIndex
							+ " Text="
							+ templateText.substring(nextIndex, Math.min(templateText.length(), nextIndex + 20)));

				tokens.add(new Token(DATA, templateText.substring(tokenStart, tokenEnd)));
				currentIndex = tokenEnd + 1;
			}
		}

	}
	/**
	 * @param properties
	 */
	/**
	 * @param out
	 */
	public void write(Writer out, Map<String,Object> values) throws IOException
	{
		for (int i = 0; i < tokens.size(); i++)
		{
			Token token = (Token) tokens.get(i);
			token.write(out, values);
		}
	}
	public void write(Writer out, TokenHandler handler) throws IOException
	{
		for (int i = 0; i < tokens.size(); i++)
		{
			Token token = (Token) tokens.get(i);
			token.write(out, handler);
		}
	}

	/**
	 * @return
	 */
	public List<Token> getTokens()
	{
		return tokens;
	}

}

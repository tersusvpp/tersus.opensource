package tersus.util;

import java.util.EventListener;

public interface ListListener<T> extends EventListener
{
    public void contentChanged(IObservableList<T> list);
    public void itemAdded(final T item);
    public void itemChanged(T item, String propertyName, Object oldValue, Object newValue);
}

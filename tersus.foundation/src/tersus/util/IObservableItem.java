package tersus.util;

import java.beans.PropertyChangeListener;

/**
 * An item that can be monitored by PropertyChangeListeners
 * @author youval
 *
 */
public interface IObservableItem
{   
    void addListener(PropertyChangeListener listener);
    void removeListener(PropertyChangeListener listener);
}

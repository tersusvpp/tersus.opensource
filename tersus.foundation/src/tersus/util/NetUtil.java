package tersus.util;

import java.io.IOException;
import java.net.ServerSocket;

import javax.net.ServerSocketFactory;

public class NetUtil
{

	public static int findFreePort(int fromPort, int toPort)
	{
		for (int port = fromPort; port <= toPort; port++)
		{
			ServerSocket ss = null;
			try
			{
				ss = ServerSocketFactory.getDefault().createServerSocket(port);
				return port;
			}
			catch (Exception e)
			{
				// Port assumed to be busy
			}
			finally
			{
				if (ss != null)
				{
					try
					{
						ss.close();
					}
					catch (IOException ioe)
					{
					}
				}
			}
		}
		return -1;
	}

}

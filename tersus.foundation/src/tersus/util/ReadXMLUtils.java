package tersus.util;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class ReadXMLUtils
{
	public static Element getElement(Document document, String tag, boolean mandatory)
	{
		NodeList list = document.getElementsByTagName(tag);
		if (list.getLength() > 1)
			throw new ReadXMLException("Invalid plugin descriptor: multiple <" + tag + "> elements");
		if (list.getLength() == 0)
		{
			if (mandatory)
				throw new ReadXMLException("Invalid plugin descriptor: missing element <" + tag
						+ ">");
			else
				return null;
		}
		return (Element) list.item(0);
	}

	public static String getElementContent(Document document, String tag)
	{
		Element element = getElement(document, tag, false);
		if (element == null)
			return null;

		NodeList childNodes = element.getChildNodes();
		if (childNodes.getLength() > 1)
			throw new ReadXMLException("Element <" + tag + "> unexpectedly contains multiple nodes");

		if (childNodes.getLength() == 0)
			return null;

		String text = ((Text) childNodes.item(0)).getData();
		if (text == null)
			return null;

		text = text.trim();
		if (text.length() == 0)
			return null;

		return text;
	}
}

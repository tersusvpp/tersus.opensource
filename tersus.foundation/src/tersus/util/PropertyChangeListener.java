package tersus.util;

public interface PropertyChangeListener
{
	void propertyChange(Object sourceObject, String property, Object oldValue, Object newValue);
}

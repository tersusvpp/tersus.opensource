/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.util;



/**
 * @author Youval Bronicki
 *  
 */
public class Multiplicity
{
    private static final String EXAMPLE = "example";
    public String toString()
    {
        return str;
    }

    public int min=1, max=1, def=1;

    private String str;

    public boolean isExample;

    /**
     * @param property
     */
    public Multiplicity(String str)
    {
        this.str = str;
        if (str == null)
            this.str = "";
        if (this.str.length() == 0)
            min = max = def = 1;
        else if (this.str.equals(EXAMPLE))
        {
            max = def = 1;
            min = 0;
            isExample = true;
        }
        else
        {
            String regex = "^([-?*+0-9]+)?(\\[([0-9]+)\\])?";
            String[] m = Regex.exec(regex, str);
            if (m == null)
                throw new InvalidInputException(
                        "Invalid multiplicity pattern '" + str + "'");
            setValidRange(m[1]);
            setDefault(m[3]);
        }
    }

    /**
     * @param string
     */
    private void setDefault(String defaultStr)
    {
        if (defaultStr != null)
        {
            def = Integer.parseInt(defaultStr);
            if (def < min)
                throw new InvalidInputException("In multiplicity pattern '"+str+"', the default number of instances ("+def+") is smaller than the minimal valid number of instances ("+min+")");
        }
        else if (min>1)
            def = min;
        else
            def = 1;
    }

    /**
     * @param str
     * @param rangeStr
     */
    private void setValidRange(String rangeStr)
    {
        if (rangeStr == null)
            return;
        min = 0;
        max = Integer.MAX_VALUE;
        if ("+".equals(rangeStr))
        {
            min = 1;
        }
        else if ("*".equals(rangeStr))
        {
            min = 0;
        }
        else if ("?".equals(rangeStr))
        {
            min = 0;
            max = 1;
        }
        else
        {
            int i = rangeStr.indexOf('-');
            if (i < 0)
            {
                min = max = Integer.parseInt(rangeStr);
            }
            else
            {
                if (i > 0)
                    min = Integer.parseInt(rangeStr.substring(0, i));
                if (i < rangeStr.length() - 1)
                    max = Integer.parseInt(rangeStr.substring(i + 1));
            }
        }
    }

    /**
     * @param instances
     * @return
     */
    public boolean validate(int instances)
    {
        return instances >= min && instances <= max;
    }

}
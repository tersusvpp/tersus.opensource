package tersus.util;
public class Watchdog extends Thread
{
    private boolean done;
    private Thread watched;
    private long timeoutMilliseconds;
    public synchronized void done()
    {
        this.done = true;
        this.interrupt();
    }
    synchronized boolean isDone()
    {
        return done;
    }
    
    public Watchdog(Thread watched, long timeoutMilliseconds)
    {
        this.watched = watched;
        this.timeoutMilliseconds = timeoutMilliseconds;
    }
    
    public void run()
    {
        try
        {
            sleep(timeoutMilliseconds);
            if (!isDone())
                watched.interrupt();
        }
        catch (InterruptedException e)
        {
            
        }
    }

    
}

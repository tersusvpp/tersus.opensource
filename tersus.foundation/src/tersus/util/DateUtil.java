/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.util;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * 
 * Date-related utilities (e.g. formatting).
 * 
 * @author Ofer Brandes
 *
 */

public class DateUtil
{
	// Calendar defaults (Ofer, 19/1/2005)
	public static final int FIRST_DAY_OF_WEEK = GregorianCalendar.MONDAY;	// Per ISO-8601
	public static final int LAST_DAY_OF_WEEK = GregorianCalendar.SUNDAY;	// Per ISO-8601
	public static final int MINIMAL_DAYS_IN_FIRST_WEEK = 4;					// Per ISO-8601
	
	// List of recognized date formats (for format syntax see 'java.text.SimpleDateFormat')
	// FUNC3 Properly handle cases when the input matches multiple formats
	private static String[] formats = {	"yyyy-MM-dd",
	        							"MMMM-dd-yyyy",	// BUG2 "MMMM-dd-yyyy" doesn't work
										"MMMM dd yyyy",	// BUG2 "MMMM dd yyyy" doesn't work
										"MMM dd yyyy",	// BUG2 "MMM dd yyyy" doesn't work
										"dd/MM/yy",
										"dd/MM/yyyy",
										"yyyy-MM-dd"
};
							
	private static SimpleDateFormat dater = new SimpleDateFormat();

	/**
	 * Finds the format of a string representation of a date.
	 * 
	 * Tries to interpret the input string according each of a list of formats (as defined in 'DateUtil.formats'),
	 * and returns the first matching format (or null if none matches).
	 *
	 * @param s A string representation of some date (e.g. "29/07/2003").
	 * @return The format of the input string representation (e.g. "dd/MM/yyyy"), or null if the input string seems not to contain a valid date.
	 * 
	 * As of 1/2006, only used by the (obsolete?) 'ImportXMLDataStructureByExampleCommand'.
	 */										
	public static String formatOf (String s)
	{
		for (int i = 0; i < formats.length; i++)
		{
			dater.applyPattern (formats[i]);
			ParsePosition pos = new ParsePosition(0);
			if (dater.parse(s,pos) != null)
				return (formats[i]);
		}
		
		return (null);
	}
	/**
     * Parses the string argument as a Date.
     * 
     * Tries to parse the input string according each of a list of formats (as defined in 'DateUtil.formats'),
	 * and returns the Date resulting in the first sucessful parse (or null if none matches).
	 * @param s The input string
	 * @return The date resulting from parsing the string, or null if parsing failed
	 * 
	 * As of 1/2006, not used anywhere.
	 */ 
	public static Date parseDate(String s)
	{
		Date date;
		for (int i = 0; i < formats.length; i++)
		{
			dater.applyPattern (formats[i]);
			ParsePosition pos = new ParsePosition(0);
			if ((date = dater.parse(s,pos)) != null)
				return date;
		}
		
		return (null);
	}

	public static Date getDate (GregorianCalendar calendar) // Set time to default
	{
		calendar.set(GregorianCalendar.HOUR_OF_DAY,0);	// Is 12 better (high noon)?
		calendar.set(GregorianCalendar.MINUTE,0);
		calendar.set(GregorianCalendar.SECOND,0);
		calendar.set(GregorianCalendar.MILLISECOND,0);

		return calendar.getTime();
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.util;

import java.util.List;

/**
 * 
 * @author Liat Shiff
 */
public interface IObservableList<T>
{
    void addListener(ListListener<T> l);
    void removeListener(ListListener<T> l);
    List<T> getContent();
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.util;

import java.util.ArrayList;
import java.util.List;


/**
 * NICE2 [Description]
 * 
 * @author Youval Bronicki
 *
 */
public class Bidi
{
	private class Segment
	{
		int direction = UNKNOWN;
		int end;
		int start;

		void appendToOutput()
		{
			if (direction == RTL || direction == UNKNOWN && mainDirection == RTL)
			{
				for (int i = end - 1; i >= start; i--)
				{
					output.append(paragraph.charAt(i));
				}
			}
			else
			{
				//LTR
				for (int i = start; i < end; i++)
				{
					output.append(paragraph.charAt(i));
				}
			}
		}
	}
	private static final int LTR = 1;
	private static final int RTL = 2;
	private static final int SPACE = 3;
	/**
	 * 
	 */
	private static final int UNKNOWN = 0;
	public static String reorderVisually(String paragraph)
	{
		Bidi bidi = new Bidi(paragraph);
		bidi.reorderVisually();
		return bidi.output.toString();
	}
	private int length;
	private StringBuffer output;
	private String paragraph;
	private int position;
	private int mainDirection = UNKNOWN;

	private Bidi(String paragraph)
	{
		this.paragraph = paragraph;
		length = paragraph.length();
		output = new StringBuffer();
		position = 0;
	}
	/**
	 * @param start
	 * @param length
	 * @return
	 */
	private List analyseLine(int start, int end)
	{
		int position = start;
		List segments = new ArrayList();
		Segment currentSegment = new Segment();
		segments.add(currentSegment);
		currentSegment.start = start;
		while (position < end)
		{
			char c = paragraph.charAt(position);
			int directionality = BidiCharacter.getDirectionality(c);
			int direction = UNKNOWN;
			switch (directionality)
			{
				case BidiCharacter.DIRECTIONALITY_LEFT_TO_RIGHT :
				case BidiCharacter.DIRECTIONALITY_EUROPEAN_NUMBER :
					direction = LTR;
					break;
				case BidiCharacter.DIRECTIONALITY_RIGHT_TO_LEFT :
					direction = RTL;
					break;
				case BidiCharacter.DIRECTIONALITY_WHITESPACE :
					direction = SPACE;
					break;
			}
			if (direction != UNKNOWN && direction != currentSegment.direction)
			{
				if (currentSegment.direction == UNKNOWN)
				{
					currentSegment.direction = direction;
				}
				else
				{
					// Switch direction - start a new segment
					currentSegment.end = position;
					currentSegment = new Segment();
					currentSegment.start = position;
					currentSegment.direction = direction;
					segments.add(currentSegment);
				}
			}
			++position;
		}
		if (Misc.ASSERTIONS)
			Misc.assertion(position == end);
		currentSegment.end = end;
		return segments;
	}
	/**
	 * @param lineStart
	 * @param lineEnd
	 */
	private void reorderLine(int start, int end)
	{
		List segments = analyseLine(start, end);
		if (Misc.ASSERTIONS)
			Misc.assertion(segments.size() > 0);
		mainDirection = ((Segment) segments.get(0)).direction;
		if (mainDirection == RTL)
		{
			// add segments in reverse order
			for (int i = segments.size() - 1; i >= 0; i--)
			{
				Segment segment = (Segment) segments.get(i);
				segment.appendToOutput();
			}
		}
		else
		{
			//LTR
			for (int i = 0; i < segments.size(); i++)
			{
				Segment segment = (Segment) segments.get(i);
				segment.appendToOutput();
			}
		}

	}

	private void reorderVisually()
	{
		reorderLine(0, length); //FUNC3 handle multiple lines
	}

}

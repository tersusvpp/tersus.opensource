package tersus.util;

public interface EntryFilter
{
	boolean accept (String entryName);

}

package tersus.util;

public class ReadXMLException extends RuntimeException
{
	public ReadXMLException(String message)
	{
		super(message);
	}

	public ReadXMLException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public ReadXMLException(Throwable cause)
	{
		super(cause);
	}
}

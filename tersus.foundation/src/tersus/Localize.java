/*******************************************************************************
 * Copyright (c) 2003-2020 Tersus Software Ltd. and others. All rights reserved.
 * 
 * This program is made available under the terms of the GNU General Public
 * License v2, which is part of this distribution and is available at
 * http://www.gnu.org/licenses/gpl.txt.
 * 
 * Contributors: Tersus Software Ltd.� Initial API and implementation
 ******************************************************************************/
package tersus;

import java.util.Arrays;
import java.util.Locale;

/**
 * @author David Davidson
 *
 */
public interface Localize
{
	/**
	 * @param filename
	 * @return
	 */
	public static String getLocalizedFilename(String filename)
	{
		String lang = getLocale();
		
		return getLocalizedFilename(filename, lang);
	}

	/**
	 * @param filename
	 * @param lang
	 * @return
	 */
	public static String getLocalizedFilename(String filename, String lang)
	{
		String[] parts = filename.split("\\" + ProjectStructure.FILE_SEPARATOR);
		int length = parts.length;

		String[] langParts = Arrays.copyOf(parts, length + 1);
		langParts[length-1] = lang;
		langParts[length] = parts[length-1];
		
		return String.join(ProjectStructure.FILE_SEPARATOR, langParts);
	}

	/**
	 * @return
	 */
	public static String getLocale()
	{
		return Locale.getDefault().getLanguage();
	}

}

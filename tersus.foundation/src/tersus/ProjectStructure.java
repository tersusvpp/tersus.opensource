package tersus;

public interface ProjectStructure
{
	String WEB="web";
	String SCRIPTS="scripts";
	String STYLES="styles";
	String TINY_MCE="tiny_mce";
	String MODELS="models";
	String SCRIPT_EXCLUDE=".script-exclude";
	String STYLE_EXCLUDE=".style-exclude";
	String DATABASE="database";
	String WORK="work";
	String LOGS="logs";
	String TRACE="trace";
	String CONFIGURATION_XML="Configuration.xml";
	String DEFAULT_NAV_SCRIPT_NAME = "tersus-default-navigation.js";
	String URL_NAV_SCRIPT_NAME = "tersus-url-navigation.js";
	String IMAGES = "images";
	String SOUNDS = "sounds";
	String SERVER_SCRIPTS="server-scripts";
	String PROBLEMS="problems.txt";
	String FILE_SEPARATOR=".";
	String DEFAULT_LOCALE="en";
	public static final String PALETTE_XML = "palette" + FILE_SEPARATOR + "xml";
}

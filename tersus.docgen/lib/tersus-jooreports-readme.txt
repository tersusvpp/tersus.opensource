This library contains a modified version of JOOReports versions 2.0.0.

The original JOOReports library and the version modified by Tersus are distributed under the terms of the LGPL license, which is part of this distribution.

The source code of the modified version can be downloaded from  http://downloads.tersus.com/misc_files/lib/tersus-jooreports-2.0.0-src.jar

Note:

We modified JOOReports version 2.0.0 in order to make it compatible with JODConverter 2.2.2.

/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.docgen;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.BinaryValue;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.ColorUtil;
import tersus.util.InvalidInputException;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

/**
 * @author Youval Bronicki
 * 
 */
public class StampPDF extends Plugin
{

    protected SlotHandler textTrigger, inputPDFTrigger, outputPDFExit;

    private SlotHandler xTrigger;

    private SlotHandler yTrigger;

    private SlotHandler rotationTrigger;

    private SlotHandler textColorTrigger;

    private SlotHandler backgroundColorTrigger;

    private SlotHandler fontSizeTrigger;

    protected final static Role TEXT = Role.get("<Text>");

    protected final static Role X = Role.get("<X>");

    protected final static Role Y = Role.get("<Y>");

    protected final static Role TEXT_COLOR = Role.get("<Text Color>");

    protected final static Role BACKGROUND_COLOR = Role
            .get("<Background Color>");

    protected final static Role ROTATION = Role.get("<Rotation>");

    protected final static Role INPUT_PDF = Role.get("<Input PDF>");

    protected final static Role OUTPUT_PDF = Role.get("<Output PDF>");

    private static final Role FONT_SIZE = Role.get("<Font Size>");

    /**
     * 
     */

    public void start(RuntimeContext context, FlowInstance flow)
    {
        Invocation invocation = new Invocation(context, flow);
        invocation.run();
    }

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        textTrigger = getRequiredMandatoryTrigger(TEXT, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        textColorTrigger = getNonRequiredTrigger(TEXT_COLOR, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        backgroundColorTrigger = getNonRequiredTrigger(BACKGROUND_COLOR,
                Boolean.FALSE, BuiltinModels.TEXT_ID);
        fontSizeTrigger = getNonRequiredTrigger(FONT_SIZE, Boolean.FALSE,
                BuiltinModels.NUMBER_ID);
        inputPDFTrigger = getRequiredMandatoryTrigger(INPUT_PDF, Boolean.FALSE,
                BuiltinModels.BINARY_ID);
        xTrigger = getNonRequiredTrigger(X, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        yTrigger = getNonRequiredTrigger(Y, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        rotationTrigger = getNonRequiredTrigger(ROTATION, Boolean.FALSE,
                BuiltinModels.NUMBER_ID);
        outputPDFExit = getRequiredExit(OUTPUT_PDF, Boolean.FALSE,
                BuiltinModels.BINARY_ID);
    }

    private class Invocation
    {
        private static final String LEFT = "LEFT";

        private static final String RIGHT = "RIGHT";

        private static final String CENTER = "CENTER";

        private static final String MIDDLE = "MIDDLE";

        private static final String TOP = "TOP";

        private static final String BOTTOM = "BOTTOM";

        private RuntimeContext context;

        private FlowInstance flow;

        int fontSize = 12;

        int marginLeft = 15;

        int marginRight = 15;

        int marginTop = 10;

        int marginBottom = 10;

        private String stampText;

        private BaseFont baseFont;

        private Color backgroundColor = Color.WHITE;

        private Color textColor = Color.BLACK;

        private float textHeight, textWidth;

        private String xInput = LEFT;

        private String yInput = TOP;

        private float textRotation = 0;

        private int alignment = Element.ALIGN_CENTER;

        private float yPosition;

        private float xPosition;

        private double rotationRadians;

        private float textAscent;

        private float textDescent;

        Invocation(RuntimeContext context, FlowInstance flow)
        {
            this.context = context;
            this.flow = flow;
        }

        public void run()
        {
            try
            {
                init();
                BinaryValue input = (BinaryValue) inputPDFTrigger.get(context,
                        flow);
                // we create a reader for a certain document
                PdfReader reader = new PdfReader(input.toInputStream());
                int n = reader.getNumberOfPages();
                // we create a stamper that will copy the document to a new file
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                PdfStamper stamp = new PdfStamper(reader, out);
                // Adding text to each page
                int i = 0;
                PdfContentByte over;
                while (i < n)
                {
                    i++;
                    over = stamp.getOverContent(i);
                    Rectangle pageSize = reader.getPageSize(i);
                    int rotation = reader.getPageRotation(i);
                    float pageHeight, pageWidth;
                    if (rotation == 90 || rotation == 270)
                    {
                        pageHeight = pageSize.getWidth();
                        pageWidth = pageSize.getHeight();
                    }
                    else
                    {
                        pageHeight = pageSize.getHeight();
                        pageWidth = pageSize.getWidth();
                    }
                    setTextPosition(pageHeight, pageWidth);
                    if (backgroundColor != null)
                    {
                        over.setColorFill(backgroundColor);
                        over.rectangle(0, yPosition, pageSize.getWidth(),
                                textHeight);
                        over.fill();
                    }
                    over.setColorFill(textColor);
                    over.beginText();
                    over.setFontAndSize(baseFont, fontSize);
                    over.showTextAligned(alignment, stampText, xPosition,
                            yPosition, textRotation);
                    over.endText();
                }
                stamp.close();
                BinaryValue outputPDF = new BinaryValue(out.toByteArray());
                setExitValue(outputPDFExit, context, flow, outputPDF);
            }
            catch (Exception e)
            {
                throw new ModelExecutionException("Failed to stamp PDF", e
                        .getMessage(), e);
            }

        }

        private void init() throws DocumentException, IOException
        {
            setXYInput();
            setRotation();

            stampText = ((String) textTrigger.get(context, flow));
            if (fontSizeTrigger != null
                    && fontSizeTrigger.get(context, flow) != null)
                fontSize = (int) ((Number) fontSizeTrigger.get(context, flow)).value;
            baseFont = BaseFont.createFont(BaseFont.HELVETICA,
                    BaseFont.WINANSI, BaseFont.EMBEDDED);
            textAscent = baseFont.getAscentPoint(stampText, fontSize); // 
            textDescent = baseFont.getDescentPoint(stampText, fontSize);
            textHeight = textAscent - textDescent;
            textWidth = baseFont.getWidthPoint(stampText, fontSize);

            if (backgroundColorTrigger != null)
                backgroundColor = ColorUtil.getColor((String) backgroundColorTrigger
                        .get(context, flow));
            if (textColorTrigger != null
                    && textColorTrigger.get(context, flow) != null)
                textColor = ColorUtil.getColor((String) textColorTrigger.get(
                        context, flow));

        }

        private void setRotation()
        {
            if (rotationTrigger != null
                    && rotationTrigger.get(context, flow) != null)
                textRotation = (float) ((tersus.runtime.Number) rotationTrigger
                        .get(context, flow)).value;
            rotationRadians = 2 * Math.PI * textRotation / 360.0;
        }

        private void setTextPosition(float pageHeight, float pageWidth)
        {
            if (xInput.endsWith("%"))
            {
                xPosition = (float) (pageWidth * 0.01 * Double.parseDouble(xInput
                        .substring(0, xInput.length() - 1)));
                alignment = Element.ALIGN_LEFT;
            }
            if (yInput.endsWith("%"))
            {
                yPosition = pageHeight  -
                         (float) (pageHeight * 0.01 * Double.parseDouble(yInput.substring(
                                0, yInput.length() - 1))) - textAscent;
            }
            if (yInput == TOP)
                yPosition = pageHeight - marginTop - textAscent;
            else if (yInput == BOTTOM)
            {
                yPosition = marginBottom - textDescent;
            }
            else if (yInput == MIDDLE)
                yPosition = marginBottom
                        + (pageHeight - marginBottom - marginTop - textHeight)
                        / 2;
            if (xInput == LEFT)
            {
                xPosition = marginLeft;
                alignment = Element.ALIGN_LEFT;
            }
            else if (xInput == CENTER)
            {
                xPosition = pageWidth / 2;
                alignment = Element.ALIGN_CENTER;
            }
            else if (xInput == RIGHT)
            {
                xPosition = pageWidth - marginRight;
                alignment = Element.ALIGN_RIGHT;
            }
            if (rotationRadians != 0)
            {
                double verticalDelta = Math.sin(rotationRadians) * textWidth;
                double horizontalDelta = Math.sin(rotationRadians) * textHeight;
                if (xInput == CENTER)
                {
                    if (yInput == TOP)
                        yPosition -= Math.abs(verticalDelta / 2);
                    else if (yInput == BOTTOM)
                        yPosition += Math.abs(verticalDelta / 2);
                }
                else if (xInput == RIGHT || xInput == LEFT)
                {
                    if (xInput == RIGHT)
                        verticalDelta = -verticalDelta;
                    if (yInput == TOP && verticalDelta > 0 || yInput == BOTTOM
                            && verticalDelta < 0)
                        yPosition -= verticalDelta;
                    if (horizontalDelta > 0 && xInput == LEFT
                            || horizontalDelta < 0 && xInput == RIGHT)
                        xPosition += horizontalDelta;
                }
            }
        }

        private void setXYInput()
        {
            if (xTrigger != null && xTrigger.get(context, flow) != null)
                xInput = (String) xTrigger.get(context, flow);
            if (xInput.equalsIgnoreCase(LEFT))
                xInput = LEFT;
            else if (xInput.equalsIgnoreCase(RIGHT))
                xInput = RIGHT;
            else if (xInput.equalsIgnoreCase(CENTER)
                    || xInput.equalsIgnoreCase(MIDDLE))
                xInput = CENTER;
            else if (!xInput.endsWith("%"))
                throw new InvalidInputException("Invalid <X> specification '"
                        + xInput + "' (supported options: right, left, center, X%");

            if (yTrigger != null && yTrigger.get(context, flow) != null)
                yInput = (String) yTrigger.get(context, flow);
            if (yInput.equalsIgnoreCase(TOP))
                yInput = TOP;
            else if (yInput.equalsIgnoreCase(BOTTOM))
                yInput = BOTTOM;
            else if (yInput.equalsIgnoreCase(CENTER)
                    || yInput.equalsIgnoreCase(MIDDLE))
                yInput = MIDDLE;
            else if (!yInput.endsWith("%"))
                throw new InvalidInputException("Invalid <Y> specification '"
                        + yInput + "' (supported options: top, bottom, middle, X%");
        }
    }
}

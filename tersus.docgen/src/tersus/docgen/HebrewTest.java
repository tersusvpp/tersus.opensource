/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.docgen;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import net.sf.jooreports.templates.ZippedDocumentTemplate;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import tersus.util.FileUtils;

import freemarker.template.TemplateException;

/**
 * @author Youval Bronicki
 *
 */
public class HebrewTest
{
    public void testHebrew() throws Exception {
        File templateFile = new File("test-data/hebrew-template.odf");
        
        //The following code is an awkward way of reading an XML file with a single element. There should be a better way ...
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document data = builder.parse(new File("test-data/hebrew-data.xml"));
        NodeList children = data.getDocumentElement().getChildNodes();
        StringBuffer text = new StringBuffer();
        for (int i=0; i<children.getLength(); i++)
        {
            Node node = children.item(i);
            if (node instanceof Text)
            {
                text.append(((Text)node).getData());
            }
            
        }
        HashMap model = new HashMap();
        model.put(data.getDocumentElement().getTagName(), text.toString());
        ZippedDocumentTemplate template = new ZippedDocumentTemplate(new File("test-data/hebrew-template.odt"));
        File tempFile = File.createTempFile("generated-hebrew-document",".odt");
        System.out.println(tempFile.getPath());
        tempFile.deleteOnExit();
        template.createDocument(model, new FileOutputStream(tempFile));
        File expectedOutputFile = new File("test-data/hebrew-output.odt");
        if (! FileUtils.compareFiles(tempFile, expectedOutputFile))
            throw new AssertionError("Output differs from expected");
    }
    public static void main(String[] args) throws Exception
    {
        (new HebrewTest()).testHebrew();
    }

}

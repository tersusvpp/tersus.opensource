/*
 * Copyright (c) 2004 Tersus Software Ltd. All rights reserved.
 */
package tersus.docgen;

import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import tersus.runtime.BinaryValue;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.Number;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SubFlowHandler;
import tersus.util.Misc;

/**
 * Wraps a runtime object in a java.util.Map interface, suitable for use by JooReports
 * @author Youval Bronicki
 *
 */
public class RuntimeObjectAdapter extends AbstractMap implements Map
{
    private class ElementEntry implements Entry
    {
        ElementHandler element;
        public ElementEntry(ElementHandler element)
        {
            super();
            this.element = element;
        }
        /* (non-Javadoc)
         * @see java.util.Map.Entry#getKey()
         */
        public Object getKey()
        {
            return Misc.sqlize(element.getRole().toString());
        }

        /* (non-Javadoc)
         * @see java.util.Map.Entry#getValue()
         */
        public Object getValue()
        {
            Object _value = element.get(context, instance);
            if (element.isRepetitive())
                return new RuntimeObjectListAdapter(context, element.getChildInstanceHandler(), (List)_value);
            else if (element instanceof SubFlowHandler)
                return new ActionMethodWrapper(context, (SubFlowHandler)element, (FlowInstance)instance);
            else
                return wrap(context, element.getChildInstanceHandler(), _value);
        }

        /* (non-Javadoc)
         * @see java.util.Map.Entry#setValue(java.lang.Object)
         */
        public Object setValue(Object value)
        {
            throw new UnsupportedOperationException();
        }
        
    }
    private class RuntimeObjectEntryIterator implements Iterator
    {

        int i=0;

        /* (non-Javadoc)
         * @see java.util.Iterator#hasNext()
         */
        public boolean hasNext()
        {
            return i < handler.elementHandlers.length;
        }

        /* (non-Javadoc)
         * @see java.util.Iterator#next()
         */
        public Object next()
        {
            return new ElementEntry(handler.elementHandlers[i++]);
        }
        /* (non-Javadoc)
         * @see java.util.Iterator#remove()
         */
        public void remove()
        {
            throw new UnsupportedOperationException();
        }
        
    }
    private class RuntimeObjectEntrySet extends AbstractSet
    {

        /* (non-Javadoc)
         * @see java.util.AbstractCollection#iterator()
         */
        public Iterator iterator()
        {
            return new RuntimeObjectEntryIterator();
        }

        /* (non-Javadoc)
         * @see java.util.AbstractCollection#size()
         */
        public int size()
        {
            return handler.elementHandlers.length;
        }
        
    }
    private RuntimeContext context;
    private RuntimeObjectEntrySet entrySet;
    private InstanceHandler handler;
    private Object instance;

    public static Object wrap(RuntimeContext context, InstanceHandler handler, Object instance)
    {
        if (instance instanceof BinaryValue)
        {
            return new SimpleObjectWrapper(instance);
        }
        else if (instance instanceof Number)
        {
            return new Double(((Number)instance).value);
        }
        else if (handler instanceof LeafDataHandler)
        {
            return instance == null? "": instance;
        }
        else 
            return new RuntimeObjectAdapter(context, handler, instance);
    }
    private RuntimeObjectAdapter(RuntimeContext context, InstanceHandler handler, Object instance)
    {
        this.context = context;
        this.handler = handler;
        this.instance = instance;
        this.entrySet = new RuntimeObjectEntrySet();
    }
    /* (non-Javadoc)
     * @see java.util.AbstractMap#entrySet()
     */
    public Set entrySet()
    {
        return entrySet;
    }

}

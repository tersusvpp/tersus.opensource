/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.docgen;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.jooreports.templates.DocumentTemplate;
import tersus.util.Misc;

/**
 * A JooReports ContentWrapper with the following functions: 1) Add xml escaping
 * 2) Convert newlines ('\n') to OpenDocument line breaks ( <text:line-break />)
 * 3) Handle
 * 
 * @row ...@ directives by replacing the containing row with the content of the
 *      directive. These directives are used to create repetitive rows in
 *      tables.
 * @author Youval Bronicki
 *  
 */
public class PreProcessor implements DocumentTemplate.ContentWrapper
{
    static final String FTL = "<#ftl>\n";

    static final String PREFIX = "<#escape any as any?xml?replace(\"\\n\",\"<text:line-break />\")>";

    static final String SUFFIX = "</#escape>";

    static final String FTL_ALT = "[#ftl]\n";

    static final String PREFIX_ALT = "[#escape any as any?xml?replace(\"\\n\",\"<text:line-break />\")]";

    static final String SUFFIX_ALT = "[/#escape]";

    protected String macros = "";

    public PreProcessor(String macros)
    {
        if (macros != null)
            this.macros = macros;
    }

    /*
     * (non-Javadoc)
     * 
     * @see net.sf.jooreports.templates.DocumentTemplate.ContentWrapper#wrapContent(java.lang.String)
     */
    public String wrapContent(String content)
    {
        content = unescapeExpression(content);
        content = processPattern(content, "row", "table:table-row");
        content = processPattern(content, "p", "text:p");
        content = processPattern(content, "note", "office:annotation");
        content = processPattern(content, "table", "table:table");
        return FTL + PREFIX + macros + content + SUFFIX;
    }

    private String unescapeExpression(String content)
    {
        Pattern expressionPattern = Pattern.compile("\\$\\{[^}]+\\}");
        Matcher m = expressionPattern.matcher(content);
        StringBuffer out = new StringBuffer(content.length());
        int last = 0;
        while (m.find())
        {
            String expression = m.group();
            expression = unescapeXML(expression);
            out.append(content.substring(last, m.start()));
            out.append(expression);
            last = m.end();
        }
        if (last < content.length())
            out.append(content.substring(last));
        String outputString = out.toString();
        return outputString;
    }

    private String processPattern(String content, String keyword, String tag)
    {
        Pattern wholePattern = Pattern.compile("<" + tag + "[ >].*?@" + keyword
                + ":?(.*?)(?<!&lt;)@.*?</" + tag + ">");
        Pattern startPattern = Pattern.compile("<" + tag + "[ >]");
        Pattern startP = Pattern.compile("<text:p.*?>");
        Pattern endP = Pattern.compile("</text:p>");
        Matcher m = wholePattern.matcher(content);
        StringBuffer out = new StringBuffer(content.length());
        int last = 0;
        while (m.find())
        {
            String op = m.group(1);
            int start = m.start();
            int end = m.end();
            String match = content.substring(start, end);
//            System.out.println("Found " + match);
//            System.out.println("start=" + start + " end=" + end);
            Matcher m1 = startPattern.matcher(match);
            int lastOpeningTag = 0;
            while (m1.find())
                lastOpeningTag = m1.start();
            start += lastOpeningTag;
            if (lastOpeningTag > 0)
            {
                System.out.println("adjusted: start=" + start + " end=" + end);
                System.out.println("Found " + content.substring(start, end));
            }
            System.out.println("op=" + op);
            op = removeParagraphs(op, startP, endP);
            op = unescapeXML(op);
            System.out.println("op=" + op);
            out.append(content.substring(last, start));
            out.append("\n");
            out.append(op);
            out.append("\n");
            last = end;
        }
        if (last < content.length())
            out.append(content.substring(last));
        String outputString = out.toString();
        return outputString;
    }

    private String removeParagraphs(String op, Pattern startP, Pattern endP)
    {
        op = startP.matcher(op).replaceAll("");
        op = endP.matcher(op).replaceAll("\n");

        return op;
    }

    private String unescapeXML(String s)
    {
        s = Misc.replaceAll(s, "&lt;", "<");
        s = Misc.replaceAll(s, "&gt;", ">");
        s = Misc.replaceAll(s, "&amp;", "&");
        s = Misc.replaceAll(s, "&quot;", "\"");
        return s;
    }

}
/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.docgen;

import java.io.IOException;

import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfReader;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.BinaryValue;
import tersus.runtime.ElementHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * @author Liat Shiff
 */
public class GetPDFAttributes extends Plugin
{
	protected final static Role PDF_ATTRIBUTES = Role.get("<PDF Attributes>");

	protected final static Role PDF = Role.get("<PDF>");
	private static final Role PAGE_INDEX = Role.get("<Page Index>");

	protected SlotHandler pdfTrigger, pageIdexTrigger;

	protected SlotHandler AttributeExit;
	
	private ElementHandler widthHandler;
	private ElementHandler heightHandler;

	public void start(RuntimeContext context, FlowInstance flow)
	{

		BinaryValue pdfData = (BinaryValue) pdfTrigger.get(context, flow);
		PdfReader r = null;
		try
		{
			r = new PdfReader(pdfData.toByteArray());
		}
		catch (IOException e1)
		{
			throw new EngineException("Failed to read PDF:"+e1.getClass().getName()+" - "+e1.getMessage(), null, e1);
		}
		
		if (pageIdexTrigger != null)
		{
			Number indexNumber = (Number) pageIdexTrigger.get(context, flow);
			Rectangle rec = r.getPageSize((int)indexNumber.value);
			setAttributes(rec, context, flow);
		}
		else
		{
			for (int i = 1; i <= r.getNumberOfPages(); i++)
			{
				setAttributes(r.getPageSize(i), context, flow);
			}
		}
	}

	private void setAttributes(Rectangle rec, RuntimeContext context, FlowInstance flow)
	{
		Object messageDescriptor = AttributeExit.getChildInstanceHandler().newInstance(context);
		
		widthHandler.set(context, messageDescriptor, new Number(rec.getRight()));
		heightHandler.set(context, messageDescriptor, new Number(rec.getTop()));

		accumulateExitValue(AttributeExit, context, flow, messageDescriptor);
	}
	
	public void initializeFromModel(Model model)

	{
		super.initializeFromModel(model);

		pdfTrigger = getRequiredMandatoryTrigger(PDF, Boolean.FALSE, BuiltinModels.BINARY_ID);

		pageIdexTrigger = getNonRequiredTrigger(PAGE_INDEX, null, BuiltinModels.NUMBER_ID);

		AttributeExit = getRequiredExit(PDF_ATTRIBUTES, Boolean.TRUE,
				DocumentAttributesStructure.DOCUMENT_ATTRIBUTES_ID);
		
		InstanceHandler pdfPageAttributesDescriptorHandler = AttributeExit.getChildInstanceHandler();
		widthHandler = pdfPageAttributesDescriptorHandler.getElementHandler(DocumentAttributesStructure.WIDTH);
		heightHandler = pdfPageAttributesDescriptorHandler.getElementHandler(DocumentAttributesStructure.HEIGHT);
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.docgen;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import com.artofsolving.jodconverter.DocumentConverter;
import com.artofsolving.jodconverter.openoffice.converter.OpenOfficeDocumentConverter;
import com.artofsolving.jodconverter.openoffice.connection.OpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.connection.OpenOfficeException;
import com.artofsolving.jodconverter.openoffice.connection.SocketOpenOfficeConnection;
import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.BinaryValue;
import tersus.runtime.ElementHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.FileUtils;
import tersus.util.Watchdog;

public abstract class ConvertDocumentBase extends Plugin
{
    static
    {
        FreemarkerSetup.setup();
    }

    private static final Role INPUT_FILE_ROLE = Role.get("<Input File>");
    private static final Role TIMEOUT = Role.get("<Timeout>");
    private SlotHandler inputFileTrigger;
    private ElementHandler contentTypeElement;
    private ElementHandler contentElement;
    private ElementHandler filenameElement;
    private InstanceHandler fileHandler;
    private SlotHandler timeoutTrigger;

    protected void convert(RuntimeContext context, FlowInstance flow, SlotHandler exit, String outputFileExtension,
            String outputContentType)
    {
        Object file = inputFileTrigger.get(context, flow);
        String filename = (String) filenameElement.get(context, file);
        BinaryValue content = (BinaryValue) contentElement.get(context, file);
        if (!outputFileExtension.startsWith("."))
            outputFileExtension = "." + outputFileExtension;
        if (filename == null)
            throw new ModelExecutionException("'File Name' is missing in <Input File>");
        if (content == null)
            throw new ModelExecutionException("'Content' is missing in <Input File>");
        else if (content.isEmpty())
            throw new ModelExecutionException("'Content' is empty in <Input File>");

        int dotIndex = filename.lastIndexOf('.');
        String suffix = filename.substring(dotIndex);
        String prefix = filename.substring(0, dotIndex);
        File ooInputFile = null;
        File ooOutputFile = null;
        try
        {
            ooInputFile = File.createTempFile("convertDocumentIn.", suffix);
            ooInputFile.deleteOnExit();
            ooOutputFile = File.createTempFile("convertDocumentOut.", outputFileExtension);
            ooOutputFile.deleteOnExit();
            FileOutputStream in = new FileOutputStream(ooInputFile);
            in.write(content.toByteArray());
            in.close();
        }
        catch (IOException e)
        {
            throw new EngineException("Conversion Error", "Failed to prepare files for document conversion", e);
        }
        long start = System.currentTimeMillis();
        OpenOfficeConnection connection = new SocketOpenOfficeConnection();
        long timeoutMilliseconds = context.getRemainingTimeMilliseconds();
        Number explicitTimeoutSeconds = null;
        if (timeoutTrigger != null)
            explicitTimeoutSeconds = (Number)getTriggerValue(context, flow, timeoutTrigger);
        if (explicitTimeoutSeconds != null && explicitTimeoutSeconds.value > 0)
            timeoutMilliseconds = Math.min( timeoutMilliseconds, Math.round(explicitTimeoutSeconds.value*1000) );
        Watchdog watchdog = new Watchdog(Thread.currentThread(), timeoutMilliseconds);
        long startTime = System.currentTimeMillis();
        watchdog.start();

        try
        {
            long time;
            try
            {
                connection.connect();
            }
            catch (Exception e)
            {
                time = System.currentTimeMillis() - startTime;
                String message = "Failed to connect to the document conversion server";
                if (time >= timeoutMilliseconds) // We're using time measurement to detect a timeout because we don't have direct access to the InterruptException that's thrown
                    message += " (timed out) ";
                message += ". Please make sure OpenOffice is listening on port " + SocketOpenOfficeConnection.DEFAULT_PORT
                + ".\nError Message=" + e.getMessage();
                throw new EngineException("Failed to connect to the document conversion server.",
                        message, e);
            }
            long connectionTime = System.currentTimeMillis() - start;
            try
            {
                start = System.currentTimeMillis();
                DocumentConverter converter = new OpenOfficeDocumentConverter(connection);
                converter.convert(ooInputFile, ooOutputFile);
                long conversionTime = System.currentTimeMillis() - start;
                start = System.currentTimeMillis();
                connection.disconnect();
                long disconnectTime = System.currentTimeMillis() - start;
                connection = null;
                if (false)
                    System.out.println("Connection:" + connectionTime + " conversion:" + conversionTime
                            + " disconnect:" + disconnectTime);
            }
            catch (OpenOfficeException e)
            {
                time = System.currentTimeMillis() - startTime;
                String message = "Failed to convert the documnet to PDF";
                String details;
                if (time >= timeoutMilliseconds) // We're using time measurement to detect a timeout because we don't have direct access to the InterruptException that's thrown
                    details = " Conversion timed out after "+time/1000.0+" seconds.";
                else
                    details = "This could be a problem in the source document (possibly an error in the template),"
                        + " or an infrastructure bug.";
                if (e.getCause() instanceof OpenOfficeException)
                {
                    details += "\nError message: " + e.getCause().getMessage();
                    throw new EngineException(message, details, e.getCause());
                }
                else
                    details += "\nError message: " + e.getMessage();
                throw new EngineException(message, details, e);
            }
        }
        finally
        {
            watchdog.done();
            if (connection != null)
                connection.disconnect();
        }
        byte[] outputBytes;
        try
        {
            outputBytes = FileUtils.readBytes(new FileInputStream(ooOutputFile), true);
        }
        catch (IOException e)
        {
            throw new EngineException("Conversion Error", "Failed to read output file", e);
        }
        String outputFileName = prefix + outputFileExtension;
        Object output = fileHandler.newInstance(context);
        contentElement.set(context, output, new BinaryValue(outputBytes));
        if (outputContentType != null)
            contentTypeElement.set(context, output, outputContentType);
        filenameElement.set(context, output, outputFileName);
        setExitValue(exit, context, flow, output);

    }

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        this.inputFileTrigger = getRequiredMandatoryTrigger(INPUT_FILE_ROLE, Boolean.FALSE, BuiltinModels.FILE_ID);
        fileHandler = this.inputFileTrigger.getChildInstanceHandler();
        this.contentTypeElement = fileHandler.getElementHandler(BuiltinModels.CONTENT_TYPE_ROLE);
        this.contentElement = fileHandler.getElementHandler(BuiltinModels.CONTENT_ROLE);
        this.filenameElement = fileHandler.getElementHandler(BuiltinModels.FILENAME_ROLE);
        timeoutTrigger = getNonRequiredTrigger(TIMEOUT, Boolean.FALSE, BuiltinModels.NUMBER_ID);

    }
    
}

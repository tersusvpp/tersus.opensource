/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.docgen;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import com.artofsolving.jodconverter.openoffice.connection.OpenOfficeException;
import com.artofsolving.jodconverter.openoffice.connection.SocketOpenOfficeConnection;
import net.sf.jooreports.templates.ZippedDocumentTemplate;
import net.sf.jooreports.templates.images.ByteArrayImageProvider;
import net.sf.jooreports.templates.images.ImageProvider;
import tersus.model.BuiltinModels;
import tersus.model.BuiltinPlugins;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.BinaryValue;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.sapi.CompositeValue;
import tersus.runtime.sapi.SAPIManager;
import tersus.util.FileUtils;

import com.sun.star.beans.PropertyValue;
import com.sun.star.document.XDocumentInsertable;
import com.sun.star.frame.XComponentLoader;
import com.sun.star.frame.XStorable;
import com.sun.star.lang.XComponent;
import com.sun.star.text.XParagraphCursor;
import com.sun.star.text.XTextCursor;
import com.sun.star.text.XTextDocument;
import com.sun.star.text.XTextRange;
import com.sun.star.ucb.XFileIdentifierConverter;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.util.XSearchDescriptor;
import com.sun.star.util.XSearchable;

import freemarker.template.TemplateHashModel;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;

/**
 * @author Youval Bronicki
 * 
 */
public class CreateOpenDocumentFromTemplate extends Plugin
{

    static
    {
        FreemarkerSetup.setup();
    }

    private SlotHandler templateTrigger;

    private static final Role TEMPLATE_ROLE = Role.get("<Template>");

    private static final Role DOCUMENT_ROLE = Role.get("<Document>");

    private static final Role IMAGES_ROLE = Role.get("<Images>");

    private static final Role DATA_ROLE = Role.get("<Data>");

    private static PropertyValue property(String name, Object value)
    {
        PropertyValue property = new PropertyValue();
        property.Name = name;
        property.Value = value;
        return property;
    }

    private static final PropertyValue[] HIDDEN = new PropertyValue[] { property(
            "Hidden", Boolean.TRUE) };

    private SlotHandler documentExit;

    private SlotHandler dataTrigger;

    private SlotHandler imagesTrigger;

    private static final Role MACROS = Role.get("<Macros>");

    private SlotHandler macrosTrigger;

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext,
     *      tersus.runtime.FlowInstance)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
        Object data;
        final SimpleObjectWrapper hasAttachment = new SimpleObjectWrapper(
                Boolean.FALSE);
        if (dataTrigger != null)
            data = RuntimeObjectAdapter.wrap(context, dataTrigger
                    .getChildInstanceHandler(), dataTrigger.get(context, flow));
        else
            data = RuntimeObjectAdapter.wrap(context, flow.getHandler(), flow);

        // We replace 'data' with a map that also contains the 'insertFile'
        // method.
        // This may better be done by adding the method as a shared variable.
        // Note that if we do so we
        // will have to replace "hasAttachment" with another mechanism that will
        // tell us whether there
        // are attachments in the file.
        HashMap data1 = new HashMap();
        data1.putAll((Map) data);
        data1.put("insertFile", new TemplateMethodModelEx() {

            public Object exec(List arguments) throws TemplateModelException
            {
                if (arguments.size() != 1)
                {
                    throw new TemplateModelException("Illegal arguments");
                }
                hasAttachment.setValue(Boolean.TRUE);
                Object arg = arguments.get(0);
                byte[] content = null;
                String filename = null;
                String contentType = null;
                try
                {

                    TemplateHashModel fileStructure = (TemplateHashModel) arg;
                    SimpleObjectWrapper contentWrapper = (SimpleObjectWrapper) fileStructure
                            .get(BuiltinModels.CONTENT_ROLE.toString());
                    content = ((BinaryValue) contentWrapper.getValue())
                            .toByteArray();
                    filename = fileStructure.get(
                            "File_Name").toString();
                }
                catch (ClassCastException e)
                {
                    throw new TemplateModelException("Illegal argument " + arg,
                            e);
                }
                File tempFile = null;
                try
                {
                    tempFile = File.createTempFile("createDocument.", filename);
                    tempFile.deleteOnExit();
                    FileOutputStream in = new FileOutputStream(tempFile);
                    in.write(content);
                    in.close();
                }
                catch (IOException e)
                {
                    throw new DocGenException(
                            "Failed to create temporary file", e);
                }

                return "@file:" + tempFile.getAbsolutePath() + "@";
            }
        });
        data = data1;
        byte[] templateBytes = ((BinaryValue) templateTrigger
                .get(context, flow)).toByteArray();
        ZippedDocumentTemplate template = new ZippedDocumentTemplate(
                new ByteArrayInputStream(templateBytes));
        String macros = null;

        if (macrosTrigger != null)
            macros = (String) macrosTrigger.get(context, flow);
        ByteArrayImageProvider imageProvider = null;
        if (imagesTrigger != null)
        {
            imageProvider = new ByteArrayImageProvider();
            if (imagesTrigger.isRepetitive())
            {
                List images = (List) imagesTrigger.get(context, flow);
                for (Iterator i = images.iterator(); i.hasNext();)
                    addImage(context, imageProvider, i.next());
            }
            else
                addImage(context, imageProvider, imagesTrigger.get(context,
                        flow));

        }
        template.setContentWrapper(new PreProcessor(macros));
        ByteArrayOutputStream documentStream = new ByteArrayOutputStream();
        try
        {
            template.createDocument(data, documentStream, imageProvider);
        }
        catch (Exception e)
        {
            fireError(context, flow, new DocGenException(
                    "Failed to create document from template", e));
            return;
        }
        byte[] documentBytes = documentStream.toByteArray();
        if (Boolean.TRUE == hasAttachment.getValue())
            documentBytes = insertFiles(context, documentBytes);
        setExitValue(documentExit, context, flow,
                new BinaryValue(documentBytes));
    }

    private void addImage(RuntimeContext context,
            ByteArrayImageProvider imageProvider, Object object)
    {
        if (object == null)
            return;
        CompositeValue file = (CompositeValue) context.getSapiManager()
                .fromInstance(imagesTrigger.getChildInstanceHandler(), object);

        BinaryValue content = (BinaryValue) file.get(BuiltinModels.CONTENT_ROLE
                .toString());
        String filename = (String) file.get(BuiltinModels.FILENAME_ROLE
                .toString());
        imageProvider.setImage(filename, content.toByteArray());

    }

    /**
     * Converts a document by inserting attachments using the pattern
     * 
     * @attach: <Path>@
     * @param documentBytes
     * @return
     */
    private byte[] insertFiles(RuntimeContext context, byte[] input)
    {
        File tempFile = null;
        try
        {
            tempFile = File.createTempFile("createDocument.", ".od");
            tempFile.deleteOnExit();
            FileOutputStream in = new FileOutputStream(tempFile);
            in.write(input);
            in.close();
        }
        catch (IOException e)
        {
            throw new DocGenException("Failed to create temporary file", e);
        }

        attachFiles(context, tempFile);
        byte[] output;
        try
        {
            output = FileUtils.readBytes(new FileInputStream(tempFile), true);
        }
        catch (IOException e)
        {
            throw new DocGenException("Failed to read temporary file", e);
        }
        return output;
    }

    public static void attachFiles(RuntimeContext context, File tempFile)
    {
        SocketOpenOfficeConnection connection = null;
        connection = new SocketOpenOfficeConnection();
        try
        {
            connection.connect();
        }
        catch (Exception e)
        {
            throw new EngineException(
                    "Failed to connect to the document conversion server.",
                    "Please make sure OpenOffice is listening on port "
                            + SocketOpenOfficeConnection.DEFAULT_PORT
                            + ".\nError Message=" + e.getMessage(), e);
        }
        try
        {
            XFileIdentifierConverter fileContentProvider = connection
                    .getFileContentProvider();
            String inputUrl = fileContentProvider.getFileURLFromSystemPath("",
                    tempFile.getAbsolutePath());
            XComponentLoader desktop = connection.getDesktop();
            XComponent document = desktop.loadComponentFromURL(inputUrl,
                    "_blank", 0, HIDDEN);
            if (document == null)
            {
                throw new OpenOfficeException("could not load document "
                        + inputUrl);
            }
            try
            {
                XTextDocument textDocument = (XTextDocument) UnoRuntime
                        .queryInterface(XTextDocument.class, document);
                XSearchable searchable = (XSearchable) UnoRuntime
                        .queryInterface(XSearchable.class, textDocument);
                XSearchDescriptor prefixDescriptor = searchable
                        .createSearchDescriptor();
                prefixDescriptor.setSearchString("@file:");
                XSearchDescriptor suffixDescriptor = searchable
                        .createSearchDescriptor();
                suffixDescriptor.setSearchString("@");
                Object next = searchable.findFirst(prefixDescriptor);
                while (next != null)
                {
                    XTextRange prefixRange = (XTextRange) UnoRuntime
                            .queryInterface(XTextRange.class, next);
                    XTextRange suffix = (XTextRange) UnoRuntime.queryInterface(
                            XTextRange.class, searchable.findNext(prefixRange,
                                    suffixDescriptor));
                    if (suffix == null)
                        next = null;
                    else
                    {
                        XParagraphCursor cursor = (XParagraphCursor) UnoRuntime
                                .queryInterface(XParagraphCursor.class,
                                        textDocument.getText()
                                                .createTextCursorByRange(
                                                        prefixRange));
                        XTextCursor contentCursor = textDocument.getText()
                                .createTextCursorByRange(prefixRange.getEnd());
                        contentCursor.gotoRange(suffix.getStart(), true);
                        String filePath = contentCursor.getString();
                        File attachmentFile = new File(filePath);
                        String attachmentURL = fileContentProvider
                                .getFileURLFromSystemPath("", attachmentFile
                                        .getAbsolutePath());
                        // cursor.gotoEndOfParagraph(true);
                        // cursor.gotoStartOfParagraph(true);
                        cursor.gotoRange(suffix.getEnd(), true);
                        cursor.setString("");
                        XDocumentInsertable inserter = (XDocumentInsertable) UnoRuntime
                                .queryInterface(XDocumentInsertable.class,
                                        cursor);
                        inserter.insertDocumentFromURL(attachmentURL,
                                new PropertyValue[0]);
                        next = searchable.findNext(cursor.getEnd(),
                                prefixDescriptor);
                    }
                }

                XStorable storable = (XStorable) UnoRuntime.queryInterface(
                        XStorable.class, textDocument);
                storable.store();

            }
            finally
            {
                document.dispose();
            }
            connection.disconnect();
            connection = null;

        }
        catch (Exception e)
        {
            throw new DocGenException("Failed to convert document", e);
        }
        finally
        {
            if (connection != null)
                connection.disconnect();
        }
    }

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        this.templateTrigger = getRequiredMandatoryTrigger(TEMPLATE_ROLE,
                Boolean.FALSE, BuiltinModels.BINARY_ID);
        this.dataTrigger = getNonRequiredMandatoryTrigger(DATA_ROLE,
                Boolean.FALSE, null);
        this.documentExit = getRequiredExit(DOCUMENT_ROLE, Boolean.FALSE,
                BuiltinModels.BINARY_ID);
        this.macrosTrigger = getNonRequiredTrigger(MACROS, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        this.imagesTrigger = getNonRequiredTrigger(IMAGES_ROLE, null,
                BuiltinModels.FILE_ID);
    }
}
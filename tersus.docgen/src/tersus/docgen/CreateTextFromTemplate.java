/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.docgen;

import java.io.IOException;
import java.io.StringWriter;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * @author Youval Bronicki
 * 
 */
public class CreateTextFromTemplate extends Plugin
{


    static
    {
        FreemarkerSetup.setup();
    }

    private SlotHandler templateTrigger;

    private static final Role TEMPLATE_ROLE = Role.get("<Template>");

    private static final Role OUTPUT_ROLE = Role.get("<Text>");

    private static final Role DATA_ROLE = Role.get("<Data>");

    private SlotHandler outputExit;

    private SlotHandler dataTrigger;


    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext,
     *      tersus.runtime.FlowInstance)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
        Object data;
        if (dataTrigger != null)
            data = RuntimeObjectAdapter.wrap(context, dataTrigger
                    .getChildInstanceHandler(), dataTrigger.get(context, flow));
        else
            data = RuntimeObjectAdapter.wrap(context, flow.getHandler(), flow);

        try
        {
            Template template = (Template) templateTrigger.get(context, flow);

            StringWriter out = new StringWriter();
            template.process(data, out);
            out.flush();
            setExitValue(outputExit, context, flow, out.toString());
        }
        catch (IOException e)
        {
            throw new EngineException("Failed to create string from template", null, e);
        }
        catch (TemplateException e)
        {
            throw new ModelExecutionException("A problem occured when intiatiating template","Error message:"+e.getMessage(),e);
        }  
        
    }

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        this.templateTrigger = getRequiredMandatoryTrigger(TEMPLATE_ROLE,
                Boolean.FALSE, null);
        this.dataTrigger = getNonRequiredMandatoryTrigger(DATA_ROLE,
                Boolean.FALSE, null);
        this.outputExit = getRequiredExit(OUTPUT_ROLE, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        createFunctions();
    }

    private void createFunctions()
    {
    }

}
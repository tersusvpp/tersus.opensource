/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.docgen;

import tersus.runtime.RuntimeContext;
import freemarker.log.Logger;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;

/**
 * @author Youval Bronicki
 *
 */
public class FreemarkerSetup
{
    static boolean alreadySet = false;
    static
    {
        setup();
    }
    /**
     * 
     */
    private static final String FREEMARKER_CONFIGURATION_KEY = "FreemarkerConfiguration";
    synchronized public static void setup()
    {
        if (! alreadySet)
        {
            alreadySet = true;
            try
            {
                Logger.selectLoggerLibrary(Logger.LIBRARY_NONE);
            }
            catch (ClassNotFoundException e1)
            {
                try
                {
                    Logger.selectLoggerLibrary(Logger.LIBRARY_NONE);
                }
                catch (ClassNotFoundException e2)
                {
                }
            }

        }
    }
    public static Configuration getFMCongiguration(final RuntimeContext context)
    {
        
        Configuration cfg = (Configuration)context.getProperty(FREEMARKER_CONFIGURATION_KEY);
        if (cfg == null)
        {
            cfg = new Configuration();
            cfg.setObjectWrapper(new DefaultObjectWrapper());
            context.setProperty(FREEMARKER_CONFIGURATION_KEY, cfg);
        }
        return cfg;
    }
    
}

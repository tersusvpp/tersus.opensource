/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.docgen;

import freemarker.template.TemplateModelException;
import freemarker.template.TemplateScalarModel;

/**
 * @author Youval Bronicki
 *
 */
public class SimpleObjectWrapper implements TemplateScalarModel
{
    private Object value;

    public SimpleObjectWrapper(Object value)
    {
        this.setValue(value);
    }

    /**
     * @param value The value to set.
     */
    public void setValue(Object value)
    {
        this.value = value;
    }

    /**
     * @return Returns the value.
     */
    public Object getValue()
    {
        return value;
    }

    /* (non-Javadoc)
     * @see freemarker.template.TemplateScalarModel#getAsString()
     */
    public String getAsString() throws TemplateModelException
    {
        return String.valueOf(value);
    }
}

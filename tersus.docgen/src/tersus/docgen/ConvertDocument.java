/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.docgen;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

public class ConvertDocument extends ConvertDocumentBase
{
    static final Role OUTPUT_FILE = Role.get("<Output File>");
    static final Role OUTPUT_FILE_EXTENSION = Role.get("<Output File Extension>");

    SlotHandler convertedFileExit, outputFileExtensionTrigger;

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext,
     *      tersus.runtime.FlowInstance)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
        String outputFileExtension = (String)outputFileExtensionTrigger.get(context, flow);
        convert(context, flow, convertedFileExit, outputFileExtension, null);
    }

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        this.outputFileExtensionTrigger = getRequiredMandatoryTrigger(OUTPUT_FILE_EXTENSION, Boolean.FALSE,BuiltinModels.TEXT_ID);
        this.convertedFileExit = getRequiredExit(OUTPUT_FILE, Boolean.FALSE,
                BuiltinModels.FILE_ID);

    }

}

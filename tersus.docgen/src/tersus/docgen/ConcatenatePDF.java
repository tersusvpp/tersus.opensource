/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.docgen;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.model.validation.Problems;
import tersus.runtime.BinaryValue;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * @author Youval Bronicki
 *  
 */
public class ConcatenatePDF extends Plugin
{
    protected SlotHandler inputTriggers[], outputPDFExit;

    protected final static Role OUTPUT_PDF = Role.get("<Output PDF>");

    public void start(RuntimeContext context, FlowInstance flow)
    {
        ArrayList inputs = new ArrayList();
        for (int i = 0; i < inputTriggers.length; i++)
        {
            if (inputTriggers[i].isRepetitive())
            {
                List list = (List) inputTriggers[i].get(context, flow);
                if (list != null)
                    inputs.addAll(list);
            }
            else
            {
                BinaryValue value = (BinaryValue) inputTriggers[i].get(context,
                        flow);
                if (value != null)
                    inputs.add(value);
            }
        }
        if (inputs.size() > 0)
        {
            BinaryValue output;
            if (inputs.size() == 1)
                output = (BinaryValue) inputs.get(0);
            else
                output = concatenatePDFs(inputs);

            setExitValue(outputPDFExit, context, flow, output);
        }
    }

    private BinaryValue concatenatePDFs(ArrayList inputs)
    {
        try
        {
            int nFiles = inputs.size();
            int pageOffset = 0;
            Document document = null;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            PdfCopy writer = null;
            for (int i = 0; i < nFiles; i++)
            {
                // we create a reader for a certain document
                PdfReader reader = new PdfReader(((BinaryValue) inputs.get(i))
                        .toInputStream());
                int nPages = reader.getNumberOfPages();
                pageOffset += nPages;
                if (i == 0)
                {
                    document = new Document(reader.getPageSizeWithRotation(1));
                    writer = new PdfCopy(document, out);
                    document.open();
                }
                PdfImportedPage page;
                for (int j = 0; j < nPages;)
                {
                    ++j;
                    page = writer.getImportedPage(reader, j);
                    writer.addPage(page);
                }
            }
            // step 5: we close the document
            document.close();
            return new BinaryValue(out.toByteArray());
        }
        catch (Exception e)
        {
            throw new ModelExecutionException("Failed to concatenate PDFs", e
                    .getMessage(), e);
        }
    }

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        outputPDFExit = getRequiredExit(OUTPUT_PDF, Boolean.FALSE,
                BuiltinModels.BINARY_ID);
        ArrayList inputTriggerList = new ArrayList();
        for (int i = 0; i < triggers.length; i++)
        {
            if (triggers[i].getChildModelId().equals(BuiltinModels.BINARY_ID))
                inputTriggerList.add(triggers[i]);
            else if (!triggers[i].isEmpty())
                notifyInvalidModel(triggers[i].getRole(),
                        Problems.INVALID_TRIGGER,
                        "Only empty and binary triggers are allowed");
        }

        Comparator c = new Comparator() {

            public int compare(Object o1, Object o2)
            {
                SlotHandler trigger1 = (SlotHandler) o1;
                SlotHandler trigger2 = (SlotHandler) o2;
                return (trigger1.getRole().toString().compareTo(trigger2
                        .getRole().toString()));
            }
        };
        Collections.sort(inputTriggerList, c);
        inputTriggers = (SlotHandler[]) inputTriggerList
                .toArray(new SlotHandler[inputTriggerList.size()]);
    }

}
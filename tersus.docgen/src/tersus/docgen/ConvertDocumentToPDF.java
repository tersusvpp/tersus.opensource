/************************************************************************************************
 * Copyright (c) 2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.docgen;


import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * @author Youval Bronicki
 *  
 */
public class ConvertDocumentToPDF extends ConvertDocumentBase
{

    static final Role PDF_EXIT_ROLE = Role.get("<PDF File>");

    SlotHandler pdfExit;

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext,
     *      tersus.runtime.FlowInstance)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
        convert(context, flow, pdfExit, ".pdf", "application/pdf");
    }

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        this.pdfExit = getRequiredExit(PDF_EXIT_ROLE, Boolean.FALSE,
                BuiltinModels.FILE_ID);

    }
    
    
}
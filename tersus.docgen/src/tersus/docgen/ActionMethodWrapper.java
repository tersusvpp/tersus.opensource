/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.docgen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import tersus.model.ModelException;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Number;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.SubFlowHandler;

import freemarker.template.TemplateBooleanModel;
import freemarker.template.TemplateDateModel;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateNumberModel;
import freemarker.template.TemplateScalarModel;

public class ActionMethodWrapper implements TemplateMethodModelEx
{

    private RuntimeContext context;
    private SubFlowHandler handler;
    private FlowInstance parent;
    private FlowHandler flowHandler;

    public ActionMethodWrapper(RuntimeContext context, SubFlowHandler handler, FlowInstance parent)
    {
        this.context = context;
        this.handler = handler;
        this.flowHandler = (FlowHandler)handler.getChildInstanceHandler();
        this.parent = parent;
    }

    public Object exec(List arguments) throws TemplateModelException
    {
        if (this.parent == null)
            throw new ModelExecutionException("Attempt to execute function "+handler.getRole() + " but parent instance is null");
       List sortedTriggers = new ArrayList();
        FlowInstance instance = (FlowInstance)flowHandler.newInstance(context);
        instance.setParent(parent);
        final SlotHandler[] triggers = flowHandler.getTriggers();
        for (int i=0;i<triggers.length;i++)
        {
            sortedTriggers.add(triggers[i]);
        }
        Collections.sort(sortedTriggers, new Comparator(){

            public int compare(Object o1, Object o2)
            {
                return ((SlotHandler)o1).getRole().toString().compareTo(((SlotHandler)o2).getRole().toString());
            }});
        int triggerNumber=0;
        for (Iterator params = arguments.iterator(); params.hasNext();)
        {
            TemplateModel  p = (TemplateModel)params.next();
            Object value = null;
            if (p instanceof TemplateNumberModel)
                value = new Number(((TemplateNumberModel)p).getAsNumber().doubleValue());
            else if (p instanceof TemplateScalarModel)
                value = ((TemplateScalarModel)p).getAsString();
            else if (p instanceof TemplateDateModel)
                value = ((TemplateDateModel)p).getAsDate();
            else if (p instanceof TemplateBooleanModel)
                value = Boolean.valueOf(((TemplateBooleanModel)p).getAsBoolean());
            if (value != null)
            {
                SlotHandler trigger = (SlotHandler)sortedTriggers.get(triggerNumber);
                trigger.set(context, instance, value);
            }
            
            
            ++triggerNumber;
        }
        handler.invoke(context, instance);
        if (flowHandler.getExits().length != 1)
        {
            throw new ModelException("Incompatible model function in \"Create text from template\" - expected a single exit but found "+flowHandler.getExits().length);
        }
        SlotHandler exit = flowHandler.getExits()[0];
        Object output = exit.get(context, instance);
        return RuntimeObjectAdapter.wrap(context, exit.getChildInstanceHandler(), output);
    }

}

/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.docgen;

import tersus.model.ModelId;
import tersus.model.Role;

/**
 * @author Liat Shiff
 */
public class DocumentAttributesStructure
{
	public static final ModelId DOCUMENT_ATTRIBUTES_ID = new ModelId("Common/Data Structures/Document/Document Attributes");
	
	public static final Role WIDTH = Role.get("Width");
    public static final Role HEIGHT = Role.get("Height");
}

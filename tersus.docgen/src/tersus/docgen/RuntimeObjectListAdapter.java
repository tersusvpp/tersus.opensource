/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.docgen;

import java.util.AbstractList;
import java.util.Collections;
import java.util.List;

import tersus.runtime.InstanceHandler;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.RuntimeContext;

/**
 * @author Youval Bronicki
 *
 */
public class RuntimeObjectListAdapter extends AbstractList
{
    private RuntimeContext context;
    private InstanceHandler handler;
    private List instanceList;

    public RuntimeObjectListAdapter(RuntimeContext context,
            InstanceHandler handler, List instanceList)
    {
        this.context = context;
        this.handler = handler;
        this.instanceList = instanceList;
        if (this.instanceList == null)
            this.instanceList = Collections.EMPTY_LIST;
    }
    /* (non-Javadoc)
     * @see java.util.List#get(int)
     */
    public Object get(int index)
    {
        Object _value = instanceList.get(index);
        if ( handler instanceof LeafDataHandler)
            return _value;
        else
            return RuntimeObjectAdapter.wrap(context, handler, _value);
    }

    /* (non-Javadoc)
     * @see java.util.Collection#size()
     */
    public int size()
    {
        return instanceList.size();
    }

}

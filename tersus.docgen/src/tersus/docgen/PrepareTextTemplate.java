/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.docgen;

import java.io.StringReader;

import freemarker.template.Template;
import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

public class PrepareTextTemplate extends Plugin
{
    private static final Role TEXT = Role.get("<Text>");
    private static final Role TEMPLATE = Role.get("<Template>");
    private SlotHandler textTrigger;
    private SlotHandler templateExit;
    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        textTrigger = getRequiredMandatoryTrigger(TEXT,Boolean.FALSE, BuiltinModels.TEXT_ID);
        templateExit = getRequiredExit(TEMPLATE, null, null);
    }
    public void start(RuntimeContext context, FlowInstance flow)
    {
        String text = (String)textTrigger.get(context, flow);
        try
        {
            Template template = new Template("Template", new StringReader(text),FreemarkerSetup.getFMCongiguration(context));
            setExitValue(templateExit, context, flow, template);
        }
        catch (Exception e)
        {
            throw new ModelExecutionException("Failed to create template",e.getMessage(), e);
        }
        
    }
    
    
}

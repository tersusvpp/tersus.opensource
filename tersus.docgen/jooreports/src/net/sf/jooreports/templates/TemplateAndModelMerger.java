//
// JOOReports - The Open Source Java/OpenOffice Report Engine
// Copyright (C) 2004-2006 - Mirko Nasato <mirko@artofsolving.com>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// http://www.gnu.org/copyleft/lesser.html
//
package net.sf.jooreports.templates;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Map;

import net.sf.jooreports.templates.AbstractDocumentTemplate.OdtEntryHandler;
import net.sf.jooreports.templates.images.ImageProvider;

import org.apache.commons.io.IOUtils;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * Merges a pre-processed template with the data model to
 * produce the output document.
 */
class TemplateAndModelMerger implements OdtEntryHandler {

	private static final String UTF_8 = "UTF-8";
	private static final String PICTURES_PATH = "Pictures/";
	private static final Configuration FREEMARKER_CONFIGURATION = new Configuration();

	private String[] xmlEntries;
	private final Map/*<String,String>*/ images;
	private final ImageProvider imageProvider;

	public TemplateAndModelMerger(String[] xmlEntries, Map images, ImageProvider imageProvider) {
		this.xmlEntries = xmlEntries;
		this.images = images;
		this.imageProvider = imageProvider;
	}

	public void handleEntry(String entryName, InputStream input, Object model, OutputStream output) throws IOException, DocumentTemplateException {
        if (Arrays.binarySearch(xmlEntries, entryName) >= 0) {
        	Template template = new Template(entryName, new InputStreamReader(input, UTF_8), FREEMARKER_CONFIGURATION);
        	try {
        		template.process(model, new OutputStreamWriter(output, UTF_8));
			} catch (TemplateException templateException) {
				throw new DocumentTemplateException(templateException);
			}
        } else if (isImageToReplace(entryName)) {
        	replaceImage(entryName, output);
        } else {
        	IOUtils.copy(input, output);
        }
	}

	private boolean isImageToReplace(String entryName) {
        if (!entryName.startsWith(PICTURES_PATH)) {
            return false;
        }
        return imageProvider != null
        	&& imageProvider.contains(getImageName(entryName));
	}

    private void replaceImage(String entryName, OutputStream output) throws IOException {
        InputStream input = imageProvider.getInputStream(getImageName(entryName));
        IOUtils.copy(input, output);
    }

	private String getImageName(String entryName) {
		String imageName = (String) images.get(entryName);
		return imageName;
	}
}

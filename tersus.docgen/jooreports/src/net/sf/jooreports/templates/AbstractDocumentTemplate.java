//
// JOOReports - The Open Source Java/OpenOffice Report Engine
// Copyright (C) 2004-2006 - Mirko Nasato <mirko@artofsolving.com>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// http://www.gnu.org/copyleft/lesser.html
//
package net.sf.jooreports.templates;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import net.sf.jooreports.templates.images.ImageProvider;
import net.sf.jooreports.templates.odt.OdtEntry;
import net.sf.jooreports.templates.odt.OdtReader;
import net.sf.jooreports.templates.odt.ZippedOdtReader;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;

public abstract class AbstractDocumentTemplate implements DocumentTemplate {

	public static interface OdtEntryHandler {

		public void handleEntry(String entryName, InputStream input, Object model, OutputStream output) throws IOException, DocumentTemplateException;

	}

	private static final ContentWrapper DEFAULT_CONTENT_WRAPPER = new ContentWrapper() {

		public String wrapContent(String content) {
			return "[#ftl]\n"
				+ "[#escape any as any?xml?replace(\"\\n\",\"<text:line-break />\")]\n"
				+ content
				+ "[/#escape]";
		}

	};

	private String[] xmlEntries = new String[] {
		"content.xml",
		"styles.xml"
	};

	private ContentWrapper contentWrapper = DEFAULT_CONTENT_WRAPPER;

	/**
	 * Cached pre-processed template in zip format.
	 * 
	 * TODO: this is always in-memory; create a pluggable template cache instead
	 */
	private byte[] preProcessedTemplate;
	
	/**
	 * Map image href to image name
	 */
	private Map/*<String,String>*/ images;

	public void setXmlEntries(String[] xmlEntries) {
		this.xmlEntries = xmlEntries;
		Arrays.sort(this.xmlEntries);
	}

	public void setContentWrapper(ContentWrapper contentWrapper) {
		this.contentWrapper = contentWrapper;
	}

    /**
     * Must be overriden by subclasses to provide a suitable {@link OdtReader}.
     * 
     * @return
     */
    protected abstract OdtReader getOdtReader();

    public void createDocument(Object model, OutputStream output) throws IOException, DocumentTemplateException {
    	createDocument(model, output, null);
    }

    public void createDocument(Object model, OutputStream output, ImageProvider imageProvider) throws IOException, DocumentTemplateException {
    	if (preProcessedTemplate == null) {
    		preProcess();
    	}
        ZippedOdtReader odtReader = new ZippedOdtReader(new ByteArrayInputStream(preProcessedTemplate));
		processEntries(odtReader, model, output, new TemplateAndModelMerger(xmlEntries, images, imageProvider));
    }

    private void preProcess() throws IOException, DocumentTemplateException {
    	ByteArrayOutputStream output = new ByteArrayOutputStream();
    	TemplatePreProcessor templatePreProcessor = new TemplatePreProcessor(xmlEntries, contentWrapper);
    	processEntries(getOdtReader(), null, output, templatePreProcessor);
    	preProcessedTemplate = output.toByteArray();
    	images = templatePreProcessor.getImages();
    }

    private void processEntries(OdtReader odtReader, Object model, OutputStream output, OdtEntryHandler entryHandler) throws IOException, DocumentTemplateException {
        ZipOutputStream zipOutput = null;
        try {
            zipOutput = new ZipOutputStream(output);
            while (true) {
            	OdtEntry entry = odtReader.getNextEntry();
                if (entry == null) break;
                String entryName = entry.getName();
                zipOutput.putNextEntry(new ZipEntry(entryName));
                entryHandler.handleEntry(entryName, entry.getInputStream(), model, zipOutput);
                zipOutput.closeEntry();
                odtReader.closeEntry();
            }
        } finally {
        	IOUtils.closeQuietly(zipOutput);
            if (odtReader != null) {
            	try { 
            		odtReader.close();
            	} catch (IOException ioException) {
            		// ignored
            	}
            }
        }
    }
}

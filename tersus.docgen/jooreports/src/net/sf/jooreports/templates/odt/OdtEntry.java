//
// JOOReports - The Open Source Java/OpenOffice Report Engine
// Copyright (C) 2004-2006 - Mirko Nasato <mirko@artofsolving.com>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// http://www.gnu.org/copyleft/lesser.html
//
package net.sf.jooreports.templates.odt;

import java.io.InputStream;

/**
 * Represents an entry in a ODT archive.
 */
public class OdtEntry {

	private String name;
	private InputStream inputStream;
	
	public OdtEntry(String name, InputStream input) {
		this.name = name;
		this.inputStream = input;
	}
	
	public String getName() {
		return name;
	}
	
	public InputStream getInputStream() {
		return inputStream;
	}

}

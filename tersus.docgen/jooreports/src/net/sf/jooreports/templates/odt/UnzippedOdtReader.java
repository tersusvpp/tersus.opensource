//
// JOOReports - The Open Source Java/OpenOffice Report Engine
// Copyright (C) 2004-2006 - Mirko Nasato <mirko@artofsolving.com>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// http://www.gnu.org/copyleft/lesser.html
//
package net.sf.jooreports.templates.odt;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * {@link OdtReader} for ODT archives that have been unzipped into
 * a specified directory.
 */
public class UnzippedOdtReader implements OdtReader {

	private Iterator entryIterator;
	private InputStream entryInputStream;

	public UnzippedOdtReader(File odtDirectory) {
        if (!odtDirectory.isDirectory()) {
            throw new IllegalArgumentException("not a directory: "+ odtDirectory);
        }
        Map entries = listEntries(null, odtDirectory, new HashMap());
        entryIterator = entries.entrySet().iterator();
	}

	private Map/*<String,File>*/ listEntries(String parentPath, File directory, Map entries) {
        File[] files = directory.listFiles();
        for (int i = 0; i < files.length; i++) {
            File file = files[i];
            String path = (parentPath == null) ? file.getName() : parentPath + "/" + file.getName();            
            if (file.isDirectory()) {
                listEntries(path, file, entries);
            } else {
            	entries.put(path, file);
            }
        }
        return entries;
	}

	public OdtEntry getNextEntry() throws IOException {
		if (entryIterator.hasNext()) {
			Map.Entry mapEntry = (Map.Entry) entryIterator.next();
			String name = (String) mapEntry.getKey();
			File file = (File) mapEntry.getValue();
			entryInputStream = new FileInputStream(file);
			return new OdtEntry(name, entryInputStream);
		} else {
			return null;
		}
	}

	public void closeEntry() throws IOException {
		entryInputStream.close();
	}

	public void close() throws IOException {
		// nothing to do
	}
}

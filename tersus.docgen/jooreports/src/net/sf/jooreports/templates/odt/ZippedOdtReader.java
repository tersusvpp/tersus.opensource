//
// JOOReports - The Open Source Java/OpenOffice Report Engine
// Copyright (C) 2004-2006 - Mirko Nasato <mirko@artofsolving.com>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// http://www.gnu.org/copyleft/lesser.html
//
package net.sf.jooreports.templates.odt;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * {@link OdtReader} for ODT archives in ZIP format (the default).
 */
public class ZippedOdtReader implements OdtReader {

    /**
     * Prevents third-party classes from closing the input stream when we
     * are still processing it.
     */
    private static class WrappedZipInputStream extends ZipInputStream {

        public WrappedZipInputStream(InputStream input) {
            super(input);
        }

        public void close() {
            // not unless you say please
        }

        public void pleaseClose() throws IOException {
            super.close();
        }
    }

    private WrappedZipInputStream zipInputStream;
    
    public ZippedOdtReader(InputStream inputStream) {
		zipInputStream = new WrappedZipInputStream(inputStream);
	}

	public OdtEntry getNextEntry() throws IOException {
		ZipEntry zipEntry = zipInputStream.getNextEntry();
		if (zipEntry != null) {
			return new OdtEntry(zipEntry.getName(), zipInputStream);
		} else {
			return null;
		}
	}

	public void closeEntry() throws IOException {
		zipInputStream.closeEntry();
	}

	public void close() throws IOException {
		zipInputStream.pleaseClose();
	}
}

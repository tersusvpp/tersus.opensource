//
// JOOReports - The Open Source Java/OpenOffice Report Engine
// Copyright (C) 2004-2006 - Mirko Nasato <mirko@artofsolving.com>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// http://www.gnu.org/copyleft/lesser.html
//
package net.sf.jooreports.templates;

import java.io.File;

import net.sf.jooreports.templates.odt.OdtReader;
import net.sf.jooreports.templates.odt.UnzippedOdtReader;

/**
 * Same as {@link ZippedDocumentTemplate} but in unzipped form.
 */
public class UnzippedDocumentTemplate extends AbstractDocumentTemplate {
    private File templateDirectory;

    public UnzippedDocumentTemplate(File templateDirectory) {
        if (!templateDirectory.isDirectory()) {
            throw new IllegalArgumentException("not a directory: "+ templateDirectory);
        }        
        this.templateDirectory = templateDirectory;
    }

    protected OdtReader getOdtReader() {
    	return new UnzippedOdtReader(templateDirectory);
    }
}

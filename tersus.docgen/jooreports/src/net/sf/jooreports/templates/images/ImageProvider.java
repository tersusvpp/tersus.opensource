//
// JOOReports - The Open Source Java/OpenOffice Report Engine
// Copyright (C) 2004-2006 - Mirko Nasato <mirko@artofsolving.com>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// http://www.gnu.org/copyleft/lesser.html
//
package net.sf.jooreports.templates.images;

import java.io.InputStream;

/**
 * Interface for classes providing images to be inserted into templates.
 * <p>
 * In all methods the <i>imageName</i> parameter is the string specified
 * when composing the template in Writer as the <i>Name</i> field in the
 * <i>Picture... / Option</i> dialog. 
 */
public interface ImageProvider {

	/**
	 * Should the image with this name be replaced in the template?
	 * 
	 * @param imageName
	 * @return
	 */
	public boolean contains(String imageName);

	/**
	 * Returns the image data as an {@link InputStream}
	 * 
	 * @param imageName
	 * @return
	 */
	public InputStream getInputStream(String imageName);

}

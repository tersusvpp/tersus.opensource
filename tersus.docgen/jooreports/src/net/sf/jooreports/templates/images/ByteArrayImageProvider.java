//
// JOOReports - The Open Source Java/OpenOffice Report Engine
// Copyright (C) 2004-2006 - Mirko Nasato <mirko@artofsolving.com>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// http://www.gnu.org/copyleft/lesser.html
//
package net.sf.jooreports.templates.images;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link ImageProvider} implementation that keeps all
 * images in memory as byte arrays.
 */
public class ByteArrayImageProvider implements ImageProvider {

	private Map/*<String,byte[]>*/ images = new HashMap();

	public void setImage(String name, byte[] data) {
		images.put(name, data);
	}

	public boolean contains(String imageName) {
		return images.containsKey(imageName);
	}

	public InputStream getInputStream(String imageName) {
		byte[] imageData = (byte[]) images.get(imageName);
		return new ByteArrayInputStream(imageData);
	}
}

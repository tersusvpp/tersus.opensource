//
// JOOReports - The Open Source Java/OpenOffice Report Engine
// Copyright (C) 2004-2006 - Mirko Nasato <mirko@artofsolving.com>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// http://www.gnu.org/copyleft/lesser.html
//
package net.sf.jooreports.templates.xmlfilters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import net.sf.jooreports.templates.DocumentTemplateException;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Nodes;
import nu.xom.ParentNode;
import nu.xom.Text;

/**
 * OpenDocument XML entry filter that replaces <tt>script</tt> elements with FreeMarker directives.
 * <p>
 * Scripts can contain FreeMarker directives to be placed at the location of the script itself, or
 * at some enclosing tag. For example a script inside a table cell can contain a
 * <tt>[#list items as item]</tt> directive to be inserted at the enclosing table row so that the
 * entire row will be repeated for each item in the list.
 */
public class ScriptTagFilter extends XmlEntryFilter {

	private static class ScriptPart {

		private StringBuffer text = new StringBuffer();
		private String location;

		public ScriptPart() {
			// no location
		}

		public ScriptPart(String location) {
			this.location = location;
		}

		public void appendText(String line) {
			text.append(line + "\n");
		}

		public String getText() {
			return text.toString().trim();
		}

		public String getLocation() {
			return location;
		}
	}

	public void doFilter(Document document) throws DocumentTemplateException {
		// bloody xpath... no easier way to do a case-insentive match than translate()
		Nodes scriptNodes = document.query("//text:script[translate(@script:language, 'CIJOPRST', 'cijoprst')='jooscript']", XPATH_CONTEXT);
		for (int nodeIndex = 0; nodeIndex < scriptNodes.size(); nodeIndex++) {
			Element scriptElement = (Element) scriptNodes.get(nodeIndex);
			try {
				String replacementText = addScriptDirectives(scriptElement);
				if (replacementText.length() > 0) {
					scriptElement.getParent().replaceChild(scriptElement, new Text(replacementText));
				} else {
					scriptElement.detach();
				}
			} catch (IOException ioException) {
				ioException.printStackTrace(System.err);
			}
		}
	}

	private static List/*<ScriptPart>*/ parseScriptParts(String scriptText) throws IOException {
		List scriptParts = new ArrayList();
		BufferedReader stringReader = new BufferedReader(new StringReader(scriptText));
		ScriptPart scriptPart = new ScriptPart();
		scriptParts.add(scriptPart);
		for (String line; (line = stringReader.readLine()) != null;) {
			if (line.startsWith("@")) {
				String location = line.trim().substring(1);
				scriptPart = new ScriptPart(location);
				scriptParts.add(scriptPart);
			} else {
				scriptPart.appendText(line);
			}
		}
		return scriptParts;
	}

	/**
	 * @param scriptElement
	 * @return the text that should replace the input field
	 * @throws IOException
	 * @throws DocumentTemplateException 
	 */
	private static String addScriptDirectives(Element scriptElement) throws IOException, DocumentTemplateException {
		String scriptReplacement = "";
		
		List scriptParts = parseScriptParts(scriptElement.getValue());
		for (int index = 0; index < scriptParts.size(); index++) {
			ScriptPart scriptPart = (ScriptPart) scriptParts.get(index);
			if (scriptPart.getLocation() == null) {
				scriptReplacement = scriptPart.getText();
			} else {
				boolean afterEndTag = false; 
				String enclosingTagName = scriptPart.getLocation();
				if (enclosingTagName.startsWith("/")) {
					afterEndTag = true;
					enclosingTagName = enclosingTagName.substring(1);
				}
				
				Element enclosingElement = findEnclosingElement(scriptElement, enclosingTagName);
				ParentNode parent = enclosingElement.getParent();
				int parentIndex = parent.indexOf(enclosingElement);
				if (afterEndTag) {
					parent.insertChild(new Text(scriptPart.getText()), parentIndex + 1);
				} else {
					// before start tag
					parent.insertChild(new Text(scriptPart.getText()), parentIndex);
				}
			}
		}
		
		return scriptReplacement;		
	}

	private static Element findEnclosingElement(Element element, String enclosingTagName) throws DocumentTemplateException {
		Nodes ancestors = element.query("ancestor::" + enclosingTagName, XPATH_CONTEXT);
		if (ancestors.size() == 0) {
			throw new DocumentTemplateException("script error: no such enclosing tag named '" + enclosingTagName +"'");
		}
		return (Element) ancestors.get(ancestors.size() - 1);
	}

}

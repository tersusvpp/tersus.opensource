//
// JOOReports - The Open Source Java/OpenOffice Report Engine
// Copyright (C) 2004-2006 - Mirko Nasato <mirko@artofsolving.com>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// http://www.gnu.org/copyleft/lesser.html
//
package net.sf.jooreports.templates.xmlfilters;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import net.sf.jooreports.templates.AbstractDocumentTemplate;
import net.sf.jooreports.templates.DocumentTemplateException;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Nodes;

/**
 * Filter that inspects an OpenDocument XML file and builds a map
 * from image names to internal filenames.
 * <p>
 * Required by {@link AbstractDocumentTemplate} for replacing images. 
 */
public class ImageIndexerFilter extends XmlEntryFilter {

	private static final String PICTURES_PATH = "Pictures/";
	private static final Log log = LogFactory.getLog(ImageIndexerFilter.class);
	
	private Map/*<String,String>*/ images = new HashMap();
	
	public void doFilter(Document document) throws DocumentTemplateException {
		Nodes textInputNodes = document.query("//draw:image", XPATH_CONTEXT);
		for (int nodeIndex = 0; nodeIndex < textInputNodes.size(); nodeIndex++) {
			Element imageElement = (Element) textInputNodes.get(nodeIndex);
			Element frameElement = (Element) imageElement.getParent();
			if (!"frame".equals(frameElement.getLocalName())) {
				throw new DocumentTemplateException("template contains a draw:image that is not inside a draw:frame");
			}
			String name = frameElement.getAttributeValue("name", DRAW_NAMESPACE);
			if (name == null) {
				throw new DocumentTemplateException("template contains an image with no draw:name");
			}
			String href = imageElement.getAttributeValue("href", XLINK_NAMESPACE);
			if (!href.startsWith(PICTURES_PATH)) {
				log.debug("skipping non-embedded image: href='" + href +"'");
				return;
			}
			images.put(href, name);
		}
	}

	public Map getImages() {
		return images;
	}
}

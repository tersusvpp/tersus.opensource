//
// JOOReports - The Open Source Java/OpenOffice Report Engine
// Copyright (C) 2004-2006 - Mirko Nasato <mirko@artofsolving.com>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// http://www.gnu.org/copyleft/lesser.html
//
// -----------------------------------------------------------------
//
// Modified by David Davidson <David.Davidson@tersus.com>
// 18/2/2013 - Support JODConverter 2.2.2
//
package net.sf.jooreports.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.ConnectException;
import java.util.Properties;

import org.apache.commons.io.FilenameUtils;
// Modified by Tersus - Start
// JODConverter 2.1rc2 support
// import net.sf.jooreports.converter.DocumentConverter;
// import net.sf.jooreports.openoffice.connection.OpenOfficeConnection;
// import net.sf.jooreports.openoffice.connection.SocketOpenOfficeConnection;
// import net.sf.jooreports.openoffice.converter.OpenOfficeDocumentConverter;
// JODConverter 2.2.2 support
import com.artofsolving.jodconverter.DocumentConverter;
import com.artofsolving.jodconverter.openoffice.converter.OpenOfficeDocumentConverter;
import com.artofsolving.jodconverter.openoffice.connection.OpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.connection.SocketOpenOfficeConnection;
//Modified by Tersus - End
import net.sf.jooreports.templates.DocumentTemplate;
import net.sf.jooreports.templates.UnzippedDocumentTemplate;
import net.sf.jooreports.templates.ZippedDocumentTemplate;
import freemarker.ext.dom.NodeModel;

/**
 * Command line tool to create a document from a template and a data file
 * and convert it to the specified format.
 * <p>
 * The data file can be in XML format or a simple <i>.properties</i> file.
 * <p>
 * Requires an OpenOffice.org service to be running on <i>localhost:8100</i>
 * (if the output format is other than ODT).
 */
public class CreateAndConvertDocument {

    public static void main(String[] args) throws Exception {
        if (args.length < 3) {
            System.err.println("USAGE: "+ CreateAndConvertDocument.class.getName() +" <template-document> <data-file> <output-document>");
            System.exit(0);
        }        
        File templateFile = new File(args[0]);
        File dataFile = new File(args[1]);
        File outputFile = new File(args[2]);

        DocumentTemplate template = null;
        if (templateFile.isDirectory()) {
            template = new UnzippedDocumentTemplate(templateFile);
        } else {
            template = new ZippedDocumentTemplate(templateFile);
        }
        
        Object model = null;
        String dataFileExtension = FilenameUtils.getExtension(dataFile.getName());
		if (dataFileExtension.equals("xml")) {
			model = NodeModel.parse(dataFile);
        } else if (dataFileExtension.equals("properties")) {
        	Properties properties = new Properties();
        	properties.load(new FileInputStream(dataFile));
        	model = properties;
        } else {
        	throw new IllegalArgumentException("data file must be 'xml' or 'properties'; unsupported type: " + dataFileExtension);
        }
		
		if ("odt".equals(FilenameUtils.getExtension(outputFile.getName()))) {
			template.createDocument(model, new FileOutputStream(outputFile));
		} else {
	        OpenOfficeConnection connection = new SocketOpenOfficeConnection();
	        try {
	            connection.connect();
	        } catch (ConnectException connectException) {
	            System.err.println("ERROR: connection failed. Please make sure OpenOffice.org is running and listening on port "+ SocketOpenOfficeConnection.DEFAULT_PORT +".");
	            System.exit(1);
	        }
			
			File temporaryFile = File.createTempFile("document", ".odt");
			temporaryFile.deleteOnExit();
	        template.createDocument(model, new FileOutputStream(temporaryFile));
	
	        try {
	            DocumentConverter converter = new OpenOfficeDocumentConverter(connection);
	            converter.convert(temporaryFile, outputFile);
	        } finally {
	            connection.disconnect();
	        }
		}
    }
}

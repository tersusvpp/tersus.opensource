/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.math;

import java.math.BigDecimal;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that rounds a number
 * 
 * @author Ofer Brandes
 * @deprecated obsolete due to bad naming.  Need to remove after warning users.
 */
public class Rounding extends Plugin
{
	private static final Role NUMBER_ROLE = Role.get("<Number>");
	private static final Role PRECISION_ROLE = Role.get("<Precision>");
	private static final Role ROUNDED_ROLE = Role.get("<Rounding>");

	SlotHandler triggerNumber, triggerPrecision;
	SlotHandler exit;
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Number number = (Number) triggerNumber.get(context, flow);
		Number precision = (Number) triggerPrecision.get(context, flow);

		BigDecimal value = new BigDecimal(String.valueOf(number.value));
		int decimalPlaces = (precision == null ? 0 : (int) precision.value);

		BigDecimal roundedValue = value.setScale(decimalPlaces,BigDecimal.ROUND_HALF_UP);
		double rounded = roundedValue.doubleValue();
		setExitValue(exit, context, flow, new Number(rounded));
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		triggerNumber = getRequiredMandatoryTrigger(NUMBER_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		triggerPrecision = getNonRequiredTrigger(PRECISION_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);

		exit = getRequiredExit(ROUNDED_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
	}
}

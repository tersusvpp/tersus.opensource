/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/package tersus.plugins.database;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.tomcat.dbcp.dbcp.BasicDataSource;

import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SQLUtils;
import tersus.webapp.Engine;

public class ClosePooledConnections extends Plugin {

	public void start(RuntimeContext context, FlowInstance flow) {
		context.getContextPool().disconnectAll();
		DataSource ds = SQLUtils.lookupDataSource(Engine.MAIN_DATA_SOURCE);
		if (ds instanceof BasicDataSource) {
			try {
				((BasicDataSource) ds).close();
			} catch (SQLException e) {
				throw new ModelExecutionException(
						"Failed to closed pooled connections", "", e);
			}
		} else
			throw new ModelExecutionException("Can't close pooled connections",
					"Unknow data source " + ds.getClass().getName());
	}

}

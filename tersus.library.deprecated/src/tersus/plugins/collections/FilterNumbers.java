/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.collections;

import java.util.List;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that filters numbers (intially we only look at the first characters of
 * <Filter> and expect only a single filtering number; in the future a general expression handling
 * will be implemented)
 * 
 * @author Ofer Brandes
 * 
 */

public class FilterNumbers extends Plugin
{
	private static final Role INPUTS_ROLE = Role.get("<Numbers>");
	private static final Role FILTER_ROLE = Role.get("<Filter>");
	private static final Role OUTPUT_ROLE = Role.get("<Matching>");

	SlotHandler inputsTrigger, filterTrigger;
	SlotHandler exit;

	public void start(RuntimeContext context, FlowInstance flow)
	{
		// Get filtering expression (currently looking only if starts with '<', '<=', '=', '>=' or
		// '>')
		String filter = (String) filterTrigger.get(context, flow);

		// Get all filtering numbers (currently expecting exactly 1)
		Number filteringNumber = null;
		for (int i = 0; i < triggers.length; i++)
		{
			SlotHandler trigger = triggers[i];
			Role role = trigger.getRole();
			if (!(INPUTS_ROLE.equals(role)) && !(FILTER_ROLE.equals(role)))
			{
				filteringNumber = (Number) trigger.get(context, flow);
				break;
			}
		}

		if (filteringNumber == null)
			filteringNumber = new Number(0);

		// Checking the inputs one by one
		if (inputsTrigger.isRepetitive())
		{
			List values = (List) inputsTrigger.get(context, flow);
			for (int j = 0; j < values.size(); j++)
			{
				Number input = (Number) values.get(j);
				if (fulfillsCondition(filter, filteringNumber.value, input.value))
				{
					accumulateExitValue(exit, context, flow, input);
				}
			}
		}
		else
		{
			Number input = (Number) inputsTrigger.get(context, flow);
			if (fulfillsCondition(filter, filteringNumber.value, input.value))
			{
				accumulateExitValue(exit, context, flow, input);
			}
		}
	}

	private boolean fulfillsCondition(String filter, double filteringNumber, double input)
	{

		boolean ok = false;

		if (filter.startsWith("<="))
		{
			ok = (input <= filteringNumber);
		}
		else if (filter.startsWith("<"))
		{
			ok = (input < filteringNumber);
		}
		else if (filter.startsWith("="))
		{
			ok = (input == filteringNumber);
		}
		else if (filter.startsWith(">="))
		{
			ok = (input >= filteringNumber);
		}
		else if (filter.startsWith(">"))
		{
			ok = (input > filteringNumber);
		}

		return (ok);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		inputsTrigger = getRequiredMandatoryTrigger(INPUTS_ROLE, null, BuiltinModels.NUMBER_ID);
		filterTrigger = getRequiredMandatoryTrigger(FILTER_ROLE, Boolean.FALSE,
				BuiltinModels.TEXT_ID);

		exit = getRequiredExit(OUTPUT_ROLE, Boolean.TRUE, BuiltinModels.NUMBER_ID);
	}
}

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
  <title>Authorization Module - A User/Role/Permission-based Authorization System Module</title></head>

<body>

<b>Authorization module -&nbsp;A Role-Based User
Authorization module</b>


<p>This module provides a working implementation of a role-based
user permission system. <br>

This module can be added to any Tersus application to control the
access and usage of specific parts of the application by users in any
desired granularity.<br><br><br><strong><u>Introduction</u></strong></p>

<p>Basically, a user may use a restricted part of the application
if he/she is assigned a role that is associated with an appropriate
permission. <br>

More formally, a user is eligible to access specific application
functionality (e.g. seeing a view or&nbsp;pressing a button), when
the following conditions are met: </p>

<ol>

  <li>The model of said functionality has been
assigned&nbsp;a permission designator <strong>P</strong>
by setting its&nbsp;<strong>requiredPermission</strong>
property to <strong>P</strong> (during modeling). </li>

  <li>A&nbsp;role <strong>R</strong> has been
assigned the permission <strong>P</strong> (using the <em><strong>Roles</strong>
    </em>view provided by this module). </li>

  <li>The user <strong>U</strong> has been assigned
the role <strong>R</strong> (using the <em><strong>Users</strong>
    </em>view provided by this module). </li>

  <li>The user <strong>U</strong> has provided
the&nbsp;required credentials (a password) when prompted to do so. </li>

</ol>

<p>For example, the module itself employs this mechanism to limit
access to the <strong>Users</strong> and <strong>Roles</strong>
views to users which have been granted the <strong>View Users</strong>
and <strong>View Roles</strong>
permissions resepctively. (It also creates a default user with said
permissions, so that the views will be accessible - see&nbsp;<a href="#ModuleImplementation">Module Implementation</a>)</p>



<p>The <strong>Tersus Server</strong> has built-in
security
infrastructure which implements this type of authorization system based
on data found in specific database tables (discussed below).<br></p>

<p><strong><u><a name="ModuleImplementation"></a>Module
Implementation</u></strong></p>

<p>The module contains 4 <strong>Database Records</strong>:<br>

<strong>User</strong> -&nbsp;Stores <em>User ID</em>s
and matching <em>Password</em>s for all users of the
application (mapped to the&nbsp;<strong>Users</strong>
table in the database).<br>

<strong>Role</strong> - Stores the <em>Role</em>s
defined for the application (mapped to the&nbsp;<strong>Roles</strong>&nbsp;table
in the database).<br>

<strong>User_Role</strong> - Stores
the&nbsp;assignments of&nbsp;<em>Role</em>s to <em>User
ID</em>s&nbsp;(mapped to the&nbsp;<strong>User_Roles</strong>
table in the database,&nbsp;a join-table&nbsp;implementing
the&nbsp;many-to-many relationship between <strong>Users</strong>
and <strong>Roles</strong>).<br>

<strong>Role_Permissions</strong> - Stores the assignments
of&nbsp;<em>Permission</em>s to <em>Role</em>s&nbsp;(mapped
to the&nbsp;<strong>Role_Permissions</strong> table in
the database,&nbsp;a join-table&nbsp;implementing
the&nbsp;many-to-many relationship between <strong>Roles</strong>
and <strong>Permissions</strong>).</p>

<p>Since the
4&nbsp;database&nbsp;tables&nbsp;specified above are used
by the security infrastructure&nbsp;built into
the&nbsp;Tersus&nbsp;Server, they should not be renamed, and
existing
fields should not be removed or changed. However, additional fields may
be added if required for specific application functionality.</p>

<p>The module provides 2 <strong>Views</strong> for
managing the user permission system:<br>

<strong>Users</strong> - This view is used to manage users
and assign
them with roles. The view is itself controlled by the user permission
system - the <em>requiredPermission</em> property of
the&nbsp;<strong>Users</strong>&nbsp;view is set
to&nbsp;<strong>View Users</strong> - users who need
access to this view, typically the system administrator, must have this
permission assigned.<br>

<strong>Roles</strong> - This view is used to manage roles
and assign them with permissions. The <em>requiredPermission</em>
property of the <strong>Roles</strong>&nbsp;view is
set to&nbsp;<strong>View&nbsp;Roles</strong> -
users who need access to this view must have this permission assigned.</p>

<p>The module also includes the <strong>Initialize</strong>
service process. Its purpose is to bootstrap the user permission system
(therefore, it is not exposed to the users).<br>

The <strong>Initialize</strong> process is executed each
time the application is started within the <strong>Tersus Server</strong>,
and includes the following sub-processes:</p>

<ol>

  <li><span style="font-weight: bold;">Maintain Role
Super with Full Permissions</span><span style="text-decoration: underline;"></span><br>

If the&nbsp;role <em>Super</em> does not exists, it is
added to the <strong>Roles</strong> table.<br>

Regardless, it is always assigned with all permissions currently
defined in the application, including the <strong>View Users</strong>
and <strong>View Roles</strong> permissions (in the <strong>Roles_Permissions</strong>
table).<br>

Note that in order to keep the role <span style="font-style: italic;">Super</span> up-to-date,
this sub-process is also included in the <span style="font-weight: bold;">Init</span> processes
of&nbsp;the <span style="font-weight: bold;">Users</span>
and <span style="font-weight: bold;">Roles</span>
views.&nbsp;</li>

  <li><span style="font-weight: bold;">Create User
Super if User Table is Empty</span><span style="font-style: italic; font-weight: bold;"></span><br style="font-weight: bold;">

If the <span style="font-weight: bold;">Users</span>
table is <span style="text-decoration: underline;">empty</span>,
the user <span style="font-style: italic;">Super</span>&nbsp;is
created&nbsp;(<em>User ID</em>=<em>Super</em>,
    <em>Password</em>=<em>Super</em>). The new
user <em>Super</em> is mapped to the role <em>Super</em>
(in the <strong>User_Roles</strong> table). </li>

</ol>

<p><span style="text-decoration: underline;">Security Note</span>: When deploying the application, the system
administrator should create&nbsp;other users&nbsp;and <u>delete
the default&nbsp;<i>Super</i> user</u>.</p>

<p>&nbsp;</p>

<p><strong><u>Usage Instructions</u></strong></p>

<p>To use this module, select it from the <span style="font-weight: bold;">Palette</span> and drop it
into the <span style="font-weight: bold;">Root Model</span>.<br>

This will add a new system (with the default name&nbsp;<span style="font-weight: bold;">Authorization</span>),
which in turn will appear as an additional perspective in the browser.</p>

<p>In order to view the changes to your application in the
browser, you should do the following:</p>

<ol>

  <li>All browser windows should be closed.<span style="text-decoration: underline;"></span></li><li><span style="text-decoration: underline;"></span>If you application overrides the default configuration by specifiying, in the project root folder, a local <span style="font-weight: bold;">Configuration.XML</span> file, make sure to add to the <span style="font-weight: bold;">&lt;Application&gt;</span> element, the property <span style="font-weight: bold;">authenticationMethod</span>&nbsp;<span style="font-weight: bold;"></span>with the value <span style="font-weight: bold;">JDBC</span>. </li>

  <li>

Start the&nbsp;application in the <span style="font-weight: bold;">Application Server</span>.
If the application is already started, it must be stopped, then
restarted so that the required database updates are performed.</li>

  <li>Launch the application in a browser window.</li>

  <li>When prompted for a username and password, you may supply
the default values (username=<span style="font-style: italic;">Super</span>,
password=<span style="font-style: italic;">Super</span>).</li>

  <li>Note the additional perspective (called <span style="font-weight: bold;">Authorization,</span> by
default), now appearing.<span style="font-style: italic;"></span></li>

</ol>

<br>

<p><strong><u>See Also</u></strong></p>

<p>The security infrastructure is discussed in the <a href="../../Security/Check%20Permissions/Check%20Permissions.html">Check
Permissions</a> and <a href="../../Security/Get%20All%20Permissions/Get%20All%20Permissions.html">Get
All Permissions</a> template documentation.</p>

</body></html>
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
import java.util.ArrayList;
import java.util.List;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An skeleton of a user defined atomic flow handler.
 * 
 * Replace this comment with a descrption of your plug-in (including triggers and exits).
 * 
 * Code is also just a skeleton - Replace, modify and add the code to fit your specific plug-in.
 * 
 * @author Ofer Brandes
 *
 */
public class MyAction extends Plugin
{
	// Triggers 
	private static final Role INPUTS = Role.get("<Items>");
	private static final Role SORT_CRITERIA = Role.get("<Sort Criteria>");
	private static final Role USE_NEGATIVE_NUMBERS = Role.get("<Use Negatives>");

	SlotHandler	inputTrigger,			// Required input (the plug-in must get this input - input value(s) must be received at runtime)
				sortCriteriaTrigger,	// Non-required input (optional input for the plug-in - the trigger may be non-mandatory or be removed altogether while modeling, and in both cases it is possible no input value will be received at runtime)
				specialUseFlagTrigger;	// Non-required input through a mandatory trigger (optional input for the plug-in, but if the trigger is not removed while modeling, it means input value(s) must be received at runtime)

	// Exits
	private static final Role RESULTS = Role.get("<Results>");
	private static final Role NO_INPUT = Role.get("<No Input>");

	SlotHandler	resultsExit,			// Required output (it doesn't make sense to use the plug-in without asking for this output)
				noInputIndicationExit;	// Non-required output (output the modeler may be uninterested at, so the exit may be removed while modeling)

	/**
	 * Initialize the handler from the action's model (done only once for each model).
	 * 
	 * Note that this method is executed not only at runtime (by the server),
	 * but also for model validation (by Tersus Studio).
	 * 
	 * @param model The model from which the flow handler is to be initialized.
	 */
	public void initializeFromModel(Model model)
	{
		// Standard initializations for all flow handlers
		super.initializeFromModel(model);

		// Get triggers (including validation of mandatority, repetitiveness and type)
		inputTrigger = getRequiredMandatoryTrigger(INPUTS, null, null);
		sortCriteriaTrigger = getNonRequiredTrigger(SORT_CRITERIA, Boolean.FALSE, BuiltinModels.TEXT_ID);
		specialUseFlagTrigger = getNonRequiredMandatoryTrigger(USE_NEGATIVE_NUMBERS, Boolean.FALSE, BuiltinModels.NOTHING_ID);
		
		// Get exits (including validation of mandatority, repetitiveness and type)
		resultsExit = getRequiredExit(RESULTS, Boolean.TRUE, null);
		noInputIndicationExit = getNonRequiredExit(NO_INPUT, Boolean.FALSE, BuiltinModels.NOTHING_ID);

		// Ensure the input and output are of the same type (or at least one of them is Anything)
		checkTypeMismatch(inputTrigger, resultsExit);
	}

	/**
	 * Actually perform the action (executed by the server once for each activation of the model).
	 * 
	 * @param context Runtime context for services such as tracing, logging and database conenctivity
	 * @param flow The action's runtime instance
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		// Method 1 to get input - a single input value
		String sortCriteria = null;
		if (sortCriteriaTrigger != null) // Can be null if the trigger has been removed while modeling
			sortCriteria = (String) sortCriteriaTrigger.get(context,flow); // Can be null if the trigger is non-mandatory and no input is received through it at runtime

		// Method 2 to get input - multiple input value from a repetitive trigger
		List inputItems = null;
		if (inputTrigger.isRepetitive())
		{
			inputItems = (List) inputTrigger.get(context,flow);
		}
		else // A single input value (use again method 1)
		{
			inputItems = new ArrayList();
			Object item = inputTrigger.get(context,flow);
			inputItems.add (item);
		}

		if (inputItems == null)
		{
			if (noInputIndicationExit != null)
				chargeEmptyExit (context, flow, noInputIndicationExit);
			return;
		}

		// Method 3 to get input - iterate over all input values from all triggers
		//
		// This method is useful when the plug-in can get any number of inputs
		// through several triggers (repetitive and nonrepetitive) with any names.
		//
		// Optionally, you can skip one specific trigger (typically a trigger with
		// a reserved name that has a special treatment elsewhere)
		inputItems = new ArrayList();
		TriggerIterator iterator = new TriggerIterator(context,flow,SORT_CRITERIA);
		while (iterator.hasNext())
		{
			Object item = iterator.next();
			inputItems.add (item);
		}
		if (inputItems.size() == 0)
		{
			if (noInputIndicationExit != null)
				chargeEmptyExit (context, flow, noInputIndicationExit);
			return;
		}

		// Now we can do whatever we want with the inputs
		List results = new ArrayList();
		for (int i = 0; i < inputItems.size(); i++)
		{
			Object item = inputItems.get(i);

			// ...
			// Do whatever you want with 'item'
			// ...

			results.add (item); // A simple case - simply accumulating the input objects
		}

		// ...

		// At some point we output the results (multiple results in this case, as the exit is repetitive)
		for (int i = 0; i < results.size(); i++)
			accumulateExitValue(resultsExit, context, flow, results.get(i));

		// If the exit was non-repetitive, we would have used setExitValue()
		// instead of accumulateExitValue()
	}

	/**
	 * Resume the action after suspension (not required for actions).
	 * 
	 * @param context Runtime context for services such as tracing, logging and database conenctivity
	 * @param flow The action's runtime instance
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}
}

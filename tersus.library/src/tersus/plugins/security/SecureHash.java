package tersus.plugins.security;

import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.BinaryValue;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

public class SecureHash extends Plugin
{
	public static final Role KEY = Role.get("<Key>");
	public static final Role DATA = Role.get("<Data>");
	public static final Role ALGORITHM = Role.get("<Algorithm>");
	public static final Role HASH = Role.get("<Hash>");
	private SlotHandler dataTrigger, keyTrigger;
	private SlotHandler algorithmTrigger;
	private SlotHandler hashExit;

	public void initializeFromModel(Model model)
	{
		// TODO Auto-generated method stub
		super.initializeFromModel(model);
		dataTrigger = getRequiredMandatoryTrigger(DATA, Boolean.FALSE, BuiltinModels.BINARY_ID);
		keyTrigger = getNonRequiredTrigger(KEY, Boolean.FALSE, BuiltinModels.BINARY_ID);
		algorithmTrigger = getRequiredMandatoryTrigger(ALGORITHM, Boolean.FALSE,
				BuiltinModels.TEXT_ID);
		hashExit = getRequiredExit(HASH, Boolean.FALSE, BuiltinModels.BINARY_ID);
	}

	public void start(RuntimeContext context, FlowInstance flow)
	{
		BinaryValue data = (BinaryValue) dataTrigger.get(context, flow);
		String algorithm = (String) algorithmTrigger.get(context, flow);
		try
		{
			BinaryValue hash = null;
			if (algorithm.toLowerCase().startsWith("hmac"))
			{
				
				BinaryValue key = (BinaryValue) getTriggerValue(context, flow, keyTrigger);
				if (key == null)
					throw new ModelExecutionException("<Key> required for MAC algorithm \""
							+ algorithm + "\"");

				SecretKeySpec signingKey = new SecretKeySpec(key.toByteArray(), algorithm);

				Mac mac = Mac.getInstance(algorithm);
				mac.init(signingKey);
				byte[] macBytes = mac.doFinal(data.toByteArray());

				hash = new BinaryValue(macBytes);
			}
			else
			{
				MessageDigest md = MessageDigest.getInstance(algorithm);
				hash = new BinaryValue(md.digest(data.toByteArray()));
			}
			setExitValue(hashExit, context, flow, hash);
		}
		catch (InvalidKeyException e)
		{
			throw new ModelExecutionException("Invalid <Key> provided for alogorithm \""
					+ algorithm + "\"t");
		}
		catch (NoSuchAlgorithmException e)
		{
			throw new ModelExecutionException("Secure hash algorithm '" + algorithm
					+ "' does not exist");
		}
	}

}

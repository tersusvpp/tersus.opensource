/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.security;

import javax.crypto.Cipher;

import tersus.runtime.BinaryValue;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.RuntimeContext;
import tersus.runtime.TextHandler;
import tersus.util.Base64;

/**
 * @author Youval Bronicki
 *
 */
public class Decrypt extends Crypt
{

    public void start(RuntimeContext context, FlowInstance flow)
    {
        Object input = inputTrigger.get(context, flow);
        try
        {
        	byte[] inputBytes = input instanceof String ? Base64.decode((String)input) : ((BinaryValue)input).toByteArray();
            Cipher cipher = getCipher(context, flow, Cipher.DECRYPT_MODE);
            byte[] outputBytes = cipher.doFinal(inputBytes);
            if (outputExit != null)
            {
            	if (outputExit.getChildInstanceHandler() instanceof TextHandler)
            	{
            		String outputString = new String(outputBytes,UTF8);
            		setExitValue(outputExit, context, flow, outputString);
            	}
            	else
            		setExitValue(outputExit, context, flow, outputBytes);
            }
        }
        catch (Exception e)
        {
            throw new ModelExecutionException("Decryption failed",e.getMessage(),e);
        }

    }

}

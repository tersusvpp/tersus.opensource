/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.security;
import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
/**
 * An atomic flow handler that returns the current logged-in-user 
 *  (the HTTP request's remote user(
 * @author Youval Bronicki
 *
 */
public class GetLoggedInUser extends Plugin
{
	private static final Role USER_ID_ROLE = Role.get("<User ID>");
    SlotHandler exit;
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		String user = context.getLoggedInUser();
		setExitValue(exit, context, flow, user);
	}
	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		exit = getRequiredExit(USER_ID_ROLE, Boolean.FALSE, BuiltinModels.TEXT_ID);
	}
}

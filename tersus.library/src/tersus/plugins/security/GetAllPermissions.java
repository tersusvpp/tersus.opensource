/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.security;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import tersus.model.BuiltinModels;
import tersus.model.BuiltinProperties;
import tersus.model.FlowModel;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Role;
import tersus.model.indexer.ElementEntry;
import tersus.model.indexer.ModelEntry;
import tersus.model.indexer.RepositoryIndex;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * @author Youval Bronicki
 * 
 * Returns all permissions defined in the current solution.
 * This plugin scans the model hierarchy for any permissions specified, 
 * and returns the list of all scanned permissions.
 * 
 * Triggers: none 
 * Exits: <Permissions> (Repetitive Text)
 * 
 */
public class GetAllPermissions extends Plugin
{
	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	private static final Role EXIT_ROLE = Role.get("<Permissions>");
	private SlotHandler exit;
	private static final String ALL_PERMISSIONS_KEY="All Permissions";
	public void start(RuntimeContext context, FlowInstance flow)
	{
		List<String> allPermissions = (List<String>) context.getProperty(ALL_PERMISSIONS_KEY);
		if (allPermissions == null)
		{
			allPermissions = loadPermissions(context);
			context.setProperty(ALL_PERMISSIONS_KEY, allPermissions);
		}
		for (Iterator iter = allPermissions.iterator(); iter.hasNext();)
		{
			String permission = (String)iter.next();
			accumulateExitValue(exit, context, flow, permission);
		}
		
		
	}
	/**
	 * Scans the whole model for the list of permissions
	 * @param context
	 * @return
	 */
	private List<String> loadPermissions(RuntimeContext context)
	{
	    HashSet<String> permissionSet = new HashSet<String>();
		HashSet<ModelId> scanned = new HashSet<ModelId>();
		ModelId rootModelId = context.getContextPool().getRootModel().getId();
		RepositoryIndex index = context.getModelLoader().getRepository().getUpdatedRepositoryIndex();
		scanPermissions(index, rootModelId, permissionSet, scanned);
		for (ModelId moduleId: index.getAllModuleIds())
		{
		    scanPermissions(index, moduleId, permissionSet, scanned);
		}
		ArrayList<String> list = new ArrayList<String>(permissionSet.size());
		list.addAll(permissionSet);
		Collections.sort(list);
		return list;
	}
	/**
	 * Scans a hierarchy of flow models for required permissions
	 * @param rootModel - the root of the hierarchy
	 * @param permissions - a List to which permission will be added
	 * @param scanned - a set of model ids that have already been scanned (and should be ignored to prevent loops)
	 */
	private void scanPermissions(RepositoryIndex index, ModelId modelId, Set<String> permissions, Set<ModelId> scanned)
	{
		if (scanned.contains(modelId))
			return;
		scanned.add(modelId);
        
		// Add this models permission if it's set
		String modelPermission = index.getPermissionMap().get(modelId);
        if (modelPermission != null)
            permissions.add(modelPermission);

        // If this models is "CheckPermission", add its permissions
        ModelEntry modelEntry = index.getModelEntry(modelId);
        if (modelEntry == null)
        	throw new EngineException("Missing model "+modelId,null,null);
		if (modelEntry.getDescriptor() != null && CheckPermissions.class.getName().equals(modelEntry.getDescriptor().getJavaClass()))
		{
            Model model = index.getRepository().getModel(modelId, true);
			List<String> checkedPermissions = CheckPermissions.getPermissionList((FlowModel)model);
			permissions.addAll(checkedPermissions);
			
		}
		
		
		// Scan elements
		for (Iterator iter = index.getElementEntries(modelId).iterator(); iter.hasNext();)
		{
			ElementEntry element = (ElementEntry)iter.next();
			if (element.isSubflow())
				scanPermissions(index, element.getRefId(), permissions, scanned);
		}
	}
	
	
	/* (non-Javadoc)
	 * @see tersus.runtime.InstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		exit = getRequiredExit(EXIT_ROLE, Boolean.TRUE, BuiltinModels.TEXT_ID);
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.security;
import javax.servlet.http.HttpSession;

import org.apache.log4j.MDC;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.webapp.ContextPool;
import tersus.webapp.LoggingHelper;
import tersus.webapp.WebContext;
/**
 * An atomic flow handler that returns the current logged-in-user 
 *  (the HTTP request's remote user(
 * @author Youval Bronicki
 *
 */
public class SetLoggedInUser extends Plugin
{
	private static final Role USER_ID_ROLE = Role.get("<User Id>");
    SlotHandler userIdTrigger;
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		String user = (String)userIdTrigger.get(context, flow);
		
		if (context instanceof WebContext)
        {
			WebContext webContext = (WebContext)context;

			HttpSession session = webContext.getSession();
            if (session == null)
           		webContext.setRootUser(user);
            else
            {
    		    if (user == null)
                {
                    session.setAttribute("LOGGED_OUT", Boolean.TRUE);
                    session.invalidate();
                    MDC.remove(LoggingHelper.USER_ID);
                }
                else
                {
                    session.setAttribute(ContextPool.REMOTE_USER, user);
                    MDC.put(LoggingHelper.USER_ID,user);
                }
            }
        }
	}
	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		userIdTrigger = getRequiredTrigger(USER_ID_ROLE, Boolean.FALSE, BuiltinModels.TEXT_ID);
	}
}

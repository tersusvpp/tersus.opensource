/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.security;

import java.util.Date;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.InvalidInputException;
import tersus.webapp.ContextPool;
import tersus.webapp.WebContext;

/**
 * @author Youval Bronicki
 * 
 */
public class GetSessionAttributes extends Plugin
{
	public static final Role SESSION_ID = Role.get("<Session Id>");
	public static final Role REMOTE_ADDRESS = Role.get("<Remote Address>");
	public static final Role REMOTE_USER = Role.get("<Remote User>");
	public static final Role CREATION_TIME = Role.get("<Creation Time>");
	public static final Role LAST_ACCESS_TIME = Role.get("<Last Access Time>");
	public static final Role REFERRER = Role.get("<Referrer>");
	public static final Role USER_AGENT = Role.get("<User Agent>");
	public static final Role REQUEST_URI = Role.get("<Request URI>");
	public static final Role BASE_URL=Role.get("<Base URL>");
	public static final Role QUERY_STRTING=Role.get("<Query String>");
	public static final Role REQUEST_URL = Role.get("<Request URL>");
	private static final Role LANGUAGES = Role.get("<Languages>");
	private static final Role THREAD_ID=Role.get("<Thread Id>");

	private SlotHandler sessionIdExit, remoteAddressExit,
		remoteUserExit,	creationTimeExit, lastAccessTimeExit, 
		requestURIExit,	requestURLExit, baseURLExit, queryStringExit,
		userAgentExit, referrerExit, languagesExit, threadIdExit;

	public void start(RuntimeContext context, FlowInstance flow)
	{
		if (context instanceof WebContext)
		{
			HttpSession session = ((WebContext) context).getSession();
			if (session != null)
			{
				String sessionId = session.getId();
				long creationTime = session.getCreationTime();
				String remoteUser = (String) session
						.getAttribute(ContextPool.REMOTE_USER);
				String remoteAddress = (String) session
						.getAttribute(ContextPool.REMOTE_ADDR);
				long lastAccessTime = session.getLastAccessedTime();
				setExitValue(sessionIdExit, context, flow, sessionId);
				setExitValue(creationTimeExit, context, flow,
						new Date(creationTime));
				setExitValue(remoteUserExit, context, flow, remoteUser);
				setExitValue(remoteAddressExit, context, flow, remoteAddress);
				setExitValue(lastAccessTimeExit, context, flow, new Date(
						lastAccessTime));
			}

			HttpServletRequest request = ((WebContext) context).getRequest();
			if (request != null)
			{
				String requestURI = request.getRequestURI();
				String requestURL = String.valueOf(request.getRequestURL());
				String referrer = request.getHeader("referer");
				String userAgent = request.getHeader("User-Agent");
				setExitValue(referrerExit, context, flow, referrer);
				setExitValue(userAgentExit, context, flow, userAgent);
				setExitValue(requestURIExit, context, flow, requestURI);
                setExitValue(requestURLExit, context, flow, requestURL);
                
                if (baseURLExit != null)
                {
                	StringBuffer buffer = new StringBuffer();
                	String scheme = request.getScheme();
					buffer.append(scheme);
                	buffer.append("://");
                	buffer.append(request.getServerName());
                	int port = request.getServerPort();
                	if  (port > 0 && !(("http".equals(scheme) && port == 80) || ("https".equals(scheme) && port == 443)))
                	{
                		buffer.append(':');
                		buffer.append(port);
                	}
                	buffer.append(request.getContextPath());
                	setExitValue(baseURLExit, context, flow, buffer.toString());
                }
                
                setExitValue(queryStringExit, context, flow, request.getQueryString());
                
				if (languagesExit != null)
				{
					String acceptLanguage = request.getHeader("Accept-Language");
					if (acceptLanguage != null)
					{
						Pattern p = Pattern.compile("([^;]+)(;q=([0-9]+))?");
	
						StringTokenizer tok = new StringTokenizer(acceptLanguage,
								",");
						LanguagePref[] languages = new LanguagePref[tok
								.countTokens()];
						for (int i = 0; i < languages.length; i++)
						{
							String languageToken = tok.nextToken();
							Matcher m = p.matcher(languageToken);
							if (!m.find())
								throw new InvalidInputException(
										"Invalid language token '" + languageToken
												+ "'");
							String language = m.group(1);
							String priority = m.group(3);
							if (language == null)
								throw new InvalidInputException(
										"Invalid language token '" + languageToken
												+ "'");
							LanguagePref pref = new LanguagePref();
							pref.language = language;
							if (priority != null)
								pref.priority = Double.parseDouble(priority);
							else
								pref.priority = 1;
							languages[i] = pref;
						}
						// Sort languages by priority (bubble sort)
						for (int i = 1; i < languages.length; i++)
						{
							for (int j = 0; j < i; j++)
							{
								if (languages[j].priority < languages[i].priority)
								{
									LanguagePref tmp = languages[i];
									languages[i] = languages[j];
									languages[j] = tmp;
								}
							}
						}
						for (int i = 0; i < languages.length; i++)
						{
							accumulateExitValue(languagesExit, context, flow,
									languages[i].language);
						}
					}
				}
			}
		}
		setExitValue(threadIdExit, context, flow, Thread.currentThread().getName());
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		sessionIdExit = getNonRequiredExit(SESSION_ID, Boolean.FALSE,
				BuiltinModels.TEXT_ID);
		remoteUserExit = getNonRequiredExit(REMOTE_USER, Boolean.FALSE,
				BuiltinModels.TEXT_ID);
		remoteAddressExit = getNonRequiredExit(REMOTE_ADDRESS, Boolean.FALSE,
				BuiltinModels.TEXT_ID);
		creationTimeExit = getNonRequiredTimeExit(CREATION_TIME, Boolean.FALSE);
		lastAccessTimeExit = getNonRequiredTimeExit(LAST_ACCESS_TIME,
				Boolean.FALSE);
		languagesExit = getNonRequiredExit(LANGUAGES, Boolean.TRUE,
				BuiltinModels.TEXT_ID);
		userAgentExit = getNonRequiredExit(USER_AGENT, Boolean.FALSE,
				BuiltinModels.TEXT_ID);
		referrerExit = getNonRequiredExit(REFERRER, Boolean.FALSE,
				BuiltinModels.TEXT_ID);
		requestURIExit = getNonRequiredExit(REQUEST_URI, Boolean.FALSE,
				BuiltinModels.TEXT_ID);
        requestURLExit = getNonRequiredExit(REQUEST_URL, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        baseURLExit = getNonRequiredExit(BASE_URL, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        queryStringExit = getNonRequiredExit(QUERY_STRTING, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
		threadIdExit = getNonRequiredExit(THREAD_ID, Boolean.FALSE,
				BuiltinModels.TEXT_ID);
	}
}

class LanguagePref
{
	double priority;
	String language;
}

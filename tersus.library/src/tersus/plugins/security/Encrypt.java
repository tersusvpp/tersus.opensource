/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.security;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;

import tersus.runtime.BinaryValue;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.RuntimeContext;
import tersus.runtime.TextHandler;

/**
 * @author Youval Bronicki
 * 
 */
public class Encrypt extends Crypt
{

	/**
     *
     */

	public void start(RuntimeContext context, FlowInstance flow)
	{
		try
		{
			byte[] inputBytes;
			if (inputTrigger.getChildInstanceHandler() instanceof TextHandler)
			{
				 inputBytes = getTriggerText(context, flow, inputTrigger).getBytes(UTF8);
			}
			else
			{
				inputBytes = ((BinaryValue)getTriggerValue(context, flow, inputTrigger)).toByteArray();
			}
			
			Cipher cipher = getCipher(context, flow, Cipher.ENCRYPT_MODE);
			byte[] outputBytes = cipher.doFinal(inputBytes);
			if (outputExit != null)
			{
				if (outputExit.getChildInstanceHandler() instanceof TextHandler)
				{
					String outputString = new String(Base64.encodeBase64(outputBytes));
					setExitValue(outputExit, context, flow, outputString);
				}
				else
				{
					setExitValue(outputExit, context, flow, outputBytes);
				}
			}
		}
		catch (Exception e)
		{
			throw new ModelExecutionException("Encryption failed", e.getMessage(), e);
		}

	}

}

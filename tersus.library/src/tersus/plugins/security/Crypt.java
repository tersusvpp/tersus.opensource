/************************************************************************************************
 * Copyright (c) 2003-2022 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.security;

import java.security.AlgorithmParameters;
import java.security.Key;
import java.security.KeyException;
import java.security.KeyFactory;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.BinaryValue;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.Base64;

/**
 * @author Youval Bronicki
 * 
 */
public abstract class Crypt extends Plugin
{

	protected static final Role INPUT = Role.get("<Input>");

	protected static final Role OUTPUT = Role.get("<Output>");

	protected static final Role PARAMETERS = Role.get("<Parameters>");

	protected static final Role ALGORITHM = Role.get("<Algorithm>");
	protected static final Role TRANSFORMATION = Role.get("<Transformation>");

	protected static final Role KEY = Role.get("<Key>");

	protected static final String UTF8 = "UTF-8";
	public static final String RSA = "RSA";

	protected static final String DEFAULT_ALGORITHM = "DESede";

	protected SlotHandler inputTrigger, keyTrigger, algorithmTrigger, transformationTrigger, parametersTrigger, parametersExit,  outputExit;

	/**
	 * @throws KeyException
	 * 
	 */
	public Cipher getCipher(RuntimeContext context, FlowInstance flow, int mode) throws Exception
	{
		String transformation = null;
		if (transformationTrigger != null)
			transformation = (String) transformationTrigger.get(context, flow);
		else
			if (algorithmTrigger != null)
				transformation = (String) algorithmTrigger.get(context, flow);
		if (transformation == null)
			transformation = DEFAULT_ALGORITHM;

		Cipher cipher = Cipher.getInstance(transformation);
		String algorithm = transformation.split("/")[0];
		if (getPluginVersion() == 0)
		{
			String keyStr = (String) keyTrigger.get(context, flow);
			SecureRandom rand = new SecureRandom(keyStr.getBytes("UTF-8"));
			KeyGenerator keyGenerator = KeyGenerator.getInstance(algorithm);
			keyGenerator.init(rand);
			SecretKey key = keyGenerator.generateKey();
			cipher.init(mode, key);
		}
		else
		{
			SlotHandler trigger = keyTrigger;
			byte[] keyBytes = getBytes(trigger, context, flow);
			byte[] parameterBytes = getBytes(parametersTrigger, context, flow);
			AlgorithmParameters parameters = null;

			if (parameterBytes != null)
			{
				parameters = AlgorithmParameters.getInstance(algorithm);
				parameters.init(parameterBytes);
			}
			Key key;
			if (RSA.equals(algorithm))
			{
				KeySpec keySpec;
				KeyFactory keyFactory = KeyFactory.getInstance("RSA");

				if (mode == Cipher.DECRYPT_MODE)
				{
					// Key is expected to be private key in PKCS#8 format
					keySpec = new PKCS8EncodedKeySpec(keyBytes);
					key = keyFactory.generatePrivate(keySpec);
				}
				else
				{
					// Keys is expected to be public key in X509 format
					keySpec = new X509EncodedKeySpec(keyBytes);
					key = keyFactory.generatePublic(keySpec);
				}

			}
			else
			{
				key = new SecretKeySpec(keyBytes, algorithm);
				//SecretKeyFactory.getInstance(cipher.getAlgorithm()).generateSecret(new SecretKeySpec(keyBytes, cipher.getAlgorithm()));
			}
			try
			{
				cipher.init(mode, key, parameters);
			}
			catch (Exception e)
			{
				throw new ModelExecutionException("Failed to create "+transformation+" cipher", e.getMessage(), e);
			}
			if (parametersExit != null)
			{
				parameters = cipher.getParameters();
				if (parameters != null)
					setExitValueWithBase64Conversion(parametersExit, context, flow, parameters.getEncoded());
			}
		}
		return cipher;

	}

	private byte[] getBytes(SlotHandler trigger, RuntimeContext context, FlowInstance flow)
	{
		if (trigger == null)
			return null;
		Object obj = trigger.get(context, flow);
		byte[] bytes = null;
		if (obj instanceof String)
		{
			try
			{
				bytes = Base64.decode((String) obj);
			}
			catch (Exception e)
			{
				
			}
		}
		else if (obj instanceof BinaryValue)
		{
			bytes = ((BinaryValue) obj).toByteArray();
		}
		else if (obj != null)
			throw new ModelExecutionException(trigger.getRole()
					+ " must containt Text or Binary data");
		return bytes;
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		inputTrigger = getRequiredMandatoryTrigger(INPUT, Boolean.FALSE, null);
		keyTrigger = getRequiredMandatoryTrigger(KEY, Boolean.FALSE, null);
		algorithmTrigger = getNonRequiredTrigger(ALGORITHM, Boolean.FALSE,
				BuiltinModels.TEXT_ID);
		transformationTrigger = getNonRequiredTrigger(TRANSFORMATION, Boolean.FALSE,
				BuiltinModels.TEXT_ID);
		parametersTrigger = getNonRequiredTrigger(PARAMETERS, Boolean.FALSE, null);
		parametersExit = getNonRequiredExit(PARAMETERS, Boolean.FALSE, null);
		outputExit = getRequiredExit(OUTPUT, Boolean.FALSE, null);
		
		validateType(inputTrigger, BuiltinModels.TEXT_ID, BuiltinModels.BINARY_ID);
		validateType(outputExit, BuiltinModels.TEXT_ID, BuiltinModels.BINARY_ID);
		validateType(keyTrigger, BuiltinModels.TEXT_ID, BuiltinModels.BINARY_ID);
		validateType(parametersTrigger, BuiltinModels.TEXT_ID, BuiltinModels.BINARY_ID);
		validateType(parametersExit, BuiltinModels.TEXT_ID, BuiltinModels.BINARY_ID);

	}
}
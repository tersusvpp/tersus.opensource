/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.security;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import tersus.model.BuiltinModels;
import tersus.model.BuiltinProperties;
import tersus.model.FlowModel;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.Role;
import tersus.model.SlotType;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * Check which of a set of permissions are granted
 * 
 * @author Youval Bronicki
 *
 */
public class CheckPermissions extends Plugin
{
	private static final Role NONE = Role.get("<None>");
    private static final Role PERMISSION_NAME = Role.get("<Permission Name>");
    private static final Role MODEL_ID = Role.get("<Model Id>");
    private static final Role PERMITTED = Role.get("<Permitted>");
    private static final Role NOT_PERMITTED = Role.get("<Not Permitted>");
	private SlotHandler noneExit, permissionNameTrigger, permittedExit, notPermittedExit;
    private SlotHandler modelIdTrigger;

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
        if (modelIdTrigger != null)
            checkPermissionByModelId(context,flow);
        else if (permissionNameTrigger != null)
            checkPermissionByName(context, flow);
        else
            checkPermissionsBySlotNames(context, flow);
	}

    private void checkPermissionByModelId(RuntimeContext context, FlowInstance flow)
    {
        String modelId = (String)modelIdTrigger.get(context, flow);
        if (modelId == null)
            return;
        if (context.isPermitted(new ModelId(modelId)))
        {
            if (permittedExit != null)
                setExitValue(permittedExit, context, flow, modelId);
        }
        else
        {
            if (notPermittedExit != null)
                setExitValue(notPermittedExit, context, flow, modelId);
        }
        
    }
    private void checkPermissionByName(RuntimeContext context, FlowInstance flow)
    {
        String permissionName = (String)permissionNameTrigger.get(context, flow);
        if (permissionName == null)
            return;
        if (context.isPermitted(permissionName))
        {
            if (permittedExit != null)
                setExitValue(permittedExit, context, flow, permissionName);
        }
        else
        {
            if (notPermittedExit != null)
                setExitValue(notPermittedExit, context, flow, permissionName);
        }
        
    }

    private void checkPermissionsBySlotNames(RuntimeContext context, FlowInstance flow)
    {
        boolean found = false;
		for (int i=0; i< exits.length; i++)
		{
			SlotHandler exit = exits[i];
			if (exit != noneExit)
			{
				String permission = exit.getRole().toString();
				if (context.isPermitted(permission))
				{
					chargeEmptyExit(context, flow, exit);
					found = true;
				}
			}
		}
		if (!found)
			if (noneExit != null)
				chargeEmptyExit(context, flow, noneExit);
    }
	
	/* (non-Javadoc)
	 * @see tersus.runtime.InstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		noneExit = getNonRequiredExit(NONE, Boolean.FALSE, BuiltinModels.NOTHING_ID);
        permissionNameTrigger = getNonRequiredTrigger(PERMISSION_NAME, Boolean.FALSE, BuiltinModels.TEXT_ID);
        modelIdTrigger = getNonRequiredTrigger(MODEL_ID, Boolean.FALSE, BuiltinModels.TEXT_ID);
        permittedExit = getNonRequiredExit(PERMITTED, Boolean.FALSE, BuiltinModels.TEXT_ID);
        notPermittedExit = getNonRequiredExit(NOT_PERMITTED, Boolean.FALSE, BuiltinModels.TEXT_ID);
	}

	/**
	 * @param model
	 * @return The list of permissions checked by a model
	 */
	public static List<String> getPermissionList(FlowModel model)
	{
		List<String> permissions = new ArrayList<String>();
		for (ModelElement element : model.getElements())
		{
			if (element.getProperty(BuiltinProperties.TYPE) == SlotType.EXIT)
			{
				String roleStr = element.getRole().toString();
				// if (! (element.getRole().equals(NONE) || element.getRole().equals(PERMITTED) || element.getRole().equals(NOT_PERMITTED)))
				if (! (element.getRole().isDistinguished()))
					permissions.add(roleStr);
			}
			
		}
		return permissions;
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2023 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.security;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import javax.crypto.KeyGenerator;


import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

public class GenerateKey extends Plugin
{

	public static final Role KEY_LENGTH = Role.get("<Key Size>");
	public static final Role ALGORITHM = Role.get("<Algorithm>");
	public static final Role PUBLIC_KEY = Role.get("<Public Key>");
	public static final Role PRIVATE_KEY = Role.get("<Private Key>");
	public static final Role SECRET_KEY = Role.get("<Secret Key>");

	protected SlotHandler keySizeTrigger, algorithmTrigger, publicKeyExit, privateKeyExit, secretKeyExit;

	@Override
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		keySizeTrigger = getNonRequiredTrigger(KEY_LENGTH, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		algorithmTrigger = getNonRequiredTrigger(ALGORITHM, Boolean.FALSE,
				BuiltinModels.TEXT_ID);
		privateKeyExit = getNonRequiredExit(PRIVATE_KEY, Boolean.FALSE, null);
		publicKeyExit = getNonRequiredExit(PUBLIC_KEY, Boolean.FALSE, null);
		secretKeyExit = getNonRequiredExit(SECRET_KEY, Boolean.FALSE, null);
		
		validateType(privateKeyExit, BuiltinModels.TEXT_ID, BuiltinModels.BINARY_ID);
		validateType(publicKeyExit, BuiltinModels.TEXT_ID, BuiltinModels.BINARY_ID);
		validateType(secretKeyExit, BuiltinModels.TEXT_ID, BuiltinModels.BINARY_ID);

	}

	@Override
	public void start(RuntimeContext context, FlowInstance flow)
	{
		String algorithm = getTriggerText(context, flow, algorithmTrigger);
		int keySize = getTriggerValue(keySizeTrigger, context, flow, -1);

		if ("RSA".equals(algorithm))
		{
			KeyPairGenerator kpg;
			try
			{
				kpg = KeyPairGenerator.getInstance(algorithm);
			}
			catch (NoSuchAlgorithmException e)
			{
				throw new ModelExecutionException("The algorithm '" + algorithm
						+ "' is not supported in this environment", e);
			}
			if (keySize > -1)
				kpg.initialize(keySize);
			KeyPair kp = kpg.generateKeyPair();
			RSAPublicKey pubk = (RSAPublicKey) kp.getPublic();
			RSAPrivateKey prvk = (RSAPrivateKey) kp.getPrivate();
			setExitValueWithBase64Conversion(publicKeyExit, context, flow, pubk.getEncoded());
			setExitValueWithBase64Conversion(privateKeyExit, context, flow, prvk.getEncoded());
		}
		else
		{
			try
			{
				if (algorithm == null)
					algorithm=Crypt.DEFAULT_ALGORITHM;
				KeyGenerator kg = KeyGenerator.getInstance(algorithm);
				if (keySize > -1)
					kg.init(keySize);
				byte[] keyBytes = kg.generateKey().getEncoded();
				setExitValueWithBase64Conversion(secretKeyExit, context, flow, keyBytes);
			}
			catch (NoSuchAlgorithmException e)
			{
				throw new ModelExecutionException("The algorithm '" + algorithm
						+ "' is not supported", e);
			}
		}
	}

}

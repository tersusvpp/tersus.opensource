package tersus.plugins.display;

import tersus.model.Composition;
import tersus.model.DataElement;
import tersus.model.DataType;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.Path;
import tersus.model.validation.IValidationContext;
import tersus.model.validation.Problems;
import tersus.model.validation.ValidatorBase;

public class SimpleTableValidator extends ValidatorBase
{

	public void validate(IValidationContext context, Model model)
	{
		super.validate(context, model);
		validateRowElement(context, model);
	}

	/**
	 * Validates that the table has exactly one row element - a repetitive composite element
	 * @param context
	 * @param model
	 */
	private void validateRowElement(IValidationContext context, Model model)
	{
		ModelElement rowElement = null;
		for (int i=0;i<model.getElements().size(); i++)
		{
			ModelElement e = (ModelElement)model.getElements().get(i);
			if (e instanceof DataElement && ! e.getRole().isDistinguished())
			{
				if (rowElement != null)
				{
					context.addProblem(Problems.INVALID_DATA_ELEMENT, "Simple table can contain only a single row element", e.getRole());
					return;
				}
				rowElement = e;
				if (Composition.COLLECTION != ((DataType)e.getReferredModel()).getComposition())
					context.addProblem(Problems.INVALID_DATA_ELEMENT, "A simple table row must be composite (a data structure or a database record)", e.getRole());
					
				if (!e.isRepetitive())
					context.addProblem(Problems.INVALID_DATA_ELEMENT, "A simple table row must be repetitive", e.getRole());
				
					
					
			}
		}
		if (rowElement == null)
			context.addProblem(Problems.MISSING_ELEMENT, "A simple table must have a row element", Path.EMPTY);

	}

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Path;
import tersus.model.Role;
import tersus.runtime.DatabaseAdapter;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SQLUtils;
import tersus.runtime.SlotHandler;
import tersus.util.Misc;

/**
 * An atomic flow handler that finds the next sequence number for a given key
 * 
 * @author Youval Bronicki
 * 
 */

public class SequenceNumber extends DatabaseAction
{
    private static final Role OUTPUT_ROLE = Role.get("<Next>");
    protected static final Role TABLE_NAME_ROLE = Role.get("<Table Name>");
    private static final Role KEY = Role.get("<Key>");

    private String defaultKey;
    private String defaultTableName;
    SlotHandler keyTrigger, tableNameTrigger, exitHandler;
    private static String KEY_PROPERTY = "Key";
    private static String TABLE_NAME_PROPERTY = "sequenceNumberTable";

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
    	String tableName = getTriggerValue(tableNameTrigger, context, flow, defaultTableName);
        String key = getTriggerValue(keyTrigger, context, flow, defaultKey);
        Number out = new Number(nextSequenceNumber(context, flow, tableName, key));
        setExitValue(exitHandler, context, flow, out);
    }

    private int nextSequenceNumber(RuntimeContext context, FlowInstance flow, String tableName, String key)
    {
        PreparedStatement insertStatement = null;
        PreparedStatement selectStatement = null;
        PreparedStatement updateStatement = null;
        ArrayList<Object>values = new ArrayList<Object>(1);
        boolean autoCommit = getTriggerValue(autoCommitTrigger, context, flow, false);
        boolean completed = false;
        ResultSet rs = null;
        int nextSequenceNumber;
        Connection conn = null;
        try
        {
            conn = getConnection(context, flow);
            if (autoCommit)
            	conn.setAutoCommit(false); // We don't really use autocommit, but rather a short transaction
            DatabaseAdapter adapter = context.getDBAdapter(conn);
            String sql = adapter.getSelectForUpdate("Value", defaultTableName, "Name=?");
            selectStatement = conn.prepareStatement(sql);
            selectStatement.setString(1, key);
            values.add(key);
            rs = context.executeQuery(selectStatement, sql, values, 0);
            if (!rs.next())
            {
                rs.close();
                selectStatement.close();
                selectStatement = null;
                String insertSQL = "INSERT INTO " + tableName + " (Name, Value) VALUES ('" + key + "',1)";
                insertStatement = conn.prepareStatement(insertSQL);
                context.executeUpdate(insertStatement, insertSQL, Collections.emptyList(), 0);
                insertStatement.close();
                insertStatement = null;
                nextSequenceNumber = 1;

            }
            else
            {
	            nextSequenceNumber = rs.getInt(1) + 1;
	            rs.close();
	            rs = null;
	            selectStatement.close();
	            selectStatement = null;
	            String updateSQL = "UPDATE "+tableName+" SET Value=? WHERE Name=?";
	            updateStatement = conn.prepareStatement(updateSQL);
	            values.clear();
	            updateStatement.setInt(1, nextSequenceNumber);
	            values.add(nextSequenceNumber);
	            updateStatement.setString(2, key);
	            values.add(key);
	            int updatedRows = context.executeUpdate(updateStatement, updateSQL, values, 0);
	            if (Misc.ASSERTIONS)
	                Misc.assertion(updatedRows == 1);
	            updateStatement.close();
	            updateStatement = null;
            }
	        if (autoCommit)
	        {
	        	conn.commit();
	        	conn.setAutoCommit(true);
		        completed = true;
	        }
	        return nextSequenceNumber;
        }
        catch (SQLException e)
        {

            e.printStackTrace();
            throw new EngineException("Failed to generate sequence number", "Table = " + tableName + ", Key = " + key,
                    e);
        }
        finally
        {
        	if (autoCommit && ! completed && conn != null)
        	{
        		try
        		{
        			conn.rollback();
        		}
        		catch (SQLException e)
        		{
        			context.getContextPool().engineLog.error("Failed to rollback changes (failure generating sequence number in auto-commit mode");
        		}
        		try
        		{
        			conn.setAutoCommit(true);
        		}
        		catch (SQLException e)
        		{
        			context.getContextPool().engineLog.error("Failed to restore auto-commit (failure generating sequence number in auto-commit mode");
        		}
        	}
            if (rs != null)
            {
                try
                {
                    rs.close();
                }
                catch (SQLException e1)
                {
                    // Another exception already thrown
                }
            }
            if (selectStatement != null)
            {
                try
                {
                    selectStatement.close();
                }
                catch (SQLException e1)
                {
                    // Another exception already thrown
                }
            }
            if (updateStatement != null)
            {
                try
                {
                    updateStatement.close();
                }
                catch (SQLException e1)
                {
                    // Another exception already thrown
                }
            }
        }
    }

 
    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
     */
    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);

        defaultKey = (String) model.getProperty(KEY_PROPERTY);
        if (defaultKey == null || defaultKey.length() == 0)
        {
            if (getPluginVersion() > 0)
                defaultKey = model.getName();
            else
                defaultKey = getModelId().getPath();
        }
        keyTrigger = getNonRequiredTrigger(KEY, Boolean.FALSE, BuiltinModels.TEXT_ID);
        tableNameTrigger = getNonRequiredTrigger(TABLE_NAME_ROLE, Boolean.FALSE, BuiltinModels.TEXT_ID);
        defaultTableName = (String) model.getProperty(TABLE_NAME_PROPERTY);
        if (defaultTableName == null)
            notifyInvalidModel(Path.EMPTY, "Missing Property", "No '" + TABLE_NAME_PROPERTY + "' specified");
   
        exitHandler = getRequiredExit(OUTPUT_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
    }

    public String checkDB(RuntimeContext context)
    {
        if (!SQLUtils.tableExists(context, context.getDefaultDataSourceName(), defaultTableName, false))
            return "Missing table " + defaultTableName;
        else
        {
            List<String> columns = SQLUtils.getColumns(context, defaultTableName);
            List<String> missing = new ArrayList<String>();
            if (!columns.contains("NAME"))
                missing.add("NAME");
            if (!columns.contains("VALUE"))
                missing.add("VALUE");
            if (missing.isEmpty())
                return null;
            else
                return "Missing columns in table " + defaultTableName + " : " + Misc.concatenateList(missing, " , ");

        }

    }

    public void initDB(RuntimeContext context)
    {
        String sql = "";
        if (tableNameTrigger  == null && !SQLUtils.tableExists(context, context.getDefaultDataSourceName(), defaultTableName, false))
        {
            sql = "CREATE TABLE " + defaultTableName + " (Name " + context.getDefaultDBAdapter().getVarchar(0)
                    + " PRIMARY KEY, Value " + context.getDefaultDBAdapter().getInt() + " )";
            context.executeUpdate(sql, true);
        }
        return;

    }
}
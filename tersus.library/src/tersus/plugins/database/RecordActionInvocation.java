/************************************************************************************************
 * Copyright (c) 2003-2007 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.database;

import java.sql.Connection;

import tersus.runtime.CompositeDataHandler;
import tersus.runtime.DatabaseAdapter;
import tersus.runtime.FieldList;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.ModelIdHelper;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SQLUtils;

public class RecordActionInvocation
{
    

    protected RuntimeContext context;

    protected FlowInstance flow;

    protected String tableName;

    protected FieldList fields;
    
    protected Object record;

    protected CompositeDataHandler recordHandler;

    protected Connection connection = null;

    protected DatabaseAdapter dbAdapter;

    private RecordAction actionHandler;
    
    boolean useQuotedIdentifiers;

    RecordActionInvocation(RecordAction actionHandler, RuntimeContext context, FlowInstance flow,
            Object record)
    {
        this.actionHandler = actionHandler;
        this.context = context;
        this.flow = flow;
        this.record = record;
        this.useQuotedIdentifiers = actionHandler.getLoader().useQuotedIdentifiers();
        init();
    }

    void init()
    {

        initConnection();
        initRecordHandler();
        initFields();
        initTableName();
    }


    private void initRecordHandler()
    {
        recordHandler = (CompositeDataHandler) context.getModelLoader()
                .getHandler(ModelIdHelper.getCompositeModelId(record));
    }

    private void initTableName()
    {
        if (actionHandler.tableNameTrigger != null)
            tableName = (String) actionHandler.tableNameTrigger.get(context, flow);
        if (tableName == null)
            tableName = recordHandler.tableName;
        if (tableName == null)
        	throw new ModelExecutionException("No table name specified and "+recordHandler.getModelId() + " is not persistent");
        if (useQuotedIdentifiers)
        	tableName=SQLUtils.quote(tableName);
    }
    
    private void initFields()
    {
        fields = recordHandler.getFields();
    }

    private Connection initConnection()
    {
        connection = actionHandler.getConnection(context, flow);
        dbAdapter = context.getDBAdapter(connection);
        return connection;
    }

}

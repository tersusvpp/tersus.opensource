/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.database;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Path;
import tersus.model.Role;
import tersus.runtime.ColumnDescriptor;
import tersus.runtime.DataHandler;
import tersus.runtime.ElementHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.MapHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.PathHandler;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.Misc;
import tersus.util.SQLUtils;

/**
 * @author Ofer Brandes
 * 
 *         A base class for databse plugins with output result sets (content refactored from
 *         'DatabaseQuery')
 * 
 */
public abstract class QueryPlugin extends DatabaseAction

{
	private static final Role OUTPUT_COLUMNS = Role.get("<Output Columns>");
	protected static final Role NAME = Role.get("Name");
	protected static final Role TYPE = Role.get("Type");
	protected static final Role NUMBER_OF_RECORDS = Role.get("<Number of Records>");
	protected SlotHandler outputColumnsExit;

	protected InstanceHandler columnDescriptorHandler;
	protected ElementHandler nameElementHandler;
	protected ElementHandler typeElementHandler;

	protected SlotHandler numberOfRecordsTrigger;

	protected List<Object> getResults(ResultSet rs, DataHandler resultType, String sql,
			List<Object> values, RuntimeContext context)
	{
		try
		{
			List<Object> results = Collections.emptyList();
			PathHandler rootHandler = new PathHandler(resultType, Path.EMPTY);
			if (resultType instanceof MapHandler)
			{
				ResultSetMetaData metaData = rs.getMetaData();
				int nCols = metaData.getColumnCount();
				ArrayList<String> columnNames = getColumnNameList(nCols, metaData);
				while (rs.next())
				{
					HashMap<String, Object> record = new HashMap<String, Object>(nCols);
					for (int i = 0; i < nCols; i++)
					{
						Object value = rs.getObject(i + 1);
						if (value instanceof java.lang.Number)
							value = new tersus.runtime.Number(((java.lang.Number) value)
									.doubleValue());
						else if (value instanceof java.sql.Timestamp)
							value = new java.util.Date(((java.sql.Timestamp) value).getTime());

						record.put(columnNames.get(i), value);
					}
					if (results.isEmpty())
						results = new ArrayList<Object>();
					results.add(record);
				}
			}
			else
			{
				List<PathHandler> leafHandlers = rootHandler.allLeafHandlers(resultType);

				// Ensure the structure of the SQL result set matches the output
				// structure
				int[] elementPositionsInRs = getElementPositions(sql, values, rs, leafHandlers,
						resultType);
				while (rs.next())
				{
					if (results.isEmpty())
						results = new ArrayList<Object>();
					results.add(nextResult(context, rs, resultType, leafHandlers,
							elementPositionsInRs));
				}
			}
			return results;
		}
		catch (SQLException e)
		{
			throw new EngineException("Error in SQL Query",
					queryDetails(sql.toString(), values, e), e);
		}

	}

	private ArrayList<String> getColumnNameList(int nCols, ResultSetMetaData metaData)
			throws SQLException
	{
		ArrayList<String> columnNames = new ArrayList<String>();
		for (int i = 1; i <= nCols; i++)
		{
			columnNames.add(getValidColumnName(metaData.getColumnLabel(i), columnNames));
		}

		return columnNames;
	}

	protected int outputResults(ResultSet rs, SlotHandler exit, String sql,
			List<Object> values, RuntimeContext context, FlowInstance flow)
	{
		DataHandler resultType = (DataHandler) exit.getChildInstanceHandler();
		List<Object> results = getResults(rs, resultType, sql, values, context);
		for (Object value : results)
		{
			if (exit.isRepetitive())
				accumulateExitValue(exit, context, flow, value);
			else
				setExitValue(exit, context, flow, value);
		}
		return results.size();
	}

	protected int outputResults(ResultSet rs, SlotHandler[] exits, String sql,
			List<Object> values, RuntimeContext context, FlowInstance flow)
	{
		int numberOfResults = 0;

		// Find an exit for which the structure of the SQL result set matches
		// the output structure
		StringBuilder b = new StringBuilder();
		for (SlotHandler exit : exits)
		{
			try
			{
				numberOfResults = outputResults(rs, exit, sql, values, context, flow);
				b.setLength(0);
				break;
			}
			catch (QueryMismatchException e) // Mismatch
			{
				b.append("For exit \"");
				b.append(exit.getRole());
				b.append("\":\n");
				b.append(e.getDetails());
				b.append("\n");
				continue; // try next exit
			}
		}

		if (b.length()>0)
			throw new ModelExecutionException("No matching exit found for result set", b.toString()); // Maybe a better description is needed

		return numberOfResults;
	}

	private int[] getElementPositions(String sql, List<Object> values, ResultSet rs,
			List<PathHandler> leafHandlers, DataHandler resultType)
	{
		try
		{
			ResultSetMetaData metaData = rs.getMetaData();
			ResultSetMetaData metadata = metaData;
			int rsColumnCount = metadata.getColumnCount();

			int resultElementCount = leafHandlers.size();

			int[] elementPositions = new int[resultElementCount];

			if (resultType instanceof LeafDataHandler)
			{
				if (rsColumnCount != 1)
				{
					StringBuffer details = new StringBuffer();
					details.append(queryDetails(sql.toString(), values, null) + "\n");
					details.append("Result Columns: ");
					for (int i = 0; i < rsColumnCount; i++)
					{
						if (i > 0)
							details.append(", ");
						details.append(metadata.getColumnLabel(i + 1));
					}
					throw new QueryMismatchException(
							"Database query returned mutiple columns, but only a single column is expected",
							details.toString());
				}
			}
			else
			{
				StringBuffer missingColumns = null;
				for (int i = 0; i < resultElementCount; i++)
				{
					String outputColumnName = ColumnDescriptor.columnDisplayName(leafHandlers
							.get(i));
					boolean found = false;
					for (int pos = 1; pos <= rsColumnCount && !found; pos++)
					{
						String rsColumnName = metadata.getColumnLabel(pos);
						if (outputColumnName.compareToIgnoreCase(rsColumnName) == 0)
						{
							elementPositions[i] = pos;
							found = true;
						}
					}
					if (!found)
					{
						String alternateColumnName = Misc.sqlize(outputColumnName);
						if (!alternateColumnName.equals(outputColumnName))
							for (int pos = 1; pos <= rsColumnCount && !found; pos++)
							{
								String rsColumnName = metadata.getColumnLabel(pos);
								if (alternateColumnName.compareToIgnoreCase(rsColumnName) == 0)
								{
									elementPositions[i] = pos;
									found = true;
								}
							}
					}
					if (!found)
					{
						if (missingColumns == null)
						{
							missingColumns = new StringBuffer("Missing Columns: "
									+ outputColumnName);
						}
						else
						{
							missingColumns.append(", " + outputColumnName);
						}
					}

					if (missingColumns != null)
					{
						StringBuffer details = new StringBuffer();
						details.append(missingColumns);
						details.append("\nQuery result columns: ");
						for (int col = 0; col < rsColumnCount; col++)
						{
							if (col > 0)
								details.append(", ");
							details.append(metadata.getColumnLabel(col + 1));
						}
						throw new QueryMismatchException("Missing column in database query result",
								details.toString());
					}
				}
			}
			return elementPositions;
		}
		catch (SQLException e)
		{
			throw new EngineException("Error in SQL Query",
					queryDetails(sql.toString(), values, e), e);
		}
	}

	private Object nextResult(RuntimeContext context, ResultSet rs, DataHandler resultType,
			List<PathHandler> leafHandlers, int[] elementPositionsInRs) throws SQLException
	{
		Object value;
		if (resultType instanceof LeafDataHandler)
		{
			value = ((LeafDataHandler) resultType).fromSQL(rs, 1);
		}
		else
		{
			value = resultType.newInstance(context);
			for (int i = 0; i < leafHandlers.size(); i++)
			{
				PathHandler leafHandler = leafHandlers.get(i);
				Object resultElementValue = ((LeafDataHandler) leafHandler.getValueHandler())
						.fromSQL(rs, elementPositionsInRs[i]);
				if (resultElementValue != null)
				{
					if (leafHandler.isRepetitive())
						leafHandler.accumulate(context, value, resultElementValue);
					else
						leafHandler.set(context, value, resultElementValue);
				}
			}
		}
		return value;
	}

	static protected String queryDetails(String sql, List<Object> values, SQLException e)
	{
		String details = (e != null ? "\nSQL Error: " + ((SQLException) e).getMessage() : "");

		return "SQL: '" + sql + "'" + "\nValues: " + Misc.toString(values) + details;
	}

	@Override
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		outputColumnsExit = getNonRequiredExit(OUTPUT_COLUMNS, Boolean.TRUE,
				BuiltinModels.COLUMN_DESCIPTOR);
		numberOfRecordsTrigger = getNonRequiredTrigger(NUMBER_OF_RECORDS, Boolean.FALSE,
				BuiltinModels.NUMBER_ID);

		if (outputColumnsExit != null)
		{
			columnDescriptorHandler = outputColumnsExit.getChildInstanceHandler();
			setColumnDescriptorHandlerElements();
		}
	}

	protected void setColumnDescriptorHandlerElements()
	{
		nameElementHandler = columnDescriptorHandler.getElementHandler(NAME);
		typeElementHandler = columnDescriptorHandler.getElementHandler(TYPE);
	}

	public int getNumberOfRecords(RuntimeContext context, FlowInstance flow)
	{
		return getTriggerValue(numberOfRecordsTrigger, context, flow, 0);
	}

	protected void outputColumns(ResultSet rs, String sql, RuntimeContext context, FlowInstance flow)
	{
		if (outputColumnsExit != null)
		{
			ArrayList<Object> columnDescriptorList = getColumnDescriptorList(rs, outputColumnsExit
					.getChildInstanceHandler(), context, sql);

			for (Object columnDescriptor : columnDescriptorList)
			{
				accumulateExitValue(outputColumnsExit, context, flow, columnDescriptor);
			}
		}
	}

	protected ArrayList<Object> getColumnDescriptorList(ResultSet rs,
			InstanceHandler instanceHandler, RuntimeContext context, String sql)
	{
		ArrayList<Object> columnDescriptorList = new ArrayList<Object>();
		

		try
		{
			ResultSetMetaData m = rs.getMetaData();
			int nCols = m.getColumnCount();
			ArrayList<String> columnNameList = getColumnNameList(nCols, m);
			
			for (int i = 1; i <= nCols; i++)
			{
				Object columnDescriptor = instanceHandler.newInstance(context);

				nameElementHandler.set(context, columnDescriptor, columnNameList.get(i - 1));
				typeElementHandler.set(context, columnDescriptor, SQLUtils.getSQLTypeName(m
						.getColumnType(i)));

				columnDescriptorList.add(columnDescriptor);
			}
		}
		catch (SQLException e)
		{
			throw new ModelExecutionException("Failed to get SQL output", "sql query:" + sql, e);
		}

		return columnDescriptorList;
	}

	private String getValidColumnName(String name, ArrayList<String> columnNameList)
	{
		String validColumnName = name;

		int inc = 1;
		while (columnNameList.contains(validColumnName)) // Already exists
		{
			validColumnName = name + " (" + ++inc + ")";
		}

		return validColumnName;
	}

}

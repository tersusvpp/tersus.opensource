package tersus.plugins.database;
/************************************************************************************************
 * Copyright (c) 2003-20059 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.model.TersusTableFieldDescriptor;
import tersus.runtime.ElementHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.SQLUtils;


/**
 * 
 * Returns the given table Columns 
 * 
 * @author Danny Shmueli
 */
public class GetColumns extends DatabaseAction
{
	// Triggers 
	private static final Role TABLE= Role.get("<Table>");
	private ElementHandler tableNameElementHandler;
	private ElementHandler tableSchemaElementHandler;
	private ElementHandler tableCatalogElementHandler;
	SlotHandler	 tableTrigger, notFoundExitHandler;
	
	// Exits
	private static final Role NAME = Role.get("Name");
    private static final Role TYPE = Role.get("Type");
    private static final Role SCHEMA = Role.get("Schema Name");
    private static final Role CATALOG = Role.get("Catalog Name");
    
    
    private ElementHandler columnNameElementHandler;
    private ElementHandler columnTypeElementHandler;
   
	private static final Role COLUMNS= Role.get("<Columns>");
    protected static final Role NOT_FOUND = Role.get("<None>");
	
	SlotHandler	outputColumnsExit;			// Required output (it doesn't make sense to use the plug-in without asking for this output)
				
	
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		
		tableTrigger = getRequiredTrigger(TABLE, Boolean.FALSE, BuiltinModels.TABLE_DESCRIPTOR);
		if (tableTrigger != null)
        {
            InstanceHandler tableDescriptorHandler = tableTrigger.getChildInstanceHandler();
            tableNameElementHandler = tableDescriptorHandler.getElementHandler(NAME);
            tableSchemaElementHandler = tableDescriptorHandler.getElementHandler(SCHEMA);
            tableCatalogElementHandler = tableDescriptorHandler.getElementHandler(CATALOG);
        }
		// Get exits (including validation of mandatory, repetitiveness and type)
		notFoundExitHandler = getNonRequiredExit(NOT_FOUND, Boolean.FALSE, BuiltinModels.NOTHING_ID);
		outputColumnsExit = getRequiredExit(COLUMNS, Boolean.TRUE, BuiltinModels.COLUMN_DESCIPTOR);
		if (outputColumnsExit != null)
        {
			InstanceHandler columnDescriptorHandler = outputColumnsExit.getChildInstanceHandler();
            columnNameElementHandler = columnDescriptorHandler.getElementHandler(NAME);
            columnTypeElementHandler = columnDescriptorHandler.getElementHandler(TYPE);   
        }
	}
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Connection connection = getConnection(context, flow);
		
		Object tableDescriptor =  tableTrigger.get(context, flow);
		String table = (String) tableNameElementHandler.get(context, tableDescriptor);
		String schema = (String) tableSchemaElementHandler.get(context, tableDescriptor);
		String catlog = (String) tableCatalogElementHandler.get(context, tableDescriptor);
		
		HashSet<TersusTableFieldDescriptor> columns = null;
		
		try
		{
			columns = SQLUtils.getColumnsParameters(connection,catlog,schema, table);
			if (columns.isEmpty())
				setExitValue(notFoundExitHandler, context, flow, Boolean.TRUE);
			else
				for (TersusTableFieldDescriptor column: columns)
		        {
					Object columnDescriptor = outputColumnsExit.getChildInstanceHandler().newInstance(context);
					columnNameElementHandler.set(context, columnDescriptor, column.getColumnName());
					columnTypeElementHandler.set(context, columnDescriptor, column.getColumnType());
					accumulateExitValue(outputColumnsExit, context, flow, columnDescriptor);
		        }
		}
		catch (SQLException e) {
				throw new EngineException("Error in getting SQL Schemas","", e);
		}
	}		
}
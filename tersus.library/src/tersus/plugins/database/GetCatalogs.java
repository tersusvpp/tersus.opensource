package tersus.plugins.database;
/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.SQLUtils;

/**
 * 
 * Returns the Database catalogs 
 *
 * 
 * @author Danny Shmueli
 */
public class GetCatalogs extends DatabaseAction
{
	private static final Role CATLOGS = Role.get("<Catalogs>");

	SlotHandler	resultsExit;
				

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		resultsExit = getRequiredExit(CATLOGS, Boolean.TRUE, BuiltinModels.TEXT_ID);
	}

	public void start(RuntimeContext context, FlowInstance flow)
	{
		Connection connection = getConnection(context, flow);
		
		ResultSet rs =  null;
        try {
        	rs = connection.getMetaData().getCatalogs();
			while (rs.next())
			{
				String catalogName = (String)rs.getObject(1);
				accumulateExitValue(resultsExit, context, flow, catalogName);
			}
			rs.close();
			rs = null;
		}
        catch (SQLException e) {
			throw new EngineException("Error in getting SQL Catalogs","", e);
			
		}	
        finally
        {
        	SQLUtils.forceClose(rs);
        }
	}
}

package tersus.plugins.database;

import java.sql.Connection;

import tersus.model.BuiltinModels;
import tersus.model.BuiltinProperties;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

public abstract class DatabaseAction extends Plugin
{

	protected static final Role NONE = Role.get("<None>");
	protected static final Role DATA_SOURCE = Role.get("<Data Source>");
    protected static final Role AUTO_COMMIT = Role.get("<Auto Commit>");

	protected SlotHandler noneExit;
	protected SlotHandler dataSourceTrigger, autoCommitTrigger;

	public DatabaseAction()
	{
		super();
	}

	protected Connection getConnection(RuntimeContext context, FlowInstance flow)
	{
	    String dataSourceName = null;
	    if (dataSourceTrigger != null)
	        dataSourceName = (String) dataSourceTrigger.get(context, flow);
	    if (dataSourceName == null)
	    {
	    	InstanceHandler recordHandler = getRecordHandler();
	    	if (recordHandler != null)
	    	{
	    		dataSourceName = (String)recordHandler.getModel().getProperty(BuiltinProperties.DATA_SOURCE);
	    	}
	    }
	    boolean autoCommit = getTriggerValue(autoCommitTrigger, context, flow, false);
	    
	    return context.getConnection(dataSourceName, autoCommit);
	}

	@Override
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
        noneExit = getNonRequiredExit(NONE, Boolean.FALSE,
                BuiltinModels.NOTHING_ID);
        dataSourceTrigger = getNonRequiredTrigger(DATA_SOURCE, Boolean.FALSE, BuiltinModels.TEXT_ID);
        autoCommitTrigger = getNonRequiredTrigger(AUTO_COMMIT, Boolean.FALSE, BuiltinModels.BOOLEAN_ID);
	}
	
	protected InstanceHandler getRecordHandler()
	{
		return null;
	}

}
/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.DataHandler;
import tersus.runtime.ElementHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Number;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * @author Liat Shiff
 */
public class SQLBatch extends QueryPlugin
{
	public static final Role COLUMN_DESCRIPTOR = Role.get("Column Descriptor");
	public static final Role ROWS = Role.get("Rows");

	private static Role SQL_TRIGGER_ROLE = Role.get("<SQL>");
	private static Role RESULT_SET_TRIGGER_ROLE = Role.get("<Result Set>");
	private static Role UPDATE_COUNT_ROLE = Role.get("<Update Count>");

	private SlotHandler sqlTrigger;
	private SlotHandler resultSetExit;
	private SlotHandler updateCountExit;

	private ElementHandler columnDescriptorListHandler;
	private ElementHandler rowsHandler;

	private ArrayList<Object> columnDescriptorList;

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		sqlTrigger = getRequiredTrigger(SQL_TRIGGER_ROLE, Boolean.FALSE, BuiltinModels.TEXT_ID);
		numberOfRecordsTrigger = getNonRequiredTrigger(NUMBER_OF_RECORDS, Boolean.FALSE,
				BuiltinModels.NUMBER_ID);

		updateCountExit = getNonRequiredExit(UPDATE_COUNT_ROLE, Boolean.TRUE,
				BuiltinModels.NUMBER_ID);
		resultSetExit = getRequiredExit(RESULT_SET_TRIGGER_ROLE, Boolean.TRUE,
				BuiltinModels.RESULT_SET);

		InstanceHandler descriptorHandler = resultSetExit.getChildInstanceHandler();
		columnDescriptorListHandler = descriptorHandler.getElementHandler(COLUMN_DESCRIPTOR);
		rowsHandler = descriptorHandler.getElementHandler(ROWS);

		columnDescriptorHandler = columnDescriptorListHandler.getChildInstanceHandler();

		setColumnDescriptorHandlerElements();
	}

	public void start(RuntimeContext context, FlowInstance flow)
	{
		String sql = (String) sqlTrigger.get(context, flow);
		Statement st = null;
		
		try
		{
			Connection connection = getConnection(context, flow);
			st = connection.createStatement();

			if (numberOfRecordsTrigger != null)
			{
				int numberOfRecords = getNumberOfRecords(context, flow);
				st.setMaxRows(numberOfRecords);
			}

			boolean isResultSet = context.executeBatch(st, sql, 0);
			ArrayList<Number> updateCountList = new ArrayList<Number>();
			
			while (true)
			{
				if (isResultSet)
				{
					ResultSet rs = st.getResultSet();
					getResultSet(rs, sql, context, flow);
					isResultSet = st.getMoreResults();
					continue;
				}
				else
				{
					int rowCount = st.getUpdateCount();
					if (rowCount >= 0)
					{ 
						// rowCount > 0 : this is an update count
						// rowCount == 0 : DDL command or 0 updates
						updateCountList.add(new Number(rowCount));
						isResultSet = st.getMoreResults();
						continue;
					}
					break; // there are no more results
				}
			}

			st.close();
			st = null;
			
			if (updateCountExit != null)
			{
				for (int i = 0; i < updateCountList.size(); i++)
				{
					accumulateExitValue(updateCountExit, context, flow, updateCountList.get(i));
				}
			}
		}
		catch (SQLException e)
		{
			throw new EngineException("Error in executing SQL batch", queryDetails(sql, Collections
					.emptyList(), e), e);
		}
		catch (ModelExecutionException e)
		{
			throw new ModelExecutionException("Failed to execute SQL batch: " + e.getMessage(),
					queryDetails(sql, null, null), e);

		}
		finally
		{
			if (st != null)
			{
				try
				{
					st.close();
					st = null;
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}

			}
		}
	}

	private boolean getResultSet(ResultSet rs, String sql, RuntimeContext context, FlowInstance flow)
	{
		Object resultSetHandler = resultSetExit.getChildInstanceHandler().newInstance(context);
		if (rs != null)
		{
			addColumnDescriptorList(rs, context, resultSetHandler, sql);
			addRowsMap(rs, sql, context, resultSetHandler);

			accumulateExitValue(resultSetExit, context, flow, resultSetHandler);
			return true;
		}
		else
			return false;
	}

	private void addColumnDescriptorList(ResultSet rs, RuntimeContext context,
			Object resultSetHendler, String sql)
	{
		columnDescriptorList = getColumnDescriptorList(rs, columnDescriptorHandler, context, sql);

		for (Object columnDescriptor : columnDescriptorList)
		{
			columnDescriptorListHandler.accumulate(context, resultSetHendler, columnDescriptor);
		}
	}

	private void addRowsMap(ResultSet rs, String sql, RuntimeContext context,
			Object resultSetHendler)
	{
		if (rs != null)
		{
			List<Object> rows = getResults(rs, (DataHandler) rowsHandler.getChildInstanceHandler(),
					sql, Collections.emptyList(), context);
			for (Object row : rows)
			{
				rowsHandler.accumulate(context, resultSetHendler, row);
			}
		}
	}

}
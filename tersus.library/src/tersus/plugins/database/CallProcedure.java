/************************************************************************************************
 * Copyright (c) 2003-2011 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.database;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.model.SlotType;
import tersus.runtime.CompositeDataHandler;
import tersus.runtime.DatabaseAdapter;
import tersus.runtime.EngineException;
import tersus.runtime.Field;
import tersus.runtime.FieldList;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.MapHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SQLUtils;
import tersus.runtime.SlotHandler;

/**
 * @author oferb
 * 
 */

public class CallProcedure extends QueryPlugin

{
	private static final String OUTPUT = "Output Parameter";
	private static Role SP_TRIGGER_ROLE = Role.get("<Procedure>");
	private static Role INPUT_PARAMETERS = Role.get("<Input Parameters>");
	SlotHandler spTrigger, parameterSlots[], inputParametersTrigger;
	SlotHandler resultSetExit, resultSetExits[];

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext,
	 * tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		// Create call string and accumulate input parameters
		String spName = (String) spTrigger.get(context, flow);

		StringBuffer call = new StringBuffer();
		List<Object> values = new ArrayList<Object>();
		List<InstanceHandler> types = new ArrayList<InstanceHandler>();

		// Prepare call, execute it and retrieve outputs
		CallableStatement preparedCall = null;
		ResultSet rs = null;
		String sql = "";
		try
		{
			final Connection conn = getConnection(context, flow);
			// Prepare call
			DatabaseAdapter adapter = context.getDBAdapter(conn);
			call.append("{call ");
			call.append(spName);

			boolean first = true;
			int inputParametersInStructure = 0;
			if (inputParametersTrigger != null)
			{
				Object inputParameters = inputParametersTrigger.get(context, flow);
				FieldList fields = ((CompositeDataHandler) inputParametersTrigger
						.getChildInstanceHandler()).getFields();
				inputParametersInStructure = fields.getFields().length;
				for (Field f : fields.getFields())
				{
					String parameterName = f.name;
					Object value = SQLUtils.handleNullsAndPadding(context, adapter, f.element,
							f.getValue(context, inputParameters));
					String clause = adapter.getStoredProcedureClause(parameterName);
					call.append((first ? " (" : ", ") + clause);
					value = ((LeafDataHandler) f.element.getChildInstanceHandler()).toSQL(value);
					types.add(f.element.getChildInstanceHandler());
					values.add(value);
					first = false;
				}
			}
			for (int i = 0; i < parameterSlots.length; i++)
			{
				SlotHandler slot = parameterSlots[i];
				String parameterName = slot.getRole().toString();
				Object value = SQLUtils.handleNullsAndPadding(context, adapter, slot,
						slot.get(context, flow));
				String clause = adapter.getStoredProcedureClause(parameterName);
				call.append((first ? " (" : ", ") + clause);

				if (slot.getType() == SlotType.TRIGGER)
				{
					value = ((LeafDataHandler) slot.getChildInstanceHandler()).toSQL(value);
					values.add(value);
				}
				else
					values.add(OUTPUT);
				types.add(slot.getChildInstanceHandler());
				first = false;
			}
			if (!first)
				call.append(")");

			call.append("}");
			sql = call.toString();
			preparedCall = conn.prepareCall(sql);
			for (int i = 0; i < values.size(); i++)
			{
				Object value = values.get(i);
				LeafDataHandler type = (LeafDataHandler) types.get(i);
				int sqlType = type.getSQLType();
				if (value == OUTPUT)
					preparedCall.registerOutParameter(i + 1,
							sqlType);
				else if (value != null)
					preparedCall.setObject(i + 1, value, sqlType);
				else
					preparedCall.setNull(i + 1, sqlType);
			}
			// Execute call
			boolean moreResultsExist = context.execute(preparedCall, sql, values, 0);

			// Retrieve result sets and update counts
			// (should be done before retrieving values of output parameters)
			int numberOfResults = 0;
			while (moreResultsExist || preparedCall.getUpdateCount() != -1)
			{
				rs = preparedCall.getResultSet();
				if (rs != null)
				{
					if (resultSetExit != null)
					{
						outputColumns(rs, sql, context, flow);
						numberOfResults += outputResults(rs, resultSetExit, sql, values, context,
								flow);
					}
					else
						// Multiple result set exits - using rs.getMetaData() to check which result
						// set corresponds to which exit
						numberOfResults += outputResults(rs, resultSetExits, sql, values, context, flow);

					rs.close();
					rs = null;
				}
				else
				{
					// Get get the update count, if we need it
				}

				moreResultsExist = preparedCall.getMoreResults(); // Supposed to also implicitly
																	// close the previously obtained
																	// ResultSet
			}
			
			
			

			// Retrieving values of output parameters

			int numberOfOutputs = 0;
			for (int i = 0; i < parameterSlots.length; i++)
			{
				SlotHandler exit = parameterSlots[i];
				if (exit.getType() == SlotType.EXIT)
				{
					LeafDataHandler h = (LeafDataHandler) exit.getChildInstanceHandler();
					Object value = h.fromSQL(preparedCall, inputParametersInStructure + i + 1);
					if (value != null)
					{
						setExitValue(exit, context, flow, value);
						++numberOfOutputs;
					}

				}
			}
			if (numberOfResults == 0 && numberOfOutputs == 0)
				chargeEmptyExit(context, flow, noneExit);
			// That's it
			preparedCall.close();
			preparedCall = null;
		}
		catch (SQLException e)
		{
			throw new EngineException("Error in executing SQL stored procedure", queryDetails(sql,
					values, e), e);
		}
		catch (ModelExecutionException e)
		{
			throw new ModelExecutionException("Failed to execute stored procedure: "
					+ e.getMessage(), queryDetails(sql, values, null), e);

		}

		finally
		{
			SQLUtils.forceClose(rs);
			SQLUtils.forceClose(preparedCall);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.InstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		spTrigger = getRequiredMandatoryTrigger(SP_TRIGGER_ROLE, Boolean.FALSE,
				BuiltinModels.TEXT_ID);
		inputParametersTrigger = getNonRequiredTrigger(INPUT_PARAMETERS, Boolean.FALSE, null);

		List<SlotHandler> parameterList = new ArrayList<SlotHandler>();
		if (inputParametersTrigger == null)
		{
			for (SlotHandler t : triggers)
				if (t != spTrigger && t != dataSourceTrigger && t != autoCommitTrigger)
				{
					checkDataType(t, Boolean.TRUE, true);
					if (!t.isEmpty())
						parameterList.add(t);
				}
		}
		// Find output parameter and result set exits
		ArrayList<SlotHandler> resultExitList = new ArrayList<SlotHandler>();
		if (exits != null)
		{
			for (int i = 0; i < exits.length; i++)
			{
				SlotHandler exit = exits[i];
				if (exit == outputColumnsExit || exit.isEmpty() || exit.getType() == SlotType.ERROR)
					continue;
				InstanceHandler exitType = exit.getChildInstanceHandler();
				if (exitType instanceof MapHandler || exitType instanceof CompositeDataHandler)
				{
					resultExitList.add(exit);
				}
				else
					parameterList.add(exit);
			}
			if (resultExitList.size() == 1)
				resultSetExit = resultExitList.get(0);
			else
				resultSetExits = resultExitList.toArray(new SlotHandler[resultExitList.size()]);
		}
		Collections.sort(parameterList, new Comparator<SlotHandler>()
		{

			public int compare(SlotHandler o1, SlotHandler o2)
			{
				String name1 = o1.getRole().toString();
				String name2 = o2.getRole().toString();
				return name1.compareTo(name2);
			}
		});

		parameterSlots = (SlotHandler[]) parameterList
				.toArray(new SlotHandler[parameterList.size()]);
	}
}

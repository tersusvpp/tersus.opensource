/************************************************************************************************
 * Copyright (c) 2003-2007 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.database;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.DatabaseException;
import tersus.runtime.Field;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SQLUtils;
import tersus.runtime.SlotHandler;
import tersus.util.Misc;

/**
 * /**
 * An atomic flow handler that deletes an instance of its specified data type from the corresponding database table.
 * 
 * @author Ofer Brandes
 *
 */
public class Delete extends RecordAction
{
	private static final Role DELETED_ROLE = Role.get("<Deleted>");
	private static final Role NOT_FOUND_ROLE = Role.get("<Not Found>");
    private SlotHandler notFoundExit;
	private SlotHandler deletedExit;


	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
        Object value = trigger.get(context, flow);
        DeleteRecordInvocation invocation = new DeleteRecordInvocation(this, context,
                flow, value);
        invocation.run();
//		Object value = trigger.get(context, flow);
//		if (persistenceHandler.delete(context, value))
//		{
//			if (deletedExit != null)
//				setExitValue(deletedExit, context, flow, value);
//		}
//		else
//		{
//			if (notFoundExit != null)
//				setExitValue(notFoundExit, context, flow, value);
//		}
	}
    class DeleteRecordInvocation extends RecordActionInvocation
    {
        DeleteRecordInvocation(RecordAction actionHandler, RuntimeContext context, FlowInstance flow,
                Object record)
        {
            super(actionHandler, context, flow, record);
        }

        void run()
        {

            try
            {
                delete();
            }
            catch (DatabaseException e)
            {
                fireError(context, flow, e);
            }

        }

        private void delete()
        {
            PreparedStatement stmt = null;
            Field[] keyFields = fields.getPrimaryKeyFields();
            if (Misc.ASSERTIONS)
                Misc.assertion(keyFields.length > 0);
            ArrayList<Object> values = new ArrayList<Object>(keyFields.length);
            String sql = getDeleteSQL();
            try
            {
                stmt = connection.prepareStatement(sql);

                int fieldPos = 1;
                for (int i = 0; i < keyFields.length; i++)
                {
                    Object fieldValue = keyFields[i].getValue(context, record);
                    values.add(fieldValue);
                    keyFields[i]
                            .setValueInStatement(stmt, fieldPos, fieldValue);
                    ++fieldPos;

                }
                int c = context.executeUpdate(stmt,sql,values,0);
                
                if (c == 1)
                {
                    if (deletedExit != null)
                        setExitValue(deletedExit, context, flow, record);
                }
                else if (c == 0)
                {
                    if (notFoundExit != null)
                        setExitValue(notFoundExit, context, flow, record);
                }
                else
                    throw new ModelExecutionException(
                            "Delete resulted in deleting multiple records in table "
                                    + tableName
                                    + ". Check primary key settings","SQL statement:"+sql);
            }
            catch (SQLException e)
            {
                throw new DatabaseException(context, sql, values,
                        dbAdapter.getExceptionType(e), e);
            }
            finally
            {
                SQLUtils.forceClose(stmt);
            }
        }

        private String getDeleteSQL()
        {
            String cacheKey = "DeleteSQL:" + tableName + ":"
                    + recordHandler.getModelId();
            String sql = (String) context.getDerivedMetadataFromCache(cacheKey);
            if (sql == null)
            {
                StringBuffer sqlBuffer = new StringBuffer();
                sqlBuffer.append("DELETE FROM ");
                sqlBuffer.append(tableName);
                sqlBuffer.append(" WHERE ");
                Field[] keyFields = fields.getPrimaryKeyFields();
                for (int i = 0; i < keyFields.length; i++)
                {
                    if (i > 0)
                        sqlBuffer.append(" AND ");
                    sqlBuffer.append(keyFields[i].getSQLName(useQuotedIdentifiers));
                    sqlBuffer.append("=?");

                }
                sql = sqlBuffer.toString();
                context.caceDerivedMetaData(cacheKey, sql);
            }
            return sql;
        }

    }

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

			
		deletedExit = getNonRequiredExit(DELETED_ROLE, Boolean.FALSE, null);
		notFoundExit = getNonRequiredExit(NOT_FOUND_ROLE, Boolean.FALSE, null);

		checkTypeMismatch(trigger, deletedExit);
		checkTypeMismatch(trigger, notFoundExit);
	}
}

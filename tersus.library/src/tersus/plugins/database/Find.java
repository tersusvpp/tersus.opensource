/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.database;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.model.validation.Problems;
import tersus.runtime.ColumnDescriptor;
import tersus.runtime.CompositeDataHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.PersistenceHandler;
import tersus.runtime.Query;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.Misc;

/**
 * An atomic flow handler that finds instances of the output data type
 * 
 * @author Youval Bronicki
 *  
 */

public class Find extends QueryPlugin
{
    protected SlotHandler orderByTrigger;

    protected static final Role ORDER_BY = Role.get("<Order By>");
    protected static final Role RECORDS = Role.get("<Records>");

    private ArrayList<ColumnDescriptor> queryColumns;

    private ArrayList<SlotHandler> queryTriggers;

    protected PersistenceHandler persistenceHandler;

    private ArrayList<InstanceHandler> parameterHandlers = null;

    private StringBuffer sqlBuffer = null;

    protected SlotHandler recordsExit;
    protected InstanceHandler recordHandler;

	private boolean useQuotedIdentifiers;



    //FUNC3 refactor to handle the case of multi-column references.
    //FUNC3 (?)implement dynamic output type for static queries
    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
        String orderBy = null;
        if (orderByTrigger != null)
        {
            orderBy = (String) orderByTrigger.get(context, flow);
            if (context.getContextPool().useQuotedIdentifiers())
            	orderBy=quoteOrderBy(orderBy);
        }

        List instances;
        try
        {
            instances = executeStaticSQLQuery(orderBy, context, flow);
            if (instances.size() == 0)
            {
                if (noneExit != null)
                    setExitValue(noneExit, context, flow,
                            Boolean.TRUE);
            }
            else
            {
                if (recordsExit != null)
                    for (int i = 0; i < instances.size(); i++)
                        accumulateExitValue(recordsExit, context, flow,
                                instances.get(i));
            }
        }
        catch (EngineException e)
        {
            if (e.getCause() instanceof SQLException)
            {
                fireError(context, flow, e);
            }
            else
                throw e;
        }
    }

    protected String quoteOrderBy(String orderBy)
	{
    	Pattern p = Pattern.compile("^ *(.+) *( (asc|desc)) *$",  Pattern.CASE_INSENSITIVE);
    	if (orderBy == null)
    		return orderBy;
    	StringBuffer res = new StringBuffer();
    	for (String part : orderBy.split(",") )
    	{
			if (res.length()>0)
				res.append(",");
    		if (part.trim().startsWith("\""))
    			res.append(part);
    		else
    		{
	    		Matcher m = p.matcher(part);
	    		String col = part.trim(), modifier=null;
	    		if (m.find())
	    		{
	    			col = m.group(1);
	    			modifier = m.group(2);
	    		}
				res.append('"');
				res.append(col);
				res.append('"');
				if (modifier != null)
					res.append(modifier);
    		}
    	}
    	return res.toString();
	}

	private List executeStaticSQLQuery(String orderBy, RuntimeContext context,
            FlowInstance flow)
    {
        List instances;
        ArrayList<Object> values = new ArrayList<Object>(queryTriggers.size());
        boolean hasNullValues = false;
        for (int i = 0; i < queryTriggers.size(); i++)
        {
            SlotHandler trigger = queryTriggers.get(i);
            Object value = trigger.get(context, flow);
            hasNullValues = hasNullValues || value == null;
            values.add(value);
        }
        if (hasNullValues)
        {
            instances = executeQueryWithNulls(orderBy, context, flow, values);
        }
        else
            instances = executeDefaultSQLQuery(orderBy, context, flow, values);
        return instances;
    }

    /**
     * Execute a specific query for the case of null values;
     * 
     * @param context
     * @param values
     * @return
     */
    private List executeQueryWithNulls(String orderBy, RuntimeContext context, FlowInstance flow, 
            ArrayList<Object> values)
    {
        StringBuffer condition = new StringBuffer();
        ArrayList<Object> nonNullValues = new ArrayList<Object>();
        ArrayList<InstanceHandler> nonNullTypes = new ArrayList<InstanceHandler>();
        for (int i = 0; i < queryTriggers.size(); i++)
        {
            SlotHandler trigger = queryTriggers.get(i);
            ColumnDescriptor column = queryColumns.get(i);
            Object value = values.get(i);
            if (i > 0)
                condition.append(" AND ");
            condition.append(column.getColumnName(useQuotedIdentifiers));
            if (value == null)
                condition.append(" is null");
            else
            {
                condition.append(" = ?");
                nonNullValues.add(value);
                nonNullTypes.add(trigger.getChildInstanceHandler());
            }

        }
        Query adHocQuery = new Query(persistenceHandler, condition.toString(),
                orderBy, nonNullTypes);
        return adHocQuery.execute(context, nonNullValues, getConnection(context, flow));
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
     */
    public void resume(RuntimeContext context, FlowInstance flow)
    {
        // Nothing to do (This is an action)
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
     */
    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
		useQuotedIdentifiers = getLoader().useQuotedIdentifiers();

        initRecordsExit();
        setRecordHandler(recordsExit);
        validateRecordsExit();
        initSQLQuery();
    }

    protected void setRecordHandler(SlotHandler slot)
    {
        if (slot !=  null)
        {
            recordHandler = slot.getChildInstanceHandler();
            if (recordHandler != null)
                persistenceHandler = recordHandler.getPeristenceHandler();
        }
    }

    public InstanceHandler getRecordHandler()
	{
		return recordHandler;
	}

	protected void initRecordsExit()
    {
        recordsExit = getRequiredExit(RECORDS, Boolean.TRUE, null);
    }

    protected void validateRecordsExit()
    {
        checkPersistenceType(recordsExit);
    }

    protected String getOutputTableName()
    {
        String tableName = null;
        if (recordHandler != null)
        {
            if (recordHandler instanceof CompositeDataHandler)
                tableName = ((CompositeDataHandler)recordHandler).tableName;
        }
        return useQuotedIdentifiers ? Misc.quote(tableName) : Misc.sqlize(tableName);
    }
    protected void initSQLQuery()
    {
        initOrderByTrigger();
        initDefaultSQLQuery(useQuotedIdentifiers);
    }

    /**
     *  
     */
    protected void initOrderByTrigger()
    {
        orderByTrigger = getNonRequiredTrigger(ORDER_BY, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
    }

    /**
     * Execute a default SQL query (that consists of an 'AND' of all triggers)
     *  
     */
    private List executeDefaultSQLQuery(String orderBy, RuntimeContext context, FlowInstance flow, 
            ArrayList<Object> values)
    {
        Query query = new Query(persistenceHandler, sqlBuffer.toString(),
                orderBy, parameterHandlers);
        return query.execute(context, values, getConnection(context, flow));
    }

    /**
     * Initializes a default SQL query (that consists of an 'AND' of all
     * triggers)
     *  
     */
    private void initDefaultSQLQuery(boolean useQuotedIdentifiers)
    {
        if (persistenceHandler == null)
            notifyInvalidModel(recordsExit.getRole(),
                    Problems.INVALID_EXIT,
                    "Exit type is not persistent (use a Database Record)");
        sqlBuffer = new StringBuffer();
        parameterHandlers = new ArrayList<InstanceHandler>();
        queryTriggers = new ArrayList<SlotHandler>();
        queryColumns = new ArrayList<ColumnDescriptor>();
        for (int i = 0; i < triggers.length; i++)
        {
            SlotHandler trigger = triggers[i];
            if (trigger == orderByTrigger || trigger == dataSourceTrigger)
                continue;
            if (BuiltinModels.NOTHING_ID.equals(trigger.getChildModelId()))
                continue;

            ColumnDescriptor column = persistenceHandler.getColumnDescriptor(trigger.getRole().toString());
            if (column == null)
            {
                notifyInvalidModel(trigger.getRole(), Problems.INVALID_TRIGGER, "No element '"+trigger.getRole()+"' in '"+recordsExit.getChildModelId()+"'");
                continue;
            }
            queryTriggers.add(trigger);
            queryColumns.add(column);
            if (parameterHandlers.size() > 0)
                sqlBuffer.append(" AND ");
            sqlBuffer.append(column.getColumnName(useQuotedIdentifiers));
            sqlBuffer.append("=?");
            parameterHandlers.add(column.getDataHandler());
            if (column.getDataHandler() != trigger.getChildInstanceHandler())
            {
                if (!(column.getDataHandler() instanceof LeafDataHandler && 
                		trigger.getChildInstanceHandler() != null &&
                		column.getDataHandler().getClass().equals(trigger.getChildInstanceHandler().getClass())))
                    notifyInvalidModel(trigger.getRole(),
                            Problems.INCONSISTENT_TYPES,
                            "Trigger type is incomaptible with column type");
            }
        }
    }
    //FUNC2 add model validation (when mechanism for plugin validation is
    // there)
}
/************************************************************************************************
 * Copyright (c) 2003-2007 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import tersus.InternalErrorException;
import tersus.model.Model;
import tersus.model.Path;
import tersus.model.Role;
import tersus.runtime.DatabaseAdapter;
import tersus.runtime.DatabaseException;
import tersus.runtime.EngineException;
import tersus.runtime.Field;
import tersus.runtime.FlowInstance;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SQLUtils;
import tersus.runtime.SlotHandler;
import tersus.runtime.TraceWriter;
import tersus.runtime.trace.EventType;
import tersus.util.Misc;

/**
 * 
 * An atomic flow handler that inserts an instance of its specified data type to the corresponding
 * database table.
 * 
 * @author Ofer Brandes
 * @author Youval Bronicki
 * 
 */
public class Insert extends RecordAction
{
	private static final Role INSERTED_ROLE = Role.get("<Inserted>");

	private static final Role DUPLICATE_ROLE = Role.get("<Duplicate>");

	private SlotHandler duplicateExit;

	private SlotHandler insertedExit;

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext,
	 * tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Object value = trigger.get(context, flow);
		// if (tableNameTrigger == null)
		// doInsertDefault(context, flow, value);
		// else
		{
			InsertRecordInvocation invocation = new InsertRecordInvocation(this, context, flow,
					value);
			invocation.run();
		}
	}

	private class InsertRecordInvocation extends RecordActionInvocation
	{
		InsertRecordInvocation(RecordAction handler, RuntimeContext context, FlowInstance flow,
				Object record)
		{
			super(handler, context, flow, record);
		}

		void run()
		{
			if (!dbAdapter.test(context))
				throw new InternalErrorException("Invalid Database Adapter");
			if (duplicateExit != null && !dbAdapter.canResumeAfterExceptions())
			{
				if (isDuplicateRecord())
				{
					setExitValue(duplicateExit, context, flow, record);
					return;
				}
			}
			try
			{
				insert();
			}
			catch (DatabaseException e)
			{
				if ((duplicateExit != null)
						&& (DatabaseAdapter.DUPLICATE_KEY.equals(e.getExceptionType())))
					setExitValue(duplicateExit, context, flow, record);
				else
					fireError(context, flow, e);
			}

		}

		private void insert()
		{
			PreparedStatement stmt = null;
			Field[] allFields = fields.getFields();
			ArrayList<Object> values = new ArrayList<Object>(allFields.length);
			String sql = getInsertSQL();
			ResultSet keys = null;
			try
			{
				if (fields.hasDatabaseGeneratedFields)
				{
					if (dbAdapter.useColumnNamesForDBGenereatedColumns())
						stmt = connection.prepareStatement(sql, useQuotedIdentifiers ? fields.quotedDbGeneratedColumns : fields.sqlizedDbGeneratedColumns);
					else
						stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				}
				else
					stmt = connection.prepareStatement(sql);

				for (int i = 0, c = 0; i < allFields.length; i++)
				{
					Field f = allFields[i];
					if (f.databaseGenerated)
						continue;
					Object v0 = f.getValue(context, record);
					Object v1 = SQLUtils.handleNullsAndPadding(context, dbAdapter, f.element, v0);
					values.add(v1);
					f.setValueInStatement(stmt, ++c, v1);
				}
				int c = context.executeUpdate(stmt, sql, values, 0);
				if (fields.hasDatabaseGeneratedFields)
				{
					keys = stmt.getGeneratedKeys();
					if (keys == null)
						throw new EngineException(
								"Failed to retrieve database generated keys in statemnet",
								"Statement:" + sql, null);
					int keyIndex = 1;
					if (keys.next())
					{
						for (Field f : allFields)
						{
							if (f.databaseGenerated)
							{
								try
								{
									int intValue = keys.getInt(keyIndex);
									tersus.runtime.Number value = new tersus.runtime.Number(
											intValue);
									f.setValue(context, record, value);
								}
								catch (SQLException e)
								{
									throw new EngineException(
											"Failed to retrieve database generated keys in statemnet",
											"Statement:" + sql, e);

								}
							}
						}
					}
					keys.close();
					keys = null;
				}
				if (Misc.ASSERTIONS)
					Misc.assertion(c == 1);
				if (insertedExit != null)
					setExitValue(insertedExit, context, flow, record);
			}
			catch (SQLException e)
			{
				if (context.trace.traceSQL)
				{
					context.trace.add(EventType.SQL, null, TraceWriter.RUN,
							"SQL Exection - error code:" + e.getErrorCode() + " error message: "
									+ e.getMessage(), Path.EMPTY);
				}
				throw new DatabaseException(context, sql, values, dbAdapter.getExceptionType(e), e);
			}
			finally
			{
				SQLUtils.forceClose(keys);
				SQLUtils.forceClose(stmt);
			}
		}

		private boolean isDuplicateRecord()
		{
			boolean duplicate = false;
			PreparedStatement stmt = null;
			ResultSet rs = null;
			Field[] keyColumns = fields.getPrimaryKeyFields();
			List<Object> keyValues = new ArrayList<Object>(keyColumns.length);
			String sql = getCheckDuplicatesSQL();
			try
			{
				stmt = connection.prepareStatement(sql);
				for (int i = 0; i < keyColumns.length; i++)
				{
					Field f = keyColumns[i];
					Object keyValue = f.getValue(context, record);
					keyValues.add(keyValue);
					f.setValueInStatement(stmt, i + 1, keyValue);
				}
				rs = context.executeQuery(stmt, sql, keyValues, 0);
				rs.next(); // SELECT count(*) exepcted to reurn 1 row
				int count = rs.getInt(1);
				duplicate = count > 0;
			}
			catch (SQLException e)
			{
				throw new EngineException("Failed to execute SQL Query ", sql + "values:"
						+ Misc.toString(keyValues), e);
			}
			finally
			{
				SQLUtils.forceClose(rs);
				SQLUtils.forceClose(stmt);
			}
			return duplicate;
		}

		private String getInsertSQL()
		{
			String cacheKey = "InsertSQL:" + tableName + ":" + modelId;
			String sql = (String) context.getDerivedMetadataFromCache(cacheKey);
			if (sql == null)
			{
				StringBuffer sqlBuffer = new StringBuffer();
				sqlBuffer.append("INSERT INTO ");
				sqlBuffer.append(tableName);
				sqlBuffer.append(" (");
				Field[] allFields = fields.getFields();
				for (int i = 0, c = 0; i < allFields.length; i++)
				{
					Field field = allFields[i];
					if (field.databaseGenerated)
						continue;
					if (c > 0)
						sqlBuffer.append(',');
					++c;
					sqlBuffer.append(field.getSQLName(useQuotedIdentifiers));
				}

				sqlBuffer.append(") VALUES (");
				for (int i = 0, c = 0; i < allFields.length; i++)
				{
					Field field = allFields[i];
					if (field.databaseGenerated)
						continue;
					if (c > 0)
						sqlBuffer.append(',');
					++c;
					sqlBuffer.append('?');

				}
				sqlBuffer.append(')');
				sql = sqlBuffer.toString();
				context.caceDerivedMetaData(cacheKey, sql);
			}
			return sql;
		}

		private String getCheckDuplicatesSQL()
		{
			String cacheKey = "CheckDuplicatesSQL:" + tableName + ":" + modelId;
			String sql = (String) context.getDerivedMetadataFromCache(cacheKey);
			if (sql == null)
			{
				StringBuffer sqlB = new StringBuffer("SELECT COUNT(*) FROM ");
				sqlB.append(tableName);
				sqlB.append(" WHERE ");
				Field[] pkFields = fields.getPrimaryKeyFields();
				for (int i = 0; i < pkFields.length; i++)
				{
					if (i > 0)
						sqlB.append(" AND ");
					Field field = pkFields[i];
					sqlB.append(field.getSQLName(useQuotedIdentifiers));
					sqlB.append(" = ?");

				}
				sql = sqlB.toString();
				context.caceDerivedMetaData(cacheKey, sql);

			}
			return sql;
		}
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		insertedExit = getNonRequiredExit(INSERTED_ROLE, Boolean.FALSE, null);
		duplicateExit = getNonRequiredExit(DUPLICATE_ROLE, Boolean.FALSE, null);

		checkTypeMismatch(trigger, insertedExit);
		checkTypeMismatch(trigger, duplicateExit);

	}

}

/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tersus.InternalErrorException;
import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.DatabaseAdapter;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Number;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SQLUtils;
import tersus.runtime.SlotHandler;
import tersus.util.Template;
import tersus.util.Template.Token;
/**
 * @author Youval Bronicki
 *
 */
public class DatabaseUpdate extends DatabaseAction
{
	private static Role SQL_TRIGGER_ROLE = Role.get("<SQL Statement>");
	SlotHandler sqlTrigger;
    private static final Role NUMBER_OF_RECORDS_EXIT_ROLE = Role.get("<Number of Records Affected>");
    private SlotHandler numberOfRecordsExit;
	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		String sqlTemplate = (String) sqlTrigger.get(context, flow);
		Template template = new Template(sqlTemplate);
		StringBuffer sql = new StringBuffer();
		List<Token> tokens = template.getTokens();
		List<Object> values = new ArrayList<Object>();
		List<InstanceHandler> types = new ArrayList<InstanceHandler>();
		for (int i = 0; i < tokens.size(); i++)
		{
			//FUNC3 handle null values - requires a big refactoring (including analysis of the query)
			Template.Token token = (Template.Token) tokens.get(i);
			if (token.getType() == Template.TEXT)
				sql.append(token.getValue());
			else
			{
				String roleStr = token.getValue();
				SlotHandler trigger = (SlotHandler) getElementHandler(roleStr);
				if (trigger == null)
					throw new ModelExecutionException(
						"SQL statement '"
							+ sqlTemplate
							+ "' contains a reference to a non existing trigger '"
							+ roleStr
							+ "' in "
							+ getModelId());
				Object value = trigger.get(context, flow);
				value = ((LeafDataHandler)trigger.getChildInstanceHandler()).toSQL(value);
				sql.append('?');
				values.add(value);
				types.add(trigger.getChildInstanceHandler());
			}
		}
		PreparedStatement stmt = null;
        Connection connection = null;
		try
		{
           connection = getConnection(context, flow);
           DatabaseAdapter dbAdapter = context.getDBAdapter(connection);
			if (!dbAdapter.test(context))
				throw new InternalErrorException("Invalid Database Adapter");

            String sqlString = sql.toString();
            stmt = connection.prepareStatement(sqlString);
			for (int i = 0; i < values.size(); i++)
			{
				Object value = values.get(i);
				int sqlType = ((LeafDataHandler)types.get(i)).getSQLType();
				if (value != null)
				stmt.setObject(i + 1, value, sqlType);
				else
				    stmt.setNull(i+1, sqlType);
				    
			}
			int numberOfRecordsAffected = context.executeUpdate(stmt, sqlString, values, 0);
			if (numberOfRecordsExit != null)
			    setExitValue(numberOfRecordsExit, context, flow, new Number(numberOfRecordsAffected));
			stmt.close();
			stmt = null;
		}
		catch (SQLException e)
		{
			throw new EngineException(
				"Error in SQL Update",
				QueryPlugin.queryDetails(sqlTemplate,values,e),
				e);
		}
		finally
		{
            SQLUtils.forceClose(stmt);
		}
	}
	/* (non-Javadoc)
	 * @see tersus.runtime.InstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		sqlTrigger = getRequiredMandatoryTrigger(SQL_TRIGGER_ROLE, Boolean.FALSE, BuiltinModels.TEXT_ID);
		numberOfRecordsExit = getNonRequiredExit(NUMBER_OF_RECORDS_EXIT_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		for (SlotHandler t: triggers)
			if (t!= sqlTrigger && t != dataSourceTrigger && t != autoCommitTrigger)
				checkDataType (t, Boolean.TRUE, true);
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.database;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.InstanceHandler;
import tersus.runtime.SlotHandler;

public class RecordAction extends DatabaseAction
{

    protected static final Role RECORD_ROLE = Role.get("<Record>");
    protected static final Role TABLE_NAME_ROLE = Role.get("<Table Name>");
    
    protected SlotHandler tableNameTrigger;
    protected SlotHandler trigger;
    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        trigger = getRequiredMandatoryTrigger(RECORD_ROLE, Boolean.FALSE, null);
        tableNameTrigger = getNonRequiredTrigger(TABLE_NAME_ROLE,
                Boolean.FALSE, BuiltinModels.TEXT_ID);

        if (tableNameTrigger == null && ! BuiltinModels.ANYTHING_ID.equals(trigger.getChildModelId()))
            checkPersistenceType(trigger);
    }
	@Override
	protected InstanceHandler getRecordHandler()
	{
		return trigger == null ? null : trigger.getChildInstanceHandler();
	}

}

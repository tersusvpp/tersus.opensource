/************************************************************************************************
 * Copyright (c) 2003-2007 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.database;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import tersus.InternalErrorException;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.DatabaseException;
import tersus.runtime.Field;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SQLUtils;
import tersus.runtime.SlotHandler;
import tersus.util.Misc;

/**
 * 
 * An atomic flow handler that updates an instance of its specified data type in
 * the corresponding database table.
 * 
 * @author Ofer Brandes
 * 
 */
public class Update extends RecordAction
{

    private static final Role UPDATED_ROLE = Role.get("<Updated>");

    private static final Role NOT_FOUND_ROLE = Role.get("<Not Found>");

    private SlotHandler notFoundExit;

    private SlotHandler updatedExit;


    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext,
     *      tersus.runtime.FlowInstance)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
        Object value = trigger.get(context, flow);
        UpdateRecordInvocation invocation = new UpdateRecordInvocation(this, context,
                flow, value);
        invocation.run();

        // if (persistenceHandler.update(context, value))
        // {
        // if (updatedExit != null)
        // setExitValue(updatedExit, context, flow, value);
        // }
        // else
        // {
        // if (notFoundExit != null)
        // setExitValue(notFoundExit, context, flow, value);
        // }
    }

    class UpdateRecordInvocation extends RecordActionInvocation
    {
        UpdateRecordInvocation(RecordAction actionHandler, RuntimeContext context, FlowInstance flow,
                Object record)
        {
            super(actionHandler, context, flow, record);
        }

        void run()
        {

            try
            {
                update();
            }
            catch (DatabaseException e)
            {
                fireError(context, flow, e);
            }

        }

        private void update()
        {
            PreparedStatement stmt = null;
            Field[] allFields = fields.getFields();
            Field[] keyFields = fields.getPrimaryKeyFields();
            if (Misc.ASSERTIONS)
                Misc.assertion(keyFields.length > 0);
            ArrayList<Object> values = new ArrayList<Object>(allFields.length);
            String sql = getUpdateSQL();
            try
            {
                stmt = connection.prepareStatement(sql);
    			if (!dbAdapter.test(context))
    				throw new InternalErrorException("Invalid Database Adapter");

                int fieldPos = 1;
                for (int i = 0; i < allFields.length; i++)
                {
                    if (!allFields[i].primaryKey)
                    {
                        Field field = allFields[i];
                        Object fieldValue = SQLUtils.handleNullsAndPadding(context, dbAdapter, field.element, field.getValue(context, record));
                        values.add(fieldValue);
                        allFields[i].setValueInStatement(stmt, fieldPos,
                                fieldValue);
                        ++fieldPos;
                    }
                }
                for (int i = 0; i < keyFields.length; i++)
                {
                    Field field = keyFields[i];
                    Object fieldValue = SQLUtils.handleNullsAndPadding(context, dbAdapter, field.element, field.getValue(context, record));
                    values.add(fieldValue);
                    keyFields[i]
                            .setValueInStatement(stmt, fieldPos, fieldValue);
                    ++fieldPos;

                }
                int c = context.executeUpdate(stmt, sql, values, 0);

                if (c == 1)
                {
                    if (updatedExit != null)
                        setExitValue(updatedExit, context, flow, record);
                }
                else if (c == 0)
                {
                    if (notFoundExit != null)
                        setExitValue(notFoundExit, context, flow, record);
                }
                else
                    throw new ModelExecutionException(
                            "Update resulted in updating multiple records in table "
                                    + tableName
                                    + ". Check primary key settings");
            }
            catch (SQLException e)
            {
                throw new DatabaseException(context, sql, values,
                        dbAdapter.getExceptionType(e), e);
            }
            finally
            {
                SQLUtils.forceClose(stmt);
            }
        }

        private String getUpdateSQL()
        {
            String cacheKey = "InsertSQL:" + tableName + ":"
                    + recordHandler.getModelId();
            String sql = (String) context.getDerivedMetadataFromCache(cacheKey);
            if (sql == null)
            {
                StringBuffer sqlBuffer = new StringBuffer();
                sqlBuffer.append("UPDATE ");
                sqlBuffer.append(tableName);
                sqlBuffer.append(" SET ");
                Field[] allFields = fields.getFields();
                Field[] keyFields = fields.getPrimaryKeyFields();
                int pos = 0;
                for (int i = 0; i < allFields.length; i++)
                {
                    if (!allFields[i].primaryKey)
                    {
                        if (pos > 0)
                            sqlBuffer.append(" , ");
                        sqlBuffer.append(allFields[i].getSQLName(useQuotedIdentifiers));
                        sqlBuffer.append("=?");
                        ++pos;
                    }
                }

                sqlBuffer.append(" WHERE ");
                for (int i = 0; i < keyFields.length; i++)
                {
                    if (i > 0)
                        sqlBuffer.append(" AND ");
                    sqlBuffer.append(keyFields[i].getSQLName(useQuotedIdentifiers));
                    sqlBuffer.append("=?");

                }
                sql = sqlBuffer.toString();
                context.caceDerivedMetaData(cacheKey, sql);
            }
            return sql;
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
     */
    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);

        updatedExit = getNonRequiredExit(UPDATED_ROLE, Boolean.FALSE, null);
        notFoundExit = getNonRequiredExit(NOT_FOUND_ROLE, Boolean.FALSE, null);

        checkTypeMismatch(trigger, updatedExit);
        checkTypeMismatch(trigger, notFoundExit);
    }
}

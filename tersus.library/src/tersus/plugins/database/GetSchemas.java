package tersus.plugins.database;
/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 

import java.sql.Connection;
import java.sql.SQLException;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.SQLUtils;


/**
 * 
 * Returns the Database schemas 
 *
 * 
 * @author Danny Shmueli
 */
public class GetSchemas extends DatabaseAction
{
	// Triggers 
	private static Role CATALOG = Role.get("<Catalog>");
	SlotHandler	catalogTrigger;

	// Exits
	private static final Role SCHEMAS = Role.get("<Schemas>");
	SlotHandler	resultsExit;
	
	public void initializeFromModel(Model model)
	{
		// Standard initializations for all flow handlers
		super.initializeFromModel(model);

		// Get triggers (including validation of mandatory, repetitiveness and type)

		catalogTrigger = getNonRequiredTrigger(CATALOG, Boolean.FALSE, BuiltinModels.TEXT_ID);

		// Get exits (including validation of mandatory, repetitiveness and type)
		resultsExit = getRequiredExit(SCHEMAS, Boolean.TRUE, BuiltinModels.TEXT_ID);
		resultsExit.getChildInstanceHandler();
	}

	public void start(RuntimeContext context, FlowInstance flow)
	{
		Connection connection = getConnection(context, flow);
		try
		{
			for (String schemaName : SQLUtils.getSchemaNameList(connection, getTriggerText(context, flow, catalogTrigger))) 
				accumulateExitValue(resultsExit, context, flow, schemaName);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
}
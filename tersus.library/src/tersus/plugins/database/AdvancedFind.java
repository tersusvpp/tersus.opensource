/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Path;
import tersus.model.Role;
import tersus.model.validation.Problems;
import tersus.runtime.CompositeDataHandler;
import tersus.runtime.DataHandler;
import tersus.runtime.DatabaseAdapter;
import tersus.runtime.EngineException;
import tersus.runtime.Field;
import tersus.runtime.FieldList;
import tersus.runtime.FlowInstance;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.MapField;
import tersus.runtime.MapHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Number;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SQLUtils;
import tersus.runtime.SlotHandler;
import tersus.runtime.TraceWriter;
import tersus.runtime.trace.EventType;
import tersus.util.Misc;
import tersus.util.Template;
import tersus.util.Template.Token;

/**
 * @author Youval Bronicki
 * 
 */
public class AdvancedFind extends Find
{
	protected static final Role FILTER = Role.get("<Filter>");

	private static final Role TABLE = Role.get("<Table>");

	private static final Role COLUMNS = Role.get("<Columns>");

	private static final Role FROM = Role.get("<From>");

	private static final Role TO = Role.get("<To>");

	private static final Role MATCH = Role.get("<Match>");

	private static final Role START_FROM = Role.get("<Start From>");

	private static final Role OFFSET = Role.get("<Offset>");

	private static final Role MORE = Role.get("<More>");

	private static final Role OPTIMIZATION = Role.get("<Optimization>");

	private static final Role NUMBERS_OF_RECORDS_RETURNED = Role
			.get("<Number of Records Returned>");

	private static final Role NUMBERS_OF_MATCHES = Role.get("<Number of Matches>");

	private static final Role BACKWARD = Role.get("<Backward>");

	private static final Role POSITION = Role.get("<Position>");

	private SlotHandler toTrigger;

	private SlotHandler fromTrigger;

	private SlotHandler matchTrigger;

	private SlotHandler tableTrigger;

	private SlotHandler columnsTrigger;

	private SlotHandler startFromTrigger;

	private SlotHandler backwardTrigger;

	protected SlotHandler filterTrigger;

	protected SlotHandler offsetTrigger;

	protected SlotHandler optimizationTrigger;

	private SlotHandler numberOfRecordsReturnedExit;
	private SlotHandler numberOfMatchesExit;
	private SlotHandler moreExit;
	private SlotHandler positionExit;

	private final static String NO_N_MATCHES = "No Number of Matches";
	private final static String NO_POSITION = "No Position";
	private final static String NO_RECORDS = "No Records";

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext,
	 * tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Invocation invocation = new Invocation(context, flow);
		invocation.run();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.plugins.database.Find#initSQLQuery()
	 */
	protected void initSQLQuery()
	{
		initFilterTrigger();
		initOrderByTrigger();
		fromTrigger = getNonRequiredTrigger(FROM, Boolean.FALSE, null);
		tableTrigger = getNonRequiredTrigger(TABLE, Boolean.FALSE, BuiltinModels.TEXT_ID);
		toTrigger = getNonRequiredTrigger(TO, Boolean.FALSE, null);
		matchTrigger = getNonRequiredTrigger(MATCH, Boolean.FALSE, null);
		startFromTrigger = getNonRequiredTrigger(START_FROM, Boolean.FALSE, null);
		backwardTrigger = getNonRequiredTrigger(BACKWARD, Boolean.FALSE, BuiltinModels.BOOLEAN_ID);

		numberOfRecordsTrigger = getNonRequiredTrigger(NUMBER_OF_RECORDS, Boolean.FALSE,
				BuiltinModels.NUMBER_ID);

		offsetTrigger = getNonRequiredTrigger(OFFSET, Boolean.FALSE, BuiltinModels.NUMBER_ID);

		optimizationTrigger = getNonRequiredTrigger(OPTIMIZATION, null, BuiltinModels.TEXT_ID);
		numberOfRecordsReturnedExit = getNonRequiredExit(NUMBERS_OF_RECORDS_RETURNED,
				Boolean.FALSE, BuiltinModels.NUMBER_ID);
		numberOfMatchesExit = getNonRequiredExit(NUMBERS_OF_MATCHES, Boolean.FALSE,
				BuiltinModels.NUMBER_ID);

		positionExit = getNonRequiredExit(POSITION, Boolean.FALSE, BuiltinModels.NUMBER_ID);

		moreExit = getNonRequiredExit(MORE, Boolean.FALSE, BuiltinModels.NOTHING_ID);

		setRecordHandler(fromTrigger);
		setRecordHandler(toTrigger);
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		columnsTrigger = getNonRequiredTrigger(COLUMNS, Boolean.FALSE, BuiltinModels.TEXT_ID);

		if (recordsExit == null && numberOfMatchesExit == null)
			notifyInvalidModel(Path.EMPTY, Problems.MISSING_TRIGGER,
					"Advanced Find must contain at least one of the following triggers " + RECORDS
							+ "," + NUMBERS_OF_MATCHES);
		if (numberOfRecordsTrigger == null && moreExit != null)
			notifyInvalidModel(MORE, Problems.INVALID_EXIT, "The exit " + MORE
					+ " is only relevant together with the trigger " + NUMBER_OF_RECORDS);
		if (recordsExit == null && numberOfRecordsReturnedExit != null)
			notifyInvalidModel(NUMBERS_OF_RECORDS_RETURNED, Problems.INVALID_EXIT, "The exit "
					+ NUMBERS_OF_RECORDS_RETURNED + " is only relevant together with the exit "
					+ RECORDS);
		if (recordsExit == null && noneExit != null)
			notifyInvalidModel(NONE, Problems.INVALID_EXIT, "The exit " + NONE
					+ " is only relevant together with the exit " + RECORDS);
		if (tableTrigger == null && recordsExit != null
				&& !recordsExit.getChildInstanceHandler().isPersistent())
			notifyInvalidModel(RECORDS, Problems.INVALID_TRIGGER, "In Advanced Find, " + RECORDS
					+ " needs to be a persistent type (unless " + TABLE + " is specified)");

		checkTypeConsistency(new SlotHandler[]
		{ recordsExit, fromTrigger, toTrigger, startFromTrigger, matchTrigger });

	}

	private void checkTypeConsistency(SlotHandler[] slotHandlers)
	{
		SlotHandler baseSlot = null;
		for (SlotHandler slot : slotHandlers)
		{
			if (slot == null)
				continue;
			if (slot.getChildInstanceHandler() == null)
				continue;
			if (baseSlot == null)
			{
				baseSlot = slot;
				continue;
			}
			if (!baseSlot.getChildModelId().equals(slot.getChildModelId()))
				notifyInvalidModel(slot.getRole(), Problems.INCONSISTENT_TYPES, "The "
						+ slot.getRole() + " " + slot.getType()
						+ " should have the same type as the " + baseSlot + " "
						+ baseSlot.getType());

		}

	}

	/**
     * 
     */
	protected void initFilterTrigger()
	{
		filterTrigger = getNonRequiredTrigger(FILTER, Boolean.FALSE, BuiltinModels.TEXT_ID);
	}

	/**
	 * We do the actual work in an inner class,because the handler is shared among many instances
	 * and threads.
	 * 
	 * @author Youval Bronicki
	 * 
	 */
	class Invocation
	{

		private RuntimeContext context;

		private FlowInstance flow;

		private ArrayList<Object> values;

		private ArrayList<LeafDataHandler> types;

		private StringBuffer condition;

		private String orderBy;

		private int queryTimeout = -1;

		private static final String DESC = "DESC";

		private HashMap<String, String> columnOrder;

		private ArrayList<Field> orderedColumns;

		private static final String ASC = "ASC";

		private StringBuffer table; /* SQL From Clause */

		private FieldList fields;

		private int offset;

		private Connection connection = null;

		private int more;

		private int numberOfRecords;

		private Object from,to,match,startFrom;
		String columnList;

		private HashSet<String> optimizations;

		private String explicitColumns, givenOrderBy;

		/**
		 * @param context
		 * @param flow
		 */
		
		
		boolean useQuotedIdentifiers; // useQuotedIdentifiers

		private DatabaseAdapter dbAdapter;
		protected Invocation(RuntimeContext context, FlowInstance flow)
		{
			this.context = context;
			this.flow = flow;
			values = new ArrayList<Object>();
			types = new ArrayList<LeafDataHandler>();
			condition = new StringBuffer();
			table = new StringBuffer();
			useQuotedIdentifiers = getLoader().useQuotedIdentifiers();


		}

		protected void run()
		{
			setExplicitColumns();
			setFrom();
			setTo();
			setMatch();
			setStartFrom();
			setGivenOrderBy();
			setFields();
			
			setOptimizations();
			boolean backward = getTriggerValue(backwardTrigger, context, flow, false);

			setOrderBy(backward);
			addTable();
			addDynamicFilterConditions();
			addRangeConditions();
			addMatchConditions();
			initConnection();
			initNumberOfRecords();
			initMore();
			int numberOfMatches = -1;
			if (numberOfMatchesExit != null && !optimizations.contains(NO_N_MATCHES) || backward
					&& positionExit != null && !optimizations.contains(NO_POSITION)
					&& startFrom == null)
			{
				// We need to know the number of matches in order to calculate the position when
				// searching backward with no "start from"
				numberOfMatches = countMatches(dbAdapter);
				setExitValue(numberOfMatchesExit, context, flow, new Number(numberOfMatches));
			}
			if (startFrom != null)
				addStartFromConditions();
			int numberOfRecords = 0;
			if (recordsExit != null && !optimizations.contains(NO_RECORDS))
				numberOfRecords = executeQuery(backward);

			if (positionExit != null && !optimizations.contains(NO_POSITION))
			{
				int position;

				if (backward)
				{
					if (startFrom != null)
						position = getStartFromPosition() + 1 - numberOfRecords - offset;
					else
						position = numberOfMatches - numberOfRecords - offset;
				}
				else
				{
					position = getStartFromPosition() + offset;
				}
				setExitValue(positionExit, context, flow, new Number(position));
			}

		}

		private void setGivenOrderBy()
		{
			givenOrderBy = getTriggerText(context, flow, orderByTrigger);
		}

		/**
		 * Calculates the position of the current search relative to the total result set (total =
		 * ignoring <Offset> and <Start From> )
		 * 
		 * @return
		 */
		protected int getStartFromPosition()
		{
			if (startFrom == null)
				return 0;
			else
			{
				condition.setLength(0);
				values.clear();
				types.clear();
				addDynamicFilterConditions();
				addRangeConditions();
				setOrderBy(true); // By reversing the order we end up with a count of all records
									// before our record, including our record
				addMatchConditions();
				addStartFromConditions();
				return countMatches(dbAdapter) - 1; // Assuming the start from record is found

			}
		}

		private void initMore()
		{
			more = numberOfRecords > 0 && moreExit != null ? 1 : 0;
			// If the <More> exit is present and we are querying a limited number of records, we
			// query one more - just to check if there's more
		}

		private void initConnection()
		{
			connection = getConnection(context, flow);
			dbAdapter = context.getDBAdapter(connection);

		}

		private void setFields()
		{
			if (recordHandler instanceof CompositeDataHandler)
				fields = ((CompositeDataHandler) recordHandler).getFields();
			else if (recordHandler instanceof MapHandler)
			{
				fields = new FieldList((MapHandler)recordHandler);
				fields.addMapFields((Map<String,Object>)from);
				fields.addMapFields((Map<String,Object>)to);
				fields.addMapFields((Map<String,Object>)match);
				fields.addMapFields((Map<String,Object>)startFrom);
				
				if (explicitColumns != null)
				{
					for (String name : explicitColumns.split("\\s*,\\s*"))
					{
						fields.addMapField(name);
					}
				}
				if (givenOrderBy != null)
				{
					for (String clause : givenOrderBy.split("\\s*,\\s*"))
					{
						String[] parts = clause.split("\\s+");
						if (parts.length > 0)
						{
							fields.addMapField(parts[0]);
						}
					}
					
				}
				fields.prepare();
			}
		}

		@SuppressWarnings("unchecked")
		private void setOptimizations()
		{
			optimizations = new HashSet<String>();
			if (optimizationTrigger != null)
			{
				if (optimizationTrigger.isRepetitive())
				{
					List<String> values = (List<String>) optimizationTrigger.get(context, flow);
					if (values != null)
						optimizations.addAll(values);
				}
				else
				{
					String value = (String) optimizationTrigger.get(context, flow);
					if (value != null)
						optimizations.add(value);

				}
			}
		}

		private void addTable()
		{
			String tableExpr = null;
			if (tableTrigger != null)
			{
				tableExpr = (String) tableTrigger.get(context, flow);
				if (tableExpr != null)
				{
					prepareSQLExpression(table, tableExpr);
				}
			}
			if (tableExpr == null)
			{
				tableExpr = getOutputTableName();
				table.append(tableExpr);
			}
			if (table == null)
			{
				throw new ModelExecutionException("No table name specified for query");
			}
		}

		/**
         * 
         */
		private void setOrderBy(boolean reverse)
		{
			if (recordsExit != null)
			{
				orderBy = null;
				columnOrder = new HashMap<String, String>();
				orderedColumns = new ArrayList<Field>();
				handleExplicitOrderBy(reverse);
				if (fields != null)
					addPrimaryKeyToSortOrder(reverse);
			}
		}

		private void handleExplicitOrderBy(boolean reverse)
		{
			StringBuffer reversed = null;
		
			orderBy = givenOrderBy;
			if (orderBy != null)
			{
				StringTokenizer tok = new StringTokenizer(orderBy, ",");
				while (tok.hasMoreTokens())
				{
					Field column;
					String clause = tok.nextToken().trim();
					String columnName;
					Boolean hasASC = true;
					Boolean hasDESC = false;
					Boolean hasModifier = false;
					try
					{
						StringBuffer columnNameBuffer = new StringBuffer();
						String[] clauseParts = clause.split(" ");
						if (clauseParts.length > 1)
						{
							hasASC = clauseParts[clauseParts.length-1].equalsIgnoreCase(ASC);
							hasDESC = clauseParts[clauseParts.length-1].equalsIgnoreCase(DESC);
							hasModifier = hasASC || hasDESC;
							columnNameBuffer.append(clauseParts[0]);
							for (int p=1 ; p < (clauseParts.length - (hasModifier ? 1 : 0)) ; p++)
							{
								columnNameBuffer.append(" ");
								columnNameBuffer.append(clauseParts[p]);
							}
						}
						else
							columnNameBuffer.append(clause);
						columnName = columnNameBuffer.toString();
					}
					catch (Exception e)
					{
						throw new ModelExecutionException("Invalid " + ORDER_BY + ": '" + orderBy
								+ "'");
					}
					if (useQuotedIdentifiers)
						columnName = unqouteColumnName(columnName);
					if (fields != null)
					{
						column = fields.getField(columnName, true);
						if (column == null)
							throw new ModelExecutionException("Invalid " + ORDER_BY + " ('" + orderBy
								+ "': unknown column '" + columnName + "')");
						orderedColumns.add(column);
					}
					else
					{
						column = new MapField(columnName);
						orderedColumns.add(column);
					}
					if (reverse)
					{
						if (reversed == null)
							reversed = new StringBuffer();
						else
							reversed.append(", ");
					}
					if (hasDESC)
					{
						if (reverse)
						{
							reversed.append(columnName);
							columnOrder.put(column.name, ASC);
						}
						else
							columnOrder.put(column.name, DESC);
					}
					else
					{
						if (reverse)
						{
							reversed.append(columnName);
							reversed.append(" " + DESC);
							columnOrder.put(column.name, DESC);
						}
						else
							columnOrder.put(column.name, ASC);
					}

				}
			}
			if (reverse)
				orderBy = reversed == null ? null : reversed.toString();
			if (useQuotedIdentifiers)
				orderBy = quoteOrderBy(orderBy);
		}

		private String unqouteColumnName(String columnName)
		{
	    	Pattern p = Pattern.compile("^\"(.*)\"$");
	    	Matcher m = p.matcher(columnName);
	    	String unqoutedColumnName=columnName;
	    	
	    	if (m.find())
	    		unqoutedColumnName = m.group(1);

    		return unqoutedColumnName;	
		}

		/**
         * 
         */
		private void addPrimaryKeyToSortOrder(boolean reverse)
		{
			Field[] pkColumns = fields.getPrimaryKeyFields();
			for (int i = 0; i < pkColumns.length; i++)
			{
				Field column = pkColumns[i];
				if (columnOrder.get(column.name) == null)
				{
					String columnSqlName = column.getSQLName(useQuotedIdentifiers);
					if (orderBy == null)
						orderBy = columnSqlName;
					else
						orderBy += ", " + columnSqlName;
					orderedColumns.add(column);
					if (reverse)
					{
						orderBy += " DESC";
						columnOrder.put(column.name, DESC);
					}
					else
					{
						columnOrder.put(column.name, ASC);
					}
				}
			}
		}

		/**
         */
		private int executeQuery(boolean reverse)
		{
			PreparedStatement stmt = null;
			ResultSet rs = null;
			String sql = "SQL not prepared";
			try
			{
				sql = prepareSelectStatementSQL(dbAdapter);
				stmt = connection.prepareStatement(sql.toString());
				for (int i = 0; i < values.size(); i++)
				{
					Object value = values.get(i);
					LeafDataHandler type = (LeafDataHandler) types.get(i);
					if (value != null)
						stmt.setObject(i + 1, type.toSQL(value), type.getSQLType());
					else
						stmt.setNull(i + 1, type.getSQLType());

				}
				rs = context.executeQuery(stmt, sql, values, queryTimeout);
				if (!dbAdapter.supportsQueryPaging() && offset > 0)
				{
					// For databases that do not support paging, we need to skip the first rows
					if (context.trace.traceSQL)
					{
						context.trace.add(EventType.SQL, null, TraceWriter.RUN, "Skipping "
								+ offset + " rows", Path.EMPTY);
					}
					for (int i = 0; i < offset; i++)
						if (!rs.next())
							break;
				}
				List<Object> results = getResults(rs, (DataHandler) recordsExit
						.getChildInstanceHandler(), sql, values, context);
				// extra - the number of extra records retrieved in order to check if there are more
				// records (0 or 1)
				int extra = numberOfRecords > 0 && results.size() > numberOfRecords ? 1 : 0;
				if (extra == 1)
					chargeEmptyExit(context, flow, moreExit);
				int numberOfRecordsReturned = results.size() - extra;
				if (numberOfRecordsReturnedExit != null)
					setExitValue(numberOfRecordsReturnedExit, context, flow, new Number(
							numberOfRecordsReturned));
				if (results.isEmpty())
				{
					if (noneExit != null)
						setExitValue(noneExit, context, flow, Boolean.TRUE);
				}
				else
				{
					if (recordsExit != null)
					{
						if (!reverse)
						{
							for (int i = 0; i < numberOfRecordsReturned; i++)
								accumulateExitValue(recordsExit, context, flow, results.get(i));
						}
						else
						{
							for (int i = results.size() - 1 - extra; i >= 0; i--)
								accumulateExitValue(recordsExit, context, flow, results.get(i));

						}
					}
				}
				outputColumns(rs, sql, context, flow);
				rs.close();
				stmt.close();
				return numberOfRecordsReturned;
			}
			catch (SQLException e)
			{
				throw new EngineException("Error in SQL Query", QueryPlugin.queryDetails(sql
						.toString(), values, e), e);
			}
			finally
			{
				SQLUtils.forceClose(rs);
				SQLUtils.forceClose(stmt);
			}

		}

		private int countMatches(DatabaseAdapter dbAdapter)
		{
			PreparedStatement stmt = null;
			ResultSet rs = null;
			String sql = "SQL not prepared";
			int count = -1;
			try
			{
				sql = dbAdapter.prepareCountStatement(table, condition, optimizations);
				stmt = connection.prepareStatement(sql.toString());
				for (int i = 0; i < values.size(); i++)
				{
					Object value = values.get(i);
					LeafDataHandler type = (LeafDataHandler) types.get(i);
					int sqlType = type.getSQLType();
					if (value != null)
						stmt.setObject(i + 1, type.toSQL(value), sqlType);
					else
						stmt.setNull(i + 1, sqlType);

				}
				rs = context.executeQuery(stmt, sql, values, queryTimeout);
				rs.next();
				count = rs.getInt(1);
				rs.close();
				stmt.close();
			}
			catch (SQLException e)
			{
				throw new EngineException("Error in SQL Query", QueryPlugin.queryDetails(sql
						.toString(), values, e), e);
			}
			finally
			{
				SQLUtils.forceClose(rs);
				SQLUtils.forceClose(stmt);
			}
			return count;
		}

		private String prepareSelectStatementSQL(DatabaseAdapter adapter)
		{
			StringBuffer columns = new StringBuffer();
			String primayKey = null;
			if (! (recordHandler instanceof MapHandler))
			{
				Field[] allFields = fields.getFields();
				for (int i = 0; i < allFields.length; i++)
				{
					if (i > 0)
						columns.append(',');
					columns.append(allFields[i].getSQLName(useQuotedIdentifiers));
				}

				primayKey = fields.getPrimaryKeyFields().length == 1 ? fields.getPrimaryKeyFields()[0]
						.getSQLName(useQuotedIdentifiers)
						: null;
			}
			else
			{
				setExplicitColumns();
				if (explicitColumns == null)
					columns.append('*');
				else
					columns.append(explicitColumns);
			}
			return adapter.prepareSelectStatement(columns, table, primayKey, condition, orderBy,
					numberOfRecords + more, offset, optimizations);
		}

		private void setExplicitColumns()
		{
			explicitColumns = getTriggerText(context, flow, columnsTrigger);
		}

		private void initNumberOfRecords()
		{
			numberOfRecords = getNumberOfRecords(context, flow);
			offset = getTriggerValue(offsetTrigger, context, flow, 0);
		}

		private void addStartFromConditions()
		{
			if (startFrom == null)
				return;
			StringBuffer startFromCondition = new StringBuffer();
			ArrayList<Object> previousTermsValues = new ArrayList<Object>();
			ArrayList<LeafDataHandler> previousTermsTypes = new ArrayList<LeafDataHandler>();
			int nTerms = 0;
			StringBuffer previousTermsClause = new StringBuffer();
			for (int i = 0; i < orderedColumns.size(); i++)
			{
				Field column = (Field) orderedColumns.get(i);
				String direction = (String) columnOrder.get(column.name);
				if (direction == null)
					continue;
				String columnSqlName = column.getSQLName(useQuotedIdentifiers);
				Object value = column.getValue(context, startFrom);
				boolean last = (i == orderedColumns.size() - 1);
				if (value == null)
				{
					if (last)
					{
						if (direction == DESC) // If the value of the last column is null, we only
												// care about it if it's sorted desc
						{
							if (previousTermsClause.length() > 0)
							{
								previousTermsClause.append(" AND ");
								previousTermsClause.append(columnSqlName);
								previousTermsClause.append(" IS NULL");
							}

						}
						if (startFromCondition.length() > 0 && previousTermsClause.length() > 0)
						{
							startFromCondition.append(" OR (");
							startFromCondition.append(previousTermsClause);
							startFromCondition.append(")");
						}
						else if (previousTermsClause.length() > 0)
						{
							startFromCondition.append(previousTermsClause);
						}
						values.addAll(previousTermsValues);
						types.addAll(previousTermsTypes);
						++nTerms;
					}
					else
					{
						if (direction == ASC)
						{
							if (startFromCondition.length() == 0)
							{
								startFromCondition.append("( ");
							}
							else
							{
								startFromCondition.append(" OR ( ");
							}
							if (previousTermsClause.length() > 0)
							{
								startFromCondition.append(previousTermsClause);
								startFromCondition.append(" AND ");
								values.addAll(previousTermsValues);
								types.addAll(previousTermsTypes);
							}
							startFromCondition.append(columnSqlName);
							startFromCondition.append(" IS NOT NULL )");
							++nTerms;
						}
						if (previousTermsClause.length() > 0)
						{
							previousTermsClause.append(" AND ");
						}
						previousTermsClause.append(columnSqlName);
						previousTermsClause.append(" IS NULL");

					}
				}
				else
				{
					if (startFromCondition.length() > 0 && previousTermsClause.length() > 0)
					{
						startFromCondition.append(" OR (");
						startFromCondition.append(previousTermsClause);
						startFromCondition.append(" AND (");
						values.addAll(previousTermsValues);
						types.addAll(previousTermsTypes);
					}
					else if (previousTermsClause.length() > 0) // No previous condition
					{
						startFromCondition.append(previousTermsClause);
						startFromCondition.append(" AND ((");
						values.addAll(previousTermsValues);
						types.addAll(previousTermsTypes);
					}
					else if (startFromCondition.length() == 0)
						startFromCondition.append("(( ");
					
					startFromCondition.append(columnSqlName);

					values.add(value);
					types.add(column.getValueHandler());
					if (last)
					{
						if (direction == ASC)
							startFromCondition.append(" >= ? ))");
						else
						{
							startFromCondition.append(" <= ? ");
							// we need to explicitly include null values
							appendOrIsNull(startFromCondition, columnSqlName);
							startFromCondition.append(")");
						}
					}
					else
					{
						if (direction == ASC)
							startFromCondition.append(" > ? ))");
						else
						{
							startFromCondition.append(" < ?");
							// we need to explicitly include null values
							appendOrIsNull(startFromCondition, columnSqlName);
							startFromCondition.append(")");
						}

						if (previousTermsClause.length() > 0)
							previousTermsClause.append(" AND ");
						previousTermsValues.add(value);
						previousTermsClause.append(columnSqlName);
						previousTermsClause.append(" = ?");
						previousTermsTypes.add(column.getValueHandler());
					}
					++nTerms;
				}
			}
			if (nTerms == 0)
				return;
			if (condition.length() == 0)
				condition = startFromCondition;
			else
			{
				condition.append(" AND ");
				if (nTerms == 1)
					condition.append(startFromCondition);
				else if (nTerms > 1)
				{
					condition.append(" (");
					condition.append(startFromCondition);
					condition.append(" )");
				}
			}

		}

		private void setFrom()
		{
			from = null;
			if (fromTrigger != null)
				from = fromTrigger.get(context, flow);
		}
		private void setTo()
		{
			to = null;
			if (toTrigger != null)
				to = toTrigger.get(context, flow);
		}
		private void setMatch()
		{
			match = null;
			if (matchTrigger != null)
				match = matchTrigger.get(context, flow);
		}
		private void setStartFrom()
		{
			startFrom = null;
			if (startFromTrigger != null)
				startFrom = startFromTrigger.get(context, flow);
		}

		private void appendOrIsNull(StringBuffer startFromCondition, String columnName)
		{
			startFromCondition.append(" OR ");
			startFromCondition.append(columnName);
			startFromCondition.append(" IS NULL )");
		}

		private void addRangeConditions()
		{
			if (recordHandler != null)
			{
				Object from = null;
				Object to = null;
				if (fromTrigger != null)
					from = fromTrigger.get(context, flow);
				if (toTrigger != null)
					to = toTrigger.get(context, flow);
				if (from == null && to == null)
					return;
				Field[] columns = fields.getFields();
				for (int i = 0; i < columns.length; i++)
				{
					Field column = columns[i];
					Object fromValue = null;
					Object toValue = null;
					if (from != null)
						fromValue = column.getValue(context, from);
					if (to != null)
						toValue = column.getValue(context, to);

					if (fromValue == null && toValue != null)
					{
						if (condition.length() > 0)
							condition.append(" AND ");

						condition.append("(");
						addCondition(column, "<=", toValue);
						appendOrIsNull(condition, column.getSQLName(useQuotedIdentifiers));
					}
					else
					{
						if (fromValue != null)
							addCondition(column, ">=", fromValue);
						if (toValue != null)
							addCondition(column, "<=", toValue);
					}
				}
			}
		}

		private void addMatchConditions()
		{
			if (recordHandler == null || matchTrigger == null)
				return;
			Object match = matchTrigger.get(context, flow);
			if (match == null)
				return;

			Field[] columns = fields.getFields();
			for (int i = 0; i < columns.length; i++)
			{
				Field column = columns[i];
				Object matchValue = column.getValue(context, match);

				if (matchValue != null)
				{
					boolean isPattern = matchValue instanceof String
							&& ((String) matchValue).contains("%");
					String op = isPattern ? " like " : " = ";
					addCondition(column, op, matchValue);
				}
			}
		}

		private void addCondition(Field column, String operator, Object value)
		{
			if (condition.length() > 0 && condition.charAt(condition.length() - 1) != '(')
			{
				condition.append(" AND ");
			}
			condition.append(column.getSQLName(useQuotedIdentifiers));
			condition.append(operator);
			condition.append("?");
			types.add(column.getValueHandler());
			values.add(value);

		}

		/**
         * 
         */
		private void addDynamicFilterConditions()
		{
			if (filterTrigger == null)
				return;
			String filter = (String) filterTrigger.get(context, flow);
			if (filter != null && filter.length() > 0)
			{
				condition.append("(");
				Template template = new Template(filter);
				for (Token token : template.getTokens())
				{
					if (token.getType() == Template.TEXT)
						condition.append(token.getValue());
					else
					{
						String roleStr = token.getValue();
						SlotHandler trigger = (SlotHandler) getElementHandler(roleStr);
						if (trigger == null)
							throw new EngineException("Error in query filter",
									"<Filter> contains ${" + roleStr
											+ "}, but there is no trigger named '" + roleStr + "'",
									null);
						Object value = trigger.get(context, flow);
						condition.append('?');
						values.add(value);
						types.add((LeafDataHandler) trigger.getChildInstanceHandler());
					}

				}
				condition.append(") ");
			}

		}

		/**
		 * Processes a "template" expression (with ${parameters} clauses), and appends the resulting
		 * SQL expression (with ? ) to 'sqlExpression'
		 */
		public void prepareSQLExpression(StringBuffer sqlExpression, String templateExpression)
		{
			Template template = new Template(templateExpression);
			for (Token token : template.getTokens())
			{
				// FUNC3 handle null values - requires a big refactoring
				// (including analysis of the query)
				if (token.getType() == Template.TEXT)
					sqlExpression.append(token.getValue());
				else
				{
					String roleStr = token.getValue();
					SlotHandler trigger = (SlotHandler) getElementHandler(roleStr);
					if (trigger == null)
						throw new ModelExecutionException("Error in SQL conversion",
								"Input contains ${" + roleStr
										+ "}, but there is no trigger named '" + roleStr + "'",
								null);
					Object value = trigger.get(context, flow);
					sqlExpression.append('?');
					values.add(value);
					types.add((LeafDataHandler) trigger.getChildInstanceHandler());
				}

			}
		}
	}

	protected void initRecordsExit()
	{
		recordsExit = getNonRequiredExit(RECORDS, Boolean.TRUE, null);
	}

	public void validateRecordsExit()
	{
	}

}

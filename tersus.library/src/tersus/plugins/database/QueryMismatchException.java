package tersus.plugins.database;

import tersus.runtime.ModelExecutionException;

public class QueryMismatchException extends ModelExecutionException
{

    private static final long serialVersionUID = 1L;

    public QueryMismatchException(String message)
    {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public QueryMismatchException(String message, String details)
    {
        super(message, details);
        // TODO Auto-generated constructor stub
    }

    public QueryMismatchException(String message, String details, Throwable e)
    {
        super(message, details, e);
        // TODO Auto-generated constructor stub
    }

}

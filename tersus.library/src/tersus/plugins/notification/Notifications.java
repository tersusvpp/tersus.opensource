package tersus.plugins.notification;

import tersus.model.ModelId;
import tersus.model.Role;

public abstract class Notifications
{

    public static final ModelId MESSAGE_MODEL_ID = new ModelId("Common/Templates/Notification/Message");
    public static final Role TIMESTAMP = Role.get("Timestamp");
    public static final Role CHANNEL = Role.get("Channel");
    public static final Role TYPE = Role.get("Type");
    public static final Role CONTENT = Role.get("Content");
	public static final Role SOURCE = Role.get("Source");
       

}
package tersus.plugins.notification;

import java.util.Date;
import java.util.List;

import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.webapp.NotificationServer;

public class Publish extends Plugin
{
    public static Role MESSAGE = Role.get("<Message>");
    private SlotHandler messageTrigger;
    private ElementHandler timestampElement;
    private ElementHandler channelElement;
	private ElementHandler sourceElement;
    private ElementHandler typeElement;
    private ElementHandler contentElement;

    @SuppressWarnings("unchecked")
    @Override
    public void start(RuntimeContext context, FlowInstance flow)
    {
        NotificationServer server = NotificationServer.getInstance(context.getContextPool());

        Object value = messageTrigger.get(context, flow);
        if (value == null)
            return;
        if (messageTrigger.isRepetitive())
        {
            for (Object notification : (List<Object>) value)
                send(context, server, notification);
        }
        else
            send(context, server, value);

    }

    private void send(RuntimeContext context, NotificationServer server, Object notification)
    {
        Date timestamp = (Date) timestampElement.get(context, notification);
        String channel = (String) channelElement.get(context, notification);
        String source = (String)sourceElement.get(context, notification);
        String type = (String) typeElement.get(context, notification);
        String content = (String) contentElement.get(context, notification);
        server.send(timestamp, channel, source, type, content, null);
    }

    @Override
    public void initializeFromModel(Model model)
    {

        super.initializeFromModel(model);
        messageTrigger = getRequiredMandatoryTrigger(MESSAGE, null, Notifications.MESSAGE_MODEL_ID);
        timestampElement = messageTrigger.getChildInstanceHandler().getElementHandler(Notifications.TIMESTAMP);
        channelElement = messageTrigger.getChildInstanceHandler().getElementHandler(Notifications.CHANNEL);
        sourceElement = messageTrigger.getChildInstanceHandler().getElementHandler(Notifications.SOURCE);
        typeElement = messageTrigger.getChildInstanceHandler().getElementHandler(Notifications.TYPE);
        contentElement = messageTrigger.getChildInstanceHandler().getElementHandler(Notifications.CONTENT);

    }

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.math;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that subtracts two numbers
 * 
 * @author Ofer Brandes
 *
 */
public class Subtract extends Plugin
{
	private static final Role X_ROLE = Role.get("<X>");
	private static final Role Y_ROLE = Role.get("<Y>");
	private static final Role DIFF_ROLE = Role.get("<Difference>");

	SlotHandler triggerX, triggerY;
	SlotHandler exit;
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Number x = (Number) triggerX.get(context, flow);
		Number y = (Number) triggerY.get(context, flow);

		double difference = x.value - y.value;
		Number ratioObj = new Number(difference); 
		setExitValue(exit, context, flow, ratioObj);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		triggerX = getRequiredMandatoryTrigger(X_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		triggerY = getRequiredMandatoryTrigger(Y_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);

		exit = getRequiredExit(DIFF_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
	}
}

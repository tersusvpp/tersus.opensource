/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.math;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that checks whether two numbers are equal with tolerance.
 * 
 * @see NumberUtil.essentiallyEqual
 * 
 * @author Ofer Brandes
 *
 */
public class ApproximatelyEqual extends Plugin
{
	private static final Role X_ROLE = Role.get("<X>");
	private static final Role Y_ROLE = Role.get("<Y>");
	private static final Role TOLERANCE_ROLE = Role.get("<Tolerance>");
	private static final Role EQUAL_ROLE = Role.get("<Yes>");
	private static final Role DIFFERENT_ROLE = Role.get("<No>");

	SlotHandler input1, input2, tolerance;
	SlotHandler equalExit, differentExit;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		double in1 = ((Number) input1.get(context,flow)).value;
		double in2 = ((Number) input2.get(context,flow)).value;
		double delta = ((Number) tolerance.get(context,flow)).value;

		boolean approximatelyEqual = (in1 >= in2-delta) && (in1 <= in2+delta);
		
		SlotHandler exit = (approximatelyEqual ? equalExit : differentExit);
		if (exit != null)
			setExitValue(exit, context, flow, input1.get(context,flow));
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		
		input1 = getRequiredMandatoryTrigger(X_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		input2 = getRequiredMandatoryTrigger(Y_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		tolerance = getRequiredMandatoryTrigger(TOLERANCE_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		
		equalExit = getNonRequiredExit(EQUAL_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		differentExit = getNonRequiredExit(DIFFERENT_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
	}
}

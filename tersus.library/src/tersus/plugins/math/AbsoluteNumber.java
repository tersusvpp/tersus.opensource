package tersus.plugins.math;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * @author Liat Shiff
 */
public class AbsoluteNumber extends Plugin
{
	private static final Role X_ROLE = Role.get("<X>");
	private static final Role ABSOLUTE_VALUE_ROLE = Role.get("<Absolute Value>");
	
	SlotHandler triggerX;
	SlotHandler absoluteValueExit;
	
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Number x = (Number) triggerX.get(context, flow);

		double absoluteValue = Math.abs(x.value);
		Number absoluteValueObj = new Number(absoluteValue); 
		setExitValue(absoluteValueExit, context, flow, absoluteValueObj);
	}
	
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		triggerX = getRequiredMandatoryTrigger(X_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		absoluteValueExit = getRequiredExit(ABSOLUTE_VALUE_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.math;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that checks whether a number is equal to zero
 * 
 * @author Ofer Brandes
 *
 */
public class IsZero extends Plugin
{
	private static final Role X_ROLE = Role.get("<X>");
	private static final Role YES_ROLE = Role.get("<Yes>");
	private static final Role NO_ROLE = Role.get("<No>");

	SlotHandler trigger;
	SlotHandler yesExit, noExit;

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Number x = (Number) trigger.get(context, flow);

		SlotHandler exit = (x.value == 0) ? yesExit : noExit;
		if (exit != null)
			setExitValue(exit, context, flow, x);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		trigger = getRequiredMandatoryTrigger(X_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);

		yesExit = getNonRequiredExit(YES_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		noExit = getNonRequiredExit(NO_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
	}
}

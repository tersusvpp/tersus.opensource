package tersus.plugins.math;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

public class SquareRoot extends Plugin
{
	private static final Role X_ROLE = Role.get("<X>");
	private static final Role SQAURE_ROOT_ROLE = Role.get("<Square Root>");
	
	SlotHandler triggerX;
	SlotHandler exit;
	
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Number x = (Number) triggerX.get(context, flow);

		double squareRoot = Math.sqrt(x.value);
		Number squareRootObj = new Number(squareRoot); 
		setExitValue(exit, context, flow, squareRootObj);
	}
	
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}
	
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		triggerX = getRequiredMandatoryTrigger(X_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		exit = getRequiredExit(SQAURE_ROOT_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
	}
}

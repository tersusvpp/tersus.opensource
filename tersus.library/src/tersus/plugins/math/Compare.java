/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.math;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that compares two numbers.
 * 
 * @author Ofer Brandes
 *
 */
public class Compare extends Plugin
{
	private static final Role X_ROLE = Role.get("<X>");
	private static final Role Y_ROLE = Role.get("<Y>");
	private static final Role SMALLER_ROLE = Role.get("<X<Y>");
	private static final Role EQUAL_ROLE = Role.get("<X=Y>");
	private static final Role BIGGER_ROLE = Role.get("<X>Y>");

	SlotHandler inputX, inputY;
	SlotHandler firstSmallerExit, equalExit, firstBiggerExit;

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Number x = (Number) inputX.get(context, flow);
		Number y = (Number) inputY.get(context, flow);

		SlotHandler exit = (x.value == y.value ? equalExit : (x.value < y.value ? firstSmallerExit : firstBiggerExit));
		if (exit != null)
			setExitValue(exit, context, flow, x);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		inputX = getRequiredMandatoryTrigger(X_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		inputY = getRequiredMandatoryTrigger(Y_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);

		firstSmallerExit = getNonRequiredExit(SMALLER_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		equalExit = getNonRequiredExit(EQUAL_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		firstBiggerExit = getNonRequiredExit(BIGGER_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
	}
}

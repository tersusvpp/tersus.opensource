/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.math;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * @author Youval Bronicki
 *
 */
public class ConvertTextToNumber extends Plugin
{
    static final Role TEXT = Role.get("<Text>");
    static final Role NUMBER = Role.get("<Number>");
    static final Role NOT_A_NUMBER = Role.get("<Not a Number>");
    private SlotHandler textTrigger;
    private SlotHandler numberExit;
    private SlotHandler nanExit;
    
    /* (non-Javadoc)
     * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
        String text = (String)textTrigger.get(context, flow);
        try
        {
            numberExit.create(context, flow, text, false);
        }
        catch (NumberFormatException e)
        {
            if (nanExit !=  null)
                setExitValue(nanExit, context, flow, text);
            else
                throw e;
        }
    }

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        textTrigger = getRequiredMandatoryTrigger(TEXT, Boolean.FALSE, BuiltinModels.TEXT_ID);
        numberExit = getRequiredExit(NUMBER, Boolean.FALSE, BuiltinModels.NUMBER_ID);
        nanExit = getNonRequiredExit(NOT_A_NUMBER, Boolean.FALSE, BuiltinModels.TEXT_ID);
    }
}

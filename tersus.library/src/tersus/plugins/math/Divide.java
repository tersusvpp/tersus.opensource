/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.math;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that divides X by Y
 * 
 * @author Youval Bronicki
 *
 */
public class Divide extends Plugin
{
	private static final Role X = Role.get("<X>");
	private static final Role Y = Role.get("<Y>");
	private static final Role RATIO = Role.get("<Ratio>");
    private static final Role QUOTIENT = Role.get("<Quotient>");
    private static final Role REMAINDER = Role.get("<Remainder>");

	SlotHandler triggerX, triggerY;
	SlotHandler ratioExit, quotientExit, remainderExit;
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Number x = (Number) triggerX.get(context, flow);
		Number y = (Number) triggerY.get(context, flow);

		if (y.value == 0)	
			fireError(context, flow, new EngineException("Cannot divide by zero",null,null));

        if (ratioExit != null)
        {
            double ratio = x.value / y.value;
            Number ratioObj = new Number(ratio); 
            setExitValue(ratioExit, context, flow, ratioObj);
        }
        if (quotientExit != null || remainderExit != null)
        {
            long x_int = (long)x.value;
            long y_int = (long)y.value;
            if (quotientExit != null)
            {
                setExitValue(quotientExit, context, flow, new Number((long) (x.value/y.value))); // Updated by Ofer, 20/11/12, to match the client-side way of handling non-integers
            }
            if (remainderExit != null)
            {
                setExitValue(remainderExit, context, flow, new Number(x.value % y.value)); // Updated by Ofer, 20/11/12, to match the client-side way of handling non-integers
            }
            
        }
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		triggerX = getRequiredMandatoryTrigger(X, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		triggerY = getRequiredMandatoryTrigger(Y, Boolean.FALSE, BuiltinModels.NUMBER_ID);

		ratioExit = getNonRequiredExit(RATIO, Boolean.FALSE, BuiltinModels.NUMBER_ID);
        quotientExit = getNonRequiredExit(QUOTIENT, Boolean.FALSE, BuiltinModels.NUMBER_ID);
        remainderExit = getNonRequiredExit(REMAINDER, Boolean.FALSE, BuiltinModels.NUMBER_ID);
	}
}

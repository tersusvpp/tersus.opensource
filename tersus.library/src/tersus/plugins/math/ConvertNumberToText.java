/************************************************************************************************
 * Copyright (c) 2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.math;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Map;

import tersus.InternalErrorException;
import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.plugins.notification.Notifications;
import tersus.runtime.ContextPool;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * A plugin that formats a number value using a format string
 * 
 * @author Youval Bronicki
 * 
 */
public class ConvertNumberToText extends Plugin
{
    private static final Role NUMBER = Role.get("<Number>");
    private static final Role FORMAT = Role.get("<Format>");
    private static final Role TEXT = Role.get("<Text>");
    private static final String RQ_NUMBER = "Number";
    private static final String RQ_FORMAT = "Format";

    SlotHandler valueTrigger, formatTrigger;
    SlotHandler textExit;

    public String handlePluginRequest(ContextPool pool,
            Map<String, String> request)
    {
        String numberStr = request.get(RQ_NUMBER);
        String formatStr = request.get(RQ_FORMAT);
        double number = Double.parseDouble(numberStr);
        DecimalFormat format = new DecimalFormat(formatStr);
        return format.format(number);
    }

    public void start(RuntimeContext context, FlowInstance flow)
    {
        tersus.runtime.Number valueObj = (tersus.runtime.Number) valueTrigger
                .get(context, flow);
        NumberFormat format = null;
        if (valueObj != null)
        {
            double value = valueObj.value;
            String formatStr = getTriggerText(context, flow, formatTrigger);
            String text;
            if (formatStr != null)
            {
                format = context.getNumberFormat(formatStr);
                text = format.format(value);
            }
            else
            {
                if (value == Math.round(value))
                    text = String.valueOf((long)value);
                else
                    text = String.valueOf(value);
            }
            setExitValue(textExit, context, flow, text);
        }
    }

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);

        valueTrigger = getRequiredMandatoryTrigger(NUMBER, Boolean.FALSE,
                BuiltinModels.NUMBER_ID);
        formatTrigger = getNonRequiredTrigger(FORMAT, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        textExit = getRequiredExit(TEXT, Boolean.FALSE, BuiltinModels.TEXT_ID);
    }
}

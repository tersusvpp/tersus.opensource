/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.math;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.NumberHandler;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that multiplies all input values
 * 
 * @author Ofer Brandes
 *
 */
public class Multiply extends Plugin
{
	private static final Role OUTPUT_ROLE = Role.get("<Product>");

	SlotHandler exit;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		double result = 1.0;
		TriggerIterator iterator = new TriggerIterator(context,flow,null);
		while (iterator.hasNext()) {
			Number input = (Number) iterator.next();
			result *= input.value;
		}

		Number out = new Number(result);
		setExitValue(exit, context, flow, out);
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		exit = getRequiredExit(OUTPUT_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);

		for (int i=0; i<triggers.length; i++)
		{
			if (!(triggers[i].getChildInstanceHandler() instanceof NumberHandler))
				notifyInvalidModel(triggers[i].getRole(), "Invalid Trigger", "Trigger data type '"+triggers[i].getChildModelId()+"' is not a number");
		}
	}
}

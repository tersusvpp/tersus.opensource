/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.math;

import java.security.SecureRandom;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

public class RandomNumber extends Plugin
{

    private static final Role FROM = Role.get("<From>");
    private static final Role TO = Role.get("<To>");
    private static final Role REAL = Role.get("<Real>");
    private static final Role INTEGER = Role.get("<Integer>");
    private SlotHandler fromTrigger;
    private SlotHandler toTrigger;
    private SlotHandler integerExit;
    private SlotHandler realExit;

    public void start(RuntimeContext context, FlowInstance flow)
    {
        double from = getTriggerDoubleValue(context, flow, fromTrigger,0);
        double to = getTriggerDoubleValue(context, flow, toTrigger,1);
        SecureRandom r = (SecureRandom)context.getProperty(SecureRandom.class.getName());
        if (r == null)
        {
        	r= new SecureRandom();
        	context.setProperty(SecureRandom.class.getName(), r);
        }
        double rand =r.nextDouble();
        if (realExit != null)
        {
            Number realValue = new Number(from+rand*(to-from));
            setExitValue(realExit, context, flow, realValue);
        }
        if (integerExit != null)
        {
            Number integerValue = new Number(Math.floor(from + rand*(to+1-from)));
            setExitValue(integerExit, context, flow, integerValue);
        }
    }

    private double getTriggerDoubleValue(RuntimeContext context, FlowInstance flow, SlotHandler trigger, double defaultValue)
    {
        if (trigger==null)
            return defaultValue;
        Number n = (Number)trigger.get(context, flow);
        if (n == null)
            return defaultValue;
        else
            return n.value;
    }

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        fromTrigger = getNonRequiredTrigger(FROM, Boolean.FALSE, BuiltinModels.NUMBER_ID);
        toTrigger = getNonRequiredTrigger(TO, Boolean.FALSE, BuiltinModels.NUMBER_ID);
        integerExit = getNonRequiredExit(INTEGER, Boolean.FALSE, BuiltinModels.NUMBER_ID);
        realExit = getNonRequiredExit(REAL, Boolean.FALSE, BuiltinModels.NUMBER_ID);
    }
    

}

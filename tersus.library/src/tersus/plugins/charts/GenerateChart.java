/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.charts;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import tersus.model.Role;
import tersus.runtime.BinaryValue;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.sapi.CompositeValue;
import tersus.util.ColorUtil;
import tersus.util.InvalidInputException;

/**
 * @author Youval Bronicki
 * 
 */
public class GenerateChart extends Plugin
{

    /**
     * 
     */

    public void start(RuntimeContext context, FlowInstance flow)
    {
        CompositeValue root = (CompositeValue) context.getSapiManager()
                .fromInstance(flow.getHandler(), flow);
        String type = (String) root.get("<Chart Type>");
        String format = (String) root.get("<Output Format>");
        String title = (String) root.get("<Title>");
        int width = ((Double) root.get("<Width>")).intValue();
        int height = ((Double) root.get("<Height>")).intValue();
        CompositeValue rawDataSet = (CompositeValue) root.get("<Data Set>");
        CompositeValue options = (CompositeValue) root.get("<Options>");
        JFreeChart chart;
        Boolean addLegendObj = options == null ? null: (Boolean)options.get("<Add Legend>");
        String backgroundColor = options == null ? null: (String)options.get("<Background Color>");
        String outerBackgroundColor = options == null ? null: (String)options.get("<Outer Background Color>");
        boolean addLegend = addLegendObj == null? false: addLegendObj.booleanValue();
        if (type.equals("PIE_CHART"))
        {
            PieDataset dataset = createPieDataSet(rawDataSet);
            chart = ChartFactory.createPieChart(title, dataset, addLegend, // legend?
                    false, // tooltips?
                    false // URLs?
                    );
            PiePlot piePlot = (PiePlot) chart.getPlot();
            setColors(piePlot, rawDataSet);
			if (options != null)
            {
                String labelFormat = (String) options.get("<Label Format>");
                StandardPieSectionLabelGenerator labelGenerator = null;
                if (labelFormat != null)
                    labelGenerator = new StandardPieSectionLabelGenerator(
                            labelFormat);
                else
                    labelGenerator = new StandardPieSectionLabelGenerator();
                piePlot.setLabelGenerator(labelGenerator);
            }
            piePlot.setLabelBackgroundPaint(null);
            piePlot.setLabelShadowPaint(null);
            piePlot.setLabelOutlinePaint(null);
            piePlot.setLabelOutlineStroke(null);
            piePlot.setShadowPaint(null);

        }
        else if (type.equals("BAR_CHART"))
        {
            CompositeValue[] series = (CompositeValue[])options.get("<Series>");
            HashMap seriesMap = new HashMap();
            if (series != null)
            {
                for (int order=0;order<series.length; order++)
                {
                    String name = (String)series[order].get("<Name>");
                    String caption = (String)series[order].get("<Caption>");
                    String colorStr = (String)series[order].get("<Color>");
                    Color color = ColorUtil.getColor(colorStr);
                    seriesMap.put(name, new SeriesSpec(name, order, color, caption));
                }
            }
            CategoryDataset dataset = createCategoryDataset(rawDataSet, seriesMap);
            String categoryAxisLabel = null;
            String valueAxisLabel = null;
            String orientationStr = (String)options.get("<Orientation>");
            PlotOrientation orientation = null;
            if (orientationStr != null)
            {
                if (orientationStr.equalsIgnoreCase("Horizontal"))
                    orientation = PlotOrientation.HORIZONTAL;
                else if (orientationStr.equalsIgnoreCase("Vertical"))
                    orientation = PlotOrientation.VERTICAL;
                else
                    throw new InvalidInputException("Unrecognized orientation '"+orientationStr+"'");
            }
            else
                orientation = PlotOrientation.VERTICAL;
            chart = ChartFactory.createBarChart(title, categoryAxisLabel, valueAxisLabel, dataset, orientation, addLegend, false, false);
            CategoryPlot plot = (CategoryPlot)chart.getPlot();
            if (seriesMap != null)
            {
                BarRenderer  renderer = (BarRenderer)plot.getRenderer();
                for (Iterator i = seriesMap.values().iterator(); i.hasNext();)
                {
                    SeriesSpec s = (SeriesSpec)i.next();
                    Color color = s.getColor();
                    if (color != null)
                    renderer.setSeriesPaint(s.getOrder(), color);
                }
            }
            ArrayList list = new ArrayList();
            list.addAll(seriesMap.values());
            Collections.sort(list);
            
        }
        else if (type.equals("CATEGORY_LINE_CHART"))
        {
            CompositeValue[] series = (CompositeValue[])options.get("<Series>");
            HashMap seriesMap = new HashMap();
            if (series != null)
            {
                for (int order=0;order<series.length; order++)
                {
                    String name = (String)series[order].get("<Name>");
                    String caption = (String)series[order].get("<Caption>");
                    String colorStr = (String)series[order].get("<Color>");
                    Color color = ColorUtil.getColor(colorStr);
                    seriesMap.put(name, new SeriesSpec(name, order, color, caption));
                }
            }
            CategoryDataset dataset = createCategoryDataset(rawDataSet, seriesMap);
            String categoryAxisLabel = null;
            String valueAxisLabel = null;
            String orientationStr = (String)options.get("<Orientation>");
            PlotOrientation orientation = null;
            if (orientationStr != null)
            {
                if (orientationStr.equalsIgnoreCase("Horizontal"))
                    orientation = PlotOrientation.HORIZONTAL;
                else if (orientationStr.equalsIgnoreCase("Vertical"))
                    orientation = PlotOrientation.VERTICAL;
                else
                    throw new InvalidInputException("Unrecognized orientation '"+orientationStr+"'");
            }
            else
                orientation = PlotOrientation.VERTICAL;
            chart = ChartFactory.createLineChart(title, categoryAxisLabel, valueAxisLabel, dataset, orientation, addLegend, false, false);
            CategoryPlot plot = (CategoryPlot)chart.getPlot();
            if (seriesMap != null)
            {
                 CategoryItemRenderer renderer = plot.getRenderer();
                for (Iterator i = seriesMap.values().iterator(); i.hasNext();)
                {
                    SeriesSpec s = (SeriesSpec)i.next();
                    Color color = s.getColor();
                    if (color != null)
                    renderer.setSeriesPaint(s.getOrder(), color);
                }
            }
            ArrayList list = new ArrayList();
            list.addAll(seriesMap.values());
            Collections.sort(list);
            
        }
        else if (type.equals("LINE_CHART"))
        {
            CompositeValue[] rawSeries = (CompositeValue[])rawDataSet.get("<Series>");
            Color[] colors = new Color[rawSeries.length];
            XYSeriesCollection dataset = new XYSeriesCollection();
            if (rawSeries != null)
            {
                for (int seriesIndex=0;seriesIndex<rawSeries.length; seriesIndex++)
                {
                    String label = (String)rawSeries[seriesIndex].get("<Label>");
                    if (label == null)
                        label = "Series "+(seriesIndex+1);
                    String colorStr = (String)rawSeries[seriesIndex].get("<Color>");
                    if (colorStr != null)
                    {
                        Color color = ColorUtil.getColor(colorStr);
                        colors[seriesIndex] = color;
                    }
                    XYSeries series = new XYSeries(label);
                    CompositeValue[] entries = (CompositeValue[]) rawSeries[seriesIndex]
                    .get("<Entries>");
                    if (entries != null)
                    {
                        for (int i = 0; i < entries.length; i++)
                        {
                            CompositeValue entry = entries[i];
                            Double x  = (Double) entry.get("<X>");
                            Double y = (Double) entry.get("<Y>");
                            series.add(x, y);
                         }
                    }
                    dataset.addSeries(series);
                }
            }
            
            String xAxisLabel = (String)options.get("<X Axis Label>");
            String yAxisLabel = (String)options.get("<Y Axis Label>");
            String orientationStr = (String)options.get("<Orientation>");
            PlotOrientation orientation = null;
            if (orientationStr != null)
            {
                if (orientationStr.equalsIgnoreCase("Horizontal"))
                    orientation = PlotOrientation.HORIZONTAL;
                else if (orientationStr.equalsIgnoreCase("Vertical"))
                    orientation = PlotOrientation.VERTICAL;
                else
                    throw new InvalidInputException("Unrecognized orientation '"+orientationStr+"'");
            }
            else
                orientation = PlotOrientation.VERTICAL;
            chart = ChartFactory.createXYLineChart(title, xAxisLabel, yAxisLabel, dataset, orientation, addLegend, false, false);
            XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer)((XYPlot)chart.getPlot()).getRenderer();
            for ( int seriesIndex = 0; seriesIndex< colors.length; seriesIndex++)
            {
                Color color = colors[seriesIndex];
                if (color != null)
                {
                    renderer.setSeriesOutlinePaint(seriesIndex, color);
                    renderer.setSeriesFillPaint(seriesIndex, color);
                    renderer.setSeriesPaint(seriesIndex, color);
                }
            }
        }
        else
            throw new ModelExecutionException("Unknown chart type " + type);
        
        if (backgroundColor != null)
        	chart.getPlot().setBackgroundPaint(ColorUtil.getColor(backgroundColor));
        if (outerBackgroundColor != null)
        	chart.setBackgroundPaint(ColorUtil.getColor(outerBackgroundColor));
        	
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        if (format.equals("PNG"))
        {
            try
            {
                //ChartUtilities.writeChartAsPNG(out, chart, width, height);
                ChartUtilities.writeChartAsPNG(out, chart, width, height, null, true,9);
            }
            catch (IOException e)
            {
                throw new ModelExecutionException(
                        "Failed to format chart as PNG", e.getMessage(), e);
            }
        }
        else if (format.equals("JPEG"))
        {
            try
            {
                ChartUtilities.writeChartAsJPEG(out, chart, width, height);
            }
            catch (IOException e)
            {
                throw new ModelExecutionException(
                        "Failed to format chart as JPEG", e.getMessage(), e);
            }
        }
        setExitValue("<Output>", context, flow, new BinaryValue(out
                .toByteArray()));
    }

    private void setExitValue(String exitName, RuntimeContext context,
            FlowInstance flow, BinaryValue value)
    {
        SlotHandler exit = getExit(Role.get(exitName));
        if (exit != null)
            setExitValue(exit, context, flow, value);
    }

    private void setColors(PiePlot piePlot, CompositeValue rawDataSet)
	{
		CompositeValue[] entries = (CompositeValue[]) rawDataSet.get("<Entries>");
		if (entries != null)
		{
			for (int i = 0; i < entries.length; i++)
			{
				CompositeValue entry = entries[i];
				String color = (String) entry.get("<Color>");
				if (color != null)
					piePlot.setSectionPaint(i, ColorUtil.getColor(color));
			}
		}
	}
    private PieDataset createPieDataSet(CompositeValue rawDataSet)
    {
        DefaultPieDataset dataset = new DefaultPieDataset();
        CompositeValue[] entries = (CompositeValue[]) rawDataSet
                .get("<Entries>");
        if (entries != null)
        {
            for (int i = 0; i < entries.length; i++)
            {
                CompositeValue entry = entries[i];
                String label = (String) entry.get("<Label>");
                Double value = (Double) entry.get("<Value>");
                dataset.setValue(label, value);
            }
        }
        return dataset;
    }
    private CategoryDataset createCategoryDataset(CompositeValue rawDataSet, final HashMap seriesMap)
    {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        CompositeValue[] entries = (CompositeValue[]) rawDataSet
                .get("<Entries>");
        if (seriesMap != null)
        Arrays.sort(entries, new Comparator(){

            public int compare(Object o1, Object o2)
            {
                CompositeValue e1 = (CompositeValue)o1;
                CompositeValue e2 = (CompositeValue)o2;
                String seriesName1 = (String) e1.get("<Series>");
                String seriesName2 = (String) e2.get("<Series>");
                SeriesSpec s1 = (SeriesSpec)seriesMap.get(seriesName1);
                SeriesSpec s2 = (SeriesSpec)seriesMap.get(seriesName2);
                if (s1 != null && s2 != null)
                    return s1.getOrder() - s2.getOrder();
                else
                    return 0;
            }});
        if (entries != null)
        {
            for (int i = 0; i < entries.length; i++)
            {
                CompositeValue entry = entries[i];
                String label = (String) entry.get("<Label>");
                Double value = (Double) entry.get("<Value>");
                String seriesName = (String) entry.get("<Series>");
                Comparable seriesObj = null;
                if (seriesName == null)
                    seriesObj = "Default";
                else if (seriesMap!= null)
                {
                    seriesObj = (SeriesSpec)seriesMap.get(seriesName);
                    if (seriesObj==null)
                        throw new InvalidInputException("Series '"+seriesName+"' missing in <Series> specification");
                }
                dataset.addValue(value,seriesObj, label);
            }
        }
        return dataset;
    }
}

class SeriesSpec implements Comparable
{
    private String name;
    private Color color;
    private String caption;
    private int order;
    public SeriesSpec(String name, int order, Color color, String caption)
    {
        super();
        this.name = name;
        this.order = order;
        this.color = color;
        this.caption = caption;
    }

    public int compareTo(Object o)
    {
        return order-((SeriesSpec)o).order;
    }
    
    public String toString()
    {
        return caption!= null?caption:name;
    }

    public Color getColor()
    {
        return color;
    }

    public int getOrder()
    {
        return order;
    }
    
}

/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.images;

import tersus.model.ModelId;
import tersus.model.Role;

/**
 * @author Liat Shiff
 */
public class TagStructure
{
	public static final ModelId TAG_ID = new ModelId("Common/Templates/Images/Shared/Tag");
	
	public static final Role TAG_NAME = Role.get("Name");
    public static final Role TAG_VALUE = Role.get("Value");
    public static final Role DIRECTORY_NAME = Role.get("Directory");
}

package tersus.plugins.images;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.RenderedImage;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Hashtable;

import javax.imageio.ImageIO;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.BinaryValue;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

import com.lowagie.text.Document;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfWriter;
import com.sun.media.jai.codec.ByteArraySeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.SeekableStream;
import com.sun.media.jai.codec.TIFFDecodeParam;

public class ConvertImage extends Plugin
{
	public static final Role IMAGE = Role.get("<Image>");
	public static final Role CONVERTED_IMAGE = Role.get("<Converted Image>");
	public static final Role FORMAT = Role.get("<Format>");
	private SlotHandler imageTrigger;
	private SlotHandler formatTrigger;
	private SlotHandler convertedImageExit;

	/**
	 * @param args
	 */
	@Override
	public void start(RuntimeContext context, FlowInstance flow)
	{
		try
		{
			boolean isTIFF = false;
			BinaryValue in = (BinaryValue) imageTrigger.get(context, flow);
			RenderedImage img = ImageIO.read(new ByteArrayInputStream(in.toByteArray()));
			String format = getTriggerText(context, flow, formatTrigger).toLowerCase();
			if (img != null)
				output(context, flow, in, img, format);
			else
			{
				// Try to decode as TIFF (depends on JAI)
				try
				{
					SeekableStream s = null;
					TIFFDecodeParam param = null;
					s = new ByteArraySeekableStream(in.toByteArray());
					ImageDecoder dec = ImageCodec.createImageDecoder("tiff", s, param);
					int numofpages = dec.getNumPages();
					for (int i = 0; i < numofpages; i++)
					{
						img = dec.decodeAsRenderedImage(i);
						isTIFF = true;
						output(context, flow, in, img, format);
					}
				}
				catch (NoClassDefFoundError e)
				{
					throw new ModelExecutionException(
							"Unrecognized Image Format (Install JAI for TIFF support)");
				}
			}
		}
		catch (IOException e)
		{
			throw new ModelExecutionException("Failed to convert image", null, e);
		}
	}

	private void output(RuntimeContext context, FlowInstance flow, BinaryValue imageBinary,
			RenderedImage img, String format) throws IOException
	{

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		if (format.equals("pdf"))
		{
			Image img1 = null;
			byte[] bytes = imageBinary.toByteArray();
			boolean isJPEG = bytes.length>4 && bytes[0]==(byte)0xff  && bytes[1]==(byte)0xd8 & bytes[2]==(byte)0xff && bytes[3]==(byte)0xe0;
			boolean isPNG = bytes.length>4 && bytes[0]==(byte)0xff  && bytes[1]==(byte)0x89 & bytes[2]==(byte)0x4e && bytes[3]==(byte)0x47;
			if (isJPEG || isPNG)
			{
				try
				{
					img1 = Image.getInstance(imageBinary.toByteArray());
				}
				catch (Exception e)
				{
					img1 = null;
				}
			}
			try
			{
				if (img1 == null)
				{
					BufferedImage bi = null;
					int width = img.getWidth();
					int height = img.getHeight();
					if (img instanceof BufferedImage)
						bi = (BufferedImage) img;
					else
					{
						ColorModel cm = img.getColorModel();
						WritableRaster raster = cm.createCompatibleWritableRaster(width, height);
						boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
						Hashtable<String, Object> properties = new Hashtable<String, Object>();
						String[] keys = img.getPropertyNames();
						if (keys != null)
						{
							for (int i = 0; i < keys.length; i++)
							{
								properties.put(keys[i], img.getProperty(keys[i]));
							}
						}
						bi = new BufferedImage(cm, raster, isAlphaPremultiplied, properties);
						img.copyData(raster);
					}
					img1 = Image.getInstance(bi, null, false);
				}
				Document document = new Document();
				document.setPageSize(PageSize.A4);
				document.setMargins(3, 3, 3, 3);
				PdfWriter.getInstance(document, out);
				document.open();

				Rectangle pageSize = document.getPageSize();
				if (img1.getScaledHeight() > pageSize.getHeight()
						|| img1.getScaledWidth() > pageSize.getWidth())
					img1.scaleToFit(pageSize.getWidth(), pageSize.getHeight());
				document.add(img1);

				document.close();
			}
			catch (Exception e)
			{
				throw new ModelExecutionException("Failed to convert image to PDF", null, e);
			}

		}
		else
			ImageIO.write(img, format, out);
		BinaryValue transformedImageBinary = new BinaryValue(out.toByteArray());
		if (convertedImageExit.isRepetitive())
			accumulateExitValue(convertedImageExit, context, flow, transformedImageBinary);
		else
			setExitValue(convertedImageExit, context, flow, transformedImageBinary);
	}

	@Override
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		imageTrigger = getRequiredMandatoryTrigger(IMAGE, Boolean.FALSE, BuiltinModels.BINARY_ID);
		formatTrigger = getRequiredMandatoryTrigger(FORMAT, Boolean.FALSE, BuiltinModels.TEXT_ID);
		convertedImageExit = getRequiredExit(CONVERTED_IMAGE, null, BuiltinModels.BINARY_ID);
	}

}

package tersus.plugins.images;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;

import org.krysalis.barcode4j.HumanReadablePlacement;
import org.krysalis.barcode4j.impl.AbstractBarcodeBean;
import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.impl.code39.Code39Bean;
import org.krysalis.barcode4j.impl.datamatrix.DataMatrixBean;
import org.krysalis.barcode4j.impl.int2of5.Interleaved2Of5Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.BinaryValue;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.sapi.CompositeValue;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class GenerateBarcode extends Plugin
{
	private SlotHandler textTrigger, optionsTrigger, imageExit;
	private static final Role TEXT = Role.get("<Text>");
	private static final Role OPTIONS = Role.get("<Options>");
	private static final Role IMAGE = Role.get("<Image>");

	static private HashMap<String, Class<? extends AbstractBarcodeBean>> classes = new HashMap<String, Class<? extends AbstractBarcodeBean>>();
	static private HashMap<String, HumanReadablePlacement> positions = new HashMap<String, HumanReadablePlacement>();
	static private HashMap<String, String> mimeTypes = new HashMap<String, String>();
	static
	{
		classes.put("int2of5", Interleaved2Of5Bean.class);
		classes.put("code39", Code39Bean.class);
		classes.put("code128", Code128Bean.class);
		classes.put("datamatrix", DataMatrixBean.class);

		positions.put("top", HumanReadablePlacement.HRP_TOP);
		positions.put("bottom", HumanReadablePlacement.HRP_BOTTOM);
		positions.put("none", HumanReadablePlacement.HRP_NONE);

		mimeTypes.put("png", "image/png");
		mimeTypes.put("jpeg", "image/jpeg");
	}

	@Override
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		textTrigger = getRequiredMandatoryTrigger(TEXT, Boolean.FALSE, BuiltinModels.TEXT_ID);
		imageExit = getRequiredExit(IMAGE, Boolean.FALSE, BuiltinModels.BINARY_ID);
		optionsTrigger = getNonRequiredTrigger(OPTIONS, Boolean.FALSE, null);
	}

	@Override
	public void start(RuntimeContext context, FlowInstance flow)
	{
		String text = getTriggerText(context, flow, textTrigger);
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		Object _options = getTriggerValue(context, flow, optionsTrigger);
		double fontSize = 8;
		double barHeight = 20;
		int resolution = 100;
		String type = "code128";
		String imageFormat = "png";
		String fontName = null;
		String textPosition = "TOP";
		if (_options != null)
		{
			CompositeValue options = (CompositeValue) context.getSapiManager().fromInstance(
					optionsTrigger.getChildInstanceHandler(), _options);
			type = options.getString("Barcode Type", type);
			fontSize = options.getDouble("Font Size", fontSize);
			barHeight = options.getDouble("Bar Height", barHeight);
			resolution = (int) options.getDouble("Image Resolution", resolution);
			imageFormat = options.getString("Image Format", imageFormat);
			textPosition = options.getString("Text Position", textPosition);
			fontName = options.getString("Font Name", fontName);
		}
		if (classes.containsKey(type.toLowerCase().replaceAll("[ _]","")))
		{
			AbstractBarcodeBean bean = null;
			Class<? extends AbstractBarcodeBean> c = classes.get(type.toLowerCase().replaceAll("[ _]",""));
			if (c == null)
				throw new ModelExecutionException("Unrecognized barcode type '" + type + "'");
			try
			{
				bean = c.newInstance();
			}
			catch (Exception e)
			{
				throw new ModelExecutionException("Failed to generate bacode of type '" + type
						+ "'", null, e);
			}
			bean.setVerticalQuietZone(10);
			bean.setFontSize(fontSize);
			if (fontName != null)
				bean.setFontName(fontName);
			bean.setFontSize(fontSize);
			bean.setBarHeight(barHeight);
			bean.setQuietZone(fontSize / 2);
			bean.setModuleWidth(0.5);

			HumanReadablePlacement p = positions.get(textPosition.toLowerCase());
			if (p == null)
				throw new ModelExecutionException("Unrecogized text position '" + textPosition
						+ "'");
			bean.setMsgPosition(p);

			String mimeType = mimeTypes.get(imageFormat.toLowerCase());
			if (mimeType == null)
				throw new ModelExecutionException("Unrecognized image fromat '" + imageFormat + "'");
			BitmapCanvasProvider canvas = new BitmapCanvasProvider(out, mimeType, resolution,
					BufferedImage.TYPE_BYTE_BINARY, false, 0);

			bean.generateBarcode(canvas, text);
			try
			{
				canvas.finish();
			}
			catch (IOException e)
			{
				throw new ModelExecutionException("Failed to generate barcode", null, e);
			}
		}
		else
		{
			BitMatrix matrix = null;
			com.google.zxing.Writer writer = new MultiFormatWriter();
			try
			{
				BarcodeFormat format = null;
				try
				{
					format = BarcodeFormat.valueOf(type.toUpperCase().replaceAll(" ","_"));
				}
				catch (IllegalArgumentException e)
				{
					throw new ModelExecutionException("Unknown barcode type \""+type+"\"");
				}
				Hashtable<EncodeHintType, Object> hints = new Hashtable<EncodeHintType, Object>(2);
				hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
				hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
				matrix = writer.encode(text, format, resolution,
						resolution, hints);
				MatrixToImageWriter.writeToStream(matrix, imageFormat, out);

			}
			catch (com.google.zxing.WriterException e)
			{
				throw new ModelExecutionException("Failed to generate barcode image", e);
			}
			catch (IOException e)
			{
				throw new ModelExecutionException("Failed to generate barcode image", e);
			}

		}
	
		BinaryValue image = new BinaryValue(out.toByteArray());
		setExitValue(imageExit, context, flow, image);
	}

}

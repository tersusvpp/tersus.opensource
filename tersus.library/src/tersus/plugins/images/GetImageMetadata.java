/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.images;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.util.HashSet;
import java.util.Iterator;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.MetadataException;
import com.drew.metadata.Tag;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.BinaryValue;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * @author Liat Shiff
 */
public class GetImageMetadata extends Plugin
{
	// Trigger constant
	private static final Role IMAGE_CONTENT = Role.get("<Image Content>");
	private static final Role DIRECTORY_NAME = Role.get("<Directory Name>");
	private static final Role TAG_NAME = Role.get("<Tag Name>");

	// Exits constants
	private static final Role TAGS = Role.get("<Tags>");

	// Trigger
	private SlotHandler imageContentTrigger;
	private SlotHandler directoryNameTrigger;
	private SlotHandler tagNameTrigger;

	// Exits
	private SlotHandler tagsExit;

	private ElementHandler tagNameHandler;
	private ElementHandler tagValueHandler;
	private ElementHandler directoryHandler;

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		imageContentTrigger = getRequiredMandatoryTrigger(IMAGE_CONTENT, null,
				BuiltinModels.BINARY_ID);

		directoryNameTrigger = getNonRequiredTrigger(DIRECTORY_NAME, null, BuiltinModels.TEXT_ID);
		tagNameTrigger = getNonRequiredTrigger(TAG_NAME, null, BuiltinModels.TEXT_ID);

		tagsExit = getRequiredExit(TAGS, Boolean.TRUE, TagStructure.TAG_ID);

		InstanceHandler tagHandler = tagsExit.getChildInstanceHandler();
		tagNameHandler = tagHandler.getElementHandler(TagStructure.TAG_NAME);
		tagValueHandler = tagHandler.getElementHandler(TagStructure.TAG_VALUE);
		directoryHandler = tagHandler.getElementHandler(TagStructure.DIRECTORY_NAME);
	}

	public void start(RuntimeContext context, FlowInstance flow)
	{

		BinaryValue in = (BinaryValue) imageContentTrigger.get(context, flow);
		byte[] byteArray = in.toByteArray();
		BufferedInputStream bufferedInputStream = new BufferedInputStream(new ByteArrayInputStream(
				byteArray));

		String directoryNameString = null;
		if (directoryNameTrigger != null)
			directoryNameString = (String) directoryNameTrigger.get(context, flow);

		String tagNameString = null;
		if (tagNameTrigger != null)
			tagNameString = (String) tagNameTrigger.get(context, flow);

		HashSet<Tag> tags = getTags(bufferedInputStream, directoryNameString, tagNameString);

		try
		{
			for (Tag tag : tags)
			{
				Object tagDescriptor = tagsExit.getChildInstanceHandler().newInstance(context);
				tagNameHandler.set(context, tagDescriptor, tag.getTagName());
				tagValueHandler.set(context, tagDescriptor, tag.getDescription());
				directoryHandler.set(context, tagDescriptor, tag.getDirectoryName());

				accumulateExitValue(tagsExit, context, flow, tagDescriptor);
			}
		}
		catch (MetadataException e)
		{
			e.printStackTrace();
		}
	}

	public HashSet<Tag> getTags(BufferedInputStream Buf, String directoryName, String tagName)
	{
		HashSet<Tag> tags = new HashSet<Tag>();

		try
		{
			Metadata metadata = ImageMetadataReader.readMetadata(Buf);

			Iterator<?> directories = metadata.getDirectoryIterator();
			while (directories.hasNext())
			{
				Directory directory = (Directory) directories.next();
				if (directoryName!= null && !directoryName.equals(directory.getName()))
					continue;
				Iterator<?> tagsIterator = directory.getTagIterator();
				while (tagsIterator.hasNext())
				{
					Tag tag = (Tag) tagsIterator.next();
					if (tagName != null && !tagName.equals(tag.getTagName()))
						continue;
					tags.add(tag);
				}
			}
		}
		catch (ImageProcessingException e)
		{
			e.printStackTrace();
		}

		return tags;
	}
}

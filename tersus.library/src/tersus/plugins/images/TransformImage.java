package tersus.plugins.images;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;
import javax.imageio.stream.MemoryCacheImageOutputStream;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.BinaryValue;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.sapi.CompositeValue;

public class TransformImage extends Plugin
{
    public static final Role IMAGE = Role.get("<Image>");
    public static final Role TRANSFORMED_IMAGE = Role
            .get("<Transformed Image>");
    public static final Role ROTATION = Role.get("<Rotation>");
    public static final Role OUTPUT_FORMAT = Role.get("<Output Format>");
    public static final Role QUALITY = Role.get("<Quality>");
    public static final Role AFFINE_TRANSFORM = Role.get("<Affine Transform>");
    private SlotHandler imageTrigger;
    private SlotHandler qualityTrigger;
    private SlotHandler outputFormatTrigger;
    private SlotHandler rotationTrigger;
    private SlotHandler transformTrigger;
    private SlotHandler transformedImageExit;

    /**
     * @param args
     */
    @Override
    public void start(RuntimeContext context, FlowInstance flow)
    {
        try
        {
            BinaryValue in = (BinaryValue) imageTrigger.get(context, flow);
            int quality = getTriggerValue(qualityTrigger, context, flow, -1);
            BufferedImage img = ImageIO.read(new ByteArrayInputStream(in
                    .toByteArray()));
            String outputFormat = getTriggerText(context, flow, outputFormatTrigger);
            if (outputFormat == null)
            	outputFormat = getImageFormat(in.toByteArray());
            int h = img.getHeight();
            int w = img.getWidth();
            Number rotation = (Number) getTriggerValue(context, flow, rotationTrigger);
            Object affineTransformObj = getTriggerValue(context, flow, transformTrigger);
            if (rotation != null && affineTransformObj != null
                    || rotation == null && affineTransformObj == null)
                throw new ModelExecutionException(
                        "Either rotation or transform shoulf be specified");
            AffineTransform transform = null;
            if (rotation != null)
            {
                double theta = rotation.value / 180.0 * Math.PI;
                double cos = Math.cos(theta);
                double sin = Math.sin(theta);

                transform = new AffineTransform(cos, sin, -sin, cos, -min(0, -h
                        * sin, w * cos, -h * sin + w * cos), -min(0, h * cos, w
                        * sin, h * cos + w * sin));
            }
            else
            {
                assert affineTransformObj != null;
                CompositeValue v = (CompositeValue)context.getSapiManager().fromInstance(transformTrigger.getChildInstanceHandler(), affineTransformObj);
                transform = new AffineTransform(v.getDouble("m00",0),v.getDouble("m10",0), v.getDouble("m01",0),v.getDouble("m11",0), v.getDouble("m02",0), v.getDouble("m12",0));
            }
            AffineTransformOp op = new AffineTransformOp(transform,
                    AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
            BufferedImage transformedImg = op.filter(img, null);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            if ("jpeg".equalsIgnoreCase(outputFormat) && quality>=0) // Explicit quality given
            {
            	Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName("jpeg");
            	ImageWriter writer = (ImageWriter)iter.next();
            	ImageWriteParam p = writer.getDefaultWriteParam();
            	p.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            	p.setCompressionQuality(quality/(float)100.0); 
            	ImageOutputStream ios = new MemoryCacheImageOutputStream(out);
				
            	writer.setOutput(ios);
            	IIOImage image = new IIOImage(transformedImg, null, null);
            	writer.write(null, image, p);
            	writer.dispose();
            	ios.close();
            }
            else
            {
            	ImageIO.write(transformedImg, outputFormat, out);
            	out.close();
            }
            BinaryValue transformedImageBinary = new BinaryValue(out.toByteArray());
            setExitValue(transformedImageExit,context, flow, transformedImageBinary);
        }
        catch (IOException e)
        {
            throw new ModelExecutionException("Failed to transform image",
                    null, e);
        }
    }

    private static String getImageFormat(byte[] bytes) throws IOException
    {
        ImageInputStream is = ImageIO
                .createImageInputStream(new ByteArrayInputStream(bytes));
        Iterator<ImageReader> i = ImageIO.getImageReaders(is);
        if (!i.hasNext())
            return null;

        ImageReader reader = i.next();
		String format = reader.getFormatName();
        return format;
    }

    private static double min(double a, double b, double c, double d)
    {

        return Math.min(a, Math.min(b, Math.min(c, d)));
    }

    @Override
    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        imageTrigger = getRequiredMandatoryTrigger(IMAGE, Boolean.FALSE,
                BuiltinModels.BINARY_ID);
        qualityTrigger=getNonRequiredTrigger(QUALITY, Boolean.FALSE, BuiltinModels.NUMBER_ID);
        outputFormatTrigger=getNonRequiredTrigger(OUTPUT_FORMAT, Boolean.FALSE, BuiltinModels.TEXT_ID);
        rotationTrigger = getNonRequiredTrigger(ROTATION, Boolean.FALSE,
                BuiltinModels.NUMBER_ID);
        transformTrigger = getNonRequiredTrigger(AFFINE_TRANSFORM,
                Boolean.FALSE, null);
        transformedImageExit = getRequiredExit(TRANSFORMED_IMAGE,
                Boolean.FALSE, BuiltinModels.BINARY_ID);
    }

}

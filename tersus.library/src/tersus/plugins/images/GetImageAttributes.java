package tersus.plugins.images;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.BinaryValue;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

public class GetImageAttributes extends Plugin
{
	@Override
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		imageTrigger = getRequiredMandatoryTrigger(IMAGE, Boolean.FALSE, BuiltinModels.BINARY_ID);
		heightExit = getNonRequiredExit(HEIGHT, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		widthExit = getNonRequiredExit(WIDTH, Boolean.FALSE, BuiltinModels.NUMBER_ID);
	}

	public static final Role IMAGE = Role.get("<Image>");
	public static final Role HEIGHT = Role.get("<Height>");
	public static final Role WIDTH = Role.get("<Width>");
	protected SlotHandler imageTrigger, heightExit, widthExit;

	@Override
	public void start(RuntimeContext context, FlowInstance flow)
	{
		BufferedImage img = null;
		BinaryValue data = (BinaryValue)imageTrigger.get(context, flow);
		try
		{
			 img = ImageIO.read(new ByteArrayInputStream(data
			        .toByteArray()));
		}
		catch (IOException e)
		{
			throw new ModelExecutionException("Failed to create image",null,e);
		}
		setExitValue(heightExit, context, flow, new Number(img.getHeight()));
		setExitValue(widthExit, context, flow, new Number(img.getWidth()));
	}
	
	

}

package tersus.plugins.misc;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Path;
import tersus.model.Repository;
import tersus.model.Role;
import tersus.model.validation.Problems;
import tersus.runtime.CompositeDataHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

public class GetModel extends Plugin
{
	
	private static final Role MODEL_ID = Role.get("<Model Id>");
	private static final Role INSTANCE = Role.get("<Instance>");
    private static final Role LEVEL = Role.get("<Level>");
	private static final Role Model = Role.get("<Model>");
	
	private SlotHandler modelIdTrigger, instanceTrigger, levelTrigger, modelExit;

	@Override
	public void initializeFromModel(Model model)
	{
		// TODO Auto-generated method stub
		super.initializeFromModel(model);
		modelIdTrigger = getNonRequiredTrigger(MODEL_ID,Boolean.FALSE, BuiltinModels.TEXT_ID);
		instanceTrigger = getNonRequiredTrigger(INSTANCE,Boolean.FALSE, null);
		levelTrigger = getNonRequiredTrigger(LEVEL,Boolean.FALSE, BuiltinModels.NUMBER_ID);
		
		if ((modelIdTrigger != null ? 1 : 0) + (instanceTrigger != null ? 1 : 0) + (levelTrigger != null ? 1 : 0) > 1)
			notifyInvalidModel(Path.EMPTY,
                    Problems.INVALID_TRIGGERS, MODEL_ID + ", " + INSTANCE + " and " + LEVEL + "  must be used separately");
		
		modelExit = getRequiredExit(Model, Boolean.FALSE, BuiltinModels.MODEL_ID);
	}

	@Override
	public void start(RuntimeContext context, FlowInstance flow)
	{
		ModelId modelId = null;
		Repository repository = context.getModelLoader().getRepository();
		if (modelIdTrigger != null)
		{
			String modelIdStr = (String)modelIdTrigger.get(context, flow);
			if (modelIdStr != null)
			{
				modelId = repository.getModelId(modelIdStr);
			}
		}
		else
			if (instanceTrigger != null)
			{
				Object instance = instanceTrigger.get(context, flow);
				if (instance != null)
					modelId  = CompositeDataHandler.getModelIdEx(instance);
			}
			else
			{
				int level = 1;
				if (levelTrigger != null)
					level = (int)((Number)levelTrigger.get(context, flow)).value;

				if (level < 0)
					modelId = context.getServiceModelId();
				else
				{
					FlowInstance parent = flow, self = flow;
					while (level-- > 0 && parent != null)
					{
						self = parent;
						parent = self.getParent();
					}
					modelId = (parent != null ? parent: self).getHandler().getModelId();					
				}
			}
			
		if (modelId != null)
		{
			Model model = repository.getModel(modelId, true);
			setExitValue(modelExit,context, flow , model);
		}
	}

}

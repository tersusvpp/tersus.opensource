package tersus.plugins.misc;

import java.util.ArrayList;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.model.validation.Problems;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.webapp.LoggingHelper;

public class Log extends Plugin
{

	private static final Role LEVEL = Role.get("<Level>");
	private static final Role MESSAGE = Role.get("<Message>");
	private static final Role LOGGER_NAME = Role.get("<Logger Name>");
	private static final Role ERROR = Role.get("<Error>");
	private static final Role ENABLED = Role.get("<Enabled>");
	private static final Role DISABLED = Role.get("<Disabled>");
	public static final String DEFAULT_LOGGER = "APPLICATION";
	private SlotHandler levelTrigger, errorTrigger, messageTrigger, loggerNameTrigger, enabledExit,
			disabledExit;
	private SlotHandler[] contextTriggers;
	private ElementHandler errorMessageElement, errorDetailsElement, errorLocationElement;

	@Override
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		levelTrigger = getNonRequiredTrigger(LEVEL, Boolean.FALSE, BuiltinModels.TEXT_ID);
		messageTrigger = getNonRequiredTrigger(MESSAGE, Boolean.FALSE, BuiltinModels.TEXT_ID);
		loggerNameTrigger = getNonRequiredTrigger(LOGGER_NAME, Boolean.FALSE, BuiltinModels.TEXT_ID);
		errorTrigger = getNonRequiredTrigger(ERROR, Boolean.FALSE, BuiltinModels.ERROR_ID);
		if (errorTrigger != null)
		{
			errorMessageElement = errorTrigger.getChildInstanceHandler().getElementHandler(
					BuiltinModels.ERROR_MESSAGE);
			errorDetailsElement = errorTrigger.getChildInstanceHandler().getElementHandler(
					BuiltinModels.ERROR_DETAILS);
			errorLocationElement = errorTrigger.getChildInstanceHandler().getElementHandler(
					BuiltinModels.ERROR_LOCATION);
		}
		enabledExit = getNonRequiredExit(ENABLED, Boolean.FALSE, BuiltinModels.NOTHING_ID);
		disabledExit = getNonRequiredExit(DISABLED, Boolean.FALSE, BuiltinModels.NOTHING_ID);
		ArrayList<SlotHandler> list = new ArrayList<SlotHandler>();
		for (SlotHandler trigger : this.triggers)
		{

			if (!trigger.getRole().isDistinguished() && !trigger.isEmpty())
			{
				if (trigger.isRepetitive())
				{
					notifyInvalidModel(trigger.getRole(), Problems.INVALID_TRIGGER,
							"Context triggers cannot be repetitive");
					break;
				}

				list.add(trigger);
			}
		}
		contextTriggers = list.toArray(new SlotHandler[list.size()]);
	}

	@Override
	public void start(RuntimeContext context, FlowInstance flow)
	{
		for (SlotHandler t : contextTriggers)
		{
			Object value = t.get(context, flow);
			String key = t.getRole().toString();

			if (value == null)
				MDC.remove(key);
			else if (value instanceof tersus.runtime.Number)
				MDC.put(key, ((LeafDataHandler) t.getChildInstanceHandler()).toSQL(value));
			else
				MDC.put(key, value);
		}

		String message = getTriggerText(context, flow, messageTrigger);
		Object error = getTriggerValue(context, flow, errorTrigger);
		String loggerName = getTriggerText(context, flow, loggerNameTrigger);
		String levelName = getTriggerText(context, flow, levelTrigger);
		Logger logger = context.getContextPool().getLogger(
				loggerName == null ? DEFAULT_LOGGER : loggerName);

		Level level = Level.toLevel(levelName);
		boolean enabled = logger.isEnabledFor(level);
		if (enabled)
			chargeEmptyExit(context, flow, enabledExit);
		else
			chargeEmptyExit(context, flow, disabledExit);
		if (message == null && error == null)
			return;
		// No need to actually log anything

		String errorMessage = null;
		String errorDetails = null;
		String errorLocation = null;
		if (error != null)
		{
			errorMessage = (String) errorMessageElement.get(context, error);
			errorDetails = (String) errorDetailsElement.get(context, error);
			errorLocation = (String) errorLocationElement.get(context, error);
		}
		message = message == null ? errorMessage : message;
		String location = errorLocation != null ? errorLocation : context.currentFlowPath
				.toString();
		try
		{
			if (errorDetails != null)
				MDC.put(LoggingHelper.ERROR_DETAILS, errorDetails);
			if (location != null)
				MDC.put(LoggingHelper.LOCATION, location);
			logger.log(level, message);
		}
		finally
		{
			MDC.remove(LoggingHelper.ERROR_DETAILS);
			MDC.remove(LoggingHelper.LOCATION);
		}
	}
}

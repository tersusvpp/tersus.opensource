/*
 * Copyright (c) 2003-2022 Tersus Software Ltd.
 * 
 * Created on 18/10/2004
 *
 */

package tersus.plugins.misc;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import tersus.InternalErrorException;
import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.plugins.misc.Excel;
import tersus.runtime.BinaryValue;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.Template;

/**
 * An atomic flow handler that replaces specific sub-strings in an Excel sheet.
 * 
 * @param <File>
 *            The Excel file.
 * @param <Sheet Name> The name of the sheet in the file into which data is inserted (if missing,
 *        the default is to use the first sheet).
 * @param ... The sheet contains place-holders of the form "$(Role)", which are substituted by the
 *        content of the slot with role 'Role'. E.g. if the sheet contains a cell with the text
 *        "Week ${Week Number}", and the action has input trigger 'Week Number', then assuming 'Week
 *        Number' is 5, the cell content will be updated to "Week 5".
 * @param <<File>> The updated Excel file.
 * @param Error
 *            Optional error message
 * 
 * @author Ofer Brandes
 * 
 */

public class UpdateExcelText extends Plugin
{
	private static final Role INPUT_FILE_ROLE = Role.get("<File>");
	private static final Role SHEET_NAME_ROLE = Role.get("<Sheet Name>");
	private static final Role OUTPUT_ROLE = Role.get("<<File>>");

	SlotHandler inputHandler, sheetNameTrigger, outputHandler;

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		try
		{
			// Open the workbook
			BinaryValue data = (BinaryValue) inputHandler.get(context, flow);
			Workbook wb = Excel.openWorkBook(context, data);

			// Get the specified (or first) sheet
			String sheetName = (sheetNameTrigger == null ? null : (String) sheetNameTrigger.get(
					context, flow));
			Sheet sheet = Excel.getSheet(context, wb, sheetName);

			// Get the input to be inserted into the sheet
			Map<String, Object> properties = new HashMap<String, Object>();
			for (int i = 0; i < triggers.length; i++)
			{
				SlotHandler trigger = triggers[i];
				String role = trigger.getRole().toString();
				if (!inputHandler.equals(trigger))
				{
					Object value = trigger.get(context, flow);
					if (value == null)
						value = "";
					InstanceHandler childInstanceHandler = trigger.getChildInstanceHandler();
					if (childInstanceHandler instanceof LeafDataHandler)
						properties.put(role,
								((LeafDataHandler) childInstanceHandler).toString(context, value));
					else
						properties.put(role, value.toString());
				}
			}

			// Insert the input into the proper places in the sheet
			for (int r = 0; r <= sheet.getLastRowNum(); r++)
			{
				Row row = sheet.getRow(r);
				if (row != null)
				{
					for (short c = 0; c <= row.getLastCellNum(); c++)
					{
						Cell cell = row.getCell(c);
						if ((cell != null) && (cell.getCellType() == Cell.CELL_TYPE_STRING))
						{
							String cellContent = cell.getStringCellValue();
							if (cellContent.indexOf("${") >= 0)
							{
								Template template = new Template(cellContent);
								StringWriter output = new StringWriter();
								try
								{
									template.write(output, properties);
								}
								catch (IOException e3)
								{
									throw new EngineException("faild to update '" + cellContent
											+ "'", null, e3);
								}
								String updatedContent = output.toString();
								cell.setCellValue(updatedContent);
							}
						}
					}
				}
			}

			// Output the updated file
			BinaryValue updatedData = null;

			try
			{
				ByteArrayOutputStream output = new ByteArrayOutputStream();
				wb.write(output);
				updatedData = new BinaryValue(output.toByteArray());
				output.close();
			}
			catch (IOException e2)
			{
				throw new EngineException("Failed to output the updated Excel file", null, e2);
			}

			setExitValue(outputHandler, context, flow, updatedData);

			// // temporary patch - write out the modofied file to "C:/temp/Modified Excel.xls"
			// Excel.saveCopy(wb);
		}
		catch (EngineException e)
		{
			fireError(context, flow, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		inputHandler = getRequiredMandatoryTrigger(INPUT_FILE_ROLE, Boolean.FALSE,
				BuiltinModels.BINARY_ID);
		sheetNameTrigger = getNonRequiredMandatoryTrigger(SHEET_NAME_ROLE, Boolean.FALSE,
				BuiltinModels.TEXT_ID);

		outputHandler = getRequiredExit(OUTPUT_ROLE, Boolean.FALSE, BuiltinModels.BINARY_ID);

		for (int i = 0; i < triggers.length; i++)
			if ((triggers[i] != inputHandler) && (triggers[i] != sheetNameTrigger))
				checkDataType(triggers[i], Boolean.TRUE, true);
	}
}

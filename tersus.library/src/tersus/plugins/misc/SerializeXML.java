/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.misc;

import java.io.IOException;
import java.util.HashMap;

import org.apache.log4j.Logger;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Repository;
import tersus.model.Role;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.ModelIdHelper;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.XMLSerializer;
import tersus.util.SpecialRoles;
import tersus.webapp.JavaScriptFormat;

/**
 * @author oferb
 * 
 * Creating a well formed XML document (opposite of 'ParseXML').
 * 
 * This class was cloned from 'CallWebService' - we may want to merge the common parts of the two classes.
 *
 */

public class SerializeXML extends Plugin

{
	private static final Role INPUT_ROLE = Role.get("<Data Structure>");
	private static final Role OUTPUT_ROLE = Role.get("<XML Document>");
	private static final Role NAMESPACE = Role.get("<Namespace>");
	private static final Role INCLUDE_EMPTY_ELEMENTS = Role.get("<Include Empty Elements>");

	Repository repository = null;

	SlotHandler dataTrigger, namespaceTrigger, aliasesTrigger, includeEmptyElementsTrigger;
	SlotHandler outputExit;

	HashMap<String, String> nameSpaceAliases = null;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
        Logger log = context.getContextPool().getLogger(getClass().getName());

        // Serialize input as an XML document
        try
        {
        	String xmlDoc = createXmlDoc (flow, context);
    		if (log.isDebugEnabled()) 
            {
                log.debug("XML Document:\n"+xmlDoc);
            }

    		outputExit.set(context, flow, xmlDoc);
        }
        catch (IOException e)
        {
        	throw new ModelExecutionException("Failed to serialize XML",null,e);
        }
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.InstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		dataTrigger = getRequiredMandatoryTrigger(INPUT_ROLE, Boolean.FALSE, null);
		outputExit = getRequiredExit(OUTPUT_ROLE, Boolean.FALSE, BuiltinModels.TEXT_ID);
		namespaceTrigger = getNonRequiredTrigger(NAMESPACE, Boolean.FALSE, BuiltinModels.TEXT_ID);
		includeEmptyElementsTrigger = getNonRequiredTrigger(INCLUDE_EMPTY_ELEMENTS, Boolean.FALSE, BuiltinModels.BOOLEAN_ID);
		aliasesTrigger = getNonRequiredTrigger(SpecialRoles.ALIASES_TRIGGER_ROLE, Boolean.FALSE, BuiltinModels.TEXT_ID);
		repository = (Repository)model.getRepository();
		
		nameSpaceAliases = new HashMap<String, String>(); // In the future we may want to use a trigger to get namespace aliases

	}

	private String createXmlDoc (FlowInstance flow, RuntimeContext context) throws IOException
	{
		String globalNamespace = getTriggerText(context, flow, namespaceTrigger);
		Object value = dataTrigger.get(context,flow);
	    ModelId valueModelId = ModelIdHelper.getCompositeModelId(value);
	    if (valueModelId == null)
	        valueModelId = dataTrigger.getChildModelId();

		InstanceHandler handler = context.getModelLoader().getHandler(valueModelId);
		String topElementName = handler.getName();

		boolean includeEmptyElements = getTriggerValue(includeEmptyElementsTrigger, context, flow, false);
		XMLSerializer serializer = new XMLSerializer(null, (globalNamespace == null ? nameSpaceAliases : null), includeEmptyElements, null, context);

		boolean useAliases = (aliasesTrigger != null);
		if (useAliases)
			serializer.alsoListUsedNameSpaces (dataTrigger);
		
		serializer.addXmlElement (	null, ("none".equalsIgnoreCase(globalNamespace) ? null : globalNamespace),
				(globalNamespace == null) || useAliases, topElementName, useAliases, value, dataTrigger);

		return serializer.done();
	}
}

package tersus.plugins.misc;

import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.webapp.WebContext;

public class GetHTTPHeaders extends Plugin
{
	private static final Role HEADER_NAMES = Role.get("<Header Names>");
    private static final Role HEADERS = Role.get("<Headers>");
    private static final Role NAME = Role.get("Name"); 
    private static final Role VALUES = Role.get("Values"); 
    
    
    private SlotHandler headerNamesTrigger, headersExit;
    private InstanceHandler headerType;
    private ElementHandler nameElement, valuesElement;
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		headerNamesTrigger = getNonRequiredTrigger(HEADER_NAMES, Boolean.TRUE, BuiltinModels.TEXT_ID);
		headersExit = getRequiredExit(HEADERS, null, BuiltinModels.HTTP_HEADER_ID);
		headerType =  headersExit.getChildInstanceHandler();
		nameElement = headerType.getElementHandler(NAME);
		valuesElement = headerType.getElementHandler(VALUES);
	}
	@SuppressWarnings("unchecked")
	@Override
	public void start(RuntimeContext context, FlowInstance flow)
	{
		if (!(context instanceof WebContext))
			return;
		HttpServletRequest request = ((WebContext)context).getRequest();
		if (request == null )
			return;
		List<String> names = null;
		if (headerNamesTrigger != null)
		{
			names = (List<String>) headerNamesTrigger.get(context, flow);
		}
		else
		{
			names = Collections.list(request.getHeaderNames());
		}
		for (String name : names)
		{
			Enumeration<String> values = request.getHeaders(name);
			if (values.hasMoreElements())
			{
				Object obj = headerType.newInstance(context);
				nameElement.set(context, obj, name);
				while (values.hasMoreElements())
				{
					valuesElement.accumulate(context, obj, values.nextElement());
				}					
				if (headersExit.isRepetitive())
					accumulateExitValue(headersExit, context, flow, obj);
				else
					setExitValue(headersExit, context, flow,obj);
			}
		}	
	}
	
    
    
}

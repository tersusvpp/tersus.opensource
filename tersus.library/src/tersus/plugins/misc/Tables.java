/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.misc;

import tersus.runtime.ElementHandler;
import tersus.runtime.InstanceHandler;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.RuntimeContext;

public class Tables
{
    public static boolean isEmpty(RuntimeContext context,InstanceHandler handler,  Object dataValue)
    {
        if (dataValue == null)
            return true;
        else if (handler instanceof LeafDataHandler)
            return false;
        ElementHandler[] elementHandlers = handler.elementHandlers;
        for (int i=0; i< elementHandlers.length; i++)
        {
            if (! isEmpty(context, elementHandlers[i].getChildInstanceHandler(), elementHandlers[i].get(context, dataValue)))
                return false;
        }
        return true;
    }

}


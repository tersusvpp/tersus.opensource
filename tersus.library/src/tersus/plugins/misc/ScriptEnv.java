package tersus.plugins.misc;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import tersus.model.BuiltinPlugins;
import tersus.model.ModelElement;
import tersus.runtime.BinaryValue;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.RuntimeContext;
import tersus.webapp.ApplicationSettings;
import tersus.webapp.ContextPool;
import tersus.webapp.Engine;
import tersus.webapp.SetupScriptGenerator;
import tersus.webapp.WebContext;

public class ScriptEnv
{
    private RuntimeContext context;

    public ScriptEnv(RuntimeContext context)
    {
        this.context = context;
    }
    
    public String getInputAdapters()
    {
    	StringBuffer b = new StringBuffer();
    	for (ModelElement e: context.getContextPool().getRootModel().getElements())
    	{
    		if (e.getRefId() != null && BuiltinPlugins.INPUT_ADAPTER.equals(e.getReferredModel().getPlugin()))
    		{
    			if (b.length() > 0)
    				b.append(",");
    			b.append(e.getRole());
    				 
    		}
    		
    	}
    	return b.toString();
    }
    
    public String getCurrentOutputAdapter()
    {
    	return context.currentFlowPath.getSegment(0).toString();
    }

    public long getScriptsLastModified()
    {
        return ((tersus.webapp.ContextPool)context.getContextPool()).getScriptAggregator().getLastModified();
    }
    public long getCSSLastModified()
    {
        return ((tersus.webapp.ContextPool)context.getContextPool()).getScriptAggregator().getLastModified();
    }
    public Object instantiate(String className)
    {
            try
            {
                return context.getContextPool().getClassLoader().loadClass(className).newInstance();
            }
            catch (InstantiationException e)
            {
                throw new ModelExecutionException("Script error: failed to instantiate class "+className,null,e);
            }
            catch (IllegalAccessException e)
            {
                throw new ModelExecutionException("Script error: failed to instantiate class "+className,null,e);
            }
            catch (ClassNotFoundException e)
            {
               throw new ModelExecutionException("Script error: class "+className+" not found");
            }
    }
    public String getSetupScript()
    {
        // TODO: refactor to improve reuse with Engine.getWelcomePageContent()
        ContextPool pool =(ContextPool)context.getContextPool();
        ApplicationSettings settings = new ApplicationSettings(pool, context.getUserPermissions());// TODO cache settings
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.putAll(settings.getProperties());
        HttpServletRequest request = ((WebContext)context).getRequest();
		String pathSuffix = request.getServletPath();
		String uri = request.getRequestURI();
		String contextPath = request.getContextPath();
			pathSuffix = uri.substring(contextPath.length());
		properties.put(Engine.URL_PATH_SUFFIX, pathSuffix);
		
        String loggedInUser = context.getLoggedInUser();
        String userName = loggedInUser == null ? Engine.GUEST : loggedInUser;
        long timestamp = Math.max(pool.getTimestamp(), context.getLastPermissionUpdate());
        long cssLastModified = getCSSLastModified();
        StringBuffer setupScript = SetupScriptGenerator.prepareSetupScript(settings.getPerspectives(), properties, userName, timestamp, cssLastModified);
        return setupScript.toString();
    }
    
    public String getWelcomePageContent()
    {
        return Engine.getWelcomePageContent(context);
    }
    
    
    public String getExcelFormula(BinaryValue v, int row, int column)
    {
    	Workbook wb = Excel.openWorkBook(context, v);

        // Get the specified (or first) sheet
        Sheet sheet = Excel.getSheet(context, wb, null);
        return sheet.getRow(row).getCell(column).getCellFormula();
    }
    public double getExcelNumericValue(BinaryValue v, int row, int column)
    {
    	Workbook wb = Excel.openWorkBook(context, v);

        // Get the specified (or first) sheet
        Sheet sheet = Excel.getSheet(context, wb, null);
        return sheet.getRow(row).getCell(column).getNumericCellValue();
    }
    
    public String getRequestPath()
    {
    	return Engine.getPathStr(((WebContext)context).getRequest());
    }
    public String getContextPath()
    {
    	return ((WebContext)context).getRequest().getContextPath();
    }
}

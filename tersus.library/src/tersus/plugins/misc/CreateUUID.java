/************************************************************************************************
 * Copyright (c) 2003-2022 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.misc;

import java.util.UUID;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that returns a UUID/GUID.
 * 
 * @author David Davidson
 * 
 */
public class CreateUUID extends Plugin
{
    private static final Role UUID_EXIT = Role.get("<UUID>");

    private SlotHandler uuidExit;

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
    	String uuid = UUID.randomUUID().toString();

        setExitValue(uuidExit, context, flow, uuid.toString());
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
     */
    public void resume(RuntimeContext context, FlowInstance flow)
    { // Nothing to do (This is an action)
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
     * 
     * Create a triggers sorted by lexicographically (by role)
     */
    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        
        uuidExit = getRequiredExit(UUID_EXIT, Boolean.FALSE,
                BuiltinModels.TEXT_ID);

    }
}
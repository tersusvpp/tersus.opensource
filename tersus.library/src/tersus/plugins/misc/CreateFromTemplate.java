/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.misc;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.Template;

/**
 * An atomic flow handler that creates a text from a template
 * by susbtituting embedded place-holder by specific strings.
 * 
 * The template contains place-holders of the form "$(Role)",
 * which are substituted by the content of the slot with role 'Role'.
 * 
 * E.g. if the template is "Purchase request ${Number} was submitted by ${Requester}",
 * and the action has input triggers 'Number' and 'Requester',
 * then assuming 'Number' is 5 and 'Requester' is "Harel",
 * the flow's output will be "Purchase request 5 was submitted by Harel".
 *
 * @author Ofer Brandes
 *
 */

public class CreateFromTemplate extends Plugin
{
	private static final Role TEMPLATE=Role.get("<Template>");	// Same as in CreateTableCommand.addOrUpdateValidValuesModeling()
	private static final Role TEXT=Role.get("<Text>");			// Same as in CreateTableCommand.addOrUpdateValidValuesModeling()

	SlotHandler templateTrigger;
	SlotHandler exit;

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Role templateTriggerRole = templateTrigger.getRole();
		String templateText = (String) templateTrigger.get(context, flow);
		Template template = new Template(templateText);
		
		Map properties = new HashMap();
		for (int i = 0; i < triggers.length; i++)
		{
			SlotHandler trigger = triggers[i];
			Role role = trigger.getRole();
			if (trigger != templateTrigger)
			{
				Object value = trigger.get(context,flow);
				if (value == null)
					value = "";
				InstanceHandler childInstanceHandler = trigger.getChildInstanceHandler();
				if (childInstanceHandler instanceof LeafDataHandler)
					properties.put(role.toString(), ((LeafDataHandler)childInstanceHandler).toString(context, value));
				else
					properties.put(role.toString(), value.toString());
			}
		}

		StringWriter output = new StringWriter();
		try
		{
			template.write(output,properties);
		}
		catch (IOException e)
		{
			throw new EngineException("Failed to create text from template \"" + templateText +"\"", null, e);
		}
		String exitValue = output.toString();
		setExitValue(exit, context, flow, exitValue);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		
		templateTrigger = getRequiredMandatoryTrigger(TEMPLATE, Boolean.FALSE, BuiltinModels.TEXT_ID);
		
		exit = getRequiredExit(TEXT, Boolean.FALSE, BuiltinModels.TEXT_ID);
	}

}

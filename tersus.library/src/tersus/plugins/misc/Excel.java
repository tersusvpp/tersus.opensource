/************************************************************************************************
 * Copyright (c) 2003-2007 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.misc;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import tersus.runtime.BinaryValue;
import tersus.runtime.CompositeDataHandler;
import tersus.runtime.DataHandler;
import tersus.runtime.DateAndTimeHandler;
import tersus.runtime.DateHandler;
import tersus.runtime.ElementHandler;
import tersus.runtime.EngineException;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.NumberHandler;
import tersus.runtime.RuntimeContext;
import tersus.util.InvalidInputException;
import tersus.util.Misc;

/**
 * Utilities to be used by flow handlers that deal with Excel files.
 * 
 * @author Ofer Brandes
 *  
 */

public class Excel
{
    /**
     * Create a Work Book from binary data
     *  
     */
    public static Workbook openWorkBook(RuntimeContext context,
            BinaryValue data)
    {
        // Create a new org.apache.poi.poifs.filesystem.Filesystem

        try
        {
        	Workbook wb = WorkbookFactory.create(new ByteArrayInputStream(data
                    .toByteArray()));
        	return wb;
        }
        catch (Exception e)
        {
            throw new EngineException("Failed to read spreadsheet (possibly not a valid xls/xlsx file)'",
                    null,
                    e);
        }
    }

    /**
     * Get a specific (or the first) sheet in the work book
     *  
     */
    public static Sheet getSheet(RuntimeContext context, Workbook wb,
            String sheetName)
    {
        Sheet sheet = null;

        if (sheetName == null)
        {
            sheet = wb.getSheetAt(0);
            if (sheet == null)
                throw new ModelExecutionException("Missing sheet in Excel file");
        }
        else
        {
            sheet = wb.getSheet(sheetName);
            if (sheet == null)
                throw new ModelExecutionException("Missing sheet '" + sheetName
                        + "' in Excel file");
        }

        return sheet;
    }

    /**
     * Locate the row in the sheet that contains a specific title (or -1 if not
     * found)
     *  
     */
    public static int findTitleRow(RuntimeContext context, Sheet sheet,
            String title)
    {
        int rowFound = -1;

        if (title != null)
        {
            for (int r = 0; r <= sheet.getLastRowNum(); r++)
            {
                Row row = sheet.getRow(r);
                if (row != null)
                {
                    for (short c = 0; c <= row.getLastCellNum(); c++)
                    {
                        Cell cell = row.getCell(c);
                        if ((cell != null)
                                && (cell.getCellType() == Cell.CELL_TYPE_STRING))
                        {
                            String cellContent = cell.getStringCellValue();
                            if (title.equals(cellContent))
                            {
                                rowFound = r;
                                break;
                            }
                        }
                    }
                    if (rowFound >= 0)
                        break;
                }
            }
            if (rowFound < 0)
                throw new EngineException("Title '" + title
                        + "' not found in Excel", null, null);
        }

        return rowFound;
    }

    public class TableTitleRow
    {
        public int[] columns = null;

        public int row = -1;
    }

    /**
     * Locate the row in the sheet that contains all (leaf) elements in the
     * output data model
     * 
     * @throws InvalidInputException
     *             if any of the elements are missing in the title row
     */
    public static TableTitleRow findTableTitleRow(RuntimeContext context,
            Sheet sheet, int startRow, CompositeDataHandler rowHandler)
    {
        TableTitleRow tableTitleRow = new Excel().new TableTitleRow();

        if (rowHandler.getNumberOfElements() < 1)
            throw new EngineException("Need at least one item in each row",
                    null, null);

        tableTitleRow.columns = new int[rowHandler.getNumberOfElements()];

        List<String> bestMatchMissingColumns = null;

        for (int r = startRow; r <= sheet.getLastRowNum(); r++)
        {
            Row row = sheet.getRow(r);
            if (row != null)
            {
                List<String> missingColumns = new ArrayList<String>();
                for (int col = 0; col < tableTitleRow.columns.length; col++)
                {
                    ElementHandler elementHandler = rowHandler
                            .getElementHandler(col);
                    String role = elementHandler.getRole().toString();
                    if (elementHandler.getChildInstanceHandler() instanceof LeafDataHandler)
                    {
                        tableTitleRow.columns[col] = -1;
                        for (short c = 0; c <= row.getLastCellNum(); c++)
                        {
                            Cell titleCell = row.getCell(c);
                            if (compareCellContent(titleCell, role))
                                tableTitleRow.columns[col] = c;
                        }
                        if (tableTitleRow.columns[col] < 0)
                            missingColumns.add(role);
                    }
                    else
                    {
                        //TODO Handle composite structures
                        throw new InvalidInputException(
                                rowHandler.getModelId()
                                        + " contains a composite element '"
                                        + role
                                        + "', which is (currently) not supported in Excel files");
                    }
                }

                if (missingColumns.isEmpty())
                {
                    tableTitleRow.row = r;
                    return tableTitleRow;
                }
                else if ((bestMatchMissingColumns == null)
                        || (bestMatchMissingColumns.size() > missingColumns
                                .size()))
                    bestMatchMissingColumns = missingColumns;
            }
        }

        if (bestMatchMissingColumns != null)
            throw new InvalidInputException(
                    "Bad file format - missing columns: "
                            + Misc
                                    .concatenateList(bestMatchMissingColumns,
                                            ","));
        else
            throw new InvalidInputException("Bad file format - empty file");
    }

    /**
     * @param row
     * @param rowHandler
     * @return
     */
    public static Object loadExcelRow(RuntimeContext context, int[] columns,
            Row row, CompositeDataHandler rowHandler)
    {
        Object rowObj = rowHandler.newInstance(context);
        ElementHandler elementHandler;
        DataHandler leafHandler;
        for (int i = 0; i < rowHandler.getNumberOfElements(); i++)
        {
            elementHandler = rowHandler.getElementHandler(i);
            leafHandler = (DataHandler) elementHandler
                    .getChildInstanceHandler();
            if (leafHandler instanceof LeafDataHandler)
            {
                Cell cell = row.getCell(columns[i]);
                Object leafObj = null;
                if ((cell != null)
                        && (cell.getCellType() != Cell.CELL_TYPE_BLANK))
                {
                    if ((leafHandler instanceof DateHandler) || (leafHandler instanceof DateAndTimeHandler))
                    {
                        switch (cell.getCellType())
                        {
                            case Cell.CELL_TYPE_FORMULA:
                        	if (Cell.CELL_TYPE_STRING == cell.getCachedFormulaResultType())
                        	{
                        		leafObj = convertStringValue(context, leafHandler, cell);
                        		break;
                        	} // Numeric formula falls through to numeric case
                            case Cell.CELL_TYPE_NUMERIC:
                            		Date date = cell.getDateCellValue();
                            		leafObj = ((LeafDataHandler)leafHandler).newInstance(context, date);
                                break;
                            case Cell.CELL_TYPE_STRING:
                                leafObj = convertStringValue(context, leafHandler, cell);
                                break;
                            default:
                                throw new InvalidInputException(
                                        "Illegal cell type for date: row="
                                        + (row.getRowNum()+1) + " cell="
                                        + (columns[i]+1) + " column="+elementHandler.getRole());
                        }
                    }
                    else if (leafHandler instanceof NumberHandler)
                    {
                        switch (cell.getCellType())
                        {
                            case Cell.CELL_TYPE_FORMULA:
                            	
                            	if (Cell.CELL_TYPE_STRING == cell.getCachedFormulaResultType())
                            	{
                            		leafObj = convertStringValue(context, leafHandler, cell);
                            		break;
                            	} // Numeric formula falls through to numeric case
                            case Cell.CELL_TYPE_NUMERIC:
                                double number = cell.getNumericCellValue();
                                if (Double.isNaN(number))
                                    leafObj = null;
                                else
                                    leafObj = ((NumberHandler) leafHandler)
                                            .newInstance(context, number);
                                break;
                            case Cell.CELL_TYPE_STRING:
                                convertStringValue(context, leafHandler, cell);
                                break;
                            case Cell.CELL_TYPE_BLANK:
                            	leafObj = null;
                            	break;
                            default:
                                throw new InvalidInputException(
                                        "Illegal cell type for number: row="
                                                + (row.getRowNum()+1) + " cell="
                                                + (columns[i]+1) + " column="+elementHandler.getRole());
                        }
                    }
                    else 
                    {
                        String text = getCellContentAsString(cell);
                        if (text == null)
                        	leafObj = null;
                        else
                        	leafObj = ((LeafDataHandler) leafHandler).newInstance(
                                context, text, false);
                    }
                    if (leafObj != null)
                        elementHandler.set(context, rowObj, leafObj);
                }
            }
            else
            {
                // CORE2 Handle composite structures
            }
        }
        return rowObj;
    }

	private static Object convertStringValue(RuntimeContext context, DataHandler leafHandler,
			Cell cell)
	{
		Object leafObj;
		String stringValue = cell.getStringCellValue();
		if (stringValue == null)
			leafObj = null;
		else
			leafObj = leafHandler.newInstance(context,
		        stringValue, false);
		return leafObj;
	}

    /**
     * @param row
     * @param newRow
     * @param rowHandler
     * @return
     */
    public static void duplicateRow(RuntimeContext context, Sheet sheet, Row row, int newRowNumber)
    {
    	Row newRow = sheet.createRow(newRowNumber);
    	
    	for (short c = 0; c <= row.getLastCellNum(); c++)
    	{
    		Cell cell = row.getCell(c);
    		if (cell != null)
    		{
	    		Cell newCell = newRow.createCell(c);
	    		newCell.setCellStyle(cell.getCellStyle());
	    		newCell.setCellType(cell.getCellType());
    		}
    	}
    }

    /**
     * Checks whether a cell contains a given String as its content. When
     * checking, whitespace is removed as a workaround for a suspected HSSF bug.
     * 
     * @param cell -
     *            a cell whose content is to be compared.
     * @param content -
     *            a String to be compared to the cell's content.
     * @return
     */
    public static boolean compareCellContent(Cell cell, String content)
    {
        return (cell != null)
                && (cell.getCellType() == Cell.CELL_TYPE_STRING)
                && (cell.getStringCellValue().equals(content) || cell
                        .getStringCellValue().trim().equals(content));
    }

    /**
     * @param cell
     * @return
     */
    public static String getCellContentAsString(Cell cell)
    {
    	int cellType = cell.getCellType();
    	if (cellType == Cell.CELL_TYPE_FORMULA)
    		cellType = cell.getCachedFormulaResultType();
        switch (cellType)
        {
            case Cell.CELL_TYPE_BLANK:
                return null;
            case Cell.CELL_TYPE_BOOLEAN:
                return String.valueOf(cell.getBooleanCellValue());
            case Cell.CELL_TYPE_ERROR:
                return "Error(code=" + cell.getErrorCellValue() + ")";
            case Cell.CELL_TYPE_NUMERIC:
                return numberAsString(cell.getNumericCellValue());
            case Cell.CELL_TYPE_STRING:
                return normalize(cell.getStringCellValue());
            default:
                return null; // This should not happen
        }
    }

    /**
     * "Normalize" the string by:
     * - Removing trailing white space (which is somethimes added by HSSF)
     * - Converting empty strings to nulls
     */ 
    private static String normalize(String s)
    {
        int i = s.length() - 1;
        while (i >= 0 && s.charAt(i) == ' ')
            i--;
        if (i >= 0)
            s = s.substring(0, i + 1);
        if (s.length() == 0)
            return null;
        else
            return s;
    }

	private static String numberAsString(double number)
	{
		if (Double.isNaN(number))
			return null;
		else if (number == ((long) number))
			return String.valueOf((long) number);
		else
			return String.valueOf(number);
	}

}
/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.misc;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.ModelException;
import tersus.model.Role;
import tersus.runtime.BinaryValue;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * Create a simple zip archive based on one or more files
 * @author Youval Bronicki
 */
public class Zip extends Plugin
{
	public static final Role FILES = Role.get("<Files>");
	public static final Role ARCHIVE = Role.get("<Zip Archive>");

	private SlotHandler filesTriggger, archiveExit;
	private ElementHandler contentElement;
	private ElementHandler filenameElement;

	public void start(RuntimeContext context, FlowInstance flow)
	{
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		ZipOutputStream zo = new ZipOutputStream(bo);
		try
		{
		for (Object f: (List<?>)filesTriggger.get(context, flow))
		{
			String filename = (String) filenameElement.get(context, f);
			BinaryValue content = (BinaryValue) contentElement.get(context, f);
			ZipEntry entry = new ZipEntry(filename);
			zo.putNextEntry(entry);
			zo.write(content.toByteArray());
			zo.closeEntry();
		}
		zo.close();
		BinaryValue archive = new BinaryValue(bo.toByteArray());
		setExitValue(archiveExit, context, flow, archive);
		}
		catch (IOException e)
		{
			throw new ModelException("Error creating zip archive",e);
		}
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		filesTriggger = getRequiredMandatoryTrigger(FILES, Boolean.TRUE, BuiltinModels.FILE_ID);
		archiveExit = getRequiredExit(ARCHIVE, Boolean.FALSE, BuiltinModels.BINARY_ID);
		InstanceHandler fileHandler = filesTriggger.getChildInstanceHandler();
		contentElement = fileHandler
				.getElementHandler(BuiltinModels.CONTENT_ROLE);

		 filenameElement = fileHandler
				.getElementHandler(BuiltinModels.FILENAME_ROLE);
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.misc;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Path;
import tersus.model.Role;
import tersus.model.validation.Problems;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.ModelIdHelper;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.Misc;

/**
 * @author Youval Bronicki
 *
 */
public class DeepCopy extends Plugin
{

    private static final Role ORIGINAL = Role.get("<Original>");
    private static final Role COPY = Role.get("<Copy>");
    private SlotHandler trigger, exit;
    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        trigger = getRequiredMandatoryTrigger(ORIGINAL, Boolean.FALSE, null);
        exit = getRequiredExit(COPY, Boolean.FALSE, null);
        if (trigger.getChildModelId() == null)
            notifyInvalidModel(trigger.getRole(), Problems.INVALID_TRIGGER, "The type of the trigger is not set");
        if (exit.getChildModelId() == null)
            notifyInvalidModel(trigger.getRole(), Problems.INVALID_EXIT, "The type of the exit is not set");
        if (! Misc.equal(trigger.getChildModelId(), exit.getChildModelId()) && ! BuiltinModels.ANYTHING_ID.equals(exit.getChildModelId()))
            notifyInvalidModel(Path.EMPTY, Problems.INCOMPATIBLE_TYPE, ORIGINAL + " and "+ COPY + " are not compatible");
    }
    public void start(RuntimeContext context, FlowInstance flow)
    {
        Object original = trigger.get(context, flow);
        Object copy = null;
        ModelId id = ModelIdHelper.getModelId(original);
        InstanceHandler handler = context.getModelLoader().getHandler(id);
        copy = handler.deepCopy(context, original);
        setExitValue(exit, context, flow, copy);
    }
}

/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.misc;

import java.util.List;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Role;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.ModelIdHelper;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;


/**
 * @author Youval Bronicki
 *
 */
public class GetElement extends Plugin
{
    private SlotHandler parentTrigger, elementNameTrigger;
    private static final Role PARENT = Role.get("<Parent>");
    private static final Role ELEMENT_NAME = Role.get("<Element Name>");
    private ElementHandler elementHandler;
    private SlotHandler elementExit;
    private static final Role ELEMENT = Role.get("<Element>");

    /* (non-Javadoc)
     * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
        Object parent = parentTrigger.get(context, flow);
        String elementName = (String)elementNameTrigger.get(context, flow);
        ModelId modelId = ModelIdHelper.getCompositeModelId(parent);
        if (modelId == null)
            throw new ModelExecutionException("GetElement - <Parent> expected composite but found " + parent.getClass().getName());
        InstanceHandler handler = context.getModelLoader().getHandler(modelId);
        elementHandler = handler.getElementHandler(elementName);
        if (elementHandler == null)
            throw new ModelExecutionException("No element '"+elementName+"' in + '"+modelId);
        Object value = elementHandler.get(context, parent);
        if (value != null)
        {
       		if (value instanceof List<?>)
       			for (Object item:(List<?>) value)
       			{
       				if (elementExit.isRepetitive())
           	            accumulateExitValue(elementExit, context, flow, item);
       				else
       					setExitValue(elementExit, context, flow, item);
       			}
       		else
       		{
   				if (elementExit.isRepetitive())
       	            accumulateExitValue(elementExit, context, flow, value);
   				else
   					setExitValue(elementExit, context, flow, value);
       		}
        }
    }


    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        parentTrigger = getRequiredMandatoryTrigger(PARENT, Boolean.FALSE, null);
        elementNameTrigger = getRequiredMandatoryTrigger(ELEMENT_NAME, Boolean.FALSE, BuiltinModels.TEXT_ID);
        elementExit = getRequiredExit(ELEMENT, null, null);
    }

}

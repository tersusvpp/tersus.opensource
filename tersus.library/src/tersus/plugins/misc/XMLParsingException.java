/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.misc;

/**
 * @author Youval Bronicki
 *
 */
public class XMLParsingException extends RuntimeException
{

    /**
     * 
     */
    public XMLParsingException()
    {
        super();
    }

    /**
     * @param message
     */
    public XMLParsingException(String message)
    {
        super(message);
    }

    /**
     * @param message
     * @param cause
     */
    public XMLParsingException(String message, Throwable cause)
    {
        super(message, cause);
    }

    /**
     * @param cause
     */
    public XMLParsingException(Throwable cause)
    {
        super(cause);
    }

}

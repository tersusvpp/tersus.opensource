package tersus.plugins.misc;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.DataHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

public class Identical extends Plugin
{
	
	public static Role IDENTICAL = Role.get("<Identical>");
	public static Role NOT_IDENTICAL = Role.get("<Not Identical>");
	private SlotHandler identicalExit;
	private SlotHandler notIdenticalExit;
	
	public void start(RuntimeContext context, FlowInstance flow)
	{	TriggerIterator iterator = new TriggerIterator(context,flow,null);

	// Compare inputs (deep comparison)
	boolean allEqual = true;
	boolean isFirst = true;
	Object firstObject = null;
	while (allEqual && iterator.hasNext()) {
		Object current = iterator.next();
		if (isFirst)
		{
			firstObject = current; 
			isFirst = false;
		}
		else
		{	
			allEqual = (current==firstObject);
		}
	}		

	if (allEqual) {
		if (identicalExit != null)
			setExitValue(identicalExit, context, flow, firstObject);
	}
	else {
		if (notIdenticalExit != null)
			chargeEmptyExit(context, flow, notIdenticalExit);
	}
}
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		
		identicalExit = getNonRequiredExit(IDENTICAL, Boolean.FALSE, null);
		notIdenticalExit = getNonRequiredExit(NOT_IDENTICAL, Boolean.FALSE, BuiltinModels.NOTHING_ID);

		for (int i = 0; i < triggers.length; i++)
			checkTypeMismatch(triggers[i], identicalExit);
	}
}

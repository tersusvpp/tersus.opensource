/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.misc;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.webapp.ContextPool;

/**
 * @author Youval Bronicki
 * 
 */
public class GetContentType extends Plugin
{
	public static final Role FILE_NAME = Role.get("<File Name>");
	public static final Role CONTENT_TYPE = Role.get("<Content Type>");

	private SlotHandler fileNameTrigger, contentTypeExit;

	public void start(RuntimeContext context, FlowInstance flow)
	{
		String fileName = (String) fileNameTrigger.get(context, flow);
		String contentType = ((ContextPool) (context.getContextPool())).getServletContext()
				.getMimeType(fileName);
		setExitValue(contentTypeExit, context, flow, contentType);
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		fileNameTrigger = getRequiredMandatoryTrigger(FILE_NAME, Boolean.FALSE,
				BuiltinModels.TEXT_ID);
		contentTypeExit = getRequiredExit(CONTENT_TYPE, Boolean.FALSE, BuiltinModels.TEXT_ID);
	}
}

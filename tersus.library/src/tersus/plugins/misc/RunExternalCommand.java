/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.misc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.model.validation.Problems;
import tersus.runtime.BinaryValue;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.Misc;
import tersus.util.ProcessRunner;
/**
 * An atomic flow handler that concatenates a number of input strings
 * 
 * @author Youval Bronicki
 * 
 */
public class RunExternalCommand extends Plugin
{
    private static final Role COMMAND_ROLE = Role.get("<Command>");

    private static final Role OK_ROLE = Role.get("<OK>");
    private static final Role OUTPUT = Role.get("<Output>");
    private static final Role TIMEOUT = Role.get("<Timeout>");

    private SlotHandler commandTrigger, timeoutTrigger;

    ArrayList<SlotHandler> argumentTriggers;

    SlotHandler okExit, outputExit;

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {

        String command = (String) commandTrigger.get(context, flow);
        ArrayList<String> args = new ArrayList<String>(argumentTriggers.size() + 1);
        for (int i = 0; i < argumentTriggers.size(); i++)
        {
            SlotHandler trigger = (SlotHandler) argumentTriggers.get(i);
            Object value = trigger.get(context, flow);
            if (value != null)
                args.add(String.valueOf(value));
        }
        String[] cmdArray = new String[args.size() + 1];
        cmdArray[0] = command;
        for (int i = 0; i < args.size(); i++)
            cmdArray[i + 1] = (String) args.get(i);

        int status;
        try
        {
            long timeoutMilliseconds = context.getRemainingTimeMilliseconds();
            Number explicitTimeoutSeconds = null;
            if (timeoutTrigger != null)
                explicitTimeoutSeconds = (Number)getTriggerValue(context, flow, timeoutTrigger);
            if (explicitTimeoutSeconds != null && explicitTimeoutSeconds.value > 0)
                timeoutMilliseconds = Math.min( timeoutMilliseconds, Math.round(explicitTimeoutSeconds.value*1000) );
            ProcessRunner runner = new ProcessRunner();
            status = runner.run(cmdArray,timeoutMilliseconds);
            if (outputExit != null)
            {
                byte[] output = runner.getOutputStream().toByteArray();
                if (BuiltinModels.BINARY_ID.equals(outputExit.getChildModelId()))
                {
                    setExitValue(outputExit, context, flow, new BinaryValue(output));
                }
                else if (BuiltinModels.TEXT_ID.equals(outputExit.getChildModelId()))
                {
                    setExitValue(outputExit, context, flow, new String(output));
                }

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new ModelExecutionException("Failed to execute command '" + command + "' - " + e.getMessage()
                    + "\nArguments:" + Misc.concatenateList(args, ","));
        }
        if (status == 0)
        {
            chargeEmptyExit(context, flow, okExit);
        }
        else
            throw new ModelExecutionException("Command '" + command + "' terminated with status " + status
                    + "\nArguments:" + Misc.concatenateList(args, ","));
    }


    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
     */
    public void resume(RuntimeContext context, FlowInstance flow)
    { // Nothing to do (This is an action)
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
     * 
     * Create a triggers sorted by lexicographically (by role)
     */
    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        argumentTriggers = new ArrayList<SlotHandler>();
        commandTrigger = getRequiredMandatoryTrigger(COMMAND_ROLE, Boolean.FALSE, BuiltinModels.TEXT_ID);
        timeoutTrigger = getNonRequiredTrigger(TIMEOUT, Boolean.FALSE, BuiltinModels.NUMBER_ID);
        outputExit = getNonRequiredExit(OUTPUT, Boolean.FALSE, null);
        if (outputExit != null && !BuiltinModels.BINARY_ID.equals(outputExit.getChildModelId())
                && !BuiltinModels.TEXT_ID.equals(outputExit.getChildModelId()))
            notifyInvalidModel(outputExit.getRole(), Problems.INVALID_EXIT, "Exit type must be binary or test");

        for (int i = 0; i < triggers.length; i++)
        {
            if ( !triggers[i].isEmpty() && ! triggers[i].getRole().isDistinguished())
                argumentTriggers.add(triggers[i]);

            Comparator<SlotHandler> c = new Comparator<SlotHandler>()
            {

                public int compare(SlotHandler trigger1, SlotHandler trigger2)
                {
                    return (trigger1.getRole().toString().compareTo(trigger2.getRole().toString()));
                }
            };
            Collections.sort(argumentTriggers, c);
            okExit = getRequiredExit(OK_ROLE, Boolean.FALSE, BuiltinModels.NOTHING_ID);
        }

    }
}


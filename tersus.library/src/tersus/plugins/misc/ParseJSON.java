/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.misc;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.BooleanHandler;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.MapHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Number;
import tersus.runtime.NumberHandler;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.InvalidInputException;
import tersus.webapp.JavaScriptFormat;

/**
 * An atomic flow handler that parses a JSON String (JSON stands for Javascript Object Notation -
 * see www.json.org)
 * 
 * @author Youval Bronicki
 * 
 */

public class ParseJSON extends Plugin
{
	private static final Role INPUT_ROLE = Role.get("<JSON Text>");
	private static final Role IGNORE_UNKNOWN = Role.get("<Ignore Unknown Elements>");
	private static final Role OUTPUT_ROLE = Role.get("<Value>");

	SlotHandler trigger, exit, ignoreUnknown;

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		String text = (String) trigger.get(context, flow);
		boolean ignoreUnknownElements = getTriggerValue(ignoreUnknown, context, flow, false);

		 if (getPluginVersion() < 1 && ! (exit.getChildInstanceHandler() instanceof MapHandler))
		{
			if (exit.isRepetitive())
			{
				List<Object> values = JavaScriptFormat.parseList(context,
						exit.getChildInstanceHandler(), text, ignoreUnknownElements, true);
				for (Object value : values)
				{
					accumulateExitValue(exit, context, flow, value);
				}
			}
			else
			{
				Object value = JavaScriptFormat.parseValue(context, exit.getChildInstanceHandler(),
						text, ignoreUnknownElements, true);
				if (value == null)
					throw new ModelExecutionException("ParseJSON failed - no object found");
				setExitValue(exit, context, flow, value);
			}
		}
		else
		{
			try
			{
				if (exit.isRepetitive())
				{
					JSONArray a = new JSONArray(text);
					for (int i = 0; i < a.length(); i++)
					{
						Object obj = a.get(i);
						Object value = convert(context, obj, exit, ignoreUnknownElements);
						accumulateExitValue(exit, context, flow, value);
					}
				}
				else
				{
					JSONObject obj = new JSONObject(text);
					Object value = convert(context, obj, exit, ignoreUnknownElements);
					setExitValue(exit, context, flow, value);
				}
			}
			catch (JSONException e)

			{
				throw new ModelExecutionException("Failed to parse JSON", e);
			}
		}
	}

	public static Object convert(RuntimeContext context, Object obj, ElementHandler elementHandler, boolean ignoreUnknownElements) throws JSONException
	{
		//TODO - merge with SAPIManager.toInstance
		InstanceHandler handler = elementHandler.getChildInstanceHandler();
		if (handler instanceof LeafDataHandler)
		{
			if (handler.isInstance(obj)) /* Includes output of JSONOBject as Map */
				return obj;
			else if (handler instanceof NumberHandler && obj instanceof java.lang.Number)
				return new Number(((java.lang.Number)obj).doubleValue());
			else if (obj instanceof String)
				return ((LeafDataHandler)handler).newInstance(context,(String)obj, elementHandler.getProperties(), false);
		}
		else if (obj instanceof JSONObject) /* Composite data structures, possibly including display data  (not tested) */
		{
			JSONObject o = (JSONObject) obj;
			
			Object value = handler.newInstance(context);
			for (String key: JSONObject.getNames(o))
			{
				ElementHandler childHandler = handler.getElementHandler(key);
				if (childHandler == null)
				{	
					if (ignoreUnknownElements )
						continue;
					else	
						throw new InvalidInputException("Unknown element "+key+" in "+handler.getName());
				}
				if (o.isNull(key))
					continue;
				Object co = o.get(key);
				if (childHandler.isRepetitive())
				{
					if (!(co instanceof JSONArray))
						throw new InvalidInputException(childHandler + " is supposed to be repetitive but found "+co.getClass().getName());
					JSONArray ca = (JSONArray) co;
					for (int i=0;i<ca.length();i++)
					{
						Object childValue = convert(context, ca.get(i), childHandler, ignoreUnknownElements);
						childHandler.accumulate(context, value, childValue);
					}
					
				}
				else
				{
					Object childValue = convert(context, co, childHandler, ignoreUnknownElements);
					childHandler.set(context,  value, childValue);
				}
			}
			return value;
		}
		else if (obj instanceof Map<?,?>) /* Composite data structures, possibly including display data  (not tested) */
		{
			Map<?,?> m = (Map<?,?>) obj;
			
			Object value = handler.newInstance(context);
			for (Object keyO: m.keySet())
			{
				if (! (keyO instanceof String))
					throw new ModelExecutionException("Can convert map elements with non-String key (found "+keyO.getClass().getName()+" )");
				String key=(String) keyO;
				ElementHandler childHandler = handler.getElementHandler(key);
				if (childHandler == null)
				{	
					if (ignoreUnknownElements )
						continue;
					else	
						throw new InvalidInputException("Unknown element "+key+" in "+handler.getModelId());
				}
				Object co = m.get(key);
				if (co == null)
					continue;
				if (childHandler.isRepetitive())
				{
					if (!(co instanceof Collection<?>))
						throw new InvalidInputException(childHandler + " is supposed to be repetitive but found "+co.getClass().getName());
					for (Object ci: ((Collection<?>)co))
					{
						Object childValue = convert(context, ci, childHandler, ignoreUnknownElements);
						childHandler.accumulate(context, value, childValue);
					}
					
				}
				else
				{
					Object childValue = convert(context, co, childHandler, ignoreUnknownElements);
					childHandler.set(context,  value, childValue);
				}
			}
			return value;
		}
		throw new ModelExecutionException("Parse error: found "+obj.getClass().getName()+ " for "+handler.getClass().getName());
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		trigger = getRequiredMandatoryTrigger(INPUT_ROLE, Boolean.FALSE, BuiltinModels.TEXT_ID);
		ignoreUnknown = getNonRequiredTrigger(IGNORE_UNKNOWN, Boolean.FALSE,
				BuiltinModels.BOOLEAN_ID);
		exit = getRequiredExit(OUTPUT_ROLE, null, null);
	}

}

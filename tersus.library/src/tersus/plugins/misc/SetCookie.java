/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.misc;

import java.util.Date;
import java.util.GregorianCalendar;

import javax.servlet.http.Cookie;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.Misc;
import tersus.webapp.WebContext;

public class SetCookie extends Plugin
{
    private static final Role NAME = Role.get("<Name>");

    private static final Role VALUE = Role.get("<Value>");
    private static final Role DOMAIN = Role.get("<Domain>");
    private static final Role PATH = Role.get("<Path>");
    private static final Role EXPIRES = Role.get("<Expires>");
    private static final Role SECURE = Role.get("<Secure>");

    private SlotHandler nameTrigger, valueTrigger, expiresTrigger, domainTrigger, pathTrigger, secureTrigger;

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        nameTrigger = getRequiredMandatoryTrigger(NAME, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        valueTrigger = getRequiredTrigger(VALUE, Boolean.FALSE, null);
        expiresTrigger = getNonRequiredTrigger(EXPIRES, Boolean.FALSE, null);
        pathTrigger = getNonRequiredTrigger(PATH, Boolean.FALSE, BuiltinModels.TEXT_ID);
        domainTrigger = getNonRequiredTrigger(DOMAIN, Boolean.FALSE, BuiltinModels.TEXT_ID);
        secureTrigger = getNonRequiredTrigger(SECURE, Boolean.FALSE, BuiltinModels.BOOLEAN_ID);            
    }

    public void start(RuntimeContext context, FlowInstance flow)
    {
        if (context instanceof WebContext)
        {
            String name = (String) nameTrigger.get(context, flow);
            name = Misc.sqlize(name);

            int maxAge = Integer.MAX_VALUE;
            if (expiresTrigger != null)
            {
                Object ex = expiresTrigger.get(context, flow);
                if (ex instanceof Date)
                	maxAge = (int) ((((Date) ex).getTime() - System.currentTimeMillis())) / 1000;
            }
            else
               	if (getPluginVersion() >= 1)
               		maxAge = Integer.MIN_VALUE;
            
            String valueStr = null;
            Object value = valueTrigger.get(context, flow);
            if (value != null)
            {
            	StringBuffer buffer = new StringBuffer();
            	valueTrigger.format(context, value, buffer);
            	valueStr = buffer.toString();
            }
            else
            	maxAge = 0;

            Cookie cookie = new Cookie(name, valueStr);
            cookie.setMaxAge(maxAge);

            String domain = getTriggerText(context, flow, domainTrigger);
            if (domain != null)
            	cookie.setDomain(domain);

            String path = getTriggerText(context, flow, pathTrigger);
            if (path != null)
            	cookie.setPath(path);

            cookie.setSecure(getTriggerValue(secureTrigger, context, flow, false));

            ((WebContext) context).getResponse().addCookie(cookie);
        }
    }
}

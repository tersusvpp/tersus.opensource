/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.misc;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.DataHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that checks whether all its inputs are equal
 * (deep comparison for composite data types, with some tolerance for numbers).
 * 
 * @see instanceDeepCompare
 * 
 * @author Ofer Brandes
 *
 */

// BUG1 Since numbers are compared with tolerance, comparing each item to the previous might be wrong (X~Y~Z does not necessarily imply X~Z).
// BUG3 Assuming the implementation of composite data types as arrays.

public class Equal extends Plugin
{
	private static final Role EQUAL_ROLE = Role.get("<Yes>");
	private static final Role DIFFERENT_ROLE = Role.get("<No>");

	SlotHandler equalExit, differentExit;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		// Accumulate inputs
		TriggerIterator iterator = new TriggerIterator(context,flow,null);

		// Compare inputs (deep comparison)
		boolean allEqual = true;
		Object previous = null;
		while (iterator.hasNext()) {
			Object current = iterator.next();
			if (previous != null) {
				if (!(allEqual = DataHandler.instanceDeepCompare(current,previous)))
					break;
			}
			previous = current;
		}		

		if (allEqual) {
			if (equalExit != null)
				setExitValue(equalExit, context, flow, previous);
		}
		else {
			if (differentExit != null)
				chargeEmptyExit(context, flow, differentExit);
		}
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		
		equalExit = getNonRequiredExit(EQUAL_ROLE, Boolean.FALSE, null);
		differentExit = getNonRequiredExit(DIFFERENT_ROLE, Boolean.FALSE, BuiltinModels.NOTHING_ID);

		for (int i = 0; i < triggers.length; i++)
			checkTypeMismatch(triggers[i], equalExit);
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.misc;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Path;
import tersus.model.Role;
import tersus.model.validation.Problems;
import tersus.runtime.ElementHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * @author Youval Bronicki
 *
 */
public class NotifyError extends Plugin
{
	private static final Role ERROR_ROLE = Role.get("<Error>");
	static final Role MESSAGE_ROLE = Role.get("<Message>");
	static final Role DETAILS_ROLE = Role.get("<Details>");

	SlotHandler errorTrigger, messageTrigger, detailsTrigger;
	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Object error = getTriggerValue(context, flow, errorTrigger);
		String message = getTriggerText(context, flow, messageTrigger);
		
		if (error instanceof String && message == null)
		{
			message = (String)error;
			error = null;
		}
		String details = getTriggerText(context, flow, detailsTrigger);
		if (error == null && message == null)
			return;
		
		if (message == null)
		{
			ElementHandler messageElement = errorTrigger.getChildInstanceHandler().getElementHandler(BuiltinModels.ERROR_MESSAGE);
			message = (String)messageElement.get(context, error);
		}
		if (details == null && error != null)
		{
			ElementHandler detailsElement = errorTrigger.getChildInstanceHandler().getElementHandler(BuiltinModels.ERROR_DETAILS);
			if (detailsElement == null) // Backwards compatibility (Error/Cause element)
				detailsElement = errorTrigger.getChildInstanceHandler().getElementHandler(BuiltinModels.ERROR_CAUSE);
			details = (String)detailsElement.get(context, error);
		}
		throw new EngineException(message, details, null);
	}
	
	/* (non-Javadoc)
	 * @see tersus.runtime.InstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		errorTrigger = getNonRequiredMandatoryTrigger(ERROR_ROLE, Boolean.FALSE, null);
		if (errorTrigger != null && ! (BuiltinModels.ERROR_ID.equals(errorTrigger.getChildModelId()) || BuiltinModels.TEXT_ID.equals(errorTrigger.getChildModelId())))
			notifyInvalidModel(ERROR_ROLE, Problems.INVALID_TRIGGER_DATA_TYPE, ERROR_ROLE + " must be of type Text or Error");
		messageTrigger = getNonRequiredTrigger(MESSAGE_ROLE,Boolean.FALSE,BuiltinModels.TEXT_ID);
		if (errorTrigger == null && messageTrigger == null)
			notifyInvalidModel(Path.EMPTY, Problems.MISSING_TRIGGER, "Either " + ERROR_ROLE + " or "
					+ MESSAGE_ROLE + " must be specified.");
		detailsTrigger = getNonRequiredTrigger(DETAILS_ROLE, Boolean.FALSE, BuiltinModels.TEXT_ID);
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.misc;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;


import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.FileUtils;

/**
 * @author Youval Bronicki
 * 
 */
public class GetFileAttributes extends Plugin
{

    private static final Role LAST_MODIFIED = Role.get("<Last Modified>");
    private static final Role LENGTH = Role.get("<Length>");
    private static final Role READABLE = Role.get("<Readable>");
    private static final Role WRITABLE = Role.get("<Writable>");
    private static final Role URL = Role.get("<URL>");
    private SlotHandler urlTrigger;
    private SlotHandler lastModifiedExit;
    private SlotHandler lengthExit;
    private SlotHandler readableExit;
    private SlotHandler writableExit;
    private static final Role MISSING = Role.get("<Missing>");
    private SlotHandler missingExit;

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext,
     *      tersus.runtime.FlowInstance)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
        String urlString = (String)urlTrigger.get(context ,flow);
	    URL url;
        if (urlString.startsWith("smb:"))
            getSMBFileAttributes(urlString,context,flow);
        else
        {
        try
        {
            url = new URL(urlString);
        }
        catch (MalformedURLException e)
        {
            throw new ModelExecutionException("Invalid URL '"+urlString+"'");
        }
        if ("file".equals(url.getProtocol()))
	    {
	        File file = FileUtils.URLtoFile(url);
	        if (file.exists())
	        {
		        if (lastModifiedExit != null)
		            setExitValue(lastModifiedExit, context, flow,  new Date(file.lastModified()));
		        if (lengthExit != null)
		            setExitValue(lengthExit, context, flow, new Number(file.length()));
		        if (readableExit != null)
		            setExitValue(readableExit, context, flow, Boolean.valueOf(file.canRead()));
		        if (writableExit != null)
		            setExitValue(writableExit, context, flow, Boolean.valueOf(file.canWrite()));
	        }
	        else
	            if (missingExit != null)
	                setExitValue(missingExit, context, flow, urlString);
	    }
        else
            throw new ModelExecutionException("Invalid URL  '"+urlString+"' (not a 'file:..' URL)");
        }
    }

    private void getSMBFileAttributes(String url, RuntimeContext context,
            FlowInstance flow)
    {    
        try
        {
            SmbFile file = new SmbFile(url);
            if (file.exists())
            {
                if (lastModifiedExit != null)
                    setExitValue(lastModifiedExit, context, flow, new Date(file
                            .lastModified()));
                if (lengthExit != null)
                    setExitValue(lengthExit, context, flow, new Number(file
                            .length()));
                if (readableExit != null)
                    setExitValue(readableExit, context, flow, Boolean
                            .valueOf(file.canRead()));
                if (writableExit != null)
                    setExitValue(writableExit, context, flow, Boolean
                            .valueOf(file.canWrite()));
            }
            else if (missingExit != null)
                setExitValue(missingExit, context, flow, url);
        }
        catch (SmbException e)
        {
            throw new ModelExecutionException("Failed to access remote file",
                    url.toString(), e);
        }
        catch (MalformedURLException e)
        {
            throw new ModelExecutionException("Failed to access remote file",
                    url.toString(), e);
        }
    }

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        urlTrigger = getRequiredMandatoryTrigger(URL, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        lastModifiedExit = getNonRequiredExit(LAST_MODIFIED, Boolean.FALSE,
                BuiltinModels.DATE_AND_TIME_ID);
        lengthExit = getNonRequiredExit(LENGTH, Boolean.FALSE,
                BuiltinModels.NUMBER_ID);
        readableExit = getNonRequiredExit(READABLE, Boolean.FALSE,
                BuiltinModels.BOOLEAN_ID);
        writableExit = getNonRequiredExit(WRITABLE, Boolean.FALSE,
                BuiltinModels.BOOLEAN_ID);
        missingExit = getNonRequiredExit(MISSING, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
    }
}

/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.misc;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.webapp.ContextPool;

/**
 * Get the value of a parameter from the tomcat context file.
 * @author Liat Shiff
 */
public class GetDeploymentParameter extends Plugin
{
	public static final Role NAME = Role.get("<Name>");
	public static final Role VALUE = Role.get("<Value>");

	private SlotHandler nameTrigger, valueExit;

	public void start(RuntimeContext context, FlowInstance flow)
	{
		String parameterName = (String) nameTrigger.get(context, flow);
		String value = ((ContextPool) (context.getContextPool())).getServletContext()
				.getInitParameter(parameterName);
		setExitValue(valueExit, context, flow, value);
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		nameTrigger = getRequiredMandatoryTrigger(NAME, Boolean.FALSE, BuiltinModels.TEXT_ID);
		valueExit = getRequiredExit(VALUE, Boolean.FALSE, BuiltinModels.TEXT_ID);
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software LFrpctd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.misc;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import tersus.model.BuiltinModels;
import tersus.model.DataElement;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Path;
import tersus.model.Repository;
import tersus.runtime.ElementHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.ModelIdHelper;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.XMLSerializer;
import tersus.util.SpecialRoles;

/**
 * @author oferb
 * 
 * This class does not properly support all theoretical WSDL bindings
 * (e.g. we assume "literal"/"encoded" and "document"/"rpc" distinctions
 * are made at the operation level, not per slot).
 * 
 * For a discussion of WSDL styles, refer to http://www-128.ibm.com/developerworks/webservices/library/ws-whichwsdl/?ca=dgr-devx-WebServicesMVP03.
 *
 * Compare with 'SerializeXML'.
 */

public class CallWebService extends Plugin

{
	Repository repository = null;

	private final boolean NO_NULL_NAME_SPACE = true;
	private String baseUrl = null;
	

	SlotHandler protocolTrigger;
	SlotHandler bindingTrigger;
	SlotHandler aliasesTrigger;
	SlotHandler urlTrigger;
	SlotHandler soapActionTrigger;
	SlotHandler methodTrigger;
	SlotHandler headerTrigger;
	SlotHandler headerExit;
	SlotHandler soapExit = null;

	// List of commonly used namespace aliases is based on http://www.w3.org/TR/wsdl#_notational
	private final String NS_XSD = "http://www.w3.org/2001/XMLSchema";
//	       XMLSerializer.NS_XSI = "http://www.w3.org/2001/XMLSchema-instance";
	private final String NS_SOAPENV = "http://schemas.xmlsoap.org/soap/envelope/";
//	private final String NS_SOAPENC = "http://schemas.xmlsoap.org/soap/encoding/";
//	private final String NS_MIME = "http://schemas.xmlsoap.org/wsdl/mime/";
//	private final String NS_HTTP = "http://schemas.xmlsoap.org/wsdl/http/";
//	private final String NS_SOAP = "http://schemas.xmlsoap.org/wsdl/soap/";
//	private final String NS_WSDL = "http://schemas.xmlsoap.org/wsdl/";
	HashMap nameSpaceAliases = null;
	String soapEnvelopeAlias = null;
	String schemaInstanceAlias = null; // Will become redundant when XMLSerializer is used
	
	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
        Logger log = context.getContextPool().getLogger(getClass().getName());
		// Get URL, soap action and method name
		String url = (String) urlTrigger.get(context,flow);
		String soapAction = (soapActionTrigger == null ? null : (String) soapActionTrigger.get(context,flow));
		String methodName = (String) methodTrigger.get(context,flow);

		// Get binding use and style
		String bindingUse = SpecialRoles.WSDL_LITERAL;		// The default is "literal"
		String bindingStyle = SpecialRoles.WSDL_DOCUMENT;	// The default is "document"
		if (bindingTrigger != null)
		{
			List values = (List) bindingTrigger.get(context,flow);
			for (int i = 0; i < values.size(); i++)
			{
				String value = (String) values.get(i);
				if (SpecialRoles.WSDL_ENCODED.equals(value))	// The default is "literal"
					bindingUse = value;
				if (SpecialRoles.WSDL_RPC.equals(value))		// The default is "document"
					bindingStyle = value;
			}
		}
		
		int p = Math.max(url.lastIndexOf('/'),url.lastIndexOf('\\'));
		baseUrl = url.substring(0,p+1);

		// Prepare SOAP or HTTP request
		try
		{
			if (protocolTrigger == null) // SOAP
			{
				// Serialize input(s) as SOAP message(s)
				String soapRequest = prepareSoapRequest (methodName, bindingUse, bindingStyle, flow, context);
				if (log.isDebugEnabled()) 
                {
                    log.debug("SOAP Request:\n"+soapRequest);
                }

				// Prepare HTTP Request
				if (soapAction == null)
					soapAction = baseUrl + methodName;	// Default in case we haven't explicitly saved it while importing the WSDL

				HttpURLConnection huc = prepareHttpRequest (url, soapAction, soapRequest);
				
				// Invoke HTTP Request
				huc.connect();
				int code = huc.getResponseCode();
				String message = huc.getResponseMessage();
				if ((code < 200) || (code >= 300))
				{
					System.err.println ("Failed to invoke service (code "+code+"): "+message);
					throw new EngineException("Failed to invoke service (code " + code + "): " + message, soapRequest, null);
				}
	
				// Get HTTP Response
				byte[] response = HttpRequest.getHttpResponse(huc);
				if (log.isDebugEnabled()) 
                {

                    log.debug("HTTP Response:\n"+response);
                }

				// Parse XML Response
				if (SpecialRoles.WSDL_RPC.equals(bindingStyle))
					parseSoapResponse (response, context, flow, soapExit);
				else
				{
					InstanceHandler outputHandler = soapExit.getChildInstanceHandler();
					Object value = outputHandler.newInstance(context);
					List modelElements = outputHandler.getModel().getElements();
					DataElement childElement = (DataElement) modelElements.get(0); // Won't work if there are multiple parts in the message!
					ElementHandler childHandler = outputHandler.getElementHandler(childElement.getRole());
					parseSoapResponse (response, context, value, childHandler);
					soapExit.set(context, flow, value);
				}
			}
			else // HTTP
			{
				
			}
		}
		catch (MalformedURLException e)
		{
			throw new EngineException("Error in connecting to URL", "URL='"+url+"'", e);
		}
		catch (IOException e)
		{
			throw new EngineException("Error in connecting to URL", "URL='"+url+"'", e);
		}
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.InstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		protocolTrigger = getNonRequiredMandatoryTrigger(SpecialRoles.PROTOCOL_TRIGGER_ROLE, Boolean.FALSE, BuiltinModels.TEXT_ID);
		if (protocolTrigger != null)
			notifyInvalidModel(protocolTrigger.getRole(), "Unsopported protocol", "Currently only SOAP is implemented");
		
		bindingTrigger = getRequiredTrigger(SpecialRoles.BINDING_TRIGGER_ROLE, Boolean.TRUE, BuiltinModels.TEXT_ID); // if none, default is "document", "literal"
		aliasesTrigger = getNonRequiredTrigger(SpecialRoles.ALIASES_TRIGGER_ROLE, Boolean.FALSE, BuiltinModels.TEXT_ID);
		urlTrigger = getRequiredMandatoryTrigger(SpecialRoles.URL_TRIGGER_ROLE, Boolean.FALSE, BuiltinModels.TEXT_ID);
		soapActionTrigger = getNonRequiredTrigger(SpecialRoles.SOAP_ACTION_TRIGGER_ROLE, Boolean.FALSE, BuiltinModels.TEXT_ID);
		methodTrigger = getRequiredMandatoryTrigger(SpecialRoles.METHOD_TRIGGER_ROLE, Boolean.FALSE, BuiltinModels.TEXT_ID);
		headerTrigger = getNonRequiredTrigger(SpecialRoles.HEADER_TRIGGER_ROLE, Boolean.FALSE, null);
		
		for (int i=0; i<triggers.length; i++)
			if ((triggers[i] != urlTrigger) && (triggers[i] != soapActionTrigger) && (triggers[i] != methodTrigger))
				checkDataType (triggers[i], null, true);

		headerExit = getNonRequiredTrigger(SpecialRoles.HEADER_EXIT_ROLE, Boolean.FALSE, null);

		if (protocolTrigger == null) // SOAP
		{
			// Expecting a single exit (which is not always true!)
			for (int i = 0; i < exits.length; i++)
			{
				if ((exits[i] != headerExit) && !exits[i].isEmpty())
					if (soapExit == null)
						soapExit = exits[i];
					else
					{
						// Temporary patch - leave the one that looks better
						boolean prevIsResponse = (soapExit.getRole().toString().indexOf("Response") >= 0);
						boolean thisIsResponse = (exits[i].getRole().toString().indexOf("Response") >= 0);
						if (prevIsResponse == thisIsResponse)
							notifyInvalidModel(Path.EMPTY, "Mutliple outputs", "Only a single SOAP output is expected");
						else if (thisIsResponse)
							soapExit = exits[i];
					}
			}
			if (soapExit == null)
				notifyInvalidModel(Path.EMPTY, "No output", "A single SOAP output is expected");
		}

		repository = (Repository)model.getRepository();
		
		nameSpaceAliases = new HashMap();
		nameSpaceAliases.put(NS_XSD, "xsd");
		nameSpaceAliases.put(XMLSerializer.NS_XSI, "xsi");
		nameSpaceAliases.put(NS_SOAPENV, "soapenv");
//		nameSpaceAliases.put(NS_SOAPENC, "soapenc");
//		nameSpaceAliases.put(NS_MIME, "mime");
//		nameSpaceAliases.put(NS_HTTP, "http");
//		nameSpaceAliases.put(NS_SOAP, "soap");
//		nameSpaceAliases.put(NS_WSDL, "wsdl");
		// In the future we may want to dynamically add aliases (for other namespaces used in the request)

		schemaInstanceAlias = (String) nameSpaceAliases.get(XMLSerializer.NS_XSI);
		soapEnvelopeAlias = (String) nameSpaceAliases.get(NS_SOAPENV);		
	}

	private String prepareSoapRequest (String methodName, String bindingUse, String bindingStyle, FlowInstance flow, RuntimeContext context) throws IOException
	{
		InstanceHandler flowHandler = flow.getFlowHandler();
		Object headerValue = null;
		ElementHandler headerElementHandler = null;
		if (headerTrigger != null)
		{
			headerValue = headerTrigger.get(context,flow);
			headerElementHandler = flowHandler.getElementHandler(headerTrigger.getRole());
		}

		XMLSerializer serializer = new XMLSerializer((NO_NULL_NAME_SPACE ? baseUrl : null), nameSpaceAliases, bindingUse, context);

		boolean useAliases = (aliasesTrigger != null);
		if (useAliases)
		{
			if (headerTrigger != null)
				serializer.alsoListUsedNameSpaces (headerElementHandler);
	
			for (int i = 0; i < triggers.length; i++)
				if ((triggers[i] != bindingTrigger) && (triggers[i] != aliasesTrigger) && (triggers[i] != urlTrigger) && (triggers[i] != soapActionTrigger) && (triggers[i] != methodTrigger) && (triggers[i] != headerTrigger) && !triggers[i].isEmpty())
					serializer.alsoListUsedNameSpaces (triggers[i]);
		}

		serializer.addXmlStartElementTag (null, NS_SOAPENV, true, "Envelope", true, null, null);

		if (headerTrigger != null) // This hasn't been tested yet (look for an example)
			serializer.addXmlElement (null, NS_SOAPENV, false, "Header", true, headerValue, headerElementHandler);

		serializer.addXmlStartElementTag (null, NS_SOAPENV, false, "Body", true, null, null);

		for (int i = 0; i < triggers.length; i++)
			if ((triggers[i] != bindingTrigger) && (triggers[i] != aliasesTrigger) && (triggers[i] != urlTrigger) && (triggers[i] != soapActionTrigger) && (triggers[i] != methodTrigger) && (triggers[i] != headerTrigger) && !triggers[i].isEmpty())
			{
				// We expect to arrive here only once - only 1 "content trigger" is expected
				if (SpecialRoles.WSDL_RPC.equals(bindingStyle))
				{
					// "rpc" - each part appears inside a wrapper element named identically to the operation name 
					serializer.addXmlSerialization (null, methodName, useAliases, triggers[i], flow);
				}
				else
				{
					// "document" - the message parts appear directly under the SOAP Body element
					Object value = triggers[i].get(context,flow);
					ModelId triggerTypeId = ModelIdHelper.getCompositeModelId(value);
					InstanceHandler handler = context.getModelLoader().getHandler(triggerTypeId);
					String nameSpace = null; // Check
					serializer.addXmlChildElements (nameSpace, useAliases, value, handler);
				}
			}

		serializer.addXmlEndElementTag(); // End of "Body"

		serializer.addXmlEndElementTag(); // End of "Envelope"

		return serializer.done();
	}

	private HttpURLConnection prepareHttpRequest (String url, String soapAction, String soapRequest) throws MalformedURLException, IOException
	{
		HttpURLConnection huc = (HttpURLConnection) (new URL(url)).openConnection();
		byte[] bytes = soapRequest.getBytes(HttpRequest.UTF8);
		huc.setRequestMethod("POST");
		huc.setRequestProperty ("SOAPAction", "\""+soapAction+"\"");
		huc.setRequestProperty ("Content-Type", "text/xml; charset=utf-8");
		huc.setRequestProperty ("Content-Length", ""+bytes.length);
		huc.setDoOutput(true);
		OutputStream os = huc.getOutputStream();
		os.write(bytes);
		
		return huc;
	}

	private void parseSoapResponse (byte[] response, RuntimeContext context, Object parent, ElementHandler outputHandler)
	{
		try
		{
			XMLReader xr = XMLReaderFactory.createXMLReader();
			XmlDocumentHandler parser = new XmlDocumentHandler(context, parent, outputHandler, true, response);
			xr.setContentHandler(parser);
			xr.setErrorHandler(parser);
//			String r = new String(response);
			xr.parse(new InputSource(new ByteArrayInputStream(response)));
		}
		catch (IOException e)
		{
			throw new EngineException("XML Parsing Failed", null, e);
		}
		catch (SAXException e)
		{
			throw XmlDocumentHandler.catchSAXException (context, e);	        
		}
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.misc;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.plugins.misc.Excel.TableTitleRow;
import tersus.runtime.BinaryValue;
import tersus.runtime.CompositeDataHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that loads a simple table from an Excel file.
 * 
 * @param inputFile
 *            The Excel file (1st row - titles, rest - data)
 * @param rows
 *            Multiple collections of leaves (or just one collection if not
 *            repetitive), where the role of each leaf corresponds to the title
 *            of one columns to load
 * @param error
 *            Optional error message
 * 
 * @author Ofer Brandes
 *  
 */
public class LoadExcelTable extends Plugin
{
    private static final Role FILE_CONTENT_ROLE = Role.get("<File>");
    private static final Role SHEET_NAME_ROLE = Role.get("<Sheet Name>");
    private static final Role SKIP_ERRORS_ROLE = Role.get("<Skip Unmatching Rows>");
    private static final Role OUTPUT_ROLE = Role.get("<Rows>");

    private static final boolean DEBUG = false;

    SlotHandler fileContentTrigger, sheetNameTrigger, skipErrorsTrigger, exitHandler;

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
    	Boolean flag = (skipErrorsTrigger == null ? null : (Boolean) skipErrorsTrigger.get(context,flow));
    	boolean skipErrors = (flag != null) && flag.booleanValue();

    	try
		{
			// Open the workbook
    		BinaryValue data = (BinaryValue) fileContentTrigger.get(context, flow);
			Workbook wb = Excel.openWorkBook(context,data);

			// Get the specified (or first) sheet
			String sheetName = (sheetNameTrigger == null ? null : (String) sheetNameTrigger.get(context,flow));
            Sheet sheet = Excel.getSheet(context,wb,sheetName);

            // Convert each row to an output collection (or just the first if
            // the output is not repetitive)
            CompositeDataHandler outputDataHandler = (CompositeDataHandler) exitHandler
                    .getChildInstanceHandler();
            TableTitleRow tableTitleRow = Excel.findTableTitleRow(context,sheet,0,outputDataHandler);
            for (int r = tableTitleRow.row+1; r <= sheet.getLastRowNum(); r++)
            {
                Row row = sheet.getRow(r);
                if (row != null)
                {
                	Object rowObj = null;
                	
                	try {
	                    rowObj = Excel.loadExcelRow(context, tableTitleRow.columns, row, outputDataHandler);
                	}
                	catch (IllegalArgumentException e)
					{
                		if (skipErrors)
                			continue; // Ignore erroneous rows
                		throw new ModelExecutionException("Erroneous Row '" + (r+1) + "' in Excel file", e.getMessage(),e);
					}

                	if (Tables.isEmpty(context, outputDataHandler, rowObj))
	                    continue; // Ignore rows with no relevant data

                	if (exitHandler.isRepetitive())
	                {
	                	accumulateExitValue(exitHandler, context, flow, rowObj);
	                }
	                else
	                {
	                	setExitValue(exitHandler, context, flow, rowObj);
	                	break;
	                }
                }
            }
        }
        catch (RuntimeException e)
        {
            fireError(context, flow, e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
     */
    public void resume(RuntimeContext context, FlowInstance flow)
    {
        // Nothing to do (This is an action)
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
     */
    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);

        fileContentTrigger = getRequiredMandatoryTrigger(FILE_CONTENT_ROLE, Boolean.FALSE, BuiltinModels.BINARY_ID);
        sheetNameTrigger = getNonRequiredMandatoryTrigger(SHEET_NAME_ROLE, Boolean.FALSE, BuiltinModels.TEXT_ID);
        skipErrorsTrigger = getNonRequiredTrigger(SKIP_ERRORS_ROLE, Boolean.FALSE, BuiltinModels.BOOLEAN_ID);

        exitHandler = getRequiredExit(OUTPUT_ROLE, Boolean.TRUE, null);
        checkFlatDataType (exitHandler);
    }
}
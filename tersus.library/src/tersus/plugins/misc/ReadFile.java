/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.misc;

import java.io.IOException;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * Read the content of a resource file (file's path is relative to "models/").
 * 
 * @author Ofer Brandes
 *
 */
public class ReadFile extends Plugin
{
	private static final Role INPUT_ROLE = Role.get("<File Name>");
	private static final Role OUTPUT_ROLE = Role.get("<File Text>");

	SlotHandler trigger, exit;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		String fileName = (String) trigger.get(context, flow);
		
		byte[] fileContent;
		try
		{
			fileContent = context.getModelLoader().getRepository().getResource(fileName);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			throw new EngineException("Failed to read file '" + fileName+ "'", null, e);
		}
		if (fileContent == null)
			throw new EngineException("Failed to read file '" + fileName+ "'", null, null);

		String content = new String(fileContent);
		setExitValue(exit, context, flow, content);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		trigger = getRequiredMandatoryTrigger(INPUT_ROLE, Boolean.FALSE, BuiltinModels.TEXT_ID);
		exit = getRequiredExit(OUTPUT_ROLE, Boolean.FALSE, BuiltinModels.TEXT_ID);
	}

}

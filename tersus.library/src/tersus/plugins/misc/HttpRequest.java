/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.misc;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLHandshakeException;

import tersus.InternalErrorException;
import tersus.model.BuiltinModels;
import tersus.model.BuiltinProperties;
import tersus.model.Model;
import tersus.model.Role;
import tersus.model.validation.Problems;
import tersus.runtime.BinaryDataHandler;
import tersus.runtime.BinaryValue;
import tersus.runtime.ElementHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.TextHandler;
import tersus.util.FileUtils;
import tersus.webapp.ContextPool;

/**
 * @author oferb
 * 
 */

public class HttpRequest extends Plugin

{
	static final String UTF8 = "UTF-8";

	private static final Role URL_TRIGGER_ROLE = Role.get("<URL>");
	private static final Role NAME_ROLE = Role.get("Name");
	private static final Role VALUE_ROLE = Role.get("Value");
	private static final Role REQUEST_TRIGGER_ROLE = Role.get("<Request>");
	private static final Role RESPONSE_EXIT_ROLE = Role.get("<Response>");
	private static final Role CONTENT_TYPE_ROLE = Role.get("<Content Type>");
	private static final Role METHOD_TRIGGER_ROLE = Role.get("<Method>");
	private static final Role REQUEST_HEADERS_ROLE = Role.get("<Request Headers>");
	private static final Role HEADER_NAMES = Role.get("<Header Names>");
    private static final Role HEADERS = Role.get("<Response Headers>");
    private static final Role NAME = Role.get("Name"); 
    private static final Role VALUES = Role.get("Values"); 
    private static final Role STATUS_CODE = Role.get("<Status Code>");
    private static final Role STATUS_MESSAGE = Role.get("<Status Message>");
    private static final Role TIMEOUT = Role.get("<Timeout>");

	SlotHandler urlTrigger, methodTrigger, contentTypeTrigger, requestHeadersTrigger, timeoutTrigger;
	ElementHandler propertyNameElement, propertyValueElement;
	SlotHandler requestTrigger;
	SlotHandler responseExit;
    private SlotHandler headerNamesTrigger, headersExit, statusCodeExit, statusMessageExit;
    private InstanceHandler headerType;
    private ElementHandler nameElement, valuesElement;
	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext,
	 * tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		// Get URL and request
		String url = (String) urlTrigger.get(context, flow);
		Object request = getTriggerValue(context, flow, requestTrigger);
		String contentType = getTriggerText(context, flow, contentTypeTrigger);

		if (contentType == null)
		{
			if (request instanceof String)
			{
				String requestStr = (String) request;
				if (requestStr.startsWith("<?xml"))
					contentType = "text/xml; charset=utf-8";
				else
					contentType = "application/x-www-form-urlencoded; charset=utf-8";
			}
			else if (request != null)
			{
				contentType = "application/octet-stream";
			}
		}
		byte[] requestBytes = null;
		if (request != null)
			try
			{
				requestBytes = request instanceof String ? ((String) request).getBytes(UTF8)
						: ((BinaryValue) request).toByteArray();
			}
		catch (UnsupportedEncodingException e1)
		{
			throw new InternalErrorException("Missing encoding " + UTF8);
		}

		try
		{
			HttpURLConnection huc = prepareHttpRequest(context, flow, url, requestBytes,
					contentType);

			// Invoke HTTP Request
			huc.connect();
			int code = huc.getResponseCode();
			String message = huc.getResponseMessage();
			setExitValue(statusCodeExit, context, flow, code);
			setExitValue(statusMessageExit, context, flow, message);
			byte[] responseBytes = null;
			if (((code < 200) || (code >= 300)))
			{
				if (statusCodeExit == null)
				{
					String errorMessage = "HTTP Error: " + code + " (" + message + ") \n[URL:" + url
						+ "]";
					throw new EngineException(errorMessage, null, null);
				}
				responseBytes = FileUtils.readBytes(huc.getErrorStream(),true);
			}
			else
			{
				responseBytes = getHttpResponse(huc);
			}
			if (responseBytes != null && responseBytes.length > 0)
			{
				Object response;
				if (responseExit.getChildInstanceHandler() instanceof TextHandler)
										response = new String(responseBytes, UTF8);
						else
							response = new BinaryValue(responseBytes);
						setExitValue(responseExit, context, flow, response);
			}

			if (headersExit != null)
				outputResponseHeaders(context, flow, huc.getHeaderFields());	
			// Extract HTTP Response
		}
		catch (MalformedURLException e)
		{
			throw new EngineException("Error in connecting to URL", "URL='" + url + "'", e);
		}
		catch (SSLHandshakeException e)
		{
			throw new EngineException("Error in establishing SSL connection", "URL:" + url
					+ "\nError message:" + e.getMessage(), e);
		}
		catch (IOException e)
		{
			throw new EngineException("Error in connecting to URL", "URL:" + url
					+ "\nError message:" + e.getMessage(), e);
		}
	}

	protected void outputResponseHeaders(RuntimeContext context, FlowInstance flow,
			Map<String, List<String>> responseHeaders)
	{
		Collection<String> names;

		if (headerNamesTrigger != null)
		{
			names = (List<String>) headerNamesTrigger.get(context, flow);
		}
		else
		{
			names = responseHeaders.keySet();
		}
		for (String name : names)
		{
			if (name == null)
				continue; // Sometimes we get "null"  entry with the HTTP code
			List<String> values = responseHeaders.get(name);
			if (values != null && ! values.isEmpty())
			{
				Object obj = headerType.newInstance(context);
				nameElement.set(context, obj, name);
				for (String value:values)
				{
					valuesElement.accumulate(context, obj, value);
				}					
				if (headersExit.isRepetitive())
					accumulateExitValue(headersExit, context, flow, obj);
				else
					setExitValue(headersExit, context, flow,obj);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.InstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		urlTrigger = getRequiredMandatoryTrigger(URL_TRIGGER_ROLE, Boolean.FALSE,
				BuiltinModels.TEXT_ID);
		contentTypeTrigger = getNonRequiredTrigger(CONTENT_TYPE_ROLE, Boolean.FALSE,
				BuiltinModels.TEXT_ID);
		methodTrigger = getNonRequiredTrigger(METHOD_TRIGGER_ROLE, Boolean.FALSE,
				BuiltinModels.TEXT_ID);
		requestTrigger = getNonRequiredTrigger(REQUEST_TRIGGER_ROLE, Boolean.FALSE, null);
		timeoutTrigger = getNonRequiredExit(TIMEOUT, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		requestHeadersTrigger = getNonRequiredTrigger(REQUEST_HEADERS_ROLE, Boolean.TRUE,
				BuiltinModels.PROPERTY);
		if (requestHeadersTrigger != null)
		{
			propertyNameElement = requestHeadersTrigger.getChildInstanceHandler().getElementHandler(
					NAME_ROLE);
			propertyValueElement = requestHeadersTrigger.getChildInstanceHandler().getElementHandler(
					VALUE_ROLE);
		}
		if (requestTrigger != null)
		{
			InstanceHandler type = requestTrigger.getChildInstanceHandler();
			if (!(type instanceof TextHandler || type instanceof BinaryDataHandler))
				notifyInvalidModel(requestTrigger.getRole(), Problems.INVALID_TRIGGER,
						"Request type must be 'Text' or 'Binary'");
		}
		responseExit = getRequiredExit(RESPONSE_EXIT_ROLE, Boolean.FALSE, null);
		if (responseExit != null)
		{
			InstanceHandler type = responseExit.getChildInstanceHandler();
			if (!(type instanceof TextHandler || type instanceof BinaryDataHandler))
				notifyInvalidModel(responseExit.getRole(), Problems.INVALID_EXIT,
						"Response type must be 'Text' or 'Binary'");
		}
		
		statusCodeExit = getNonRequiredExit(STATUS_CODE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		statusMessageExit = getNonRequiredExit(STATUS_MESSAGE, Boolean.FALSE, BuiltinModels.TEXT_ID);
		headersExit = getNonRequiredExit(HEADERS, null, BuiltinModels.HTTP_HEADER_ID);
		if (headersExit != null)
		{
			headerNamesTrigger = getNonRequiredTrigger(HEADER_NAMES, Boolean.TRUE, BuiltinModels.TEXT_ID);
			headerType =  headersExit.getChildInstanceHandler();
			nameElement = headerType.getElementHandler(NAME);
			valuesElement = headerType.getElementHandler(VALUES);
		}

	}

	@SuppressWarnings("unchecked")
	private HttpURLConnection prepareHttpRequest(RuntimeContext context, FlowInstance flow,
			String url, byte[] requestBytes, String contentType) throws MalformedURLException,
			IOException
	{
		HttpURLConnection huc = (HttpURLConnection) (new URL(url)).openConnection();
		if (contentType != null)
			huc.setRequestProperty("Content-Type", contentType);
		if (requestBytes != null)
		{
			huc.setRequestProperty("Content-Length", "" + requestBytes.length);
			huc.setDoOutput(true);
		}
		
		String method = getTriggerValue(methodTrigger, context, flow, "POST");
		huc.setRequestMethod(method);

		int timeout  = getTriggerValue(timeoutTrigger, context, flow, 0);
		if (timeout == 0)
		{
			Object timeoutP = ((ContextPool)context.getContextPool()).getProperty("HttpRequest.timeout");
			if (timeoutP instanceof String)
				timeout = Integer.parseInt((String)timeoutP);
		}
		int remainingTime = (int) context.getRemainingTimeMilliseconds(); 

		if (timeout == 0)
			timeout = remainingTime;
		else
			timeout = Math.min(timeout, remainingTime);
		
		huc.setConnectTimeout(timeout);
		huc.setReadTimeout(timeout);

		List<Object> headers = (List<Object>) getTriggerValue(context, flow, requestHeadersTrigger);
		if (headers != null)
		{
			for (Object header : headers)
			{
				String name = (String) propertyNameElement.get(context, header);
				String value = (String) propertyValueElement.get(context, header);
				huc.setRequestProperty(name, value);
			}
		}
		if (getPluginVersion() < 1) // Maintain backwards compatability with r3372 and earlier
			huc.setDoOutput(true);  // per http://stackoverflow.com/questions/8760052 
		if (requestBytes != null)
		{
			OutputStream os = huc.getOutputStream();
			os.write(requestBytes);
		}

		return huc;
	}

	public static byte[] getHttpResponse(HttpURLConnection huc) throws IOException
	{
		/*
		 * int responseLength = 0; String header; for (int h = 0; (header = huc.getHeaderField(h))
		 * != null; h++) if ("Content-Length".equals(huc.getHeaderFieldKey(h))) { responseLength =
		 * Integer.parseInt(header); break; }
		 */
		InputStream is;
		is = huc.getInputStream();
		byte[] responseBytes = FileUtils.readBytes(is, true);
		return responseBytes;

	}
}

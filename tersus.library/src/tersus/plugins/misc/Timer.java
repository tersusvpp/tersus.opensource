/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.misc;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Path;
import tersus.model.Role;
import tersus.model.validation.Problems;
import tersus.runtime.ContextPool;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.InvocationAdapter;
import tersus.runtime.LinkHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Number;
import tersus.runtime.PathHandler;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.SubFlowHandler;
import tersus.webapp.Engine;
import tersus.webapp.LoggingHelper;

/**
 * @author Youval Bronicki
 * 
 */
public class Timer extends Plugin
{

    private static final Role INTERVAL = Role.get("<Interval>");
    private static final Role DELAY = Role.get("<Delay>");
    private static final Role FIXED_FREQUENCY = Role.get("<Fixed Frequency>");
    private static final Role FIRST_TIME = Role.get("<First Time>");
    private SlotHandler intervalTrigger, delayTrigger, fixedFrequencyTrigger, firstTimeTrigger;

    private SubFlowHandler activity;

    private HashMap triggerSourceMap;

    public static final String MUST_HAVE_ONE_SUB_PROCESS = "Timer must contain exactly one sub-process";

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        triggerSourceMap = new HashMap();
        intervalTrigger = getNonRequiredMandatoryTrigger(INTERVAL,
                Boolean.FALSE, BuiltinModels.NUMBER_ID);
        delayTrigger = getNonRequiredTrigger(DELAY,
                Boolean.FALSE, BuiltinModels.NUMBER_ID);
        fixedFrequencyTrigger = getNonRequiredTrigger(FIXED_FREQUENCY,
                Boolean.FALSE, BuiltinModels.BOOLEAN_ID);
        firstTimeTrigger = getNonRequiredTimeTrigger(FIRST_TIME, Boolean.FALSE);
        for (int i = 0; i < nElements; i++)
        {
            ElementHandler e = elementHandlers[i];
            if (e instanceof SubFlowHandler)
            {
                if (activity != null)
                    notifyInvalidModel(Path.EMPTY,
                            Problems.TOO_MANY_SUB_PROCESSES,
                            MUST_HAVE_ONE_SUB_PROCESS);
                activity = (SubFlowHandler) e;
            }

        }
        if (activity == null)
            notifyInvalidModel(Path.EMPTY, Problems.MISSING_SUB_PROCESS,
                    MUST_HAVE_ONE_SUB_PROCESS);
        if (activity.isRepetitive() && intervalTrigger == null)
            notifyInvalidModel(Path.EMPTY, Problems.MISSING_TRIGGER,
                    "A Timer with a repetitive activity must have an "
                            + INTERVAL + " trigger");
        else if (!activity.isRepetitive() && intervalTrigger != null)
            notifyInvalidModel(activity.getRole(),
                    Problems.INVALID_SUB_PROCESS, "If an " + INTERVAL
                            + " is specified, the activity must be repetitive");
        else if (!activity.isRepetitive() && fixedFrequencyTrigger != null)
            notifyInvalidModel(activity.getRole(),
                    Problems.INVALID_SUB_PROCESS, "If " + FIXED_FREQUENCY
                            + " is specified, the activity must be repetitive");
        if (delayTrigger != null && firstTimeTrigger != null)
            notifyInvalidModel(Path.EMPTY,
                    Problems.INVALID_TRIGGERS, DELAY + " and " + FIRST_TIME + " must not be used together");
        for (int i = 0; i < nElements; i++)
        {
            ElementHandler e = elementHandlers[i];
            if (e instanceof LinkHandler)
            {
                LinkHandler flow = (LinkHandler) e;
                flow.prepare();
                if (!(flow.getSourceElementHandler() instanceof SlotHandler))
                    notifyInvalidModel(flow.getRole(), Problems.INVALID_FLOW,
                            "The source must be a trigger of the Timer process");
                PathHandler targetPath = flow.getTargetPath();
                if (!(flow.getTargetElementHandler() == activity
                        && targetPath.getNumberOfSegments() == 2 && targetPath
                        .getLastSegment() instanceof SlotHandler))
                    notifyInvalidModel(flow.getRole(), Problems.INVALID_FLOW,
                            "The target must be a trigger of the activity");
                SlotHandler sourceTrigger = (SlotHandler) flow
                        .getSourceElementHandler();
                SlotHandler targetTrigger = (SlotHandler) targetPath
                        .getLastSegment();
                triggerSourceMap.put(targetTrigger, sourceTrigger);
            }
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext,
     *      tersus.runtime.FlowInstance)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
        Logger log = context.getContextPool().getLogger("tersus_engine.timer");
        HashMap triggerValues = new HashMap();
        for (Iterator i = triggerSourceMap.entrySet().iterator(); i.hasNext();)
        {
            Map.Entry entry = (Map.Entry) i.next();
            SlotHandler target = (SlotHandler) entry.getKey();
            SlotHandler source = (SlotHandler) entry.getValue();

            List values = null;
            if (source.isRepetitive())
                values = (List) source.get(context, flow);
            else
            {
                Object value = source.get(context, flow);
                if (value != null)
                {
                    values = new ArrayList();
                    values.add(value);
                }
            }
            if (values != null)
            {
                if (target.isRepetitive())
                    triggerValues.put(target.getRole(), values);
                else
                    triggerValues.put(target.getRole(), values.get(values
                            .size() - 1));
            }
        }
        java.util.Timer timer = new java.util.Timer(true);
        TimerCancellationAction cancel = new TimerCancellationAction(context.getContextPool(), timer,
		        log);
		context.getContextPool().addShutdownAction(
                cancel);
        TimerActivity timerActivity = new TimerActivity(triggerValues, activity
                .getChildModelId(), context.getContextPool(), log);
        Number interval = null;
        if (intervalTrigger != null)
        	interval = (Number) intervalTrigger.get(context, flow);
        if (interval == null && activity.isRepetitive())
            throw new ModelExecutionException("No " + INTERVAL
                    + " specified for timer");
        long intervalMillis = 0;
        if (interval != null)
        {
        	if (getPluginVersion() < 1)
        		intervalMillis = (long) (interval.value * 1000);
        	else
        		intervalMillis = (long)interval.value;
        }
        long delayMills = 0;
        Date firstTime = null;
        if (delayTrigger != null && delayTrigger.get(context, flow) != null)
        	delayMills = (long)((Number)delayTrigger.get(context, flow)).value;
        if (firstTimeTrigger != null)
        	firstTime = (Date)firstTimeTrigger.get(context, flow);
        Boolean fixedFrequency = fixedFrequencyTrigger != null ? (Boolean)fixedFrequencyTrigger.get(context,flow) : Boolean.FALSE;
        if (activity.isRepetitive())
        {
	        if (Boolean.TRUE.equals(fixedFrequency))
	        {
	        	if (firstTime != null)
	            	timer.scheduleAtFixedRate(timerActivity, firstTime, intervalMillis);
	        	else
	        		timer.scheduleAtFixedRate(timerActivity, delayMills, intervalMillis);
	        }
	        else
	        {
	        	if (firstTime != null)
	            	timer.schedule(timerActivity, firstTime, intervalMillis);
	        	else
	        		timer.schedule(timerActivity,delayMills, intervalMillis);
	        }
        }
        else
        {
        	timerActivity.setOnFinish(cancel); // Kill Timer when finished
        	if (firstTime != null)
            	timer.schedule(timerActivity, firstTime);
        	else
        		timer.schedule(timerActivity,delayMills);
        }
        if (log.isInfoEnabled())
            log.info("Timer set[modelId=" + modelId + " interval="
                    + intervalMillis + "]");
    }

}

class TimerCancellationAction implements Runnable
{
    private java.util.Timer timer;

    private ContextPool contextPool;

    private Logger log;

    public TimerCancellationAction(ContextPool contextPool,
            java.util.Timer timer, Logger log)
    {
        this.timer = timer;
        this.contextPool = contextPool;
        this.log = log;
    }

    public void run()
    {
    	if (timer != null)
    	{
	        timer.cancel();
	        if (log.isInfoEnabled())
	            log.info("Timer cancelled");
	        contextPool.removeShutdownAction(this); // This is needed to
	        timer = null;
	        contextPool = null;
	        log = null;
    	}
    }
}

class TimerActivity extends TimerTask
{

    private Map triggerValues;

    private ModelId modelId;

    private ContextPool contextPool;

    private Logger log;
    
    private Runnable onFinish;

    public TimerActivity(Map triggerValues, ModelId modelId,
            ContextPool contextPool, Logger log)
    {
        this.triggerValues = triggerValues;
        this.modelId = modelId;
        this.contextPool = contextPool;
        this.log = log;
    }
    
    public void setOnFinish(Runnable onFinish)
    {
    	this.onFinish=onFinish;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.util.TimerTask#run()
     */
    private InvocationAdapter invocationAdapter = new InvocationAdapter()
    {

        public String getLogType()
        {
            return "Timer";
        }

        public void setTriggers(RuntimeContext context, FlowInstance serviceInstance, SlotHandler[] triggers)
        {
            for (int i = 0; i < triggers.length; i++)
            {
                SlotHandler trigger = triggers[i];
                Object value = triggerValues.get(trigger.getRole());
                if (value != null)
                    trigger.set(context, serviceInstance, value);
            }
        }

        public boolean needsAccessValidation()
        {
            return false;
        }
        
    };
    public void run()
    {
        if (log.isInfoEnabled())
            log.info("Starting timer activity [modelId=" + modelId + "]");
        RuntimeContext context = null;
        LoggingHelper.get().startRequest(contextPool.activityLog, contextPool.engineLog, null);
        try
        {
            contextPool.reloadIfNeeded();
            context = contextPool.getContext();
            context.setRoot(true);
            context.setRequestType("Timer Activity");
            context.runProcess(modelId.toString(), "", null, invocationAdapter);
            getPool().poolContext(context);
            context = null;
            LoggingHelper.get().endRequest();
        }
        catch (Throwable e)
        {
            //Nothing to do - error reported, transaction rolled back and context disposed
            LoggingHelper.get().endRequest(e);
        }
        finally
        {
            if (context != null)
            {
                context.dispose();
                context = null;
            }
            LoggingHelper.dispose();
            if (onFinish != null)
            	onFinish.run();
            	
        }
    }

    private ContextPool getPool()
    {
        return contextPool;
    }
}
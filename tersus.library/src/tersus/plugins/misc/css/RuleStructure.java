/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software LFrpctd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.misc.css;

import tersus.model.ModelId;
import tersus.model.Role;

/**
 * @author Liat Shiff
 */
public class RuleStructure
{
	public static final ModelId RULE_ID = new ModelId("Common/Data Structures/CSS/Rule");
	
	public static final Role RULE_NAME = Role.get("Name");
    public static final Role RULE_PROPERTY = Role.get("Property");
}

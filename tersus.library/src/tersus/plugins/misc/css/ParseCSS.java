/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software LFrpctd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.misc.css;

import java.io.IOException;
import java.io.StringReader;

import org.w3c.css.sac.InputSource;

import com.steadystate.css.parser.SACParserCSS21;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * @author Liat Shiff
 */
public class ParseCSS extends Plugin
{
	// Constants
	private static final Role CSS_FILE_CONTENT = Role.get("<CSS File Content>");
	private static final Role RULES = Role.get("<Rules>");

	// Triggers
	private SlotHandler cssFileContentTrigger;

	// Exits
	private SlotHandler rulesExit;
	
	private ElementHandler nameHandler;
	private ElementHandler propertyHandler;

	public void start(RuntimeContext context, FlowInstance flow)
	{
		String cssFileContent = (String) cssFileContentTrigger.get(context, flow);

		getRules(context, flow, cssFileContent);
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		cssFileContentTrigger = getRequiredMandatoryTrigger(CSS_FILE_CONTENT, Boolean.FALSE,
				BuiltinModels.TEXT_ID);

		rulesExit = getRequiredExit(RULES, Boolean.TRUE, RuleStructure.RULE_ID);

		if (rulesExit != null)
		{
			InstanceHandler ruleDescriptorHandler = rulesExit.getChildInstanceHandler();
			nameHandler = ruleDescriptorHandler.getElementHandler(RuleStructure.RULE_NAME);
			propertyHandler = ruleDescriptorHandler.getElementHandler(RuleStructure.RULE_PROPERTY);
		}
	}

	private void getRules(RuntimeContext context, FlowInstance flow, String cssFileContent)
	{
		InputSource source = new InputSource(new StringReader(cssFileContent));
		SACParserCSS21 parser = new SACParserCSS21();
		parser.setDocumentHandler(new CSSDocumentHandler(context, flow, this));

		try
		{
			parser.parseStyleSheet(source);
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public SlotHandler getRulesExit()
	{
		return rulesExit;
	}
	
	public ElementHandler getRuleNameHandler()
	{
		return nameHandler;
	}
	
	public ElementHandler getPropertyHandler()
	{
		return propertyHandler;
	}

}

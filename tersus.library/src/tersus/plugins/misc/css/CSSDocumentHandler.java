/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software LFrpctd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.misc.css;

import org.w3c.css.sac.CSSException;
import org.w3c.css.sac.InputSource;
import org.w3c.css.sac.LexicalUnit;
import org.w3c.css.sac.SACMediaList;
import org.w3c.css.sac.Selector;
import org.w3c.css.sac.SelectorList;

import tersus.runtime.ElementHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.RuntimeContext;

import com.steadystate.css.sac.DocumentHandlerExt;

/**
 * @author Liat Shiff
 */
public class CSSDocumentHandler implements DocumentHandlerExt
{
	private Object ruleDescriptor;

	private RuntimeContext context;
	private FlowInstance flow;
	private ParseCSS cssPaser;

	public CSSDocumentHandler(RuntimeContext context, FlowInstance flow, ParseCSS cssPaser)
	{
		this.context = context;
		this.flow = flow;
		this.cssPaser = cssPaser;
	}

	public void charset(String characterEncoding) throws CSSException
	{
	}

	public void comment(String arg0) throws CSSException
	{
	}

	public void endDocument(InputSource arg0) throws CSSException
	{
	}

	public void endFontFace() throws CSSException
	{
	}

	public void endMedia(SACMediaList arg0) throws CSSException
	{
	}

	public void endPage(String arg0, String arg1) throws CSSException
	{
	}

	public void endSelector(SelectorList arg0) throws CSSException
	{
		cssPaser.accumulateExitValue(cssPaser.getRulesExit(), context, flow, ruleDescriptor);
	}

	public void ignorableAtRule(String arg0) throws CSSException
	{
	}

	public void importStyle(String arg0, SACMediaList arg1, String arg2) throws CSSException
	{
	}

	public void namespaceDeclaration(String arg0, String arg1) throws CSSException
	{
	}

	public void property(String arg0, LexicalUnit arg1, boolean arg2) throws CSSException
	{
		InstanceHandler propertyHandler = cssPaser.getPropertyHandler().getChildInstanceHandler();

		ElementHandler properyNameHandler = propertyHandler
				.getElementHandler(PropertyStructure.PROPERTY_NAME);
		ElementHandler propertyValueHandler = propertyHandler
				.getElementHandler(PropertyStructure.PROPERTY_VALUE);

		Object property = propertyHandler.newInstance(context);

		properyNameHandler.set(context, property, arg0);
		propertyValueHandler.set(context, property, getPropertyValue(arg1));

		cssPaser.getPropertyHandler().accumulate(context, ruleDescriptor, property);
	}

	private String getPropertyValue(LexicalUnit arg1)
	{
		if (arg1.getNextLexicalUnit() != null)
			return arg1.toString() + "," + getPropertyValue(arg1.getNextLexicalUnit());
		else
			return arg1.toString();
	}

	public void startDocument(InputSource arg0) throws CSSException
	{
	}

	public void startFontFace() throws CSSException
	{
	}

	public void startMedia(SACMediaList arg0) throws CSSException
	{
	}

	public void startPage(String arg0, String arg1) throws CSSException
	{
	}

	public void startSelector(SelectorList arg0) throws CSSException
	{
		ruleDescriptor = cssPaser.getRulesExit().getChildInstanceHandler().newInstance(context);

		ElementHandler ruleNameHandler = cssPaser.getRuleNameHandler();

		if (arg0.getLength() > 0)
		{
			Selector selector = arg0.item(0);
			ruleNameHandler.set(context, ruleDescriptor, selector.toString());
		}
		else
		{
			// Throw an exception?
		}
	}
}

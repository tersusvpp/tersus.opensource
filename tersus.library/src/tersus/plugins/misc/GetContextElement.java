/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.misc;

import java.util.ArrayList;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.model.SlotType;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * @author Youval Bronicki
 *
 */
public class GetContextElement extends Plugin
{

    private SlotHandler levelTrigger;
    private static final Role LEVEL = Role.get("<Level>");

    /* (non-Javadoc)
     * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
        int level = (int)((Number)levelTrigger.get(context, flow)).value; 
        for (int i=0;i<exits.length; i++)
        {
            SlotHandler exit = exits[i];
            if (exit.getType() != SlotType.ERROR && ! exit.isEmpty())
            {
                getContextElement(context, flow, exit, level);
            }
        }
    }

    private void getContextElement(RuntimeContext context, FlowInstance flow, SlotHandler exit, int level)
    {
        ArrayList contextElements = null;
        if (level >=0)
            contextElements = new ArrayList();
        FlowInstance ancestor = flow.getParent();
        int relativeLevel = 0;
        while (ancestor != null)
        {
            ElementHandler e = ancestor.getHandler().getElementHandler(exit.getRole());
            if (e != null && exit.getChildModelId().equals(e.getChildModelId()))
            {
                -- relativeLevel;
                if (level <0)
                {
                    if (level == relativeLevel)
                    {
                        Object value = e.get(context, ancestor);
                        if (value != null)
                        {
                            setExitValue(exit, context, flow, value);
                        }
                        return;
                    }
                }
                else
                {
                    Object value = e.get(context, ancestor);
                    contextElements.add(value);
                }
            }
            ancestor = ancestor.getParent();
            
        }
        if (level >= 0 && level< contextElements.size() )
        {
            Object value = contextElements.get(contextElements.size()-1-level);
            setExitValue(exit, context, flow, value);
        }
    }

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        levelTrigger = getRequiredMandatoryTrigger(LEVEL, Boolean.FALSE, BuiltinModels.NUMBER_ID);
    }
}

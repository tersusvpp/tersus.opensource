/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.misc;

import java.io.ByteArrayInputStream;
import java.util.Map;
import java.util.Stack;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import tersus.model.Repository;
import tersus.model.Role;
import tersus.runtime.BinaryValue;
import tersus.runtime.CompositeDataHandler;
import tersus.runtime.DataHandler;
import tersus.runtime.ElementHandler;
import tersus.runtime.EngineException;
import tersus.runtime.InstanceHandler;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.RuntimeContext;
import tersus.runtime.XMLSerializer;
import tersus.util.Misc;
import tersus.util.SpecialRoles;

/**
 *
 * A SAX handler to parse an XML document.
 * 
 *  * @author Ofer Brandes
 *
 */

public class XmlDocumentHandler extends DefaultHandler
{
	private static final boolean DEBUG = false;

	Repository repository = null;

	private RuntimeContext context;
	private Object parent; // Typically 'FlowInstance'
	private Object parsedText; // The parsed text, for better error messages
	private ElementHandler output;
	private boolean skipSoapWraps;
	private int soapHeaderDepth = 0;
	private Stack nodes = null;
	private StringBuffer content = new StringBuffer();

	public XmlDocumentHandler(RuntimeContext context, Object parent, ElementHandler output, boolean stripSoapWraps, Object parsedText)
	{
		super();
		this.context = context;
		this.parent = parent;
		this.output = output;
		this.skipSoapWraps = stripSoapWraps;
		this.parsedText = parsedText;
		this.repository = context.getModelLoader().getRepository();
	}

	private class Node
	{
		private ElementHandler handler;
		private Object value;

		private Node(ElementHandler handler)
		{
			this.handler = handler;
			this.value = null;
		}
	}

	/**
	 * Handles a notification of the start of an XML element.
	 *
	 * 'XmlDocumentHandler' is currently used in two different situations,
	 * which differ in the way they treat namespaces:
	 * 
	 * (1) Parsing according to model created by 'ImportXMLDataStructureByExampleCommand'
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 * In this case, namespaces are ignored, and the element's raw XML 1.0 name
	 * is used as the role of the corresponding model element:
	 * @param uri Namespace URI (ignored).
	 * @param name Element's local name (ignored).
	 * @param qName Element's raw XML 1.0 name (the name of the corresponding model element).
	 * @param atts The element's attributes.
	 * 
	 * (2) Parsing according to model created by 'ImportWsdlCommand'
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 * In this case, namespaces are properly handled:
	 * @param uri Namespace URI (should match the namespace of the corresponding model element).
	 * @param name Element's local name (should match the role of the corresponding model element).
	 * @param qName Element's raw XML 1.0 name (ignored).
	 * @param atts The element's attributes.
	 * 
	 *  (3) Generic Parsing
	 *  ~~~~~~~~~~~~~~~~~~~
	 */
	public void startElement(String uri, String name, String qName, Attributes atts)
	{
		if (DEBUG)
		{
			if ("".equals(uri))
				System.out.println("Start element: " + qName);
			else
				System.out.println("Start element: {" + uri + "}" + name);
		}

		if (skipSoapWraps && (isSoapWrap(uri,name) || isSoapHeader(uri,name) || (soapHeaderDepth > 0)))
		{
			if (isSoapHeader(uri,name))
				soapHeaderDepth++;
			return;
		}

		content.setLength(0); // Clear any existing content 
		ElementHandler handler;
		if (nodes == null)
		{
			nodes = new Stack();
			handler = output;
		}
		else
		{
			handler = ((Node) nodes.peek()).handler;
			InstanceHandler instanceHandler = handler.getChildInstanceHandler();
			handler = instanceHandler.getElementHandler(Role.get(qName));		// For case 1
			if (handler == null)
				handler = getMatchingElementHandler (instanceHandler,uri,name);	// For case 2
			if (handler == null)
				handler = instanceHandler.getElementHandler(SpecialRoles.XML_GENERIC_ELEMENT_ROLE); 		// For case 3
			if (handler == null)
			{
				String parsedTextDisplay;
				if (parsedText instanceof BinaryValue)
					parsedTextDisplay = (String) parsedText;
				else if (parsedText instanceof byte[])
					parsedTextDisplay = new String((byte[]) parsedText);
				else if (parsedText instanceof BinaryValue)
				{
					byte[] bytes = ((BinaryValue) parsedText).toByteArray();
					parsedTextDisplay = new String(bytes);
				}
				else
					parsedTextDisplay = parsedText.toString();
			    throw new XMLParsingException("Unexpected element '"+qName+"'"+"\n"+"In: "+parsedTextDisplay);
			}
		}
		nodes.push(new Node(handler));

		DataHandler dataHandler = (DataHandler) handler.getChildInstanceHandler();
		if (DEBUG)
			System.out.println("Handler(" + qName + "): " + dataHandler.getModelId());

		if (dataHandler instanceof CompositeDataHandler)
		{
			Object value = dataHandler.newInstance(context);

			if (SpecialRoles.XML_GENERIC_ELEMENT_ROLE.equals(handler.getRole().toString()))
			{
				ElementHandler childHandler = dataHandler.getElementHandler(SpecialRoles.XML_GENERIC_NAME_ROLE);
				childHandler.set(context, value, name);
				childHandler = dataHandler.getElementHandler(SpecialRoles.XML_GENERIC_NAMESPACE_ROLE);
				childHandler.set(context, value, uri);
			}

			for (int i = 0; i < atts.getLength(); i++)
			{
				if (DEBUG)
					System.out.println("\t" + atts.getQName(i) + " = " + atts.getValue(i));

				String role = SpecialRoles.DEPENDENT_MODEL_ROLE_PREFIX + atts.getQName(i);
				ElementHandler attHandler = dataHandler.getElementHandler(Role.get(role));
				boolean isGenericAtt = false;
				if (attHandler == null)
				{
					attHandler = dataHandler.getElementHandler(SpecialRoles.XML_GENERIC_ATTRIBUTE_ROLE);	// For case 3
					isGenericAtt = (attHandler != null);
				}
				if (attHandler == null)
				{
					// What should we do in such a case?
					System.err.println("\tUnexpcted attribute "+atts.getQName(i)+" - Skipped");
					continue;
				}
				if (isGenericAtt)
				{
					DataHandler attDataHandler = (DataHandler) attHandler.getChildInstanceHandler();
					if (DEBUG)
						System.out.println("\tHandler(" + role + "): " + attDataHandler.getModelId());
					if (Misc.ASSERTIONS)
						Misc.assertion(attHandler.isRepetitive());
					Object CompositeAttValue = attDataHandler.newInstance(context);
					ElementHandler childHandler = attDataHandler.getElementHandler(SpecialRoles.XML_GENERIC_NAME_ROLE);
					LeafDataHandler leafHandler = (LeafDataHandler) childHandler.getChildInstanceHandler();
					Object attName = leafHandler.newInstance(context, atts.getLocalName(i), attHandler.getProperties(), false);
					childHandler.set(context, CompositeAttValue, attName);
					childHandler = attDataHandler.getElementHandler(SpecialRoles.XML_GENERIC_VALUE_ROLE);
					leafHandler = (LeafDataHandler) childHandler.getChildInstanceHandler();
					Object attValue = leafHandler.newInstance(context, atts.getValue(i), attHandler.getProperties(), false);
					childHandler.set(context, CompositeAttValue, attValue);
					attHandler.accumulate(context, value, CompositeAttValue);
					if (DEBUG)
						System.out.println("\tValue: '" + attHandler.get(context, value) + "'");
				}
				else // Non-Generic
				{
					LeafDataHandler attDataHandler = (LeafDataHandler) attHandler.getChildInstanceHandler();
					if (DEBUG)
						System.out.println("\tHandler(" + role + "): " + attDataHandler.getModelId());
					if (Misc.ASSERTIONS)
						Misc.assertion(!attHandler.isRepetitive());
					Object attValue = attDataHandler.newInstance(context, atts.getValue(i), attHandler.getProperties(), false);
					attHandler.set(context, value, attValue);
					if (DEBUG)
						System.out.println("\tValue: '" + attHandler.get(context, value) + "'");
				}
			}

			((Node) nodes.peek()).value = value;
		}
	}

	/**
	 * Handles a notification of character data inside an XML element.
	 * 
	 * @param ch A characters array containing the element's data.
	 * @param start The start position in the character array.
	 * @param length The number of characters to use from the characters array.
	 */
	public void characters(char ch[], int start, int length)
	{
		content.append(ch, start, length);

		if (DEBUG)
			System.out.print("\tCharacters: \"");
		for (int i = start; i < start + length; i++)
		{
			switch (ch[i])
			{
				case '\\' :
					if (DEBUG)
						System.out.print("\\\\");
					break;
				case '"' :
					if (DEBUG)
						System.out.print("\\\"");
					break;
				case '\n' :
					if (DEBUG)
						System.out.print("\\n");
					break;
				case '\r' :
					if (DEBUG)
						System.out.print("\\r");
					break;
				case '\t' :
					if (DEBUG)
						System.out.print("\\t");
					break;
				default :
					if (DEBUG)
						System.out.print(ch[i]);
					break;
			}
		}
		if (DEBUG)
			System.out.print("\"\n");

	}

	private void addContent()
	{
		String contentStr = content.toString().trim();
		if (contentStr.length() == 0)
			return;
		ElementHandler handler = ((Node) nodes.peek()).handler;
		DataHandler dataHandler = (DataHandler) handler.getChildInstanceHandler();
		Object parentValue = ((Node) nodes.peek()).value;
		if (dataHandler instanceof CompositeDataHandler)
		{
			String role = SpecialRoles.XML_CONTENT_LEAF_ROLE;
			handler = dataHandler.getElementHandler(Role.get(role));
			if (handler == null)
				throw new XMLParsingException("Element '"+ role + "' is expected in model '" + dataHandler.getModelId().getPath() + "'");
			dataHandler = (LeafDataHandler) handler.getChildInstanceHandler();
			if (DEBUG)
				System.out.println("\tHandler(" + role + "): " + dataHandler.getModelId());
		}
		else // OPT replace pop() & push() by Vector.get()
			{
			if (Misc.ASSERTIONS)
				Misc.assertion(dataHandler instanceof LeafDataHandler);
			Node node = (Node) nodes.pop();
			parentValue = ((Node) nodes.peek()).value;
			nodes.push(node);
		}
		
		Object value = ((LeafDataHandler) dataHandler).newInstance(context, contentStr, handler.getProperties(), false);
		
		if (handler.isRepetitive())
			handler.accumulate(context, parentValue, value);
		else if (handler.get(context, parentValue) != null)
			throw new XMLParsingException("Element '"+handler.getRole()+ "' is non-repetitive in the model, but has multiple values");
		else
			handler.set(context, parentValue, value);
		
		if (DEBUG)
			System.out.println("\tValue: '" + handler.get(context, parentValue) + "'");
	}

	/**
	 * Handles a notification of the end of an XML element.
	 *
	 * 'XmlDocumentHandler' is currently used in two different situations,
	 * which differ in the way they treat namespaces:
	 * 
	 * (1) Parsing according to model created by 'ImportXMLDataStructureByExampleCommand'
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 * In this case, namespaces are ignored, and the element's raw XML 1.0 name
	 * is used as the role of the corresponding model element:
	 * @param uri Namespace URI (ignored).
	 * @param name Element's local name (ignored).
	 * @param qName Element's raw XML 1.0 name (the name of the corresponding model element).
	 * @param atts The element's attributes.
	 * 
	 * (2) Parsing according to model created by 'ImportWsdlCommand'
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 * In this case, namespaces are properly handled:
	 * @param uri Namespace URI (should match the namespace of the corresponding model element).
	 * @param name Element's local name (should match the role of the corresponding model element).
	 * @param qName Element's raw XML 1.0 name (ignored).
	 * 
	 *  (3) Generic Parsing
	 *  ~~~~~~~~~~~~~~~~~~~
	 */
	public void endElement(String uri, String name, String qName)
	{
		if (DEBUG)
		{
			if ("".equals(uri))
				System.out.println("End element: " + qName);
			else
				System.out.println("End element:   {" + uri + "}" + name);
		}

		if (skipSoapWraps && (isSoapWrap(uri,name) || isSoapHeader(uri,name) || (soapHeaderDepth > 0)))
		{
			if (isSoapHeader(uri,name))
				soapHeaderDepth--;
			return;
		}

		addContent(); // Add accumulated character content (if any)
		content.setLength(0);
		Node node = (Node) nodes.pop();

		if (nodes.size() > 0)
		{
			Node parentNode = (Node) nodes.peek();
			if (node.handler.getChildInstanceHandler() instanceof CompositeDataHandler)
				if (node.handler.isRepetitive())
					node.handler.accumulate(context, parentNode.value, node.value);
				else if (node.handler.get(context, parentNode.value) != null)
					throw new XMLParsingException("Element '"+node.handler.getRole()+ "' is non-repetitive in the model, but has multiple values");
				else
					node.handler.set(context, parentNode.value, node.value);
			if (DEBUG)
			{
				System.out.println(
					"Handler: " + ((DataHandler) parentNode.handler.getChildInstanceHandler()).getModelId());
				System.out.println("Value: '" + node.handler.get(context, parentNode.value) + "'");
			}
		}
		else
		{
			node.handler.set(context, parent, node.value);
			if (DEBUG)
				System.out.println("Value: '" + node.handler.get(context, parent) + "'");

		}
	}

	private boolean isSoapWrap (String uri, String name)
	{
		return	("http://schemas.xmlsoap.org/soap/envelope/".equals(uri)) &&
				("Envelope".equalsIgnoreCase(name) || "Body".equalsIgnoreCase(name));
	}

	private boolean isSoapHeader (String uri, String name)
	{
		return	("http://schemas.xmlsoap.org/soap/envelope/".equals(uri)) &&
				("Header".equalsIgnoreCase(name));
	}

	private ElementHandler getMatchingElementHandler (InstanceHandler instanceHandler, String xmlUri, String xmlName) // Compare with the generic tersus.runtime.InstanceHandler.getElementHandler()
	{
		// For case 2 (qualified name):
		// - The role is the XML "net name" (also allowing a pseudo-namespace prefix, e.g. "1:DataSet")
		// - Element's namespace should match the XML element's URI

		for (int i = 0; i < instanceHandler.elementHandlers.length; i++)
		{
			ElementHandler elementHandler = instanceHandler.elementHandlers[i];
			String netRole = XMLSerializer.netRole(elementHandler.getRole().toString());
			String nameSpace = XMLSerializer.elementNameSpace(elementHandler,null);
			if (nameSpace == null)
				nameSpace = "";
			if (netRole.equals(xmlName) && nameSpace.equals(xmlUri) )
				return elementHandler;
		}

		return null;
	}

	public static EngineException catchSAXException (RuntimeContext context, SAXException e)
	{
	    String details;
	    Exception embedded = e.getException();
	    if (embedded instanceof XMLParsingException)
	        details = embedded.getMessage();
	    else if (embedded != null)
	        details = embedded.getClass().getName()+ ": "+embedded.getMessage();
	    else
	        details = e.getClass().getName()+ ": "+e.getMessage();
	    if (e instanceof SAXParseException)
	    {
	        int line = ((SAXParseException)e).getLineNumber();
	        int col = ((SAXParseException)e).getColumnNumber();
	        details += " (line "+line+" column "+col+")";
	    }
		return (new EngineException("XML Parsing Failed", details, e));	
	}
}


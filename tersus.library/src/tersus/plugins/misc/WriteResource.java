/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.misc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import jcifs.smb.SmbFile;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.BinaryValue;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

public class WriteResource extends Plugin
{
    private static final Role URL_ROLE = Role.get("<URL>");
    private static final Role CONTENT_ROLE = Role.get("<Content>");
    private static final Role OK_ROLE = Role.get("<OK>");
    private static final Role CREATE_FOLDERS = Role.get("<Create Folders>");

    SlotHandler urlTrigger, contentTrigger, okExit, createFoldersTrigger;

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext,
     *      tersus.runtime.FlowInstance)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
        String urlString = (String) urlTrigger.get(context, flow);
        BinaryValue content = (BinaryValue) contentTrigger.get(context, flow);

        byte[] bytes = content.toByteArray();
        try
        {
            OutputStream os;
            if (urlString.startsWith("smb:"))
                os = openSMBFIle(urlString, context, flow);
            else
            {
                URL url = new URL(urlString);
                if ("file".equals(url.getProtocol()))
                {
                    boolean createFolders = getTriggerValue(createFoldersTrigger, context, flow, false);
                    String path = url.getPath();
                    path=path.replace("%20"," "); // Temporary
					File file = new File(path);
                    if (createFolders)
                    	file.getParentFile().mkdirs();
                    os = new FileOutputStream(file);
                }
                else
                {
                    URLConnection connection = url.openConnection();
                    connection.setDoOutput(true);
                    os = connection.getOutputStream();
                }
            }
            os.write(bytes);
            os.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            throw new EngineException("Failed to write to URL '" + urlString
                    + "'", null, e);
        }
        chargeEmptyExit(context, flow, okExit);
    }

    private OutputStream openSMBFIle(String urlString, RuntimeContext context,
            FlowInstance flow)
    {
        try
        {
            SmbFile file = new SmbFile(urlString);
            return file.getOutputStream();
        }
        catch (Exception e)
        {
            throw new ModelExecutionException("Failed to open remote file",urlString,e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
     */
    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        urlTrigger = getRequiredMandatoryTrigger(URL_ROLE, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        contentTrigger = getRequiredMandatoryTrigger(CONTENT_ROLE,
                Boolean.FALSE, BuiltinModels.BINARY_ID);
        okExit = getRequiredExit(OK_ROLE, Boolean.FALSE,
                BuiltinModels.NOTHING_ID);
        createFoldersTrigger = getNonRequiredTrigger(CREATE_FOLDERS, Boolean.FALSE, BuiltinModels.BOOLEAN_ID);
    }

}

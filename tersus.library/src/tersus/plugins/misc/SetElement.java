/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.misc;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Role;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.ModelIdHelper;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;


/**
 * @author Youval Bronicki
 *
 */
public class SetElement extends Plugin
{
    private SlotHandler parentTrigger, elementNameTrigger;
    private static final Role PARENT = Role.get("<Parent>");
    private static final Role ELEMENT_NAME = Role.get("<Element Name>");
    private ElementHandler elementHandler;
    private SlotHandler elementTrigger;
    private static final Role VALUE = Role.get("<Value>");

    /* (non-Javadoc)
     * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
        Object parent = parentTrigger.get(context, flow);
        String elementName = (String)elementNameTrigger.get(context, flow);
        ModelId modelId = ModelIdHelper.getModelId(parent);
        InstanceHandler handler = context.getModelLoader().getHandler(modelId);
        elementHandler = handler.getElementHandler(elementName);
        if (elementHandler == null)
            throw new ModelExecutionException("No such element \""+elementName+"\" in " + modelId);
        Object value = elementTrigger.get(context, flow);

        if (value == null)
            elementHandler.remove(context, parent);
        else
        {
            if ( elementHandler.getChildInstanceHandler() != null && ! elementHandler.getChildInstanceHandler().isInstance(value))
                throw new ModelExecutionException("Type mismatch: expected "+elementHandler.getChildModelId() + " but found "+InstanceHandler.getObjectTypeStr(value));
            if (elementHandler.isRepetitive())
                elementHandler.accumulate(context, parent, value);
            else
                elementHandler.set(context, parent, value);
        }
    }


    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        parentTrigger = getRequiredMandatoryTrigger(PARENT, Boolean.FALSE, null);
        elementNameTrigger = getRequiredMandatoryTrigger(ELEMENT_NAME, Boolean.FALSE, BuiltinModels.TEXT_ID);
        elementTrigger = getNonRequiredTrigger(VALUE, Boolean.FALSE, null);
    }

}

/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.misc;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.plugins.misc.Excel.TableTitleRow;
import tersus.runtime.CompositeDataHandler;
import tersus.runtime.DateAndTimeHandler;
import tersus.runtime.DateHandler;
import tersus.runtime.EngineException;
import tersus.runtime.Field;
import tersus.runtime.FlowInstance;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.NumberHandler;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.InvalidInputException;
import tersus.util.Misc;

public class LoadCSVTable extends Plugin
{
    public static final Role TEXT = Role.get("<Text>");

    public static final Role ROWS = Role.get("<Rows>");

    public static void main(String[] args) throws Exception
    {
        FileReader in = new FileReader("c:/temp/test.csv");
        String[][] table = parseCSV(in);
        for (int i = 0; i < table.length; i++)
        {
            for (int j = 0; j < table[i].length; j++)
            {
                System.out.print(String.valueOf(table[i][j]));
                System.out.print('\t');
            }
            System.out.println();
        }

    }

    private SlotHandler textTrigger;

    private SlotHandler rowsExit;

    private CompositeDataHandler rowHandler;

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        this.textTrigger = getRequiredMandatoryTrigger(TEXT, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        this.rowsExit = getRequiredExit(ROWS, Boolean.TRUE, null);
        this.rowHandler = (CompositeDataHandler) checkDataType(rowsExit,
                Boolean.FALSE, false);
    }

    public void start(RuntimeContext context, FlowInstance flow)
    {
        String text = getTriggerText(context, flow, textTrigger);
        boolean skipErrors = false;
        try
        {
            String[][] data = parseCSV(new StringReader(text));
            ITableData tableData = new StringTableData(data);
            TableTitleRow titleRow = findTableTitleRow(context, tableData, 0,
                    rowHandler);
            for (int r = titleRow.row + 1; r <= tableData.getLastRowNum(); r++)
            {
                Object rowObj = null;

                try
                {
                    rowObj = loadTableRow(context, titleRow.columns, tableData,
                            r, rowHandler);
                }
                catch (IllegalArgumentException e)
                {
                    if (skipErrors)
                        continue; // Ignore erroneous rows
                    throw new ModelExecutionException("Error in table Row '"
                            + (r + 1) + "'", e.getMessage(), e);
                }

                if (Tables.isEmpty(context,rowHandler, rowObj))
                    continue; // Ignore rows with no relevant data

                if (rowsExit.isRepetitive())
                {
                    accumulateExitValue(rowsExit, context, flow, rowObj);
                }
                else
                {
                    setExitValue(rowsExit, context, flow, rowObj);
                    break;
                }
            }

        }
        catch (IOException e)
        {
            throw new EngineException("Failed to parse CSV text", null, e);
        }
    }

    public static TableTitleRow findTableTitleRow(RuntimeContext context,
            ITableData table, int startRow, CompositeDataHandler rowHandler)
    {
        TableTitleRow tableTitleRow = new Excel().new TableTitleRow();

        if (rowHandler.getNumberOfElements() < 1)
            throw new EngineException("Need at least one item in each row",
                    null, null);
        Field[] fields = rowHandler.getFields().getFields();
        tableTitleRow.columns = new int[fields.length];

        List bestMatchMissingColumns = null;

        for (int r = startRow; r <= table.getLastRowNum(); r++)
        {
            List missingColumns = new ArrayList();
            for (int iField = 0; iField < fields.length; iField++)
            {
                tableTitleRow.columns[iField] = -1;
                for (int c = table.getFirstCellNum(r); c <= table
                        .getLastCellNum(r); c++)
                {
                    if (fields[iField].name.equals(table.getCellAsString(r, c)))
                        tableTitleRow.columns[iField] = (short) c;
                }

                if (tableTitleRow.columns[iField] < 0)
                    missingColumns.add(fields[iField].name);
            }

            if (missingColumns.isEmpty())
            {
                tableTitleRow.row = r;
                return tableTitleRow;
            }
            else if ((bestMatchMissingColumns == null)
                    || (bestMatchMissingColumns.size() > missingColumns.size()))
                bestMatchMissingColumns = missingColumns;
        }

        if (bestMatchMissingColumns != null)
            throw new InvalidInputException(
                    "Bad file format - missing columns: "
                            + Misc
                                    .concatenateList(bestMatchMissingColumns,
                                            ","));
        else
            throw new InvalidInputException("Bad file format - empty file");
    }

    public static Object loadTableRow(RuntimeContext context, int[] columns,
            ITableData table, int row, CompositeDataHandler rowHandler)
    {
        Object rowObj = rowHandler.newInstance(context);
        Field[] fields = rowHandler.getFields().getFields();
        for (int i = 0; i < fields.length; i++)
        {
            int col = columns[i];
            LeafDataHandler leafHandler = fields[i].getValueHandler();
            Object leafObj = null;
            if ((leafHandler instanceof DateHandler || leafHandler instanceof DateAndTimeHandler)
                    && table.isDate(row, col))
            {
                Date date = table.getCellAsDate(row, col);
                leafObj = leafHandler.newInstance(context, date);
            }
            else if (leafHandler instanceof NumberHandler
                    && table.isNumber(row, col))
            {
                Number number = table.getCellAsNumber(row, col);
                leafObj = leafHandler.newInstance(context, number);
            }
            else
            {
                String stringValue = table.getCellAsString(row, col);
                leafObj = leafHandler.newInstance(context, stringValue, false);
            }

            fields[i].setValue(context, rowObj, leafObj);
        }
        return rowObj;
    }

    private static final int REG = 0;

    private static final int QUOTE = 1;

    public static String[][] parseCSV(Reader in) throws IOException
    {
        int rowLength = 0;
        BufferedReader in0 = new BufferedReader(in);
        String line = null;
        ArrayList rows = new ArrayList();
        StringBuffer cellBuffer = new StringBuffer();
        while ((line = in0.readLine()) != null)
        {
            ArrayList row = new ArrayList();
            cellBuffer.setLength(0);
            rows.add(row);
            int mode = REG;
            for (int i = 0; i < line.length(); i++)
            {
                char c = line.charAt(i);
                char next = i + 1 < line.length() ? line.charAt(i + 1) : 0;

                switch (mode)
                {
                    case REG:
                        switch (c)
                        {
                            case ',': // Next cell

                                if (cellBuffer.length() > 0)
                                    row.add(cellBuffer.toString());
                                else
                                    row.add(null);
                                cellBuffer.setLength(0);
                                break;
                            case '"':
                                mode = QUOTE;
                                break;
                            default:
                                cellBuffer.append(c);

                        }
                        break;
                    case QUOTE:
                        if (c == '"')
                        {
                            if (next == '"')
                            {
                                ++i; // Skip next
                                cellBuffer.append('"');
                            }
                            else
                                mode = REG;
                        }
                        else
                            cellBuffer.append(c);
                        break;
                }
            }
            if (cellBuffer.length() > 0)
                row.add(cellBuffer.toString());
            else
                row.add(null);
            rowLength = Math.max(rowLength, row.size());
        }
        String[][] out = new String[rows.size()][rowLength];
        for (int i = 0; i < rows.size(); i++)
        {
            List row = (List) rows.get(i);
            for (int j = 0; j < row.size(); j++)
            {
                out[i][j] = (String) row.get(j);
            }
        }
        return out;
    }
}

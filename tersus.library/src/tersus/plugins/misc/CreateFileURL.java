/************************************************************************************************
 * Copyright (c) 2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.misc;

import java.io.File;
import java.net.MalformedURLException;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that creates a file URL from a file system path.
 * 
 * @author Youval Bronicki
 * 
 */
public class CreateFileURL extends Plugin
{
    private static final Role PATH_ROLE = Role.get("<Path>");

    private static final Role URL_ROLE = Role.get("<URL>");

    private SlotHandler pathTrigger, urlExit;

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
        String path = (String) pathTrigger.get(context, flow);
        String url = null;
        if (path.startsWith("\\\\")
                && System.getProperty("os.name").indexOf("Windows") < 0)
        {
            url = "smb:" + path.replace('\\', '/');
        }
        else
        {
            File file = new File(path);
            try
            {
                url = file.toURI().toURL().toExternalForm();
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
                throw new ModelExecutionException(
                        "Failed to create url for path " + path);
            }
        }
        setExitValue(urlExit, context, flow, url);
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
     */
    public void resume(RuntimeContext context, FlowInstance flow)
    { // Nothing to do (This is an action)
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
     * 
     * Create a triggers sorted by lexicographically (by role)
     */
    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        pathTrigger = getRequiredMandatoryTrigger(PATH_ROLE, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        urlExit = getRequiredExit(URL_ROLE, Boolean.FALSE,
                BuiltinModels.TEXT_ID);

    }
}
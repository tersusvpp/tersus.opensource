/*
 * Copyright (c) 2003-2022 Tersus Software Ltd.
 * 
 * Created on 12/10/2004
 *
 */

package tersus.plugins.misc;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import tersus.InternalErrorException;
import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.plugins.misc.Excel;
import tersus.plugins.misc.Tables;
import tersus.plugins.misc.Excel.TableTitleRow;
import tersus.runtime.BinaryValue;
import tersus.runtime.CompositeDataHandler;
import tersus.runtime.DataHandler;
import tersus.runtime.ElementHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that inserts data into a simple sub-table of an Excel
 * sheet.
 * 
 * @param <File>
 *            The Excel file.
 * @param <Sheet
 *            Name> The name of the sheet in the file into which data is
 *            inserted (if missing, the default is to use the first sheet).
 * @param <Title>
 *            An optional title of the sub-table to be updated in the sheet (if
 *            exists, the table is searched in the sheet starting at the
 *            location where the title is found; if no title is provided, then
 *            the table is searched starting at the beginning of the sheet).
 * @param <Rows>
 *            Multiple data structures (collections of leaves), where the role
 *            of each leaf corresponds to the title of one columns to update
 *            (not necessarily consecutive columns in the Excel sheet, but all
 *            column titles should appear in the same row).
 * @param <
 *            <File>>The updated Excel file (with the rows replacing the rows
 *            of the sub-table, all formatted according to the first row
 *            following the row contaning the column titles of the sub-table).
 * @param Error
 *            Optional error message
 * 
 * @author Ofer Brandes
 *  
 */

public class UpdateExcelTable extends Plugin
{
    private static final Role INPUT_FILE_ROLE = Role.get("<File>");

    private static final Role SHEET_NAME_ROLE = Role.get("<Sheet Name>");

    private static final Role TITLE_ROLE = Role.get("<Title>");

    private static final Role ROWS_ROLE = Role.get("<Rows>");

    private static final Role OUTPUT_ROLE = Role.get("<<File>>");

    SlotHandler inputTrigger, sheetNameTrigger, titleTrigger, rowsTrigger;

    SlotHandler outputExit;

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
     */
    @SuppressWarnings("unchecked")
	public void start(RuntimeContext context, FlowInstance flow)
    {
		try
        {
            // Open the workbook
            BinaryValue data = (BinaryValue) inputTrigger.get(context, flow);
            Workbook wb = Excel.openWorkBook(context, data);
            CreationHelper helper = wb.getCreationHelper();

            // Get the specified (or first) sheet
            String sheetName = (sheetNameTrigger == null ? null
                    : (String) sheetNameTrigger.get(context, flow));
            Sheet sheet = Excel.getSheet(context, wb, sheetName);

            // Locate the title
            String title = (titleTrigger == null ? null : (String) titleTrigger
                    .get(context, flow));
            int startRow = Excel.findTitleRow(context, sheet, title) + 1;

            // Locate the sub-table titles
            CompositeDataHandler rowHandler = (CompositeDataHandler) rowsTrigger
                    .getChildInstanceHandler();
            TableTitleRow tableTitleRow = Excel.findTableTitleRow(context,
                    sheet, startRow, rowHandler);

            // Get input row(s)
            List<Object> inputs = new ArrayList<Object>();
            if (rowsTrigger.isRepetitive())
                inputs.addAll((List<Object>) rowsTrigger.get(context, flow));
            else
                inputs.add(rowsTrigger.get(context, flow));

            // Count the initial number of rows in the Excel sheet (at least one
            // should exist)
            int rowsInSubTable = 0;
            for (int r = tableTitleRow.row + 1; r <= sheet.getLastRowNum(); r++)
            {
                Row excelRow = sheet.getRow(r);
                boolean correctFormat = false;
                if (excelRow != null)
                {
                	try
                	{
                		Object rowObj = Excel.loadExcelRow(context,
                				tableTitleRow.columns, excelRow, rowHandler);
                        correctFormat = !Tables.isEmpty(context, rowHandler, rowObj);
                	}
                	catch (Exception e)
                	{
                		//Assuming the exception is called by a row which doesn't match the row structure
                		correctFormat = false;
                	}
                	
                }
                if (correctFormat)
                    rowsInSubTable++;
                else
                    break;
            }

            if (rowsInSubTable < 1)
                throw new EngineException("Excel contains no data rows starting at row "
                        + (sheet.getLastRowNum() + 1 + 1)
                        + " - cannot replace non-existing rows",
                        null, null);

            // Ensure the Excel sheet contains the required number of rows
            // (additional rows formatted as the first one)
            int addedRows = inputs.size() - rowsInSubTable;
            if (addedRows > 0)
            {
                // Duplicate first row (must add rows in the middle, not the
                // end, in case there are functions referring to the whole range
                // of rows)
            	if (tableTitleRow.row +2 < sheet.getLastRowNum())
            		sheet.shiftRows(tableTitleRow.row + 2, sheet.getLastRowNum(),
                        addedRows, true, false);
            	if (tableTitleRow.row < sheet.getLastRowNum())
            	{
            		Row excelRow = sheet.getRow(tableTitleRow.row + 1);
            		for (int r = tableTitleRow.row + 1 + 1; r <= tableTitleRow.row
                        + 1 + addedRows; r++)
            			Excel.duplicateRow(context, sheet, excelRow, r);
            	}
            }
            else
            {
                 if (addedRows < 0)
                 {
                	int firstRowToDelete = tableTitleRow.row +  inputs.size() + 1; 
                	int lastRowToDelete = tableTitleRow.row + rowsInSubTable;
                	
                	for (int i=lastRowToDelete; i>= firstRowToDelete; i--)
                	{
                        Row excelRow = sheet.getRow(i);
                        sheet.removeRow(excelRow);
                	}
                
                 }
            }

            // Place the input rows in the Excel sheet (replacing existing
            // values)
            for (int r = 1; r <= inputs.size(); r++)
            {
                Row excelRow = sheet.getRow(tableTitleRow.row + r);
                if (!(inputs.get(r - 1) instanceof Object[]))
                    throw new EngineException("Input Row " + r
                            + " is not compoite", null, null);
                Object[] dataRow = (Object[]) inputs.get(r - 1);

                for (int col = 0; col < tableTitleRow.columns.length; col++)
                {
                    Cell cell = excelRow
                            .getCell(tableTitleRow.columns[col]);
                    if (cell == null)
                        	cell = excelRow.createCell(tableTitleRow.columns[col]);
                    ElementHandler elementHandler = rowHandler
                            .getElementHandler(col);
                    Object leafValue = elementHandler.get(context, dataRow);
                    if (leafValue == null)
                    {
                        cell.setCellType(Cell.CELL_TYPE_BLANK);
                    }
                    else
                    {
                        DataHandler leafHandler = (DataHandler) elementHandler
                                .getChildInstanceHandler();
                        ExcelWriting.setCellContent(context, cell, leafValue, leafHandler, r, helper);
                    }
                }
            }

            // Output the updated file
            BinaryValue updatedData = ExcelWriting.toBinaryValue(context, wb);

            setExitValue(outputExit, context, flow, updatedData);

            //			// temporary patch - write out the modofied file to
            // "C:/temp/Modified Excel.xls"
            //			Excel.saveTempCopy(wb);
        }
        catch (EngineException e)
        {
            fireError(context, flow, e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
     */public void resume(RuntimeContext context, FlowInstance flow)
    {
        // Nothing to do (This is an action)
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
     */
    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);

        inputTrigger = getRequiredMandatoryTrigger(INPUT_FILE_ROLE,
                Boolean.FALSE, BuiltinModels.BINARY_ID);
        sheetNameTrigger = getNonRequiredMandatoryTrigger(SHEET_NAME_ROLE,
                Boolean.FALSE, BuiltinModels.TEXT_ID);
        titleTrigger = getNonRequiredMandatoryTrigger(TITLE_ROLE,
                Boolean.FALSE, BuiltinModels.TEXT_ID);
        rowsTrigger = getRequiredMandatoryTrigger(ROWS_ROLE, Boolean.TRUE, null);
        checkFlatDataType(rowsTrigger);

        outputExit = getRequiredExit(OUTPUT_ROLE, Boolean.FALSE,
                BuiltinModels.BINARY_ID);
    }
}
/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.misc;

import javax.servlet.http.Cookie;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.Misc;
import tersus.webapp.WebContext;

public class GetCookie extends Plugin
{

    private static final Role NAME = Role.get("<Name>");

    private static final Role VALUE = Role.get("<Value>");

    private SlotHandler nameTrigger, valueExit;

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        nameTrigger = getRequiredMandatoryTrigger(NAME, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        valueExit = getRequiredExit(VALUE, Boolean.FALSE, null);
    }

    public void start(RuntimeContext context, FlowInstance flow)
    {
        super.start(context, flow);
        String name = (String) nameTrigger.get(context, flow);
        name = Misc.sqlize(name);
        if (context instanceof WebContext)
        {
            Cookie[] cookies = ((WebContext) context).getRequest().getCookies();
            if (cookies != null)
            {
                for (int i = 0; i < cookies.length; i++)
                {
                    Cookie cookie = cookies[i];
                    if (name.equals(cookie.getName()))
                    {
                        String valueStr = cookie.getValue();
                        if (valueStr != null && valueStr.length() > 0)
                        {
                            InstanceHandler valueType = valueExit
                                    .getChildInstanceHandler();
                            if (valueType != null)
                            {
                                Object valueObj = valueType.newInstance(
                                        context, valueStr, null, false);
                                setExitValue(valueExit, context, flow, valueObj);
                            }
                        }
                    }
                }
            }

        }
    }

}

/************************************************************************************************
 * Copyright (c) 2003-2012 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.misc;

import java.io.FileNotFoundException;
import java.io.IOException;

import jcifs.smb.SmbFile;
import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.model.validation.Problems;
import tersus.runtime.BinaryDataHandler;
import tersus.runtime.BinaryValue;
import tersus.runtime.ContextPool;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.TextHandler;
import tersus.util.FileUtils;

public class ReadResource extends Plugin
{
	private static final Role URL = Role.get("<URL>");
	private static final Role NOT_FOUND = Role.get("<Not Found>");
	private static final Role NOT_AUTHORIZED = Role.get("<Not Authorized>");

	private static final Role CONTENT = Role.get("<Content>");

	SlotHandler trigger, exit, notFoundExit, notAuthorizedExit;
	private boolean isBinary;

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext,
	 * tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		String urlString = (String) trigger.get(context, flow);

		Object out;
		try
		{
		if (urlString.startsWith("smb:"))
		{
			byte[] fileContent = readSMBFile(urlString);
			if (isBinary)
				out = getBinaryValue(fileContent);
			else
				out = getString(fileContent);
		}
		else
		{
			ContextPool contextPool = context.getContextPool();
			if (isBinary)
				out = getBinaryValue(contextPool.readResource(getModelId().getPackageId(),
						urlString));
			else

				out = contextPool.readResourceAsString(getModelId().getPackageId(), urlString);
		}
		if (out == null)
		{
			if (notFoundExit == null)
				throw new ModelExecutionException("Failed to read resource '" + urlString + "'");
			else
				chargeEmptyExit(context, flow, notFoundExit);
		}
		else
		{
			setExitValue(exit, context, flow, out);
		}
		}
		catch (IOException e)
		{
			if (notFoundExit != null && ( this.getPluginVersion() == 0 || e instanceof FileNotFoundException))
			{
				chargeEmptyExit(context, flow, notFoundExit);
			}
			else if (notAuthorizedExit != null && e.getMessage().contains(" 403 "))
				chargeEmptyExit(context, flow, notAuthorizedExit);
			else
				throw new ModelExecutionException("Failed to read resource "+urlString, e);
		}

	}

	private String getString(byte[] data)
	{
		return data == null ? null : new String(data);
	}

	private BinaryValue getBinaryValue(byte[] data)
	{
		return data == null ? null : new BinaryValue(data);
	}

	private byte[] readSMBFile(String urlString)
	{
		try
		{
			SmbFile f = new SmbFile(urlString);
			return FileUtils.readBytes(f.getInputStream(), true);
		}
		catch (Exception e)
		{
			throw new ModelExecutionException("Failed to read remote file", urlString, e);
		}
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		trigger = getRequiredMandatoryTrigger(URL, Boolean.FALSE, BuiltinModels.TEXT_ID);
		exit = getRequiredExit(CONTENT, Boolean.FALSE, null);
		isBinary = exit.getChildInstanceHandler() instanceof BinaryDataHandler;
		if (!isBinary && !(exit.getChildInstanceHandler() instanceof TextHandler))
		{
			notifyInvalidModel(CONTENT, Problems.INVALID_EXIT, "Data type must be Text or Binary");
		}
		notFoundExit = getNonRequiredExit(NOT_FOUND, Boolean.FALSE, BuiltinModels.NOTHING_ID);
		notAuthorizedExit = getNonRequiredExit(NOT_AUTHORIZED, Boolean.FALSE, BuiltinModels.NOTHING_ID);
	}

}
/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.misc;

import java.util.Date;

public interface ITableData
{

    int getFirstRowNum();
    int getLastRowNum();

    int getLastCellNum(int row);

    int getFirstCellNum(int row);

    String getCellAsString(int row, int col);

    boolean isDate(int row, int col);

    Date getCellAsDate(int row, int col);

    Number getCellAsNumber(int row, int col);

    boolean isNumber(int row, int col);

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.misc;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.FileUtils;

/**
 * @author Youval Bronicki
 *  
 */
public class GetFolderContents extends Plugin
{
    public static final Role FOLDER_URL = Role.get("<Folder URL>");

    public static final Role FILE_URLS = Role.get("<File URLs>");

    public static final Role FOLDER_URLS = Role.get("<Folder URLs>");

    public static final Role NO_SUCH_FOLDER = Role.get("<No Such Folder>");

    private SlotHandler folderURLTrigger, fileURLsExit, folderURLsExit,
            noSuchFolderExit;

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        folderURLTrigger = getRequiredMandatoryTrigger(FOLDER_URL,
                Boolean.FALSE, BuiltinModels.TEXT_ID);
        fileURLsExit = getNonRequiredExit(FILE_URLS, Boolean.TRUE,
                BuiltinModels.TEXT_ID);
        folderURLsExit = getNonRequiredExit(FOLDER_URLS, Boolean.TRUE,
                BuiltinModels.TEXT_ID);
        noSuchFolderExit = getNonRequiredExit(NO_SUCH_FOLDER, Boolean.TRUE,
                BuiltinModels.TEXT_ID);
    }

    public void start(RuntimeContext context, FlowInstance flow)
    {
        String parentURL = (String) folderURLTrigger.get(context, flow);
        URL url;
        try
        {
            url = new URL(parentURL);
        }
        catch (MalformedURLException e)
        {
            throw new ModelExecutionException("Invalid URL '" + parentURL + "'");
        }
        if ("file".equals(url.getProtocol()))
        {
            File file = FileUtils.URLtoFile(url);
            if (file.isDirectory())

            {
                String[] contents = file.list();
                if (!parentURL.endsWith("/"))
                    parentURL += "/";
                for (int i = 0; i < contents.length; i++)
                {
                    String name = contents[i];
                    String childURL = parentURL + name;
                    File childFile = new File(file, name);
                    if (childFile.isDirectory())
                        accumulateExitValue(folderURLsExit, context, flow,
                                childURL);
                    else
                        accumulateExitValue(fileURLsExit, context, flow,
                                childURL);
                }

            }
            else
            {
                if (noSuchFolderExit != null)
                    setExitValue(noSuchFolderExit, context, flow, parentURL);
                else
                    throw new ModelExecutionException("No such folder:"
                            + parentURL);
            }

        }

    }

}
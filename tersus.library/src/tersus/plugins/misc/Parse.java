/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.misc;

import java.util.StringTokenizer;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.Role;
import tersus.model.validation.Problems;
import tersus.runtime.CompositeDataHandler;
import tersus.runtime.ElementHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.FlowStatus;
import tersus.runtime.InstanceHandler;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.EscapeUtil;

/**
 * 
 * A plugin for parsing formatted structures
 * 
 * (The initial Implementation only supports simple separated concatenations)
 * 
 * @author Youval Bronicki
 * 
 */
public class Parse extends Plugin
{
    private static final String FORMAT_SEPARATOR = "format.separator";
    private static final String MIN_WIDTH = "format.minWidth";
    private static final String MAX_WIDTH = "format.maxWidth";
    private InstanceHandler exitType;
    private static Role TEXT = Role.get("<Text>");
    private static Role STRUCTURE = Role.get("<Structure>");
    private SlotHandler textTrigger;
    private SlotHandler structureExit;
    private String separator;
    private int[] widths = null;

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext,
     *      tersus.runtime.FlowInstance)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
        traceStarted(context, flow);
        setStatus(flow, FlowStatus.STARTED);
        String inputText = (String) textTrigger.get(context, flow);
        Object exitValue = exitType.newInstance(context);
        if (separator != null)
            parseSeparated(context, flow, inputText, exitValue);
        else
            parseFixed(context, flow, inputText, exitValue);

        structureExit.set(context, flow, exitValue);

        setStatus(flow, FlowStatus.FINISHED);
        traceFinish(context, flow);
    }

    private void parseFixed(RuntimeContext context, FlowInstance flow, String inputText, Object exitValue)
    {
        int position = 0;
        int elementNum = 0;
        for (ElementHandler e : exitType.elementHandlers)
        {
            int width = widths[elementNum];
            if (position + width <= inputText.length())
            {
                String token = inputText.substring(position, position + width);
                e.create(context, exitValue, token, false);
            }
            else if (e.isMandatory())
            {
                fireError(context, flow, new EngineException("Parse Failed", "Text=" + inputText
                        + " missing value for " + e.getRole(), null));
            }
            position += width;
            ++elementNum;
        }
    }

    private void parseSeparated(RuntimeContext context, FlowInstance flow, String inputText, Object exitValue)
    {
        StringTokenizer tokenizer = new StringTokenizer(inputText, separator);

        int elementNumber = 0;
        ElementHandler[] outputElements = exitType.elementHandlers;
        int numberOfElements = outputElements.length;
        while (tokenizer.hasMoreTokens())
        {
            if (elementNumber >= numberOfElements)
                fireError(context, flow, new EngineException("Parse Failed", "Text=" + inputText + " too many tokens ("
                        + (elementNumber + 1) + ")", null));
            String token = tokenizer.nextToken();
            outputElements[elementNumber].create(context, exitValue, token, false);
            elementNumber++;

        }
        while (elementNumber < numberOfElements)
        {
            if (outputElements[elementNumber].isMandatory())
            {
                fireError(context, flow, new EngineException("Parse Failed", "Text=" + inputText
                        + " no value for mandatory element '" + outputElements[elementNumber].getRole() + "'", null));
            }
            elementNumber ++;

        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
     */
    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        textTrigger = getRequiredMandatoryTrigger(TEXT, Boolean.FALSE, BuiltinModels.TEXT_ID);
        structureExit = getRequiredExit(STRUCTURE, Boolean.FALSE, null);
        exitType = structureExit.getChildInstanceHandler();
        Model exitModel = model.getElement(structureExit.getRole()).getReferredModel();
        separator = EscapeUtil.unescape((String) exitModel.getProperty(FORMAT_SEPARATOR));
        if (separator == null)
            widths = getFixedWidths(exitType);
        if (!(exitType instanceof CompositeDataHandler))
            notifyInvalidModel(STRUCTURE, Problems.INVALID_EXIT, STRUCTURE + " must be a concatenation");

    }

    private int[] getFixedWidths(InstanceHandler type)
    {
        Model m = type.getModel();
        StringBuffer errors = new StringBuffer();
        int[] widths = new int[m.getElements().size()];
        int i = 0;
        for (ModelElement e : m.getElements())
        {
            int min = getIntPropertyValue(e, MIN_WIDTH, errors);
            int max = getIntPropertyValue(e, MAX_WIDTH, errors);
            if (min > 0 && max > 0)
            {
                if (min != max)
                    addLine(errors, MIN_WIDTH + " and " + MAX_WIDTH + " currently must be equal");
                else
                    widths[i]=min;
            }
            ++i;
        }
        if (errors.length() > 0)
        {
            notifyInvalidModel(STRUCTURE, Problems.INVALID_EXIT, "If "+FORMAT_SEPARATOR + " is not specified, "+MIN_WIDTH+ " and "+MAX_WIDTH + " should be set for all elements (and must currently be equal)");
        }
        return widths;
    }

    private int getIntPropertyValue(ModelElement e, String pName, StringBuffer errors)
    {
        String error = null;
        int intValue = -1;
        Object pValue = e.getProperty(pName);
        if (pValue == null)
            error = "Missing property " + pName + " in element " + e.getRole() + "\n";
        else
        {
            try
            {
                intValue = Integer.parseInt((String) pValue);

            }

            catch (NumberFormatException e1)
            {
                error = "Invalid value (" + pValue + ") for property " + pName + " in element " + e.getRole() + "\n";
            }
            addLine(errors, error);
        }
        return intValue;
    }

    private void addLine(StringBuffer buffer, String line)
    {
        if (line != null)
        {
            if (buffer.length() > 0)
                buffer.append("\n");
            buffer.append(line);
        }
    }
}

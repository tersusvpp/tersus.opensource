/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.misc;

import java.io.StringWriter;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelIdHelper;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.webapp.JavaScriptFormat;

/**
 * An atomic flow handler that parses a JSON String
 * (JSON stands for Javascript Object Notation - see www.json.org)
 * 
 * @author Youval Bronicki
 *
 */

public class CreateJSON extends Plugin
{
	private static final Role OUTPUT_ROLE = Role.get("<JSON Text>");
	private static final Role INPUT_ROLE = Role.get("<Value>");
	private static final Role FORCE_DEFAULT_FORMATS = Role.get("<Force Default Formats>");

	private static final boolean DEBUG = false;
	SlotHandler trigger, exit, defaultFormatTrigger;
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
	    Object value = trigger.get(context, flow);
	    StringWriter buffer = new StringWriter();
	    ModelId valueModelId = ModelIdHelper.getCompositeModelId(value);
	    if (valueModelId == null)
	        valueModelId = trigger.getChildModelId();
	    boolean forceDefault = getTriggerValue(defaultFormatTrigger, context, flow, true);
	    JavaScriptFormat.serialize(value, context.getModelLoader().getHandler(valueModelId), buffer, context, forceDefault);
	    setExitValue(exit, context, flow, buffer.toString());
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		trigger = getRequiredMandatoryTrigger(INPUT_ROLE, null, null);
		defaultFormatTrigger = getNonRequiredTrigger(FORCE_DEFAULT_FORMATS, Boolean.FALSE, BuiltinModels.BOOLEAN_ID);
		exit = getRequiredExit(OUTPUT_ROLE, Boolean.FALSE, BuiltinModels.TEXT_ID);
	}


}

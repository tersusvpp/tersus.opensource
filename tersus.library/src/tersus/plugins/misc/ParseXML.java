/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.misc;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.ModelException;
import tersus.model.ModelId;
import tersus.model.Role;
import tersus.model.validation.Problems;
import tersus.runtime.BinaryValue;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that parses an XML document
 * (for which a data model has already been created via 'CreateModelByXmlExample').
 * 
 * @author Ofer Brandes
 *
 */

public class ParseXML extends Plugin
{
	private static final Role INPUT_ROLE = Role.get("<XML Document>");
	private static final Role OUTPUT_ROLE = Role.get("<Parsed Tree>");

	private static final boolean DEBUG = false;
	SlotHandler inputHandler, exitHandler;
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Object input = inputHandler.get(context, flow);
		
		//		System.setProperty ( "org.xml.sax.driver" , "org.apache.xerces.parsers.SAXParser" );
		//		XMLReader xr = XMLReaderFactory.createXMLReader();
		//		FUNC3 Restore the usage of 'XMLReaderFactory'?

		try
		{
			InputSource source;
			if (input instanceof BinaryValue)
				source = new InputSource(new ByteArrayInputStream(((BinaryValue) input).toByteArray()));
			else if (input instanceof String)
				source = new InputSource(new StringReader((String) input));
			else
				throw new ModelExecutionException("Unexpected type for trigger "+INPUT_ROLE);
			XMLReader xr = XMLReaderFactory.createXMLReader();
			
			// xr.setFeature("http://xml.org/sax/features/validation", false); // Ofer, 29/7/07 (just tried something)
			XmlDocumentHandler parser = new XmlDocumentHandler(context, flow, exitHandler, false, input);
			xr.setContentHandler(parser);
			xr.setErrorHandler(parser);
			xr.parse(source);
		}
		catch (IOException e)
		{
			throw new EngineException("XML Parsing Failed", null, e);
		}
		catch (SAXException e)
		{
			throw XmlDocumentHandler.catchSAXException (context, e);        
		}
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		inputHandler = getRequiredMandatoryTrigger(INPUT_ROLE, Boolean.FALSE, null);
		ModelId inputType = inputHandler.getChildModelId();
		if (!BuiltinModels.BINARY_ID.equals(inputType) && ! BuiltinModels.TEXT_ID.equals(inputType))
			notifyInvalidModel(inputHandler.getRole(),Problems.INVALID_TRIGGER, INPUT_ROLE + " must be either Text or Bindary");
				
		exitHandler = getRequiredExit(OUTPUT_ROLE, Boolean.FALSE, null);
	}
}

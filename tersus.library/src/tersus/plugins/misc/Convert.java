package tersus.plugins.misc;

import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.CompositeDataHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

public class Convert extends Plugin
{
	private static final Role INPUT = Role.get("<Input>");
	private static final Role OUTPUT = Role.get("<Output>");
	private static final Role STRICT = Role.get("<Strict>");
	
	
	private SlotHandler inputTrigger, outputExit, strictTrigger;
	@Override
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		inputTrigger = getRequiredMandatoryTrigger(INPUT, Boolean.FALSE, null);
		strictTrigger = getNonRequiredTrigger(STRICT, Boolean.FALSE, BuiltinModels.BOOLEAN_ID);
		outputExit = getRequiredExit(OUTPUT, Boolean.FALSE, null);
	}
	
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Object input = getTriggerValue(context, flow, inputTrigger);
		if (!(outputExit.getChildInstanceHandler() instanceof CompositeDataHandler))
			throw new ModelExecutionException("Convert currently only supports Data Structure as output");
		boolean strict = getTriggerValue(strictTrigger, context, flow, true);
	
		if (input instanceof Map<?, ?> ||  input instanceof JSONObject)
		{
			try
			{
				Object output = ParseJSON.convert(context, input, outputExit, !strict);
				if (output != null)
					setExitValue(outputExit, context, flow, output);			}
			catch (JSONException e)
			{
				throw new ModelExecutionException("Data conversion failed",e);
			}
		}
		else
		{
			throw new ModelExecutionException("Convert currently only supports map as input");
		}
		
	}

}

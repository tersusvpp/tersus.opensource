/************************************************************************************************
 * Copyright (c) 2003-2022 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 *************************************************************************************************/
package tersus.plugins.misc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ConditionalFormatting;
import org.apache.poi.ss.usermodel.ConditionalFormattingRule;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.SheetConditionalFormatting;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Comparator;

import tersus.InternalErrorException;
import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Path;
import tersus.model.Role;
import tersus.model.validation.Problems;
import tersus.plugins.misc.Excel;
import tersus.runtime.BinaryValue;
import tersus.runtime.DateAndTimeHandler;
import tersus.runtime.DateHandler;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.ModelIdHelper;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * @author Youval Bronicki
 * 
 */
public class ExportToExcel extends Plugin
{

	private static final Role RECORDS = Role.get("<Records>");
	private static final Role SHEETS = Role.get("<Sheets>");
	private static final Role FILE = Role.get("<File>");
	private static final Role SHEET_NAME = Role.get("<Sheet Name>");
	private static final Role TEMPLATE = Role.get("<Template>");

	private static final Role CELL = Role.get("Cell");
	private static final Role VALUE = Role.get("Value");
	private static final Role FORMAT = Role.get("Format");
	private static final Role FILE_FORMAT = Role.get("<File Format>");
	private static final String XLSX="xlsx";
	private static final Role LEVEL_COLUMN = Role.get("<Level Column>");
	private static final Role STYLE_COLUMN = Role.get("<Style Column>");

	private static final String DATE_FORMAT = "d-mmm-yy";
	private static final String DATE_AND_TIME_FORMAT = "m/d/yy h:mm";

	private SlotHandler sheetNameTrigger, recordsTrigger, sheetsTrigger, fileExit, levelColumnTrigger,
			styleColumnTrigger, templateTrigger, fileFormatTrigger;
	private ElementHandler sheetNameElement;
	private ElementHandler recordsElement;
	private ElementHandler cellElement;
	private ElementHandler valueElement;
	private ElementHandler formatElement;
	private InstanceHandler rowHandler;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext,
	 * tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		(new Invocation(context, flow)).run();
	}

	private class Template
	{
		HashMap<Object, Row> templateRowMap;
		HashMap<Object, CFTemplate> rowConditionalFormattingMap;
		ArrayList<CFTemplate> preambleConditionalFormatting;
		private int nPreambleRows;
		private Workbook workBook;
		private Sheet templateSheet;
		private int styleColumn;
		private int templateSheetIndex;

		Template(Workbook workBook, int templateSheetIndex, int styleColumn)
		{
			this.templateSheetIndex = templateSheetIndex;
			this.workBook = workBook;
			this.templateSheet = workBook.getSheetAt(templateSheetIndex);
			this.styleColumn = styleColumn;
			templateRowMap = new HashMap<Object, Row>();
			rowConditionalFormattingMap = new HashMap<Object, CFTemplate>();
			preambleConditionalFormatting = new ArrayList<CFTemplate>();
			nPreambleRows = 0;
			if (styleColumn >= 0)
			{
				boolean inPreamble = true;
				for (int rowNum = 0; rowNum <= templateSheet.getLastRowNum(); rowNum++)
				{
					Row row = templateSheet.getRow(rowNum);
					Object content = null;
					if (row != null)
					{
						Cell firstCell = row.getCell(styleColumn);
						content = getCellContent(firstCell);
					}
					if (content != null)
					{
						inPreamble = false;
						if (!templateRowMap.containsKey(content))
							templateRowMap.put(content, row);
					}
					else if (inPreamble)
						++nPreambleRows;
				}
			}
			SheetConditionalFormatting scf = templateSheet.getSheetConditionalFormatting();
			for (int i=0;i<scf.getNumConditionalFormattings();i++)
			{
				ConditionalFormatting cf = scf.getConditionalFormattingAt(i);
				ConditionalFormattingRule[] rules = getRules(cf);
				for (Object key : templateRowMap.keySet())
				{
					 Row templateRow = templateRowMap.get(key);
					int rowNum = templateRow.getRowNum();
					ArrayList<CellRangeAddress> ranges = new ArrayList<CellRangeAddress>();
					for(CellRangeAddress range: cf.getFormattingRanges())
					{
						if (range.getFirstRow() <= rowNum && range.getLastRow()>=rowNum)
						{
							ranges.add(new CellRangeAddress(0,0,range.getFirstColumn(), range.getLastColumn()));
						}
					}
					if (ranges.size() > 0)
						rowConditionalFormattingMap.put(key, new CFTemplate(rules, ranges));
					
				}
				ArrayList<CellRangeAddress> ranges = new ArrayList<CellRangeAddress>();
				for(CellRangeAddress range: cf.getFormattingRanges())
				{
					if (range.getLastRow() <= nPreambleRows)
						ranges.add(new CellRangeAddress(range.getFirstRow(),nPreambleRows, range.getFirstColumn(), range.getLastColumn()));
				}
				if (ranges.size()>0)
					preambleConditionalFormatting.add(new CFTemplate(rules, ranges));
			}
		}

		private Object getCellContent(Cell firstCell)
		{
			if (firstCell == null)
				return null;
			Object content = null;
			switch (firstCell.getCellType())
			{
				case Cell.CELL_TYPE_STRING:
					content = Excel.getCellContentAsString(firstCell);
					break;
				case Cell.CELL_TYPE_NUMERIC:
					content = Double.valueOf(firstCell.getNumericCellValue());
					break;
			}
			return content;
		}

		public void applyStyles(Sheet sheet)
		{
			int firstRow = sheet.getFirstRowNum();
			int lastRow = sheet.getLastRowNum();
			if (styleColumn >= 0)
			{
				for (int rowNum = firstRow; rowNum <= lastRow; rowNum++)
				{
					Row row = sheet.getRow(rowNum);
					if (row == null)
						continue;
					Cell styleCell = row.getCell(styleColumn);
					Object content = getCellContent(styleCell);
					if (content != null)
					{
						Row templateRow = templateRowMap.get(content);
						copyStyle(row, templateRow);
						setConditionalFormatting(row, rowConditionalFormattingMap.get(content));
					}
				}
			}
			else
			{
				copyStyle(sheet.getRow(firstRow), templateSheet.getRow(firstRow));
				for (int rowNum = firstRow + 1; rowNum <= lastRow; rowNum++)
				{
					copyStyle(sheet.getRow(rowNum), templateSheet.getRow(firstRow + 1));
				}
			}
		}

		private void setConditionalFormatting(Row row,
				CFTemplate cfTemplate)
		{
			if (cfTemplate == null)
				return;
			CellRangeAddress[] ranges = new CellRangeAddress[cfTemplate.ranges.length];
			for (int i=0;i<ranges.length;i++)
			{
				CellRangeAddress tr = cfTemplate.ranges[i];
				ranges[i] = new CellRangeAddress(row.getRowNum(), row.getRowNum(), tr.getFirstColumn(), tr.getLastColumn());
			}
			row.getSheet().getSheetConditionalFormatting().addConditionalFormatting(ranges, cfTemplate.rules);
		}

		private void copyStyle(Row row, Row templateRow)
		{
			if (templateRow == null || row == null)
				return;
			if (templateRow.getRowStyle() != null)
				row.setRowStyle(templateRow.getRowStyle());
			row.setHeight(templateRow.getHeight());
			int first = row.getFirstCellNum();
			int last = row.getLastCellNum();
			for (int i = first; i < last; i++)
			{
				Cell templateCell = templateRow.getCell(i);

				Cell dataCell = row.getCell(i);
				if (dataCell == null)
					continue;
				CellStyle style = templateCell == null ? templateRow.getRowStyle()
						: templateCell.getCellStyle();
				if (style != null)
					dataCell.setCellStyle(style);
			}
		
		}

		public Sheet createSheet(String sheetName)
		{
			Sheet sheet = workBook.cloneSheet(templateSheetIndex);
			workBook.setSheetName(workBook.getSheetIndex(sheet), sheetName);
			
			SheetConditionalFormatting scf = sheet.getSheetConditionalFormatting();
			// Remove template conditional formatting
			for (int i=scf.getNumConditionalFormattings()-1; i>=0; i--)
			{
				scf.removeConditionalFormatting(i);
			}
			// Add relevant preamble level formatting 
			for (CFTemplate cft: preambleConditionalFormatting)
			{
				scf.addConditionalFormatting(cft.ranges, cft.rules);
			}
			for (int i = sheet.getLastRowNum(); i >= nPreambleRows; i--)
			{
				Row row = sheet.getRow(i);
				if (row != null)
					sheet.removeRow(row);
			}
			return sheet;
		}
	}

	private class Invocation
	{
		private RuntimeContext context;
		private FlowInstance flow;
		private int levelColumn;
		private Template template;
		private boolean useXslxFormat;
		private CreationHelper helper;
		ArrayList<Sheet> newSheets = new ArrayList<Sheet>();

		Invocation(RuntimeContext context, FlowInstance flow)
		{
			this.context = context;
			this.flow = flow;
			
			useXslxFormat = XLSX.equals(getTriggerValue(fileFormatTrigger, context, flow, "xls").toLowerCase());
		}

		@SuppressWarnings("unchecked")
		public void run()
		{

			Workbook wb = null;
			BinaryValue templateData = (BinaryValue) getTriggerValue(context, flow, templateTrigger);
			levelColumn = getTriggerValue(levelColumnTrigger, context, flow, 0) - 1;
			int styleColumn = getTriggerValue(styleColumnTrigger, context, flow, 0) - 1;

			if (templateData != null)
			{
				wb = Excel.openWorkBook(context, templateData);
				String sheetName = (sheetNameTrigger == null ? null : (String) getTriggerValue(context, flow, sheetNameTrigger));
				int index = (sheetName == null ? 0 : wb.getSheetIndex(sheetName));
				template = new Template(wb, index, styleColumn);
			}
			else
			{
				if (useXslxFormat)
					wb = new XSSFWorkbook();
				else
					wb = new HSSFWorkbook();
			}

			helper = wb.getCreationHelper();
			HashMap<String, CellStyle> styles = new HashMap<String, CellStyle>();

			createStyle(wb, styles, DATE_FORMAT);
			createStyle(wb, styles, DATE_AND_TIME_FORMAT);

			if (recordsTrigger != null)
			{
				List<Object> records = (List<Object>) recordsTrigger.get(context, flow);
				String sheetName = (sheetNameTrigger == null ? "Data" : (String) getTriggerValue(context, flow, sheetNameTrigger));
				addSheet(context, wb, styles, sheetName, records);
			}
			else
			{
				if (sheetsTrigger.isRepetitive())
				{
					List<Object> sheets = (List<Object>) sheetsTrigger.get(context, flow);
					for (int i = 0; i < sheets.size(); i++)
						addSheet(context, wb, styles, sheetsTrigger.getChildInstanceHandler(),
								sheets.get(i));
				}
				else
					addSheet(context, wb, styles, sheetsTrigger.getChildInstanceHandler(),
							sheetsTrigger.get(context, flow));

			}
			if (template != null)
			{
				wb.removeSheetAt(template.templateSheetIndex);
				for (int i=0; i < newSheets.size(); i++)
				{
					wb.setSheetOrder(newSheets.get(i).getSheetName(), template.templateSheetIndex+i);
				}
			}

			BinaryValue file = ExcelWriting.toBinaryValue(context, wb);
			setExitValue(fileExit, context, flow, file);
		}

		/**
		 * @param context
		 * @param wb
		 * @param childInstanceHandler
		 * @param object
		 */
		@SuppressWarnings("unchecked")
		private void addSheet(RuntimeContext context, Workbook wb,
				HashMap<String, CellStyle> styles, InstanceHandler sheetHandler,
				Object sheetObject)
		{
			if (sheetObject == null)
			{
				return;
			}
			String sheetName = (String) sheetNameElement.get(context, sheetObject);
			List<Object> records = (List<Object>) recordsElement.get(context, sheetObject);
			addSheet(context, wb, styles, sheetName, records);
		}

		private void addSheet(RuntimeContext context, Workbook wb,
				HashMap<String, CellStyle> styles, String sheetName, List<Object> records)
		{
			if (records == null)
				return;

			InstanceHandler recordHandler = null;
			ArrayList<Object> mapKeys = null;
			boolean sortOutputColumns = false;

			if (wb.getSheet(sheetName) != null)
			{
				wb.setSheetName(wb.getSheetIndex(sheetName), "template_" + sheetName);
			}

			Sheet sheet;
			if (template == null)
				sheet = wb.createSheet(sheetName);
			else
				sheet = template.createSheet(sheetName);
			newSheets.add(sheet);
			for (int i = 0; i < records.size(); i++)
			{
				Object record = records.get(i);
				if (record instanceof Map<?, ?> || record instanceof JSONObject)
				{
					sortOutputColumns = true; 
					if (mapKeys == null)
					{
						mapKeys = new ArrayList<Object>();
						if (record instanceof Map<?,?>)
							for (Object k:((Map<?,?>)record).keySet())
							{
								mapKeys.add(k);
							}
						else
						{
							Iterator<?> it = ((JSONObject)record).keys();
							while (it.hasNext())
							{
								mapKeys.add(it.next());
							}
						}
						
						addHeaderRow(sheet, helper, mapKeys, sortOutputColumns);
					}
					addDataRow(context, wb, sheet, mapKeys, record, styles);
				}
				else
				{
					ModelId id = ModelIdHelper.getCompositeModelId(record);
					if (id == null)
						throw new ModelExecutionException(
								"In 'Export To Excel', records must be composite but found "
										+ record.getClass().getName() + " (sheet=" + sheetName
										+ " row=" + i + 1 + ")");
					if (recordHandler == null || !id.equals(recordHandler.getModelId()))
					{
						// Looks like the behavior is strange if we have a mixed set of records
						recordHandler = context.getModelLoader().getHandler(id);
						if (recordHandler != rowHandler)
							addHeaderRow(context, sheet, recordHandler, helper); // When using explicit rows we
						// don't need to add a
						// header
					}
					addDataRow(context, wb, sheet, recordHandler, record, styles);
					
				}
			}

			if (levelColumn >= 0)
			{
				sheet.setRowSumsBelow(false);
				int firstRow = sheet.getFirstRowNum();
				int lastRow = sheet.getLastRowNum();
				for (int start = firstRow; start < lastRow; start++)
				{
					Row row = sheet.getRow(start);
					if (row == null)
						continue;
					Cell cell = row.getCell(levelColumn);
					if (cell == null || cell.getCellType() != Cell.CELL_TYPE_NUMERIC)
						continue;
					double startLevel = cell.getNumericCellValue();
					for (int current = start + 1; current <= lastRow; current++)
					{
						double currentLevel = startLevel;
						Row currentRow = sheet.getRow(current);
						// Empty rows or cells break the grouping
						if (currentRow != null)
						{
							Cell currentLevelCell = currentRow.getCell(levelColumn);
							if (currentLevelCell != null
									&& currentLevelCell.getCellType() == Cell.CELL_TYPE_NUMERIC)
								currentLevel = currentLevelCell.getNumericCellValue();
						}
						if (currentLevel <= startLevel || current == lastRow)
						{
							if (current > start + 1)
							{
								sheet.groupRow(start + 1, current - 1);
							}
							break;
						}
					}
				}

			}
			if (template != null)
			{
				template.applyStyles(sheet);
			}

		}

		@SuppressWarnings("unchecked")
		private void addDataRow(RuntimeContext context, Workbook wb, Sheet sheet,
				InstanceHandler recordHandler, Object record, HashMap<String, CellStyle> styles)
		{
			int rowNum = sheet.getLastRowNum() + 1;
			Row row = sheet.createRow(rowNum);
			if (recordHandler == rowHandler)
			{
				// Row created from a list of cells
				int i = 0;
				for (Object cellData : (List<Object>) cellElement.get(context, record))
				{
					Cell cell = row.createCell(i);
					Object value = valueElement.get(context, cellData);
					ExcelWriting.setCellContent(context, cell, value, rowNum, helper);
					String format = (String) formatElement.get(context, cellData);
					if (format == null && value instanceof Date)
						format = DATE_FORMAT;
					if (format != null)
					{
						CellStyle style = getStyle(wb, styles, format);
						cell.setCellStyle(style);
					}
					++i;
				}
			}
			else
			{
				// Row created from a data structure
				for (int i = 0; i < recordHandler.elementHandlers.length; i++)
				{
					ElementHandler e = recordHandler.elementHandlers[i];
					Cell cell = row.createCell(i);
					Object value = e.get(context, record);
					ExcelWriting.setCellContent(context, cell, value, e.getChildInstanceHandler(),
							rowNum, helper);
					if (e.getChildInstanceHandler() instanceof DateAndTimeHandler)
						cell.setCellStyle(getStyle(wb, styles, DATE_AND_TIME_FORMAT));
					else if (e.getChildInstanceHandler() instanceof DateHandler)
						cell.setCellStyle(getStyle(wb, styles, DATE_FORMAT));
				}
			}

		}

		private void addDataRow(RuntimeContext context, Workbook wb, Sheet sheet,
				List<Object> keys, Object record, HashMap<String, CellStyle> styles)
		{
			int rowNum = sheet.getLastRowNum() + 1;
			Row row = sheet.createRow(rowNum);

			// Row created from a Map or JSONObject
			int i = 0;
			for (Object k : keys)
			{
				Cell cell = row.createCell(i++);
				Object value = null;
				if (record instanceof Map)
					value = ((Map<?, ?>) record).get(k);
				else
				{
					try
					{
						String key = String.valueOf(k);
						JSONObject j = ((JSONObject) record);
						if (j.has(key) && !j.isNull(key))
							value = j.get(key);
					}
					catch (JSONException e)
					{
						throw new InternalErrorException("Unexpected error", e);
					}
				}
				ExcelWriting.setCellContent(context, cell, value, rowNum, helper);
				if (value instanceof Date)
				{
					if (k.toString().toLowerCase().contains("time"))
						cell.setCellStyle(getStyle(wb, styles, DATE_AND_TIME_FORMAT));
					else
						cell.setCellStyle(getStyle(wb, styles, DATE_FORMAT));
				}
			}

		}
		private CellStyle getStyle(Workbook wb, HashMap<String, CellStyle> styles,
				String format)
		{
			CellStyle style = styles.get(format);
			if (style == null)
			{
				style = createStyle(wb, styles, format);
			}
			return style;
		}

		private CellStyle createStyle(Workbook wb, HashMap<String, CellStyle> styles,
				String format)
		{
			CellStyle style;
			style = ExcelWriting.creatCellStyle(wb, format);
			styles.put(format, style);
			return style;
		}

		private void addHeaderRow(RuntimeContext context, Sheet sheet,
				InstanceHandler recordHandler,  CreationHelper helper)
		{
			ArrayList<Object> columns = new ArrayList<Object>();
			for (int i = 0; i < recordHandler.elementHandlers.length; i++)
			{
				ElementHandler e = recordHandler.elementHandlers[i];
				columns.add(e.getRole());

			}
			addHeaderRow(sheet, helper, columns, false);
		}

		protected void addHeaderRow(Sheet sheet, CreationHelper helper, ArrayList<Object> columns, boolean sortOutputColumns)
		{
			int last = sheet.getLastRowNum();
			int rowNum;
			if (last > 0)
			{
				// add an empty row as a separator
				sheet.createRow(last + 1);
				rowNum = last + 2;
			}
			else
				rowNum = 0;
			Row row = sheet.createRow(rowNum);
			if (sortOutputColumns)
				Collections.sort(columns,new Comparator<Object>()
				{
					
					public int compare(Object o1, Object o2)
					{
						return String.valueOf(o1).compareToIgnoreCase(String.valueOf(o2));
					}
				}); 
			int i = 0;
			for (Object c: columns)
			{
				Cell cell = row.createCell(i);
				++i;
				RichTextString text = helper.createRichTextString(String.valueOf(c));
				cell.setCellValue(text);
			}
		}
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		fileExit = getRequiredExit(FILE, Boolean.FALSE, BuiltinModels.BINARY_ID);
		recordsTrigger = getNonRequiredMandatoryTrigger(RECORDS, Boolean.TRUE, null);
		sheetNameTrigger = getNonRequiredTrigger(SHEET_NAME, Boolean.FALSE, BuiltinModels.TEXT_ID);
		sheetsTrigger = getNonRequiredMandatoryTrigger(SHEETS, null, null);
		fileFormatTrigger = getNonRequiredTrigger(FILE_FORMAT, Boolean.FALSE, BuiltinModels.TEXT_ID);
		levelColumnTrigger = getNonRequiredTrigger(LEVEL_COLUMN, Boolean.FALSE,
				BuiltinModels.NUMBER_ID);
		styleColumnTrigger = getNonRequiredTrigger(STYLE_COLUMN, Boolean.FALSE,
				BuiltinModels.NUMBER_ID);
		templateTrigger = getNonRequiredTrigger(TEMPLATE, Boolean.FALSE, BuiltinModels.BINARY_ID);
		if (recordsTrigger == null && sheetsTrigger == null)
			notifyInvalidModel(Path.EMPTY, Problems.MISSING_TRIGGER, "Either " + RECORDS + " or "
					+ SHEETS + " must be specified.");
		else if (recordsTrigger != null && sheetsTrigger != null)
			notifyInvalidModel(RECORDS, Problems.INVALID_TRIGGER, "Either " + RECORDS + " or "
					+ SHEETS + " must be specified (but not both).");

		if (sheetsTrigger != null)
		{
			sheetNameElement = sheetsTrigger.getChildInstanceHandler()
					.getElementHandler(SHEET_NAME);
			recordsElement = sheetsTrigger.getChildInstanceHandler().getElementHandler(RECORDS);
			if (sheetNameElement == null)
				notifyInvalidModel(SHEETS, Problems.INVALID_TRIGGER,
						"The input type doesn't contain a " + SHEET_NAME + " element");
			else if (!BuiltinModels.TEXT_ID.equals(sheetNameElement.getChildModelId()))
				notifyInvalidModel(SHEETS, Problems.INVALID_TRIGGER, "Element " + SHEET_NAME
						+ " of the input type should be of type Text");
			if (recordsElement == null)
				notifyInvalidModel(SHEETS, Problems.INVALID_TRIGGER,
						"The input type doesn't contain a " + RECORDS + " element");
			else if (!recordsElement.isRepetitive())
				notifyInvalidModel(SHEETS, Problems.INVALID_TRIGGER, "The element " + RECORDS
						+ " of the trigger type should be repetitive");
		}
		rowHandler = getLoader().getHandler(BuiltinModels.ROW_ID);
		InstanceHandler cellHandler = getLoader().getHandler(BuiltinModels.CELL_ID);
		cellElement = rowHandler.getElementHandler(CELL);
		valueElement = cellHandler.getElementHandler(VALUE);
		formatElement = cellHandler.getElementHandler(FORMAT);

	}
	
	static ConditionalFormattingRule[] getRules(ConditionalFormatting cf)
	{
		ConditionalFormattingRule[] rules =  new ConditionalFormattingRule[cf.getNumberOfRules()];
		for (int j=0;j<rules.length;j++)
		{
			rules[j]=cf.getRule(j);
		}
		return rules;
	}
	class CFTemplate
	{
		public CFTemplate(ConditionalFormattingRule[] rules,
				ArrayList<CellRangeAddress> ranges)
		{
			this.rules =  rules;
			this.ranges = ranges.toArray(new CellRangeAddress[ranges.size()]);
		}
		CellRangeAddress[] ranges;
		ConditionalFormattingRule[] rules;
	}
}

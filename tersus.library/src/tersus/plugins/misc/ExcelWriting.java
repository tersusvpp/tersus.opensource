/************************************************************************************************
 * Copyright (c) 2003-2022 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 *************************************************************************************************/
package tersus.plugins.misc;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Workbook;

import tersus.runtime.BinaryValue;
import tersus.runtime.BooleanHandler;
import tersus.runtime.DateAndTimeHandler;
import tersus.runtime.DateHandler;
import tersus.runtime.EngineException;
import tersus.runtime.InstanceHandler;
import tersus.runtime.LeafDataHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Number;
import tersus.runtime.NumberHandler;
import tersus.runtime.RuntimeContext;
import tersus.runtime.TextHandler;

/**
 * @author Youval Bronicki
 * 
 */
public class ExcelWriting
{
    public static CellStyle creatCellStyle(Workbook wb, String format)
    {
        CreationHelper helper = wb.getCreationHelper();
        CellStyle cellStyle = wb.createCellStyle();
        
        final short builtinFormat = wb.createDataFormat().getFormat(format);
        if (builtinFormat == -1)
        {
            try
            {
                cellStyle.setDataFormat(helper.createDataFormat().getFormat(format));
            }
            catch (Exception e)
            {
                throw new ModelExecutionException("Failed to create cell style", "format string:" + format, e);
            }
        }
        else
            cellStyle.setDataFormat(builtinFormat);
        return cellStyle;
    }

    public static void setCellContent(RuntimeContext context, Cell cell, Object value, InstanceHandler handler,
            int rowNum, CreationHelper helper)
    {
    	if (value == null)
        {
            cell.setCellType(Cell.CELL_TYPE_BLANK);
            return;
        }
        if (handler instanceof LeafDataHandler)
        {
            if (handler instanceof DateHandler)
            {
                Date date = (Date) value;
                cell.setCellValue(date);
            }
            else if (handler instanceof DateAndTimeHandler)
            {
                Date date = (Date) value;
                cell.setCellValue(date);
            }
            else if (handler instanceof NumberHandler)
            {
                cell.setCellValue(((Number) value).value);
            }
            else if (handler instanceof TextHandler)
            {
                cell.setCellValue(helper.createRichTextString((String) value));
            }
            else if (handler instanceof BooleanHandler)
            {
            	cell.setCellValue(helper.createRichTextString(((Boolean)value).booleanValue() ? "Yes": "No"));
            }
            else
                throw new EngineException("Input Row " + rowNum + ": item " + (cell.getColumnIndex()+ 1)
                        + " is not a boolean, date, number or text", null, null);
        }
        else
            throw new EngineException("Input Row " + rowNum + ": item " + (cell.getColumnIndex() + 1) + " is not a leaf",
                    null, null);
    }

    public static void setCellContent(RuntimeContext context, Cell cell, Object value, int rowNum, CreationHelper helper)
    {
        if (value == null)
        {
            cell.setCellType(Cell.CELL_TYPE_BLANK);
        }
        else if (value instanceof Date)
        {
            cell.setCellValue((Date) value);
        }
        else if (value instanceof Number)
        {
            cell.setCellValue(((Number) value).value);
        }
        else if (value instanceof java.lang.Number)
        {
            cell.setCellValue(((java.lang.Number) value).doubleValue());
        }
        else if (value instanceof String)
        {
            cell.setCellValue(helper.createRichTextString((String) value));
        }
        else if (value instanceof Boolean)
        {
        	cell.setCellValue(helper.createRichTextString(((Boolean)value).booleanValue() ? "Yes": "No"));
        }
        else
            throw new EngineException("Input Row " + rowNum + ": item " + (cell.getColumnIndex() + 1) + " ["+value.getClass().getName()+"] "
                    + " is not a boolean, date, number or text", null, null);
    }

    public static BinaryValue toBinaryValue(RuntimeContext context, Workbook wb)
    {
        BinaryValue updatedData = null;

        try
        {
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            wb.write(output);
            updatedData = new BinaryValue(output.toByteArray());
            output.close();
        }
        catch (IOException e2)
        {
            throw new EngineException("Failed to output the updated Excel file", null, e2);
        }
        return updatedData;
    }

}

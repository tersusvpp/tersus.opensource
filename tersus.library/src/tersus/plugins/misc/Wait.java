/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.misc;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * @author Youval Bronicki
 *
 */
public class Wait extends Plugin
{

    private static final Role MILLISECONDS = Role.get("<Milliseconds>");
    SlotHandler millisecondsTrigger;
    /* (non-Javadoc)
     * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
        if (millisecondsTrigger != null)
        {
            Number _time = (Number)millisecondsTrigger.get(context, flow);
            long time = Math.round(_time.value);
            try
            {
                Thread.sleep(time);
            }
            catch (InterruptedException e)
            {
            }
        }
    }

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        millisecondsTrigger = getNonRequiredTrigger(MILLISECONDS, Boolean.FALSE, BuiltinModels.NUMBER_ID);
    }
}

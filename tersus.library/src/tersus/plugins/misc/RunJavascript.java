package tersus.plugins.misc;

import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.NativeArray;
import org.mozilla.javascript.NativeJavaObject;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;

import tersus.ProjectStructure;
import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.sapi.SAPIManager;
import tersus.util.FileUtils;
import tersus.webapp.ContextPool;

public class RunJavascript extends Plugin
{

	static double cumulativeTime;
	public static final Role SCRIPT_TEXT = Role.get("<Script Text>");
	private SlotHandler scriptTrigger;

	@Override
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		scriptTrigger = getRequiredMandatoryTrigger(SCRIPT_TEXT, Boolean.FALSE,
				BuiltinModels.TEXT_ID);
	}

	@Override
	public void start(RuntimeContext context, FlowInstance flow)
	{

		Context cx = Context.enter();
		try
		{
			cx.setOptimizationLevel(-1);
			ContextPool pool = (tersus.webapp.ContextPool) (context.getContextPool());
			File scriptFolder = new File(pool.getProjectRoot(), ProjectStructure.SERVER_SCRIPTS);
			
			ScriptableObject contextScope = (ScriptableObject) context
					.getPluginHelper("RunJavascript.Scope");
			if (contextScope != null)
			{
				if (pool.isDebug())
				{
					//In debug mode - refresh scripts if needed - check every 10 seconds
					//TODO find a better mechanism for refreshing the scripts (add a mechanism that lets plugin helpers check their own validity and call once per request)
					Date previousTimestamp = (Date) context.getPluginHelper("RunJavascript.Timestamp");
					if (previousTimestamp == null || previousTimestamp != null
							&& System.currentTimeMillis() - previousTimestamp.getTime() > 10000)
					{
						if (previousTimestamp == null || calculateLastModified(scriptFolder) > previousTimestamp.getTime())
						{
							contextScope = null;
						}
					}

				}
			}
			if (contextScope == null)
			{
				contextScope = cx.initStandardObjects();
				ScriptableObject.putProperty(contextScope, "_env", new ScriptEnv(context));
				loadServerScripts(scriptFolder, cx, contextScope);
				contextScope.sealObject();
				context.putPluginHelper("RunJavascript.Scope", contextScope);
				context.putPluginHelper("RunJavascript.Timestmap", new Date());
			}
			Scriptable scope = cx.newObject(contextScope);
			scope.setPrototype(contextScope);
			scope.setParentScope(null);
//			Scriptable scope=contextScope;
			SAPIManager sapiManager = context.getSapiManager();
			String script = getTriggerText(context, flow, scriptTrigger);
			for (SlotHandler trigger : getTriggers())
			{
				if (!trigger.getRole().isDistinguished()) // ignore 'special' triggers
				{
					if (trigger.isRepetitive())
					{
						List<?> values = (List<?>) trigger.get(context, flow);
						NativeArray array = new NativeArray(values.size());
						for (int i = 0; i < values.size(); i++)
						{
							ScriptableObject.putProperty(array, i, sapiManager.fromInstance(
									trigger.getChildInstanceHandler(), values.get(i)));
						}
						ScriptableObject.putProperty(scope, trigger.getRole().toString(), array);
					}
					else
					{
						ScriptableObject.putProperty(
								scope,
								trigger.getRole().toString(),
								sapiManager.fromInstance(trigger.getChildInstanceHandler(),
										trigger.get(context, flow)));
					}
				}
			}
			cx.evaluateString(scope, script, SCRIPT_TEXT.toString(), 1, null);
			for (SlotHandler exit : getExits())
			{
				if (!exit.getRole().isDistinguished())
				{
					Object value = ScriptableObject.getProperty(scope, exit.getRole().toString());
					if (value != ScriptableObject.NOT_FOUND)
					{
						if (value instanceof NativeJavaObject)
							value = ((NativeJavaObject) value).unwrap();
						if (exit.isRepetitive())
						{
							if (!(value instanceof NativeArray))
								throw new ModelExecutionException(
										"Unexpected script output - repetitive exit '"
												+ exit.getRole()
												+ "' expected to receive Javascript array but found "
												+ value.getClass().getName());
							NativeArray array = (NativeArray) value;
							for (int i = 0; i < array.getLength(); i++)
							{
								Object v = ScriptableObject.getProperty(array, i);
								if (v != null)
								{
									Object instance = SAPIManager.toInstance(
											exit.getChildInstanceHandler(), v);
									accumulateExitValue(exit, context, flow, instance);

								}
							}
						}
						else
						{
							Object instance = SAPIManager.toInstance(
									exit.getChildInstanceHandler(), value);
							setExitValue(exit, context, flow, instance);
						}
					}

				}
			}

		}
		finally
		{
			Context.exit();
		}
	}

	protected void loadServerScripts(File scriptFolder, Context cx, ScriptableObject contextScope)
	{
		if (scriptFolder.isDirectory())
		{
			ArrayList<String> names = new ArrayList<String>();
			for (String name : scriptFolder.list(new FilenameFilter()
			{
				public boolean accept(File dir, String name)
				{
					return name.endsWith(".js");
				}
			}))
			{
				names.add(name);
			}
			Collections.sort(names);
			for (String name : names)
			{
				loadFile(cx, contextScope, new File(scriptFolder, name));
			}

		}
	}

	protected void loadFile(Context cx, ScriptableObject contextScope, File initFile)
	{
		if (initFile.isFile())
		{
			FileReader in = null;
			try
			{
				in = new FileReader(initFile);
				cx.evaluateReader(contextScope, in, initFile.getName(), 1, null);
				in.close();
				in = null;
			}
			catch (Exception e)
			{
				throw new ModelExecutionException("Failed to initialize Javascript scope",
						"Error loading " + initFile.getAbsolutePath(), e);
			}
			finally
			{
				FileUtils.forceClose(in);
			}
		}
	}

	long calculateLastModified(File dir)
	{
		long timestamp = 0;
		timestamp = Math.max(timestamp, dir.lastModified());
		for (File file : dir.listFiles())
			timestamp = Math.max(timestamp, file.lastModified());
		return timestamp;
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2007 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.misc;

import java.util.Date;

public class StringTableData implements ITableData
{

    private String[][] table;

    public StringTableData(String[][] data)
    {
        this.table = data;
    }

    public Date getCellAsDate(int row, int col)
    {
        return null;
    }

    public Number getCellAsNumber(int row, int col)
    {
        return null;
    }

    public String getCellAsString(int row, int col)
    {
        return table[row][col];
    }

    public int getFirstCellNum()
    {
        return 0;
    }

    public int getLastCellNum(int row)
    {
        return table[row].length - 1;
    }

    public boolean isDate(int row, int col)
    {
        return false;
    }

    public boolean isNumber(int row, int col)
    {
        return false;
    }

    public int getLastRowNum()
    {
        return table.length - 1;
    }

    public int getFirstCellNum(int row)
    {
        return 0;
    }

    public int getFirstRowNum()
    {
        return 0;
    }

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.collections;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that returns (in increasing order) all the integers between two numbers.
 * 
 * @author Ofer Brandes
 *
 */
public class Range extends Plugin
{
	private static final Role LIMIT1_ROLE = Role.get("<Limit 1>");
	private static final Role LIMIT2_ROLE = Role.get("<Limit 2>");
	private static final Role OUTPUT_ROLE = Role.get("<Numbers>");

	SlotHandler input1, input2;
	SlotHandler exit;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Number n1 = (Number) input1.get(context, flow);
		Number n2 = (Number) input2.get(context, flow);

		double minimum = Math.ceil(Math.min(n1.value,n2.value));
		double maximum = Math.max(n1.value,n2.value);		

		for ( double n = minimum ; n <= maximum ; n++ ) {
			Number exitValue = new Number(n);
			accumulateExitValue(exit, context, flow, exitValue);
		}
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		
		input1 = getRequiredMandatoryTrigger(LIMIT1_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		input2 = getRequiredMandatoryTrigger(LIMIT2_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);

		exit = getRequiredExit(OUTPUT_ROLE, Boolean.TRUE, BuiltinModels.NUMBER_ID);
	}
}

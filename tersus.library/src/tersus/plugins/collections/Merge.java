/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import tersus.model.IRepository;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.Path;
import tersus.model.Role;
import tersus.runtime.CompositeDataHandler;
import tersus.runtime.DataHandler;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * Merge two lists of objects
 * 
 * @author Ofer Brandes
 *
 */
public class Merge extends Plugin
{
	private static final Role INPUT1_ROLE = Role.get("<List 1>");
	private static final Role INPUT2_ROLE = Role.get("<List 2>");
	private static final Role OUTPUT_ROLE = Role.get("<Merged List>");
	SlotHandler trigger1, trigger2, exit;
	int[] commonElementIndices1 = null;
	int[] commonElementIndices2 = null;
	DataHandler[] commonChildHandlers = null;
	int commonSize = 0;
	CompositeDataHandler handler1, handler2, outputHandler;
	RuntimeContext runtimeContext = null;

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowState)
	 */
	public void start(final RuntimeContext context, FlowInstance flow)
	{
		runtimeContext = context; // used by the comparison function
		
		// Sort each input list
		List<?> list1 = (List<?>) trigger1.get(context,flow);
		Collections.sort(list1, new CompareMatchingElements(commonElementIndices1,commonElementIndices1));

		List<?> list2 = (List<?>) trigger2.get(context,flow);
		Collections.sort(list2, new CompareMatchingElements(commonElementIndices2,commonElementIndices2));

		// Merge the two lists
		List<Object> outputs = new ArrayList<Object>();
		
		Comparator<Object> comperator = new CompareMatchingElements(commonElementIndices1,commonElementIndices2);

		int index1 = 0;
		int index2 = 0;
		while ((index1 < list1.size()) && (index2 < list2.size()))
		{
			Object o1 = list1.get(index1);
			Object o2 = list2.get(index2);
			int order = comperator.compare(o1,o2);
			if (order < 0)  // o1 < o2
			{
				outputs.add(mergeDataStructures(context,o1,null));
				index1++;
			}
			else if (order == 0)  // o1 == o2
			{
				outputs.add(mergeDataStructures(context,o1,o2));
				index1++;
				index2++;
			}
			else  // O2 < o1
			{
				outputs.add(mergeDataStructures(context,null,o2));
				index2++;
			}
		}
		while (index1 < list1.size())
			outputs.add(mergeDataStructures(context,list1.get(index1++),null));
		while (index2 < list2.size())
			outputs.add(mergeDataStructures(context,null,list2.get(index2++)));
		
		for (int i=0; i<outputs.size(); i++)
		    accumulateExitValue(exit, context, flow, outputs.get(i));
	}


	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		trigger1 = getNonRequiredTrigger(INPUT1_ROLE, Boolean.TRUE, null);
		trigger2 = getNonRequiredTrigger(INPUT2_ROLE, Boolean.TRUE, null);

		handler1 = (CompositeDataHandler) trigger1.getChildInstanceHandler();
		handler2 = (CompositeDataHandler) trigger2.getChildInstanceHandler();

		IRepository repository = model.getRepository();
		ModelId modelId1 = trigger1.getChildModelId();
		ModelId modelId2 = trigger2.getChildModelId();
		if ((modelId1 != null) && (modelId2 != null)) { // Can be null only during validations, not at runtime
			Model model1 = repository.getModel(modelId1, true);
			Model model2 = repository.getModel(modelId2, true);
			findCommonElementRoles(model1, model2);
		}
		
		exit = getRequiredExit(OUTPUT_ROLE, Boolean.TRUE, null);
		outputHandler = (CompositeDataHandler) exit.getChildInstanceHandler();
	}

	private void findCommonElementRoles (Model model1, Model model2)
	{
		commonElementIndices1 = new int[model1.getElements().size()];
		commonElementIndices2 = new int[model2.getElements().size()];
		commonChildHandlers = new DataHandler[model1.getElements().size()];
		
		for ( int index1 = 0; index1 < model1.getElements().size(); index1++) {
			ModelElement e1 = (ModelElement) model1.getElements().get(index1);
			ModelElement e2 = model2.getElement(e1.getRole());
			if (e2 != null) {
				if (!e1.getRefId().equals(e2.getRefId()))
					notifyInvalidModel(Path.EMPTY, "Inconsistent Data Types",
							"'"+INPUT1_ROLE+"/"+e1.getRole()+"' and '"+INPUT2_ROLE+"/"+e2.getRole()+"' have different data types");
				commonElementIndices1[commonSize]= index1;
				commonElementIndices2[commonSize]= model2.indexOf(e2);
				ElementHandler handler = handler1.getElementHandler(index1);
				if (handler != null) // Can be null only during validations, not at runtime
					commonChildHandlers[commonSize++]= (DataHandler) handler.getChildInstanceHandler();
			}
		}
	}
	
	private class CompareMatchingElements implements Comparator<Object>
	{
		private int[] elementIndices1;
		private int[] elementIndices2;
		
		private CompareMatchingElements (int[] elementIndices1, int[] elementIndices2)
		{
			this.elementIndices1 = elementIndices1;
			this.elementIndices2 = elementIndices2;
		}

		public int compare (Object o1, Object o2)
		{
			for (int i=0; i < commonSize; i++) {
				Object child1 = ((Object []) o1)[1+elementIndices1[i]];
				Object child2 = ((Object []) o2)[1+elementIndices2[i]];
				DataHandler childHandler = commonChildHandlers[i];
				int order = childHandler.compare(runtimeContext, child1, child2);
				if (order != 0)
					return order;
			}
	
			return (0); // Equal
		}
	}
	
	private Object mergeDataStructures (RuntimeContext context, Object o1, Object o2)
	{
		Object merged = outputHandler.newInstance(context);

		if (o1 != null)
			enrichDataStructure(context, o1, handler1, merged, outputHandler);

		if (o2 != null)
			enrichDataStructure(context, o2, handler2, merged, outputHandler);
		
		return merged;
	}

	private void enrichDataStructure (RuntimeContext context,	Object source, CompositeDataHandler sourceHandler,
																Object target, CompositeDataHandler targetHandler)
	{
		for (int i = 0; i < sourceHandler.getNumberOfElements(); i++) {
			ElementHandler sourceElementHandler = sourceHandler.getElementHandler(i);
			Role role = sourceElementHandler.getRole();
			ElementHandler targetElementHandler = targetHandler.getElementHandler(role);
			if (targetElementHandler != null) {  // Same role exists at target
				Object current = targetElementHandler.get(context,target);
				if (current == null || current == Collections.EMPTY_LIST) { // But is still empty
					Object value = sourceElementHandler.get(context, source);
					targetElementHandler.set(context, target, value);
				}
			}
		}
	}
}

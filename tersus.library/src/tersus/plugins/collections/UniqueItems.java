/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.collections;

import java.util.ArrayList;
import java.util.List;

import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.DataHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that returns the set of unique items in its repetitive trigger
 * 
 * @author Youval Bronicki
 *
 */
public class UniqueItems extends Plugin
{
	private static final Role OUTPUT_ROLE = Role.get("<Unique Items>");
	SlotHandler exit;

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		List outputs = new ArrayList();
		TriggerIterator iterator = new TriggerIterator(context,flow,null);
		while (iterator.hasNext()) {
			Object current = iterator.next();
			boolean alreadyListed = false;
			for (int i = 0; i < outputs.size(); i++) {
				if (DataHandler.instanceDeepCompare(outputs.get(i),current))
				{
					alreadyListed = true;
					break;
				}
			}
			if (!alreadyListed) {
				accumulateExitValue(exit, context, flow, current);
				outputs.add(current);
			}
		}
	}


	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		exit = getRequiredExit(OUTPUT_ROLE, Boolean.TRUE, null);
	}
}

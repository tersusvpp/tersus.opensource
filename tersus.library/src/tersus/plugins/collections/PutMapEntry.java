package tersus.plugins.collections;

import java.util.Map;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

public class PutMapEntry extends Plugin
{

    private static final Role KEY = Role.get("<Key>");
    private static final Role VALUE = Role.get("<Value>");
    private static final Role MAP = Role.get("<Map>");
    
    private SlotHandler keyTrigger, mapTrigger, valueTrigger;
    @Override
    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        keyTrigger = getRequiredMandatoryTrigger(KEY, Boolean.FALSE, BuiltinModels.TEXT_ID);
        valueTrigger = getRequiredTrigger(VALUE, Boolean.FALSE, null);
        mapTrigger = getRequiredTrigger(MAP, Boolean.FALSE, BuiltinModels.MAP_ID);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void start(RuntimeContext context, FlowInstance flow)
    {
        Object map = mapTrigger.get(context,flow);
        if (! (map instanceof Map<?, ?>))
        {
            throw new ModelExecutionException("<Map> trigger expects a Map");
        }
        String key = (String)keyTrigger.get(context, flow);
        Object value = valueTrigger.get(context, flow);
        if (value == null)
            ((Map<String, Object>)map).remove(key);
        else
        	((Map<String, Object>)map).put(key, value);
     }

}

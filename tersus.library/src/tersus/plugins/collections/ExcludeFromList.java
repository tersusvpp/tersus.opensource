/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.collections;

import java.util.ArrayList;
import java.util.List;

import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.DataHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that removes from a list of objects
 * any occurence of a specific object (or objects from another list)
 * (when checking whether two objects are equal, deep comparison is used
 * for composite data types,with some tolerance for numbers).
 * 
 * @author Ofer Brandes
 *
 */

public class ExcludeFromList extends Plugin
{
	private static final Role LIST_ROLE = Role.get("<List>");
	private static final Role EXCLUDE_ROLE = Role.get("<Exclude>");
	private static final Role OUTPUT_ROLE = Role.get("<Remaining>");

	SlotHandler excludeTrigger, listTrigger;
	SlotHandler exit;

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		List list = (List) listTrigger.get(context,flow);

		Object excluded = excludeTrigger.get(context,flow);
		
		List left = null;
		if (excluded != null) {
			left = new ArrayList();
			if (excludeTrigger.isRepetitive()) {
				List excludeList = (List) excluded;
				for (int i = 0; i < list.size(); i++) {
					Object current = list.get(i);
					boolean appears = false;
					for (int j =0; j < excludeList.size(); j++) {
						excluded = excludeList.get(j);
						if (DataHandler.instanceDeepCompare(excluded,current)) {
							appears = true;
							break;
						}
					}
					if (!appears)
						left.add(current);
				}
			}
			else {
				for (int i = 0; i < list.size(); i++) {
					Object current = list.get(i);
					if (!DataHandler.instanceDeepCompare(excluded,current))
						left.add(current);
				}
			}
		}
		else
			left = new ArrayList(list);

		setExitValue (exit, context, flow, left);
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		listTrigger = getRequiredMandatoryTrigger(LIST_ROLE, Boolean.TRUE, null);
		excludeTrigger = getNonRequiredTrigger(EXCLUDE_ROLE, null, null);

		exit = getRequiredExit(OUTPUT_ROLE, Boolean.TRUE, null);
		
		checkTypeMismatch(listTrigger, excludeTrigger);
		checkTypeMismatch(listTrigger, exit);
		checkTypeMismatch(excludeTrigger, exit);  // This check is needed in case listTrigger is Anything
	}
}

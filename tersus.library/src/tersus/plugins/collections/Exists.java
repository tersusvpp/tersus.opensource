/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.collections;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that checks if it has received a value through any of its triggers
 * 
 * @author Youval Bronicki
 *
 */
public class Exists extends Plugin
{
	private static final Role YES_ROLE = Role.get("<Yes>");
	private static final Role NO_ROLE = Role.get("<No>");
	
	SlotHandler yesExit, noExit;

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		TriggerIterator iterator = new TriggerIterator(context,flow,null);
		if (iterator.hasNext())
		{
			if (yesExit != null)
				setExitValue (yesExit, context, flow, iterator.next());
		}
		else
		{
			if (noExit != null)
				chargeEmptyExit (context, flow, noExit);
		}
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		yesExit = getNonRequiredExit(YES_ROLE, Boolean.FALSE, null);
		noExit = getNonRequiredExit(NO_ROLE, Boolean.FALSE, BuiltinModels.NOTHING_ID);
	}
}

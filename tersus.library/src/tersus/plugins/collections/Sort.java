/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Path;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.PathHandler;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * Sorts all items in a list (in ascending order)
 * 
 * @author Youval Bronicki
 *
 */
public class Sort extends Plugin
{
	private static final Role OUTPUT_ROLE = Role.get("<Sorted Items>");
	private static final Role ORDER_BY_ROLE = Role.get("<Order By>");
	SlotHandler orderByTrigger, exit;

	public void start(final RuntimeContext context, FlowInstance flow)
	{
		List<Object> outputs = new ArrayList<Object>();
		String orderBy = getTriggerText(context, flow, orderByTrigger); 
		TriggerIterator iterator = new TriggerIterator(context,flow,ORDER_BY_ROLE);
		while (iterator.hasNext()) {
			Object current = iterator.next();
				outputs.add(current);
			}
		Comparator<Object> comparator = null;
		if (orderBy == null || orderBy.equalsIgnoreCase("asc") || orderBy.equalsIgnoreCase("ascending"))
		{
			comparator = new Comparator<Object>(){

			    public int compare(Object o1, Object o2)
			    {
			        return exit.getChildInstanceHandler().compare(context, o1, o2);
			    }};
		}
		else if (orderBy.equalsIgnoreCase("desc") || orderBy.equalsIgnoreCase("descending"))
		{
			comparator = new Comparator<Object>(){

			    public int compare(Object o1, Object o2)
			    {
			        return exit.getChildInstanceHandler().compare(context, o2, o1);
			    }};
		}
		else
		{
			
			String[] clauses = orderBy.split("\\s*,\\s*");
			
			final SortComponent[] sortComponents = new SortComponent[clauses.length];
			for (int i=0; i< clauses.length; i++)
			{
				String clause = clauses[i];
				String path = clause;
				boolean desc  = false;
				String[] parts = clause.split(" ");
				if (parts.length>1)
				{
					String suffix = parts[parts.length-1];
					if (suffix.equalsIgnoreCase("asc") || suffix.equalsIgnoreCase("ascending"))
					{
						path = clause.substring(0, clause.length()-suffix.length()).trim();
					}
					else if (suffix.equalsIgnoreCase("desc") || suffix.equalsIgnoreCase("descending"))
					{
						path = clause.substring(0, clause.length()-suffix.length()).trim();
						desc = true;
					}
				}
				sortComponents[i] = new SortComponent(path, desc);
			}
			comparator = new Comparator<Object>(){

				public int compare(Object o1, Object o2)
				{
					for (SortComponent c: sortComponents)
					{
						int cc = c.compare(context, o1,o2);
						if (cc !=0)
							return cc;
					}
					return 0;
				}};

		}
		Collections.sort(outputs, comparator);
		for (int i=0; i<outputs.size(); i++)
		    accumulateExitValue(exit, context, flow, outputs.get(i));
	}


	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		exit = getRequiredExit(OUTPUT_ROLE, Boolean.TRUE, null);
		orderByTrigger = getNonRequiredTrigger(ORDER_BY_ROLE, Boolean.FALSE, BuiltinModels.TEXT_ID);
	}


	class SortComponent
	{
		Path path;
		boolean desc;
		
		public SortComponent(String path, boolean desc)
		{
			this.path = new Path(path);
			this.desc = desc;
		}

		public int compare(RuntimeContext context, Object o1, Object o2)
		{
			if (desc)
			{
				Object t = o1;
				o1 = o2;
				o2 = t;
			}
			if (o1 == null && o2 == null)
				return 0;
			if (o1 == null)
				return -1;
			if (o2 == null)
				return 1;
			ModelId id1 = InstanceHandler.getModelIdEx(o1);
			ModelId id2 = InstanceHandler.getModelIdEx(o2);
			if (!id1.equals(id2))
			{
				throw new ModelExecutionException("Can't compare elements of different types (found '"+id1+ "' and '"+id2+"')");
			}
			InstanceHandler handler = context.getModelLoader().getHandler(id1);
			PathHandler pathHandler  = new PathHandler(handler, path);
			if (pathHandler.isRepetitive())
				throw new ModelExecutionException("Can't compare repetitive elements (path='"+path+"')");
			//TODO we actually CAN define a logic for repetitive elements, but we'll do it later
			Object v1 = pathHandler.get(context, o1);
			Object v2 = pathHandler.get(context, o2);
			if (v1 == null && v2 == null)
				return 0;
			if (v1 == null)
				return -1;
			if (v2 == null)
				return 1;
			
			InstanceHandler vh = pathHandler.getValueHandler();
			return vh.compare(context, v1, v2); 
		}
	}

}

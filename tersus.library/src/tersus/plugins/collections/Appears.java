/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.collections;

import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.DataHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that checks whether a specific object is one of its other inputs
 * (deep comparison for composite data types, with some tolerance for numbers).
 * 
 * @see instanceDeepCompare
 * 
 * In any case (appearing or missing), the searched object is returned
 * 
 * @author Ofer Brandes
 *
 */
public class Appears extends Plugin
{
	private static final Role ITEM_ROLE = Role.get("<Item>");
	private static final Role APPEARS_ROLE = Role.get("<Yes>");
	private static final Role MISSING_ROLE = Role.get("<No>");

	SlotHandler item;
	SlotHandler appears, missing;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Object searched = item.get(context, flow);

		TriggerIterator iterator = new TriggerIterator(context,flow,ITEM_ROLE);
		SlotHandler exit = missing;
		if (searched != null)
			while (iterator.hasNext()) {
				Object current = iterator.next();
				if (DataHandler.instanceDeepCompare(searched,current))
				{
					exit = appears;
					break;
				}
			}

		if (exit != null)
			setExitValue (exit, context, flow, searched);
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		item = getRequiredMandatoryTrigger(ITEM_ROLE, Boolean.FALSE, null);

		appears = getNonRequiredExit(APPEARS_ROLE, Boolean.FALSE, null);
		missing = getNonRequiredExit(MISSING_ROLE, Boolean.FALSE, null);
		
		checkTypeMismatch(item, appears);
		checkTypeMismatch(item, missing);
		checkTypeMismatch(appears, missing);  // This check is needed in case item is Anything
	}
}

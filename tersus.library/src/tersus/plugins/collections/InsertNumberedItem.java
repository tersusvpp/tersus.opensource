/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.collections;

import java.util.List;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Role;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.ModelIdHelper;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.Misc;

/**
 * A plugin that inserts an item into a list
 * 
 * @author Youval Bronicki
 * 
 */
public class InsertNumberedItem extends Plugin
{
	private static final Role PARENT = Role.get("<Parent>");
	private static final Role INDEX = Role.get("<Index>"); // Counting from 1
	private static final Role ITEM = Role.get("<Item>");
	private static final Role INSERTED = Role.get("<Inserted>");
	private static final Role INVALID_INDEX = Role.get("<Invalid Index>");
	private static final Role INVALID_ITEM = Role.get("<Invalid Item>");
	private static final Role INVALID_PARENT = Role.get("<Invalid Parent>");

	SlotHandler parentTrigger, indexTrigger, itemTrigger, invalidIndexExit, invalidItemExit, invalidParentExit,	insertedExit;

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext,
	 * tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Object parent = parentTrigger.get(context, flow);
		Object item = itemTrigger.get(context, flow);

		ElementHandler[] elements = getElements(parent);
		if (elements == null || elements.length==0)
		{
			if (invalidParentExit != null)
			{
				setExitValue(invalidParentExit, context, flow, parent);
				return;
			}
			else
			{
				throw new ModelExecutionException("Invalid parent", "There are no elements in the input parent.");
			}
		}

		ElementHandler element = getElement(elements, item);
		if (element == null)
		{
			if (invalidItemExit != null)
			{
				setExitValue(invalidItemExit, context, flow, item);
				return;
			}
			else
			{
				Object itemDescription = ModelIdHelper.getCompositeModelId(item);
				if (itemDescription == null)
					itemDescription = Misc.getShortName(item.getClass());
				throw new ModelExecutionException("Invalid item", "There is no element with type "
						+ itemDescription + " in the input parent.");
			}
		}

		int currentIndex = ((List) element.get(context, parent)).size();
		int elementIndex = (int) ((Number) indexTrigger.get(context, flow)).value;
		
		int indexDelta = 0;
		if (getPluginVersion() >= 1)
		{
			indexDelta = -1;
			elementIndex += indexDelta;
		}
		
		if (elementIndex < 0 || currentIndex < elementIndex)
		{
			if (invalidIndexExit != null)
				setExitValue(invalidIndexExit, context, flow, new Number(elementIndex-indexDelta));
			else
				throw new ArrayIndexOutOfBoundsException("Invalid index " + (elementIndex-indexDelta)
						+ " for element " + element.getRole());
			return;
		}

		element.set(context, parent, item, elementIndex);
		if (insertedExit != null)
			setExitValue(insertedExit, context, flow, item);

	}

	private ElementHandler[] getElements(Object parent)
	{
		ElementHandler[] elements = null;

		if (parent instanceof FlowInstance)
			elements = (((FlowInstance) parent).getFlowHandler()).elementHandlers;
		else
		{
			InstanceHandler parentHandler = getLoader().getHandler(ModelIdHelper.getCompositeModelId(parent));
			elements = parentHandler.elementHandlers;
		}

		return elements;
	}

	private ElementHandler getElement(ElementHandler[] elements, Object item)
	{
		ModelId itemModelId = ModelIdHelper.getCompositeModelId(item);
		if (itemModelId == null)
			itemModelId = itemTrigger.getChildModelId();

		for (int i = 0; i < elements.length; i++)
		{
			ElementHandler element = elements[i];
			if (element.isRepetitive() && element.getChildModelId().equals(itemModelId))
				return element;
		}
		return null;
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		parentTrigger = getRequiredMandatoryTrigger(PARENT, Boolean.FALSE, null);
		itemTrigger = getRequiredMandatoryTrigger(ITEM, Boolean.FALSE, null);
		indexTrigger = getRequiredMandatoryTrigger(INDEX, Boolean.FALSE, BuiltinModels.NUMBER_ID);

		invalidIndexExit = getNonRequiredExit(INVALID_INDEX, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		invalidItemExit = getNonRequiredExit(INVALID_ITEM, Boolean.FALSE, null);
		invalidParentExit = getNonRequiredExit(INVALID_PARENT, Boolean.FALSE, null);
		insertedExit = getNonRequiredExit(INSERTED, Boolean.FALSE, null);
		checkTypeMismatch(itemTrigger, invalidItemExit);
		checkTypeMismatch(itemTrigger, insertedExit);
	}
}

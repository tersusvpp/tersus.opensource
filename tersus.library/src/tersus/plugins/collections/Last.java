/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.collections;

import java.util.List;

import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that returns the last of its repetitive inputs
 * 
 * @author Youval Bronicki
 *
 */
public class Last extends Plugin
{
	private static final Role LIST_ROLE = Role.get("<List>");
	private static final Role LAST_ROLE = Role.get("<Last>");

	SlotHandler trigger, exit;

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		List inputs = (List) trigger.get(context, flow);
		if (inputs.size() > 0)
		{
			Object out = inputs.get(inputs.size() - 1);
			setExitValue (exit, context, flow, out);
		}
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		trigger = getRequiredMandatoryTrigger(LIST_ROLE, Boolean.TRUE, null);
		exit = getRequiredExit(LAST_ROLE, Boolean.FALSE, null);
		checkTypeMismatch(trigger, exit);
	}
}

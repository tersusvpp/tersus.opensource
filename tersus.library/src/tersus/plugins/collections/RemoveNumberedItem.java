/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.collections;

import java.util.List;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.ModelIdHelper;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * A plugin that removes an item from a list by index
 * 
 * @author Liat Shiff
 * 
 */
public class RemoveNumberedItem extends Plugin
{
	private static final Role PARENT = Role.get("<Parent>");
	private static final Role INDEX = Role.get("<Index>");
	private static final Role ELEMENT_NAME = Role.get("<Element Name>");

	private static final Role REMOVED_ITEM = Role.get("<Removed Item>");
	private static final Role INVALID_INDEX = Role.get("<Invalid Index>");
	private static final Role INVALID_PARENT = Role.get("<Invalid Parent>");

	SlotHandler parentTrigger, indexTrigger, invalidIndexExit, removedItemExit, invalidParentExit,
			elementNameTrigger;

	public void start(RuntimeContext context, FlowInstance flow)
	{
		Object parent = parentTrigger.get(context, flow);
		
		Object elementName = null;
		if (elementNameTrigger != null)
			elementName = elementNameTrigger.get(context, flow);

		ElementHandler listHandler;
		if (elementName != null)
			listHandler = geListHandler(parent, (String) elementName);
		else
			listHandler = geListHandler(parent, null);

		if (listHandler == null)
		{
			if (invalidParentExit != null)
			{
				setExitValue(invalidParentExit, context, flow, parent);
				return;
			}
			else
			{
				if (elementName != null)
				{
					throw new ModelExecutionException("Invalid parent",
							"There is no repetitive element in the parent with element name equals to '"
									+ (String)elementName + "'");
				}
				else
					throw new ModelExecutionException("Invalid parent",
							"There is no repetitive element in the parent");
			}
		}

		int currentIndex = ((List) listHandler.get(context, parent)).size();
		int index = (int) ((Number) indexTrigger.get(context, flow)).value;

		if (index < 1 || index > currentIndex)
		{
			if (invalidIndexExit != null)
				setExitValue(invalidIndexExit, context, flow, new Number(index));
			else
				throw new ArrayIndexOutOfBoundsException("Invalid index " + index);
			return;
		}

		if (removedItemExit != null)
		{
			Object removedItem = listHandler.get(context, parent, index - 1);
			setExitValue(removedItemExit, context, flow, removedItem);
		}

		listHandler.remove(context, parent, index);
	}

	/**
	 * Returns the list with a name equals to elementName (If it exists) Otherwise it returns the
	 * first repetitive list.
	 */
	public ElementHandler geListHandler(Object parent, String elementName)
	{
		ElementHandler[] elements = null;
		if (parent instanceof FlowInstance)
			elements = (((FlowInstance) parent).getFlowHandler()).elementHandlers;
		else
		{
			InstanceHandler parentHandler = getLoader().getHandler(
					ModelIdHelper.getCompositeModelId(parent));
			elements = parentHandler.elementHandlers;
		}

		for (ElementHandler element : elements)
		{
			if (elementName != null)
			{
				if (elementName.equals(element.getRole().toString()) && element.isRepetitive())
					return element;
			}
			else if (element.isRepetitive())
				return element;
		}
		return null;
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		parentTrigger = getRequiredMandatoryTrigger(PARENT, Boolean.FALSE, null);
		indexTrigger = getRequiredMandatoryTrigger(INDEX, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		elementNameTrigger = getNonRequiredMandatoryTrigger(ELEMENT_NAME, Boolean.FALSE, BuiltinModels.TEXT_ID);

		invalidIndexExit = getNonRequiredExit(INVALID_INDEX, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		removedItemExit = getNonRequiredExit(REMOVED_ITEM, Boolean.FALSE, null);
		invalidParentExit = getNonRequiredExit(INVALID_PARENT, Boolean.FALSE, null);
	}
}

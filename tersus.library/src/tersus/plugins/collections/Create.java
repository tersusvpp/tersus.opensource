/************************************************************************************************
 * Copyright (c) 2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.collections;

import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * @author Youval Bronicki
 *  
 */
public class Create extends Plugin
{


    private static final Role OBJECT = Role.get("<Object>");

    private SlotHandler exit;


    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext,
     *      tersus.runtime.FlowInstance)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
        Object obj = exit.getChildInstanceHandler().newInstance(context);
        setExitValue(exit, context, flow, obj);

    }


    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        exit = getRequiredExit(OBJECT, Boolean.FALSE, null);
    }

}
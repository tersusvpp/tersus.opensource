/************************************************************************************************
 * Copyright (c) 2003-2011 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.collections;

import java.util.HashSet;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang3.mutable.MutableInt;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Role;
import tersus.model.validation.Problems;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.ModelIdHelper;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.runtime.TextHandler;
import tersus.util.Misc;
import tersus.util.NotImplementedException;

/**
 * @author Youval Bronicki
 * 
 */
public class FindElements extends Plugin
{

	private static final Role SCOPE = Role.get("<Scope>");

	private static final Role ELEMENTS = Role.get("<Elements>");
	private static final Role OPTIONS = Role.get("<Options>");
    private static final Role NONE = Role.get("<None>");
    private static final Role MAX_RESULTS = Role.get("<Max Results>");
	

	private SlotHandler scopeTrigger, exit, noneExit, optionsTrigger, maxResultsTrigger;

	private SlotHandler[] filterTriggers;

	private ElementHandler[] filterElements;

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext,
	 *      tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		if (scopeTrigger == null)
			throw new ModelExecutionException(
					"Missing Trigger "
							+ SCOPE
							+ " (this triggered is required on the server side versio of the copy)");
		if (BuiltinModels.ANYTHING_ID.equals(exit.getChildModelId()))
		{
			throw new NotImplementedException("Find Elements with dynamic type not implemented on server side");
		}
		boolean allowPartial=false;
		boolean useRegex=false;
		boolean ignoreCase=false;
		int maxResults = getTriggerValue(maxResultsTrigger, context, flow, -1);
		if (optionsTrigger != null)
		{
			@SuppressWarnings("unchecked")
			List<String> options = (List<String>)optionsTrigger.get(context, flow);
			for (String option: options)
			{
				if (option.toLowerCase().contains("ignore case"))
					ignoreCase=true;
				if (option.toLowerCase().contains("regular expressions"))
					useRegex=true;
				if (option.toLowerCase().contains("allow partial"))
					allowPartial=true;
			}
		}
		Object[] testValues = getTestValues(context, flow, ignoreCase, useRegex, allowPartial);
		HashSet<Object> scanned = new HashSet<Object>();
		if (scopeTrigger.isRepetitive())
		{
			@SuppressWarnings("unchecked")
			List<Object> inputs = (List<Object>) scopeTrigger.get(context, flow);
			if (inputs != null)
			{
				for (int i = 0; i < inputs.size(); i++)
					scan(context, flow, testValues, inputs.get(i), ignoreCase, scanned, maxResults, new MutableInt(0));
			}
		} else
		{
			Object scope = scopeTrigger.get(context, flow);
			if (scope != null)
				scan(context, flow, testValues, scope, ignoreCase, scanned, maxResults, new MutableInt(0));
		}
		if (((List<?>)exit.get(context, flow)).isEmpty() && noneExit != null)
		    chargeEmptyExit(context, flow, noneExit);

	}

	/**
	 * @param context
	 * @param flow
	 * @param testValues
	 * @param object
	 * @param scanned
	 */
	private boolean scan(RuntimeContext context, FlowInstance flow,
			Object[] testValues, Object object,boolean ignoreCase,  HashSet<Object> scanned, int maxResults, MutableInt resultsFound)
	{
		if (scanned.contains(object))
			return false;
		if (maxResults>-1 && resultsFound.intValue()>=maxResults)
			return true;
		scanned.add(object);
		if (exit.getChildInstanceHandler().isInstance(object))
			if (checkFilters(context, flow, testValues, object, ignoreCase))
			{
				accumulateExitValue(exit, context, flow, object);
				resultsFound.increment();
			}
		InstanceHandler handler = null;
		if (object instanceof FlowInstance)
		{
			handler = ((FlowInstance) object).getHandler();
		} else
		{
			ModelId id = ModelIdHelper.getCompositeModelId(object);
			if (id != null)
				handler = context.getModelLoader().getHandler(id);
		}
		if (handler != null)
		{
			if (scanChildren(context, flow, testValues, object, ignoreCase,  handler, scanned, maxResults, resultsFound))
				return true;
		}
		return false;
			
			
	}

	/**
	 * @param context
	 * @param flow
	 * @param testValues
	 * @param object
	 * @return
	 */
	private boolean checkFilters(RuntimeContext context, FlowInstance flow,
			Object[] testValues, Object object,  boolean ignoreCase)
	{
		for (int i = 0; i < filterElements.length; i++)
		{
			Object testValue = testValues[i];
			Object actualValue = filterElements[i].get(context, object);
			if (testValue instanceof Pattern)
			{
				if (! (actualValue instanceof String) || ! ((Pattern)testValue).matcher((String)actualValue).find())
					return false;
			}
			else
			{
				if (!Misc.equal(actualValue, testValue, ignoreCase))
					return false;
			}
		}
		return true;
	}

	/**
	 * @param context
	 * @param flow
	 * @param values
	 * @param object
	 * @param handler
	 * @param scanned
	 */
	private boolean scanChildren(RuntimeContext context, FlowInstance flow,
			Object[] values, Object object, boolean ignoreCase, InstanceHandler handler,
			HashSet<Object> scanned, int maxResults, MutableInt resultsFound)
	{
		for (int i = 0; i < handler.elementHandlers.length; i++)
		{
			ElementHandler e = handler.elementHandlers[i];
			if (e.isRepetitive())
			{
				@SuppressWarnings("unchecked")
				List<Object> childValues = (List<Object>) e.get(context, object);
				if (childValues != null)
				{
					for (int j = 0; j < childValues.size(); j++)
					{
						Object child = childValues.get(j);
						if (scan(context, flow, values, child, ignoreCase, scanned, maxResults, resultsFound))
							return true;
					}
				}
			}
			else if (getPluginVersion() >= 1)
			{
				Object child = e.get(context, object);
				if (scan(context, flow, values, child, ignoreCase, scanned, maxResults, resultsFound))
					return true;
			}
		}
		return false;
	}

	/**
	 * @param context
	 * @param flow
	 * @param ignoreCase TODO
	 * @param useRegex TODO
	 * @param allowPartial TODO
	 * @return
	 */
	private Object[] getTestValues(RuntimeContext context, FlowInstance flow, boolean ignoreCase, boolean useRegex, boolean allowPartial)
	{
		Object[] testValues = new Object[filterTriggers.length];
		for (int i = 0; i < testValues.length; i++)
		{
			testValues[i] = filterTriggers[i].get(context, flow);
			if (useRegex && filterTriggers[i].getChildInstanceHandler() instanceof TextHandler)
			{
				String pattern = (String)testValues[i];
				if (!allowPartial)
					pattern = '^'+pattern+'$';
				testValues[i]=Pattern.compile(pattern, ignoreCase ? Pattern.CASE_INSENSITIVE : 0);
			}
				
		}
		return testValues;
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		scopeTrigger = getNonRequiredTrigger(SCOPE, null, null);
		exit = getRequiredExit(ELEMENTS, Boolean.TRUE, null);
		noneExit = getNonRequiredExit(NONE, Boolean.FALSE, BuiltinModels.NOTHING_ID);
		optionsTrigger = getNonRequiredTrigger(OPTIONS, Boolean.TRUE, BuiltinModels.TEXT_ID);
		maxResultsTrigger = getNonRequiredTrigger(MAX_RESULTS, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		if (scopeTrigger != null)
			initFilters(model);
	}

	/**
	 * @param model
	 * @return
	 */
	private void initFilters(Model model)
	{
		filterTriggers = new SlotHandler[triggers.length - 1];
		int count = 0;
		for (int i = 0; i < triggers.length; i++)
		{
			SlotHandler trigger = triggers[i];
			Role role = trigger.getRole();
			if (!trigger.isEmpty() && !role.isDistinguished())
			{
				filterTriggers[count] = trigger;
				if (trigger.isRepetitive())
					notifyInvalidModel(role, Problems.INVALID_TRIGGER,
							"Filter triggers must not be repetitive");
				++count;
			}
		}
		if (count != filterTriggers.length)
		{
			SlotHandler[] tmp = filterTriggers;
			filterTriggers = new SlotHandler[count];
			System.arraycopy(tmp, 0, filterTriggers, 0, count);
		}
		if (! BuiltinModels.ANYTHING_ID.equals(exit.getChildModelId()))
		{
			filterElements = new ElementHandler[count];
			for (int i = 0; i < count; i++)
			{
				Role role = filterTriggers[i].getRole();
				filterElements[i] = exit.getChildInstanceHandler()
						.getElementHandler(role);
				if (filterElements[i] == null)
					notifyInvalidModel(role, Problems.INVALID_TRIGGER,
							"There is no element '" + role
									+ "' in the output type ("
									+ exit.getChildModelId() + ")");
				if (filterElements[i] == null)
					notifyInvalidModel(role, Problems.INVALID_TRIGGER,
							"There is no element '" + role
									+ "' in the output type ("
									+ exit.getChildModelId() + ")");
				if (!Misc.equal(filterElements[i].getChildModelId(),
						filterTriggers[i].getChildModelId()))
					notifyInvalidModel(role, Problems.INCONSISTENT_TYPES,
							"The triggers type doesn't match the corresponding element type");
			}
		}
	}
}
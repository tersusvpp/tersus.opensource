package tersus.plugins.collections;

import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import tersus.InternalErrorException;
import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

public class GetMapEntry extends Plugin
{

    private static final Role KEY = Role.get("<Key>");
    private static final Role VALUE = Role.get("<Value>");
    private static final Role MAP = Role.get("<Map>");
    private static final Role NONE = Role.get("<None>");
    
    private SlotHandler keyTrigger, mapTrigger, valueExit, noneExit;
    @Override
    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        keyTrigger = getRequiredMandatoryTrigger(KEY, Boolean.FALSE, BuiltinModels.TEXT_ID);
        valueExit = getRequiredExit(VALUE, Boolean.FALSE, null);
        noneExit = getNonRequiredExit(NONE, Boolean.FALSE, BuiltinModels.NOTHING_ID);
        mapTrigger = getRequiredTrigger(MAP, Boolean.FALSE, BuiltinModels.MAP_ID);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void start(RuntimeContext context, FlowInstance flow)
    {
        Object map = mapTrigger.get(context,flow);
        String key = (String)keyTrigger.get(context, flow);
        Object value = null;
        if (map instanceof JSONObject)
        {
        	try
        	{
        		value = ((JSONObject)map).get(key);
        	}
        	catch (JSONException e)
        	{
        		throw new InternalErrorException("Possible concurrent access to JSON object",e);
        	}
        }
        else if  (map instanceof Map<?, ?>)
        {
            value = ((Map<String, Object>)map).get(key);
        }
        else
        {
            throw new ModelExecutionException("<Map> trigger expects a Map");
        }
        if (value != null)
            setExitValue(valueExit,context, flow, value);
        else
            chargeEmptyExit(context, flow, noneExit);
     }

}

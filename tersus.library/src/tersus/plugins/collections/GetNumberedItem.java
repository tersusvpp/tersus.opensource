/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.collections;

import java.util.List;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that returns one of its repetitive inputs (by its index)
 * 
 * @author Ofer Brandes
 *
 */
public class GetNumberedItem extends Plugin
{
	private static final Role LIST = Role.get("<List>");
	private static final Role INDEX = Role.get("<Index>"); // Counting from 1
	private static final Role ITEM = Role.get("<Item>");
    private static final Role INVALID_INDEX = Role.get("<Invalid Index>");

	SlotHandler inputTrigger, indexTrigger, itemExit, invalidIndexExit;

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		List inputs = (List) inputTrigger.get(context,flow);
		int index = (int) ((Number) indexTrigger.get(context,flow)).value;
        if (index >= 1 && index <= inputs.size())
        {
            Object out = inputs.get(index-1);
            setExitValue (itemExit, context, flow, out);
        }
        else if (invalidIndexExit != null)
            setExitValue(invalidIndexExit, context, flow, indexTrigger.get(context,flow));
        else
            throw new ModelExecutionException("Invalid Index", "Index ="+index + " List size="+inputs.size());
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		inputTrigger = getRequiredMandatoryTrigger(LIST, Boolean.TRUE, null);
		indexTrigger = getRequiredMandatoryTrigger(INDEX, Boolean.FALSE, BuiltinModels.NUMBER_ID);

		itemExit = getRequiredExit(ITEM, Boolean.FALSE, null);
		invalidIndexExit = getNonRequiredExit(INVALID_INDEX, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		checkTypeMismatch(inputTrigger, itemExit);
	}
}

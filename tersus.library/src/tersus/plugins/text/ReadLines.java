package tersus.plugins.text;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.FileUtils;

public class ReadLines extends Plugin
{
	private static final int BUFFER_SIZE = 1<<19; // 512K
	private static final Role PATH = Role.get("<Path>");
	private static final Role MAX_LINES = Role.get("<Max Lines>");
	private static final Role BYTE_OFFSET = Role.get("<Byte Offset>");
	private static final Role ENCODING = Role.get("<Encoding>");
	private static final Role TEXT = Role.get("<Text>");
	private static final Role TOTAL_SIZE = Role.get("<Total Size>");
	private static final Role END_OFFSET = Role.get("<End Offset>");
	private static final Role NUMBER_OF_LINES_RETURNED = Role.get("<Number of Lines Returned>");
	private static final Role MORE_LINES = Role.get("<More Lines>");

	private SlotHandler pathTrigger, maxLinesTrigger, byteOffsetTrigger, encodingTrigger;
	private SlotHandler textExit, totalSizeExit, endOffsetExit, numberOfLinesReturnedExit,
			moreLinesExit;

	@Override
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		pathTrigger = getRequiredMandatoryTrigger(PATH, Boolean.FALSE, BuiltinModels.TEXT_ID);
		byteOffsetTrigger = getNonRequiredTrigger(BYTE_OFFSET, Boolean.FALSE,
				BuiltinModels.NUMBER_ID);
		maxLinesTrigger = getNonRequiredTrigger(MAX_LINES, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		encodingTrigger = getNonRequiredTrigger(ENCODING, Boolean.FALSE, BuiltinModels.TEXT_ID);

		textExit = getNonRequiredExit(TEXT, Boolean.FALSE, BuiltinModels.TEXT_ID);
		totalSizeExit = getNonRequiredExit(TOTAL_SIZE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		endOffsetExit = getNonRequiredExit(END_OFFSET, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		numberOfLinesReturnedExit = getNonRequiredExit(NUMBER_OF_LINES_RETURNED, Boolean.FALSE,
				BuiltinModels.NUMBER_ID);
		moreLinesExit = getNonRequiredExit(MORE_LINES, Boolean.FALSE, BuiltinModels.NOTHING_ID);
	}

	@Override
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Invocation i = new Invocation(context, flow);
		i.run();
	}

	class Invocation
	{

		RuntimeContext context;
		FlowInstance flow;
		long maxLines;
		long byteOffset;
		long bufferOffset; //relative to whole file
		int bufferSize;
		byte[] nl;
		boolean singleByte;
		String encoding;
		FileChannel fc;
		ByteBuffer bb;
		long totalSize;
		long lastPosition; //relative to whole file
		StringBuffer out = new StringBuffer();

		public Invocation(RuntimeContext context, FlowInstance flow)
		{
			this.context = context;
			this.flow = flow;
		}

		public void run()
		{
			String path = getTriggerText(context, flow, pathTrigger);
			maxLines = getTriggerValue(maxLinesTrigger, context, flow, -1);
			byteOffset = getTriggerValue(byteOffsetTrigger, context, flow, -1);
			encoding = getTriggerText(context, flow, encodingTrigger);
			FileInputStream fis = null;
			try
			{

				if (encoding == null)
					nl = "\n".getBytes();
				else
					nl = "\n".getBytes(encoding);
				singleByte = nl.length == 1;

				File f = new File(path);
				fis = new FileInputStream(f);
				fc = fis.getChannel();

				totalSize = (long) fc.size();
				setExitValue(totalSizeExit, context, flow, totalSize);
				if (textExit != null || endOffsetExit != null || moreLinesExit != null)
				{
					bufferSize = (int)Math.min(BUFFER_SIZE,
							totalSize - byteOffset);
					lastPosition = bufferOffset = byteOffset;
					bb = fc.map(FileChannel.MapMode.READ_ONLY, bufferOffset, bufferSize);
					long count = 0;
					while ( (count < maxLines || maxLines < 0) && gotoEOL())
					{
						++count;
					}
					setExitValue(endOffsetExit, context, flow, lastPosition);
					setExitValue(numberOfLinesReturnedExit, context, flow, count);
					if (textExit != null && out.length() > 0)
						setExitValue(textExit, context, flow, out.toString());
					
					if (moreLinesExit != null && count == maxLines)
					{
						if (gotoEOL())
							chargeEmptyExit(context, flow, moreLinesExit);
					}
	
				}
				fis.close();
				fis = null;
		
			}
			catch (Exception e)
			{
				throw new ModelExecutionException("Failed to read lines from file:" + path,null,e);
			}
			finally
			{
				FileUtils.forceClose(fis);
			}
		}

		private boolean gotoEOL() throws IOException
		{
			int startPosition = (int)(lastPosition - bufferOffset); //Relative to buffer
			int nextMatch = nl[0];
			int nMatch = 0;
			boolean found = false;
			while (bb.hasRemaining() && !found)
			{
				int b = bb.get();
				if (b == nextMatch)
				{
					if (singleByte)
					{
						found = true;

					}
					else
					{
						++nMatch;
						if (nMatch == nl.length)
						{
							nMatch = 0;
							found = true;
						}
					}
				}
				if (!singleByte)
					nextMatch = nl[nMatch];
			}
			if (found)
			{
				byte[] bytes = new byte[bb.position() - startPosition];
				bb.position(startPosition);
				bb.get(bytes, 0, bytes.length);
				out.append(new String(bytes,encoding));
				lastPosition=bb.position()+bufferOffset;
			}
			else
			{
				if (bufferOffset + bufferSize < totalSize) // Buffer doesn't extend to end of file. Extend buffer towards end of file and continue searching 
				{
					long filePosition = bufferOffset+bb.position();
					bufferOffset = lastPosition;
					bufferSize = (int)Math.min(Math.max(BUFFER_SIZE, bufferSize) * 2, totalSize-bufferOffset);
					bb = fc.map(FileChannel.MapMode.READ_ONLY, bufferOffset, bufferSize);
					bb.position((int)(filePosition- bufferOffset - nMatch)); // Continue scanning from current position in file (but go back a few bytes if we have  a partial match)
					return gotoEOL();
				}
			}
			return found;
		}

	}

}

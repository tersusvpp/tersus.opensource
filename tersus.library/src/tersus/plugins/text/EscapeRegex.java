/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.text;

import java.util.regex.Pattern;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.Misc;

/**
 * Quotes text characters so that they are treated literally instead of
 * being interpreted as unescaped regular expression constructs
 * 
 * @author David Davidson
 * 
 */

public class EscapeRegex extends Plugin
{
    private static final Role TEXT = Role.get("<Text>");

    private static final Role ESCAPED = Role.get("<Escaped Text>");

    SlotHandler textTrigger, escapedExit;

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
        String text = (String) textTrigger.get(context, flow);

//		We could have used:
//
//			String escaped = Pattern.quote(text);
//
//		However, Javascript does not provide a similar quote method,
//      so in order to minimize the risk of incompatibilities,
//      we copy of the client-side implementation:
        String escaped = text.replaceAll(
        		"(["+Pattern.quote(".?*+^$[]\\(){}|-")+"])", "\\\\$1");
        setExitValue(escapedExit, context, flow, escaped);
    }

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        textTrigger = getRequiredMandatoryTrigger(TEXT, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        escapedExit = getRequiredExit(ESCAPED, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
    }
}

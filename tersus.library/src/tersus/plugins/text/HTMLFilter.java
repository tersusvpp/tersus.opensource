package tersus.plugins.text;

/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import org.htmlparser.Node;
import org.htmlparser.Parser;
import org.htmlparser.nodes.TagNode;
import org.htmlparser.util.NodeIterator;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * Filters HTML text for malicious content, by allowing only certain "safe" tags and attributes to pass through
 * , and filtering attribute values using pre-defined regular expressions.
 * @author Youval Bronicki
 *
 */
public class HTMLFilter extends Plugin
{

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        htmlTextTrigger = getRequiredMandatoryTrigger(HTML_TEXT, Boolean.FALSE, BuiltinModels.TEXT_ID);
        validTagsTrigger = getNonRequiredTrigger(VALID_TAGS, null, BuiltinModels.TEXT_ID);
        validAttributesTrigger = getNonRequiredTrigger(VALID_ATTRIBUTES, null, BuiltinModels.TEXT_ID);
        filteredTextExit = getRequiredExit(FILTERED_TEXT, Boolean.FALSE, BuiltinModels.TEXT_ID);
        filteringMessagesExit = getNonRequiredExit(FILTERING_MESSAGES, Boolean.FALSE, BuiltinModels.TEXT_ID);
    }

    public void start(RuntimeContext context, FlowInstance flow)
    {
       String text = (String)htmlTextTrigger.get(context, flow);
       Set<String> validTags = getValueSet(context, flow, validTagsTrigger, defaultValidTags);
       Set<String> validAttributes = getValueSet(context, flow, validAttributesTrigger, attributePatterns.keySet());
       Invocation invocation = new Invocation(validTags, validAttributes);
       String filteredText = invocation.filter(text);
       setExitValue(filteredTextExit, context, flow, filteredText);
       if (invocation.getMessages().length() > 0)
       {
           setExitValue(filteringMessagesExit, context ,flow, invocation.getMessages().toString());
       }
    }

    @SuppressWarnings("unchecked")
    private Set<String> getValueSet(RuntimeContext context, FlowInstance flow,
            SlotHandler trigger, Set<String> defaultSet)
    {
        Set<String> valueSet = defaultSet;
        if (trigger!= null)
           {
               Object validTagsInput = trigger.get(context, flow);
               if (validTagsInput != null)
               {
                   valueSet = new HashSet<String>();
                   if (trigger.isRepetitive())
                       valueSet.addAll((List<String>)validTagsInput);
                   else
                   {
                       StringTokenizer tok = new StringTokenizer((String)validTagsInput," ,");
                       while (tok.hasMoreTokens())
                           valueSet.add(tok.nextToken());
                   }
               }
           }
        return valueSet;
    }

    private static final String TAG_NAME_PARSER_ATTRIBUTE = "$<TAGNAME>$";
    private static final String SLASH = "/";



    private static final Role HTML_TEXT = Role.get("<HTML Text>");
    private static final Role VALID_TAGS = Role.get("<Valid Tags>");
    private static final Role VALID_ATTRIBUTES = Role.get("<Valid Attributes>");
    private static final Role FILTERED_TEXT = Role.get("<Filtered Text>"); 
    private static final Role FILTERING_MESSAGES = Role.get("<Filtering Messages>"); 
    
    
    private SlotHandler htmlTextTrigger,validTagsTrigger,validAttributesTrigger, filteredTextExit, filteringMessagesExit;
    static HashSet<String> defaultValidTags;

    static HashMap<String, Pattern> attributePatterns;

    static Pattern P_CLASS = Pattern.compile("[-a-zA-Z0-9_ ]+");

    static Pattern P_NAME = Pattern.compile("[-a-zA-Z0-9_]+");

    static Pattern P_LENGTH = Pattern.compile("[0-9]+(px|em|%)?");

    static Pattern P_URI = Pattern
            .compile("((http|https|ftp)://[a-zA-Z0-9_.]+(:\\d+)?)?[-_+?/${}@&#%.;()0-9a-zA-Z=]*");

    static Pattern P_TEXT = Pattern.compile(".*");

    static Pattern P_NUMBER = Pattern.compile("[0-9]*");

    static Pattern P_ALLIGN = Pattern
            .compile("right|left|center|middle|top|bottom|justify");

    static
    {
        defaultValidTags = new HashSet<String>();
        defaultValidTags.addAll(Arrays.asList(new String[] { "DIV", "SPAN",
                "A", "P", "BR", "BLOCKQUOTE", "Q","BR", "IMG", "B", "I", "U", "CODE", "EM", "STRONG",
                "DEL", "STRIKE", "SUP", "SUB", "ADDRESS", "H1", "H2", "H3",
                "H4", "H5", "H6", "OL", "UL", "LI", "SPAN", "IMG", "TABLE",
                "TBODY", "THEAD", "TR", "TD", "TH", "CAPTION", "CODE", }));
        attributePatterns = new HashMap<String,Pattern>();
        attributePatterns.put("HEIGHT", P_LENGTH);
        attributePatterns.put("WIDTH", P_LENGTH);
        attributePatterns.put("CLASS", P_CLASS);
        attributePatterns.put("SRC", P_URI);
        attributePatterns.put("HREF", P_URI);
        attributePatterns.put("ALT", P_TEXT);
        attributePatterns.put("COLSPAN", P_NUMBER);
        attributePatterns.put("ROWSPAN", P_NUMBER);
        attributePatterns.put("ALIGN", P_ALLIGN);
        attributePatterns.put("TARGET", P_NAME);
        attributePatterns.put("NAME", P_NAME);
        attributePatterns.put("ID", P_NAME);
        attributePatterns.put("CITE", P_URI);
    }

    public HTMLFilter()
    {
    }

    private class Invocation
    {
        StringBuffer messages = new StringBuffer();
        Set<String> validTags,validAttributes;
        public Invocation(Set<String> validTags,Set<String> validAttributes)
        {
            this.validTags = validTags;
            this.validAttributes = validAttributes;
        }
        public StringBuffer getMessages()
        {
            return messages;
        }
        public String filter(String htmlText)
        {
            StringBuffer out = new StringBuffer(htmlText.length());
            try
            {
                Parser parser = new Parser();
                parser.setInputHTML(htmlText);
                NodeList validNodes = new NodeList();
                for (NodeIterator i = parser.elements(); i.hasMoreNodes();)
                {
                    Node node = i.nextNode();
                    filterNode(node, validNodes);
                }
                for (int i=0;i<validNodes.size(); i++)
                        out.append((validNodes.elementAt(i)).toHtml());

            }
            catch (ParserException e)
            {
                throw new ModelExecutionException("Failed to parse HTML text",
                        null, e);
            }
            return out.toString();
        }

        private void addMessage(String message)
        {
            if (messages.length() > 0)
                messages.append('\n');
            messages.append(message);
        }
        private void filterNode(Node node, NodeList validNodes)
        {
            if (node instanceof TagNode)
            {
                TagNode tNode = (TagNode) node;
                if (!checkTag(tNode))
                {
                    addMessage("Invalid tag " + tNode.getTagName());
                    NodeList validChildren = getValidChildren(tNode);
                    validNodes.add(validChildren);
                }
                else
                {
                    filterAttributes(tNode);
                    filterChildren(tNode);
                    validNodes.add(tNode);
                }
            }
            else
                validNodes.add(node);
        }

        private void filterChildren(TagNode node)
        {
            node.setChildren(getValidChildren(node));
        }
        private NodeList getValidChildren(TagNode node)
        {
            NodeList originalChildren = node.getChildren();
            NodeList validChildren = new NodeList();
            if (originalChildren != null)
            {
                for (int i = 0; i < originalChildren.size(); i++)
                {
                    Node child = originalChildren.elementAt(i);
                    filterNode(child, validChildren);
                }
            }
            return validChildren;
        }

        private void filterAttributes(TagNode node)
        {
            for (Enumeration attributeNames = node.getAttributes().keys(); attributeNames
                    .hasMoreElements();)
            {
                String name = (String) attributeNames.nextElement();
                String value = node.getAttribute(name);
                if (!checkAttribute(name, value))
                {
                    node.removeAttribute(name);
                }
            }
        }

        private boolean checkAttribute(String name, String value)
        {
            if (SLASH.equals(name) && value == null)
                return true; // The slash in self-closing tags (like <BR />) is interpreted as an attribute  
            if (TAG_NAME_PARSER_ATTRIBUTE.equals(name))
                return true;
            if (! validAttributes.contains(name))
            {
                addMessage("Invalid attribute "+name);
                return false;
            }
            Pattern p = (Pattern)attributePatterns.get(name);
            if (! p.matcher(value).matches())
            {
                addMessage("Invalid value '"+value+"' for attribute '"+name+"'");
                return false;
            }
            return true;
        }

        private boolean checkTag(TagNode tNode)
        {
            String tag = tNode.getTagName().toUpperCase();
            return validTags.contains(tag);
        }
    }
}

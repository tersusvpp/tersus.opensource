/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.text;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * /**
 * A plugin from trimming white-space from the beginning and end of a text
 * 
 * @author Youval Bronicki
 *
 */
public class Trim extends Plugin
{
	private static final Role TEXT = Role.get("<Text>");
	private static final Role OUTPUT = Role.get("<Trimmed Text>");

	private SlotHandler textTrigger,exit;

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		String text = (String) textTrigger.get(context, flow);
        if (text != null)
        {
            String trimmed = text.trim();
			accumulateExitValue(exit, context, flow, trimmed);
		}
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		textTrigger = getRequiredMandatoryTrigger(TEXT, Boolean.FALSE, BuiltinModels.TEXT_ID);
		exit = getRequiredExit(OUTPUT, Boolean.FALSE, BuiltinModels.TEXT_ID);
	}

}

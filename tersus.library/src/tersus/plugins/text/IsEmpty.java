/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.text;
import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that checks whether a text string is of length 0.
 * 
 * @param <Text> (non-repetitive text trigger [not required]): A text string.
 * @param <Yes> (non-repetitive numeric exit [not required]): The input text
 * (the value of <Text>) if <Text> is of length 0 (an empty string).
 * @param <No> (non-repetitive numeric exit [not required]): The input text
 * (the value of <Text>) if <Text> is of length greater than 0
 * (contains at least one character).
 * @param <Missing> (non-repetitive empty exit [not required]): Activated if no input
 * is received through <Text> (this is possible only if <Text> is non-mandatory). 
 * 
 * @author Ofer Brandes
 *
 */
public class IsEmpty extends Plugin
{
	// Triggers 
	private static final Role TEXT = Role.get("<Text>");

	SlotHandler	inputTrigger;

	// Exits
	private static final Role YES = Role.get("<Yes>");
	private static final Role NO = Role.get("<No>");
	private static final Role MISSING = Role.get("<Missing>");

	SlotHandler	yesExit, noExit, missingExit;

	/**
	 * Initialize the handler from the action's model (done only once for each model).
	 * 
	 * Note that this method is executed not only at runtime (by the server),
	 * but also for model validation (by Tersus Studio).
	 * 
	 * @param model The model from which the flow handler is to be initialized.
	 */
	public void initializeFromModel(Model model)
	{
		// Standard initializations for all flow handlers
		super.initializeFromModel(model);

		// Get trigger
		inputTrigger = getNonRequiredTrigger(TEXT, Boolean.FALSE, BuiltinModels.TEXT_ID);
	
		// Get exits (including validation of mandatority, repetitiveness and type)
		yesExit = getNonRequiredExit(YES, Boolean.FALSE, BuiltinModels.TEXT_ID);
		noExit = getNonRequiredExit(NO, Boolean.FALSE, BuiltinModels.TEXT_ID);
		missingExit = getNonRequiredExit(MISSING, Boolean.FALSE, BuiltinModels.NOTHING_ID);
	}

	/**
	 * Actually perform the action (executed by the server once for each activation of the model).
	 * 
	 * @param context Runtime context for services such as tracing, logging and database conenctivity
	 * @param flow The action's runtime instance
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		// Get the input text (if exists)
		String text = null;
		if (inputTrigger != null)
			text = (String) inputTrigger.get(context,flow);

		// Notify if no input text is received
		if (text == null)
		{
			if (missingExit != null)
				chargeEmptyExit (context, flow, missingExit);
		}

		// Check whether the string's is empty
		else
		{
			SlotHandler exit = (text.length() == 0 ? yesExit: noExit);
			if (exit != null)
				setExitValue (exit, context, flow, text);
		}
	}

	/**
	 * Resume the action after suspension (not required for actions).
	 * 
	 * @param context Runtime context for services such as tracing, logging and database conenctivity
	 * @param flow The action's runtime instance
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}
}

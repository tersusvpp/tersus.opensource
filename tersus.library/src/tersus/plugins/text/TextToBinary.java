/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.text;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.BinaryValue;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.Base64;

/**
 * @author Youval Bronicki
 *
 */
public class TextToBinary extends Plugin
{

    protected Role TEXT = Role.get("<Text>");
    protected Role BINARY = Role.get("<Binary>");
    protected Role ENCODING = Role.get("<Encoding>");
    protected Role OPTIONS = Role.get("<Options>");
   
    protected SlotHandler textTrigger, encodingTrigger, binaryExit, optionsTrigger;
    /**
     *
     */

    public static final String BASE64 = "Base64";
    public static final String BASE64GZ = "Base64GZ";
	public static final String HEX = "Hex";
	public static final String OPTION_GZIP = "gzip";
	public static final String OPTION_NO_LINE_BREAKS = "no line breaks";
    public void start(RuntimeContext context, FlowInstance flow)
    {
        String encoding = null;
        String text = (String)textTrigger.get(context, flow);
        if (encodingTrigger != null)
            encoding = (String)encodingTrigger.get(context, flow);
        byte[] bytes = null;
        if (encoding == null)
            bytes = text.getBytes();
        else
            try
            {
                Integer base64Options = null; 
                if (BASE64.equalsIgnoreCase(encoding))
                	base64Options = Base64.NO_OPTIONS;
                else if (BASE64GZ.equalsIgnoreCase(encoding))
                	base64Options = Base64.GZIP;
                    
                if (base64Options != null)
                {
            		if (optionsTrigger != null)
            		{
            			@SuppressWarnings("unchecked")
            			List<String> options = (List<String>)optionsTrigger.get(context, flow);
            			for (String option: options)
            			{
            				if (option.toLowerCase().contains(OPTION_GZIP))
            					base64Options |= Base64.GZIP;
//            				if (option.toLowerCase().contains(OPTION_NO_LINE_BREAKS))
//            					base64Options |= Base64.DONT_BREAK_LINES;
            			}
            		}
            		bytes = Base64.decode(text, base64Options);
                }
                else if (HEX.equalsIgnoreCase(encoding))
					try
					{
						bytes = Hex.decodeHex(text.toCharArray());
					} catch (DecoderException e)
					{
		                throw new ModelExecutionException("Can't convert text (hex) to binary"+encoding);
					}
				else
                    bytes = text.getBytes(encoding);
            }
            catch (UnsupportedEncodingException e)
            {
                throw new ModelExecutionException("Can't convert text to binary: unknown encoding "+encoding);
            }
        BinaryValue out = new BinaryValue(bytes);
        setExitValue(binaryExit, context, flow, out);
    }

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        textTrigger = getRequiredMandatoryTrigger(TEXT, Boolean.FALSE, BuiltinModels.TEXT_ID);
        encodingTrigger = getNonRequiredTrigger(ENCODING, Boolean.FALSE, BuiltinModels.TEXT_ID);
        binaryExit  = getRequiredExit(BINARY, Boolean.FALSE, BuiltinModels.BINARY_ID);
        optionsTrigger = getNonRequiredTrigger(OPTIONS, Boolean.TRUE, BuiltinModels.TEXT_ID);
    }
}

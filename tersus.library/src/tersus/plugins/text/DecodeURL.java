/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.text;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.Misc;

/**
 * Encodes a string using form-url encoding base on a given character encoding
 * 
 * @author Youval Bronicki
 * 
 */

public class DecodeURL extends Plugin
{
    private static final Role DECODED = Role.get("<Decoded Text>");

    private static final Role ENCODING = Role.get("<Encoding>");

    private static final Role ENCODED = Role.get("<Text>");

    SlotHandler encodedTextTrigger, encodingTrigger, decodedTextExit;

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
        String encodedText = (String) encodedTextTrigger.get(context, flow);
        String encoding = null;
        if (encodingTrigger != null)
            encoding = (String) encodingTrigger.get(context, flow);
        if (encoding == null)
            encoding = "UTF-8";
        try
        {
            String decodedText = URLDecoder.decode(Misc.replaceAll(encodedText,
                    "%20","+"), encoding);
            setExitValue(decodedTextExit, context, flow, decodedText);
        
        }
        catch (UnsupportedEncodingException e)
        {
            throw new ModelExecutionException("Unsupported encoding "
                    + encoding);
        }
    }

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        encodedTextTrigger = getRequiredMandatoryTrigger(ENCODED, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        encodingTrigger = getNonRequiredTrigger(ENCODING, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        decodedTextExit = getRequiredExit(DECODED, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
    }
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.text;

import org.apache.commons.lang3.StringEscapeUtils;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * Unescapes the HTML character entities in a String.
 * 
 * @author Youval Bronicki
 * 
 */

public class UnescapeHTML extends Plugin
{
    private static final Role ESCAPED = Role.get("<Escaped Text>");

    private static final Role UNESCAPED = Role.get("<Unescaped Text>");

    SlotHandler escapedTextTrigger,  unescapedTextExit;

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
        String escapedText = (String) escapedTextTrigger.get(context, flow);
        String unescapedText = StringEscapeUtils.unescapeHtml4(escapedText);
        setExitValue(unescapedTextExit, context, flow, unescapedText);
    }

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        escapedTextTrigger = getRequiredMandatoryTrigger(ESCAPED, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        unescapedTextExit = getRequiredExit(UNESCAPED, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
    }
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.text;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * 
 * A plugin that extracts a segment (substring) from a text using a fixed
 * position and an optional length
 * 
 * @author Youval Bronicki
 *  
 */
public class Extract extends Plugin
{
    private static final Role TEXT = Role.get("<Text>");

    private static final Role POSITION = Role.get("<Position>");

    private static final Role LENGTH = Role.get("<Length>");

    private static final Role SEGMENT = Role.get("<Segment>");

    private InstanceHandler exitType;

    private SlotHandler textTrigger;

    private SlotHandler positionTrigger;

    private SlotHandler lengthTrigger;

    private SlotHandler segmentExit;

    private String separators;

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext,
     *      tersus.runtime.FlowInstance)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
        String inputText = (String) textTrigger.get(context, flow);
        if (inputText != null)
        {
            Number position = (Number) positionTrigger.get(context, flow);
            int beginIndex = (int) position.value;
            int endIndex = inputText.length();
            if (lengthTrigger != null)
            {
                Number length = (Number) lengthTrigger.get(context, flow);
                int l = (int) length.value;
                if (beginIndex < 0)
                    throw new ModelExecutionException("Negative value '"
                            + beginIndex + "' for " + POSITION);
                if (l < 0)
                    throw new ModelExecutionException("Negative value '" + l
                            + "' for " + LENGTH);
                endIndex = beginIndex + l;
                if (endIndex > inputText.length())
                    throw new ModelExecutionException(TEXT + " ('" + inputText
                            + "') too short to extract a segment of length "
                            + l + " at position " + beginIndex + ".");
            }
            String segment = inputText.substring(beginIndex, endIndex);
            setExitValue(segmentExit, context, flow, segment);
        }
    }

    /**
     * @param string
     */

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
     */
    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);

        textTrigger = getRequiredMandatoryTrigger(TEXT, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        positionTrigger = getRequiredMandatoryTrigger(POSITION, Boolean.FALSE,
                BuiltinModels.NUMBER_ID);
        lengthTrigger = getNonRequiredMandatoryTrigger(LENGTH, Boolean.FALSE,
                BuiltinModels.NUMBER_ID);

        segmentExit = getRequiredExit(SEGMENT, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
    }

}
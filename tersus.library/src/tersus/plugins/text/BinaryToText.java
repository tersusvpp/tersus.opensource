/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.text;

import java.io.UnsupportedEncodingException;
import java.util.List;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.BinaryValue;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.Base64;

/**
 * @author Youval Bronicki
 *
 */
public class BinaryToText extends Plugin
{

    protected Role TEXT = Role.get("<Text>");
    protected Role BINARY = Role.get("<Binary>");
    protected Role ENCODING = Role.get("<Encoding>");
    protected Role OPTIONS = Role.get("<Options>");
    
    protected SlotHandler textExit, encodingTrigger, binaryTrigger, optionsTrigger;
    /**
     *
     */

    public void start(RuntimeContext context, FlowInstance flow)
    {
        String encoding = null;
        BinaryValue binary = (BinaryValue)binaryTrigger.get(context, flow);
        if (encodingTrigger != null)
            encoding = (String)encodingTrigger.get(context, flow);
        byte[] bytes = binary.toByteArray();
        String text;
        if (encoding == null)
            text = new String(bytes);
        else
            try
            {
                Integer base64Options = null; 
                if (TextToBinary.BASE64.equalsIgnoreCase(encoding))
                	base64Options = Base64.NO_OPTIONS;
                else if (TextToBinary.BASE64GZ.equalsIgnoreCase(encoding))
                	base64Options = Base64.GZIP;
                
                if (base64Options != null)
                {
            		if (optionsTrigger != null)
            		{
            			@SuppressWarnings("unchecked")
            			List<String> options = (List<String>)optionsTrigger.get(context, flow);
            			for (String option: options)
            			{
            				if (option.toLowerCase().contains(TextToBinary.OPTION_GZIP))
            					base64Options |= Base64.GZIP;
            				if (option.toLowerCase().contains(TextToBinary.OPTION_NO_LINE_BREAKS))
            					base64Options |= Base64.DONT_BREAK_LINES;
            			}
            		}
                    text = Base64.encodeBytes(bytes, base64Options);
                }
                else if (TextToBinary.HEX.equalsIgnoreCase(encoding))
                	text = new String(org.apache.commons.codec.binary.Hex.encodeHex(bytes));
                else
                    text = new String(bytes,encoding);
            }
            catch (UnsupportedEncodingException e)
            {
                throw new ModelExecutionException("Can't convert text to binary: unknown encoding "+encoding);
            }
        setExitValue(textExit, context, flow, text);
    }

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        binaryTrigger = getRequiredMandatoryTrigger(BINARY, Boolean.FALSE, BuiltinModels.BINARY_ID);
        encodingTrigger = getNonRequiredTrigger(ENCODING, Boolean.FALSE, BuiltinModels.TEXT_ID);
        textExit  = getRequiredExit(TEXT, Boolean.FALSE, BuiltinModels.TEXT_ID);
        optionsTrigger = getNonRequiredTrigger(OPTIONS, Boolean.TRUE, BuiltinModels.TEXT_ID);
    }
}

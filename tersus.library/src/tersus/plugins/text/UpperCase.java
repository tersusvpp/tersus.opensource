/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.text;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * 
 * An atomic flow handler that converts a string to upper case.
 * 
 * @author Ofer Brandes
 *
 */
public class UpperCase extends Plugin
{
	private SlotHandler exit;
	private SlotHandler textTrigger;
	
	private static final Role TEXT=Role.get("<Text>");
	private static final Role OUTPUT=Role.get("<Upper Case>");

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		String text = (String)textTrigger.get(context, flow);
		String out  = text.toUpperCase();
		setExitValue(exit, context, flow, out);
	}


	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		textTrigger = getRequiredMandatoryTrigger(TEXT, Boolean.FALSE, BuiltinModels.TEXT_ID);
		exit = getRequiredExit(OUTPUT, Boolean.FALSE, BuiltinModels.TEXT_ID);
	}

}

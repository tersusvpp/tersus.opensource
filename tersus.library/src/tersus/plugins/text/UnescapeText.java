/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.text;

import org.apache.commons.lang3.StringEscapeUtils;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * Unescapes the HTML character entities in a String.
 * 
 * @author Youval Bronicki
 * 
 */

public class UnescapeText extends Plugin
{
    private static final Role FORMAT = Role.get("<Format>");

    private static final Role ESCAPED = Role.get("<Escaped Text>");

    private static final Role UNESCAPED = Role.get("<Unescaped Text>");

    private static final Role INVALID_FORMAT = Role.get("<Invalid Format>");

    SlotHandler formatTrigger, escapedTextTrigger,  unescapedTextExit, invalidFormatExit;

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
    	String format = (String) formatTrigger.get(context, flow);
        String escapedText = (String) escapedTextTrigger.get(context, flow);
        String unescapedText = null;
        boolean validFormat = true;
        
        if ("CSV".equalsIgnoreCase(format))
        	unescapedText = StringEscapeUtils.unescapeCsv(escapedText);
        else
            if ("ECMASCRIPT".equalsIgnoreCase(format) || "JAVASCRIPT".equalsIgnoreCase(format))
            	unescapedText = StringEscapeUtils.unescapeEcmaScript(escapedText);
            else
                if ("HTML3".equalsIgnoreCase(format))
                	unescapedText = StringEscapeUtils.unescapeHtml3(escapedText);
                else
                    if ("HTML4".equalsIgnoreCase(format) || "HTML".equalsIgnoreCase(format))
                    	unescapedText = StringEscapeUtils.unescapeHtml4(escapedText);
                    else
                        if ("JAVA".equalsIgnoreCase(format))
                        	unescapedText = StringEscapeUtils.unescapeJava(escapedText);
                        else
                            if ("JSON".equalsIgnoreCase(format))
                            	unescapedText = StringEscapeUtils.unescapeJson(escapedText);
                            else
                                if ("XML10".equalsIgnoreCase(format) || "XML11".equalsIgnoreCase(format) || "XML".equalsIgnoreCase(format))
                                	unescapedText = StringEscapeUtils.unescapeXml(escapedText);
                                else
                                	validFormat = false;
        
        if (validFormat)
            setExitValue(unescapedTextExit, context, flow, unescapedText);
        else
            if (invalidFormatExit != null)
                setExitValue(invalidFormatExit, context, flow, formatTrigger.get(context,flow));
            else
                throw new ModelExecutionException("Invalid format", "'" + format + "' is not supported");

    }

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        formatTrigger = getRequiredMandatoryTrigger(FORMAT, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        escapedTextTrigger = getRequiredMandatoryTrigger(ESCAPED, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        unescapedTextExit = getRequiredExit(UNESCAPED, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
		invalidFormatExit = getNonRequiredExit(INVALID_FORMAT, Boolean.FALSE,
				BuiltinModels.TEXT_ID);
     }
}

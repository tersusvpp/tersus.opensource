/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.text;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * A plugin that checks whether a text string (<Text>) starts with a given prefix (<Prefix>)
 * 
 * @author Youval Bronicki
 *
 */


public class StartsWith extends Plugin
{
	private static final Role TEXT = Role.get("<Text>");
	private static final Role PREFIX = Role.get("<Prefix>");
	private static final Role YES = Role.get("<Yes>");
	private static final Role NO = Role.get("<No>");

	SlotHandler textTrigger, prefixTrigger, yesExit, noExit;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
	    String text = (String)textTrigger.get(context, flow);
	    String prefix = (String)prefixTrigger.get(context, flow);
	    if (text.startsWith(prefix))
	    	setExitValue(yesExit, context, flow, text);
	    else
	    	setExitValue(noExit, context, flow, text);
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		textTrigger = getRequiredMandatoryTrigger(TEXT, Boolean.FALSE, BuiltinModels.TEXT_ID);
		prefixTrigger = getRequiredMandatoryTrigger(PREFIX, Boolean.FALSE, BuiltinModels.TEXT_ID);
		yesExit = getRequiredExit(YES, Boolean.FALSE, BuiltinModels.TEXT_ID);
		noExit = getRequiredExit(NO, Boolean.FALSE, BuiltinModels.TEXT_ID);
	}
}

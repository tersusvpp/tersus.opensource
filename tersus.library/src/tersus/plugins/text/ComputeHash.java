/************************************************************************************************
 * Copyright (c) 2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.text;
import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler to calculate a hash value for a string.
 * 
 * @author Youval Bronicki
 *
 */
public class ComputeHash extends Plugin
{
	// Triggers 
	private static final Role TEXT = Role.get("<Text>");

	SlotHandler	trigger;

	// Exits
	private static final Role HASH_VALUE = Role.get("<Hash Value>");

	SlotHandler	exit;

	/**
	 * Initialize the handler from the action's model (done only once for each model).
	 * 
	 * Note that this method is executed not only at runtime (by the server),
	 * but also for model validation (by Tersus Studio).
	 * 
	 * @param model The model from which the flow handler is to be initialized.
	 */
	public void initializeFromModel(Model model)
	{
		// Standard initializations for all flow handlers
		super.initializeFromModel(model);

		// Get trigger
		trigger = getRequiredMandatoryTrigger(TEXT, Boolean.FALSE, BuiltinModels.TEXT_ID);
	
		// Get exits (including validation of mandatority, repetitiveness and type)
		exit = getRequiredExit(HASH_VALUE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
	}

	/**
	 * Actually perform the action (executed by the server once for each activation of the model).
	 * 
	 * @param context Runtime context for services such as tracing, logging and database conenctivity
	 * @param flow The action's runtime instance
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		String text = null;
		text = (String) trigger.get(context,flow);


		Number hash = new Number(text.hashCode());
		setExitValue (exit, context, flow, hash);
	}

	/**
	 * Resume the action after suspension (not required for actions).
	 * 
	 * @param context Runtime context for services such as tracing, logging and database conenctivity
	 * @param flow The action's runtime instance
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}
}

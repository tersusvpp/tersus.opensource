/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.text;

import java.util.StringTokenizer;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * /**
 * An atomic flow handler that splits a String into a sequence of sub-strings.
 * 
 * @author Youval Bronicki
 *
 */
public class Split extends Plugin
{
	private static final Role TEXT = Role.get("<Text>");
	private static final Role SEPARATORS = Role.get("<Separators>");
	private static final Role OUTPUT = Role.get("<Segments>");
	private static final Role RET_EMPTY = Role.get("<Return Empty Strings>");
	private InstanceHandler exitType;
	private SlotHandler textTrigger, seperatorsTrigger, returnEmptyStringsTrigger;
	private SlotHandler exit;

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		
		String sep = (String)seperatorsTrigger.get(context, flow);
		String text = (String) textTrigger.get(context, flow);
		
		boolean allowEmpty = getTriggerValue(returnEmptyStringsTrigger,context, flow,false);
/*		if (returnEmptyStringsTrigger != null && returnEmptyStrings)	//it's not mandatory trigger
			splitWithEmpty(text,separators,context,flow);
		else		
			regularSplit(text, separators,context,flow);//returnEmptyStringsTrigger*/
		split (text, sep, context, flow, allowEmpty);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		textTrigger = getRequiredMandatoryTrigger(TEXT, Boolean.FALSE, BuiltinModels.TEXT_ID);
		seperatorsTrigger = getRequiredMandatoryTrigger(SEPARATORS, Boolean.FALSE, BuiltinModels.TEXT_ID);
		returnEmptyStringsTrigger = getNonRequiredMandatoryTrigger(RET_EMPTY,Boolean.FALSE, BuiltinModels.BOOLEAN_ID);
		exit = getRequiredExit(OUTPUT, Boolean.TRUE, BuiltinModels.TEXT_ID);
		exitType = exit.getChildInstanceHandler();
	}
	
	private void split(String text, String sep, RuntimeContext context, FlowInstance flow, boolean allowEmpty)
	{
		int start = 0;
		int pos = 0;
		while (pos < text.length())
		{
			char c = text.charAt(pos);
			if (sep.indexOf(c) >= 0) // Found a separator
			{
				if (allowEmpty || pos > start)
					accumulateExitValue(exit, context, flow, text.substring(start, pos));
				++pos;
				start = pos;
			}
			else
				++pos;
		}
		if (allowEmpty || pos > start) // End of text is considered a separator
			accumulateExitValue(exit, context, flow, text.substring(start, pos));

			
		
	}
}
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.text;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 *
 * A plugin that finds the position of a segment (substring) within a longer text
 * @author Youval Bronicki
 */
public class GetPosition extends Plugin
{
    private static final Role TEXT=Role.get("<Text>");
    private static final Role POSITION=Role.get("<Position>");
    private static final Role SEGMENT = Role.get("<Segment>");
	private InstanceHandler exitType;
	private SlotHandler textTrigger;
	private SlotHandler positionExit;
	private SlotHandler segmentTrigger;
	private String separators;
    private static final Role NOT_FOUND = Role.get("<Not Found>");
    private SlotHandler notFoundExit;

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		String text = (String) textTrigger.get(context, flow);
		String segment = (String)segmentTrigger.get(context,flow);
		int p = text.indexOf(segment);
		if (p>=0)
		{
		   Number position = new Number(p);
		   setExitValue(positionExit, context, flow, position);
		}
		else
		{
		    if (notFoundExit != null)
		        setExitValue(notFoundExit, context, flow, segment);
		    else
		        throw new ModelExecutionException("Segment '"+segment+"' is not contained in '"+text+"' but "+NOT_FOUND+" exit is missing");
		}
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		textTrigger = getRequiredMandatoryTrigger(TEXT, Boolean.FALSE, BuiltinModels.TEXT_ID);
		segmentTrigger = getRequiredMandatoryTrigger(SEGMENT, Boolean.FALSE, BuiltinModels.TEXT_ID);
		positionExit = getRequiredExit(POSITION, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		notFoundExit = getNonRequiredExit(NOT_FOUND, Boolean.FALSE, BuiltinModels.TEXT_ID);
	}
}

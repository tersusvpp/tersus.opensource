/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.text;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.Misc;

/**
 * 
 * An atomic flow handler that replaces a string within another string:
 * 
 * Within the input '<Text>', any occurance of '<Replace>' is replaced by '<By>'
 * 
 * @author Youval Bronicki
 *
 */
public class Replace extends Plugin
{
	private SlotHandler exit;
	private SlotHandler textTrigger;
	private SlotHandler replaceTrigger;
	private SlotHandler byTrigger;
	
	private static final Role TEXT=Role.get("<Text>");
	private static final Role REPLACE=Role.get("<Replace>");
	private static final Role BY=Role.get("<By>");
	private static final Role OUTPUT=Role.get("<Updated Text>");

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		String text = (String)textTrigger.get(context, flow);
		String replace = (String)replaceTrigger.get(context, flow);
		String by = (String)byTrigger.get(context, flow);
        if (by == null)
            by = "";

		String out  = Misc.replaceAll(text, replace, by);
		setExitValue(exit, context, flow, out);
	}


	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		textTrigger = getRequiredMandatoryTrigger(TEXT, Boolean.FALSE, BuiltinModels.TEXT_ID);
		replaceTrigger = getRequiredMandatoryTrigger(REPLACE, Boolean.FALSE, BuiltinModels.TEXT_ID);
		byTrigger = getRequiredTrigger(BY, Boolean.FALSE, BuiltinModels.TEXT_ID);

		exit = getRequiredExit(OUTPUT, Boolean.FALSE, BuiltinModels.TEXT_ID);
	}

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.text;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.Misc;

/**
 * Encodes a string using form-url encoding base on a given character encoding
 * 
 * @author Youval Bronicki
 * 
 */

public class EncodeURL extends Plugin
{
    private static final Role TEXT = Role.get("<Text>");

    private static final Role ENCODING = Role.get("<Encoding>");

    private static final Role ENCODED = Role.get("<Encoded Text>");

    SlotHandler textTrigger, encodingTrigger, encodedExit;

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
        String text = (String) textTrigger.get(context, flow);
        String encoding = null;
        if (encodingTrigger != null)
            encoding = (String) encodingTrigger.get(context, flow);
        if (encoding == null)
            encoding = "UTF-8";
        try
        {
            String encoded = URLEncoder.encode(text, encoding);
            setExitValue(encodedExit, context, flow, Misc.replaceAll(encoded,
                    "+", "%20"));
        }
        catch (UnsupportedEncodingException e)
        {
            throw new ModelExecutionException("Unsupported encoding "
                    + encoding);
        }
    }

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        textTrigger = getRequiredMandatoryTrigger(TEXT, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        encodingTrigger = getNonRequiredTrigger(ENCODING, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        encodedExit = getRequiredExit(ENCODED, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
    }
}

/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.text;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.NumberHandler;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * A plugin that checks whether a text string (<Text>) matches a given pattern (<Pattern>)
 * 
 * The pattern is as defined by java.util.regex.Pattern
 * 
 * @author Ofer Brandes
 *
 */
public class Match extends Plugin
{
	private static final Role TEXT = Role.get("<Text>");
	private static final Role PATTERN = Role.get("<Pattern>");
	private static final Role YES = Role.get("<Yes>");
	private static final Role NO = Role.get("<No>");
	private static final Role MISSING = Role.get("<Missing>");
	private static final Role IGNORE_CASE = Role.get("<Ignore Case>");
	private static final Role ALLOW_PARTIAL_MATCH = Role.get("<Allow Partial Match>");
	private static final Role MATCHES = Role.get("<Matches>");
	private static final Role MATCH_GROUPS = Role.get("<Match Groups>");
	private static final Role INVALID_GROUPS = Role.get("<Invalid Groups>");

	SlotHandler textTrigger, patternTrigger, yesExit, noExit, missingExit, ignoreCaseTrigger, allowPartialTrigger, matchesExit, matchGroupsTrigger, invalidGroupsExit;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
	    String text = (String) textTrigger.get(context, flow);

	    if (text == null)
	    {
	    	if (missingExit != null)
	    		chargeEmptyExit (context, flow, missingExit);
	    }
	    else
	    {
	    	String pattern = (String) patternTrigger.get(context, flow);
			boolean allowPartial = getTriggerValue(allowPartialTrigger, context, flow, false);
			if (!allowPartial)
			{
				if (pattern.charAt(0) != '^')
					pattern = '^'+pattern;
				if (pattern.charAt(pattern.length()-1) != '$')
					pattern = pattern + '$';
			}
			int flags = getTriggerValue(ignoreCaseTrigger, context, flow, false) ? Pattern.CASE_INSENSITIVE : 0;
			Pattern p = Pattern.compile(pattern, flags);// Consider caching compiled  patterns
			Matcher m = p.matcher(text);
		    if (m.find())
		    {
		    	setExitValue(yesExit, context, flow, text);
		    	if (matchesExit != null)
		    	{
		    		int groupCount = m.groupCount();
		    		boolean[] matchGroup = new boolean[groupCount+1];
		    		boolean matchGroupsSpecified = false; 
					if (matchGroupsTrigger != null)
					{
						@SuppressWarnings("unchecked")
						List<Number> matchGroups = (List<Number>)matchGroupsTrigger.get(context, flow);
						for (Number g : matchGroups)
							if ((int)g.value <= groupCount)
							{
								matchGroupsSpecified = matchGroup[(int)g.value] = true;
							}
							else
								if (invalidGroupsExit != null)
									accumulateExitValue(invalidGroupsExit, context, flow, g);
					}

					do
			    	{
			    		for (int i=0;i<=groupCount;i++)
			    			if (!matchGroupsSpecified || matchGroup[i])
			    				accumulateExitValue(matchesExit, context, flow, m.group(i));
			    	} while ((getPluginVersion() > 0) && m.find());
		    	}
		    }
		    else
		    	setExitValue(noExit, context, flow, text);
	    }
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		textTrigger = getNonRequiredTrigger(TEXT, Boolean.FALSE, BuiltinModels.TEXT_ID);
		patternTrigger = getRequiredMandatoryTrigger(PATTERN, Boolean.FALSE, BuiltinModels.TEXT_ID);
		yesExit = getRequiredExit(YES, Boolean.FALSE, BuiltinModels.TEXT_ID);
		noExit = getRequiredExit(NO, Boolean.FALSE, BuiltinModels.TEXT_ID);
		missingExit = getNonRequiredExit(MISSING, Boolean.FALSE, BuiltinModels.NOTHING_ID);
		ignoreCaseTrigger = getNonRequiredTrigger(IGNORE_CASE, Boolean.FALSE, BuiltinModels.BOOLEAN_ID);
		allowPartialTrigger = getNonRequiredTrigger(ALLOW_PARTIAL_MATCH, Boolean.FALSE, BuiltinModels.BOOLEAN_ID);
		matchesExit = getNonRequiredExit(MATCHES, Boolean.TRUE, BuiltinModels.TEXT_ID);
		matchGroupsTrigger = getNonRequiredTrigger(MATCH_GROUPS, Boolean.TRUE, BuiltinModels.NUMBER_ID);
		invalidGroupsExit = getNonRequiredExit(INVALID_GROUPS, Boolean.TRUE, BuiltinModels.NUMBER_ID);
	}
}

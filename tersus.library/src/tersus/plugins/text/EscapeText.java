/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.text;

import org.apache.commons.lang3.StringEscapeUtils;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * Escapes a String using HTML entities.
 * 
 * 
 * @author David Davidson
 * 
 */

public class EscapeText extends Plugin
{
    private static final Role FORMAT = Role.get("<Format>");

    private static final Role UNESCAPED = Role.get("<Unescaped Text>");

    private static final Role ESCAPED = Role.get("<Escaped Text>");

    private static final Role INVALID_FORMAT = Role.get("<Invalid Format>");

    SlotHandler formatTrigger, unescapedTextTrigger,  escapedTextExit, invalidFormatExit;

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
     */
    public void start(RuntimeContext context, FlowInstance flow)
    {
    	String format = (String) formatTrigger.get(context, flow);
    	String unescapedText = (String) unescapedTextTrigger.get(context, flow);
        String escapedText = null;
        boolean validFormat = true;
        
        if ("CSV".equalsIgnoreCase(format))
        	escapedText = StringEscapeUtils.escapeCsv(unescapedText);
        else
            if ("ECMASCRIPT".equalsIgnoreCase(format) || "JAVASCRIPT".equalsIgnoreCase(format))
            	escapedText = StringEscapeUtils.escapeEcmaScript(unescapedText);
            else
                if ("HTML3".equalsIgnoreCase(format))
                	escapedText = StringEscapeUtils.escapeHtml3(unescapedText);
                else
                    if ("HTML4".equalsIgnoreCase(format) || "HTML".equalsIgnoreCase(format))
                    	escapedText = StringEscapeUtils.escapeHtml4(unescapedText);
                    else
                        if ("JAVA".equalsIgnoreCase(format))
                        	escapedText = StringEscapeUtils.escapeJava(unescapedText);
                        else
                            if ("JSON".equalsIgnoreCase(format))
                            	escapedText = StringEscapeUtils.escapeJson(unescapedText);
                            else
                                if ("XML10".equalsIgnoreCase(format))
                                	escapedText = StringEscapeUtils.escapeXml10(unescapedText);
	                            else
	                                if ("XML11".equalsIgnoreCase(format) || "XML".equalsIgnoreCase(format))
	                                	escapedText = StringEscapeUtils.escapeXml11(unescapedText);
	                                else
	                                	validFormat = false;
        
        if (validFormat)
            setExitValue(escapedTextExit, context, flow, escapedText);
        else
            if (invalidFormatExit != null)
                setExitValue(invalidFormatExit, context, flow, formatTrigger.get(context,flow));
            else
                throw new ModelExecutionException("Invalid format", "'" + format + "' is not supported");
    }

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        formatTrigger = getRequiredMandatoryTrigger(FORMAT, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        unescapedTextTrigger = getRequiredMandatoryTrigger(UNESCAPED, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        escapedTextExit = getRequiredExit(ESCAPED, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
		invalidFormatExit = getNonRequiredExit(INVALID_FORMAT, Boolean.FALSE,
				BuiltinModels.TEXT_ID);
    }
}

/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that concatenates a number of input strings
 * 
 * @author Youval Bronicki
 *
 */
public class Concatenate extends Plugin
{
	private static final Role SEPARATOR = Role.get("<Separator>");
	private static final Role OUTPUT_ROLE = Role.get("<Concatenation>");

	private ElementHandler separatorTrigger;
	ArrayList<SlotHandler> inputs;
	SlotHandler exit;

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		StringBuffer out = new StringBuffer();
		String separatorString = null;
		if (separatorTrigger != null)
			separatorString = (String) separatorTrigger.get(context, flow);
		int nValues = 0;
		for (int i = 0; i < inputs.size(); i++)
		{
			SlotHandler trigger = (SlotHandler) inputs.get(i);
			Object value = trigger.get(context, flow);
			//FUNC3 ignore non-atomic inputs (what about validation?)
			if (value != null)
			{
				if (!trigger.isRepetitive())
				{
					if (nValues > 0 && separatorString != null)
						out.append(separatorString);
					nValues++;
					out.append(value);
				}
				else
				{
					List<?> values = (List<?>) value;
					for (int j = 0; j < values.size(); j++)
					{
						if (nValues > 0 && separatorString != null)
							out.append(separatorString);
						nValues++;
						out.append(values.get(j));
					}
				}
			}
		}
		String exitValue = out.toString();
		setExitValue(exit, context, flow, exitValue);
	}
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{ // Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 * 
	 * Create a triggers sorted by lexicographically (by role)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		inputs = new ArrayList<SlotHandler>();
		separatorTrigger = getNonRequiredTrigger(SEPARATOR, Boolean.FALSE, BuiltinModels.TEXT_ID);
		for (int i = 0; i < triggers.length; i++)
			if (!triggers[i].getRole().isDistinguished())
			{
				inputs.add(triggers[i]);
			}

		Comparator<SlotHandler> c = new Comparator<SlotHandler>()
		{

			public int compare(SlotHandler trigger1,SlotHandler trigger2)
			{
				return (trigger1.getRole().toString().compareTo(trigger2.getRole().toString()));
			}
		};
		Collections.sort(inputs, c);
		exit = getRequiredExit(OUTPUT_ROLE, Boolean.FALSE, BuiltinModels.TEXT_ID);
	}

}

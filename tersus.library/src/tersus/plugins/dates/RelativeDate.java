/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.dates;

import java.util.Date;
import java.util.GregorianCalendar;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.DateUtil;

/**
 * An atomic flow handler that calculates a date relative to a given base date.
 * 
 * @param Optional <Base Date>: A base date (if none, current date is used)
 * @param Optional <Years>: Number of years to add to the base date
 * @param Optional <Months>: Number of months to add to the base date
 * @param Optional <Days>: Number of days to add to the base date
 * 
 * @author Ofer Brandes
 *
 */
public class RelativeDate extends Plugin
{
	private static final Role BASE_DATE_ROLE = Role.get("<Base Date>");
	private static final Role YEARS_ROLE = Role.get("<Years>");
	private static final Role MONTHS_ROLE = Role.get("<Months>");
	private static final Role DAYS_ROLE = Role.get("<Days>");
	private static final Role DATE_ROLE = Role.get("<Date>");
		
	SlotHandler baseTrigger, yearsTrigger, monthsTrigger, daysTrigger;
	SlotHandler exit;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtimle.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		GregorianCalendar calendar;

		calendar = new GregorianCalendar();
		if (baseTrigger != null) {
			Date date = (Date) baseTrigger.get(context,flow);
			if (date != null)
				calendar.setTime(date);
		}
		DateUtil.getDate(calendar);
	
//		System.out.println("Initial Date: "+calendar.getTime());

		calendar.setLenient(true);
		
		// BUG2 Some pathological cases may occur (Februray 29th + 1 year - 1 day = February 27)
		
		if (yearsTrigger != null) {
			Number years = (Number) yearsTrigger.get(context,flow);
			if (years != null)
				calendar.add(GregorianCalendar.YEAR,(int) years.value);
		}
		
		if (monthsTrigger != null) {
			Number months = (Number) monthsTrigger.get(context,flow);
			if (months != null)
				calendar.add(GregorianCalendar.MONTH,(int) months.value);
		}
		
		if (daysTrigger != null) {
			Number days = (Number) daysTrigger.get(context,flow);
			if (days != null)
				calendar.add(GregorianCalendar.DAY_OF_MONTH,(int) days.value);
		}

		Date relativeDate = calendar.getTime();
//		System.out.println("Relative Date: "+relativeDate);
		setExitValue(exit, context, flow, relativeDate);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		baseTrigger = getNonRequiredTrigger(BASE_DATE_ROLE, Boolean.FALSE, BuiltinModels.DATE_ID);
		yearsTrigger = getNonRequiredTrigger(YEARS_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		monthsTrigger = getNonRequiredTrigger(MONTHS_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		daysTrigger = getNonRequiredTrigger(DAYS_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		
		exit = getRequiredExit(DATE_ROLE, Boolean.FALSE, BuiltinModels.DATE_ID);
	}
}

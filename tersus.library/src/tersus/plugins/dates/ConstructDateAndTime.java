/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.dates;
import java.util.Date;
import java.util.GregorianCalendar;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
/**
 * An atomic flow handler that creates a date from year, month and day numbers
 * Year numbers are full years (e.g. 1999, 2004)
 * Month numbers are 1-12
 * Day numbers are 1-31
 * 
 * @author Youval Bronicki
 *
 */
public class ConstructDateAndTime extends Plugin
{
	private static final Role YEAR = Role.get("<Year>");
	private static final Role MONTH = Role.get("<Month>");
	private static final Role DAY = Role.get("<Day>");
    private static final Role HOUR = Role.get("<Hour>");
    private static final Role MINUTE = Role.get("<Minute>");
    private static final Role SECOND = Role.get("<Second>");
    private static final Role MILLISECOND = Role.get("<Millisecond>");
	private static final Role DATE_AND_TIME = Role.get("<Date and Time>");

	SlotHandler dateAndTimeExit;
	SlotHandler yearTrigger, monthTrigger, dayTrigger;
    SlotHandler hourTrigger, minuteTrigger, secondTrigger, millisecondTrigger;
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Number yearObj = (Number) yearTrigger.get(context, flow);
		Number monthObj = (Number) monthTrigger.get(context, flow);
		Number dayObj = (Number) dayTrigger.get(context, flow);

		int year = (int) yearObj.value;
		int month = (int) (monthObj.value - 1);
		int day = (int) dayObj.value;
        
        int hours = 0;
        int minutes = 0;
        int seconds = 0;
        int milliseconds = 0;
        
        if (hourTrigger != null)
        {
            Number hoursObj = (Number)hourTrigger.get(context, flow);
            if (hoursObj != null)
                hours = (int)hoursObj.value;
        }
        if (minuteTrigger != null)
        {
            Number minutesObj = (Number)minuteTrigger.get(context, flow);
            if (minutesObj != null)
                minutes = (int)minutesObj.value;
        }
        if (secondTrigger != null)
        {
            Number secondsObj = (Number)secondTrigger.get(context, flow);
            if (secondsObj != null)
                seconds = (int)secondsObj.value;
        }
        if (millisecondTrigger != null)
        {
            Number millisecondsObj = (Number)millisecondTrigger.get(context, flow);
            if (millisecondsObj != null)
                milliseconds = (int)millisecondsObj.value;
        }
		GregorianCalendar calendar = new GregorianCalendar();
        calendar.set(year, month, day, hours, minutes, seconds);
        calendar.set(GregorianCalendar.MILLISECOND,milliseconds);
        Date date = calendar.getTime();
		setExitValue(dateAndTimeExit, context, flow, date);
	}
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}
	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		yearTrigger = getRequiredMandatoryTrigger(YEAR, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		monthTrigger = getRequiredMandatoryTrigger(MONTH, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		dayTrigger = getRequiredMandatoryTrigger(DAY, Boolean.FALSE, BuiltinModels.NUMBER_ID);
        hourTrigger = getNonRequiredTrigger(HOUR, Boolean.FALSE, BuiltinModels.NUMBER_ID);
        minuteTrigger = getNonRequiredTrigger(MINUTE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
        secondTrigger = getNonRequiredTrigger(SECOND, Boolean.FALSE, BuiltinModels.NUMBER_ID);
        millisecondTrigger = getNonRequiredTrigger(MILLISECOND, Boolean.FALSE, BuiltinModels.NUMBER_ID);

		dateAndTimeExit = getRequiredExit(DATE_AND_TIME, Boolean.FALSE, BuiltinModels.DATE_AND_TIME_ID);
	}
}

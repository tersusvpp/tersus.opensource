/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.dates;

import java.util.Date;

import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that checks whether one point in time is later than (or equal to) another
 * 
 * @author Youval Bronicki
 *
 */
public class LaterOrEqual extends Plugin
{
	private static final Role TIME1_ROLE = Role.get("<Time 1>");
	private static final Role TIME2_ROLE = Role.get("<Time 2>");
	private static final Role YES_ROLE = Role.get("<Yes>");
	private static final Role NO_ROLE = Role.get("<No>");

	SlotHandler trigger1, trigger2;
	SlotHandler yesExit, noExit;

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Date time1 = (Date) trigger1.get(context, flow);
		Date time2 = (Date) trigger2.get(context, flow);
		SlotHandler exit = (time1.getTime() >= time2.getTime()) ? yesExit : noExit;
		if (exit != null)
			setTimeExitValue(exit, context, flow, time1);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		trigger1 = getRequiredMandatoryTimeTrigger(TIME1_ROLE, Boolean.FALSE);
		trigger2 = getRequiredMandatoryTimeTrigger(TIME2_ROLE, Boolean.FALSE);

		yesExit = getNonRequiredTimeExit(YES_ROLE, Boolean.FALSE);
		noExit = getNonRequiredTimeExit(NO_ROLE, Boolean.FALSE);
	}
}

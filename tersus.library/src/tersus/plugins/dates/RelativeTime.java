/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.dates;

import java.util.Date;
import java.util.GregorianCalendar;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that calculates a point in time relative to a given base point in time.
 * 
 * @param Optional <Base Time>: A base point in time (if none, current date and time is used)
 * @param Optional <Seconds>: Number of seconds to add to the base point in time
 * 
 * @author Ofer Brandes
 *
 */
public class RelativeTime extends Plugin
{
	private static final Role BASE_TIME_ROLE = Role.get("<Base Time>");
	private static final Role MILLISECONDS_ROLE = Role.get("<Milliseconds>");
	private static final Role SECONDS_ROLE = Role.get("<Seconds>");
	private static final Role MINUTES_ROLE = Role.get("<Minutes>");
	private static final Role HOURS_ROLE = Role.get("<Hours>");
	private static final Role TIME_ROLE = Role.get("<Time>");
		
	SlotHandler baseTimeTrigger, millisecondsTrigger, secondsTrigger, minutesTrigger, hoursTrigger, timeExit;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtimle.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Date baseTime = (Date) getTriggerValue(context, flow, baseTimeTrigger);
		long time = baseTime != null ? baseTime.getTime() : System.currentTimeMillis();

		long hours = getTriggerValue(hoursTrigger, context, flow, 0);
		long minutes = getTriggerValue(minutesTrigger, context, flow, 0);
		long seconds = getTriggerValue(secondsTrigger, context, flow, 0);
		long milliseconds = getTriggerValue(millisecondsTrigger, context, flow, 0);
		long delta = milliseconds + (seconds + (minutes + (hours * 60)) * 60) * 1000;		

		setExitValue(timeExit, context, flow, new Date(time + delta));
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		baseTimeTrigger = getNonRequiredTrigger(BASE_TIME_ROLE, Boolean.FALSE, BuiltinModels.DATE_AND_TIME_ID);
		millisecondsTrigger = getNonRequiredTrigger(MILLISECONDS_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		secondsTrigger = getNonRequiredTrigger(SECONDS_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		minutesTrigger = getNonRequiredTrigger(MINUTES_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		hoursTrigger = getNonRequiredTrigger(HOURS_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		
		timeExit = getRequiredExit(TIME_ROLE, Boolean.FALSE, BuiltinModels.DATE_AND_TIME_ID);
	}
}

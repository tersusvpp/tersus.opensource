/************************************************************************************************
 * Copyright (c) 2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.dates;
import java.text.DateFormat;
import java.util.Date;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
/**
 * A plugin that formats a date/time value using a format string and a timezone
 * 
 * @author Youval Bronicki
 *
 */
public class FormatDateAndTime extends Plugin
{
	private static final Role DATE_AND_TIME = Role.get("<Date and Time>");
    private static final Role TIME_ZONE = Role.get("<Time Zone>");
    private static final Role TIME_ZONE_OFFSET = Role.get("<Time Zone Offset>");
    private static final Role FORMAT = Role.get("<Format>");
    private static final Role TEXT = Role.get("<Text>");
	private static final int NO_OFFSET = Integer.MIN_VALUE;

	SlotHandler dateTrigger, tzTrigger, tzOffsetTrigger, formatTrigger;
	SlotHandler textExit;
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Date dateTime = (Date) dateTrigger.get(context, flow);

        String formatStr = (String)formatTrigger.get(context, flow);
        DateFormat format = context.getDateFormat(formatStr);
        format.setTimeZone(TimeZone.getDefault());
        if (tzTrigger != null)
        {
            String timeZone = (String)tzTrigger.get(context, flow);
            if (timeZone != null)
                format.setTimeZone(TimeZone.getTimeZone(timeZone));
        }
        int tzOffset = getTriggerValue(tzOffsetTrigger, context, flow, NO_OFFSET);
        if (tzOffset != NO_OFFSET)
        {
        	String name;
        	if (tzOffset>0)
        		name = "GMT+"+tzOffset/60;
        	else if (tzOffset<0)
        		name = "GMT"+tzOffset/60;
        	else
        		name = "GMT";
        	format.setTimeZone( new SimpleTimeZone(tzOffset*60*1000,name));
        		
        }
        String text = format.format(dateTime);
        format.setTimeZone(TimeZone.getDefault()); // To be on the safe side ..
        setExitValue(textExit,  context, flow, text);
	}
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}
	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		dateTrigger = getRequiredMandatoryTimeTrigger(DATE_AND_TIME, Boolean.FALSE);  
        formatTrigger = getRequiredMandatoryTrigger(FORMAT, Boolean.FALSE, BuiltinModels.TEXT_ID);
        tzTrigger = getNonRequiredTrigger(TIME_ZONE, Boolean.FALSE, BuiltinModels.TEXT_ID);
        tzOffsetTrigger = getNonRequiredTrigger(TIME_ZONE_OFFSET, Boolean.FALSE, BuiltinModels.NUMBER_ID);
        textExit = getRequiredExit(TEXT, Boolean.FALSE, BuiltinModels.TEXT_ID);
	}
}

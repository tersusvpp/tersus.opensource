/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.dates;

import java.util.Date;
import java.util.GregorianCalendar;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.DateUtil;

/**
 * An atomic flow handler that returns the current date (the hour is 00:00:00)
 * 
 * @author Youval Bronicki
 *
 */
public class Today extends Plugin
{
	private static final Role OUTPUT_ROLE = Role.get("<Today>");
	SlotHandler exit;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		GregorianCalendar calendar = new GregorianCalendar(); //Current Date and Time
		//Override system time (used for regression tests)
		if (context.getDebugTime() != null)
			calendar.setTime(context.getDebugTime());		
		Date exitValue = DateUtil.getDate(calendar);
		setExitValue(exit, context, flow, exitValue);
	}


	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		exit = getRequiredExit(OUTPUT_ROLE, Boolean.FALSE, BuiltinModels.DATE_ID);
	}
}

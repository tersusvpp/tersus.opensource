/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.dates;

import java.util.Date;
import java.util.GregorianCalendar;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.DateUtil;
import tersus.util.InvalidInputException;

/**
 * An atomic flow handler that calculates the first and last days of a week.
 * 
 * @param "Year": The year (e.g.2004)
 * @param "Week Number": The number of the week in the year (usually 1-52 or 1-53, but "lenient" values are also acceptable)
 * @param "First Day": "Sunday" if weeks start on Sunday (this is an optional trigger - if missing, the default is Monday)
 * 
 * 19/1/2005: GetWeek, WeekDates and all Java Scripts dealing with dates are currently
 * using the ISO-8601 definitions as presented in http://www.tondering.dk/claus/calendar.html:
 * First day of the week is Monday, a week belongs to the year in which most of its
 * days lie.
 * 
 * @author Ofer Brandes
 *
 */
public class WeekDates extends Plugin
{
	private static final Role YEAR_ROLE = Role.get("<Year>");
	private static final Role WEEK_ROLE = Role.get("<Week Number>");
	private static final Role FIRST_DAY_ROLE = Role.get("<First Day>");
	private static final Role FIRST_DATE_ROLE = Role.get("<First Date>");
	private static final Role LAST_DATE_ROLE = Role.get("<Last Date>");
	private static final Role YEAR_EXIT_ROLE = Role.get("<<Year>>");
	private static final Role WEEK_EXIT_ROLE = Role.get("<<Week Number>>");
		
	SlotHandler yearTrigger, weekNumberTrigger, firstDayTrigger;
	SlotHandler firstDateExit, lastDateExit;
	SlotHandler yearExit, weekNumberExit;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtimle.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		int year = (int) ((Number) yearTrigger.get(context,flow)).value;
		int weekNumber = (int) ((Number) weekNumberTrigger.get(context,flow)).value;

		int firstDayOfWeek = DateUtil.FIRST_DAY_OF_WEEK;
		int lastDayOfWeek = DateUtil.LAST_DAY_OF_WEEK;
		if (firstDayTrigger != null) {
			String firstDay = (String) firstDayTrigger.get(context,flow);
			if ("Sunday".equals(firstDay)) {
				firstDayOfWeek = GregorianCalendar.SUNDAY;
				lastDayOfWeek = GregorianCalendar.SATURDAY;
			}
			else if (firstDay != null)
				throw new InvalidInputException("First Day of Week is Monday unless Sunday specified");
		}
		
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setLenient(true);
		calendar.setFirstDayOfWeek(firstDayOfWeek);
		calendar.setMinimalDaysInFirstWeek(DateUtil.MINIMAL_DAYS_IN_FIRST_WEEK);

		calendar.set(GregorianCalendar.YEAR,year);
		calendar.set(GregorianCalendar.WEEK_OF_YEAR,weekNumber);
		calendar.set(GregorianCalendar.DAY_OF_WEEK,firstDayOfWeek);
		Date firstDate = DateUtil.getDate(calendar);

		calendar.set(GregorianCalendar.YEAR,year);
		calendar.set(GregorianCalendar.WEEK_OF_YEAR,weekNumber);		
		calendar.set(GregorianCalendar.DAY_OF_WEEK,lastDayOfWeek);
		Date lastDate = DateUtil.getDate(calendar);
		
		if (firstDateExit != null)
			setExitValue(firstDateExit, context, flow, firstDate);

		if (lastDateExit != null)
			setExitValue(lastDateExit, context, flow, lastDate);
		
		if (yearExit != null) {
			weekNumber = calendar.get(GregorianCalendar.WEEK_OF_YEAR);
			int month = calendar.get(GregorianCalendar.MONTH);
			year = calendar.get(GregorianCalendar.YEAR);
			if (weekNumber == 1  && month == GregorianCalendar.DECEMBER)
				year++;
			else
			if (weekNumber >= 52  && month == GregorianCalendar.JANUARY)
				year--;
			setExitValue(yearExit, context, flow, new Number(year));
		}

		if (weekNumberExit != null) {
			weekNumber = calendar.get(GregorianCalendar.WEEK_OF_YEAR);
			setExitValue(weekNumberExit, context, flow, new Number(weekNumber));
		}
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		
		yearTrigger = getRequiredMandatoryTrigger(YEAR_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		weekNumberTrigger = getRequiredMandatoryTrigger(WEEK_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		firstDayTrigger = getNonRequiredTrigger(FIRST_DAY_ROLE, Boolean.FALSE, BuiltinModels.TEXT_ID);
		
		firstDateExit = getNonRequiredExit(FIRST_DATE_ROLE, Boolean.FALSE, BuiltinModels.DATE_ID);
		lastDateExit = getNonRequiredExit(LAST_DATE_ROLE, Boolean.FALSE, BuiltinModels.DATE_ID);
		yearExit = getNonRequiredExit(YEAR_EXIT_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		weekNumberExit = getNonRequiredExit(WEEK_EXIT_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
	}
}

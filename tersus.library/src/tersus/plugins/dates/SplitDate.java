/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.dates;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
/**
 * An atomic flow handler that splits a date into year, month and day numbers
 * Year numbers are full years (e.g. 1999, 2004)
 * Month numbers are 1-12
 * Day numbers are 1-31
 * 
 * @author Youval Bronicki
 *
 */
public class SplitDate extends Plugin
{
	private static final Role DATE_ROLE = Role.get("<Date>");
	private static final Role YEAR_ROLE = Role.get("<Year>");
	private static final Role MONTH_ROLE = Role.get("<Month>");
	private static final Role DAY_ROLE = Role.get("<Day>");
    private static final Role DAY_OF_WEEK_ROLE = Role.get("<Day of Week>");

	SlotHandler dateTrigger;
	SlotHandler yearExit, monthExit, dayExit, dayOfWeekExit;
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Date date = (Date) dateTrigger.get(context, flow);

		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);
        int dayOfOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		if (yearExit != null)
			setExitValue(yearExit, context, flow, new Number(year));
		if (monthExit != null)
			setExitValue(monthExit, context, flow, new Number(month));
		if (dayExit != null)
			setExitValue(dayExit, context, flow, new Number(day));
        if (dayOfWeekExit != null)
            setExitValue(dayOfWeekExit, context, flow, new Number(dayOfOfWeek));
	}
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}
	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		dateTrigger = getRequiredMandatoryTimeTrigger(DATE_ROLE, Boolean.FALSE);

		yearExit = getNonRequiredExit(YEAR_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		monthExit = getNonRequiredExit(MONTH_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		dayExit = getNonRequiredExit(DAY_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
        dayOfWeekExit = getNonRequiredExit(DAY_OF_WEEK_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
	}
}

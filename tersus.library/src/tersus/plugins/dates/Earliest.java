/************************************************************************************************
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.dates;

import java.util.Date;
import java.util.List;

import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that returns the earliest of all point in time input values
 * 
 * @author Youval Bronicki
 *
 */
public class Earliest extends Plugin
{
	private static final Role TIMES_ROLE = Role.get("<Times>");
	private static final Role EARLIEST_ROLE = Role.get("<Earliest>");

	SlotHandler trigger, exit;
	
	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Date earliest = null;
		List inputs = (List) trigger.get(context, flow);
		if (inputs.size() > 0) {
			for (int i=0; i<inputs.size(); i++)
			{
				Date value = (Date) inputs.get(i);
				if (earliest == null || earliest.after(value))
					earliest = value;
			}
			
			setTimeExitValue(exit, context, flow, earliest);
		}
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		trigger = getRequiredMandatoryTimeTrigger(TIMES_ROLE, Boolean.TRUE);
		exit = getRequiredTimeExit(EARLIEST_ROLE, Boolean.FALSE);
	}
}

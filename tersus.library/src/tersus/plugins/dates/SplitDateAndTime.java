/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.dates;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

import tersus.model.BuiltinModels;
import tersus.model.BuiltinProperties;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * An atomic flow handler that splits a date and time value into year, month and day, hour, minute,
 * second and millisecond components Year numbers are full years (e.g. 1999, 2004) Month numbers are
 * 1-12 Day numbers are 1-31
 * 
 * @author Youval Bronicki
 * 
 */
public class SplitDateAndTime extends Plugin
{
	private static final Role DATE_AND_TIME = Role.get("<Date and Time>");
	private static final Role YEAR = Role.get("<Year>");
	private static final Role MONTH = Role.get("<Month>");
	private static final Role DAY = Role.get("<Day>");
	private static final Role HOUR = Role.get("<Hour>");
	private static final Role MINUTE = Role.get("<Minute>");
	private static final Role SECOND = Role.get("<Second>");
	private static final Role MILLISECOND = Role.get("<Millisecond>");
	private static final Role TIMEZONE_OFFSELT = Role.get("<Time Zone Offset>");
	private static final int NO_OFFSET = Integer.MIN_VALUE;

	SlotHandler dateTrigger, tzOffsetTrigger;
	SlotHandler yearExit, monthExit, dayExit;
	SlotHandler hourExit, minuteExit, secondExit, millisecondExit;

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Date date = (Date) dateTrigger.get(context, flow);

		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		int tzOffset = getTriggerValue(tzOffsetTrigger, context, flow, NO_OFFSET);
		if (tzOffset != NO_OFFSET)
		{
			String name;
			if (tzOffset > 0)
				name = "GMT+" + tzOffset + "M";
			else if (tzOffset < 0)
				name = "GMT" + tzOffset + "M";
			else
				name = "GMT";
			calendar.setTimeZone(new SimpleTimeZone(tzOffset * 60 * 1000, name));
		}
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		int second = calendar.get(Calendar.SECOND);
		int millisecond = calendar.get(Calendar.MILLISECOND);
		if (yearExit != null)
			setExitValue(yearExit, context, flow, new Number(year));
		if (monthExit != null)
			setExitValue(monthExit, context, flow, new Number(month));
		if (dayExit != null)
			setExitValue(dayExit, context, flow, new Number(day));
		if (hourExit != null)
			setExitValue(hourExit, context, flow, new Number(hour));
		if (minuteExit != null)
			setExitValue(minuteExit, context, flow, new Number(minute));
		if (secondExit != null)
			setExitValue(secondExit, context, flow, new Number(second));
		if (millisecondExit != null)
			setExitValue(millisecondExit, context, flow, new Number(millisecond));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		dateTrigger = getRequiredMandatoryTimeTrigger(DATE_AND_TIME, Boolean.FALSE);
		tzOffsetTrigger = getNonRequiredTrigger(TIMEZONE_OFFSELT, Boolean.FALSE,
				BuiltinModels.NUMBER_ID);
		yearExit = getNonRequiredExit(YEAR, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		monthExit = getNonRequiredExit(MONTH, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		dayExit = getNonRequiredExit(DAY, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		hourExit = getNonRequiredExit(HOUR, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		minuteExit = getNonRequiredExit(MINUTE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		secondExit = getNonRequiredExit(SECOND, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		millisecondExit = getNonRequiredExit(MILLISECOND, Boolean.FALSE, BuiltinModels.NUMBER_ID);

	}
}

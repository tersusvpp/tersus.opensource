/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.dates;
import java.util.Date;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
/**
 * A plugin for calculating the difference between 2 points in time
 * (calculates the difference from <From> to <To>)
 * return the result in any of the following units:
 * <Days>
 * <Hours>
 * <Minutes>
 * <Seconds>
 * <Milliseconds>
 * 
 * @author Youval Bronicki
 *
 */
public class TimeDifference extends Plugin
{
    private static final Role FROM = Role.get("<From>");
    private static final Role TO = Role.get("<To>");
	private static final Role DAYS = Role.get("<Days>");
    private static final Role HOURS = Role.get("<Hours>");
    private static final Role MINUTES = Role.get("<Minutes>");
    private static final Role SECONDS = Role.get("<Seconds>");
    private static final Role MILLISECONDS = Role.get("<Milliseconds>");

	SlotHandler fromTrigger, toTrigger;
	SlotHandler daysExit, hoursExit, minutesExit, secondsExit, millisecondsExit;
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Date from = (Date) fromTrigger.get(context, flow);
        Date to = (Date) toTrigger.get(context, flow);

        double differenceMilliseconds = to.getTime() - from.getTime();
        double differenceSeconds = differenceMilliseconds/1000.0;
        double differneceMinutes = differenceSeconds/60.0;
        double differenceHours = differneceMinutes/60.0;
        double differenceDays = differenceHours/24.0;
        
        setExitValue(millisecondsExit, context, flow, new Number(differenceMilliseconds));
        setExitValue(secondsExit, context, flow, new Number(differenceSeconds));
        setExitValue(minutesExit, context, flow, new Number(differneceMinutes));
        setExitValue(hoursExit, context, flow, new Number(differenceHours));
        setExitValue(daysExit, context, flow, new Number(differenceDays));
	}
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		fromTrigger = getRequiredMandatoryTimeTrigger(FROM, Boolean.FALSE);
        toTrigger = getRequiredMandatoryTimeTrigger(TO, Boolean.FALSE);

		daysExit = getNonRequiredExit(DAYS, Boolean.FALSE, BuiltinModels.NUMBER_ID);
        hoursExit = getNonRequiredExit(HOURS, Boolean.FALSE, BuiltinModels.NUMBER_ID);
        minutesExit = getNonRequiredExit(MINUTES, Boolean.FALSE, BuiltinModels.NUMBER_ID);
        secondsExit = getNonRequiredExit(SECONDS, Boolean.FALSE, BuiltinModels.NUMBER_ID);
        millisecondsExit = getNonRequiredExit(MILLISECONDS, Boolean.FALSE, BuiltinModels.NUMBER_ID);
	}
}

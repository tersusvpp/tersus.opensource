/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.dates;
import java.util.Date;
import java.util.GregorianCalendar;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.DateUtil;
/**
 * An atomic flow handler that creates a date from year, month and day numbers
 * Year numbers are full years (e.g. 1999, 2004)
 * Month numbers are 1-12
 * Day numbers are 1-31
 * 
 * @author Youval Bronicki
 *
 */
public class ConstructDate extends Plugin
{
	private static final Role YEAR_ROLE = Role.get("<Year>");
	private static final Role MONTH_ROLE = Role.get("<Month>");
	private static final Role DAY_ROLE = Role.get("<Day>");
	private static final Role DATE_ROLE = Role.get("<Date>");

	SlotHandler dateExit;
	SlotHandler yearTrigger, monthTrigger, dayTrigger;
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Number yearObj = (Number) yearTrigger.get(context, flow);
		Number monthObj = (Number) monthTrigger.get(context, flow);
		Number dayObj = (Number) dayTrigger.get(context, flow);

		int year = (int) yearObj.value;
		int month = (int) (monthObj.value - 1);
		int day = (int) dayObj.value;
		GregorianCalendar calendar = new GregorianCalendar(year, month, day);
		Date date = DateUtil.getDate(calendar);
		setExitValue(dateExit, context, flow, date);
	}
	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}
	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		yearTrigger = getRequiredMandatoryTrigger(YEAR_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		monthTrigger = getRequiredMandatoryTrigger(MONTH_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		dayTrigger = getRequiredMandatoryTrigger(DAY_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);

		dateExit = getRequiredExit(DATE_ROLE, Boolean.FALSE, BuiltinModels.DATE_ID);
	}
}

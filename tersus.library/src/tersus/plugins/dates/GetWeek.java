/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.dates;

import java.util.Date;
import java.util.GregorianCalendar;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.DateUtil;
import tersus.util.InvalidInputException;

/**
 * An atomic flow handler that returns the year (e.g. 2005) and the week number
 * in the year (1-52 or 1-53) of a given date.
 * 
 * 19/1/2005: GetWeek, WeekDates and all Java Scripts dealing with dates are currently
 * using the ISO-8601 definitions as presented in http://www.tondering.dk/claus/calendar.html:
 * First day of the week is Monday, a week belongs to the year in which most of its
 * days lie.
 * 
 * Notice that the first week of a year may start in December of the previous year.
 * E.g. the 1st week of 2004 is 29/12/2003 through 4/1/2004.
 * 
 * @author Ofer Brandes
 *
 */
public class GetWeek extends Plugin
{
	private static final Role DATE_ROLE = Role.get("<Date>");
    private static final Role FIRST_DAY_ROLE = Role.get("<First Day>");
	private static final Role WEEK_ROLE = Role.get("<Week Number>");
	private static final Role YEAR_ROLE = Role.get("<Year>");

	SlotHandler trigger;
	SlotHandler weekExit, yearExit;
    private SlotHandler firstDayTrigger;

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Date date = (Date) trigger.get(context, flow);
        int firstDayOfWeek = DateUtil.FIRST_DAY_OF_WEEK;
        if (firstDayTrigger != null) {
            String firstDay = (String) firstDayTrigger.get(context,flow);
            if ("Sunday".equals(firstDay)) {
                firstDayOfWeek = GregorianCalendar.SUNDAY;
            }
            else if (firstDay != null)
                throw new InvalidInputException("First Day of Week is Monday unless Sunday specified");
        }
        
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setLenient(true);
        calendar.setFirstDayOfWeek(firstDayOfWeek);
		calendar.setMinimalDaysInFirstWeek(DateUtil.MINIMAL_DAYS_IN_FIRST_WEEK);
		
		calendar.setTime(date);

		int weekNumber = calendar.get(GregorianCalendar.WEEK_OF_YEAR);
								
		if (weekExit != null)
			setExitValue(weekExit, context, flow, new Number(weekNumber));

		if (yearExit != null) {
			int month = calendar.get(GregorianCalendar.MONTH);
			int year = calendar.get(GregorianCalendar.YEAR);
			if (weekNumber == 1  && month == GregorianCalendar.DECEMBER)
				year++;
			else
			if (weekNumber >= 52  && month == GregorianCalendar.JANUARY)
				year--;
			setExitValue(yearExit, context, flow, new Number(year));
		}
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IFlowHandler#resume(tersus.runtime.FlowState)
	 */
	public void resume(RuntimeContext context, FlowInstance flow)
	{
		// Nothing to do (This is an action)
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		trigger = getRequiredMandatoryTimeTrigger(DATE_ROLE, Boolean.FALSE);
        firstDayTrigger = getNonRequiredTrigger(FIRST_DAY_ROLE, Boolean.FALSE, BuiltinModels.TEXT_ID);

		weekExit = getNonRequiredExit(WEEK_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
		yearExit = getNonRequiredExit(YEAR_ROLE, Boolean.FALSE, BuiltinModels.NUMBER_ID);
	}
}

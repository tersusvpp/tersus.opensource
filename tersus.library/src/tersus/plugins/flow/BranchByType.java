package tersus.plugins.flow;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

public class BranchByType extends Plugin
{
    private static  Role INPUT = Role.get("<Input>");
    private static  Role OTHER = Role.get("<Other>");
    
    private SlotHandler inputTrigger, otherExit;
    @Override
    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        inputTrigger = getRequiredMandatoryTrigger(INPUT, Boolean.FALSE, BuiltinModels.ANYTHING_ID);
        otherExit = getNonRequiredExit(OTHER, Boolean.FALSE, BuiltinModels.ANYTHING_ID);
    }
    @Override
    public void start(RuntimeContext context, FlowInstance flow)
    {
        Object value = getTriggerValue(context, flow, inputTrigger);
        for (SlotHandler exit: exits)
        {
            if (exit != otherExit)
            {
                if (exit.getChildInstanceHandler().isInstance(value))
                {
                    setExitValue(exit, context, flow, value);
                    return;
                }
            }
        }
        if (otherExit != null)
            setExitValue(otherExit, context, flow, value);
    }
    
    
    
    

}

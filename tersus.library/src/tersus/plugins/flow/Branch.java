/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.plugins.flow;

import org.apache.commons.lang3.StringEscapeUtils;

import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.CompositeFlowHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.NotImplementedException;

/**
 * 
 * An atomic flow handler that branches according to the content of an input value.
 * 
 * New Inteface, 21/7/05
 * ~~~~~~~~~~~~~~~~~~~~~
 * <Selector> (mandatory) - A branching value, which is compared to
 * the 'value' attribute of each exit (or to the exit's role if 'value' is missing).
 * <Data> (optional) - A value that is exposed through the matching exit
 * (or through the <Other> exit is none matches). If <Data> is missing, the value
 * of <Selector> is used instead.
 * 
 * Old Interface till 21/7/05 (still supported)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * <Value> serving as both <Selector> and <Data> - The input value is compared to
 * the 'value' attribute of each exit (or to the exit's role if 'value' is missing). 
 * 
 * @author Youval Bronicki
 *
 */
public class Branch extends Plugin
{
	private static final Role OLD_TRIGGER = Role.get("<Value>");
	private static final Role SELECTOR_TRIGGER = Role.get("<Selector>");
	private static final Role DATA_TRIGGER = Role.get("<Data>");
	private static final Role DAFAULT_EXIT = Role.get("<Other>");

	private SlotHandler selectorTrigger, dataTrigger;
	private SlotHandler defaultExit;
	private static final String VALUE_PROPERTY = "value";

	/* (non-Javadoc)
	 * @see tersus.runtime.FlowHandler#start(tersus.runtime.RuntimeContext, tersus.runtime.FlowInstance)
	 */
	public void start(RuntimeContext context, FlowInstance flow)
	{
		Object selectionValue = selectorTrigger.get(context,flow);
		
		Object dataValue = (dataTrigger != null ? dataTrigger.get(context,flow) : selectionValue);

		InstanceHandler childHandler = selectorTrigger.getChildInstanceHandler();
		if (childHandler instanceof CompositeFlowHandler)
			throw new NotImplementedException("Branching by composite type not yet implemented");

		for (int i = 0; i < exits.length; i++)
		{
			SlotHandler exit = exits[i];
			if (exit == defaultExit)
				continue;

			String valueStr = (String) exit.getProperty(VALUE_PROPERTY);
			if (valueStr == null)
				valueStr = exit.getRole().toString();
			if (getPluginVersion() >=1)
				valueStr = StringEscapeUtils.unescapeEcmaScript(valueStr);
			
			Object branchingValue = childHandler.newInstance(context,valueStr, null, false);
			if (selectionValue.equals(branchingValue))
			{
				setExitValue(exit, context, flow, dataValue);
				return;
			}
		}
		if (defaultExit != null)
			setExitValue(defaultExit, context, flow, dataValue);
	}

	/* (non-Javadoc)
	 * @see tersus.runtime.IInstanceHandler#intializeFromModel(tersus.model.Model)
	 */
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		selectorTrigger = getNonRequiredMandatoryTrigger(SELECTOR_TRIGGER, Boolean.FALSE, null);
		if (selectorTrigger != null)
			dataTrigger = getNonRequiredMandatoryTrigger(DATA_TRIGGER, Boolean.FALSE, null);
		else
			selectorTrigger = getRequiredMandatoryTrigger(OLD_TRIGGER, Boolean.FALSE, null);

		defaultExit = getNonRequiredExit(DAFAULT_EXIT, Boolean.FALSE, null);

		if ((dataTrigger == null ? selectorTrigger : dataTrigger).getChildModelId() != null)
		{
			// Temporarily allowing for exits without type (Ofer, 29/11/05)
			for (int i = 0; i < exits.length; i++)
				if (exits[i].getChildModelId() != null)
					checkTypeMismatch((dataTrigger == null ? selectorTrigger : dataTrigger), exits[i]);
		}
		else
		{
			// This is the correct validation
			for (int i = 0; i < exits.length; i++)
				checkTypeMismatch((dataTrigger == null ? selectorTrigger : dataTrigger), exits[i]);
		}
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.mail;

import java.util.List;

import javax.mail.internet.InternetAddress;

import tersus.runtime.FlowInstance;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * @author Youval Bronicki
 *
 */
abstract class MailPlugin extends Plugin
{

    protected InternetAddress[] getInternetAddresses(RuntimeContext context, FlowInstance flow, SlotHandler trigger)
    {
        InternetAddress[] addresses = EMPTY;
        if (trigger != null)
        {
            if (trigger.isRepetitive())
            {
                List addressStrings = (List) trigger.get(context, flow);
                addresses = new InternetAddress[addressStrings.size()];
    
                for (int i = 0; i < addressStrings.size(); i++)
                {
                    String addressStr = (String) addressStrings.get(i);
                    addresses[i] = MailUtilities.getInternetAddress(addressStr);
                }
            }
            else
            {
                String addressString = (String) trigger.get(context, flow);
                if (addressString != null)
                    addresses = new InternetAddress[] { MailUtilities.getInternetAddress(
                            addressString) };
            }
        }
        return addresses;
    }

    static InternetAddress[] EMPTY = new InternetAddress[0];

}

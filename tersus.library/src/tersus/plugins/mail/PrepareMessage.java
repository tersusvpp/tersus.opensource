/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.mail;

import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.BinaryValue;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * A plugin for generating an email message (in MIME format)
 * 
* 
 * @author Youval Bronicki
 *  
 */
public class PrepareMessage extends MailPlugin
{

    //Exit: the prepared message

    private static final Role MESSAGE = Role.get("<Message>");

    // Triggers: addresses and content
    
    private static final Role FROM = Role.get("<From>");

    private static final Role TO = Role.get("<To>");

    private static final Role REPLY_TO = Role.get("<Reply To>");

    private static final Role CC = Role.get("<Cc>");

    private static final Role BCC = Role.get("<Bcc>");

    private static final Role BODY = Role.get("<Body>");

    private static final Role HTML_BODY = Role.get("<HTML Body>");

    private static final Role SUBJECT = Role.get("<Subject>");

    private static final Role ATTACHMENTS = Role.get("<Attachments>");

//    private static final Role SERVER = Role.get("<Server>");

    private SlotHandler messageExit;

    private SlotHandler fromTrigger;

    private SlotHandler toTrigger;

    private SlotHandler ccTrigger;

    private SlotHandler bccTrigger;

    private SlotHandler replyToTrigger;

    private SlotHandler subjectTrigger;

    private SlotHandler bodyTrigger;

    private SlotHandler htmlBodyTrigger;

    private SlotHandler attachmentsTrigger;

    private ElementHandler contentElement;

    private ElementHandler contentTypeElement;

    private ElementHandler filenameElement;

//    private static final String USER_PREFIX = "user=";
//
//    private static final String PASSWORD_PREFIX = "password=";
//
//    private static final String PORT_PERFIX = "port=";

    /*
     * (non-Javadoc)
     * 
     * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
     */

    public void start(RuntimeContext context, FlowInstance flow)
    {

        MimeMessage mimeMessage;
        try
        {
            mimeMessage = createMessage(context, flow, Session
                    .getDefaultInstance(new Properties()));
        }
        catch (MessagingException e)
        {
            throw new RuntimeException("Failed to create email message", e);
        }

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        String messageText;
        try
        {
            mimeMessage.writeTo(os);
            messageText = new String(os.toByteArray(), MailUtilities.ENCODING);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e); // We don't expect exceptions here
        }
        setExitValue(messageExit, context, flow, messageText);

    }

    private MimeMessage createMessage(RuntimeContext context,
            FlowInstance flow, Session session) throws MessagingException
    {
        MimeMessage mimeMessage = new MimeMessage(session);
        String sender =  getTriggerText(context, flow, fromTrigger);
        String subject = getTriggerText(context, flow, subjectTrigger);
        String body = getTriggerText(context, flow, bodyTrigger);
        String htmlBody = getTriggerText(context, flow, htmlBodyTrigger);
        List attachments = Collections.EMPTY_LIST;
        if (attachmentsTrigger != null)
            attachments = (List) attachmentsTrigger.get(context, flow);
        mimeMessage.setFrom(MailUtilities.getInternetAddress(sender));
        mimeMessage.setRecipients(Message.RecipientType.TO,
                getInternetAddresses(context, flow, toTrigger));
        mimeMessage.setRecipients(Message.RecipientType.CC,
                getInternetAddresses(context, flow, ccTrigger));
        mimeMessage.setRecipients(Message.RecipientType.BCC,
                getInternetAddresses(context, flow, bccTrigger));
        mimeMessage.setReplyTo(getInternetAddresses(context, flow,
                replyToTrigger));
        mimeMessage.setSubject(subject);
        mimeMessage.setSentDate(new Date());
        if (attachments.size() == 0 && htmlBody == null)
        {
            if (body != null)
            {
                mimeMessage.setText(body, MailUtilities.ENCODING);
            }
        }
        else
        {
            MimeMultipart content = new MimeMultipart();
            if (body != null)
            {
                MimeBodyPart part = new MimeBodyPart();
                part.setDisposition(javax.mail.Part.INLINE);
                part.setText(body, MailUtilities.ENCODING);
                content.addBodyPart(part);

            }
            if (htmlBody != null)
            {
                MimeBodyPart part = new MimeBodyPart();
                part.setDisposition(javax.mail.Part.INLINE);
                part.setText(htmlBody, MailUtilities.ENCODING, "html");
                content.addBodyPart(part);
            }
            for (Iterator i = attachments.iterator(); i.hasNext();)
            {
                Object file = i.next();
                String filename = (String) filenameElement.get(context, file);
                BinaryValue fileContent = (BinaryValue) contentElement.get(context, file);
                String contentType = (String) contentTypeElement.get(context,
                        file);
                if (fileContent == null)
                    throw new ModelExecutionException("Missing content for file '"+filename+"'");
                byte[] bytes = fileContent.toByteArray();
                MimeBodyPart part = new MimeBodyPart();
                part.setDataHandler(new javax.activation.DataHandler(
                        new ByteArrayDataSource(bytes, contentType)));
                if (filename != null)
                    part.setFileName(filename);
                if (attachments.size() == 1 && body == null && htmlBody == null)
                    part.setDisposition(javax.mail.Part.INLINE);
                else
                    part.setDisposition(javax.mail.Part.ATTACHMENT);
                content.addBodyPart(part);
            }
            mimeMessage.setContent(content);
        }
        return mimeMessage;
    }

    public void initializeFromModel(Model model)
    {
        super.initializeFromModel(model);
        messageExit = getRequiredExit(MESSAGE, Boolean.FALSE, BuiltinModels.TEXT_ID);
        fromTrigger = getRequiredMandatoryTrigger(FROM, Boolean.FALSE,
                BuiltinModels.TEXT_ID);
        toTrigger = getNonRequiredTrigger(TO, null, BuiltinModels.TEXT_ID);
        ccTrigger = getNonRequiredTrigger(CC, null, BuiltinModels.TEXT_ID);
        bccTrigger = getNonRequiredTrigger(BCC, null, BuiltinModels.TEXT_ID);
        replyToTrigger = getNonRequiredTrigger(REPLY_TO, null,
                BuiltinModels.TEXT_ID);
        subjectTrigger = getNonRequiredTrigger(SUBJECT, null,
                BuiltinModels.TEXT_ID);
        bodyTrigger = getNonRequiredTrigger(BODY, null, BuiltinModels.TEXT_ID);
        htmlBodyTrigger = getNonRequiredTrigger(HTML_BODY, null, BuiltinModels.TEXT_ID);
        attachmentsTrigger = getNonRequiredTrigger(ATTACHMENTS, null,
                BuiltinModels.FILE_ID);

        if (attachmentsTrigger != null)
        {
            InstanceHandler fileHandler = attachmentsTrigger
                    .getChildInstanceHandler();
            contentElement = fileHandler
                    .getElementHandler(BuiltinModels.CONTENT_ROLE);
            contentTypeElement = fileHandler
                    .getElementHandler(BuiltinModels.CONTENT_TYPE_ROLE);
            filenameElement = fileHandler
                    .getElementHandler(BuiltinModels.FILENAME_ROLE);
        }

    }

}
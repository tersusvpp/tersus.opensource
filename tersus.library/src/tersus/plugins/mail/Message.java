/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.mail;

import tersus.model.ModelId;
import tersus.model.Role;

/**
 * @author Liat Shiff
 */
public class Message
{
	public static final ModelId MESSAGE_ID = new ModelId("Common/Templates/Mail/Message");
    
	public static final Role MESSAGE_NUMBER = Role.get("Message Number");
	public static final Role FROM = Role.get("From");
    public static final Role TO = Role.get("To");
    public static final Role CC = Role.get("Cc");
    public static final Role BCC = Role.get("Bcc");
    public static final Role SUBJECT = Role.get("Subject");
    public static final Role REPLY_TO = Role.get("Reply to");
    public static final Role SENT_DATE = Role.get("Sent Date");
    public static final Role RECEIVED_DATE = Role.get("Received Date");
    public static final Role CONTENT = Role.get("Content");
    public static final Role ATTACHMENTS = Role.get("Attachments");
}

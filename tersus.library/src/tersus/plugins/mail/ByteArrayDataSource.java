/*
 * Copyright (c) 2004 Tersus Software Ltd.
 * 
 * Created on Jan 24, 2004
 *
 */
package tersus.plugins.mail;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.activation.DataSource;

import tersus.util.NotImplementedException;

/**
 * A data source for data that exists in a byte array
 * 
 * @author Youval Bronicki
 *
 */
public class ByteArrayDataSource implements DataSource
{

	private byte[] data;
	private String contentType;
	
	
	public ByteArrayDataSource(byte[] data, String contentType)
	{
		if (data == null)
			throw new IllegalArgumentException("data must not be null");
		if (contentType == null)
			throw new IllegalArgumentException("contentType must not be null");
			
		this.data = data;
		this.contentType = contentType;
	}
	/* (non-Javadoc)
	 * @see javax.activation.DataSource#getInputStream()
	 */
	public InputStream getInputStream() throws IOException
	{
		return new ByteArrayInputStream(data);
	}

	/* (non-Javadoc)
	 * @see javax.activation.DataSource#getOutputStream()
	 */
	public OutputStream getOutputStream() throws IOException
	{
		throw new NotImplementedException();
	}

	/* (non-Javadoc)
	 * @see javax.activation.DataSource#getContentType()
	 */
	public String getContentType()
	{
		return contentType;
	}

	/* (non-Javadoc)
	 * @see javax.activation.DataSource#getName()
	 */
	public String getName()
	{
		return "NoName";
	}

}

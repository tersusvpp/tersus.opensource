/************************************************************************************************
 * Copyright (c) 2003-2006 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.mail;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.URLName;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.ParseException;

import org.xbill.DNS.Lookup;
import org.xbill.DNS.MXRecord;
import org.xbill.DNS.Record;
import org.xbill.DNS.TextParseException;
import org.xbill.DNS.Type;

import tersus.runtime.EngineException;

/**
 * @author Youval Bronicki
 *  
 */
public class MailUtilities
{

    public static final String ENCODING = "UTF-8";
	
    //Forward the message using the session's mail server
    public static List<Status> forward(Transport transport, MimeMessage message, Address[] recipients)
    {

        List<Status> status = send(transport, message, recipients);
        return status;

    }


    /**
     * Deliver a message to given addresses by directly contacting the target mail servers
     */
    public static List<Status> deliver(Session session,
            MimeMessage message, Address[] recipients) 
    {
        List<Status> status = new ArrayList<Status>();
        Map<String, Set<InternetAddress>> groups = groupByDomains(recipients, status);
        for (String domain: groups.keySet())
        {
            Set<InternetAddress> addressCollection = groups.get(domain);
            Address[] addresses = (Address[]) addressCollection
                    .toArray(new Address[addressCollection.size()]);
            status.addAll(deliver(session, message, domain, addresses));
        }
        return status;
    }

    private static List<Status> createStatus(String type,
            Address[] addresses, MessagingException ex)
    {
        List<Status> errors = new ArrayList<Status>(addresses.length);
        for (int i = 0; i < addresses.length; i++)
            errors.add(new Status(type, addresses[i], ex));
        return errors;
    }

    /**
     * Deliver a message to a set of addresses belonging to a given domain
     */

    private static List<Status> deliver(Session session, MimeMessage message,
            String domain, Address[] addresses)
    {
        try
        {
            String[] servers;
            try
            {
                servers = getMailServers(domain);
            }
            catch (MessagingException e)
            {
                return createStatus(Status.UNSENT, addresses,e);
            }
            if (servers.length == 0)
                return createStatus(Status.INVALID, addresses, new MessagingException(
                        "Invalid address: no mail servers found for '" + domain + "'"));
            MessagingException lastException = null;
            String server = null;
            Transport transport = null;
            for (int i = 0; i < servers.length; i++)
            {
                server = servers[i];
                URLName serverURL = new URLName("smtp://" + server);
                try
                {
                    transport = session.getTransport(serverURL);
                }
                catch (NoSuchProviderException e)
                {
                    throw new RuntimeException("Failed to send Email",e);
                }
                try
                {
                    transport.connect();
                    break;
                }
                catch (MessagingException e)
                {
                    lastException = e;
                    transport = null;
                }
            }
            if (transport == null)
                return createStatus(Status.UNSENT,
                        addresses, new MessagingException(
                        "Failed to connect to any mail server (last error included)",
                        lastException));
            else
                
                try
            {
                    return  send(transport, message, addresses);
            }
            finally
            {
                try
                {
                    transport.close();
                }
                catch (MessagingException e)
                {
                    throw new RuntimeException("Error delivering email",e);
                }
            }
        }
        finally
        {
        }

    }

    private static List<Status> send(Transport transport, MimeMessage message,
            Address[] addresses)
    {
        List<Status> status = new ArrayList<Status>();
        try
        {
            transport.sendMessage(message, addresses);
            status.addAll(createStatus(
                    Status.SENT,
                    addresses, new MessagingException("Message sent to "+transport.getURLName().getHost())));
        }
        catch (SendFailedException e)
        {
            MessagingException reason = e.getNextException() instanceof MessagingException ? (MessagingException)e.getNextException() : null;
            Address[] invalidAddresses = e.getInvalidAddresses();
            if (invalidAddresses != null)
                status.addAll(createStatus(
                        Status.INVALID,
                        invalidAddresses, reason));
            Address[] validAddresses = e.getValidUnsentAddresses();
            if (validAddresses != null)
                status.addAll(createStatus(
                        Status.UNSENT,
                        validAddresses, e));
            Address[] sentAddresses = e.getValidSentAddresses();
            if (sentAddresses != null)
                status.addAll(createStatus(
                        Status.SENT,
                        sentAddresses, new MessagingException("Message delivered to "+transport.getURLName().getHost())));
            
           

        }
        catch (MessagingException e)
        {
            MessagingException reason = e.getNextException() instanceof MessagingException ? (MessagingException)e.getNextException() : e;
            status.addAll(createStatus(
                    Status.UNSENT,
                    addresses, reason));
        }
        return status;
    }

    /**
     * @param host
     * @return
     */
    private static String[] getMailServers(String host)
            throws MessagingException
    {
        Record[] records;
        try
        {
            records = new Lookup(host, Type.MX).run();
        }
        catch (TextParseException e)
        {
            throw new MessagingException("Failed to lookup mail servers for '"
                    + host + "'", e);
        }
        if (records == null)
            return new String[0];
        String[] servers = new String[records.length];
        for (int i = 0; i < records.length; i++)
        {
            MXRecord mx = (MXRecord) records[i];
            String target = mx.getTarget().toString();
            servers[i] = target.substring(0, target.length() - 1);
        }
        return servers;
    }

    /**
     * Groups email addresses by domain. Errors in extracting the host part of an email address
     * are added to the given 'errors' list.
     * @param recipients
     * @return
     */
    private static Map<String, Set<InternetAddress>> groupByDomains(Address[] recipients, List<Status> errors)
    {
        Map<String, Set<InternetAddress>> groups = new HashMap<String, Set<InternetAddress>>();
        for (int i = 0; i < recipients.length; i++)
        {
            InternetAddress address = (InternetAddress) recipients[i];
            String domain;
            try
            {
                domain = getDomain(address);
            }
            catch (ParseException e)
            {
                errors.add(new Status(Status.INVALID,address, e));
                continue;
            }
            Set<InternetAddress> hostAddresses = groups.get(domain);
            if (hostAddresses == null)
            {
                hostAddresses = new HashSet<InternetAddress>();
                groups.put(domain, hostAddresses);
            }
            hostAddresses.add(address);

        }
        return groups;
    }

    /**
     * Extracts the domain part of Internet mail addresses:
     * 
     * Handles the following types of addresses:
     *                                             [user]@[domain]
     *                                              "[Display Name]" <[user]@[domain]>
     * 
     * @returns the domain part of the email address.
     */
    private static String getDomain(InternetAddress address)
            throws ParseException
    {
        String str = address.getAddress().trim();
        int domainBegin = str.indexOf('@');
        if (domainBegin < 0)
            throw new ParseException("Missing '@' in email address " + str);
        String domain;
        if (str.endsWith(">"))
            domain = str.substring(domainBegin + 1, str.length() - 1).trim();
        else
            domain = str.substring(domainBegin + 1);
        return domain;
    }

    private static Pattern internetAddressPattern = Pattern.compile("(.*)\\s*<(.*)>");

    /**
     * Creates an InternetAddress from a String
     * 
     * @throws EngineException
     *             if the input String does not contain a valid E-mail address
     */
    public static InternetAddress getInternetAddress(String addressStr)
    {
        try
        {
        	InternetAddress addressesUnicode[] = InternetAddress.parse(addressStr,false);

        	for (int i=0; i<addressesUnicode.length; i++)
        	{
	        	try
	        	{
	        		addressesUnicode[i] = new InternetAddress(addressesUnicode[i].getAddress(), addressesUnicode[i].getPersonal());
	        	}
	    		catch (UnsupportedEncodingException e)
	    		{
	                throw new RuntimeException("Invalid e-mail address '"
	                        + addressStr + "'", e);
	    		}
        	}

        	return addressesUnicode[0];
        }
        catch (AddressException e)
        {
            throw new RuntimeException("Invalid e-mail address '"
                    + addressStr + "'", e);
        }

    }
}
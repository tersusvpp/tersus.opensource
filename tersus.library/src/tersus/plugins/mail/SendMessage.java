/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.plugins.mail;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.URLName;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import sun.security.action.GetBooleanAction;
import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.ModelId;
import tersus.model.Role;
import tersus.runtime.ElementHandler;
import tersus.runtime.EngineException;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * A plugin for sending mail messages
 * 
 * The <Message>trigger contains must contain a structure with the addresses and content of the
 * message The <Server Settings> trigger may contain parameters for connecting to the mail server
 * 
 * @author Youval Bronicki
 * 
 */
public class SendMessage extends MailPlugin
{
	private static final String JNDI_PREFIX = "jndi:";

	private static final String SMTP_PREFIX = "smtp:";

	private static final Role MESSAGE = Role.get("<Message>");

	private static final Role RECIPIENTS = Role.get("<Recipients>");

	private static final Role SERVER = Role.get("<Server>");

	private static final Role USER = Role.get("<User>");

	private static final Role PASSWORD = Role.get("<Password>");

	private static final Role SENT = Role.get("<Sent>");

	private static final Role NOT_SENT = Role.get("<Not Sent>");

	private static final Role ENABLE_TLS = Role.get("<Enable TLS>");

	private static final Role PARITALLY_SENT = Role.get("<Partially Sent>");

	private SlotHandler messageTrigger;

	private SlotHandler serverTrigger;

	private SlotHandler userTrigger;

	private SlotHandler passwordTrigger;
	
	private SlotHandler enableTLSTrigger;

	private SlotHandler recipientsTrigger;

	private SlotHandler sentExit;

	private SlotHandler notSentExit;

	private SlotHandler partiallySentExit;

	private InstanceHandler statusHandler;
	private ElementHandler addressElement;
	private ElementHandler deliveryStatusElement;
	private ElementHandler messageElement;
	private ElementHandler deliveryMessageElement;
	private ElementHandler sentAddressesElement;
	private ElementHandler unsentAddressesElement;
	private ElementHandler invalidAddressesElement;

	private static final ModelId DELIVERY_STATUS_ID = new ModelId(
			"Common/Data Structures/Email/Delivery Status");

	private static final Role MESSAGE_ELEMENT = Role.get("Message");

	private static final Role SENT_ADDRESSES = Role.get("Sent Addresses");

	private static final Role UNSENT_ADDRESSES = Role.get("Unsent Addresses");

	private static final Role INVALID_ADDRESSES = Role.get("Invalid Addresses");

	private static final Role ADDRESS = Role.get("Address");

	private static final Role DELIVERY_STATUS = Role.get("Delivery Status");

	private static final Role DELIVERY_MESSAGE = Role.get("Delivery Message");

	private InstanceHandler addressStatusHandler;

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.runtime.IFlowHandler#start(tersus.runtime.FlowState)
	 */

	public void start(RuntimeContext context, FlowInstance flow)
	{
		String server = getTriggerText(context, flow, serverTrigger);
		Session session = null;
		Properties props = new Properties();
		props.put("mail.smtp.connectiontimeout", "5000");
		props.put("mail.smtp.timeout", "10000");
		Transport transport = null;
		try
		{
			if (server != null)
			{
				try
				{
					if (server != null && server.startsWith(JNDI_PREFIX))
					{
						session = getJNDISession(context, server);
						transport = session.getTransport();
						transport.connect();
					}
					else
					{
						boolean enableTLS = getTriggerValue(enableTLSTrigger, context, flow, getPluginVersion()>0 ? true : false);
						if (enableTLS)
							props.put("mail.smtp.starttls.enable", "true");
						URLName url = null;
						if (server.startsWith(SMTP_PREFIX))
						{
							url = new URLName(server);
							if (url.getUsername() != null)
								props.put("mail.smtp.auth", "true");
						}
						else
							url = new URLName(SMTP_PREFIX + "//" + server);

						String user = null;
						String password = null;
						if (userTrigger != null && passwordTrigger != null)
						{
							user = (String) userTrigger.get(context, flow);
							password = (String) passwordTrigger.get(context, flow);
						}
						if (user == null || password == null)
						{
							session = Session.getInstance(props);
							transport = session.getTransport(url);
							transport.connect();
						}
						else
						{
							props.put("mail.smtp.auth", "true");
							props.put("mail.transport.protocol", "smtp");
							session = Session.getInstance(props);
							transport = session.getTransport();
							transport.connect(url.getHost(), url.getPort(), user, password);
						}
					}
				}
				catch (NoSuchProviderException e)
				{
					throw new RuntimeException("Failed to send email message", e);
				}
				catch (MessagingException e)
				{
					throw new RuntimeException("Failed to send email message", e);
				}
			}
			else
			{
				session = Session.getInstance(props);
			}
			MimeMessage mimeMessage;
			mimeMessage = getMessage(context, flow, session);
			List status = null;
			InternetAddress[] recipients = getInternetAddresses(context, flow, recipientsTrigger);
			if (server == null)
				status = MailUtilities.deliver(session, mimeMessage, recipients);
			else
				status = MailUtilities.forward(transport, mimeMessage, recipients);

			reportStatus(context, flow, status);
		}
		finally
		{
			if (transport != null)
				try
				{
					transport.close();
				}
				catch (MessagingException e)
				{
					throw new RuntimeException("Error sending email", e);
				}
		}
	}

	/**
	 * @param context
	 * @param flow
	 * @param status
	 */
	private void reportStatus(RuntimeContext context, FlowInstance flow, List status)
	{
		int sent = 0;
		int unsent = 0;
		int invalid = 0;
		Object statusReport = statusHandler.newInstance(context);
		messageElement.set(context, statusReport, getMessage(context, flow));
		for (Iterator i = status.iterator(); i.hasNext();)
		{
			Status s = (Status) i.next();
			Object addressStatus = addressStatusHandler.newInstance(context);
			addressElement.set(context, addressStatus, s.getAddress().toString());
			String deliveryStatus = s.getType();
			deliveryStatusElement.set(context, addressStatus, deliveryStatus);
			deliveryMessageElement.set(context, addressStatus, s.getDeliveryMessage());
			if (deliveryStatus == Status.SENT)
			{
				sentAddressesElement.accumulate(context, statusReport, addressStatus);
				++sent;
			}
			else if (deliveryStatus == Status.UNSENT)
			{
				unsentAddressesElement.accumulate(context, statusReport, addressStatus);
				++unsent;
			}
			else if (deliveryStatus == Status.INVALID)
			{
				invalidAddressesElement.accumulate(context, statusReport, addressStatus);
				++invalid;
			}
		}
		if (invalid == 0 && unsent == 0)
			setExitValue(sentExit, context, flow, statusReport);
		else if (sent == 0)
			setExitValue(notSentExit, context, flow, statusReport);
		else
			setExitValue(partiallySentExit, context, flow, statusReport);
	}

	private MimeMessage getMessage(RuntimeContext context, FlowInstance flow, Session session)
	{
		String messageText = getMessage(context, flow);
		InputStream is;
		try
		{
			is = new ByteArrayInputStream(messageText.getBytes(MailUtilities.ENCODING));
		}
		catch (UnsupportedEncodingException e)
		{
			throw new RuntimeException(e); // We don't expect this exception
		}
		MimeMessage mimeMessage;
		try
		{
			mimeMessage = new MimeMessage(session, is);
		}
		catch (MessagingException e)
		{
			throw new RuntimeException("Bad Message Format", e);
		}
		return mimeMessage;
	}

	private String getMessage(RuntimeContext context, FlowInstance flow)
	{
		String messageText = getTriggerText(context, flow, messageTrigger);
		return messageText;
	}

	private Session getJNDISession(RuntimeContext context, String serverURL)
	{
		String jndiName = serverURL.substring(JNDI_PREFIX.length());
		try
		{
			Context initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			return (Session) envCtx.lookup(jndiName);
		}
		catch (NamingException e)
		{
			throw new EngineException("Failed to obtain mail sesssion '" + jndiName
					+ "' from jndi context", null, e);
		}
	}

	/**
	 * Converts a list of Strings to an array of InternetAddresses
	 * 
	 */
	private InternetAddress[] getInternetAddresses(RuntimeContext context, List list)
	{
		InternetAddress[] addresses = new InternetAddress[list.size()];
		for (int i = 0; i < list.size(); i++)
		{
			String addressStr = (String) list.get(i);
			addresses[i] = getInternetAddress(context, addressStr);
		}
		return addresses;
	}

	/**
	 * Creates an InternetAddress from a String
	 * 
	 * @throws EngineException
	 *             if the input String does not contain a valid E-mail address
	 */
	private InternetAddress getInternetAddress(RuntimeContext context, String addressStr)
	{
		try
		{
			return new InternetAddress(addressStr);
		}
		catch (AddressException e)
		{
			throw new EngineException("Invalid e-mail address '" + addressStr + "'", null, e);
		}
	}

	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);
		messageTrigger = getRequiredMandatoryTrigger(MESSAGE, Boolean.FALSE, BuiltinModels.TEXT_ID);
		serverTrigger = getNonRequiredTrigger(SERVER, Boolean.FALSE, BuiltinModels.TEXT_ID);
		userTrigger = getNonRequiredTrigger(USER, Boolean.FALSE, BuiltinModels.TEXT_ID);
		passwordTrigger = getNonRequiredTrigger(PASSWORD, Boolean.FALSE, BuiltinModels.TEXT_ID);
		enableTLSTrigger = getNonRequiredTrigger(ENABLE_TLS,Boolean.FALSE, BuiltinModels.BOOLEAN_ID);
		recipientsTrigger = getRequiredMandatoryTrigger(RECIPIENTS, Boolean.TRUE,
				BuiltinModels.TEXT_ID);

		sentExit = getRequiredExit(SENT, Boolean.FALSE, DELIVERY_STATUS_ID);
		notSentExit = getRequiredExit(NOT_SENT, Boolean.FALSE, DELIVERY_STATUS_ID);
		partiallySentExit = getRequiredExit(PARITALLY_SENT, Boolean.FALSE, DELIVERY_STATUS_ID);

		statusHandler = sentExit.getChildInstanceHandler();
		messageElement = statusHandler.getElementHandler(MESSAGE_ELEMENT);
		sentAddressesElement = statusHandler.getElementHandler(SENT_ADDRESSES);
		unsentAddressesElement = statusHandler.getElementHandler(UNSENT_ADDRESSES);
		invalidAddressesElement = statusHandler.getElementHandler(INVALID_ADDRESSES);
		addressStatusHandler = sentAddressesElement.getChildInstanceHandler();
		addressElement = addressStatusHandler.getElementHandler(ADDRESS);
		deliveryStatusElement = addressStatusHandler.getElementHandler(DELIVERY_STATUS);
		deliveryMessageElement = addressStatusHandler.getElementHandler(DELIVERY_MESSAGE);
	}

}
/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.mail;

import javax.mail.Address;
import javax.mail.MessagingException;
/**
 * @author Youval Bronicki
 *  
 */
public class Status
{
    public static String UNSENT = "Communication Problem";

    public static String INVALID = "Invalid Address";
    
    public static String SENT = "Sent";

    private String type;

    private Address address;

    private MessagingException exception;

    public Status(String type, Address address,
            MessagingException exception)
    {
        super();
        this.type = type;
        this.address = address;
        this.exception = exception;
    }

    public Address getAddress()
    {
        return address;
    }

    public MessagingException getException()
    {
        return exception;
    }

    /**
     * @return
     */
    public String getType()
    {
        return type;
    }

    private String getErrorMessage()
    {
        if (exception == null)
            return "";
        else
            return exception.getMessage();
    }

    /**
     * @return
     */
    public String getDetailedMessage()
    {
        return getAddress()+" : " + getType() + " [" +  
                 getErrorMessage()+"]";
    }

    /**
     * @return
     */
    public String getDeliveryMessage()
    {
        if (getException() == null)
            return null;
        else
            return getException().getMessage();
    }
}
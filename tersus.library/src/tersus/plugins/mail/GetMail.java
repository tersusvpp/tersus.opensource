/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.mail;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Message.RecipientType;

import tersus.InternalErrorException;
import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.BinaryValue;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.InstanceHandler;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;
import tersus.util.FileUtils;
import tersus.webapp.Engine;

/**
 * @author Liat Shiff
 */
public class GetMail extends Plugin
{
	// Triggers constants
	private static final Role CONNECTION_DETAILS = Role.get("<Connection Details>");
	private static final Role NUMBER_OF_MESSAGES = Role.get("<Number of Messages>");

	// Exits constants
	private static final Role TOTAL_MESSAGES = Role.get("<Total Messages>");
	private static final Role MESSAGES = Role.get("<Messages>");

	// Triggers
	private SlotHandler connectionDetailsTrigger;
	private SlotHandler numberOfMessagesTrigger;

	// Exits
	private SlotHandler totalMessagesExit;
	private SlotHandler messagesExit;

	private ElementHandler hostElementHandler;
	private ElementHandler userElementHandler;
	private ElementHandler passwordElementHandler;
	private ElementHandler protocolElementHandler;
	private ElementHandler folderElementHandler;
	private ElementHandler socketFactoryHandler;

	private ElementHandler messageNumberHandler;
	private ElementHandler fromHandler;
	private ElementHandler subjectHandler;
	private ElementHandler toHandler;
	private ElementHandler ccHandler;
	private ElementHandler bccHandler;
	private ElementHandler replyToHandler;
	private ElementHandler sentDateHandler;
	private ElementHandler receivedDateHandler;
	private ElementHandler contentHandler;
	private ElementHandler attachmentsHandler;

	@Override
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		connectionDetailsTrigger = getRequiredMandatoryTrigger(CONNECTION_DETAILS, null,
				ConnectionDetails.MAIL_CONNECTION_DETAILS_ID);
		if (connectionDetailsTrigger != null)
			getConnectionDetails();

		numberOfMessagesTrigger = getNonRequiredMandatoryTrigger(NUMBER_OF_MESSAGES, null,
				BuiltinModels.NUMBER_ID);

		messagesExit = getRequiredExit(MESSAGES, Boolean.TRUE,
				tersus.plugins.mail.Message.MESSAGE_ID);
		if (messagesExit != null)
		{
			InstanceHandler columnDescriptorHandler = messagesExit.getChildInstanceHandler();
			messageNumberHandler = columnDescriptorHandler
					.getElementHandler(tersus.plugins.mail.Message.MESSAGE_NUMBER);
			fromHandler = columnDescriptorHandler
					.getElementHandler(tersus.plugins.mail.Message.FROM);
			subjectHandler = columnDescriptorHandler
					.getElementHandler(tersus.plugins.mail.Message.SUBJECT);
			toHandler = columnDescriptorHandler.getElementHandler(tersus.plugins.mail.Message.TO);
			ccHandler = columnDescriptorHandler.getElementHandler(tersus.plugins.mail.Message.CC);
			bccHandler = columnDescriptorHandler.getElementHandler(tersus.plugins.mail.Message.BCC);
			replyToHandler = columnDescriptorHandler
					.getElementHandler(tersus.plugins.mail.Message.REPLY_TO);
			sentDateHandler = columnDescriptorHandler
					.getElementHandler(tersus.plugins.mail.Message.SENT_DATE);
			receivedDateHandler = columnDescriptorHandler
					.getElementHandler(tersus.plugins.mail.Message.RECEIVED_DATE);;
			contentHandler = columnDescriptorHandler
					.getElementHandler(tersus.plugins.mail.Message.CONTENT);
			attachmentsHandler = columnDescriptorHandler
					.getElementHandler(tersus.plugins.mail.Message.ATTACHMENTS);
		}
	}

	private void getConnectionDetails()
	{
		hostElementHandler = connectionDetailsTrigger.getChildInstanceHandler().getElementHandler(
				ConnectionDetails.HOST);
		userElementHandler = connectionDetailsTrigger.getChildInstanceHandler().getElementHandler(
				ConnectionDetails.USER);
		passwordElementHandler = connectionDetailsTrigger.getChildInstanceHandler()
				.getElementHandler(ConnectionDetails.PASSWORD);
		protocolElementHandler = connectionDetailsTrigger.getChildInstanceHandler()
				.getElementHandler(ConnectionDetails.PROTOCOL);
		folderElementHandler = connectionDetailsTrigger.getChildInstanceHandler()
				.getElementHandler(ConnectionDetails.FOLDER);
		socketFactoryHandler = connectionDetailsTrigger.getChildInstanceHandler()
				.getElementHandler(ConnectionDetails.SOCKET_FACTORY);
	}

	public void start(RuntimeContext context, FlowInstance flow)
	{
		Object connectionDetailsDescriptor = connectionDetailsTrigger.get(context, flow);
		String host = (String) hostElementHandler.get(context, connectionDetailsDescriptor);
		String user = (String) userElementHandler.get(context, connectionDetailsDescriptor);
		String password = (String) passwordElementHandler.get(context, connectionDetailsDescriptor);
		String protocol = (String) protocolElementHandler.get(context, connectionDetailsDescriptor);
		String folder = (String) folderElementHandler.get(context, connectionDetailsDescriptor);
		String socketFactory = (String) socketFactoryHandler.get(context,
				connectionDetailsDescriptor);

		javax.mail.Store store = null;

		try
		{
			Properties props = new Properties();
			props.setProperty("mail." + protocol + ".host", host);
			props.setProperty("mail." + protocol + ".enable", "true");

			/*
			 * if (socketFactory != null)
			 * java.security.Security.setProperty("ssl.SocketFactory.provider", socketFactory);
			 */
			javax.mail.Session sess = javax.mail.Session.getInstance(props);
			store = sess.getStore(protocol);

			store.connect(user, password);

			javax.mail.Folder folder1 = store.getFolder(folder);
			folder1.open(javax.mail.Folder.READ_WRITE);

			Message[] messages = folder1.getMessages();

			if (messages != null)
			{
				totalMessagesExit = getNonRequiredExit(TOTAL_MESSAGES, Boolean.FALSE,
						BuiltinModels.NUMBER_ID);

				if (totalMessagesExit != null)
					totalMessagesExit.setValue(context, flow, new Number(messages.length));

				Number numberOfMessages = new Number(-1); // Default value
				if (numberOfMessagesTrigger != null)
					numberOfMessages = (Number) numberOfMessagesTrigger.get(context, flow);

				messagesExit = getNonRequiredExit(MESSAGES, Boolean.TRUE,
						tersus.plugins.mail.Message.MESSAGE_ID);

				if (messagesExit != null)
					returnMessages(messages, numberOfMessages.value, context, flow);
			}

			folder1.close(true);
			store.close();
		}
		catch (MessagingException ex)
		{
			throw new ModelExecutionException("Error getting mail", ex);
		}
		finally
		{
			if (store != null)
				try
				{
					store.close();
				}
				catch (MessagingException e)
				{
					e.printStackTrace();
				}
		}
	}

	private void returnMessages(Message[] messages, double numberOfMessages,
			RuntimeContext context, FlowInstance flow)
	{
		double numberOfMessagesReturn;

		if (numberOfMessages == -1 || numberOfMessages > messages.length)
			numberOfMessagesReturn = messages.length;
		else
			numberOfMessagesReturn = numberOfMessages;

		for (int i = 0; i < numberOfMessagesReturn; i++)
		{
			getMessage(messages[i], context, flow);
		}
	}

	private void getMessage(Message msg, RuntimeContext context, FlowInstance flow)
	{
		try
		{
			Object messageDescriptor = messagesExit.getChildInstanceHandler().newInstance(context);
			messageNumberHandler.set(context,messageDescriptor,new Number(msg.getMessageNumber()));
			fromHandler.set(context, messageDescriptor, msg.getFrom()[0].toString());
			subjectHandler.set(context, messageDescriptor, msg.getSubject());
			setRecipients(msg, context, messageDescriptor, RecipientType.TO, toHandler);
			setRecipients(msg, context, messageDescriptor, RecipientType.CC, ccHandler);
			setRecipients(msg, context, messageDescriptor, RecipientType.BCC, bccHandler);
			replyToHandler.set(context, messageDescriptor, msg.getReplyTo()[0].toString());
			sentDateHandler.set(context, messageDescriptor, msg.getSentDate());
			receivedDateHandler.set(context, messageDescriptor, msg.getReceivedDate());
			contentHandler.set(context, messageDescriptor, readContent(msg));
			readAttachment(msg, context, messageDescriptor);

			accumulateExitValue(messagesExit, context, flow, messageDescriptor);
		}
		catch (MessagingException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String readContent(Part p) throws MessagingException, IOException
	{
		StringBuffer content = new StringBuffer("");
		// String disposition = null;

		String[] headers = p.getHeader("Content-Disposition");

		if (headers != null && headers.length != 0)
			return "";

		if (p.isMimeType("text/plain"))
			content.append((String) p.getContent());
		else if (p.isMimeType("multipart/*"))
		{
			Multipart mp = (Multipart) p.getContent();
			int count = mp.getCount();

			for (int i = 0; i < count; i++)
				content.append(readContent(mp.getBodyPart(i)));
		}
		else if (p.isMimeType("message/rfc822"))
			content.append(readContent((Part) p.getContent()));

		return content.toString();
	}

	private void readAttachment(Part msg, RuntimeContext context, Object messageDescriptor)
			throws MessagingException, IOException
	{
		InstanceHandler attachmentHandler = attachmentsHandler.getChildInstanceHandler();

		ElementHandler content = attachmentHandler.getElementHandler("Content");
		ElementHandler contentType = attachmentHandler.getElementHandler("Content Type");
		ElementHandler fileName = attachmentHandler.getElementHandler("File Name");

		Object contentObj = msg.getContent();
		if (contentObj instanceof Multipart)

		{
			Multipart mp = (Multipart) contentObj;

			for (int i = 0, n = mp.getCount(); i < n; i++)
			{
				Part part = mp.getBodyPart(i);

				String disposition = null;
				String[] headers = part.getHeader("Content-Disposition");

				if (headers != null && headers.length != 0)
					disposition = part.getDisposition();

				if (getPluginVersion()>0 ||
					(disposition != null)
						&& ((disposition.equals(Part.ATTACHMENT) || (disposition
								.equals(Part.INLINE)))))
				{
					Object attachment = attachmentHandler.newInstance(context);

					Object obj = part.getContent();

					byte[] fileContent = null;
					if (obj instanceof InputStream)
					{
						InputStream in = (InputStream) part.getContent();
						fileContent = FileUtils.readBytes(in, true);
						content.set(context, attachment, new BinaryValue(fileContent));
					}
					else if (obj instanceof String)
					{
						fileContent = ((String) obj).getBytes(Engine.ENCODING);
						content.set(context, attachment, new BinaryValue(fileContent));
					}

					contentType.set(context, attachment,
							part.getContentType() != null ? part.getContentType() : "");
					fileName.set(context, attachment,
							part.getFileName() != null ? part.getFileName() : "");
					attachmentsHandler.accumulate(context, messageDescriptor, attachment);
				}
			}
		}
		else if (contentObj instanceof String)
		{
			contentHandler.set(context, messageDescriptor, contentObj);
		}
		else if (contentObj !=null)
			throw new InternalErrorException("Unexpected content type "+contentObj.getClass().getName()); 
		
	}

	private void setRecipients(Message msg, RuntimeContext context, Object messageDescriptor,
			RecipientType type, ElementHandler handler) throws MessagingException
	{
		Address[] adds = msg.getRecipients(type);

		if (adds != null && adds.length != 0)
		{
			for (Address add : adds)
			{
				handler.accumulate(context, messageDescriptor, add.toString());
			}
		}
	}
}

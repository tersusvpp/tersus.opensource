/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.mail;

import tersus.model.ModelId;
import tersus.model.Role;

/**
 * @author Liat Shiff
 */
public abstract class ConnectionDetails
{
	public static final ModelId MAIL_CONNECTION_DETAILS_ID = new ModelId("Common/Templates/Mail/Connection Details");
    
	public static final Role HOST = Role.get("Host");
    public static final Role USER = Role.get("User");
    public static final Role PASSWORD = Role.get("Password");
    public static final Role PROTOCOL = Role.get("Protocol");
    public static final Role FOLDER = Role.get("Folder");
    public static final Role SOCKET_FACTORY = Role.get("Socket Factory");
}

/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.plugins.mail;

import java.util.Properties;

import javax.mail.Flags;
import javax.mail.Message;
import javax.mail.MessagingException;

import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.Role;
import tersus.runtime.ElementHandler;
import tersus.runtime.FlowInstance;
import tersus.runtime.ModelExecutionException;
import tersus.runtime.Number;
import tersus.runtime.Plugin;
import tersus.runtime.RuntimeContext;
import tersus.runtime.SlotHandler;

/**
 * @author Liat Shiff
 */
public class DeleteMail extends Plugin
{
	// Triggers constants
	private static final Role CONNECTION_DETAILS = Role.get("<Connection Details>");
	private static final Role NUMBER_OF_MESSAGES = Role.get("<Number of Messages>");
	private static final Role FIRST_MESSAGE_TO_DELETE = Role.get("<First Message to Delete>");

	// Exits constants
	private static final Role NUMBER_OF_MESSAGES_LEFT = Role.get("<Number of Messages Left>");

	// Triggers
	private SlotHandler numberOfMessagesToDeleteTrigger;
	private SlotHandler connectionDetailsTrigger;
	private SlotHandler firstMessageToDeleteTrigger;

	// Exits
	private SlotHandler numberOfMessagesLeftExit;

	private ElementHandler hostElementHandler;
	private ElementHandler userElementHandler;
	private ElementHandler passwordElementHandler;
	private ElementHandler protocolElementHandler;
	private ElementHandler folderElementHandler;
	private ElementHandler socketFactoryHandler;

	private String host;
	private String user;
	private String password;
	private String protocol;
	private String folder;
	private String socketFactory;

	@Override
	public void initializeFromModel(Model model)
	{
		super.initializeFromModel(model);

		connectionDetailsTrigger = getRequiredMandatoryTrigger(CONNECTION_DETAILS, null,
				ConnectionDetails.MAIL_CONNECTION_DETAILS_ID);

		if (connectionDetailsTrigger != null)
			getConnectionDetails();

		numberOfMessagesToDeleteTrigger = getNonRequiredMandatoryTrigger(NUMBER_OF_MESSAGES,
				null, BuiltinModels.NUMBER_ID);

		firstMessageToDeleteTrigger = getNonRequiredMandatoryTrigger(FIRST_MESSAGE_TO_DELETE,
				null, BuiltinModels.NUMBER_ID);

		numberOfMessagesLeftExit = getRequiredExit(NUMBER_OF_MESSAGES_LEFT, Boolean.FALSE,
				BuiltinModels.NUMBER_ID);
	}

	private void getConnectionDetails()
	{
		hostElementHandler = connectionDetailsTrigger.getChildInstanceHandler().getElementHandler(
				ConnectionDetails.HOST);
		userElementHandler = connectionDetailsTrigger.getChildInstanceHandler().getElementHandler(
				ConnectionDetails.USER);
		passwordElementHandler = connectionDetailsTrigger.getChildInstanceHandler()
				.getElementHandler(ConnectionDetails.PASSWORD);
		protocolElementHandler = connectionDetailsTrigger.getChildInstanceHandler()
				.getElementHandler(ConnectionDetails.PROTOCOL);
		folderElementHandler = connectionDetailsTrigger.getChildInstanceHandler()
				.getElementHandler(ConnectionDetails.FOLDER);
		socketFactoryHandler = connectionDetailsTrigger.getChildInstanceHandler()
				.getElementHandler(ConnectionDetails.SOCKET_FACTORY);
	}

	public void start(RuntimeContext context, FlowInstance flow)
	{
		Object connectionDetailsDescriptor = connectionDetailsTrigger.get(context, flow);
		host = (String) hostElementHandler.get(context, connectionDetailsDescriptor);
		user = (String) userElementHandler.get(context, connectionDetailsDescriptor);
		password = (String) passwordElementHandler.get(context, connectionDetailsDescriptor);
		protocol = (String) protocolElementHandler.get(context, connectionDetailsDescriptor);
		folder = (String) folderElementHandler.get(context, connectionDetailsDescriptor);
		socketFactory = (String) socketFactoryHandler.get(context,
				connectionDetailsDescriptor);

		javax.mail.Store store = null;
		javax.mail.Folder folder1 = null;

		try
		{
			Properties props = new Properties();
			props.setProperty("mail." + protocol + ".host", host);
			props.setProperty("mail." + protocol + ".enable", "true");

			/*
			 * if (socketFactory != null)
			 * java.security.Security.setProperty("ssl.SocketFactory.provider", socketFactory);
			 */
			javax.mail.Session sess = javax.mail.Session.getInstance(props);
			store = sess.getStore(protocol);

			store.connect(user, password);

			folder1 = store.getFolder(folder);
			folder1.open(javax.mail.Folder.READ_WRITE);

			Message[] messages = folder1.getMessages();
			double totalMessagesLeft = messages.length;

			Number firstMessageToDelete = new Number(1);
			if (firstMessageToDeleteTrigger != null)
				firstMessageToDelete = (Number) firstMessageToDeleteTrigger.get(context, flow);
			
			Number numberOfMessagesToDelete = new Number (1);
			if (numberOfMessagesToDeleteTrigger != null)
				numberOfMessagesToDelete = (Number) numberOfMessagesToDeleteTrigger.get(
					context, flow);

			double lastMessageToDelete =
					Math.min(numberOfMessagesToDelete.value+firstMessageToDelete.value-1, totalMessagesLeft);

			if ((firstMessageToDeleteTrigger != null) || (numberOfMessagesToDeleteTrigger != null))
				for (int i = (int) firstMessageToDelete.value-1; i < lastMessageToDelete; i++)
				{
					messages[i].setFlag(Flags.Flag.DELETED, true);
					totalMessagesLeft--;
				}

			Number totalMessageLeftObj = new Number(totalMessagesLeft);
			setExitValue(numberOfMessagesLeftExit, context, flow, totalMessageLeftObj);

			folder1.close(true);
			store.close();
		}
		catch (MessagingException ex)
		{
			throw new ModelExecutionException("Error getting mail", ex);
		}
		finally
		{
			try
			{
				if (store != null)
					store.close();
			}
			catch (MessagingException e)
			{
				e.printStackTrace();
			}
		}
	}
}

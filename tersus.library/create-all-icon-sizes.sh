#!/bin/bash

#
# Create all template icon sizes using imagemagick
#

# iconfiles=("largest.png" "large.png" "medium.png" "small.png" "tiny.png")
# iconsizes=(64 32 20 16 11)
iconfiles=("tiny.png" "small.png" "medium.png" "large.png" "largest.png")
iconsizes=(11 16 20 32 64)
oldiconsdir="oldicons"

find . -type d -print | sort | while read DIR
do
	DIR_COUNT=`ls -la "${DIR}"|grep \^d|wc -l`
	
	# no subdirs?
	if [ ${DIR_COUNT} -eq 2 ]
	then
		# has files
		FILE_COUNT=`ls -la "${DIR}"|grep \^\-|wc -l`
		if [ ${FILE_COUNT} -gt 0 ]
		then
			pushd "${DIR}" > /dev/null
			noicon=true
			
			# check if at least one icon file was found
			# and bacup all icon files to $oldiconsdir
			for if in ${!iconfiles[@]}
			do
				ifile=${iconfiles[if]}
				if [ -f $ifile ]
				then
					if ( $noicon )
					then
						mkdir $oldiconsdir
						noicon=false
					fi
					oldiconfile=$oldiconsdir/$ifile
					cp $ifile $oldiconfile
					touch -r $ifile $oldiconfile
				fi
			done

			if (! $noicon) # at least one icon file found
			then
				echo $PWD
				# loop through all icon file names to find missing icons
				for if in ${!iconfiles[@]}
				do
					ifile=${iconfiles[if]}
					if [ ! -f $ifile ] # check if icon file is missing
					then
						unset pdif
						isize=${iconsizes[if]} # get size of missing file
						# loop through all icon file names to find existing icons in backup folder
						for iof in ${!iconfiles[@]}
						do
							iofile=${iconfiles[iof]}
							oldiconfile=$oldiconsdir/$iofile
							if [ -f $oldiconfile ] # check if icon file exists in backup folder
							then
								idif=$((if-iof))	# get distance of missing icon from current backup file
								adif=${idif#-}		# e.g. medium is 1 from small and 2 from largest
								if [ -z "$pdif" ] || [ $adif -le $pdif ]	# check if current backup file is closer than previous files checked
								then										# we assume closer files are better resize options, and resizing from a smaller file is better than a bigger file
									echo converting $iofile to $ifile
									convert $oldiconfile -adaptive-resize $isize -adaptive-sharpen $isize $ifile
								fi
								pdif=$adif
							fi
						done
					fi
				done
				rm -rf $oldiconsdir
			fi
			popd  > /dev/null
		fi
	fi
done
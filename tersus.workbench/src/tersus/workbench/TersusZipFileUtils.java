package tersus.workbench;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;

import tersus.ProjectStructure;

public class TersusZipFileUtils
{
	public static byte[] prepareZipFile(IProject project, boolean includeDBFolder)
	{
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		ZipOutputStream zo = new ZipOutputStream(bo);
		try
		{
			project.refreshLocal(IResource.DEPTH_INFINITE, null);
			writeToZip(project, zo, includeDBFolder);
			zo.close();
		}
		catch (Exception e)
		{
			TersusWorkbench.error("Failed to create zip file", e);
			return null;
		}
		return bo.toByteArray();
	}

	public static void writeToZip(IResource res, ZipOutputStream zo, boolean includeDBFolder) throws IOException,
			CoreException
	{
		if (res.getName().equals(ProjectStructure.WORK) && res.getParent().equals(res.getProject()))
			return; // Ignoring the "work" folder
		if (!includeDBFolder && res.getName().equals(ProjectStructure.DATABASE) && res.getParent().equals(res.getProject()))
			return; // Ignoring the "database" folder

		if (res instanceof IFile)
		{
			ZipEntry entry = new ZipEntry(res.getFullPath().toString().substring(1));
			zo.putNextEntry(entry);
			byte[] readBuffer = new byte[8192];
			InputStream contentStream = ((IFile) res).getContents(true);
			try
			{
				int n;
				while ((n = contentStream.read(readBuffer)) > 0)
				{
					zo.write(readBuffer, 0, n);
				}
			}
			finally
			{
				if (contentStream != null)
				{
					contentStream.close();
				}
			}
			zo.closeEntry();
		}
		else if (res instanceof IContainer)
		{
			for (IResource member : ((IContainer) res).members())
			{
				writeToZip(member, zo, includeDBFolder);
			}
		}
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.workbench;

import tersus.model.ModelId;
import tersus.model.PackageId;

/**
 * @author Youval Bronicki
 */
public interface IRepositoryChangeListener
{
	void cacheCleared(WorkspaceRepository repository);

    void modelRemoved(WorkspaceRepository repository, ModelId modelId);
    void modelAdded(WorkspaceRepository repository, ModelId modelId);
    void modelRenamed(WorkspaceRepository repository, ModelId oldModelId, ModelId newModelId);
    
    void packageAdded(WorkspaceRepository repository, PackageId packageId);
    void packageRemoved(WorkspaceRepository repository, PackageId packageId);
    void packageRenamed(WorkspaceRepository repository, PackageId originalId, PackageId newId);
    void packageReadOnlyStatusChanged(WorkspaceRepository repository, PackageId packageId);

    void repositorySaved(WorkspaceRepository repository);
    void repositoryRefreshed(WorkspaceRepository repository);
}

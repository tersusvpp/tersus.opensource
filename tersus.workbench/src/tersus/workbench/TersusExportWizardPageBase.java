/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.workbench;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * @author Liat Shiff
 */
public abstract class TersusExportWizardPageBase extends WizardPage implements IWizardPage
{
//	protected WeakHashMap< Object, WeakReference<Label>> labels = new WeakHashMap< Object, WeakReference<Label>>();
	protected Combo projectCombo;
	
    protected Text locationText;
    
    protected Button browseButton;
    
    protected Composite container;
    
	protected TersusExportWizardPageBase(String pageName)
	{
		super(pageName);
	}
	
/*	public Label getLabel(Object o)
	{
		WeakReference<Label> r = labels.get(o);
		return r == null ? null : r.get();
	}*/
	public void createControl(Composite parent)
	{
		container = new Composite(parent, SWT.NULL);
        addComponents(container);
        setPageComplete(false);
        setControl(container);
	}
	
	protected void initialize()
    {
       setPageComplete(true);
    }
	
	protected void addComponents(final Composite container)
    {
        initLayout(container);
		initProjectCombo(container); 
        addLocation(container);
    }

	protected void initProjectCombo(final Composite container)
	{
		projectCombo = addCombo(container, "&Project:");
	}

	protected void initLayout(final Composite container)
	{
		GridLayout layout = new GridLayout();
        container.setLayout(layout);
        layout.numColumns = 3;
        layout.verticalSpacing = 9;
	}

	protected void addLocation(final Composite container)
	{
		Label label = new Label(container, SWT.NULL);
		label.setText("&Export Location:");
        
        locationText = new Text(container, SWT.BORDER | SWT.SINGLE);
        GridData gd1 = new GridData(GridData.FILL_HORIZONTAL);
        locationText.setLayoutData(gd1);
  
        locationText.setEnabled(true);
        browseButton = new Button(container, SWT.PUSH);
        browseButton.setText("Browse...");
        
        browseButton.addSelectionListener(new SelectionAdapter() 
        {
            public void widgetSelected(SelectionEvent e)
            {
                promptForLocation();
            }
        });
	}

	protected Text addText(final Composite container, String labelText)
	{
		Label label = new Label(container, SWT.NULL);
		label.setText(labelText);
        
        Text text= new Text(container, SWT.BORDER | SWT.SINGLE);

        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan=2;
        text.setLayoutData(gd);
//        putLabel(text, label);
		return text;
		
	}
/*
	private void putLabel(Object key, Label label)
	{
		labels.put(key, new WeakReference<Label>(label));
	}*/
	protected Combo addCombo(final Composite container, String labelText)
	{
		Label label = new Label(container, SWT.NULL);
		label.setText(labelText);
        
        
        Combo combo= new Combo(container, SWT.DROP_DOWN | SWT.READ_ONLY);
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan=2;
        combo.setLayoutData(gd);
//        putLabel(combo, label);
		return combo;
	}
	
	private FileDialog getFileDialog()
	{
		FileDialog dialog = new FileDialog(container.getShell()); 
		
		String location = locationText.getText();
        String dirPath = null;
        
        if (location != null)
        {
        	int index = location.lastIndexOf('/');
        	if (index > 0)
        		dirPath = location.substring(0, index+1);
        		
        }
        dialog.setFilterPath(dirPath);
        
        return dialog;
	}
	
	protected Text getLocationText()
	{
		return locationText;
	}
	
	protected Combo getProjectCombo()
	{
		return projectCombo;
	}
	
	protected abstract void initProjectComboBox();
	protected abstract void initLocationText();

	protected void promptForLocation()
	{
		FileDialog dialog = getFileDialog();
		locationText.setText(dialog.open());
	}
}

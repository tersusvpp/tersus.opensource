/************************************************************************************************
 * Copyright (c) 2003-2009 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. -  Initial API and implementation
 *************************************************************************************************/

package tersus.workbench;

/**
 * @author Liat Shiff
 */
public class ConfigurationConstants
{
	// Application Element
	public static final String APPLICATION_TAG = "Application";
	public static final String NAME_ATTR = "name";
	public static final String APPLICATION_ROOT_SYSTEM_ATTR = "rootSystem";
	public static final String APPLICATION_AUTHENTICATION_METHOD_ATTR = "authenticationMethod";
	public static final String LIBRARY_ATTR = "library";

	// Data Source Element
	public static final String DATA_SOURCE_TAG = "DataSource";
	public static final String FACTORY_TAG = "factory";
	public static final String DRIVER_CLASS_NAME_TAG = "driverClassName";
	public static final String USERNAME_TAG = "username";
	public static final String PASSWORD_TAG = "password";
	public static final String URL_TAG = "url";
	public static final String VALIDATION_QUERY_TAG = "validationQuery";

	// Resource Element
	public static final String RESOURCE_TAG = "Resource";
	public static final String AUTH_TAG = "auth";
	public static final String TYPE_TAG = "type";
	public static final String JOTM_TIMEOUT_TAG = "jotm.timeout";
	
	// Parameter Element
	public static final String PARAMETER_TAG = "Parameter";
	public static final String VALUE_ATTR = "value";
	
}

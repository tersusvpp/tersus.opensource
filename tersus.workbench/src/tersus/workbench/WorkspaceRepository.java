/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/

package tersus.workbench;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.mutable.MutableBoolean;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourceAttributes;
import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.widgets.Display;

import tersus.eclipse.Extensions;
import tersus.model.BuiltinPlugins;
import tersus.model.ISaveMonitor;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelException;
import tersus.model.ModelId;
import tersus.model.Package;
import tersus.model.PackageCache;
import tersus.model.PackageId;
import tersus.model.Repository;
import tersus.model.SaveFailedException;
import tersus.model.SimplePackageCache;
import tersus.model.WeakPackageCache;
import tersus.model.indexer.ElementEntry;
import tersus.model.indexer.RepositoryIndex;
import tersus.util.FileUtils;

public class WorkspaceRepository extends Repository
{
	private static Map<String, WorkspaceRepository> repositories = new HashMap<String, WorkspaceRepository>();

	private static final int MIN_DIRTY_MODELS_FOR_UPDATE_REPOSITORY_INDEX_PROGRESS_DIALOG = 100;

	private static final String LIBRARIES = "libraries";

	private IPropertyChangeListener preferenceListener;

	static synchronized public WorkspaceRepository getRepository(IProject project)
	{
		WorkspaceRepository repository = repositories.get(project.getName());
		if (repository == null)
		{
			repository = new WorkspaceRepository();
			repository.setRepositoryRoot(TersusWorkbench.getRepositoryRoot(project));
			repository.loadExtensionLibraries();
			repository.loadProjectLibraries(project);
			repositories.put(project.getName(), repository);
		}
		return repository;
	}

	private WorkspaceRepository()
	{
		initCachedPackages();
		hookPreferenceListener();
	}

	private void initCachedPackages()
	{
		if (Preferences.getBoolean(Preferences.AUTOMATIC_RELEASE_CACHE))
			cachedPackages = new WeakPackageCache();
		else
			cachedPackages = new SimplePackageCache();
	}

	private void hookPreferenceListener()
	{
		preferenceListener = new IPropertyChangeListener()
		{
			public void propertyChange(PropertyChangeEvent event)
			{
				PackageCache tempPackageCache = cachedPackages;

				Boolean automaticReleaseChace = Preferences
						.getBoolean(Preferences.AUTOMATIC_RELEASE_CACHE);

				if (automaticReleaseChace && cachedPackages instanceof SimplePackageCache)
					cachedPackages = new WeakPackageCache();
				else if (!automaticReleaseChace && cachedPackages instanceof WeakPackageCache)
					cachedPackages = new SimplePackageCache();

				tempPackageCache.fill(cachedPackages);
			}
		};
		TersusWorkbench.getDefault().getPreferenceStore()
				.addPropertyChangeListener(preferenceListener);
	}

	public void removeProjectFromRepositories(IProject project)
	{
		if (repositories != null && repositories.containsKey(project.getName()))
			repositories.remove(project.getName());
	}

	private void loadExtensionLibraries()
	{
		File[] libraryFiles = Extensions.getModelLibraries();
		for (int i = 0; i < libraryFiles.length; i++)
		{
			File libraryFile = libraryFiles[i];
			if (!(getProject().getName().equals("tersus.library")
					&& libraryFile.getName().equals("common.trl") && libraryFile
					.getParentFile().getName().equals("tersus.library")))
				addLibrary(libraryFile);
		}
	}

	private void loadProjectLibraries(IProject project)
	{
		IFolder folder = project.getFolder(LIBRARIES);
		if (folder.exists())
		{
			File f = folder.getLocation().toFile();
			for (File childFile : f.listFiles())
			{
				if (childFile.isFile())
					addLibrary(childFile);
			}
		}
	}

	public void refreshLibraries()
	{
		getLibraries().clear();
		clearLibraryPackageIds();

		loadExtensionLibraries();
		loadProjectLibraries(getProject());
	}

	private HashMap<String, ImageLocator> descriptorCache = new HashMap<String, ImageLocator>();

	public Model newModel(ModelId newModelId, ModelId templateModelId, boolean createElements)
	{
		Model model = super.newModel(newModelId, templateModelId, createElements);
		for (int i = 0; i < listeners.size(); i++)
		{
			listeners.get(i).modelAdded(this, newModelId);
		}
		return model;

	}

	public Model newModel(ModelId newModelId, Class modelClass, Map properties)
	{
		Model model = super.newModel(newModelId, modelClass, properties);
		for (int i = 0; i < listeners.size(); i++)
		{
			listeners.get(i).modelAdded(this, newModelId);
		}
		return model;
	}

	public Package newPackage(PackageId packageId)
	{
		Package pkg = super.newPackage(packageId);
		notifyNewPackage(packageId);
		return pkg;
	}

	public void notifyNewPackage(PackageId packageId)
	{
		for (int i = 0; i < listeners.size(); i++)
		{
			listeners.get(i).packageAdded(this, packageId);
		}
	}

	public void clearCache()
	{
		super.clearCache();
		descriptorCache.clear();
		for (int i = 0; i < listeners.size(); i++)
		{
			listeners.get(i).cacheCleared(this);
		}
	}

	IContainer repositoryRoot = null;

	private ArrayList<IRepositoryChangeListener> listeners = new ArrayList<IRepositoryChangeListener>();

	int referenceCount = 0;

	public IContainer getRepositoryRoot()
	{
		return repositoryRoot;
	}

	public void setRepositoryRoot(IContainer resource)
	{
		repositoryRoot = resource;
	}

	public byte[] read(String path) throws IOException
	{
		try
		{
			IFile file = getResourceFile(path);
			// TODO Handle files that exist in the workspace but not in the
			// filesystem
			if (!file.exists())
				return null;
			file.refreshLocal(IResource.DEPTH_ZERO, null);
			byte[] serialization = FileUtils.readBytes(file.getContents(true), true);
			return serialization;
		}
		catch (CoreException e)
		{
			TersusWorkbench.log(e);
			throw new ModelException("Failed to read file " + path + " :" + e, e);
		}
	}

	protected boolean exists(String path)
	{
		IFile file = getResourceFile(path);
		return file.exists();
	}

	/**
	 * renameModel - Change the id of a model (update cache, save renamed model with new id, delete
	 * old file).
	 * 
	 * @param model
	 * @param newModelId
	 */
	public void renameModel(Model model, ModelId newModelId, int indexInPackage)
	{
		ModelId oldModelId = model.getId();
		super.renameModel(model, newModelId, indexInPackage);
		for (int i = 0; i < listeners.size(); i++)
		{
			IRepositoryChangeListener listener = listeners.get(i);
			listener.modelRenamed(this, oldModelId, newModelId);
		}
	}

	public IProject getProject()
	{
		return repositoryRoot.getProject();
	}

	public IFile getResourceFile(String path)
	{
		IFile file = repositoryRoot.getFile(new Path(path));
		return file;
	}

	public IFolder getPackageFolder(PackageId packageId)
	{
		IFolder folder = repositoryRoot.getFolder(new Path(packageId.getPath()));
		return folder;
	}

	public Package getPackageByFile(IFile file)
	{
		Package p = null;

		String fullPath = file.getFullPath().toString();
		int surfixIndex = fullPath.indexOf(".trp");
		int prefix = fullPath.indexOf("models/") + 7;

		if (surfixIndex > -1)
		{
			String path = fullPath.substring(prefix, surfixIndex).toString();
			PackageId packageId = getPackageId(path);
			p = getPackage(packageId);
		}
		return p;
	}

	public boolean isFileReadOnly(PackageId packageId)
	{
		IFile file = getPackageFile(packageId);

		if (file != null)
			return file.isReadOnly();

		return false;
	}

	public boolean isPackageFolderReadOnly(PackageId packageId)
	{
		IFolder folder = getPackageFolder(packageId);
		ResourceAttributes attributes = folder.getResourceAttributes();

		if (attributes != null)
			return attributes.isReadOnly();

		return false;
	}

	public tersus.util.IProgressMonitor wrapProgressMonitor(final IProgressMonitor monitor)
	{
		return new tersus.util.IProgressMonitor()
		{

			public void beginTask(String name, int totalWork)
			{
				if (monitor != null)
					monitor.beginTask(name, totalWork);
			}

			public void done()
			{
				if (monitor != null)
					monitor.done();
			}

			public boolean isCancelled()
			{
				return monitor.isCanceled();
			}

			public void worked(int work)
			{
				if (monitor != null)
					monitor.worked(work);
			}

		};
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.model.Repository#getAllPackages()
	 */
	public List<PackageId> getAllPackages(IProgressMonitor monitor)
	{
		return getPackageHierarchy(null, monitor);
	}

	public RepositoryIndex getUpdatedRepositoryIndex(IProgressMonitor monitor)
	{
		RepositoryIndex index = null;
		if (repositoryIndexExists()
				&& getRepositoryIndexDirtyCount() < MIN_DIRTY_MODELS_FOR_UPDATE_REPOSITORY_INDEX_PROGRESS_DIALOG) // No
			// need
			// for
			// progress
			// dialog
			index = super.getUpdatedRepositoryIndex(wrapProgressMonitor(monitor));
		else
		{
			ProgressMonitorDialog dialog = null;

			try
			{
				if (monitor == null && Display.getCurrent() != null
						&& TersusWorkbench.getActiveWorkbenchShell() != null)
				{
					dialog = new ProgressMonitorDialog(TersusWorkbench.getActiveWorkbenchShell());
					dialog.open();
					monitor = dialog.getProgressMonitor();
				}
				if (monitor == null)
					monitor = new NullProgressMonitor();
				index = super.getUpdatedRepositoryIndex(wrapProgressMonitor(monitor));
				monitor.worked(1);
				monitor.done();
			}
			finally
			{
				if (dialog != null)
					dialog.close();
			}
		}
		return index;
	}

	public RepositoryIndex getUpdatedRepositoryIndex()
	{
		final RepositoryIndex[] a = new RepositoryIndex[1];
		Display d = Display.getDefault();
		d.syncExec(new Runnable()
		{

			public void run()
			{
				a[0] = getUpdatedRepositoryIndex((IProgressMonitor) null);
			}
		});

		return a[0];
	}

	/**
	 * @param packageId
	 * @param namePrefix
	 * @return
	 */
	public ModelId getNewModelId(PackageId packageId, String namePrefix)
	{
		ModelId id;
		String name = namePrefix;
		int i = 1;

		do
		{
			id = new ModelId(packageId, name);
			name = namePrefix + " " + ++i;
		}
		while (modelExists(id));

		return id;
	}

	protected void writeResource(String path, byte[] bytes) throws IOException
	{
		IFile file = getResourceFile(path);
		TersusWorkbench.write(file, bytes);

	}

	/**
	 * @param packageId
	 * @return
	 */
	public IFile getPackageFile(PackageId packageId)
	{
		return ((IContainer) getRepositoryRoot()).getFile(new Path(getPackageFilePath(packageId)));
	}

	/**
	 * Removes a model from the repository's cached (and from the containg parent) <b>WARNING: this
	 * method does not handle referential integrity (there may be other models referencing this
	 * model). </b>
	 * 
	 * @param model
	 */
	public void removeModel(Model model)
	{
		ModelId modelId = model.getId();
		super.removeModel(model);
		for (int i = 0; i < listeners.size(); i++)
		{
			listeners.get(i).modelRemoved(this, modelId);
		}
	}

	/**
	 * @param packageId
	 * @param name
	 * @return
	 */
	public PackageId getNewSubPackageId(PackageId packageId, String namePrefix)
	{
		PackageId id;
		String name = namePrefix;
		int i = 1;

		do
		{
			id = new PackageId(packageId, name);
			name = namePrefix + " " + ++i;
		}
		while (getPackage(id) != null);

		return id;
	}

	protected void deletePackageFile(PackageId packageId) throws IOException
	{
		try
		{
			if (getPackageFile(packageId).exists())
				getPackageFile(packageId).delete(true, null);
		}
		catch (CoreException e)
		{
			TersusWorkbench.log(e);
			throw new ModelException("Failed to delete package file for " + packageId + " :" + e, e);
		}
	}

	protected void deletePackageFolder(PackageId packageId) throws IOException
	{
		try
		{
			if (getPackageFolder(packageId).exists())
				getPackageFolder(packageId).delete(true, null);
		}
		catch (CoreException e)
		{
			TersusWorkbench.log(e);
			throw new ModelException("Failed to delete package folder for " + packageId + " :" + e,
					e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tersus.model.Repository#getSavedSubPackageIds(tersus.model.PackageId)
	 */
	protected List<PackageId> getSavedSubPackageIds(PackageId parent)
	{
		IContainer packageFolder = parent == null ? getRepositoryRoot() : getPackageFolder(parent);
		if (!packageFolder.exists())
			return Collections.emptyList();
		IResource[] children;
		try
		{
			children = packageFolder.members();
		}
		catch (CoreException e)
		{
			TersusWorkbench.log(e);
			return Collections.emptyList();
		}
		if (children.length == 0)
			return Collections.emptyList();
		ArrayList<PackageId> subPackageIds = new ArrayList<PackageId>();
		for (int i = 0; i < children.length; i++)
		{
			IResource childResource = children[i];
			String childName = childResource.getName();
			if (ignoredFiles.contains(childName))
				continue;
			PackageId subPackageId = null;
			if (childResource instanceof IFolder)
			{
				subPackageId = new PackageId(parent, childName);
			}
			else if (childResource instanceof IFile && childName.endsWith(PACKAGE_SUFFIX))
			{
				subPackageId = new PackageId(parent, childName.substring(0,
						childName.lastIndexOf(PACKAGE_SUFFIX)));
			}
			if (subPackageId != null && !subPackageIds.contains(subPackageId))
				subPackageIds.add(subPackageId);
		}
		return subPackageIds;
	}

	public boolean packageFolderExists(PackageId packageId)
	{
		return getPackageFolder(packageId).exists();
	}

	/**
	 * @param entry
	 */
	public void addRepositoryChangeListener(IRepositoryChangeListener listener)
	{
		if (!listeners.contains(listener))
			listeners.add(listener);
	}

	public void removeRepositoryChangeListener(IRepositoryChangeListener listener)
	{
		listeners.remove(listener);
	}

	public void addModel(Model model, int indexInPackage)
	{
		Package pkg = model.getPackage();
		pkg.addModel(model, indexInPackage, true);
		markModelDirtyInIndex(model.getModelId());

		for (int i = 0; i < listeners.size(); i++)
			listeners.get(i).modelAdded(this, model.getId());
	}

	/**
	 * @param id
	 * @return a list of ModelElements referring the given id
	 */
	public List<ModelElement> getReferences(ModelId id)
	{
		List<ModelElement> references = getUpdatedRepositoryIndex().getReferences(id);
		return references;
	}

	public List<ModelElement> getExternalReferences(PackageId packageId)
	{
		boolean includeInternalReferences = false;
		return getReferences(packageId, includeInternalReferences);
	}

	public List<ModelElement> getReferences(PackageId packageId, boolean includeInternalReferences)
	{
		List<ModelElement> references = getUpdatedRepositoryIndex().getReferences(packageId,
				includeInternalReferences);
		return references;
	}

	/**
	 * @param packageId
	 */
	private void addPackageHierarchy(Package root, List<Package> packageList)
	{
		packageList.add(root);
		for (PackageId subPackageId : getSubPackageIds(root.getPackageId()))
		{
			Package subPackage = getPackage(subPackageId);
			addPackageHierarchy(subPackage, packageList);
		}
	}

	/**
	 * Removes a package from the repository (includes sub-packages)
	 * 
	 * @param packageId
	 * @return a list containing the removed packages.
	 */
	public List<Package> removePackage(PackageId packageId)
	{
		List<Package> packages = new ArrayList<Package>();
		Package root = getPackage(packageId);
		addPackageHierarchy(root, packages);
		for (Package pkg : packages)
		{
			if (pkg != null)
			{
				removePackageFromCache(pkg);
			}
		}
		for (int i = 0; i < listeners.size(); i++)
		{
			listeners.get(i).packageRemoved(this, packageId);
		}
		return packages;
	}

	/**
	 * Restores a previously deleted hierarchy of packages
	 * 
	 * @param packageIds
	 *            a list of packages to be restored. It is assumed that the first id in the list is
	 *            the root of the hierarchy
	 * @return
	 */
	public void restorePackage(List<Package> packages)
	{
		Package root = (Package) packages.get(0);
		for (Package pkg : packages)
		{
			addPackageToCache(pkg);
		}
		for (int i = 0; i < listeners.size(); i++)
		{
			listeners.get(i).packageAdded(this, root.getPackageId());
		}

	}

	/**
	 * @param parent
	 * @param name
	 * @return
	 */
	public PackageId getSubPackageIdCaseInsensitive(PackageId parent, String name)
	{
		List<PackageId> siblings = getSubPackageIds(parent);
		for (PackageId packageId : siblings)
		{
			if (packageId.getName().compareToIgnoreCase(name) == 0)
				return packageId;
		}
		return null;
	}

	/**
	 * @param monitor
	 * @param originalPackageId
	 * @return
	 */
	public List<PackageId> getPackageHierarchy(PackageId rootId, IProgressMonitor monitor)
	{
		ArrayList<PackageId> packageIds = new ArrayList<PackageId>();
		addPackageHierarchy(rootId, packageIds, monitor);
		return packageIds;
	}

	/**
	 * @param rootId
	 * @param monitor
	 *            TODO
	 */
	private void addPackageHierarchy(PackageId rootId, List<PackageId> packageIds,
			IProgressMonitor monitor)
	{
		if (monitor != null && monitor.isCanceled())
			return;
		if (rootId != null)
			packageIds.add(rootId);
		List<PackageId> subPackageIds = getSubPackageIds(rootId);
		for (PackageId subPackageId : subPackageIds)
		{
			addPackageHierarchy(subPackageId, packageIds, monitor);
		}

	}

	public void renamePackage(PackageId originalPackageId, PackageId newPackageId)
	{
		List<ModelElement> references = getReferences(originalPackageId, true);
		List<PackageId> packageIds = getPackageHierarchy(originalPackageId, null);
		for (Iterator<PackageId> i = packageIds.iterator(); i.hasNext();)
		{
			PackageId pkgId = i.next();
			PackageId newId = getNewPackageId(originalPackageId, newPackageId, pkgId);
			movePackage(pkgId, newId, false);
		}
		for (ModelElement element : references)
		{
			renameReference(originalPackageId, newPackageId, element);
		}
		for (int i = 0; i < listeners.size(); i++)
		{
			listeners.get(i).packageRenamed(this, originalPackageId, newPackageId);
		}
	}

	private PackageId getNewPackageId(PackageId originalPackageId, PackageId newPackageId,
			PackageId pkgId)
	{
		return new PackageId(newPackageId.getPath()
				+ pkgId.getPath().substring(originalPackageId.getPath().length()));
	}

	public void renameReference(PackageId originalPackageId, PackageId newPackageId,
			ModelElement element)
	{
		ModelId refId = element.getRefId();
		String newRefIdStr = newPackageId.getPath()
				+ refId.getPath().substring(originalPackageId.getPath().length());
		ModelId newRefId = new ModelId(newRefIdStr);
		element.basicSetRefId(newRefId);
	}

	/**
	 * @param pkgId
	 * @param newId
	 * 
	 *            WARNING: This message does not handle references. It is assumed that references
	 *            are handled separately
	 */
	protected void movePackage(PackageId pkgId, PackageId newId, boolean notifyListeners)
	{
		super.movePackage(pkgId, newId, notifyListeners);
		if (notifyListeners)
			for (int i = 0; i < listeners.size(); i++)
			{
				listeners.get(i).packageRenamed(this, pkgId, newId);
			}
	}

	/**
	 * Returns the list of files in the gived package's folder (if exists). The list is the list of
	 * the files in the currently saved folder.
	 * 
	 * Note: To support explicit adding, moving and deleting files as part of the repository
	 * management mechanism, the implementation of this messsage should change: We will have to
	 * maintain the set of "pre-persisted" files, possibly in a similar way to the way we handle
	 * packages today. We will also have to decide what happens if a file is added, removed or
	 * modified externally (specifically if this happens after a package is moved and before it is
	 * saved).
	 * 
	 * 
	 * @param packageId
	 * @return
	 */
	public List<IFile> getFiles(PackageId packageId)
	{
		Package cachedPackage = getCachedPackage(packageId);

		List<IFile> files = Collections.emptyList();
		if (cachedPackage == null)
			return files;
		if (cachedPackage.getSavedPackageId() != null)
		{
			IFolder folder = getPackageFolder(cachedPackage.getSavedPackageId());
			try
			{
				if (folder.exists())
				{
					IResource[] members = folder.members();
					for (int i = 0; i < members.length; i++)
					{
						IResource member = members[i];
						if (member.getType() == IResource.FILE
								&& !member.getName().endsWith(PACKAGE_SUFFIX))
						{
							if (files.isEmpty())
								files = new ArrayList<IFile>();
							files.add((IFile) member);
						}
					}
				}
			}
			catch (CoreException e)
			{
				TersusWorkbench.log(e);
			}
		}
		return files;

	}

	/**
	 * @param originalPackageId
	 * @param newPackageId
	 */

	protected void move(Package pkg)
	{
		for (IFile originalFile : getFiles(pkg.getPackageId()))
		{
			IFile newFile = getPackageFolder(pkg.getPackageId()).getFile(
					new Path(originalFile.getName()));
			try
			{
				TersusWorkbench.createFolder(getPackageFolder(pkg.getPackageId()), true, null);
				originalFile
						.move(newFile.getFullPath(), false/* force */, false/* history */, null /*
																							 * progress
																							 * monitor
																							 */);
			}
			catch (CoreException e)
			{
				String message = MessageFormat.format(Errors.MOVE_FILE_ERROR_MESSAGE, new Object[]
				{ originalFile.getFullPath(), newFile.getFullPath() });
				TersusWorkbench.log(new Status(Status.ERROR, TersusWorkbench.getPluginId(),
						Errors.MOVE_FILE_ERROR_CODE, message, e));
			}
		}
	}

	public void saveAllModifiedPackages(final Runnable callback)
	{
		final WorkspaceRepository repository = this;
		WorkspaceJob job = new WorkspaceJob("Saving models")
		{

			public IStatus runInWorkspace(final IProgressMonitor progressMonitor)
					throws CoreException
			{
				IStatus status = Status.OK_STATUS;
				ISaveMonitor monitor = new ISaveMonitor()
				{

					public void begin(int numberOfPackages)
					{
						progressMonitor.beginTask("Saving packages", numberOfPackages);
					}

					public void saving(PackageId id)
					{
						progressMonitor.subTask(id.getPath());
						progressMonitor.worked(1);
					}

				};

				final MutableBoolean ok = new MutableBoolean(false);
				try
				{
					repository.saveAllModifiedPackages(monitor);
					ok.setValue(true);
				}
				catch (SaveFailedException e)
				{
					IStatus[] children = new IStatus[e.getErrors().length];
					for (int i = 0; i < children.length; i++)
						children[i] = new Status(Status.ERROR, TersusWorkbench.getPluginId(), 1,
								e.getErrors()[i].getMessage(), e.getErrors()[i]);
					status = new MultiStatus(TersusWorkbench.getPluginId(), 1, children,
							"Save failed", null);
				}
				Display.getDefault().asyncExec(new Runnable()
				{
					public void run()
					{
						if (ok.booleanValue())
							callback.run();
						repository.notifyRepositorySaved();
					}
				});
				if (progressMonitor.isCanceled())
					return Status.CANCEL_STATUS;
				return status;
			}

		};
		job.setRule(getRepositoryRoot());
		job.setUser(true);
		job.setPriority(Job.INTERACTIVE);
		job.schedule();

	}

	public boolean anyDatabaseRecordsModified()
	{
		HashSet<ModelId> scanned = new HashSet<ModelId>();

		for (Package modifiedPackage : modifiedPackageMap.values())
		{
			for (Model model : modifiedPackage.getModels(BuiltinPlugins.DATABASE_RECORD))
			{
				if (searchForModifiedModels(model, scanned))
					return true;
			}
		}
		return false;
	}

	private boolean searchForModifiedModels(Model root, Set<ModelId> scanned)
	{
		if (root == null)
			return false;
		if (root.isModified())
			return true;
		if (scanned.contains(root.getId()))
			return false;
		scanned.add(root.getId());
		for (ModelElement e : root.getElements())
		{
			if (searchForModifiedModels(e.getReferredModel(), scanned))
				return true;
		}
		return false;
	}

	public void notifyRepositorySaved()
	{
		for (int i = 0; i < listeners.size(); i++)
			listeners.get(i).repositorySaved(this);

	}

	public ImageLocator getImageLocator(String imagePath)
	{
		ImageLocator descriptor = descriptorCache.get(imagePath);
		if (descriptor == null)
		{
			descriptor = new ImageLocator(this, imagePath);
			descriptorCache.put(imagePath, descriptor);
		}
		return descriptor;
	}


	/**
	 * @param id
	 * @return
	 */
	public boolean isLibraryPackage(PackageId id)
	{
		if (getPackageFile(id).exists())
			return false;
		if (getPackageFolder(id).exists())
			return false;
		return getLibraryPackageIds().contains(id);
	}

	/**
	 * @param monitor
	 * @return
	 */
	public Model[] getRootModels(IProgressMonitor monitor, boolean includeUnused)
	{
		RepositoryIndex index = getUpdatedRepositoryIndex(monitor);
		List<ModelId> sortedList = new ArrayList<ModelId>();
		if (includeUnused)
		{
			sortedList.addAll(index.getRootModelIds());
		}
		else
		{
			HashSet<ModelId> rootModelSet = new HashSet<ModelId>();
			rootModelSet.add(Configurator.getRootModel(getProject()).getId());
			rootModelSet.addAll(index.getAllModuleIds());
			sortedList.addAll(rootModelSet);
		}
		Collections.sort(sortedList, new Comparator<ModelId>()
		{

			public int compare(ModelId o1, ModelId o2)
			{
				return o1.getPath().compareTo(o2.getPath());
			}
		});
		Model[] rootModels = new Model[sortedList.size()];
		for (int i = 0; i < rootModels.length; i++)
		{
			rootModels[i] = getModel((ModelId) sortedList.get(i), true);
		}
		return rootModels;
	}

	/**
	 * Checks whetehr a model (and its entire element hierachy) is contained in library packages (as
	 * opposed to being an editable model in the current project)
	 * 
	 * @param monitor
	 *            TODO
	 * 
	 */
	private boolean isLibraryModel(Model model, Set<Model> checkedModels, IProgressMonitor monitor)
	{
		if (monitor != null && monitor.isCanceled())
			return false;
		if (checkedModels == null)
			checkedModels = new HashSet<Model>();
		if (checkedModels.contains(model))
			return true;
		checkedModels.add(model);
		if (!model.getPackage().isReadOnly())
			return false;
		List<ModelElement> elements = model.getElements();
		for (int i = 0; i < elements.size(); i++)
		{
			ModelElement element = (ModelElement) elements.get(i);
			Model referredModel = element.getReferredModel();
			if (referredModel != null && !isLibraryModel(referredModel, checkedModels, monitor))
				return false;
		}
		return true;
	}

	public Set<Model> getSubModels(Model rootModel, Set<Model> alreadyListed,
			boolean includeLibraryModels)
	{
		if (alreadyListed == null)
			alreadyListed = new HashSet<Model>();
		if (includeLibraryModels || !isLibraryModel(rootModel, null, null))
			if (!alreadyListed.contains(rootModel))
			{
				alreadyListed.add(rootModel);
				List<ModelElement> elements = rootModel.getElements();
				for (int j = 0; j < elements.size(); j++)
				{
					ModelElement element = (ModelElement) elements.get(j);
					if (element.getRefId() != null)
						getSubModels(element.getReferredModel(), alreadyListed,
								includeLibraryModels);
				}
			}

		return alreadyListed;
	}

	public Model[] getUnusedModels(PackageId ignoredPackage)
	{
		RepositoryIndex index = getUpdatedRepositoryIndex();
		HashSet<ModelId> usedModels = new HashSet<ModelId>();
		Model root = Configurator.getRootModel(getProject());
		if (root != null)
			addUsedModels(usedModels, index, root.getId());
		for (ModelId moduleId : index.getAllModuleIds())
		{
			addUsedModels(usedModels, index, moduleId);
		}
		HashSet<ModelId> unused = new HashSet<ModelId>();
		unused.addAll(index.getAllModelIds());
		unused.removeAll(usedModels);
		ArrayList<Model> unusedModels = new ArrayList<Model>(unused.size());
		for (ModelId id : unused)
		{
			if (ignoredPackage != null && ignoredPackage.isAncestorOf(id))
				continue;
			Model m = getModel(id, true);
			if (m != null && !m.isReadOnly())
				unusedModels.add(m);
		}
		return unusedModels.toArray(new Model[unusedModels.size()]);
	}

	public void addUsedModels(HashSet<ModelId> usedModels, RepositoryIndex index, ModelId modelId)
	{
		if (modelId.getPath().equals(
				"OMS/Debug/Requisitioning/Requisitioning Pane/Tree Pane/Set Background Style"))
			System.out.println("Found");
		if (usedModels.contains(modelId))
			return;
		usedModels.add(modelId);
		for (ElementEntry entry : index.getElementEntries(modelId))
		{
			if (entry.getRefId() != null)
				addUsedModels(usedModels, index, entry.getRefId());
		}

	}

	/**
	 * @return
	 */
	public List<IRepositoryChangeListener> getListeners()
	{
		return Collections.unmodifiableList(listeners);
	}

	public String toString()
	{
		return "Workspace Repository [" + getProject().getName() + "]";
	}

	/*
	 * Refresh all project entries (refreshing the repository) and refresh all subModels(refreshing
	 * the editor)
	 */
	public void refreshReadOnly(Package pkg)
	{
		if (pkg.isReadOnly() != pkg.getSavedReadOnly())
		{
			pkg.setSavedReadOnly(pkg.isReadOnly());
			for (int i = 0; i < listeners.size(); i++)
			{
				listeners.get(i).packageReadOnlyStatusChanged(this, pkg.getPackageId());
			}

			for (Model childModel : pkg.getModels())
			{
				childModel.fireReadOnlyStatusChanged();
			}
		}
	}

	public void notifyRepositoryRefreshed()
	{
		for (int i = 0; i < listeners.size(); i++)
		{
			listeners.get(i).repositoryRefreshed(this);
		}
	}

	public void dispose()
	{
		TersusWorkbench.getDefault().getPreferenceStore()
				.removePropertyChangeListener(preferenceListener);
	}
}
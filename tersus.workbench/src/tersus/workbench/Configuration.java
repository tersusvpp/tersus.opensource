/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. -  Initial API and implementation
 *************************************************************************************************/

package tersus.workbench;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.digester.Digester;
import org.apache.commons.digester.Rule;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;

import tersus.model.ModelId;
import tersus.util.Misc;
import tersus.util.XMLWriter;

/**
 * @author Youval Bronicki
 */
public class Configuration
{
	public static final String DATA_SOURCE_NAME = "name";
	public static final String TYPE_PROPERTY = "type";
	private String name;

	private File descriptor;

	private List<Properties> dataSources = new ArrayList<Properties>();
	private List<Properties> resources = new ArrayList<Properties>();
	private ModelId rootSystemId;

	private File repositoryRoot;
	private String authenticationMethod;
	private ArrayList<Parameter> parameters = new ArrayList<Parameter>();

	public static final String STARTED = "Started";

	public static final String STOPPED = "Stopped";

	public static final String PENDING = "Pending";

	private File databaseFolder;
	private boolean isLibrary = false;
	public static final String PROJECT = "project";
	public Map<String, Object> properties = new HashMap<String, Object>();

	public String getAuthenticationMethod()
	{
		return authenticationMethod;
	}

	public void setAuthenticationMethod(String authenticationMethod)
	{
		this.authenticationMethod = authenticationMethod;
	}

	public File getRepositoryRoot()
	{
		return repositoryRoot;
	}

	public void setRepositoryRoot(File repositoryRoot)
	{
		this.repositoryRoot = repositoryRoot;
	}

	public ModelId getRootSystemId()
	{
		return rootSystemId;
	}

	public void setRootSystem(String path)
	{
		setRootSystemId(new ModelId(path));
	}

	public void setRootSystemId(ModelId rootModelId)
	{
		this.rootSystemId = rootModelId;
	}

	public void addDataSource(Properties ds)
	{
		dataSources.add(ds);
	}

	public void addResource(Properties ds)
	{
		resources.add(ds);
	}

	public void addParameter(Parameter p)
	{
		parameters.add(p);
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getEscapedName()
	{
    	String escapedName = getName().replace(" ", "_");
    	
    	for (int i = 0; i < Misc.INVALID_FILENAME_CHARACTERS.length(); i++)
		{
			char currentChar = Misc.INVALID_FILENAME_CHARACTERS.charAt(i);
			if (escapedName.indexOf(currentChar) >= 0)
				escapedName = escapedName.replaceAll(String.valueOf(currentChar), "");		
		}
			
		return escapedName;
	}
	
	public boolean equals(Object obj)
	{
		if (obj instanceof Configuration)
		{
			Configuration other = (Configuration) obj;
			return Misc.equal(getRootSystemId(), other.getRootSystemId())
					&& Misc.equal(getRepositoryRoot(), other.getRepositoryRoot());
		}
		else
			return false;
	}

	public String toString()
	{
		return getName();
	}

	public File getDescriptor()
	{
		return descriptor;
	}

	public void setDescriptor(File descriptor)
	{
		this.descriptor = descriptor;
	}

	public static Configuration loadFromFile(File file)
	{
		Digester digester = new Digester();
		digester.setClassLoader(Configuration.class.getClassLoader());
		digester.addObjectCreate("Application", Configuration.class);
		digester.addSetProperties("Application");

		digester.addObjectCreate("Application/DataSource", Properties.class);
		digester.addRule("Application/DataSource", new Rule()
		{
			public void begin(String namespace, String name, org.xml.sax.Attributes attributes)
			{
				Properties dataSourceProperties = (Properties) getDigester().peek();
				for (int i = 0; i < attributes.getLength(); i++)
				{
					dataSourceProperties.put(attributes.getQName(i), attributes.getValue(i));
				}
			}
		});
		digester.addSetNext("Application/DataSource", "addDataSource");

		digester.addObjectCreate("Application/Resource", Properties.class);
		digester.addRule("Application/Resource", new Rule()
		{
			public void begin(String namespace, String name, org.xml.sax.Attributes attributes)
			{
				Properties resourceProperties = (Properties) getDigester().peek();
				for (int i = 0; i < attributes.getLength(); i++)
				{
					resourceProperties.put(attributes.getQName(i), attributes.getValue(i));
				}
			}
		});
		digester.addSetNext("Application/Resource", "addResource");

		digester.addObjectCreate("Application/Parameter", Parameter.class);
		digester.addSetProperties("Application/Parameter");
		digester.addSetNext("Application/Parameter", "addParameter");

		try
		{
			digester.parse(file);
		}
		catch (Exception e)
		{
			TersusWorkbench.log(e);
			return null;
		}
		return (Configuration) digester.getRoot();
	}

	public Properties findDataSourceProperties(String name)
	{
		for (int i = 0; i < dataSources.size(); i++)
		{
			Properties p = dataSources.get(i);
			if (name.equals(p.getProperty(DATA_SOURCE_NAME)))
				return p;
		}
		return null;
	}

	public void setDatabaseFolder(File folder)
	{
		databaseFolder = folder;
	}

	public File getDatabaseFolder()
	{
		return databaseFolder;
	}

	public String getContextPath()
	{
		return "/" + getEscapedName();
	}

	public List<Properties> getDataSources()
	{
		return dataSources;
	}

	public List<Properties> getResources()
	{
		return resources;
	}

	public ArrayList<Parameter> getParameters()
	{
		return parameters;
	}
	public void save(IFile configFile)
	{
		try
		{
			ByteArrayOutputStream outputBytes = new ByteArrayOutputStream();
			XMLWriter xmlWriter = new XMLWriter(outputBytes, "\n");

			xmlWriter.openTag(ConfigurationConstants.APPLICATION_TAG);

			if (getName() != null)
				xmlWriter.writeAttribute(ConfigurationConstants.NAME_ATTR, getName());

			if (getRootSystemId() != null && getRootSystemId().getPath() != null)
				xmlWriter.writeAttribute(ConfigurationConstants.APPLICATION_ROOT_SYSTEM_ATTR,
						getRootSystemId().getPath());

			if (getAuthenticationMethod() != null)
				xmlWriter.writeAttribute(
						ConfigurationConstants.APPLICATION_AUTHENTICATION_METHOD_ATTR,
						getAuthenticationMethod());
			if (isLibrary)
				xmlWriter.writeAttribute(ConfigurationConstants.LIBRARY_ATTR, "Yes");

			xmlWriter.closeTag();

			if (getDataSources() != null)
			{
				for (Properties properties : getDataSources())
				{
					xmlWriter.openTag(ConfigurationConstants.DATA_SOURCE_TAG);
					xmlWriter.writeAttribute(ConfigurationConstants.NAME_ATTR, properties
							.getProperty(ConfigurationConstants.NAME_ATTR));
					xmlWriter.writeAttribute(ConfigurationConstants.FACTORY_TAG, properties
							.getProperty(ConfigurationConstants.FACTORY_TAG));
					xmlWriter.writeAttribute(ConfigurationConstants.DRIVER_CLASS_NAME_TAG,
							properties.getProperty(ConfigurationConstants.DRIVER_CLASS_NAME_TAG));
					xmlWriter.writeAttribute(ConfigurationConstants.USERNAME_TAG, properties
							.getProperty(ConfigurationConstants.USERNAME_TAG));
					xmlWriter.writeAttribute(ConfigurationConstants.PASSWORD_TAG, properties
							.getProperty(ConfigurationConstants.PASSWORD_TAG));
					xmlWriter.writeAttribute(ConfigurationConstants.URL_TAG, properties
							.getProperty(ConfigurationConstants.URL_TAG));
					xmlWriter.writeAttribute(ConfigurationConstants.VALIDATION_QUERY_TAG,
							properties.getProperty(ConfigurationConstants.VALIDATION_QUERY_TAG));

					xmlWriter.endElement();
				}
			}

			if (getResources() != null)
			{
				for (Properties properties : getDataSources())
				{
					xmlWriter.openTag(ConfigurationConstants.RESOURCE_TAG);
					xmlWriter.writeAttribute(ConfigurationConstants.NAME_ATTR, properties
							.getProperty(ConfigurationConstants.NAME_ATTR));
					xmlWriter.writeAttribute(ConfigurationConstants.AUTH_TAG, properties
							.getProperty(ConfigurationConstants.AUTH_TAG));
					xmlWriter.writeAttribute(ConfigurationConstants.TYPE_TAG, properties
							.getProperty(ConfigurationConstants.TYPE_TAG));
					xmlWriter.writeAttribute(ConfigurationConstants.FACTORY_TAG, properties
							.getProperty(ConfigurationConstants.FACTORY_TAG));
					xmlWriter.writeAttribute(ConfigurationConstants.JOTM_TIMEOUT_TAG, properties
							.getProperty(ConfigurationConstants.JOTM_TIMEOUT_TAG));

					xmlWriter.endElement();
				}
			}

			if (getParameters() != null && !getParameters().isEmpty())
			{
				for (Parameter parameter : getParameters())
				{
					xmlWriter.openTag(ConfigurationConstants.PARAMETER_TAG);
					xmlWriter.writeAttribute(ConfigurationConstants.NAME_ATTR, parameter.getName());
					xmlWriter.writeAttribute(ConfigurationConstants.VALUE_ATTR, parameter
							.getValue());

					xmlWriter.endElement();
				}
			}


			xmlWriter.endElement();
			xmlWriter.close();

			if (configFile != null)
			{
				TersusWorkbench.write(configFile, outputBytes.toByteArray());
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public boolean isLibrary()
	{
		return isLibrary;
	}

	public void setLibrary(boolean isLibrary)
	{
		this.isLibrary = isLibrary;
	}
}
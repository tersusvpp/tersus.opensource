/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. -  Initial API and implementation
 *************************************************************************************************/
 
package tersus.workbench;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import tersus.ProjectStructure;
import tersus.model.ModelException;

public class TersusWorkbench extends AbstractUIPlugin
{
	// Error codes 
	public static int INTERNAL_ERROR = 1;
	//The shared instance.
	private static TersusWorkbench plugin;

	//Resource bundle.
	private ResourceBundle resourceBundle;
	/**
	 * The constructor.
	 */
	public TersusWorkbench()
	{
		super();
		plugin = this;
		try
		{
			resourceBundle = ResourceBundle.getBundle("tersus.workbench.TersusCoreResources");
		}
		catch (MissingResourceException x)
		{
			resourceBundle = null;
		}
	}

	/**
	 * Returns the shared instance.
	 */
	public static TersusWorkbench getDefault()
	{
		return plugin;
	}

	/**
	 * Returns the workspace instance.
	 */
	public static IWorkspace getWorkspace()
	{
		return ResourcesPlugin.getWorkspace();
	}

	/**
	 * Returns the string from the plugin's resource bundle,
	 * or 'key' if not found.
	 */
	public static String getResourceString(String key)
	{
		ResourceBundle bundle = TersusWorkbench.getDefault().getResourceBundle();
		try
		{
			return bundle.getString(key);
		}
		catch (MissingResourceException e)
		{
			return key;
		}
	}

	/**
	 * Returns the plugin's resource bundle,
	 */
	public ResourceBundle getResourceBundle()
	{
		return resourceBundle;
	}



	/** 
	 *  Creates a folder described by an <code>IFolder</code>, automatically creating any missing parent folders.  
	 * @param folder an IFolder identifying the folder to be created
	 * @param force a flag controlling how to deal with resources that
	 *    are not in sync with the local file system
	 * @param monitor a progress monitor, or <code>null</code> if progress
	 *    reporting and cancellation are not desired
	 * @throws CoreException
	 */
	public static void createFolder(IFolder folder, boolean force, IProgressMonitor monitor) throws CoreException
	{
	    if (monitor == null)
	        monitor = new NullProgressMonitor();
		if (folder.exists())
			return;
		if (!folder.getParent().exists())
			 createFolder((IFolder)folder.getParent(),force, new SubProgressMonitor(monitor,1));
		folder.create(force, true, new SubProgressMonitor(monitor,1));
	}

	public static IWorkbenchWindow getActiveWorkbenchWindow()
	{
		return getDefault().getWorkbench().getActiveWorkbenchWindow();
	}

	public static Shell getActiveWorkbenchShell()
	{
		IWorkbenchWindow activeWorkbenchWindow = getActiveWorkbenchWindow();
		if (activeWorkbenchWindow != null)
		    return activeWorkbenchWindow.getShell();
		else
		    return null;
	}

	public static String getPluginId()
	{
		return getDefault().getBundle().getSymbolicName();
	}

	public static void log(IStatus status)
	{
		getDefault().getLog().log(status);
	}
	public static void log(Throwable e)
	{
		if (e instanceof CoreException)
		{
			log(((CoreException )e).getStatus());
		}
		log(new Status(IStatus.ERROR, getPluginId(), INTERNAL_ERROR, TersusMessages.Internal_Error, e)); //$NON-NLS-1$
	}

	/**
	 * A utility method that checks whether one resource is an ancestor of another
	 * @param ancestor the suspected ancestor resource
	 * @param child the suspected child resource
	 * @return true if 'parent' is a container and its path is a prefix of the child's path.
	 */
    public static boolean isAncestor(IResource ancestor, IResource child)
    {
        if (! (ancestor instanceof IContainer))
            return false;
        while (child != null)
        {
            if (child.equals(ancestor))
                return true;
            child = child.getParent();
        }
        return false;
    }

    public static IFolder getRepositoryRoot(IProject project)
    {
        return project
                .getFolder(ProjectStructure.MODELS);
    }

    public static IFolder getDatabaseRoot(IProject project)
    {
        return project
                .getFolder(ProjectStructure.DATABASE);
    }
    /**
     * @param project
     * @return
     */
    public static boolean isTersusProject(IProject project)
    {
        if ( project.isOpen())
        	return getRepositoryRoot(project).exists();
        else
        	return getRepositoryRoot(project).getLocation()!=null;   
    }

    public static Image getImage(Object key, ImageDescriptor descriptor)
    {
    	Map<Object, Image> images = getDefault().images;
    	if (key == null)
    	{
    		return null;
    	}
    	Image img = null;
    	Object obj = images.get(key);
    	if (obj != null)
    	{
    		img = (Image) obj;
    	}
    	else if (descriptor != null)
    	{
            img = descriptor.createImage(false);
    		images.put(key, img);
    	}
    	return img;
    }
    public static Image getImage(ImageDescriptor key)
    {
    	return getImage(key, key);
    }

    private Map<Object, Image> images = new HashMap<Object, Image>(11);


    void disposeImages()
    {
    	for (Image img: images.values())
    	{
    		if (img != null)
    			img.dispose();
    	}
    	images.clear();
    }

    public static void registerImage(Object key, Image image)
    {
        getDefault().images.put(key, image);
    }

    public static void warn(String message, Throwable e)
    {
        log (new Status(Status.ERROR,getDefault().getBundle().getSymbolicName(), 0, message, e));
    }
    
    public void start(BundleContext context) throws Exception 
    {
    	super.start(context);
//    	ReadOnlyChangeListener listener = new ReadOnlyChangeListener();
//    	listener.start();
    }
    
    public void stop(BundleContext context) throws Exception
    {
        disposeImages();
        super.stop(context);
    }

    public static void write(IFile file, byte[] bytes)
    {
    	ByteArrayInputStream contentStream = new ByteArrayInputStream(bytes);
    	write(file, contentStream);
    }
    
    public static void write(IFile file, InputStream input)
	{
		String path = file.getLocation().toString();
		try
		{
			if (file.exists())
				file.setContents(input, false/* force */, false/* history */, null);
			else
			{
				IContainer container = file.getParent();
				if (!container.exists())
				{
					if (container instanceof IFolder)
						TersusWorkbench.createFolder((IFolder) container, true, null);
					else
						throw new RuntimeException("Can't save file " + path + " (resource "
								+ container.getFullPath() + " does not exit)");
				}
				file.create(input, false, null);
			}
		}
		catch (CoreException e)
		{

			throw new ModelException("Failed to save file " + path, e);
		}
	}

    public static void log(String message, Exception e)
    {
        log(new Status(IStatus.ERROR, TersusWorkbench.getPluginId(), 1, message,e));
    }

    public static void error(final String message, Exception e)
    {
        final Status status = new Status(IStatus.ERROR, TersusWorkbench.getPluginId(), 1, message,e);
        Display.getDefault().asyncExec(new Runnable() {
        	public void run()
        	{
                ErrorDialog.openError(getActiveWorkbenchShell(), "Error", message, status);
                log(status);
        	}
        });
    }
    
    
}

package tersus.workbench;

import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;

public class ReadOnlyChangeListener implements IResourceChangeListener
{
	
	public void start()
    {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		workspace.addResourceChangeListener(this, IResourceChangeEvent.POST_CHANGE);
    }

	private void handleResourceDelta(IResourceDelta delta)
    {
        if ((delta.getKind() & IResourceDelta.CHANGED) != 0)
        {
        	System.out.println("resourceChanged");
        	System.out.println("delta.getKind() = " + delta.getKind());
        }

        IResourceDelta[] deltas = delta.getAffectedChildren();
        for (int i = 0; i < deltas.length; i++)
        {
            handleResourceDelta(deltas[i]);
        }
    }

	public void resourceChanged(IResourceChangeEvent event) 
	{
		handleResourceDelta(event.getDelta());
	}
}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.workbench;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;

/**
 * @author Youval Bronicki
 */
public class SelectionHelper
{
    public static Object getSelectedItem(ISelection selection, Class type)
    {
        if (!(selection instanceof IStructuredSelection))
            return null;
        if (((IStructuredSelection) selection).size() != 1)
            return null;
        Object selectedItem = ((IStructuredSelection) selection)
                .getFirstElement();
        if (type.isAssignableFrom(selectedItem.getClass()))
            return selectedItem;
        else
            return null;
    }
}
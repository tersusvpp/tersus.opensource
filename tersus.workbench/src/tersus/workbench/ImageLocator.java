/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/

package tersus.workbench;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;

import tersus.model.Library;
import tersus.model.ModelException;

import com.sun.mail.iap.ByteArray;

/**
 *  
 */
public class ImageLocator
{
	static Map descriptorCache = new HashMap();

	static String[] suffixes =
	{ "tiny.png", "small.png", "medium.png", "large.png", "largest.png" };

	Dimension[] dimensions;

	ImageDescriptor[] descriptors;

	/**
	 * @param imagePath
	 */
	ImageLocator(WorkspaceRepository repository, String imagePath)
	{
		this.imagePath = imagePath;
		this.repository = repository;
		load();
	}

	/**
     *  
     */
	private void load()
	{
		ArrayList dimensionList = new ArrayList();
		ArrayList descriptorList = new ArrayList();
		for (int i = 0; i < suffixes.length; i++)
		{
			// IPath path = imagePath.addFileExtension(suffixes[i]);
			String path = imagePath + "/" + suffixes[i];
			//URL imageURL = repository.getResourceURL(path);
			try
			{
				byte[] imageBytes = repository.getResource(path);
				if (imageBytes != null)
				{
					ImageData data=  new ImageData(new ByteArrayInputStream(imageBytes));
					ImageDescriptor descriptor = ImageDescriptor.createFromImageData(data);
					
					Object key = null;
					IFile localFile = repository.getResourceFile(path);
					if (localFile.exists())
					{
						key = localFile;
					}
					else
					{
						Library lib = repository.getLibrary(path);
						key = lib.getSourceFile().getCanonicalPath()+":"+path;
					}
					Image image = TersusWorkbench.getImage(key, descriptor);
					Dimension dimension = new Dimension(image.getBounds().width,
							image.getBounds().height);
					descriptorList.add(descriptor);
					dimensionList.add(dimension);
				}
			}
			catch (IOException e)
			{
				throw new ModelException("Failed to load image "+path, e);
			}
					
		}
		descriptors = (ImageDescriptor[]) descriptorList.toArray(new ImageDescriptor[descriptorList
				.size()]);
		dimensions = (Dimension[]) dimensionList.toArray(new Dimension[dimensionList.size()]);
	}

	private String imagePath;
	private WorkspaceRepository repository;

	public boolean imageExists()
	{
		return (dimensions.length > 0);
	}

	public Image getImage(int maxWidth, int maxHeight)
	{
		ImageDescriptor descriptor = getImageDescriptor(maxWidth, maxHeight);
		if (descriptor == null)
			return null;
		return TersusWorkbench.getImage(descriptor);
	}

	public Image get16x16Image()
	{
		Image image = getImage(16, 16);
		if (image == null)
			return null;
		int width = image.getBounds().width;
		int height = image.getBounds().height;
		if (width == 16 && height == 16)
			return image;
		String imageKey = imagePath.toString() + ".16x16";
		Image image16x16 = TersusWorkbench.getImage(imageKey, null);
		if (image16x16 != null)
			return image16x16;
		if (width <= 16 || height <= 16)
			image16x16 = createPaddedImage(image);
		else
			image16x16 = createScaledImage(image);
		TersusWorkbench.registerImage(imageKey, image16x16);
		return image16x16;
	}

	private Image createPaddedImage(Image image)
	{
		ImageData data = image.getImageData();
		int width = image.getBounds().width;
		int height = image.getBounds().height;
		int leftPadding = (17 - width) / 2;
		int topPadding = (15 - height) / 2;
		ImageData newData = new ImageData(16, 16, data.depth, data.palette);
		newData.transparentPixel = data.transparentPixel;
		newData.depth = data.depth;
		newData.maskPad = data.maskPad;
		newData.maskData = data.maskData;
		switch (data.getTransparencyType())
		{
			case SWT.TRANSPARENCY_PIXEL:
				for (int x = 0; x < 16; x++)
					for (int y = 0; y < 16; y++)
						newData.setPixel(x, y, newData.transparentPixel);
				break;
			case SWT.TRANSPARENCY_MASK:
				ImageData transparencyMask = new ImageData(16, 16, 1, bwPalette);
				ImageData originalMask = data.getTransparencyMask();
				for (int x = 0; x < width; x++)
					for (int y = 0; y < height; y++)
					{
						transparencyMask.setPixel(x + leftPadding, y + topPadding,
								originalMask.getPixel(x, y));
					}
				newData.maskPad = transparencyMask.scanlinePad;
				newData.maskData = transparencyMask.data;
				break;
			case SWT.TRANSPARENCY_ALPHA:
				for (int x = 0; x < 16; x++)
					for (int y = 0; y < 16; y++)
					{
						int x0 = x - leftPadding;
						int y0 = y - topPadding;

						if (x0 < 0 || x0 >= width || y0 < 0 || y0 >= height)
							newData.setAlpha(x, y, 0);
						else
							newData.setAlpha(x, y, data.getAlpha(x0, y0));
					}
				break;
		}
		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++)
			{
				newData.setPixel(x + leftPadding, y + topPadding, data.getPixel(x, y));
			}
		return new Image(Display.getCurrent(), newData);
	}

	private Image createScaledImage(Image image)
	{
		ImageData data = image.getImageData();
		return new Image(Display.getCurrent(), data.scaledTo(16, 16));
	}

	/**
	 * Returns an <code>ImageDescriptor</code> for an image that fits in the given dimenstion
	 * 
	 * @param maxWidth
	 *            - Maximum width, in pixels, for the desired image
	 * @param maxHeight
	 *            - Maximum height, in pixels, for the desired image
	 * @return An <code>ImageDescriptor</code>, or the smallest available image if there is no such
	 *         image in the specified folder
	 */

	public ImageDescriptor getImageDescriptor(int maxWidth, int maxHeight)
	{
		if (!imageExists())
			return null;
		for (int i = dimensions.length - 1; i > 0; i--)
		{
			Dimension dim = dimensions[i];
			if (dim.width <= maxWidth && dim.height <= maxHeight)
				return descriptors[i];
		}
		return descriptors[0];
	}

	static PaletteData bwPalette = new PaletteData(new RGB[]
	{ new RGB(0, 0, 0), new RGB(255, 255, 255) });
}
/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.workbench;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;

/**
 * @author Liat Shiff
 */
public class Preferences extends AbstractPreferenceInitializer
{
	public static final String AUTOMATIC_RELEASE_CACHE = "AUTOMATIC_RELEASE_CACHE";

	public void initializeDefaultPreferences()
	{
		TersusWorkbench.getDefault().getPluginPreferences().setDefault(AUTOMATIC_RELEASE_CACHE,
				false);
	}

	static public boolean getBoolean(String key)
	{
		return TersusWorkbench.getDefault().getPreferenceStore()
				.getBoolean(key);
	}
}

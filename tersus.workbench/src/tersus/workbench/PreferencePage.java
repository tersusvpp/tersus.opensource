/************************************************************************************************
 * Copyright (c) 2003-2010 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.workbench;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

/**
 * @author Liat Shiff
 */
public class PreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage
{
	public PreferencePage()
	{
		setPreferenceStore(TersusWorkbench.getDefault().getPreferenceStore());
	}

	protected void createFieldEditors()
	{
		addField(new BooleanFieldEditor(Preferences.AUTOMATIC_RELEASE_CACHE,
				"Use automatic release cache", getFieldEditorParent()));
	}

	public void init(IWorkbench workbench)
	{

	}
}

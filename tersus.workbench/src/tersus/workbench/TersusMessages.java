/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.workbench;

/**
 * @author Joel Bar-El
 *
 */
public class TersusMessages {
//TODO eliminate this class?
	public static String Internal_Error = "Internal Error";
	public static String See_Error_Log = "See Error Log";

}

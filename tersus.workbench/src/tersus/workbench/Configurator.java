/************************************************************************************************
 * Copyright (c) 2003-2023 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.workbench;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.runtime.CoreException;

import tersus.ProjectStructure;
import tersus.model.BuiltinProperties;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelException;
import tersus.model.ModelId;

/**
 * @author Youval Bronicki
 * 
 */
public class Configurator
{

	public static List<Configuration> findApplications()
	{
		IWorkspace workspace = TersusWorkbench.getWorkspace();
		IProject[] projects = workspace.getRoot().getProjects();
		List<Configuration> applications = new ArrayList<Configuration>();
		for (int i = 0; i < projects.length; i++)
		{
			IProject project = projects[i];
			if (project.isOpen() && TersusWorkbench.isTersusProject(project))
			{
				Configuration application = getConfiguration(project);
				if (application != null)
					applications.add(application);

			}

		}
		return applications;
	}

	// Need to change method name to 'getConfiguration'
	public static Configuration getConfiguration(IProject project)
	{
		Configuration application = null;
		IFile configFile = project.getFile(ProjectStructure.CONFIGURATION_XML);
		if (configFile.exists())
		{
			application = Configuration.loadFromFile(configFile.getLocation().toFile());
			if (application != null)
			{
				if (application.getRootSystemId() == null && getDefaultRootModel(project) != null)
					application.setRootSystemId(getDefaultRootModel(project).getId());
				application.setRepositoryRoot(TersusWorkbench.getRepositoryRoot(project)
						.getLocation().toFile());
				application.properties.put(Configuration.PROJECT, project);
				if (application.getName() == null)
					application.setName(project.getName());
			}

		}
		else
		{
			Model rootModel = getDefaultRootModel(project);
			if (null != rootModel)
			{
				application = new Configuration();
				application.setName(project.getName());
				application.setRootSystemId(rootModel.getId());
				application.setRepositoryRoot(TersusWorkbench.getRepositoryRoot(project)
						.getLocation().toFile());
				application.properties.put(Configuration.PROJECT, project);
				if (application.getAuthenticationMethod() == null)
				{
					application.setAuthenticationMethod((String)rootModel
							.getProperty(BuiltinProperties.AUTHENTICATION_METHOD));
					if (application.getAuthenticationMethod() == null
							&& scanForUserTable(rootModel, null))
						application.setAuthenticationMethod("JDBC");
				}
			}
		}
		if (application != null)
		{
			IFolder dbFolder = TersusWorkbench.getDatabaseRoot(project);
			/*
			if (!dbFolder.exists())
			{
				try
				{
					dbFolder.create(true, false, null);
				}
				catch (CoreException e)
				{
					TersusWorkbench.log(e);
					throw new RuntimeException("Failed to create database folder "
							+ dbFolder.getFullPath().toString()); // TODO better exception and maybe
																	// move this to
																	// ApplicationServer
				}
			}*/
			application.setDatabaseFolder(dbFolder.getLocation().toFile());
		}
		return application;
	}

	/**
	 * Scans the model hierarchy for the 'User_Roles' table (indicating that the application
	 * requires authentication
	 * 
	 * @param rootModel
	 * @param object
	 * @return
	 */
	private static boolean scanForUserTable(Model rootModel, HashSet<Model> scannedModels)
	{
		if (rootModel == null)
			return false;
		if (scannedModels == null)
			scannedModels = new HashSet<Model>();
		if (scannedModels.contains(rootModel))
			return false;
		scannedModels.add(rootModel);
		if ("User_Roles".equalsIgnoreCase((String) rootModel
				.getProperty(BuiltinProperties.TABLE_NAME)))
			return true;
		for (ModelElement e : rootModel.getElements())
		{
			try
			{
				if (scanForUserTable(e.getReferredModel(), scannedModels))
					return true;
			}
			catch (ModelException ex)
			{
				// Missing models shouldn't stop us here
			}
		}
		return false;
	}

	public static Model getRootModel(IProject project)
	{
		Configuration application = getConfiguration(project);
		if (application == null)
			return null;
		return WorkspaceRepository.getRepository(project).getModel(application.getRootSystemId(),true);
	}

	private static Model getDefaultRootModel(IProject project)
	{
		String name = project.getName();
		ModelId modelId = new ModelId(name + "/" + name); // FUNC2 compare
		// WorkspaceRepository.getUnusedModels()
		WorkspaceRepository repository = WorkspaceRepository.getRepository(project);
		Model rootModel = repository.getModel(modelId, true);
		return rootModel;
	}
}
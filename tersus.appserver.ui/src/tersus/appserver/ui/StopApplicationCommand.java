/************************************************************************************************
 * Copyright (c) 2003-2023 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/

package tersus.appserver.ui;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import tersus.appserver.ApplicationServer;
import tersus.appserver.ui.views.ApplicationServerView;
import tersus.editor.TersusEditor;
import tersus.workbench.Configuration;
import tersus.workbench.Configurator;

public class StopApplicationCommand extends AbstractHandler
{
	public StopApplicationCommand() // Constructor
	{
	}

	public Object execute(ExecutionEvent event) throws ExecutionException
	{
		// The command has been executed, so extract the needed information from the application context
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		TersusEditor targetEditor = (TersusEditor) window.getActivePage().getActiveEditor();
		
        if (targetEditor != null)
        {
            Job job = new Job("Stop application")
            {

                @Override
                protected IStatus run(IProgressMonitor monitor)
                {
                    Configuration app = Configurator.getConfiguration(targetEditor.getRepository().getProject());
                    if (app != null)
                    {
                        ApplicationServer server = ApplicationServerView.getServer();
                        monitor.beginTask("Stopping the application",1);
                        if (server.isStarted())
                        {
                            String status = server.getStatus(app.getContextPath());
                            if (status == ApplicationServer.AVAILABLE || status == ApplicationServer.NOT_AVAILABLE)
                            {
                                server.stop(app, true);
                                monitor.worked(1);
                            }
                        }
                    }
                    return Status.OK_STATUS;
                }

            };
            job.setUser(true);
            job.setPriority(Job.INTERACTIVE);
            job.schedule();
        }

        return null;
	}
}

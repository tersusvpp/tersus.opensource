/************************************************************************************************
 * Copyright (c) 2003-2023 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/

package tersus.appserver.ui;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.part.FileEditorInput;

import tersus.appserver.ui.views.ApplicationServerView;
import tersus.editor.TersusEditor;
import tersus.workbench.Configuration;
import tersus.workbench.Configurator;
import tersus.workbench.TersusWorkbench;

public class ShowApplicationLogCommand extends AbstractHandler
{
	public ShowApplicationLogCommand() // Constructor
	{
	}

	public Object execute(ExecutionEvent event) throws ExecutionException
	{
		// The command has been executed, so extract the needed information from the application context
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		TersusEditor targetEditor = (TersusEditor) window.getActivePage().getActiveEditor();

        if (targetEditor != null)
        {
            Configuration app = Configurator.getConfiguration(targetEditor.getRepository().getProject());
            IFile logFile = ApplicationServerView.getLogFile(app);
            if (logFile == null)
                showMessage("Log file not found");
            else
            {
                try
                {
                    TersusWorkbench.getActiveWorkbenchWindow().getActivePage().openEditor(new FileEditorInput(logFile),
                            "org.eclipse.ui.DefaultTextEditor");
                    return null;
                }
                catch (PartInitException e)
                {
                    showMessage("Failed to open log file " + logFile.getFullPath());
                }

            }
        }

        return null;
	}

    public static void showMessage(String message)
    {
        MessageDialog.openInformation(TersusWorkbench.getActiveWorkbenchShell(), "Message", message);
    }
}

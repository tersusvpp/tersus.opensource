/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
package tersus.appserver.ui;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;

/**
 * @author Youval Bronicki
 *
 */
public class Preferences extends AbstractPreferenceInitializer
{

	public static final String PREFERRED_PORT = "PREFERRED_PORT";
	public static final int DEFAULT_PREFERRED_PORT = 8080;
	/* (non-Javadoc)
     * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
     */
    public void initializeDefaultPreferences()
    {
        TersusAppServerUIPlugin.getDefault().getPluginPreferences().setDefault(PREFERRED_PORT, DEFAULT_PREFERRED_PORT);
    }
    public static String getString(String key)
    {
        String value = TersusAppServerUIPlugin.getDefault().getPreferenceStore().getString(key);
        return value;
    }
    public static int getInt(String key)
    {
        int value = TersusAppServerUIPlugin.getDefault().getPreferenceStore().getInt(key);
        return value;
    }
    /**
     * @param key
     * @param value
     */
    public static void setValue(String key, String value)
    {
        TersusAppServerUIPlugin.getDefault().getPreferenceStore().setValue(key, value);
    }
    public static void setValue(String key, int value)
    {
        TersusAppServerUIPlugin.getDefault().getPreferenceStore().setValue(key, value);
    }

}

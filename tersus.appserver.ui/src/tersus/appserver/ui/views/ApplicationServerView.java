/************************************************************************************************
 * Copyright (c) 2003-2008 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/

package tersus.appserver.ui.views;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.ViewPart;

import tersus.appserver.ApplicationServer;
import tersus.appserver.ApplicationServerListener;
import tersus.appserver.AppserverPlugin;
import tersus.appserver.ui.Preferences;
import tersus.appserver.ui.TersusAppServerUIPlugin;
import tersus.editor.TersusEditor;
import tersus.webapp.ContextPool;
import tersus.workbench.Configuration;
import tersus.workbench.Configurator;
import tersus.workbench.TersusWorkbench;

/**
 * This view provides functionality for controlling the embedded Tersus Application Server
 */

public class ApplicationServerView extends ViewPart
{
	private TableViewer viewer;

	@Override
	public void dispose()
	{
		if (appServerListener != null)
			getServer().removeListener(appServerListener);
		super.dispose();
	}

	private Action startApplicationAction;

	private Action stopAllAction;

	private Action refreshListAction;

	private ImageDescriptor startApplicationImageDescriptor = TersusAppServerUIPlugin
			.imageDescriptorFromPlugin(TersusAppServerUIPlugin.getPluginId(),
					"icons/startApplication.gif");

	private ImageDescriptor stopAllImageDescriptor = TersusAppServerUIPlugin
			.imageDescriptorFromPlugin(TersusAppServerUIPlugin.getPluginId(), "icons/stopAll.gif");

	private ImageDescriptor runningImageDescriptor = TersusAppServerUIPlugin
			.imageDescriptorFromPlugin(TersusAppServerUIPlugin.getPluginId(), "icons/running.gif");

	private ImageDescriptor idleImageDescriptor = TersusAppServerUIPlugin
			.imageDescriptorFromPlugin(TersusAppServerUIPlugin.getPluginId(), "icons/idle.gif");

	private ImageDescriptor errorImageDescriptor = TersusAppServerUIPlugin
			.imageDescriptorFromPlugin(TersusAppServerUIPlugin.getPluginId(), "icons/error.gif");

	private Action doubleClickAction;

	private ApplicationServerListener appServerListener;

	class ViewContentProvider implements IStructuredContentProvider
	{
		public void inputChanged(Viewer v, Object oldInput, Object newInput)
		{
		}

		public void dispose()
		{
		}

		public Object[] getElements(Object parent)
		{
			Configuration[] applications = Configurator.findApplications().toArray(
					new Configuration[]
					{});
			return applications;

		}
	}

	class ViewLabelProvider extends LabelProvider implements ITableLabelProvider
	{
		public String getColumnText(Object obj, int index)
		{
			return getText(obj);
		}

		public Image getColumnImage(Object obj, int index)
		{
			return getImage(obj);
		}

		public Image getImage(Object obj)
		{
			if (obj instanceof Configuration)
			{
				Configuration application = (Configuration) obj;
				if (getServer().isAvaiable(application))
					return TersusWorkbench.getImage(runningImageDescriptor);
				else if (getServer().isInError(application))
					return TersusWorkbench.getImage(errorImageDescriptor);
			}
			return TersusWorkbench.getImage(idleImageDescriptor);
		}
	}

	/**
	 * The constructor.
	 */
	public ApplicationServerView()
	{
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize it.
	 */
	public void createPartControl(Composite parent)
	{
		viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		viewer.setContentProvider(new ViewContentProvider());
		viewer.setLabelProvider(new ViewLabelProvider());
		viewer.setInput(getViewSite());
		makeActions();
		appServerListener = new ApplicationServerListener()
		{

			public void statusChanged(String contextPath)
			{
				if (!viewer.getControl().isDisposed())
					viewer.getControl().getDisplay().asyncExec(new Runnable()
					{
						public void run()
						{
							viewer.refresh();
						}
					});
			}

		};
		getServer().addListener(appServerListener);
		IActionBars actionBars = getViewSite().getActionBars();
		actionBars.updateActionBars();
		fillToolBar(actionBars.getToolBarManager());
		viewer.addDoubleClickListener(new IDoubleClickListener()
		{
			public void doubleClick(DoubleClickEvent event)
			{
				doubleClickAction.run();
			}
		});

	}

	private void fillToolBar(IToolBarManager manager)
	{
		manager.add(startApplicationAction);
		manager.add(stopAllAction);
		manager.add(refreshListAction);
	}

	private void makeActions()
	{

		startApplicationAction = new Action()
		{
			public void run()
			{
				Configuration app = getSelectedApplication();
				if (app != null)
				{
					startServer();
					getServer().stop(app, false);
					getServer().start(app, true, true);
				}
				else
				{
					MessageDialog.openInformation(getSite().getShell(), "Select an application",
							"You need to select an application first");
				}
			}
		};
		startApplicationAction.setText("Start Application");
		startApplicationAction.setToolTipText("Start the selected application");
		startApplicationAction.setImageDescriptor(startApplicationImageDescriptor);

		stopAllAction = new Action()
		{
			public void run()
			{
				getServer().shutdown();
			}
		};
		stopAllAction.setText("Stop All");
		stopAllAction.setToolTipText("Stop all applications");
		stopAllAction.setImageDescriptor(stopAllImageDescriptor);
		doubleClickAction = new Action()
		{
			public void run()
			{
				Configuration application = getSelectedApplication();
				launchBrowser(application, false);
			}

		};

		refreshListAction = new Action()
		{
			public void run()
			{
				viewer.refresh();
			}
		};
		refreshListAction.setText("Refresh");
		refreshListAction.setToolTipText("Refresh the list of applications");
		refreshListAction.setImageDescriptor(ImageDescriptor.createFromFile(TersusEditor.class,
				"icons/refresh.gif"));
	}

	private Configuration getSelectedApplication()
	{
		if (viewer.getSelection() instanceof IStructuredSelection)
		{
			IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
			if (selection.size() == 1)
				return (Configuration) selection.getFirstElement();
		}
		return null;
	}

	public static void showMessage(String message)
	{
		MessageDialog
				.openInformation(TersusWorkbench.getActiveWorkbenchShell(), "Message", message);
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus()
	{
		viewer.getControl().setFocus();
	}

	public static void launchBrowser(Configuration application, boolean secure)
	{
		if (application == null)
			return;
		String url = "";
		String errorMessage = null;
		if (!getServer().isStarted())
			errorMessage = "The application server is not running";
		else if (!getServer().isAvaiable(application))
		{
			if (getServer().getStatus(application.getContextPath()) == ApplicationServer.NOT_AVAILABLE)
			{
				IFile logFile = getLogFile(application);
				if (logFile == null)
					errorMessage = "Log file not found";
				else
				{
					try
					{
						showMessage("The application has not started properly - displaying server log file");
						TersusWorkbench
								.getActiveWorkbenchWindow()
								.getActivePage()
								.openEditor(new FileEditorInput(logFile),
										"org.eclipse.ui.DefaultTextEditor");
						return;
					}
					catch (PartInitException e)
					{
						errorMessage = "Failed to open log file " + logFile.getFullPath();
					}
				}
			}
			errorMessage = "The application '" + (application).getName() + "' is not available";
		}
		else
		{
			if (secure)
				url = "https://localhost:" + getServer().getSecurePort() + "/"
						+ (application).getEscapedName();
			else
				url = "http://localhost:" + getServer().getPort() + "/"
						+ (application).getEscapedName();
			TersusEditor.openBrowser(url);
		}
		if (errorMessage != null)
			showMessage(errorMessage);
	}

	public static void startServer()
	{
		getServer().setPreferredPort(Preferences.getInt(Preferences.PREFERRED_PORT));
		getServer().startup();
	}

	public static ApplicationServer getServer()
	{
		return AppserverPlugin.getDefault().getAppServer();
	}

	public static IFile getLogFile(Configuration application)
	{
		IProject project = (IProject) application.properties.get(Configuration.PROJECT);
		IFolder logFolder = project.getFolder(new Path("work/logs"));
		try
		{
			logFolder.refreshLocal(IResource.DEPTH_ONE, null);
			IFile logFile = logFolder.getFile(ContextPool.DEFAULT_LOG_FILE);
			if (logFile.exists())
				return logFile;
			else
			{
				TersusWorkbench.error("The application log file is missing", null);
				return null;
			}
		}
		catch (CoreException e)
		{
			TersusWorkbench.error("Failed to open log file", e);
			return null;
		}
	}
}
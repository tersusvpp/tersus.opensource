/************************************************************************************************
 * Copyright (c) 2003-2023 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/

package tersus.appserver.ui;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.FileEditorInput;

import tersus.appserver.ui.views.ApplicationServerView;
import tersus.editor.TersusEditor;
import tersus.workbench.Configuration;
import tersus.workbench.Configurator;
import tersus.workbench.TersusWorkbench;

public class ShowApplicationLogActionDelegate implements IEditorActionDelegate
{

    public TersusEditor targetEditor;

    public void setActiveEditor(IAction action, IEditorPart targetEditor)
    {
        this.targetEditor = (TersusEditor) targetEditor;

    }

    public void run(IAction action)
    {
        if (targetEditor != null)
        {
            Configuration app = Configurator.getConfiguration(targetEditor.getRepository().getProject());
            IFile logFile = ApplicationServerView.getLogFile(app);
            if (logFile == null)
                showMessage("Log file not found");
            else
            {
                try
                {
                    TersusWorkbench.getActiveWorkbenchWindow().getActivePage().openEditor(new FileEditorInput(logFile),
                            "org.eclipse.ui.DefaultTextEditor");
                    return;
                }
                catch (PartInitException e)
                {
                    showMessage("Failed to open log file " + logFile.getFullPath());
                }

            }
        }
    }

    public void selectionChanged(IAction action, ISelection selection)
    {
        // TODO Auto-generated method stub

    }

    public static void showMessage(String message)
    {
        MessageDialog.openInformation(TersusWorkbench.getActiveWorkbenchShell(), "Message", message);
    }

}

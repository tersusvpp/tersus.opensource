package tersus.appserver.ui;

import java.net.URL;
import java.net.URLConnection;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;

import tersus.appserver.AppserverPlugin;
import tersus.editor.TersusEditor;
import tersus.editor.preferences.Preferences;
import tersus.util.FileUtils;
import tersus.util.MultiPartFormOutputStream;
import tersus.workbench.TersusWorkbench;
import tersus.workbench.TersusZipFileUtils;

public class QuickHostingActionDelegate implements IEditorActionDelegate
{

    private static final String QUICK_HOSTING_ACTION_TEXT = "Quick Hosting";
    private static final String QUICK_HOSTING_UPLOAD_QUESTION= "To help you test your applications, Tersus offers free hosting (for a limited duration).\n\nDeployment is automatic and immediate.\n\nAre you sure you want to upload your project to a Tersus.com server?";
    TersusEditor editor = null;
    public void setActiveEditor(IAction action, IEditorPart targetEditor)
    {
        if (targetEditor instanceof TersusEditor)
            editor = (TersusEditor)targetEditor;
        else
            editor = null;
    }

    public void run(IAction action)
    {
        if (editor == null)
            return;
        if (!MessageDialog.openQuestion(editor.getEditorSite().getShell(), QUICK_HOSTING_ACTION_TEXT,
                QUICK_HOSTING_UPLOAD_QUESTION))
            return;
        UploadJob job = new UploadJob(editor.getRepository().getProject());
        job.setUser(true);
        job.setPriority(Job.SHORT);
        job.schedule();
    }

    public void selectionChanged(IAction action, ISelection selection)
    {
        // TODO Auto-generated method stub

    }
    private class UploadJob extends Job
    {
        private Display display;
        private IProject project;

        UploadJob(IProject project)
        {
            super("Uploading " + project.getName());
            this.project = project;
            display = Display.getCurrent();

        }

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.core.runtime.jobs.Job#run(org.eclipse.core.runtime.IProgressMonitor)
         */
        protected IStatus run(IProgressMonitor monitor)
        {
            byte[] zipBytes = TersusZipFileUtils.prepareZipFile(project, true);
            if (zipBytes == null)
                return Status.OK_STATUS;
            MultiPartFormOutputStream out = null;

            String nextURL = null;
            try
            {
                URL url = new URL(Preferences.getString(Preferences.HOSTING_UPLOAD_URL));
                // create a boundary string
                String boundary = MultiPartFormOutputStream.createBoundary();
                URLConnection urlConn = MultiPartFormOutputStream.createConnection(url);
                urlConn.setRequestProperty("Accept", "*/*");
                urlConn.setRequestProperty("Content-Type", MultiPartFormOutputStream.getContentType(boundary));
                urlConn.setRequestProperty("Connection", "Keep-Alive");
                urlConn.setRequestProperty("Cache-Control", "no-cache");
                out = new MultiPartFormOutputStream(urlConn.getOutputStream(), boundary);
                // write a text field element

                out.writeFile("File", "application/zip", project.getName() + ".zip", zipBytes);
                out.close();
                out = null;
                // read response from server
                nextURL = FileUtils.readString(urlConn.getInputStream(), urlConn.getContentEncoding(), true);
            }
            catch (Exception e)
            {
                String id = AppserverPlugin.getDefault().getBundle().getSymbolicName();
                return new Status(Status.ERROR,id, "Upload failed",e);
            }
            finally
            {
                if (out != null)
                {
                    try

                    {
                        out.close();
                    }
                    catch (Exception e)
                    {
                        TersusWorkbench.log(e);
                    }
                }
            }

            if (nextURL != null)
            {
                final String url = nextURL;
                display.syncExec(new Runnable()
                {

                    public void run()
                    {
                        TersusEditor.openBrowser(url);
                    }

                });
            }

            return Status.OK_STATUS;
        }
    }
}

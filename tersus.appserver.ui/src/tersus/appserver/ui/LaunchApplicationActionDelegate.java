/************************************************************************************************
 * Copyright (c) 2003-2023 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd. - Initial API and implementation
 *************************************************************************************************/

package tersus.appserver.ui;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;

import tersus.ProjectStructure;
import tersus.appserver.ApplicationServer;
import tersus.appserver.ui.views.ApplicationServerView;
import tersus.editor.TersusEditor;
import tersus.model.BuiltinModels;
import tersus.model.Model;
import tersus.model.ModelElement;
import tersus.model.ModelId;
import tersus.model.ModelUtils;
import tersus.model.indexer.RepositoryIndex;
import tersus.workbench.Configuration;
import tersus.workbench.Configurator;
import tersus.workbench.TersusWorkbench;

public class LaunchApplicationActionDelegate implements IEditorActionDelegate
{

    public TersusEditor targetEditor;

    public void setActiveEditor(IAction action, IEditorPart targetEditor)
    {
        this.targetEditor = (TersusEditor) targetEditor;
    }

    public void run(IAction action)
    {
    	if (targetEditor == null)
    	{
    		MessageDialog.openInformation(TersusWorkbench.getActiveWorkbenchShell(), "Select Project", "Please select a Tersus project by opening a model or focusing on the model editor area");
    	}
    	else
        {
            final Configuration app = Configurator.getConfiguration(targetEditor.getRepository().getProject());
            if (app == null)
            {
            	MessageDialog.openError(TersusWorkbench.getActiveWorkbenchShell(), "Error", "Failed to launch the application for project '"+targetEditor.getRepository().getProject().getFullPath().segment(0)+"'.\nPlease verify that '"+ProjectStructure.CONFIGURATION_XML+"' is valid.");
            	return;
            }
            Model rootModel = targetEditor.getRepository().getModel(app.getRootSystemId(),true);
            RepositoryIndex index = targetEditor.getRepository().getUpdatedRepositoryIndex();
			final boolean secure = index.calculateUIModelIds(rootModel, null).contains(BuiltinModels.SECRET_TEXT_ID);
        	Job job = new Job("Launch application")
            {

                @Override
                protected IStatus run(IProgressMonitor monitor)
                {
                    
                    if (app != null)
                    {
                        String message;
                        ApplicationServer server = ApplicationServerView.getServer();
                        int work;
                        if (!server.isStarted())
                        {
                            work = 3;
                            message = "Starting application server and launching browser";
                        }
                        else if (server.isAvaiable(app))
                        {
                            work = 1;
                            message = "Launching browser";
                        }
                        else if (server.isInError(app))
                        {
                            work = 3;
                            message = "Restarting application and launching browser";
                        }
                        else
                        {
                            work = 2;
                            message = "Starting application and launching browser";
                        }
                        monitor.beginTask(message, work);
                        if (!server.isStarted())
                        {
                            ApplicationServerView.startServer();
                            monitor.worked(1);
                        }

                        String status = server.getStatus(app.getContextPath());
                        boolean available = (status == ApplicationServer.AVAILABLE);
                        if (!available)
                        {
                            if (status == null)
                            {
                                server.start(app,true, true);
                                monitor.worked(1);
                            }
                            else
                            {
                                server.stop(app,false);
                                monitor.worked(1);
                                server.start(app, true, true);
                                monitor.worked(1);
                            }

                        }
                        Display d = ((TersusEditor) targetEditor).getDisplay();
                        final Configuration application = app;
                        d.syncExec(new Runnable()
                        {

                            public void run()
                            {
                                ApplicationServerView.launchBrowser(application, secure);
                            }
                        });
                    }
                    monitor.worked(1);

                    return Status.OK_STATUS;
                }

            };
            job.setUser(true);
            job.setPriority(Job.INTERACTIVE);
            job.schedule();
        }
    }
    
	public void selectionChanged(IAction action, ISelection selection)
    {
        // TODO Auto-generated method stub

    }

}

/************************************************************************************************
 * Copyright (c) 2003-2005 Tersus Software Ltd. and others.
 * All rights reserved.
 *
 * This program is made available under the terms of the GNU General Public License v2,
 * which is part of this distribution and is available at http://www.gnu.org/licenses/gpl.txt.
 *
 * Contributors:
 *     Tersus Software Ltd.� Initial API and implementation
 *************************************************************************************************/
 
package tersus.appserver.ui;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

/*
 * Preference Page for the Application Server View
 * @author Youval Bronicki
 *
 */public class PreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage
{

	public PreferencePage()
	{
		super(GRID);
		setPreferenceStore(TersusAppServerUIPlugin.getDefault().getPreferenceStore());
		setDescription("Settings for the Tersus Application Server view");
	}

	public void createFieldEditors()
	{
		addField( new IntegerFieldEditor(Preferences.PREFERRED_PORT, "Preferred Port", getFieldEditorParent()));
	}

	public void init(IWorkbench workbench)
	{
	}
}